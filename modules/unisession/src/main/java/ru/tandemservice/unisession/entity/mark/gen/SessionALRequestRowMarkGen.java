package ru.tandemservice.unisession.entity.mark.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.mark.SessionALRequestRow;
import ru.tandemservice.unisession.entity.mark.SessionALRequestRowMark;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Перезачитываемая оценка
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionALRequestRowMarkGen extends EntityBase
 implements INaturalIdentifiable<SessionALRequestRowMarkGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.mark.SessionALRequestRowMark";
    public static final String ENTITY_NAME = "sessionALRequestRowMark";
    public static final int VERSION_HASH = -1071462237;
    private static IEntityMeta ENTITY_META;

    public static final String L_REQUEST_ROW = "requestRow";
    public static final String L_CONTROL_ACTION = "controlAction";
    public static final String L_MARK = "mark";

    private SessionALRequestRow _requestRow;     // Строка заявления о переводе на ускоренное обучение
    private EppFControlActionType _controlAction;     // Форма итогового контроля
    private SessionMarkGradeValueCatalogItem _mark;     // Оценка (из шкалы оценок)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Строка заявления о переводе на ускоренное обучение. Свойство не может быть null.
     */
    @NotNull
    public SessionALRequestRow getRequestRow()
    {
        return _requestRow;
    }

    /**
     * @param requestRow Строка заявления о переводе на ускоренное обучение. Свойство не может быть null.
     */
    public void setRequestRow(SessionALRequestRow requestRow)
    {
        dirty(_requestRow, requestRow);
        _requestRow = requestRow;
    }

    /**
     * @return Форма итогового контроля. Свойство не может быть null.
     */
    @NotNull
    public EppFControlActionType getControlAction()
    {
        return _controlAction;
    }

    /**
     * @param controlAction Форма итогового контроля. Свойство не может быть null.
     */
    public void setControlAction(EppFControlActionType controlAction)
    {
        dirty(_controlAction, controlAction);
        _controlAction = controlAction;
    }

    /**
     * @return Оценка (из шкалы оценок).
     */
    public SessionMarkGradeValueCatalogItem getMark()
    {
        return _mark;
    }

    /**
     * @param mark Оценка (из шкалы оценок).
     */
    public void setMark(SessionMarkGradeValueCatalogItem mark)
    {
        dirty(_mark, mark);
        _mark = mark;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SessionALRequestRowMarkGen)
        {
            if (withNaturalIdProperties)
            {
                setRequestRow(((SessionALRequestRowMark)another).getRequestRow());
                setControlAction(((SessionALRequestRowMark)another).getControlAction());
            }
            setMark(((SessionALRequestRowMark)another).getMark());
        }
    }

    public INaturalId<SessionALRequestRowMarkGen> getNaturalId()
    {
        return new NaturalId(getRequestRow(), getControlAction());
    }

    public static class NaturalId extends NaturalIdBase<SessionALRequestRowMarkGen>
    {
        private static final String PROXY_NAME = "SessionALRequestRowMarkNaturalProxy";

        private Long _requestRow;
        private Long _controlAction;

        public NaturalId()
        {}

        public NaturalId(SessionALRequestRow requestRow, EppFControlActionType controlAction)
        {
            _requestRow = ((IEntity) requestRow).getId();
            _controlAction = ((IEntity) controlAction).getId();
        }

        public Long getRequestRow()
        {
            return _requestRow;
        }

        public void setRequestRow(Long requestRow)
        {
            _requestRow = requestRow;
        }

        public Long getControlAction()
        {
            return _controlAction;
        }

        public void setControlAction(Long controlAction)
        {
            _controlAction = controlAction;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof SessionALRequestRowMarkGen.NaturalId) ) return false;

            SessionALRequestRowMarkGen.NaturalId that = (NaturalId) o;

            if( !equals(getRequestRow(), that.getRequestRow()) ) return false;
            if( !equals(getControlAction(), that.getControlAction()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getRequestRow());
            result = hashCode(result, getControlAction());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getRequestRow());
            sb.append("/");
            sb.append(getControlAction());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionALRequestRowMarkGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionALRequestRowMark.class;
        }

        public T newInstance()
        {
            return (T) new SessionALRequestRowMark();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "requestRow":
                    return obj.getRequestRow();
                case "controlAction":
                    return obj.getControlAction();
                case "mark":
                    return obj.getMark();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "requestRow":
                    obj.setRequestRow((SessionALRequestRow) value);
                    return;
                case "controlAction":
                    obj.setControlAction((EppFControlActionType) value);
                    return;
                case "mark":
                    obj.setMark((SessionMarkGradeValueCatalogItem) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "requestRow":
                        return true;
                case "controlAction":
                        return true;
                case "mark":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "requestRow":
                    return true;
                case "controlAction":
                    return true;
                case "mark":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "requestRow":
                    return SessionALRequestRow.class;
                case "controlAction":
                    return EppFControlActionType.class;
                case "mark":
                    return SessionMarkGradeValueCatalogItem.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionALRequestRowMark> _dslPath = new Path<SessionALRequestRowMark>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionALRequestRowMark");
    }
            

    /**
     * @return Строка заявления о переводе на ускоренное обучение. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.mark.SessionALRequestRowMark#getRequestRow()
     */
    public static SessionALRequestRow.Path<SessionALRequestRow> requestRow()
    {
        return _dslPath.requestRow();
    }

    /**
     * @return Форма итогового контроля. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.mark.SessionALRequestRowMark#getControlAction()
     */
    public static EppFControlActionType.Path<EppFControlActionType> controlAction()
    {
        return _dslPath.controlAction();
    }

    /**
     * @return Оценка (из шкалы оценок).
     * @see ru.tandemservice.unisession.entity.mark.SessionALRequestRowMark#getMark()
     */
    public static SessionMarkGradeValueCatalogItem.Path<SessionMarkGradeValueCatalogItem> mark()
    {
        return _dslPath.mark();
    }

    public static class Path<E extends SessionALRequestRowMark> extends EntityPath<E>
    {
        private SessionALRequestRow.Path<SessionALRequestRow> _requestRow;
        private EppFControlActionType.Path<EppFControlActionType> _controlAction;
        private SessionMarkGradeValueCatalogItem.Path<SessionMarkGradeValueCatalogItem> _mark;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Строка заявления о переводе на ускоренное обучение. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.mark.SessionALRequestRowMark#getRequestRow()
     */
        public SessionALRequestRow.Path<SessionALRequestRow> requestRow()
        {
            if(_requestRow == null )
                _requestRow = new SessionALRequestRow.Path<SessionALRequestRow>(L_REQUEST_ROW, this);
            return _requestRow;
        }

    /**
     * @return Форма итогового контроля. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.mark.SessionALRequestRowMark#getControlAction()
     */
        public EppFControlActionType.Path<EppFControlActionType> controlAction()
        {
            if(_controlAction == null )
                _controlAction = new EppFControlActionType.Path<EppFControlActionType>(L_CONTROL_ACTION, this);
            return _controlAction;
        }

    /**
     * @return Оценка (из шкалы оценок).
     * @see ru.tandemservice.unisession.entity.mark.SessionALRequestRowMark#getMark()
     */
        public SessionMarkGradeValueCatalogItem.Path<SessionMarkGradeValueCatalogItem> mark()
        {
            if(_mark == null )
                _mark = new SessionMarkGradeValueCatalogItem.Path<SessionMarkGradeValueCatalogItem>(L_MARK, this);
            return _mark;
        }

        public Class getEntityClass()
        {
            return SessionALRequestRowMark.class;
        }

        public String getEntityName()
        {
            return "sessionALRequestRowMark";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
