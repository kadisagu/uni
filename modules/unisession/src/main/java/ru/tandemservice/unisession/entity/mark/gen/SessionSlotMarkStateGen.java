package ru.tandemservice.unisession.entity.mark.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unisession.entity.catalog.SessionMarkStateCatalogItem;
import ru.tandemservice.unisession.entity.mark.SessionSlotMarkState;
import ru.tandemservice.unisession.entity.mark.SessionSlotRegularMark;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Оценка студента в сессии (отметка)
 *
 * Показывает, что человек не получил оценку за контрольное мероприятие, в объекте указывается причина
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionSlotMarkStateGen extends SessionSlotRegularMark
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.mark.SessionSlotMarkState";
    public static final String ENTITY_NAME = "sessionSlotMarkState";
    public static final int VERSION_HASH = -1427102139;
    private static IEntityMeta ENTITY_META;

    public static final String L_VALUE = "value";

    private SessionMarkStateCatalogItem _value;     // Отметка

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Отметка. Свойство не может быть null.
     */
    @NotNull
    public SessionMarkStateCatalogItem getValue()
    {
        return _value;
    }

    /**
     * @param value Отметка. Свойство не может быть null.
     */
    public void setValue(SessionMarkStateCatalogItem value)
    {
        dirty(_value, value);
        _value = value;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof SessionSlotMarkStateGen)
        {
            setValue(((SessionSlotMarkState)another).getValue());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionSlotMarkStateGen> extends SessionSlotRegularMark.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionSlotMarkState.class;
        }

        public T newInstance()
        {
            return (T) new SessionSlotMarkState();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "value":
                    return obj.getValue();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "value":
                    obj.setValue((SessionMarkStateCatalogItem) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "value":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "value":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "value":
                    return SessionMarkStateCatalogItem.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionSlotMarkState> _dslPath = new Path<SessionSlotMarkState>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionSlotMarkState");
    }
            

    /**
     * @return Отметка. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.mark.SessionSlotMarkState#getValue()
     */
    public static SessionMarkStateCatalogItem.Path<SessionMarkStateCatalogItem> value()
    {
        return _dslPath.value();
    }

    public static class Path<E extends SessionSlotMarkState> extends SessionSlotRegularMark.Path<E>
    {
        private SessionMarkStateCatalogItem.Path<SessionMarkStateCatalogItem> _value;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Отметка. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.mark.SessionSlotMarkState#getValue()
     */
        public SessionMarkStateCatalogItem.Path<SessionMarkStateCatalogItem> value()
        {
            if(_value == null )
                _value = new SessionMarkStateCatalogItem.Path<SessionMarkStateCatalogItem>(L_VALUE, this);
            return _value;
        }

        public Class getEntityClass()
        {
            return SessionSlotMarkState.class;
        }

        public String getEntityName()
        {
            return "sessionSlotMarkState";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
