/* $Id$ */
package ru.tandemservice.unisession.component.student.StudentSessionTab;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author oleyba
 * @since 4/8/11
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setStudent(this.getNotNull(Student.class, model.getStudent().getId()));
    }
}
