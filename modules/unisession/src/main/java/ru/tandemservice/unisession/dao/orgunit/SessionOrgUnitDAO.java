package ru.tandemservice.unisession.dao.orgunit;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.dao.settings.IEppSettingsDAO;

/**
 * @author vdanilov
 */
public class SessionOrgUnitDAO extends UniBaseDao implements ISessionOrgUnitDAO {

    @Override public boolean canShowSessionOrgUnitTab(final Long orgUnitId) {
        return IEppSettingsDAO.instance.get().isGroupOrgUnit(orgUnitId);
    }

}
