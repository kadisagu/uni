package ru.tandemservice.unisession.entity.catalog;

import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import ru.tandemservice.unisession.entity.catalog.gen.UnisessionBulletinTemplateGen;

/**
 * Печатные шаблоны ведомостей модуля «Сессия»
 */
public class UnisessionBulletinTemplate extends UnisessionBulletinTemplateGen implements ITemplateDocument
{
}