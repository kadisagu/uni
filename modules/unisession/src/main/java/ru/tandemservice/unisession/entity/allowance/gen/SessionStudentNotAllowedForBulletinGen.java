package ru.tandemservice.unisession.entity.allowance.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unisession.entity.allowance.ISessionStudentNotAllowed;
import ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowedForBulletin;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Недопуск к сдаче по ведомости
 *
 * Недопуск студента к сдаче данной конкретной ведомости.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionStudentNotAllowedForBulletinGen extends EntityBase
 implements ISessionStudentNotAllowed, INaturalIdentifiable<SessionStudentNotAllowedForBulletinGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowedForBulletin";
    public static final String ENTITY_NAME = "sessionStudentNotAllowedForBulletin";
    public static final int VERSION_HASH = 181267;
    private static IEntityMeta ENTITY_META;

    public static final String L_BULLETIN = "bulletin";
    public static final String L_STUDENT = "student";
    public static final String P_CREATE_DATE = "createDate";
    public static final String P_REMOVAL_DATE = "removalDate";

    private SessionBulletinDocument _bulletin;     // Ведомость
    private Student _student;     // Студент
    private Date _createDate;     // Дата создания
    private Date _removalDate;     // Дата снятия

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Ведомость. Свойство не может быть null.
     */
    @NotNull
    public SessionBulletinDocument getBulletin()
    {
        return _bulletin;
    }

    /**
     * @param bulletin Ведомость. Свойство не может быть null.
     */
    public void setBulletin(SessionBulletinDocument bulletin)
    {
        dirty(_bulletin, bulletin);
        _bulletin = bulletin;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Дата создания.
     */
    public Date getCreateDate()
    {
        return _createDate;
    }

    /**
     * @param createDate Дата создания.
     */
    public void setCreateDate(Date createDate)
    {
        dirty(_createDate, createDate);
        _createDate = createDate;
    }

    /**
     * @return Дата снятия.
     */
    public Date getRemovalDate()
    {
        return _removalDate;
    }

    /**
     * @param removalDate Дата снятия.
     */
    public void setRemovalDate(Date removalDate)
    {
        dirty(_removalDate, removalDate);
        _removalDate = removalDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SessionStudentNotAllowedForBulletinGen)
        {
            if (withNaturalIdProperties)
            {
                setBulletin(((SessionStudentNotAllowedForBulletin)another).getBulletin());
                setStudent(((SessionStudentNotAllowedForBulletin)another).getStudent());
            }
            setCreateDate(((SessionStudentNotAllowedForBulletin)another).getCreateDate());
            setRemovalDate(((SessionStudentNotAllowedForBulletin)another).getRemovalDate());
        }
    }

    public INaturalId<SessionStudentNotAllowedForBulletinGen> getNaturalId()
    {
        return new NaturalId(getBulletin(), getStudent());
    }

    public static class NaturalId extends NaturalIdBase<SessionStudentNotAllowedForBulletinGen>
    {
        private static final String PROXY_NAME = "SessionStudentNotAllowedForBulletinNaturalProxy";

        private Long _bulletin;
        private Long _student;

        public NaturalId()
        {}

        public NaturalId(SessionBulletinDocument bulletin, Student student)
        {
            _bulletin = ((IEntity) bulletin).getId();
            _student = ((IEntity) student).getId();
        }

        public Long getBulletin()
        {
            return _bulletin;
        }

        public void setBulletin(Long bulletin)
        {
            _bulletin = bulletin;
        }

        public Long getStudent()
        {
            return _student;
        }

        public void setStudent(Long student)
        {
            _student = student;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof SessionStudentNotAllowedForBulletinGen.NaturalId) ) return false;

            SessionStudentNotAllowedForBulletinGen.NaturalId that = (NaturalId) o;

            if( !equals(getBulletin(), that.getBulletin()) ) return false;
            if( !equals(getStudent(), that.getStudent()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getBulletin());
            result = hashCode(result, getStudent());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getBulletin());
            sb.append("/");
            sb.append(getStudent());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionStudentNotAllowedForBulletinGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionStudentNotAllowedForBulletin.class;
        }

        public T newInstance()
        {
            return (T) new SessionStudentNotAllowedForBulletin();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "bulletin":
                    return obj.getBulletin();
                case "student":
                    return obj.getStudent();
                case "createDate":
                    return obj.getCreateDate();
                case "removalDate":
                    return obj.getRemovalDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "bulletin":
                    obj.setBulletin((SessionBulletinDocument) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "createDate":
                    obj.setCreateDate((Date) value);
                    return;
                case "removalDate":
                    obj.setRemovalDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "bulletin":
                        return true;
                case "student":
                        return true;
                case "createDate":
                        return true;
                case "removalDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "bulletin":
                    return true;
                case "student":
                    return true;
                case "createDate":
                    return true;
                case "removalDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "bulletin":
                    return SessionBulletinDocument.class;
                case "student":
                    return Student.class;
                case "createDate":
                    return Date.class;
                case "removalDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionStudentNotAllowedForBulletin> _dslPath = new Path<SessionStudentNotAllowedForBulletin>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionStudentNotAllowedForBulletin");
    }
            

    /**
     * @return Ведомость. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowedForBulletin#getBulletin()
     */
    public static SessionBulletinDocument.Path<SessionBulletinDocument> bulletin()
    {
        return _dslPath.bulletin();
    }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowedForBulletin#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Дата создания.
     * @see ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowedForBulletin#getCreateDate()
     */
    public static PropertyPath<Date> createDate()
    {
        return _dslPath.createDate();
    }

    /**
     * @return Дата снятия.
     * @see ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowedForBulletin#getRemovalDate()
     */
    public static PropertyPath<Date> removalDate()
    {
        return _dslPath.removalDate();
    }

    public static class Path<E extends SessionStudentNotAllowedForBulletin> extends EntityPath<E>
    {
        private SessionBulletinDocument.Path<SessionBulletinDocument> _bulletin;
        private Student.Path<Student> _student;
        private PropertyPath<Date> _createDate;
        private PropertyPath<Date> _removalDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Ведомость. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowedForBulletin#getBulletin()
     */
        public SessionBulletinDocument.Path<SessionBulletinDocument> bulletin()
        {
            if(_bulletin == null )
                _bulletin = new SessionBulletinDocument.Path<SessionBulletinDocument>(L_BULLETIN, this);
            return _bulletin;
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowedForBulletin#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Дата создания.
     * @see ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowedForBulletin#getCreateDate()
     */
        public PropertyPath<Date> createDate()
        {
            if(_createDate == null )
                _createDate = new PropertyPath<Date>(SessionStudentNotAllowedForBulletinGen.P_CREATE_DATE, this);
            return _createDate;
        }

    /**
     * @return Дата снятия.
     * @see ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowedForBulletin#getRemovalDate()
     */
        public PropertyPath<Date> removalDate()
        {
            if(_removalDate == null )
                _removalDate = new PropertyPath<Date>(SessionStudentNotAllowedForBulletinGen.P_REMOVAL_DATE, this);
            return _removalDate;
        }

        public Class getEntityClass()
        {
            return SessionStudentNotAllowedForBulletin.class;
        }

        public String getEntityName()
        {
            return "sessionStudentNotAllowedForBulletin";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
