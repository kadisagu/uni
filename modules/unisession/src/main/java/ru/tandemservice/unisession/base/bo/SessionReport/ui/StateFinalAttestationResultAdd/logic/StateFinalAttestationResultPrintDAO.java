/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.StateFinalAttestationResultAdd.logic;

import jxl.Workbook;
import jxl.format.*;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import jxl.write.Number;
import jxl.write.biff.RowsExceededException;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unisession.base.bo.SessionReport.logic.SfaRaw;
import ru.tandemservice.unisession.base.bo.SessionReport.logic.StateFinalAttestationUtil;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.mark.SessionSlotRegularMark;
import ru.tandemservice.unisession.entity.report.StateFinalAttestationResult;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.Boolean;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static ru.tandemservice.unisession.base.bo.SessionReport.logic.StateFinalAttestationUtil.addTextCell;
import static ru.tandemservice.unisession.base.bo.SessionReport.logic.StateFinalAttestationUtil.fillArea;


/**
 * @author Andrey Andreev
 * @since 28.10.2016
 */
public abstract class StateFinalAttestationResultPrintDAO<R1 extends SfaRaw, R2 extends Row> extends UniBaseDao implements IStateFinalAttestationResultPrintDAO
{
    protected int _tableWidth;


    @Override
    public StateFinalAttestationResult createStoredReport(StateFinalAttestationResult report, Long orgUnitId)
    {
        DatabaseFile databaseFile = new DatabaseFile();

        byte[] content = null;
        try
        {
            content = print(report);
        }
        catch (WriteException | IOException e)
        {
            e.printStackTrace();
        }

        databaseFile.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_VND_MS_EXCEL);
        databaseFile.setContent(content);
        databaseFile.setFilename(getFileName(report));
        save(databaseFile);
        report.setContent(databaseFile);

        save(report);

        return report;
    }

    /**
     * Имя файла
     */
    protected abstract String getFileName(StateFinalAttestationResult report);

    /**
     * печать отчета
     *
     * @param report параметры отчета
     * @return файл отчета
     */
    protected byte[] print(StateFinalAttestationResult report) throws WriteException, IOException
    {
        SfaReportInfo<R1> reportInfo = initReportInfo(report);

        init();

        ByteArrayOutputStream out = new ByteArrayOutputStream();

        WritableWorkbook workbook = Workbook.createWorkbook(out);
        WritableSheet sheet = workbook.createSheet("Лист 1)", 0);

        int row = printTitle(report, sheet);

        row = printParametersTable(reportInfo, sheet, row);

        row++;
        row = addDataTable(reportInfo, sheet, row);

        row++;
        row = printNote(reportInfo, sheet, row);

        row++;
        printSignature(reportInfo, sheet, row);

        workbook.write();
        workbook.close();
        return out.toByteArray();
    }

    public abstract SfaReportInfo<R1> initReportInfo(StateFinalAttestationResult report);

    /**
     * Инициализация
     * по умолчанию инициализируются шрифты и стили ячеек.
     */
    protected void init() throws WriteException
    {
        WritableFont font10 = new WritableFont(WritableFont.TIMES, 10);

        WritableFont redFont10 = new WritableFont(font10);
        redFont10.setColour(Colour.RED);

        WritableFont fontBold10 = new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD);
        WritableFont redFontBold10 = new WritableFont(fontBold10);
        redFontBold10.setColour(Colour.RED);

        Colour yellow = Colour.VERY_LIGHT_YELLOW;


        _titleStyle = new WritableCellFormat(new WritableFont(WritableFont.TIMES, 14, WritableFont.BOLD));
        _titleStyle.setBorder(Border.NONE, BorderLineStyle.NONE, Colour.BLACK);
        _titleStyle.setWrap(false);
        _titleStyle.setVerticalAlignment(VerticalAlignment.BOTTOM);
        _titleStyle.setAlignment(Alignment.LEFT);


        _parametersTableStyle = new WritableCellFormat(new WritableFont(WritableFont.TIMES, 12));
        _parametersTableStyle.setBorder(Border.NONE, BorderLineStyle.NONE, Colour.BLACK);
        _parametersTableStyle.setWrap(true);
        _parametersTableStyle.setVerticalAlignment(VerticalAlignment.CENTRE);
        _parametersTableStyle.setAlignment(Alignment.LEFT);


        _horHeaderDataTableStyle = new WritableCellFormat(font10);
        _horHeaderDataTableStyle.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        _horHeaderDataTableStyle.setWrap(true);
        _horHeaderDataTableStyle.setVerticalAlignment(VerticalAlignment.CENTRE);
        _horHeaderDataTableStyle.setAlignment(Alignment.CENTRE);

        _verHeaderDataTableStyle = new WritableCellFormat(_horHeaderDataTableStyle);
        _verHeaderDataTableStyle.setOrientation(Orientation.PLUS_90);

        _horHeaderBoldDataTableStyle = new WritableCellFormat(new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD));
        _horHeaderBoldDataTableStyle.setBorder(Border.ALL, BorderLineStyle.THICK, Colour.BLACK);
        _horHeaderBoldDataTableStyle.setWrap(true);
        _horHeaderBoldDataTableStyle.setVerticalAlignment(VerticalAlignment.CENTRE);
        _horHeaderBoldDataTableStyle.setAlignment(Alignment.CENTRE);


        _intColumnDataTableStyle = new WritableCellFormat(font10);
        _intColumnDataTableStyle.setBorder(Border.BOTTOM, BorderLineStyle.THIN, Colour.BLACK);
        _intColumnDataTableStyle.setBorder(Border.LEFT, BorderLineStyle.THIN, Colour.BLACK);
        _intColumnDataTableStyle.setBorder(Border.RIGHT, BorderLineStyle.THIN, Colour.BLACK);
        _intColumnDataTableStyle.setWrap(false);
        _intColumnDataTableStyle.setVerticalAlignment(VerticalAlignment.BOTTOM);
        _intColumnDataTableStyle.setAlignment(Alignment.CENTRE);

        _intRedColumnDataTableStyle = new WritableCellFormat(_intColumnDataTableStyle);
        _intRedColumnDataTableStyle.setFont(redFont10);

        _titleColumnDataTableStyle = new WritableCellFormat(_intColumnDataTableStyle);
        _titleColumnDataTableStyle.setAlignment(Alignment.LEFT);

        _doubleColumnDataTableStyle = new WritableCellFormat(font10, new NumberFormat("0.00"));
        _doubleColumnDataTableStyle.setBorder(Border.BOTTOM, BorderLineStyle.THIN, Colour.BLACK);
        _doubleColumnDataTableStyle.setBorder(Border.LEFT, BorderLineStyle.THIN, Colour.BLACK);
        _doubleColumnDataTableStyle.setBorder(Border.RIGHT, BorderLineStyle.THIN, Colour.BLACK);
        _doubleColumnDataTableStyle.setWrap(false);
        _doubleColumnDataTableStyle.setVerticalAlignment(VerticalAlignment.BOTTOM);
        _doubleColumnDataTableStyle.setAlignment(Alignment.CENTRE);


        _totalIntColumnDataTableStyle = new WritableCellFormat(_intColumnDataTableStyle);
        _totalIntColumnDataTableStyle.setBackground(yellow);
        _totalIntColumnDataTableStyle.setFont(fontBold10);

        _totalRedIntColumnDataTableStyle = new WritableCellFormat(_totalIntColumnDataTableStyle);
        _totalRedIntColumnDataTableStyle.setFont(redFont10);

        _totalTitleColumnDataTableStyle = new WritableCellFormat(_titleColumnDataTableStyle);
        _totalTitleColumnDataTableStyle.setBackground(yellow);
        _totalTitleColumnDataTableStyle.setFont(fontBold10);

        _totalDoubleColumnDataTableStyle = new WritableCellFormat(_doubleColumnDataTableStyle);
        _totalDoubleColumnDataTableStyle.setBackground(yellow);
        _totalDoubleColumnDataTableStyle.setFont(fontBold10);


        _noteStyle = new WritableCellFormat(new WritableFont(WritableFont.TIMES, 9));
        _noteStyle.setBorder(Border.NONE, BorderLineStyle.NONE, Colour.BLACK);
        _noteStyle.setWrap(true);
        _noteStyle.setVerticalAlignment(VerticalAlignment.CENTRE);
        _noteStyle.setAlignment(Alignment.LEFT);


        _signatureStyle = new WritableCellFormat(_parametersTableStyle);

        _signatureLineStyle = new WritableCellFormat(_parametersTableStyle);
        _signatureLineStyle.setBorder(Border.BOTTOM, BorderLineStyle.THIN, Colour.BLACK);
        _signatureLineStyle.setAlignment(Alignment.CENTRE);

        _subSignatureStyle = new WritableCellFormat(_parametersTableStyle);
        _subSignatureStyle.setFont(new WritableFont(WritableFont.TIMES, 11));
        _subSignatureStyle.setAlignment(Alignment.CENTRE);

        // Суппорт
        _topThickBoardTS = new WritableCellFormat();
        _topThickBoardTS.setBorder(Border.BOTTOM, BorderLineStyle.THICK, Colour.BLACK);

        _rightThickTableBoardTS = new WritableCellFormat();
        _rightThickTableBoardTS.setBorder(Border.LEFT, BorderLineStyle.THICK, Colour.BLACK);

        _bottomThickBoardTS = new WritableCellFormat();
        _bottomThickBoardTS.setBorder(Border.TOP, BorderLineStyle.THICK, Colour.BLACK);
    }


    /**
     * добавление название отчета
     *
     * @return номер строки после названия
     */
    protected int printTitle(StateFinalAttestationResult report, WritableSheet sheet)
    {
        int row = 0;

        addTextCell(sheet, 0, row, 10, 1, "Результаты ГИА", _titleStyle);

        return ++row;
    }


    /**
     * Вставляет в начало страници таблицу с параметрами отчета
     *
     * @param row номер строки с которой начнется таблица
     * @return номер строки следующий за таблицей
     */
    protected int printParametersTable(SfaReportInfo<R1> reportInfo, WritableSheet sheet, int row)
    {
        StateFinalAttestationResult report = reportInfo.getReport();

        row = addProperty2HeadTable(sheet, row, "Учебный год", report.getEducationYear().getTitle(), _parametersTableStyle);

        if (report.getQualification() != null)
            row = addProperty2HeadTable(sheet, row, "Уровень образования", report.getQualification().getTitle(), _parametersTableStyle);

        if (report.getFormativeOrgUnit() != null)
            row = addProperty2HeadTable(sheet, row, "Формирующее подразделение", StringUtils.capitalize(report.getFormativeOrgUnit().getPrintTitle()), _parametersTableStyle);

        if (report.getTerritorialOrgUnit() != null)
            row = addProperty2HeadTable(sheet, row, "Территориальное подразделение", StringUtils.capitalize(report.getTerritorialOrgUnit().getPrintTitle()), _parametersTableStyle);

        if (report.getOwnerOrgUnit() != null)
            row = addProperty2HeadTable(sheet, row, "Выпускающее подразделение", StringUtils.capitalize(report.getOwnerOrgUnit().getPrintTitle()), _parametersTableStyle);

        EducationLevelsHighSchool educationLevelHighSchool = report.getEducationLevelHighSchool();
        String programSubjectKindTitle = educationLevelHighSchool.getEducationLevel().getEduProgramSubject().getEduProgramKind().getEduProgramSubjectKindTitle(InflectorVariantCodes.RU_GENITIVE);
        row = addProperty2HeadTable(sheet, row, "Шифр и наименование " + programSubjectKindTitle.toLowerCase(), educationLevelHighSchool.getDisplayableTitle(), _parametersTableStyle);

        if (report.getDevelopForm() != null)
            row = addProperty2HeadTable(sheet, row, "Форма освоения", report.getDevelopForm().getTitle(), _parametersTableStyle);

        if (report.getDevelopCondition() != null)
            row = addProperty2HeadTable(sheet, row, "Условие освоения", report.getDevelopCondition().getTitle(), _parametersTableStyle);

        if (report.getDevelopTech() != null)
            row = addProperty2HeadTable(sheet, row, "Технология освоения", report.getDevelopTech().getTitle(), _parametersTableStyle);

        if (report.getDevelopPeriod() != null)
            row = addProperty2HeadTable(sheet, row, "Нормативный срок освоения", report.getDevelopPeriod().getTitle(), _parametersTableStyle);

        if (report.getCompensationType() != null)
            row = addProperty2HeadTable(sheet, row, "Вид возмещения затрат", report.getCompensationType().getTitle(), _parametersTableStyle);

        return row;
    }

    /**
     * ширина названия параметра отчета
     */
    protected abstract int getPropertyTitleWidth();

    /**
     * добавляет параметр в таблицу параметров отчета
     *
     * @param row   номер строки с которой начнется таблица
     * @param title название параметра
     * @param value значение параметра
     * @return номер строки следующий за таблицей
     */
    protected int addProperty2HeadTable(WritableSheet sheet, int row, String title, String value, WritableCellFormat format)
    {
        int indent = 1;
        int titleWidth = getPropertyTitleWidth();

        if (value != null && value.length() > 100)
            try
            {
                sheet.setRowView(row, 700);
            }
            catch (RowsExceededException e)
            {
                e.printStackTrace();
            }
        StateFinalAttestationUtil.addTextCell(sheet, indent, row, titleWidth, 1, title, format);
        StateFinalAttestationUtil.addTextCell(sheet, titleWidth + indent, row, _tableWidth - (titleWidth + indent), 1, value, format);

        return ++row;
    }


    /**
     * Добавляет таблицу с данными отчета
     *
     * @param rowIndex номер строки с которой начнется таблица
     * @return номер строки следующий за таблицей
     */
    protected int addDataTable(SfaReportInfo<R1> reportInfo, WritableSheet sheet, int rowIndex)
    {
        int startTable = rowIndex;

        rowIndex = addHeaderDataTable(reportInfo, sheet, rowIndex);

        List<R2> rows = getRows(reportInfo);

        int rowNumber = 1;
        for (R2 row : rows) rowIndex = addRow2Table(row, sheet, rowIndex, rowNumber++);

        R2 totalDir;
        totalDir = newTotalDirectionRow(reportInfo, "ВСЕГО:");
        totalDir.addFromRaw(reportInfo.getRaws());
        rowIndex = addRow2Table(totalDir, sheet, rowIndex, 0);

        fillArea(sheet, _tableWidth + 1, _tableWidth + 1, startTable, rowIndex - 1, _rightThickTableBoardTS);
        fillArea(sheet, 0, _tableWidth, rowIndex, rowIndex, _bottomThickBoardTS);

        return rowIndex;
    }

    /**
     * добавляет шапку таблици
     *
     * @param rowIndex номер строки с которой начнется таблица
     * @return номер строки следующий за шапкой таблици
     */
    protected abstract int addHeaderDataTable(SfaReportInfo<R1> reportInfo, WritableSheet sheet, int rowIndex);


    protected abstract List<R2> getRows(SfaReportInfo<R1> reportInfo);

    /**
     * Добалвяет строку мероприятия
     */
    protected int addRow2Table(R2 row, WritableSheet sheet, int rowIndex, int rowNumber)
    {
        int col = 0;

        try
        {
            if (rowNumber != 0) sheet.addCell(new Number(col++, rowIndex, rowNumber, row.getIntStyle()));
            else sheet.addCell(new Label(col++, rowIndex, "", row.getIntStyle()));
            sheet.addCell(new Label(col++, rowIndex, row.getTitle(), row.getTitleStyle()));
        }
        catch (WriteException e)
        {
            e.printStackTrace();
        }

        addDataRow(sheet, col, rowIndex, row);

        return ++rowIndex;
    }

    /**
     * Добавляет числовые данные строки в таблицу
     */
    protected abstract void addDataRow(WritableSheet sheet, int startCol, int rowIndex, R2 row);

    // Билдер оценок ГИА
    /**
     * alias SessionDocumentSlot main
     */
    protected final static String SLOT_ALIAS = "slot";
    /**
     * alias SessionSlotRegularMark inner join
     */
    protected final static String MARK_ALIAS = "mark";
    /**
     * alias EppStudentWorkPlanElement inner join
     */
    protected final static String WPE_ALIAS = "wpe";
    /**
     * alias EppRegistryElement inner join
     */
    protected final static String REG_ALIAS = "reg";
    /**
     * alias Student inner join
     */
    protected final static String STU_ALIAS = "stu";
    /**
     * alias EducationOrgUnit inner join
     */
    protected final static String EOU_ALIAS = "eou";

    /**
     * билдер записей мероприятия студентов
     */
    protected DQLSelectBuilder getSlotSelectBuilder(SfaReportInfo<R1> reportInfo)
    {
        StateFinalAttestationResult report = reportInfo.getReport();

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(SessionDocumentSlot.class, SLOT_ALIAS);

        builder.joinEntity(SLOT_ALIAS, DQLJoinType.left, SessionSlotRegularMark.class, MARK_ALIAS,
                           and(eq(property(MARK_ALIAS, SessionSlotRegularMark.slot()), property(SLOT_ALIAS)),
                               betweenDays(SessionSlotRegularMark.performDate().fromAlias(MARK_ALIAS), report.getDateFrom(), report.getDateTo()))
        );

        builder.joinPath(DQLJoinType.inner, SessionDocumentSlot.studentWpeCAction().studentWpe().fromAlias(SLOT_ALIAS), WPE_ALIAS)
                .where(eq(property(WPE_ALIAS, EppStudentWorkPlanElement.year().educationYear()), value(report.getEducationYear())));

        builder.joinPath(DQLJoinType.inner, EppStudentWorkPlanElement.registryElementPart().registryElement().fromAlias(WPE_ALIAS), REG_ALIAS);

        builder.joinPath(DQLJoinType.inner, EppStudentWorkPlanElement.student().fromAlias(WPE_ALIAS), STU_ALIAS)
                .where(eq(property(STU_ALIAS, Student.archival()), value(Boolean.FALSE)));
        FilterUtils.applySelectFilter(builder, STU_ALIAS, Student.compensationType(), report.getCompensationType());

        builder.joinPath(DQLJoinType.inner, Student.educationOrgUnit().fromAlias(STU_ALIAS), EOU_ALIAS);

        FilterUtils.applySelectFilter(builder, EOU_ALIAS, EducationOrgUnit.educationLevelHighSchool().educationLevel().qualification(), report.getQualification());
        FilterUtils.applySelectFilter(builder, EOU_ALIAS, EducationOrgUnit.formativeOrgUnit(), report.getFormativeOrgUnit());
        FilterUtils.applySelectFilter(builder, EOU_ALIAS, EducationOrgUnit.territorialOrgUnit(), report.getTerritorialOrgUnit());
        FilterUtils.applySelectFilter(builder, EOU_ALIAS, EducationOrgUnit.educationLevelHighSchool().orgUnit(), report.getOwnerOrgUnit());
        FilterUtils.applySelectFilter(builder, EOU_ALIAS, EducationOrgUnit.educationLevelHighSchool(), report.getEducationLevelHighSchool());
        FilterUtils.applySelectFilter(builder, EOU_ALIAS, EducationOrgUnit.developForm(), report.getDevelopForm());
        FilterUtils.applySelectFilter(builder, EOU_ALIAS, EducationOrgUnit.developCondition(), report.getDevelopCondition());
        FilterUtils.applySelectFilter(builder, EOU_ALIAS, EducationOrgUnit.developTech(), report.getDevelopTech());
        FilterUtils.applySelectFilter(builder, EOU_ALIAS, EducationOrgUnit.developPeriod(), report.getDevelopPeriod());

        return builder;
    }


    /**
     * Печать примечания
     *
     * @param row номер строки с которой начинается прмиечание
     * @return номер строки следующий за прмиечанием
     */
    protected abstract int printNote(SfaReportInfo<R1> reportInfo, WritableSheet sheet, int row);


    /**
     * Печать подписи
     *
     * @param row номер строки с которой начинается подпись
     * @return номер строки следующий за подписью
     */
    protected int printSignature(SfaReportInfo<R1> reportInfo, WritableSheet sheet, int row)
    {
        addTextCell(sheet, 0, row, 2, 1, "Администратор ООП", _signatureStyle);

        addTextCell(sheet, 2, row, 5, 1, "", _signatureLineStyle);
        addTextCell(sheet, 2, row + 1, 5, 1, "подпись", _subSignatureStyle);

        UserContext userContext = ContextLocal.getUserContext();
        if (userContext != null)
            addTextCell(sheet, 11, row, 4, 1, userContext.getPrincipalContext().getFio(), _signatureLineStyle);
        else addTextCell(sheet, 11, row + 1, 4, 1, "", _signatureLineStyle);
        addTextCell(sheet, 12, row + 1, 2, 1, "ф.и.о.", _subSignatureStyle);

        return ++row;
    }


    // Стили ячеек
    protected WritableCellFormat _titleStyle;
    protected WritableCellFormat _parametersTableStyle;
    protected WritableCellFormat _horHeaderDataTableStyle;
    protected WritableCellFormat _verHeaderDataTableStyle;
    protected WritableCellFormat _horHeaderBoldDataTableStyle;
    protected WritableCellFormat _intColumnDataTableStyle;
    protected WritableCellFormat _intRedColumnDataTableStyle;
    protected WritableCellFormat _titleColumnDataTableStyle;
    protected WritableCellFormat _doubleColumnDataTableStyle;
    protected WritableCellFormat _totalIntColumnDataTableStyle;
    protected WritableCellFormat _totalRedIntColumnDataTableStyle;
    protected WritableCellFormat _totalTitleColumnDataTableStyle;
    protected WritableCellFormat _totalDoubleColumnDataTableStyle;
    protected WritableCellFormat _noteStyle;
    protected WritableCellFormat _signatureStyle;
    protected WritableCellFormat _signatureLineStyle;
    protected WritableCellFormat _subSignatureStyle;

    protected WritableCellFormat _topThickBoardTS;
    protected WritableCellFormat _bottomThickBoardTS;
    protected WritableCellFormat _rightThickTableBoardTS;

    /**
     * Строка мероприятия
     */
    protected abstract R2 newRegElRow(SfaReportInfo<R1> reportInfo, String title);

    /**
     * Строка "Всего по направлению"
     */
    protected abstract R2 newTotalDirectionRow(SfaReportInfo<R1> reportInfo, String title);
}