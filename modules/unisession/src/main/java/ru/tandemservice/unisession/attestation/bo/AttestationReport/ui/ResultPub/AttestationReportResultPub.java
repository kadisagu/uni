/**
 *$Id:$
 */
package ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.ResultPub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author Alexander Shaburov
 * @since 14.11.12
 */
@Configuration
public class AttestationReportResultPub extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .create();
    }
}
