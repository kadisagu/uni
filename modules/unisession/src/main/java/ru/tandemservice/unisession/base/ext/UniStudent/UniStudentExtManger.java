/* $Id$ */
package ru.tandemservice.unisession.base.ext.UniStudent;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.uni.base.bo.UniStudent.logic.sessionTransferProtocol.ISessionTransferProtocolDao;
import ru.tandemservice.unisession.base.ext.UniStudent.logic.SessionTransferProtocolDao;

/**
 * @author Andrey Andreev
 * @since 10.12.2015
 */
@Configuration
public class UniStudentExtManger extends BusinessObjectExtensionManager
{



    @Bean
    @BeanOverride
    public ISessionTransferProtocolDao sessionTransferProtocolDao()
    {
        return new SessionTransferProtocolDao();
    }
}