/* $*/

package ru.tandemservice.unisession.component.sessionObject.SessionObjectPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowed;
import ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowedForBulletin;
import ru.tandemservice.unisession.entity.document.SessionObject;

/**
 * @author oleyba
 * @since 3/24/11
 */
@State({
    @Bind(key= PublisherActivator.PUBLISHER_ID_KEY, binding = "session.id"),
    @Bind(key= "selectedTab", binding = "selectedTab"),
    @Bind(key= "selectedDataTab", binding = "selectedDataTab")
})
public class Model
{
    private SessionObject session = new SessionObject();

    private DynamicListDataSource<SessionStudentNotAllowed> sessionDataSource;
    private DynamicListDataSource<SessionStudentNotAllowedForBulletin> bulletinDataSource;
    private String selectedTab;
    private String selectedDataTab;

    public SessionObject getSession()
    {
        return this.session;
    }

    public void setSession(final SessionObject session)
    {
        this.session = session;
    }

    public DynamicListDataSource<SessionStudentNotAllowed> getSessionDataSource()
    {
        return this.sessionDataSource;
    }

    public void setSessionDataSource(final DynamicListDataSource<SessionStudentNotAllowed> sessionDataSource)
    {
        this.sessionDataSource = sessionDataSource;
    }

    public DynamicListDataSource<SessionStudentNotAllowedForBulletin> getBulletinDataSource()
    {
        return this.bulletinDataSource;
    }

    public void setBulletinDataSource(final DynamicListDataSource<SessionStudentNotAllowedForBulletin> bulletinDataSource)
    {
        this.bulletinDataSource = bulletinDataSource;
    }

    public String getSelectedTab()
    {
        return this.selectedTab;
    }

    public void setSelectedTab(final String selectedTab)
    {
        this.selectedTab = selectedTab;
    }

    public String getSelectedDataTab()
    {
        return this.selectedDataTab;
    }

    public void setSelectedDataTab(final String selectedDataTab)
    {
        this.selectedDataTab = selectedDataTab;
    }
}
