package ru.tandemservice.unisession.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.unisession.entity.document.SessionObject;
import ru.tandemservice.unisession.entity.report.UnisessionGroupBulletinListReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Отчет «Сводные данные по ведомостям (на академ. группу)»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UnisessionGroupBulletinListReportGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.report.UnisessionGroupBulletinListReport";
    public static final String ENTITY_NAME = "unisessionGroupBulletinListReport";
    public static final int VERSION_HASH = 75916228;
    private static IEntityMeta ENTITY_META;

    public static final String L_CONTENT = "content";
    public static final String P_FORMING_DATE = "formingDate";
    public static final String P_EXECUTOR = "executor";
    public static final String L_SESSION_OBJECT = "sessionObject";
    public static final String P_PERFORM_DATE_FROM = "performDateFrom";
    public static final String P_PERFORM_DATE_TO = "performDateTo";
    public static final String P_DISC_KINDS = "discKinds";
    public static final String P_COURSE = "course";
    public static final String P_GROUPS = "groups";
    public static final String P_CONTROL_ACTION_TYPES = "controlActionTypes";
    public static final String P_REGISTRY_STRUCTURE = "registryStructure";
    public static final String P_FORMING_DATE_STR = "formingDateStr";

    private DatabaseFile _content;     // Печатная форма
    private Date _formingDate;     // Дата формирования
    private String _executor;     // Исполнитель
    private SessionObject _sessionObject;     // Сессия
    private Date _performDateFrom;     // Дата сдачи мероприятия в ведомости с
    private Date _performDateTo;     // Дата сдачи мероприятия в ведомости по
    private String _discKinds;     // Включать дисциплины
    private String _course;     // Курс
    private String _groups;     // Группы
    private String _controlActionTypes;     // Форма контроля
    private String _registryStructure;     // Вид мероприятия реестра

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     */
    @NotNull
    public DatabaseFile getContent()
    {
        return _content;
    }

    /**
     * @param content Печатная форма. Свойство не может быть null.
     */
    public void setContent(DatabaseFile content)
    {
        dirty(_content, content);
        _content = content;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата формирования. Свойство не может быть null.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    /**
     * @return Исполнитель.
     */
    @Length(max=255)
    public String getExecutor()
    {
        return _executor;
    }

    /**
     * @param executor Исполнитель.
     */
    public void setExecutor(String executor)
    {
        dirty(_executor, executor);
        _executor = executor;
    }

    /**
     * @return Сессия. Свойство не может быть null.
     */
    @NotNull
    public SessionObject getSessionObject()
    {
        return _sessionObject;
    }

    /**
     * @param sessionObject Сессия. Свойство не может быть null.
     */
    public void setSessionObject(SessionObject sessionObject)
    {
        dirty(_sessionObject, sessionObject);
        _sessionObject = sessionObject;
    }

    /**
     * @return Дата сдачи мероприятия в ведомости с.
     */
    public Date getPerformDateFrom()
    {
        return _performDateFrom;
    }

    /**
     * @param performDateFrom Дата сдачи мероприятия в ведомости с.
     */
    public void setPerformDateFrom(Date performDateFrom)
    {
        dirty(_performDateFrom, performDateFrom);
        _performDateFrom = performDateFrom;
    }

    /**
     * @return Дата сдачи мероприятия в ведомости по.
     */
    public Date getPerformDateTo()
    {
        return _performDateTo;
    }

    /**
     * @param performDateTo Дата сдачи мероприятия в ведомости по.
     */
    public void setPerformDateTo(Date performDateTo)
    {
        dirty(_performDateTo, performDateTo);
        _performDateTo = performDateTo;
    }

    /**
     * Названия видов дисциплин РУП, выбранных при построении отчета, через запятую.
     *
     * @return Включать дисциплины. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getDiscKinds()
    {
        return _discKinds;
    }

    /**
     * @param discKinds Включать дисциплины. Свойство не может быть null.
     */
    public void setDiscKinds(String discKinds)
    {
        dirty(_discKinds, discKinds);
        _discKinds = discKinds;
    }

    /**
     * @return Курс.
     */
    @Length(max=255)
    public String getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс.
     */
    public void setCourse(String course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Группы.
     */
    public String getGroups()
    {
        return _groups;
    }

    /**
     * @param groups Группы.
     */
    public void setGroups(String groups)
    {
        dirty(_groups, groups);
        _groups = groups;
    }

    /**
     * @return Форма контроля.
     */
    public String getControlActionTypes()
    {
        return _controlActionTypes;
    }

    /**
     * @param controlActionTypes Форма контроля.
     */
    public void setControlActionTypes(String controlActionTypes)
    {
        dirty(_controlActionTypes, controlActionTypes);
        _controlActionTypes = controlActionTypes;
    }

    /**
     * @return Вид мероприятия реестра.
     */
    public String getRegistryStructure()
    {
        return _registryStructure;
    }

    /**
     * @param registryStructure Вид мероприятия реестра.
     */
    public void setRegistryStructure(String registryStructure)
    {
        dirty(_registryStructure, registryStructure);
        _registryStructure = registryStructure;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UnisessionGroupBulletinListReportGen)
        {
            setContent(((UnisessionGroupBulletinListReport)another).getContent());
            setFormingDate(((UnisessionGroupBulletinListReport)another).getFormingDate());
            setExecutor(((UnisessionGroupBulletinListReport)another).getExecutor());
            setSessionObject(((UnisessionGroupBulletinListReport)another).getSessionObject());
            setPerformDateFrom(((UnisessionGroupBulletinListReport)another).getPerformDateFrom());
            setPerformDateTo(((UnisessionGroupBulletinListReport)another).getPerformDateTo());
            setDiscKinds(((UnisessionGroupBulletinListReport)another).getDiscKinds());
            setCourse(((UnisessionGroupBulletinListReport)another).getCourse());
            setGroups(((UnisessionGroupBulletinListReport)another).getGroups());
            setControlActionTypes(((UnisessionGroupBulletinListReport)another).getControlActionTypes());
            setRegistryStructure(((UnisessionGroupBulletinListReport)another).getRegistryStructure());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UnisessionGroupBulletinListReportGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UnisessionGroupBulletinListReport.class;
        }

        public T newInstance()
        {
            return (T) new UnisessionGroupBulletinListReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "content":
                    return obj.getContent();
                case "formingDate":
                    return obj.getFormingDate();
                case "executor":
                    return obj.getExecutor();
                case "sessionObject":
                    return obj.getSessionObject();
                case "performDateFrom":
                    return obj.getPerformDateFrom();
                case "performDateTo":
                    return obj.getPerformDateTo();
                case "discKinds":
                    return obj.getDiscKinds();
                case "course":
                    return obj.getCourse();
                case "groups":
                    return obj.getGroups();
                case "controlActionTypes":
                    return obj.getControlActionTypes();
                case "registryStructure":
                    return obj.getRegistryStructure();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "content":
                    obj.setContent((DatabaseFile) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
                case "executor":
                    obj.setExecutor((String) value);
                    return;
                case "sessionObject":
                    obj.setSessionObject((SessionObject) value);
                    return;
                case "performDateFrom":
                    obj.setPerformDateFrom((Date) value);
                    return;
                case "performDateTo":
                    obj.setPerformDateTo((Date) value);
                    return;
                case "discKinds":
                    obj.setDiscKinds((String) value);
                    return;
                case "course":
                    obj.setCourse((String) value);
                    return;
                case "groups":
                    obj.setGroups((String) value);
                    return;
                case "controlActionTypes":
                    obj.setControlActionTypes((String) value);
                    return;
                case "registryStructure":
                    obj.setRegistryStructure((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "content":
                        return true;
                case "formingDate":
                        return true;
                case "executor":
                        return true;
                case "sessionObject":
                        return true;
                case "performDateFrom":
                        return true;
                case "performDateTo":
                        return true;
                case "discKinds":
                        return true;
                case "course":
                        return true;
                case "groups":
                        return true;
                case "controlActionTypes":
                        return true;
                case "registryStructure":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "content":
                    return true;
                case "formingDate":
                    return true;
                case "executor":
                    return true;
                case "sessionObject":
                    return true;
                case "performDateFrom":
                    return true;
                case "performDateTo":
                    return true;
                case "discKinds":
                    return true;
                case "course":
                    return true;
                case "groups":
                    return true;
                case "controlActionTypes":
                    return true;
                case "registryStructure":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "content":
                    return DatabaseFile.class;
                case "formingDate":
                    return Date.class;
                case "executor":
                    return String.class;
                case "sessionObject":
                    return SessionObject.class;
                case "performDateFrom":
                    return Date.class;
                case "performDateTo":
                    return Date.class;
                case "discKinds":
                    return String.class;
                case "course":
                    return String.class;
                case "groups":
                    return String.class;
                case "controlActionTypes":
                    return String.class;
                case "registryStructure":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UnisessionGroupBulletinListReport> _dslPath = new Path<UnisessionGroupBulletinListReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UnisessionGroupBulletinListReport");
    }
            

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupBulletinListReport#getContent()
     */
    public static DatabaseFile.Path<DatabaseFile> content()
    {
        return _dslPath.content();
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupBulletinListReport#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * @return Исполнитель.
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupBulletinListReport#getExecutor()
     */
    public static PropertyPath<String> executor()
    {
        return _dslPath.executor();
    }

    /**
     * @return Сессия. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupBulletinListReport#getSessionObject()
     */
    public static SessionObject.Path<SessionObject> sessionObject()
    {
        return _dslPath.sessionObject();
    }

    /**
     * @return Дата сдачи мероприятия в ведомости с.
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupBulletinListReport#getPerformDateFrom()
     */
    public static PropertyPath<Date> performDateFrom()
    {
        return _dslPath.performDateFrom();
    }

    /**
     * @return Дата сдачи мероприятия в ведомости по.
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupBulletinListReport#getPerformDateTo()
     */
    public static PropertyPath<Date> performDateTo()
    {
        return _dslPath.performDateTo();
    }

    /**
     * Названия видов дисциплин РУП, выбранных при построении отчета, через запятую.
     *
     * @return Включать дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupBulletinListReport#getDiscKinds()
     */
    public static PropertyPath<String> discKinds()
    {
        return _dslPath.discKinds();
    }

    /**
     * @return Курс.
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupBulletinListReport#getCourse()
     */
    public static PropertyPath<String> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Группы.
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupBulletinListReport#getGroups()
     */
    public static PropertyPath<String> groups()
    {
        return _dslPath.groups();
    }

    /**
     * @return Форма контроля.
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupBulletinListReport#getControlActionTypes()
     */
    public static PropertyPath<String> controlActionTypes()
    {
        return _dslPath.controlActionTypes();
    }

    /**
     * @return Вид мероприятия реестра.
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupBulletinListReport#getRegistryStructure()
     */
    public static PropertyPath<String> registryStructure()
    {
        return _dslPath.registryStructure();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupBulletinListReport#getFormingDateStr()
     */
    public static SupportedPropertyPath<String> formingDateStr()
    {
        return _dslPath.formingDateStr();
    }

    public static class Path<E extends UnisessionGroupBulletinListReport> extends EntityPath<E>
    {
        private DatabaseFile.Path<DatabaseFile> _content;
        private PropertyPath<Date> _formingDate;
        private PropertyPath<String> _executor;
        private SessionObject.Path<SessionObject> _sessionObject;
        private PropertyPath<Date> _performDateFrom;
        private PropertyPath<Date> _performDateTo;
        private PropertyPath<String> _discKinds;
        private PropertyPath<String> _course;
        private PropertyPath<String> _groups;
        private PropertyPath<String> _controlActionTypes;
        private PropertyPath<String> _registryStructure;
        private SupportedPropertyPath<String> _formingDateStr;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupBulletinListReport#getContent()
     */
        public DatabaseFile.Path<DatabaseFile> content()
        {
            if(_content == null )
                _content = new DatabaseFile.Path<DatabaseFile>(L_CONTENT, this);
            return _content;
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupBulletinListReport#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(UnisessionGroupBulletinListReportGen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * @return Исполнитель.
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupBulletinListReport#getExecutor()
     */
        public PropertyPath<String> executor()
        {
            if(_executor == null )
                _executor = new PropertyPath<String>(UnisessionGroupBulletinListReportGen.P_EXECUTOR, this);
            return _executor;
        }

    /**
     * @return Сессия. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupBulletinListReport#getSessionObject()
     */
        public SessionObject.Path<SessionObject> sessionObject()
        {
            if(_sessionObject == null )
                _sessionObject = new SessionObject.Path<SessionObject>(L_SESSION_OBJECT, this);
            return _sessionObject;
        }

    /**
     * @return Дата сдачи мероприятия в ведомости с.
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupBulletinListReport#getPerformDateFrom()
     */
        public PropertyPath<Date> performDateFrom()
        {
            if(_performDateFrom == null )
                _performDateFrom = new PropertyPath<Date>(UnisessionGroupBulletinListReportGen.P_PERFORM_DATE_FROM, this);
            return _performDateFrom;
        }

    /**
     * @return Дата сдачи мероприятия в ведомости по.
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupBulletinListReport#getPerformDateTo()
     */
        public PropertyPath<Date> performDateTo()
        {
            if(_performDateTo == null )
                _performDateTo = new PropertyPath<Date>(UnisessionGroupBulletinListReportGen.P_PERFORM_DATE_TO, this);
            return _performDateTo;
        }

    /**
     * Названия видов дисциплин РУП, выбранных при построении отчета, через запятую.
     *
     * @return Включать дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupBulletinListReport#getDiscKinds()
     */
        public PropertyPath<String> discKinds()
        {
            if(_discKinds == null )
                _discKinds = new PropertyPath<String>(UnisessionGroupBulletinListReportGen.P_DISC_KINDS, this);
            return _discKinds;
        }

    /**
     * @return Курс.
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupBulletinListReport#getCourse()
     */
        public PropertyPath<String> course()
        {
            if(_course == null )
                _course = new PropertyPath<String>(UnisessionGroupBulletinListReportGen.P_COURSE, this);
            return _course;
        }

    /**
     * @return Группы.
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupBulletinListReport#getGroups()
     */
        public PropertyPath<String> groups()
        {
            if(_groups == null )
                _groups = new PropertyPath<String>(UnisessionGroupBulletinListReportGen.P_GROUPS, this);
            return _groups;
        }

    /**
     * @return Форма контроля.
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupBulletinListReport#getControlActionTypes()
     */
        public PropertyPath<String> controlActionTypes()
        {
            if(_controlActionTypes == null )
                _controlActionTypes = new PropertyPath<String>(UnisessionGroupBulletinListReportGen.P_CONTROL_ACTION_TYPES, this);
            return _controlActionTypes;
        }

    /**
     * @return Вид мероприятия реестра.
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupBulletinListReport#getRegistryStructure()
     */
        public PropertyPath<String> registryStructure()
        {
            if(_registryStructure == null )
                _registryStructure = new PropertyPath<String>(UnisessionGroupBulletinListReportGen.P_REGISTRY_STRUCTURE, this);
            return _registryStructure;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupBulletinListReport#getFormingDateStr()
     */
        public SupportedPropertyPath<String> formingDateStr()
        {
            if(_formingDateStr == null )
                _formingDateStr = new SupportedPropertyPath<String>(UnisessionGroupBulletinListReportGen.P_FORMING_DATE_STR, this);
            return _formingDateStr;
        }

        public Class getEntityClass()
        {
            return UnisessionGroupBulletinListReport.class;
        }

        public String getEntityName()
        {
            return "unisessionGroupBulletinListReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getFormingDateStr();
}
