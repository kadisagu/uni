package ru.tandemservice.unisession.entity.mark.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.unisession.entity.mark.SessionALRequest;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Заявление о переводе на ускоренное обучение
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionALRequestGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.mark.SessionALRequest";
    public static final String ENTITY_NAME = "sessionALRequest";
    public static final int VERSION_HASH = 946468913;
    private static IEntityMeta ENTITY_META;

    public static final String P_REQUEST_DATE = "requestDate";
    public static final String L_STUDENT = "student";
    public static final String L_EDU_DOCUMENT = "eduDocument";
    public static final String L_BLOCK = "block";
    public static final String P_TITLE = "title";

    private Date _requestDate;     // Дата заявления
    private Student _student;     // Студент
    private PersonEduDocument _eduDocument;     // Документ об образовании и (или) квалификации
    private EppEduPlanVersionBlock _block;     // Блок УПв

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата заявления. Свойство не может быть null.
     */
    @NotNull
    public Date getRequestDate()
    {
        return _requestDate;
    }

    /**
     * @param requestDate Дата заявления. Свойство не может быть null.
     */
    public void setRequestDate(Date requestDate)
    {
        dirty(_requestDate, requestDate);
        _requestDate = requestDate;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Документ об образовании и (или) квалификации. Свойство не может быть null.
     */
    @NotNull
    public PersonEduDocument getEduDocument()
    {
        return _eduDocument;
    }

    /**
     * @param eduDocument Документ об образовании и (или) квалификации. Свойство не может быть null.
     */
    public void setEduDocument(PersonEduDocument eduDocument)
    {
        dirty(_eduDocument, eduDocument);
        _eduDocument = eduDocument;
    }

    /**
     * @return Блок УПв. Свойство не может быть null.
     */
    @NotNull
    public EppEduPlanVersionBlock getBlock()
    {
        return _block;
    }

    /**
     * @param block Блок УПв. Свойство не может быть null.
     */
    public void setBlock(EppEduPlanVersionBlock block)
    {
        dirty(_block, block);
        _block = block;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SessionALRequestGen)
        {
            setRequestDate(((SessionALRequest)another).getRequestDate());
            setStudent(((SessionALRequest)another).getStudent());
            setEduDocument(((SessionALRequest)another).getEduDocument());
            setBlock(((SessionALRequest)another).getBlock());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionALRequestGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionALRequest.class;
        }

        public T newInstance()
        {
            return (T) new SessionALRequest();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "requestDate":
                    return obj.getRequestDate();
                case "student":
                    return obj.getStudent();
                case "eduDocument":
                    return obj.getEduDocument();
                case "block":
                    return obj.getBlock();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "requestDate":
                    obj.setRequestDate((Date) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "eduDocument":
                    obj.setEduDocument((PersonEduDocument) value);
                    return;
                case "block":
                    obj.setBlock((EppEduPlanVersionBlock) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "requestDate":
                        return true;
                case "student":
                        return true;
                case "eduDocument":
                        return true;
                case "block":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "requestDate":
                    return true;
                case "student":
                    return true;
                case "eduDocument":
                    return true;
                case "block":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "requestDate":
                    return Date.class;
                case "student":
                    return Student.class;
                case "eduDocument":
                    return PersonEduDocument.class;
                case "block":
                    return EppEduPlanVersionBlock.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionALRequest> _dslPath = new Path<SessionALRequest>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionALRequest");
    }
            

    /**
     * @return Дата заявления. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.mark.SessionALRequest#getRequestDate()
     */
    public static PropertyPath<Date> requestDate()
    {
        return _dslPath.requestDate();
    }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.mark.SessionALRequest#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Документ об образовании и (или) квалификации. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.mark.SessionALRequest#getEduDocument()
     */
    public static PersonEduDocument.Path<PersonEduDocument> eduDocument()
    {
        return _dslPath.eduDocument();
    }

    /**
     * @return Блок УПв. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.mark.SessionALRequest#getBlock()
     */
    public static EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> block()
    {
        return _dslPath.block();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.entity.mark.SessionALRequest#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends SessionALRequest> extends EntityPath<E>
    {
        private PropertyPath<Date> _requestDate;
        private Student.Path<Student> _student;
        private PersonEduDocument.Path<PersonEduDocument> _eduDocument;
        private EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> _block;
        private SupportedPropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата заявления. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.mark.SessionALRequest#getRequestDate()
     */
        public PropertyPath<Date> requestDate()
        {
            if(_requestDate == null )
                _requestDate = new PropertyPath<Date>(SessionALRequestGen.P_REQUEST_DATE, this);
            return _requestDate;
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.mark.SessionALRequest#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Документ об образовании и (или) квалификации. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.mark.SessionALRequest#getEduDocument()
     */
        public PersonEduDocument.Path<PersonEduDocument> eduDocument()
        {
            if(_eduDocument == null )
                _eduDocument = new PersonEduDocument.Path<PersonEduDocument>(L_EDU_DOCUMENT, this);
            return _eduDocument;
        }

    /**
     * @return Блок УПв. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.mark.SessionALRequest#getBlock()
     */
        public EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> block()
        {
            if(_block == null )
                _block = new EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock>(L_BLOCK, this);
            return _block;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.entity.mark.SessionALRequest#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(SessionALRequestGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return SessionALRequest.class;
        }

        public String getEntityName()
        {
            return "sessionALRequest";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getTitle();
}
