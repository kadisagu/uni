/* $Id:$ */
package ru.tandemservice.unisession.base.bo.SessionReport.util;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Transformer;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.*;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import org.tandemframework.tapsupport.component.selection.SimpleListResultBuilder;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.StudentCustomState;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeFCA;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.catalog.EppWorkPlanRowKind;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import static java.util.Arrays.asList;
import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 27.05.2015
 */
public class UniSessionFilterAddon extends CommonFilterAddon<EppStudentWpeCAction> {

    public UniSessionFilterAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @Override
    public Class<EppStudentWpeCAction> getEntityClass() {
        return EppStudentWpeCAction
                .class;
    }

    public static final String SETTINGS_NAME_COURSE = "course";
    public static final String SETTINGS_NAME_GROUP = "groups";
    public static final String SETTINGS_NAME_GROUP_WITH_NOGROUP = "groupsWithNoGroup";
    public static final String SETTINGS_NAME_STUDENT_STATUS = "studentStatus";
    public static final String SETTINGS_NAME_TARGET_ADMISSION = "targetAdmission";
    public static final String SETTINGS_NAME_GROUP_ORG_UNIT = "groupOrgUnit";
    public static final String SETTINGS_NAME_TERRITORIAL_ORG_UNIT = "orgUnit";
    public static final String SETTINGS_NAME_COMPENSATION_TYPE = "compensationType";
    public static final String SETTINGS_NAME_DEVELOP_FORM = "developForm";
    public static final String SETTINGS_NAME_DEVELOP_CONDITION = "developCondition";
    public static final String SETTINGS_NAME_DEVELOP_PERIOD = "developPeriod";
    public static final String SETTINGS_NAME_DEVELOP_TECH = "developTech";
    public static final String SETTINGS_NAME_EDUCATION_LEVELS = "educationLevels";
    public static final String SETTINGS_NAME_REGISTRY_STRUCTURE = "registryStructure";
    public static final String SETTINGS_NAME_CUSTOM_STATE = "customState";
    public static final String SETTINGS_NAME_CONTROL_FORM = "controlForm";
    public static final String SETTINGS_NAME_DISC_KIND = "discKind";








    public static final CommonFilterContentConfig<Course> COURSE = new CommonFilterContentConfig<>(Course.class, SETTINGS_NAME_COURSE, "Курс", EppStudentWpeCAction.studentWpe().student().course(), Course.title(), asList(Course.title()), asList(Course.title()));

    public static final CommonFilterContentConfig<Group> GROUP = new CommonFilterContentConfig<>(Group.class, SETTINGS_NAME_GROUP, "Группа", EppStudentWpeCAction.studentWpe().student().group(), Group.title(), asList(Group.title()), asList(Group.title()));
    public static final CommonFilterContentConfig<StudentStatus> STUDENT_STATUS = new CommonFilterContentConfig<>(StudentStatus.class, SETTINGS_NAME_STUDENT_STATUS, "Состояние студента", EppStudentWpeCAction.studentWpe().student().status(), StudentStatus.title(), asList(StudentStatus.title()), asList(StudentStatus.title()));
    public static final CommonFilterContentConfig<OrgUnit> GROUP_ORG_UNIT = new CommonFilterContentConfig<>(OrgUnit.class, SETTINGS_NAME_GROUP_ORG_UNIT, "Деканат", EppStudentWpeCAction.studentWpe().student().educationOrgUnit().groupOrgUnit(), OrgUnit.title(), asList(OrgUnit.title()), asList(OrgUnit.title()));
    public static final CommonFilterContentConfig<OrgUnit> TERRITORIAL_ORG_UNIT = new CommonFilterContentConfig<>(OrgUnit.class, SETTINGS_NAME_TERRITORIAL_ORG_UNIT, "Территориальное подразделение", EppStudentWpeCAction.studentWpe().student().educationOrgUnit().territorialOrgUnit(), OrgUnit.title(), asList(OrgUnit.title()), asList(OrgUnit.title()));
    public static final CommonFilterContentConfig<CompensationType> COMPENSATION_TYPE = new CommonFilterContentConfig<>(CompensationType.class, SETTINGS_NAME_COMPENSATION_TYPE, "Вид возмещения затрат", EppStudentWpeCAction.studentWpe().student().compensationType(), CompensationType.title(), asList(CompensationType.title()), asList(CompensationType.title()));
    public static final CommonFilterContentConfig<EducationLevelsHighSchool> EDUCATION_LEVELS = new CommonFilterContentConfig<>(EducationLevelsHighSchool.class, SETTINGS_NAME_EDUCATION_LEVELS, "Направление подготовки (специальность)", EppStudentWpeCAction.studentWpe().student().educationOrgUnit().educationLevelHighSchool(), EducationLevelsHighSchool.title(), asList(EducationLevelsHighSchool.title()), asList(EducationLevelsHighSchool.title()));

    public static final CommonFilterContentConfig<DevelopForm> DEVELOP_FORM = new CommonFilterContentConfig<>(DevelopForm.class, SETTINGS_NAME_DEVELOP_FORM, "Форма освоения", EppStudentWpeCAction.studentWpe().student().educationOrgUnit().developForm(), DevelopForm.title(), asList(DevelopForm.title()), asList(DevelopForm.title()));
    public static final CommonFilterContentConfig<DevelopCondition> DEVELOP_CONDITION = new CommonFilterContentConfig<>(DevelopCondition.class, SETTINGS_NAME_DEVELOP_CONDITION, "Условие освоения", EppStudentWpeCAction.studentWpe().student().educationOrgUnit().developCondition(), DevelopCondition.title(), asList(DevelopCondition.title()), asList(DevelopCondition.title()));
    public static final CommonFilterContentConfig<DevelopPeriod> DEVELOP_PERIOD = new CommonFilterContentConfig<>(DevelopPeriod.class, SETTINGS_NAME_DEVELOP_PERIOD, "Срок освоения", EppStudentWpeCAction.studentWpe().student().educationOrgUnit().developPeriod(), DevelopPeriod.title(), asList(DevelopPeriod.title()), asList(DevelopPeriod.title()));
    public static final CommonFilterContentConfig<DevelopTech> DEVELOP_TECH = new CommonFilterContentConfig<>(DevelopTech.class, SETTINGS_NAME_DEVELOP_TECH, "Технология освоения", EppStudentWpeCAction.studentWpe().student().educationOrgUnit().developTech(), DevelopTech.title(), asList(DevelopTech.title()), asList(DevelopTech.title()));

    public static final CommonFilterContentConfig<EppWorkPlanRowKind> DISC_KIND = new CommonFilterContentConfig<>(EppWorkPlanRowKind.class, SETTINGS_NAME_DISC_KIND, "Включать дисциплины", EppStudentWpeCAction.studentWpe().sourceRow().kind(), EppWorkPlanRowKind.title(), asList(EppWorkPlanRowKind.title()), asList(EppWorkPlanRowKind.title()));

    /** Фильтр "Вид мероприятия реестра"
     * */
    public static final ICommonFilterFactory<IEntity> REGISTRY_STRUCTURE = (RegistryStructureFilterItem::new);

    /** Фильтр "Форма контроля"
     * */
    public static final ICommonFilterFactory<IEntity> CONTROL_FORM = (ControlFormFilterItem::new);

    /** Фильтр "Дополнительный статус"
     * */
    public static final ICommonFilterFactory<IEntity> STUDENT_CUSTOM_STATE = (StudentCustomStateFilterItem::new);

    /** Фильтр "Целевой прием"
     * */
    public static final ICommonFilterFactory<IEntity> TARGET_ADMISSION = (TargetAdmissionFilterItem::new);

    /** Фильтр "Группа", тайтл "Группа"
     * */
    public static final ICommonFilterFactory<IEntity> GROUP_WITH_NO_GROUP = (CommonFilterFormConfig config) -> (new GroupsWithNoGroupFilterItem(config, "Группа"));

    /** Фильтр "Группа", тайтл "Академическая группа"
     * */
    public static final ICommonFilterFactory<IEntity> GROUP_WITH_NO_GROUP_ALT_TITLE = (CommonFilterFormConfig config) -> (new GroupsWithNoGroupFilterItem(config, "Академическая группа"));


    public static class RegistryStructureFilterItem extends CustomFilterItem
    {
        public RegistryStructureFilterItem(CommonFilterFormConfig config) {
           super(config, "Вид мероприятия реестра", SETTINGS_NAME_REGISTRY_STRUCTURE);
        }


        @Override
        public void apply(DQLSelectBuilder builder, String alias) {

            final Object value = getValue();
            if (value instanceof List) {
                builder.where(or(
                        in(property(alias, EppStudentWpeCAction.studentWpe().registryElementPart().registryElement().parent()), (List)value),
                        in(property(alias, EppStudentWpeCAction.studentWpe().registryElementPart().registryElement().parent().parent()), (List)value)
                ));
            } else if (value instanceof EppRegistryStructure)
            {
                builder.where(or(
                        eq(property(alias, EppStudentWpeCAction.studentWpe().registryElementPart().registryElement().parent()), value((EppRegistryStructure) value)),
                        eq(property(alias, EppStudentWpeCAction.studentWpe().registryElementPart().registryElement().parent().parent()), value((EppRegistryStructure)value))
                ));
            }

        }


        @Override
        public void init(final CommonFilterAddon commonFilterAddon, final String alias) {
            if (getModel() != null) return;

            setModel(new CommonFilterSelectModel() {
                @Override
                protected IListResultBuilder createBuilder(String filter, Object o) {
                    DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppRegistryStructure.class, "s");

                    if (!StringUtils.isEmpty(filter))
                         builder.where(likeUpper(property("s", EppRegistryStructure.title()), value(CoreStringUtils.escapeLike(filter))));
                    if (o != null) {
                        builder.where(((o instanceof Collection ? CommonFilterAdditionalFilterType.COLLECTION : CommonFilterAdditionalFilterType.EQ).whereExpression("s", EppRegistryStructure.id(), o)));
                    }

                    final List<EppRegistryStructure> list = builder.createStatement(commonFilterAddon.getSession()).list();
                    return new SimpleListResultBuilder<>(list);
                }
            });

        }

    }



    public static class ControlFormFilterItem extends CustomFilterItem
    {
        public ControlFormFilterItem(CommonFilterFormConfig config) {
            super(config, "Форма контроля", SETTINGS_NAME_CONTROL_FORM);
        }

        @Override
        public void apply(DQLSelectBuilder builder, String alias) {

            final Object value = getValue();
            if (value instanceof List) {

                final Collection list = CollectionUtils.collect(((List) value), new Transformer<EppFControlActionType, EppGroupTypeFCA>() {
                    @Override
                    public EppGroupTypeFCA transform(EppFControlActionType o) {
                        return o.getEppGroupType();
                    }
                });

                builder.where(in(
                        property(alias, EppStudentWpeCAction.type()),
                        list));
            } else if (value instanceof EppFControlActionType)
            {
                builder.where(eq(
                        property(alias, EppStudentWpeCAction.type()),
                        value(((EppFControlActionType) value).getEppGroupType())));
            }
        }


        @Override
        public void init(final CommonFilterAddon commonFilterAddon, final String alias) {
            if (getModel() != null) return;

            setModel(new CommonFilterSelectModel() {
                @Override
                protected IListResultBuilder createBuilder(String filter, Object o) {
                    DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppFControlActionType.class, "s");

                    if (!StringUtils.isEmpty(filter))
                        builder.where(likeUpper(property("s", EppFControlActionType.title()), value(CoreStringUtils.escapeLike(filter))));
                    if (o != null) {
                        builder.where(((o instanceof Collection ? CommonFilterAdditionalFilterType.COLLECTION : CommonFilterAdditionalFilterType.EQ).whereExpression("s", EppFControlActionType.id(), o)));
                    }

                    final List<EppFControlActionType> list = builder.createStatement(commonFilterAddon.getSession()).list();
                    return new SimpleListResultBuilder<>(list);
                }
            });

        }

    }

    public static class StudentCustomStateFilterItem extends CustomFilterItem
    {
        public StudentCustomStateFilterItem(CommonFilterFormConfig config) {
            super(config, "Дополнительный статус", SETTINGS_NAME_CUSTOM_STATE);
        }

        @Override
        public void apply(DQLSelectBuilder builder, String alias) {
            final Object value = getValue();


            builder
                    .joinPath(DQLJoinType.inner, EppStudentWpeCAction.studentWpe().fromAlias(alias), "swpe")
                    .joinPath(DQLJoinType.inner, EppStudentWorkPlanElement.student().fromAlias("swpe"), "student")
                    .joinEntity("student", DQLJoinType.inner, StudentCustomState.class, "customState",
                            eq(property("student"), property("customState", StudentCustomState.student())));

            if (value instanceof List) {

                builder.where(in(
                        property("customState", StudentCustomState.customState()),
                        value));
            } else if (value instanceof StudentCustomStateCI)
            {
                builder.where(eq(
                        property("customState", StudentCustomState.customState()),
                        value((StudentCustomStateCI)value)));
            }
        }


        @Override
        public void init(final CommonFilterAddon commonFilterAddon, final String alias) {
            if (getModel() != null) return;

            setModel(new CommonFilterSelectModel() {
                @Override
                protected IListResultBuilder createBuilder(String filter, Object o) {
                    DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentCustomStateCI.class, "s");

                    if (!StringUtils.isEmpty(filter))
                        builder.where(likeUpper(property("s", StudentCustomStateCI.title()), value(CoreStringUtils.escapeLike(filter))));
                    if (o != null) {
                        builder.where(((o instanceof Collection ? CommonFilterAdditionalFilterType.COLLECTION : CommonFilterAdditionalFilterType.EQ).whereExpression("s", StudentCustomStateCI.id(), o)));
                    }

                    final List<StudentCustomStateCI> list = builder.createStatement(commonFilterAddon.getSession()).list();
                    return new SimpleListResultBuilder<>(list);
                }
            });

        }
    }


    public static class TargetAdmissionFilterItem extends CustomFilterItem
    {
        public TargetAdmissionFilterItem(CommonFilterFormConfig config) {
            super(config, "Целевой прием", SETTINGS_NAME_TARGET_ADMISSION);
        }

        @Override
        public void apply(DQLSelectBuilder builder, String alias) {
            final Object value = getValue();
            if (value instanceof List) {
                return; //ничего не делаем, мультселект не используется
            } else if (value instanceof DataWrapper)
            {
                final DataWrapper wrapper = (DataWrapper) value;
                boolean targetAdmission = TwinComboDataSourceHandler.getSelectedValueNotNull(wrapper);
                builder.where(eq(
                        property(alias, EppStudentWpeCAction.studentWpe().student().targetAdmission()), value(targetAdmission)));
            }
        }

        @Override
        public void init(CommonFilterAddon commonFilterAddon, String alias) {
            if (getModel() != null) return;

            setModel(TwinComboDataSourceHandler.getYesNoDefaultSelectModel());
        }
    }


    public static class GroupsWithNoGroupFilterItem extends CustomFilterItem
    {
        public GroupsWithNoGroupFilterItem(CommonFilterFormConfig config, String title) {
            super(config, title, SETTINGS_NAME_GROUP_WITH_NOGROUP);
        }

        @Override
        public void apply(DQLSelectBuilder builder, String alias) {
            final Object value = getValue();
            if (value instanceof List) {
                if (((List) value).contains(GroupModel.WRAPPER_NO_GROUP))
                    builder.where(or(
                        isNull(property(alias, EppStudentWpeCAction.studentWpe().student().group())),
                        in(property(alias, EppStudentWpeCAction.studentWpe().student().group().id()), CollectionUtils.collect((List)value, new Transformer<IdentifiableWrapper, Long>() {
                            @Override
                            public Long transform(IdentifiableWrapper identifiableWrapper) {
                                return identifiableWrapper.getId();
                            }
                        }))
                    ));
                else builder.where(
                        in(property(alias, EppStudentWpeCAction.studentWpe().student().group().id()), CollectionUtils.collect((List) value, new Transformer<IdentifiableWrapper, Long>() {
                            @Override
                            public Long transform(IdentifiableWrapper identifiableWrapper) {
                                return identifiableWrapper.getId();
                            }
                        })));
            } else if (value instanceof IdentifiableWrapper)
            {
                if (GroupModel.WRAPPER_NO_GROUP.equals(value))
                {
                    builder.where(isNull(property(alias, EppStudentWpeCAction.studentWpe().student().group())));
                } else {
                    builder.where(eq(
                            property(alias, EppStudentWpeCAction.studentWpe().student().group().id()), value(((IdentifiableWrapper) value).getId())));
                }
            }
        }

        @Override
        public void init(final CommonFilterAddon commonFilterAddon, String alias) {
            if (getModel() != null) return;

            setModel(new CommonFilterSelectModel() {
                @Override
                protected IListResultBuilder createBuilder(String filter, Object o) {

                    DQLSelectBuilder builder = commonFilterAddon.prepareEntityBuilder("s", GroupsWithNoGroupFilterItem.this);

                    FilterUtils.applyLikeFilter(builder, filter, EppStudentWpeCAction.studentWpe().student().group().title().fromAlias("s"));
                    FilterUtils.applySelectFilter(builder, "s", EppStudentWpeCAction.studentWpe().student().group().id(), o);

                    builder.column(property("s", EppStudentWpeCAction.studentWpe().student().group()))
                            .column(property("s", EppStudentWpeCAction.studentWpe().student()));


                    final List<Object[]> list = builder.createStatement(commonFilterAddon.getSession()).list();

                    Set<IdentifiableWrapper> groups = new TreeSet<>(CommonCollator.TITLED_WITH_ID_COMPARATOR);

                    for (Object[] objects : list) {
                        groups.add(new IdentifiableWrapper<>((Group)objects[0]));
                    }
                    if (groups.contains(null))
                    {
                        groups.add(GroupModel.WRAPPER_NO_GROUP);
                    }


                    return new SimpleListResultBuilder<>(groups);
                }
            });
        }
    }
}
