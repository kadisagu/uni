/* $*/

package ru.tandemservice.unisession.base.bo.SessionMark.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.document.SessionDocument;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.Set;

/**
 * @author oleyba
 * @since 2/24/11
 */
public interface ISessionMarkDAO extends INeedPersistenceSupport
{
    String LOCK = ISessionMarkDAO.class.getName()+".lock";

    /**
     * Интерфейс для сохранения данных об оценке студента
     */
    interface MarkData
    {
        /**
         * @return дата выставления оценки
         */
        Date getPerformDate();

        /**
         * @return баллы, соответствующие выставляемой оценке
         * (полный эквивалент выставляемому SessionMarkCatalogItem в баллах,
         * а не только та часть баллов, которая была получена на мероприятии)
         */
        Double getPoints();

        /**
         * @return выставляемая оценка
         */
        SessionMarkCatalogItem getMarkValue();

        /**
         * @return комментарий к оценке
         */
        String getComment();
    }

    /**
     * Модели для выбора оценок и отметок для каждого слота - в соответствии с типом мероприятия, допуском, и использованием БРС
     * @param slots набор слотов
     * @return мап (слот -> модель)
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    Map<SessionDocumentSlot, ISelectModel> getMarkModelMap(Collection<SessionDocumentSlot> slots);

    /**
     * Модель для выбора оценок и отметок для МСРП-ФК
     * Игнорирует включенную БРС и параметры допуска
     * @param slot МСРП-ФК
     * @param currentValue текущее значение оценки (если есть)
     * @param gradesOnly в модели только оценки, без отметок
     * @return модель
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    ISelectModel prepareSimpleMarkModel(EppStudentWpeCAction slot, SessionMarkCatalogItem currentValue, boolean gradesOnly);

    /**
     * Модель для выбора оценок и отметок для МСРП-ФК
     * Игнорирует включенную БРС и параметры допуска
     * @param slot МСРП-ФК
     * @param currentValue текущее значение оценки (если есть)
     * @param gradesOnly в модели только оценки, без отметок
     * @return модель
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    ISelectModel prepareSimpleMarkModelWithShortTitles(EppStudentWpeCAction slot, SessionMarkCatalogItem currentValue, boolean gradesOnly);

    /**
     * Модель для выбора оценок и отметок для МСРП-ФК, в соответствии с типом мероприятия, допуском, и использованием БРС
     * @param slot МСРП-ФК
     * @param currentValue текущее значение оценки (если есть)
     * @param gradesOnly в модели только оценки, без отметок
     * @return модель
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    ISelectModel prepareMarkModel(OrgUnit groupOu, EppStudentWpeCAction slot, SessionMarkCatalogItem currentValue, boolean gradesOnly);

    /**
     * Сохраняет оценку, меняет класс, если это нужно.
     * В следующих версиях метод будет удален, вместо него необходимо использовать saveOrUpdateMark(SessionDocumentSlot slot, MarkData markValue);
     * @param slot слот
     * @param markValue значение оценки
     * @param performDate дата сдачи
     * @return сохраненная оценка
     */
    @Deprecated
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    SessionMark saveOrUpdateMark(SessionDocumentSlot slot, SessionMarkCatalogItem markValue, Date performDate);

    /**
     * Сохраняет оценку, меняет класс, если это нужно
     * @param slot слот
     * @param markValue значение оценки
     * @return сохраненная оценка
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    SessionMark saveOrUpdateMark(SessionDocumentSlot slot, MarkData markValue);

    /**
     * Удаляет неактуальную оценку вместе со слотом в документе, игнорируя проверки на закрытость документа
     * @param id - id оценки
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void deleteUnactualMark(Long id);

    /**
     * Предоставляет id студентов, которые не допущены до сдачи
     * Учитывает для ведомостей допуск до сдачи сессии и допуск по ведомости,
     * и для всех документов с включенным БРС и данными по БРС - допуск по БРС
     * @param document документ сессии
     * @return id объектов Student
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    Set<Long> getDisallowedStudents(SessionDocument document);
}
