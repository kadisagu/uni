/* $Id$ */
package ru.tandemservice.unisession.dao.eduplan;

import com.google.common.base.Preconditions;
import org.tandemframework.shared.commonbase.base.util.INumberQueueDAO;
import ru.tandemservice.uniepp.dao.eduplan.EppCustomEduPlanDAO;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf;
import ru.tandemservice.unisession.dao.eduplan.data.SessionCustomPlanWrapper;
import ru.tandemservice.unisession.entity.document.SessionTransferProtocolDocument;

/**
 * @author Alexey Lopatin
 * @since 25.11.2015
 */
public class SessionCustomEduPlanDAO extends EppCustomEduPlanDAO implements ISessionCustomEduPlanDAO
{
    @Override
    public void saveCustomEduPlan(EppCustomEduPlan customEduPlan, SessionTransferProtocolDocument protocol)
    {

        Preconditions.checkNotNull(customEduPlan);
        Preconditions.checkNotNull(customEduPlan.getEpvBlock());

        customEduPlan.setPlan((EppEduPlanProf) customEduPlan.getEpvBlock().getEduPlanVersion().getEduPlan());
        customEduPlan.setState(getCatalogItem(EppState.class, EppState.STATE_FORMATIVE));
        customEduPlan.setNumber(INumberQueueDAO.instance.get().getNextNumber(customEduPlan));

        saveOrUpdate(customEduPlan);

        saveCustomEduPlanContent(customEduPlan, new SessionCustomPlanWrapper(customEduPlan, protocol));
    }
}
