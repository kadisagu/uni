// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisession.dao.sessionObject;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.shared.commonbase.base.util.INumberGenerationRule;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unisession.entity.document.SessionObject;

import java.util.Collection;
import java.util.Map;

/**
 * @author oleyba
 * @since 2/8/11
 */
public interface ISessionObjectDAO
{
    SpringBeanCache<ISessionObjectDAO> instance = new SpringBeanCache<ISessionObjectDAO>(ISessionObjectDAO.class.getName());

    /**
     * правило генерации номера для сессии при создании новой
     * @return правило
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    INumberGenerationRule<SessionObject> getNumberGenerationRule();

    /**
     * Для каждого студента вычисляется номер последненго семестра, в котором у него есть оценки
     * @param studentIds список идентифиаторов студентов
     * @return { student.id -> term.intValue }
     */
    Map<Long, Integer> lastTermNumberMap(final Collection<Long> studentIds);

    /**
     * Получить сессию для указанного деканата, учебного года и части года
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    SessionObject getSessionObject(OrgUnit groupOrgUnit, EducationYear year, YearDistributionPart part);
}
