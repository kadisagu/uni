/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionProtocol.ui.MassAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;

import static org.tandemframework.hibsupport.dql.DQLExpressions.exists;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Andrey Andreev
 * @since 12.09.2016
 */
@Configuration
public class SessionProtocolMassAddEdit extends BusinessComponentManager
{
    public static final String BULLETIN_ID = "bulletinId";
    public static final String SESSION_DOCUMENT_SLOT_IDS = "sessionDocumentSlotIds";
    public static final String PROTOCOL_IDS = "protocolIds";

    public static final String EXAM_DAY_PARAM = "examDay";
    public static final String PPS_DS = "ppsDS";
    public static final String BULLETIN_PARAM = "bulletin";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(PPS_DS, ppsDSHandler())
                                       .addColumn(PpsEntry.fio().s())
                                       .addColumn(PpsEntry.orgUnit().shortTitle().s())
                                       .addColumn(PpsEntry.titlePostOrTimeWorkerData().s()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> ppsDSHandler()
    {
        EntityComboDataSourceHandler handler = PpsEntry.defaultSelectDSHandler(getName());

        handler.customize((alias, dql, context, filter) -> {

            SessionBulletinDocument bulletin = context.get(BULLETIN_PARAM);
            dql.where(exists(SessionComissionPps.class,
                             SessionComissionPps.pps().s(), property(alias),
                             SessionComissionPps.commission().s(), bulletin.getCommission()));
            return dql;
        });

        return handler;
    }
}