package ru.tandemservice.unisession.entity.document.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unisession.entity.catalog.SessionsSimpleDocumentReason;
import ru.tandemservice.unisession.entity.document.SessionDocument;
import ru.tandemservice.unisession.entity.document.SessionSimpleDocument;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Списочный документ сессии
 *
 * Списочный (сформированный вне основного процесса) документ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionSimpleDocumentGen extends SessionDocument
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.document.SessionSimpleDocument";
    public static final String ENTITY_NAME = "sessionSimpleDocument";
    public static final int VERSION_HASH = 1353393796;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORG_UNIT = "orgUnit";
    public static final String L_REASON = "reason";
    public static final String P_ISSUE_DATE = "issueDate";
    public static final String P_DEADLINE_DATE = "deadlineDate";
    public static final String P_TITLE = "title";

    private OrgUnit _orgUnit;     // Подразделение
    private SessionsSimpleDocumentReason _reason;     // Причина
    private Date _issueDate;     // Дата выдачи
    private Date _deadlineDate;     // Срок действия

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение. Свойство не может быть null.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Причина. Свойство не может быть null.
     */
    @NotNull
    public SessionsSimpleDocumentReason getReason()
    {
        return _reason;
    }

    /**
     * @param reason Причина. Свойство не может быть null.
     */
    public void setReason(SessionsSimpleDocumentReason reason)
    {
        dirty(_reason, reason);
        _reason = reason;
    }

    /**
     * @return Дата выдачи.
     */
    public Date getIssueDate()
    {
        return _issueDate;
    }

    /**
     * @param issueDate Дата выдачи.
     */
    public void setIssueDate(Date issueDate)
    {
        dirty(_issueDate, issueDate);
        _issueDate = issueDate;
    }

    /**
     * @return Срок действия.
     */
    public Date getDeadlineDate()
    {
        return _deadlineDate;
    }

    /**
     * @param deadlineDate Срок действия.
     */
    public void setDeadlineDate(Date deadlineDate)
    {
        dirty(_deadlineDate, deadlineDate);
        _deadlineDate = deadlineDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof SessionSimpleDocumentGen)
        {
            setOrgUnit(((SessionSimpleDocument)another).getOrgUnit());
            setReason(((SessionSimpleDocument)another).getReason());
            setIssueDate(((SessionSimpleDocument)another).getIssueDate());
            setDeadlineDate(((SessionSimpleDocument)another).getDeadlineDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionSimpleDocumentGen> extends SessionDocument.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionSimpleDocument.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("SessionSimpleDocument is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "orgUnit":
                    return obj.getOrgUnit();
                case "reason":
                    return obj.getReason();
                case "issueDate":
                    return obj.getIssueDate();
                case "deadlineDate":
                    return obj.getDeadlineDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "reason":
                    obj.setReason((SessionsSimpleDocumentReason) value);
                    return;
                case "issueDate":
                    obj.setIssueDate((Date) value);
                    return;
                case "deadlineDate":
                    obj.setDeadlineDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "orgUnit":
                        return true;
                case "reason":
                        return true;
                case "issueDate":
                        return true;
                case "deadlineDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "orgUnit":
                    return true;
                case "reason":
                    return true;
                case "issueDate":
                    return true;
                case "deadlineDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "orgUnit":
                    return OrgUnit.class;
                case "reason":
                    return SessionsSimpleDocumentReason.class;
                case "issueDate":
                    return Date.class;
                case "deadlineDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionSimpleDocument> _dslPath = new Path<SessionSimpleDocument>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionSimpleDocument");
    }
            

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionSimpleDocument#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Причина. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionSimpleDocument#getReason()
     */
    public static SessionsSimpleDocumentReason.Path<SessionsSimpleDocumentReason> reason()
    {
        return _dslPath.reason();
    }

    /**
     * @return Дата выдачи.
     * @see ru.tandemservice.unisession.entity.document.SessionSimpleDocument#getIssueDate()
     */
    public static PropertyPath<Date> issueDate()
    {
        return _dslPath.issueDate();
    }

    /**
     * @return Срок действия.
     * @see ru.tandemservice.unisession.entity.document.SessionSimpleDocument#getDeadlineDate()
     */
    public static PropertyPath<Date> deadlineDate()
    {
        return _dslPath.deadlineDate();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.entity.document.SessionSimpleDocument#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends SessionSimpleDocument> extends SessionDocument.Path<E>
    {
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private SessionsSimpleDocumentReason.Path<SessionsSimpleDocumentReason> _reason;
        private PropertyPath<Date> _issueDate;
        private PropertyPath<Date> _deadlineDate;
        private SupportedPropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionSimpleDocument#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Причина. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionSimpleDocument#getReason()
     */
        public SessionsSimpleDocumentReason.Path<SessionsSimpleDocumentReason> reason()
        {
            if(_reason == null )
                _reason = new SessionsSimpleDocumentReason.Path<SessionsSimpleDocumentReason>(L_REASON, this);
            return _reason;
        }

    /**
     * @return Дата выдачи.
     * @see ru.tandemservice.unisession.entity.document.SessionSimpleDocument#getIssueDate()
     */
        public PropertyPath<Date> issueDate()
        {
            if(_issueDate == null )
                _issueDate = new PropertyPath<Date>(SessionSimpleDocumentGen.P_ISSUE_DATE, this);
            return _issueDate;
        }

    /**
     * @return Срок действия.
     * @see ru.tandemservice.unisession.entity.document.SessionSimpleDocument#getDeadlineDate()
     */
        public PropertyPath<Date> deadlineDate()
        {
            if(_deadlineDate == null )
                _deadlineDate = new PropertyPath<Date>(SessionSimpleDocumentGen.P_DEADLINE_DATE, this);
            return _deadlineDate;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.entity.document.SessionSimpleDocument#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(SessionSimpleDocumentGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return SessionSimpleDocument.class;
        }

        public String getEntityName()
        {
            return "sessionSimpleDocument";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getTitle();
}
