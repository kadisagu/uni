/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionTransfer.ui.ProtocolAddEdit;

import com.beust.jcommander.internal.Lists;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.pps.PpsEntryByEmployeePost;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.unisession.base.bo.SessionTransfer.SessionTransferManager;
import ru.tandemservice.unisession.entity.comission.SessionComission;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionTransferProtocolDocument;
import ru.tandemservice.unisession.entity.mark.SessionALRequestRow;

import java.util.Date;
import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 11.09.2015
 */
@State({
        @Bind(key = SessionTransferProtocolAddEditUI.PARAM_STUDENT_ID, binding = "student.id", required = true),
        @Bind(key = SessionTransferProtocolAddEditUI.PARAM_PROTOCOL_ID, binding = "protocol.id")
})
public class SessionTransferProtocolAddEditUI extends UIPresenter
{
    public static final String PARAM_PROTOCOL_ID = "protocolId";
    public static final String PARAM_STUDENT_ID = "studentId";

    public static final String PARAM_REQUEST = "request";

    private Student _student = new Student();
    private SessionTransferProtocolDocument _protocol = new SessionTransferProtocolDocument();
    private List<PpsEntryByEmployeePost> _commissionPpsList = Lists.newArrayList();
    private ViewWrapper<EppWorkPlanBase> _workPlan;

    @Override
    public void onComponentRefresh()
    {
        _student = DataAccessServices.dao().getNotNull(_student.getId());
        if (isEditForm())
        {
            _protocol = DataAccessServices.dao().getNotNull(_protocol.getId());
            setWorkPlan(new ViewWrapper<>(_protocol.getWorkPlan()));

            List<SessionComissionPps> commissionPpsList = DataAccessServices.dao().getList(SessionComissionPps.class, SessionComissionPps.commission(), _protocol.getCommission());
            commissionPpsList.forEach(commissionPps -> _commissionPpsList.add((PpsEntryByEmployeePost) commissionPps.getPps()));
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(PARAM_STUDENT_ID, getStudent().getId());

        if (SessionTransferProtocolAddEdit.REQUEST_DS.equals(dataSource.getName()))
        {
            dataSource.put(PARAM_STUDENT_ID, getStudent().getId());
        }
        else if (SessionTransferProtocolAddEdit.WORK_PLAN_DS.equals(dataSource.getName()))
        {
            dataSource.put(PARAM_REQUEST, _protocol.getRequest());
        }
    }

    public void onClickApply()
    {

        SessionComission commission = SessionTransferManager.instance().dao().saveCommission(_protocol.getCommission(), _commissionPpsList);
        boolean addForm = !isEditForm();

        if (addForm)
            _protocol.setFormingDate(new Date());

        _protocol.setWorkPlan(_workPlan.getEntity());
        _protocol.setCommission(commission);
        DataAccessServices.dao().saveOrUpdate(_protocol);

        if (addForm)
        {
            List<SessionALRequestRow> requestRows = DataAccessServices.dao().getList(SessionALRequestRow.class, SessionALRequestRow.request().id(), _protocol.getRequest().getId());
            SessionTransferManager.instance().dao().saveProtocolRowAndMark(_protocol, requestRows);
        }

        deactivate();
    }

    // Utils

    public boolean isEditForm()
    {
        return getProtocol().getId() != null;
    }

    // Getters & Setters

    public Student getStudent()
    {
        return _student;
    }

    public void setStudent(Student student)
    {
        _student = student;
    }

    public SessionTransferProtocolDocument getProtocol()
    {
        return _protocol;
    }

    public void setProtocol(SessionTransferProtocolDocument protocol)
    {
        _protocol = protocol;
    }

    public List<PpsEntryByEmployeePost> getCommissionPpsList()
    {
        return _commissionPpsList;
    }

    public void setCommissionPpsList(List<PpsEntryByEmployeePost> commissionPpsList)
    {
        _commissionPpsList = commissionPpsList;
    }

    public ViewWrapper<EppWorkPlanBase> getWorkPlan()
    {
        return _workPlan;
    }

    public void setWorkPlan(ViewWrapper<EppWorkPlanBase> workPlan)
    {
        _workPlan = workPlan;
    }
}
