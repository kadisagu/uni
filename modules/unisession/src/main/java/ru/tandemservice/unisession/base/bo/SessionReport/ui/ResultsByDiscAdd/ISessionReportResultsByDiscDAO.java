/* $Id:$ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.ResultsByDiscAdd;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.report.UnisessionResultsByDiscReport;

import java.util.Collection;
import java.util.List;

/**
 * @author oleyba
 * @since 2/19/12
 */
public interface ISessionReportResultsByDiscDAO
{
    SpringBeanCache<ISessionReportResultsByDiscDAO> instance = new SpringBeanCache<>(ISessionReportResultsByDiscDAO.class.getName());

    interface ISessionReportResultsByDiscGrouping extends ITitled, Comparable
    {
        String getKey();
        
        List<ISessionReportResultsByDiscRow> getReportRowGroups(SessionReportResultsByDiscAddUI model, Collection<ISessionReportResultsByDiscMarkData> marks);
        List<ISessionReportResultsColumn> getReportColumns(SessionReportResultsByDiscAddUI model, Collection<ISessionReportResultsByDiscMarkData> marks);
    }

    interface ISessionReportResultsByDiscMarkData
    {
        Long getSlotId();
        Long getStudentId();

        Course getCourse();
        Term getTerm();
        String getIndex();
        EppRegistryElementPart getDisc();
        SessionMarkCatalogItem getMark();
    }

    interface ISessionReportResultsByDiscRow
    {
        Object getKey();
        List<String> getTableRowStart();
        Collection<ISessionReportResultsByDiscMarkData> getMarks();
        List<ISessionReportResultsByDiscRow> getSubRows();
    }
    
    interface ISessionReportResultsColumn
    {
        String cell(ISessionReportResultsByDiscRow row);
    }    

    UnisessionResultsByDiscReport createStoredReport(SessionReportResultsByDiscAddUI model, ISessionReportResultsByDiscGrouping grp) throws Exception;
}
