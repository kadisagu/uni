/* $Id$ */
package ru.tandemservice.unisession.component.sessionRetakeDoc.SessionRetakeDocPubInline;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.utils.PublisherColumnBuilder;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.mark.SessionMark;

/**
 * @author oleyba
 * @since 6/15/11
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);
        this.prepareListDataSource(component);
    }

    private void prepareListDataSource(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        final DynamicListDataSource<SessionDocumentSlot> dataSource = new DynamicListDataSource<>(component, component1 -> {
            Controller.this.getDao().prepareListDataSource(model);
        });

        dataSource.addColumn(new SimpleColumn("№ зач. книжки", SessionDocumentSlot.actualStudent().bookNumber().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new PublisherColumnBuilder("ФИО", Student.FIO_KEY, "studentNewSessionTab").subTab("studentSessionMarkTab").tabBind(ru.tandemservice.uni.component.student.StudentPub.Model.SELECTED_TAB_BIND_KEY).entity(SessionDocumentSlot.actualStudent().s()).build());
        dataSource.addColumn(new SimpleColumn("Вид затрат", SessionDocumentSlot.actualStudent().compensationType().shortTitle().s()).setOrderable(false).setClickable(false));
        if (model.isShowCATypeColumn())
            dataSource.addColumn(new SimpleColumn("Форма контроля", SessionDocumentSlot.studentWpeCAction().type().title().s()).setOrderable(false).setClickable(false));
        if (model.isUseCurrentRating())
		{
            dataSource.addColumn(new SimpleColumn("Текущий рейтинг", "currentRating").setClickable(false).setOrderable(false));
            dataSource.addColumn(new SimpleColumn("Баллы за сдачу мероприятия", "scoredPoints").setClickable(false).setOrderable(false));
            dataSource.addColumn(new SimpleColumn("Суммарный рейтинг", "points").setClickable(false).setOrderable(false));
        }
        else if (model.isUsePoints())
            dataSource.addColumn(new SimpleColumn("Баллы за сдачу мероприятия", "points").setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("В сессию", SessionDocumentSlot.inSession().s(), YesNoFormatter.INSTANCE).setClickable(false).setOrderable(false));
		if (model.isThemeRequired())
			dataSource.addColumn(new SimpleColumn("Тема", IDAO.PROP_PROJECT_THEME).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Оценка", IDAO.V_MARK + "." + SessionMark.valueShortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Дата сдачи", IDAO.V_MARK + "." + SessionMark.performDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Число пересдач", "", "onClickShowHistory").setTextKey(IDAO.V_MARK_COUNT).setDisplayHeader(true).setPermissionKey("viewMarkHistorySessionRetakeDoc"));

        model.setDataSource(dataSource);
        this.getDao().prepareListDataSource(model);
    }

    public void onClickShowHistory(final IBusinessComponent component)
    {
        final SessionDocumentSlot slot = UniDaoFacade.getCoreDao().get(SessionDocumentSlot.class, component.getListenerParameter());
        if (null != slot) {
            ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                    ru.tandemservice.unisession.component.student.ControlActionMarkHistory.Model.COMPONENT_NAME,
                    new ParametersMap().add("controlActionId", slot.getStudentWpeCAction().getId()).add("showAsSingle", true)));
        }
    }

    public void onClickMark(final IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(
                                          ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinMark.Model.COMPONENT_NAME,
                                          new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, this.getModel(component).getRetakeDoc().getId()))
        );
    }

    public void onClickEdit(final IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                ru.tandemservice.unisession.component.sessionRetakeDoc.SessionRetakeDocEdit.Model.COMPONENT_NAME,
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, this.getModel(component).getRetakeDoc().getId())));
    }
}