package ru.tandemservice.unisession.events;

import java.util.Date;

import org.hibernate.Session;
import org.tandemframework.core.bean.FastBeanUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.event.single.ISingleEntityEvent;
import org.tandemframework.hibsupport.event.single.listener.IHibernateEventListener;

import ru.tandemservice.unisession.base.bo.SessionMark.daemon.SessionMarkDaemonBean;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.mark.SessionSlotRegularMark;

/**
 * @author vdanilov
 * TODO: dset
 */
public class SessionMarkModificationListener implements IHibernateEventListener<ISingleEntityEvent> {

    @Override public void onEvent(final ISingleEntityEvent event) {
        final IEntity entity = event.getEntity();
        if (entity instanceof SessionMark) {
            final Date now = new Date();
            final SessionMark sessionMark = (SessionMark)entity;
            if (null == sessionMark.getModificationDate() || sessionMark.getModificationDate().before(now)) {
                sessionMark.setModificationDate(now);
            }

            if (entity instanceof SessionSlotRegularMark) {
                // если изменилась обычная оценка - обновляем итоговые (ссылки на нее)
                SessionMarkDaemonBean.DAEMON.registerAfterCompleteWakeUp(event.getSession());
            }
        }
    }
}
