// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisession.util.selectModel;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectValueStyle;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import org.tandemframework.tapsupport.component.selection.hselect.IHSelectModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 2/16/11
 */
public abstract class SessionTermModel implements IHSelectModel, ISingleSelectModel, IMultiSelectModel
{
    public interface IStudentBasedDataOwner
    {
        Student getStudent();
        EppStudentWpeCAction getEppSlot();
    }

	public static SessionTermModel createForStudent(final Student student)
	{
		return create(getSingleStudentOwner(student));
	}

	public static SessionTermModel createForStudent(final Student student, boolean onlyActiveWpeCAction)
	{
		return create(getSingleStudentOwner(student), onlyActiveWpeCAction);
	}

	private static IStudentBasedDataOwner getSingleStudentOwner(final Student student)
	{
		return new IStudentBasedDataOwner()
		{
			@Override
			public Student getStudent()
			{
				return student;
			}

			@Override
			public EppStudentWpeCAction getEppSlot()
			{
				return null;
			}
		};
	}

	public static SessionTermModel create(final IStudentBasedDataOwner dataOwner)
    {
        return create(dataOwner, true);
    }

	public static SessionTermModel create(final IStudentBasedDataOwner dataOwner, boolean onlyActiveWpeCAction)
	{
		return new SessionTermModel()
		{
			@Override
			public Map<EducationYear, Set<YearDistributionPart>> getValueMap()
			{
				Student student = dataOwner.getStudent();
				if (student == null) return new LinkedHashMap<>();

				final DQLSelectBuilder slotDQL = new DQLSelectBuilder()
						.fromEntity(EppStudentWpeCAction.class, "eppSlot")
						.predicate(DQLPredicateType.distinct)
						.column(property("baseSlot", EppStudentWorkPlanElement.year().educationYear().id()))
						.column(property("baseSlot", EppStudentWorkPlanElement.part().id()))
						.joinPath(DQLJoinType.inner, EppStudentWpeCAction.studentWpe().fromAlias("eppSlot"), "baseSlot")
						.where(eq(property("baseSlot", EppStudentWorkPlanElement.student()), value(student)));

				if (onlyActiveWpeCAction)
					slotDQL.where(isNull(property("eppSlot", EppStudentWpeCAction.removalDate())));

				return getYear2YearParts(slotDQL);
			}
		};
	}

    protected Map<EducationYear, Set<YearDistributionPart>> getYear2YearParts(DQLSelectBuilder wpeCaBuilder)
    {
        return IUniBaseDao.instance.get().getCalculatedValue(session -> {
            final Map<EducationYear, Set<YearDistributionPart>> result = new TreeMap<>(Comparator.comparingInt(EducationYear::getIntValue));
            for (final Object[] row : wpeCaBuilder.createStatement(session).<Object[]>list())
            {
                final EducationYear year = (EducationYear) session.get(EducationYear.class, (Long) row[0]);
                final YearDistributionPart yearPart = (YearDistributionPart) session.get(YearDistributionPart.class, (Long) row[1]);
                SafeMap.safeGet(result, year, key -> new TreeSet<>(Comparator.comparingInt(YearDistributionPart::getNumber))).add(yearPart);
            }
            return result;
        });
    }

    public abstract Map<EducationYear, Set<YearDistributionPart>> getValueMap();

    @Override
    public int getLevel(final Object value)
    {
        if (null == value)
            return 0;

        if (!(value instanceof SessionTermModel.TermWrapper))
            return 0;

        final SessionTermModel.TermWrapper wrapper = (SessionTermModel.TermWrapper) value;
        if (null == wrapper.getPart())
            return 0;

        return 1;
    }

    @Override
    @Deprecated
    public boolean isSelectable(final Object value)
    {
        if (null == value)
            return false;

        if (!(value instanceof SessionTermModel.TermWrapper))
            return false;

        final SessionTermModel.TermWrapper wrapper = (SessionTermModel.TermWrapper) value;
        return wrapper.getPart() != null;
    }

    @Override
    public Object getRawValue(final Object value)
    {
        return value;
    }

    @Override
    public Object getPackedValue(final Object value)
    {
        return value;
    }

    @Override
    public Object getPrimaryKey(final Object value)
    {
        if (null == value)
            return null;

        if (!(value instanceof SessionTermModel.TermWrapper))
            return null;

        return new SessionTermModel.TermKey((SessionTermModel.TermWrapper) value);
    }

    @Override
    public int getColumnCount()
    {
        return 1;
    }

    @Override
    public String[] getColumnTitles()
    {
        return null;
    }

    @Override
    public String getLabelFor(final Object value, final int columnIndex)
    {
        if (null == value)
            return "";

        if (!(value instanceof SessionTermModel.TermWrapper))
            return "";

        final SessionTermModel.TermWrapper wrapper = (SessionTermModel.TermWrapper) value;
        return (wrapper.getPart() == null) ? wrapper.getYear().getTitle() : wrapper.getPart().getTitle();
    }

    @Override
    public String getFullLabel(final Object value)
    {
        return this.getLabelFor(value, 0);
    }

    @Override
    public ISelectValueStyle getValueStyle(final Object value)
    {
        return null;
    }

    @Override
    public List getValues(final Set primaryKeys)
    {
        final Map<EducationYear, Set<YearDistributionPart>> valueMap = this.getValueMap();
        final List<Object> result = new ArrayList<>();
        for (final Object primaryKey : primaryKeys)
        {
            if (primaryKey == null || !(primaryKey instanceof SessionTermModel.TermKey))
                continue;

            final SessionTermModel.TermKey key = (SessionTermModel.TermKey) primaryKey;
            for (final Map.Entry<EducationYear, Set<YearDistributionPart>> entry : valueMap.entrySet())
            {
                final EducationYear year = entry.getKey();
                if (year.getId().equals(key.getYearId()))
                {
                    if (key.getPartId() == null) {
                        result.add(new SessionTermModel.TermWrapper(year));
                    } else {
                        for (final YearDistributionPart part : entry.getValue()) {
                            if (part.getId().equals(key.getPartId())) {
                                result.add(new SessionTermModel.TermWrapper(year, part));
                            }
                        }
                    }
                }
            }
        }
        return result;
    }

    @Override
    public Object getValue(final Object primaryKey)
    {
        final List values = this.getValues(Collections.singleton(primaryKey));
        if (values.isEmpty())
            return null;
        return values.get(0);
    }

    @Override
    public ListResult findValues(final String filter)
    {
        final Map<EducationYear, Set<YearDistributionPart>> valueMap = this.getValueMap();
        final List<SessionTermModel.TermWrapper> result = new ArrayList<>();
        for (final Map.Entry<EducationYear, Set<YearDistributionPart>> entry : valueMap.entrySet())
        {
            final EducationYear year = entry.getKey();
            result.add(new SessionTermModel.TermWrapper(year));
            for (final YearDistributionPart part : entry.getValue()) {
                result.add(new SessionTermModel.TermWrapper(year, part));
            }
        }
        return new ListResult<>(result);
    }

    @SuppressWarnings("serial")
    public static class TermWrapper implements Serializable
    {
        private EducationYear year;
        private YearDistributionPart part;
        private TermKey key;

        public TermWrapper(final SessionDocumentSlot slot)
        {
            this(slot.getStudentWpeCAction());
        }

        public TermWrapper(final EppStudentWpeCAction eppSlot)
        {
            this(eppSlot.getStudentWpe().getYear().getEducationYear(), eppSlot.getStudentWpe().getPart());
        }

        public TermWrapper(final EducationYear year, final YearDistributionPart part)
        {
            this.year = year;
            this.part = part;
            this.key = new TermKey(this);
        }

        public TermWrapper(final EducationYear year)
        {
            this.year = year;
            this.key = new TermKey(this);
        }

        public EducationYear getYear()
        {
            return this.year;
        }

        public YearDistributionPart getPart()
        {
            return this.part;
        }

        @Override
        public int hashCode()
        {
            return this.key.hashCode();
        }

        @Override
        public boolean equals(final Object obj)
        {
            if (obj == this)
                return true;

            if (obj instanceof TermWrapper)
            {
                final TermWrapper that = (TermWrapper) obj;
                return this.key.equals(that.key);
            }
            return false;
        }

        private void writeObject(final java.io.ObjectOutputStream out) throws IOException
        {
            out.writeLong(this.year.getId());
            out.writeLong(this.part.getId());
        }

        private void readObject(final java.io.ObjectInputStream in) throws IOException, ClassNotFoundException
        {
            this.year = IUniBaseDao.instance.get().get(EducationYear.class, in.readLong());
            this.part = IUniBaseDao.instance.get().get(YearDistributionPart.class, in.readLong());
            this.key = new TermKey(this);
        }

        public String getFullTitle()
        {
            String y = year == null ? null : " (" + year.getTitle() + ")";
            String p = part == null ? null : part.getTitle();

            if (p != null && y != null) return p + y;

            if (p != null) return p;

            if (y != null) return y;

            return "";
        }
    }

    public static class TermKey implements Serializable
    {
        private static final long serialVersionUID = 1L;
        private final Long yearId;
        private Long partId;
        private final MultiKey key;

        public TermKey(final SessionTermModel.TermWrapper wrapper)
        {
            this.yearId = wrapper.getYear().getId();
            if (wrapper.getPart() != null) {
                this.partId = wrapper.getPart().getId();
            }
            this.key = new MultiKey(this.yearId, this.partId);
        }

        public Long getYearId()
        {
            return this.yearId;
        }

        public Long getPartId()
        {
            return this.partId;
        }

        @Override
        public int hashCode()
        {
            return this.key.hashCode();
        }

        @Override
        public boolean equals(final Object obj)
        {
            if (obj == this)
                return true;

            if (obj instanceof TermKey)
            {
                final TermKey that = (TermKey) obj;
                return this.key.equals(that.key);
            }
            return false;
        }
    }


    /**
     * Билдер актуальных мероприятий МРСП без колонок для выбранного студента.
     * @param student Студент.
     * @param debtsOnly Ограничить выдачу мероприятиями по которым у студента долг.
     * @param alias Алиас МСРП-ФК.
     */
    public static DQLSelectBuilder getStudentWpeCActionsBuilder(Student student, boolean debtsOnly, String alias)
    {
        DQLSelectBuilder wpeCaBuilder = new DQLSelectBuilder()
                .fromEntity(EppStudentWpeCAction.class, alias)
                .where(isNull(property(EppStudentWpeCAction.removalDate().fromAlias(alias))))
                .where(eq(property(alias, EppStudentWpeCAction.studentWpe().student()), value(student)));

        if (debtsOnly)
        {
            DQLSelectBuilder markDql = new DQLSelectBuilder()
                    .fromEntity(SessionMark.class, "mark")
                    .where(eq(property("mark", SessionMark.slot().studentWpeCAction().studentWpe().student()), value(student)))
                    .order(property("mark", SessionMark.performDate()), OrderDirection.desc);
            List<SessionMark> marks = DataAccessServices.dao().getList(markDql);

            Map<EppStudentWpeCAction, SessionMark> wpeCA2LastMark = marks.stream()
                    .collect(Collectors.groupingBy(
                            mark -> mark.getSlot().getStudentWpeCAction(),
                            Collectors.mapping(mark -> mark, Collectors.reducing(null, (mark1, mark2) ->
                            {
                                if (mark1 == null) return mark2;
                                if (mark2 == null) return mark1;
                                if (mark1.getPerformDate().compareTo(mark2.getPerformDate()) > 0) return mark1;
                                else return mark2;
                            }))
                    ));

            List<EppStudentWpeCAction> WpeCas = wpeCA2LastMark.entrySet().stream()
                    .filter(entry -> !entry.getValue().getCachedMarkPositiveStatus())
                    .map(Map.Entry::getKey)
                    .collect(Collectors.toList());

            wpeCaBuilder.where(in(property(alias), WpeCas));
        }

        return wpeCaBuilder;
    }
}
