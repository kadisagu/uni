/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionMark;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unisession.base.bo.SessionMark.logic.ISessionMarkDAO;
import ru.tandemservice.unisession.base.bo.SessionMark.logic.SessionMarkDAO;

/**
 * @author oleyba
 * @since 10/14/11
 */
@Configuration
public class SessionMarkManager extends BusinessObjectManager
{
    public static SessionMarkManager instance()
    {
        return instance(SessionMarkManager.class);
    }

    @Bean
    public ISessionMarkDAO regularMarkDao() {
        return new SessionMarkDAO();
    }

}
