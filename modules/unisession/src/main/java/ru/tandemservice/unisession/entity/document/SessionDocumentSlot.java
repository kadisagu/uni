package ru.tandemservice.unisession.entity.document;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.shared.commonbase.base.util.Wiki;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.entity.document.gen.SessionDocumentSlotGen;

/**
 * Запись мероприятия студента в сессии
 *
 * показывает, что указанный слот студента по форме контроля дотупен для отметки в рамках указанного документа
 */
public class SessionDocumentSlot extends SessionDocumentSlotGen implements ITitled
{
    public SessionDocumentSlot()
    {
    }

    public SessionDocumentSlot(final SessionDocument document, final EppStudentWpeCAction student, final boolean inSession) {
        this.setDocument(document);
        this.setStudentWpeCAction(student);
        this.setInSession(inSession);
    }

    @Override
    @EntityDSLSupport(parts = {
            SessionDocumentSlotGen.L_STUDENT_WPE_C_ACTION + "." + EppStudentWpeCAction.L_STUDENT_WPE + "." + EppStudentWorkPlanElement.L_YEAR + "." + EppYearEducationProcess.L_EDUCATION_YEAR + "." + EducationYear.P_INT_VALUE,
            SessionDocumentSlotGen.L_STUDENT_WPE_C_ACTION + "." + EppStudentWpeCAction.L_STUDENT_WPE + "." + DevelopGridTerm.L_PART + "." + YearDistributionPart.P_NUMBER
    })
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1060324")
    public String getTermTitle() {
        return this.getStudentWpeCAction().getStudentWpe().getPart().getTitle() + " " + this.getStudentWpeCAction().getStudentWpe().getYear().getEducationYear().getTitle();
    }

    public static final String L_ACTUAL_STUDENT =  SessionDocumentSlotGen.L_STUDENT_WPE_C_ACTION + "." + EppStudentWpeCAction.L_STUDENT_WPE + "." + EppStudentWorkPlanElement.L_STUDENT;
    private static final Student.Path _actualStudentInstance = new Student.Path(SessionDocumentSlot.L_ACTUAL_STUDENT);
    public static Student.Path actualStudent() { return SessionDocumentSlot._actualStudentInstance; }

    public Student getActualStudent() {
        return this.getStudentWpeCAction().getStudentWpe().getStudent();
    }

    @Override public String toString() {
        return this.getClass().getSimpleName()+"@"+this.getId()+"("+this.getStudentWpeCAction().getId()+","+this.getDocument().getId()+")";
    }

    @Override
    public String getTitle()
    {
        if (getStudentWpeCAction() == null) {
            return this.getClass().getSimpleName();
        }
        return "Запись студента (" + getStudentWpeCAction().getTitle() + ") в документе сессии (" + getDocument().getTypeTitle() + ")";
    }

    /**
     * @deprecated use getStudentWpeCAction()
     * @return studentWpeCAction
     */
    public EppStudentWpeCAction getStudent() {
        return getStudentWpeCAction();
    }

}