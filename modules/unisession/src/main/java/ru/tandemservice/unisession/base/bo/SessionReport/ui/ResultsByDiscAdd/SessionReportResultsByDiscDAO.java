/* $Id:$ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.ResultsByDiscAdd;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.bo.Declinable.DeclinableManager;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.ICommonFilterItem;
import org.tandemframework.shared.commonbase.catalog.entity.InflectorVariant;
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.util.PersonSecurityUtil;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.DebugUtils;
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale;
import ru.tandemservice.uniepp.entity.catalog.codes.EppGradeScaleCodes;
import ru.tandemservice.uniepp.entity.catalog.codes.EppGroupTypeFCACodes;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.base.bo.SessionReport.util.SessionReportUtil;
import ru.tandemservice.unisession.base.bo.SessionReport.util.UniSessionFilterAddon;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.catalog.UnisessionTemplate;
import ru.tandemservice.unisession.entity.catalog.codes.UnisessionCommonTemplateCodes;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionStudentGradeBookDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.report.UnisessionResultsByDiscReport;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 2/19/12
 */
public class SessionReportResultsByDiscDAO extends UniBaseDao implements ISessionReportResultsByDiscDAO
{
    @Override
    public UnisessionResultsByDiscReport createStoredReport(SessionReportResultsByDiscAddUI model, ISessionReportResultsByDiscGrouping grp) throws Exception
    {
        final UnisessionResultsByDiscReport report = new UnisessionResultsByDiscReport();

        report.setFormingDate(new Date());
        report.setExecutor(PersonSecurityUtil.getExecutor());

        report.setOrgUnit(model.getOrgUnit());
        report.setEducationYear(model.getYear());
        report.setYearDistributionPart(model.getPart());

        if (null == grp)
            throw new IllegalStateException();

        report.setGrouping(grp.getTitle());


        final UniSessionFilterAddon filterAddon = model.getSessionFilterAddon();

        if (model.isPerformDateFromActive())
        {
            report.setPerformDateFrom(model.getPerformDateFrom());
        }
        if (model.isPerformDateToActive())
        {
            report.setPerformDateFrom(model.getPerformDateTo());
        }


        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_DISC_KIND, UnisessionResultsByDiscReport.P_DISC_KINDS, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_COMPENSATION_TYPE, UnisessionResultsByDiscReport.P_COMPENSATION_TYPE, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_GROUP_ORG_UNIT, UnisessionResultsByDiscReport.P_GROUP_ORG_UNIT, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_TERRITORIAL_ORG_UNIT, UnisessionResultsByDiscReport.P_TERRITORIAL_ORG_UNIT, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_DEVELOP_FORM, UnisessionResultsByDiscReport.P_DEVELOP_FORM, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_DEVELOP_CONDITION, UnisessionResultsByDiscReport.P_DEVELOP_CONDITION, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_DEVELOP_TECH, UnisessionResultsByDiscReport.P_DEVELOP_TECH, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_DEVELOP_PERIOD, UnisessionResultsByDiscReport.P_DEVELOP_PERIOD, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_COURSE, UnisessionResultsByDiscReport.P_COURSE, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_STUDENT_STATUS, UnisessionResultsByDiscReport.P_STUDENT_STATUS, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_CUSTOM_STATE, UnisessionResultsByDiscReport.P_CUSTOM_STATE, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_TARGET_ADMISSION, UnisessionResultsByDiscReport.P_TARGET_ADMISSION, "title");


        DatabaseFile content = new DatabaseFile();
        content.setContent(print(model, grp));
        save(content);
        report.setContent(content);

        save(report);

        return report;
    }
    
    /**
     * Проверка шкал оценок - может ли быть построен отчет с такими данными.
     * Продуктовая логика работает только в том случае, если все оценки по экзаменам - из пятибалльной шкалы,
     * поэтому если найдены оценки в другой шкале, в этом методе выдается сообщение об ошибке.
     * Соответственно, если переопределена логика работы отчета, нужно переопределить и логику работы этой проверки
     * @param marks подготовленный набор данных о студентах и оценках, попавших в статистику отчета
     */
    protected void checkScales(Collection<ISessionReportResultsByDiscMarkData> marks)
    {
        for (ISessionReportResultsByDiscMarkData mark : marks) {
            if (mark.getMark() instanceof SessionMarkGradeValueCatalogItem) {
                EppGradeScale scale = ((SessionMarkGradeValueCatalogItem) mark.getMark()).getScale();
                if (!EppGradeScaleCodes.SCALE_5.equals(scale.getCode()))
                    throw new ApplicationException("Отчет не может быть построен, т.к. у некоторых студентов есть экзамены со шкалой оценок отличной от пятибалльной.");
            }
        }
    }    

    private byte[] print(SessionReportResultsByDiscAddUI model, ISessionReportResultsByDiscGrouping grp) throws Exception
    {
        final ICommonFilterItem compensationFilterItem = model.getSessionFilterAddon().getFilterItem(UniSessionFilterAddon.SETTINGS_NAME_COMPENSATION_TYPE);

        UnisessionTemplate templateItem = getCatalogItem(UnisessionTemplate.class, UnisessionCommonTemplateCodes.RESULTS_BY_DISC_REPORT);
        if (templateItem == null) {
            throw new RuntimeException("Печатный шаблон есть, но не импортирован в справочник шаблонов.");
        }
        RtfDocument rtf = new RtfReader().read(templateItem.getContent());

        Collection<ISessionReportResultsByDiscMarkData> marks = prepareMarksData(model);
        checkScales(marks);

        List<ISessionReportResultsColumn> columns = grp.getReportColumns(model, marks);
        List<ISessionReportResultsByDiscRow> reportRowGroups = grp.getReportRowGroups(model, marks);

        MutableInt rowNum = new MutableInt(1);
        List<String[]> reportTable = new ArrayList<>();
        for (ISessionReportResultsByDiscRow group : reportRowGroups) {
            print(columns, rowNum, group, reportTable);
        }

        new RtfTableModifier().put("T", reportTable.toArray(new String[reportTable.size()][])).modify(rtf);

        OrgUnit academy = TopOrgUnit.getInstance();
        InflectorVariant inflVariant = IUniBaseDao.instance.get().getCatalogItem(InflectorVariant.class, InflectorVariantCodes.RU_GENITIVE);

        RtfInjectModifier modifier = new RtfInjectModifier();
        modifier.put("vuzTitle", academy.getPrintTitle());
        if (model.getOrgUnit() == null)
            SharedRtfUtil.removeParagraphsWithTagsRecursive(rtf, Collections.singletonList("groupOrgUnitTitle"), false, false);
        else
            modifier.put("groupOrgUnitTitle", model.getOrgUnit().getPrintTitle());
        modifier.put("compType", compensationFilterItem.isEnableCheckboxChecked() ? "(" + ((CompensationType) compensationFilterItem.getValue()).getShortTitle() + ")" : "");
        modifier.put("season", StringUtils.trimToEmpty(DeclinableManager.instance().dao().getPropertyValue(model.getPart(), YearDistributionPart.DECLINATION_PROPERTY_SESSION_SHORT_TITLE, inflVariant)));
        modifier.put("eduYear", model.getYear().getTitle());
        modifier.put("executor", PersonSecurityUtil.getExecutor());
        modifier.put("formingDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()));

        //должность и ФИО руководителя подразделения
        SessionReportManager.addOuLeaderData(modifier, model.getOrgUnit(), "ouleader", "FIOouleader");
        modifier.modify(rtf);

        return RtfUtil.toByteArray(rtf);
    }

    private Collection<ISessionReportResultsByDiscMarkData> prepareMarksData(final SessionReportResultsByDiscAddUI model) throws Exception
    {
        final Set<Long> students = new HashSet<>();
        
        DebugUtils.debug(logger, new DebugUtils.Section("prepareStudentList")
        {
            @Override
            public void execute()
            {
                DQLSelectBuilder dql = new DQLSelectBuilder()
                    .fromEntity(Student.class, "s")
                    .fromEntity(SessionDocumentSlot.class, "slot")
                    .where(eq(property(SessionDocumentSlot.actualStudent().fromAlias("slot")), property("s")))
                    .fromEntity(SessionBulletinDocument.class, "b")
                    .where(eq(property(SessionDocumentSlot.document().fromAlias("slot")), property("b")))
                    .joinPath(DQLJoinType.inner, SessionDocumentSlot.studentWpeCAction().fromAlias("slot"), "wpeCa")
                    .column("s.id");

                dql
                    // активные и неархивные студенты
                    .where(eq(property(Student.archival().fromAlias("s")), value(Boolean.FALSE)))
                    .where(eq(property(Student.status().active().fromAlias("s")), value(Boolean.TRUE)))
                    // только актуальные мероприятия, по которым студент включен в ведомость
                    .where(isNull(property(SessionDocumentSlot.studentWpeCAction().removalDate().fromAlias("slot"))))
                    // только текущий год
                    .where(eq(property(SessionBulletinDocument.sessionObject().educationYear().fromAlias("b")), value(model.getYear())))
                    // обязательные параметры отчета - часть года, виды дисциплин
                    .where(eq(property(SessionBulletinDocument.sessionObject().yearDistributionPart().fromAlias("b")), value(model.getPart())))
                ;

                model.getSessionFilterAddon().applyFilters(dql, "wpeCa");

                students.addAll(dql.createStatement(getSession()).<Long>list());
            }
        });
        
        final List<ISessionReportResultsByDiscMarkData> marks = new ArrayList<>();

        DebugUtils.debug(logger, new DebugUtils.Section("prepareMarkData")
        {
            @Override
            public void execute()
            {
                BatchUtils.execute(students, 256, elements -> {
                    DQLSelectBuilder dql = new DQLSelectBuilder()
                        .fromEntity(Student.class, "s")
                        .where(in(property(Student.id().fromAlias("s")), elements))
                        .fromEntity(SessionDocumentSlot.class, "slot")
                        .where(eq(property(SessionDocumentSlot.actualStudent().fromAlias("slot")), property("s")))
                        .fromEntity(SessionStudentGradeBookDocument.class, "g")
                        .where(eq(property(SessionDocumentSlot.document().fromAlias("slot")), property("g")))
                        .joinEntity("slot", DQLJoinType.left, SessionMark.class, "m", eq(property(SessionMark.slot().fromAlias("m")), property("slot")))
                        .joinEntity("m", DQLJoinType.left, SessionMarkCatalogItem.class, "mvalue", eq(property(SessionMark.cachedMarkValue().fromAlias("m")), property("mvalue")))
                        .joinEntity("slot", DQLJoinType.left, EppStudentWpeCAction.class, "wpca", eq(property(SessionDocumentSlot.studentWpeCAction().fromAlias("slot")), property("wpca")))
                        .joinEntity("wpca", DQLJoinType.left, EppStudentWorkPlanElement.class, "epvSlot", eq(property(EppStudentWpeCAction.studentWpe().fromAlias("wpca")), property("epvSlot")))
                        .joinEntity("epvSlot", DQLJoinType.left, EppWorkPlanRegistryElementRow.class, "wpRow", eq(property(EppStudentWorkPlanElement.sourceRow().fromAlias("epvSlot")), property("wpRow")))
                        .column("slot.id")
                        .column("s.id")
                        .column(property(EppStudentWorkPlanElement.course().fromAlias("epvSlot")))
                        .column(property(EppStudentWorkPlanElement.term().fromAlias("epvSlot")))
                        .column(property(EppWorkPlanRegistryElementRow.number().fromAlias("wpRow")))
                        .column(property(EppStudentWorkPlanElement.registryElementPart().fromAlias("epvSlot")))
                        .column("mvalue")
                        .column(property(SessionDocumentSlot.studentWpeCAction().id().fromAlias("slot")))
                        ;

                    dql
                        // актуальные МСРП-ФК по году и части построения отчета (т.е. текущему году и выбранной части)
                        .where(isNull(property(SessionDocumentSlot.studentWpeCAction().removalDate().fromAlias("slot"))))
                        .where(eq(property(SessionDocumentSlot.studentWpeCAction().studentWpe().year().educationYear().fromAlias("slot")), value(model.getYear())))
                        .where(eq(property(SessionDocumentSlot.studentWpeCAction().studentWpe().part().fromAlias("slot")), value(model.getPart())))
                        // форма контроля - экзамен
                        .where(eq(property(SessionDocumentSlot.studentWpeCAction().type().code().fromAlias("slot")), value(EppGroupTypeFCACodes.CONTROL_ACTION_EXAM)))
                        // только итоговые
                        .where(eq(property(SessionDocumentSlot.inSession().fromAlias("slot")), value(Boolean.TRUE)))
                        // обязательные параметры отчета - виды дисциплин
                        .where(in(property(EppWorkPlanRegistryElementRow.kind().fromAlias("wpRow")), model.getSessionFilterAddon().getFilterItem(UniSessionFilterAddon.SETTINGS_NAME_DISC_KIND).getValue()))
                    ;

                    model.getSessionFilterAddon().applyFilters(dql, "wpca");

                    dql.where(betweenDays(SessionMark.performDate().fromAlias("m"), model.isPerformDateFromActive() ? model.getPerformDateFrom() : null,
                            model.isPerformDateToActive()? model.getPerformDateTo() : null));


                    for (final Object[] row : dql.createStatement(getSession()).<Object[]>list()) {


                        marks.add(new ISessionReportResultsByDiscMarkData()
                        {
                            @Override public Long getSlotId() { return (Long) row[0]; }
                            @Override public Long getStudentId() { return (Long) row[1]; }
                            @Override public Course getCourse() { return (Course) row[2]; }
                            @Override public Term getTerm() { return (Term) row[3]; }
                            @Override public String getIndex() { return (String) row[4]; }
                            @Override public EppRegistryElementPart getDisc() { return (EppRegistryElementPart) row[5]; }
                            @Override public SessionMarkCatalogItem getMark() { return (SessionMarkCatalogItem) row[6]; }
                        });
                    }
                });
            }
        });

        return marks;       
    }

    private void print(List<ISessionReportResultsColumn> columns, MutableInt rowNum, ISessionReportResultsByDiscRow row, List<String[]> reportTable)
    {
        if (!CollectionUtils.isEmpty(row.getSubRows()))
            for (ISessionReportResultsByDiscRow subRow : row.getSubRows()) {
                print(columns, rowNum, subRow, reportTable);
            }
        List<String> tableRow = new ArrayList<>();
        tableRow.add(String.valueOf(rowNum)); rowNum.increment();
        tableRow.addAll(row.getTableRowStart());
        for (ISessionReportResultsColumn column : columns)
            tableRow.add(column.cell(row));
        reportTable.add(tableRow.toArray(new String[tableRow.size()]));
    }
}
