package ru.tandemservice.unisession.entity.document.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unisession.entity.document.SessionTransferOperation;
import ru.tandemservice.unisession.entity.mark.SessionSlotRegularMark;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Перезачтенное мероприятие
 *
 * Показывает, что указанная оценка получена студентом в результате перезачтения
 * (фактически определяет откуда именно перезачтена оценка)
 * как только связь студента с УП(в) становится неактуальной - closeDate выставляется в removalDate
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionTransferOperationGen extends EntityBase
 implements INaturalIdentifiable<SessionTransferOperationGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.document.SessionTransferOperation";
    public static final String ENTITY_NAME = "sessionTransferOperation";
    public static final int VERSION_HASH = -715341198;
    private static IEntityMeta ENTITY_META;

    public static final String L_TARGET_MARK = "targetMark";
    public static final String P_COMMENT = "comment";

    private SessionSlotRegularMark _targetMark;     // Перезачтенная оценка
    private String _comment;     // Комментарий

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Перезачтенная оценка. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public SessionSlotRegularMark getTargetMark()
    {
        return _targetMark;
    }

    /**
     * @param targetMark Перезачтенная оценка. Свойство не может быть null и должно быть уникальным.
     */
    public void setTargetMark(SessionSlotRegularMark targetMark)
    {
        dirty(_targetMark, targetMark);
        _targetMark = targetMark;
    }

    /**
     * @return Комментарий.
     */
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SessionTransferOperationGen)
        {
            if (withNaturalIdProperties)
            {
                setTargetMark(((SessionTransferOperation)another).getTargetMark());
            }
            setComment(((SessionTransferOperation)another).getComment());
        }
    }

    public INaturalId<SessionTransferOperationGen> getNaturalId()
    {
        return new NaturalId(getTargetMark());
    }

    public static class NaturalId extends NaturalIdBase<SessionTransferOperationGen>
    {
        private static final String PROXY_NAME = "SessionTransferOperationNaturalProxy";

        private Long _targetMark;

        public NaturalId()
        {}

        public NaturalId(SessionSlotRegularMark targetMark)
        {
            _targetMark = ((IEntity) targetMark).getId();
        }

        public Long getTargetMark()
        {
            return _targetMark;
        }

        public void setTargetMark(Long targetMark)
        {
            _targetMark = targetMark;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof SessionTransferOperationGen.NaturalId) ) return false;

            SessionTransferOperationGen.NaturalId that = (NaturalId) o;

            if( !equals(getTargetMark(), that.getTargetMark()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getTargetMark());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getTargetMark());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionTransferOperationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionTransferOperation.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("SessionTransferOperation is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "targetMark":
                    return obj.getTargetMark();
                case "comment":
                    return obj.getComment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "targetMark":
                    obj.setTargetMark((SessionSlotRegularMark) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "targetMark":
                        return true;
                case "comment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "targetMark":
                    return true;
                case "comment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "targetMark":
                    return SessionSlotRegularMark.class;
                case "comment":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionTransferOperation> _dslPath = new Path<SessionTransferOperation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionTransferOperation");
    }
            

    /**
     * @return Перезачтенная оценка. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unisession.entity.document.SessionTransferOperation#getTargetMark()
     */
    public static SessionSlotRegularMark.Path<SessionSlotRegularMark> targetMark()
    {
        return _dslPath.targetMark();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unisession.entity.document.SessionTransferOperation#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    public static class Path<E extends SessionTransferOperation> extends EntityPath<E>
    {
        private SessionSlotRegularMark.Path<SessionSlotRegularMark> _targetMark;
        private PropertyPath<String> _comment;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Перезачтенная оценка. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unisession.entity.document.SessionTransferOperation#getTargetMark()
     */
        public SessionSlotRegularMark.Path<SessionSlotRegularMark> targetMark()
        {
            if(_targetMark == null )
                _targetMark = new SessionSlotRegularMark.Path<SessionSlotRegularMark>(L_TARGET_MARK, this);
            return _targetMark;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unisession.entity.document.SessionTransferOperation#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(SessionTransferOperationGen.P_COMMENT, this);
            return _comment;
        }

        public Class getEntityClass()
        {
            return SessionTransferOperation.class;
        }

        public String getEntityName()
        {
            return "sessionTransferOperation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
