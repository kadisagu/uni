/* $Id$ */
package ru.tandemservice.unisession.dao.transfer;

import org.tandemframework.shared.commonbase.base.util.INumberGenerationRule;
import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.unisession.entity.document.SessionTransferDocument;

/**
 * @author oleyba
 * @since 5/3/11
 */
public interface ISessionTransferDAO
{
    SpringBeanCache<ISessionTransferDAO> instance = new SpringBeanCache<ISessionTransferDAO>(ISessionTransferDAO.class.getName());

    /**
     * правило генерации номера при создании нового документа о перезачтении
     * @return правило
     */
    INumberGenerationRule<SessionTransferDocument> getNumberGenerationRule();
}
