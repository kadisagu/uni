package ru.tandemservice.unisession.base.bo.SessionMark.event;

import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;

import ru.tandemservice.unisession.base.bo.SessionMark.daemon.SessionMarkDaemonBean;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.mark.SessionSlotMarkGradeValue;
import ru.tandemservice.unisession.entity.mark.SessionSlotMarkState;

/**
 * @author vdanilov
 */
public class SessionMarkChangeDSetListener {

    public void init()
    {
        // при изменениях оценки - запускаем демон
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, SessionMark.class, SessionMarkDaemonBean.DAEMON.getAfterCompleteWakeUpListener());
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, SessionMark.class, SessionMarkDaemonBean.DAEMON.getAfterCompleteWakeUpListener());
        DSetEventManager.getInstance().registerListener(DSetEventType.afterDelete, SessionMark.class, SessionMarkDaemonBean.DAEMON.getAfterCompleteWakeUpListener());

        // изменение оценок
        {
            SessionRegularMarkFiledAndCommissionChecker gradeValueMarkChecker = new SessionRegularMarkFiledAndCommissionChecker<SessionSlotMarkGradeValue>(
            SessionSlotMarkGradeValue.class,
            SessionSlotMarkGradeValue.L_SLOT,
            SessionSlotMarkGradeValue.P_COMMENT,
            SessionSlotMarkGradeValue.L_COMMISSION,
            SessionSlotMarkGradeValue.P_MODIFICATION_DATE,
            SessionSlotMarkGradeValue.P_PERFORM_DATE,
            SessionSlotMarkGradeValue.L_VALUE,
            SessionSlotMarkGradeValue.P_POINTS_AS_LONG
            );
            DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, gradeValueMarkChecker.getKlass(), gradeValueMarkChecker);
            DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, gradeValueMarkChecker.getKlass(), gradeValueMarkChecker);
        }

        // изменение отметок
        {
            SessionRegularMarkFiledAndCommissionChecker stateMarkChecker = new SessionRegularMarkFiledAndCommissionChecker<SessionSlotMarkState>(
            SessionSlotMarkState.class,
            SessionSlotMarkState.L_SLOT,
            SessionSlotMarkState.P_COMMENT,
            SessionSlotMarkState.L_COMMISSION,
            SessionSlotMarkState.P_MODIFICATION_DATE,
            SessionSlotMarkState.P_PERFORM_DATE,
            SessionSlotMarkState.L_VALUE
            );
            DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, stateMarkChecker.getKlass(), stateMarkChecker);
            DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, stateMarkChecker.getKlass(), stateMarkChecker);
        }
    }

}
