package ru.tandemservice.unisession.entity.stateFinalExam.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unisession.entity.catalog.SessionRecommendation4FQW;
import ru.tandemservice.unisession.entity.catalog.SessionSource4FQWTheme;
import ru.tandemservice.unisession.entity.stateFinalExam.SessionFQWProtocol;
import ru.tandemservice.unisession.entity.stateFinalExam.SessionStateFinalExamProtocol;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Протокол на ВКР
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionFQWProtocolGen extends SessionStateFinalExamProtocol
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.stateFinalExam.SessionFQWProtocol";
    public static final String ENTITY_NAME = "sessionFQWProtocol";
    public static final int VERSION_HASH = -1646582626;
    private static IEntityMeta ENTITY_META;

    public static final String P_WITH_HONORS = "withHonors";
    public static final String L_CONSULTANT = "consultant";
    public static final String L_SOURCE4_THEME = "source4Theme";
    public static final String L_RECOMMENDATION = "recommendation";

    private boolean _withHonors;     // С отличием
    private PpsEntry _consultant;     // Консультант
    private SessionSource4FQWTheme _source4Theme;     // Источник темы ВКР
    private SessionRecommendation4FQW _recommendation;     // Рекомендация по ВКР

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return С отличием. Свойство не может быть null.
     */
    @NotNull
    public boolean isWithHonors()
    {
        return _withHonors;
    }

    /**
     * @param withHonors С отличием. Свойство не может быть null.
     */
    public void setWithHonors(boolean withHonors)
    {
        dirty(_withHonors, withHonors);
        _withHonors = withHonors;
    }

    /**
     * @return Консультант.
     */
    public PpsEntry getConsultant()
    {
        return _consultant;
    }

    /**
     * @param consultant Консультант.
     */
    public void setConsultant(PpsEntry consultant)
    {
        dirty(_consultant, consultant);
        _consultant = consultant;
    }

    /**
     * @return Источник темы ВКР.
     */
    public SessionSource4FQWTheme getSource4Theme()
    {
        return _source4Theme;
    }

    /**
     * @param source4Theme Источник темы ВКР.
     */
    public void setSource4Theme(SessionSource4FQWTheme source4Theme)
    {
        dirty(_source4Theme, source4Theme);
        _source4Theme = source4Theme;
    }

    /**
     * @return Рекомендация по ВКР.
     */
    public SessionRecommendation4FQW getRecommendation()
    {
        return _recommendation;
    }

    /**
     * @param recommendation Рекомендация по ВКР.
     */
    public void setRecommendation(SessionRecommendation4FQW recommendation)
    {
        dirty(_recommendation, recommendation);
        _recommendation = recommendation;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof SessionFQWProtocolGen)
        {
            setWithHonors(((SessionFQWProtocol)another).isWithHonors());
            setConsultant(((SessionFQWProtocol)another).getConsultant());
            setSource4Theme(((SessionFQWProtocol)another).getSource4Theme());
            setRecommendation(((SessionFQWProtocol)another).getRecommendation());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionFQWProtocolGen> extends SessionStateFinalExamProtocol.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionFQWProtocol.class;
        }

        public T newInstance()
        {
            return (T) new SessionFQWProtocol();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "withHonors":
                    return obj.isWithHonors();
                case "consultant":
                    return obj.getConsultant();
                case "source4Theme":
                    return obj.getSource4Theme();
                case "recommendation":
                    return obj.getRecommendation();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "withHonors":
                    obj.setWithHonors((Boolean) value);
                    return;
                case "consultant":
                    obj.setConsultant((PpsEntry) value);
                    return;
                case "source4Theme":
                    obj.setSource4Theme((SessionSource4FQWTheme) value);
                    return;
                case "recommendation":
                    obj.setRecommendation((SessionRecommendation4FQW) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "withHonors":
                        return true;
                case "consultant":
                        return true;
                case "source4Theme":
                        return true;
                case "recommendation":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "withHonors":
                    return true;
                case "consultant":
                    return true;
                case "source4Theme":
                    return true;
                case "recommendation":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "withHonors":
                    return Boolean.class;
                case "consultant":
                    return PpsEntry.class;
                case "source4Theme":
                    return SessionSource4FQWTheme.class;
                case "recommendation":
                    return SessionRecommendation4FQW.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionFQWProtocol> _dslPath = new Path<SessionFQWProtocol>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionFQWProtocol");
    }
            

    /**
     * @return С отличием. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.stateFinalExam.SessionFQWProtocol#isWithHonors()
     */
    public static PropertyPath<Boolean> withHonors()
    {
        return _dslPath.withHonors();
    }

    /**
     * @return Консультант.
     * @see ru.tandemservice.unisession.entity.stateFinalExam.SessionFQWProtocol#getConsultant()
     */
    public static PpsEntry.Path<PpsEntry> consultant()
    {
        return _dslPath.consultant();
    }

    /**
     * @return Источник темы ВКР.
     * @see ru.tandemservice.unisession.entity.stateFinalExam.SessionFQWProtocol#getSource4Theme()
     */
    public static SessionSource4FQWTheme.Path<SessionSource4FQWTheme> source4Theme()
    {
        return _dslPath.source4Theme();
    }

    /**
     * @return Рекомендация по ВКР.
     * @see ru.tandemservice.unisession.entity.stateFinalExam.SessionFQWProtocol#getRecommendation()
     */
    public static SessionRecommendation4FQW.Path<SessionRecommendation4FQW> recommendation()
    {
        return _dslPath.recommendation();
    }

    public static class Path<E extends SessionFQWProtocol> extends SessionStateFinalExamProtocol.Path<E>
    {
        private PropertyPath<Boolean> _withHonors;
        private PpsEntry.Path<PpsEntry> _consultant;
        private SessionSource4FQWTheme.Path<SessionSource4FQWTheme> _source4Theme;
        private SessionRecommendation4FQW.Path<SessionRecommendation4FQW> _recommendation;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return С отличием. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.stateFinalExam.SessionFQWProtocol#isWithHonors()
     */
        public PropertyPath<Boolean> withHonors()
        {
            if(_withHonors == null )
                _withHonors = new PropertyPath<Boolean>(SessionFQWProtocolGen.P_WITH_HONORS, this);
            return _withHonors;
        }

    /**
     * @return Консультант.
     * @see ru.tandemservice.unisession.entity.stateFinalExam.SessionFQWProtocol#getConsultant()
     */
        public PpsEntry.Path<PpsEntry> consultant()
        {
            if(_consultant == null )
                _consultant = new PpsEntry.Path<PpsEntry>(L_CONSULTANT, this);
            return _consultant;
        }

    /**
     * @return Источник темы ВКР.
     * @see ru.tandemservice.unisession.entity.stateFinalExam.SessionFQWProtocol#getSource4Theme()
     */
        public SessionSource4FQWTheme.Path<SessionSource4FQWTheme> source4Theme()
        {
            if(_source4Theme == null )
                _source4Theme = new SessionSource4FQWTheme.Path<SessionSource4FQWTheme>(L_SOURCE4_THEME, this);
            return _source4Theme;
        }

    /**
     * @return Рекомендация по ВКР.
     * @see ru.tandemservice.unisession.entity.stateFinalExam.SessionFQWProtocol#getRecommendation()
     */
        public SessionRecommendation4FQW.Path<SessionRecommendation4FQW> recommendation()
        {
            if(_recommendation == null )
                _recommendation = new SessionRecommendation4FQW.Path<SessionRecommendation4FQW>(L_RECOMMENDATION, this);
            return _recommendation;
        }

        public Class getEntityClass()
        {
            return SessionFQWProtocol.class;
        }

        public String getEntityName()
        {
            return "sessionFQWProtocol";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
