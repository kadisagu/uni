package ru.tandemservice.unisession.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.unisession.entity.catalog.UnisessionBulletinTemplate;
import ru.tandemservice.unisession.entity.catalog.UnisessionTemplate;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Печатные шаблоны ведомостей модуля «Сессия»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UnisessionBulletinTemplateGen extends UnisessionTemplate
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.catalog.UnisessionBulletinTemplate";
    public static final String ENTITY_NAME = "unisessionBulletinTemplate";
    public static final int VERSION_HASH = 226507576;
    private static IEntityMeta ENTITY_META;

    public static final String L_TYPE = "type";
    public static final String P_PRINT_POINTS = "printPoints";
    public static final String P_PRINT_CURRENT_RATING = "printCurrentRating";

    private EppFControlActionType _type;     // Форма итогового контроля
    private boolean _printPoints = false;     // Выводить рейтинговый эквивалент оценки
    private boolean _printCurrentRating = false;     // Выводить тек. рейтинг

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Форма итогового контроля.
     */
    public EppFControlActionType getType()
    {
        return _type;
    }

    /**
     * @param type Форма итогового контроля.
     */
    public void setType(EppFControlActionType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Выводить рейтинговый эквивалент оценки. Свойство не может быть null.
     */
    @NotNull
    public boolean isPrintPoints()
    {
        return _printPoints;
    }

    /**
     * @param printPoints Выводить рейтинговый эквивалент оценки. Свойство не может быть null.
     */
    public void setPrintPoints(boolean printPoints)
    {
        dirty(_printPoints, printPoints);
        _printPoints = printPoints;
    }

    /**
     * @return Выводить тек. рейтинг. Свойство не может быть null.
     */
    @NotNull
    public boolean isPrintCurrentRating()
    {
        return _printCurrentRating;
    }

    /**
     * @param printCurrentRating Выводить тек. рейтинг. Свойство не может быть null.
     */
    public void setPrintCurrentRating(boolean printCurrentRating)
    {
        dirty(_printCurrentRating, printCurrentRating);
        _printCurrentRating = printCurrentRating;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof UnisessionBulletinTemplateGen)
        {
            setType(((UnisessionBulletinTemplate)another).getType());
            setPrintPoints(((UnisessionBulletinTemplate)another).isPrintPoints());
            setPrintCurrentRating(((UnisessionBulletinTemplate)another).isPrintCurrentRating());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UnisessionBulletinTemplateGen> extends UnisessionTemplate.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UnisessionBulletinTemplate.class;
        }

        public T newInstance()
        {
            return (T) new UnisessionBulletinTemplate();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "type":
                    return obj.getType();
                case "printPoints":
                    return obj.isPrintPoints();
                case "printCurrentRating":
                    return obj.isPrintCurrentRating();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "type":
                    obj.setType((EppFControlActionType) value);
                    return;
                case "printPoints":
                    obj.setPrintPoints((Boolean) value);
                    return;
                case "printCurrentRating":
                    obj.setPrintCurrentRating((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "type":
                        return true;
                case "printPoints":
                        return true;
                case "printCurrentRating":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "type":
                    return true;
                case "printPoints":
                    return true;
                case "printCurrentRating":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "type":
                    return EppFControlActionType.class;
                case "printPoints":
                    return Boolean.class;
                case "printCurrentRating":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UnisessionBulletinTemplate> _dslPath = new Path<UnisessionBulletinTemplate>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UnisessionBulletinTemplate");
    }
            

    /**
     * @return Форма итогового контроля.
     * @see ru.tandemservice.unisession.entity.catalog.UnisessionBulletinTemplate#getType()
     */
    public static EppFControlActionType.Path<EppFControlActionType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Выводить рейтинговый эквивалент оценки. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.catalog.UnisessionBulletinTemplate#isPrintPoints()
     */
    public static PropertyPath<Boolean> printPoints()
    {
        return _dslPath.printPoints();
    }

    /**
     * @return Выводить тек. рейтинг. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.catalog.UnisessionBulletinTemplate#isPrintCurrentRating()
     */
    public static PropertyPath<Boolean> printCurrentRating()
    {
        return _dslPath.printCurrentRating();
    }

    public static class Path<E extends UnisessionBulletinTemplate> extends UnisessionTemplate.Path<E>
    {
        private EppFControlActionType.Path<EppFControlActionType> _type;
        private PropertyPath<Boolean> _printPoints;
        private PropertyPath<Boolean> _printCurrentRating;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Форма итогового контроля.
     * @see ru.tandemservice.unisession.entity.catalog.UnisessionBulletinTemplate#getType()
     */
        public EppFControlActionType.Path<EppFControlActionType> type()
        {
            if(_type == null )
                _type = new EppFControlActionType.Path<EppFControlActionType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Выводить рейтинговый эквивалент оценки. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.catalog.UnisessionBulletinTemplate#isPrintPoints()
     */
        public PropertyPath<Boolean> printPoints()
        {
            if(_printPoints == null )
                _printPoints = new PropertyPath<Boolean>(UnisessionBulletinTemplateGen.P_PRINT_POINTS, this);
            return _printPoints;
        }

    /**
     * @return Выводить тек. рейтинг. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.catalog.UnisessionBulletinTemplate#isPrintCurrentRating()
     */
        public PropertyPath<Boolean> printCurrentRating()
        {
            if(_printCurrentRating == null )
                _printCurrentRating = new PropertyPath<Boolean>(UnisessionBulletinTemplateGen.P_PRINT_CURRENT_RATING, this);
            return _printCurrentRating;
        }

        public Class getEntityClass()
        {
            return UnisessionBulletinTemplate.class;
        }

        public String getEntityName()
        {
            return "unisessionBulletinTemplate";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
