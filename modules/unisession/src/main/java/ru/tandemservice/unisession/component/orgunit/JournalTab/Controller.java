/* $Id: Controller.java 11311 2010-02-03 11:27:19Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unisession.component.orgunit.JournalTab;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.NoWrapFormatter;
import org.tandemframework.core.view.list.column.FormatterColumn;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import ru.tandemservice.uni.entity.orgstruct.Group;

/**
 * @author vip_delete
 */
public class Controller extends ru.tandemservice.uni.component.group.AbstractGroupList.Controller
{
    @Override
    protected String getSettingsKey() {
        return "SessionJournalGroupList.filter";
    }

    @Override
    protected boolean isListAccessable(final IBusinessComponent component) {
        return false;
    }

    @Override
    protected FormatterColumn buildGroupTitleClickableColumn() {
        final IPublisherLinkResolver resolver = new IPublisherLinkResolver() {
            @Override public String getComponentName(final IEntity entity) {
                return null;
            }
            @Override public Object getParameters(final IEntity entity) {
                return new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, entity.getId()).add("selectedTabId", "sessionGroupJournalTab");
            }
        };

        return new PublisherLinkColumn(this.getMessage("group.title"), Group.P_TITLE).setResolver(resolver ).setFormatter(NoWrapFormatter.INSTANCE);
    }
}
