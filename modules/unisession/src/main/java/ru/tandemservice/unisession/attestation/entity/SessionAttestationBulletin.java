package ru.tandemservice.unisession.attestation.entity;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.sec.ISecLocalEntityOwner;
import org.tandemframework.shared.commonbase.base.util.INumberGenerationRule;
import org.tandemframework.shared.commonbase.base.util.INumberObject;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unisession.attestation.bo.AttestationBulletin.AttestationBulletinManager;
import ru.tandemservice.unisession.attestation.entity.gen.SessionAttestationBulletinGen;
import ru.tandemservice.unisession.sec.ISessionSecDao;

import java.util.Collection;

/**
 * Аттестационная ведомость
 *
 * Аттестационная ведомость - документ для проведения межсессионной аттестации.
 */
public class SessionAttestationBulletin extends SessionAttestationBulletinGen implements ITitled, INumberObject, ISecLocalEntityOwner
{
    public SessionAttestationBulletin() {}
    public SessionAttestationBulletin(SessionAttestation attestation, EppRegistryElementPart registryElementPart) {
        setAttestation(attestation);
        setRegistryElementPart(registryElementPart);
    }

    @Override
    public INumberGenerationRule getNumberGenerationRule() {
        return AttestationBulletinManager.instance().dao().getNumberGenerationRule();
    }

    @Override
    @EntityDSLSupport(parts = "number")
    public String getTitle()
    {
        if (getRegistryElementPart() == null) {
            return this.getClass().getSimpleName();
        }
        return "№" +this.getNumber() + " " + this.getRegistryElementPart().getTitleWithNumber();
    }

    public String getTypeTitle()
    {
        return "Атт. ведомость №" + this.getNumber();
    }

    public OrgUnit getOrgUnit()
    {
        return getAttestation().getOrgUnit();
    }

    @Override
    @EntityDSLSupport(parts = "closeDate")
    public boolean isClosed()
    {
        return getCloseDate() != null || getAttestation().isClosed();
    }

    @Override
    public Collection<IEntity> getSecLocalEntities()
    {
        return ISessionSecDao.instance.get().getSecLocalEntities(this);
    }
}