package ru.tandemservice.unisession.attestation.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unisession.attestation.entity.SessionAttestation;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin;
import ru.tandemservice.unisession.entity.comission.SessionComission;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Аттестационная ведомость
 *
 * Аттестационная ведомость - документ для проведения межсессионной аттестации, т.е. для выставления студентам отметок о прохождении\непрохождении аттестации.
 * Существует в рамках конкретной межсессионной аттестации.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionAttestationBulletinGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin";
    public static final String ENTITY_NAME = "sessionAttestationBulletin";
    public static final int VERSION_HASH = -1743044584;
    private static IEntityMeta ENTITY_META;

    public static final String P_NUMBER = "number";
    public static final String P_FORMING_DATE = "formingDate";
    public static final String P_CLOSE_DATE = "closeDate";
    public static final String L_ATTESTATION = "attestation";
    public static final String P_PERFORM_DATE = "performDate";
    public static final String L_COMMISSION = "commission";
    public static final String L_REGISTRY_ELEMENT_PART = "registryElementPart";
    public static final String P_CLOSED = "closed";
    public static final String P_TITLE = "title";

    private String _number;     // Номер документа
    private Date _formingDate;     // Дата формирования (фактическая)
    private Date _closeDate;     // Дата закрытия (фактическая)
    private SessionAttestation _attestation;     // Аттестация
    private Date _performDate;     // Дата проведения (фактическая)
    private SessionComission _commission;     // Комиссия
    private EppRegistryElementPart _registryElementPart;     // Мероприятие (элемент реестра), часть

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Номер документа. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер документа. Свойство не может быть null.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Дата формирования (фактическая). Свойство не может быть null.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата формирования (фактическая). Свойство не может быть null.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    /**
     * @return Дата закрытия (фактическая).
     */
    public Date getCloseDate()
    {
        return _closeDate;
    }

    /**
     * @param closeDate Дата закрытия (фактическая).
     */
    public void setCloseDate(Date closeDate)
    {
        dirty(_closeDate, closeDate);
        _closeDate = closeDate;
    }

    /**
     * @return Аттестация. Свойство не может быть null.
     */
    @NotNull
    public SessionAttestation getAttestation()
    {
        return _attestation;
    }

    /**
     * @param attestation Аттестация. Свойство не может быть null.
     */
    public void setAttestation(SessionAttestation attestation)
    {
        dirty(_attestation, attestation);
        _attestation = attestation;
    }

    /**
     * @return Дата проведения (фактическая).
     */
    public Date getPerformDate()
    {
        return _performDate;
    }

    /**
     * @param performDate Дата проведения (фактическая).
     */
    public void setPerformDate(Date performDate)
    {
        dirty(_performDate, performDate);
        _performDate = performDate;
    }

    /**
     * @return Комиссия. Свойство не может быть null.
     */
    @NotNull
    public SessionComission getCommission()
    {
        return _commission;
    }

    /**
     * @param commission Комиссия. Свойство не может быть null.
     */
    public void setCommission(SessionComission commission)
    {
        dirty(_commission, commission);
        _commission = commission;
    }

    /**
     * @return Мероприятие (элемент реестра), часть. Свойство не может быть null.
     */
    @NotNull
    public EppRegistryElementPart getRegistryElementPart()
    {
        return _registryElementPart;
    }

    /**
     * @param registryElementPart Мероприятие (элемент реестра), часть. Свойство не может быть null.
     */
    public void setRegistryElementPart(EppRegistryElementPart registryElementPart)
    {
        dirty(_registryElementPart, registryElementPart);
        _registryElementPart = registryElementPart;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SessionAttestationBulletinGen)
        {
            setNumber(((SessionAttestationBulletin)another).getNumber());
            setFormingDate(((SessionAttestationBulletin)another).getFormingDate());
            setCloseDate(((SessionAttestationBulletin)another).getCloseDate());
            setAttestation(((SessionAttestationBulletin)another).getAttestation());
            setPerformDate(((SessionAttestationBulletin)another).getPerformDate());
            setCommission(((SessionAttestationBulletin)another).getCommission());
            setRegistryElementPart(((SessionAttestationBulletin)another).getRegistryElementPart());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionAttestationBulletinGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionAttestationBulletin.class;
        }

        public T newInstance()
        {
            return (T) new SessionAttestationBulletin();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "number":
                    return obj.getNumber();
                case "formingDate":
                    return obj.getFormingDate();
                case "closeDate":
                    return obj.getCloseDate();
                case "attestation":
                    return obj.getAttestation();
                case "performDate":
                    return obj.getPerformDate();
                case "commission":
                    return obj.getCommission();
                case "registryElementPart":
                    return obj.getRegistryElementPart();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
                case "closeDate":
                    obj.setCloseDate((Date) value);
                    return;
                case "attestation":
                    obj.setAttestation((SessionAttestation) value);
                    return;
                case "performDate":
                    obj.setPerformDate((Date) value);
                    return;
                case "commission":
                    obj.setCommission((SessionComission) value);
                    return;
                case "registryElementPart":
                    obj.setRegistryElementPart((EppRegistryElementPart) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "number":
                        return true;
                case "formingDate":
                        return true;
                case "closeDate":
                        return true;
                case "attestation":
                        return true;
                case "performDate":
                        return true;
                case "commission":
                        return true;
                case "registryElementPart":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "number":
                    return true;
                case "formingDate":
                    return true;
                case "closeDate":
                    return true;
                case "attestation":
                    return true;
                case "performDate":
                    return true;
                case "commission":
                    return true;
                case "registryElementPart":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "number":
                    return String.class;
                case "formingDate":
                    return Date.class;
                case "closeDate":
                    return Date.class;
                case "attestation":
                    return SessionAttestation.class;
                case "performDate":
                    return Date.class;
                case "commission":
                    return SessionComission.class;
                case "registryElementPart":
                    return EppRegistryElementPart.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionAttestationBulletin> _dslPath = new Path<SessionAttestationBulletin>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionAttestationBulletin");
    }
            

    /**
     * @return Номер документа. Свойство не может быть null.
     * @see ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Дата формирования (фактическая). Свойство не может быть null.
     * @see ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * @return Дата закрытия (фактическая).
     * @see ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin#getCloseDate()
     */
    public static PropertyPath<Date> closeDate()
    {
        return _dslPath.closeDate();
    }

    /**
     * @return Аттестация. Свойство не может быть null.
     * @see ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin#getAttestation()
     */
    public static SessionAttestation.Path<SessionAttestation> attestation()
    {
        return _dslPath.attestation();
    }

    /**
     * @return Дата проведения (фактическая).
     * @see ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin#getPerformDate()
     */
    public static PropertyPath<Date> performDate()
    {
        return _dslPath.performDate();
    }

    /**
     * @return Комиссия. Свойство не может быть null.
     * @see ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin#getCommission()
     */
    public static SessionComission.Path<SessionComission> commission()
    {
        return _dslPath.commission();
    }

    /**
     * @return Мероприятие (элемент реестра), часть. Свойство не может быть null.
     * @see ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin#getRegistryElementPart()
     */
    public static EppRegistryElementPart.Path<EppRegistryElementPart> registryElementPart()
    {
        return _dslPath.registryElementPart();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin#isClosed()
     */
    public static SupportedPropertyPath<Boolean> closed()
    {
        return _dslPath.closed();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends SessionAttestationBulletin> extends EntityPath<E>
    {
        private PropertyPath<String> _number;
        private PropertyPath<Date> _formingDate;
        private PropertyPath<Date> _closeDate;
        private SessionAttestation.Path<SessionAttestation> _attestation;
        private PropertyPath<Date> _performDate;
        private SessionComission.Path<SessionComission> _commission;
        private EppRegistryElementPart.Path<EppRegistryElementPart> _registryElementPart;
        private SupportedPropertyPath<Boolean> _closed;
        private SupportedPropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Номер документа. Свойство не может быть null.
     * @see ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(SessionAttestationBulletinGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Дата формирования (фактическая). Свойство не может быть null.
     * @see ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(SessionAttestationBulletinGen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * @return Дата закрытия (фактическая).
     * @see ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin#getCloseDate()
     */
        public PropertyPath<Date> closeDate()
        {
            if(_closeDate == null )
                _closeDate = new PropertyPath<Date>(SessionAttestationBulletinGen.P_CLOSE_DATE, this);
            return _closeDate;
        }

    /**
     * @return Аттестация. Свойство не может быть null.
     * @see ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin#getAttestation()
     */
        public SessionAttestation.Path<SessionAttestation> attestation()
        {
            if(_attestation == null )
                _attestation = new SessionAttestation.Path<SessionAttestation>(L_ATTESTATION, this);
            return _attestation;
        }

    /**
     * @return Дата проведения (фактическая).
     * @see ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin#getPerformDate()
     */
        public PropertyPath<Date> performDate()
        {
            if(_performDate == null )
                _performDate = new PropertyPath<Date>(SessionAttestationBulletinGen.P_PERFORM_DATE, this);
            return _performDate;
        }

    /**
     * @return Комиссия. Свойство не может быть null.
     * @see ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin#getCommission()
     */
        public SessionComission.Path<SessionComission> commission()
        {
            if(_commission == null )
                _commission = new SessionComission.Path<SessionComission>(L_COMMISSION, this);
            return _commission;
        }

    /**
     * @return Мероприятие (элемент реестра), часть. Свойство не может быть null.
     * @see ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin#getRegistryElementPart()
     */
        public EppRegistryElementPart.Path<EppRegistryElementPart> registryElementPart()
        {
            if(_registryElementPart == null )
                _registryElementPart = new EppRegistryElementPart.Path<EppRegistryElementPart>(L_REGISTRY_ELEMENT_PART, this);
            return _registryElementPart;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin#isClosed()
     */
        public SupportedPropertyPath<Boolean> closed()
        {
            if(_closed == null )
                _closed = new SupportedPropertyPath<Boolean>(SessionAttestationBulletinGen.P_CLOSED, this);
            return _closed;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(SessionAttestationBulletinGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return SessionAttestationBulletin.class;
        }

        public String getEntityName()
        {
            return "sessionAttestationBulletin";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract boolean isClosed();

    public abstract String getTitle();
}
