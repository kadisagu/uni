package ru.tandemservice.unisession.entity.document.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unisession.entity.document.SessionDocument;
import ru.tandemservice.unisession.entity.document.SessionTransferDocument;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Перезачтение
 *
 * Документ, показывающий перезачтение группы оценок (из старых рп или иного образовательного учреждения)
 * в требуемые оценки студента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionTransferDocumentGen extends SessionDocument
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.document.SessionTransferDocument";
    public static final String ENTITY_NAME = "sessionTransferDocument";
    public static final int VERSION_HASH = -1197796176;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDUCATION_YEAR = "educationYear";
    public static final String L_ORG_UNIT = "orgUnit";
    public static final String L_TARGET_STUDENT = "targetStudent";
    public static final String P_EDU_INSTITUTION_TITLE = "eduInstitutionTitle";
    public static final String P_COMMENT = "comment";
    public static final String P_TITLE = "title";

    private EducationYear _educationYear;     // Учебный год перезачтения
    private OrgUnit _orgUnit;     // Подразделение
    private Student _targetStudent;     // Студент
    private String _eduInstitutionTitle;     // Учреждение
    private String _comment;     // Комментарий

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Учебный год перезачтения. Свойство не может быть null.
     */
    @NotNull
    public EducationYear getEducationYear()
    {
        return _educationYear;
    }

    /**
     * @param educationYear Учебный год перезачтения. Свойство не может быть null.
     */
    public void setEducationYear(EducationYear educationYear)
    {
        dirty(_educationYear, educationYear);
        _educationYear = educationYear;
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение. Свойство не может быть null.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * Студент, которому перезачитываются оценки
     *
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getTargetStudent()
    {
        return _targetStudent;
    }

    /**
     * @param targetStudent Студент. Свойство не может быть null.
     */
    public void setTargetStudent(Student targetStudent)
    {
        dirty(_targetStudent, targetStudent);
        _targetStudent = targetStudent;
    }

    /**
     * @return Учреждение.
     */
    @Length(max=255)
    public String getEduInstitutionTitle()
    {
        return _eduInstitutionTitle;
    }

    /**
     * @param eduInstitutionTitle Учреждение.
     */
    public void setEduInstitutionTitle(String eduInstitutionTitle)
    {
        dirty(_eduInstitutionTitle, eduInstitutionTitle);
        _eduInstitutionTitle = eduInstitutionTitle;
    }

    /**
     * @return Комментарий.
     */
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof SessionTransferDocumentGen)
        {
            setEducationYear(((SessionTransferDocument)another).getEducationYear());
            setOrgUnit(((SessionTransferDocument)another).getOrgUnit());
            setTargetStudent(((SessionTransferDocument)another).getTargetStudent());
            setEduInstitutionTitle(((SessionTransferDocument)another).getEduInstitutionTitle());
            setComment(((SessionTransferDocument)another).getComment());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionTransferDocumentGen> extends SessionDocument.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionTransferDocument.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("SessionTransferDocument is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "educationYear":
                    return obj.getEducationYear();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "targetStudent":
                    return obj.getTargetStudent();
                case "eduInstitutionTitle":
                    return obj.getEduInstitutionTitle();
                case "comment":
                    return obj.getComment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "educationYear":
                    obj.setEducationYear((EducationYear) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "targetStudent":
                    obj.setTargetStudent((Student) value);
                    return;
                case "eduInstitutionTitle":
                    obj.setEduInstitutionTitle((String) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "educationYear":
                        return true;
                case "orgUnit":
                        return true;
                case "targetStudent":
                        return true;
                case "eduInstitutionTitle":
                        return true;
                case "comment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "educationYear":
                    return true;
                case "orgUnit":
                    return true;
                case "targetStudent":
                    return true;
                case "eduInstitutionTitle":
                    return true;
                case "comment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "educationYear":
                    return EducationYear.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "targetStudent":
                    return Student.class;
                case "eduInstitutionTitle":
                    return String.class;
                case "comment":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionTransferDocument> _dslPath = new Path<SessionTransferDocument>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionTransferDocument");
    }
            

    /**
     * @return Учебный год перезачтения. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionTransferDocument#getEducationYear()
     */
    public static EducationYear.Path<EducationYear> educationYear()
    {
        return _dslPath.educationYear();
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionTransferDocument#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * Студент, которому перезачитываются оценки
     *
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionTransferDocument#getTargetStudent()
     */
    public static Student.Path<Student> targetStudent()
    {
        return _dslPath.targetStudent();
    }

    /**
     * @return Учреждение.
     * @see ru.tandemservice.unisession.entity.document.SessionTransferDocument#getEduInstitutionTitle()
     */
    public static PropertyPath<String> eduInstitutionTitle()
    {
        return _dslPath.eduInstitutionTitle();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unisession.entity.document.SessionTransferDocument#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.entity.document.SessionTransferDocument#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends SessionTransferDocument> extends SessionDocument.Path<E>
    {
        private EducationYear.Path<EducationYear> _educationYear;
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private Student.Path<Student> _targetStudent;
        private PropertyPath<String> _eduInstitutionTitle;
        private PropertyPath<String> _comment;
        private SupportedPropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Учебный год перезачтения. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionTransferDocument#getEducationYear()
     */
        public EducationYear.Path<EducationYear> educationYear()
        {
            if(_educationYear == null )
                _educationYear = new EducationYear.Path<EducationYear>(L_EDUCATION_YEAR, this);
            return _educationYear;
        }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionTransferDocument#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * Студент, которому перезачитываются оценки
     *
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionTransferDocument#getTargetStudent()
     */
        public Student.Path<Student> targetStudent()
        {
            if(_targetStudent == null )
                _targetStudent = new Student.Path<Student>(L_TARGET_STUDENT, this);
            return _targetStudent;
        }

    /**
     * @return Учреждение.
     * @see ru.tandemservice.unisession.entity.document.SessionTransferDocument#getEduInstitutionTitle()
     */
        public PropertyPath<String> eduInstitutionTitle()
        {
            if(_eduInstitutionTitle == null )
                _eduInstitutionTitle = new PropertyPath<String>(SessionTransferDocumentGen.P_EDU_INSTITUTION_TITLE, this);
            return _eduInstitutionTitle;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unisession.entity.document.SessionTransferDocument#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(SessionTransferDocumentGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.entity.document.SessionTransferDocument#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(SessionTransferDocumentGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return SessionTransferDocument.class;
        }

        public String getEntityName()
        {
            return "sessionTransferDocument";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getTitle();
}
