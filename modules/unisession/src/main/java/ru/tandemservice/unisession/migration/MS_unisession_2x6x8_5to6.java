package ru.tandemservice.unisession.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unisession_2x6x8_5to6 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.8")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность sessionListDocument

		// создано обязательное свойство student
		{
			// создать колонку
			tool.createColumn("session_doc_list_t", new DBColumn("student_id", DBType.LONG));

            final Set<Long> updated = new HashSet<>();
            PreparedStatement update = tool.prepareStatement("update session_doc_list_t set student_id=? where id=?");
            Statement stmt = tool.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery("select slot.document_id, wpe.student_id " +
                " from session_doc_slot_t slot " +
                " inner join epp_student_wpe_caction_t wpeca on wpeca.id = slot.studentWpeCAction_id " +
                " inner join epp_student_wpe_t wpe on wpeca.studentWpe_id = wpe.id");

            while(rs.next()) {
                Long documentId = rs.getLong(1);
                Long studentId = rs.getLong(2);

                if (updated.contains(documentId)) continue;
                updated.add(documentId);

                update.setLong(1, studentId);
                update.setLong(2, documentId);
                update.executeUpdate();
            }

			// сделать колонку NOT NULL
			tool.setColumnNullable("session_doc_list_t", "student_id", false);
		}
    }
}