/* $Id:$ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.GroupBulletinListAdd;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppWorkPlanRowKind;
import ru.tandemservice.unisession.base.bo.SessionReport.util.ISessionReportGroupFilterParams;
import ru.tandemservice.unisession.entity.document.SessionObject;
import ru.tandemservice.unisession.entity.report.UnisessionGroupBulletinListReport;

import java.util.Collection;

/**
 * @author oleyba
 * @since 2/24/12
 */
public interface ISessionGroupBulletinListPrintDAO extends INeedPersistenceSupport
{
    SpringBeanCache<ISessionGroupBulletinListPrintDAO> instance = new SpringBeanCache<ISessionGroupBulletinListPrintDAO>(ISessionGroupBulletinListPrintDAO.class.getName());

    UnisessionGroupBulletinListReport createStoredReport(SessionReportGroupBulletinListAddUI model);
}
