package ru.tandemservice.unisession.component.orgunit.SessionSimpleDocListTab;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.tapsupport.component.selection.CommonMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unisession.entity.document.SessionDocument;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionGlobalDocument;
import ru.tandemservice.unisession.entity.document.SessionStudentGradeBookDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.Collection;
import java.util.Date;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class DAO extends UniBaseDao implements IDAO
{
    private final DQLOrderDescriptionRegistry order = new DQLOrderDescriptionRegistry(SessionDocument.class, "doc");

    @Override
    public void prepare(final Model model)
    {
        model.getOrgUnitHolder().refresh();

        model.setYearModel(EducationCatalogsManager.getEducationYearModel());
        model.setPartsModel(EducationCatalogsManager.getYearDistributionPartModel());

        model.setCourseCurrentModel(new CommonMultiSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Course.class, "b").column(property("b"))
                        .where(likeUpper(property(Course.title().fromAlias("b")), value(CoreStringUtils.escapeLike(filter, true))))
                        .order(property(Course.title().fromAlias("b")));

                if (set != null)
                    builder.where(in(property(Course.id().fromAlias("b")), set));

                return new DQLListResultBuilder(builder, 50);
            }
        });

        model.setGroupCurrentModel(new CommonMultiSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                final DQLSelectBuilder rootDql = order.buildDQLSelectBuilder();
                rootDql.column(property(SessionDocument.id().fromAlias("doc")));
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SessionDocumentSlot.class, "a").column(property(SessionDocumentSlot.studentWpeCAction().studentWpe().student().group().fromAlias("a")))
                        .where(in(property(SessionDocumentSlot.document().id().fromAlias("a")), rootDql.buildQuery()))
                        .where(likeUpper(property(SessionDocumentSlot.actualStudent().group().title().fromAlias("a")), value(CoreStringUtils.escapeLike(filter, true))))
                        .order(property(SessionDocumentSlot.actualStudent().group().title().fromAlias("a")))
                        .predicate(DQLPredicateType.distinct);

                Collection courseCurrentList = model.getSettings().get("courseCurrentList");
                if (courseCurrentList != null && !courseCurrentList.isEmpty())
                    builder.where(in(property(SessionDocumentSlot.actualStudent().group().course().fromAlias("a")), courseCurrentList));

                if (set != null)
                    builder.where(in(property(SessionDocumentSlot.actualStudent().group().id().fromAlias("a")), set));

                return new DQLListResultBuilder(builder, 50);
            }
        });
    }

    @Override
    public void refreshDataSource(final Model model)
    {
        final Session session = this.getSession();
        final DynamicListDataSource<SessionDocument> dataSource = model.getDataSource();

        {
            final DQLSelectBuilder dql = this.order.buildDQLSelectBuilder();
            dql.where(notInstanceOf(order.getRootAlias(), SessionStudentGradeBookDocument.class));
            dql.where(notInstanceOf(order.getRootAlias(), SessionGlobalDocument.class));

            DQLSelectBuilder slotDql = new DQLSelectBuilder()
                .fromEntity(SessionDocumentSlot.class, "slot")
                .where(eq(property(SessionDocumentSlot.studentWpeCAction().studentWpe().student().educationOrgUnit().groupOrgUnit().fromAlias("slot")), value(model.getOrgUnit())))
                .where(eq(property(order.getRootAlias()), property(SessionDocumentSlot.document().fromAlias("slot"))));
            applyFilters(slotDql, "slot", "mark", model.getSettings());
            dql.where(exists(slotDql.buildQuery()));

            this.order.applyOrder(dql, dataSource.getEntityOrder());
            UniBaseUtils.createPage(dataSource, dql.column(property("doc")), session);
        }
    }

    /** Фильтрует по стандартному для документов набору фильтров
     * @param dql - билдер запроса
     * @param slotAlias - алиас слота
     * @param settings - IDataSettings, содержащие значения фильтров
     */
    public static void applyFilters(final DQLSelectBuilder dql, final String slotAlias, final String markAlias, final IDataSettings settings)
    {
        FilterUtils.applySelectFilter(dql, slotAlias, SessionDocumentSlot.studentWpeCAction().studentWpe().year().educationYear().s(), settings.get("year"));
        FilterUtils.applySelectFilter(dql, slotAlias, SessionDocumentSlot.studentWpeCAction().studentWpe().part().s(), settings.get("part"));
        final String docNumber = settings.get("docNumber");
        if (StringUtils.isNotBlank(docNumber)) {
            dql.where(eq(property(SessionDocumentSlot.document().number().fromAlias(slotAlias)), value(docNumber)));
        }
        FilterUtils.applySimpleLikeFilter(dql, slotAlias, SessionDocumentSlot.actualStudent().person().identityCard().lastName().s(), settings.<String>get("studentLastName"));

        FilterUtils.applySelectFilter(dql, slotAlias, SessionDocumentSlot.actualStudent().course().s(), settings.get("courseCurrentList"));
        FilterUtils.applySelectFilter(dql, slotAlias, SessionDocumentSlot.actualStudent().group().s(), settings.get("groupCurrentList"));

        FilterUtils.applyBetweenFilter(dql, slotAlias, SessionDocumentSlot.document().formingDate().s(), settings.<Date>get("formedFrom"), settings.<Date>get("formedTo"));

        final Date performDateFrom = settings.get("performDateFrom");
        final Date performDateTo = settings.get("performDateTo");
        if (null != performDateFrom || null != performDateTo)
        {
            if (!dql.hasAlias("mark")) {
                dql.joinEntity("slot", DQLJoinType.inner, SessionMark.class, markAlias, eq(property("slot.id"), property(SessionMark.slot().id().fromAlias(markAlias))));
            }
            FilterUtils.applyBetweenFilter(dql, markAlias, SessionMark.performDate().s(), performDateFrom, performDateTo);
        }
    }
}
