/* $Id:$ */
package ru.tandemservice.unisession.base.bo.SessionListDocument.ui.Edit;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.unisession.entity.catalog.SessionsSimpleDocumentReason;

/**
 * @author oleyba
 * @since 4/10/15
 */
@Configuration
public class SessionListDocumentEdit extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .create();
    }
}