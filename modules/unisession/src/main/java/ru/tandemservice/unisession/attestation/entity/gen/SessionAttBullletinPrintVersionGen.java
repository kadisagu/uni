package ru.tandemservice.unisession.attestation.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unisession.attestation.entity.SessionAttBullletinPrintVersion;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Печатная форма закрытой атт. ведомости
 *
 * При закрытии аттестационной ведомости ее печатная форма на момент закрытия сохраняется в системе, и в дальнейшем печатается именно сохраненная форма, механизм печати заново не запускается.
 * Таким образом, если данные студентов изменятся, печатная форма останется прежней.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionAttBullletinPrintVersionGen extends EntityBase
 implements INaturalIdentifiable<SessionAttBullletinPrintVersionGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.attestation.entity.SessionAttBullletinPrintVersion";
    public static final String ENTITY_NAME = "sessionAttBullletinPrintVersion";
    public static final int VERSION_HASH = 399972809;
    private static IEntityMeta ENTITY_META;

    public static final String L_DOC = "doc";
    public static final String P_CONTENT = "content";

    private SessionAttestationBulletin _doc;     // Аттестационная ведомость
    private byte[] _content;     // Сохраненная печатная форма

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Аттестационная ведомость. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public SessionAttestationBulletin getDoc()
    {
        return _doc;
    }

    /**
     * @param doc Аттестационная ведомость. Свойство не может быть null и должно быть уникальным.
     */
    public void setDoc(SessionAttestationBulletin doc)
    {
        dirty(_doc, doc);
        _doc = doc;
    }

    /**
     * @return Сохраненная печатная форма. Свойство не может быть null.
     */
    @NotNull
    public byte[] getContent()
    {
        return _content;
    }

    /**
     * @param content Сохраненная печатная форма. Свойство не может быть null.
     */
    public void setContent(byte[] content)
    {
        dirty(_content, content);
        _content = content;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SessionAttBullletinPrintVersionGen)
        {
            if (withNaturalIdProperties)
            {
                setDoc(((SessionAttBullletinPrintVersion)another).getDoc());
            }
            setContent(((SessionAttBullletinPrintVersion)another).getContent());
        }
    }

    public INaturalId<SessionAttBullletinPrintVersionGen> getNaturalId()
    {
        return new NaturalId(getDoc());
    }

    public static class NaturalId extends NaturalIdBase<SessionAttBullletinPrintVersionGen>
    {
        private static final String PROXY_NAME = "SessionAttBullletinPrintVersionNaturalProxy";

        private Long _doc;

        public NaturalId()
        {}

        public NaturalId(SessionAttestationBulletin doc)
        {
            _doc = ((IEntity) doc).getId();
        }

        public Long getDoc()
        {
            return _doc;
        }

        public void setDoc(Long doc)
        {
            _doc = doc;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof SessionAttBullletinPrintVersionGen.NaturalId) ) return false;

            SessionAttBullletinPrintVersionGen.NaturalId that = (NaturalId) o;

            if( !equals(getDoc(), that.getDoc()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getDoc());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getDoc());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionAttBullletinPrintVersionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionAttBullletinPrintVersion.class;
        }

        public T newInstance()
        {
            return (T) new SessionAttBullletinPrintVersion();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "doc":
                    return obj.getDoc();
                case "content":
                    return obj.getContent();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "doc":
                    obj.setDoc((SessionAttestationBulletin) value);
                    return;
                case "content":
                    obj.setContent((byte[]) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "doc":
                        return true;
                case "content":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "doc":
                    return true;
                case "content":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "doc":
                    return SessionAttestationBulletin.class;
                case "content":
                    return byte[].class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionAttBullletinPrintVersion> _dslPath = new Path<SessionAttBullletinPrintVersion>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionAttBullletinPrintVersion");
    }
            

    /**
     * @return Аттестационная ведомость. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unisession.attestation.entity.SessionAttBullletinPrintVersion#getDoc()
     */
    public static SessionAttestationBulletin.Path<SessionAttestationBulletin> doc()
    {
        return _dslPath.doc();
    }

    /**
     * @return Сохраненная печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unisession.attestation.entity.SessionAttBullletinPrintVersion#getContent()
     */
    public static PropertyPath<byte[]> content()
    {
        return _dslPath.content();
    }

    public static class Path<E extends SessionAttBullletinPrintVersion> extends EntityPath<E>
    {
        private SessionAttestationBulletin.Path<SessionAttestationBulletin> _doc;
        private PropertyPath<byte[]> _content;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Аттестационная ведомость. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unisession.attestation.entity.SessionAttBullletinPrintVersion#getDoc()
     */
        public SessionAttestationBulletin.Path<SessionAttestationBulletin> doc()
        {
            if(_doc == null )
                _doc = new SessionAttestationBulletin.Path<SessionAttestationBulletin>(L_DOC, this);
            return _doc;
        }

    /**
     * @return Сохраненная печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unisession.attestation.entity.SessionAttBullletinPrintVersion#getContent()
     */
        public PropertyPath<byte[]> content()
        {
            if(_content == null )
                _content = new PropertyPath<byte[]>(SessionAttBullletinPrintVersionGen.P_CONTENT, this);
            return _content;
        }

        public Class getEntityClass()
        {
            return SessionAttBullletinPrintVersion.class;
        }

        public String getEntityName()
        {
            return "sessionAttBullletinPrintVersion";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
