/* $Id$ */
package ru.tandemservice.unisession.catalog.ext.Catalog.ui.DynamicPub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.ui.DynamicPub.CatalogDynamicPub;
import ru.tandemservice.unisession.entity.catalog.SessionCurrentMarkScale;
import ru.tandemservice.unisession.entity.catalog.SessionMarkStateCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionsSimpleDocumentReason;

/**
 * @author Nikolay Fedorovskih
 * @since 07.10.2014
 */
@Configuration
public class CatalogDynamicPubExt extends BusinessComponentExtensionManager
{
    @Autowired
    private CatalogDynamicPub _catalogDynamicPub;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_catalogDynamicPub.presenterExtPoint())
                .addAction(new SessionCurrentMarkScale.ChangeActiveScaleAction())
                .addAction(new SessionsSimpleDocumentReason.ChangeFormOnDebtsAction())
		        .addAction(new SessionMarkStateCatalogItem.ToggleMarkStatePropAction(SessionMarkStateCatalogItem.ToggleMarkStatePropActionType.VALID))
		        .addAction(new SessionMarkStateCatalogItem.ToggleMarkStatePropAction(SessionMarkStateCatalogItem.ToggleMarkStatePropActionType.REMARKABLE))
		        .addAction(new SessionMarkStateCatalogItem.ToggleMarkStatePropAction(SessionMarkStateCatalogItem.ToggleMarkStatePropActionType.VISIBLE))
		        .create();
    }

}
