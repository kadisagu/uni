/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionProtocol.logic;


import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unisession.entity.catalog.SessionRoleInGEC;
import ru.tandemservice.unisession.entity.stateFinalExam.SessionGECMember;
import ru.tandemservice.unisession.entity.stateFinalExam.SessionGovExamCommission;
import ru.tandemservice.unisession.entity.stateFinalExam.SessionStateFinalExamProtocol;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Andreev
 * @since 15.09.2016
 */
public class Utils
{

    /**
     * Удаляет Комиссии ГИА на которые не ссылается ни один Протокол ГИА.
     * Члены ГЭК удаляются каскадно вслед за Коммисией     *
     */
    public static void deleteUselessCommissions()
    {
        ICommonDAO.DAO_CACHE.get().doInTransaction(session -> {

            DQLSelectBuilder selectBuilder = new DQLSelectBuilder()
                    .fromEntity(SessionGovExamCommission.class, "cmm")
                    .column(property("cmm", SessionGovExamCommission.id()))
                    .where(notExists(SessionStateFinalExamProtocol.class, SessionStateFinalExamProtocol.commission().s(), property("cmm")));

            new DQLDeleteBuilder(SessionGovExamCommission.class)
                    .fromEntity(SessionGovExamCommission.class, "cmm")
                    .where(in(property(SessionGovExamCommission.id()), selectBuilder.buildQuery()))
                    .createStatement(session).execute();

            return null;
        });
    }

    /**
     * Создает Членов ГЭК и возвращает Коммисию ГИА их связывающую
     *
     * @return Коммисию для ГИА или Null если ниодного члена коммиссии не создано.
     */
    public static SessionGovExamCommission createCommission(Map<SessionRoleInGEC, List<PpsEntry>> ppsListByRole)
    {
        List<SessionGECMember> memberList = new ArrayList<>();
        for (Map.Entry<SessionRoleInGEC, List<PpsEntry>> entry : ppsListByRole.entrySet())
        {
            List<PpsEntry> ppsEntries = entry.getValue();
            SessionRoleInGEC role = entry.getKey();
            if (ppsEntries.isEmpty()) continue;

            ppsEntries.forEach(pps -> {
                SessionGECMember member = new SessionGECMember();
                member.setPps(pps);
                member.setRoleInGEC(role);
                memberList.add(member);
            });
        }

        if (memberList.isEmpty()) return null;


        ICommonDAO dao = DataAccessServices.dao();

        SessionGovExamCommission commission = new SessionGovExamCommission();
        dao.save(commission);

        memberList.forEach(member -> {
            member.setCommission(commission);
            dao.save(member);
        });

        return commission;
    }
}