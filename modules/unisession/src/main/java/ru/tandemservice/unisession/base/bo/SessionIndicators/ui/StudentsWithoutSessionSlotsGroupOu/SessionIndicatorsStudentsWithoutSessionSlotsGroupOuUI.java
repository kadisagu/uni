/**
 *$Id$
 */
package ru.tandemservice.unisession.base.bo.SessionIndicators.ui.StudentsWithoutSessionSlotsGroupOu;

import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList.AbstractUniStudentListUI;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uniepp.dao.student.EppStudentSlotDAO;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.gen.EppRegistryElementGen;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpePart;
import ru.tandemservice.uniepp.ui.EppRegistryElementSelectModel;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.entity.document.SessionObject;

import java.util.Date;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 20.03.13
 */
@State
({
    @Bind(key = SessionReportManager.BIND_ORG_UNIT, binding = "orgUnitHolder.id", required = true)
})
public class SessionIndicatorsStudentsWithoutSessionSlotsGroupOuUI extends AbstractUniStudentListUI
{
    public static final String PROP_ORG_UNIT = "orgUnit";
    public static final String PROP_SESSION_OBJECT = "sessionObject";
    public static final String PROP_SEARCH_OPTION = "searchOption";
    public static final String PROP_REGISTRY_ELEMENT_LIST = "registryElement";

    private final OrgUnitHolder _orgUnitHolder = new OrgUnitHolder();

    private CommonPostfixPermissionModel _sec;

    private IMultiSelectModel _registryElementModel;

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();

        _orgUnitHolder.refresh();
        _sec = new OrgUnitSecModel(_orgUnitHolder.getValue());

        _registryElementModel = new EppRegistryElementSelectModel<EppRegistryElement>(EppRegistryElement.class)
        {
            @Override
            protected DQLSelectBuilder query(final String alias, final String filter)
            {
                final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppRegistryElement.class, alias);

                final SessionObject sessionObject = getSettings().get(PROP_SESSION_OBJECT);
                if (null == sessionObject)
                {
                    dql.where(isNull(alias + ".id"));
                    return dql;
                }

                final DQLSelectBuilder s = new DQLSelectBuilder().fromEntity(EppStudentWpePart.class, "wpds").column(property(EppStudentWpePart.studentWpe().registryElementPart().registryElement().id().fromAlias("wpds")))
                        .where(isNull(property(EppStudentWpePart.removalDate().fromAlias("wpds"))))
                        .where(eq(property(EppStudentWpePart.studentWpe().year().educationYear().current().fromAlias("wpds")), value(Boolean.TRUE)))
                        .where(eq(property(EppStudentWpePart.studentWpe().part().fromAlias("wpds")), value(sessionObject.getYearDistributionPart())))
                        .predicate(DQLPredicateType.distinct);

                dql.where(in(property(alias + ".id"), s.buildQuery()));

                if (null != filter)
                    dql.where(this.getFilterCondition(alias, filter));

                dql.order(property(EppRegistryElementGen.title().fromAlias(alias)));
                dql.order(property(EppRegistryElementGen.owner().title().fromAlias(alias)));
                return dql;
            }
        };
    }

    public String getDaemonStatus(final String message)
    {
        final Long date = EppStudentSlotDAO.DAEMON.getCompleteStatus();
        if (null != date)
            return DateFormatter.DATE_FORMATTER_WITH_TIME.format(new Date(date));

        return message;
    }

    public void onClickSync()
    {
        // TODO: call daemon
        onComponentRefresh();
    }

    @Override
    public String getSettingsKey()
    {
        return "session.StudentsWithoutSessionSlotsGroupOu." + _orgUnitHolder.getValue().getId() + ".filter";
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        super.onBeforeDataSourceFetch(dataSource);

        dataSource.put(PROP_ORG_UNIT, _orgUnitHolder.getValue());
        dataSource.put(PROP_SESSION_OBJECT, getSettings().get(PROP_SESSION_OBJECT));
        dataSource.put(PROP_SEARCH_OPTION, getSettings().get(PROP_SEARCH_OPTION));
        dataSource.put(PROP_REGISTRY_ELEMENT_LIST, getSettings().get(PROP_REGISTRY_ELEMENT_LIST));
    }

    // Getters & Setters

    public IMultiSelectModel getRegistryElementModel()
    {
        return _registryElementModel;
    }

    public void setRegistryElementModel(IMultiSelectModel registryElementModel)
    {
        _registryElementModel = registryElementModel;
    }

    public OrgUnitHolder getOrgUnitHolder()
    {
        return _orgUnitHolder;
    }

    public CommonPostfixPermissionModel getSec()
    {
        return _sec;
    }

    public void setSec(CommonPostfixPermissionModel sec)
    {
        _sec = sec;
    }
}
