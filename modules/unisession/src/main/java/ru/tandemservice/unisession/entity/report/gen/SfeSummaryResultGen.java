package ru.tandemservice.unisession.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unisession.entity.report.SfaSummaryResult;
import ru.tandemservice.unisession.entity.report.SfeSummaryResult;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Отчет «Сводный отчет по результатам гос. экзамена»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SfeSummaryResultGen extends SfaSummaryResult
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.report.SfeSummaryResult";
    public static final String ENTITY_NAME = "sfeSummaryResult";
    public static final int VERSION_HASH = 1703455549;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof SfeSummaryResultGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SfeSummaryResultGen> extends SfaSummaryResult.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SfeSummaryResult.class;
        }

        public T newInstance()
        {
            return (T) new SfeSummaryResult();
        }
    }
    private static final Path<SfeSummaryResult> _dslPath = new Path<SfeSummaryResult>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SfeSummaryResult");
    }
            

    public static class Path<E extends SfeSummaryResult> extends SfaSummaryResult.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return SfeSummaryResult.class;
        }

        public String getEntityName()
        {
            return "sfeSummaryResult";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
