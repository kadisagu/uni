/* $Id:$ */
package ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinMark;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;

/**
 * @author oleyba
 * @since 3/2/11
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component) {
        final Model model = getModel(component);
        this.getDao().prepareRatingSettings(model);
        if (model.isNeedToSaveCurrentRatingData()) {
            ISessionBrsDao.instance.get().saveCurrentRating(model.getDocument());
        }
        this.getDao().prepare(model);
    }

    public void onClickApply(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        if (this.getDao().validate(model))
        {
            this.getDao().update(model);
            this.deactivate(component);
        }
    }

    public void onClickSaveCurrentRating(final IBusinessComponent component) {
        Model model = this.getModel(component);

        ISessionBrsDao.instance.get().saveCurrentRating(model.getDocument());
        getDao().prepareCurrentRatingData(model);
        getDao().prepareAllowance(model);
    }
}