package ru.tandemservice.unisession.attestation.entity.report;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.uni.IStorableReport;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.unisession.attestation.entity.SessionAttestation;
import ru.tandemservice.unisession.attestation.entity.report.gen.SessionAttestationTotalResultReportGen;

/**
 * Отчет «Итоги межсессионной аттестации студентов»
 */
public class SessionAttestationTotalResultReport extends SessionAttestationTotalResultReportGen implements IStorableReport
{
    @Override
    @EntityDSLSupport(parts = {P_ATTESTATION_NUMBER, L_YEAR_DISTRIBUTION_PART + "." + YearDistributionPart.P_TITLE, L_EDUCATION_YEAR + "." + EducationYear.P_TITLE, L_ATTESTATION + "." + SessionAttestation.P_SHORT_TITLE})
    public String getAttestationTitle()
    {
        if (getAttestation() == null)
            return "№" + getAttestationNumber() + " " + getYearDistributionPart().getTitle() + " " + getEducationYear().getTitle();
        else
            return getAttestation().getShortTitle();
    }
}