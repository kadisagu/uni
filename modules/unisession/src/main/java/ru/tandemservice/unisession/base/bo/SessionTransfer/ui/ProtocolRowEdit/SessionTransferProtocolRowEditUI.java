/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionTransfer.ui.ProtocolRowEdit;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unisession.base.bo.SessionMark.daemon.SessionMarkDaemonBean;
import ru.tandemservice.unisession.base.bo.SessionTransfer.SessionTransferManager;
import ru.tandemservice.unisession.entity.document.SessionTransferProtocolMark;
import ru.tandemservice.unisession.entity.document.SessionTransferProtocolRow;

import java.util.List;

import static ru.tandemservice.unisession.base.bo.SessionReexamination.SessionReexaminationManager.*;

/**
 * @author Alexey Lopatin
 * @since 17.09.2015
 */
@State({@Bind(key = UIPresenter.PUBLISHER_ID, binding = "row.id")})
public class SessionTransferProtocolRowEditUI extends UIPresenter
{
    private SessionTransferProtocolRow _row = new SessionTransferProtocolRow();
    private List<SessionTransferProtocolMark> _protocolMarks;
    private SessionTransferProtocolMark _currentProtocolMark;

    @Override
    public void onComponentRefresh()
    {
        _row = DataAccessServices.dao().getNotNull(_row.getId());
        _protocolMarks = DataAccessServices.dao().getList(SessionTransferProtocolMark.class, SessionTransferProtocolMark.protocolRow(), _row, SessionTransferProtocolMark.controlAction().priority().s());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (dataSource.getName().equals(MARK_DS))
        {
            dataSource.put(PARAM_REG_ELEMENT_PART_ID, _currentProtocolMark.getProtocolRow().getRequestRow().getRegElementPart().getId());
            dataSource.put(PARAM_FCA_TYPE_ID, _currentProtocolMark.getControlAction().getId());
        }
    }

    public void onClickApply()
    {
        DataAccessServices.dao().update(_row);
        for (SessionTransferProtocolMark protocolMark : _protocolMarks)
            DataAccessServices.dao().update(protocolMark);

        SessionTransferManager.instance().dao().onCreateSessionDocumentSlot(_row.getProtocol(), _row);
        SessionMarkDaemonBean.DAEMON.wakeUpDaemon();

        deactivate();
    }

    public void onChangeProtocolMark()
    {
        if (null != _currentProtocolMark.getSlot() && null == getCurrentProtocolMark().getMark())
            throw new ApplicationException("Нельзя удалять оценку строки протокола, если студенту по ней уже выставлена оценка.");
    }

    // Getters & Setters

    public DataWrapper getItemReexamination()
    {
        SelectDataSource ds = getConfig().getDataSource(REEXAMINATION_DS);
        return ds.getRecordById(_row.isNeedRetake() ? ITEM_REATTESTATION : ITEM_REEXAMINATION);
    }

    public void setItemReexamination(DataWrapper item)
    {
        _row.setNeedRetake(ITEM_REATTESTATION.equals(item.getId()));
    }

    public SessionTransferProtocolRow getRow()
    {
        return _row;
    }

    public void setRow(SessionTransferProtocolRow row)
    {
        _row = row;
    }

    public List<SessionTransferProtocolMark> getProtocolMarks()
    {
        return _protocolMarks;
    }

    public void setProtocolMarks(List<SessionTransferProtocolMark> protocolMarks)
    {
        _protocolMarks = protocolMarks;
    }

    public SessionTransferProtocolMark getCurrentProtocolMark()
    {
        return _currentProtocolMark;
    }

    public void setCurrentProtocolMark(SessionTransferProtocolMark currentProtocolMark)
    {
        _currentProtocolMark = currentProtocolMark;
    }
}
