/* $Id$ */
package ru.tandemservice.unisession.dao.io;

import com.google.common.collect.Multimap;
import com.google.common.collect.TreeMultimap;
import com.healthmarketscience.jackcess.*;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.hibernate.Session;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SingleValueCache;
import org.tandemframework.core.util.concurrent.semaphore.IThreadSemaphore;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.*;
import ru.tandemservice.uni.dao.mdbio.BaseIODao;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.DebugUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.base.bo.SessionMark.SessionMarkManager;
import ru.tandemservice.unisession.base.bo.SessionMark.event.SessionRegularMarkFiledAndCommissionChecker;
import ru.tandemservice.unisession.base.bo.SessionMark.logic.ISessionMarkDAO;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.comission.SessionComission;
import ru.tandemservice.unisession.entity.document.*;
import ru.tandemservice.unisession.entity.document.gen.SessionDocumentSlotGen;
import ru.tandemservice.unisession.entity.document.gen.SessionProjectThemeGen;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.mark.SessionSlotLinkMark;

import java.io.*;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 11/2/11
 */
public class SessionIODAO extends BaseIODao implements ISessionIODAO
{

    public static final String IMPORT_MARKS_ERROR_TEXT = "Импорт данных провести не удалось: произошла ошибка при обращении к файлу импорта. Обратитесь к логу импорта (файл sessionMarksImport.log) на сервере приложения для получения детальной информации об ошибке.";
    private static final Object IMPORT_MARKS_MUTEX = new Object();

    protected static final Logger log4j_logger = Logger.getLogger(SessionIODAO.class);

    @Override
    public void exportSessionTemplate(OutputStream outputStream)
    {
        try
        {
            final File file = File.createTempFile("mdb-io-", "mdb");
            try
            {
                final Database mdb = Database.create(Database.FileFormat.V2003, file, false);
                exportSessionTemplate(mdb);
                mdb.close();

                InputStream in = new FileInputStream(file);
                IOUtils.copy(in, outputStream);
                in.close();
            } finally
            {
                file.delete();
            }
        } catch (final Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    @Override
    public void exportSessionTemplate(final Database mdb) throws Exception
    {
        DebugUtils.debug(logger, new DebugUtils.Section("exportSessionTemplate")
        {
            @Override
            public void execute() throws Exception
            {
                if (null != mdb.getTable("student_t"))
                {
                    return;
                }

                DebugUtils.debug(
                        new DebugUtils.Section("marks")
                        {
                            @Override
                            public void execute() throws Exception
                            {
                                export_Marks(mdb);
                            }
                        }
                );
            }
        });
    }

    private void export_Marks(final Database mdb) throws Exception
    {
        if (null != mdb.getTable("mark_t"))
        {
            return;
        }

        final Session session = this.getSession();

        final TableBuilder studentTableBuilder = new TableBuilder("student_t")
                .addColumn(new ColumnBuilder("id", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("fio", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder("personal_number", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("group", DataType.TEXT).setCompressedUnicode(true).toColumn());

        final Table student_t = studentTableBuilder.toTable(mdb);

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(Student.class, "s")
                .joinPath(DQLJoinType.left, Student.group().fromAlias("s"), "g")
                .where(eq(property(Student.archival().fromAlias("s")), value(Boolean.FALSE)))
                .column(property(Student.id().fromAlias("s")))
                .column(property(Student.person().identityCard().fullFio().fromAlias("s")))
                .column(property(Student.personalNumber().fromAlias("s")))
                .column(property(Group.title().fromAlias("g")));

        BatchUtils.execute(dql.createStatement(session).<Object[]>list(), 128, rows -> {
            try
            {
                for (final Object[] row : rows)
                {
                    student_t.addRow(Long.toHexString((Long) row[0]), row[1], row[2], row[3]);
                }
                mdb.flush();
            } catch (final Throwable t)
            {
                throw CoreExceptionUtils.getRuntimeException(t);
            }
        });

        final Map markCatalogMap = catalog(SessionMarkCatalogItem.class, "title").export(mdb);

        final TableBuilder tableBuilder = new TableBuilder("mark_t")
                .addColumn(new ColumnBuilder("wpe_ca_id", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("student_id", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("year", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("term", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("yearPart", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("discipline_title", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder("discipline_number", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("control_action_type", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("import_mark", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("import_mark_date", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("final_mark", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("final_mark_id", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("import_theme", DataType.MEMO).setCompressedUnicode(true).setLength(1024 * JetFormat.TEXT_FIELD_UNIT_SIZE).toColumn());

        final Table mark_t = tableBuilder.toTable(mdb);


        final List<Long> wpeCActionIds = new DQLSelectBuilder().fromEntity(EppStudentWpeCAction.class, "wpca").column(property("wpca.id"))
                .where(isNull(property(EppStudentWpeCAction.removalDate().fromAlias("wpca"))))
                .where(DQLExpressions.eq(property(EppStudentWpeCAction.studentWpe().student().archival().fromAlias("wpca")), DQLExpressions.value(Boolean.FALSE)))
                .createStatement(session)
                .list();

        BatchUtils.execute(wpeCActionIds, 128, wpeCActionIdsPart -> {
            try
            {

                IDQLSelectableQuery impMarkDql = new DQLSelectBuilder()
                        .fromEntity(SessionGlobalDocument.class, "gd")
                        .fromEntity(SessionMark.class, "m")
                        .where(eq(property(SessionMark.slot().document().fromAlias("m")), property("gd")))
                        .column(property(SessionMark.slot().studentWpeCAction().id().fromAlias("m")), "wpca_id")
                        .column(property(SessionMark.cachedMarkValue().id().fromAlias("m")), "mark_value")
                        .column(property(SessionMark.performDate().fromAlias("m")), "mark_date")
                        .buildQuery();

                DQLSelectBuilder finalMarkDql = new DQLSelectBuilder()
                        .fromEntity(SessionStudentGradeBookDocument.class, "gb")
                        .fromEntity(SessionMark.class, "m")
                        .where(eq(property(SessionMark.slot().document().fromAlias("m")), property("gb")))
                        .where(eq(property(SessionMark.slot().inSession().fromAlias("m")), value(Boolean.FALSE)))
                        .column(property(SessionMark.slot().studentWpeCAction().id().fromAlias("m")), "wpca_id")
                        .column(property(SessionMark.cachedMarkValue().id().fromAlias("m")), "mark_value")
                        .column(property(SessionMark.id().fromAlias("m")), "mark_id")
                        ;

                DQLSelectBuilder dql1 = new DQLSelectBuilder()
                        .fromEntity(EppStudentWpeCAction.class, "wpca")
                        .joinDataSource("wpca", DQLJoinType.left, impMarkDql, "imp_mark", eq("wpca.id", "imp_mark.wpca_id"))
                        .joinDataSource("wpca", DQLJoinType.left, finalMarkDql.buildQuery(), "final_mark", eq("wpca.id", "final_mark.wpca_id"))
                        .joinEntity("wpca", DQLJoinType.inner, EppFControlActionType.class, "caType",
									eq(property("wpca", EppStudentWpeCAction.type()), property("caType", EppFControlActionType.eppGroupType())))
                        .column(property(EppStudentWpeCAction.id().fromAlias("wpca")))
                        .column(property(EppStudentWpeCAction.studentWpe().student().id().fromAlias("wpca")))
                        .column(property(EppStudentWpeCAction.studentWpe().year().educationYear().intValue().fromAlias("wpca")))
                        .column(property(EppStudentWpeCAction.studentWpe().term().intValue().fromAlias("wpca")))
                        .column(property(EppStudentWpeCAction.studentWpe().part().shortTitle().fromAlias("wpca")))
                        .column(property(EppStudentWpeCAction.studentWpe().registryElementPart().registryElement().title().fromAlias("wpca")))
                        .column(property(EppStudentWpeCAction.studentWpe().registryElementPart().registryElement().number().fromAlias("wpca")))
                        .column(property(EppStudentWpeCAction.type().title().fromAlias("wpca")))
                        .column("imp_mark.mark_value")
                        .column("imp_mark.mark_date")
                        .column("final_mark.mark_value")
                        .column("final_mark.mark_id")
                        .column(property("caType", EppFControlActionType.group().themeRequired()), "caType_themeRequired")
                        .where(in(property(EppStudentWpeCAction.id().fromAlias("wpca")), wpeCActionIdsPart));

	            Map<Long, String> cAction2Theme = getWpeCAction2theme(wpeCActionIdsPart);

                for (final Object[] row : dql1.createStatement(session).<Object[]>list())
                {
	                String theme = cAction2Theme.get((Long)row[0]);
                    mark_t.addRow(
                            Long.toHexString((Long) row[0]),
                            Long.toHexString((Long) row[1]),
                            String.valueOf(row[2]),
                            String.valueOf(row[3]),
                            row[4],
                            row[5],
                            row[6],
                            row[7],
                            row[8] == null ? "" : markCatalogMap.get(row[8]),
                            row[9] == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format((Date) row[9]),
                            row[10] == null ? "" : markCatalogMap.get(row[10]),
                            row[11] == null ? "" : Long.toHexString((Long) row[11]),
		                    (Boolean)(row[12])? (theme != null ? theme: ""):"0"
                    );
                }

                mdb.flush();
            } catch (final Throwable t)
            {
                throw CoreExceptionUtils.getRuntimeException(t);
            }
        });
    }

	/**
	 * Получить для заданных МСРП-ФК темы работы в сессии. Для МСРП-ФК берется слот в документе, для которого выставлена итоговая оценка, для этого слота берется тема.
	 * Если для МСРП-ФК нет итоговой оценки, тема берется для слота документа с наибольшей датой формирования.
	 * @param wpeCActionIds id МСРП-ФК, для которых нужны темы
	 * @return {id МСРП-ФК} -> {тема}
	 */
	private Map<Long, String> getWpeCAction2theme(Collection<Long> wpeCActionIds)
	{
		Map<Long, String> result = getCActionWithFinalMark2Theme(wpeCActionIds);
		Collection<Long> cActionsWOFinalMarks = wpeCActionIds.stream().filter(id -> !result.containsKey(id)).collect(Collectors.toList());
		result.putAll(getCActionWOFinalMark2Theme(cActionsWOFinalMarks));
		return result;
	}

	/**
	 * Получить для МСРП-ФК, имеющих итоговую оценку, темы работы в сессии. Итоговая оценка - это {@link SessionSlotLinkMark}, выставленная в электронной зачетке, для которой "в сессию" = false.
	 * @param wpeCActionIds id МСРП-ФК, которые нужно проверить на наличие итоговой оценки, и при наличии получить тему.
	 * @return {id МСРП-ФК} -> {тема}
	 */
	private Map<Long, String> getCActionWithFinalMark2Theme(Collection<Long> wpeCActionIds)
	{
		final String linkMarkAlias = "linkMark";
		final String regularSlotAlias = "regularSlot";
		final String linkMarkDocumentAlias = "linkMarkDocument";
		final String themeAlias = "theme";
		DQLSelectBuilder cActionDql = new DQLSelectBuilder().fromEntity(SessionSlotLinkMark.class, linkMarkAlias)
				.joinPath(DQLJoinType.left, SessionSlotLinkMark.target().slot().fromAlias(linkMarkAlias), regularSlotAlias)
				.joinPath(DQLJoinType.left, SessionSlotLinkMark.slot().document().fromAlias(linkMarkAlias), linkMarkDocumentAlias)
				.where(in(property(regularSlotAlias, SessionDocumentSlot.studentWpeCAction().id()), wpeCActionIds))
				.where(is(linkMarkDocumentAlias, SessionStudentGradeBookDocument.class))
				.where(eq(property(linkMarkAlias, SessionSlotLinkMark.slot().inSession()), value(Boolean.FALSE)))
				.joinEntity(regularSlotAlias, DQLJoinType.inner, SessionProjectTheme.class, themeAlias, eq(property(regularSlotAlias), property(themeAlias, SessionProjectTheme.slot())))
				.column(property(regularSlotAlias, SessionDocumentSlot.studentWpeCAction().id()))
				.column(property(themeAlias, SessionProjectTheme.theme()));

		Map<Long, String> result = new HashMap<>();
		cActionDql.createStatement(getSession()).<Object[]>list().forEach((Object[] row) -> result.put((Long)row[0], (String)row[1]));
		return result;
	}

	/**
	 * Получить для МСРП-ФК без итоговой оценки темы работы в сессии. Для таких МСРП-ФК берутся темы для всех документов, из них выбирается тема для документа с наибольшей датой формирования.
	 * @param wpeCActionWOFinalMarkIds id МСРП-ФК, не имеющих итоговой оценки.
	 * @return {id МСРП-ФК} -> {тема}
	 */
	private Map<Long, String> getCActionWOFinalMark2Theme(Collection<Long> wpeCActionWOFinalMarkIds)
	{
		final String slotAlias = "slot";
		final String themeAlias = "theme";
		DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(SessionDocumentSlot.class, slotAlias)
				.where(in(property(slotAlias, SessionDocumentSlot.studentWpeCAction().id()), wpeCActionWOFinalMarkIds))
				.joinEntity(slotAlias, DQLJoinType.inner, SessionProjectTheme.class, themeAlias, eq(property(slotAlias), property(themeAlias, SessionProjectTheme.slot())))
				.column(property(slotAlias, SessionDocumentSlot.studentWpeCAction().id()))
				.column(property(slotAlias, SessionDocumentSlot.document().formingDate()))
				.column(property(themeAlias, SessionProjectTheme.theme()));

		Multimap<Long, Object[]> cAction2sortedRows = TreeMultimap.create(Long::compare, Comparator.comparing((Object[] row) -> (Date)row[1]).reversed());

		dql.createStatement(getSession()).<Object[]>list()
				.forEach(row -> cAction2sortedRows.put((Long) row[0], row));

		Map<Long, String> cAction2Theme = new HashMap<>();
		cAction2sortedRows.asMap().forEach((cActionId, rows) -> cAction2Theme.put(cActionId, (String)rows.iterator().next()[2]));
		return cAction2Theme;
	}

    @Override
    public void doImportMarks(Database mdb)
    {
        synchronized (IMPORT_MARKS_MUTEX)
        {

            Debug.stopLogging();

            // дополнительно будем логировать в отдельный файл
            final FileAppender appender = this.registerMarksImportAppender();

            // усыпляем листенеры проверки консистентности отметок
            final IThreadSemaphore lock = SessionRegularMarkFiledAndCommissionChecker.lock();

            // выключаем системное логирование, чтобы не тормозило
            final IEventServiceLock eventLock = CoreServices.eventService().lock();

            try
            {
                importMarks(mdb);
            } catch (ApplicationException e)
            {
                throw e;
            } catch (Throwable t)
            {
                log4j_logger.fatal(t.getMessage(), t);
                throw new ApplicationException(IMPORT_MARKS_ERROR_TEXT);
            } finally
            {

                Debug.resumeLogging();

                // отцепляем дополнительное логирование
                if (null != appender)
                {
                    log4j_logger.removeAppender(appender);
                }

                // будим обратно листенеры проверки консистентности отметок
                lock.release();

                // включаем системное логирование
                eventLock.release();
            }
        }

    }

    private static class StudentWpeCaData
    {

        Long id;
        Long eduYearId;
        Long yearPartId;
        Long importMarkId;
        Long importMarkSlotId;
        Long importMarkValueId;
        String importMarkDate;
        boolean caGroupTypeRequireTheme;

        private StudentWpeCaData(Long id, Long eduYearId, Long yearPartId, Long importMarkId, Long importMarkSlotId, Long importMarkValueId, Date importMarkDate, boolean caGroupTypeRequireTheme)
        {
            this.eduYearId = eduYearId;
            this.id = id;
            this.importMarkDate = DateFormatter.DEFAULT_DATE_FORMATTER.format(importMarkDate);
            this.importMarkId = importMarkId;
            this.importMarkSlotId = importMarkSlotId;
            this.importMarkValueId = importMarkValueId;
            this.yearPartId = yearPartId;
            this.caGroupTypeRequireTheme = caGroupTypeRequireTheme;
        }
    }

    private void importMarks(Database mdb)
    {
        final Session session = getSession();

        final Map<Long, StudentWpeCaData> wpcaMap = new HashMap<>();
        final Map<MultiKey, SessionGlobalDocument> globalDocumentMap = new HashMap<>();

        IDQLSelectableQuery impMarkDql = new DQLSelectBuilder()
                .fromEntity(SessionGlobalDocument.class, "gd")
                .fromEntity(SessionMark.class, "m")
                .where(eq(property(SessionMark.slot().document().fromAlias("m")), property("gd")))
                .column(property(SessionMark.slot().studentWpeCAction().id().fromAlias("m")), "wpca_id")
                .column(property(SessionMark.id().fromAlias("m")), "mark_id")
                .column(property(SessionMark.slot().id().fromAlias("m")), "slot_id")
                .column(property(SessionMark.cachedMarkValue().id().fromAlias("m")), "mark_value")
                .column(property(SessionMark.performDate().fromAlias("m")), "mark_date")
                .buildQuery();

        IDQLSelectableQuery impSlotDql = new DQLSelectBuilder()
                .fromEntity(SessionGlobalDocument.class, "gd")
                .fromEntity(SessionDocumentSlot.class, "slot")
                .where(eq(property(SessionDocumentSlot.document().fromAlias("slot")), property("gd")))
                .column(property(SessionDocumentSlot.studentWpeCAction().id().fromAlias("slot")), "wpca_id")
                .column(property(SessionDocumentSlot.id().fromAlias("slot")), "slot_id")
                .buildQuery();

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppStudentWpeCAction.class, "wpca")
                .where(isNull(property(EppStudentWpeCAction.removalDate().fromAlias("wpca"))))
                .joinDataSource("wpca", DQLJoinType.left, impMarkDql, "imp_mark", eq("wpca.id", "imp_mark.wpca_id"))
                .joinDataSource("wpca", DQLJoinType.left, impSlotDql, "imp_slot", eq("wpca.id", "imp_slot.wpca_id"))
                .joinEntity("wpca", DQLJoinType.left, EppFControlActionType.class, "caType",
                            eq(property("wpca", EppStudentWpeCAction.type()), property("caType", EppFControlActionType.eppGroupType())))
                .column(property(EppStudentWpeCAction.id().fromAlias("wpca")))
                .column(property(EppStudentWpeCAction.studentWpe().year().educationYear().id().fromAlias("wpca")))
                .column(property(EppStudentWpeCAction.studentWpe().part().id().fromAlias("wpca")))
                .column("imp_mark.mark_id")
                .column("imp_slot.slot_id")
                .column("imp_mark.mark_value")
                .column("imp_mark.mark_date")
                .column(property("caType", EppFControlActionType.group().themeRequired()));


        for (Object[] row : dql.createStatement(session).<Object[]>list())
        {
            wpcaMap.put((Long) row[0], new StudentWpeCaData((Long) row[0], (Long) row[1], (Long) row[2], (Long) row[3], (Long) row[4], (Long) row[5], (Date) row[6], (Boolean)row[7]));
        }

        for (SessionGlobalDocument doc : getList(SessionGlobalDocument.class))
        {
            globalDocumentMap.put(new MultiKey(doc.getEducationYear().getId(), doc.getYearDistributionPart().getId()), doc);
        }

        final Map<String, SessionMarkCatalogItem> markValueMap = SessionIODAO.this.catalog(SessionMarkCatalogItem.class, "title").lookup(false);
        final SimpleDateFormat dateFormat = new SimpleDateFormat(DateFormatter.PATTERN_DEFAULT);

        final Date currentTime = new Date();

        final SingleValueCache<SessionComission> cachedValue = new SingleValueCache<SessionComission>()
        {
            @Override
            protected SessionComission resolve()
            {
                // TODO: return ISessionCommissionDAO.instance.get().getEmptyComission();
                // пока создаем новую сессию, но! только если было обращение
                final SessionComission c = new SessionComission();
                session.save(c);
                return c;
            }
        };

        final Table mark_t;
        try
        {
            mark_t = mdb.getTable("mark_t");
        } catch (IOException e)
        {
            throw new ApplicationException("Не удалось обратиться к таблице mark_t в mdb-файле.", e);
        }

        if (null == mark_t)
        {
            throw new ApplicationException("Таблица «mark_t» отсутствует в базе данных.");
        }

        for (Map<String, Object> row : mark_t)
        {

            Long wpcaId = id(row, "wpe_ca_id");
            StudentWpeCaData studentWpeCaData = wpcaMap.get(wpcaId);

            if (studentWpeCaData == null)
            {
                logger.error("Student workplan control action not found, id = " + wpcaId);
                continue;
            }

            final String markValueStr = (String) row.get("import_mark");
            if (StringUtils.isEmpty(markValueStr))
            {
                if (null != studentWpeCaData.importMarkId)
                {
                    delete(studentWpeCaData.importMarkId);
                }
                continue;
            }

            final SessionMarkCatalogItem markValue = markValueMap.get(markValueStr);
            if (null == markValue)
            {
                logger.error("Mark \"" + markValueStr + "\" not found");
                continue;
            }

            final String dateStr = (String) row.get("import_mark_date");
            Date date;
            try
            {
                date = dateFormat.parse(dateStr);
            } catch (ParseException e)
            {
                date = currentTime;
            }
            final Date performDate = date;

            final ISessionMarkDAO.MarkData markData = new ISessionMarkDAO.MarkData()
            {
                @Override
                public Date getPerformDate()
                {
                    return performDate;
                }

                @Override
                public Double getPoints()
                {
                    return null;
                }

                @Override
                public SessionMarkCatalogItem getMarkValue()
                {
                    return markValue;
                }

                @Override
                public String getComment()
                {
                    return null;
                }
            };

            SessionDocumentSlot docSlot;

            if (studentWpeCaData.importMarkId != null)
            {

                if (studentWpeCaData.importMarkValueId.equals(markValue.getId()) && (dateStr == null || studentWpeCaData.importMarkDate.equals(dateStr)))
                {
                    continue;
                }

                docSlot = (SessionDocumentSlot) session.load(SessionDocumentSlot.class, studentWpeCaData.importMarkSlotId);

                SessionMarkManager.instance().regularMarkDao().saveOrUpdateMark(docSlot, markData);
            } else
            {

                MultiKey docKey = new MultiKey(studentWpeCaData.eduYearId, studentWpeCaData.yearPartId);
                SessionGlobalDocument document = globalDocumentMap.get(docKey);
                if (document == null)
                {
                    EducationYear year = get(EducationYear.class, studentWpeCaData.eduYearId);
                    YearDistributionPart part = get(YearDistributionPart.class, studentWpeCaData.yearPartId);
                    document = new SessionGlobalDocument();
                    document.setNumber(year.getTitle() + ", " + part.getTitle());
                    document.setFormingDate(currentTime);
                    document.setYearDistributionPart(part);
                    document.setEducationYear(year);
                    session.save(document);
                    globalDocumentMap.put(docKey, document);
                }

                if (null != studentWpeCaData.importMarkSlotId)
                {
                    docSlot = (SessionDocumentSlot) session.load(SessionDocumentSlot.class, studentWpeCaData.importMarkSlotId);
                } else
                {
                    EppStudentWpeCAction wpca = (EppStudentWpeCAction) session.load(EppStudentWpeCAction.class, studentWpeCaData.id);
                    docSlot = this.getByNaturalId(new SessionDocumentSlotGen.NaturalId(document, wpca, false));
                    if (null == docSlot)
                    {
                        docSlot = new SessionDocumentSlot(document, wpca, false);
                        docSlot.setCommission(cachedValue.get());
                        session.save(docSlot);
                    }
                }

                SessionMarkManager.instance().regularMarkDao().saveOrUpdateMark(docSlot, markData);
            }

            String theme = (String)row.get("import_theme");
            if (StringUtils.isEmpty(theme))
                continue;

            if (studentWpeCaData.caGroupTypeRequireTheme)
            {
                SessionProjectTheme projectTheme = getByNaturalId(new SessionProjectThemeGen.NaturalId(docSlot));
                if (null == projectTheme)
                {
                    projectTheme = new SessionProjectTheme();
                    projectTheme.setSlot(docSlot);
                }
                projectTheme.setTheme(theme);
                session.save(projectTheme);
            }
            else if (! theme.equals("0"))
                logger.error("Theme can not be imported. Control action type cannot have the theme, id = " + wpcaId);
        }
    }

    private FileAppender registerMarksImportAppender()
    {
        try
        {
            final FileAppender appender = new FileAppender(new PatternLayout("%-4r [%t] %-5p %c %x - %m%n"), Paths.get(System.getProperty("catalina.base"), "logs", "sessionMarksImport.log").toString());
            appender.setName("sessionMarksImportLogAppender");
            appender.setThreshold(Level.INFO);
            SessionIODAO.log4j_logger.addAppender(appender);
            SessionIODAO.log4j_logger.setLevel(Level.INFO);
            SessionIODAO.log4j_logger.setAdditivity(false);
            return appender;
        } catch (final Throwable t)
        {
            SessionIODAO.log4j_logger.error(t.getMessage(), t);
            return null;
        }
    }
}
