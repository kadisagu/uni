/* $Id$ */
package ru.tandemservice.unisession.component.sessionRetakeDoc.SessionRetakeDocPub;

import ru.tandemservice.uni.dao.IPrepareable;
import ru.tandemservice.unisession.entity.document.SessionRetakeDocument;

/**
 * @author oleyba
 * @since 6/15/11
 */
public interface IDAO extends IPrepareable<Model>
{
    void doCloseBulletin(Model model);

    void doOpenBulletin(Model model);

    void delete(SessionRetakeDocument retakeDoc);

    void doPrintBulletin(Model model);
}
