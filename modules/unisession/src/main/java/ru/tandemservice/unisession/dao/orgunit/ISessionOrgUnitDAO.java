package ru.tandemservice.unisession.dao.orgunit;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import org.tandemframework.core.util.cache.SpringBeanCache;

/**
 * @author vdanilov
 */
public interface ISessionOrgUnitDAO {

    SpringBeanCache<ISessionOrgUnitDAO> instance = new SpringBeanCache<ISessionOrgUnitDAO>(ISessionOrgUnitDAO.class.getName());

    /**
     * показывать вкладку "Сессия" на подразделении, или нет
     * @param orgUnitId id подразделения
     * @return показывать\нет
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    boolean canShowSessionOrgUnitTab(Long orgUnitId);
}
