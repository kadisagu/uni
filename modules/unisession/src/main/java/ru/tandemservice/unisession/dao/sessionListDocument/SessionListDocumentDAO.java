// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisession.dao.sessionListDocument;

import com.google.common.collect.Collections2;
import org.springframework.util.ClassUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.INumberGenerationRule;
import org.tandemframework.shared.commonbase.base.util.SimpleNumberGenerationRule;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unisession.entity.document.SessionListDocument;

import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author iolshvang
 * @since 05.05.2011
 */
public class SessionListDocumentDAO extends UniBaseDao implements ISessionListDocumentDAO
{
    @Override
    public INumberGenerationRule<SessionListDocument> getNumberGenerationRule()
    {
        // реализована сквозная (по всму вузу) нумерация хвостовок в рамках года
        return new SimpleNumberGenerationRule<SessionListDocument>()
        {
            @Override
            public Set<String> getUsedNumbers(final SessionListDocument listDocument)
            {
                final List<String> list = new DQLSelectBuilder()
                .fromEntity(ClassUtils.getUserClass(listDocument), "sld")
                .column(property(SessionListDocument.number().fromAlias("sld")))
                .where(eq(DQLExpressions.property(SessionListDocument.orgUnit().fromAlias("sld")), value(listDocument.getOrgUnit())))
                .where(like(property(SessionListDocument.number().fromAlias("sld")), value(listDocument.getNumberPrefix() + "%")))
                .predicate(DQLPredicateType.distinct)
                .createStatement(SessionListDocumentDAO.this.getSession()).list();

                final int prefixLength = listDocument.getNumberPrefix().length();

                return new HashSet<>(Collections2.transform(list, input -> input.substring(prefixLength, input.length())));
            }

            @Override
            public String getNumberQueueName(final SessionListDocument listDocument)
            {
                final Calendar formingDate = Calendar.getInstance();
                formingDate.setTime(listDocument.getFormingDate());
                final int year = formingDate.get(Calendar.YEAR);

                return ClassUtils.getUserClass(listDocument).getSimpleName() + "." + String.valueOf(year) + "." + listDocument.getOrgUnit().getId();
            }
        };
    }
}
