/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionTransfer.ui.InsideAddEdit;

import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.base.bo.SessionTransfer.logic.ISessionTransferDao;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.document.SessionProjectTheme;
import ru.tandemservice.unisession.entity.document.SessionTransferInsideOperation;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.util.selectModel.SessionTermModel;

/**
 * @author oleyba
 * @since 10/19/11
 */
public class SessionTransferInOpWrapper implements ISessionTransferDao.SessionTransferInsideOperationInfo
{
    private int id;

    private SessionTermModel.TermWrapper sourceTerm;
    private EppStudentWpeCAction sourceEppSlot;
    private SessionMark sourceMark;

    private SessionTermModel.TermWrapper term;
    private EppStudentWpeCAction eppSlot;
    private SessionMarkCatalogItem mark;
    private Double rating;

    private String comment;

	private String theme;
	private boolean themeExistForDocSlot;

    public SessionTransferInOpWrapper()
    {
        this.id = System.identityHashCode(this);
		themeExistForDocSlot = false;
    }

    public SessionTransferInOpWrapper(SessionTransferInsideOperation operation, SessionProjectTheme theme)
    {
        this();
        setSourceTerm(new SessionTermModel.TermWrapper(operation.getSourceMark().getSlot()));
        setSourceEppSlot(operation.getSourceMark().getSlot().getStudentWpeCAction());
        setSourceMark(operation.getSourceMark());
        setRating(operation.getTargetMark().getPoints());
        setTerm(new SessionTermModel.TermWrapper(operation.getTargetMark().getSlot()));
        setEppSlot(operation.getTargetMark().getSlot().getStudentWpeCAction());
        setMark(operation.getTargetMark().getValueItem());
        setComment(operation.getComment());

		themeExistForDocSlot = theme != null;
		setTheme(themeExistForDocSlot ? theme.getTheme() : null);
    }

    public SessionTransferInOpWrapper(SessionTransferInOpWrapper other)
    {
        this();
        setSourceTerm(other.getSourceTerm());
        setSourceEppSlot(other.getSourceEppSlot());
        setSourceMark(other.getSourceMark());
        setRating(other.getRating());
        setTerm(other.getTerm());
        setEppSlot(other.getEppSlot());
        setMark(other.getMark());
        setComment(other.getComment());
		themeExistForDocSlot = other.themeExistForDocSlot;
		setTheme(other.getTheme());
    }

    @Override
    public int hashCode()
    {
        return getId();
    }

    @Override
    public boolean equals(Object obj)
    {
        if (null == obj || !SessionTransferInOpWrapper.class.isAssignableFrom(obj.getClass()))
            return false;
        return getId() == ((SessionTransferInOpWrapper) obj).getId();
    }

    public int getId()
    {
        return id;
    }

    public SessionTermModel.TermWrapper getSourceTerm() {
        return sourceTerm;
    }

    public void setSourceTerm(SessionTermModel.TermWrapper sourceTerm) {
        this.sourceTerm = sourceTerm;
    }

    public EppStudentWpeCAction getSourceEppSlot() {
        return sourceEppSlot;
    }

    public void setSourceEppSlot(EppStudentWpeCAction sourceEppSlot) {
        this.sourceEppSlot = sourceEppSlot;
    }

    public SessionMark getSourceMark() {
        return sourceMark;
    }

    public void setSourceMark(SessionMark sourceMark) {
        this.sourceMark = sourceMark;
    }

    public SessionTermModel.TermWrapper getTerm() {
        return term;
    }

    public void setTerm(SessionTermModel.TermWrapper term) {
        this.term = term;
    }

    public EppStudentWpeCAction getEppSlot() {
        return eppSlot;
    }

    public void setEppSlot(EppStudentWpeCAction eppSlot) {
        this.eppSlot = eppSlot;
    }

    public SessionMarkCatalogItem getMark() {
        return mark;
    }

    public void setMark(SessionMarkCatalogItem mark) {
        this.mark = mark;
    }

	public String getTheme()
	{
		return theme;
	}

	public void setTheme(final String theme)
	{
		this.theme = theme;
	}

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

	public boolean isThemeExistForDocSlot()
	{
		return themeExistForDocSlot;
	}
}
