/* $Id$ */
package ru.tandemservice.unisession.component.student.StudentSessionProjectThemeTab;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionObject;
import ru.tandemservice.unisession.entity.document.SessionProjectTheme;
import ru.tandemservice.unisession.entity.document.SessionStudentGradeBookDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.util.selectModel.SessionTermModel;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 6/22/11
 */
public class DAO extends UniBaseDao implements IDAO
{
    protected static final long ALL_MARKS_OPTION_ID = 0L;
    protected static final long ACTUAL_MARKS_OPTION_ID = 1L;
    protected static final long UNACTUAL_MARKS_OPTION_ID = 2L;

    @Override
    public void prepare(final Model model)
    {
        final Student student = this.get(Student.class, model.getStudent().getId());
        model.setStudent(student);
        model.setStateModel(new LazySimpleSelectModel<>(ImmutableList.of(
                new IdentifiableWrapper(DAO.ALL_MARKS_OPTION_ID, "Все"),
                new IdentifiableWrapper(DAO.ACTUAL_MARKS_OPTION_ID, "Только актуальные"),
                new IdentifiableWrapper(DAO.UNACTUAL_MARKS_OPTION_ID, "Только неактуальные"))));
        model.setTermModel(SessionTermModel.createForStudent(student, false));
        if (model.getSettings().get("state") == null) {
            model.getSettings().set("state", new IdentifiableWrapper(DAO.ACTUAL_MARKS_OPTION_ID, "Только актуальные"));
        }
    }

    @Override
    public void prepareListDataSource(final Model model)
    {
        // todo
        // показываем все слоты на курсовую работу и курсовой проект
        // кроме слотов в семестровом журнале БЕЗ оценок
        // и слотов в электронной зачетной книжке

        final DynamicListDataSource<SessionDocumentSlot> dataSource = model.getDataSource();

        final IdentifiableWrapper state = model.getSettings().get("state");
        final SessionTermModel.TermWrapper term = model.getSettings().get("term");

        final DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(SessionDocumentSlot.class, "slot")
            .joinPath(DQLJoinType.inner, SessionDocumentSlot.document().fromAlias("slot"), "doc")
            .column("slot")
            .where(eq(property(SessionDocumentSlot.actualStudent().fromAlias("slot")), value(model.getStudent())))
            .where(exists(EppFControlActionType.class,
                          EppFControlActionType.eppGroupType().s(), property("slot", SessionDocumentSlot.studentWpeCAction().type()),
                          EppFControlActionType.group().themeRequired().s(), Boolean.TRUE
            ))
            // не в зачетке
            .where(notInstanceOf("doc", SessionStudentGradeBookDocument.class))
            // или с оценкой, или не в семестровом журнале
            .joinEntity("slot", DQLJoinType.left, SessionMark.class, "mark", eq(property(SessionMark.slot().fromAlias("mark")), property("slot")))
            .where(or(isNotNull("mark.id"), notInstanceOf("doc", SessionObject.class)))
            ;

        if (null != state)
        {
            if (state.getId().equals(DAO.ACTUAL_MARKS_OPTION_ID)) {
                dql.where(isNull(SessionDocumentSlot.studentWpeCAction().removalDate().fromAlias("slot")));
            } else if (state.getId().equals(DAO.UNACTUAL_MARKS_OPTION_ID)) {
                dql.where(isNotNull(SessionDocumentSlot.studentWpeCAction().removalDate().fromAlias("slot")));
            }
        }

        if (null != term)
        {
            dql.where(eq(property(SessionDocumentSlot.studentWpeCAction().studentWpe().year().educationYear().fromAlias("slot")), value(term.getYear())));
            dql.where(eq(property(SessionDocumentSlot.studentWpeCAction().studentWpe().part().fromAlias("slot")), value(term.getPart())));
        }

        new DQLOrderDescriptionRegistry(EppStudentWpeCAction.class, "slot").applyOrder(dql, dataSource.getEntityOrder());

        UniBaseUtils.createFullPage(dataSource, dql.createStatement(this.getSession()).<SessionDocumentSlot>list());

        for (List<ViewWrapper<SessionDocumentSlot>> wrappers : Lists.partition(ViewWrapper.<SessionDocumentSlot>getPatchedList(dataSource), DQL.MAX_VALUES_ROW_NUMBER)) {

            final Map<SessionDocumentSlot, SessionMark> markMap = new HashMap<>();
            final Map<SessionDocumentSlot, String> themeMap = new HashMap<>();
            final Map<SessionDocumentSlot, LinkedHashSet<PpsEntry>> tutorsMap = new HashMap<>();

            final DQLSelectBuilder markDql = new DQLSelectBuilder()
                    .fromEntity(SessionMark.class, "mark")
                    .column("mark")
                    .fetchPath(DQLJoinType.inner, SessionMark.slot().fromAlias("mark"), "slot")
                    .where(in(property(SessionMark.slot().fromAlias("mark")), ids(wrappers)))
                    .order(property(SessionMark.id().fromAlias("mark")));

            for (final SessionMark mark : markDql.createStatement(DAO.this.getSession()).<SessionMark>list()) {
                markMap.put(mark.getSlot(), mark);
            }

            final DQLSelectBuilder ppsDQL = new DQLSelectBuilder()
                    .fromEntity(SessionComissionPps.class, "rel")
                    .column(property(SessionComissionPps.pps().fromAlias("rel")))
                    .joinEntity("rel", DQLJoinType.inner, SessionDocumentSlot.class, "slot", eq(property(SessionDocumentSlot.commission().fromAlias("slot")), property(SessionComissionPps.commission().fromAlias("rel"))))
                    .column("slot")
                    .where(in(SessionDocumentSlot.id().fromAlias("slot"), ids(wrappers)));

            for (final Object[] row : ppsDQL.createStatement(DAO.this.getSession()).<Object[]>list())
            {
                final PpsEntry pps = (PpsEntry) row[0];
                final SessionDocumentSlot slot = (SessionDocumentSlot) row[1];
                SafeMap.safeGet(tutorsMap, slot, LinkedHashSet.class).add(pps);
            }

            final DQLSelectBuilder themeDQL = new DQLSelectBuilder()
                    .fromEntity(SessionProjectTheme.class, "theme")
                    .fetchPath(DQLJoinType.inner, SessionProjectTheme.slot().fromAlias("theme"), "slot")
                    .where(in(property(SessionProjectTheme.slot().fromAlias("theme")), ids(wrappers)));

            for (final SessionProjectTheme theme : themeDQL.createStatement(DAO.this.getSession()).<SessionProjectTheme>list()) {
                themeMap.put(theme.getSlot(), theme.getTheme());
            }

            for (final ViewWrapper<SessionDocumentSlot> wrapper : wrappers)
            {
                wrapper.setViewProperty("mark", markMap.get(wrapper.getEntity()));
                wrapper.setViewProperty("tutors", tutorsMap.get(wrapper.getEntity()));
                wrapper.setViewProperty("theme", themeMap.get(wrapper.getEntity()));
            }
        }
    }
}
