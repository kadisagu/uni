package ru.tandemservice.unisession.base.bo.SessionBulletin.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;

import java.util.List;

/**
 * @author avedernikov
 * @since 08.08.2016
 */
public class SessionBulletinThemesDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
	public static final String PARAM_ROWS = "rows";

	public SessionBulletinThemesDSHandler(final String ownerId)
	{
		super(ownerId);
	}

	@Override
	protected DSOutput execute(DSInput input, ExecutionContext context)
	{
		List<SessionBulletinThemeRow> rows = context.getNotNull(PARAM_ROWS);
		return ListOutputBuilder.get(input, rows).build();
	}
}
