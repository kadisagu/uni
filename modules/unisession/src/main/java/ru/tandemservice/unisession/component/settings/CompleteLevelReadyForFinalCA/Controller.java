/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unisession.component.settings.CompleteLevelReadyForFinalCA;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.IEntityHandler;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel;

/**
 * @author iolshvang
 * @since 30.05.11 12:05
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        this.prepareDataSource(component);
        getDao().prepare(getModel(component));
    }

    @Override
    public void onRenderComponent(IBusinessComponent component)
    {
        this.prepareDataSource(component);
    }

    private void prepareDataSource(final IBusinessComponent context)
    {
        final Model model = this.getModel(context);
        final DynamicListDataSource<EppRealEduGroupCompleteLevel> dataSource = UniBaseUtils.createDataSource(context, this.getDao());
        dataSource.addColumn(new SimpleColumn("Название", EppRealEduGroupCompleteLevel.P_TITLE).setClickable(false).setOrderable(false).setTreeColumn(true));
        final Boolean isFormAnyStates = getDao().isFormAnyStates();
        final IEntityHandler disabler = entity -> Boolean.TRUE.equals(isFormAnyStates);
        dataSource.addColumn(getColumn().setDisableHandler(disabler));
        model.setDataSource(dataSource);
        model.setFormAnyStates(getDao().isFormAnyStates());
    }

    public void onClickReadyForFCA(final IBusinessComponent context)
    {
        this.getDao().doToggleReadyForFCA(context.getListenerParameter());
    }

    public ToggleColumn getColumn()
    {
        final ToggleColumn column = new ToggleColumn("Использовать", EppRealEduGroupCompleteLevel.P_READY_FOR_FINAL_CONTROL_ACTION)
        {
            @Override
            public Item getContent(final IEntity entity)
            {
                if (entity.getProperty("parent") == null)
                {
                    return null;
                }
                return super.getContent(entity);
            }
        };
        column.setListener("onClickReadyForFCA");
        return column;
    }

    public void onClickFormAnyStates(final IBusinessComponent component)
    {
        getDao().setFormAnyStates(getModel(component).isFormAnyStates());
    }
}
