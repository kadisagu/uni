/* $Id:$ */
package ru.tandemservice.unisession.attestation.bo.AttestationBulletin.logic;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Session;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.INumberGenerationRule;
import org.tandemframework.shared.commonbase.base.util.INumberQueueDAO;
import org.tandemframework.shared.commonbase.base.util.SimpleNumberGenerationRule;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.pupnag.gen.EppYearEducationProcessGen;
import ru.tandemservice.uniepp.entity.pupnag.gen.EppYearPartGen;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartModule;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModuleALoad;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4LoadTypeRow;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unisession.attestation.bo.AttestationBulletin.AttestationBulletinManager;
import ru.tandemservice.unisession.attestation.entity.SessionAttBullletinPrintVersion;
import ru.tandemservice.unisession.attestation.entity.SessionAttestation;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlot;
import ru.tandemservice.unisession.dao.document.ISessionDocumentBaseDAO;
import ru.tandemservice.unisession.entity.comission.SessionComission;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionObject;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.min;

/**
 * @author oleyba
 * @since 10/9/12
 */
public class SessionAttestationBulletinDao extends UniBaseDao implements ISessionAttestationBulletinDao
{
    @Override
    public void changeState(final SessionAttestationBulletin bulletin)
    {
        if (null == bulletin.getCloseDate()) {
            bulletin.setCloseDate(new Date());
        } else {
            bulletin.setCloseDate(null);
        }
        this.save(bulletin);
    }

    @Override
    public INumberGenerationRule<SessionAttestationBulletin> getNumberGenerationRule()
    {
        // номерация глобальная в рамках года
        return new SimpleNumberGenerationRule<SessionAttestationBulletin>() {
            @Override public Set<String> getUsedNumbers(final SessionAttestationBulletin object) {
                return new HashSet<>(
                new DQLSelectBuilder()
                .fromEntity(SessionAttestationBulletin.class, "x")
                .column(property(SessionAttestationBulletin.number().fromAlias("x")))
                .where(eq(property(SessionAttestationBulletin.attestation().sessionObject().educationYear().fromAlias("x")), value(object.getAttestation().getSessionObject().getEducationYear())))
                .predicate(DQLPredicateType.distinct)
                .createStatement(SessionAttestationBulletinDao.this.getSession()).<String>list()
                );
            }
            @Override public String getNumberQueueName(final SessionAttestationBulletin object) {
                return "SessionAttestation."+object.getAttestation().getSessionObject().getEducationYear().getIntValue();
            }
        };
    }

    @Override
    public void doGenerateBulletins(final SessionAttestation attestation, Collection<Long> eduGroupIds)
    {
        final Date now = new Date();
        final Session session = this.getSession();
        final SessionObject sessionObject = attestation.getSessionObject();
        final EppYearEducationProcess eppYear = this.getByNaturalId(new EppYearEducationProcessGen.NaturalId(sessionObject.getEducationYear()));
        final EppYearPart eppYearPart = this.getByNaturalId(new EppYearPartGen.NaturalId(eppYear, sessionObject.getYearDistributionPart()));
        final boolean ignoreCompleteLevel = ISessionDocumentBaseDAO.instance.get().isIgnoreRealEduGroupCompleteLevel();


        // список дисциплиночастей студентов
        List<Long> regElPartIds;
        if (CollectionUtils.isNotEmpty(eduGroupIds))
        {
            regElPartIds = new DQLSelectBuilder().fromEntity(EppRealEduGroup.class, "g").distinct()
                    .column(property("g", EppRealEduGroup.activityPart().id()))
                    .where(in(property("g", EppRealEduGroup.id()), eduGroupIds))
                    .createStatement(getSession()).list();
        }
        else
        {
            regElPartIds = new DQLSelectBuilder()
                    .predicate(DQLPredicateType.distinct)
                    .fromEntity(EppStudentWorkPlanElement.class, "s")
                    .column(property(EppStudentWorkPlanElement.registryElementPart().id().fromAlias("s")))
                    .where(isNull(property(EppStudentWorkPlanElement.removalDate().fromAlias("s")))) // только актуальные студенты
                    .where(eq(property(EppStudentWorkPlanElement.year().fromAlias("s")), value(eppYear))) // указанного года
                    .where(eq(property(EppStudentWorkPlanElement.part().fromAlias("s")), value(sessionObject.getYearDistributionPart()))) // указанной части года
                    .where(eq(property(EppStudentWorkPlanElement.student().educationOrgUnit().groupOrgUnit().fromAlias("s")), value(sessionObject.getOrgUnit()))) // студенты с указанного деканата (на текущий момент)
                    .createStatement(session).list();
        }

        // для каждой дисциплиночасти получаем список академ. групп из атт.ведомости и из записей студента в УГС, чтобы в дальнейшем их сравнить
        // { part.id -> group.id }
        Map<Long, Set<Long>> foundSlotGroupMap = getFoundSlot4GroupMap(attestation, regElPartIds);
        // { part.id -> { type.id -> { group.id } } }
        Map<Long, Map<Long, Set<Long>>> loadPart2GroupMap = Maps.newHashMap();

        List<Object[]> loadPartGroupList = new DQLSelectBuilder().fromEntity(EppRealEduGroup4LoadTypeRow.class, "r")
                .column(property("r", EppRealEduGroup4LoadTypeRow.studentWpePart().studentWpe().registryElementPart().id()))
                .column(property("r", EppRealEduGroup4LoadTypeRow.group().type().id()))
                .column(property("r", EppRealEduGroup4LoadTypeRow.studentWpePart().studentWpe().student().group().id()))
                .distinct()
                .where(isNull(property("r", EppRealEduGroup4LoadTypeRow.removalDate())))
                .where(eq(property("r", EppRealEduGroup4LoadTypeRow.group().summary().yearPart()), value(eppYearPart)))
                .where(in(property("r", EppRealEduGroup4LoadTypeRow.studentWpePart().studentWpe().registryElementPart().id()), regElPartIds))
                .where(eq(property("r", EppRealEduGroup4LoadTypeRow.studentWpePart().studentWpe().student().educationOrgUnit().groupOrgUnit()), value(sessionObject.getOrgUnit()))) // студенты с указанного деканата (на текущий момент)
                .where(ignoreCompleteLevel ? null : eq(property("r", EppRealEduGroup4LoadTypeRow.group().level().readyForFinalControlAction()), value(Boolean.TRUE)))
                .group(property("r", EppRealEduGroup4LoadTypeRow.studentWpePart().studentWpe().registryElementPart().id()))
                .group(property("r", EppRealEduGroup4LoadTypeRow.group().type().id()))
                .group(property("r", EppRealEduGroup4LoadTypeRow.studentWpePart().studentWpe().student().group().id()))
                .where(CollectionUtils.isEmpty(eduGroupIds) ? null : in(property("r", EppRealEduGroup4LoadTypeRow.group().id()), eduGroupIds))
                .createStatement(getSession()).list();

        for (Object[] row : loadPartGroupList)
        {
            Map<Long, Set<Long>> loadGroupMap = SafeMap.safeGet(loadPart2GroupMap, (Long) row[0], HashMap.class);
            SafeMap.safeGet(loadGroupMap, (Long) row[1], HashSet.class).add((Long)row[2]);
        }

        // для каждой дисциплино-части создаем спиисок ведомостей отдельно
        for (final Long regElPartId : regElPartIds)
        {
            Set<Long> foundSlotGroupIds = Sets.newHashSet();
            final EppRegistryElementPart regElPart = this.get(EppRegistryElementPart.class, regElPartId);

            // DEV-1446: ведомости строятся на остнове УГС-по-ВАН,
            // DEV-1446: при этом для каждой дисциплиночасти выбирается приоритетный вид нагрузки (пока что по принципу лекции < практики < лабораторные)
            final EppALoadType suitableLoadType = this.getSuitableLoadType(regElPart);
            if (null == suitableLoadType) { continue; }

            // проверяем, что для дисциплины нет открытых ведомостей в рамках данной аттестации и академической группы
            {
                final Number count = new DQLSelectBuilder()
                        .fromEntity(SessionAttestationBulletin.class, "b").column(DQLFunctions.count(property("b.id")))
                        .where(isNull(property(SessionAttestationBulletin.closeDate().fromAlias("b"))))
                        .where(eq(property(SessionAttestationBulletin.registryElementPart().fromAlias("b")), value(regElPart)))
                        .where(eq(property(SessionAttestationBulletin.attestation().fromAlias("b")), value(attestation)))
                        .createStatement(session).uniqueResult();

                if ((null != count) && (count.intValue() > 0))
                {
                    foundSlotGroupIds = foundSlotGroupMap.getOrDefault(regElPartId, Sets.newHashSet());
                    Set<Long> loadGroupIds = loadPart2GroupMap.getOrDefault(regElPartId, Maps.newHashMap()).getOrDefault(suitableLoadType.getEppGroupType().getId(), Sets.newHashSet());
                    if (CollectionUtils.isEqualCollection(foundSlotGroupIds, loadGroupIds)) continue;
                }
            }

            // DEV-1446: находим УГС, найденного типа, содержащих студентов данного деканата (на текущий момент) на указанный год и часть
            // XXX: от себя - два разных механизма поиска студентов - сначала по МСРП, затем через УГС - это может привести к разнличиям в списках

            // { group.id -> { student-epv-slot.id } }
            final Map<Long, Set<Long>> map = new HashMap<>();
            {
                final DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppRealEduGroup4LoadTypeRow.class, "r")
                .column(property(EppRealEduGroup4LoadTypeRow.group().id().fromAlias("r")))
                .column(property("wpe.id"))
                .column(property("wpe", EppStudentWorkPlanElement.student().group().id()))
                .joinPath(DQLJoinType.inner, EppRealEduGroup4LoadTypeRow.studentWpePart().studentWpe().fromAlias("r"), "wpe")
                .where(isNull(property(EppRealEduGroup4LoadTypeRow.removalDate().fromAlias("r"))))
                .where(eq(property(EppRealEduGroup4LoadTypeRow.group().type().fromAlias("r")), value(suitableLoadType.getEppGroupType())))
                .where(eq(property(EppRealEduGroup4LoadTypeRow.group().summary().yearPart().fromAlias("r")), value(eppYearPart)))
                .where(eq(property("wpe", EppStudentWorkPlanElement.registryElementPart()), value(regElPart)))
                .where(eq(property("wpe", EppStudentWorkPlanElement.student().educationOrgUnit().groupOrgUnit()), value(sessionObject.getOrgUnit()))); // студенты с указанного деканата (на текущий момент)

                if (!ignoreCompleteLevel) {
                    dql.where(eq(property(EppRealEduGroup4LoadTypeRow.group().level().readyForFinalControlAction().fromAlias("r")), value(Boolean.TRUE)));
                }
                if (CollectionUtils.isNotEmpty(eduGroupIds))
                    dql.where(in(property("r", EppRealEduGroup4LoadTypeRow.group().id()), eduGroupIds));

                for (final Object[] row: UniBaseDao.scrollRows(dql.createStatement(session)))
                {
                    if (foundSlotGroupIds.contains(row[2])) continue;
                    SafeMap.safeGet(map, (Long) row[0], HashSet.class).add((Long)row[1]);
                }
            }

            // DEV-1446: на каждую УГС формируем ведомость, включая всех студентов УГС, обучающихся на данном деканате (на текущий момент), и не включенных в атт. ведомости по данной дисциплиночасти.
            for (final Map.Entry<Long, Set<Long>> e: map.entrySet()) {

                final Set<Long> epvSlotIds = e.getValue();

                // DEV-1446: удаляем всех уже включенных студентов из данной аттестации
                epvSlotIds.removeAll(
                    new DQLSelectBuilder()
                    .fromEntity(SessionAttestationSlot.class, "s").column(property(SessionAttestationSlot.studentWpe().id().fromAlias("s")))
                    .where(eq(property(SessionAttestationSlot.bulletin().registryElementPart().fromAlias("s")), value(regElPart)))
                    .where(eq(property(SessionAttestationSlot.bulletin().attestation().fromAlias("s")), value(attestation)))
                    .createStatement(session).<Long>list()
                );

                if (epvSlotIds.isEmpty()) { continue; }

                // для каждой ведомости - своя комиссия (заполняем из УГС)
                final SessionComission commission = new SessionComission();
                session.save(commission);
                {
                    final List<Long> tutorIds = new DQLSelectBuilder()
                    .fromEntity(EppPpsCollectionItem.class, "ppsi")
                    .column(property(EppPpsCollectionItem.pps().id().fromAlias("ppsi")))
                    .where(eq(property(EppPpsCollectionItem.list().id().fromAlias("ppsi")), value(e.getKey())))
                    .createStatement(session).list();
                    for (final Long tutorId: tutorIds) {
                        session.save(new SessionComissionPps(commission, (PpsEntry)session.load(PpsEntry.class, tutorId)));
                    }
                }

                // создаем ведомость
                final SessionAttestationBulletin bulletin = new SessionAttestationBulletin(attestation, regElPart);
                bulletin.setNumber(INumberQueueDAO.instance.get().getNextNumber(bulletin));
                bulletin.setFormingDate(now);
                bulletin.setCommission(commission);
                session.save(bulletin);

                // включаем студентов в ведомость
                for (final Long relId: epvSlotIds) {
                    final EppStudentWorkPlanElement student = (EppStudentWorkPlanElement)session.load(EppStudentWorkPlanElement.class, relId);
                    session.save(new SessionAttestationSlot(bulletin, student));
                }

                SessionAttestationBulletinDao.this.logger.info("do-create-bulletin["+e.getKey()+"]");
            }
        }
    }


    /**
     * выбирает для части дисциплины вид аудиторной нагрузки (через связи с модулями) для формирования ведомостей
     * текущая реализация (DEV-1446): по приоритетам вида назгузки лекции < практики < лабораторные
     * @param regElPart - часть дисциплины
     * @return {@link ru.tandemservice.uniepp.entity.catalog.EppALoadType}.id
     */
    @Override
    public EppALoadType getSuitableLoadType(final EppRegistryElementPart regElPart) {

        final Object[] row = new DQLSelectBuilder()
        .fromEntity(EppRegistryElementPartModule.class, "repm")
        .where(eq(property(EppRegistryElementPartModule.part().fromAlias("repm")), value(regElPart)))

        .joinEntity("repm", DQLJoinType.inner, EppRegistryModuleALoad.class, "rmal", eq(
            property(EppRegistryModuleALoad.module().fromAlias("rmal")),
            property(EppRegistryElementPartModule.module().fromAlias("repm"))
        ))

        .where(gt(property(EppRegistryModuleALoad.load().fromAlias("rmal")), value(0L))) // нагрузка должна быть

        .predicate(DQLPredicateType.distinct)
        .column(property(EppRegistryModuleALoad.loadType().id().fromAlias("rmal")), "tp_id")
        .column(property(EppRegistryModuleALoad.loadType().code().fromAlias("rmal")), "tp_code")
        .order(property(EppRegistryModuleALoad.loadType().code().fromAlias("rmal"))) // вот она сортировка по приоритету (если потребуется другая сортировка, то придется дописать запрос)

        .createStatement(this.getSession()).setMaxResults(1).uniqueResult();

        if (null == row) { return null; }
        return this.get(EppALoadType.class, (Long)row[0]); // кэшируемо
    }

    @Override
    public void doUpdateAttBulletin(SessionAttestationBulletin entity, List<PpsEntry> ppsList)
    {
        List<SessionComissionPps> commissionPpsList = new DQLSelectBuilder().fromEntity(SessionComissionPps.class, "p")
        .where(eq(property(SessionComissionPps.commission().fromAlias("p")), value(entity.getCommission())))
        .createStatement(getSession()).list();

        List<Long> relationIdToDelete = new ArrayList<>();
        for (SessionComissionPps relation : commissionPpsList)
        {
            if (ppsList.contains(relation.getPps()))
                // если для ППСа уже есть релейшен, то удаляем его из списка, что бы ниже повторно не создавать для него релейшен
                ppsList.remove(relation.getPps());
            else
                // если есть релейшен для ППСа, которого нет в списке, то такой релейшен надо дропнуть
                relationIdToDelete.add(relation.getId());

        }

        if (!relationIdToDelete.isEmpty())
        {
            new DQLDeleteBuilder(SessionComissionPps.class)
            .where(in(property(SessionComissionPps.id()), relationIdToDelete))
            .createStatement(getSession()).execute();
        }

        for (PpsEntry ppsEntry : ppsList)
        {
            SessionComissionPps relationPps = new SessionComissionPps();
            relationPps.setCommission(entity.getCommission());
            relationPps.setPps(ppsEntry);
            save(relationPps);
        }

        update(entity);
    }

    @Override
    public void deleteBulletin(Long bulletinId)
    {
        // todo условие на выставленные оценки
        SessionAttestationBulletin bulletin = get(SessionAttestationBulletin.class, bulletinId);
        if (bulletin.isClosed())
            throw new ApplicationException("Невозможно удалить закрытую ведомость.");
        delete(bulletin);
    }

    @Override
    public List<EppStudentWorkPlanElement> getAttBulletinStudentList(SessionAttestationBulletin bulletin)
    {
        return new DQLSelectBuilder().fromEntity(SessionAttestationSlot.class, "b").column(property(SessionAttestationSlot.studentWpe().fromAlias("b")))
        .where(eq(property(SessionAttestationSlot.bulletin().fromAlias("b")), value(bulletin)))
        .createStatement(getSession()).list();
    }

    @Override
    public void doAddStudents(SessionAttestationBulletin bulletin, List<EppStudentWorkPlanElement> studentEpvSlotList)
    {
        List<SessionAttestationSlot> alreadyAddedSlotList = new DQLSelectBuilder().fromEntity(SessionAttestationSlot.class, "b")
        .where(eq(property(SessionAttestationSlot.bulletin().fromAlias("b")), value(bulletin)))
        .createStatement(getSession()).list();

        List<Long> slotIdToDelete = new ArrayList<>();
        for (SessionAttestationSlot slot : alreadyAddedSlotList)
        {
            if (studentEpvSlotList.contains(slot.getStudentWpe()))
                studentEpvSlotList.remove(slot.getStudentWpe());
            else
                slotIdToDelete.add(slot.getId());
        }

        if (!slotIdToDelete.isEmpty())
        {
            new DQLDeleteBuilder(SessionAttestationSlot.class)
            .where(in(property(SessionAttestationSlot.id()), slotIdToDelete))
            .createStatement(getSession()).execute();
        }

        for (EppStudentWorkPlanElement slot : studentEpvSlotList)
        {
            SessionAttestationSlot newSlot = new SessionAttestationSlot();
            newSlot.setBulletin(bulletin);
            newSlot.setStudentWpe(slot);

            save(newSlot);
        }
    }

    @Override
    public void doReopenBulletin(SessionAttestationBulletin bulletin) {

        //находим сохранненную печатную форму и удаляем ее
        SessionAttBullletinPrintVersion printVersion = get(SessionAttBullletinPrintVersion.class, SessionAttBullletinPrintVersion.L_DOC, bulletin);
        if (printVersion != null)
        {
            IUniBaseDao.instance.get().delete(printVersion);
        }
        changeState(bulletin);

    }

    @Override
    public void doPrintClosedBulletin(SessionAttestationBulletin bulletin) {

        final SessionAttBullletinPrintVersion rel = get(SessionAttBullletinPrintVersion.class, SessionAttBullletinPrintVersion.doc().id().s(), bulletin.getId());
        if (null != rel) {
            final byte[] content = rel.getContent();
            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().document(content).fileName("Ведомость.rtf"), true);
        } else {
            throw new ApplicationException("Для закрытой ведомости отсутствует сохраненная печатная форма. Чтобы сохранить ее вновь, откройте и закройте ведомость заново.");
        }
    }



    @Override
    public void doCloseBulletin(SessionAttestationBulletin bulletin) {
        if (existsEntity(SessionAttestationSlot.class,
                         SessionAttestationSlot.L_BULLETIN, bulletin,
                         SessionAttestationSlot.L_MARK, null))
        {
            throw new ApplicationException("Нельзя закрыть ведомость № " + bulletin.getNumber() + ", так как не всем студентам в ведомости выставлены оценки.");
        }

        final DQLSelectBuilder ppsDQL = new DQLSelectBuilder().fromEntity(SessionComissionPps.class, "p")
        .where(eq(property(SessionComissionPps.commission().fromAlias("p")), value(bulletin.getCommission())));
        final Number countPps = ppsDQL.createCountStatement(new DQLExecutionContext(this.getSession())).uniqueResult();
        if (countPps != null && countPps.intValue() < 1) {
            throw new ApplicationException("Нельзя закрыть ведомость № " + bulletin.getNumber() + ", так как не указан преподаватель.");
        }

        final DQLSelectBuilder performDateDQL = new DQLSelectBuilder()
        .fromEntity(SessionAttestationBulletin.class, "bull")
        .where(eq(property(SessionAttestationBulletin.id().fromAlias("bull")), value(bulletin)))
        .where(isNull(property(SessionAttestationBulletin.performDate().fromAlias("bull"))));
        final Number countPerformDate = performDateDQL.createCountStatement(new DQLExecutionContext(this.getSession())).uniqueResult();
        if (countPerformDate != null && countPerformDate.intValue() > 0) {
            throw new ApplicationException("Нельзя закрыть ведомость № " + bulletin.getNumber() + ",так как не установлена дата сдачи.");
        }

        //сохраним печатную форму
        //проверяем существование печатной формы
        SessionAttBullletinPrintVersion printVersion = get(SessionAttBullletinPrintVersion.class, SessionAttBullletinPrintVersion.L_DOC, bulletin);
        if (printVersion == null)
        {
            //если ее нет - создаем
            printVersion = new SessionAttBullletinPrintVersion();
            printVersion.setDoc(bulletin);
        }
        printVersion.setContent(AttestationBulletinManager.instance().printDao().printBulletin(bulletin.getId()));
        getSession().saveOrUpdate(printVersion);
        changeState(bulletin);
    }

    @Override
    public void doSaveManualMark(SessionAttestationBulletin bulletin, List<SessionAttestationSlot> slotList)
    {
        for (SessionAttestationSlot slot : slotList)
        {
            if (slot.getMark() != null)
                slot.setFixedRatingAsLong(0L);
            else
                slot.setFixedRatingAsLong(null);

            update(slot);
        }

        update(bulletin);
    }

    public void checkEditAllowed(long bulletinId)
    {
        DQLSelectBuilder check = new DQLSelectBuilder()
        .fromEntity(SessionAttestationBulletin.class, "b")
        .where(eq(property(SessionAttestationBulletin.id().fromAlias("b")), value(bulletinId)))
        .where(isNotNull(property(SessionAttestationBulletin.closeDate().fromAlias("b"))));
        Number checkResult = check.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult();
        if (checkResult != null && checkResult.intValue() > 0) {
            throw new ApplicationException("Ведомость закрыта, редактирование невозможно.");
        }
    }

    @Override
    public Map<Long, Set<Long>> getFoundSlot4GroupMap(SessionAttestation attestation, List<Long> regElementPartIds)
    {
        Map<Long, Set<Long>> resultMap = Maps.newHashMap();

        List<Object[]> foundSlotGroupList = new DQLSelectBuilder()
                .fromEntity(SessionAttestationSlot.class, "slot")
                .fromEntity(SessionAttestationBulletin.class, "b")
                .column(property("b", SessionAttestationBulletin.registryElementPart().id()))
                .column(property("g.id"))
                .distinct()
                .joinPath(DQLJoinType.left, SessionAttestationSlot.studentWpe().student().fromAlias("slot"), "st")
                .joinPath(DQLJoinType.left, Student.group().fromAlias("st"), "g")
                .where(eq(property("slot", SessionAttestationSlot.bulletin().id()), property("b.id")))
                .where(isNull(property("b", SessionAttestationBulletin.closeDate())))
                .where(in(property("b", SessionAttestationBulletin.registryElementPart().id()), regElementPartIds))
                .where(eq(property("b", SessionAttestationBulletin.attestation()), value(attestation)))
                .group(property("b", SessionAttestationBulletin.registryElementPart().id()))
                .group(property("g.id"))
                .createStatement(getSession()).list();

        for (Object[] row : foundSlotGroupList)
            SafeMap.safeGet(resultMap, (Long) row[0], HashSet.class).add((Long)row[1]);

        return resultMap;
    }

    @Override
    public void filterNotExistsAlreadyCreatedBulletin(String alias, DQLSelectBuilder dql, SessionAttestation attestation)
    {
        DQLSelectBuilder groupBuilder = new DQLSelectBuilder()
                .fromEntity(SessionAttestationSlot.class, "slot")
                .fromEntity(SessionAttestationBulletin.class, "b")
                .column(property("b", SessionAttestationBulletin.registryElementPart().id()), "e_id")
                .column(property("g.id"), "g_id")
                .distinct()
                .joinPath(DQLJoinType.left, SessionAttestationSlot.studentWpe().student().fromAlias("slot"), "st")
                .joinPath(DQLJoinType.left, Student.group().fromAlias("st"), "g")
                .where(eq(property("slot", SessionAttestationSlot.bulletin().id()), property("b.id")))
                .where(isNull(property("b", SessionAttestationBulletin.closeDate())))
                .where(eq(property("b", SessionAttestationBulletin.attestation()), value(attestation)))
                .group(property("b", SessionAttestationBulletin.registryElementPart().id()))
                .group(property("g.id"));

        DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(SessionAttestationSlot.class, "s")
                .where(eq(property(alias, EppRealEduGroupRow.studentWpePart().studentWpe().id()), property("s", SessionAttestationSlot.studentWpe().id())))
                .where(eq(property("s", SessionAttestationSlot.bulletin().attestation()), value(attestation)));

        DQLSelectBuilder subBuilder2 =  new DQLSelectBuilder()
                .fromDataSource(groupBuilder.buildQuery(), "gb")
                .where(eq(property(alias, EppRealEduGroupRow.group().activityPart().id()), property("gb.e_id")))
                .where(eq(property(alias, EppRealEduGroupRow.studentWpePart().studentWpe().student().group().id()), property("gb.g_id")));

        dql.where(notExists(subBuilder.buildQuery()));
        dql.where(notExists(subBuilder2.buildQuery()));
    }

    @Override
    public void filterExistsPriorityLoadType(String alias, DQLSelectBuilder dql)
    {
        dql.where(exists(
                new DQLSelectBuilder()
                        .fromDataSource(
                                new DQLSelectBuilder().fromEntity(EppRegistryElementPartModule.class, "pm").distinct()
                                        .joinEntity("pm", DQLJoinType.inner, EppRegistryModuleALoad.class, "al", eq(property("al", EppRegistryModuleALoad.module()), property("pm", EppRegistryElementPartModule.module())))
                                        .joinPath(DQLJoinType.inner, EppRegistryModuleALoad.loadType().fromAlias("al"), "load_type")
                                        .where(gt(property("al", EppRegistryModuleALoad.load()), value(0L))) // нагрузка должна быть
                                        .group(property("pm", EppRegistryElementPartModule.part()))
                                        .column(property("pm", EppRegistryElementPartModule.part().id()), "part_id")
                                        .column(min(property("load_type", EppALoadType.code())), "alt_code")
                                        .buildQuery(),
                                "ddd").distinct()
                        .joinEntity("ddd", DQLJoinType.inner, EppALoadType.class, "alt", eq(property("alt", EppALoadType.code()), property("ddd.alt_code")))
                        .where(eq(property(alias, EppRealEduGroupRow.group().activityPart().id()), property("ddd.part_id")))
                        .where(in(property(alias, EppRealEduGroupRow.group().type()), property("alt", EppALoadType.eppGroupType())))
                        .buildQuery()
        ));
    }

    @Override
    public Set<Long> getBulletinWithoutMarks(Collection<Long> ids)
    {
        HashSet<Long> documentsCanBeDeleted = Sets.newHashSet();
        DQLSelectBuilder builder = new DQLSelectBuilder().column("bul.id")
                .fromEntity(SessionAttestationBulletin.class, "bul")
                .where(in(property("bul", SessionAttestationBulletin.id()), ids))
                .where(isNull(property("bul", SessionAttestationBulletin.closeDate())))
                .where(notExists(
                        new DQLSelectBuilder()
                                .fromEntity(SessionAttestationSlot.class, "slot")
                                .where(eq(property("slot", SessionAttestationSlot.bulletin().id()), property("bul.id")))
                                .where(isNotNull(property("slot", SessionAttestationSlot.mark())))
                                .buildQuery()
                ));

        documentsCanBeDeleted.addAll(builder.createStatement(getSession()).<Long>list());
        return documentsCanBeDeleted;
    }
}
