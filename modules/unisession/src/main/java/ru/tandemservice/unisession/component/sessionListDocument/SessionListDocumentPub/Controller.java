package ru.tandemservice.unisession.component.sessionListDocument.SessionListDocumentPub;

import org.tandemframework.caf.service.impl.CAFLegacySupportService;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.formatter.RowCollectionFormatter;
import org.tandemframework.core.view.formatter.RawFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unisession.base.bo.SessionListDocument.ui.Edit.SessionListDocumentEdit;
import ru.tandemservice.unisession.base.bo.SessionListDocument.ui.Mark.SessionListDocumentMark;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.dao.document.ISessionDocumentBaseDAO;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.print.ISessionListDocumentPrintDAO;

/**
 * @author iolshvang
 * @since 13.04.2011
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        final IDAO dao = this.getDao();
        dao.prepare(model);

        final DynamicListDataSource<SessionDocumentSlot> dataSource = UniBaseUtils.createDataSource(component, dao);
        dataSource.addColumn(new SimpleColumn("Мероприятие", SessionDocumentSlot.studentWpeCAction().registryElementTitle().s()).setClickable(false).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Семестр", SessionDocumentSlot.termTitle().s()).setClickable(false).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Оценка", "markColumn").setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("В сессию", "inSessionColumn").setClickable(false).setOrderable(false));
        if (ISessionBrsDao.instance.get().getSettings().isUseCurrentRatingForListDocument())
            dataSource.addColumn(new SimpleColumn("Рейтинг", "ratingColumn", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Преподаватель", "ppsColumn").setFormatter(new RowCollectionFormatter(RawFormatter.INSTANCE)).setClickable(false).setOrderable(false));
		if (ISessionDocumentBaseDAO.instance.get().isThemeRequired(model.getCard()))
			dataSource.addColumn(new SimpleColumn("Тема", IDAO.PROP_PROJECT_THEME).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Комментарий", "markCommentColumn").setClickable(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteDiscipline", "Удалить дисциплину «{0}»?", SessionDocumentSlot.studentWpeCAction().registryElementTitle().s()).setPermissionKey("deleteDisciplineSessionListDocument"));

        model.setDataSource(dataSource);
        dao.prepareListDataSource(model);
    }

    public void onClickDeleteDiscipline(final IBusinessComponent component)
    {
        this.getDao().deleteSlot(component, this.getModel(component));
        this.onRefreshComponent(component);
    }

    public void onClickDelete(final IBusinessComponent component)
    {
        this.getDao().delete(this.getModel(component).getCard());
        this.deactivate(component);
    }

    public void onClickEdit(final IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
            SessionListDocumentEdit.class.getSimpleName(),
            new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, this.getModel(component).getCard().getId())));
    }

    public void onClickMark(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);

        CAFLegacySupportService.asDesktopRoot(SessionListDocumentMark.class.getSimpleName())
                .parameter(UIPresenter.PUBLISHER_ID, model.getCard().getId())
                .activate();
    }

    public void onClickAdd(final IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                ru.tandemservice.unisession.component.sessionListDocument.SessionListDocumentAddEdit.Model.COMPONENT_NAME,
                new ParametersMap()
                    .add("cardId", this.getModel(component).getCard().getId())
                    .add(PublisherActivator.PUBLISHER_ID_KEY, this.getModel(component).getCard().getOrgUnit().getId())));
    }

    public void onClickPrint(final IBusinessComponent component)
    {
        final RtfDocument document = ISessionListDocumentPrintDAO.instance.get().printSessionListDocument(this.getModel(component).getCard().getId());
        final byte[] content = RtfUtil.toByteArray(document);
        final Integer temporaryId = PrintReportTemporaryStorage.registerTemporaryPrintForm(content, "ExamCard.rtf");
        this.activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT, new ParametersMap().add("id", temporaryId).add("zip", Boolean.FALSE)));
    }
}
