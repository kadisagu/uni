/**
 *$Id$
 */
package ru.tandemservice.unisession.base.bo.SessionSystemAction;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unisession.base.bo.SessionSystemAction.logic.ISessionSystemActionDao;
import ru.tandemservice.unisession.base.bo.SessionSystemAction.logic.SessionSystemActionDao;

/**
 * @author Alexander Shaburov
 * @since 12.02.13
 */
@Configuration
public class SessionSystemActionManager extends BusinessObjectManager
{
    public static SessionSystemActionManager instance()
    {
        return instance(SessionSystemActionManager.class);
    }

    @Bean
    public ISessionSystemActionDao dao()
    {
        return new SessionSystemActionDao();
    }
}
