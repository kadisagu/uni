/**
 *$Id$
 */
package ru.tandemservice.unisession.base.bo.SessionIndicators.ui.StudentsWithoutSessionSlotsGroupOu;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.column.ColumnBase;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.list.column.IMergeRowIdResolver;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList.AbstractUniStudentList;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.unisession.base.bo.SessionIndicators.logic.SessionIndicatorsStudentsWithoutSessionSlotsGroupOuSearchDSHandler;
import ru.tandemservice.unisession.entity.document.SessionObject;

import java.util.List;

/**
 * @author Alexander Shaburov
 * @since 20.03.13
 */
@Configuration
public class SessionIndicatorsStudentsWithoutSessionSlotsGroupOu extends AbstractUniStudentList
{
    public static final String SESSION_OBJECT_DS = "sessionObjectDS";
    public static final String SEARCH_OPTION_DS = "searchOptionDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return  super.presenterExtPoint(
                presenterExtPointBuilder()
                        .addDataSource(selectDS(SESSION_OBJECT_DS, sessionObjectDSHandler()))
                        .addDataSource(selectDS(SEARCH_OPTION_DS, searchOptionDSHandler()))
                        .addDataSource(searchListDS(STUDENT_SEARCH_LIST_DS, studentSearchListDSColumnExtPoint(), studentSearchListDSHandler())));
    }

    @Override
    protected List<ColumnBase> customizeStudentSearchListColumns(List<ColumnBase> columnList)
    {
        removeColumns(columnList,
                CITIZENSHIP_COLUMN,
                PERSONAL_NUMBER_COLUMN,
                PERSONAL_FILE_NUMBER_COLUMN,
                FORMATIVE_ORG_UNIT_COLUMN,
                TERRITORIAL_ORG_UNIT_COLUMN,
                PRODUCTIVE_ORG_UNIT_COLUMN,
                SPECIALITY_COLUMN,
                SPECIALIZATION_COLUMN,
                QUALIFICATION_COLUMN,
                ORIENTATION_COLUMN,
                DEVELOP_FORM_COLUMN,
                DEVELOP_CONDITION_COLUMN,
                DEVELOP_PERIOD_COLUMN);

        insertColumns(columnList, textColumn(SessionIndicatorsStudentsWithoutSessionSlotsGroupOuSearchDSHandler.VIEW_PROP_BROKEN_SLOT_LIST, SessionIndicatorsStudentsWithoutSessionSlotsGroupOuSearchDSHandler.VIEW_PROP_BROKEN_SLOT_LIST)
                .formatter(UniEppUtils.NEW_LINE_FORMATTER)
                .required(true)
                .width("80")
                .create());

        return columnList;
    }

    @Bean
    public ColumnListExtPoint studentSearchListDSColumnExtPoint()
    {
        return createStudentSearchListColumnsBuilder()
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> studentSearchListDSHandler()
    {
        return new SessionIndicatorsStudentsWithoutSessionSlotsGroupOuSearchDSHandler(getName());
    }

    @Override
    protected IMergeRowIdResolver newMergeRowIdResolver()
    {
        return entity -> String.valueOf(entity.getProperty("person.id"));
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> sessionObjectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), SessionObject.class)
                .where(SessionObject.orgUnit(), SessionIndicatorsStudentsWithoutSessionSlotsGroupOuUI.PROP_ORG_UNIT)
                .where(SessionObject.educationYear().current(), true)
                .order(SessionObject.startupDate());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> searchOptionDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
                .addRecord(SessionIndicatorsStudentsWithoutSessionSlotsGroupOuSearchDSHandler.OPTION_ID_NO_BULLETINS, "не включенным в ведомость")
                .addRecord(SessionIndicatorsStudentsWithoutSessionSlotsGroupOuSearchDSHandler.OPTION_ID_NO_DOCS, "не включенным в ведомость, карточку, экз. лист")
                .addRecord(SessionIndicatorsStudentsWithoutSessionSlotsGroupOuSearchDSHandler.OPTION_ID_NO_FINAL_MARKS, "нет итоговой оценки по мероприятию");
    }
}
