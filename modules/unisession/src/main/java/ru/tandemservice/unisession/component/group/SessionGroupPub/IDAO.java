package ru.tandemservice.unisession.component.group.SessionGroupPub;

import ru.tandemservice.uni.dao.IListDataSourceDao;
import ru.tandemservice.uni.dao.IUpdateable;

public interface IDAO extends IUpdateable<Model>, IListDataSourceDao<Model>
{

}
