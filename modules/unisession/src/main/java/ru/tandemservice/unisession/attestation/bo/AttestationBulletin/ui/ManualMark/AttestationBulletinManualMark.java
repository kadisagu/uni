/**
 *$Id$
 */
package ru.tandemservice.unisession.attestation.bo.AttestationBulletin.ui.ManualMark;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlot;
import ru.tandemservice.unisession.entity.catalog.SessionAttestationMarkCatalogItem;

import java.util.List;

/**
 * @author Alexander Shaburov
 * @since 20.11.12
 */
@Configuration
public class AttestationBulletinManualMark extends BusinessComponentManager
{
    //ds
    public static final String STUDENT_SEARCH_DS = "studentSearchDS";
    public static final String MARK_DS = "markDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(STUDENT_SEARCH_DS, studentSearchDSColumns(), studentSearchDSHandler()))
                .addDataSource(selectDS(MARK_DS, markDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint studentSearchDSColumns()
    {
        return columnListExtPointBuilder(STUDENT_SEARCH_DS)
                .addColumn(textColumn("bookNumber", SessionAttestationSlot.studentWpe().student().bookNumber()))
                .addColumn(textColumn("group", SessionAttestationSlot.studentWpe().student().group().title()).visible("ui:groupColumnVisible"))
                .addColumn(textColumn("fio", SessionAttestationSlot.studentWpe().student().person().identityCard().fullFio()))
                .addColumn(blockColumn("mark").width("120px"))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> studentSearchDSHandler()
    {
        return new DefaultSearchDataSourceHandler(getName(), SessionAttestationSlot.class)
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                List<SessionAttestationSlot> slotList = context.get(AttestationBulletinManualMarkUI.PROP_SLOT_LIST);

                return ListOutputBuilder.get(input, slotList).pageable(false).build();
            }
        };
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> markDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), SessionAttestationMarkCatalogItem.class)
                .where(SessionAttestationMarkCatalogItem.scale().active(), true)
                .order(SessionAttestationMarkCatalogItem.priority())
                .filter(SessionAttestationMarkCatalogItem.title());
    }
}
