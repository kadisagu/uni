package ru.tandemservice.unisession.entity.mark.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.mark.SessionSlotMarkGradeValue;
import ru.tandemservice.unisession.entity.mark.SessionSlotRegularMark;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Оценка студента в сессии (оценка)
 *
 * Показывает, что человек был на контрольном мероприятии и получил оценку
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionSlotMarkGradeValueGen extends SessionSlotRegularMark
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.mark.SessionSlotMarkGradeValue";
    public static final String ENTITY_NAME = "sessionSlotMarkGradeValue";
    public static final int VERSION_HASH = 166621569;
    private static IEntityMeta ENTITY_META;

    public static final String L_VALUE = "value";
    public static final String P_POINTS_AS_LONG = "pointsAsLong";
    public static final String P_CHECKED_GRADE_BOOK = "checkedGradeBook";
    public static final String P_POINTS = "points";

    private SessionMarkGradeValueCatalogItem _value;     // Оценка
    private Long _pointsAsLong;     // Рейтинговый эквивалент оценки
    private boolean _checkedGradeBook = false;     // Сверена с зачеткой

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Оценка. Свойство не может быть null.
     */
    @NotNull
    public SessionMarkGradeValueCatalogItem getValue()
    {
        return _value;
    }

    /**
     * @param value Оценка. Свойство не может быть null.
     */
    public void setValue(SessionMarkGradeValueCatalogItem value)
    {
        dirty(_value, value);
        _value = value;
    }

    /**
     * Содержит эквивалент оценки в баллах, умноженный на 100 для выделения дробной части.
     * Поле не должно использоваться для ввода баллов за мероприятие, если в оценку по документу входят и другие баллы -
     * в таком случае в поле должна быть полная сумма баллов всех компонентов оценки.
     *
     * @return Рейтинговый эквивалент оценки.
     */
    public Long getPointsAsLong()
    {
        return _pointsAsLong;
    }

    /**
     * @param pointsAsLong Рейтинговый эквивалент оценки.
     */
    public void setPointsAsLong(Long pointsAsLong)
    {
        dirty(_pointsAsLong, pointsAsLong);
        _pointsAsLong = pointsAsLong;
    }

    /**
     * @return Сверена с зачеткой. Свойство не может быть null.
     */
    @NotNull
    public boolean isCheckedGradeBook()
    {
        return _checkedGradeBook;
    }

    /**
     * @param checkedGradeBook Сверена с зачеткой. Свойство не может быть null.
     */
    public void setCheckedGradeBook(boolean checkedGradeBook)
    {
        dirty(_checkedGradeBook, checkedGradeBook);
        _checkedGradeBook = checkedGradeBook;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof SessionSlotMarkGradeValueGen)
        {
            setValue(((SessionSlotMarkGradeValue)another).getValue());
            setPointsAsLong(((SessionSlotMarkGradeValue)another).getPointsAsLong());
            setCheckedGradeBook(((SessionSlotMarkGradeValue)another).isCheckedGradeBook());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionSlotMarkGradeValueGen> extends SessionSlotRegularMark.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionSlotMarkGradeValue.class;
        }

        public T newInstance()
        {
            return (T) new SessionSlotMarkGradeValue();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "value":
                    return obj.getValue();
                case "pointsAsLong":
                    return obj.getPointsAsLong();
                case "checkedGradeBook":
                    return obj.isCheckedGradeBook();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "value":
                    obj.setValue((SessionMarkGradeValueCatalogItem) value);
                    return;
                case "pointsAsLong":
                    obj.setPointsAsLong((Long) value);
                    return;
                case "checkedGradeBook":
                    obj.setCheckedGradeBook((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "value":
                        return true;
                case "pointsAsLong":
                        return true;
                case "checkedGradeBook":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "value":
                    return true;
                case "pointsAsLong":
                    return true;
                case "checkedGradeBook":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "value":
                    return SessionMarkGradeValueCatalogItem.class;
                case "pointsAsLong":
                    return Long.class;
                case "checkedGradeBook":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionSlotMarkGradeValue> _dslPath = new Path<SessionSlotMarkGradeValue>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionSlotMarkGradeValue");
    }
            

    /**
     * @return Оценка. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.mark.SessionSlotMarkGradeValue#getValue()
     */
    public static SessionMarkGradeValueCatalogItem.Path<SessionMarkGradeValueCatalogItem> value()
    {
        return _dslPath.value();
    }

    /**
     * Содержит эквивалент оценки в баллах, умноженный на 100 для выделения дробной части.
     * Поле не должно использоваться для ввода баллов за мероприятие, если в оценку по документу входят и другие баллы -
     * в таком случае в поле должна быть полная сумма баллов всех компонентов оценки.
     *
     * @return Рейтинговый эквивалент оценки.
     * @see ru.tandemservice.unisession.entity.mark.SessionSlotMarkGradeValue#getPointsAsLong()
     */
    public static PropertyPath<Long> pointsAsLong()
    {
        return _dslPath.pointsAsLong();
    }

    /**
     * @return Сверена с зачеткой. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.mark.SessionSlotMarkGradeValue#isCheckedGradeBook()
     */
    public static PropertyPath<Boolean> checkedGradeBook()
    {
        return _dslPath.checkedGradeBook();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.entity.mark.SessionSlotMarkGradeValue#getPoints()
     */
    public static SupportedPropertyPath<Double> points()
    {
        return _dslPath.points();
    }

    public static class Path<E extends SessionSlotMarkGradeValue> extends SessionSlotRegularMark.Path<E>
    {
        private SessionMarkGradeValueCatalogItem.Path<SessionMarkGradeValueCatalogItem> _value;
        private PropertyPath<Long> _pointsAsLong;
        private PropertyPath<Boolean> _checkedGradeBook;
        private SupportedPropertyPath<Double> _points;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Оценка. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.mark.SessionSlotMarkGradeValue#getValue()
     */
        public SessionMarkGradeValueCatalogItem.Path<SessionMarkGradeValueCatalogItem> value()
        {
            if(_value == null )
                _value = new SessionMarkGradeValueCatalogItem.Path<SessionMarkGradeValueCatalogItem>(L_VALUE, this);
            return _value;
        }

    /**
     * Содержит эквивалент оценки в баллах, умноженный на 100 для выделения дробной части.
     * Поле не должно использоваться для ввода баллов за мероприятие, если в оценку по документу входят и другие баллы -
     * в таком случае в поле должна быть полная сумма баллов всех компонентов оценки.
     *
     * @return Рейтинговый эквивалент оценки.
     * @see ru.tandemservice.unisession.entity.mark.SessionSlotMarkGradeValue#getPointsAsLong()
     */
        public PropertyPath<Long> pointsAsLong()
        {
            if(_pointsAsLong == null )
                _pointsAsLong = new PropertyPath<Long>(SessionSlotMarkGradeValueGen.P_POINTS_AS_LONG, this);
            return _pointsAsLong;
        }

    /**
     * @return Сверена с зачеткой. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.mark.SessionSlotMarkGradeValue#isCheckedGradeBook()
     */
        public PropertyPath<Boolean> checkedGradeBook()
        {
            if(_checkedGradeBook == null )
                _checkedGradeBook = new PropertyPath<Boolean>(SessionSlotMarkGradeValueGen.P_CHECKED_GRADE_BOOK, this);
            return _checkedGradeBook;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.entity.mark.SessionSlotMarkGradeValue#getPoints()
     */
        public SupportedPropertyPath<Double> points()
        {
            if(_points == null )
                _points = new SupportedPropertyPath<Double>(SessionSlotMarkGradeValueGen.P_POINTS, this);
            return _points;
        }

        public Class getEntityClass()
        {
            return SessionSlotMarkGradeValue.class;
        }

        public String getEntityName()
        {
            return "sessionSlotMarkGradeValue";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract Double getPoints();
}
