/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionProtocol.ui.FinalQualWorkAllEdit;


import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import ru.tandemservice.unisession.base.bo.SessionProtocol.logic.ProtocolRow;
import ru.tandemservice.unisession.base.bo.SessionProtocol.ui.AbstractAllEdit.SessionProtocolAbstractAllEditUI;
import ru.tandemservice.unisession.entity.stateFinalExam.SessionFQWProtocol;


/**
 * @author Andrey Andreev
 * @since 12.09.2016
 */
@Input({@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "bulletinId", required = true)})
public class SessionProtocolFinalQualWorkAllEditUI extends SessionProtocolAbstractAllEditUI
{
    public void onClickOnHonors()
    {
        setProtocolWithHonors(getListenerParameterAsLong(), true);
    }

    public void onClickOffHonors()
    {
        setProtocolWithHonors(getListenerParameterAsLong(), false);
    }

    protected void setProtocolWithHonors(Long id, boolean withHonor)
    {
        ProtocolRow protocol = getDataSource().<ProtocolRow>getRecords().stream()
                .filter(protocolRow -> protocolRow.getId().equals(id)).findFirst().orElse(null);

        if (protocol == null) return;

        ((SessionFQWProtocol) protocol.getProtocol()).setWithHonors(withHonor);
    }
}