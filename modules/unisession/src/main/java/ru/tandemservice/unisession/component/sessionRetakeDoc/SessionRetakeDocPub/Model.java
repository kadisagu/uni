/* $Id$ */
package ru.tandemservice.unisession.component.sessionRetakeDoc.SessionRetakeDocPub;

/**
 * @author oleyba
 * @since 6/15/11
 */

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import ru.tandemservice.unisession.entity.document.SessionRetakeDocument;

@State({
    @Bind(key= PublisherActivator.PUBLISHER_ID_KEY, binding="retakeDoc.id"),
    @Bind(key = "selectedTabId", binding = "selectedTabId")
})
public class Model
{
    public static final String COMPONENT_NAME = Model.class.getPackage().getName();

    private String _selectedTabId;

    private SessionRetakeDocument retakeDoc = new SessionRetakeDocument();
    private boolean withUnknownMarks;

	private boolean themeRequired;

    public SessionRetakeDocument getRetakeDoc()
    {
        return this.retakeDoc;
    }

    public void setRetakeDoc(final SessionRetakeDocument retakeDoc)
    {
        this.retakeDoc = retakeDoc;
    }

    public boolean isWithUnknownMarks()
    {
        return this.withUnknownMarks;
    }

    public void setWithUnknownMarks(final boolean withUnknownMarks)
    {
        this.withUnknownMarks = withUnknownMarks;
    }

    public String getSelectedTabId()
    {
        return _selectedTabId;
    }

    public void setSelectedTabId(String selectedTabId)
    {
        _selectedTabId = selectedTabId;
    }

	public boolean isThemeRequired()
	{
		return themeRequired;
	}

	public void setThemeRequired(boolean themeRequired)
	{
		this.themeRequired = themeRequired;
	}
}
