package ru.tandemservice.unisession.attestation.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.unisession.attestation.entity.SessionAttestation;
import ru.tandemservice.unisession.attestation.entity.report.SessionAttestationResultReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Отчет «Сводная ведомость по межсессионной аттестации (на академ. группу)»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionAttestationResultReportGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.attestation.entity.report.SessionAttestationResultReport";
    public static final String ENTITY_NAME = "sessionAttestationResultReport";
    public static final int VERSION_HASH = -352054550;
    private static IEntityMeta ENTITY_META;

    public static final String L_CONTENT = "content";
    public static final String P_FORMING_DATE = "formingDate";
    public static final String L_ATTESTATION = "attestation";
    public static final String P_COURSE = "course";
    public static final String P_GROUP = "group";

    private DatabaseFile _content;     // Печатная форма
    private Date _formingDate;     // Дата формирования
    private SessionAttestation _attestation;     // Аттестация
    private String _course;     // Курс
    private String _group;     // Академ. группа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     */
    @NotNull
    public DatabaseFile getContent()
    {
        return _content;
    }

    /**
     * @param content Печатная форма. Свойство не может быть null.
     */
    public void setContent(DatabaseFile content)
    {
        dirty(_content, content);
        _content = content;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата формирования. Свойство не может быть null.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    /**
     * @return Аттестация. Свойство не может быть null.
     */
    @NotNull
    public SessionAttestation getAttestation()
    {
        return _attestation;
    }

    /**
     * @param attestation Аттестация. Свойство не может быть null.
     */
    public void setAttestation(SessionAttestation attestation)
    {
        dirty(_attestation, attestation);
        _attestation = attestation;
    }

    /**
     * @return Курс.
     */
    @Length(max=255)
    public String getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс.
     */
    public void setCourse(String course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Академ. группа.
     */
    public String getGroup()
    {
        return _group;
    }

    /**
     * @param group Академ. группа.
     */
    public void setGroup(String group)
    {
        dirty(_group, group);
        _group = group;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SessionAttestationResultReportGen)
        {
            setContent(((SessionAttestationResultReport)another).getContent());
            setFormingDate(((SessionAttestationResultReport)another).getFormingDate());
            setAttestation(((SessionAttestationResultReport)another).getAttestation());
            setCourse(((SessionAttestationResultReport)another).getCourse());
            setGroup(((SessionAttestationResultReport)another).getGroup());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionAttestationResultReportGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionAttestationResultReport.class;
        }

        public T newInstance()
        {
            return (T) new SessionAttestationResultReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "content":
                    return obj.getContent();
                case "formingDate":
                    return obj.getFormingDate();
                case "attestation":
                    return obj.getAttestation();
                case "course":
                    return obj.getCourse();
                case "group":
                    return obj.getGroup();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "content":
                    obj.setContent((DatabaseFile) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
                case "attestation":
                    obj.setAttestation((SessionAttestation) value);
                    return;
                case "course":
                    obj.setCourse((String) value);
                    return;
                case "group":
                    obj.setGroup((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "content":
                        return true;
                case "formingDate":
                        return true;
                case "attestation":
                        return true;
                case "course":
                        return true;
                case "group":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "content":
                    return true;
                case "formingDate":
                    return true;
                case "attestation":
                    return true;
                case "course":
                    return true;
                case "group":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "content":
                    return DatabaseFile.class;
                case "formingDate":
                    return Date.class;
                case "attestation":
                    return SessionAttestation.class;
                case "course":
                    return String.class;
                case "group":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionAttestationResultReport> _dslPath = new Path<SessionAttestationResultReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionAttestationResultReport");
    }
            

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationResultReport#getContent()
     */
    public static DatabaseFile.Path<DatabaseFile> content()
    {
        return _dslPath.content();
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationResultReport#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * @return Аттестация. Свойство не может быть null.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationResultReport#getAttestation()
     */
    public static SessionAttestation.Path<SessionAttestation> attestation()
    {
        return _dslPath.attestation();
    }

    /**
     * @return Курс.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationResultReport#getCourse()
     */
    public static PropertyPath<String> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Академ. группа.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationResultReport#getGroup()
     */
    public static PropertyPath<String> group()
    {
        return _dslPath.group();
    }

    public static class Path<E extends SessionAttestationResultReport> extends EntityPath<E>
    {
        private DatabaseFile.Path<DatabaseFile> _content;
        private PropertyPath<Date> _formingDate;
        private SessionAttestation.Path<SessionAttestation> _attestation;
        private PropertyPath<String> _course;
        private PropertyPath<String> _group;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationResultReport#getContent()
     */
        public DatabaseFile.Path<DatabaseFile> content()
        {
            if(_content == null )
                _content = new DatabaseFile.Path<DatabaseFile>(L_CONTENT, this);
            return _content;
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationResultReport#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(SessionAttestationResultReportGen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * @return Аттестация. Свойство не может быть null.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationResultReport#getAttestation()
     */
        public SessionAttestation.Path<SessionAttestation> attestation()
        {
            if(_attestation == null )
                _attestation = new SessionAttestation.Path<SessionAttestation>(L_ATTESTATION, this);
            return _attestation;
        }

    /**
     * @return Курс.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationResultReport#getCourse()
     */
        public PropertyPath<String> course()
        {
            if(_course == null )
                _course = new PropertyPath<String>(SessionAttestationResultReportGen.P_COURSE, this);
            return _course;
        }

    /**
     * @return Академ. группа.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationResultReport#getGroup()
     */
        public PropertyPath<String> group()
        {
            if(_group == null )
                _group = new PropertyPath<String>(SessionAttestationResultReportGen.P_GROUP, this);
            return _group;
        }

        public Class getEntityClass()
        {
            return SessionAttestationResultReport.class;
        }

        public String getEntityName()
        {
            return "sessionAttestationResultReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
