/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionDocument.ui.MarkListTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.core.view.list.column.DefaultPublisherLinkResolver;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.util.Icon;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.entity.catalog.EppWorkPlanRowKind;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.base.bo.SessionDocument.ui.logic.SessionDocumentMarkListDSHandler;
import ru.tandemservice.unisession.base.bo.SessionDocument.ui.logic.SessionMarkDSHandler;
import ru.tandemservice.unisession.entity.document.SessionRetakeDocument;
import ru.tandemservice.unisession.entity.document.SessionSheetDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.Arrays;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.unisession.base.bo.SessionDocument.ui.logic.SessionDocumentMarkListDSHandler.*;

/**
 * @author Alexey Lopatin
 * @since 12.05.2015
 */
@Configuration
public class SessionDocumentMarkListTab extends BusinessComponentManager
{
    public static final String MARK_DS = "markDS";

    public static final String WORK_PLAN_ROW_KIND_DS = "workPlanRowKindDS";
    public static final String STATUS_MARK_DS = "statusMarkDS";
    public static final String SESSION_MARK_DS = "sessionMarkDS";
    public static final String GROUP_DS = "groupDS";
    public static final String CHECKED_GRADE_BOOK_DS = "checkedGradeBookDS";

    public static final IEntity MARK_EMPTY_ITEM = new IdentifiableWrapper(1L, "нет оценки");
    public static final IEntity RETAKE_ITEM = new IdentifiableWrapper(2L, "требует пересдачи");
    public static final IEntity NOT_RETAKE_ITEM = new IdentifiableWrapper(3L, "не требует пересдачи");
    public static final IEntity MARK_POSITIVE_ITEM = new IdentifiableWrapper(4L, "положительная оценка");

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(EducationCatalogsManager.instance().eduYearDSConfig())
                .addDataSource(EducationCatalogsManager.instance().yearPartDSConfig())
                .addDataSource(selectDS(WORK_PLAN_ROW_KIND_DS, workPlanRowKindDS()))
                .addDataSource(selectDS(STATUS_MARK_DS, statusMarkDS()))
                .addDataSource(selectDS(SESSION_MARK_DS, sessionMarkDS()))
                .addDataSource(UniStudentManger.instance().courseDSConfig())
                .addDataSource(selectDS(GROUP_DS, groupDS()).addColumn(Group.title().s()))
                .addDataSource(selectDS(CHECKED_GRADE_BOOK_DS, checkedGradeBookDSHandler()))
                .addDataSource(searchListDS(MARK_DS, markDS(), markDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> workPlanRowKindDS()
    {
        return new EntityComboDataSourceHandler(getName(), EppWorkPlanRowKind.class)
                .filter(EppWorkPlanRowKind.title())
                .order(EppWorkPlanRowKind.title());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> sessionMarkDS()
    {
        return new SessionMarkDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> statusMarkDS()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
                .addAll(Arrays.asList(MARK_EMPTY_ITEM, RETAKE_ITEM, NOT_RETAKE_ITEM, MARK_POSITIVE_ITEM));
    }

    @Bean
    public IDefaultComboDataSourceHandler checkedGradeBookDSHandler()
    {
        return new TwinComboDataSourceHandler(getName())
                .yesTitle("да, присутствует в зачетке")
                .noTitle("нет, не было сверки с зачеткой");
    }

    @Bean
    public IDefaultComboDataSourceHandler groupDS()
    {
        return new EntityComboDataSourceHandler(getName(), Group.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                List<Course> courseList = context.get(SessionDocumentMarkListTabUI.PARAM_COURSE_LIST);

                if (null != courseList && !courseList.isEmpty())
                    dql.where(in(property(alias, Group.course()), courseList));

                dql.where(eq(property(alias, Group.archival()), value(Boolean.FALSE)));
            }
        }
                .pageable(true)
                .order(Group.title())
                .filter(Group.title());
    }



    @Bean
    public ColumnListExtPoint markDS()
    {
        IFormatter<SessionSheetDocument> sheetFormatter = document -> document == null ? "" : "№" + document.getNumber() + (document.getDeadlineDate() == null ? "" : " (до " + DateFormatter.DEFAULT_DATE_FORMATTER.format(document.getDeadlineDate()) + ")");
        IFormatter<SessionRetakeDocument> retakeFormatter = document -> document == null ? "" : "№" + document.getNumber();

        IPublisherLinkResolver studentResolver = new DefaultPublisherLinkResolver()
        {
            @Override
            public Object getParameters(final IEntity entity)
            {
                EppStudentWorkPlanElement element = (EppStudentWorkPlanElement) entity.getProperty(PROPERTY_ELEMENT);
                return new ParametersMap()
                        .add(PublisherActivator.PUBLISHER_ID_KEY, element.getStudent().getId())
                        .add("selectedStudentTab", "studentNewSessionTab");
            }
        };
        IPublisherLinkResolver sheetResolver = new IPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                SessionSheetDocument document = (SessionSheetDocument) entity.getProperty(PROPERTY_SHEET_DOCUMENT);
                return new ParametersMap().add(UIPresenter.PUBLISHER_ID, document.getId());
            }

            @Override
            public String getComponentName(IEntity entity)
            {
                return ru.tandemservice.unisession.component.sessionSheet.SessionSheetPub.Model.class.getPackage().getName();
            }
        };
        IPublisherLinkResolver retakeResolver = new IPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                SessionRetakeDocument document = (SessionRetakeDocument) entity.getProperty(PROPERTY_RETAKE_DOCUMENT);
                return new ParametersMap().add(UIPresenter.PUBLISHER_ID, document.getId());
            }

            @Override
            public String getComponentName(IEntity entity)
            {
                return ru.tandemservice.unisession.component.sessionRetakeDoc.SessionRetakeDocPub.Model.class.getPackage().getName();
            }
        };

        return columnListExtPointBuilder(MARK_DS)
                .addColumn(checkboxColumn("select"))
                .addColumn(publisherColumn("student", EppStudentWorkPlanElement.student().fio().fromAlias(PROPERTY_ELEMENT)).publisherLinkResolver(studentResolver))
                .addColumn(textColumn("group", Group.title().fromAlias(PROPERTY_GROUP)))
                .addColumn(textColumn("formativeOrgUnit", EducationOrgUnit.formativeOrgUnit().title().fromAlias(PROPERTY_EOU)))
                .addColumn(textColumn("territorialOrgUnit", EducationOrgUnit.territorialOrgUnit().territorialTitle().fromAlias(PROPERTY_EOU)))
                .addColumn(textColumn("elhs", EducationOrgUnit.educationLevelHighSchool().fullTitle().fromAlias(PROPERTY_EOU)))
                .addColumn(textColumn("developForm", EducationOrgUnit.developForm().title().fromAlias(PROPERTY_EOU)))
                .addColumn(textColumn("developCondition", EducationOrgUnit.developCondition().title().fromAlias(PROPERTY_EOU)))
                .addColumn(textColumn("developTech", EducationOrgUnit.developTech().title().fromAlias(PROPERTY_EOU)))
                .addColumn(textColumn("developPeriod", EducationOrgUnit.developPeriod().title().fromAlias(PROPERTY_EOU)))
                .addColumn(textColumn("discipline", EppStudentWorkPlanElement.registryElementPart().titleWithNumber().fromAlias(PROPERTY_ELEMENT)))
                .addColumn(textColumn("actionType", EppStudentWpeCAction.type().title().fromAlias(PROPERTY_ACTION)))
                .addColumn(textColumn("sessionMark", SessionMark.valueTitle().fromAlias(PROPERTY_SESSION_MARK)))
                .addColumn(textColumn("totalMark", SessionMark.valueTitle().fromAlias(PROPERTY_TOTAL_MARK)))
                .addColumn(textColumn(PROPERTY_TEACHER, PROPERTY_TEACHER))
                .addColumn(toggleColumn(PROPERTY_CHECKED_GRADE_BOOK, PROPERTY_CHECKED_GRADE_BOOK).disabled(PROPERTY_CHECKED_GRADE_BOOK_DISABLED).permissionKey("ui:sec.sessionMarkListCheckedGradeBook")
                                   .toggleOnListener("onClickChangeCheckedGradeBook").toggleOnLabel("markDS.checkedGradeBook.yes")
                                   .toggleOffListener("onClickChangeCheckedGradeBook").toggleOffLabel("markDS.checkedGradeBook.no")
                )
                .addColumn(actionColumn(PROPERTY_RETAKE_COUNT, PROPERTY_RETAKE_COUNT, "onClickShowHistory").disabled("ui:disabledShowHistory").permissionKey("ui:sec.sessionMarkListPrintRetakeCount"))
                .addColumn(publisherColumn(PROPERTY_SHEET_DOCUMENT, PROPERTY_SHEET_DOCUMENT).formatter(sheetFormatter).publisherLinkResolver(sheetResolver))
                .addColumn(publisherColumn(PROPERTY_RETAKE_DOCUMENT, PROPERTY_RETAKE_DOCUMENT).formatter(retakeFormatter).publisherLinkResolver(retakeResolver))
                .addColumn(actionColumn("printSheetDocument", CommonDefines.ICON_PRINT, "onClickPrintSheetDocument").disabled("ui:disabledPrintSheetDocument").permissionKey("ui:sec.sessionMarkListPrintSheetDocument"))
                .addColumn(actionColumn("printRetakeDocument", CommonDefines.ICON_PRINT, "onClickPrintRetakeDocument").disabled("ui:disabledPrintRetakeDocument").permissionKey("ui:sec.sessionMarkListPrintRetakeDocument"))
                .addColumn(actionColumn("bulletinsForming", new Icon("add_sheet", "Сформировать ЭЛ"), "onClickOneSheetForming").permissionKey("ui:sec.sessionMarkLisSheetForming"))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> markDSHandler()
    {
        return new SessionDocumentMarkListDSHandler(getName());
    }
}