/* $Id$ */
package ru.tandemservice.unisession.component.sessionTheme.SessionProjectThemeAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;

import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;

/**
 * @author oleyba
 * @since 6/24/11
 */
@Input({ @Bind(key= PublisherActivator.PUBLISHER_ID_KEY, binding="slot.id") })
public class Model
{
    public static final String COMPONENT_NAME = Model.class.getPackage().getName();

    private SessionDocumentSlot slot = new SessionDocumentSlot();
    private String theme;
    private String comment;

    boolean initialized = false;

    public SessionDocumentSlot getSlot()
    {
        return this.slot;
    }

    public void setSlot(final SessionDocumentSlot slot)
    {
        this.slot = slot;
    }

    public String getTheme()
    {
        return this.theme;
    }

    public void setTheme(final String theme)
    {
        this.theme = theme;
    }

    public String getComment()
    {
        return this.comment;
    }

    public void setComment(final String comment)
    {
        this.comment = comment;
    }

    public boolean isInitialized()
    {
        return this.initialized;
    }

    public void setInitialized(final boolean initialized)
    {
        this.initialized = initialized;
    }
}
