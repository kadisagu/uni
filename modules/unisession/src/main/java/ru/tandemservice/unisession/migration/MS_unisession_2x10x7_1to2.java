package ru.tandemservice.unisession.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unisession_2x10x7_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.10.7")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность sessionRoleInGEC

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("sessionroleingec_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_sessionroleingec"),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("title_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("fulltitle_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("priority_p", DBType.INTEGER).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("sessionRoleInGEC");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность sessionRecommendation4FQW

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("sessionrecommendation4fqw_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_sessionrecommendation4fqw"),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("title_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("printtitle_p", DBType.createVarchar(255)).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("sessionRecommendation4FQW");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность sessionSource4FQWTheme

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("sessionsource4fqwtheme_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_sessionsource4fqwtheme"),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("title_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("printtitle_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("description_p", DBType.createVarchar(255))
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("sessionSource4FQWTheme");
        }
    }
}