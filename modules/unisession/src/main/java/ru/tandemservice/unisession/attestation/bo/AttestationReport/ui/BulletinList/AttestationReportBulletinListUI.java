/* $Id: SessionReportSummaryBulletinListUI.java 22487 2012-04-04 13:16:00Z vzhukov $ */
package ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.BulletinList;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ListResult;
import org.tandemframework.tapsupport.component.selection.SingleSelectTextModel;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.unisession.attestation.bo.AttestationReport.AttestationReportManager;
import ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.BulletinAdd.AttestationReportBulletinAdd;
import ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.BulletinAdd.AttestationReportBulletinAddUI;
import ru.tandemservice.unisession.attestation.entity.SessionAttestation;
import ru.tandemservice.unisession.attestation.entity.report.SessionAttestationBulletinReport;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 12/16/11
 */
@State
({
    @Bind(key = SessionReportManager.BIND_ORG_UNIT, binding = "ouHolder.id", required = true)
})
public class AttestationReportBulletinListUI extends UIPresenter
{
    private OrgUnitHolder ouHolder = new OrgUnitHolder();

    // actions

    @Override
    public void onComponentRefresh()
    {
        getOuHolder().refresh();
    }

    public void onSearchParamsChange()
    {
        onComponentRefresh();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(AttestationReportManager.PARAM_ORG_UNIT, getOrgUnit());
        dataSource.put(AttestationReportManager.PARAM_ATTESTATION, getSettings().get("attestation"));
        dataSource.put(AttestationReportManager.PARAM_GROUP, getSettings().get("group"));
    }

    public void onClickAddReport()
    {
        SessionAttestation attestation = getSettings().get("attestation");
        getActivationBuilder().asRegion(AttestationReportBulletinAdd.class)
            .parameter(SessionReportManager.BIND_ORG_UNIT, getOrgUnit().getId())
            .parameter(AttestationReportBulletinAddUI.BIND_ATTESTATION, attestation == null ? null : attestation.getId())
            .activate();
    }

    public void onClickPrint()
    {
        getActivationBuilder().asDesktopRoot(IUniComponents.DOWNLOAD_STORABLE_REPORT).parameter("reportId", getListenerParameterAsLong()).parameter("extension", "rtf").parameter("zip", Boolean.FALSE).activate();
    }

    public void onDeleteEntityFromList()
    {
        IUniBaseDao.instance.get().delete(getListenerParameterAsLong());
    }

    // preseneter

    public OrgUnit getOrgUnit()
    {
        return getOuHolder().getValue();
    }

    // getters and setters

    public OrgUnitHolder getOuHolder()
    {
        return ouHolder;
    }

    public CommonPostfixPermissionModelBase getSec()
    {
        return getOuHolder().getSecModel();
    }

    public SingleSelectTextModel getGroupsModel()
    {

        return new SingleSelectTextModel()
        {
            @Override
            public ListResult findValues(String filter) {
                final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SessionAttestationBulletinReport.class, "rep")
                        .column(property("rep", SessionAttestationBulletinReport.group()))
                        .order(property("rep", SessionAttestationBulletinReport.group()));

                final Object attestation = _uiSettings.get("attestation");
                if (null != attestation && attestation instanceof SessionAttestation)
                    builder.where(eq(property("rep", SessionAttestationBulletinReport.attestation()), value((SessionAttestation) attestation)));
                if (ouHolder.getValue() != null)
                    builder.where(eq(property("rep", SessionAttestationBulletinReport.attestation().sessionObject().orgUnit()), value(ouHolder.getValue())));


                if (!StringUtils.isEmpty(filter))
                    builder.where(likeUpper(property("rep", SessionAttestationBulletinReport.group()), value(CoreStringUtils.escapeLike(filter))));

                final List<String> list = DataAccessServices.dao().getList(builder);

                return new ListResult<>(list);
            }
        };

    }


    public String getViewPermissionKey(){ return getSec().getPermission("orgUnit_viewAttestationReportBulletinList"); }

    public String getAddStorableReportPermissionKey(){ return getSec().getPermission("orgUnit_addAttestationReportBulletinList"); }

    public String getDeleteStorableReportPermissionKey(){ return getSec().getPermission("orgUnit_deleteAttestationReportBulletinList"); }


}
