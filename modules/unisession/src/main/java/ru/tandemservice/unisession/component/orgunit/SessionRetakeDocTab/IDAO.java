/* $Id$ */
package ru.tandemservice.unisession.component.orgunit.SessionRetakeDocTab;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.uni.dao.IPrepareable;
import ru.tandemservice.unisession.entity.document.SessionObject;

/**
 * @author oleyba
 * @since 6/13/11
 */
public interface IDAO extends IPrepareable<Model>
{
    SessionObject getSessionObject(Model model);

    @Transactional(propagation= Propagation.SUPPORTS)
    void refreshDataSource(Model model);

    void doPrintRetakeDoc(Long id);

    void doRefreshFinalMarks();
}
