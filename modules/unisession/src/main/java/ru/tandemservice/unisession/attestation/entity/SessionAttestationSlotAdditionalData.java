package ru.tandemservice.unisession.attestation.entity;

import ru.tandemservice.unisession.attestation.entity.gen.SessionAttestationSlotAdditionalDataGen;

/**
 * Доп. данные для записи студента в атт. ведомости
 *
 * Доп. данные для записи студента в атт. ведомости - вместе с отметкой об аттестации может фиксироваться число пропусков, и т.д.
 * Для создания подкласса в проектном слое и сохранения там же.
 * В продуктовом коде предполагается, что данные могут как присутствовать, так и отсутствовать, для каждой конкретной записи.
 */
public abstract class SessionAttestationSlotAdditionalData extends SessionAttestationSlotAdditionalDataGen
{
}