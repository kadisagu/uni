/* $Id$ */
package ru.tandemservice.unisession.attestation.bo.CurrentMarkScale.ui.MarkAddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.unisession.attestation.bo.CurrentMarkScale.CurrentMarkScaleManager;
import ru.tandemservice.unisession.entity.catalog.SessionAttestationMarkCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionCurrentMarkScale;

/**
 * @author Nikolay Fedorovskih
 * @since 07.10.2014
 */
@Input({
               @Bind(key = CurrentMarkScaleMarkAddEditUI.SCALE_BIND_PARAM, binding = "scaleId", required = false),
               @Bind(key = CurrentMarkScaleMarkAddEditUI.MARK_BIND_PARAM, binding = "markId", required = false)
       })
public class CurrentMarkScaleMarkAddEditUI extends UIPresenter
{
    public static final String SCALE_BIND_PARAM = "scale";
    public static final String MARK_BIND_PARAM = "mark";

    private Long _scaleId;
    private Long _markId;
    private SessionAttestationMarkCatalogItem _mark;
    private boolean _edit;

    private CommonPostfixPermissionModel _sec;

    @Override
    public void onComponentRefresh()
    {
        _edit = _markId != null;
        if (_edit)
        {
            _mark = CurrentMarkScaleManager.instance().dao().getNotNull(SessionAttestationMarkCatalogItem.class, _markId);
        }
        else
        {
            _mark = new SessionAttestationMarkCatalogItem();
            _mark.setScale(CurrentMarkScaleManager.instance().dao().getNotNull(SessionCurrentMarkScale.class, _scaleId));
        }

        _sec = new CommonPostfixPermissionModel(SessionCurrentMarkScale.ENTITY_NAME)
        {
            @Override
            public String getPermission(String prefix)
            {
                return "catalogItem_" + super.getPermission(prefix);
            }
        };
    }

    public Long getScaleId()
    {
        return _scaleId;
    }

    public void setScaleId(Long scaleId)
    {
        _scaleId = scaleId;
    }

    public Long getMarkId()
    {
        return _markId;
    }

    public void setMarkId(Long markId)
    {
        _markId = markId;
    }

    public SessionAttestationMarkCatalogItem getMark()
    {
        return _mark;
    }

    public boolean isEdit()
    {
        return _edit;
    }

    public CommonPostfixPermissionModel getSec()
    {
        return _sec;
    }

    public void onClickApply()
    {
        if (!isEdit())
            CurrentMarkScaleManager.instance().dao().saveNewMark(getMark());
        else
            CurrentMarkScaleManager.instance().dao().update(getMark());
        deactivate();
    }
}