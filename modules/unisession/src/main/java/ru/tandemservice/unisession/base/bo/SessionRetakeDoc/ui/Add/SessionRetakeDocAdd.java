package ru.tandemservice.unisession.base.bo.SessionRetakeDoc.ui.Add;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.BaseRawFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4ActionType;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4ActionTypeRow;
import ru.tandemservice.unisession.attestation.bo.Attestation.AttestationManager;
import ru.tandemservice.unisession.base.bo.SessionDocument.logic.SessionRealEduGroupRowHandler;
import ru.tandemservice.unisession.base.bo.SessionDocument.util.SessionRealEduGroupFilterAddon;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.dao.retakeDoc.ISessionRetakeDocDAO;
import ru.tandemservice.unisession.dao.sessionObject.ISessionObjectDAO;
import ru.tandemservice.unisession.entity.document.SessionObject;

import static ru.tandemservice.unisession.base.bo.SessionDocument.logic.SessionRealEduGroupRowHandler.*;

/**
 * @author avedernikov
 * @since 08.02.2016
 */
@Configuration
public class SessionRetakeDocAdd extends BusinessComponentManager
{
	public static final String EDU_GROUP_FILTER_ADDON = CommonFilterAddon.class.getSimpleName();

	public static final String EDU_GROUP_DS = "eduGroupDS";

	@Bean
	@Override
	public PresenterExtPoint presenterExtPoint()
	{
		return this.presenterExtPointBuilder()
				.addDataSource(SessionReportManager.instance().eduYearDSConfig())
				.addDataSource(SessionReportManager.instance().yearPartDSConfig())
				.addAddon(uiAddon(EDU_GROUP_FILTER_ADDON, SessionRealEduGroupFilterAddon.class))
				.addDataSource(searchListDS(EDU_GROUP_DS, eduGroupDSColumn(), eduGroupDSHandler()))
				.create();
	}

	@Bean
	public ColumnListExtPoint eduGroupDSColumn()
	{
		final BaseRawFormatter<String> textFormatter = UniEppUtils.NEW_LINE_FORMATTER;
		return columnListExtPointBuilder(EDU_GROUP_DS)
				.addColumn(checkboxColumn(SessionRetakeDocAddUI.SELECT_COLUMN).disabled(SessionRealEduGroupRowHandler.PROPERTY_CHECK_DISABLED).hint("ui:checkBoxHint"))
				.addColumn(textColumn(PROPERTY_COURSE, PROPERTY_COURSE).formatter(textFormatter))
				.addColumn(textColumn(PROPERTY_GROUP, PROPERTY_GROUP).formatter(textFormatter))
				.addColumn(textColumn(EppRealEduGroup.L_ACTIVITY_PART, EppRealEduGroup4ActionType.activityPart().titleWithNumber()))
				.addColumn(textColumn(EppRealEduGroup.L_TYPE, EppRealEduGroup4ActionType.type().title()))
				.addColumn(textColumn(PROPERTY_PPS, PROPERTY_PPS).formatter(textFormatter))
				.addColumn(textColumn(PROPERTY_OWNER, PROPERTY_OWNER).formatter(textFormatter))
				.create();
	}

	@Bean
	public IReadAggregateHandler<DSInput, DSOutput> eduGroupDSHandler()
	{
		return new SessionRealEduGroupRowHandler(getName(), EppRealEduGroup4ActionType.class, EppRealEduGroup4ActionTypeRow.class)
		{
			@Override
			public DQLSelectBuilder createBuilder(String alias, ExecutionContext context)
			{
				OrgUnit orgUnit = context.get(AttestationManager.PARAM_ORG_UNIT);
				EducationYear eduYear = context.get(AttestationManager.PARAM_EDU_YEAR);
				YearDistributionPart yearPart = context.get(AttestationManager.PARAM_YEAR_PART);
				SessionObject sessionObject = ISessionObjectDAO.instance.get().getSessionObject(orgUnit, eduYear, yearPart);

				DQLSelectBuilder dql = super.createBuilder(alias, context);
				ISessionRetakeDocDAO.instance.get().filterGroupsNeedRetake(dql, alias, sessionObject);
				return dql;
			}
		};
	}
}