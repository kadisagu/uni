package ru.tandemservice.unisession.entity.document.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionProjectTheme;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Тема работы студента в сессии
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionProjectThemeGen extends EntityBase
 implements INaturalIdentifiable<SessionProjectThemeGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.document.SessionProjectTheme";
    public static final String ENTITY_NAME = "sessionProjectTheme";
    public static final int VERSION_HASH = 1958946862;
    private static IEntityMeta ENTITY_META;

    public static final String L_SLOT = "slot";
    public static final String P_THEME = "theme";
    public static final String P_COMMENT = "comment";
    public static final String L_SUPERVISOR = "supervisor";

    private SessionDocumentSlot _slot;     // Строка в документе сессии
    private String _theme;     // Тема
    private String _comment;     // Комментарий
    private PpsEntry _supervisor;     // Руководитель

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Строка в документе сессии. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public SessionDocumentSlot getSlot()
    {
        return _slot;
    }

    /**
     * @param slot Строка в документе сессии. Свойство не может быть null и должно быть уникальным.
     */
    public void setSlot(SessionDocumentSlot slot)
    {
        dirty(_slot, slot);
        _slot = slot;
    }

    /**
     * @return Тема.
     */
    public String getTheme()
    {
        return _theme;
    }

    /**
     * @param theme Тема.
     */
    public void setTheme(String theme)
    {
        dirty(_theme, theme);
        _theme = theme;
    }

    /**
     * @return Комментарий.
     */
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    /**
     * @return Руководитель.
     */
    public PpsEntry getSupervisor()
    {
        return _supervisor;
    }

    /**
     * @param supervisor Руководитель.
     */
    public void setSupervisor(PpsEntry supervisor)
    {
        dirty(_supervisor, supervisor);
        _supervisor = supervisor;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SessionProjectThemeGen)
        {
            if (withNaturalIdProperties)
            {
                setSlot(((SessionProjectTheme)another).getSlot());
            }
            setTheme(((SessionProjectTheme)another).getTheme());
            setComment(((SessionProjectTheme)another).getComment());
            setSupervisor(((SessionProjectTheme)another).getSupervisor());
        }
    }

    public INaturalId<SessionProjectThemeGen> getNaturalId()
    {
        return new NaturalId(getSlot());
    }

    public static class NaturalId extends NaturalIdBase<SessionProjectThemeGen>
    {
        private static final String PROXY_NAME = "SessionProjectThemeNaturalProxy";

        private Long _slot;

        public NaturalId()
        {}

        public NaturalId(SessionDocumentSlot slot)
        {
            _slot = ((IEntity) slot).getId();
        }

        public Long getSlot()
        {
            return _slot;
        }

        public void setSlot(Long slot)
        {
            _slot = slot;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof SessionProjectThemeGen.NaturalId) ) return false;

            SessionProjectThemeGen.NaturalId that = (NaturalId) o;

            if( !equals(getSlot(), that.getSlot()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getSlot());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getSlot());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionProjectThemeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionProjectTheme.class;
        }

        public T newInstance()
        {
            return (T) new SessionProjectTheme();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "slot":
                    return obj.getSlot();
                case "theme":
                    return obj.getTheme();
                case "comment":
                    return obj.getComment();
                case "supervisor":
                    return obj.getSupervisor();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "slot":
                    obj.setSlot((SessionDocumentSlot) value);
                    return;
                case "theme":
                    obj.setTheme((String) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
                case "supervisor":
                    obj.setSupervisor((PpsEntry) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "slot":
                        return true;
                case "theme":
                        return true;
                case "comment":
                        return true;
                case "supervisor":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "slot":
                    return true;
                case "theme":
                    return true;
                case "comment":
                    return true;
                case "supervisor":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "slot":
                    return SessionDocumentSlot.class;
                case "theme":
                    return String.class;
                case "comment":
                    return String.class;
                case "supervisor":
                    return PpsEntry.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionProjectTheme> _dslPath = new Path<SessionProjectTheme>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionProjectTheme");
    }
            

    /**
     * @return Строка в документе сессии. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unisession.entity.document.SessionProjectTheme#getSlot()
     */
    public static SessionDocumentSlot.Path<SessionDocumentSlot> slot()
    {
        return _dslPath.slot();
    }

    /**
     * @return Тема.
     * @see ru.tandemservice.unisession.entity.document.SessionProjectTheme#getTheme()
     */
    public static PropertyPath<String> theme()
    {
        return _dslPath.theme();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unisession.entity.document.SessionProjectTheme#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * @return Руководитель.
     * @see ru.tandemservice.unisession.entity.document.SessionProjectTheme#getSupervisor()
     */
    public static PpsEntry.Path<PpsEntry> supervisor()
    {
        return _dslPath.supervisor();
    }

    public static class Path<E extends SessionProjectTheme> extends EntityPath<E>
    {
        private SessionDocumentSlot.Path<SessionDocumentSlot> _slot;
        private PropertyPath<String> _theme;
        private PropertyPath<String> _comment;
        private PpsEntry.Path<PpsEntry> _supervisor;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Строка в документе сессии. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unisession.entity.document.SessionProjectTheme#getSlot()
     */
        public SessionDocumentSlot.Path<SessionDocumentSlot> slot()
        {
            if(_slot == null )
                _slot = new SessionDocumentSlot.Path<SessionDocumentSlot>(L_SLOT, this);
            return _slot;
        }

    /**
     * @return Тема.
     * @see ru.tandemservice.unisession.entity.document.SessionProjectTheme#getTheme()
     */
        public PropertyPath<String> theme()
        {
            if(_theme == null )
                _theme = new PropertyPath<String>(SessionProjectThemeGen.P_THEME, this);
            return _theme;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unisession.entity.document.SessionProjectTheme#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(SessionProjectThemeGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * @return Руководитель.
     * @see ru.tandemservice.unisession.entity.document.SessionProjectTheme#getSupervisor()
     */
        public PpsEntry.Path<PpsEntry> supervisor()
        {
            if(_supervisor == null )
                _supervisor = new PpsEntry.Path<PpsEntry>(L_SUPERVISOR, this);
            return _supervisor;
        }

        public Class getEntityClass()
        {
            return SessionProjectTheme.class;
        }

        public String getEntityName()
        {
            return "sessionProjectTheme";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
