package ru.tandemservice.unisession.base.bo.SessionBulletin.logic;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;

/**
 * Строка, содержащая информацию о теме работы студента в экзаменационной ведомости - саму тему и руководителя.
 * Используется в БК редактирования тем для ввода данных в пределах SearchList-а (элементы ввода в блок-колонках списка забиндены на проперти этой строки).
 * @author avedernikov
 * @since 08.08.2016
 */
public class SessionBulletinThemeRow extends DataWrapper
{
	private final SessionDocumentSlot slot;
	private String theme;
	private PpsEntry supervisor;

	public SessionBulletinThemeRow(final SessionDocumentSlot slot, final String theme, final PpsEntry supervisor)
	{
		super(slot);
		this.slot = slot;
		this.theme = theme;
		this.supervisor = supervisor;
	}

	public SessionDocumentSlot getSlot()
	{
		return slot;
	}

	public String getTheme()
	{
		return theme;
	}

	public void setTheme(final String theme)
	{
		this.theme = theme;
	}

	public PpsEntry getSupervisor()
	{
		return supervisor;
	}

	public void setSupervisor(final PpsEntry supervisor)
	{
		this.supervisor = supervisor;
	}
}
