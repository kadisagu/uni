/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionTransfer.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.pps.PpsEntryByEmployeePost;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.base.bo.SessionTransfer.ui.InsideAddEdit.SessionTransferInOpWrapper;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.comission.SessionComission;
import ru.tandemservice.unisession.entity.document.*;
import ru.tandemservice.unisession.entity.mark.SessionALRequestRow;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author oleyba
 * @since 10/19/11
 */
public interface ISessionTransferDao extends INeedPersistenceSupport
{
    /**
     * Интерфейс для передачи информации о строке внешнего перезачтения
     */
    interface SessionTransferOutsideOperationInfo
    {
        String getSourceDiscipline();

        EppFControlActionType getSourceControlAction();

        String getSourceMark();

        EppStudentWpeCAction getEppSlot();

        SessionMarkGradeValueCatalogItem getMark();

        Double getRating();

		String getTheme();

        String getComment();
    }

    /**
     * Сохраняет внешнее перезачтение
     *
     * @param document - документ, может быть вновь созданный
     * @param markDate - дата выставления оценок
     * @param rowList  - список перезачитываемых мероприятий
     */
    void saveOrUpdateOutsideTransfer(SessionTransferOutsideDocument document, Date markDate, List<? extends SessionTransferOutsideOperationInfo> rowList);

    /**
     * Удаляет перезачтение
     *
     * @param documentId идентификатор перезачтения (SessionTransferDocument), которое надо удалить
     */
    void deleteSessionTransfer(Long documentId);

    /**
     * Интерфейс для передачи информации о строке внутреннего перезачтения
     */
    interface SessionTransferInsideOperationInfo
    {
        SessionMark getSourceMark();

        EppStudentWpeCAction getEppSlot();

        SessionMarkCatalogItem getMark();

        String getTheme();

        String getComment();

        Double getRating();
    }

    /**
     * Сохраняет внутреннее перезачтение
     *
     * @param document - документ, может быть вновь созданный
     * @param markDate - дата выставления оценок
     * @param rowList  - список перезачитываемых мероприятий
     */
    void saveOrUpdateInsideTransfer(SessionTransferInsideDocument document, Date markDate, List<? extends SessionTransferInsideOperationInfo> rowList);

    /**
     * Формирует автоматическое внутреннее перезачтение неактуальных оценок студента.
     * @param studentId студент
     * @return строки перезачтения
     */
    List<SessionTransferInOpWrapper> getAutoFormInnerTransfers(Long studentId);

    /**
     * Сохраняет комиссию и список преподавателей в ней
     *
     * @param commission        Комиссия
     * @param commissionPpsList Запись в реестре ППС (на базе employeePost)
     * @return сохраненная комиссия, null - если в списке нет ни одного ППС
     */
    SessionComission saveCommission(SessionComission commission, List<PpsEntryByEmployeePost> commissionPpsList);

    /**
     * Сохраняет строки и оценки протокола по данным строк указанного заявления и их перезачитываемых оценок
     *
     * @param protocol    Протокол
     * @param requestRows Строки протокола
     */
    void saveProtocolRowAndMark(SessionTransferProtocolDocument protocol, List<SessionALRequestRow> requestRows);

    /**
     * Удаляет текущую версию РУПа и делает активной предыдущую связь студента с РУП, который указан в протоколе
     * Удаляет оценки, выставленные текущим протоколом
     * Удаляет записи студента по мероприятию для перезачитываемой оценки протокола
     *
     * @param protocol протокол
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void onDeleteWorkPlanVersion(SessionTransferProtocolDocument protocol);

    /**
     * Создание версии РУПа на основе строк протокола и РУПа, указанного в протоколе перезачтения
     *
     * @param protocol протокол
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void onCreateWorkPlanVersion(SessionTransferProtocolDocument protocol);

    /**
     * Создает записи студента по мероприятию, на основе строк протокола и выставляет по ним оценки студенту.
     *
     * @param protocol протокол
     * @param row      строка протокола (null - все строки протокола)
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void onCreateSessionDocumentSlot(SessionTransferProtocolDocument protocol, SessionTransferProtocolRow row);

    /**
     * Проверяет, есть ли в заявлении строки, которых нет в протоколе.
     *
     * @param protocol протокол
     */
    boolean isCheckProtocolRowMiss(SessionTransferProtocolDocument protocol);

    /**
     * Удаляет ссылку на ИУП из УП студента
     * Удаляет сам ИУП со всеми его перемещениями, если на ИУП не ссылается УП других студентов, удаляет ссылку на ИУП в РУП.
     *
     * @param student студент
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void onDeleteCustomEduPlan(Student student);

	/**
	 * Отображение Перезачтенное мероприятие -> тема работы в сессии.
	 * @param operations Список перезачтенных мероприятий, для которых нужно получить темы.
	 * @param <TransferOperation> Внешнее или внутреннее перезачтения.
	 * @return Перезачтенному мероприятию сопоставляется тема записи студента в документе сессии, по которому выставлена перезачтенная оценка.
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	<TransferOperation extends SessionTransferOperation> Map<TransferOperation, SessionProjectTheme> getThemesByTransferOperations(List<TransferOperation> operations);
}
