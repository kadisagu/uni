/* $Id$ */
package ru.tandemservice.unisession.brs.dao;

/**
 * @author Nikolay Fedorovskih
 * @since 20.01.2015
 */
public interface ISessionBrsSettings
{
    /**
     * Использовать рейтинг БРС при выставлении перезачтения (внутреннего или внешнего)
     */
    boolean isUseCurrentRatingForTransferDocument();

    /**
     * Обязательность рейтинга при выставлении перезачтения (внутреннего или внешнего)
     */
    boolean isCurrentRatingRequiredForTransferDocument();

    /**
     * Использовать рейтинг БРС при заполнении экзаменационной карточки
     */
    boolean isUseCurrentRatingForListDocument();

    /**
     * Обязательность рейтинга при заполнении экзаменационной карточки
     */
    boolean isCurrentRatingRequiredForListDocument();
}