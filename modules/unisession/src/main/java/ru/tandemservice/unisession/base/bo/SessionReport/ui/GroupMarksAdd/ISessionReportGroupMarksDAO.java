/* $Id:$ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.GroupMarksAdd;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.unisession.entity.report.UnisessionGroupMarksReport;

/**
 * @author oleyba
 * @since 2/21/12
 */
public interface ISessionReportGroupMarksDAO extends INeedPersistenceSupport
{
    SpringBeanCache<ISessionReportGroupMarksDAO> instance = new SpringBeanCache<>(ISessionReportGroupMarksDAO.class.getName());
    
    UnisessionGroupMarksReport createStoredReport(SessionReportGroupMarksAddUI model) throws Exception;
}
