package ru.tandemservice.unisession.entity.document;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.unisession.entity.document.gen.SessionTransferOutsideDocumentGen;

/**
 * Внешнее перезачтение
 */
public class SessionTransferOutsideDocument extends SessionTransferOutsideDocumentGen
{
    @Override
    @EntityDSLSupport(parts=SessionDocument.P_NUMBER)
    public String getTypeTitle() { return "Внешнее перезачтение №" + this.getNumber(); }

    @Override
    @EntityDSLSupport(parts=SessionDocument.P_NUMBER)
    public String getTypeShortTitle()
    {
        return "Внеш. перезачт. №" + this.getNumber();
    }
}