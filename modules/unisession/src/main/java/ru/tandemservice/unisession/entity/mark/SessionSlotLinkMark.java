package ru.tandemservice.unisession.entity.mark;

import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.mark.gen.SessionSlotLinkMarkGen;

/**
 * Оценка студента в сессию (ссылка)
 *
 * показщывает, что данная оценка является копией другой оценки
 */
public class SessionSlotLinkMark extends SessionSlotLinkMarkGen
{
    public SessionSlotLinkMark() {}

    public SessionSlotLinkMark(final SessionDocumentSlot slot, final SessionSlotRegularMark target) {
        this.setSlot(slot);
        this.setTarget(target);
    }

    @Override public SessionMarkCatalogItem getValueItem() {
        return this.getTarget().getValueItem();
    }

    @Override
    public Double getPoints() {
        return getTarget().getPoints();
    }

    @Override
    public String getDisplayableTitle()
    {
        return this.getTarget().getDisplayableTitle();
    }
}