package ru.tandemservice.unisession.base.bo.SessionMark.event;

import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.concurrent.semaphore.IThreadSemaphore;
import org.tandemframework.core.util.concurrent.semaphore.ThreadSemaphore;
import org.tandemframework.core.util.concurrent.semaphore.ThreadSemaphoreHolder;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionGlobalDocument;
import ru.tandemservice.unisession.entity.document.SessionObject;
import ru.tandemservice.unisession.entity.document.SessionTransferDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.mark.SessionSlotRegularMark;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.count;

/**
 * @author vdanilov
 */
public class SessionRegularMarkFiledAndCommissionChecker<T extends SessionSlotRegularMark> implements IDSetEventListener {

    private final Class<T> klass;
    private final Collection<String> immutableFields;

    public Class<T> getKlass() { return this.klass; }

    public SessionRegularMarkFiledAndCommissionChecker(final Class<T> klass, final String ...immutableFields) {
        super();
        this.klass = klass;
        this.immutableFields = Arrays.asList(immutableFields);
    }

    private static final ThreadSemaphoreHolder<ThreadSemaphore> _semaphoreHolder = new ThreadSemaphoreHolder<ThreadSemaphore>();
    public static IThreadSemaphore lock() {
        return new ThreadSemaphore(_semaphoreHolder);
    }

    @Override
    public void onEvent(final DSetEvent event)
    {
        if (_semaphoreHolder.hasSemaphore()) { return; }

        // проверяем, что коммиссия не пустая
        this.checkMarkCommission(event);

        // проверяем, что оценка в закрытом документе не меняется
        this.checkMarkChangeInClosedDocument(event);
    }

    private void checkMarkCommission(final DSetEvent event)
    {
        final DQLSelectBuilder marks = new DQLSelectBuilder()
        .fromEntity(this.klass, "mark").column(count("mark.id"))
        .joinPath(DQLJoinType.inner, SessionMark.slot().fromAlias("mark"), "s")

        .where(in(property("mark.id"), event.getMultitude().getInExpression()))

        // нет членов комиссии
        .joinEntity("mark", DQLJoinType.left, SessionComissionPps.class, "rel", eq(property(SessionComissionPps.commission().fromAlias("rel")), property(SessionMark.commission().fromAlias("mark"))))
        .where(isNull(property("rel.id")))

        // оценка не является перезачтением
        .joinEntity("s", DQLJoinType.left, SessionTransferDocument.class, "t", eq(property(SessionDocumentSlot.document().fromAlias("s")), property("t")))
        .where(isNull(property("t.id")))

        // оценка не в семестровом журнале
        .joinEntity("s", DQLJoinType.left, SessionGlobalDocument.class, "so", eq(property(SessionDocumentSlot.document().fromAlias("s")), property("so")))
        .where(isNull(property("so.id")));

        final Number count = marks.createCountStatement(event.getContext()).uniqueResult();
        if ((count != null) && (count.longValue() > 0)) {
            throw new ApplicationException("Нельзя сохранять оценку без указания преподавателя.", true);
        }
    }

    @SuppressWarnings("unchecked")
    private void checkMarkChangeInClosedDocument(final DSetEvent event)
    {
        final Set<String> affectedProperties = event.getMultitude().getAffectedProperties();
        final Collection<String> properties = CollectionUtils.intersection(affectedProperties, this.immutableFields);
        if (properties.isEmpty()) { return; }

        final DQLSelectBuilder marks = new DQLSelectBuilder()
        .fromEntity(this.klass, "mark").column("mark.id")
        .where(isNotNull(property(SessionSlotRegularMark.slot().document().closeDate().fromAlias("mark"))))
        .where(in(property("mark.id"), event.getMultitude().getInExpression()));

        final Number count = marks.createCountStatement(event.getContext()).uniqueResult();
        if ((count != null) && (count.longValue() > 0)) {
            throw new ApplicationException("Нельзя редактировать оценки в закрытом документе.", true);
        }
    }

}
