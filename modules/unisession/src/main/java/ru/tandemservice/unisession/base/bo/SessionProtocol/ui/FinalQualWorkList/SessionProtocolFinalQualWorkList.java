/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionProtocol.ui.FinalQualWorkList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.IColumnListExtPointBuilder;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.unisession.base.bo.SessionProtocol.SessionProtocolManager;
import ru.tandemservice.unisession.base.bo.SessionProtocol.ui.AbstractList.SessionProtocolAbstractList;
import ru.tandemservice.unisession.entity.stateFinalExam.SessionFQWProtocol;

/**
 * @author Andrey Andreev
 * @since 12.09.2016
 */
@Configuration
public class SessionProtocolFinalQualWorkList extends SessionProtocolAbstractList
{
    public static final String SEARCH_LIST_COLUMN_CONSULTANT = "consultant";
    protected static final String SEARCH_LIST_COLUMN_RECOMMENDATION = "recommendation";
    protected static final String SEARCH_LIST_COLUMN_SOURCE_4_THEME = "source4Theme";
    protected static final String SEARCH_LIST_COLUMN_WITH_HONORS = "withHonors";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(STATE_FINAL_EXAM_PROTOCOL_DS, stateFinalExamProtocolDSColumnExtPoint(),
                                            SessionProtocolManager.instance().stateFinalExamProtocolDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint stateFinalExamProtocolDSColumnExtPoint()
    {
        IColumnListExtPointBuilder builder = columnListExtPointBuilder(STATE_FINAL_EXAM_PROTOCOL_DS);
        addDefaultProtocolListColumns(builder);
        return builder.create();
    }

    @Override
    public void customProtocolListColumns(IColumnListExtPointBuilder builder)
    {
        builder.addColumn(textColumn(SEARCH_LIST_COLUMN_CONSULTANT, SessionFQWProtocol.consultant().fio().s()))
                .addColumn(textColumn(SEARCH_LIST_COLUMN_RECOMMENDATION, SessionFQWProtocol.recommendation().title().s()))
                .addColumn(textColumn(SEARCH_LIST_COLUMN_SOURCE_4_THEME, SessionFQWProtocol.source4Theme().title().s()))
                .addColumn(booleanColumn(SEARCH_LIST_COLUMN_WITH_HONORS, SessionFQWProtocol.withHonors().s()));
    }


    @Override
    public String getDeletePermissionKey()
    {
        return "deleteFQWProtocol";
    }

    @Override
    public String getPrintPermissionKey()
    {
        return "printFQWProtocol";
    }
}