package ru.tandemservice.unisession.component.orgunit.SessionObjectListTab;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.uni.dao.IPrepareable;

/**
 * @author vdanilov
 */
public interface IDAO extends IPrepareable<Model> {

    @Transactional(propagation= Propagation.SUPPORTS)
    void refreshDataSource(Model model);
}
