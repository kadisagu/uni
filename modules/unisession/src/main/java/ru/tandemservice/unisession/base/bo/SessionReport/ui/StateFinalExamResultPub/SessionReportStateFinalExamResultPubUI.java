/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.StateFinalExamResultPub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.StateFinalAttestationResultPub.SessionReportStateFinalAttestationResultPubUI;
import ru.tandemservice.unisession.entity.report.StateFinalExamResult;

/**
 * @author Andrey Andreev
 * @since 27.10.2016
 */
@State({@Bind(key = UIPresenter.PUBLISHER_ID, binding = "report.id"),
        @Bind(key = SessionReportManager.BIND_ORG_UNIT, binding = "ouHolder.id")})
public class SessionReportStateFinalExamResultPubUI extends SessionReportStateFinalAttestationResultPubUI<StateFinalExamResult>
{
    @Override
    public StateFinalExamResult initReport()
    {
        return new StateFinalExamResult();
    }

    @Override
    public String getViewPermissionKey()
    {
        return getSec().getPermission(getOuHolder().getId() != null ? "orgUnit_viewStateFinalExamResultList" : "stateFinalExamResultReport");
    }

    @Override
    public String getDeletePermissionKey()
    {
        return getSec().getPermission(getOuHolder().getId() != null ? "orgUnit_deleteStateFinalExamResult" : "deleteSessionStorableReport");
    }
}
