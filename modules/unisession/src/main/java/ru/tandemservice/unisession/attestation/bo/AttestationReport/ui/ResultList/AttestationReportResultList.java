/**
 *$Id:$
 */
package ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.ResultList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.core.view.util.Icon;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unisession.attestation.bo.Attestation.AttestationManager;
import ru.tandemservice.unisession.attestation.bo.AttestationReport.AttestationReportManager;
import ru.tandemservice.unisession.attestation.entity.SessionAttestation;
import ru.tandemservice.unisession.attestation.entity.report.SessionAttestationResultReport;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;

import java.util.Collections;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 09.11.12
 */
@Configuration
public class AttestationReportResultList extends BusinessComponentManager
{
    // selects ds
    public static final String EDUCATION_YEAR_DS = SessionReportManager.DS_EDU_YEAR;
    public static final String YEAR_DISTRIBUTION_PART_DS = SessionReportManager.DS_YEAR_PART;
    public static final String ATTESTATION_DS = AttestationReportManager.DS_ATTESTATION;

    // searchList ds
    public static final String SESS_ATT_RESULT_REPORT_LIST_DS = "sessionAttestationResultReportListDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(SessionReportManager.instance().eduYearDSConfig())
                .addDataSource(SessionReportManager.instance().yearPartDSConfig())
                .addDataSource(AttestationManager.instance().attestationDSConfig())
                .addDataSource(searchListDS(SESS_ATT_RESULT_REPORT_LIST_DS, sessionAttestationResultReportDSColumns(), sessionAttestationResultReportDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint sessionAttestationResultReportDSColumns()
    {
        return columnListExtPointBuilder(SESS_ATT_RESULT_REPORT_LIST_DS)
                .addColumn(indicatorColumn("report").defaultIndicatorItem(new IndicatorColumn.Item("report")))
                .addColumn(publisherColumn("formingDate", SessionAttestationResultReport.formingDate()).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).order())
                .addColumn(textColumn("attestation", SessionAttestationResultReport.attestation().shortTitle()))
                .addColumn(textColumn("course", SessionAttestationResultReport.course()))
                .addColumn(textColumn("group", SessionAttestationResultReport.group()))
                .addColumn(actionColumn("printReport", new Icon("printer"), "onClickPrintReport"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, new Icon("delete"), "onClickDeleteReport").permissionKey("ui:deleteStorableReportPermissionKey")
                        .alert(FormattedMessage.with().template("sessionAttestationResultReportListDS.delete.alert").parameter(SessionAttestationResultReport.formingDate().s(), DateFormatter.DATE_FORMATTER_WITH_TIME).create())
                        )
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> sessionAttestationResultReportDSHandler()
    {
        return new DefaultSearchDataSourceHandler(getName(), SessionAttestationResultReport.class)
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                OrgUnit orgUnit = context.get(SessionReportManager.PARAM_ORG_UNIT);
                EducationYear eduYear = context.get(SessionReportManager.PARAM_EDU_YEAR);
                YearDistributionPart yearPart = context.get(AttestationManager.PARAM_YEAR_PART);
                SessionAttestation attestation = context.get(AttestationReportResultListUI.PARAM_ATTESTATION);

                if (null == orgUnit) {
                    return ListOutputBuilder.get(input, Collections.emptyList()).build();
                }

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SessionAttestationResultReport.class, "r");

                if (attestation != null)
                {
                    builder.where(eq(property(SessionAttestationResultReport.attestation().fromAlias("r")), value(attestation)));
                }
                else
                {
                    builder.where(eq(property(SessionAttestationResultReport.attestation().sessionObject().orgUnit().fromAlias("r")), value(orgUnit)));
                    if (eduYear != null)
                        builder.where(eq(property(SessionAttestationResultReport.attestation().sessionObject().educationYear().fromAlias("r")), value(eduYear)));
                    if (yearPart != null)
                        builder.where(eq(property(SessionAttestationResultReport.attestation().sessionObject().yearDistributionPart().fromAlias("r")), value(yearPart)));
                }

                new DQLOrderDescriptionRegistry(SessionAttestationResultReport.class, "r").applyOrder(builder, input.getEntityOrder());

                return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build();
            }
        };
    }
}
