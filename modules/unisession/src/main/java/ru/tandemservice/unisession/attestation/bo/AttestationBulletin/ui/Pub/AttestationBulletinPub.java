/* $Id:$ */
package ru.tandemservice.unisession.attestation.bo.AttestationBulletin.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlot;
import ru.tandemservice.unisession.brs.util.BrsRatingValueFormatter;

/**
 * @author oleyba
 * @since 10/9/12
 */
@Configuration
public class AttestationBulletinPub extends BusinessComponentManager
{
    public static final String COLUMN_RATING = "rating";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
            .addDataSource(this.searchListDS("studentDS", this.studentDS(), this.studentDSHandler()))
            .create();
    }

    @Bean
    public ColumnListExtPoint studentDS() {
        final IPublisherLinkResolver resolver = new IPublisherLinkResolver() {
            @Override public Object getParameters(final IEntity entity) {
                return new ParametersMap()
                    .add(PublisherActivator.PUBLISHER_ID_KEY, entity.getProperty(SessionAttestationSlot.studentWpe().student().id().s()))
                    .add("selectedStudentTab", "studentNewSessionTab")
                    .add("selectedDataTab", "studentSessionDocumentTab")
                    ;
            }
            @Override public String getComponentName(final IEntity entity) {
                return "ru.tandemservice.uni.component.student.StudentPub";
            }
        };

        return this.columnListExtPointBuilder("studentDS")
            .addColumn(textColumn("bookNumber", SessionAttestationSlot.studentWpe().student().bookNumber().s()).order().create())
            .addColumn(textColumn("group", SessionAttestationSlot.studentWpe().student().group().title().s()).visible("ui:groupVisible").order().create())// visible(true) set in onAfterDataSourceFetch if need
            .addColumn(publisherColumn("fio", SessionAttestationSlot.studentWpe().student().person().identityCard().fullFio().s()).publisherLinkResolver(resolver).order().create())
            .addColumn(textColumn("compensationType", SessionAttestationSlot.studentWpe().student().compensationType().shortTitle().s()).create())
            .addColumn(textColumn(COLUMN_RATING, SessionAttestationSlot.fixedRating().s()).visible("ui:useRating").formatter(BrsRatingValueFormatter.instance.get()).create())
            .addColumn(textColumn("mark", SessionAttestationSlot.mark().shortTitle()))
            .addColumn(actionColumn("deleteSessionAttestationSlot", CommonDefines.ICON_DELETE, "onClickDeleteSessionAttSlot")
                    .alert(new FormattedMessage("studentDS.delete.alert", SessionAttestationSlot.studentWpe().student().person().identityCard().fullFio().s(), SessionAttestationSlot.bulletin().title().s()))
                    .permissionKey("deleteStudentSessionAttBulletin")
                    .visible("mvel:!presenter.bulletin.closed"))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> studentDSHandler() {
        return new SessionAttestationBulletinStudentsDSHandler(this.getName());
    }

}
