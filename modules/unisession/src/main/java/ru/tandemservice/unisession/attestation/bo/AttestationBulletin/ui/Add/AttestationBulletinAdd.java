/* $Id$ */
package ru.tandemservice.unisession.attestation.bo.AttestationBulletin.ui.Add;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4LoadType;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4LoadTypeRow;
import ru.tandemservice.unisession.attestation.bo.Attestation.AttestationManager;
import ru.tandemservice.unisession.attestation.bo.AttestationBulletin.AttestationBulletinManager;
import ru.tandemservice.unisession.base.bo.SessionDocument.logic.SessionRealEduGroupRowHandler;
import ru.tandemservice.unisession.attestation.entity.SessionAttestation;
import ru.tandemservice.unisession.base.bo.SessionDocument.util.SessionRealEduGroupFilterAddon;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;

import static ru.tandemservice.unisession.base.bo.SessionDocument.logic.SessionRealEduGroupRowHandler.*;

/**
 * @author Alexey Lopatin
 * @since 26.11.2015
 */
@Configuration
public class AttestationBulletinAdd extends BusinessComponentManager
{
    public static final String DS_EDU_GROUP = "eduGroupDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(SessionReportManager.instance().eduYearDSConfig())
                .addDataSource(SessionReportManager.instance().yearPartDSConfig())
                .addDataSource(AttestationManager.instance().attestationDSConfig())
                .addAddon(uiAddon(CommonFilterAddon.class.getSimpleName(), SessionRealEduGroupFilterAddon.class))
                .addDataSource(searchListDS(DS_EDU_GROUP, eduGroupDS(), eduGroupDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint eduGroupDS()
    {
        return this.columnListExtPointBuilder(DS_EDU_GROUP)
                .addColumn(checkboxColumn("check").disabled(PROPERTY_CHECK_DISABLED).hint("ui:checkBoxHint"))
                .addColumn(textColumn(PROPERTY_COURSE, PROPERTY_COURSE).formatter(UniEppUtils.NEW_LINE_FORMATTER).width("1"))
                .addColumn(textColumn(PROPERTY_GROUP, PROPERTY_GROUP).formatter(UniEppUtils.NEW_LINE_FORMATTER).width("1"))
                .addColumn(textColumn(EppRealEduGroup.L_ACTIVITY_PART, EppRealEduGroup.activityPart().titleWithNumber()).width("50"))
                .addColumn(textColumn(EppRealEduGroup.L_TYPE, EppRealEduGroup.type().title()).width("1"))
                .addColumn(textColumn(PROPERTY_PPS, PROPERTY_PPS).formatter(UniEppUtils.NEW_LINE_FORMATTER).width("20"))
                .addColumn(textColumn(PROPERTY_OWNER, PROPERTY_OWNER).formatter(UniEppUtils.NEW_LINE_FORMATTER))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> eduGroupDSHandler()
    {
        return new SessionRealEduGroupRowHandler(getName(), EppRealEduGroup4LoadType.class, EppRealEduGroup4LoadTypeRow.class)
        {
            @Override
            public DQLSelectBuilder createBuilder(String alias, ExecutionContext context)
            {
                DQLSelectBuilder dql = super.createBuilder(alias, context);
                SessionAttestation attestation = context.get(AttestationBulletinAddUI.PARAM_ATTESTATION);
                AttestationBulletinManager.instance().dao().filterNotExistsAlreadyCreatedBulletin(alias, dql, attestation);
                AttestationBulletinManager.instance().dao().filterExistsPriorityLoadType(alias, dql);
                return dql;
            }
        };
    }
}
