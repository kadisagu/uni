/* $Id:$ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.SfaSummaryResultAdd.logic;

import ru.tandemservice.unisession.entity.report.SfaSummaryResult;

/**
 * @author Andrey Andreev
 * @since 28.10.2016
 */
public interface ISfaSummaryResultPrintDAO
{
    /**
     * Формирует отчет по параметрам и добавляет его к сущности
     */
    SfaSummaryResult createStoredReport(SfaSummaryResult report);
}
