/* $Id$ */
package ru.tandemservice.unisession.print;

import com.google.common.collect.Iterables;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.base.entity.IPersistentEmployeePost;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.NumberAsStringComparator;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.DelegatePropertyComparator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeFCA;
import ru.tandemservice.uniepp.entity.catalog.codes.EppGroupTypeFCACodes;
import ru.tandemservice.uniepp.entity.registry.EppRegistryAttestation;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryPractice;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.brs.util.BrsRatingValueFormatter;
import ru.tandemservice.unisession.dao.document.ISessionDocumentBaseDAO;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkStateCatalogItem;
import ru.tandemservice.unisession.entity.catalog.UnisessionCommonTemplate;
import ru.tandemservice.unisession.entity.catalog.codes.UnisessionCommonTemplateCodes;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.*;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.mark.SessionMarkRatingData;
import ru.tandemservice.unisession.util.SessionBulletinPrintUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 6/16/11
 */
public class SessionRetakeDocPrintDAO extends UniBaseDao implements ISessionRetakeDocPrintDAO
{
    protected static final DateFormatter DATE_FORMATTER = new DateFormatter("dd.MM.yy");

    protected enum ColumnType
    {
        NUMBER, FIO, MARK, BOOK_NUMBER, GROUP, THEME, EMPTY, POINT, CURRENT_RATING, SCORED_POINTS, DATE, PPS, STUDENT_NUMBER
    }

    // Для кастомизации в проектах выделены следующие методы.
    // Пожалуйста, пользуйтесь ими.

    // дополнительные метки
    protected void printAdditionalInfo(BulletinPrintInfo printData)
    {
        // для переопределения в проектах
    }

    // переопределение содержимого базовых меток
    protected void customizeSimpleTags(final RtfInjectModifier modifier, final BulletinPrintInfo printInfo)
    {
        // для переопределения в проектах
    }

    // набор колонок для данной конкретной ведомости.
    // может содержать меньше колонок, чем шаблон, тогда лишние будут удалены
    protected List<ColumnType> prepareColumnList(final BulletinPrintInfo printData)
    {
        final Set<Group> groups = new HashSet<>();
        for (final BulletinPrintRow row : printData.getContentMap().values())
        {
            groups.add(row.getSlot().getActualStudent().getGroup());
        }
        final List<ColumnType> columns = new ArrayList<>();
        columns.addAll(Arrays.asList(
                ColumnType.NUMBER,
                ColumnType.FIO,
                ColumnType.BOOK_NUMBER));
        if (groups.size() > 1)
        {
            columns.add(ColumnType.GROUP);
        }
		if (ISessionDocumentBaseDAO.instance.get().isThemeRequired(printData.getBulletin()))
			columns.add(ColumnType.THEME);
        if (printData.isUseCurrentRating())
        {
            columns.add(ColumnType.CURRENT_RATING);
            columns.add(ColumnType.SCORED_POINTS);
            columns.add(ColumnType.POINT);
        } else if (printData.isUsePoints())
        {
            columns.add(ColumnType.POINT);
        }
        columns.addAll(Arrays.asList(
                ColumnType.MARK,
                ColumnType.DATE));
        return columns;
    }

    // набор колонок в шаблоне для ведомости. в разных вузах может отличаться
    protected List<ColumnType> getTemplateColumnList(BulletinPrintInfo printInfo)
    {
        if (printInfo.isUseCurrentRating())
        {
            return Arrays.asList(
                    ColumnType.NUMBER,
                    ColumnType.FIO,
                    ColumnType.BOOK_NUMBER,
                    ColumnType.GROUP,
					ColumnType.THEME,
                    ColumnType.CURRENT_RATING,
                    ColumnType.SCORED_POINTS,
                    ColumnType.POINT,
                    ColumnType.MARK,
                    ColumnType.DATE);
        }
        if (printInfo.isUsePoints())
        {
            return Arrays.asList(
                    ColumnType.NUMBER,
                    ColumnType.FIO,
                    ColumnType.BOOK_NUMBER,
                    ColumnType.GROUP,
					ColumnType.THEME,
                    ColumnType.POINT,
                    ColumnType.MARK,
                    ColumnType.DATE
            );
        }
        return Arrays.asList(
                ColumnType.NUMBER,
                ColumnType.FIO,
                ColumnType.BOOK_NUMBER,
                ColumnType.GROUP,
				ColumnType.THEME,
                ColumnType.MARK,
                ColumnType.DATE);
    }

    // печать строчки ведомости
    protected String[] fillTableRow(BulletinPrintRow printRow, final List<ColumnType> columns, final int rowNumber)
    {
        final List<String> result = new ArrayList<>();

        SessionMarkCatalogItem mark = printRow.getMark() == null ? null : printRow.getMark().getValueItem();
        Double points = printRow.getMark() == null ? null : printRow.getMark().getPoints();
        Double currentRating = printRow.getSlotRatingData() == null ? null : printRow.getSlotRatingData().getFixedCurrentRating();
        Double scoredPoints = printRow.getMarkRatingData() == null ? null : printRow.getMarkRatingData().getScoredPoints();

        for (final ColumnType column : columns)
        {
            switch (column)
            {
                case EMPTY:
                    result.add("");
                    break;
                case FIO:
                    result.add(printRow.getSlot().getActualStudent().getPerson().getFullFio());
                    break;
                case BOOK_NUMBER:
                    result.add(StringUtils.trimToEmpty(printRow.getSlot().getActualStudent().getBookNumber()));
                    break;
                case GROUP:
                    result.add(printRow.getSlot().getActualStudent().getGroup() == null ? "" : printRow.getSlot().getActualStudent().getGroup().getTitle());
                    break;
                case THEME:
                    result.add(printRow.getTheme() == null ? "" : StringUtils.trimToEmpty(printRow.getTheme().getTheme()));
                    break;
                case MARK:
                    result.add(mark == null ? "" : StringUtils.trimToEmpty(mark.getPrintTitle()));
                    break;
                case NUMBER:
                    result.add(String.valueOf(rowNumber));
                    break;
                case CURRENT_RATING:
                    result.add(BrsRatingValueFormatter.instance.get().format(currentRating));
                    break;
                case POINT:
                    result.add(mark instanceof SessionMarkStateCatalogItem ? "-" : BrsRatingValueFormatter.instance.get().format(points));
                    break;
                case SCORED_POINTS:
                    result.add(mark instanceof SessionMarkStateCatalogItem ? "-" : BrsRatingValueFormatter.instance.get().format(scoredPoints));
                    break;
                case DATE:
                    result.add(printRow.getMark() == null || printRow.getMark().getPerformDate() == null ? "" : DATE_FORMATTER.format(printRow.getMark().getPerformDate()));
                    break;
                case PPS:
                    result.add(UniStringUtils.join(printRow.getCommission(), PpsEntry.person().identityCard().fio().s(), ", "));
                    break;
            }
        }
        return result.toArray(new String[result.size()]);
    }

    /**
     * Метод для кастомизации в проектах
     * Позволяет переопределить определение шаблонов по ведомости
     * Чтобы ведомость печаталась по определенному шаблону,
     * нужно в BulletinPrintInfo инициализировать RtfPrintDocument на clone нужного шаблона
     * Соответственно, в переданном мапе нужно сделать именно это в значениях
     *
     * @param printInfoMap ведомость -> печатная форма с данными ведомости
     */
    protected void initTemplates(final Map<SessionRetakeDocument, BulletinPrintInfo> printInfoMap)
    {
        final RtfReader reader = new RtfReader();
        RtfDocument templateBase = reader.read(getCatalogItem(UnisessionCommonTemplate.class, UnisessionCommonTemplateCodes.RETAKE_DOC).getContent());
        RtfDocument templateWithPoints = reader.read(getCatalogItem(UnisessionCommonTemplate.class, UnisessionCommonTemplateCodes.RETAKE_DOC_WITH_POINTS).getContent());
        RtfDocument templateWithRating = reader.read(getCatalogItem(UnisessionCommonTemplate.class, UnisessionCommonTemplateCodes.RETAKE_DOC_WITH_CURRENT_RATING).getContent());

        for (final BulletinPrintInfo printInfo : printInfoMap.values())
        {
            if (printInfo.isUseCurrentRating())
                printInfo.setDocument(templateWithRating.getClone());
            else if (printInfo.isUsePoints())
                printInfo.setDocument(templateWithPoints.getClone());
            else
                printInfo.setDocument(templateBase.getClone());
        }
    }

    /**
     * Метод для кастомизации в проектах
     * Позволяет переопределить группировку и сортировку студентов в ведомости
     * При выводе строк в печатной форме они будут выводиться по группам, определенным в этом мапе
     * Если группа не одна, то перед набором студентов группы будет выводиться строка с названием группы
     * При этом строка с названием группы будет болдом и все ячейки смерджены в одну
     *
     * @param printInfo печатная форма и данные ведомости
     * @return {название группы -> отсортированный список студентов этой группы}
     */
    protected TreeMap<String, List<BulletinPrintRow>> getGroupedAndSortedContent(final BulletinPrintInfo printInfo)
    {
        final TreeSet<String> fcaTitles = new TreeSet<>();
        for (final BulletinPrintRow row : printInfo.getContentMap().values())
        {
            fcaTitles.add(row.getSlot().getStudentWpeCAction().getType().getTitle());
        }

        final TreeMap<String, List<BulletinPrintRow>> result = new TreeMap<>();
        if (fcaTitles.size() <= 1)
        {
            result.put("", new ArrayList<>(printInfo.getContentMap().values()));
            return result;
        }

        // группируем
        for (final BulletinPrintRow row : printInfo.getContentMap().values())
        {
            SafeMap.safeGet(result, row.getSlot().getStudentWpeCAction().getType().getTitle(), ArrayList.class).add(row);
        }

        return result;
    }

    // ---- методы ISessionBulletinPrintDAO

    @Override
    public byte[] printDocListToZipArchive(final Collection<Long> ids)
    {
        final Map<SessionRetakeDocument, BulletinPrintInfo> bulletinMap = this.prepareData(ids);
        if (bulletinMap.isEmpty())
        {
            throw new ApplicationException("Не выбраны ведомости для печати.");
        }

        ByteArrayOutputStream out;
        byte[] result;
        try
        {
            out = new ByteArrayOutputStream();
            final ZipOutputStream zipOut = new ZipOutputStream(out);
            for (final Map.Entry<SessionRetakeDocument, BulletinPrintInfo> entry : bulletinMap.entrySet())
            {
                SessionRetakeDocument bulletin = entry.getKey();

                byte[] content;
                if (bulletin.isClosed())
                {
                    SessionDocumentPrintVersion rel = this.get(SessionDocumentPrintVersion.class, SessionDocumentPrintVersion.doc().id().s(), bulletin.getId());
                    if (rel == null)
                        throw new ApplicationException("Отсутствует печатная форма закрытого документа сессии.");
                    content = rel.getContent();
                } else
                {
                    final RtfDocument document = this.printBulletin(entry.getValue());
                    content = RtfUtil.toByteArray(document);
                }
                zipOut.putNextEntry(new ZipEntry("Vedomost " + CoreStringUtils.transliterate(bulletin.getNumber()) + " " + bulletin.getSessionObject().getEducationYear().getTitle() + ".xls"));
                zipOut.write(content);
                zipOut.closeEntry();
            }
            zipOut.close();
            result = out.toByteArray();
            out.close();
        } catch (final IOException e)
        {
            throw new IllegalStateException(e);
        }

        return result;
    }

    @Override
    public RtfDocument printDocList(final Collection<Long> ids)
    {
        final Map<SessionRetakeDocument, BulletinPrintInfo> bulletinMap = this.prepareData(ids);
        if (bulletinMap.isEmpty())
        {
            throw new ApplicationException("Не выбраны ведомости для печати.");
        }

        RtfDocument result = null;

        for (final Map.Entry<SessionRetakeDocument, BulletinPrintInfo> entry : bulletinMap.entrySet())
        {
            SessionRetakeDocument bulletin = entry.getKey();

            if (bulletin.isClosed() && bulletinMap.size() > 1)
            {
                throw new ApplicationException("Массовая печать закрытых ведомостей невозможна. Выберите только открытые ведомости.");
            }
            final RtfDocument document = this.printBulletin(entry.getValue());
            if (null == result)
            {
                result = document;
            } else
            {
                RtfUtil.modifySourceList(result.getHeader(), document.getHeader(), document.getElementList());
                result.getElementList().addAll(document.getElementList());
            }
        }
        if (null == result)
        {
            throw new ApplicationException("Не выбраны ведомости для печати.");
        }
        return result;
    }

    @Override
    public RtfDocument printDoc(final Long id)
    {
        return this.printDocList(Collections.singleton(id));
    }

    // вспомогательное

    private RtfDocument printBulletin(BulletinPrintInfo printData)
    {
        SessionRetakeDocument bulletin = printData.getBulletin();
        Map<EppStudentWpeCAction, BulletinPrintRow> contentMap = printData.getContentMap();

        if (null == printData.getDocument())
        {
            throw new ApplicationException("Для формы контроля и настроек рейтинга ведомости " + bulletin.getTitle() + " не задан печатный шаблон ведомости.");
        }

        final RtfDocument document = printData.getDocument();

        // печатаем шапку

        final TopOrgUnit academy = TopOrgUnit.getInstance();
        final OrgUnit orgUnit = bulletin.getSessionObject().getOrgUnit();
        final TreeSet<String> eduTitles = new TreeSet<>();
        final TreeSet<String> groupTitles = new TreeSet<>();
        final TreeSet<String> courseTitles = new TreeSet<>();
        final TreeSet<String> termNumbers = new TreeSet<>();
        final Set<String> caTypes = new HashSet<>();
        for (final BulletinPrintRow row : contentMap.values())
        {
            eduTitles.add(row.getSlot().getActualStudent().getEducationOrgUnit().getEducationLevelHighSchool().getTitle());
            groupTitles.add(StringUtils.trimToEmpty(row.getSlot().getActualStudent().getGroup() == null ? "" : row.getSlot().getActualStudent().getGroup().getTitle()));
            courseTitles.add(String.valueOf(row.getSlot().getStudentWpeCAction().getStudentWpe().getCourse().getIntValue()));
            termNumbers.add(String.valueOf(row.getSlot().getStudentWpeCAction().getStudentWpe().getTerm().getIntValue()));
            caTypes.add(row.getSlot().getStudentWpeCAction().getType().getTitle());
        }
        final EppRegistryElementPart registryElementPart = bulletin.getRegistryElementPart();
        final EppRegistryElement registryElement = registryElementPart.getRegistryElement();
        List<PpsEntry> commission = printData.getCommission();
        if (commission == null)
        {
            commission = Collections.emptyList();
        }
        final int commissionSize = commission.size();

        final RtfInjectModifier modifier = new RtfInjectModifier();
        modifier.put("academyTitle", academy.getNominativeCaseTitle() == null ? academy.getTitle() == null ? "" : academy.getTitle() : academy.getNominativeCaseTitle());
        modifier.put("ouTitle", orgUnit.getNominativeCaseTitle() == null ? orgUnit.getTitle() : orgUnit.getNominativeCaseTitle());
        modifier.put("documentNumber", bulletin.getNumber());
        modifier.put("educationOrgUnitTitle", StringUtils.join(eduTitles.iterator(), ", "));
        modifier.put("eduYear", bulletin.getSessionObject().getEducationYear().getTitle());
        modifier.put("course", StringUtils.join(courseTitles.iterator(), ", "));
        modifier.put("group", StringUtils.join(groupTitles.iterator(), ", "));
        modifier.put("term", bulletin.getSessionObject().getYearDistributionPart().getTitle());
        modifier.put("termNumber", StringUtils.join(termNumbers, ", "));
        if (registryElement instanceof EppRegistryAttestation)
        {
            modifier.put("regType", "Мероприятие ГИА");
        } else if (registryElement instanceof EppRegistryPractice)
        {
            modifier.put("regType", "Практика");
        } else
        {
            modifier.put("regType", "Дисциплина");
        }
        modifier.put("registryElement", registryElement.getTitle());
        modifier.put("cathedra", registryElement.getOwner().getPrintTitle());
        modifier.put("caType", StringUtils.join(caTypes, ", "));
        modifier.put("caType_N", StringUtils.join(caTypes, ", "));
        modifier.put("totalHours", String.valueOf(registryElementPart.getSize() / 100));
        if (caTypes.size() == 1)
        {
            final EppGroupTypeFCA caType = contentMap.values().iterator().next().getSlot().getStudentWpeCAction().getType();
            if (caType.getCode().equals(EppGroupTypeFCACodes.CONTROL_ACTION_EXAM) || caType.getCode().equals(EppGroupTypeFCACodes.CONTROL_ACTION_EXAM_ACCUM))
            {
                modifier.put("documentType", "Экзаменационная ведомость");
            } else
            {
                modifier.put("documentType", "Зачетная ведомость");
            }
        } else
        {
            modifier.put("documentType", "Ведомость");
        }
        modifier.put("tutors", UniStringUtils.join(commission, PpsEntry.person().identityCard().fio().s(), ", "));
        modifier.put("formingDate", bulletin.getFormingDate() == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(bulletin.getFormingDate()));
        modifier.put("date", bulletin.getPerformDate() == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(bulletin.getPerformDate()));
        modifier.put("performDate", bulletin.getPerformDate() == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(bulletin.getPerformDate()));
        modifier.put("labor", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(registryElementPart.getLaborAsDouble()));

        IPersistentEmployeePost head = bulletin.getSessionObject().getOrgUnit().getHead();
        modifier.put("documentHeadFio", head == null ? "" : head.getEmployee().getPerson().getIdentityCard().getFio());

        SessionReportManager.addOuLeaderData(modifier, orgUnit, "ouleader", "FIOouleader");
        SessionReportManager.addOuLeaderData(modifier, orgUnit, "leader", "leaderTitle");


        // Дополнительные метки
        // направления подготовки профессионального образования
        SessionBulletinPrintUtils.printEduProgramSubject(modifier, printData.getContentMap().keySet());

        // направленности высшего профессионального образования
        SessionBulletinPrintUtils.printEduProgramSpecialization(modifier, printData.getContentMap().keySet());

        // уровни образования
        SessionBulletinPrintUtils.printEduLevels(modifier, printData.getContentMap().keySet());

        // формы обучения студентов
        SessionBulletinPrintUtils.printDevelopForms(modifier, printData.getContentMap().keySet());

        customizeSimpleTags(modifier, printData);

        modifier.modify(document);

        new RtfTableModifier().put("sign", new String[commissionSize == 0 ? 0 : commissionSize - 1][]).modify(document);

        // печатаем таблицу

        final List<ColumnType> columns = this.prepareColumnList(printData);
        final List<ColumnType> templateColumns = this.getTemplateColumnList(printData);
        final int fioColumnIndex = templateColumns.indexOf(ColumnType.FIO);
        if (fioColumnIndex == -1)
        {
            throw new IllegalStateException(); // колонка с фио должна быть
        }

        final TreeMap<String, List<BulletinPrintRow>> groupedContentMap = this.getGroupedAndSortedContent(printData);
        final boolean useGrouping = groupedContentMap.size() > 1;

        final List<String[]> tableRows = new ArrayList<>();
        int rowNumber = 1;

        for (final Map.Entry<String, List<BulletinPrintRow>> entry : groupedContentMap.entrySet())
        {
            if (useGrouping)
            {
                tableRows.add(new String[]{entry.getKey()});
            }
            for (final BulletinPrintRow row : entry.getValue())
            {
                tableRows.add(this.fillTableRow(row, columns, rowNumber++));
            }
        }

        final String[][] tableData = tableRows.toArray(new String[tableRows.size()][]);
        final RtfTableModifier tableModifier = new RtfTableModifier().put("T", tableData);
        tableModifier.put("T", new RtfRowIntercepterBase()
        {
            @Override
            public void beforeModify(final RtfTable table, final int currentRowIndex)
            {
                for (int i = templateColumns.size() - 1; i >= 0; i--)
                {
                    if (!columns.contains(templateColumns.get(i)))
                    {
                        this.deleteCellFioIncrease(table.getRowList().get(currentRowIndex), i, fioColumnIndex); //ячейка в строке с меткой T
                        this.deleteCellFioIncrease(table.getRowList().get(currentRowIndex - 1), i, fioColumnIndex); //ячейка в шапке
                    }
                }
            }

            private void deleteCellFioIncrease(final RtfRow row, final int cellIndex, final int fioCellIndex)
            {
                final RtfCell cell = row.getCellList().get(cellIndex);
                final RtfCell destCell = row.getCellList().get(fioCellIndex);
                destCell.setWidth(destCell.getWidth() + cell.getWidth());
                row.getCellList().remove(cellIndex);
            }

            @Override
            public List<IRtfElement> beforeInject(final RtfTable table, final RtfRow row, final RtfCell cell, final int rowIndex, final int colIndex, final String value)
            {
                if (useGrouping)
                {
                    if (tableData[rowIndex].length == 1)
                    {
                        return new RtfString().boldBegin().append(value).boldEnd().toList();
                    }
                }
                return null;
            }

            @Override
            public void afterModify(final RtfTable table, final List<RtfRow> newRowList, final int startIndex)
            {
                if (useGrouping)
                {
                    for (int i = 0; i < tableData.length; i++)
                    {
                        if (tableData[i].length == 1)
                        {
                            final RtfRow row = newRowList.get(startIndex + i);
                            RtfUtil.unitAllCell(row, 0);
                        }
                    }
                }
            }
        });
        tableModifier.modify(document);

        // печатаем что там еще захочется отдельно допечатать в проектном слое
        this.printAdditionalInfo(printData);

        return document;
    }

	/** Заполнение контейнера данных о ведомости пересдач. */
    protected Map<SessionRetakeDocument, BulletinPrintInfo> prepareData(final Collection<Long> retakeDocIds)
    {
        final List<SessionRetakeDocument> bulletinList = this.getList(SessionRetakeDocument.class, SessionRetakeDocument.id().s(), retakeDocIds);
        Collections.sort(bulletinList, new DelegatePropertyComparator(NumberAsStringComparator.INSTANCE, SessionRetakeDocument.P_NUMBER));

        final Map<SessionRetakeDocument, BulletinPrintInfo> result = new LinkedHashMap<>();
        for (final SessionRetakeDocument bulletin : bulletinList)
        {
            BulletinPrintInfo printData = createPrintInfo(bulletin);

            result.put(bulletin, printData);

            Map<ISessionBrsDao.ISessionRatingSettingsKey, ISessionBrsDao.ISessionRatingSettings> ratingSettings = ISessionBrsDao.instance.get().getRatingSettings(bulletin);
            printData.setUseCurrentRating(CollectionUtils.exists(ratingSettings.values(), ISessionBrsDao.ISessionRatingSettings::useCurrentRating));
            printData.setUsePoints(CollectionUtils.exists(ratingSettings.values(), ISessionBrsDao.ISessionRatingSettings::usePoints));
        }

        this.initTemplates(result);
        this.initContentData(result);
        this.initCommissionData(result);

        return result;
    }

	/**
	 * Создать пустой контейнер для данных о ведомости пересдач. Для переопределения в проектах (создания наследников {@link BulletinPrintInfo} с доп. данными).
	 * Заполнение - в {@link SessionRetakeDocPrintDAO#prepareData}.
	 */
	protected BulletinPrintInfo createPrintInfo(SessionRetakeDocument bulletin)
	{
		return new BulletinPrintInfo(bulletin);
	}

    private void initContentData(final Map<SessionRetakeDocument, BulletinPrintInfo> bulletinMap)
    {
        for (List<SessionRetakeDocument> elements : Iterables.partition(bulletinMap.keySet(), DQL.MAX_VALUES_ROW_NUMBER))
        {

            final DQLSelectBuilder dql = new DQLSelectBuilder()
                    .fromEntity(SessionDocumentSlot.class, "slot")
                    .joinEntity("slot", DQLJoinType.left, SessionMark.class, "mark", eq(property(SessionMark.slot().id().fromAlias("mark")), property("slot.id")))
                    .joinEntity("slot", DQLJoinType.left, SessionProjectTheme.class, "theme", eq(property(SessionProjectTheme.slot().fromAlias("theme")), property("slot")))
                    .joinEntity("slot", DQLJoinType.left, SessionSlotRatingData.class, "slotR", eq(property(SessionSlotRatingData.slot().fromAlias("slotR")), property("slot")))
                    .joinEntity("mark", DQLJoinType.left, SessionMarkRatingData.class, "markR", eq(property(SessionMarkRatingData.mark().fromAlias("markR")), property("mark")))
                    .joinPath(DQLJoinType.inner, SessionDocumentSlot.actualStudent().person().identityCard().fromAlias("slot"), "idc")
                    .column("slot")
                    .column("mark")
                    .column("theme")
                    .column("slotR")
                    .column("markR")
                    .where(in(property(SessionDocumentSlot.document().fromAlias("slot")), elements))
                    .order(property("idc", IdentityCard.lastName()))
                    .order(property("idc", IdentityCard.firstName()))
                    .order(property("idc", IdentityCard.middleName()));

            for (final Object[] row : dql.createStatement(getSession()).<Object[]>list())
            {
                final SessionDocumentSlot slot = (SessionDocumentSlot) row[0];
                final SessionMark mark = (SessionMark) row[1];
                final SessionProjectTheme theme = (SessionProjectTheme) row[2];
                final SessionSlotRatingData slotR = (SessionSlotRatingData) row[3];
                final SessionMarkRatingData markR = (SessionMarkRatingData) row[4];
                final SessionRetakeDocument bulletin = (SessionRetakeDocument) slot.getDocument();
                bulletinMap.get(bulletin).getContentMap().put(slot.getStudentWpeCAction(), new BulletinPrintRow(slot, mark, theme, slotR, markR));
            }
        }
    }

    private void initCommissionData(final Map<SessionRetakeDocument, BulletinPrintInfo> bulletinMap)
    {
        for (List<SessionRetakeDocument> elements : Iterables.partition(bulletinMap.keySet(), DQL.MAX_VALUES_ROW_NUMBER))
        {

            DQLSelectBuilder dql = new DQLSelectBuilder()
                    .fromEntity(SessionComissionPps.class, "rel")
                    .column(SessionComissionPps.pps().fromAlias("rel").s())
                    .joinPath(DQLJoinType.inner, SessionComissionPps.pps().person().identityCard().fromAlias("rel").s(), "idc")
                    .joinEntity("rel", DQLJoinType.inner, SessionRetakeDocument.class, "bulletin", eq(property(SessionRetakeDocument.commission().id().fromAlias("bulletin")), property(SessionComissionPps.commission().id().fromAlias("rel"))))
                    .column(property("bulletin"))
                    .where(in(property("bulletin"), elements))
                    .order(property("idc", IdentityCard.lastName()))
                    .order(property("idc", IdentityCard.firstName()))
                    .order(property("idc", IdentityCard.middleName()));

            for (final Object[] row : dql.createStatement(getSession()).<Object[]>list())
            {
                SessionRetakeDocument sessionDocument = (SessionRetakeDocument) row[1];
                bulletinMap.get(sessionDocument).getCommission().add((PpsEntry) row[0]);
            }

            dql = new DQLSelectBuilder()
                    .fromEntity(SessionComissionPps.class, "rel")
                    .joinPath(DQLJoinType.inner, SessionComissionPps.pps().person().identityCard().fromAlias("rel").s(), "idc")
                    .joinEntity("rel", DQLJoinType.inner, SessionDocumentSlot.class, "slot", eq(property(SessionDocumentSlot.commission().id().fromAlias("slot")), property(SessionComissionPps.commission().id().fromAlias("rel"))))
                    .where(in(property(SessionDocumentSlot.document().fromAlias("slot")), elements))
                    .column(property(SessionComissionPps.pps().fromAlias("rel")))
                    .column(property(SessionDocumentSlot.document().fromAlias("slot")))
                    .column(property(SessionDocumentSlot.studentWpeCAction().fromAlias("slot")))
                    .order(property("idc", IdentityCard.lastName()))
                    .order(property("idc", IdentityCard.firstName()))
                    .order(property("idc", IdentityCard.middleName()));

            for (final Object[] row : dql.createStatement(getSession()).<Object[]>list())
            {
                SessionRetakeDocument sessionDocument = (SessionRetakeDocument) row[1];
                EppStudentWpeCAction studentWpeAction = (EppStudentWpeCAction) row[2];
                bulletinMap.get(sessionDocument).getContentMap().get(studentWpeAction).getCommission().add((PpsEntry) row[0]);
            }
        }
    }

    /**
     * Печатная форма и данные конкретной ведомости
     */
    protected static class BulletinPrintInfo
    {
        private RtfDocument document;
        private final SessionRetakeDocument bulletin;
        private final List<PpsEntry> commission = new ArrayList<>();
        private final Map<EppStudentWpeCAction, BulletinPrintRow> contentMap = new LinkedHashMap<>();

        private boolean useCurrentRating;
        private boolean usePoints;

        public BulletinPrintInfo(final SessionRetakeDocument bulletin)
        {
            this.bulletin = bulletin;
        }

        /**
         * @return Ведомость
         */
        public SessionRetakeDocument getBulletin()
        {
            return this.bulletin;
        }

        /**
         * @return Собственно печатная форма
         */
        public RtfDocument getDocument()
        {
            return this.document;
        }

        public void setDocument(final RtfDocument document)
        {
            this.document = document;
        }

        /**
         * @return Комиссия
         */
        public List<PpsEntry> getCommission()
        {
            return this.commission;
        }

        public Map<EppStudentWpeCAction, BulletinPrintRow> getContentMap()
        {
            return contentMap;
        }

        public boolean isUseCurrentRating()
        {
            return useCurrentRating;
        }

        public void setUseCurrentRating(boolean useCurrentRating)
        {
            this.useCurrentRating = useCurrentRating;
        }

        public boolean isUsePoints()
        {
            return usePoints;
        }

        public void setUsePoints(boolean usePoints)
        {
            this.usePoints = usePoints;
        }
    }

    protected static class BulletinPrintRow
    {
        private SessionDocumentSlot slot;
        private SessionMark mark;
        private SessionSlotRatingData slotRatingData;
        private SessionMarkRatingData markRatingData;
        private SessionProjectTheme theme;
        private List<PpsEntry> commission = new ArrayList<>();

        public BulletinPrintRow(SessionDocumentSlot slot, SessionMark mark, SessionProjectTheme theme, SessionSlotRatingData slotR, SessionMarkRatingData markR)
        {
            this.slot = slot;
            this.mark = mark;
            this.slotRatingData = slotR;
            this.markRatingData = markR;
            this.theme = theme;
        }

        public SessionDocumentSlot getSlot()
        {
            return slot;
        }

        public SessionMark getMark()
        {
            return mark;
        }

        public SessionSlotRatingData getSlotRatingData()
        {
            return slotRatingData;
        }

        public SessionMarkRatingData getMarkRatingData()
        {
            return markRatingData;
        }

        public SessionProjectTheme getTheme()
        {
            return theme;
        }

        public List<PpsEntry> getCommission()
        {
            return commission;
        }

        public void setCommission(List<PpsEntry> commission)
        {
            this.commission = commission;
        }
    }
}
