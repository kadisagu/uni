package ru.tandemservice.unisession.component.orgunit.SessionBulletinListTab;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.uni.dao.IPrepareable;

/**
 * @author vdanilov
 */
public interface IDAO extends IPrepareable<Model> {
    void doPrintBulletin(Long id);

    @Transactional(propagation= Propagation.SUPPORTS)
    void refreshDataSource(Model model);

    void deleteBulletin(Long id);
}
