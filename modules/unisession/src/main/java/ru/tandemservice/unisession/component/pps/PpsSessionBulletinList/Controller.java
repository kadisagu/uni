// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisession.component.pps.PpsSessionBulletinList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.RowCollectionFormatter;
import org.tandemframework.core.view.formatter.RawFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;

import java.util.Iterator;

/**
 * @author iolshvang
 * @since 11.04.2011
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        model.setPrincipalContext(component.getUserContext().getPrincipalContext());
        this.getDao().prepare(model);

        model.setSettings(component.getSettings());
        this.initYearAndPart(model);

        final DynamicListDataSource<SessionBulletinDocument> dataSource = new DynamicListDataSource<>(component, component1 -> {
            Controller.this.getDao().refreshDataSource(Controller.this.getModel(component1));
        });

        dataSource.addColumn(new SimpleColumn("Дата\n формирования", SessionBulletinDocument.formingDate(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setWidth(1));
        dataSource.addColumn(new PublisherLinkColumn("Ведомость", "title").setResolver(new IPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                return new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, entity.getId());
            }

            @Override
            public String getComponentName(IEntity entity)
            {
                return "ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinPubForPps";
            }
        }));
        dataSource.addColumn(new MultiValuesColumn("Курс", "course").setFormatter(new RowCollectionFormatter(RawFormatter.INSTANCE)).setClickable(false).setWidth(1));
        dataSource.addColumn(new MultiValuesColumn("Группа", "groups").setFormatter(new RowCollectionFormatter(RawFormatter.INSTANCE)).setClickable(false).setWidth(1));
        dataSource.addColumn(new SimpleColumn("Дата\n сдачи", SessionBulletinDocument.performDate(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setWidth(1));
        dataSource.addColumn(new SimpleColumn("Дата\n закрытия", SessionBulletinDocument.closeDate(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setWidth(1));
        dataSource.addColumn(new ActionColumn("Выставить оценку", "edit_mark", "onClickMark"));
        model.setDataSource(dataSource);
    }

    public void onClickSearch(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);
        model.getDataSource().showFirstPage();
        model.getDataSource().refresh();
        DataSettingsFacade.saveSettings(model.getSettings());
    }

    public void onClickClear(final IBusinessComponent context)
    {
        this.getModel(context).getSettings().clear();
        this.onClickSearch(context);
    }

    public void onClickMark(final IBusinessComponent context)
    {
        context.createDefaultChildRegion(new ComponentActivator(
                ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinMark.Model.COMPONENT_NAME,
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, context.<Long>getListenerParameter())));
    }

    private void initYearAndPart(final Model model)
    {
        if (null != model.getYear() && null != model.getPart()) {
            return;
        }

        final Object[] row = UniDaoFacade.getCoreDao().getCalculatedValue(session -> {
            final DQLSelectBuilder rootDql = new DQLSelectBuilder().fromEntity(SessionBulletinDocument.class, "b");
            //todo
            //           rootDql.where(DQLExpressions.eq(DQLExpressions.property(SessionBulletinDocument.sessionObject().orgUnit().fromAlias("b")), DQLExpressions.value(model.getOrgUnit())));
            if (null != model.getYear()) {
                rootDql.where(DQLExpressions.eq(DQLExpressions.property(SessionBulletinDocument.sessionObject().educationYear().fromAlias("b")), DQLExpressions.value(model.getYear())));
            }

            rootDql.column(DQLExpressions.property(SessionBulletinDocument.sessionObject().educationYear().id().fromAlias("b")));
            rootDql.column(DQLExpressions.property(SessionBulletinDocument.sessionObject().yearDistributionPart().id().fromAlias("b")));
            rootDql.order(DQLExpressions.property(SessionBulletinDocument.formingDate().fromAlias("b")), OrderDirection.desc);
            final Iterator<Object[]> it = UniBaseDao.scrollRows(rootDql.createStatement(session)).iterator();
            if (it.hasNext()) { return it.next(); }
            return null;
        });

        if (null != row) {
            model.setYear(UniDaoFacade.getCoreDao().getNotNull(EducationYear.class, (Long)row[0]));
            model.setPart(UniDaoFacade.getCoreDao().getNotNull(YearDistributionPart.class, (Long)row[1]));
        }
    }
}