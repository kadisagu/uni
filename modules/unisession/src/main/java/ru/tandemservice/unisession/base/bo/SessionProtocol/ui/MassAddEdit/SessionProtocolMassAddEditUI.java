/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionProtocol.ui.MassAddEdit;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.catalog.codes.EppRegistryStructureCodes;
import ru.tandemservice.unisession.base.bo.SessionMark.SessionMarkManager;
import ru.tandemservice.unisession.base.bo.SessionMark.logic.ISessionMarkDAO;
import ru.tandemservice.unisession.base.bo.SessionProtocol.logic.Utils;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionRoleInGEC;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.stateFinalExam.*;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Andreev
 * @since 12.09.2016
 */
@Input({
        @Bind(key = SessionProtocolMassAddEdit.BULLETIN_ID, binding = "bulletinId", required = true),
        @Bind(key = SessionProtocolMassAddEdit.SESSION_DOCUMENT_SLOT_IDS, binding = "sessionDocumentSlotIds"),
        @Bind(key = SessionProtocolMassAddEdit.PROTOCOL_IDS, binding = "protocolIds")
})
public class SessionProtocolMassAddEditUI extends UIPresenter
{
    private Long _bulletinId;
    private List<Long> _sessionDocumentSlotIds;
    private List<Long> _protocolIds;

    private boolean _addMode;

    private SessionBulletinDocument _bulletin;
    private List<SessionStateFinalExamProtocol> _protocols;

    private Map<SessionRoleInGEC, List<PpsEntry>> _ppsListByRole;
    private SessionRoleInGEC _currentRole;


    @Override
    public void onComponentRefresh()
    {
        ICommonDAO dao = DataAccessServices.dao();
        _bulletin = dao.get(SessionBulletinDocument.class, _bulletinId);

        _addMode = CollectionUtils.isEmpty(_protocolIds);

        _ppsListByRole = dao
                .getList(SessionRoleInGEC.class)
                .stream()
                .collect(Collectors.toMap(role -> role, role -> new ArrayList<>(), (p1, p2) -> p1,
                                          () -> new TreeMap<>((o1, o2) -> Integer.compare(o1.getPriority(), o2.getPriority()))));

        if (!_addMode)
        {
            _protocols = dao.getList(SessionStateFinalExamProtocol.class, _protocolIds);

            SessionGovExamCommission commission = _protocols.get(0).getCommission();
            dao.getList(SessionGECMember.class, SessionGECMember.commission().s(), commission)
                    .forEach(member -> _ppsListByRole.get(member.getRoleInGEC()).add(member.getPps()));
        }
    }


    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(SessionProtocolMassAddEdit.BULLETIN_PARAM, _bulletin);
    }

    // Listeners
    public void onClickApply()
    {
        createProtocols(Utils.createCommission(_ppsListByRole));
        Utils.deleteUselessCommissions();
        deactivate();
    }


    // Getters and Setters
    public Long getBulletinId()
    {
        return _bulletinId;
    }

    public void setBulletinId(Long bulletinId)
    {
        this._bulletinId = bulletinId;
    }

    public List<Long> getSessionDocumentSlotIds()
    {
        return _sessionDocumentSlotIds;
    }

    public void setSessionDocumentSlotIds(List<Long> sessionDocumentSlotIds)
    {
        _sessionDocumentSlotIds = sessionDocumentSlotIds;
    }

    public List<Long> getProtocolIds()
    {
        return _protocolIds;
    }

    public void setProtocolIds(List<Long> protocolIds)
    {
        _protocolIds = protocolIds;
    }

    public SessionRoleInGEC getCurrentRole()
    {
        return _currentRole;
    }

    public void setCurrentRole(SessionRoleInGEC currentRole)
    {
        _currentRole = currentRole;
    }

    public List<PpsEntry> getCurrentPpsList()
    {
        List<PpsEntry> ppsEntries = _ppsListByRole.get(_currentRole);
        if (ppsEntries == null)
        {
            ppsEntries = new ArrayList<>();
            _ppsListByRole.put(_currentRole, ppsEntries);
        }

        return ppsEntries;
    }

    public void setCurrentPpsList(List<PpsEntry> currentPpsList)
    {
        _ppsListByRole.put(_currentRole, currentPpsList);
    }

    public Collection<SessionRoleInGEC> getRoleList()
    {
        return _ppsListByRole.keySet();
    }

    public boolean isAddMode()
    {
        return _addMode;
    }


    protected void createProtocols(SessionGovExamCommission commission)
    {
        ICommonDAO dao = DataAccessServices.dao();

        Map<SessionDocumentSlot, SessionMark> markMap = new HashMap<>();

        if (_addMode)
        {
            String alias = "sds";
            List<Object[]> rows = dao.getList(
                    new DQLSelectBuilder()
                            .fromEntity(SessionDocumentSlot.class, alias)
                            .column(property(alias))
                            .where(in(property(alias, SessionDocumentSlot.id()), _sessionDocumentSlotIds))

                            .joinEntity(alias, DQLJoinType.left, SessionMark.class, "mark",
                                        eq(property("mark", SessionMark.slot()), property(alias)))
                            .column(property("mark"))
            );

            String structure = _bulletin.getGroup().getActivityPart().getRegistryElement().getParent().getCode();
            Date now = new Date();
            String numberPrefix = _bulletin.getNumber();
            for (Object[] o : rows)
            {
                SessionDocumentSlot slot = (SessionDocumentSlot) o[0];
                SessionStateFinalExamProtocol protocol;
                if (EppRegistryStructureCodes.REGISTRY_ATTESTATION_DIPLOMA.equals(structure))
                    protocol = new SessionFQWProtocol();
                else
                    protocol = new SessionStateExamProtocol();

                protocol.setFormingDate(now);
                protocol.setNumber(numberPrefix + "-" + String.valueOf(slot.getStudentWpeCAction().getStudentWpe().getStudent().getPerNumber()));
                protocol.setDocumentSlot(slot);
                protocol.setCommission(commission);
                dao.save(protocol);

                markMap.put(slot, (SessionMark) o[1]);
            }
        }
        else
        {
            String alias = "mark";
            dao.<SessionMark>getList(
                    new DQLSelectBuilder()
                            .fromEntity(SessionMark.class, alias)
                            .column(property(alias))
                            .where(exists(SessionStateFinalExamProtocol.class,
                                          SessionStateFinalExamProtocol.documentSlot().s(), property(alias, SessionMark.slot()),
                                          SessionStateFinalExamProtocol.id().s(), _protocolIds))
            )
                    .forEach(mark -> markMap.put(mark.getSlot(), mark));

            _protocols.forEach(protocol -> {
                protocol.setCommission(commission);
                dao.save(protocol);
            });
        }


        Date examDay = getSettings().get(SessionProtocolMassAddEdit.EXAM_DAY_PARAM);
        if (examDay != null)
        {
            markMap.entrySet().forEach(row -> {
                SessionDocumentSlot slot = row.getKey();
                SessionMark mark = row.getValue();
                if (mark != null)
                {
                    SessionMarkManager.instance().regularMarkDao().saveOrUpdateMark(slot, new ISessionMarkDAO.MarkData()
                    {
                        @Override
                        public Date getPerformDate()
                        {
                            return examDay;
                        }

                        @Override
                        public Double getPoints()
                        {
                            return mark.getPoints();
                        }

                        @Override
                        public SessionMarkCatalogItem getMarkValue()
                        {
                            return mark.getValueItem();
                        }

                        @Override
                        public String getComment()
                        {
                            return mark.getComment();
                        }
                    });
                }
            });
        }
    }
}