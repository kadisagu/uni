/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.StateFinalAttestationResultAdd;


import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniEduProgram.UniEduProgramManager;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unisession.entity.report.StateFinalAttestationResult;

import java.util.Date;

/**
 * @author Andrey Andreev
 * @since 02.11.2016
 */
public abstract class SessionReportStateFinalAttestationResultAddUI<Report extends StateFinalAttestationResult> extends UIPresenter
{
    protected final static String EDUCATION_YEAR_PARAM = "educationYear";
    protected final static String COMPENSATION_TYPE_PARAM = "compensationType";
    protected final static String DATE_FROM_PARAM = "dateFrom";
    protected final static String DATE_TO_PARAM = "dateTo";


    protected Report _report = initReport();
    protected OrgUnitHolder _ouHolder = new OrgUnitHolder();
    protected UniEduProgramEducationOrgUnitAddon _educationOrgUnitUtil;


    public abstract Report initReport();

    @Override
    public void onComponentRefresh()
    {
        configEducationOrgUnitFields();

        _uiSettings.set(EDUCATION_YEAR_PARAM, EducationYear.getCurrentRequired());

        _report.setFormingDate(new Date());
    }


    /**
     * Конфигурация утили для выбора НПП по фильтрам
     */
    protected void configEducationOrgUnitFields()
    {
        _educationOrgUnitUtil = (UniEduProgramEducationOrgUnitAddon) getConfig().getAddon(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME);

        if (_educationOrgUnitUtil != null)
        {
            UniEduProgramEducationOrgUnitAddon.Filters formativeOrgUnitFilter = UniEduProgramEducationOrgUnitAddon.Filters.FORMATIVE_ORG_UNIT;
            _educationOrgUnitUtil.configSettings(this.getSettingsKey());
            _educationOrgUnitUtil.configUseFilters(
                    UniEduProgramEducationOrgUnitAddon.Filters.QUALIFICATION,
                    formativeOrgUnitFilter,
                    UniEduProgramEducationOrgUnitAddon.Filters.TERRITORIAL_ORG_UNIT,
                    UniEduProgramEducationOrgUnitAddon.Filters.PRODUCING_ORG_UNIT,
                    UniEduProgramEducationOrgUnitAddon.Filters.EDUCATION_LEVEL_HIGH_SCHOOL,
                    UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_CONDITION,
                    UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_FORM,
                    UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_PERIOD,
                    UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_TECH
            );
            if (getOuHolder().getId() != null)
            {
                _educationOrgUnitUtil.configDisabledFilters(formativeOrgUnitFilter);
                _educationOrgUnitUtil.getSettings().set(formativeOrgUnitFilter.getSettingsName(), _ouHolder.getValue());
                _educationOrgUnitUtil.getValuesMap().put(formativeOrgUnitFilter, _ouHolder.getValue());
            }

            _educationOrgUnitUtil.configRequiredFilters(UniEduProgramEducationOrgUnitAddon.Filters.EDUCATION_LEVEL_HIGH_SCHOOL);
            _educationOrgUnitUtil.configMultiselectFilters();
        }
    }

    public void saveSettings()
    {
        super.saveSettings();
        if (_educationOrgUnitUtil != null)
            _educationOrgUnitUtil.saveSettings();
    }

    // Listeners
    public void onClickApply()
    {
        _report.setEducationYear(_uiSettings.get(EDUCATION_YEAR_PARAM));
        _report.setQualification((Qualifications) _educationOrgUnitUtil.getFilterValue(UniEduProgramEducationOrgUnitAddon.Filters.QUALIFICATION));
        _report.setFormativeOrgUnit((OrgUnit) _educationOrgUnitUtil.getFilterValue(UniEduProgramEducationOrgUnitAddon.Filters.FORMATIVE_ORG_UNIT));
        _report.setTerritorialOrgUnit((OrgUnit) _educationOrgUnitUtil.getFilterValue(UniEduProgramEducationOrgUnitAddon.Filters.TERRITORIAL_ORG_UNIT));
        _report.setOwnerOrgUnit((OrgUnit) _educationOrgUnitUtil.getFilterValue(UniEduProgramEducationOrgUnitAddon.Filters.PRODUCING_ORG_UNIT));
        _report.setEducationLevelHighSchool((EducationLevelsHighSchool) _educationOrgUnitUtil.getFilterValue(UniEduProgramEducationOrgUnitAddon.Filters.EDUCATION_LEVEL_HIGH_SCHOOL));
        _report.setDevelopCondition((DevelopCondition) _educationOrgUnitUtil.getFilterValue(UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_CONDITION));
        _report.setDevelopForm((DevelopForm) _educationOrgUnitUtil.getFilterValue(UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_FORM));
        _report.setDevelopPeriod((DevelopPeriod) _educationOrgUnitUtil.getFilterValue(UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_PERIOD));
        _report.setDevelopTech((DevelopTech) _educationOrgUnitUtil.getFilterValue(UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_TECH));
        _report.setCompensationType(_uiSettings.get(COMPENSATION_TYPE_PARAM));
        _report.setDateFrom(_uiSettings.get(DATE_FROM_PARAM));
        _report.setDateTo(_uiSettings.get(DATE_TO_PARAM));

        saveSettings();

        createStoredReport(_report);

        deactivate();

        openReportPub(_report);
    }

    public abstract void createStoredReport(Report report);

    public abstract void openReportPub(Report report);


    // Getters & Setters
    public Report getReport()
    {
        return _report;
    }

    public void setReport(Report report)
    {
        _report = report;
    }

    public OrgUnitHolder getOuHolder()
    {
        return _ouHolder;
    }

    public UniEduProgramEducationOrgUnitAddon getEducationOrgUnitUtil()
    {
        return _educationOrgUnitUtil;
    }


    public CommonPostfixPermissionModelBase getSec()
    {
        return getOuHolder().getSecModel();
    }

    @Override
    public ISecured getSecuredObject()
    {
        return getOuHolder().getId() == null ? super.getSecuredObject() : getOuHolder().getValue();
    }

    public abstract String getAddPermissionKey();
}
