/* $Id: $ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.SfaSummaryResultAdd.logic;

import jxl.Workbook;
import jxl.format.*;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import jxl.write.biff.RowsExceededException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.organization.base.bo.OrgUnit.logic.OrgUnitRankComparator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndexGeneration;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unisession.base.bo.SessionReport.logic.SfaRaw;
import ru.tandemservice.unisession.base.bo.SessionReport.logic.StateFinalAttestationUtil;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.mark.SessionSlotRegularMark;
import ru.tandemservice.unisession.entity.report.SfaSummaryResult;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.Boolean;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static ru.tandemservice.unisession.base.bo.SessionReport.logic.StateFinalAttestationUtil.*;


/**
 * @author Andrey Andreev
 * @since 28.10.2016
 */
public abstract class SfaSummaryResultPrintDAO<R1 extends SfaRaw, R2 extends Row> extends UniBaseDao implements ISfaSummaryResultPrintDAO
{
    protected int tableWidth;

    @Override
    public SfaSummaryResult createStoredReport(SfaSummaryResult report)
    {
        DatabaseFile databaseFile = new DatabaseFile();

        byte[] content = null;
        try
        {
            content = print(report);
        }
        catch (WriteException | IOException e)
        {
            e.printStackTrace();
        }

        databaseFile.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_VND_MS_EXCEL);
        databaseFile.setContent(content);
        databaseFile.setFilename(getFileName(report));
        save(databaseFile);
        report.setContent(databaseFile);

        save(report);

        return report;
    }

    protected abstract String getFileName(SfaSummaryResult report);

    /**
     * печать отчета
     *
     * @param report параметры отчета
     * @return файл отчета
     */
    protected byte[] print(SfaSummaryResult report) throws WriteException, IOException
    {
        SfaSumReportInfo<R1> absReportInfo = initReportInfo(report);

        init();

        ByteArrayOutputStream out = new ByteArrayOutputStream();

        WritableWorkbook workbook = Workbook.createWorkbook(out);
        WritableSheet sheet = workbook.createSheet("Лист 1)", 0);

        int row = printTitle(report, sheet);

        String topTitle = TopOrgUnit.getInstance().getPrintTitle();
        if (topTitle != null && topTitle.length() > 80)
            try
            {
                sheet.setRowView(row, 700);
            }
            catch (RowsExceededException e)
            {
                e.printStackTrace();
            }
        StateFinalAttestationUtil.addTextCell(sheet, 0, row++, 10, 1, topTitle, _subTitleStyle);

        row = printParametersTable(absReportInfo, sheet, row);

        row++;
        addTable(absReportInfo, sheet, row);

        workbook.write();
        workbook.close();
        return out.toByteArray();
    }

    /**
     * Иницализация данных необходимых для построения отчета
     */
    public abstract SfaSumReportInfo<R1> initReportInfo(SfaSummaryResult report);

    /**
     * Инициализация
     * по умолчанию инициализируются шрифты и стили ячеек.
     */
    protected void init() throws WriteException
    {
        Colour ouTitle = Colour.VERY_LIGHT_YELLOW;

        WritableFont font10 = new WritableFont(WritableFont.TIMES, 10);
        WritableFont fontBold10 = new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD);
        WritableFont fontBold11 = new WritableFont(WritableFont.TIMES, 11, WritableFont.BOLD);

        WritableFont redFont10 = new WritableFont(font10);
        redFont10.setColour(Colour.RED);
        WritableFont redFontBold10 = new WritableFont(fontBold10);
        redFontBold10.setColour(Colour.RED);


        _titleStyle = new WritableCellFormat(new WritableFont(WritableFont.TIMES, 14, WritableFont.BOLD));
        _titleStyle.setBorder(Border.NONE, BorderLineStyle.NONE, Colour.BLACK);
        _titleStyle.setWrap(false);
        _titleStyle.setVerticalAlignment(VerticalAlignment.CENTRE);
        _titleStyle.setAlignment(Alignment.LEFT);

        _subTitleStyle = new WritableCellFormat(new WritableFont(WritableFont.TIMES, 12));
        _subTitleStyle.setBorder(Border.NONE, BorderLineStyle.NONE, Colour.BLACK);
        _subTitleStyle.setWrap(true);
        _subTitleStyle.setVerticalAlignment(VerticalAlignment.CENTRE);
        _subTitleStyle.setAlignment(Alignment.LEFT);

        _parametersTS = new WritableCellFormat(new WritableFont(WritableFont.TIMES, 12));
        _parametersTS.setBorder(Border.NONE, BorderLineStyle.NONE, Colour.BLACK);
        _parametersTS.setWrap(true);
        _parametersTS.setVerticalAlignment(VerticalAlignment.CENTRE);
        _parametersTS.setAlignment(Alignment.LEFT);


        _horHeaderDataTS = new WritableCellFormat(font10);
        _horHeaderDataTS.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        _horHeaderDataTS.setWrap(true);
        _horHeaderDataTS.setVerticalAlignment(VerticalAlignment.CENTRE);
        _horHeaderDataTS.setAlignment(Alignment.CENTRE);

        _verHeaderDataTS = new WritableCellFormat(_horHeaderDataTS);
        _verHeaderDataTS.setOrientation(Orientation.PLUS_90);

        _horHeaderBoldDataTS = new WritableCellFormat(new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD));
        _horHeaderBoldDataTS.setBorder(Border.ALL, BorderLineStyle.THICK, Colour.BLACK);
        _horHeaderBoldDataTS.setWrap(true);
        _horHeaderBoldDataTS.setVerticalAlignment(VerticalAlignment.CENTRE);
        _horHeaderBoldDataTS.setAlignment(Alignment.CENTRE);


        _regElDetailIntTS = new WritableCellFormat(font10);
        _regElDetailIntTS.setBorder(Border.BOTTOM, BorderLineStyle.THIN, Colour.BLACK);
        _regElDetailIntTS.setBorder(Border.LEFT, BorderLineStyle.THIN, Colour.BLACK);
        _regElDetailIntTS.setBorder(Border.RIGHT, BorderLineStyle.THIN, Colour.BLACK);
        _regElDetailIntTS.setWrap(true);
        _regElDetailIntTS.setVerticalAlignment(VerticalAlignment.CENTRE);
        _regElDetailIntTS.setAlignment(Alignment.CENTRE);

        _regElDetailRedIntTS = new WritableCellFormat(_regElDetailIntTS);
        _regElDetailRedIntTS.setFont(redFont10);

        _regElTitleDetailTS = new WritableCellFormat(_regElDetailIntTS);
        _regElTitleDetailTS.setAlignment(Alignment.LEFT);

        _regElDetailDoubleTS = new WritableCellFormat(font10, new NumberFormat("0.00"));
        _regElDetailDoubleTS.setBorder(Border.BOTTOM, BorderLineStyle.THIN, Colour.BLACK);
        _regElDetailDoubleTS.setBorder(Border.LEFT, BorderLineStyle.THIN, Colour.BLACK);
        _regElDetailDoubleTS.setBorder(Border.RIGHT, BorderLineStyle.THIN, Colour.BLACK);
        _regElDetailDoubleTS.setWrap(true);
        _regElDetailDoubleTS.setVerticalAlignment(VerticalAlignment.CENTRE);
        _regElDetailDoubleTS.setAlignment(Alignment.CENTRE);

        _generationDtlTS = new WritableCellFormat(fontBold10);
        _generationDtlTS.setAlignment(Alignment.CENTRE);
        _generationDtlTS.setVerticalAlignment(VerticalAlignment.CENTRE);
        _generationDtlTS.setBorder(Border.BOTTOM, BorderLineStyle.THIN, Colour.BLACK);
        _generationDtlTS.setBorder(Border.LEFT, BorderLineStyle.THIN, Colour.BLACK);
        _generationDtlTS.setBorder(Border.RIGHT, BorderLineStyle.THIN, Colour.BLACK);

        // Направление подготовки
        WritableFont font4Mag = new WritableFont(font10);
        font4Mag.setColour(Colour.DARK_RED);
        WritableFont font4Spec = new WritableFont(font10);
        font4Spec.setColour(Colour.BLUE);
        _lvlPrefixDtlTS = new WritableCellFormat(font10);
        _lvlPrefixDtlTS.setAlignment(Alignment.CENTRE);
        _lvlPrefixDtlTS.setVerticalAlignment(VerticalAlignment.CENTRE);
        _lvlPrefixDtlTS.setBorder(Border.RIGHT, BorderLineStyle.THIN, Colour.BLACK);
        _lvlPrefixDtlTS.setBorder(Border.BOTTOM, BorderLineStyle.THIN, Colour.BLACK);
        _lvlPrefixDtlTS.setBorder(Border.LEFT, BorderLineStyle.THIN, Colour.BLACK);
        _lvlPrefixDtlTS.setWrap(true);
        _lvlTitleDtlTS = new WritableCellFormat(_lvlPrefixDtlTS);
        _lvlTitleDtlTS.setAlignment(Alignment.LEFT);
        _lvlPrefixMagDtlTS = new WritableCellFormat(_lvlPrefixDtlTS);
        _lvlPrefixMagDtlTS.setFont(font4Mag);
        _lvlTitleMagDtlTS = new WritableCellFormat(_lvlTitleDtlTS);
        _lvlTitleMagDtlTS.setFont(font4Mag);
        _lvlPrefixSpecDtlTS = new WritableCellFormat(_lvlPrefixDtlTS);
        _lvlPrefixSpecDtlTS.setFont(font4Spec);
        _lvlTitleSpecDtlTS = new WritableCellFormat(_lvlTitleDtlTS);
        _lvlTitleSpecDtlTS.setFont(font4Spec);

        // Всего по территориальному
        _touTitleDtlTS = new WritableCellFormat(fontBold11);
        _touTitleDtlTS.setBorder(Border.TOP, BorderLineStyle.THICK, Colour.BLACK);
        _touTitleDtlTS.setBorder(Border.BOTTOM, BorderLineStyle.THICK, Colour.BLACK);
        _touTitleDtlTS.setBackground(ouTitle);
        _touTitleDtlTS.setWrap(true);

        _touTotalStdDtlTS = new WritableCellFormat(fontBold11);
        _touTotalStdDtlTS.setBorder(Border.TOP, BorderLineStyle.THICK, Colour.BLACK);
        _touTotalStdDtlTS.setBorder(Border.LEFT, BorderLineStyle.THIN, Colour.BLACK);
        _touTotalStdDtlTS.setBorder(Border.BOTTOM, BorderLineStyle.THICK, Colour.BLACK);
        _touTotalStdDtlTS.setBorder(Border.RIGHT, BorderLineStyle.THIN, Colour.BLACK);
        _touTotalStdDtlTS.setWrap(true);
        _touTotalCellDtlTS = new WritableCellFormat(fontBold11);
        _touTotalCellDtlTS.setBorder(Border.LEFT, BorderLineStyle.THIN, Colour.BLACK);
        _touTotalCellDtlTS.setBorder(Border.BOTTOM, BorderLineStyle.THIN, Colour.BLACK);
        _touTotalCellDtlTS.setBorder(Border.RIGHT, BorderLineStyle.THIN, Colour.BLACK);
        _touTotalCellDtlTS.setWrap(true);
        _touTotalCellDtlTS.setVerticalAlignment(VerticalAlignment.CENTRE);
        _touTotalCellThickTopDtlTS = new WritableCellFormat(_touTotalCellDtlTS);
        _touTotalCellThickTopDtlTS.setBorder(Border.TOP, BorderLineStyle.THICK, Colour.BLACK);

        _touTotalIntDtlTS = new WritableCellFormat(_regElDetailIntTS);
        _touTotalIntDtlTS.setFont(fontBold10);
        _touTotalIntThickTopDtlTS = new WritableCellFormat(_touTotalIntDtlTS);
        _touTotalIntThickTopDtlTS.setBorder(Border.TOP, BorderLineStyle.THICK, Colour.BLACK);
        _touTotalRedIntDtlTS = new WritableCellFormat(_touTotalIntDtlTS);
        _touTotalRedIntDtlTS.setFont(redFontBold10);
        _touTotalRedIntThickTopDtlTS = new WritableCellFormat(_touTotalIntThickTopDtlTS);
        _touTotalRedIntThickTopDtlTS.setFont(redFontBold10);

        _touTotalDoubleDtlTS = new WritableCellFormat(_regElDetailDoubleTS);
        _touTotalDoubleDtlTS.setFont(fontBold10);
        _touTotalDoubleThickTopDtlTS = new WritableCellFormat(_touTotalDoubleDtlTS);
        _touTotalDoubleThickTopDtlTS.setBorder(Border.TOP, BorderLineStyle.THICK, Colour.BLACK);

        // Всего в филиалах по формирующему
        Colour tousTotal = Colour.LIGHT_BLUE;
        _tousTotalStdDtlTS = new WritableCellFormat(fontBold11);
        _tousTotalStdDtlTS.setBorder(Border.TOP, BorderLineStyle.THICK, Colour.BLACK);
        _tousTotalStdDtlTS.setBorder(Border.LEFT, BorderLineStyle.THIN, Colour.BLACK);
        _tousTotalStdDtlTS.setBorder(Border.BOTTOM, BorderLineStyle.THICK, Colour.BLACK);
        _tousTotalStdDtlTS.setBorder(Border.RIGHT, BorderLineStyle.THIN, Colour.BLACK);
        _tousTotalStdDtlTS.setBackground(tousTotal);
        _tousTotalStdDtlTS.setWrap(true);
        _tousTotalCellDtlTS = new WritableCellFormat(fontBold11);
        _tousTotalCellDtlTS.setBorder(Border.LEFT, BorderLineStyle.THIN, Colour.BLACK);
        _tousTotalCellDtlTS.setBorder(Border.BOTTOM, BorderLineStyle.THIN, Colour.BLACK);
        _tousTotalCellDtlTS.setBorder(Border.RIGHT, BorderLineStyle.THIN, Colour.BLACK);
        _tousTotalCellDtlTS.setBackground(tousTotal);
        _tousTotalCellDtlTS.setWrap(true);
        _tousTotalCellThickTopDtlTS = new WritableCellFormat(_tousTotalCellDtlTS);
        _tousTotalCellThickTopDtlTS.setBorder(Border.TOP, BorderLineStyle.THICK, Colour.BLACK);

        _tousTotalIntDtlTS = new WritableCellFormat(_regElDetailIntTS);
        _tousTotalIntDtlTS.setFont(fontBold10);
        _tousTotalIntDtlTS.setBackground(tousTotal);
        _tousTotalIntThickTopDtlTS = new WritableCellFormat(_tousTotalIntDtlTS);
        _tousTotalIntThickTopDtlTS.setBorder(Border.TOP, BorderLineStyle.THICK, Colour.BLACK);
        _tousTotalRedIntDtlTS = new WritableCellFormat(_tousTotalIntDtlTS);
        _tousTotalRedIntDtlTS.setFont(redFont10);
        _tousTotalRedIntThickTopDtlTS = new WritableCellFormat(_tousTotalRedIntDtlTS);
        _tousTotalRedIntThickTopDtlTS.setBorder(Border.TOP, BorderLineStyle.THICK, Colour.BLACK);

        _tousTotalDoubleDtlTS = new WritableCellFormat(_regElDetailDoubleTS);
        _tousTotalDoubleDtlTS.setFont(fontBold10);
        _tousTotalDoubleDtlTS.setBackground(tousTotal);
        _tousTotalDoubleThickTopDtlTS = new WritableCellFormat(_tousTotalDoubleDtlTS);
        _tousTotalDoubleThickTopDtlTS.setBorder(Border.TOP, BorderLineStyle.THICK, Colour.BLACK);


        // Всего по формирующему
        Colour fouTotal = Colour.PALE_BLUE;
        _fouTitleDtlTS = new WritableCellFormat(fontBold11);
        _fouTitleDtlTS.setBorder(Border.TOP, BorderLineStyle.THICK, Colour.BLACK);
        _fouTitleDtlTS.setBorder(Border.BOTTOM, BorderLineStyle.THICK, Colour.BLACK);
        _fouTitleDtlTS.setBackground(ouTitle);
        _fouTitleDtlTS.setWrap(true);
        _fouTotalStdDtlTS = new WritableCellFormat(fontBold11);
        _fouTotalStdDtlTS.setAlignment(Alignment.CENTRE);
        _fouTotalStdDtlTS.setVerticalAlignment(VerticalAlignment.CENTRE);
        _fouTotalStdDtlTS.setBorder(Border.TOP, BorderLineStyle.THICK, Colour.BLACK);
        _fouTotalStdDtlTS.setBorder(Border.BOTTOM, BorderLineStyle.THICK, Colour.BLACK);
        _fouTotalStdDtlTS.setBackground(fouTotal);
        _fouTotalStdDtlTS.setWrap(true);
        _fouTotalCellDtlTS = new WritableCellFormat(fontBold11);
        _fouTotalCellDtlTS.setBorder(Border.LEFT, BorderLineStyle.THIN, Colour.BLACK);
        _fouTotalCellDtlTS.setBorder(Border.BOTTOM, BorderLineStyle.THIN, Colour.BLACK);
        _fouTotalCellDtlTS.setBorder(Border.RIGHT, BorderLineStyle.THIN, Colour.BLACK);
        _fouTotalCellDtlTS.setBackground(fouTotal);
        _fouTotalCellDtlTS.setWrap(true);
        _fouTotalCellThickTopDtlTS = new WritableCellFormat(_fouTotalCellDtlTS);
        _fouTotalCellThickTopDtlTS.setBorder(Border.TOP, BorderLineStyle.THICK, Colour.BLACK);

        _fouTotalIntDtlTS = new WritableCellFormat(_regElDetailIntTS);
        _fouTotalIntDtlTS.setFont(fontBold10);
        _fouTotalIntDtlTS.setBackground(fouTotal);
        _fouTotalIntThickTopDtlTS = new WritableCellFormat(_fouTotalIntDtlTS);
        _fouTotalIntThickTopDtlTS.setBorder(Border.TOP, BorderLineStyle.THICK, Colour.BLACK);
        _fouTotalRedIntDtlTS = new WritableCellFormat(_fouTotalIntDtlTS);
        _fouTotalRedIntDtlTS.setFont(redFontBold10);
        _fouTotalRedIntThickTopDtlTS = new WritableCellFormat(_fouTotalRedIntDtlTS);
        _fouTotalRedIntThickTopDtlTS.setBorder(Border.TOP, BorderLineStyle.THICK, Colour.BLACK);

        _fouTotalDoubleDtlTS = new WritableCellFormat(_regElDetailDoubleTS);
        _fouTotalDoubleDtlTS.setFont(fontBold10);
        _fouTotalDoubleDtlTS.setBackground(fouTotal);
        _fouTotalDoubleThickTopDtlTS = new WritableCellFormat(_fouTotalDoubleDtlTS);
        _fouTotalDoubleThickTopDtlTS.setBorder(Border.TOP, BorderLineStyle.THICK, Colour.BLACK);


        // Всего по территориальным подразделениям
        Colour finalTotal = Colour.LIGHT_ORANGE;
        _finalTotalStdDtlTS = new WritableCellFormat(fontBold11);
        _finalTotalStdDtlTS.setBorder(Border.TOP, BorderLineStyle.THICK, Colour.BLACK);
        _finalTotalStdDtlTS.setBorder(Border.LEFT, BorderLineStyle.THIN, Colour.BLACK);
        _finalTotalStdDtlTS.setBorder(Border.BOTTOM, BorderLineStyle.THICK, Colour.BLACK);
        _finalTotalStdDtlTS.setBorder(Border.RIGHT, BorderLineStyle.THIN, Colour.BLACK);
        _finalTotalStdDtlTS.setBackground(finalTotal);
        _finalTotalStdDtlTS.setWrap(true);
        _finalTotalStdDtlTS.setAlignment(Alignment.CENTRE);
        _finalTotalStdDtlTS.setVerticalAlignment(VerticalAlignment.CENTRE);
        _finalTotalCellDtlTS = new WritableCellFormat(fontBold11);
        _finalTotalCellDtlTS.setBorder(Border.LEFT, BorderLineStyle.THIN, Colour.BLACK);
        _finalTotalCellDtlTS.setBorder(Border.BOTTOM, BorderLineStyle.THIN, Colour.BLACK);
        _finalTotalCellDtlTS.setBorder(Border.RIGHT, BorderLineStyle.THIN, Colour.BLACK);
        _finalTotalCellDtlTS.setBackground(finalTotal);
        _finalTotalCellDtlTS.setWrap(true);
        _finalTotalCellThickTopDtlTS = new WritableCellFormat(_finalTotalCellDtlTS);
        _finalTotalCellThickTopDtlTS.setBorder(Border.TOP, BorderLineStyle.THICK, Colour.BLACK);

        _finalTotalIntDtlTS = new WritableCellFormat(_regElDetailIntTS);
        _finalTotalIntDtlTS.setFont(fontBold10);
        _finalTotalIntDtlTS.setBackground(finalTotal);
        _finalTotalIntThickTopDtlTS = new WritableCellFormat(_finalTotalIntDtlTS);
        _finalTotalIntThickTopDtlTS.setBorder(Border.TOP, BorderLineStyle.THICK, Colour.BLACK);
        _finalTotalRedIntDtlTS = new WritableCellFormat(_finalTotalIntDtlTS);
        _finalTotalRedIntDtlTS.setFont(redFont10);
        _finalTotalRedIntThickTopDtlTS = new WritableCellFormat(_finalTotalRedIntDtlTS);
        _finalTotalRedIntThickTopDtlTS.setBorder(Border.TOP, BorderLineStyle.THICK, Colour.BLACK);

        _finalTotalDoubleDtlTS = new WritableCellFormat(_regElDetailDoubleTS);
        _finalTotalDoubleDtlTS.setFont(fontBold10);
        _finalTotalDoubleDtlTS.setBackground(finalTotal);
        _finalTotalDoubleThickTopDtlTS = new WritableCellFormat(_finalTotalDoubleDtlTS);
        _finalTotalDoubleThickTopDtlTS.setBorder(Border.TOP, BorderLineStyle.THICK, Colour.BLACK);


        // СВОД
        Colour svod = Colour.ORANGE;
        _svodTotalStdDtlTS = new WritableCellFormat(fontBold11);
        _svodTotalStdDtlTS.setBorder(Border.TOP, BorderLineStyle.THICK, Colour.BLACK);
        _svodTotalStdDtlTS.setBorder(Border.LEFT, BorderLineStyle.THIN, Colour.BLACK);
        _svodTotalStdDtlTS.setBorder(Border.BOTTOM, BorderLineStyle.THICK, Colour.BLACK);
        _svodTotalStdDtlTS.setBorder(Border.RIGHT, BorderLineStyle.THIN, Colour.BLACK);
        _svodTotalStdDtlTS.setBackground(svod);
        _svodTotalStdDtlTS.setAlignment(Alignment.CENTRE);
        _svodTotalStdDtlTS.setWrap(true);
        _svodTotalStdDtlTS.setVerticalAlignment(VerticalAlignment.CENTRE);
        _svodTotalCellDtlTS = new WritableCellFormat(fontBold11);
        _svodTotalCellDtlTS.setBorder(Border.LEFT, BorderLineStyle.THIN, Colour.BLACK);
        _svodTotalCellDtlTS.setBorder(Border.BOTTOM, BorderLineStyle.THIN, Colour.BLACK);
        _svodTotalCellDtlTS.setBorder(Border.RIGHT, BorderLineStyle.THIN, Colour.BLACK);
        _svodTotalCellDtlTS.setBackground(svod);
        _svodTotalCellDtlTS.setWrap(true);
        _svodTotalCellThickTopDtlTS = new WritableCellFormat(_svodTotalCellDtlTS);
        _svodTotalCellThickTopDtlTS.setBorder(Border.TOP, BorderLineStyle.THICK, Colour.BLACK);

        _svodTotalIntDtlTS = new WritableCellFormat(_regElDetailIntTS);
        _svodTotalIntDtlTS.setFont(fontBold10);
        _svodTotalIntDtlTS.setBackground(svod);
        _svodTotalIntThickTopDtlTS = new WritableCellFormat(_svodTotalIntDtlTS);
        _svodTotalIntThickTopDtlTS.setBorder(Border.TOP, BorderLineStyle.THICK, Colour.BLACK);
        _svodTotalRedIntDtlTS = new WritableCellFormat(_svodTotalIntDtlTS);
        _svodTotalRedIntDtlTS.setFont(redFont10);
        _svodTotalRedIntThickTopDtlTS = new WritableCellFormat(_svodTotalRedIntDtlTS);
        _svodTotalRedIntThickTopDtlTS.setBorder(Border.TOP, BorderLineStyle.THICK, Colour.BLACK);

        _svodTotalDoubleDtlTS = new WritableCellFormat(_regElDetailDoubleTS);
        _svodTotalDoubleDtlTS.setFont(fontBold10);
        _svodTotalDoubleDtlTS.setBackground(svod);
        _svodTotalDoubleThickTopDtlTS = new WritableCellFormat(_svodTotalDoubleDtlTS);
        _svodTotalDoubleThickTopDtlTS.setBorder(Border.TOP, BorderLineStyle.THICK, Colour.BLACK);


        // для не детализированного отчета
        WritableFont font11 = new WritableFont(WritableFont.TIMES, 11);
        WritableFont fontRed11 = new WritableFont(font11);
        fontRed11.setColour(Colour.RED);
        WritableFont fontRedBold11 = new WritableFont(fontRed11);
        fontRedBold11.setBoldStyle(WritableFont.BOLD);
        _fouTitleTS = new WritableCellFormat(fontBold11);
        _fouTitleTS.setBorder(Border.LEFT, BorderLineStyle.THIN, Colour.BLACK);
        _fouTitleTS.setBorder(Border.BOTTOM, BorderLineStyle.THIN, Colour.BLACK);
        _fouTitleTS.setBorder(Border.RIGHT, BorderLineStyle.THIN, Colour.BLACK);
        _fouTitleTS.setWrap(true);

        _fouSubTitleTS = new WritableCellFormat(font11);
        _fouSubTitleTS.setAlignment(Alignment.RIGHT);
        _fouSubTitleTS.setBorder(Border.LEFT, BorderLineStyle.THIN, Colour.BLACK);
        _fouSubTitleTS.setBorder(Border.BOTTOM, BorderLineStyle.THIN, Colour.BLACK);
        _fouSubTitleTS.setBorder(Border.RIGHT, BorderLineStyle.THIN, Colour.BLACK);

        _fouIntTS = new WritableCellFormat(fontBold11);
        _fouIntTS.setAlignment(Alignment.CENTRE);
        _fouIntTS.setBorder(Border.LEFT, BorderLineStyle.THIN, Colour.BLACK);
        _fouIntTS.setBorder(Border.BOTTOM, BorderLineStyle.THIN, Colour.BLACK);
        _fouIntTS.setBorder(Border.RIGHT, BorderLineStyle.THIN, Colour.BLACK);
        _fouSubIntTS = new WritableCellFormat(font11);
        _fouSubIntTS.setAlignment(Alignment.CENTRE);
        _fouSubIntTS.setBorder(Border.LEFT, BorderLineStyle.THIN, Colour.BLACK);
        _fouSubIntTS.setBorder(Border.BOTTOM, BorderLineStyle.THIN, Colour.BLACK);
        _fouSubIntTS.setBorder(Border.RIGHT, BorderLineStyle.THIN, Colour.BLACK);
        _fouRedIntTS = new WritableCellFormat(_fouIntTS);
        _fouRedIntTS.setFont(fontRed11);
        _fouSubRedIntTS = new WritableCellFormat(_fouSubIntTS);
        _fouSubRedIntTS.setFont(fontRedBold11);

        _fouDoubleTS = new WritableCellFormat(_regElDetailDoubleTS);
        _fouDoubleTS.setFont(fontBold11);
        _fouSubDoubleTS = new WritableCellFormat(_regElDetailDoubleTS);
        _fouSubDoubleTS.setFont(font11);


        Colour total = Colour.ORANGE;
        Colour totalSub = Colour.LIGHT_ORANGE;
        _totalTitleTS = new WritableCellFormat(fontBold11);
        _totalTitleTS.setBorder(Border.TOP, BorderLineStyle.THICK, Colour.BLACK);
        _totalTitleTS.setBorder(Border.LEFT, BorderLineStyle.THIN, Colour.BLACK);
        _totalTitleTS.setBorder(Border.BOTTOM, BorderLineStyle.THIN, Colour.BLACK);
        _totalTitleTS.setBorder(Border.RIGHT, BorderLineStyle.THIN, Colour.BLACK);
        _totalTitleTS.setBackground(total);
        _totalTitleTS.setWrap(true);

        _totalSubTitleTS = new WritableCellFormat(font11);
        _totalSubTitleTS.setAlignment(Alignment.RIGHT);
        _totalSubTitleTS.setBorder(Border.LEFT, BorderLineStyle.THIN, Colour.BLACK);
        _totalSubTitleTS.setBorder(Border.BOTTOM, BorderLineStyle.THIN, Colour.BLACK);
        _totalSubTitleTS.setBorder(Border.RIGHT, BorderLineStyle.THIN, Colour.BLACK);
        _totalSubTitleTS.setBackground(totalSub);

        _totalIntTS = new WritableCellFormat(fontBold11);
        _totalIntTS.setAlignment(Alignment.CENTRE);
        _totalIntTS.setBorder(Border.TOP, BorderLineStyle.THICK, Colour.BLACK);
        _totalIntTS.setBorder(Border.LEFT, BorderLineStyle.THIN, Colour.BLACK);
        _totalIntTS.setBorder(Border.BOTTOM, BorderLineStyle.THIN, Colour.BLACK);
        _totalIntTS.setBorder(Border.RIGHT, BorderLineStyle.THIN, Colour.BLACK);
        _totalIntTS.setBackground(total);
        _totalSubIntTS = new WritableCellFormat(font11);
        _totalSubIntTS.setAlignment(Alignment.CENTRE);
        _totalSubIntTS.setBorder(Border.LEFT, BorderLineStyle.THIN, Colour.BLACK);
        _totalSubIntTS.setBorder(Border.BOTTOM, BorderLineStyle.THIN, Colour.BLACK);
        _totalSubIntTS.setBorder(Border.RIGHT, BorderLineStyle.THIN, Colour.BLACK);
        _totalSubIntTS.setBackground(totalSub);
        _totalRedIntTS = new WritableCellFormat(_totalIntTS);
        _totalRedIntTS.setFont(fontRed11);
        _totalSubRedIntTS = new WritableCellFormat(_totalSubIntTS);
        _totalSubRedIntTS.setFont(fontRedBold11);

        _totalDoubleTS = new WritableCellFormat(_regElDetailDoubleTS);
        _totalDoubleTS.setBorder(Border.TOP, BorderLineStyle.THICK, Colour.BLACK);
        _totalDoubleTS.setBorder(Border.LEFT, BorderLineStyle.THIN, Colour.BLACK);
        _totalDoubleTS.setBorder(Border.BOTTOM, BorderLineStyle.THIN, Colour.BLACK);
        _totalDoubleTS.setBorder(Border.RIGHT, BorderLineStyle.THIN, Colour.BLACK);
        _totalDoubleTS.setFont(fontBold11);
        _totalDoubleTS.setBackground(total);
        _totalSubDoubleTS = new WritableCellFormat(_regElDetailDoubleTS);
        _totalSubDoubleTS.setFont(font11);
        _totalSubDoubleTS.setBackground(totalSub);


        // Суппорт
        _topThickBoardTS = new WritableCellFormat();
        _topThickBoardTS.setBorder(Border.BOTTOM, BorderLineStyle.THICK, Colour.BLACK);

        _rightThickTableBoardTS = new WritableCellFormat();
        _rightThickTableBoardTS.setBorder(Border.LEFT, BorderLineStyle.THICK, Colour.BLACK);

        _bottomThickBoardTS = new WritableCellFormat();
        _bottomThickBoardTS.setBorder(Border.TOP, BorderLineStyle.THICK, Colour.BLACK);
    }


    /**
     * Добавляет название отчета в первую строку
     *
     * @return номер строки после названия
     */
    protected int printTitle(SfaSummaryResult report, WritableSheet sheet)
    {
        int row = 0;

        addTextCell(sheet, 0, row, 10, 1, "Сводный отчет по результатам ГИА на " + report.getEducationYear().getTitle() + " уч. год", _titleStyle);

        return ++row;
    }


    /**
     * Вставляет в начало страници таблицу с параметрами отчета
     *
     * @param row номер строки с которой начнется таблица
     * @return номер строки следующий за таблицей
     */
    protected int printParametersTable(SfaSumReportInfo<R1> reportInfo, WritableSheet sheet, int row) throws WriteException
    {
        SfaSummaryResult report = reportInfo.getReport();

        if (report.getDevelopForm() != null)
            row = addProperty2HeadTable(sheet, row, "Форма освоения", report.getDevelopForm().getTitle(), _parametersTS);

        if (report.getDevelopTech() != null)
            row = addProperty2HeadTable(sheet, row, "Технология освоения", report.getDevelopTech().getTitle(), _parametersTS);

        if (report.getQualification() != null)
            row = addProperty2HeadTable(sheet, row, "Уровень образования", report.getQualification().getTitle(), _parametersTS);

        if (report.getFormativeOrgUnit() != null)
            row = addProperty2HeadTable(sheet, row, "Формирующее подразделение", StringUtils.capitalize(report.getFormativeOrgUnit().getPrintTitle()), _parametersTS);

        if (report.getTerritorialOrgUnit() != null)
            row = addProperty2HeadTable(sheet, row, "Территориальное подразделение", StringUtils.capitalize(report.getTerritorialOrgUnit().getPrintTitle()), _parametersTS);

        if (report.getOwnerOrgUnit() != null)
            row = addProperty2HeadTable(sheet, row, "Выпускающее подразделение", StringUtils.capitalize(report.getOwnerOrgUnit().getPrintTitle()), _parametersTS);

        EducationLevelsHighSchool educationLevelHighSchool = report.getEducationLevelHighSchool();
        if (educationLevelHighSchool != null)
        {
            String programSubjectKindTitle = educationLevelHighSchool.getEducationLevel().getEduProgramSubject().getEduProgramKind().getEduProgramSubjectKindTitle(InflectorVariantCodes.RU_GENITIVE);
            row = addProperty2HeadTable(sheet, row, "Шифр и наименование " + programSubjectKindTitle.toLowerCase(), educationLevelHighSchool.getDisplayableTitle(), _parametersTS);
        }

        if (report.getDevelopCondition() != null)
            row = addProperty2HeadTable(sheet, row, "Условие освоения", report.getDevelopCondition().getTitle(), _parametersTS);

        if (report.getDevelopPeriod() != null)
            row = addProperty2HeadTable(sheet, row, "Нормативный срок освоения", report.getDevelopPeriod().getTitle(), _parametersTS);

        String examsPeriod = report.getExamsPeriod();
        if (!examsPeriod.isEmpty())
            row = addProperty2HeadTable(sheet, row, "Период государственных экзаменов", examsPeriod, _parametersTS);

        return row;
    }

    /**
     * добавляет параметр в таблицу параметров отчета
     *
     * @param row   номер строки с которой начнется таблица
     * @param title название параметра
     * @param value значение параметра
     * @return номер строки следующий за таблицей
     */
    protected int addProperty2HeadTable(WritableSheet sheet, int row, String title, String value, WritableCellFormat format)
    {
        if (value != null && value.length() > 80)
            try
            {
                sheet.setRowView(row, 700);
            }
            catch (RowsExceededException e)
            {
                e.printStackTrace();
            }

        StateFinalAttestationUtil.addTextCell(sheet, 0, row, 3, 1, title, format);
        StateFinalAttestationUtil.addTextCell(sheet, 3, row, 7, 1, value, format);

        return ++row;
    }

    /**
     * Добавляет таблицу с данными отчета
     *
     * @param rowIndex номер строки с которой начнется таблица
     * @return номер строки следующий за таблицей
     */
    protected int addTable(SfaSumReportInfo<R1> reportInfo, WritableSheet sheet, int rowIndex)
    {
        boolean detailByDirections = reportInfo.getReport().isDetailByDirections();

        int startTable = rowIndex;

        rowIndex = detailByDirections ? addHeaderTableDetail(reportInfo, sheet, rowIndex) : addHeaderTable(reportInfo, sheet, rowIndex);
        sheet.getSettings().setVerticalFreeze(rowIndex);

        rowIndex = detailByDirections ? addDataTableDetail(reportInfo, sheet, rowIndex) : addDataTable(reportInfo, sheet, rowIndex);

        fillArea(sheet, tableWidth + 1, tableWidth + 1, startTable, rowIndex - 1, _rightThickTableBoardTS);
        fillArea(sheet, 0, tableWidth, rowIndex, rowIndex, _bottomThickBoardTS);

        return ++rowIndex;
    }


    /**
     * добавляет шапку таблицы данных без детализации
     *
     * @param rowIndex номер строки с которой начнется таблица
     * @return номер строки следующий за шапкой таблицы
     */
    protected abstract int addHeaderTable(SfaSumReportInfo<R1> reportInfo, WritableSheet sheet, int rowIndex);

    /**
     * добавляет таблицу данных без детализаии
     *
     * @param rowIndex номер строки с которой начнется таблица
     * @return номер строки следующий за шапкой таблицы
     */
    protected int addDataTable(SfaSumReportInfo<R1> reportInfo, WritableSheet sheet, int rowIndex)
    {
        Map<EducationOrgUnit, List<R1>> rawsByEou = reportInfo.getRaws().stream().collect(Collectors.groupingBy(
                R1::getEducationOrgUnit,
                Collectors.mapping(raw -> raw, Collectors.toList())
        ));

        TreeMap<OrgUnit, Map<Boolean, List<EducationOrgUnit>>> eouByFouMap = reportInfo.getEducationOrgUnits().stream().collect(Collectors.groupingBy(
                EducationOrgUnit::getFormativeOrgUnit,
                () -> new TreeMap<>(OrgUnitRankComparator.INSTANCE),
                Collectors.mapping(raw -> raw, Collectors.groupingBy(raw -> raw.getTerritorialOrgUnit().isTop(), Collectors.mapping(raw -> raw, Collectors.toList())))
        ));

        int[] fouIndex = {1};
        List<R2> rows = new ArrayList<>();
        List<R2> topRows = new ArrayList<>();
        List<R2> branchesRows = new ArrayList<>();
        for (Map.Entry<OrgUnit, Map<Boolean, List<EducationOrgUnit>>> eouByFou : eouByFouMap.entrySet())
        {
            OrgUnit orgUnit = eouByFou.getKey();
            Map<Boolean, List<EducationOrgUnit>> eouByOrgUnitType = eouByFou.getValue();
            List<EducationOrgUnit> eous4Top = eouByOrgUnitType.get(Boolean.TRUE);
            List<EducationOrgUnit> eous4Branches = eouByOrgUnitType.get(Boolean.FALSE);

            R2 fouRow = newFouRow(reportInfo, String.valueOf(fouIndex[0]++) + ". " + orgUnit.getPrintTitle(), true);
            rows.add(fouRow);
            R2 row4Top = null;
            if (eous4Top != null)
            {
                List<R1> raws4Top = eous4Top.stream()
                        .map(rawsByEou::get)
                        .filter(Objects::nonNull)
                        .flatMap(Collection::stream)
                        .collect(Collectors.toList());
                if (!raws4Top.isEmpty() || !reportInfo.getReport().isSkipEmpty())
                {
                    row4Top = newFouRow(reportInfo, "в т.ч. головной вуз", false);
                    if (!raws4Top.isEmpty())
                    {
                        row4Top.addFromRaw(raws4Top);
                        topRows.add(row4Top);
                        fouRow.addFromRow(row4Top);
                    }
                }
            }

            R2 row4Branches = null;
            if (eous4Branches != null)
            {
                List<R1> raws4Branches = eous4Branches.stream()
                        .map(rawsByEou::get)
                        .filter(Objects::nonNull)
                        .flatMap(Collection::stream)
                        .collect(Collectors.toList());
                if (!raws4Branches.isEmpty() || !reportInfo.getReport().isSkipEmpty())
                {
                    row4Branches = newFouRow(reportInfo, "в т.ч. филиалы", false);
                    if (!raws4Branches.isEmpty())
                    {
                        row4Branches.addFromRaw(raws4Branches);
                        branchesRows.add(row4Branches);
                        fouRow.addFromRow(row4Branches);
                    }
                }
            }

            if (row4Top != null && row4Branches != null)
            {
                rows.add(row4Top);
                rows.add(row4Branches);
            }

            fouRow.calcProgress();
        }

        R2 totalRow = newFinalRow(reportInfo, "ВСЕГО", true);
        rows.add(totalRow);

        R2 totalRow4Top = null;
        if (!topRows.isEmpty() || !reportInfo.getReport().isSkipEmpty())
        {
            totalRow4Top = newFinalRow(reportInfo, "в т.ч. головной вуз", false);
            if (!topRows.isEmpty())
            {
                totalRow4Top.addFromRows(topRows);
                totalRow.addFromRow(totalRow4Top);
            }
        }


        R2 totalRow4Branches = null;
        if (!branchesRows.isEmpty() || !reportInfo.getReport().isSkipEmpty())
        {
            totalRow4Branches = newFinalRow(reportInfo, "в т.ч. филиалы", false);
            if (!branchesRows.isEmpty())
            {
                totalRow4Branches.addFromRows(branchesRows);
                totalRow.addFromRow(totalRow4Branches);
            }
        }


        totalRow.calcProgress();

        if (totalRow4Top != null && totalRow4Branches != null)
        {
            rows.add(totalRow4Top);
            rows.add(totalRow4Branches);
        }

        int[] index = {rowIndex};
        rows.forEach(row -> index[0] = addRow2Table(row, sheet, index[0]));

        return index[0];
    }


    /**
     * добавляет шапку таблицы данных с детализацией
     *
     * @param rowIndex номер строки с которой начнется таблица
     * @return номер строки следующий за шапкой таблицы
     */
    protected abstract int addHeaderTableDetail(SfaSumReportInfo<R1> reportInfo, WritableSheet sheet, int rowIndex);

    /**
     * добавляет таблицу данных с детализацией
     *
     * @param rowIndex номер строки с которой начнется таблица
     * @return номер строки следующий за шапкой таблицы
     */
    protected int addDataTableDetail(SfaSumReportInfo<R1> reportInfo, WritableSheet sheet, int rowIndex)
    {
        TreeMap<OrgUnit, List<R1>> rawByFouMap = reportInfo.getRaws().stream().collect(
                Collectors.groupingBy(R1::getFormativeOrgUnit, () -> new TreeMap<>(OrgUnitRankComparator.INSTANCE), Collectors.mapping(raw -> raw, Collectors.toList())));

        TreeMap<OrgUnit, List<EducationOrgUnit>> eouByFou = reportInfo.getEducationOrgUnits().stream().collect(
                Collectors.groupingBy(EducationOrgUnit::getFormativeOrgUnit, () -> new TreeMap<>(OrgUnitRankComparator.INSTANCE), Collectors.mapping(eou -> eou, Collectors.toList())));

        int fouIndex = 1;
        for (Map.Entry<OrgUnit, List<EducationOrgUnit>> entry : eouByFou.entrySet())
        {
            OrgUnit formativeOrgUnit = entry.getKey();
            List<EducationOrgUnit> eous4Fou = entry.getValue();
            List<R1> raws4Fou = rawByFouMap.get(entry.getKey());

            int startFouIndex = rowIndex;
            rowIndex = addFormativeOrgUnitRows(reportInfo, sheet, rowIndex, formativeOrgUnit, fouIndex, eous4Fou, raws4Fou);
            if (startFouIndex < rowIndex) fouIndex++;
        }

        List<R1> topRaws = reportInfo.getRaws().stream().filter(raw -> raw.getTerritorialOrgUnit().isTop()).collect(Collectors.toList());
        List<R1> branchRaws = reportInfo.getRaws().stream().filter(raw -> !raw.getTerritorialOrgUnit().isTop()).collect(Collectors.toList());

        String topShortTitle = TopOrgUnit.getInstance().getTerritorialShortTitle();
        rowIndex = addTotalRows2Table(sheet, reportInfo, rowIndex, topRaws, ROW_TYPES.DETAIL_FINAL,
                                      q -> getQualificationRowTitle(q).toUpperCase(), "ВСЕГО СТУДЕНТОВ (" + topShortTitle + ")", topShortTitle, _finalTotalStdDtlTS);

        rowIndex = addTotalRows2Table(sheet, reportInfo, rowIndex, branchRaws, ROW_TYPES.DETAIL_FINAL,
                                      q -> getQualificationRowTitle(q).toUpperCase(), "ВСЕГО СТУДЕНТОВ (ФИЛИАЛЫ)", "Филиалы", _finalTotalStdDtlTS);

        rowIndex = addTotalRows2Table(sheet, reportInfo, rowIndex, reportInfo.getRaws(), ROW_TYPES.DETAIL_SVOD,
                                      q -> getQualificationRowTitle(q).toUpperCase(), "ВСЕГО СТУДЕНТОВ", "СВОД", _svodTotalStdDtlTS);

        return rowIndex;
    }

    protected int addFormativeOrgUnitRows(SfaSumReportInfo<R1> reportInfo, WritableSheet sheet, int rowIndex, OrgUnit formativeOrgUnit, int fouIndex, List<EducationOrgUnit> educationOrgUnits, List<R1> raws)
    {
        int startFouIndex = rowIndex;

        String fouTitle = formativeOrgUnit.getPrintTitle();
        TreeMap<OrgUnit, List<R1>> rawsByTouMap = raws == null ? null : raws.stream()
                .collect(Collectors.groupingBy(R1::getTerritorialOrgUnit, () -> new TreeMap<>(TERRITORIAL_ORG_UNIT_COMPARATOR), Collectors.mapping(raw -> raw, Collectors.toList())));
        TreeMap<OrgUnit, List<EducationOrgUnit>> eouByTouMap = educationOrgUnits.stream()
                .collect(Collectors.groupingBy(EducationOrgUnit::getTerritorialOrgUnit, () -> new TreeMap<>(TERRITORIAL_ORG_UNIT_COMPARATOR), Collectors.mapping(eou -> eou, Collectors.toList())));

        if ((rawsByTouMap != null && rawsByTouMap.size() > 0) || (!reportInfo.getReport().isSkipEmpty() && eouByTouMap.size() > 0))
            rowIndex = addOuTitle(sheet, rowIndex, String.valueOf(fouIndex) + ". " + fouTitle, _fouTitleDtlTS);

        for (Map.Entry<OrgUnit, List<EducationOrgUnit>> eouByTou : eouByTouMap.entrySet())
        {
            OrgUnit territorialOrgUnit = eouByTou.getKey();
            List<R1> raws4Tou = rawsByTouMap == null ? null : rawsByTouMap.get(eouByTou.getKey());
            rowIndex = addTerritorialOrgUnitRows(reportInfo, sheet, rowIndex, territorialOrgUnit, eouByTou.getValue(), raws4Tou);
        }

        if (startFouIndex < rowIndex)
        {
            List<R1> branchesRaw = raws == null ? null : raws.stream().filter(row -> !row.getTerritorialOrgUnit().isTop()).collect(Collectors.toList());//всего по филлиалам
            rowIndex = addTotalRows2Table(sheet, reportInfo, rowIndex, branchesRaw, ROW_TYPES.DETAIL_BRANCHES,
                                          q -> getQualificationRowTitle(q) + " (филиалы)", "Всего " + fouTitle + " (филиалы)", "", _tousTotalStdDtlTS);

            rowIndex = addTotalRows2Table(sheet, reportInfo, rowIndex, raws, ROW_TYPES.DETAIL_FOU,
                                          this::getQualificationRowTitle, "Всего " + fouTitle, "СВОД", _fouTotalStdDtlTS);
        }

        return rowIndex;
    }

    protected int addTerritorialOrgUnitRows(SfaSumReportInfo<R1> reportInfo, WritableSheet sheet, int rowIndex, OrgUnit territorialOrgUnit, List<EducationOrgUnit> educationOrgUnits, List<R1> raws)
    {
        TreeMap<EduProgramSubjectIndexGeneration, List<EducationOrgUnit>> eouByGenMap = getEducationOrgUnitByGeneration(educationOrgUnits);

        Map<EducationOrgUnit, List<R1>> rawsByEou = raws == null ? null : raws.stream()
                .collect(Collectors.groupingBy(R1::getEducationOrgUnit, Collectors.mapping(raw -> raw, Collectors.toList())));

        if (((rawsByEou != null && rawsByEou.size() > 0) || (!reportInfo.getReport().isSkipEmpty() && eouByGenMap.size() > 0)) && !territorialOrgUnit.isTop())
            rowIndex = addOuTitle(sheet, rowIndex, territorialOrgUnit.getPrintTitle(), _touTitleDtlTS);

        int startTouIndex = rowIndex;
        for (Map.Entry<EduProgramSubjectIndexGeneration, List<EducationOrgUnit>> eousByGen : eouByGenMap.entrySet())
        {
            Map<EducationLevels, List<EducationOrgUnit>> eou4Gen = eousByGen.getValue().stream()
                    .collect(Collectors.groupingBy(eou -> eou.getEducationLevelHighSchool().getEducationLevel(),
                                                   () -> new TreeMap<>(LEVELS_COMPARATOR),
                                                   Collectors.mapping(raw -> raw, Collectors.toList())));

            int startGen = rowIndex;
            for (Map.Entry<EducationLevels, List<EducationOrgUnit>> eousByLvl : eou4Gen.entrySet())
            {
                EducationLevels levels = eousByLvl.getKey();

                List<R1> raws4Lvl = rawsByEou == null ? null : eousByLvl.getValue().stream().map(rawsByEou::get).filter(Objects::nonNull).flatMap(Collection::stream).collect(Collectors.toList());
                rowIndex = addEducationLevelsRows(reportInfo, sheet, rowIndex, levels, raws4Lvl);
            }

            if (startGen < rowIndex)
                addTextCell(sheet, 0, startGen, 1, rowIndex - startGen, eousByGen.getKey().getTitle(), _generationDtlTS);
        }

        if (startTouIndex < rowIndex)
            rowIndex = addTotalRows2Table(sheet, reportInfo, rowIndex, raws, ROW_TYPES.DETAIL_TOU,
                                          this::getQualificationRowTitle, "Всего " + territorialOrgUnit.getTerritorialShortTitle(), "", _touTotalStdDtlTS);

        return rowIndex;
    }

    protected int addEducationLevelsRows(SfaSumReportInfo<R1> reportInfo, WritableSheet sheet, int rowIndex, EducationLevels levels, List<R1> raws)
    {

        if (CollectionUtils.isEmpty(raws))
        {
            if (!reportInfo.getReport().isSkipEmpty())
            {
                addDirectionCells(sheet, rowIndex, rowIndex + 1, levels);
                fillArea(sheet, 3, tableWidth, rowIndex, rowIndex, _regElDetailIntTS);
                rowIndex++;
            }
            return rowIndex;
        }

        List<R2> rows = getRegistryElementRows(reportInfo, raws);

        R2 totalDir;
        if (rows.size() > 1)
        {
            totalDir = newTotalDirectionRow(reportInfo);
            totalDir.setTitle("Всего по направлению " + levels.getTitleCodePrefix());
            totalDir.addFromRaw(raws);
            rows.add(totalDir);
        }

        int startDir = rowIndex;
        for (R2 row : rows) rowIndex = addDetailRegElRow2Table(row, sheet, rowIndex);
        addDirectionCells(sheet, startDir, rowIndex, levels);

        return rowIndex;
    }


    protected TreeMap<EduProgramSubjectIndexGeneration, List<EducationOrgUnit>> getEducationOrgUnitByGeneration(List<EducationOrgUnit> educationOrgUnits)
    {
        return educationOrgUnits.stream()
                .collect(Collectors.groupingBy(
                        eou ->
                        {
                            EduProgramSubject subject = eou.getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject();
                            EduProgramSubjectIndexGeneration generation;
                            if (subject == null)
                            {
                                generation = new EduProgramSubjectIndexGeneration();
                                generation.setId(0L);
                                generation.setTitle("");
                            }
                            else
                                generation = subject.getSubjectIndex().getGeneration();
                            return generation;
                        },
                        () -> new TreeMap<>(Comparator.comparingInt(EduProgramSubjectIndexGeneration::getPriority)),
                        Collectors.mapping(eou -> eou, Collectors.toList())
                ));
    }

    protected List<R2> getRegistryElementRows(SfaSumReportInfo<R1> reportInfo, List<R1> raws)
    {
        TreeMap<EppRegistryElement, List<R1>> rawsByRegEl = raws.stream().collect(Collectors.groupingBy(
                R1::getRegistryElement,
                () -> new TreeMap<>(CommonCollator.TITLED_WITH_ID_COMPARATOR),
                Collectors.mapping(raw -> raw, Collectors.toList())
        ));

        return rawsByRegEl.entrySet().stream()
                .map(e ->
                     {
                         List<R1> raw4RegEl = e.getValue();
                         R2 row = newRegElRow(reportInfo, raw4RegEl.get(0).getEducationOrgUnit(), e.getKey().getTitle());
                         row.addFromRaw(raw4RegEl);
                         return row;
                     })
                .collect(Collectors.toList());
    }

    protected TreeMap<Qualifications, List<R1>> getRowsByQualifications(Collection<R1> raws)
    {
        return raws.stream().collect(Collectors.groupingBy(
                raw -> raw.getEducationLevels().getQualification(),
                () -> new TreeMap<>(Comparator.comparingInt(Qualifications::getOrder)),
                Collectors.mapping(raw -> raw, Collectors.toList())
        ));
    }

    protected String getQualificationRowTitle(Qualifications qualification)
    {
        switch (qualification.getCode())
        {
            case QualificationsCodes.BAKALAVR:
                return "Всего бакалавров";
            case QualificationsCodes.MAGISTR:
                return "Всего магистров";
            case QualificationsCodes.SPETSIALIST:
                return "Всего специалистов";
            default:
                return "Всего " + qualification.getTitle();
        }
    }


    protected static final Comparator<OrgUnit> TERRITORIAL_ORG_UNIT_COMPARATOR = (o1, o2) ->
    {
        if (o1.isTop())
        {
            if (o2.isTop())
                return 0;
            else
                return -1;
        }
        else if (o2.isTop())
            return 1;

        return OrgUnitRankComparator.INSTANCE.compare(o1, o2);
    };

    protected static final Comparator<EducationLevels> LEVELS_COMPARATOR = (o1, o2) ->
    {
        int result = o1.getTitleCodePrefix().compareTo(o2.getTitleCodePrefix());
        if (result != 0) return result;

        return o1.getTitle().compareTo(o2.getTitle());
    };

    // Билдер оценок ГИА
    /**
     * alias SessionDocumentSlot main
     */
    protected final static String SLOT_ALIAS = "slot";
    /**
     * alias SessionMark inner join
     */
    protected final static String MARK_ALIAS = "mark";
    /**
     * alias EppStudentWorkPlanElement inner join
     */
    protected final static String WPE_ALIAS = "wpe";
    /**
     * alias EppRegistryElement inner join
     */
    protected final static String REG_ALIAS = "reg";
    /**
     * alias Student inner join
     */
    protected final static String STU_ALIAS = "stu";
    /**
     * alias EducationOrgUnit inner join
     */
    protected final static String EOU_ALIAS = "eou";

    /**
     * @return билдер НПП
     */
    protected DQLSelectBuilder getEducationOrgUnitSelectBuilder(SfaSumReportInfo<R1> reportInfo)
    {
        SfaSummaryResult report = reportInfo.getReport();

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EducationOrgUnit.class, EOU_ALIAS)
                .column(EOU_ALIAS);

        FilterUtils.applySelectFilter(builder, EOU_ALIAS, EducationOrgUnit.educationLevelHighSchool().educationLevel().qualification(), report.getQualification());
        FilterUtils.applySelectFilter(builder, EOU_ALIAS, EducationOrgUnit.formativeOrgUnit(), report.getFormativeOrgUnit());
        FilterUtils.applySelectFilter(builder, EOU_ALIAS, EducationOrgUnit.territorialOrgUnit(), report.getTerritorialOrgUnit());
        FilterUtils.applySelectFilter(builder, EOU_ALIAS, EducationOrgUnit.educationLevelHighSchool().orgUnit(), report.getOwnerOrgUnit());
        FilterUtils.applySelectFilter(builder, EOU_ALIAS, EducationOrgUnit.educationLevelHighSchool(), report.getEducationLevelHighSchool());
        FilterUtils.applySelectFilter(builder, EOU_ALIAS, EducationOrgUnit.developForm(), report.getDevelopForm());
        FilterUtils.applySelectFilter(builder, EOU_ALIAS, EducationOrgUnit.developCondition(), report.getDevelopCondition());
        FilterUtils.applySelectFilter(builder, EOU_ALIAS, EducationOrgUnit.developTech(), report.getDevelopTech());
        FilterUtils.applySelectFilter(builder, EOU_ALIAS, EducationOrgUnit.developPeriod(), report.getDevelopPeriod());

        return builder;
    }

    /**
     * билдер записей мероприятия студентов
     */
    protected DQLSelectBuilder getSlotSelectBuilder(SfaSumReportInfo<R1> reportInfo, DQLSelectBuilder eouBuilder)
    {
        SfaSummaryResult report = reportInfo.getReport();

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(SessionDocumentSlot.class, SLOT_ALIAS)
                .where(in(property(SLOT_ALIAS, SessionDocumentSlot.studentWpeCAction().studentWpe().student().educationOrgUnit()), eouBuilder.buildQuery()));

        builder.joinEntity(SLOT_ALIAS, DQLJoinType.inner, SessionSlotRegularMark.class, MARK_ALIAS,
                           and(eq(property(MARK_ALIAS, SessionSlotRegularMark.slot()), property(SLOT_ALIAS)),
                               betweenDays(SessionSlotRegularMark.performDate().fromAlias(MARK_ALIAS), report.getDateFrom(), report.getDateTo()))
        );

        builder.joinPath(DQLJoinType.inner, SessionDocumentSlot.studentWpeCAction().studentWpe().fromAlias(SLOT_ALIAS), WPE_ALIAS)
                .where(eq(property(WPE_ALIAS, EppStudentWorkPlanElement.year().educationYear()), value(report.getEducationYear())));

        builder.joinPath(DQLJoinType.inner, EppStudentWorkPlanElement.registryElementPart().registryElement().fromAlias(WPE_ALIAS), REG_ALIAS);

        builder.joinPath(DQLJoinType.inner, EppStudentWorkPlanElement.student().fromAlias(WPE_ALIAS), STU_ALIAS)
                .where(eq(property(STU_ALIAS, Student.archival()), value(Boolean.FALSE)));

        builder.joinPath(DQLJoinType.inner, Student.educationOrgUnit().fromAlias(STU_ALIAS), EOU_ALIAS);

        return builder;
    }


    // Стили ячеек
    protected WritableCellFormat _titleStyle;
    protected WritableCellFormat _subTitleStyle;

    protected WritableCellFormat _parametersTS;

    protected WritableCellFormat _horHeaderDataTS;
    protected WritableCellFormat _verHeaderDataTS;
    protected WritableCellFormat _horHeaderBoldDataTS;

    protected WritableCellFormat _regElDetailIntTS;
    protected WritableCellFormat _regElDetailRedIntTS;
    protected WritableCellFormat _regElTitleDetailTS;
    protected WritableCellFormat _regElDetailDoubleTS;

    // detail
    protected WritableCellFormat _generationDtlTS;

    protected WritableCellFormat _lvlPrefixDtlTS;
    protected WritableCellFormat _lvlPrefixMagDtlTS;
    protected WritableCellFormat _lvlPrefixSpecDtlTS;
    protected WritableCellFormat _lvlTitleDtlTS;
    protected WritableCellFormat _lvlTitleMagDtlTS;
    protected WritableCellFormat _lvlTitleSpecDtlTS;

    protected WritableCellFormat _fouTitleDtlTS;
    protected WritableCellFormat _fouTotalStdDtlTS;
    protected WritableCellFormat _fouTotalCellDtlTS;
    protected WritableCellFormat _fouTotalCellThickTopDtlTS;
    protected WritableCellFormat _fouTotalIntDtlTS;
    protected WritableCellFormat _fouTotalIntThickTopDtlTS;
    protected WritableCellFormat _fouTotalRedIntDtlTS;
    protected WritableCellFormat _fouTotalRedIntThickTopDtlTS;
    protected WritableCellFormat _fouTotalDoubleDtlTS;
    protected WritableCellFormat _fouTotalDoubleThickTopDtlTS;

    protected WritableCellFormat _touTitleDtlTS;
    protected WritableCellFormat _touTotalStdDtlTS;
    protected WritableCellFormat _touTotalCellDtlTS;
    protected WritableCellFormat _touTotalCellThickTopDtlTS;
    protected WritableCellFormat _touTotalIntDtlTS;
    protected WritableCellFormat _touTotalIntThickTopDtlTS;
    protected WritableCellFormat _touTotalRedIntDtlTS;
    protected WritableCellFormat _touTotalRedIntThickTopDtlTS;
    protected WritableCellFormat _touTotalDoubleDtlTS;
    protected WritableCellFormat _touTotalDoubleThickTopDtlTS;

    protected WritableCellFormat _tousTotalStdDtlTS;
    protected WritableCellFormat _tousTotalCellDtlTS;
    protected WritableCellFormat _tousTotalCellThickTopDtlTS;
    protected WritableCellFormat _tousTotalIntDtlTS;
    protected WritableCellFormat _tousTotalIntThickTopDtlTS;
    protected WritableCellFormat _tousTotalRedIntDtlTS;
    protected WritableCellFormat _tousTotalRedIntThickTopDtlTS;
    protected WritableCellFormat _tousTotalDoubleDtlTS;
    protected WritableCellFormat _tousTotalDoubleThickTopDtlTS;

    protected WritableCellFormat _finalTotalStdDtlTS;
    protected WritableCellFormat _finalTotalCellDtlTS;
    protected WritableCellFormat _finalTotalCellThickTopDtlTS;
    protected WritableCellFormat _finalTotalIntDtlTS;
    protected WritableCellFormat _finalTotalIntThickTopDtlTS;
    protected WritableCellFormat _finalTotalRedIntDtlTS;
    protected WritableCellFormat _finalTotalRedIntThickTopDtlTS;
    protected WritableCellFormat _finalTotalDoubleDtlTS;
    protected WritableCellFormat _finalTotalDoubleThickTopDtlTS;

    protected WritableCellFormat _svodTotalStdDtlTS;
    protected WritableCellFormat _svodTotalCellDtlTS;
    protected WritableCellFormat _svodTotalCellThickTopDtlTS;
    protected WritableCellFormat _svodTotalIntDtlTS;
    protected WritableCellFormat _svodTotalIntThickTopDtlTS;
    protected WritableCellFormat _svodTotalRedIntDtlTS;
    protected WritableCellFormat _svodTotalRedIntThickTopDtlTS;
    protected WritableCellFormat _svodTotalDoubleDtlTS;
    protected WritableCellFormat _svodTotalDoubleThickTopDtlTS;

    // not detail
    protected WritableCellFormat _fouTitleTS;
    protected WritableCellFormat _fouSubTitleTS;
    protected WritableCellFormat _fouIntTS;
    protected WritableCellFormat _fouSubIntTS;
    protected WritableCellFormat _fouRedIntTS;
    protected WritableCellFormat _fouSubRedIntTS;
    protected WritableCellFormat _fouDoubleTS;
    protected WritableCellFormat _fouSubDoubleTS;

    protected WritableCellFormat _totalTitleTS;
    protected WritableCellFormat _totalSubTitleTS;
    protected WritableCellFormat _totalIntTS;
    protected WritableCellFormat _totalSubIntTS;
    protected WritableCellFormat _totalRedIntTS;
    protected WritableCellFormat _totalSubRedIntTS;
    protected WritableCellFormat _totalDoubleTS;
    protected WritableCellFormat _totalSubDoubleTS;


    protected WritableCellFormat _topThickBoardTS;
    protected WritableCellFormat _bottomThickBoardTS;
    protected WritableCellFormat _rightThickTableBoardTS;

    protected enum ROW_TYPES
    {
        NOT_DETAIL_FOU, NOT_DETAIL_FINAL,
        DETAIL_TOU, DETAIL_BRANCHES, DETAIL_FOU, DETAIL_FINAL, DETAIL_SVOD
    }

    protected R2 newTotalRow(SfaSumReportInfo<R1> reportInfo, String title, boolean first, ROW_TYPES type)
    {
        switch (type)
        {
            case DETAIL_TOU:
                return newDetailTouRow(reportInfo, title, first);
            case DETAIL_BRANCHES:
                return newDetailBranchesRow(reportInfo,  title, first);
            case DETAIL_FOU:
                return newDetailFouRow(reportInfo,  title, first);
            case DETAIL_FINAL:
                return newDetailFinalRow(reportInfo, title, first);
            case DETAIL_SVOD:
                return newDetailSvodRow(reportInfo, title, first);
            default:
                return null;
        }
    }

    /**
     * Создает строки "Всего" и "Всего по квалификации" из списка строк
     *
     * @param rowIndex           номер строки с которой добавляются строки
     * @param raws               суммируемые строки
     * @param type               тип результирующих строк
     * @param qualificationTitle преобразует квалификацию в название строки
     * @param totalTitle         название строки полной суммы
     * @param groupTitle         текст для групирующей ячейки
     * @param groupStyle         стиль для групирующей ячейки
     * @return номер строки следующий за добавленными строками
     */
    protected int addTotalRows2Table(WritableSheet sheet, SfaSumReportInfo<R1> reportInfo, int rowIndex,
                                     Collection<R1> raws, ROW_TYPES type,
                                     Function<Qualifications, String> qualificationTitle,
                                     String totalTitle, String groupTitle, WritableCellFormat groupStyle)
    {
        if(CollectionUtils.isEmpty(raws)) return rowIndex;

        int startIndex = rowIndex;
        boolean first = true;
        for (Map.Entry<Qualifications, List<R1>> entry : getRowsByQualifications(raws).entrySet())
        {
            R2 totalQualRow = newTotalRow(reportInfo, qualificationTitle.apply(entry.getKey()), first, type);
            totalQualRow.addFromRaw(entry.getValue());
            rowIndex = addDetailTotalRow2Table(totalQualRow, sheet, rowIndex);
            if (first) first = false;
        }
        R2 totalRow = newTotalRow(reportInfo, totalTitle, first, type);
        totalRow.addFromRaw(raws);
        rowIndex = addDetailTotalRow2Table(totalRow, sheet, rowIndex);
        addTextCell(sheet, 0, startIndex, 1, rowIndex - startIndex, groupTitle, groupStyle);
        return rowIndex;
    }


    // Не Детализованные строки

    /**
     * Строка формирующего подразделения
     */
    protected abstract R2 newFouRow(SfaSumReportInfo<R1> reportInfo, String title, boolean first);

    /**
     * Итоговая строка
     */
    protected abstract R2 newFinalRow(SfaSumReportInfo<R1> reportInfo, String title, boolean first);


    // Детализованные строки

    /**
     * Строка мероприятия
     */
    protected abstract R2 newRegElRow(SfaSumReportInfo<R1> reportInfo, EducationOrgUnit educationOrgUnit, String title);

    /**
     * Строка "Всего по направлению"
     */
    protected abstract R2 newTotalDirectionRow(SfaSumReportInfo<R1> reportInfo);

    /**
     * Строка территориального подразделения
     */
    protected abstract R2 newDetailTouRow(SfaSumReportInfo<R1> reportInfo, String title, boolean first);

    /**
     * Строка Всего по филлиалам на формирующем
     */
    protected abstract R2 newDetailBranchesRow(SfaSumReportInfo<R1> reportInfo, String title, boolean first);

    /**
     * Строка Всего по формирующему
     */
    protected abstract R2 newDetailFouRow(SfaSumReportInfo<R1> reportInfo, String title, boolean first);

    /**
     * Строка Всего по всем филиалам или по Топу
     */
    protected abstract R2 newDetailFinalRow(SfaSumReportInfo<R1> reportInfo, String title, boolean first);

    /**
     * Строка СВОД
     */
    protected abstract R2 newDetailSvodRow(SfaSumReportInfo<R1> reportInfo, String title, boolean first);


    // Суппорт печати

    protected int addOuTitle(WritableSheet sheet, int row, String title, CellFormat format)
    {
        try
        {
            sheet.setRowView(row, 350);
        }
        catch (RowsExceededException e)
        {
            e.printStackTrace();
        }

        fillArea(sheet, 0, 1, row, row, format);
        addTextCell(sheet, 2, row, tableWidth - 1, 1, title, format);

        return ++row;
    }

    protected void addDirectionCells(WritableSheet sheet, int startRow, int row, EducationLevels levels)
    {
        WritableCellFormat lvlPrefixTS;
        WritableCellFormat lvlTitleTS;

        Qualifications qualification = levels.getQualification();
        if (qualification == null)
        {
            lvlPrefixTS = _lvlPrefixDtlTS;
            lvlTitleTS = _lvlTitleDtlTS;
        }
        else
            switch (qualification.getCode())
            {
                case QualificationsCodes.MAGISTR:
                    lvlPrefixTS = _lvlPrefixMagDtlTS;
                    lvlTitleTS = _lvlTitleMagDtlTS;
                    break;
                case QualificationsCodes.SPETSIALIST:
                    lvlPrefixTS = _lvlPrefixSpecDtlTS;
                    lvlTitleTS = _lvlTitleSpecDtlTS;
                    break;
                default:
                    lvlPrefixTS = _lvlPrefixDtlTS;
                    lvlTitleTS = _lvlTitleDtlTS;
            }

        addTextCell(sheet, 1, startRow, 1, row - startRow, levels.getTitleCodePrefix(), lvlPrefixTS);
        addTextCell(sheet, 2, startRow, 1, row - startRow, levels.getTitle(), lvlTitleTS);
    }

    /**
     * Добалвяет строку мероприятия в детализованную таблицу
     */
    protected int addDetailRegElRow2Table(R2 row, WritableSheet sheet, int rowIndex)
    {
        int col = 3;

        try
        {
            sheet.addCell(new Label(col++, rowIndex, row.getTitle(), row.getTitleStyle()));
            sheet.addCell(new Label(col++, rowIndex, row.getDates(), row.getIntStyle()));
        }
        catch (WriteException e)
        {
            e.printStackTrace();
        }

        addDataRow(sheet, col, rowIndex, row);

        return ++rowIndex;
    }

    /**
     * Добалвяет суммарую строку в детализованную таблицу
     */
    protected int addDetailTotalRow2Table(R2 row, WritableSheet sheet, int rowIndex)
    {
        try
        {
            sheet.setRowView(rowIndex, 350);
        }
        catch (RowsExceededException e)
        {
            e.printStackTrace();
        }

        int col = 5;

        StateFinalAttestationUtil.addTextCell(sheet, 1, rowIndex, 4, 1, row.getTitle(), row.getTitleStyle());

        addDataRow(sheet, col, rowIndex, row);

        return ++rowIndex;
    }

    /**
     * добавляет строку в не детализованную таблицу
     */
    protected int addRow2Table(R2 row, WritableSheet sheet, int rowIndex)
    {
        try
        {
            sheet.setRowView(rowIndex, 350);
        }
        catch (RowsExceededException e)
        {
            e.printStackTrace();
        }

        int col = 0;

        addTextCell(sheet, col++, rowIndex, 1, 1, row.getTitle(), row.getTitleStyle());

        addDataRow(sheet, col, rowIndex, row);

        return ++rowIndex;
    }

    /**
     * Добавляет числовые данные строки в таблицу
     */
    protected abstract void addDataRow(WritableSheet sheet, int startCol, int rowIndex, R2 row);
}
