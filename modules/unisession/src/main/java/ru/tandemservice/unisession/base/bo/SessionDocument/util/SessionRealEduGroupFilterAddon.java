/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionDocument.util;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Transformer;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.dsl.MetaDSLPath;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import org.tandemframework.tapsupport.component.selection.SimpleListResultBuilder;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.education.gen.EducationOrgUnitGen;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.entity.orgstruct.gen.GroupGen;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeALT;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.unisession.attestation.bo.AttestationBulletin.AttestationBulletinManager;
import ru.tandemservice.unisession.base.bo.SessionReport.util.GroupModel;

import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import static java.util.Arrays.asList;
import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 10.12.2015
 */
public class SessionRealEduGroupFilterAddon extends CommonFilterAddon<EppRealEduGroupRow>
{
    public final static String UTIL_NAME = SessionRealEduGroupFilterAddon.class.getSimpleName();

    private IUIPresenter _presenter;

    private AddonItemsFilter _customFilter;

	private boolean _needRefresh = true;
	private boolean _initialRefreshPerformed = false;

	@Override
	public void onComponentRefresh()
	{
		if (_needRefresh || !_initialRefreshPerformed)
		{
			super.onComponentRefresh();
			_initialRefreshPerformed = true;
		}
	}

    public SessionRealEduGroupFilterAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
        _presenter = presenter;
    }

    @Override
    public Class<EppRealEduGroupRow> getEntityClass()
    {
        return EppRealEduGroupRow.class;
    }

    @Override
    protected DQLSelectBuilder applyFilters(DQLSelectBuilder builder, String alias, ICommonFilterItem filter)
    {
        DQLSelectBuilder dql = super.applyFilters(builder, alias, filter);
        if (_customFilter != null)
            _customFilter.filterItems(builder, alias, filter, _presenter);
        return dql;
    }

    public SessionRealEduGroupFilterAddon customFilter(AddonItemsFilter _customFilter)
    {
        this._customFilter = _customFilter;
        return this;
    }

    public static final String SETTINGS_NAME_DEVELOP_FORM = "developForm";
    public static final String SETTINGS_NAME_DEVELOP_CONDITION = "developCondition";
    public static final String SETTINGS_NAME_EDU_LEVEL = "eduLevel";
    public static final String SETTINGS_NAME_TERRITORIAL_ORG_UNIT = "territorialOrgUnit";
    public static final String SETTINGS_NAME_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String SETTINGS_NAME_COURSE = "course";
    public static final String SETTINGS_NAME_GROUP = "group";
    public static final String SETTINGS_NAME_DISCIPLINE = "discipline";
    public static final String SETTINGS_NAME_LOAD_TYPE = "loadType";

    private static EducationOrgUnitGen.Path<EducationOrgUnit> orgUnitPath = EppRealEduGroupRow.studentWpePart().studentWpe().student().educationOrgUnit();

    private static <T extends IEntity, U> CommonFilterContentConfig<T> filterConfig(Class<T> entityClass, String settingsName, String displayName, MetaDSLPath entityPath, MetaDSLPath<U> propertyPath)
    {
        return new CommonFilterContentConfig<>(entityClass, settingsName, displayName, entityPath, propertyPath, asList(propertyPath), asList(propertyPath));
    }

    private static <T extends IEntity, U> CommonFilterContentConfig<T> filterConfig(Class<T> entityClass, String settingsName, String displayName, MetaDSLPath entityPath, MetaDSLPath<U> propertyPath, List<? extends MetaDSLPath> filterAndOrderProps)
    {
        return new CommonFilterContentConfig<>(entityClass, settingsName, displayName, entityPath, propertyPath, filterAndOrderProps, filterAndOrderProps);
    }

    public static final CommonFilterContentConfig<DevelopForm> DEVELOP_FORM = filterConfig(DevelopForm.class, SETTINGS_NAME_DEVELOP_FORM, "Форма освоения", orgUnitPath.developForm(), DevelopForm.title());
    public static final CommonFilterContentConfig<DevelopCondition> DEVELOP_CONDITION = filterConfig(DevelopCondition.class, SETTINGS_NAME_DEVELOP_CONDITION, "Условие освоения", orgUnitPath.developCondition(), DevelopCondition.title());

    public static final CommonFilterContentConfig<EducationLevelsHighSchool> EDU_LEVEL = filterConfig(EducationLevelsHighSchool.class, SETTINGS_NAME_EDU_LEVEL, "Направление подготовки (специальность)", orgUnitPath.educationLevelHighSchool(), EducationLevelsHighSchool.displayableTitle(), asList(EducationLevelsHighSchool.code(), EducationLevelsHighSchool.title()));
    public static final CommonFilterContentConfig<OrgUnit> TERRITORIAL_ORG_UNIT = filterConfig(OrgUnit.class, SETTINGS_NAME_TERRITORIAL_ORG_UNIT, "Территориальное подразделение", orgUnitPath.territorialOrgUnit(), OrgUnit.territorialFullTitle());
    public static final CommonFilterContentConfig<OrgUnit> FORMATIVE_ORG_UNIT = filterConfig(OrgUnit.class, SETTINGS_NAME_FORMATIVE_ORG_UNIT, "Формирующее подразделение", orgUnitPath.formativeOrgUnit(), OrgUnit.fullTitle());

    public static final CommonFilterContentConfig<Course> COURSE = filterConfig(Course.class, SETTINGS_NAME_COURSE, "Курс", EppRealEduGroupRow.studentWpePart().studentWpe().course(), Course.title());

    public static final ICommonFilterFactory<IEntity> GROUP = (GroupFilterItem::new);
    public static final ICommonFilterFactory<IEntity> DISCIPLINE = (DisciplineFilterItem::new);

	public boolean isNeedRefresh()
	{
		return _needRefresh;
	}

	public void setNeedRefresh(boolean needRefresh)
	{
		_needRefresh = needRefresh;
	}

    public static class GroupFilterItem extends CustomFilterItem
    {
        private static final GroupGen.Path<Group> studentGroupPath = EppRealEduGroupRow.studentWpePart().studentWpe().student().group();

        public GroupFilterItem(CommonFilterFormConfig config)
        {
            super(config, "Группа", SETTINGS_NAME_GROUP);
        }

        @Override
        public void apply(DQLSelectBuilder builder, String alias)
        {
            final Object value = getValue();
            if (value instanceof List)
            {
                Transformer<IdentifiableWrapper, Long> transformer = IdentifiableWrapper::getId;
                if (((List) value).contains(GroupModel.WRAPPER_NO_GROUP))
                    builder.where(or(
                            isNull(property(alias, studentGroupPath)),
                            in(property(alias, studentGroupPath.id()), CollectionUtils.collect((List) value, transformer))
                    ));
                else
                    builder.where(in(property(alias, studentGroupPath.id()), CollectionUtils.collect((List) value, transformer)));
            }
            else if (value instanceof IdentifiableWrapper)
            {
                if (GroupModel.WRAPPER_NO_GROUP.equals(value))
                    builder.where(isNull(property(alias, studentGroupPath)));
                else
                    builder.where(eq(property(alias, studentGroupPath.id()), value(((IdentifiableWrapper) value).getId())));
            }
        }

        @Override
        public void init(final CommonFilterAddon commonFilterAddon, String alias)
        {
            if (null != getModel()) return;
            setModel(new CommonFilterSelectModel()
            {
                @Override
                protected IListResultBuilder createBuilder(String filter, Object o)
                {
                    DQLSelectBuilder builder = commonFilterAddon.prepareEntityBuilder(ALIAS, GroupFilterItem.this);

                    FilterUtils.applyLikeFilter(builder, filter, studentGroupPath.title().fromAlias(ALIAS));
                    FilterUtils.applySelectFilter(builder, ALIAS, studentGroupPath.id(), o);

                    builder.column(property(ALIAS, studentGroupPath));
                    builder.column(property(ALIAS, EppRealEduGroupRow.studentWpePart().studentWpe().student()));


                    final List<Object[]> list = builder.createStatement(commonFilterAddon.getSession()).list();

                    Set<IdentifiableWrapper> groups = new TreeSet<>(Comparator.nullsLast(Comparator.comparing((IdentifiableWrapper wrapper) -> wrapper.getTitle(), String.CASE_INSENSITIVE_ORDER)
                            .thenComparing(IdentifiableWrapper::getId)));

                    boolean containsNoGroup = false;

                    for (Object[] objects : list)
                    {
                        groups.add(new IdentifiableWrapper<>((Group) objects[0]));
                        if (null == objects[0])
                            containsNoGroup = true;
                    }
                    if (containsNoGroup)
                        groups.add(GroupModel.WRAPPER_NO_GROUP);

                    return new SimpleListResultBuilder<>(groups);
                }
            });
        }
    }

    public static class DisciplineFilterItem extends CustomFilterItem
    {
        public DisciplineFilterItem(CommonFilterFormConfig config)
        {
            super(config, "Дисциплина", SETTINGS_NAME_DISCIPLINE);
        }

        @Override
        @SuppressWarnings("unchecked")
        public void apply(DQLSelectBuilder builder, String alias)
        {
            final Object value = getValue();
            if (value instanceof List)
                builder.where(in(property(alias, EppRealEduGroupRow.group().activityPart().id()), (CollectionUtils.collect((List<IdentifiableWrapper>) value, IdentifiableWrapper::getId))));
            else if (value instanceof DisciplineWrapper)
                builder.where(eq(property(alias, EppRealEduGroupRow.group().activityPart()), value(((DisciplineWrapper) value).getValue())));
        }

        @Override
        public void init(CommonFilterAddon commonFilterAddon, String alias)
        {
            if (null != getModel()) return;

            setModel(new CommonFilterSelectModel()
            {
                @Override
                protected IListResultBuilder createBuilder(String filter, Object o)
                {
                    // FIXME Почему достаются ЧАСТИ, когда фильтр по ДИСЦИПЛИНЕ

                    DQLSelectBuilder builder = commonFilterAddon.prepareEntityBuilder(ALIAS, DisciplineFilterItem.this).column(property("disc"))
                            .joinPath(DQLJoinType.inner, EppRealEduGroupRow.group().activityPart().fromAlias(ALIAS), "disc")
                            .joinPath(DQLJoinType.inner, EppRegistryElementPart.registryElement().fromAlias("disc"), "e");

                    if (!StringUtils.isEmpty(filter))
                    {
                        filter = CoreStringUtils.escapeLike(filter);
                        // TODO use FilterUtils
                        builder.where(
                                or(
                                        likeUpper(property("e", EppRegistryElement.title()), value(filter)),
                                        likeUpper(property("e", EppRegistryElement.shortTitle()), value(filter)),
                                        likeUpper(property("e", EppRegistryElement.fullTitle()), value(filter)),
                                        likeUpper(property("e", EppRegistryElement.number()), value(filter))
                                )
                        );
                    }

                    FilterUtils.applySelectFilter(builder, "disc", EppRegistryElementPart.id(), o);

                    builder
                            .order(property("e", EppRegistryElement.title()))
                            .order(property("e", EppRegistryElement.number()))
                            .order(property("e", EppRegistryElement.size()))
                            .order(property("e", EppRegistryElement.parts()));

                    final List<EppRegistryElementPart> list = builder.createStatement(commonFilterAddon.getSession()).list();
                    Set<DisciplineWrapper> wrappedSet = new TreeSet<>((DisciplineWrapper a, DisciplineWrapper b) -> {
                        int res = a.getTitle().compareToIgnoreCase(b.getTitle());
                        if (res != 0) return res;
                        return a.getId().compareTo(b.getId());
                    });

                    list.forEach((EppRegistryElementPart element) -> wrappedSet.add(new DisciplineWrapper<>(element, element.getTitleWithNumber())));
                    return new SimpleListResultBuilder<>(wrappedSet);
                }
            });
        }
    }

    public static class LoadTypeFilterItem extends CustomFilterItem
    {
        private final Class<? extends EppGroupType> _typeClazz;

        public LoadTypeFilterItem(CommonFilterFormConfig config, String title, Class<? extends EppGroupType> typeClazz)
        {
            super(config, title, SETTINGS_NAME_LOAD_TYPE);
            _typeClazz = typeClazz;
        }

        @Override
        @SuppressWarnings("unchecked")
        public void apply(DQLSelectBuilder builder, String alias)
        {
            final Object value = getValue();
            if (value instanceof List)
                builder.where(in(property(alias, EppRealEduGroupRow.group().type().id()), (CollectionUtils.collect((List<IdentifiableWrapper>) value, IdentifiableWrapper::getId))));
            else
                builder.where(eq(property(alias, EppRealEduGroupRow.group().type()), value((EppGroupType) value)));
        }

        @Override
        public void init(CommonFilterAddon commonFilterAddon, String alias)
        {
            if (null != getModel()) return;

            setModel(new CommonFilterSelectModel()
            {
                @Override
                protected IListResultBuilder createBuilder(String filter, Object o)
                {
                    DQLSelectBuilder builder = commonFilterAddon.prepareEntityBuilder(ALIAS, LoadTypeFilterItem.this).column(property("t"))
                            .joinPath(DQLJoinType.inner, EppRealEduGroupRow.group().type().fromAlias(ALIAS), "t")
                            .where(instanceOf("t", _typeClazz));

                    if (_typeClazz.equals(EppGroupTypeALT.class))
                        AttestationBulletinManager.instance().dao().filterExistsPriorityLoadType(ALIAS, builder);

                    DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(_typeClazz, "gt")
                            .where(exists(builder.where(eq(property("gt"), property("t"))).buildQuery()))
                            .order(property("gt", EppGroupType.priority()));

                    FilterUtils.applyLikeFilter(builder, filter, EppGroupType.title().fromAlias("gt"));
                    FilterUtils.applySelectFilter(builder, "gt", EppGroupType.id(), o);

                    return new SimpleListResultBuilder<>(dql.createStatement(commonFilterAddon.getSession()).list());
                }
            });
        }
    }

    // WTF?
    // FIXME почему DisciplineWrapper (дисциплина), а T extends EppRegistryElementPart (часть)
    public static class DisciplineWrapper<T extends EppRegistryElementPart> extends IdentifiableWrapper
    {
        private T value;

        protected DisciplineWrapper(T entity, String title)
        {
            super(entity, title);
            value = entity;
        }

        public T getValue()
        {
            return value;
        }
    }

    public interface AddonItemsFilter
    {
        void filterItems(DQLSelectBuilder dql, String alias, ICommonFilterItem item, IUIPresenter presenter);
    }
}
