package ru.tandemservice.unisession.component.sessionListDocument.SessionListDocumentAddEdit;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.tandemframework.core.entity.IdentifiableWrapper;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.util.selectModel.SessionTermModel;

import java.util.ArrayList;
import java.util.List;

/**
 * @author iolshvang
 * @since 25.04.11 08:54
 */
public class TermWithActionSlot extends IdentifiableWrapper
{
    private SessionTermModel.TermWrapper _term;
    private List<EppStudentWpeCAction> _eppSlotList = new ArrayList<>();
    private Long _id;

    public TermWithActionSlot()
    {
        super((Long) null, "");
    }

    public Long getId()
    {
        if (null == _id)
	        _id = (long) System.identityHashCode(this);
        return _id;
    }

    public SessionTermModel.TermWrapper getTerm()
    {
        return _term;
    }

    public void setTerm(final SessionTermModel.TermWrapper term)
    {
        this._term = term;
    }

    public List<EppStudentWpeCAction> getEppSlotList()
    {
        return _eppSlotList;
    }

    public void setEppSlotList(final List<EppStudentWpeCAction> eppSlotList)
    {
        this._eppSlotList = eppSlotList;
    }

    public MultiKey getRowFullKey()
    {
        return new MultiKey(new Long[] { getTerm().getPart().getId(), getTerm().getYear().getId() }, false);
    }
}