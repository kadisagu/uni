package ru.tandemservice.unisession.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unisession.entity.catalog.SessionAttestationMarkCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionCurrentMarkScale;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Оценка из шкалы текущего контроля
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionAttestationMarkCatalogItemGen extends EntityBase
 implements INaturalIdentifiable<SessionAttestationMarkCatalogItemGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.catalog.SessionAttestationMarkCatalogItem";
    public static final String ENTITY_NAME = "sessionAttestationMarkCatalogItem";
    public static final int VERSION_HASH = -1093985864;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_PRINT_TITLE = "printTitle";
    public static final String L_SCALE = "scale";
    public static final String P_PRIORITY = "priority";
    public static final String P_POSITIVE = "positive";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private String _shortTitle;     // Сокращенное название
    private String _printTitle;     // Печатное название
    private SessionCurrentMarkScale _scale;     // Шкала оценок текущего контроля
    private int _priority;     // Приоритет
    private boolean _positive;     // Положительная
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название. Свойство не может быть null.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return Печатное название.
     */
    @Length(max=255)
    public String getPrintTitle()
    {
        return _printTitle;
    }

    /**
     * @param printTitle Печатное название.
     */
    public void setPrintTitle(String printTitle)
    {
        dirty(_printTitle, printTitle);
        _printTitle = printTitle;
    }

    /**
     * @return Шкала оценок текущего контроля. Свойство не может быть null.
     */
    @NotNull
    public SessionCurrentMarkScale getScale()
    {
        return _scale;
    }

    /**
     * @param scale Шкала оценок текущего контроля. Свойство не может быть null.
     */
    public void setScale(SessionCurrentMarkScale scale)
    {
        dirty(_scale, scale);
        _scale = scale;
    }

    /**
     * @return Приоритет. Свойство не может быть null.
     */
    @NotNull
    public int getPriority()
    {
        return _priority;
    }

    /**
     * @param priority Приоритет. Свойство не может быть null.
     */
    public void setPriority(int priority)
    {
        dirty(_priority, priority);
        _priority = priority;
    }

    /**
     * @return Положительная. Свойство не может быть null.
     */
    @NotNull
    public boolean isPositive()
    {
        return _positive;
    }

    /**
     * @param positive Положительная. Свойство не может быть null.
     */
    public void setPositive(boolean positive)
    {
        dirty(_positive, positive);
        _positive = positive;
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SessionAttestationMarkCatalogItemGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((SessionAttestationMarkCatalogItem)another).getCode());
            }
            setShortTitle(((SessionAttestationMarkCatalogItem)another).getShortTitle());
            setPrintTitle(((SessionAttestationMarkCatalogItem)another).getPrintTitle());
            setScale(((SessionAttestationMarkCatalogItem)another).getScale());
            setPriority(((SessionAttestationMarkCatalogItem)another).getPriority());
            setPositive(((SessionAttestationMarkCatalogItem)another).isPositive());
            setTitle(((SessionAttestationMarkCatalogItem)another).getTitle());
        }
    }

    public INaturalId<SessionAttestationMarkCatalogItemGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<SessionAttestationMarkCatalogItemGen>
    {
        private static final String PROXY_NAME = "SessionAttestationMarkCatalogItemNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof SessionAttestationMarkCatalogItemGen.NaturalId) ) return false;

            SessionAttestationMarkCatalogItemGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionAttestationMarkCatalogItemGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionAttestationMarkCatalogItem.class;
        }

        public T newInstance()
        {
            return (T) new SessionAttestationMarkCatalogItem();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "shortTitle":
                    return obj.getShortTitle();
                case "printTitle":
                    return obj.getPrintTitle();
                case "scale":
                    return obj.getScale();
                case "priority":
                    return obj.getPriority();
                case "positive":
                    return obj.isPositive();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "printTitle":
                    obj.setPrintTitle((String) value);
                    return;
                case "scale":
                    obj.setScale((SessionCurrentMarkScale) value);
                    return;
                case "priority":
                    obj.setPriority((Integer) value);
                    return;
                case "positive":
                    obj.setPositive((Boolean) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "shortTitle":
                        return true;
                case "printTitle":
                        return true;
                case "scale":
                        return true;
                case "priority":
                        return true;
                case "positive":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "shortTitle":
                    return true;
                case "printTitle":
                    return true;
                case "scale":
                    return true;
                case "priority":
                    return true;
                case "positive":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "shortTitle":
                    return String.class;
                case "printTitle":
                    return String.class;
                case "scale":
                    return SessionCurrentMarkScale.class;
                case "priority":
                    return Integer.class;
                case "positive":
                    return Boolean.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionAttestationMarkCatalogItem> _dslPath = new Path<SessionAttestationMarkCatalogItem>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionAttestationMarkCatalogItem");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unisession.entity.catalog.SessionAttestationMarkCatalogItem#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.catalog.SessionAttestationMarkCatalogItem#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return Печатное название.
     * @see ru.tandemservice.unisession.entity.catalog.SessionAttestationMarkCatalogItem#getPrintTitle()
     */
    public static PropertyPath<String> printTitle()
    {
        return _dslPath.printTitle();
    }

    /**
     * @return Шкала оценок текущего контроля. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.catalog.SessionAttestationMarkCatalogItem#getScale()
     */
    public static SessionCurrentMarkScale.Path<SessionCurrentMarkScale> scale()
    {
        return _dslPath.scale();
    }

    /**
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.catalog.SessionAttestationMarkCatalogItem#getPriority()
     */
    public static PropertyPath<Integer> priority()
    {
        return _dslPath.priority();
    }

    /**
     * @return Положительная. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.catalog.SessionAttestationMarkCatalogItem#isPositive()
     */
    public static PropertyPath<Boolean> positive()
    {
        return _dslPath.positive();
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.unisession.entity.catalog.SessionAttestationMarkCatalogItem#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends SessionAttestationMarkCatalogItem> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<String> _printTitle;
        private SessionCurrentMarkScale.Path<SessionCurrentMarkScale> _scale;
        private PropertyPath<Integer> _priority;
        private PropertyPath<Boolean> _positive;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unisession.entity.catalog.SessionAttestationMarkCatalogItem#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(SessionAttestationMarkCatalogItemGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.catalog.SessionAttestationMarkCatalogItem#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(SessionAttestationMarkCatalogItemGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return Печатное название.
     * @see ru.tandemservice.unisession.entity.catalog.SessionAttestationMarkCatalogItem#getPrintTitle()
     */
        public PropertyPath<String> printTitle()
        {
            if(_printTitle == null )
                _printTitle = new PropertyPath<String>(SessionAttestationMarkCatalogItemGen.P_PRINT_TITLE, this);
            return _printTitle;
        }

    /**
     * @return Шкала оценок текущего контроля. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.catalog.SessionAttestationMarkCatalogItem#getScale()
     */
        public SessionCurrentMarkScale.Path<SessionCurrentMarkScale> scale()
        {
            if(_scale == null )
                _scale = new SessionCurrentMarkScale.Path<SessionCurrentMarkScale>(L_SCALE, this);
            return _scale;
        }

    /**
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.catalog.SessionAttestationMarkCatalogItem#getPriority()
     */
        public PropertyPath<Integer> priority()
        {
            if(_priority == null )
                _priority = new PropertyPath<Integer>(SessionAttestationMarkCatalogItemGen.P_PRIORITY, this);
            return _priority;
        }

    /**
     * @return Положительная. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.catalog.SessionAttestationMarkCatalogItem#isPositive()
     */
        public PropertyPath<Boolean> positive()
        {
            if(_positive == null )
                _positive = new PropertyPath<Boolean>(SessionAttestationMarkCatalogItemGen.P_POSITIVE, this);
            return _positive;
        }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.unisession.entity.catalog.SessionAttestationMarkCatalogItem#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(SessionAttestationMarkCatalogItemGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return SessionAttestationMarkCatalogItem.class;
        }

        public String getEntityName()
        {
            return "sessionAttestationMarkCatalogItem";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
