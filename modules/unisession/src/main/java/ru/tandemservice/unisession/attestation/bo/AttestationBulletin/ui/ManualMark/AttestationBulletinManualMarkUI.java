/**
 *$Id$
 */
package ru.tandemservice.unisession.attestation.bo.AttestationBulletin.ui.ManualMark;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unisession.attestation.bo.AttestationBulletin.AttestationBulletinManager;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlot;
import ru.tandemservice.unisession.entity.catalog.SessionAttestationMarkCatalogItem;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Alexander Shaburov
 * @since 20.11.12
 */
@State({
    @Bind(key= UIPresenter.PUBLISHER_ID, binding="bulletinId")
})
public class AttestationBulletinManualMarkUI extends UIPresenter
{
    public static final String PROP_SLOT_LIST = "slotList";

    private Long _bulletinId;
    private SessionAttestationBulletin _bulletin;
    private List<SessionAttestationSlot> _slotList;

    @Override
    public void onComponentRefresh()
    {
        _bulletin = DataAccessServices.dao().get(SessionAttestationBulletin.class, _bulletinId);
        _slotList = DataAccessServices.dao().getList(SessionAttestationSlot.class, SessionAttestationSlot.bulletin().id(), _bulletinId, SessionAttestationSlot.studentWpe().student().person().identityCard().fullFio().s());
    }

    public void onClickApply()
    {
        AttestationBulletinManager.instance().dao().doSaveManualMark(_bulletin, _slotList);
        deactivate();
    }

    public String getMarkSelectId()
    {
        return "markSelectId_" + getConfig().getDataSource(AttestationBulletinManualMark.STUDENT_SEARCH_DS).getCurrentId();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(PROP_SLOT_LIST, _slotList);
    }

    // Calculate

    public boolean isGroupColumnVisible()
    {
        final Set<String> groupTitleSet = new HashSet<>();
        for (SessionAttestationSlot slot : _slotList)
        {
            if (slot.getStudentWpe().getStudent().getGroup() != null)
                groupTitleSet.add(slot.getStudentWpe().getStudent().getGroup().getTitle());
            else
                groupTitleSet.add("");
        }

        return groupTitleSet.size() > 1;
    }

    public SessionAttestationMarkCatalogItem getMark()
    {
        SessionAttestationSlot current = getConfig().getDataSource(AttestationBulletinManualMark.STUDENT_SEARCH_DS).getCurrent();
        return current.getMark();
    }

    public void setMark(SessionAttestationMarkCatalogItem mark)
    {
        SessionAttestationSlot current = getConfig().getDataSource(AttestationBulletinManualMark.STUDENT_SEARCH_DS).getCurrent();
        current.setMark(mark);
    }

    // Getters & Setters

    public Long getBulletinId()
    {
        return _bulletinId;
    }

    public void setBulletinId(Long bulletinId)
    {
        _bulletinId = bulletinId;
    }

    public SessionAttestationBulletin getBulletin()
    {
        return _bulletin;
    }

    public void setBulletin(SessionAttestationBulletin bulletin)
    {
        _bulletin = bulletin;
    }

    public List<SessionAttestationSlot> getSlotList()
    {
        return _slotList;
    }
}
