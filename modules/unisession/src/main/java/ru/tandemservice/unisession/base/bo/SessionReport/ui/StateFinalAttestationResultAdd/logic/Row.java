/* $Id: $ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.StateFinalAttestationResultAdd.logic;

import jxl.write.WritableCellFormat;
import ru.tandemservice.unisession.base.bo.SessionReport.logic.AbsStateFinalAttestationRow;

/**
 * @author Andrey Andreev
 * @since 16.12.2016
 */
public class Row extends AbsStateFinalAttestationRow
{
    final protected WritableCellFormat _titleStyle;
    final protected WritableCellFormat _intStyle;
    final protected WritableCellFormat _redIntStyle;
    final protected WritableCellFormat _doubleStyle;

    public Row(String title, WritableCellFormat titleStyle, WritableCellFormat intStyle, WritableCellFormat redIntStyle, WritableCellFormat doubleStyle)
    {
        _title = title;

        _titleStyle = titleStyle;
        _intStyle = intStyle;
        _redIntStyle = redIntStyle;
        _doubleStyle = doubleStyle;
    }

    public WritableCellFormat getTitleStyle()
    {
        return _titleStyle;
    }

    public WritableCellFormat getIntStyle()
    {
        return _intStyle;
    }

    public WritableCellFormat getRedIntStyle()
    {
        return _redIntStyle;
    }

    public WritableCellFormat getDoubleStyle()
    {
        return _doubleStyle;
    }
}
