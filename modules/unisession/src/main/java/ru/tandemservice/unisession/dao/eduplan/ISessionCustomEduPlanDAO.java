package ru.tandemservice.unisession.dao.eduplan;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.uniepp.dao.eduplan.IEppCustomEduPlanDAO;
import ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan;
import ru.tandemservice.unisession.entity.document.SessionTransferProtocolDocument;

/**
 * @author Alexey Lopatin
 * @since 25.11.2015
 */
public interface ISessionCustomEduPlanDAO extends IEppCustomEduPlanDAO
{
    SpringBeanCache<ISessionCustomEduPlanDAO> instance = new SpringBeanCache<>(ISessionCustomEduPlanDAO.class.getName());

    /** Сохранение индивидуального УП на основе протокола перезачтения. Номер выдается автоматически. */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void saveCustomEduPlan(EppCustomEduPlan customEduPlan, SessionTransferProtocolDocument protocol);
}
