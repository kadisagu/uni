/* $Id: $ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.FinalQualWorkResultAdd.logic;

import ru.tandemservice.unisession.base.bo.SessionReport.ui.StateFinalAttestationResultAdd.logic.SfaReportInfo;
import ru.tandemservice.unisession.entity.catalog.SessionRecommendation4FQW;
import ru.tandemservice.unisession.entity.catalog.SessionSource4FQWTheme;
import ru.tandemservice.unisession.entity.report.StateFinalAttestationResult;

import java.util.List;

/**
 * @author Andrey Andreev
 * @since 16.12.2016
 */
public class ReportInfo extends SfaReportInfo<Raw>
{
    List<SessionSource4FQWTheme> _source4FQWThemeList;
    List<SessionRecommendation4FQW> _recommendation4FQWList;

    public ReportInfo(StateFinalAttestationResult stateFinalAttestationResult)
    {
        super(stateFinalAttestationResult);
    }

    public List<SessionRecommendation4FQW> getRecommendation4FQWList()
    {
        return _recommendation4FQWList;
    }

    public void setRecommendation4FQWList(List<SessionRecommendation4FQW> recommendation4FQWList)
    {
        _recommendation4FQWList = recommendation4FQWList;
    }

    public List<SessionSource4FQWTheme> getSource4FQWThemeList()
    {
        return _source4FQWThemeList;
    }

    public void setSource4FQWThemeList(List<SessionSource4FQWTheme> source4FQWThemeList)
    {
        _source4FQWThemeList = source4FQWThemeList;
    }
}
