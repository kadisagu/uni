/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionTransfer.ui.OutsideAddEdit;

import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.base.bo.SessionTransfer.logic.ISessionTransferDao;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.document.SessionProjectTheme;
import ru.tandemservice.unisession.entity.document.SessionTransferOutsideOperation;
import ru.tandemservice.unisession.entity.mark.SessionSlotMarkGradeValue;
import ru.tandemservice.unisession.util.selectModel.SessionTermModel;

/**
 * @author oleyba
 * @since 10/19/11
 */
public class SessionTransferOutOpWrapper implements ISessionTransferDao.SessionTransferOutsideOperationInfo
{
    private int id;

    private String sourceDiscipline;
    private EppFControlActionType sourceControlAction;
    private String sourceMark;

    private SessionTermModel.TermWrapper term;
    private EppStudentWpeCAction eppSlot;
    private SessionMarkGradeValueCatalogItem mark;
    private Double rating;

    private String comment;

	private String theme;
	private boolean themeExistForDocSlot;

    public SessionTransferOutOpWrapper()
    {
        this.id = System.identityHashCode(this);
		themeExistForDocSlot = false;
    }

    public SessionTransferOutOpWrapper(SessionTransferOutsideOperation operation, SessionProjectTheme theme)
    {
        this();
        setSourceDiscipline(operation.getDiscipline());
        setSourceControlAction(operation.getControlAction());
        setSourceMark(operation.getMark());
        setTerm(new SessionTermModel.TermWrapper(operation.getTargetMark().getSlot()));
        setEppSlot(operation.getTargetMark().getSlot().getStudentWpeCAction());
        if (!(operation.getTargetMark() instanceof SessionSlotMarkGradeValue))
            throw new IllegalStateException(); // внешнее перезачтение работает лишь с оценками

        setMark(((SessionSlotMarkGradeValue) operation.getTargetMark()).getValue());
        setRating(operation.getTargetMark().getPoints());
        setComment(operation.getComment());

		themeExistForDocSlot = theme != null;
		setTheme(themeExistForDocSlot ? theme.getTheme() : null);
    }

    public SessionTransferOutOpWrapper(SessionTransferOutOpWrapper other)
    {
        this();
        setSourceDiscipline(other.getSourceDiscipline());
        setSourceControlAction(other.getSourceControlAction());
        setSourceMark(other.getSourceMark());
        setTerm(other.getTerm());
        setEppSlot(other.getEppSlot());
        setMark(other.getMark());
        setRating(other.getRating());
        setComment(other.getComment());
		themeExistForDocSlot = other.themeExistForDocSlot;
		setTheme(other.getTheme());
    }

    @Override
    public int hashCode()
    {
        return getId();
    }

    @Override
    public boolean equals(Object obj)
    {
        if (null == obj || !SessionTransferOutOpWrapper.class.isAssignableFrom(obj.getClass()))
            return false;
        return getId() == ((SessionTransferOutOpWrapper) obj).getId();
    }

    public int getId()
    {
        return id;
    }

    @Override
    public String getSourceDiscipline()
    {
        return sourceDiscipline;
    }

    public void setSourceDiscipline(String sourceDiscipline)
    {
        this.sourceDiscipline = sourceDiscipline;
    }

    @Override
    public EppFControlActionType getSourceControlAction()
    {
        return sourceControlAction;
    }

    public void setSourceControlAction(EppFControlActionType sourceControlAction)
    {
        this.sourceControlAction = sourceControlAction;
    }

    @Override
    public String getSourceMark()
    {
        return sourceMark;
    }

    public void setSourceMark(String sourceMark)
    {
        this.sourceMark = sourceMark;
    }

    public SessionTermModel.TermWrapper getTerm()
    {
        return term;
    }

    public void setTerm(SessionTermModel.TermWrapper term)
    {
        this.term = term;
    }

    @Override
    public EppStudentWpeCAction getEppSlot()
    {
        return eppSlot;
    }

    public void setEppSlot(EppStudentWpeCAction eppSlot)
    {
        this.eppSlot = eppSlot;
    }

    @Override
    public SessionMarkGradeValueCatalogItem getMark()
    {
        return mark;
    }

    public void setMark(SessionMarkGradeValueCatalogItem mark)
    {
        this.mark = mark;
    }

    public String getTheme()
    {
        return theme;
    }

    public void setTheme(final String theme)
    {
        this.theme = theme;
    }

    @Override
    public Double getRating()
    {
        return rating;
    }

    public void setRating(Double rating)
    {
        this.rating = rating;
    }

    @Override
    public String getComment()
    {
        return comment;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }

	public boolean isThemeExistForDocSlot()
	{
		return themeExistForDocSlot;
	}
}
