/* $Id$ */
package ru.tandemservice.unisession.settings.bo.SessionSettings;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unisession.settings.bo.SessionSettings.logic.ISessionSettingsDao;
import ru.tandemservice.unisession.settings.bo.SessionSettings.logic.SessionSettingsDao;

/**
 * @author Alexey Lopatin
 * @since 25.05.2015
 */
@Configuration
public class SessionSettingsManager extends BusinessObjectManager
{
    public static SessionSettingsManager instance()
    {
        return instance(SessionSettingsManager.class);
    }

    @Bean
    public ISessionSettingsDao dao()
    {
        return new SessionSettingsDao();
    }
}
