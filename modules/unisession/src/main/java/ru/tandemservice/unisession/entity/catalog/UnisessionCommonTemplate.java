package ru.tandemservice.unisession.entity.catalog;

import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import ru.tandemservice.unisession.entity.catalog.gen.UnisessionCommonTemplateGen;

/**
 * Печатные шаблоны общих документов «Сессия»
 */
public class UnisessionCommonTemplate extends UnisessionCommonTemplateGen implements ITemplateDocument
{
}