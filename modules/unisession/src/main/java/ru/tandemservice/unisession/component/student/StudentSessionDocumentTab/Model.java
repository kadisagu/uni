/* $Id$ */
package ru.tandemservice.unisession.component.student.StudentSessionDocumentTab;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unisession.entity.document.SessionDocument;
import ru.tandemservice.unisession.entity.document.SessionGlobalDocument;

/**
 * @author oleyba
 * @since 4/11/11
 */
@State({@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "student.id")})
public class Model
{
    private Student student = new Student();

    private DynamicListDataSource<SessionDocument> dataSource;

    private ISelectModel termModel;

    private IDataSettings settings;

    public Student getStudent()
    {
        return this.student;
    }

    public void setStudent(final Student student)
    {
        this.student = student;
    }

    public DynamicListDataSource<SessionDocument> getDataSource()
    {
        return this.dataSource;
    }

    public void setDataSource(final DynamicListDataSource<SessionDocument> dataSource)
    {
        this.dataSource = dataSource;
    }

    public ISelectModel getTermModel()
    {
        return this.termModel;
    }

    public void setTermModel(final ISelectModel termModel)
    {
        this.termModel = termModel;
    }

    public IDataSettings getSettings()
    {
        return this.settings;
    }

    public void setSettings(final IDataSettings settings)
    {
        this.settings = settings;
    }

}
