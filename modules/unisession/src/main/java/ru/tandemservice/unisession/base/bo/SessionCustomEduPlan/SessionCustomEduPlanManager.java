/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionCustomEduPlan;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

/**
 * @author Alexey Lopatin
 * @since 24.11.2015
 */
@Configuration
public class SessionCustomEduPlanManager extends BusinessObjectManager
{
    public static SessionCustomEduPlanManager instance()
    {
        return instance(SessionCustomEduPlanManager.class);
    }
}
