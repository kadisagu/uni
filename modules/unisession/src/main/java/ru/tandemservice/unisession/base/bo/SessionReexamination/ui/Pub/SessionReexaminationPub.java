/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReexamination.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow;
import ru.tandemservice.unisession.base.bo.SessionReexamination.logic.SessionReexaminationRequestRowDSHandler;
import ru.tandemservice.unisession.entity.mark.SessionALRequestRow;

/**
 * @author Alexey Lopatin
 * @since 31.08.2015
 */
@Configuration
public class SessionReexaminationPub extends BusinessComponentManager
{
    public static final String REQUEST_ROW_DS = "requestRowDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(REQUEST_ROW_DS, requestRowDS(), requestRowDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint requestRowDS()
    {
        return columnListExtPointBuilder(REQUEST_ROW_DS)
                .addColumn(textColumn(EppEpvTermDistributedRow.P_STORED_INDEX, SessionALRequestRow.rowTerm().row().storedIndex()).width("1px"))
                .addColumn(textColumn(DataWrapper.TITLE, DataWrapper.TITLE))
                .addColumn(textColumn(SessionALRequestRow.P_HOURS_AMOUNT, SessionALRequestRow.hoursAmount()).width("1px"))
                .addColumn(textColumn(SessionReexaminationRequestRowDSHandler.PROP_RE_EXAM_AND_ATTESTATION, SessionReexaminationRequestRowDSHandler.PROP_RE_EXAM_AND_ATTESTATION).width("1px"))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> requestRowDSHandler()
    {
        return new SessionReexaminationRequestRowDSHandler(getName());
    }
}
