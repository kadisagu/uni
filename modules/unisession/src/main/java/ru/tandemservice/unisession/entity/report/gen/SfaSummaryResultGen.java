package ru.tandemservice.unisession.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.report.StorableReport;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unisession.entity.report.SfaSummaryResult;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Отчет «Сводный отчет по результатам государственной итоговой аттестации»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SfaSummaryResultGen extends StorableReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.report.SfaSummaryResult";
    public static final String ENTITY_NAME = "sfaSummaryResult";
    public static final int VERSION_HASH = 1377298569;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDUCATION_YEAR = "educationYear";
    public static final String L_QUALIFICATION = "qualification";
    public static final String L_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String L_TERRITORIAL_ORG_UNIT = "territorialOrgUnit";
    public static final String L_OWNER_ORG_UNIT = "ownerOrgUnit";
    public static final String L_EDUCATION_LEVEL_HIGH_SCHOOL = "educationLevelHighSchool";
    public static final String L_DEVELOP_FORM = "developForm";
    public static final String L_DEVELOP_CONDITION = "developCondition";
    public static final String L_DEVELOP_TECH = "developTech";
    public static final String L_DEVELOP_PERIOD = "developPeriod";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String P_DATE_FROM = "dateFrom";
    public static final String P_DATE_TO = "dateTo";
    public static final String P_DETAIL_BY_DIRECTIONS = "detailByDirections";
    public static final String P_SKIP_EMPTY = "skipEmpty";

    private EducationYear _educationYear;     // Учебный год
    private Qualifications _qualification;     // Уровень образования
    private OrgUnit _formativeOrgUnit;     // Формирующее подразделение
    private OrgUnit _territorialOrgUnit;     // Территориальное подразделение
    private OrgUnit _ownerOrgUnit;     // Выпускающее подразделение
    private EducationLevelsHighSchool _educationLevelHighSchool;     // Направление подготовки (специальность)
    private DevelopForm _developForm;     // Форма освоения
    private DevelopCondition _developCondition;     // Условие освоения
    private DevelopTech _developTech;     // Технология освоения
    private DevelopPeriod _developPeriod;     // Нормативный срок освоения
    private CompensationType _compensationType;     // Вид возмещения затрат
    private Date _dateFrom;     // Мероприятия с
    private Date _dateTo;     // Мероприятия по
    private boolean _detailByDirections = false;     // Детализировать по направлениям
    private boolean _skipEmpty = true;     // Не печатать пустые строки

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     */
    @NotNull
    public EducationYear getEducationYear()
    {
        return _educationYear;
    }

    /**
     * @param educationYear Учебный год. Свойство не может быть null.
     */
    public void setEducationYear(EducationYear educationYear)
    {
        dirty(_educationYear, educationYear);
        _educationYear = educationYear;
    }

    /**
     * @return Уровень образования.
     */
    public Qualifications getQualification()
    {
        return _qualification;
    }

    /**
     * @param qualification Уровень образования.
     */
    public void setQualification(Qualifications qualification)
    {
        dirty(_qualification, qualification);
        _qualification = qualification;
    }

    /**
     * @return Формирующее подразделение.
     */
    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    /**
     * @param formativeOrgUnit Формирующее подразделение.
     */
    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        dirty(_formativeOrgUnit, formativeOrgUnit);
        _formativeOrgUnit = formativeOrgUnit;
    }

    /**
     * @return Территориальное подразделение.
     */
    public OrgUnit getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    /**
     * @param territorialOrgUnit Территориальное подразделение.
     */
    public void setTerritorialOrgUnit(OrgUnit territorialOrgUnit)
    {
        dirty(_territorialOrgUnit, territorialOrgUnit);
        _territorialOrgUnit = territorialOrgUnit;
    }

    /**
     * @return Выпускающее подразделение.
     */
    public OrgUnit getOwnerOrgUnit()
    {
        return _ownerOrgUnit;
    }

    /**
     * @param ownerOrgUnit Выпускающее подразделение.
     */
    public void setOwnerOrgUnit(OrgUnit ownerOrgUnit)
    {
        dirty(_ownerOrgUnit, ownerOrgUnit);
        _ownerOrgUnit = ownerOrgUnit;
    }

    /**
     * @return Направление подготовки (специальность).
     */
    public EducationLevelsHighSchool getEducationLevelHighSchool()
    {
        return _educationLevelHighSchool;
    }

    /**
     * @param educationLevelHighSchool Направление подготовки (специальность).
     */
    public void setEducationLevelHighSchool(EducationLevelsHighSchool educationLevelHighSchool)
    {
        dirty(_educationLevelHighSchool, educationLevelHighSchool);
        _educationLevelHighSchool = educationLevelHighSchool;
    }

    /**
     * @return Форма освоения.
     */
    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения.
     */
    public void setDevelopForm(DevelopForm developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Условие освоения.
     */
    public DevelopCondition getDevelopCondition()
    {
        return _developCondition;
    }

    /**
     * @param developCondition Условие освоения.
     */
    public void setDevelopCondition(DevelopCondition developCondition)
    {
        dirty(_developCondition, developCondition);
        _developCondition = developCondition;
    }

    /**
     * @return Технология освоения.
     */
    public DevelopTech getDevelopTech()
    {
        return _developTech;
    }

    /**
     * @param developTech Технология освоения.
     */
    public void setDevelopTech(DevelopTech developTech)
    {
        dirty(_developTech, developTech);
        _developTech = developTech;
    }

    /**
     * @return Нормативный срок освоения.
     */
    public DevelopPeriod getDevelopPeriod()
    {
        return _developPeriod;
    }

    /**
     * @param developPeriod Нормативный срок освоения.
     */
    public void setDevelopPeriod(DevelopPeriod developPeriod)
    {
        dirty(_developPeriod, developPeriod);
        _developPeriod = developPeriod;
    }

    /**
     * @return Вид возмещения затрат.
     */
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Мероприятия с.
     */
    public Date getDateFrom()
    {
        return _dateFrom;
    }

    /**
     * @param dateFrom Мероприятия с.
     */
    public void setDateFrom(Date dateFrom)
    {
        dirty(_dateFrom, dateFrom);
        _dateFrom = dateFrom;
    }

    /**
     * @return Мероприятия по.
     */
    public Date getDateTo()
    {
        return _dateTo;
    }

    /**
     * @param dateTo Мероприятия по.
     */
    public void setDateTo(Date dateTo)
    {
        dirty(_dateTo, dateTo);
        _dateTo = dateTo;
    }

    /**
     * @return Детализировать по направлениям. Свойство не может быть null.
     */
    @NotNull
    public boolean isDetailByDirections()
    {
        return _detailByDirections;
    }

    /**
     * @param detailByDirections Детализировать по направлениям. Свойство не может быть null.
     */
    public void setDetailByDirections(boolean detailByDirections)
    {
        dirty(_detailByDirections, detailByDirections);
        _detailByDirections = detailByDirections;
    }

    /**
     * @return Не печатать пустые строки. Свойство не может быть null.
     */
    @NotNull
    public boolean isSkipEmpty()
    {
        return _skipEmpty;
    }

    /**
     * @param skipEmpty Не печатать пустые строки. Свойство не может быть null.
     */
    public void setSkipEmpty(boolean skipEmpty)
    {
        dirty(_skipEmpty, skipEmpty);
        _skipEmpty = skipEmpty;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof SfaSummaryResultGen)
        {
            setEducationYear(((SfaSummaryResult)another).getEducationYear());
            setQualification(((SfaSummaryResult)another).getQualification());
            setFormativeOrgUnit(((SfaSummaryResult)another).getFormativeOrgUnit());
            setTerritorialOrgUnit(((SfaSummaryResult)another).getTerritorialOrgUnit());
            setOwnerOrgUnit(((SfaSummaryResult)another).getOwnerOrgUnit());
            setEducationLevelHighSchool(((SfaSummaryResult)another).getEducationLevelHighSchool());
            setDevelopForm(((SfaSummaryResult)another).getDevelopForm());
            setDevelopCondition(((SfaSummaryResult)another).getDevelopCondition());
            setDevelopTech(((SfaSummaryResult)another).getDevelopTech());
            setDevelopPeriod(((SfaSummaryResult)another).getDevelopPeriod());
            setCompensationType(((SfaSummaryResult)another).getCompensationType());
            setDateFrom(((SfaSummaryResult)another).getDateFrom());
            setDateTo(((SfaSummaryResult)another).getDateTo());
            setDetailByDirections(((SfaSummaryResult)another).isDetailByDirections());
            setSkipEmpty(((SfaSummaryResult)another).isSkipEmpty());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SfaSummaryResultGen> extends StorableReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SfaSummaryResult.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("SfaSummaryResult is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "educationYear":
                    return obj.getEducationYear();
                case "qualification":
                    return obj.getQualification();
                case "formativeOrgUnit":
                    return obj.getFormativeOrgUnit();
                case "territorialOrgUnit":
                    return obj.getTerritorialOrgUnit();
                case "ownerOrgUnit":
                    return obj.getOwnerOrgUnit();
                case "educationLevelHighSchool":
                    return obj.getEducationLevelHighSchool();
                case "developForm":
                    return obj.getDevelopForm();
                case "developCondition":
                    return obj.getDevelopCondition();
                case "developTech":
                    return obj.getDevelopTech();
                case "developPeriod":
                    return obj.getDevelopPeriod();
                case "compensationType":
                    return obj.getCompensationType();
                case "dateFrom":
                    return obj.getDateFrom();
                case "dateTo":
                    return obj.getDateTo();
                case "detailByDirections":
                    return obj.isDetailByDirections();
                case "skipEmpty":
                    return obj.isSkipEmpty();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "educationYear":
                    obj.setEducationYear((EducationYear) value);
                    return;
                case "qualification":
                    obj.setQualification((Qualifications) value);
                    return;
                case "formativeOrgUnit":
                    obj.setFormativeOrgUnit((OrgUnit) value);
                    return;
                case "territorialOrgUnit":
                    obj.setTerritorialOrgUnit((OrgUnit) value);
                    return;
                case "ownerOrgUnit":
                    obj.setOwnerOrgUnit((OrgUnit) value);
                    return;
                case "educationLevelHighSchool":
                    obj.setEducationLevelHighSchool((EducationLevelsHighSchool) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((DevelopForm) value);
                    return;
                case "developCondition":
                    obj.setDevelopCondition((DevelopCondition) value);
                    return;
                case "developTech":
                    obj.setDevelopTech((DevelopTech) value);
                    return;
                case "developPeriod":
                    obj.setDevelopPeriod((DevelopPeriod) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "dateFrom":
                    obj.setDateFrom((Date) value);
                    return;
                case "dateTo":
                    obj.setDateTo((Date) value);
                    return;
                case "detailByDirections":
                    obj.setDetailByDirections((Boolean) value);
                    return;
                case "skipEmpty":
                    obj.setSkipEmpty((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "educationYear":
                        return true;
                case "qualification":
                        return true;
                case "formativeOrgUnit":
                        return true;
                case "territorialOrgUnit":
                        return true;
                case "ownerOrgUnit":
                        return true;
                case "educationLevelHighSchool":
                        return true;
                case "developForm":
                        return true;
                case "developCondition":
                        return true;
                case "developTech":
                        return true;
                case "developPeriod":
                        return true;
                case "compensationType":
                        return true;
                case "dateFrom":
                        return true;
                case "dateTo":
                        return true;
                case "detailByDirections":
                        return true;
                case "skipEmpty":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "educationYear":
                    return true;
                case "qualification":
                    return true;
                case "formativeOrgUnit":
                    return true;
                case "territorialOrgUnit":
                    return true;
                case "ownerOrgUnit":
                    return true;
                case "educationLevelHighSchool":
                    return true;
                case "developForm":
                    return true;
                case "developCondition":
                    return true;
                case "developTech":
                    return true;
                case "developPeriod":
                    return true;
                case "compensationType":
                    return true;
                case "dateFrom":
                    return true;
                case "dateTo":
                    return true;
                case "detailByDirections":
                    return true;
                case "skipEmpty":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "educationYear":
                    return EducationYear.class;
                case "qualification":
                    return Qualifications.class;
                case "formativeOrgUnit":
                    return OrgUnit.class;
                case "territorialOrgUnit":
                    return OrgUnit.class;
                case "ownerOrgUnit":
                    return OrgUnit.class;
                case "educationLevelHighSchool":
                    return EducationLevelsHighSchool.class;
                case "developForm":
                    return DevelopForm.class;
                case "developCondition":
                    return DevelopCondition.class;
                case "developTech":
                    return DevelopTech.class;
                case "developPeriod":
                    return DevelopPeriod.class;
                case "compensationType":
                    return CompensationType.class;
                case "dateFrom":
                    return Date.class;
                case "dateTo":
                    return Date.class;
                case "detailByDirections":
                    return Boolean.class;
                case "skipEmpty":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SfaSummaryResult> _dslPath = new Path<SfaSummaryResult>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SfaSummaryResult");
    }
            

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.report.SfaSummaryResult#getEducationYear()
     */
    public static EducationYear.Path<EducationYear> educationYear()
    {
        return _dslPath.educationYear();
    }

    /**
     * @return Уровень образования.
     * @see ru.tandemservice.unisession.entity.report.SfaSummaryResult#getQualification()
     */
    public static Qualifications.Path<Qualifications> qualification()
    {
        return _dslPath.qualification();
    }

    /**
     * @return Формирующее подразделение.
     * @see ru.tandemservice.unisession.entity.report.SfaSummaryResult#getFormativeOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> formativeOrgUnit()
    {
        return _dslPath.formativeOrgUnit();
    }

    /**
     * @return Территориальное подразделение.
     * @see ru.tandemservice.unisession.entity.report.SfaSummaryResult#getTerritorialOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> territorialOrgUnit()
    {
        return _dslPath.territorialOrgUnit();
    }

    /**
     * @return Выпускающее подразделение.
     * @see ru.tandemservice.unisession.entity.report.SfaSummaryResult#getOwnerOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> ownerOrgUnit()
    {
        return _dslPath.ownerOrgUnit();
    }

    /**
     * @return Направление подготовки (специальность).
     * @see ru.tandemservice.unisession.entity.report.SfaSummaryResult#getEducationLevelHighSchool()
     */
    public static EducationLevelsHighSchool.Path<EducationLevelsHighSchool> educationLevelHighSchool()
    {
        return _dslPath.educationLevelHighSchool();
    }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.unisession.entity.report.SfaSummaryResult#getDevelopForm()
     */
    public static DevelopForm.Path<DevelopForm> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.unisession.entity.report.SfaSummaryResult#getDevelopCondition()
     */
    public static DevelopCondition.Path<DevelopCondition> developCondition()
    {
        return _dslPath.developCondition();
    }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.unisession.entity.report.SfaSummaryResult#getDevelopTech()
     */
    public static DevelopTech.Path<DevelopTech> developTech()
    {
        return _dslPath.developTech();
    }

    /**
     * @return Нормативный срок освоения.
     * @see ru.tandemservice.unisession.entity.report.SfaSummaryResult#getDevelopPeriod()
     */
    public static DevelopPeriod.Path<DevelopPeriod> developPeriod()
    {
        return _dslPath.developPeriod();
    }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.unisession.entity.report.SfaSummaryResult#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Мероприятия с.
     * @see ru.tandemservice.unisession.entity.report.SfaSummaryResult#getDateFrom()
     */
    public static PropertyPath<Date> dateFrom()
    {
        return _dslPath.dateFrom();
    }

    /**
     * @return Мероприятия по.
     * @see ru.tandemservice.unisession.entity.report.SfaSummaryResult#getDateTo()
     */
    public static PropertyPath<Date> dateTo()
    {
        return _dslPath.dateTo();
    }

    /**
     * @return Детализировать по направлениям. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.report.SfaSummaryResult#isDetailByDirections()
     */
    public static PropertyPath<Boolean> detailByDirections()
    {
        return _dslPath.detailByDirections();
    }

    /**
     * @return Не печатать пустые строки. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.report.SfaSummaryResult#isSkipEmpty()
     */
    public static PropertyPath<Boolean> skipEmpty()
    {
        return _dslPath.skipEmpty();
    }

    public static class Path<E extends SfaSummaryResult> extends StorableReport.Path<E>
    {
        private EducationYear.Path<EducationYear> _educationYear;
        private Qualifications.Path<Qualifications> _qualification;
        private OrgUnit.Path<OrgUnit> _formativeOrgUnit;
        private OrgUnit.Path<OrgUnit> _territorialOrgUnit;
        private OrgUnit.Path<OrgUnit> _ownerOrgUnit;
        private EducationLevelsHighSchool.Path<EducationLevelsHighSchool> _educationLevelHighSchool;
        private DevelopForm.Path<DevelopForm> _developForm;
        private DevelopCondition.Path<DevelopCondition> _developCondition;
        private DevelopTech.Path<DevelopTech> _developTech;
        private DevelopPeriod.Path<DevelopPeriod> _developPeriod;
        private CompensationType.Path<CompensationType> _compensationType;
        private PropertyPath<Date> _dateFrom;
        private PropertyPath<Date> _dateTo;
        private PropertyPath<Boolean> _detailByDirections;
        private PropertyPath<Boolean> _skipEmpty;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.report.SfaSummaryResult#getEducationYear()
     */
        public EducationYear.Path<EducationYear> educationYear()
        {
            if(_educationYear == null )
                _educationYear = new EducationYear.Path<EducationYear>(L_EDUCATION_YEAR, this);
            return _educationYear;
        }

    /**
     * @return Уровень образования.
     * @see ru.tandemservice.unisession.entity.report.SfaSummaryResult#getQualification()
     */
        public Qualifications.Path<Qualifications> qualification()
        {
            if(_qualification == null )
                _qualification = new Qualifications.Path<Qualifications>(L_QUALIFICATION, this);
            return _qualification;
        }

    /**
     * @return Формирующее подразделение.
     * @see ru.tandemservice.unisession.entity.report.SfaSummaryResult#getFormativeOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> formativeOrgUnit()
        {
            if(_formativeOrgUnit == null )
                _formativeOrgUnit = new OrgUnit.Path<OrgUnit>(L_FORMATIVE_ORG_UNIT, this);
            return _formativeOrgUnit;
        }

    /**
     * @return Территориальное подразделение.
     * @see ru.tandemservice.unisession.entity.report.SfaSummaryResult#getTerritorialOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> territorialOrgUnit()
        {
            if(_territorialOrgUnit == null )
                _territorialOrgUnit = new OrgUnit.Path<OrgUnit>(L_TERRITORIAL_ORG_UNIT, this);
            return _territorialOrgUnit;
        }

    /**
     * @return Выпускающее подразделение.
     * @see ru.tandemservice.unisession.entity.report.SfaSummaryResult#getOwnerOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> ownerOrgUnit()
        {
            if(_ownerOrgUnit == null )
                _ownerOrgUnit = new OrgUnit.Path<OrgUnit>(L_OWNER_ORG_UNIT, this);
            return _ownerOrgUnit;
        }

    /**
     * @return Направление подготовки (специальность).
     * @see ru.tandemservice.unisession.entity.report.SfaSummaryResult#getEducationLevelHighSchool()
     */
        public EducationLevelsHighSchool.Path<EducationLevelsHighSchool> educationLevelHighSchool()
        {
            if(_educationLevelHighSchool == null )
                _educationLevelHighSchool = new EducationLevelsHighSchool.Path<EducationLevelsHighSchool>(L_EDUCATION_LEVEL_HIGH_SCHOOL, this);
            return _educationLevelHighSchool;
        }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.unisession.entity.report.SfaSummaryResult#getDevelopForm()
     */
        public DevelopForm.Path<DevelopForm> developForm()
        {
            if(_developForm == null )
                _developForm = new DevelopForm.Path<DevelopForm>(L_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.unisession.entity.report.SfaSummaryResult#getDevelopCondition()
     */
        public DevelopCondition.Path<DevelopCondition> developCondition()
        {
            if(_developCondition == null )
                _developCondition = new DevelopCondition.Path<DevelopCondition>(L_DEVELOP_CONDITION, this);
            return _developCondition;
        }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.unisession.entity.report.SfaSummaryResult#getDevelopTech()
     */
        public DevelopTech.Path<DevelopTech> developTech()
        {
            if(_developTech == null )
                _developTech = new DevelopTech.Path<DevelopTech>(L_DEVELOP_TECH, this);
            return _developTech;
        }

    /**
     * @return Нормативный срок освоения.
     * @see ru.tandemservice.unisession.entity.report.SfaSummaryResult#getDevelopPeriod()
     */
        public DevelopPeriod.Path<DevelopPeriod> developPeriod()
        {
            if(_developPeriod == null )
                _developPeriod = new DevelopPeriod.Path<DevelopPeriod>(L_DEVELOP_PERIOD, this);
            return _developPeriod;
        }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.unisession.entity.report.SfaSummaryResult#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Мероприятия с.
     * @see ru.tandemservice.unisession.entity.report.SfaSummaryResult#getDateFrom()
     */
        public PropertyPath<Date> dateFrom()
        {
            if(_dateFrom == null )
                _dateFrom = new PropertyPath<Date>(SfaSummaryResultGen.P_DATE_FROM, this);
            return _dateFrom;
        }

    /**
     * @return Мероприятия по.
     * @see ru.tandemservice.unisession.entity.report.SfaSummaryResult#getDateTo()
     */
        public PropertyPath<Date> dateTo()
        {
            if(_dateTo == null )
                _dateTo = new PropertyPath<Date>(SfaSummaryResultGen.P_DATE_TO, this);
            return _dateTo;
        }

    /**
     * @return Детализировать по направлениям. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.report.SfaSummaryResult#isDetailByDirections()
     */
        public PropertyPath<Boolean> detailByDirections()
        {
            if(_detailByDirections == null )
                _detailByDirections = new PropertyPath<Boolean>(SfaSummaryResultGen.P_DETAIL_BY_DIRECTIONS, this);
            return _detailByDirections;
        }

    /**
     * @return Не печатать пустые строки. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.report.SfaSummaryResult#isSkipEmpty()
     */
        public PropertyPath<Boolean> skipEmpty()
        {
            if(_skipEmpty == null )
                _skipEmpty = new PropertyPath<Boolean>(SfaSummaryResultGen.P_SKIP_EMPTY, this);
            return _skipEmpty;
        }

        public Class getEntityClass()
        {
            return SfaSummaryResult.class;
        }

        public String getEntityName()
        {
            return "sfaSummaryResult";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
