/* $Id: SessionReportSummaryBulletinAddUI.java 22487 2012-04-04 13:16:00Z vzhukov $ */
package ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.BulletinAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilter;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilterType;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterFormConfig;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unisession.attestation.bo.AttestationReport.AttestationReportManager;
import ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.BulletinPub.AttestationReportBulletinPub;
import ru.tandemservice.unisession.attestation.entity.SessionAttestation;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlot;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.base.bo.SessionReport.util.UniAttestationFilterAddon;

import java.util.List;

/**
 * @author oleyba
 * @since 12/29/11
 */

@Input
({
    @Bind(key = SessionReportManager.BIND_ORG_UNIT, binding = "ouHolder.id", required = true),
    @Bind(key = AttestationReportBulletinAddUI.BIND_ATTESTATION, binding = "attestation.id")
})
public class AttestationReportBulletinAddUI extends UIPresenter
{
    public static final String BIND_ATTESTATION = "attestation";

    private OrgUnitHolder ouHolder = new OrgUnitHolder();

    private SessionAttestation attestation = new SessionAttestation();
    private List<EppRegistryElementPart> disciplineList;
    
    @Override
    public void onComponentRefresh()
    {
        getOuHolder().refresh();
        if (getAttestation() != null && getAttestation().getId() != null)
            setAttestation(IUniBaseDao.instance.get().get(SessionAttestation.class, getAttestation().getId()));

        configUtil(getAttestationFilterAddon());
    }

    public void onChangeAttestation()
    {
        configUtilWhere(getAttestationFilterAddon());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(SessionReportManager.PARAM_ORG_UNIT, getOrgUnit());
        dataSource.put(AttestationReportManager.PARAM_ATTESTATION, getAttestation());
    }

    public void onClickApply()
    {
        Long report = AttestationReportManager.instance().bulletinPrintDao().createStoredReport(this);
        deactivate();
        _uiActivation.asDesktopRoot(AttestationReportBulletinPub.class)
            .parameter(UIPresenter.PUBLISHER_ID, report)
            .activate();
    }

    public void doNothing()
    {
    }

    // utils

    public UniAttestationFilterAddon getAttestationFilterAddon()
    {
        return (UniAttestationFilterAddon) getConfig().getAddon(CommonFilterAddon.class.getSimpleName());
    }

    public OrgUnitHolder getOuHolder()
    {
        return ouHolder;
    }

    public OrgUnit getOrgUnit()
    {
        return getOuHolder().getValue();
    }

    public SessionAttestation getAttestation()
    {
        return attestation;
    }

    public void setAttestation(SessionAttestation attestation)
    {
        this.attestation = attestation;
    }

    public List<EppRegistryElementPart> getDisciplineList()
    {
        return disciplineList;
    }

    public void setDisciplineList(List<EppRegistryElementPart> disciplineList)
    {
        this.disciplineList = disciplineList;
    }

    private void configUtil(UniAttestationFilterAddon util)
    {
        util
                .configDoubleWidthFilters(false)
                .configUseEnableCheckbox(true)
                .configSettings(getSettingsKey());


        util.clearFilterItems();

        util

                .addFilterItem(UniAttestationFilterAddon.COURSE, CommonFilterFormConfig.DEFAULT_CHECKBOX_SELECT_FILTER_CONFIG)
                .addFilterItem(UniAttestationFilterAddon.GROUP_WITH_NO_GROUP, new CommonFilterFormConfig(true, false, true, false, true, true))
                .addFilterItem(UniAttestationFilterAddon.STUDENT_CUSTOM_STATE, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(UniAttestationFilterAddon.TARGET_ADMISSION, CommonFilterFormConfig.DEFAULT_CHECKBOX_SELECT_FILTER_CONFIG)
                .addFilterItem(UniAttestationFilterAddon.DISCIPLINE, new CommonFilterFormConfig(true, true, true, false, true, true));


        configUtilWhere(util);
    }

    public void configUtilWhere(UniAttestationFilterAddon util)
    {
        util.clearWhereFilter();

        util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, SessionAttestationSlot.studentWpe().student().status().active(), true));

        if (null != attestation) util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, SessionAttestationSlot.bulletin().attestation(), attestation));
    }

    public void onChangeYearOrPart()
    {
        configUtilWhere(getAttestationFilterAddon());
    }
}
