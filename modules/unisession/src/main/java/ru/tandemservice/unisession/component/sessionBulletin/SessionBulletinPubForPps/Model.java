// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinPubForPps;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.sec.IPrincipalContext;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;

/**
 * @author oleyba
 * @since 2/18/11
 */
@State({
    @Bind(key=PublisherActivator.PUBLISHER_ID_KEY, binding="bulletin.id")
})
@Output({
    @Bind(key=PublisherActivator.PUBLISHER_ID_KEY, binding="bulletin.id"),
    @Bind(key = ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinPubInline.Model.BIND_COLUMN_PKEY_HANDLER, binding = "columnPKeyHandler")
})
public class Model
{
    public static final String COMPONENT_NAME = Model.class.getPackage().getName();

    private IPrincipalContext _principalContext;

    private SessionBulletinDocument bulletin = new SessionBulletinDocument();

    private boolean canEditThemes;

    private ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinPubInline.Model.ISessionBulletinColumnPKeyHandler columnPKeyHandler = new ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinPubInline.Model.ISessionBulletinColumnPKeyHandler()
    {
        @Override
        public String getAllowanceColumnPKey()
        {
            return null;
        }

        @Override
        public String getAddSheetColumnPKey()
        {
            return null;
        }
    };

    // getters and setters

    public SessionBulletinDocument getBulletin()
    {
        return this.bulletin;
    }

    public void setBulletin(final SessionBulletinDocument bulletin)
    {
        this.bulletin = bulletin;
    }

    public boolean isCanEditThemes()
    {
        return this.canEditThemes;
    }

    public void setCanEditThemes(final boolean canEditThemes)
    {
        this.canEditThemes = canEditThemes;
    }

    public ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinPubInline.Model.ISessionBulletinColumnPKeyHandler getColumnPKeyHandler()
    {
        return columnPKeyHandler;
    }

    public IPrincipalContext getPrincipalContext()
    {
        return _principalContext;
    }

    public void setPrincipalContext(IPrincipalContext principalContext)
    {
        _principalContext = principalContext;
    }
}
