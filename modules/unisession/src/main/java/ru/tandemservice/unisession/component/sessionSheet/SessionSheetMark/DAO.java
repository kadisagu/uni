/* $*/

package ru.tandemservice.unisession.component.sessionSheet.SessionSheetMark;

import com.google.common.collect.ImmutableList;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.ui.selectModel.PpsEntryModel;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.base.bo.SessionMark.SessionMarkManager;
import ru.tandemservice.unisession.base.bo.SessionMark.logic.ISessionMarkDAO;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionSheetDocument;
import ru.tandemservice.unisession.entity.document.SessionSlotRatingData;
import ru.tandemservice.unisession.entity.document.gen.SessionSlotRatingDataGen;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.mark.SessionMarkRatingData;
import ru.tandemservice.unisession.entity.mark.SessionSlotRegularMark;
import ru.tandemservice.unisession.util.selectModel.SessionTermModel;

import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author oleyba
 * @since 2/22/11
 */
public class DAO<M extends Model> extends UniBaseDao implements IDAO<M>
{
    @Override
    public void prepare(final M model)
    {
        model.setSheet(this.get(SessionSheetDocument.class, model.getSheet().getId()));
        model.setSlot(this.get(SessionDocumentSlot.class, SessionDocumentSlot.document().s(), model.getSheet()));
        model.setTerm(new SessionTermModel.TermWrapper(model.getSlot()));
        model.setPpsList(CommonBaseUtil.<PpsEntry>getPropertiesList(this.getList(SessionComissionPps.class, SessionComissionPps.commission().s(), model.getSlot().getCommission()), SessionComissionPps.pps().s()));

        if (model.getPpsList().isEmpty()) {
            throw new ApplicationException("Невозможно выставить оценку, так как в экз. листе не указан преподаватель.");
        }

        model.setStudentModel(new LazySimpleSelectModel<>(ImmutableList.of(model.getSlot().getActualStudent())));
        model.setTermModel(new SessionTermModel()
        {
            @Override
            public Map<EducationYear, Set<YearDistributionPart>> getValueMap()
            {
                return Collections.singletonMap(model.getTerm().getYear(), Collections.singleton(model.getTerm().getPart()));
            }
        });
        model.setControlActionModel(new LazySimpleSelectModel<>(ImmutableList.of(model.getSlot().getStudentWpeCAction()), EppStudentWpeCAction.P_REGISTRY_ELEMENT_TITLE));
        model.setPpsModel(new PpsEntryModel()
        {
            @Override
            protected DQLSelectBuilder filter(final String alias, final DQLSelectBuilder ppsDQL)
            {
                return ppsDQL.where(in(property(alias + ".id"), UniBaseUtils.getIdList(model.getPpsList())));
            }
        });

        ISessionBrsDao brsDao = ISessionBrsDao.instance.get();
        model.setRatingSettings(brsDao.getRatingSettings(brsDao.key(model.getSlot())));

        if (model.isUseCurrentRating()) {
            SessionSlotRatingData ratingData = getByNaturalId(new SessionSlotRatingDataGen.NaturalId(model.getSlot()));
            if (null == ratingData)
                model.setNeedToSaveCurrentRatingData(true);
            else
                model.setCurrentRating(ratingData.getFixedCurrentRating());
        }

        final SessionMark previous = this.get(SessionMark.class, SessionMark.slot().s(), model.getSlot());
        if (null != previous)
        {
            model.setMark(previous.getValueItem());
            if (model.isUseCurrentRating() && previous instanceof SessionSlotRegularMark) {
                SessionMarkRatingData ratingData = get(SessionMarkRatingData.class, SessionMarkRatingData.mark(), (SessionSlotRegularMark) previous);
                if (null != ratingData)
                    model.setPoints(ratingData.getScoredPoints());
            }
            else if (model.isUsePoints())
                model.setPoints(previous.getPoints());
            model.setPerformDate(previous.getPerformDate());
            initAdditionalMarkData(model, previous);
        }

        if (!model.isUsePoints())
            model.setMarkModel(SessionMarkManager.instance().regularMarkDao().getMarkModelMap(Collections.singleton(model.getSlot())).get(model.getSlot()));
    }

    @Override
    public void prepareCurrentRatingData(M model)
    {
        SessionSlotRatingData ratingData = getByNaturalId(new SessionSlotRatingDataGen.NaturalId(model.getSlot()));
        if (null != ratingData)
            model.setCurrentRating(ratingData.getFixedCurrentRating());
    }

    @Override
    public void validate(final M model, final ErrorCollector errors)
    {
        if (model.getPerformDate() != null && model.getSheet().getDeadlineDate() != null && model.getPerformDate().after(model.getSheet().getDeadlineDate())) {
            errors.add("Дата сдачи не может быть позже срока действия.", "performDate");
        }

        if (model.getMark() != null && model.getPerformDate() == null) {
            errors.add("Чтобы выставить оценку, заполните дату сдачи.", "performDate");
        }

        if (this.getList(SessionComissionPps.class, SessionComissionPps.commission().s(), model.getSlot().getCommission()).isEmpty()) {
            errors.add("Невозможно выставить оценку, так как в экз. листе не указан преподаватель.");
        }
    }

    @Override
    public void update(final M model)
    {
        ISessionMarkDAO.MarkData markData = prepareMarkDataForUpdate(model);

        final SessionMark mark = SessionMarkManager.instance().regularMarkDao().saveOrUpdateMark(model.getSlot(), markData);

        if (null != mark) {

            model.setMarkId(mark.getId());

            if (model.isUseCurrentRating() && mark instanceof SessionSlotRegularMark) {
                SessionMarkRatingData ratingData = get(SessionMarkRatingData.class, SessionMarkRatingData.mark(), (SessionSlotRegularMark) mark);
                if (model.getPoints() != null) {
                    if (null == ratingData) {
                        ratingData = new SessionMarkRatingData();
                        ratingData.setMark((SessionSlotRegularMark) mark);
                    }
                    ratingData.setScoredPoints(model.getPoints());
                    saveOrUpdate(ratingData);
                }
                else if (null != ratingData) {
                    delete(ratingData);
                }
            }
        }

        saveAdditionalMarkData(model);
    }

    protected void initAdditionalMarkData(M model, SessionMark previous)
    {
        // для переопределения в проектах
    }

    protected ISessionMarkDAO.MarkData prepareMarkDataForUpdate(final M model)
    {
        if (model.isUsePoints()) {
            if (model.getPoints() == null) {
                return new ISessionMarkDAO.MarkData()
                {
                    @Override public Date getPerformDate() { return null; }
                    @Override public Double getPoints() { return null; }
                    @Override public SessionMarkCatalogItem getMarkValue() { return null; }
                    @Override public String getComment() { return null; }
                };
            }
            return ISessionBrsDao.instance.get().calculateMarkData(model.getPerformDate(), model.getSlot(), model.getPoints());
        }

        return new ISessionMarkDAO.MarkData()
        {
            @Override public Date getPerformDate() { return model.getPerformDate(); }
            @Override public Double getPoints() { return null; }
            @Override public SessionMarkCatalogItem getMarkValue() { return model.getMark(); }
            @Override public String getComment() { return null; }
        };
    }

    protected void saveAdditionalMarkData(M model)
    {
        // для переопределения в проектах
    }

    @Override
    public void prepareListDataSource(M m)
    {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
