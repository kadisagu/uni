package ru.tandemservice.unisession.dao.bulletin;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Session;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.commonbase.base.util.INumberGenerationRule;
import org.tandemframework.shared.commonbase.base.util.INumberQueueDAO;
import org.tandemframework.shared.commonbase.base.util.SimpleNumberGenerationRule;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.dao.group.IEppRealGroupRowDAO;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeFCA;
import ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem;
import ru.tandemservice.uniepp.entity.registry.EppRegistryAttestation;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4ActionType;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4ActionTypeRow;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.dao.document.ISessionDocumentBaseDAO;
import ru.tandemservice.unisession.entity.comission.SessionComission;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.*;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class SessionBulletinDAO extends UniBaseDao implements ISessionBulletinDAO {

    @Override
    public boolean isThemeRequired(EppGroupType eppGroupType)
    {
        return eppGroupType instanceof EppGroupTypeFCA && existsEntity(
                EppFControlActionType.class,
                EppFControlActionType.eppGroupType().s(), eppGroupType,
                EppFControlActionType.group().themeRequired().s(), Boolean.TRUE
        );
    }

    @Override
    public boolean isBulletinRequireTheme(SessionBulletinDocument bulletin)
    {
		EppRealEduGroup4ActionType group = bulletin.getGroup();
        return group.getActivityPart().getRegistryElement().getParent().isThemeRequired() || isThemeRequired(group.getType());
    }

    @Override
    @SuppressWarnings("unchecked")
    public HashSet<Long> findBulletinsToDelete(final Collection<Long> ids)
    {
        final HashSet<Long> documentsCanBeDeleted = new HashSet<>();
        final DQLSelectBuilder bulDQL = new DQLSelectBuilder()
        .fromEntity(SessionBulletinDocument.class, "bul")
        .column("bul.id")
        .where(in(property(SessionBulletinDocument.id().fromAlias("bul")), ids))
        .where(isNull(property(SessionBulletinDocument.closeDate().fromAlias("bul"))))
        .where(eq(
            new DQLSelectBuilder()
            .fromEntity(SessionMark.class, "m")
            .where(eq(property(SessionMark.slot().document().id().fromAlias("m")), property(SessionBulletinDocument.id().fromAlias("bul"))))
            .column("count(m.id)")
            .buildQuery(),
            value(0)));

        documentsCanBeDeleted.addAll(bulDQL.createStatement(this.getSession()).<Long>list());
        return documentsCanBeDeleted;
    }

    @Override
    @SuppressWarnings("unchecked")
    public INumberGenerationRule<SessionDocument> getNumberGenerationRule() {
        // реализована сквозная (по всему вузу) нумерация ведомостей в рамках года
        return new SimpleNumberGenerationRule<SessionDocument>() {
            @Override public Set<String> getUsedNumbers(final SessionDocument object) {
                final SessionObject sessionObject = this.getSessionObject(object);
                final HashSet<String> numbers = new HashSet<>();

                numbers.addAll(
                    new DQLSelectBuilder()
                    .fromEntity(SessionBulletinDocument.class, "x")
                    .column(property(SessionBulletinDocument.number().fromAlias("x")))
                    .where(eq(property(SessionBulletinDocument.sessionObject().educationYear().fromAlias("x")), value(sessionObject.getEducationYear())))
                    .predicate(DQLPredicateType.distinct)
                    .createStatement(SessionBulletinDAO.this.getSession()).<String>list()
                );

                numbers.addAll(
                    new DQLSelectBuilder()
                    .fromEntity(SessionRetakeDocument.class, "x")
                    .column(property(SessionRetakeDocument.number().fromAlias("x")))
                    .where(eq(property(SessionRetakeDocument.sessionObject().educationYear().fromAlias("x")), value(sessionObject.getEducationYear())))
                    .predicate(DQLPredicateType.distinct)
                    .createStatement(SessionBulletinDAO.this.getSession()).<String>list()
                );

                return numbers;
            }

            @Override public String getNumberQueueName(final SessionDocument object) {
                return "SessionBulletin."+this.getSessionObject(object).getEducationYear().getIntValue();
            }

            private SessionObject getSessionObject(final SessionDocument object)
            {
                if (object instanceof SessionBulletinDocument) {
                    return ((SessionBulletinDocument) object).getSessionObject();
                }
                if (object instanceof SessionRetakeDocument) {
                    return ((SessionRetakeDocument) object).getSessionObject();
                }
                throw new IllegalStateException();
            }
        };
    }

    protected Session lock(final SessionObject sessionObject) {
        final Session session = this.getSession();
        NamedSyncInTransactionCheckLocker.register(session, String.valueOf(sessionObject.getId()));
        return session;
    }


    @Override
    public Map<Long, SessionBulletinDocument> doCreateBulletin(final Long sessionObjectId, final Collection<Long> eppRealEduGroupIds)
    {
        if (CollectionUtils.isEmpty(eppRealEduGroupIds))
	        return Collections.emptyMap();

        final SessionObject sessionObject = get(SessionObject.class, sessionObjectId);
        final Session session = lock(sessionObject);

        final Date now = new Date();
        final boolean ignoreCompleteLevel = ISessionDocumentBaseDAO.instance.get().isIgnoreRealEduGroupCompleteLevel();

        final Map<Long, SessionBulletinDocument> realEduGroup2Bulletin = new HashMap<>(eppRealEduGroupIds.size());
        BatchUtils.execute(eppRealEduGroupIds, 32, new BatchUtils.Action<Long>()
        {
            private final Map<Long, EppRealEduGroup4ActionType> groupCache = SessionBulletinDAO.this.getLoadCacheMap(EppRealEduGroup4ActionType.class);
            private final Map<Long, PpsEntry> ppsCache = SessionBulletinDAO.this.getLoadCacheMap(PpsEntry.class);

            @Override
            public void execute(Collection<Long> realEduGroupIds)
            {
                if (!ignoreCompleteLevel)
                {
                    // только те, для которых есть степень готовности и она допустимая
	                List<Long> goodIds = new DQLSelectBuilder()
			                .fromEntity(EppRealEduGroup4ActionType.class, "x").column(property("x.id"))
			                .where(eq(property("x", EppRealEduGroup4ActionType.level().readyForFinalControlAction()), value(Boolean.TRUE)))
			                .where(in("x.id", realEduGroupIds))
			                .createStatement(session).list();
	                realEduGroupIds = goodIds.stream().distinct().collect(Collectors.toList());
                }

                final Multimap<Long, Long> group2WpeParts = getGroup2WpeParts(realEduGroupIds);
                final Multimap<Long, Long> groupToPpsCollection = getGroup2PpsCollection(group2WpeParts.keySet());
                for (final Map.Entry<Long, Collection<Long>> entry: group2WpeParts.asMap().entrySet())
                {
					final long groupId = entry.getKey();

					// для каждой ведомости - своя комиссия
					final SessionComission commission = new SessionComission();
					session.save(commission);

					final Collection<Long> tutors = groupToPpsCollection.get(groupId);
					for (final Long tutor: new HashSet<>(tutors))
					    session.save(new SessionComissionPps(commission, this.ppsCache.get(tutor)));

					// создается ведомость
					final SessionBulletinDocument bulletin = new SessionBulletinDocument(sessionObject, this.groupCache.get(groupId));
					bulletin.setFormingDate(now);
					bulletin.setNumber(INumberQueueDAO.instance.get().getNextNumber(bulletin));
					bulletin.setCommission(commission);
					session.save(bulletin);
					realEduGroup2Bulletin.put(groupId, bulletin);

					// включаем студентов в ведомость
					createDocumentSlots(bulletin, entry.getValue(), commission);

					SessionBulletinDAO.this.logger.info("do-create-bulletin[" + groupId + "]");
                }

                // актуализируем данные по студентам УГС (которые включаем)
                IEppRealGroupRowDAO.instance.get().doUpdateStudentRowData(relation -> in(property("x", EppRealEduGroupRow.group().id()), group2WpeParts.keySet()));

            }

            protected Multimap<Long, Long> getGroup2WpeParts(final Collection<Long> groupIds)
            {
	            if (CollectionUtils.isEmpty(groupIds))
		            return HashMultimap.create();

	            final String alias = "eduGroupRow";
                final DQLSelectBuilder dql = new DQLSelectBuilder()
						.fromEntity(EppRealEduGroup4ActionTypeRow.class, alias)
						.column(property(alias, EppRealEduGroup4ActionTypeRow.group().id()))
						.column(property(alias, EppRealEduGroup4ActionTypeRow.studentWpePart().id()))
						.where(isNull(property(alias, EppRealEduGroup4ActionTypeRow.removalDate())))
						.where(in(property(alias, EppRealEduGroup4ActionTypeRow.group().id()), groupIds));

	            final Multimap<Long, Long> group2WpeParts = HashMultimap.create();
                for (final Object[] row: UniBaseDao.scrollRows(dql.createStatement(session)))
                    group2WpeParts.put((Long)row[0], (Long)row[1]);
	            return group2WpeParts;
            }

            protected Multimap<Long, Long> getGroup2PpsCollection(final Collection<Long> groupIds)
            {
	            if (CollectionUtils.isEmpty(groupIds))
		            return HashMultimap.create();

	            final String alias = "ppsItem";
                final DQLSelectBuilder dql = new DQLSelectBuilder()
						.fromEntity(EppPpsCollectionItem.class, alias)
						.column(property(alias, EppPpsCollectionItem.list().id()))
						.column(property(alias, EppPpsCollectionItem.pps().id()))
						.where(in(property(alias, EppPpsCollectionItem.list().id()), groupIds));

	            final Multimap<Long, Long> group2PpsCollection = HashMultimap.create();
	            for (final Object[] row: UniBaseDao.scrollRows(dql.createStatement(session)))
		            group2PpsCollection.put((Long)row[0], (Long)row[1]);
	            return group2PpsCollection;
            }

	        private void createDocumentSlots(SessionBulletinDocument bulletin, Collection<Long> wpePartIds, SessionComission commission)
	        {
		        final boolean themeRequired = isFinalQualifyBulletin(bulletin);
		        for (final Long wpePartId: wpePartIds)
		        {
			        final EppStudentWpeCAction wpeCAction = (EppStudentWpeCAction)session.load(EppStudentWpeCAction.class, wpePartId);
			        final SessionDocumentSlot slot = new SessionDocumentSlot(bulletin, wpeCAction, true);
			        slot.setCommission(commission);
			        session.save(slot);

			        if (themeRequired && (slot.getActualStudent().getFinalQualifyingWorkTheme() != null))
			        {
				        SessionProjectTheme theme = createTheme(slot);
				        session.save(theme);
			        }
		        }
	        }

	        private boolean isFinalQualifyBulletin(SessionBulletinDocument bulletin)
	        {
		        EppRegistryElement elementPart = bulletin.getGroup().getActivityPart().getRegistryElement();
		        return (elementPart instanceof EppRegistryAttestation) && (elementPart.getParent().isThemeRequired());
	        }

	        private SessionProjectTheme createTheme(SessionDocumentSlot slot)
	        {
		        SessionProjectTheme theme = new SessionProjectTheme();
		        Student student = slot.getActualStudent();
		        theme.setSlot(slot);
		        theme.setTheme(student.getFinalQualifyingWorkTheme());
		        theme.setSupervisor(student.getPpsEntry());
		        return theme;
	        }

        });
        return realEduGroup2Bulletin;
    }


    @Override
    public void doRefreshBulletin(final Long bulletinId)
    {
        final SessionBulletinDocument bulletin = this.get(SessionBulletinDocument.class, bulletinId);
        if (bulletin.isClosed()) {
            throw new ApplicationException("Нельзя изменять закрытую ведомость.");
        }

        final Session session = lock(bulletin.getSessionObject());

        // актуализируем данные по студентам УГС (которые обновляем)
        // при ручном обновлении ведомости надо явно обновлять НПП и названия групп студентов
        // (только в текущем учебном году)
        IEppRealGroupRowDAO.instance.get().doUpdateStudentRowData(
                relation -> eq(property(EppRealEduGroupRow.group().fromAlias("x")), value(bulletin.getGroup()))
        );

        final List<SessionDocumentSlot> slotList = this.getList(SessionDocumentSlot.class, SessionDocumentSlot.document().id().s(), bulletinId);

        final DQLSelectBuilder marks = new DQLSelectBuilder()
        .fromEntity(SessionMark.class, "mark")
        .column(property(SessionMark.slot().fromAlias("mark")))
        .where(eq(property(SessionMark.slot().document().fromAlias("mark")), value(bulletin)));

        final Set<SessionDocumentSlot> slotsWithMark = new HashSet<>();
        slotsWithMark.addAll(marks.createStatement(session).<SessionDocumentSlot>list());

        final DQLSelectBuilder rows = new DQLSelectBuilder()
        .fromEntity(EppRealEduGroup4ActionTypeRow.class, "rel")
        .column(property(EppRealEduGroup4ActionTypeRow.studentWpePart().fromAlias("rel")))
        .where(isNull(property(EppRealEduGroup4ActionTypeRow.removalDate().fromAlias("rel"))))
        .where(eq(property(EppRealEduGroup4ActionTypeRow.group().fromAlias("rel")), value(bulletin.getGroup())));

        final Set<EppStudentWpeCAction> groupSlots = new HashSet<>();
        groupSlots.addAll(rows.createStatement(session).<EppStudentWpeCAction>list());

        for (final SessionDocumentSlot slot : slotList)
        {
            if (!groupSlots.contains(slot.getStudentWpeCAction()) && !slotsWithMark.contains(slot)) {
                session.delete(slot);
            }
            groupSlots.remove(slot.getStudentWpeCAction());
        }

        for (final EppStudentWpeCAction groupSlot : groupSlots)
        {
            final SessionDocumentSlot slot = new SessionDocumentSlot(bulletin, groupSlot, true);
            slot.setCommission(bulletin.getCommission());
            session.save(slot);
        }


    }
}
