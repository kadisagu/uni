/**
 *$Id$
 */
package ru.tandemservice.unisession.base.bo.SessionIndicators;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

/**
 * @author Alexander Shaburov
 * @since 05.12.12
 */
@Configuration
public class SessionIndicatorsManager extends BusinessObjectManager
{
    public static SessionIndicatorsManager instance()
    {
        return instance(SessionIndicatorsManager.class);
    }
}
