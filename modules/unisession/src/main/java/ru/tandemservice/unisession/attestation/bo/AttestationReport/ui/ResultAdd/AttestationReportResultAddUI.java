/**
 *$Id:$
 */
package ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.ResultAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.unisession.attestation.bo.AttestationReport.AttestationReportManager;
import ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.ResultAdd.util.Model;
import ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.ResultPub.AttestationReportResultPub;
import ru.tandemservice.unisession.attestation.entity.report.SessionAttestationResultReport;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;

/**
 * @author Alexander Shaburov
 * @since 14.11.12
 */
@State
({
    @Bind(key = SessionReportManager.BIND_ORG_UNIT, binding = "orgUnitHolder.id", required = true)
})
public class AttestationReportResultAddUI extends UIPresenter
{
    // fields
    private OrgUnitHolder _orgUnitHolder = new OrgUnitHolder();

    private Model _model;

    @Override
    public void onComponentRefresh()
    {
        _orgUnitHolder.refresh();
        _model = new Model();
    }

    public void onClickApply()
    {
        ErrorCollector errorCollector = AttestationReportManager.instance().attestationReportResultDao().validate(_model);

        if (errorCollector.hasErrors())
            return;

        _model.setOrgUnit(_orgUnitHolder.getValue());

        RtfDocument document = AttestationReportManager.instance().attestationReportResultDao().createReportRtfDocument(_model);

        SessionAttestationResultReport report = AttestationReportManager.instance().attestationReportResultDao().saveReport(_model, document);

        deactivate();
        getActivationBuilder()
                .asDesktopRoot(AttestationReportResultPub.class)
                .parameter(UIPresenter.PUBLISHER_ID, report.getId())
                .activate();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(AttestationReportManager.PARAM_ORG_UNIT, _orgUnitHolder.getValue());
        dataSource.put(AttestationReportManager.PARAM_EDU_YEAR_ONLY_CURRENT, true);
        dataSource.put(AttestationReportManager.PARAM_ATTESTATION, _model.getAttestation());
        dataSource.put(AttestationReportManager.PARAM_COURSE, _model.getCourse());
    }

    @Override
    public ISecured getSecuredObject()
    {
        return _orgUnitHolder.getValue();
    }

    // Getters & Setters


    public Model getModel()
    {
        return _model;
    }

    public OrgUnitHolder getOrgUnitHolder()
    {
        return _orgUnitHolder;
    }

    public CommonPostfixPermissionModelBase getSec()
    {
        return _orgUnitHolder.getSecModel();
    }
}
