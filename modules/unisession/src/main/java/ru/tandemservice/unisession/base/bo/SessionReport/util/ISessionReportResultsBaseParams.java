/* $Id:$ */
package ru.tandemservice.unisession.base.bo.SessionReport.util;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.catalog.EppWorkPlanRowKind;

import java.util.Date;
import java.util.List;

/**
 * @author oleyba
 * @since 2/19/12
 */
public interface ISessionReportResultsBaseParams
{
    OrgUnit getOrgUnit();

    YearDistributionPart getYearPart();

    EducationYear getEducationYear();

    List<OrgUnit> getGroupOrgUnitList();

    List<EppWorkPlanRowKind> getDiscKindList();

    boolean isTerritorialOrgUnitActive();

    List<OrgUnit> getTerritorialOrgUnitList();

    boolean isDevelopFormActive();

    List<DevelopForm> getDevelopFormList();

    boolean isDevelopConditionActive();

    List<DevelopCondition> getDevelopConditionList();

    boolean isDevelopTechActive();

    List<DevelopTech> getDevelopTechList();

    boolean isDevelopPeriodActive();

    List<DevelopPeriod> getDevelopPeriodList();

    boolean isCourseActive();

    List<Course> getCourseList();

    boolean isCompensationTypeActive();

    CompensationType getCompensationType();

    boolean isStudentStatusActive();

    List<StudentStatus> getStudentStatusList();

    boolean isDeadlineCheckActive();

    Date getDeadlineCheck();

    boolean isTargetAdmission();
}
