/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionProtocol.ui.AbstractAllEdit;


import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.IColumnListExtPointBuilder;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unisession.entity.catalog.SessionRoleInGEC;
import ru.tandemservice.unisession.entity.stateFinalExam.SessionStateFinalExamProtocol;

/**
 * @author Andrey Andreev
 * @since 12.09.2016
 */
public abstract class SessionProtocolAbstractAllEdit extends BusinessComponentManager
{
    public static final String BULLETIN_ID_PARAM = "bulletinId";
    public static final String BULLETIN_PARAM = "bulletin";
    public static final String STATE_FINAL_EXAM_PROTOCOL_DS = "protocolListDS";

    public static final String SEARCH_LIST_COLUMN_NUMBER = "number";
    public static final String SEARCH_LIST_COLUMN_STUDENT = "student";
    public static final String SEARCH_LIST_COLUMN_ROLES = "roles";
    public static final String SEARCH_LIST_COLUMN_COMMENT = "comment";


    public void addProtocolListColumns(IColumnListExtPointBuilder builder)
    {
        builder.addColumn(textColumn(SEARCH_LIST_COLUMN_NUMBER, SessionStateFinalExamProtocol.number().s()))
                .addColumn(textColumn(SEARCH_LIST_COLUMN_STUDENT, SessionStateFinalExamProtocol.documentSlot().studentWpeCAction().studentWpe().student().fullFio().s()).order());

        // Колонки ГЭК
        DataAccessServices.dao().getList(SessionRoleInGEC.class, SessionRoleInGEC.priority().s())
                .forEach(role -> builder.addColumn(blockColumn(role.getCode(), SEARCH_LIST_COLUMN_ROLES)));

        customProtocolListColumn(builder);

        builder.addColumn(blockColumn(SEARCH_LIST_COLUMN_COMMENT));
    }

    public void customProtocolListColumn(IColumnListExtPointBuilder builder)
    {

    }
}