/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.StateFinalAttestationResultAdd.logic;

import ru.tandemservice.unisession.entity.report.StateFinalAttestationResult;

/**
 * @author Andrey Andreev
 * @since 28.10.2016
 */
public interface IStateFinalAttestationResultPrintDAO
{
    /**
     * @param report параметры отчета
     * @param orgUnitId подразделение на котором создается отчет
     * @return Отчет
     */
    StateFinalAttestationResult createStoredReport(StateFinalAttestationResult report, Long orgUnitId);
}
