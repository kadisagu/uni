/* $Id:$ */
package ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinMark;

import com.google.common.base.Objects;
import com.google.common.collect.Table;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;
import ru.tandemservice.unisession.entity.document.SessionDocument;
import ru.tandemservice.unisession.entity.document.SessionObject;

import java.util.*;

/**
 * @author oleyba
 * @since 3/1/11
 */
@Input({
    @Bind(key= PublisherActivator.PUBLISHER_ID_KEY, binding="documentHolder.id")
})
public class Model
{
    public static final String COMPONENT_NAME = Model.class.getPackage().getName();

    private BulletinMarkRow _currentRow;
    private List<BulletinMarkRow> _rowList;
    private PpsEntry _currentTutor;
    private Table<Long, Long, Boolean> _slot2ppsTable;
    private boolean _eppGroupTitleColumnVisible;
    private boolean _showPracticeTutorMarkColumn;

    private final SessionMarkableDocHolder documentHolder = new SessionMarkableDocHolder();
    private List<PpsEntry> tutors;

    private String registryElementTitle;
    private boolean showCATypeColumn;

    private boolean useCurrentRating;
    private boolean usePoints;
    private boolean needToSaveCurrentRatingData;

    private Map<ISessionBrsDao.ISessionRatingSettingsKey, ISessionBrsDao.ISessionRatingSettings> ratingSettings;

    public SessionDocument getDocument()
    {
        return this.getDocumentHolder().getValue();
    }

    public Date getDocPerformDate()
    {
        return this.getDocumentHolder().getProperties().getPerformDate();
    }

    public SessionObject getSessionObject()
    {
        return this.getDocumentHolder().getProperties().getSessionObject();
    }

    public SessionBulletinDocument getBulletin()
    {
        return this.getDocument() instanceof SessionBulletinDocument ? (SessionBulletinDocument) this.getDocument() : null;
    }

    // accessors

    public SessionMarkableDocHolder getDocumentHolder()
    {
        return this.documentHolder;
    }

    public List<PpsEntry> getTutors()
    {
        return this.tutors;
    }

    public void setTutors(final List<PpsEntry> tutors)
    {
        this.tutors = tutors;
    }

    public String getRegistryElementTitle()
    {
        return this.registryElementTitle;
    }

    public void setRegistryElementTitle(final String registryElementTitle)
    {
        this.registryElementTitle = registryElementTitle;
    }

    public boolean isShowCATypeColumn()
    {
        return this.showCATypeColumn;
    }

    public void setShowCATypeColumn(final boolean showCATypeColumn)
    {
        this.showCATypeColumn = showCATypeColumn;
    }

    public boolean isUseCurrentRating()
    {
        return useCurrentRating;
    }

    public void setUseCurrentRating(boolean useCurrentRating)
    {
        this.useCurrentRating = useCurrentRating;
    }

    public boolean isNeedToSaveCurrentRatingData()
    {
        return needToSaveCurrentRatingData;
    }

    public void setNeedToSaveCurrentRatingData(boolean needToSaveCurrentRatingData)
    {
        this.needToSaveCurrentRatingData = needToSaveCurrentRatingData;
    }

    public boolean isUsePoints()
    {
        return usePoints;
    }

    public void setUsePoints(boolean usePoints)
    {
        this.usePoints = usePoints;
    }

    public Map<ISessionBrsDao.ISessionRatingSettingsKey, ISessionBrsDao.ISessionRatingSettings> getRatingSettings()
    {
        return ratingSettings;
    }

    public void setRatingSettings(Map<ISessionBrsDao.ISessionRatingSettingsKey, ISessionBrsDao.ISessionRatingSettings> ratingSettings)
    {
        this.ratingSettings = ratingSettings;
    }

    public BulletinMarkRow getCurrentRow()
    {
        return _currentRow;
    }

    public void setCurrentRow(BulletinMarkRow currentRow)
    {
        _currentRow = currentRow;
    }

    public List<BulletinMarkRow> getRowList()
    {
        return _rowList;
    }

    public void setRowList(List<BulletinMarkRow> rowList)
    {
        _rowList = rowList;
    }

    public PpsEntry getCurrentTutor()
    {
        return _currentTutor;
    }

    public void setCurrentTutor(PpsEntry currentTutor)
    {
        _currentTutor = currentTutor;
    }

    public Boolean getSlot2pps()
    {
        return Objects.firstNonNull(_slot2ppsTable.get(getCurrentRow().getSlot().getId(), getCurrentTutor().getId()), Boolean.FALSE);
    }

    public void setSlot2pps(Boolean value)
    {
        _slot2ppsTable.put(getCurrentRow().getSlot().getId(), getCurrentTutor().getId(), value);
    }

    public void setSlot2ppsTable(Table<Long, Long, Boolean> slot2ppsTable)
    {
        _slot2ppsTable = slot2ppsTable;
    }

    public Table<Long, Long, Boolean> getSlot2ppsTable()
    {
        return _slot2ppsTable;
    }

    public int getColumnCount()
    {
        return (isEppGroupTitleColumnVisible() ? 1 : 0) +
                (isShowCATypeColumn() ? 1 : 0) +
                (isShowPracticeTutorMarkColumn() ? 1 : 0) +
                (isUseCurrentRating() ? 1 : 0) +
                (isUsePoints() ? 2 : 1) +
                getTutors().size()
                + 7;
    }

    public boolean isEppGroupTitleColumnVisible()
    {
        return _eppGroupTitleColumnVisible;
    }

    public void setEppGroupTitleColumnVisible(boolean eppGroupTitleColumnVisible)
    {
        _eppGroupTitleColumnVisible = eppGroupTitleColumnVisible;
    }

    public boolean isShowPracticeTutorMarkColumn()
    {
        return _showPracticeTutorMarkColumn;
    }

    public void setShowPracticeTutorMarkColumn(boolean showPracticeTutorMarkColumn)
    {
        _showPracticeTutorMarkColumn = showPracticeTutorMarkColumn;
    }
}
