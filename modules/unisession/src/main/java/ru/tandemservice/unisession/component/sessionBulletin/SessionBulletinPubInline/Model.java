// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinPubInline;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uniepp.dao.group.EppRealGroupRowDAO;
import ru.tandemservice.uniepp.dao.student.EppStudentSlotDAO;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.catalog.codes.EppRegistryStructureCodes;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;

import java.util.*;

/**
 * @author oleyba
 * @since 2/18/11
 */
@Input({
    @Bind(key=PublisherActivator.PUBLISHER_ID_KEY, binding="bulletin.id", required = true),
    @Bind(key=ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinPubInline.Model.BIND_COLUMN_PKEY_HANDLER, binding="columnPKeyHandler", required = true)
})
public class Model
{
    public static final String COMPONENT_NAME = Model.class.getPackage().getName();
    public static final String BIND_COLUMN_PKEY_HANDLER = "columnPKeyHandler";

    private SessionBulletinDocument bulletin = new SessionBulletinDocument();
    private DynamicListDataSource<SessionDocumentSlot> dataSource = new DynamicListDataSource<SessionDocumentSlot>();

    private boolean useCurrentRating;
    private boolean usePoints;

    private boolean _showPracticeTutorMarkColumn;
    private Boolean _isStateFinalExam;

    private ISessionBulletinColumnPKeyHandler columnPKeyHandler;

    public interface ISessionBulletinColumnPKeyHandler
    {
        String getAllowanceColumnPKey();
        String getAddSheetColumnPKey();
    }

    public String getEduGroupsDaemonStatus(final String message) {
        Long date;
        {
            date = EppStudentSlotDAO.DAEMON.getCompleteStatus();
            if (null == date) { return message; }
        }
        {
            date = EppRealGroupRowDAO.DAEMON.getCompleteStatus();
            if (null == date) { return message; }
        }
        return DateFormatter.DATE_FORMATTER_WITH_TIME.format(new Date(date));
    }

    // getters and setters

    public boolean isUseCurrentRating()
    {
        return useCurrentRating;
    }

    public void setUseCurrentRating(boolean useCurrentRating)
    {
        this.useCurrentRating = useCurrentRating;
    }

    public boolean isUsePoints()
    {
        return usePoints;
    }

    public void setUsePoints(boolean usePoints)
    {
        this.usePoints = usePoints;
    }

    public SessionBulletinDocument getBulletin()
    {
        return this.bulletin;
    }

    public void setBulletin(final SessionBulletinDocument bulletin)
    {
        this.bulletin = bulletin;
    }

    public DynamicListDataSource<SessionDocumentSlot> getDataSource()
    {
        return this.dataSource;
    }

    public void setDataSource(final DynamicListDataSource<SessionDocumentSlot> dataSource)
    {
        this.dataSource = dataSource;
    }

    public ISessionBulletinColumnPKeyHandler getColumnPKeyHandler()
    {
        return columnPKeyHandler;
    }

    public void setColumnPKeyHandler(ISessionBulletinColumnPKeyHandler columnPKeyHandler)
    {
        this.columnPKeyHandler = columnPKeyHandler;
    }

    public boolean isShowPracticeTutorMarkColumn()
    {
        return _showPracticeTutorMarkColumn;
    }

    public void setShowPracticeTutorMarkColumn(boolean showPracticeTutorMarkColumn)
    {
        _showPracticeTutorMarkColumn = showPracticeTutorMarkColumn;
    }

    public boolean isStateFinalExam()
    {
        if (_isStateFinalExam == null)
        {
            EppRegistryStructure structure = bulletin.getGroup().getActivityPart().getRegistryElement().getParent().getParent();
            _isStateFinalExam = structure != null && EppRegistryStructureCodes.REGISTRY_ATTESTATION.equals(structure.getCode());
        }

        return _isStateFinalExam;
    }
}
