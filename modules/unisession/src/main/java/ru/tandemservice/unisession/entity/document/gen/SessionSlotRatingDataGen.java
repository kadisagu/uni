package ru.tandemservice.unisession.entity.document.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionSlotRatingData;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Данные рейтинга для записи студента в документе сессии
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionSlotRatingDataGen extends EntityBase
 implements INaturalIdentifiable<SessionSlotRatingDataGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.document.SessionSlotRatingData";
    public static final String ENTITY_NAME = "sessionSlotRatingData";
    public static final int VERSION_HASH = 255215138;
    private static IEntityMeta ENTITY_META;

    public static final String L_SLOT = "slot";
    public static final String P_FIXED_CURRENT_RATING_AS_LONG = "fixedCurrentRatingAsLong";
    public static final String P_ALLOWED = "allowed";
    public static final String P_FIXED_CURRENT_RATING = "fixedCurrentRating";

    private SessionDocumentSlot _slot;     // Запись студента в документе сессии
    private Long _fixedCurrentRatingAsLong;     // Текущий рейтинг
    private boolean _allowed = true;     // Допуск

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Запись студента в документе сессии. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public SessionDocumentSlot getSlot()
    {
        return _slot;
    }

    /**
     * @param slot Запись студента в документе сессии. Свойство не может быть null и должно быть уникальным.
     */
    public void setSlot(SessionDocumentSlot slot)
    {
        dirty(_slot, slot);
        _slot = slot;
    }

    /**
     * Содержит рассчитанное и зафиксированное значение текущего рейтинга,
     * для использования при выставлении оценки по данной строке.
     *
     * @return Текущий рейтинг.
     */
    public Long getFixedCurrentRatingAsLong()
    {
        return _fixedCurrentRatingAsLong;
    }

    /**
     * @param fixedCurrentRatingAsLong Текущий рейтинг.
     */
    public void setFixedCurrentRatingAsLong(Long fixedCurrentRatingAsLong)
    {
        dirty(_fixedCurrentRatingAsLong, fixedCurrentRatingAsLong);
        _fixedCurrentRatingAsLong = fixedCurrentRatingAsLong;
    }

    /**
     * Допущен ли студент до сдачи мероприятия по правилам БРС.
     *
     * @return Допуск. Свойство не может быть null.
     */
    @NotNull
    public boolean isAllowed()
    {
        return _allowed;
    }

    /**
     * @param allowed Допуск. Свойство не может быть null.
     */
    public void setAllowed(boolean allowed)
    {
        dirty(_allowed, allowed);
        _allowed = allowed;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SessionSlotRatingDataGen)
        {
            if (withNaturalIdProperties)
            {
                setSlot(((SessionSlotRatingData)another).getSlot());
            }
            setFixedCurrentRatingAsLong(((SessionSlotRatingData)another).getFixedCurrentRatingAsLong());
            setAllowed(((SessionSlotRatingData)another).isAllowed());
        }
    }

    public INaturalId<SessionSlotRatingDataGen> getNaturalId()
    {
        return new NaturalId(getSlot());
    }

    public static class NaturalId extends NaturalIdBase<SessionSlotRatingDataGen>
    {
        private static final String PROXY_NAME = "SessionSlotRatingDataNaturalProxy";

        private Long _slot;

        public NaturalId()
        {}

        public NaturalId(SessionDocumentSlot slot)
        {
            _slot = ((IEntity) slot).getId();
        }

        public Long getSlot()
        {
            return _slot;
        }

        public void setSlot(Long slot)
        {
            _slot = slot;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof SessionSlotRatingDataGen.NaturalId) ) return false;

            SessionSlotRatingDataGen.NaturalId that = (NaturalId) o;

            if( !equals(getSlot(), that.getSlot()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getSlot());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getSlot());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionSlotRatingDataGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionSlotRatingData.class;
        }

        public T newInstance()
        {
            return (T) new SessionSlotRatingData();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "slot":
                    return obj.getSlot();
                case "fixedCurrentRatingAsLong":
                    return obj.getFixedCurrentRatingAsLong();
                case "allowed":
                    return obj.isAllowed();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "slot":
                    obj.setSlot((SessionDocumentSlot) value);
                    return;
                case "fixedCurrentRatingAsLong":
                    obj.setFixedCurrentRatingAsLong((Long) value);
                    return;
                case "allowed":
                    obj.setAllowed((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "slot":
                        return true;
                case "fixedCurrentRatingAsLong":
                        return true;
                case "allowed":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "slot":
                    return true;
                case "fixedCurrentRatingAsLong":
                    return true;
                case "allowed":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "slot":
                    return SessionDocumentSlot.class;
                case "fixedCurrentRatingAsLong":
                    return Long.class;
                case "allowed":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionSlotRatingData> _dslPath = new Path<SessionSlotRatingData>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionSlotRatingData");
    }
            

    /**
     * @return Запись студента в документе сессии. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unisession.entity.document.SessionSlotRatingData#getSlot()
     */
    public static SessionDocumentSlot.Path<SessionDocumentSlot> slot()
    {
        return _dslPath.slot();
    }

    /**
     * Содержит рассчитанное и зафиксированное значение текущего рейтинга,
     * для использования при выставлении оценки по данной строке.
     *
     * @return Текущий рейтинг.
     * @see ru.tandemservice.unisession.entity.document.SessionSlotRatingData#getFixedCurrentRatingAsLong()
     */
    public static PropertyPath<Long> fixedCurrentRatingAsLong()
    {
        return _dslPath.fixedCurrentRatingAsLong();
    }

    /**
     * Допущен ли студент до сдачи мероприятия по правилам БРС.
     *
     * @return Допуск. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionSlotRatingData#isAllowed()
     */
    public static PropertyPath<Boolean> allowed()
    {
        return _dslPath.allowed();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.entity.document.SessionSlotRatingData#getFixedCurrentRating()
     */
    public static SupportedPropertyPath<Double> fixedCurrentRating()
    {
        return _dslPath.fixedCurrentRating();
    }

    public static class Path<E extends SessionSlotRatingData> extends EntityPath<E>
    {
        private SessionDocumentSlot.Path<SessionDocumentSlot> _slot;
        private PropertyPath<Long> _fixedCurrentRatingAsLong;
        private PropertyPath<Boolean> _allowed;
        private SupportedPropertyPath<Double> _fixedCurrentRating;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Запись студента в документе сессии. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unisession.entity.document.SessionSlotRatingData#getSlot()
     */
        public SessionDocumentSlot.Path<SessionDocumentSlot> slot()
        {
            if(_slot == null )
                _slot = new SessionDocumentSlot.Path<SessionDocumentSlot>(L_SLOT, this);
            return _slot;
        }

    /**
     * Содержит рассчитанное и зафиксированное значение текущего рейтинга,
     * для использования при выставлении оценки по данной строке.
     *
     * @return Текущий рейтинг.
     * @see ru.tandemservice.unisession.entity.document.SessionSlotRatingData#getFixedCurrentRatingAsLong()
     */
        public PropertyPath<Long> fixedCurrentRatingAsLong()
        {
            if(_fixedCurrentRatingAsLong == null )
                _fixedCurrentRatingAsLong = new PropertyPath<Long>(SessionSlotRatingDataGen.P_FIXED_CURRENT_RATING_AS_LONG, this);
            return _fixedCurrentRatingAsLong;
        }

    /**
     * Допущен ли студент до сдачи мероприятия по правилам БРС.
     *
     * @return Допуск. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionSlotRatingData#isAllowed()
     */
        public PropertyPath<Boolean> allowed()
        {
            if(_allowed == null )
                _allowed = new PropertyPath<Boolean>(SessionSlotRatingDataGen.P_ALLOWED, this);
            return _allowed;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.entity.document.SessionSlotRatingData#getFixedCurrentRating()
     */
        public SupportedPropertyPath<Double> fixedCurrentRating()
        {
            if(_fixedCurrentRating == null )
                _fixedCurrentRating = new SupportedPropertyPath<Double>(SessionSlotRatingDataGen.P_FIXED_CURRENT_RATING, this);
            return _fixedCurrentRating;
        }

        public Class getEntityClass()
        {
            return SessionSlotRatingData.class;
        }

        public String getEntityName()
        {
            return "sessionSlotRatingData";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract Double getFixedCurrentRating();
}
