/* $*/

package ru.tandemservice.unisession.component.allowance.SessionAllowanceAdd;

import ru.tandemservice.uni.dao.IListDataSourceDao;
import ru.tandemservice.uni.dao.IUpdateable;

/**
 * @author oleyba
 * @since 3/24/11
 */
public interface IDAO extends IUpdateable<Model>, IListDataSourceDao<Model>
{

}
