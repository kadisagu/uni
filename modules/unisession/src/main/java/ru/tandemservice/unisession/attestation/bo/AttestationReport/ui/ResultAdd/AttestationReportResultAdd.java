/**
 *$Id:$
 */
package ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.ResultAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.unisession.attestation.bo.AttestationReport.AttestationReportManager;

/**
 * @author Alexander Shaburov
 * @since 14.11.12
 */
@Configuration
public class AttestationReportResultAdd extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(AttestationReportManager.instance().attestationDSConfig())
                .addDataSource(UniStudentManger.instance().courseDSConfig())
                .addDataSource(AttestationReportManager.instance().groupDSConfig())
                .create();
    }
}
