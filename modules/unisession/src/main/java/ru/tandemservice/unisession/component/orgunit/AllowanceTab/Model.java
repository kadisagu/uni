/* $Id$ */
package ru.tandemservice.unisession.component.orgunit.AllowanceTab;

import java.util.List;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;

import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.unisession.entity.allowance.ISessionStudentNotAllowed;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;

/**
 * @author oleyba
 * @since 4/19/11
 */
@Input({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "orgUnitHolder.id")
})
public class Model
{
    private final EntityHolder<OrgUnit> orgUnitHolder = new EntityHolder<OrgUnit>();
    private DynamicListDataSource<ISessionStudentNotAllowed> dataSource;

    private CommonPostfixPermissionModel sec;
    private IDataSettings settings;

    private ISelectModel yearModel;
    private List<HSelectOption> partsList;
    private ISelectModel courseModel;
    private ISelectModel groupModel;

    public Long getOrgUnitId()
    {
        return this.getOrgUnitHolder().getId();
    }

    public OrgUnit getOrgUnit()
    {
        return this.getOrgUnitHolder().getValue();
    }

    public String getSettingsKey()
    {
        return "session.allowance." + this.getOrgUnitId();
    }

    // Колонка со ссылкой на ведомость \ надписью

    public boolean isShowBulletinLink()
    {
        return this.getDataSource().getColumnValue() instanceof SessionBulletinDocument;
    }

    public String getBulletinPubComponentName()
    {
        return ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinPub.Model.COMPONENT_NAME;
    }

    public Long getBulletinId()
    {
        if (this.getDataSource().getColumnValue() instanceof SessionBulletinDocument) {
            return ((SessionBulletinDocument) this.getDataSource().getColumnValue()).getId();
        }
        return null;
    }

    public String getBulletinTitle()
    {
        if (this.getDataSource().getColumnValue() instanceof SessionBulletinDocument) {
            return ((SessionBulletinDocument) this.getDataSource().getColumnValue()).getTitle();
        }
        return "";
    }

    // accessors

    public EntityHolder<OrgUnit> getOrgUnitHolder()
    {
        return this.orgUnitHolder;
    }

    public DynamicListDataSource<ISessionStudentNotAllowed> getDataSource()
    {
        return this.dataSource;
    }

    public void setDataSource(final DynamicListDataSource<ISessionStudentNotAllowed> dataSource)
    {
        this.dataSource = dataSource;
    }

    public CommonPostfixPermissionModel getSec()
    {
        return this.sec;
    }

    public void setSec(final CommonPostfixPermissionModel sec)
    {
        this.sec = sec;
    }

    public IDataSettings getSettings()
    {
        return this.settings;
    }

    public void setSettings(final IDataSettings settings)
    {
        this.settings = settings;
    }

    public ISelectModel getYearModel()
    {
        return this.yearModel;
    }

    public void setYearModel(final ISelectModel yearModel)
    {
        this.yearModel = yearModel;
    }

    public List<HSelectOption> getPartsList()
    {
        return this.partsList;
    }

    public void setPartsList(final List<HSelectOption> partsList)
    {
        this.partsList = partsList;
    }

    public ISelectModel getCourseModel()
    {
        return this.courseModel;
    }

    public void setCourseModel(final ISelectModel courseModel)
    {
        this.courseModel = courseModel;
    }

    public ISelectModel getGroupModel()
    {
        return this.groupModel;
    }

    public void setGroupModel(final ISelectModel groupModel)
    {
        this.groupModel = groupModel;
    }
}
