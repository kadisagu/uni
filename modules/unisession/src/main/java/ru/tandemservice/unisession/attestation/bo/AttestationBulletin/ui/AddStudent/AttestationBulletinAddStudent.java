/**
 *$Id:$
 */
package ru.tandemservice.unisession.attestation.bo.AttestationBulletin.ui.AddStudent;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlot;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 25.10.12
 */
@Configuration
public class AttestationBulletinAddStudent extends BusinessComponentManager
{
    // dataSource
    public static final String STUDENT_DS = "studentDS";

    // prop
    public static final String PROP_BULLETIN = "bulletin";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(STUDENT_DS, studentDSHandler())
                        .addColumn("ФИО", EppStudentWorkPlanElement.student().person().fullFio().s())
                        .addColumn("Группа", EppStudentWorkPlanElement.student().group().title().s()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> studentDSHandler()
    {
        EntityComboDataSourceHandler handler = new EntityComboDataSourceHandler(getName(), EppStudentWorkPlanElement.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                SessionAttestationBulletin bulletin = context.get(PROP_BULLETIN);

                DQLSelectBuilder excludeBuilder = new DQLSelectBuilder().fromEntity(SessionAttestationSlot.class, "excl").column(property(SessionAttestationSlot.studentWpe().id().fromAlias("excl")))
                        .where(eq(property(SessionAttestationSlot.bulletin().attestation().fromAlias("excl")), value(bulletin.getAttestation())))
                        .where(ne(property(SessionAttestationSlot.bulletin().fromAlias("excl")), value(bulletin)));

                dql.where(notIn(property(EppStudentWorkPlanElement.id().fromAlias(alias)), excludeBuilder.buildQuery()));
                dql.where(eq(property(EppStudentWorkPlanElement.student().educationOrgUnit().groupOrgUnit().fromAlias(alias)), value(bulletin.getOrgUnit())));
                dql.where(eq(property(EppStudentWorkPlanElement.year().educationYear().fromAlias(alias)), value(bulletin.getAttestation().getSessionObject().getEducationYear())));
                dql.where(eq(property(EppStudentWorkPlanElement.registryElementPart().fromAlias(alias)), value(bulletin.getRegistryElementPart())));
                dql.where(eq(property(EppStudentWorkPlanElement.student().archival().fromAlias(alias)), value(Boolean.FALSE)));
                dql.where(eq(property(EppStudentWorkPlanElement.student().status().active().fromAlias(alias)), value(Boolean.TRUE)));
                dql.where(isNull(property(EppStudentWorkPlanElement.removalDate().fromAlias(alias))));
            }
        };


        handler.filter(EppStudentWorkPlanElement.student().person().identityCard().fullFio());
        handler.order(EppStudentWorkPlanElement.student().person().identityCard().fullFio());

        return handler;
    }
}
