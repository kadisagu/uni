package ru.tandemservice.unisession.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Шкала оценок текущего контроля"
 * Имя сущности : sessionCurrentMarkScale
 * Файл data.xml : session.data.xml
 */
public interface SessionCurrentMarkScaleCodes
{
    /** Константа кода (code) элемента : Атт./неатт. (title) */
    String MARK_STATE = "scaleState";
    /** Константа кода (code) элемента : Четырехбалльная (title) */
    String MARK_GRADE = "scaleGrade";

    Set<String> CODES = ImmutableSet.of(MARK_STATE, MARK_GRADE);
}
