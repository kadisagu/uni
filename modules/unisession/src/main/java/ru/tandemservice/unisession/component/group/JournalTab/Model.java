package ru.tandemservice.unisession.component.group.JournalTab;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils.Pair;
import org.tandemframework.core.common.IIdentifiable;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.DisplayMode;
import org.tandemframework.core.view.formatter.NewLineFormatter;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.ui.SelectModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.catalog.codes.EppRegistryStructureCodes;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.util.selectModel.SessionTermModel;

import java.util.*;

@Input( { @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "groupHolder.id") })
@SuppressWarnings("unused")
public class Model
{
    /** хранилище всяких временных данных для ячейки */
    public static class Slot implements IIdentifiable
    {
        @Override public Long getId() { return this.getWpCASlot().getId(); }

        public Slot(final EppStudentWpeCAction wpCASlot) {
            this.wpCASlot = wpCASlot;
        }

        private final EppStudentWpeCAction wpCASlot;
        public EppStudentWpeCAction getWpCASlot() { return this.wpCASlot; }

        private SessionMarkCatalogItem totalMarkValue;
        public SessionMarkCatalogItem getTotalMarkValue() { return this.totalMarkValue; }
        public void setTotalMarkValue(final SessionMarkCatalogItem totalMarkValue) { this.totalMarkValue = totalMarkValue; }

        private SelectModel<SessionMarkCatalogItem> journalMarkSelectModel;
        public SelectModel<SessionMarkCatalogItem> getJournalMarkSelectModel() { return this.journalMarkSelectModel; }
        public void setJournalMarkSelectModel(final SelectModel<SessionMarkCatalogItem> journalMarkSelectModel) { this.journalMarkSelectModel = journalMarkSelectModel; }

    }

    /** собственно, сам журнал */
    public static class Journal
    {
        private boolean educationLevelHighSchoolVisible;

        //порядок сортировки сгруппированных коллекций ФИК
        private static Map<String, Integer> ORDER_MAP = ImmutableMap.of(
                EppRegistryStructureCodes.REGISTRY_DISCIPLINE, 1,
                EppRegistryStructureCodes.REGISTRY_PRACTICE, 2,
                EppRegistryStructureCodes.REGISTRY_ATTESTATION, 3
        );

        public Journal(final SessionTermModel.TermWrapper term) {
            this.year = term.getYear();
            this.part = term.getPart();
        }

        static Comparator<EppRegistryStructure> customEppRegistryStructureComparator = Comparator.nullsLast(Comparator.comparingInt(o -> ORDER_MAP.get(o.getCode())));

        private final EducationYear year;
        public EducationYear getYear() { return this.year; }

        private final YearDistributionPart part;
        public YearDistributionPart getPart() { return this.part; }

        private final List<Student> students = Lists.newArrayList();
        public List<Student> getStudents() { return students; }

        private Student currentStudent;
        public Student getCurrentStudent() { return currentStudent; }
        public void setCurrentStudent(Student currentStudent) { this.currentStudent = currentStudent; }

        private Integer currentIndex;
        public Integer getCurrentIndex() { return currentIndex; }
        public void setCurrentIndex(Integer currentIndex) { this.currentIndex = currentIndex; }

        private Map<EppRegistryStructure, Map<EppFControlActionType, Set<EppRegistryElementPart>>> registryActionMap = Maps.newTreeMap(customEppRegistryStructureComparator);
        public Map<EppRegistryStructure, Map<EppFControlActionType, Set<EppRegistryElementPart>>> getRegistryActionMap() { return registryActionMap; }

        private EppRegistryStructure currentRegistryStructure;
        public EppRegistryStructure getCurrentRegistryStructure(){
            return currentRegistryStructure;
        }

        public void setCurrentRegistryStructure(EppRegistryStructure currentRegistryStructure)
        {
            this.currentRegistryStructure = currentRegistryStructure;
        }

        public Set<EppFControlActionType> getCurrentActionTypeByRegistryStructure() {
            return registryActionMap.get(currentRegistryStructure).keySet();
        }

        private EppFControlActionType currentActionType;
        public EppFControlActionType getCurrentActionType() { return currentActionType; }

        public int getCurrentActionTypeColspan()
        {
            return registryActionMap.get(currentRegistryStructure).get(currentActionType).size();
        }

        public Set<EppRegistryElementPart> getCurrentElements()
        {
            return registryActionMap.get(currentRegistryStructure).get(currentActionType);
        }

        private EppRegistryElementPart currentElement;
        public EppRegistryElementPart getCurrentElement() { return currentElement; }
        public void setCurrentElement(EppRegistryElementPart currentElement) { this.currentElement = currentElement; }


        public void setCurrentActionType(EppFControlActionType currentActionType)
        {
            this.currentActionType = currentActionType;
        }

        private Pair<Long, Long> currentEditPair;
        public Pair<Long, Long> getCurrentEditPair() { return this.currentEditPair; }
        public void setCurrentEditPair(final Pair<Long, Long> currentEditPair) { this.currentEditPair = currentEditPair; }

        /* { { element.id, type.id } -> { student2epv.id -> slot } } */
        private final Map<Pair<Long, Long>, Map<Long, Slot>> slotMap = new LinkedHashMap<>();
        public Map<Pair<Long, Long>, Map<Long, Slot>> getSlotMap() { return this.slotMap; }

        public String getCurrentElementTitle()
        {
            final StringBuilder sb = new StringBuilder();
            String shortTitle = getCurrentElement().getShortTitle();
            while (shortTitle.length() > 0) {
                int l = shortTitle.indexOf(' ', 12);
                if (l < 0) { l = shortTitle.length(); }

                sb.append(shortTitle.substring(0, l)).append('\n');
                shortTitle = StringUtils.trimToEmpty(shortTitle.substring(l));
            }
            return sb.toString();
        }

        public void setEducationLevelHighSchoolVisible(boolean educationLevelHighSchoolVisible)
        {
            this.educationLevelHighSchoolVisible = educationLevelHighSchoolVisible;
        }
    }

    private final EntityHolder<Group> groupHolder = new EntityHolder<>();
    public EntityHolder<Group> getGroupHolder() { return this.groupHolder; }
    public Group getGroup() { return this.getGroupHolder().getValue(); }

    private SessionTermModel.TermWrapper term;
    public SessionTermModel.TermWrapper getTerm() { return this.term; }
    public void setTerm(final SessionTermModel.TermWrapper term) { this.term = term; }

    private ISelectModel termModel;
    public ISelectModel getTermModel() { return this.termModel; }
    public void setTermModel(final ISelectModel termModel) { this.termModel = termModel; }

    private Journal journal;
    public Journal getJournal() { return this.journal; }
    public void setJournal(final Journal journal) { this.journal = journal; }

    private boolean _withoutMSRPFK;
    public boolean isWithoutMSRPFK() { return _withoutMSRPFK; }
    public void setWithoutMSRPFK(boolean withoutMSRPFK) { _withoutMSRPFK = withoutMSRPFK;}

    private DisplayMode _mode;
    public DisplayMode getMode() { return _mode; }
    public void setMode(DisplayMode mode) { _mode = mode; }

    private Long _yearId;
    public Long getYearId() { return _yearId; }
    public void setYearId(Long yearId) { _yearId = yearId; }

    private Long _partId;
    public Long getPartId() { return _partId; }
    public void setPartId(Long partId) { _partId = partId; }

    public boolean isEditMode() { return DisplayMode.edit.equals(_mode); }
    public boolean isViewMode() { return DisplayMode.view.equals(_mode); }

    public boolean isShowCurrentTotalMark()
    {
        return getCurrentSlot() != null && getCurrentSlot().getTotalMarkValue() != null;
    }

    public boolean isShowDash(){
        //потому что слот создается только если есть соответсвующий МСРП-ФК
        return getCurrentSlot() == null;
    }

    public boolean isEducationLevelHighSchoolVisible()
    {
        return getJournal().educationLevelHighSchoolVisible;
    }

    public boolean isCurrentTotalNegativeMark(){
        return !getCurrentSlot().getTotalMarkValue().isCachedPositiveStatus();
    }

    public String getTableTitle()
    {
        return "Итоговые оценки студентов за " + getTerm().getYear().getTitle() + ", " + getTerm().getPart().getTitle();
    }

    public Slot getCurrentSlot()
    {
        final Journal j = this.getJournal();
        if (null == j) { return null; }

        try {
            Student student = getJournal().getCurrentStudent();
            Pair<Long, Long> pair = new Pair<>(journal.getCurrentElement().getId(), journal.getCurrentActionType().getEppGroupType().getId());
            return j.getSlotMap().get(pair).get(student.getId());
        } catch (final NullPointerException e) {
            return null;
        }
    }

    public boolean isJournalEditDisabled()
    {
        return !isViewMode() || getJournal() == null;
    }

    public Map getPubParameters()
    {
        return new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, journal.currentStudent.getId())
                .add(ru.tandemservice.uni.component.student.StudentPub.Model.SELECTED_TAB_BIND_KEY, "studentNewSessionTab");
    }

    public String getColumnNameBookNumber()
    {
        return NewLineFormatter.SIMPLE.format("Номер\nзачетной\nкнижки");
    }

    public String getColumnNameCompensationType()
    {
        return NewLineFormatter.SIMPLE.format("Вид\nвозмещения\nзатрат");
    }

    public String getProgramSpecializationTitle()
    {
        EduProgramSpecialization specialization = getJournal().getCurrentStudent().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getEduProgramSpecialization();
        return specialization != null ? specialization.getShortTitle() : StringUtils.EMPTY;
    }
}
