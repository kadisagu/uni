/* $Id:$ */
package ru.tandemservice.unisession.sec;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.dao.CommonDAO;
import ru.tandemservice.unisession.attestation.entity.SessionAttestation;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin;
import ru.tandemservice.unisession.entity.document.*;

import java.util.Collection;
import java.util.Collections;

/**
 * @author oleyba
 * @since 11/26/14
 */
public class SessionSecDao extends CommonDAO implements ISessionSecDao
{
    @Override
    public Collection<IEntity> getSecLocalEntities(SessionAttestation attestation)
    {
        return attestation.getSessionObject().getSecLocalEntities();
    }

    @Override
    public Collection<IEntity> getSecLocalEntities(SessionAttestationBulletin bulletin)
    {
        return bulletin.getAttestation().getSecLocalEntities();
    }

    @Override
    public Collection<IEntity> getSecLocalEntities(SessionBulletinDocument bulletin)
    {
        return Collections.singletonList((IEntity) bulletin.getSessionObject().getOrgUnit());
    }

    @Override
    public Collection<IEntity> getSecLocalEntities(SessionGlobalDocument globalDocument)
    {
        return Collections.emptySet();
    }

    @Override
    public Collection<IEntity> getSecLocalEntities(SessionListDocument listDocument)
    {
        return Collections.singletonList((IEntity) listDocument.getOrgUnit());
    }

    @Override
    public Collection<IEntity> getSecLocalEntities(SessionObject sessionObject)
    {
        return Collections.singletonList((IEntity) sessionObject.getOrgUnit());
    }

    @Override
    public Collection<IEntity> getSecLocalEntities(SessionRetakeDocument retakeDocument)
    {
        return Collections.singletonList((IEntity) retakeDocument.getSessionObject().getOrgUnit());
    }

    @Override
    public Collection<IEntity> getSecLocalEntities(SessionSheetDocument sheetDocument)
    {
        return Collections.singletonList((IEntity) sheetDocument.getOrgUnit());
    }

    @Override
    public Collection<IEntity> getSecLocalEntities(SessionStudentGradeBookDocument gradeBookDocument)
    {
        return Collections.<IEntity>singleton(gradeBookDocument.getStudent().getEducationOrgUnit().getGroupOrgUnit());
    }

    @Override
    public Collection<IEntity> getSecLocalEntities(SessionTransferDocument transferDocument)
    {
        return Collections.singletonList((IEntity) transferDocument.getOrgUnit());
    }

    @Override
    public Collection<IEntity> getSecLocalEntities(SessionTransferProtocolDocument protocol)
    {
        return Collections.singletonList((IEntity) protocol.getRequest().getStudent().getEducationOrgUnit().getGroupOrgUnit());
    }
}
