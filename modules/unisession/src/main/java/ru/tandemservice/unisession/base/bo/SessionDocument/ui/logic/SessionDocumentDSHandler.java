/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionDocument.ui.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Alexey Lopatin
 * @since 12.05.2015
 */
public class SessionDocumentDSHandler extends SimpleTitledComboDataSourceHandler
{
    public static final String BULLETIN_LIST_TAB = "bulletinListTab";
    public static final String RETAKE_DOC_LIST_TAB = "retakeDocListTab";
    public static final String SHEET_LIST_TAB = "sheetListTab";
    public static final String LIST_DOCUMENT_TAB = "listDocumentTab";
    public static final String TRANSFER_DOC_LIST_TAB = "transferDocListTab";
    public static final String SIMPLEDOC_LIST_TAB = "simpledocListTab";

    public SessionDocumentDSHandler(String ownerId)
    {
        super(ownerId);
    }

    public Map<String, SessionDocumentWrapper> getDocumentMap()
    {
        Map<String, SessionDocumentWrapper> documentMap = Maps.newLinkedHashMap();

        documentMap.put(BULLETIN_LIST_TAB, new SessionDocumentWrapper(BULLETIN_LIST_TAB, "Ведомости", ru.tandemservice.unisession.component.orgunit.SessionBulletinListTab.Model.class.getPackage().getName(), "sessionBulletinTabView"));
        documentMap.put(RETAKE_DOC_LIST_TAB, new SessionDocumentWrapper(RETAKE_DOC_LIST_TAB, "Ведомости пересдач", ru.tandemservice.unisession.component.orgunit.SessionRetakeDocTab.Model.class.getPackage().getName(), "sessionRetakeDocTabView"));
        documentMap.put(SHEET_LIST_TAB, new SessionDocumentWrapper(SHEET_LIST_TAB, "Экзам. листы", ru.tandemservice.unisession.component.orgunit.SessionSheetListTab.Model.class.getPackage().getName(), "sessionSheetTabView"));
        documentMap.put(LIST_DOCUMENT_TAB, new SessionDocumentWrapper(LIST_DOCUMENT_TAB, "Экзам. карточки", ru.tandemservice.unisession.component.orgunit.SessionListDocumentTab.Model.class.getPackage().getName(), "sessionListDocumentTabView"));
        documentMap.put(TRANSFER_DOC_LIST_TAB, new SessionDocumentWrapper(TRANSFER_DOC_LIST_TAB, "Перезачтения", ru.tandemservice.unisession.component.orgunit.SessionTransferDocListTab.Model.class.getPackage().getName(), "sessionTransferDocTabView"));
        documentMap.put(SIMPLEDOC_LIST_TAB, new SessionDocumentWrapper(SIMPLEDOC_LIST_TAB, "Все документы", ru.tandemservice.unisession.component.orgunit.SessionSimpleDocListTab.Model.class.getPackage().getName(), "sessionSimpleDocTabView"));

        return documentMap;
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Set keySet = input.getPrimaryKeys();
        Collection<SessionDocumentWrapper> documents = getDocumentMap().values();
        if (CollectionUtils.isNotEmpty(keySet))
        {
            List<SessionDocumentWrapper> resultList = Lists.newArrayList();
            for (SessionDocumentWrapper wrapper : documents)
            {
                if(keySet.contains(wrapper.getId()))
                    resultList.add(wrapper);
            }
            return ListOutputBuilder.get(input, resultList).build();
        }
        return ListOutputBuilder.get(input, documents).build();
    }

    public static class SessionDocumentWrapper extends DataWrapper
    {
        private String _code;
        private String _componentName;
        private String _permissionKey;

        public SessionDocumentWrapper(String code, String title, String componentName, String permissionKey)
        {
            super(code.hashCode(), title);
            _code = code;
            _componentName = componentName;
            _permissionKey = permissionKey;
        }

        public String getCode()
        {
            return _code;
        }

        public String getComponentName()
        {
            return _componentName;
        }

        public String getPermissionKey()
        {
            return _permissionKey;
        }

        public void setPermissionKey(String permissionKey)
        {
            _permissionKey = permissionKey;
        }
    }
}
