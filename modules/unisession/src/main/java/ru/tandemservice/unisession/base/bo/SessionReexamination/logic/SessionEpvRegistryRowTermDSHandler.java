/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReexamination.logic;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvBlockWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartWrapper;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElWrapper;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionRootBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unisession.base.bo.SessionReexamination.ui.AddEdit.SessionReexaminationAddEditUI;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 27.08.2015
 */
public class SessionEpvRegistryRowTermDSHandler extends DefaultComboDataSourceHandler
{
    public static final String ROW_TERM_PART = "part";

    private EppEduPlanVersionBlock _block;

    public SessionEpvRegistryRowTermDSHandler(String ownerId)
    {
        super(ownerId, EppEpvRowTerm.class);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        List<DataWrapper> resultList = Lists.newArrayList();
        _block = context.get(SessionReexaminationAddEditUI.PARAM_BLOCK);

        if (null == _block)
            return ListOutputBuilder.get(input, resultList).build();

        Set primaryKeys = input.getPrimaryKeys();
        String filter = input.getComboFilterByValue();

        DQLSelectBuilder builder = createBuilder("r", context);

        if (StringUtils.isNotEmpty(filter))
        {
            builder.where(or(
                    likeUpper(property("regElement", EppRegistryElement.title()), value(CoreStringUtils.escapeLike(filter, true))),
                    likeUpper(property("r", EppEpvRowTerm.term().title()), value(CoreStringUtils.escapeLike(filter, true)))));
        }
        if (null != primaryKeys && !primaryKeys.isEmpty())
        {
            builder.where(in(property("r.id"), primaryKeys));
        }

        List<EppEpvRowTerm> rowTerms = builder.createStatement(context.getSession()).list();
        List<Long> regElementIds = rowTerms.stream()
                                     .map(rowTerm -> ((EppEpvRegistryRow) rowTerm.getRow()).getRegistryElement().getId())
                                     .collect(Collectors.toList());

        IEppEpvBlockWrapper blockWrapper = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockData(_block.getId(), true);
        Map<Long, IEppRegElWrapper> regElementDataMap = IEppRegistryDAO.instance.get().getRegistryElementDataMap(regElementIds);

        for (EppEpvRowTerm rowTerm : rowTerms)
        {
            Term term = rowTerm.getTerm();
            EppEpvRegistryRow row = (EppEpvRegistryRow) rowTerm.getRow();

            IEppEpvRowWrapper rowWrapper = blockWrapper.getRowMap().get(row.getId());
            int termNumber = rowWrapper.getActiveTermNumber(term.getIntValue());

            DevelopGrid developGrid = row.getOwner().getEduPlanVersion().getDevelopGridTerm().getDevelopGrid();
            DevelopGridTerm gridTerm = DataAccessServices.dao().getByNaturalId(new DevelopGridTerm.NaturalId(developGrid, term));

            IEppRegElWrapper regElWrap = regElementDataMap.get(row.getRegistryElement().getId());
            IEppRegElPartWrapper partWrap = regElWrap.getPartMap().get(termNumber);
            if (null == partWrap) continue;

            EppRegistryElementPart part = partWrap.getItem();
            String partRegElementTitle = part.getTitleWithNumber();

            String title = row.getStoredIndex() + " " + partRegElementTitle + " " + term.getTitle() + " " + gridTerm.getPart().getTitle();

            DataWrapper wrapper = new DataWrapper(rowTerm.getId(), title, rowTerm);
            wrapper.setProperty(ROW_TERM_PART, part);
            resultList.add(wrapper);
        }

        return ListOutputBuilder.get(input, resultList).build();
    }

    public DQLSelectBuilder createBuilder(String alias, ExecutionContext context)
    {
        return new DQLSelectBuilder().fromEntity(EppEpvRowTerm.class, alias).column(property(alias))
                .joinEntity(alias, DQLJoinType.inner, EppEpvRegistryRow.class, "regRow", eq(property(alias, EppEpvRowTerm.row().id()), property("regRow.id")))
                .joinPath(DQLJoinType.inner, EppEpvRegistryRow.registryElement().fromAlias("regRow"), "regElement")
                .joinPath(DQLJoinType.inner, EppEpvRegistryRow.owner().fromAlias("regRow"), "block")
                .where(or(
                        eq(property("block"), value(_block)),
                        and(
                                eq(property("block", EppEduPlanVersionBlock.eduPlanVersion()), value(_block.getEduPlanVersion())),
                                instanceOf("block", EppEduPlanVersionRootBlock.class)
                        )
                ))
                .order(property(alias, EppEpvRowTerm.row().storedIndex()))
                .order(property(alias, EppEpvRowTerm.term().intValue()));
    }
}
