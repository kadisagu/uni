package ru.tandemservice.unisession.attestation.bo.AttestationBulletin.event;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlot;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlotAdditionalData;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * User: amakarova
 * Date: 21.11.12
 *
 */
public class AttBulletinSlotAddDataListener implements IDSetEventListener
{
    public void init()
    {
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, SessionAttestationSlot.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.beforeUpdate, SessionAttestationSlot.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.beforeDelete, SessionAttestationSlot.class, this);
    }

    @Override
    public void onEvent(final DSetEvent event)
    {
        final DQLSelectBuilder closedBull = new DQLSelectBuilder()
                .fromEntity(SessionAttestationSlotAdditionalData.class, "addslot")
                .where(in(property(SessionAttestationSlotAdditionalData.id().fromAlias("addslot")), event.getMultitude().getInExpression()))
                .where(isNotNull(property(SessionAttestationSlotAdditionalData.slot().bulletin().closeDate().fromAlias("addslot"))))
                .column(property(SessionAttestationSlotAdditionalData.slot().bulletin().id()));

        final Number countClosedBul = closedBull.createCountStatement(event.getContext()).uniqueResult();

        if (countClosedBul != null && countClosedBul.longValue() > 0) {
            throw new ApplicationException("Редактирование дополнительных данных записи студента в закрытой атт.ведомости невозможно.", true);
        }
    }
}
