/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReport;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.config.itemList.IItemListExtPointBuilder;
import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.*;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.tool.tree.IHierarchyItem;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.ui.OrgUnitUIPresenter;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppWorkPlanRowKind;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.FinalQualWorkResultAdd.logic.FinalQualWorkResultPrintDAO;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.FqwSummaryResultAdd.logic.FqwSummaryResultPrintDAO;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.ResultsAdd.ISessionReportResultsDAO;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.ResultsByDiscAdd.ISessionReportResultsByDiscDAO;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.SfaSummaryResultAdd.logic.ISfaSummaryResultPrintDAO;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.SfeSummaryResultAdd.SfeSummaryResultPrintDAO;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.StateFinalExamResultAdd.StateFinalExamResultPrintDAO;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.StateFinalAttestationResultAdd.logic.IStateFinalAttestationResultPrintDAO;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.SummaryBulletinAdd.ISessionSummaryBulletinPrintDAO;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.SummaryBulletinAdd.SessionSummaryBulletinPrintDAO;
import ru.tandemservice.unisession.base.bo.SessionReport.util.results.SessionReportResultsUtils;
import ru.tandemservice.unisession.base.bo.SessionReport.util.resultsByDisc.SessionReportResultsByDiscUtils;
import ru.tandemservice.unisession.entity.document.SessionObject;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.upper;

/**
 * @author oleyba
 * @since 12/16/11
 */
@Configuration
public class SessionReportManager extends BusinessObjectManager
{
    public static final String DS_EDU_YEAR = "eduYearDS";
    public static final String DS_YEAR_PART = "yearPartDS";
    public static final String DS_GROUP_ORG_UNIT = "groupOrgUnitDS";

    public static final String PARAM_ORG_UNIT = "orgUnit";
    public static final String PARAM_EDU_YEAR = "eduYear";
    public static final String PARAM_EDU_YEAR_ONLY_CURRENT = "eduYearOnlyCurrent";

    public static final String BIND_ORG_UNIT = OrgUnitUIPresenter.INPUT_PARAM_ORG_UNIT_ID;

    public static SessionReportManager instance()
    {
        return instance(SessionReportManager.class);
    }

    /** @deprecated use ISessionSummaryBulletinPrintDAO.instance.get(). Переопределять ISessionSummaryBulletinPrintDAO необходимо через spring.xml */
    @Deprecated
    @Bean
    public ISessionSummaryBulletinPrintDAO printDAO() {
        return new SessionSummaryBulletinPrintDAO();
    }

    @Bean
    public UIDataSourceConfig eduYearDSConfig()
    {
        return SelectDSConfig.with(DS_EDU_YEAR, this.getName())
        .dataSourceClass(SelectDataSource.class)
        .handler(this.eduYearDSHandler())
        .valueStyleSource(EducationCatalogsManager.getEduYearValueStyleSource())
        .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> eduYearDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), EducationYear.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                final Object currentOnly = context.get(PARAM_EDU_YEAR_ONLY_CURRENT);
                if (Boolean.TRUE.equals(currentOnly)) {
                    dql.where(eq(property(EducationYear.current().fromAlias(alias)), value(Boolean.TRUE)));
                    return;
                }

                final Object ou = context.get(PARAM_ORG_UNIT);
                if (null == ou)
                    return;

                final DQLSelectBuilder sessionObjectExists = new DQLSelectBuilder()
                .fromEntity(SessionObject.class, alias)
                .column(property(SessionObject.educationYear().id().fromAlias(alias)))
                .where(eq(property(SessionObject.orgUnit().fromAlias(alias)), commonValue(ou)));

                dql.where(in(property(EducationYear.id().fromAlias(alias)), sessionObjectExists.buildQuery()));
            }
        }
        .pageable(true)
        .order(EducationYear.intValue())
        .filter(EducationYear.title())
        ;
    }

    @Bean
    public UIDataSourceConfig yearPartDSConfig()
    {
        return SelectDSConfig.with(DS_YEAR_PART, this.getName())
        .dataSourceClass(SelectDataSource.class)
        .handler(this.yearPartDSHandler())
        .treeable(true)
        .valueStyleSource(EducationCatalogsManager.getYearPartValueStyleSource())
        .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> yearPartDSHandler()
    {
        return new AbstractReadAggregateHandler<DSInput, DSOutput>(getName())
        {
            @Override
            protected DSOutput execute(DSInput dsInput, ExecutionContext context)
            {
                if (!CollectionUtils.isEmpty(dsInput.getPrimaryKeys())) {
                    DQLSelectBuilder dql = new DQLSelectBuilder()
                    .fromEntity(YearDistributionPart.class, "p").column("p")
                    .where(DQLExpressions.in("p.id", dsInput.getPrimaryKeys()));

                    return ListOutputBuilder.get(dsInput, dql.createStatement(context.getSession()).list()).build();
                }

                final Object orgUnit = context.get(PARAM_ORG_UNIT);
                final String filter = dsInput.getComboFilterByValue();

                final DQLSelectBuilder partsDQL = new DQLSelectBuilder();

                if (null != orgUnit) {
                    partsDQL
                    .fromEntity(SessionObject.class, "obj")
                    .column(property(SessionObject.yearDistributionPart().fromAlias("obj")))
                    .where(eq(property(SessionObject.orgUnit().fromAlias("obj")), commonValue(orgUnit)))
                    .order(property(SessionObject.yearDistributionPart().number().fromAlias("obj")));

                    final Object eduYear = context.get(PARAM_EDU_YEAR);

                    if (null != eduYear)
                        partsDQL.where(eq(property(SessionObject.educationYear().fromAlias("obj")), commonValue(eduYear)));
                    else {
                        partsDQL.where(DQLExpressions.isNull("obj.id"));
                    }

                    if (filter != null)
                        partsDQL.where(like(upper(property(SessionObject.yearDistributionPart().title().fromAlias("obj"))), value(CoreStringUtils.escapeLike(filter, true))));
                }
                else {
                    partsDQL
                    .fromEntity(YearDistributionPart.class, "p").column("p")
                    .order(property(YearDistributionPart.yearDistribution().code().fromAlias("p")))
                    .order(property(YearDistributionPart.number().fromAlias("p")));

                    if (filter != null)
                        partsDQL.where(like(upper(property(YearDistributionPart.title().fromAlias("p"))), value(CoreStringUtils.escapeLike(filter, true))));
                }

                final Set<YearDistributionPart> parts = new LinkedHashSet<YearDistributionPart>(partsDQL.createStatement(context.getSession()).<YearDistributionPart>list());
                final List options = HierarchyUtil.listHierarchyItemsWithParents(new ArrayList<IHierarchyItem>(parts));

                return ListOutputBuilder.get(dsInput, options).build();
            }
        };
    }

    public static final long RESULT_OPTION_ID_IN_SESSION = 0L;
    public static final long RESULT_OPTION_ID_TOTAL = 1L;

    public DataWrapper getResultDefaultOption()
    {
        return new DataWrapper(RESULT_OPTION_ID_IN_SESSION, getPropertySafe("resultsDS.optionTitle.inSession"));
    }

    @Bean
    public IDefaultComboDataSourceHandler resultsDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
        .addRecord(RESULT_OPTION_ID_IN_SESSION, "resultsDS.optionTitle.inSession")
        .addRecord(RESULT_OPTION_ID_TOTAL, "resultsDS.optionTitle.total");
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> discKindDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), EppWorkPlanRowKind.class);
    }

    /**
     * точка расширения для вариантов группировки данных в отчете «Сведения об успеваемости студентов (по курсам, по группам, по направлениям)»
     * подробнее о том, как добавить свой вариант, см. в ISessionReportResultsDAO
     * @return список вариантов группировки
     */
    @Bean
    public ItemListExtPoint<ISessionReportResultsDAO.ISessionReportResultsGrouping> sessionReportResultsGroupingExtPoint()
    {
        final IItemListExtPointBuilder<ISessionReportResultsDAO.ISessionReportResultsGrouping> list = itemList(ISessionReportResultsDAO.ISessionReportResultsGrouping.class);
        for (ISessionReportResultsDAO.ISessionReportResultsGrouping gr : SessionReportResultsUtils.getDefaultGroupingList())
            list.add(gr.getKey(), gr);

        return list.create();
    }

    /**
     * точка расширения для вариантов группировки данных в отчете «Сведения об успеваемости студентов (по дисциплинам)»
     * подробнее о том, как добавить свой вариант, см. в ISessionReportResultsByDiscDAO
     * @return список вариантов группировки
     */
    @Bean
    public ItemListExtPoint<ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscGrouping> sessionReportResultsByDiscGroupingExtPoint()
    {
        final IItemListExtPointBuilder<ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscGrouping> list = itemList(ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscGrouping.class);
        for (ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscGrouping gr : SessionReportResultsByDiscUtils.getDefaultGroupingList())
            list.add(gr.getKey(), gr);

        return list.create();
    }

    @Bean
    public UIDataSourceConfig groupOrgUnitDSConfig()
    {
        return SelectDSConfig.with(DS_GROUP_ORG_UNIT, this.getName())
        .dataSourceClass(SelectDataSource.class)
        .handler(this.groupOrgUnitDSHandler())
        .addColumn(OrgUnit.fullTitle().s())
        .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> groupOrgUnitDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), OrgUnit.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                dql.where(in(alias, new DQLSelectBuilder()
                .fromEntity(EducationOrgUnit.class, "eduOu")
                .column(property(EducationOrgUnit.groupOrgUnit().fromAlias("eduOu")))
                .buildQuery()));
            }
        }
        .order(OrgUnit.fullTitle())
        .filter(OrgUnit.fullTitle())
        ;
    }

    public static void addOuLeaderData(RtfInjectModifier modifier, OrgUnit ou, String leaderPostKey, String leaderFioKey)
    {
        if (ou != null)
        {
            String ouLeader;
            if (ou.getOrgUnitType().getCode().equals(OrgUnitTypeCodes.INSTITUTE) || ou.getOrgUnitType().getCode().equals(OrgUnitTypeCodes.BRANCH) || ou.getOrgUnitType().getCode().equals(OrgUnitTypeCodes.DEPARTMENT))
                ouLeader = "Директор ";
            else
                ouLeader = "Декан факультета ";
            ouLeader = ouLeader + ou.getShortTitle();
            modifier.put(leaderPostKey, ouLeader);

            EmployeePost head = EmployeeManager.instance().dao().getHead(ou);
            modifier.put(leaderFioKey, head == null ? "" : head.getPerson().getFio());
        }
        else
        {
            modifier.put(leaderPostKey, "");
            modifier.put(leaderFioKey, "");
        }
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> eppFControlActionTypeDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), EppFControlActionType.class)
                .order(EppFControlActionType.title())
                .filter(EppFControlActionType.title());
    }

    @Bean
    public IStateFinalAttestationResultPrintDAO stateFinalExamResultPrintDAO()
    {
        return new StateFinalExamResultPrintDAO();
    }

    @Bean
    public IStateFinalAttestationResultPrintDAO finalQualWorkResultPrintDAO()
    {
        return new FinalQualWorkResultPrintDAO();
    }

    @Bean
    public ISfaSummaryResultPrintDAO sfeSummaryResultPrintDAO()
    {
        return new SfeSummaryResultPrintDAO();
    }

    @Bean
    public ISfaSummaryResultPrintDAO fqwSummaryResultPrintDAO()
    {
        return new FqwSummaryResultPrintDAO();
    }
}