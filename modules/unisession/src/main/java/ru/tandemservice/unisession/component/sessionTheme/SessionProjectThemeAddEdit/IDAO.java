/* $Id$ */
package ru.tandemservice.unisession.component.sessionTheme.SessionProjectThemeAddEdit;

import ru.tandemservice.uni.dao.IPrepareable;

/**
 * @author oleyba
 * @since 6/24/11
 */
public interface IDAO extends IPrepareable<Model>
{
    void update(Model model);
}
