/* $Id:$ */
package ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.BulletinAdd;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unisession.attestation.entity.SessionAttestation;

import java.util.List;

/**
 * @author oleyba
 * @since 11/8/12
 */
public interface IAttestationReportBulletinPrintDAO extends INeedPersistenceSupport
{
    Long createStoredReport(AttestationReportBulletinAddUI model);
}
