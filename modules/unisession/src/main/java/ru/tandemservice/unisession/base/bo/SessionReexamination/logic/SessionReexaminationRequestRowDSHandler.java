/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReexamination.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unisession.base.bo.SessionReexamination.SessionReexaminationManager;
import ru.tandemservice.unisession.base.bo.SessionReexamination.ui.Pub.SessionReexaminationPubUI;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.mark.SessionALRequest;
import ru.tandemservice.unisession.entity.mark.SessionALRequestRow;
import ru.tandemservice.unisession.entity.mark.SessionALRequestRowMark;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Alexey Lopatin
 * @since 26.08.2015
 */
public class SessionReexaminationRequestRowDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String PROP_RE_EXAM_AND_ATTESTATION = "reExamAndAttestation";
    public static final String PROP_RE_EXAM_MARK_MAP = "reExamMarkMap";
    public static final String PROP_PART_TO_FCA_MAP = "hasPart2FCAMap";

    public SessionReexaminationRequestRowDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        SessionALRequest request = context.get(SessionReexaminationPubUI.PARAM_REQUEST);

        List<DataWrapper> resultList = Lists.newArrayList();
        Map<SessionALRequestRow, Map<EppFControlActionType, SessionMarkGradeValueCatalogItem>> requestRowMarkMap = Maps.newHashMap();

        List<SessionALRequestRow> requestRows = DataAccessServices.dao().getList(SessionALRequestRow.class, SessionALRequestRow.request().id(), request.getId());

        ISessionReexaminationDao dao = SessionReexaminationManager.instance().dao();
        dao.sortALRequestRows(requestRows);
        List<EppFControlActionType> usedFcaTypes = dao.getUsedFcaTypes(request.getBlock(), requestRows);
        Map<EppRegistryElementPart, List<EppFControlActionType>> part2fcaMap = dao.getPart2FCATypeMap(requestRows);

        List<SessionALRequestRowMark> requestRowMarks = DataAccessServices.dao().getList(SessionALRequestRowMark.class, SessionALRequestRowMark.requestRow(), requestRows);
        for (SessionALRequestRowMark requestRowMark : requestRowMarks)
        {
            SafeMap.safeGet(requestRowMarkMap, requestRowMark.getRequestRow(), HashMap.class).put(requestRowMark.getControlAction(), requestRowMark.getMark());
        }

        Map<String, DataWrapper> itemMap = SessionReexaminationManager.instance().reexaminationDSExtPoint().getItems();
        for (SessionALRequestRow requestRow : requestRows)
        {
            EppEpvRowTerm rowTerm = requestRow.getRowTerm();
            EppRegistryElementPart part = requestRow.getRegElementPart();

            EppEpvTermDistributedRow row = rowTerm.getRow();
            Term term = rowTerm.getTerm();
            DevelopGrid developGrid = row.getOwner().getEduPlanVersion().getDevelopGridTerm().getDevelopGrid();
            DevelopGridTerm gridTerm = DataAccessServices.dao().getByNaturalId(new DevelopGridTerm.NaturalId(developGrid, term));

            String title = part.getTitleWithNumber() + ", " + term.getTitle() + " " + gridTerm.getPart().getTitle();

            Map<String, Boolean> hasPart2FCAMap = Maps.newHashMap();
            Map<String, SessionMarkGradeValueCatalogItem> reExamMarkMap = Maps.newHashMap();

            List<EppFControlActionType> fcaList = part2fcaMap.get(part);
            for (EppFControlActionType fcaType : usedFcaTypes)
            {
                String code = fcaType.getFullCode();
                hasPart2FCAMap.put(code, null != fcaList && fcaList.contains(fcaType));

                if (!requestRow.isNeedRetake())
                {
                    Map<EppFControlActionType, SessionMarkGradeValueCatalogItem> fcaMap = requestRowMarkMap.get(requestRow);
                    if (null != fcaMap)
                        reExamMarkMap.put(code, fcaMap.get(fcaType));
                }
            }

            DataWrapper wrapper = new DataWrapper(requestRow.getId(), title, requestRow);
            wrapper.setProperty(PROP_RE_EXAM_AND_ATTESTATION, itemMap.get(String.valueOf(requestRow.isNeedRetake())).getTitle());
            wrapper.setProperty(PROP_RE_EXAM_MARK_MAP, reExamMarkMap);
            wrapper.setProperty(PROP_PART_TO_FCA_MAP, hasPart2FCAMap);

            resultList.add(wrapper);
        }

        return ListOutputBuilder.get(input, resultList).build();
    }
}