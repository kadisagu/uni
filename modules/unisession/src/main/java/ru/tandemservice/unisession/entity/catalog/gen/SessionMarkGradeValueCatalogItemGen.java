package ru.tandemservice.unisession.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Оценка (из шкалы оценок)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionMarkGradeValueCatalogItemGen extends SessionMarkCatalogItem
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem";
    public static final String ENTITY_NAME = "sessionMarkGradeValueCatalogItem";
    public static final int VERSION_HASH = 1019240356;
    private static IEntityMeta ENTITY_META;

    public static final String L_SCALE = "scale";
    public static final String P_PRIORITY = "priority";
    public static final String P_POSITIVE = "positive";

    private EppGradeScale _scale;     // Шкала оценок
    private int _priority;     // Приоритет
    private boolean _positive;     // Положительная

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Шкала оценок. Свойство не может быть null.
     */
    @NotNull
    public EppGradeScale getScale()
    {
        return _scale;
    }

    /**
     * @param scale Шкала оценок. Свойство не может быть null.
     */
    public void setScale(EppGradeScale scale)
    {
        dirty(_scale, scale);
        _scale = scale;
    }

    /**
     * @return Приоритет. Свойство не может быть null.
     */
    @NotNull
    public int getPriority()
    {
        return _priority;
    }

    /**
     * @param priority Приоритет. Свойство не может быть null.
     */
    public void setPriority(int priority)
    {
        dirty(_priority, priority);
        _priority = priority;
    }

    /**
     * @return Положительная. Свойство не может быть null.
     */
    @NotNull
    public boolean isPositive()
    {
        return _positive;
    }

    /**
     * @param positive Положительная. Свойство не может быть null.
     */
    public void setPositive(boolean positive)
    {
        dirty(_positive, positive);
        _positive = positive;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof SessionMarkGradeValueCatalogItemGen)
        {
            setScale(((SessionMarkGradeValueCatalogItem)another).getScale());
            setPriority(((SessionMarkGradeValueCatalogItem)another).getPriority());
            setPositive(((SessionMarkGradeValueCatalogItem)another).isPositive());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionMarkGradeValueCatalogItemGen> extends SessionMarkCatalogItem.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionMarkGradeValueCatalogItem.class;
        }

        public T newInstance()
        {
            return (T) new SessionMarkGradeValueCatalogItem();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "scale":
                    return obj.getScale();
                case "priority":
                    return obj.getPriority();
                case "positive":
                    return obj.isPositive();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "scale":
                    obj.setScale((EppGradeScale) value);
                    return;
                case "priority":
                    obj.setPriority((Integer) value);
                    return;
                case "positive":
                    obj.setPositive((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "scale":
                        return true;
                case "priority":
                        return true;
                case "positive":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "scale":
                    return true;
                case "priority":
                    return true;
                case "positive":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "scale":
                    return EppGradeScale.class;
                case "priority":
                    return Integer.class;
                case "positive":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionMarkGradeValueCatalogItem> _dslPath = new Path<SessionMarkGradeValueCatalogItem>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionMarkGradeValueCatalogItem");
    }
            

    /**
     * @return Шкала оценок. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem#getScale()
     */
    public static EppGradeScale.Path<EppGradeScale> scale()
    {
        return _dslPath.scale();
    }

    /**
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem#getPriority()
     */
    public static PropertyPath<Integer> priority()
    {
        return _dslPath.priority();
    }

    /**
     * @return Положительная. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem#isPositive()
     */
    public static PropertyPath<Boolean> positive()
    {
        return _dslPath.positive();
    }

    public static class Path<E extends SessionMarkGradeValueCatalogItem> extends SessionMarkCatalogItem.Path<E>
    {
        private EppGradeScale.Path<EppGradeScale> _scale;
        private PropertyPath<Integer> _priority;
        private PropertyPath<Boolean> _positive;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Шкала оценок. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem#getScale()
     */
        public EppGradeScale.Path<EppGradeScale> scale()
        {
            if(_scale == null )
                _scale = new EppGradeScale.Path<EppGradeScale>(L_SCALE, this);
            return _scale;
        }

    /**
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem#getPriority()
     */
        public PropertyPath<Integer> priority()
        {
            if(_priority == null )
                _priority = new PropertyPath<Integer>(SessionMarkGradeValueCatalogItemGen.P_PRIORITY, this);
            return _priority;
        }

    /**
     * @return Положительная. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem#isPositive()
     */
        public PropertyPath<Boolean> positive()
        {
            if(_positive == null )
                _positive = new PropertyPath<Boolean>(SessionMarkGradeValueCatalogItemGen.P_POSITIVE, this);
            return _positive;
        }

        public Class getEntityClass()
        {
            return SessionMarkGradeValueCatalogItem.class;
        }

        public String getEntityName()
        {
            return "sessionMarkGradeValueCatalogItem";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
