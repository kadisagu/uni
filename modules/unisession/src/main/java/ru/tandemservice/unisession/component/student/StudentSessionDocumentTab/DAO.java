/* $Id$ */
package ru.tandemservice.unisession.component.student.StudentSessionDocumentTab;

import com.google.common.collect.Maps;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.entity.EntityCodes;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unisession.entity.document.*;
import ru.tandemservice.unisession.util.selectModel.SessionTermModel;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 4/13/11
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final EntityCodes<SessionObject> SESSION_OBJECT_ENTITY_CODES = new EntityCodes<>(SessionObject.class);
    private static final EntityCodes<SessionStudentGradeBookDocument> STG_ENTITY_CODES = new EntityCodes<>(SessionStudentGradeBookDocument.class);

    @Override
    public void prepare(final Model model)
    {
        final Student student = this.get(Student.class, model.getStudent().getId());
        model.setStudent(student);
        model.setTermModel(SessionTermModel.createForStudent(student, false));
    }

    @Override
    public void prepareListDataSource(final Model model)
    {
        final DynamicListDataSource<SessionDocument> dataSource = model.getDataSource();

        final SessionTermModel.TermWrapper term = model.getSettings().get("term");

        final List<Short> entityCodes = new ArrayList<>();
            entityCodes.addAll(DAO.SESSION_OBJECT_ENTITY_CODES.get());
            entityCodes.addAll(DAO.STG_ENTITY_CODES.get());
        final DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(SessionDocumentSlot.class, "slot")
            .column("slot")
            .fetchPath(DQLJoinType.inner, SessionDocumentSlot.document().fromAlias("slot").s(), "doc")
            .where(eq(property(SessionDocumentSlot.actualStudent().id().fromAlias("slot")), value(model.getStudent().getId())))
            .where(notIn(property("slot", SessionDocumentSlot.document().clazz()), entityCodes));

        if (null != term)
        {
            dql.where(eq(property(SessionDocumentSlot.studentWpeCAction().studentWpe().year().educationYear().fromAlias("slot")), value(term.getYear())));
            dql.where(eq(property(SessionDocumentSlot.studentWpeCAction().studentWpe().part().fromAlias("slot")), value(term.getPart())));
        }

        new DQLOrderDescriptionRegistry(SessionDocument.class, "doc").applyOrder(dql, dataSource.getEntityOrder());
        dql.order(property("slot", SessionDocumentSlot.document().id()));
        dql.order(property("slot", SessionDocumentSlot.studentWpeCAction().studentWpe().term().intValue()));
        dql.order(property("slot", SessionDocumentSlot.studentWpeCAction().type().title()));
        dql.order(property("slot", SessionDocumentSlot.studentWpeCAction().studentWpe().registryElementPart().registryElement().title()));

        Comparator<SessionDocument> docComparator = (e1, e2) -> e1.getId().compareTo(e2.getId());
        final Map<SessionDocument, List<SessionDocumentSlot>> docMap = Maps.newTreeMap(docComparator);
        for (final SessionDocumentSlot slot : dql.createStatement(this.getSession()).<SessionDocumentSlot>list()) {
            SafeMap.safeGet(docMap, slot.getDocument(), ArrayList.class).add(slot);
        }

        List<SessionTransferProtocolDocument> protocols = getList(SessionTransferProtocolDocument.class, SessionTransferProtocolDocument.request().student().id(), model.getStudent().getId());
        for (SessionTransferProtocolDocument protocol : protocols)
            SafeMap.safeGet(docMap, protocol, ArrayList.class);

        UniBaseUtils.createPage(dataSource, new ArrayList<>(docMap.keySet()));

        for (final ViewWrapper<SessionDocument> wrapper : ViewWrapper.<SessionDocument>getPatchedList(dataSource)) {
            wrapper.setViewProperty("slotList", docMap.get(wrapper.getEntity()));
        }
    }
}
