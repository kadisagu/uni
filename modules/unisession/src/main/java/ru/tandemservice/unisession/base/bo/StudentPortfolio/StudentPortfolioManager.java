package ru.tandemservice.unisession.base.bo.StudentPortfolio;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unisession.base.bo.StudentPortfolio.logic.IStudentPortfolioDAO;
import ru.tandemservice.unisession.base.bo.StudentPortfolio.logic.StudentPortfolioDAO;

/**
 * @author avedernikov
 * @since 26.11.2015
 */

@Configuration
public class StudentPortfolioManager extends BusinessObjectManager
{
	public static StudentPortfolioManager instance()
	{
		return instance(StudentPortfolioManager.class);
	}

	@Bean
	public IStudentPortfolioDAO dao()
	{
		return new StudentPortfolioDAO();
	}
}
