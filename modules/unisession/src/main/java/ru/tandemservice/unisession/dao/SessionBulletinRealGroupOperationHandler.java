package ru.tandemservice.unisession.dao;

import com.google.common.collect.Iterables;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.dao.group.IEppRealGroupOperationHandler;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4ActionType;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow.IEppRealEduGroupRowDescription;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;

import java.util.*;

/**
 * @author vdanilov
 */
public class SessionBulletinRealGroupOperationHandler extends UniBaseDao implements IEppRealGroupOperationHandler<EppRealEduGroup4ActionType> {

    @Override public boolean canHandle(final Class<EppRealEduGroup4ActionType> groupClass) {
        return EppRealEduGroup4ActionType.class.isAssignableFrom(groupClass);
    }

    @Override
    public void checkLocked(Collection<EppRealEduGroup> groups) throws ApplicationException {
        Iterables.partition(groups, DQL.MAX_VALUES_ROW_NUMBER).forEach(groupsPart -> {
            final DQLSelectBuilder dql = new DQLSelectBuilder();
            dql.fromEntity(SessionBulletinDocument.class, "b").column(DQLExpressions.property("b.id"));
            dql.where(DQLExpressions.in(DQLExpressions.property(SessionBulletinDocument.group().fromAlias("b")), groupsPart));

            final List<Long> ids = dql.createStatement(SessionBulletinRealGroupOperationHandler.this.getSession()).<Long>list();
            if (ids.isEmpty()) { return; }

            final SessionBulletinDocument bulletin = SessionBulletinRealGroupOperationHandler.this.get(SessionBulletinDocument.class, ids.iterator().next());
            final EppRealEduGroup4ActionType group = bulletin.getGroup();
            throw new ApplicationException("Учебная группа «"+group.getTitle()+" "+group.getActivityPart().getTitleWithNumber()+", "+group.getType().getTitle()+"» заблокирована (существует ведомость).");
        });
    }

    @Override public Collection<Long> getJoinCandidate(final Collection<EppRealEduGroupRow> rows) throws ApplicationException {

        final Set<EppRealEduGroup> groups = new HashSet<>();
        for (final EppRealEduGroupRow row: rows) { groups.add(row.getGroup()); }

        final DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(SessionBulletinDocument.class, "b").column(DQLExpressions.property(SessionBulletinDocument.group().id().fromAlias("b")));
        dql.where(DQLExpressions.in(DQLExpressions.property(SessionBulletinDocument.group().fromAlias("b")), groups));

        final List<Long> ids = this.getList(dql);
        if (ids.size() > 1) {
            final Set<String> groupTitles = new TreeSet<>();
            for (Long id : ids) {
                final EppRealEduGroup4ActionType group = this.get(EppRealEduGroup4ActionType.class, id);
                groupTitles.add(group.getTitle());
            }
            throw new ApplicationException("Учебные группы " + StringUtils.join(groupTitles, ", ") + " не могут быть объединены, так как для них созданы ведомости.");
        }

        return ids.isEmpty() ? null : Collections.singletonList(ids.get(0));
    }

    @Override public void move(final EppRealEduGroup group, final Map<EppRealEduGroupRow, EppRealEduGroupRow> rowMap) throws ApplicationException {
        // ничего не делаем
    }

    @Override public void checkAutoDelete(final IEppRealEduGroupRowDescription relation, final DQLSelectBuilder dql, final String alias) {
        // ничего не делаем (то, что нет ведомости, ссылающейся на УГС проверять не надо)
        // на группу не должны ссылаться никакие объекты - это уже проверяется в базовом условии
    }
}
