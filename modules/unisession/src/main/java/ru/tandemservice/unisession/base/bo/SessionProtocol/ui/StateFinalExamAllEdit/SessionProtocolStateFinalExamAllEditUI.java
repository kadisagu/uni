/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionProtocol.ui.StateFinalExamAllEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import ru.tandemservice.unisession.base.bo.SessionProtocol.ui.AbstractAllEdit.SessionProtocolAbstractAllEditUI;

/**
 * @author Andrey Andreev
 * @since 12.09.2016
 */
@Input({@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "bulletinId", required = true)})
public class SessionProtocolStateFinalExamAllEditUI extends SessionProtocolAbstractAllEditUI
{

}