/* $*/

package ru.tandemservice.unisession.component.orgunit.SessionListDocumentTab;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.unisession.entity.document.SessionListDocument;

/**
 * @author iolshvang
 * @since 12.04.2011
 */
@Input({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "orgUnitHolder.id")
})
@Output({
    @Bind(key = "cardId", binding = "cardId")
})
public class Model
{
    private final EntityHolder<OrgUnit> orgUnitHolder = new EntityHolder<>();
    private DynamicListDataSource<SessionListDocument> dataSource;
    private String settingsKey;
    private IDataSettings settings;
    private CommonPostfixPermissionModel sec;
    private ISelectModel yearModel;
    private ISelectModel partsModel;
    private ISelectModel courseCurrentModel;
    private ISelectModel groupCurrentModel;

    public Long getOrgUnitId()
    {
        return this.getOrgUnitHolder().getId();
    }

    public OrgUnit getOrgUnit()
    {
        return this.getOrgUnitHolder().getValue();
    }

    public EntityHolder<OrgUnit> getOrgUnitHolder()
    {
        return this.orgUnitHolder;
    }

    public DynamicListDataSource<SessionListDocument> getDataSource()
    {
        return this.dataSource;
    }

    public void setDataSource(final DynamicListDataSource<SessionListDocument> dataSource)
    {
        this.dataSource = dataSource;
    }

    public String getSettingsKey()
    {
        return this.settingsKey;
    }

    public void setSettingsKey(final String settingsKey)
    {
        this.settingsKey = settingsKey;
    }

    public IDataSettings getSettings()
    {
        return this.settings;
    }

    public void setSettings(final IDataSettings settings)
    {
        this.settings = settings;
    }

    public ISelectModel getYearModel()
    {
        return yearModel;
    }

    public void setYearModel(ISelectModel yearModel)
    {
        this.yearModel = yearModel;
    }

    public ISelectModel getPartsModel()
    {
        return partsModel;
    }

    public void setPartsModel(ISelectModel partsModel)
    {
        this.partsModel = partsModel;
    }

    public ISelectModel getCourseCurrentModel()
    {
        return courseCurrentModel;
    }

    public void setCourseCurrentModel(ISelectModel courseCurrentModel)
    {
        this.courseCurrentModel = courseCurrentModel;
    }

    public ISelectModel getGroupCurrentModel()
    {
        return groupCurrentModel;
    }

    public void setGroupCurrentModel(ISelectModel groupCurrentModel)
    {
        this.groupCurrentModel = groupCurrentModel;
    }

    public CommonPostfixPermissionModel getSec()
    {
        return this.sec;
    }

    public void setSec(final CommonPostfixPermissionModel sec)
    {
        this.sec = sec;
    }
}
