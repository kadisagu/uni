/* $*/

package ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinPubInline;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;
import org.tandemframework.shared.commonbase.utils.PublisherColumnBuilder;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.unisession.base.bo.SessionProtocol.ui.MassAddEdit.SessionProtocolMassAddEdit;
import ru.tandemservice.unisession.dao.bulletin.ISessionBulletinDAO;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.List;

/**
 * @author oleyba
 * @since 2/18/11
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);

        this.prepareListDataSource(component);
    }

    private void prepareListDataSource(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        final DynamicListDataSource<SessionDocumentSlot> dataSource = UniBaseUtils.createDataSource(component, this.getDao());

        if (model.isStateFinalExam())
            dataSource.addColumn(new CheckboxColumn("select", ""));
        dataSource.addColumn(new SimpleColumn("№ зач. книжки", SessionDocumentSlot.actualStudent().bookNumber().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Академ. группа", "eppRealEduGroup").setOrderable(false).setClickable(false));
        dataSource.addColumn(new PublisherColumnBuilder("ФИО", Student.FIO_KEY, "studentNewSessionTab").subTab("studentSessionMarkTab").tabBind(ru.tandemservice.uni.component.student.StudentPub.Model.SELECTED_TAB_BIND_KEY).entity(SessionDocumentSlot.actualStudent().s()).build());
        dataSource.addColumn(new SimpleColumn("Вид затрат", SessionDocumentSlot.actualStudent().compensationType().shortTitle().s()).setOrderable(false).setClickable(false));
        if (model.getColumnPKeyHandler().getAllowanceColumnPKey() != null)
        {
            dataSource.addColumn(new ToggleColumn("Допущен", IDAO.V_ALLOWED).setListener("onClickEditAllowance").setDisabledProperty(IDAO.V_ALLOWED_DISABLED).setOrderable(false).setPermissionKey(model.getColumnPKeyHandler().getAllowanceColumnPKey()));
        }
        else
        {
            dataSource.addColumn(new BooleanColumn("Допущен", IDAO.V_ALLOWED));
        }

        if (ISessionBulletinDAO.instance.get().isBulletinRequireTheme(model.getBulletin()))
        {
            dataSource.addColumn(new SimpleColumn("Тема", IDAO.PROP_THEME).setClickable(false).setOrderable(false));
            dataSource.addColumn(new SimpleColumn("Руководитель", IDAO.PROP_SUPERVISOR).setClickable(false).setOrderable(false));
        }
        if (model.isUseCurrentRating())
        {
            dataSource.addColumn(new SimpleColumn("Текущий рейтинг", "currentRating").setClickable(false).setOrderable(false));
            dataSource.addColumn(new SimpleColumn("Баллы за сдачу мероприятия", "scoredPoints").setClickable(false).setOrderable(false));
            dataSource.addColumn(new SimpleColumn("Суммарный рейтинг", "points").setClickable(false).setOrderable(false));
        }
        else if (model.isUsePoints())
        {
            dataSource.addColumn(new SimpleColumn("Баллы за сдачу мероприятия", "points").setClickable(false).setOrderable(false));
        }

        if (model.isShowPracticeTutorMarkColumn())
        {
            dataSource.addColumn(new SimpleColumn("Оценка руководителя", "practiceTutorMark").setClickable(false).setOrderable(false));
        }
        dataSource.addColumn(new SimpleColumn("Оценка", IDAO.V_MARK + "." + SessionMark.valueShortTitle().s()).setClickable(false).setOrderable(false));
        prepareAdditionalMarkColumns(model, dataSource);
        dataSource.addColumn(new ActionColumn("Число пересдач", "", "onClickShowHistory").setTextKey(IDAO.V_MARK_COUNT).setDisplayHeader(true));
        dataSource.addColumn(new SimpleColumn("Преподаватель", "ppsColumn").setFormatter(UniEppUtils.NEW_LINE_FORMATTER).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Дата сдачи", IDAO.V_MARK + "." + SessionMark.performDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Ранее полученная оценка", "finalMark").setClickable(false).setOrderable(false));

        if (model.getColumnPKeyHandler().getAddSheetColumnPKey() != null)
        {
            dataSource.addColumn(new ActionColumn("Добавить экзаменационный лист", "add_sheet", "onClickAddSheet").setPermissionKey(model.getColumnPKeyHandler().getAddSheetColumnPKey()));
        }

        model.setDataSource(dataSource);
        this.getDao().prepareListDataSource(model);
    }

    public void onClickAddSheet(final IBusinessComponent component)
    {
        final SessionDocumentSlot slot = UniDaoFacade.getCoreDao().get(SessionDocumentSlot.class, component.getListenerParameter());

        if (null != slot)
        {
            final SessionBulletinDocument bulletin = (SessionBulletinDocument) slot.getDocument();
            ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                    ru.tandemservice.unisession.component.sessionSheet.SessionSheetAdd.Model.COMPONENT_NAME,
                    new ParametersMap()
                            .add(PublisherActivator.PUBLISHER_ID_KEY, bulletin.getSessionObject().getOrgUnit().getId())
                            .add("controlActionId", slot.getStudentWpeCAction().getId())));
        }
    }

    public void onClickShowHistory(final IBusinessComponent component)
    {
        final SessionDocumentSlot slot = UniDaoFacade.getCoreDao().get(SessionDocumentSlot.class, component.getListenerParameter());
        if (null != slot)
        {
            ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                    ru.tandemservice.unisession.component.student.ControlActionMarkHistory.Model.COMPONENT_NAME,
                    new ParametersMap().add("controlActionId", slot.getStudentWpeCAction().getId()).add("showAsSingle", true)));
        }
    }

    public void onClickEditAllowance(final IBusinessComponent component)
    {
        this.getDao().updateAllowance(component.getListenerParameter());
    }

    protected void prepareAdditionalMarkColumns(Model model, DynamicListDataSource<SessionDocumentSlot> dataSource)
    {
    }

    public void onClickMassAddStateFinalExamProtocols(final IBusinessComponent component)
    {
        Model model = this.getModel(component);

        List<Long> slotIds = getDao().getAllowedStudentSlotIds(getModel(component));

        if (CollectionUtils.isEmpty(slotIds))
            throw new ApplicationException("Для создания протоколов ГИА должен быть выбран хотя бы один допущенный студент, не имеющий протокола по этой ведомости");

        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                SessionProtocolMassAddEdit.class.getSimpleName(),
                ParametersMap
                        .createWith(SessionProtocolMassAddEdit.BULLETIN_ID, model.getBulletin().getId())
                        .add(SessionProtocolMassAddEdit.SESSION_DOCUMENT_SLOT_IDS, slotIds)
        ));
    }
}