/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.SummaryBulletinList;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unisession.entity.document.SessionObject;
import ru.tandemservice.unisession.entity.report.UnisessionSummaryBulletinReport;

import java.util.Collections;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 12/29/11
 */
public class SessionSummaryBulletinDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String PARAM_SESSION_OBJECT = "sessionObjectKey";

    private final DQLOrderDescriptionRegistry registry = buildOrderRegistry();

    public SessionSummaryBulletinDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput dsInput, ExecutionContext context)
    {
        final SessionObject sessionObject = context.get(PARAM_SESSION_OBJECT);
        if (null == sessionObject) {
            return ListOutputBuilder.get(dsInput, Collections.emptyList()).build();
        }

        DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(UnisessionSummaryBulletinReport.class, "r")
            .where(eq(property(UnisessionSummaryBulletinReport.sessionObject().fromAlias("r")), value(sessionObject)))
            ;

        registry.applyOrder(dql, dsInput.getEntityOrder());

        return DQLSelectOutputBuilder.get(dsInput, dql, context.getSession()).build();
    }

    protected DQLOrderDescriptionRegistry buildOrderRegistry() {
        return new DQLOrderDescriptionRegistry(UnisessionSummaryBulletinReport.class, "r");
    }


}
