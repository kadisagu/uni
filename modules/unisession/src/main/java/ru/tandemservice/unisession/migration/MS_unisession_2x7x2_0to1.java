package ru.tandemservice.unisession.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused"})
public class MS_unisession_2x7x2_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[] {
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность unisessionResultsReport
    	tool.renameColumn("session_rep_results_t", "discobligation_p", "disckinds_p");

        ////////////////////////////////////////////////////////////////////////////////
        // сущность unisessionGroupBulletinListReport
        tool.renameColumn("session_rep_grp_bull_list_t", "discobligation_p", "disckinds_p");

        ////////////////////////////////////////////////////////////////////////////////
        // сущность unisessionGroupMarksReport
        tool.renameColumn("session_rep_grp_marks_t", "discobligation_p", "disckinds_p");

        ////////////////////////////////////////////////////////////////////////////////
        // сущность unisessionResultsByDiscReport
        tool.renameColumn("session_rep_results_d_t", "discobligation_p", "disckinds_p");

        ////////////////////////////////////////////////////////////////////////////////
        // сущность unisessionSummaryBulletinReport
        tool.renameColumn("session_rep_summary_bull_t", "discobligation_p", "disckinds_p");
    }
}