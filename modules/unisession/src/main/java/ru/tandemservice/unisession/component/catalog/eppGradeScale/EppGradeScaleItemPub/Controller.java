/* $Id: controllerItemPub.vm 6177 2009-01-13 14:09:27Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisession.component.catalog.eppGradeScale.EppGradeScaleItemPub;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDefine;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogItemPub.DefaultCatalogItemPubController;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;

/**
 * @author AutoGenerator
 * Created on 15.02.2011
 */
public class Controller extends DefaultCatalogItemPubController<EppGradeScale, Model, IDAO>
{
    @Override public void onRefreshComponent(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);

        if (null == model.getDataSource()) {
            final DynamicListDataSource<SessionMarkGradeValueCatalogItem> dataSource = new DynamicListDataSource<>(component, component1 -> {
                Controller.this.getDao().prepareListDataSource(model, component1.getSettings());
            });
            dataSource.addColumn(new SimpleColumn("Название", SessionMarkGradeValueCatalogItem.title().s()).setOrderable(false));
            dataSource.addColumn(new SimpleColumn("Сокращенное название", SessionMarkGradeValueCatalogItem.shortTitle().s()).setOrderable(false));
            dataSource.addColumn(new SimpleColumn("Печатное название", SessionMarkGradeValueCatalogItem.printTitle().s()).setOrderable(false));
            dataSource.addColumn(new ToggleColumn("Положительная", SessionMarkGradeValueCatalogItem.positive().s()).setListener("onClickPositive"));
            final ActionColumn veryUp = new ActionColumn("Установить максимальный приоритет", CommonBaseDefine.ICO_VERY_UP, "onClickVeryUp");
            veryUp.setIconResolver(entity -> CommonBaseDefine.ICO_VERY_UP);
            dataSource.addColumn(veryUp.setOrderable(false).setDisableHandler(this.getDao().getUpDisabledEntityHandler()));

            final ActionColumn up = new ActionColumn("Повысить приоритет", CommonBaseDefine.ICO_UP, "onClickUp");
            up.setIconResolver(entity -> EppGradeScale.class.isInstance(entity) ? null : CommonBaseDefine.ICO_UP);
            dataSource.addColumn(up.setOrderable(false).setDisableHandler(this.getDao().getUpDisabledEntityHandler()));
            final ActionColumn down = new ActionColumn("Понизить приоритет", CommonBaseDefine.ICO_DOWN, "onClickDown");
            down.setIconResolver(entity -> EppGradeScale.class.isInstance(entity) ? null : CommonBaseDefine.ICO_DOWN);
            dataSource.addColumn(down.setOrderable(false).setDisableHandler(this.getDao().getDownDisabledEntityHandler()));

            final ActionColumn veryDown = new ActionColumn("Установить минимальный приоритет", CommonBaseDefine.ICO_VERY_DOWN, "onClickVeryDown");
            veryDown.setIconResolver(entity -> CommonBaseDefine.ICO_VERY_DOWN);
            dataSource.addColumn(veryDown.setOrderable(false).setDisableHandler(this.getDao().getDownDisabledEntityHandler()));

            dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditSessionMarkGradeValue").setPermissionKey(model.getCatalogItemEdit()));

            if (!model.isSystem()) {
                dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteSessionMarkGradeValue", "Удалить оценку {0} из шкалы "+ model.getCatalogItem().getTitle() +"?", SessionMarkGradeValueCatalogItem.P_TITLE).setPermissionKey(model.getCatalogItemDelete()));
            }


            model.setDataSource(dataSource);
        }
    }

    public void onClickPositive(final IBusinessComponent component)
    {
        this.getDao().updatePositive(component.getListenerParameter());
    }

    public void onClickUp(final IBusinessComponent context)
    {
        this.getDao().updatePriorityUp(context.getListenerParameter());
    }

    public void onClickDown(final IBusinessComponent context)
    {
        this.getDao().updatePriorityDown(context.getListenerParameter());
    }

    public void onClickAddMark(final IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
            ru.tandemservice.unisession.component.catalog.sessionMarkGradeValue.SessionMarkGradeValueAddEdit.Model.COMPONENT_NAME,
            new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, this.getModel(component).getCatalogItem().getId())
            .add("catalogCode", StringUtils.uncapitalize(SessionMarkGradeValueCatalogItem.class.getSimpleName()))
            .add("sessionGradeScaleId", this.getModel(component).getCatalogItemId())
        ));
    }

    public void onClickDeleteSessionMarkGradeValue(final IBusinessComponent component)
    {
        this.getDao().deleteMark(component.getListenerParameter());
        this.onRefreshComponent(component);
    }

    public void onClickEditSessionMarkGradeValue(final IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
            ru.tandemservice.unisession.component.catalog.sessionMarkGradeValue.SessionMarkGradeValueAddEdit.Model.COMPONENT_NAME,
            new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, this.getModel(component).getCatalogItem().getId())
            .add("catalogCode", StringUtils.uncapitalize(SessionMarkGradeValueCatalogItem.class.getSimpleName()))
            .add("sessionGradeScaleId", this.getModel(component).getCatalogItemId())
            .add("catalogItemId", component.getListenerParameter())
        ));
    }

    public void onClickDeleteItem(final IBusinessComponent component)
    {
        this.getDao().delete(this.getModel(component).getCatalogItemId());
        this.deactivate(component);
    }

    public void onClickVeryUp(final IBusinessComponent context)
    {
        this.getDao().updatePriorityVeryUp(context.getListenerParameter());
    }

    public void onClickVeryDown(final IBusinessComponent context)
    {
        this.getDao().updatePriorityVeryDown(context.getListenerParameter());
    }

}
