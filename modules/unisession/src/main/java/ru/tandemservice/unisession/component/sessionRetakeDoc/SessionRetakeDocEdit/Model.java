/* $Id$ */
package ru.tandemservice.unisession.component.sessionRetakeDoc.SessionRetakeDocEdit;

import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.PpsEntry.util.PpsEntrySelectBlockData;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unisession.entity.document.SessionRetakeDocument;

import java.util.List;

/**
 * @author oleyba
 * @since 6/15/11
 */
@Input(keys = PublisherActivator.PUBLISHER_ID_KEY, bindings = "doc.id")

public class Model
{
    public static final String COMPONENT_NAME = Model.class.getPackage().getName();

    private SessionRetakeDocument doc = new SessionRetakeDocument();

    private PpsEntrySelectBlockData ppsData;

    public SessionRetakeDocument getDoc()
    {
        return this.doc;
    }

    public void setDoc(final SessionRetakeDocument doc)
    {
        this.doc = doc;
    }

    public PpsEntrySelectBlockData getPpsData()
    {
        return ppsData;
    }

    public void setPpsData(PpsEntrySelectBlockData ppsData)
    {
        this.ppsData = ppsData;
    }

    public List<PpsEntry> getPpsList()
    {
        return getPpsData().getSelectedPpsList();
    }

    public void setPpsList(final List<PpsEntry> ppsList)
    {
        getPpsData().setSelectedPps(ppsList);
    }

    public OrgUnit getOrgUnit()
    {
        return getDoc().getGroupOu();
    }
}
