// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisession.component.pps.PpsSessionBulletinList;

import com.google.common.collect.ImmutableList;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Session;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.entity.dsl.PropertyPath;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.NumberAsStringComparator;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.util.cache.SingleValueCache;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.gen.OrgUnitGen;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.tapsupport.component.selection.CommonMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniedu.base.bo.EducationYear.utils.EducationYearModel;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.catalog.EppControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.gen.EppRegistryElementGen;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4ActionTypeRow;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.uniepp.entity.student.group.gen.EppRealEduGroup4ActionTypeRowGen;
import ru.tandemservice.uniepp.ui.EppRegistryElementSelectModel;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.upper;

/**
 * @author iolshvang
 * @since 11.04.2011
 */
public class DAO extends UniBaseDao implements IDAO
{
    private final DQLOrderDescriptionRegistry order = new DQLOrderDescriptionRegistry(SessionBulletinDocument.class, "b");

    @Override
    @SuppressWarnings("unchecked")
    public void prepare(final Model model)
    {
        model.setPerson(PersonManager.instance().dao().getPerson(model.getPrincipalContext()));

        model.setYearModel(new EducationYearModel());
        model.setPartsModel(EducationCatalogsManager.getYearDistributionPartModel());

        model.setControlActionTypeModel(new DQLFullCheckSelectModel("title")
        {
            @Override
            protected DQLSelectBuilder query(final String alias, final String filter)
            {
                final DQLSelectBuilder s = DAO.this.getRootDql(model);
                s.column(DQLExpressions.property(SessionBulletinDocument.group().type().id().fromAlias("b")));

                final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppFControlActionType.class, alias);
                dql.where(DQLExpressions.in(DQLExpressions.property(alias, EppFControlActionType.eppGroupType()), s.buildQuery()));
                if (null != filter)
                {
                    dql.where(DQLExpressions.or(
                            this.like(EppControlActionType.title().fromAlias(alias), filter),
                            this.like(EppControlActionType.shortTitle().fromAlias(alias), filter)
                    ));
                }

                dql.order(DQLExpressions.property(EppControlActionType.shortTitle().fromAlias(alias)));
                dql.order(DQLExpressions.property(EppControlActionType.title().fromAlias(alias)));
                return dql;
            }
        });

        model.setRegistryOwnerModel(new DQLFullCheckSelectModel("shortTitle")
        {
            @Override
            protected DQLSelectBuilder query(final String alias, final String filter)
            {
                final DQLSelectBuilder s = DAO.this.getRootDql(model);
                s.column(DQLExpressions.property(SessionBulletinDocument.group().activityPart().registryElement().owner().id().fromAlias("b")));

                final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(OrgUnit.class, alias);
                dql.where(DQLExpressions.in(DQLExpressions.property(alias, "id"), s.buildQuery()));
                if (null != filter)
                {
                    dql.where(DQLExpressions.or(
                            this.like(OrgUnitGen.title().fromAlias(alias), filter),
                            this.like(OrgUnitGen.shortTitle().fromAlias(alias), filter)
                    ));
                }

                dql.order(DQLExpressions.property(OrgUnitGen.shortTitle().fromAlias(alias)));
                dql.order(DQLExpressions.property(OrgUnitGen.title().fromAlias(alias)));
                return dql;
            }
        });

        model.setRegistryElementModel(new EppRegistryElementSelectModel<EppRegistryElement>(EppRegistryElement.class)
        {
            @Override
            protected DQLSelectBuilder query(final String alias, final String filter)
            {
                final DQLSelectBuilder s = DAO.this.getRootDql(model);
                s.column(DQLExpressions.property(SessionBulletinDocument.group().activityPart().registryElement().id().fromAlias("b")));

                final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppRegistryElement.class, alias);
                dql.where(DQLExpressions.in(DQLExpressions.property(alias, "id"), s.buildQuery()));

                {
                    final Collection registryOwners = model.getRegistryOwners();
                    if (null != registryOwners)
                    {
                        dql.where(DQLExpressions.in(DQLExpressions.property(EppRegistryElementGen.owner().fromAlias(alias)), registryOwners));
                    }
                }

                if (null != filter) {
                    dql.where(this.getFilterCondition(alias, filter));
                }
                dql.order(DQLExpressions.property(EppRegistryElementGen.title().fromAlias(alias)));
                dql.order(DQLExpressions.property(EppRegistryElementGen.owner().title().fromAlias(alias)));
                return dql;
            }
        });

        {
            model.setGroupModel(UniEppUtils.getTitleSelectModel(new SingleValueCache<List<String>>()
            {
                @Override
                protected List<String> resolve()
                {
                    final DQLSelectBuilder s = DAO.this.getRelationDql(model);
                    s.predicate(DQLPredicateType.distinct);
                    s.column(DQLExpressions.property(EppRealEduGroup4ActionTypeRowGen.studentGroupTitle().fromAlias("rel")));
                    s.order(DQLExpressions.property(EppRealEduGroup4ActionTypeRowGen.studentGroupTitle().fromAlias("rel")));
                    s.where(DQLExpressions.isNull(DQLExpressions.property(EppRealEduGroup4ActionTypeRowGen.removalDate().fromAlias("rel"))));
                    return s.createStatement(DAO.this.getSession()).<String>list(); // если надо обновить - нажмут f5
                }
            }));
        }

        model.setBullStatusModel(new LazySimpleSelectModel<>(ImmutableList.of(
                new DataWrapper(Model.BULL_STATUS_EMPTY_ID, "Незаполненная"),
                new DataWrapper(Model.BULL_STATUS_OPEN_ID, "Открытая"),
                new DataWrapper(Model.BULL_STATUS_CLOSE_ID, "Закрытая")
        )));

        model.setRegistryStructureModel(new CommonMultiSelectModel()
        {
            {
                setHierarchical(true);
            }

            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppRegistryStructure.class, "b").column(property("b"))
                        .where(likeUpper(property(EppRegistryStructure.title().fromAlias("b")), value(CoreStringUtils.escapeLike(filter, true))))
                        .order(property(EppRegistryStructure.title().fromAlias("b")));

                if (set != null)
                    builder.where(in(property(EppRegistryStructure.id().fromAlias("b")), set));

                return new DQLListResultBuilder(builder, 50);
            }
        });

        model.setCourseModel(new CommonMultiSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Course.class, "b").column(property("b"))
                        .where(likeUpper(property(Course.title().fromAlias("b")), value(CoreStringUtils.escapeLike(filter, true))))
                        .order(property(Course.title().fromAlias("b")));

                if (set != null)
                    builder.where(in(property(Course.id().fromAlias("b")), set));

                return new DQLListResultBuilder(builder, 50);
            }
        });

        model.setCourseCurrentModel(new CommonMultiSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Course.class, "b").column(property("b"))
                        .where(likeUpper(property(Course.title().fromAlias("b")), value(CoreStringUtils.escapeLike(filter, true))))
                        .order(property(Course.title().fromAlias("b")));

                if (set != null)
                    builder.where(in(property(Course.id().fromAlias("b")), set));

                return new DQLListResultBuilder(builder, 50);
            }
        });

        model.setGroupCurrentModel(new CommonMultiSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                DQLSelectBuilder rootDql = getRootDql(model);
                rootDql.column(property(SessionBulletinDocument.id().fromAlias("b")));
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SessionDocumentSlot.class, "a").column(property(SessionDocumentSlot.studentWpeCAction().studentWpe().student().group().fromAlias("a")))
                        .where(in(property(SessionDocumentSlot.document().id().fromAlias("a")), rootDql.buildQuery()))
                        .where(likeUpper(property(SessionDocumentSlot.studentWpeCAction().studentWpe().student().group().title().fromAlias("a")), value(CoreStringUtils.escapeLike(filter, true))))
                        .order(property(SessionDocumentSlot.studentWpeCAction().studentWpe().student().group().title().fromAlias("a")))
                        .predicate(DQLPredicateType.distinct);

                if (model.getCourseCurrentList() != null)
                    builder.where(in(property(SessionDocumentSlot.studentWpeCAction().studentWpe().student().group().course().fromAlias("a")), model.getCourseCurrentList()));

                if (set != null)
                    builder.where(in(property(SessionDocumentSlot.studentWpeCAction().studentWpe().student().group().id().fromAlias("a")), set));

                return new DQLListResultBuilder(builder, 50);
            }
        });
    }

    @Override
    @SuppressWarnings("unchecked")
    public void refreshDataSource(final Model model)
    {
        final Session session = this.getSession();
        final DynamicListDataSource<SessionBulletinDocument> dataSource = model.getDataSource();

        final DQLSelectBuilder dql = this.getRootDql(model);

        final Collection registryElements = model.getRegistryElements();
        if (null != registryElements)
        {
            dql.where(DQLExpressions.in(DQLExpressions.property(SessionBulletinDocument.group().activityPart().registryElement().fromAlias("b")), registryElements));
        } else
        {
            final Collection registryOwners = model.getRegistryOwners();
            if (null != registryOwners)
            {
                dql.where(DQLExpressions.in(DQLExpressions.property(SessionBulletinDocument.group().activityPart().registryElement().owner().fromAlias("b")), registryOwners));
            }
        }

        final Collection groups = model.getGroups();
        if (null != groups)
        {
            final DQLSelectBuilder dqlX = new DQLSelectBuilder().fromEntity(EppRealEduGroup4ActionTypeRow.class, "rel").column(DQLExpressions.property(EppRealEduGroup4ActionTypeRowGen.group().id().fromAlias("rel")));
            dqlX.where(DQLExpressions.in(DQLExpressions.property(EppRealEduGroup4ActionTypeRowGen.studentGroupTitle().fromAlias("rel")), groups));
            dql.where(DQLExpressions.in(DQLExpressions.property(SessionBulletinDocument.group().id().fromAlias("b")), dqlX.buildQuery()));
        }

        final Collection controlActionTypes = model.getControlActionTypes();
        if (null != controlActionTypes)
        {
            dql.where(DQLExpressions.in(DQLExpressions.property(SessionBulletinDocument.group().type().fromAlias("b")),
                                        CommonBaseEntityUtil.getPropertiesList(controlActionTypes, EppFControlActionType.eppGroupType())));
        }

        final String documentNumber = model.getDocumentNumber();
        if (documentNumber != null)
            dql.where(eq(property(SessionBulletinDocument.number().fromAlias("b")), value(documentNumber)));

        final DataWrapper bullStatus = model.getBullStatus();
        if (bullStatus != null)
        {
            if (bullStatus.getId().equals(Model.BULL_STATUS_EMPTY_ID))
            {
                DQLSelectBuilder slotBuilder = new DQLSelectBuilder().fromEntity(SessionDocumentSlot.class, "slot").column(property(SessionDocumentSlot.id().fromAlias("slot")))
                        .where(eq(property(SessionDocumentSlot.document().id().fromAlias("slot")), property(SessionBulletinDocument.id().fromAlias("b"))))
                        .where(notExists(
                                new DQLSelectBuilder().fromEntity(SessionMark.class, "mark").column(property(SessionMark.id().fromAlias("mark")))
                                        .where(eq(property(SessionDocumentSlot.id().fromAlias("slot")), property(SessionMark.slot().id().fromAlias("mark"))))
                                        .buildQuery()
                        ));
                dql.where(exists(slotBuilder.buildQuery()));
            }
            else if (bullStatus.getId().equals(Model.BULL_STATUS_OPEN_ID))
            {
                dql.where(isNull(property(SessionBulletinDocument.closeDate().fromAlias("b"))));
            }
            else if (bullStatus.getId().equals(Model.BULL_STATUS_CLOSE_ID))
            {
                dql.where(isNotNull(property(SessionBulletinDocument.closeDate().fromAlias("b"))));
            }
        }

        final Collection<EppRegistryStructure> registryStructureList = model.getRegistryStructureList();
        if (registryStructureList != null)
            dql.where(in(property(SessionBulletinDocument.group().activityPart().registryElement().parent().fromAlias("b")), registryStructureList));

        final Collection<Course> courseList = model.getCourseList();
        if (courseList != null)
        {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppRealEduGroupRow.class, "ugsSlot").column(property(EppRealEduGroupRow.id().fromAlias("ugsSlot")))
                    .where(eq(property(EppRealEduGroupRow.group().id().fromAlias("ugsSlot")), property(SessionBulletinDocument.group().id().fromAlias("b"))))
                    .where(in(property(EppRealEduGroupRow.studentWpePart().studentWpe().course().fromAlias("ugsSlot")), courseList));

            dql.where(exists(builder.buildQuery()));
        }

        final Collection<Course> courseCurrentList = model.getCourseCurrentList();
        if (courseCurrentList != null)
        {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SessionDocumentSlot.class, "slot1").column(property(SessionDocumentSlot.id().fromAlias("slot1")))
                    .where(eq(property(SessionDocumentSlot.document().id().fromAlias("slot1")), property(SessionBulletinDocument.id().fromAlias("b"))))
                    .where(in(property(SessionDocumentSlot.studentWpeCAction().studentWpe().student().course().fromAlias("slot1")), courseCurrentList));

            dql.where(exists(builder.buildQuery()));
        }

        final Collection<Course> groupCurrentList = model.getGroupCurrentList();
        if (groupCurrentList != null)
        {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SessionDocumentSlot.class, "slot2").column(property(SessionDocumentSlot.id().fromAlias("slot2")))
                    .where(eq(property(SessionDocumentSlot.document().id().fromAlias("slot2")), property(SessionBulletinDocument.id().fromAlias("b"))))
                    .where(in(property(SessionDocumentSlot.studentWpeCAction().studentWpe().student().group().fromAlias("slot2")), groupCurrentList));

            dql.where(exists(builder.buildQuery()));
        }

        final String studentLastName = model.getStudentLastName();
        if (studentLastName != null)
        {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SessionDocumentSlot.class, "slot3").column(property(SessionDocumentSlot.id().fromAlias("slot3")))
                    .where(eq(property(SessionDocumentSlot.document().id().fromAlias("slot3")), property(SessionBulletinDocument.id().fromAlias("b"))))
                    .where(like(upper(property(SessionDocumentSlot.studentWpeCAction().studentWpe().student().person().identityCard().lastName().fromAlias("slot3"))), value(CoreStringUtils.escapeLike(studentLastName, true))));

            dql.where(exists(builder.buildQuery()));
        }


        final EntityOrder entityOrder = dataSource.getEntityOrder();
        if ("title".equals(entityOrder.getKey()))
        {
            dql.column(DQLExpressions.property("b.id"));
            dql.column(DQLExpressions.property("b.number"));
            List rows = dql.createStatement(session).list();
            Collections.sort(rows, new Comparator<Object[]>() {
                @Override public int compare(final Object[] o1, final Object[] o2) {
                    return NumberAsStringComparator.INSTANCE.compare((String) o1[1], (String) o2[1]);
                }
            });
            CollectionUtils.transform(rows, input -> ((Object[]) input)[0]);

            if (dataSource.getEntityOrder().getDirection() == OrderDirection.desc)
            {
                Collections.reverse(rows);
            }

            dataSource.setTotalSize(rows.size());
            final int startRow = (int) dataSource.getStartRow();
            final int countRow = (int) dataSource.getCountRow();
            rows = rows.subList(startRow, Math.min(startRow + countRow, rows.size()));
            dataSource.createPage(sort(this.getList(SessionBulletinDocument.class, "id", rows), rows));

        } else if ("course".equals(entityOrder.getKey()))
        {
            this.createPage(dataSource, dql, EppRealEduGroupRow.studentWpePart().studentWpe().course().intValue());
        } else if ("groups".equals(entityOrder.getKey()))
        {
            this.createPage(dataSource, dql, EppRealEduGroupRow.studentGroupTitle());
        } else
        {
            this.order.applyOrder(dql, entityOrder);
            UniBaseUtils.createPage(dataSource, dql.column(DQLExpressions.property("b")), session);
        }

        final List<ViewWrapper<SessionBulletinDocument>> list = ViewWrapper.getPatchedList(dataSource);

        BatchUtils.execute(list, 64, new BatchUtils.Action<ViewWrapper<SessionBulletinDocument>>()
                {
            private <T> Map<Long, Set<T>> getRelationMap(final Collection<Long> ids, final PropertyPath<T> relationPath)
            {
                final DQLSelectBuilder relDql = DAO.this.getRelationDql(ids);
                relDql.column(DQLExpressions.property(SessionBulletinDocument.id().fromAlias("b")));
                relDql.column(DQLExpressions.property(relationPath.fromAlias("rel")));
                relDql.predicate(DQLPredicateType.distinct);
                relDql.order(DQLExpressions.property(relationPath.fromAlias("rel")));
                final Map<Long, Set<T>> result = new HashMap<>();
                for (final Object[] row : relDql.createStatement(session).<Object[]>list())
                {
                    if (null != row[1]) {
                        SafeMap.safeGet(result, (Long) row[0], LinkedHashSet.class).add((T) row[1]);
                    }
                }
                return result;
            }

            @Override
            public void execute(final Collection<ViewWrapper<SessionBulletinDocument>> list)
            {
                final Collection<Long> ids = UniBaseUtils.getIdList(list);
                final Map<Long, Set<String>> groupMap = this.getRelationMap(ids, EppRealEduGroup4ActionTypeRow.studentGroupTitle());
                final Map<Long, Set<String>> courseMap = this.getRelationMap(ids, EppRealEduGroup4ActionTypeRow.studentWpePart().studentWpe().course().title());

                for (final ViewWrapper<SessionBulletinDocument> bulletin : list)
                {
                    final Long id = bulletin.getId();
                    bulletin.setViewProperty("course", courseMap.get(id));
                    bulletin.setViewProperty("groups", groupMap.get(id));
                }

            }
                });
    }

    @SuppressWarnings("unchecked")
    private void createPage(final DynamicListDataSource<SessionBulletinDocument> dataSource, final DQLSelectBuilder dql, final PropertyPath path)
    {
        List<Long> ids = this.getComparedIds(dql, path);
        if (dataSource.getEntityOrder().getDirection() == OrderDirection.desc)
        {
            Collections.reverse(ids);
        }

        dataSource.setTotalSize(ids.size());

        final int startRow = (int) dataSource.getStartRow();
        final int countRow = (int) dataSource.getCountRow();
        ids = ids.subList(startRow, Math.min(startRow + countRow, ids.size()));

        dataSource.createPage(sort(this.getList(SessionBulletinDocument.class, "id", ids), ids));
    }

    @SuppressWarnings("unchecked")
    private List<Long> getComparedIds(DQLSelectBuilder dql, final PropertyPath propertyPath)
    {
        final Session session = this.getSession();
        dql = this.joinRelations(dql);
        dql.column(DQLExpressions.property("b", "id"));
        dql.column(DQLExpressions.property(propertyPath.fromAlias("rel")));
        dql.predicate(DQLPredicateType.distinct);
        return this.getSortedIds(dql.createStatement(session).<Object[]>list(), 1);
    }

    private DQLSelectBuilder getRootDql(final Model model)
    {
        final DQLSelectBuilder rootDql = this.order.buildDQLSelectBuilder();

        final DQLSelectBuilder ppsDQL = new DQLSelectBuilder();
        ppsDQL.fromEntity(SessionComissionPps.class, "pps");
        ppsDQL.joinEntity("pps", DQLJoinType.inner, SessionBulletinDocument.class, "bul", DQLExpressions.eq(
                DQLExpressions.property(SessionComissionPps.commission().id().fromAlias("pps")),
                DQLExpressions.property(SessionBulletinDocument.commission().id().fromAlias("bul"))
        ));
        ppsDQL.where(DQLExpressions.eq(DQLExpressions.property(SessionComissionPps.pps().person().fromAlias("pps")), DQLExpressions.value(model.getPerson())));
        ppsDQL.column(DQLExpressions.property(SessionBulletinDocument.id().fromAlias("bul")));

        rootDql.where(DQLExpressions.in(DQLExpressions.property(SessionBulletinDocument.id().fromAlias("b")), ppsDQL.buildQuery()));
        rootDql.where(DQLExpressions.eq(DQLExpressions.property(SessionBulletinDocument.sessionObject().educationYear().fromAlias("b")), DQLExpressions.value(model.getYear())));
        rootDql.where(DQLExpressions.eq(DQLExpressions.property(SessionBulletinDocument.sessionObject().yearDistributionPart().fromAlias("b")), DQLExpressions.value(model.getPart())));
        return rootDql;
    }

    private DQLSelectBuilder getRelationDqlBase()
    {
        return this.joinRelations(this.order.buildDQLSelectBuilder());
    }

    protected DQLSelectBuilder joinRelations(final DQLSelectBuilder dql)
    {
        dql.joinEntity("b", DQLJoinType.left, SessionDocumentSlot.class, "s", DQLExpressions.eq(
                DQLExpressions.property(SessionDocumentSlot.document().fromAlias("s")), DQLExpressions.property("b")
        ));
        dql.joinEntity("s", DQLJoinType.left, EppRealEduGroup4ActionTypeRow.class, "rel", DQLExpressions.and(
                DQLExpressions.isNull(DQLExpressions.property(EppRealEduGroup4ActionTypeRow.removalDate().fromAlias("rel"))),
                DQLExpressions.eq(DQLExpressions.property(EppRealEduGroup4ActionTypeRow.studentWpePart().fromAlias("rel")), DQLExpressions.property(SessionDocumentSlot.studentWpeCAction().id().fromAlias("s")))
        ));
        return dql;
    }

    private DQLSelectBuilder getRelationDql(final Model model)
    {
        final DQLSelectBuilder rootDql = this.getRootDql(model).column(DQLExpressions.property("b.id"));
        return this.getRelationDqlBase().where(DQLExpressions.in(DQLExpressions.property("b.id"), rootDql.buildQuery()));
    }

    private DQLSelectBuilder getRelationDql(final Collection<Long> bulletinIds)
    {
        return this.getRelationDqlBase().where(DQLExpressions.in(DQLExpressions.property("b.id"), bulletinIds));
    }
}
