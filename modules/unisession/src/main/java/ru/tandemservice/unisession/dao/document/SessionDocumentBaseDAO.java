/* $Id$ */
package ru.tandemservice.unisession.dao.document;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale;
import ru.tandemservice.uniepp.entity.catalog.gen.EppGradeScaleGen;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.UnisessionDefines;
import ru.tandemservice.unisession.entity.catalog.codes.SessionMarkStateCatalogItemCodes;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;
import ru.tandemservice.unisession.entity.document.SessionDocument;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionProjectTheme;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.mark.SessionSlotMarkState;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 6/24/11
 */
public class SessionDocumentBaseDAO extends UniBaseDao implements ISessionDocumentBaseDAO
{
    @Override
    public void doCloseDocument(final SessionDocument document)
    {
        // проверим, что выставлены оценки
        final DQLSelectBuilder emptySlotDQL = new DQLSelectBuilder()
        .fromEntity(SessionDocumentSlot.class, "slot")
        .column("slot.id")
        .where(eq(property(SessionDocumentSlot.document().fromAlias("slot")), value(document)))
        .where(notExists(new DQLSelectBuilder()
        .fromEntity(SessionMark.class, "m")
        .column("m.id")
        .where(eq(property(SessionMark.slot().id().fromAlias("m")), property("slot.id")))
        .buildQuery()));
        final Number count = emptySlotDQL.createCountStatement(new DQLExecutionContext(this.getSession())).uniqueResult();
        if (count != null && count.intValue() > 0) {
            throw new ApplicationException("Нельзя закрыть документ, так как не всем студентам в нем выставлены оценки.");
        }

        // проверим, что нет временных отметок
        if (ISharedBaseDao.instance.get().existsEntity(
                SessionSlotMarkState.class,
                SessionSlotMarkState.slot().document().s(), document,
                SessionSlotMarkState.value().code().s(), SessionMarkStateCatalogItemCodes.NOT_APPEAR_UNKNOWN
        )) {
            throw new ApplicationException("Нельзя закрыть документ, так как он содержит временные оценки. Укажите причины неявок студентов.");
        }

        document.setCloseDate(new Date());
        this.update(document);
    }

    @Override
    public void updateProjectTheme(final SessionDocumentSlot slot, String theme, final String comment)
    {
        final Session session = this.getSession();
        theme = StringUtils.trimToNull(theme);

        NamedSyncInTransactionCheckLocker.register(session, "session-theme-" + String.valueOf(slot.getDocument().getId()));

        SessionProjectTheme themeObject = this.get(SessionProjectTheme.class, SessionProjectTheme.slot().s(), slot);

        if (null == theme)
        {
            if (null != themeObject) {
                this.delete(themeObject);
            }
            return;
        }

        if (null == themeObject)
        {
            themeObject = new SessionProjectTheme();
            themeObject.setSlot(slot);
        }

        themeObject.setTheme(theme);

        themeObject.setComment(comment);

        this.getSession().saveOrUpdate(themeObject);
    }

	@Override
	public void updateProjectTheme(SessionDocumentSlot slot, String theme, String comment, PpsEntry supervisor)
	{
		final Session session = getSession();
		theme = StringUtils.trimToNull(theme);

		NamedSyncInTransactionCheckLocker.register(session, "session-theme-" + String.valueOf(slot.getDocument().getId()));

		SessionProjectTheme themeObject = get(SessionProjectTheme.class, SessionProjectTheme.slot(), slot);

		if (theme == null && supervisor == null)
		{
			if (null != themeObject)
				delete(themeObject);
			return;
		}

		if (null == themeObject)
		{
			themeObject = new SessionProjectTheme();
			themeObject.setSlot(slot);
		}

		themeObject.setTheme(theme);
		themeObject.setComment(comment);
		themeObject.setSupervisor(supervisor);

		session.saveOrUpdate(themeObject);
	}

	@Override
    public EppGradeScale getGradeScale(SessionDocument document, EppStudentWpeCAction slot)
    {
        EppRegistryElementPart discipline = document instanceof SessionBulletinDocument ? ((SessionBulletinDocument) document).getGroup().getActivityPart() : slot.getStudentWpe().getRegistryElementPart();
        return getGradeScale(discipline, slot.getActionType());
    }

    @Override
    public EppGradeScale getGradeScale(EppStudentWpeCAction slot)
    {
        return getGradeScale(slot.getStudentWpe().getRegistryElementPart(), slot.getActionType());
    }

    @Override
    public EppGradeScale getGradeScale(EppRegistryElementPart discipline, EppFControlActionType fca)
    {
        IEppRegElPartWrapper discWrapper = IEppRegistryDAO.instance.get().getRegistryElementPartDataMap(Collections.singletonList(discipline.getId())).get(discipline.getId());
        EppGradeScale scale = IUniBaseDao.instance.get().getByNaturalId(new EppGradeScaleGen.NaturalId(discWrapper.getGradeScale(fca.getFullCode())));
        if (null == scale)
            throw new ApplicationException("Работа с документом невозможна: в реестре для части дисциплины «"+discipline.getTitleWithNumber()+"» отсутствует форма контроля «"+fca.getTitle()+"», предусмотренная документом.");
        return scale;
    }



    @Override
    public boolean isIgnoreRealEduGroupCompleteLevel() {
        IDataSettings settings = DataSettingsFacade.getSettings(UnisessionDefines.UNISESSION_SETTINGS);
        return Boolean.TRUE.equals(settings.get(UnisessionDefines.SETTINGS_FORM_ANY_STATES)) ;
    }

    @Override
    public void doSetIgnoreRealEduGroupCompleteLevel(boolean flag) {
        IDataSettings settings = DataSettingsFacade.getSettings(UnisessionDefines.UNISESSION_SETTINGS);
        settings.set(UnisessionDefines.SETTINGS_FORM_ANY_STATES, flag);
        DataSettingsFacade.saveSettings(settings);
    }

    @Override
    public DQLSelectBuilder fcaTypesRequiresThemeForSlots(String slotAlias)
    {
		final String fcaTypeAlias = "fcaType";
        return new DQLSelectBuilder().fromEntity(EppFControlActionType.class, fcaTypeAlias)
                .where(eq(property(fcaTypeAlias, EppFControlActionType.group().themeRequired()), value(Boolean.TRUE)))
                .where(eq(property(fcaTypeAlias, EppFControlActionType.eppGroupType()), property(slotAlias, SessionDocumentSlot.studentWpeCAction().type())));
    }

    @Override
	public boolean isThemeRequired(SessionDocument document)
	{
		final String slotAlias = "slot";
		Number count = new DQLSelectBuilder().fromEntity(SessionDocumentSlot.class, slotAlias)
				.where(eq(property(slotAlias, SessionDocumentSlot.document()), value(document)))
				.where(exists(fcaTypesRequiresThemeForSlots(slotAlias).buildQuery()))
				.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult();
		return count.intValue() > 0;
	}

    @Override
    public String themeByLastWpeThemes(EppStudentWpeCAction wpeCAction)
    {
        List<SessionProjectTheme> existingThemes = getList(SessionProjectTheme.class, SessionProjectTheme.slot().studentWpeCAction(), wpeCAction);
        if (existingThemes.isEmpty())
            return null;
        return Collections.max(existingThemes, Comparator.comparing((SessionProjectTheme theme) -> theme.getSlot().getDocument().getFormingDate())).getTheme();
    }
}
