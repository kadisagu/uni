/* $Id: SessionReportResultsPubUI.java 21825 2012-02-09 09:08:57Z oleyba $ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.ResultsByDiscPub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.entity.report.UnisessionResultsByDiscReport;

/**
 * @author oleyba
 * @since 2/6/12
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "report.id")
})
public class SessionReportResultsByDiscPubUI extends UIPresenter
{
    private UnisessionResultsByDiscReport _report = new UnisessionResultsByDiscReport();

    @Override
    public void onComponentRefresh()
    {
        setReport(DataAccessServices.dao().getNotNull(UnisessionResultsByDiscReport.class, getReport().getId()));
    }

    public void onClickDelete()
    {
        DataAccessServices.dao().delete(_report.getId());
        deactivate();
    }

    public void onClickPrint()
    {
        getActivationBuilder().asDesktopRoot(IUniComponents.DOWNLOAD_STORABLE_REPORT).parameter("reportId", _report.getId()).parameter("extension", "rtf").parameter("zip", Boolean.FALSE).activate();
    }

    @Override
    public ISecured getSecuredObject()
    {
        return null == getReport().getOrgUnit() ? super.getSecuredObject() : getReport().getOrgUnit();
    }

    public String getViewPermissionKey()
    {
        return null == getReport().getOrgUnit() ? "sessionResultsByDiscReport" : new OrgUnitSecModel(getReport().getOrgUnit()).getPermission("orgUnit_viewSessionReportResultsByDiscList");
    }

    // Getters & Setters


    public UnisessionResultsByDiscReport getReport()
    {
        return _report;
    }

    public void setReport(UnisessionResultsByDiscReport report)
    {
        _report = report;
    }

    public CommonPostfixPermissionModelBase getSec()
    {
        return new CommonPostfixPermissionModel(getReport().getOrgUnit() != null ? OrgUnitSecModel.getPostfix(getReport().getOrgUnit()) : null);

    }

    public String getDeleteStorableReportPermissionKey(){
        return getSec().getPermission(getReport().getOrgUnit() != null ? "orgUnit_deleteSessionReportResultsByDiscList" : "deleteSessionStorableReport");
    }

}
