/**
 *$Id$
 */
package ru.tandemservice.unisession.base.bo.SessionIndicators.ui.StudentsWithoutSessionSlots;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.column.ColumnBase;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.NewLineFormatter;
import org.tandemframework.core.view.list.column.IMergeRowIdResolver;
import ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList.AbstractUniStudentList;
import ru.tandemservice.unisession.base.bo.SessionIndicators.logic.SessionIndicatorsStudentsWithoutSessionSlotsSearchDSHandler;

import java.util.List;

/**
 * @author Alexander Shaburov
 * @since 20.03.13
 */
@Configuration
public class SessionIndicatorsStudentsWithoutSessionSlots extends AbstractUniStudentList
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return  super.presenterExtPoint(
                presenterExtPointBuilder()
                        .addDataSource(searchListDS(STUDENT_SEARCH_LIST_DS, studentSearchListDSColumnExtPoint(), studentSearchListDSHandler())));
    }

    @Override
    protected List<ColumnBase> customizeStudentSearchListColumns(List<ColumnBase> columnList)
    {
        removeColumns(columnList,
                CITIZENSHIP_COLUMN,
                PERSONAL_NUMBER_COLUMN,
                PERSONAL_FILE_NUMBER_COLUMN,
                FORMATIVE_ORG_UNIT_COLUMN,
                TERRITORIAL_ORG_UNIT_COLUMN,
                PRODUCTIVE_ORG_UNIT_COLUMN,
                SPECIALITY_COLUMN,
                SPECIALIZATION_COLUMN,
                QUALIFICATION_COLUMN,
                ORIENTATION_COLUMN,
                DEVELOP_FORM_COLUMN,
                DEVELOP_CONDITION_COLUMN,
                DEVELOP_PERIOD_COLUMN);

        insertColumns(columnList,
                textColumn(SessionIndicatorsStudentsWithoutSessionSlotsSearchDSHandler.VIEW_PROP_BROKEN_SLOT_LIST, SessionIndicatorsStudentsWithoutSessionSlotsSearchDSHandler.VIEW_PROP_BROKEN_SLOT_LIST)
                        .formatter(NewLineFormatter.SIMPLE)
                        .required(true)
                        .width("80")
                        .after(SPECIAL_PURPOSE_RECRUIT_COLUMN)
                        .create());

        return columnList;
    }

    @Bean
    public ColumnListExtPoint studentSearchListDSColumnExtPoint()
    {
        return createStudentSearchListColumnsBuilder()
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> studentSearchListDSHandler()
    {
        return new SessionIndicatorsStudentsWithoutSessionSlotsSearchDSHandler(getName());
    }

    @Override
    protected IMergeRowIdResolver newMergeRowIdResolver()
    {
        return entity -> String.valueOf(entity.getProperty("person.id"));
    }
}
