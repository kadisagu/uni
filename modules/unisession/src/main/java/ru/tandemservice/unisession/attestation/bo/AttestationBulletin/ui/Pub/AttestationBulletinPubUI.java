/* $Id:$ */
package ru.tandemservice.unisession.attestation.bo.AttestationBulletin.ui.Pub;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.unisession.attestation.bo.AttestationBulletin.AttestationBulletinManager;
import ru.tandemservice.unisession.attestation.bo.AttestationBulletin.ui.AddStudent.AttestationBulletinAddStudent;
import ru.tandemservice.unisession.attestation.bo.AttestationBulletin.ui.Edit.AttestationBulletinEdit;
import ru.tandemservice.unisession.attestation.bo.AttestationBulletin.ui.ManualMark.AttestationBulletinManualMark;
import ru.tandemservice.unisession.attestation.entity.SessionAttestation;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlot;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 10/9/12
 */
@State({
    @Bind(key= UIPresenter.PUBLISHER_ID, binding="bulletin.id"),
    @Bind(key= "selectedTabId", binding="selectedTabId")
})
public class AttestationBulletinPubUI extends UIPresenter
{
    private SessionAttestationBulletin bulletin = new SessionAttestationBulletin();
    private String _selectedTabId;

    private Group currentGroup;

    private String tutors;
    private boolean useRating;

    @Override
    public void onComponentRefresh()
    {
        setBulletin(IUniBaseDao.instance.get().get(SessionAttestationBulletin.class, getBulletin().getId()));
        setTutors(UniStringUtils.getAndJoin(SessionComissionPps.class, SessionComissionPps.commission(), getBulletin().getCommission(), SessionComissionPps.pps().person().identityCard().fullFio(), ", "));
        setUseRating(ISessionBrsDao.instance.get().isUseCurrentRating(getBulletin()));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(SessionAttestationBulletinStudentsDSHandler.PARAM_BULLETIN, getBulletin());
    }

    public void onClickAddStudent()
    {
        getActivationBuilder()
                .asRegionDialog(AttestationBulletinAddStudent.class)
                .parameter(UIPresenter.PUBLISHER_ID, bulletin.getId())
                .activate();
    }

    public void onClickEdit()
    {
        getActivationBuilder()
                .asRegionDialog(AttestationBulletinEdit.class)
                .parameter(UIPresenter.PUBLISHER_ID, bulletin.getId())
                .activate();
    }

    public void onClickDeleteSessionAttSlot()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public void onClickClose()
    {
        AttestationBulletinManager.instance().dao().doCloseBulletin(getBulletin());
    }

    public void onClickReopen()
    {
        AttestationBulletinManager.instance().dao().doReopenBulletin(getBulletin());
    }

    public void onClickDelete()
    {
        IUniBaseDao.instance.get().delete(getBulletin());
        deactivate();
    }

    public void onClickManualMark()
    {
        getActivationBuilder()
                .asRegion(AttestationBulletinManualMark.class)
//                .parameter()
                .activate();
    }

    public void onClickAutoMark()
    {
        ISessionBrsDao.instance.get().doFillAttestationMarkBasedOnCurrentRating(getBulletin());
    }

    public boolean isAutoMarkEnabled()
    {
        return ISessionBrsDao.instance.get().isUseCurrentRating(getBulletin());
    }

    public boolean isMarkEnabled()
    {
        return !bulletin.isClosed() && tutors != null && !tutors.isEmpty();
    }

    public void onClickPrint()
    {
        if (!getBulletin().isClosed()) {
            Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(AttestationBulletinManager.instance().printDao().printBulletin(getBulletin().getId()), "Ведомость.rtf");
            getActivationBuilder()
                .asDesktopRoot(IUniComponents.PRINT_REPORT)
                .parameters(new ParametersMap().add("id", id).add("zip", Boolean.FALSE).add("extension", "rtf"))
                .activate();
        } else {
            AttestationBulletinManager.instance().dao().doPrintClosedBulletin(getBulletin());
        }
    }

    public String getEppRealEduGroupTitles()
    {
        final List<DataWrapper> recordList = getConfig().getDataSource("studentDS").getRecords();
        final Set<String> groupTitleSet = new HashSet<>();
        for (DataWrapper dataWrapper : recordList)
        {
            final SessionAttestationSlot slot = dataWrapper.getWrapped();
            if (slot.getStudentWpe().getStudent().getGroup() != null)
                groupTitleSet.add(slot.getStudentWpe().getStudent().getGroup().getTitle());
        }

        return StringUtils.join(groupTitleSet, ", ");
    }

    public boolean isGroupVisible()
    {
        if (getBulletin() == null)
            return false;

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(SessionAttestationSlot.class, "s").column(property("s"))
                .where(eq(property(SessionAttestationSlot.bulletin().fromAlias("s")), value(getBulletin())));

        final List<SessionAttestationSlot> recordList = DataAccessServices.dao().getList(dql);
        final Set<String> groupTitleSet = new HashSet<>();
        for (SessionAttestationSlot slot : recordList)
        {
            if (slot.getStudentWpe().getStudent().getGroup() != null)
                groupTitleSet.add(slot.getStudentWpe().getStudent().getGroup().getTitle());
            else
                groupTitleSet.add("");
        }

        return groupTitleSet.size() > 1;
    }

    public SessionAttestation getAttestation()
    {
        return getBulletin().getAttestation();
    }

    public SessionAttestationBulletin getBulletin()
    {
        return bulletin;
    }

    public void setBulletin(SessionAttestationBulletin bulletin)
    {
        this.bulletin = bulletin;
    }

    public String getSelectedTabId()
    {
        return _selectedTabId;
    }

    public void setSelectedTabId(String selectedTabId)
    {
        _selectedTabId = selectedTabId;
    }

    public String getTutors()
    {
        return tutors;
    }

    public void setTutors(String tutors)
    {
        this.tutors = tutors;
    }

    public boolean isUseRating()
    {
        return useRating;
    }

    public void setUseRating(boolean useRating)
    {
        this.useRating = useRating;
    }

    public Collection<Group> getStudentGroupList()
    {
        final List<SessionAttestationSlot> slotList = DataAccessServices.dao().getList(SessionAttestationSlot.class, SessionAttestationSlot.bulletin(), bulletin);
        final List<Group> propertiesList = CommonBaseUtil.getPropertiesList(slotList, SessionAttestationSlot.studentWpe().student().group());
        Set<Group> groupSet = new TreeSet<>(new Comparator<Group>() {
            @Override
            public int compare(Group o1, Group o2) {
                if (o1 == o2) return 0;
                if (null == o1) return 1;
                if (null == o2) return -1;
                int res = o1.getTitle().compareTo(o2.getTitle());
                if (res != 0) return res;
                return o1.getId().compareTo(o2.getId());
            }
        });
        groupSet.addAll(propertiesList);
        groupSet.remove(null);
        return groupSet;
    }

    public Group getCurrentGroup() {
        return currentGroup;
    }

    public void setCurrentGroup(Group currentGroup) {
        this.currentGroup = currentGroup;
    }
}
