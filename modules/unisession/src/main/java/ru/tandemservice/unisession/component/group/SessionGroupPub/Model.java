package ru.tandemservice.unisession.component.group.SessionGroupPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;

import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.uni.entity.orgstruct.Group;

@Input( { @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "groupHolder.id") })
public class Model
{
    private final EntityHolder<Group> groupHolder = new EntityHolder<Group>();
    public EntityHolder<Group> getGroupHolder() { return this.groupHolder; }
    public Group getGroup() { return this.getGroupHolder().getValue(); }

}
