// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisession.component.student.ControlActionMarkHistory;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;

/**
 * @author oleyba
 * @since 2/9/11
 */
public class Controller extends AbstractBusinessController<IDAO, Model> implements IControlActionMarkHistoryHandler
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);

        model.setDialogMode(!this.isNotInDialog());
        this.getDao().prepare(model);
    }


    @Override
    public void refresh(final Long controlActionId, final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        final EppStudentWpeCAction controlAction = new EppStudentWpeCAction();
        controlAction.setId(controlActionId);
        model.setControlAction(controlAction);
        this.getDao().prepare(model);
    }

}