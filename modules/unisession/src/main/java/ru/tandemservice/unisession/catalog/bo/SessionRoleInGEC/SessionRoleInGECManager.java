/* $Id$ */
package ru.tandemservice.unisession.catalog.bo.SessionRoleInGEC;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

/**
 * @author Andrey Andreev
 * @since 12.09.2016
 */
@Configuration
public class SessionRoleInGECManager extends BusinessObjectManager
{
    public static SessionRoleInGECManager instance()
    {
        return instance(SessionRoleInGECManager.class);
    }
}