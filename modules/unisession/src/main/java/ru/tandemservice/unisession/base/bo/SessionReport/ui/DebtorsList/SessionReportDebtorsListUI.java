/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.DebtorsList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.DebtorsAdd.SessionReportDebtorsAdd;
import ru.tandemservice.unisession.entity.document.SessionObject;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 1/24/12
 */
@State
({
    @Bind(key = SessionReportManager.BIND_ORG_UNIT, binding = "ouHolder.id", required = true)
})
public class SessionReportDebtorsListUI extends UIPresenter
{
    private OrgUnitHolder ouHolder = new OrgUnitHolder();

    // actions

    @Override
    public void onComponentRefresh()
    {
        getOuHolder().refresh();
    }

    public void onSearchParamsChange()
    {
        onComponentRefresh();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(SessionReportManager.PARAM_ORG_UNIT, getOrgUnit());

        final Object year = getSettings().get("year");
        final Object part = getSettings().get("part");

        dataSource.put(SessionReportManager.PARAM_EDU_YEAR, year);

        if (null == year || null == part)
            return;

        DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(SessionObject.class, "obj").column("obj")
            .where(eq(property(SessionObject.orgUnit().fromAlias("obj")), value(getOrgUnit())))
            .where(eq(property(SessionObject.yearDistributionPart().fromAlias("obj")), commonValue(part)))
            .where(eq(property(SessionObject.educationYear().fromAlias("obj")), commonValue(year)))
            .order(property(SessionObject.id().fromAlias("obj")));
        final List<Object> list = dql.createStatement(_uiSupport.getSession()).list();

        if (list.isEmpty())
            return;
        if (list.size() != 1)
            throw new IllegalStateException();

        dataSource.put(SessionReportDebtorsDSHandler.PARAM_SESSION_OBJECT, list.get(0));
    }

    public void onClickAddReport()
    {
        getActivationBuilder().asRegion(SessionReportDebtorsAdd.class).parameter(SessionReportManager.BIND_ORG_UNIT, getOrgUnit().getId()).activate();
    }

    public void onClickPrint()
    {
        getActivationBuilder().asDesktopRoot(IUniComponents.DOWNLOAD_STORABLE_REPORT).parameter("reportId", getListenerParameterAsLong()).parameter("extension", "rtf").parameter("zip", Boolean.FALSE).activate();
    }

    public void onDeleteEntityFromList()
    {
        IUniBaseDao.instance.get().delete(getListenerParameterAsLong());
    }

    // preseneter

    public OrgUnit getOrgUnit()
    {
        return getOuHolder().getValue();
    }

    public String getViewPermissionKey(){ return getSec().getPermission("orgUnit_viewSessionReportDebtorsList"); }

    public String getAddStorableReportPermissionKey(){ return getSec().getPermission("orgUnit_addSessionReportDebtorsList"); }

    public String getDeleteStorableReportPermissionKey(){ return getSec().getPermission("orgUnit_deleteSessionReportDebtorsList"); }


    // getters and setters

    public OrgUnitHolder getOuHolder()
    {
        return ouHolder;
    }

    public CommonPostfixPermissionModelBase getSec()
    {
        return getOuHolder().getSecModel();
    }
}