package ru.tandemservice.unisession;

/**
 * Created with IntelliJ IDEA.
 * User: amakarova
 * Date: 15.11.12
 * Time: 17:55
 * To change this template use File | Settings | File Templates.
 */
public interface UnisessionDefines {

    String UNISESSION_SETTINGS = "unisessionSettings";
    String SETTINGS_FORM_ANY_STATES = "isFormAnyStates";        // Формировать ведомости на УГС в любых состояниях (степенях готовности)

    String SETTINGS_MARK_NEGATIVE_ITEM = "markNegative";        // Включать неудовлетворительные оценки
    String SETTINGS_MARK_RETAKE_ITEM = "markRetake";            // Включать требующие пересдачи отметки
    String SETTINGS_MARK_NOT_RETAKE_ITEM = "markNotRetake";     // Включать не требующие пересдачи отметки

    String STUDENT_REGION_NAME = "unisessionStudent";
}
