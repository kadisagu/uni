/* $Id$ */
package ru.tandemservice.unisession.component.sessionRetakeDoc.SessionRetakeDocEdit;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.uni.base.bo.PpsEntry.util.PpsEntrySelectBlockData;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unisession.dao.comission.ISessionCommissionDAO;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionRetakeDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 6/15/11
 */
public class DAO extends UniBaseDao implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setDoc(this.getNotNull(SessionRetakeDocument.class, model.getDoc().getId()));
        this.checkEditAllowed(model);
        List<PpsEntry> ppsList = CommonBaseUtil.getPropertiesList(this.getList(SessionComissionPps.class, SessionComissionPps.commission().s(), model.getDoc().getCommission()), SessionComissionPps.pps().s());
        model.setPpsData(new PpsEntrySelectBlockData(model.getDoc().getRegistryElementPart().getTutorOu().getId()).setMultiSelect(true).setInitiallySelectedPps(ppsList));
    }

    @Override
    public void update(final Model model)
    {
        this.checkEditAllowed(model);
        model.getDoc().setCommission(ISessionCommissionDAO.instance.get().saveOrUpdateCommission(model.getDoc().getCommission(), model.getPpsList()));
        this.save(model.getDoc());
    }

    protected void checkEditAllowed(final Model model)
    {
        // проверим, что все в порядке с ведомостью и ее можно редактировать
        // прямыми запросами, потому что состояние объектов в сессии могло устареть
        checkBulletinClosed(model);
        checkBulletinHasMark(model);

    }

    protected void checkBulletinClosed (final Model model) {
        DQLSelectBuilder check = new DQLSelectBuilder()
                .fromEntity(SessionRetakeDocument.class, "b")
                .where(eq(property(SessionRetakeDocument.id().fromAlias("b")), value(model.getDoc().getId())))
                .where(isNotNull(property(SessionRetakeDocument.closeDate().fromAlias("b"))));
                Number checkResult = check.createCountStatement(new DQLExecutionContext(this.getSession())).<Number>uniqueResult();
                if (checkResult != null && checkResult.intValue() > 0) {
                    throw new ApplicationException("Ведомость закрыта, редактирование невозможно.");
                }
    }

    protected void checkBulletinHasMark (Model model) {
        DQLSelectBuilder check = new DQLSelectBuilder()
                .fromEntity(SessionMark.class, "m")
                .where(eq(property(SessionMark.slot().document().id().fromAlias("m")), value(model.getDoc().getId())));
                Number checkResult = check.createCountStatement(new DQLExecutionContext(this.getSession())).<Number>uniqueResult();
                if (checkResult != null && checkResult.intValue() > 0) {
                    throw new ApplicationException("В ведомости уже есть оценки, редактирование невозможно.");
                }
    }
}
