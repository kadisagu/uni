/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.ResultsAdd;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.report.UnisessionResultsReport;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author oleyba
 * @since 2/7/12
 */
public interface ISessionReportResultsDAO extends INeedPersistenceSupport
{
    SpringBeanCache<ISessionReportResultsDAO> instance = new SpringBeanCache<>(ISessionReportResultsDAO.class.getName());

    interface ISessionResultsReportTable
    {
        void modify(RtfDocument rtf);
        String[][] getTableData(List<ISessionResultsReportColumn> columnList);
    }

    interface ISessionResultsReportColumn
    {
        String cell(ISessionResultsReportRow row);
    }

    interface ISessionResultsReportRow extends ITitled, Comparable
    {
        Collection<ISessionResultsReportStudentData> getStudents();
    }

    interface ISessionResultsReportStudentData
    {
        Long getStudentId();
        Group getGroup();
        Course getCourse();
        EducationLevelsHighSchool getLevel();

        Map<EppGradeScale, Integer> getScaleControlActionCountMap();
        Map<SessionMarkCatalogItem, Integer> getMarkCountMap();
        Map<SessionMarkCatalogItem, Integer> getInSessionMarkCountMap();

        int getControlActionCount();
        int getMarkCount();

        boolean isAllowedForSession();
        boolean isHaveToPassExams();
    }

    interface ISessionReportResultsGrouping extends ITitled, Comparable
    {
        String getKey();
        List<ISessionResultsReportTable> createTableList(SessionReportResultsAddUI model, Collection<ISessionResultsReportStudentData> studentData);
        boolean showEduLevelTitleOption();
    }

    UnisessionResultsReport createStoredReport(SessionReportResultsAddUI model, ISessionReportResultsGrouping grp) throws Exception;
}