package ru.tandemservice.unisession.base.bo.StudentPortfolio.ui.AchievementAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author avedernikov
 * @since 26.11.2015
 */

@Configuration
public class StudentPortfolioAchievementAddEdit extends BusinessComponentManager
{
	public static final String WPE_C_ACTION_DS = "wpeCActionDS";

	@Bean
	@Override
	public PresenterExtPoint presenterExtPoint()
	{
		return this.presenterExtPointBuilder()
				.addDataSource(selectDS(WPE_C_ACTION_DS, wpeCActionDSHandler()).addColumn(EppStudentWpeCAction.registryElementTitle().s()))
				.create();
	}

	@Bean
	public IDefaultComboDataSourceHandler wpeCActionDSHandler()
	{
		final String typeAlias = "cActionType";
		return new EntityComboDataSourceHandler(getName(), EppStudentWpeCAction.class)
				.customize((final String alias, final DQLSelectBuilder dql, final ExecutionContext context, final String filter) ->
				{
					final String partAlias = "part";
					Student student = context.getNotNull("student");
					dql
							.joinPath(DQLJoinType.inner, EppStudentWpeCAction.studentWpe().registryElementPart().fromAlias(alias), partAlias)
							.joinEntity(alias, DQLJoinType.inner, EppFControlActionType.class, typeAlias,
									eq(property(typeAlias, EppFControlActionType.eppGroupType()), property(alias, EppStudentWpeCAction.type())))
							.where(eq(property(alias, EppStudentWpeCAction.studentWpe().student()), value(student)));
					FilterUtils.applyLikeFilter(dql, filter, EppRegistryElementPart.registryElement().title().fromAlias(partAlias), EppRegistryElementPart.registryElement().number().fromAlias(partAlias));
					return dql;
				})
				.order(EppStudentWpeCAction.studentWpe().registryElementPart().registryElement().title())
				.order(EppStudentWpeCAction.studentWpe().registryElementPart().number())
				.order(typeAlias, EppFControlActionType.priority(), OrderDirection.asc);
	}
}