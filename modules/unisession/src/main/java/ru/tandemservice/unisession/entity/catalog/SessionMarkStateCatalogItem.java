package ru.tandemservice.unisession.entity.catalog;

import com.google.common.collect.ImmutableList;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.*;
import ru.tandemservice.unisession.entity.catalog.codes.SessionMarkStateCatalogItemCodes;
import ru.tandemservice.unisession.entity.catalog.gen.SessionMarkStateCatalogItemGen;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * Отметка
 */
public class SessionMarkStateCatalogItem extends SessionMarkStateCatalogItemGen implements IDynamicCatalogItem, IPrioritizedCatalogItem
{
    public static final String CATALOG_CODE = SessionMarkStateCatalogItem.ENTITY_NAME;

    @Override
    public String getCatalogCode()
    {
	    return SessionMarkStateCatalogItem.CATALOG_CODE;
    }

    @Override
    public boolean isCachedPositiveStatus()
    {
	    return !this.isRemarkable();
    }

	private static final List<String> HIDDEN_PROPERTIES = ImmutableList.of(P_CACHED_POSITIVE_STATUS, P_VALID, P_REMARKABLE, P_VISIBLE, P_CODE);

	public static IDynamicCatalogDesc getUiDesc()
	{
		return new BaseDynamicCatalogDesc()
		{
			@Override
			public Collection<String> getHiddenFields()
			{
				return HIDDEN_PROPERTIES;
			}

			@Override
			public Set<Long> getEditDisabledItems(Set<Long> itemIds)
			{
				return super.getEditDisabledItems(itemIds);
			}

			@Override
			public boolean isActiveToggleEnabled(ICatalogItem item)
			{
				return super.isActiveToggleEnabled(item);
			}

			@Override
			public String getAddEditComponentName()
			{
				return super.getAddEditComponentName();
			}

			@Override
			public String getAdditionalPropertiesAddEditPageName()
			{
				return super.getAdditionalPropertiesAddEditPageName();
			}

			@Override
			public void addAdditionalColumns(DynamicListDataSource<ICatalogItem> dataSource)
			{
				dataSource.addColumn(createToggleColumn("Уважительная неявка", ToggleMarkStatePropActionType.VALID));
				dataSource.addColumn(createToggleColumn("Требует пересдачи", ToggleMarkStatePropActionType.REMARKABLE));
				dataSource.addColumn(createToggleColumn("Используется", ToggleMarkStatePropActionType.VISIBLE)
						.setDisableHandler(entity -> SessionMarkStateCatalogItemCodes.NO_ADMISSION.equals(((SessionMarkStateCatalogItem) entity).getCode())));
			}

			private AbstractColumn createToggleColumn(String caption, ToggleMarkStatePropActionType actionType)
			{
				return new ToggleColumn(caption, actionType.toggledProp).setListener(actionType.actionName);
			}
		};
	}

	public enum ToggleMarkStatePropActionType
	{
		VALID("onToggleValid", P_VALID), REMARKABLE("onToggleRemarkable", P_REMARKABLE), VISIBLE("onToggleVisible", P_VISIBLE);

		String actionName;
		String toggledProp;

		ToggleMarkStatePropActionType(String actionName, String toggledProp)
		{
			this.actionName = actionName;
			this.toggledProp = toggledProp;
		}
	};

	public static class ToggleMarkStatePropAction extends NamedUIAction
	{
		private final String toggledProp;

		public ToggleMarkStatePropAction(ToggleMarkStatePropActionType type)
		{
			super(type.actionName);
			toggledProp = type.toggledProp;
		}

		@Override
		public void execute(final IUIPresenter presenter)
		{
			ICommonDAO dao = DataAccessServices.dao();
			SessionMarkStateCatalogItem markState = dao.get(SessionMarkStateCatalogItem.class, presenter.getListenerParameterAsLong());
			boolean propValue = (boolean)markState.getProperty(toggledProp);
			markState.setProperty(toggledProp, !propValue);
			dao.save(markState);
			presenter.getSupport().doRefresh();
		}
	}
}