/* $Id:$ */
package ru.tandemservice.unisession.base.bo.SessionReport.util;

import org.tandemframework.core.common.ITitled;

/**
* @author oleyba
* @since 2/19/12
*/
public abstract class SessionReportResultsGrouping implements ITitled, Comparable
{
    private int num;
    private String key;
    private String title;

    public SessionReportResultsGrouping(int num, String key, String title)
    {
        this.num = num;
        this.key = key;
        this.title = title;
    }

    public String getKey() { return key; }
    @Override public String getTitle() { return title; }

    @Override
    public int compareTo(Object o)
    {
        if (o instanceof SessionReportResultsGrouping)
            return num - ((SessionReportResultsGrouping)o).num;
        return 0;
    }
}
