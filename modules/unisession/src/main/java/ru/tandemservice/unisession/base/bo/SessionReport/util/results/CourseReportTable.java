/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReport.util.results;

import com.google.common.collect.Collections2;
import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.ResultsAdd.ISessionReportResultsDAO;

import java.util.*;

/**
* @author oleyba
* @since 2/13/12
*/
public class CourseReportTable extends ReportTable
{
    @Override
    public void modify(RtfDocument rtf)
    {
        RtfInjectModifier modifier = new RtfInjectModifier();
        modifier.put("groupTitle", "Курс");
        modifier.put("criterion", "курсам");
        modifier.put("Title", "Курс");
        modifier.modify(rtf);
        SharedRtfUtil.removeParagraphsWithTagsRecursive(rtf, Arrays.asList("eduLevelType", "eduLevelTitle"), false, false);
    }

    public CourseReportTable(Collection<ISessionReportResultsDAO.ISessionResultsReportStudentData> students)
    {
        Set<Course> courses = new HashSet<>(Collections2.transform(students, ISessionReportResultsDAO.ISessionResultsReportStudentData::getCourse));
        for (final Course course : courses) {
            rowList.add(new ReportRow(course.getTitle(), CollectionUtils.select(students, input -> input.getCourse().equals(course))));
        }
        Collections.sort(rowList, ITitled.TITLED_COMPARATOR);
        rowList.add(totalsRow(students));
    }
}
