/* $Id$ */
package ru.tandemservice.unisession.component.student.StudentSessionMarkTab;

import ru.tandemservice.uni.dao.IListDataSourceDao;
import ru.tandemservice.uni.dao.IUpdateable;

/**
 * @author oleyba
 * @since 4/8/11
 */
public interface IDAO extends IUpdateable<Model>, IListDataSourceDao<Model>
{

}
