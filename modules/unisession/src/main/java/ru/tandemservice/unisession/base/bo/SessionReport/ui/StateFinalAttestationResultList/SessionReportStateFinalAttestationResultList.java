/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.StateFinalAttestationResultList;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.IColumnListExtPointBuilder;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.StateFinalExamResultPub.SessionReportStateFinalExamResultPub;
import ru.tandemservice.unisession.entity.report.StateFinalAttestationResult;

import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Andrey Andreev
 * @since 27.10.2016
 */
public abstract class SessionReportStateFinalAttestationResultList extends BusinessComponentManager
{
    public static final String DS_REPORTS = "stateFinalExamResultDS";
    public static final String EDUCATION_YEAR_PARAM = "educationYear";
    public static final String EDUCATION_ORG_UNIT_UTIL_PARAM = "educationOrgUnitUtil";
    public static final String ORG_UNIT_ID_PARAM = "orgUnitId";

    public IColumnListExtPointBuilder addDefaultResultsListColumns(IColumnListExtPointBuilder builder)
    {
        builder.addColumn(publisherColumn("formingDate", StateFinalAttestationResult.formingDate())
                                  .addParameter(SessionReportManager.BIND_ORG_UNIT, "ui:ouHolder.id")
                                  .formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).order().create())
                .addColumn(textColumn("educationYear", StateFinalAttestationResult.educationYear().title().s()).order().create())

                .addColumn(textColumn("formativeOrgUnit", StateFinalAttestationResult.formativeOrgUnit().title().s()).create())
                .addColumn(textColumn("territorialOrgUnit", StateFinalAttestationResult.territorialOrgUnit().title().s()).create())
                .addColumn(textColumn("ownerOrgUnit", StateFinalAttestationResult.ownerOrgUnit().title().s()).order().create())
                .addColumn(textColumn("educationLevelHighSchool", StateFinalAttestationResult.educationLevelHighSchool().displayableTitle().s()).order().create())
                .addColumn(textColumn("developForm", StateFinalAttestationResult.developForm().title().s()).create())
                .addColumn(textColumn("developCondition", StateFinalAttestationResult.developCondition().title().s()).create())
                .addColumn(textColumn("developTech", StateFinalAttestationResult.developTech().title().s()).create())

                .addColumn(textColumn("periodStateExams", "")
                                   .formatter(source -> ((StateFinalAttestationResult) source).getExamsPeriod())
                                   .create());

        customResultsListColumns(builder);

        builder.addColumn(actionColumn("print", CommonDefines.ICON_PRINT, "onClickPrint"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, "onClickDelete")
                                   .permissionKey("ui:deletePermissionKey")
                                   .alert(FormattedMessage.with().template("stateFinalExamResultDS.delete.alert")
                                                  .parameter(StateFinalAttestationResult.formingDate().s()).create())
                );

        return builder;
    }

    public void customResultsListColumns(IColumnListExtPointBuilder builder)
    {

    }

    public IReadAggregateHandler<DSInput, DSOutput> getSessionReportResultsDSHandler(String ownerId,Class<? extends StateFinalAttestationResult> reportClass)
    {
        return new DefaultSearchDataSourceHandler(ownerId)
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                String alias = "e";
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(reportClass, alias)
                        .column(property(alias));

                UniEduProgramEducationOrgUnitAddon util = context.get(EDUCATION_ORG_UNIT_UTIL_PARAM);
                if (util != null)
                    util.applyFilters(builder, alias);

                FilterUtils.applySelectFilter(builder, alias, StateFinalAttestationResult.educationYear(), context.get(EDUCATION_YEAR_PARAM));

                builder.joinPath(DQLJoinType.left, StateFinalAttestationResult.ownerOrgUnit().fromAlias(alias), "owou");

                OrderDescription orderByDate = new OrderDescription(StateFinalAttestationResult.formingDate());
                DQLOrderDescriptionRegistry orderRegistry = new DQLOrderDescriptionRegistry(reportClass, alias);
                orderRegistry
                        .setOrders(StateFinalAttestationResult.formingDate(), orderByDate)
                        .setOrders(StateFinalAttestationResult.educationYear().title(), new OrderDescription(StateFinalAttestationResult.educationYear().title()))
                        .setOrders(StateFinalAttestationResult.educationLevelHighSchool().displayableTitle(), new OrderDescription(StateFinalAttestationResult.educationLevelHighSchool().displayableTitle()))
                        .setOrders(StateFinalAttestationResult.ownerOrgUnit().title(), new OrderDescription("owou", OrgUnit.title()), orderByDate);

                orderRegistry.applyOrder(builder, input.getEntityOrder());

                return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build();
            }
        };
    }
}
