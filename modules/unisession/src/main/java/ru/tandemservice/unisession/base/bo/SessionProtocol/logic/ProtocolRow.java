/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionProtocol.logic;

import com.google.common.collect.ComparisonChain;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unisession.entity.catalog.SessionRoleInGEC;
import ru.tandemservice.unisession.entity.stateFinalExam.SessionFQWProtocol;
import ru.tandemservice.unisession.entity.stateFinalExam.SessionGECMember;
import ru.tandemservice.unisession.entity.stateFinalExam.SessionStateFinalExamProtocol;
import ru.tandemservice.unisession.entity.stateFinalExam.gen.SessionGECMemberGen;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Andrey Andreev
 * @since 15.09.2016
 */
public class ProtocolRow extends DataWrapper
{
    private SessionStateFinalExamProtocol _protocol;
    private Map<SessionRoleInGEC, List<SessionGECMember>> _memberListByRole = new TreeMap<>((o1, o2) -> Integer.compare(o1.getPriority(), o2.getPriority()));
    private List<SessionGECMember> _memberList = new ArrayList<>();
    private Map<SessionRoleInGEC, List<PpsEntry>> _ppsListByRole = new TreeMap<>((o1, o2) -> Integer.compare(o1.getPriority(), o2.getPriority()));

    public ProtocolRow(SessionStateFinalExamProtocol protocol, List<SessionGECMember> gecMembers)
    {
        super(protocol);
        _protocol = protocol;

        if (gecMembers != null)
        {
            gecMembers.sort((o1, o2) -> ComparisonChain.start()
                    .compare(o1.getRoleInGEC().getPriority(), o2.getRoleInGEC().getPriority())
                    .compare(o1.getPps().getFio(), o2.getPps().getFio())
                    .result()
            );

            _memberList = gecMembers;

            _memberListByRole = gecMembers.stream()
                    .collect(Collectors.groupingBy(SessionGECMemberGen::getRoleInGEC,
                                                   () -> new TreeMap<>((o1, o2) -> Integer.compare(o1.getPriority(), o2.getPriority())),
                                                   Collectors.mapping(member -> member, Collectors.toList())));
            _ppsListByRole = gecMembers.stream()
                    .collect(Collectors.groupingBy(SessionGECMemberGen::getRoleInGEC,
                                                   () -> new TreeMap<>((o1, o2) -> Integer.compare(o1.getPriority(), o2.getPriority())),
                                                   Collectors.mapping(SessionGECMemberGen::getPps, Collectors.toList())));
        }
    }

    public boolean isWithHonors()
    {
        return ((SessionFQWProtocol) _protocol).isWithHonors();
    }

    public SessionStateFinalExamProtocol getProtocol()
    {
        return _protocol;
    }

    public List<String> getMembersListString()
    {
        return _memberListByRole.entrySet().stream().map(entry -> entry.getKey().getTitle() + ": " + CommonBaseStringUtil.join(entry.getValue(), SessionGECMember.pps().fio().s(), ","))
                .collect(Collectors.toList());
    }

    public List<SessionGECMember> getMembersList()
    {
        return _memberList;
    }

    public List<PpsEntry> getMemberList4Role(SessionRoleInGEC role)
    {
        List<PpsEntry> memberList = _ppsListByRole == null ? null : _ppsListByRole.get(role);
        return memberList == null ? new ArrayList<>() : memberList;
    }

    public void setMemberListByRole(SessionRoleInGEC role, List<PpsEntry> members)
    {
        _ppsListByRole.put(role, members);
    }


    public Map<SessionRoleInGEC, List<SessionGECMember>> getExistMemberListByRole()
    {
        return _memberListByRole;
    }

    public Map<SessionRoleInGEC, List<PpsEntry>> getPpsListByRole()
    {
        return _ppsListByRole;
    }


    public void update(ICommonDAO dao)
    {
        if (_protocol instanceof SessionFQWProtocol)
            ((SessionFQWProtocol) _protocol).setWithHonors((Boolean) getProperty("withHonors"));

        boolean needNewCommission = false;
        for (SessionRoleInGEC role : dao.getList(SessionRoleInGEC.class))
        {
            List<SessionGECMember> memberList = _memberListByRole.get(role);
            List<PpsEntry> ppsList = _ppsListByRole.get(role);


            if ((memberList != null && ppsList == null) || (memberList == null && ppsList != null))
            {
                needNewCommission = true;
                break;
            }
            else if (memberList == null && ppsList == null)
                break;

            if (memberList.size() != ppsList.size())
                needNewCommission = true;

            for (SessionGECMember member : memberList)
                if (!ppsList.contains(member.getPps()))
                {
                    needNewCommission = true;
                    break;
                }

            if (needNewCommission)
                break;
        }

        if (needNewCommission)
            _protocol.setCommission(Utils.createCommission(_ppsListByRole));

        dao.saveOrUpdate(_protocol);
    }
}