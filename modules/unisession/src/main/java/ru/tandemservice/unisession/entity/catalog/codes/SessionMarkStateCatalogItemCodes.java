package ru.tandemservice.unisession.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Отметка"
 * Имя сущности : sessionMarkStateCatalogItem
 * Файл data.xml : session.data.xml
 */
public interface SessionMarkStateCatalogItemCodes
{
    /** Константа кода (code) элемента : Не явился (уваж.) (title) */
    String NOT_APPEAR_VALID = "state.1";
    /** Константа кода (code) элемента : Не явился (неуваж.) (title) */
    String NOT_APPEAR_INVALID = "state.2";
    /** Константа кода (code) элемента : Не допущен (title) */
    String NO_ADMISSION = "state.3";
    /** Константа кода (code) элемента : Не должен сдавать (title) */
    String UNNECESSARY = "state.4";
    /** Константа кода (code) элемента : Выбыл (title) */
    String EXPELLED = "state.6";
    /** Константа кода (code) элемента : Неявка по неизвестной причине (title) */
    String NOT_APPEAR_UNKNOWN = "state.7";

    Set<String> CODES = ImmutableSet.of(NOT_APPEAR_VALID, NOT_APPEAR_INVALID, NO_ADMISSION, UNNECESSARY, EXPELLED, NOT_APPEAR_UNKNOWN);
}
