/* $Id$ */
package ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinMark;

import org.apache.commons.lang.StringUtils;
import org.apache.tapestry.form.translator.Translator;
import org.apache.tapestry.form.validator.Max;
import org.apache.tapestry.form.validator.Min;
import org.apache.tapestry.form.validator.Validator;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.translator.StrongNumberTranslator;
import ru.tandemservice.unisession.base.bo.SessionMark.logic.ISessionMarkDAO;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkStateCatalogItem;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author Nikolay Fedorovskih
 * @since 22.10.2014
 */
public class BulletinMarkRow
{
    private Model _model;
    private SessionDocumentSlot _slot;
    private String _fio;
    private String _groupTitle;
    private boolean _allowed;
    private String _currentRating;
    private ISelectModel _markSelectModel;
    private SessionMarkCatalogItem _mark;
    private String _finalMark;
    private Number _points;
    private Date _performDate;
    private List<Validator> _pointsValidators;
    private Translator _pointsTranslator;
    private boolean _inSession;
    private String _practiceTutorMark;

    public BulletinMarkRow(Model model, SessionDocumentSlot slot, ISelectModel markSelectModel)
    {
        _model = model;
        _slot = slot;
        _fio = slot.getActualStudent().getFio();
        _markSelectModel = markSelectModel;
        _inSession = slot.isInSession();

        ISessionBrsDao.ISessionPointsValidator sessionPointsValidator = null;
        if (model.isUseCurrentRating() || model.isUsePoints())
        {
            final ISessionBrsDao.ISessionRatingSettingsKey key = ISessionBrsDao.instance.get().key(slot);
            final ISessionBrsDao.ISessionRatingSettings settings = model.getRatingSettings().get(key);
            if (settings != null)
                sessionPointsValidator = settings.pointsValidator();
        }

        if (sessionPointsValidator != null)
        {
            _pointsTranslator = new StrongNumberTranslator(sessionPointsValidator.precision() <= 0 ? "pattern=#" : StringUtils.rightPad("pattern=#.", sessionPointsValidator.precision(), '#'));
            _pointsValidators = Arrays.<Validator>asList(
                    new Min("min=" + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(sessionPointsValidator.min())),
                    new Max("max=" + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(sessionPointsValidator.max()))
            );
        }
        else
        {
            _pointsTranslator = new StrongNumberTranslator();
            _pointsValidators = null;
        }
    }

    public void validate(ErrorCollector errorCollector)
    {
        if (getMark() != null && getPerformDate() == null)
            errorCollector.add("Поле \"Дата сдачи\" обязательно для заполнения, если выставлена оценка.", getPerformDateInputId());

        if (getPoints() != null && _model.isUseCurrentRating() && _model.isNeedToSaveCurrentRatingData())
            throw new ApplicationException("Текущий рейтинг заполнен не для всех студентов ведомости. Выставление оценок заблокировано.");
    }

    public List<Validator> getPointsValidators()
    {
        return _pointsValidators;
    }

    public Translator getPointsTranslator()
    {
        return _pointsTranslator;
    }

    public boolean isPointsDisabled()
    {
        return getMark() instanceof SessionMarkStateCatalogItem || !isAllowed();
    }

    public ISessionMarkDAO.MarkData calculateMarkData()
    {
        final Date performDate = getPerformDate() == null ? getModel().getDocPerformDate() : getPerformDate();

        if (getModel().isUsePoints() && getMark() == null && getPoints() != null)
            return ISessionBrsDao.instance.get().calculateMarkData(performDate, getSlot(), getPoints().doubleValue());

        return new ISessionMarkDAO.MarkData()
        {
            @Override public Date getPerformDate() { return performDate; }
            @Override public Double getPoints() { return getMark() != null && getModel().isUsePoints() ? .0 : null; }
            @Override public SessionMarkCatalogItem getMarkValue() { return getMark(); }
            @Override public String getComment() { return null; }
        };
    }

    public String getBookNumber()
    {
        return _slot.getActualStudent().getBookNumber();
    }

    public String getGroupTitle()
    {
        return _groupTitle;
    }

    public void setGroupTitle(String groupTitle)
    {
        _groupTitle = groupTitle;
    }

    public SessionDocumentSlot getSlot()
    {
        return _slot;
    }

    public boolean isAllowed()
    {
        return _allowed;
    }

    public void setAllowed(boolean allowed)
    {
        _allowed = allowed;
    }

    public String getCurrentRating()
    {
        return _currentRating;
    }

    public void setCurrentRating(String currentRating)
    {
        _currentRating = currentRating;
    }

    public String getPointsInputId()
    {
        return "points_" + getSlot().getId();
    }

    public String getPerformDateInputId()
    {
        return "performDate_" + getSlot().getId();
    }

    public String getFio()
    {
        return _fio;
    }

    public SessionMarkCatalogItem getMark()
    {
        return _mark;
    }

    public void setMark(SessionMarkCatalogItem mark)
    {
        _mark = mark;
    }

    public ISelectModel getMarkSelectModel()
    {
        return _markSelectModel;
    }

    public Date getPerformDate()
    {
        return _performDate;
    }

    public void setPerformDate(Date performDate)
    {
        _performDate = performDate;
    }

    public Number getPoints()
    {
        if (getMark() instanceof SessionMarkStateCatalogItem)
            return null;

        return _points;
    }

    public void setPoints(Number points)
    {
        _points = points;
    }

    public Model getModel()
    {
        return _model;
    }

    public String getFinalMark()
    {
        return _finalMark;
    }

    public void setFinalMark(String finalMark)
    {
        _finalMark = finalMark;
    }

    public boolean isInSession()
    {
        return _inSession;
    }

    public void setInSession(boolean inSession)
    {
        _inSession = inSession;
    }

    public String getPracticeTutorMark()
    {
        return _practiceTutorMark;
    }

    public void setPracticeTutorMark(String practiceTutorMark)
    {
        _practiceTutorMark = practiceTutorMark;
    }
}