/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionProtocol.ui.AbstractList;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import ru.tandemservice.unisession.base.bo.SessionProtocol.logic.ProtocolRow;
import ru.tandemservice.unisession.base.bo.SessionProtocol.logic.Utils;
import ru.tandemservice.unisession.base.bo.SessionProtocol.ui.MassAddEdit.SessionProtocolMassAddEdit;
import ru.tandemservice.unisession.entity.stateFinalExam.*;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Andrey Andreev
 * @since 12.09.2016
 */
public abstract class SessionProtocolAbstractListUI extends UIPresenter
{
    protected Long _bulletinId;

    protected String _currentRole;

    @Override
    public void onComponentRefresh()
    {
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(SessionProtocolAbstractList.BULLETIN_ID_PARAM, _bulletinId);
    }

    @Override
    public void onComponentBindReturnParameters(String childRegionName, Map<String, Object> returnedData)
    {
        if (UIDefines.DIALOG_REGION_NAME.equals(childRegionName)) {
            Collection<IEntity> selected = getSelectedProtocols();
            selected.clear();
        }
    }


    // Listeners
    public void onClickMassEditGEC()
    {
        Collection<IEntity> selectedProtocols = getSelectedProtocols();
        if (CollectionUtils.isEmpty(selectedProtocols))
            throw new ApplicationException("Невозможно редактировать протоколы. Необходимо выбрать хотя бы один протокол.");

        List<Long> ids = selectedProtocols.stream().map(IEntity::getId).collect(Collectors.toList());

        _uiActivation.asRegionDialog(SessionProtocolMassAddEdit.class)
                .parameters(ParametersMap
                                    .createWith(SessionProtocolMassAddEdit.BULLETIN_ID, _bulletinId)
                                    .add(SessionProtocolMassAddEdit.PROTOCOL_IDS, ids))
                .activate();
    }

    public void onClickMassEditProtocols()
    {
    }

    public void onClickMassDeleteProtocols()
    {
        Collection<IEntity> selectedProtocols = getSelectedProtocols();
        if (CollectionUtils.isEmpty(selectedProtocols))
            throw new ApplicationException("Невозможно удалить протоколы. Необходимо выбрать хотя бы один протокол.");

        ICommonDAO dao = DataAccessServices.dao();
        selectedProtocols.forEach(entity -> dao.delete(((ProtocolRow) entity).getProtocol()));
        Utils.deleteUselessCommissions();
    }

    public void onClickDelete()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public abstract void onClickPrint();

    public abstract void onClickMassPrintProtocols();


    // Getters and Setters
    public Long getBulletinId()
    {
        return _bulletinId;
    }

    public void setBulletinId(Long bulletinId)
    {
        this._bulletinId = bulletinId;
    }

    public String getCurrentRole()
    {
        return _currentRole;
    }

    public void setCurrentRole(String currentRole)
    {
        _currentRole = currentRole;
    }

    public List<String> getCurrentMemberList()
    {
        BaseSearchListDataSource dataSource = _uiConfig.getDataSource(SessionProtocolAbstractList.STATE_FINAL_EXAM_PROTOCOL_DS);
        ProtocolRow protocol = dataSource.getCurrent();

        return protocol.getMembersListString();
    }

    protected Collection<IEntity> getSelectedProtocols()
    {
        BaseSearchListDataSource dataSource = _uiConfig.getDataSource(SessionProtocolAbstractList.STATE_FINAL_EXAM_PROTOCOL_DS);
        return dataSource.getOptionColumnSelectedObjects(SessionProtocolAbstractList.SEARCH_LIST_COLUMN_SELECT);
    }

    public abstract String getMassDeletePermissionKey();

    public abstract String getMassEditGECPermissionKey();

    public abstract String getMassEditPermissionKey();

    public abstract String getMassPrintPermissionKey();

    public Boolean getIsEmpty()
    {
        return DataAccessServices.dao().getCount(SessionStateFinalExamProtocol.class, SessionStateFinalExamProtocol.documentSlot().document().id().s(), _bulletinId) == 0;
    }
}