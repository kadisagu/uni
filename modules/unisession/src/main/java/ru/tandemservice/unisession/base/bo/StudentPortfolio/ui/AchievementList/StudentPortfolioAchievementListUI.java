package ru.tandemservice.unisession.base.bo.StudentPortfolio.ui.AchievementList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.remoteDocument.dto.RemoteDocumentDTO;
import org.tandemframework.shared.commonbase.remoteDocument.service.ICommonRemoteDocumentService;
import org.tandemframework.shared.commonbase.remoteDocument.service.IRemoteDocumentProvider;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unisession.base.bo.StudentPortfolio.StudentPortfolioManager;
import ru.tandemservice.unisession.base.bo.StudentPortfolio.logic.IStudentPortfolioDAO;
import ru.tandemservice.unisession.base.bo.StudentPortfolio.ui.AchievementAddEdit.StudentPortfolioAchievementAddEdit;
import ru.tandemservice.unisession.base.bo.StudentPortfolio.ui.AchievementAddEdit.StudentPortfolioAchievementAddEditUI;
import ru.tandemservice.unisession.remoteDocument.ext.RemoteDocument.RemoteDocumentExtManager;
import ru.tandemservice.unisession.entity.catalog.StudentPortfolioElement;

/**
 * @author avedernikov
 * @since 26.11.2015
 */

@State({@Bind(key = UIPresenter.PUBLISHER_ID, binding = "student.id", required = true)})
public class StudentPortfolioAchievementListUI extends UIPresenter
{
	public static final String CONTAINING_WPE_FILTER = "containingWpeFilter";
	public static final String STUDENT_FILTER = "student";

	private Student student = new Student();

	@Override
	public void onComponentRefresh()
	{
		student = DataAccessServices.dao().getNotNull(getStudent().getId());
	}

	@Override
	public void onBeforeDataSourceFetch(IUIDataSource dataSource)
	{
		switch (dataSource.getName())
		{
			case StudentPortfolioAchievementList.ACHIEVEMENT_DS:
			{
				dataSource.put(CONTAINING_WPE_FILTER, getSettings().get("containingWpe"));
				dataSource.put(STUDENT_FILTER, student);
				break;
			}
		}
	}

	public void onClickAddAchievement()
	{
		_uiActivation.asRegionDialog(StudentPortfolioAchievementAddEdit.class)
				.parameter(StudentPortfolioAchievementAddEditUI.PARAM_STUDENT_ID, getStudent().getId())
				.activate();
	}

	public void onClickFormByWpe()
	{
		StudentPortfolioManager.instance().dao().addAchievementsByWpeCActions(getStudent());
	}

	public void onClickDownloadFile()
	{
		final RemoteDocumentDTO dto = StudentPortfolioManager.instance().dao().getAchievementAttachement(getListenerParameterAsLong());
		if (dto == null)
			throw new ApplicationException("Файл не найден в системе");

		final CommonBaseRenderer renderer = new CommonBaseRenderer().contentType(dto.getFileType()).fileName(dto.getFileName()).document(dto.getContent());
		BusinessComponentUtils.downloadDocument(renderer, true);
	}

	public void onEditEntityFromList()
	{
		_uiActivation.asRegion(StudentPortfolioAchievementAddEdit.class)
				.parameter(StudentPortfolioAchievementAddEditUI.PARAM_STUDENT_ID, getStudent().getId())
				.parameter(StudentPortfolioAchievementAddEditUI.PARAM_ACHIEVEMENT_ID, getListenerParameterAsLong())
				.top().activate();
	}

	public void onDeleteEntityFromList()
	{
		IStudentPortfolioDAO dao = StudentPortfolioManager.instance().dao();
		Long achievementId = getListenerParameterAsLong();
		dao.removeAttachementFromAchievement(achievementId);
		dao.delete(achievementId);
	}

	public Student getStudent()
	{
		return student;
	}
}
