/* $Id: $ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.SfeSummaryResultAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.activator.IRootComponentActivationBuilder;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.SfaSummaryResultAdd.SessionReportSfaSummaryResultAddUI;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.SfeSummaryResultPub.SessionReportSfeSummaryResultPub;
import ru.tandemservice.unisession.entity.report.SfeSummaryResult;

/**
 * @author Andrey Andreev
 * @since 27.10.2016
 */
@State({@Bind(key = SessionReportManager.BIND_ORG_UNIT, binding = "ouHolder.id")})
public class SessionReportSfeSummaryResultAddUI extends SessionReportSfaSummaryResultAddUI<SfeSummaryResult>
{


    @Override
    public SfeSummaryResult initReport()
    {
        return new SfeSummaryResult();
    }

    @Override
    public void createStoredReport(SfeSummaryResult report)
    {
        SessionReportManager.instance().sfeSummaryResultPrintDAO().createStoredReport(report);
    }

    @Override
    public void openReportPub(SfeSummaryResult report)
    {
        IRootComponentActivationBuilder builder = getActivationBuilder()
                .asDesktopRoot(SessionReportSfeSummaryResultPub.class)
                .parameter(UIPresenter.PUBLISHER_ID, report.getId());
        if (getOuHolder().getId() != null)
            builder.parameter(SessionReportManager.BIND_ORG_UNIT, getOuHolder().getId());
        builder.activate();
    }

    @Override
    public String getAddPermissionKey()
    {
        return getSec().getPermission(getOuHolder().getId() != null ? "orgUnit_addStateFinalExamSummaryResult" : "addSessionStorableReport");
    }
}
