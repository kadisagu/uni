/* $Id: $ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.SfeSummaryResultPub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.SfaSummaryResultPub.SessionReportSfaSummaryResultPubUI;
import ru.tandemservice.unisession.entity.report.SfeSummaryResult;

/**
 * @author Andrey Andreev
 * @since 27.10.2016
 */
@State({@Bind(key = UIPresenter.PUBLISHER_ID, binding = "report.id"),
        @Bind(key = SessionReportManager.BIND_ORG_UNIT, binding = "ouHolder.id")})
public class SessionReportSfeSummaryResultPubUI extends SessionReportSfaSummaryResultPubUI<SfeSummaryResult>
{
    @Override
    public SfeSummaryResult initReport()
    {
        return new SfeSummaryResult();
    }

    @Override
    public String getViewPermissionKey()
    {
        return getSec().getPermission(getOuHolder().getId() != null ? "orgUnit_viewStateFinalExamSummaryResultList" : "stateFinalExamSummaryResultReport");
    }

    @Override
    public String getDeletePermissionKey()
    {
        return getSec().getPermission(getOuHolder().getId() != null ? "orgUnit_deleteStateFinalExamSummaryResult" : "deleteSessionStorableReport");
    }
}
