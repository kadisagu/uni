package ru.tandemservice.unisession.entity.document.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unisession.entity.document.SessionTransferInsideOperation;
import ru.tandemservice.unisession.entity.document.SessionTransferOperation;
import ru.tandemservice.unisession.entity.mark.SessionSlotRegularMark;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Перезачтенное мероприятие внутри ОУ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionTransferInsideOperationGen extends SessionTransferOperation
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.document.SessionTransferInsideOperation";
    public static final String ENTITY_NAME = "sessionTransferInsideOperation";
    public static final int VERSION_HASH = -918972744;
    private static IEntityMeta ENTITY_META;

    public static final String L_SOURCE_MARK = "sourceMark";

    private SessionSlotRegularMark _sourceMark;     // Перезачитываемая оценка

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Перезачитываемая оценка. Свойство не может быть null.
     */
    @NotNull
    public SessionSlotRegularMark getSourceMark()
    {
        return _sourceMark;
    }

    /**
     * @param sourceMark Перезачитываемая оценка. Свойство не может быть null.
     */
    public void setSourceMark(SessionSlotRegularMark sourceMark)
    {
        dirty(_sourceMark, sourceMark);
        _sourceMark = sourceMark;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof SessionTransferInsideOperationGen)
        {
            setSourceMark(((SessionTransferInsideOperation)another).getSourceMark());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionTransferInsideOperationGen> extends SessionTransferOperation.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionTransferInsideOperation.class;
        }

        public T newInstance()
        {
            return (T) new SessionTransferInsideOperation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "sourceMark":
                    return obj.getSourceMark();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "sourceMark":
                    obj.setSourceMark((SessionSlotRegularMark) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "sourceMark":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "sourceMark":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "sourceMark":
                    return SessionSlotRegularMark.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionTransferInsideOperation> _dslPath = new Path<SessionTransferInsideOperation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionTransferInsideOperation");
    }
            

    /**
     * @return Перезачитываемая оценка. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionTransferInsideOperation#getSourceMark()
     */
    public static SessionSlotRegularMark.Path<SessionSlotRegularMark> sourceMark()
    {
        return _dslPath.sourceMark();
    }

    public static class Path<E extends SessionTransferInsideOperation> extends SessionTransferOperation.Path<E>
    {
        private SessionSlotRegularMark.Path<SessionSlotRegularMark> _sourceMark;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Перезачитываемая оценка. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionTransferInsideOperation#getSourceMark()
     */
        public SessionSlotRegularMark.Path<SessionSlotRegularMark> sourceMark()
        {
            if(_sourceMark == null )
                _sourceMark = new SessionSlotRegularMark.Path<SessionSlotRegularMark>(L_SOURCE_MARK, this);
            return _sourceMark;
        }

        public Class getEntityClass()
        {
            return SessionTransferInsideOperation.class;
        }

        public String getEntityName()
        {
            return "sessionTransferInsideOperation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
