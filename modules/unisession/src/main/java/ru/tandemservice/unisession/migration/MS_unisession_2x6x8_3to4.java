package ru.tandemservice.unisession.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unisession_2x6x8_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.15"),
                        new ScriptDependency("org.tandemframework.shared", "1.6.8")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность sessionCurrentMarkScale

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("session_c_cur_mark_scale_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("active_p", DBType.BOOLEAN).setNullable(false),
                                      new DBColumn("title_p", DBType.createVarchar(1200))
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("sessionCurrentMarkScale");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность sessionCurrentMarkCatalogItem

        // создана новая сущность
        Long passedItemId, noPassedItemId;
        {
            // создать таблицу
            DBTable dbt = new DBTable("session_c_cur_mark_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("scale_id", DBType.LONG).setNullable(false),
                                      new DBColumn("priority_p", DBType.INTEGER).setNullable(false),
                                      new DBColumn("positive_p", DBType.BOOLEAN).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("sessionCurrentMarkCatalogItem");
        }
    }
}