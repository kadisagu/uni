package ru.tandemservice.unisession.base.bo.SessionListDocument.ui.Mark;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unisession.base.bo.SessionListDocument.logic.SessionListDocumentMarkCatalogItemHandler;
import ru.tandemservice.unisession.base.bo.SessionListDocument.logic.SessionListDocumentMarkRow;
import ru.tandemservice.unisession.base.bo.SessionListDocument.logic.SessionListDocumentMarksHandler;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionProjectTheme;
import ru.tandemservice.unisession.entity.mark.SessionMark;

/**
 * @author avedernikov
 * @since 19.07.2016
 */
@Configuration
public class SessionListDocumentMark extends BusinessComponentManager
{
	public static final String LIST_DOCUMENT_MARK_DS = "listDocumentMarkDS";
	public static final String MARK_DS = "markDS";
	public static final String PPS_LIST_DS = "ppsListDS";

	@Bean
	@Override
	public PresenterExtPoint presenterExtPoint()
	{
		return this.presenterExtPointBuilder()
				.addDataSource(simpleSearchListDS(LIST_DOCUMENT_MARK_DS, regElementsDSColumn(), regElementsDSHandler()))
				.addDataSource(selectDS(MARK_DS, markDSHandler()))
				.addDataSource(CommonBaseStaticSelectDataSource.selectDS(PPS_LIST_DS, getName(), PpsEntry.defaultSelectDSHandler(getName()))
									.addColumn(PpsEntry.fio().s())
									.addColumn(PpsEntry.orgUnit().shortTitle().s())
									.addColumn(PpsEntry.titlePostOrTimeWorkerData().s()))
				.create();
	}

	@Bean
	public ColumnListExtPoint regElementsDSColumn()
	{
		return columnListExtPointBuilder(LIST_DOCUMENT_MARK_DS)
				.addColumn(textColumn(SessionListDocumentMarkRow.PROP_REG_ELEM, SessionListDocumentMarkRow.PROP_REG_ELEM))
				.addColumn(textColumn(SessionDocumentSlot.P_TERM_TITLE, SessionDocumentSlot.termTitle()))
				.addColumn(blockColumn("mark", "markBlockColumn"))
				.addColumn(blockColumn(SessionDocumentSlot.P_IN_SESSION, "inSessionBlockColumn"))
				.addColumn(blockColumn("rating", "ratingBlockColumn").visible("ui:ratingVisible"))
				.addColumn(blockColumn("pps", "ppsBlockColumn"))
				.addColumn(blockColumn(SessionMark.P_PERFORM_DATE, "performDateBlockColumn"))
				.addColumn(blockColumn(SessionProjectTheme.P_THEME, "themeBlockColumn").visible("ui:themeVisible"))
				.addColumn(blockColumn(SessionProjectTheme.P_COMMENT, "themeCommentBlockColumn"))
				.create();
	}

	@Bean
	public IBusinessHandler<DSInput, DSOutput> regElementsDSHandler()
	{
		return new SessionListDocumentMarksHandler(getName());
	}

	@Bean
	public IDefaultComboDataSourceHandler markDSHandler()
	{
		return new SessionListDocumentMarkCatalogItemHandler(getName());
	}
}
