/* $Id$ */
package ru.tandemservice.unisession.attestation.bo.AttestationBulletin.ui.PpsList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.core.view.util.Icon;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unisession.attestation.bo.AttestationBulletin.AttestationBulletinManager;
import ru.tandemservice.unisession.attestation.bo.AttestationBulletin.logic.SessionAttestationBulletinDSHandler;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;

/**
 * @author oleyba
 * @since 1/24/12
 */
@Configuration
public class AttestationBulletinPpsList extends BusinessComponentManager
{
    // dataSource
    public static final String DS_BULLETINS = "sessionAttestationBulletinDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
                .addDataSource(SessionReportManager.instance().eduYearDSConfig())
                .addDataSource(SessionReportManager.instance().yearPartDSConfig())
                .addDataSource(selectDS(AttestationBulletinManager.DS_REGISTRY_STRYCTURE, AttestationBulletinManager.instance().registryStructureDSHandler()).treeable(true))
                .addDataSource(selectDS(AttestationBulletinManager.DS_BULL_STATUS, AttestationBulletinManager.instance().bullStatusDSHandler()))
                .addDataSource(selectDS(AttestationBulletinManager.DS_OWNER_ORGUNIT, AttestationBulletinManager.instance().ownerOrgUnitDSHandler()).addColumn(OrgUnit.fullTitle().s()))
                .addDataSource(selectDS(AttestationBulletinManager.DS_DISCIPLINE, AttestationBulletinManager.instance().disciplineDSHandler()).addColumn(EppRegistryElementPart.titleWithNumber().s()))
                .addDataSource(UniStudentManger.instance().courseDSConfig())
                .addDataSource(selectDS(AttestationBulletinManager.DS_GROUP, AttestationBulletinManager.instance().groupDSHandler()).addColumn(Group.title().s()))
                .addDataSource(this.searchListDS(DS_BULLETINS, this.sessionAttestationBulletinDS(), this.sessionAttestationBulletinDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint sessionAttestationBulletinDS()
    {
        final IFormatter formatter = new CollectionFormatter(source -> {
            if (source == null)
                return "";
            if (source instanceof ITitled)
                return ((ITitled) source).getTitle();
            return source.toString();
        });

        return this.columnListExtPointBuilder(DS_BULLETINS)
            .addColumn(textColumn(SessionAttestationBulletinDSHandler.WRAPP_PROP_TITLE, SessionAttestationBulletin.title()).order().create())
            .addColumn(textColumn(SessionAttestationBulletinDSHandler.WRAPP_PROP_GROUP, SessionAttestationBulletinDSHandler.WRAPP_PROP_GROUP).formatter(formatter))
            .addColumn(textColumn(SessionAttestationBulletinDSHandler.WRAPP_PROP_STUDENT_COUNT, SessionAttestationBulletinDSHandler.WRAPP_PROP_STUDENT_COUNT))
            .addColumn(textColumn(SessionAttestationBulletinDSHandler.WRAPP_PROP_PPS, SessionAttestationBulletinDSHandler.WRAPP_PROP_PPS).formatter(formatter))
            .addColumn(textColumn(SessionAttestationBulletinDSHandler.WRAPP_PROP_DISCIPLINE, SessionAttestationBulletinDSHandler.WRAPP_PROP_DISCIPLINE))
            .addColumn(actionColumn("automark", new Icon("edit_mark", "Выставить аттестацию на основании рейтинга"), "onClickAutoMark").disabled(SessionAttestationBulletinDSHandler.WRAPP_PROP_NOT_USE_CURRENT_RATING))
            .addColumn(actionColumn("print", CommonDefines.ICON_PRINT, "onClickPrint"))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> sessionAttestationBulletinDSHandler() {
        return new SessionAttestationBulletinDSHandler(this.getName());
    }

}