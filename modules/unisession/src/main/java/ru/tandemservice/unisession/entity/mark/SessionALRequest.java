package ru.tandemservice.unisession.entity.mark;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unisession.entity.mark.gen.*;

/** @see ru.tandemservice.unisession.entity.mark.gen.SessionALRequestGen */
public class SessionALRequest extends SessionALRequestGen
{
    @Override
    @EntityDSLSupport(parts = P_REQUEST_DATE)
    public String getTitle()
    {
        return "Заявление от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getRequestDate());
    }
}