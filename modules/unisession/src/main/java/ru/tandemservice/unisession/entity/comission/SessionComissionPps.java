package ru.tandemservice.unisession.entity.comission;

import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unisession.entity.comission.gen.SessionComissionPpsGen;

/**
 * Преподаватель в коммиссии
 */
public class SessionComissionPps extends SessionComissionPpsGen
{
    public SessionComissionPps() {}
    public SessionComissionPps(final SessionComission commission, final PpsEntry ppsEntry) {
        this.setCommission(commission);
        this.setPps(ppsEntry);
    }

}