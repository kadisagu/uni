/* $*/

package ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinPub;

import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.uni.dao.IListDataSourceDao;
import ru.tandemservice.uni.dao.IUpdateable;

/**
 * @author oleyba
 * @since 2/18/11
 */
public interface IDAO extends IUpdateable<Model>, IListDataSourceDao<Model>
{
    String V_BULLETIN = "containedInBulletin";
    String V_GROUP = "containedInGroup";

    void prepareCheckDataSource(final Model model);

    void doOpenBulletin(Model model);

    void doCloseBulletin(Model model);

    void printBulletin(Model model);

    void delete(IEntity entity);
}
