/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionProtocol;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unisession.base.bo.SessionProtocol.logic.ISessionProtocolPrintDAO;
import ru.tandemservice.unisession.base.bo.SessionProtocol.logic.ProtocolRow;
import ru.tandemservice.unisession.base.bo.SessionProtocol.logic.SessionProtocolFinalQualWorkPrintDAO;
import ru.tandemservice.unisession.base.bo.SessionProtocol.logic.SessionProtocolStateFinalExamPrintDAO;
import ru.tandemservice.unisession.entity.stateFinalExam.SessionGECMember;
import ru.tandemservice.unisession.entity.stateFinalExam.SessionStateFinalExamProtocol;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Andreev
 * @since 12.09.2016
 */
@Configuration
public class SessionProtocolManager extends BusinessObjectManager
{
    public static SessionProtocolManager instance()
    {
        return instance(SessionProtocolManager.class);
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> stateFinalExamProtocolDSHandler()
    {
        return new DefaultSearchDataSourceHandler(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                Long bulletinId = context.get("bulletinId");

                String alias = "prt";
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(SessionStateFinalExamProtocol.class, alias)
                        .column(property(alias))
                        .where(eq(property(alias, SessionStateFinalExamProtocol.documentSlot().document().id()), value(bulletinId)));

                DQLOrderDescriptionRegistry orderRegistry = new DQLOrderDescriptionRegistry(SessionStateFinalExamProtocol.class, alias)
                        .setOrders("student", new OrderDescription(SessionStateFinalExamProtocol.documentSlot().studentWpeCAction().studentWpe().student().person().identityCard().fullFio()));
                orderRegistry.applyOrder(builder, input.getEntityOrder());

                input.setCountRecord(builder.createCountStatement(new DQLExecutionContext(context.getSession())).<Long>uniqueResult().intValue());
                DSOutput output = DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build();

                List<Long> commissionIds = output.<SessionStateFinalExamProtocol>getRecordList().stream()
                        .filter(p -> p.getCommission() != null)
                        .map(p -> p.getCommission().getId()).collect(Collectors.toList());

                Map<Long, List<SessionGECMember>> membersByCommission = new DQLSelectBuilder()
                        .fromEntity(SessionGECMember.class, "m")
                        .column(property("m"))
                        .where(in(property("m", SessionGECMember.commission()), commissionIds))
                        .createStatement(context.getSession()).<SessionGECMember>list()
                        .stream()
                        .collect(Collectors.groupingBy(
                                member -> member.getCommission().getId(),
                                Collectors.mapping(member -> member, Collectors.toList()))
                        );

                CollectionUtils.transform(output.getRecordList(), in -> {
                    if (in instanceof ProtocolRow)
                        return in;

                    SessionStateFinalExamProtocol protocol = (SessionStateFinalExamProtocol) in;
                    return new ProtocolRow(protocol, protocol.getCommission() == null ? null : membersByCommission.get(protocol.getCommission().getId()));
                });

                return output;
            }
        };
    }

    @Bean
    public ISessionProtocolPrintDAO stateExamProtocolPrintDAO()
    {
        return new SessionProtocolStateFinalExamPrintDAO();
    }

    @Bean
    public ISessionProtocolPrintDAO fqwProtocolPrintDAO()
    {
        return new SessionProtocolFinalQualWorkPrintDAO();
    }
}