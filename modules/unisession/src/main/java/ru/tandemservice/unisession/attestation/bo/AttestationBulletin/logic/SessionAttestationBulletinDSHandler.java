/* $Id:$ */
package ru.tandemservice.unisession.attestation.bo.AttestationBulletin.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unisession.attestation.bo.Attestation.AttestationManager;
import ru.tandemservice.unisession.attestation.bo.AttestationBulletin.AttestationBulletinManager;
import ru.tandemservice.unisession.attestation.entity.SessionAttestation;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlot;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.entity.catalog.SessionAttestationMarkCatalogItem;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.upper;

/**
 * Если
 * <code>(null == attestation || (eduYear == null && yearPart == null)) && null == ppsPerson</code>
 * , то пустой список.
 *
 * @author oleyba
 * @since 10/10/12
 */
public class SessionAttestationBulletinDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    // property
    public static final String WRAPP_PROP_TITLE = "title";
    public static final String WRAPP_PROP_GROUP = "group";
    public static final String WRAPP_PROP_STUDENT_COUNT = "studentCount";
    public static final String WRAPP_PROP_PPS = "pps";
    public static final String WRAPP_PROP_DISCIPLINE = "discipline";
    public static final String WRAPP_PROP_CLOSE_DATE = "closeDate";
    public static final String WRAPP_PROP_NOT_USE_CURRENT_RATING = "notUseCurRating";

    public SessionAttestationBulletinDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput dsInput, ExecutionContext context)
    {
        final OrgUnit orgUnit = context.get(SessionReportManager.PARAM_ORG_UNIT);
        final SessionAttestation attestation = context.get(AttestationBulletinManager.BIND_ATTESTATION);
        final Person ppsPerson = context.get(AttestationBulletinManager.BIND_PPS_PERSON);
        final EducationYear eduYear = context.get(AttestationManager.PARAM_EDU_YEAR);
        final YearDistributionPart yearPart = context.get(AttestationManager.PARAM_YEAR_PART);

        if ((null == attestation || (eduYear == null && yearPart == null)) && null == ppsPerson)
		{
			final DSOutput output = ListOutputBuilder.get(dsInput, Collections.emptyList()).build();
			output.setCountRecord(Math.max(5, dsInput.getCountRecord()));
			return output;
        }

        // получаем основной пул Аттестационных ведомостей, фильтруя по обязательным фильтрам
        DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(SessionAttestationBulletin.class, "bulletin");

        if (null != ppsPerson) {
            final DQLSelectBuilder tutorDQL = new DQLSelectBuilder()
                .fromEntity(SessionComissionPps.class, "rel")
                .where(DQLExpressions.eq(DQLExpressions.property(SessionComissionPps.pps().person().fromAlias("rel")), DQLExpressions.value(ppsPerson)))
                .column(property(SessionComissionPps.commission().id().fromAlias("rel")))
                .predicate(DQLPredicateType.distinct);

            dql.where(in(property(SessionAttestationBulletin.commission().id().fromAlias("bulletin")), tutorDQL.buildQuery()));
        }

        if (null != orgUnit) {
            dql.where(eq(property(SessionAttestationBulletin.attestation().sessionObject().orgUnit().fromAlias("bulletin")), value(orgUnit)));
        }

        if (null != attestation) {
            dql.where(eq(property(SessionAttestationBulletin.attestation().fromAlias("bulletin")), value(attestation)));
        }
        else {
            dql
                .where(eq(property(SessionAttestationBulletin.attestation().sessionObject().educationYear().fromAlias("bulletin")), value(eduYear)))
                .where(eq(property(SessionAttestationBulletin.attestation().sessionObject().yearDistributionPart().fromAlias("bulletin")), value(yearPart)));
        }

        // фильтруем по допольнительным не обязательным фильтрам
        applyFilters(dql, context);

        return getOutput(dql, dsInput, context);
    }

    /**
     * Подготавливает DSOutput со списком враперов, с данным по связным объектам.
     */
    protected DSOutput getOutput(final DQLSelectBuilder dql, DSInput input, ExecutionContext context)
    {
        EntityOrder entityOrder = input.getEntityOrder();
        dql.column(property(SessionAttestationBulletin.id().fromAlias("bulletin")));

        Comparator<Object[]> comparator;
        if (entityOrder.getColumnName().equals(WRAPP_PROP_DISCIPLINE))
        {
            dql.column(property("bulletin", SessionAttestationBulletin.registryElementPart()));
            comparator = (o1, o2) -> {
                EppRegistryElementPart d1 = (EppRegistryElementPart) o1[1];
                EppRegistryElementPart d2 = (EppRegistryElementPart) o2[1];

                int compare = d1.getTitleWithNumber().compareTo(d2.getTitleWithNumber());
                if (compare == 0) compare = Long.compare((long) o1[0], (long) o2[0]);
                return compare;
            };
        }
        else
        {
            dql.column(property("bulletin", SessionAttestationBulletin.number()));
            comparator = UniBaseDao.ID_VALUE_PAIR__NUMBER_AS_STRING__COMPARATOR_DIRECT;
        }

        List<Object[]> sortIds = dql.createStatement(context.getSession()).list();
        Collections.sort(sortIds, comparator);
        if (OrderDirection.desc.equals(entityOrder.getDirection()))
        {
            Collections.reverse(sortIds);
        }

        final List<Long> ids = UniBaseUtils.getColumn(sortIds, 0);
        final int count = sortIds.size();
        int firstResult = input.getStartRecord();
        List<Long> getSortIds = ids;
        if (firstResult >= count || firstResult < 0)
        {
            if (input.getCountRecord() != 0)
            {
                int lastPageCount = count % input.getCountRecord();
                if (lastPageCount == 0)
                    lastPageCount = input.getCountRecord();

                firstResult = count - lastPageCount;
                if (firstResult < 0)
                    firstResult = 0;

            } else
            {
                firstResult = count;
            }
        }

        int maxResult = Math.min(firstResult + input.getCountRecord(), count);
        if (firstResult <= maxResult)
            getSortIds = ids.subList(firstResult, maxResult);

//        dql.joinEntity("bulletin", DQLJoinType.left, SessionAttestationSlot.class, "slot", eq(property(SessionAttestationBulletin.id().fromAlias("bulletin")), property(SessionAttestationSlot.bulletin().id().fromAlias("slot"))));
//        dql.joinEntity("bulletin", DQLJoinType.left, SessionComissionPps.class, "pps", eq(property(SessionAttestationBulletin.commission().id().fromAlias("bulletin")), property(SessionComissionPps.commission().id().fromAlias("pps"))));
//        dql.addColumn(property("bulletin.id"), "id");
//        dql.addColumn(property("bulletin"), "bulletin");
//        dql.addColumn(property("slot"), "slot");
//        dql.addColumn(property("pps"), "pps");

        final List<Object[]> slotList = new ArrayList<>();
        final List<Object[]> ppsList = new ArrayList<>();
        final List<SessionAttestationBulletin> bulletinList = new ArrayList<>();

        BatchUtils.execute(getSortIds, DQL.MAX_VALUES_ROW_NUMBER, elements -> {
            slotList.addAll(new DQLSelectBuilder().fromEntity(SessionAttestationSlot.class, "slot")
                    .where(in(property(SessionAttestationSlot.bulletin().id().fromAlias("slot")), elements))
                    .joinPath(DQLJoinType.left, SessionAttestationSlot.studentWpe().student().fromAlias("slot"), "stud")
                    .joinPath(DQLJoinType.left, Student.group().fromAlias("stud"), "gr")
                    .column(property(SessionAttestationSlot.bulletin().id().fromAlias("slot")))
                    .column(property(Student.id().fromAlias("stud")))
                    .column(property(Group.title().fromAlias("gr")))
                    .createStatement(context.getSession()).<Object[]>list());

            ppsList.addAll(new DQLSelectBuilder().fromEntity(SessionAttestationBulletin.class, "bulletin")
                    .where(in(property(SessionAttestationBulletin.id().fromAlias("bulletin")), elements))
                    .joinEntity("bulletin", DQLJoinType.left, SessionComissionPps.class, "pps", eq(property(SessionAttestationBulletin.commission().id().fromAlias("bulletin")), property(SessionComissionPps.commission().id().fromAlias("pps"))))
                    .column(property("bulletin.id"))
                    .column(property(SessionComissionPps.pps().fromAlias("pps")))
                    .createStatement(context.getSession()).<Object[]>list());

            bulletinList.addAll(new DQLSelectBuilder().fromEntity(SessionAttestationBulletin.class, "bulletin")
                    .where(in(property(SessionAttestationBulletin.id().fromAlias("bulletin")), elements))
                    .column(property("bulletin"))
                    .createStatement(context.getSession()).<SessionAttestationBulletin>list());
        });

        Map<Long, Set<String>> groupMap = new HashMap<>();
        Map<Long, Integer> studentMap = new HashMap<>();
        Map<Long, Set<PpsEntry>> ppsListMap = new HashMap<>();
        for (Object[] objects : slotList)
        {
            Long id = (Long) objects[0];
            String groupTitle = (String) objects[2];

            Set<String> groupList = groupMap.get(id);
            if (groupList == null)
                groupMap.put(id, groupList = new HashSet<>());
            groupList.add(groupTitle);

            Integer studentCount = studentMap.get(id);
            if (studentCount == null)
                studentMap.put(id, 0);
            studentMap.put(id, studentMap.get(id) + 1);

        }
        for (Object[] objects : ppsList)
        {
            Long id = (Long) objects[0];
            PpsEntry pps = (PpsEntry) objects[1];

            Set<PpsEntry> list = ppsListMap.get(id);
            if (list == null)
                ppsListMap.put(id, list = new HashSet<>());
            list.add(pps);
        }

        List<DataWrapper> wrapperList = new ArrayList<>();
        for (SessionAttestationBulletin bulletin : bulletinList)
        {
            DataWrapper wrapper = new DataWrapper(bulletin.getId(), bulletin.getTitle());
            wrapper.setProperty(WRAPP_PROP_GROUP, groupMap.containsKey(bulletin.getId()) ? groupMap.get(bulletin.getId()) : Collections.emptyList());
            wrapper.setProperty(WRAPP_PROP_STUDENT_COUNT, studentMap.containsKey(bulletin.getId()) ? studentMap.get(bulletin.getId()) : 0);
            wrapper.setProperty(WRAPP_PROP_PPS, ppsListMap.containsKey(bulletin.getId()) ? UniBaseUtils.getPropertiesSet(ppsListMap.get(bulletin.getId()), PpsEntry.shortTitle().s()) : Collections.emptyList());
            wrapper.setProperty(WRAPP_PROP_DISCIPLINE, bulletin.getRegistryElementPart().getTitleWithNumber());
            wrapper.setProperty(WRAPP_PROP_CLOSE_DATE, DateFormatter.DEFAULT_DATE_FORMATTER.format(bulletin.getCloseDate()));
            wrapper.setProperty(WRAPP_PROP_NOT_USE_CURRENT_RATING, !ISessionBrsDao.instance.get().isUseCurrentRating(bulletin));

            wrapperList.add(wrapper);
        }

        wrapperList = CommonDAO.sort(wrapperList, getSortIds);

        DSOutput output = ListOutputBuilder.get(input, wrapperList).pageable(true).build();
        output.setTotalSize(ids.size());
		int countRecord = Math.max(5, Math.max(output.getRecordList().size(), input.getCountRecord()));
		output.setCountRecord(countRecord);

        return output;
    }

    /**
     * Применяются необязательные фильтры, если они указаны.
     */
    protected DQLSelectBuilder applyFilters(DQLSelectBuilder dql, ExecutionContext context)
    {
        final List<OrgUnit> orgUnitList = context.get(AttestationBulletinManager.BIND_OWNER_ORGUNIT_LIST);
        final List<EppRegistryElementPart> elementList = context.get(AttestationBulletinManager.BIND_DISCIPLINE_LIST);
        final List<PpsEntry> ppsList = context.get(AttestationBulletinManager.BIND_PPS_LIST);
        final List<Course> courseList = context.get(AttestationBulletinManager.BIND_COURSE_LIST);
        final List<Group> groupList = context.get(AttestationBulletinManager.BIND_GROUP_LIST);
        final List<EppRegistryStructure> registryStructureList = context.get(AttestationBulletinManager.BIND_REGISTRY_STRYCTURE);
        final Boolean passed = context.get(AttestationBulletinManager.BIND_PASSED);
        final Long attNumber = context.get(AttestationBulletinManager.BIND_ATTESTATION_NUMBER);
        final DataWrapper bullStatus = context.get(AttestationBulletinManager.BIND_BULL_STATUS);
        final String docNumber = context.get(AttestationBulletinManager.BIND_DOCUMENT_NUMBER);
        final String studentLastName = context.get(AttestationBulletinManager.BIND_STUDENT_LAST_NAME);
        final String ppsLastName = context.get(AttestationBulletinManager.BIND_PPS_LAST_NAME);

        if (ppsLastName != null)
        {
            DQLSelectBuilder includeBuilder = new DQLSelectBuilder().fromEntity(SessionComissionPps.class, "p").column(property(SessionComissionPps.commission().id().fromAlias("p")))
                    .where(like(upper(property(SessionComissionPps.pps().person().identityCard().lastName().fromAlias("p"))), value(CoreStringUtils.escapeLike(ppsLastName, true))))
                    .predicate(DQLPredicateType.distinct);

            dql.where(in(property(SessionAttestationBulletin.commission().id().fromAlias("bulletin")), includeBuilder.buildQuery()));
        }

        if (studentLastName != null)
        {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SessionAttestationSlot.class, "slot3").column(property(SessionAttestationSlot.id().fromAlias("slot3")))
                    .where(eq(property(SessionAttestationSlot.bulletin().id().fromAlias("slot3")), property(SessionAttestationBulletin.id().fromAlias("bulletin"))))
                    .where(like(upper(property(SessionAttestationSlot.studentWpe().student().person().identityCard().lastName().fromAlias("slot3"))), value(CoreStringUtils.escapeLike(studentLastName, true))));

            dql.where(exists(builder.buildQuery()));
        }

        if (registryStructureList != null && !registryStructureList.isEmpty())
        {
            dql.where(in(property(SessionAttestationBulletin.registryElementPart().registryElement().parent().id().fromAlias("bulletin")), UniBaseDao.ids(registryStructureList)));
        }

        if (docNumber != null)
        {
            dql.where(eq(property(SessionAttestationBulletin.number().fromAlias("bulletin")), value(docNumber)));
        }

        if (bullStatus != null)
        {
            if (bullStatus.getId().equals(AttestationBulletinManager.BULL_STATUS_EMPTY_ID))
            {
                DQLSelectBuilder relationBuilder = new DQLSelectBuilder().fromEntity(SessionAttestationSlot.class, "pas").column(property(SessionAttestationSlot.id().fromAlias("pas")))
                        .where(eq(property(SessionAttestationSlot.bulletin().id().fromAlias("pas")), property(SessionAttestationBulletin.id().fromAlias("bulletin"))))
                        .where(isNull(property(SessionAttestationSlot.mark().fromAlias("pas"))));

                dql.where(exists(relationBuilder.buildQuery()));
            }
            else if (bullStatus.getId().equals(AttestationBulletinManager.BULL_STATUS_OPEN_ID))
            {
                dql.where(isNull(property(SessionAttestationBulletin.closeDate().fromAlias("bulletin"))));
            }
            else if (bullStatus.getId().equals(AttestationBulletinManager.BULL_STATUS_CLOSE_ID))
            {
                dql.where(isNotNull(property(SessionAttestationBulletin.closeDate().fromAlias("bulletin"))));
            }

        }

        if (attNumber != null)
        {
            dql.where(eq(property(SessionAttestationBulletin.attestation().number().fromAlias("bulletin")), value(attNumber.intValue())));
        }

        if (orgUnitList != null && !orgUnitList.isEmpty())
        {
            dql.where(in(property(SessionAttestationBulletin.registryElementPart().registryElement().owner().id().fromAlias("bulletin")), UniBaseDao.ids(orgUnitList)));
        }

        if (elementList != null && !elementList.isEmpty())
        {
            dql.where(in(property(SessionAttestationBulletin.registryElementPart().id().fromAlias("bulletin")), UniBaseDao.ids(elementList)));
        }

        if (ppsList != null && !ppsList.isEmpty())
        {
            DQLSelectBuilder includeBuilder = new DQLSelectBuilder().fromEntity(SessionComissionPps.class, "p").column(property(SessionComissionPps.commission().id().fromAlias("p")))
                    .where(in(property(SessionComissionPps.pps().id().fromAlias("p")), UniBaseDao.ids(ppsList)))
                    .predicate(DQLPredicateType.distinct);

            dql.where(in(property(SessionAttestationBulletin.commission().id().fromAlias("bulletin")), includeBuilder.buildQuery()));
        }

        if (courseList != null && !courseList.isEmpty())
        {
            DQLSelectBuilder relationBuilder = new DQLSelectBuilder().fromEntity(SessionAttestationSlot.class, "c").column(property(SessionAttestationSlot.bulletin().id().fromAlias("c")))
                    .where(in(property(SessionAttestationSlot.studentWpe().student().course().id().fromAlias("c")), UniBaseDao.ids(courseList)))
                    .predicate(DQLPredicateType.distinct);

            dql.where(in(property(SessionAttestationBulletin.id().fromAlias("bulletin")), relationBuilder.buildQuery()));
        }

        if (groupList != null && !groupList.isEmpty())
        {
            DQLSelectBuilder relationBuilder = new DQLSelectBuilder().fromEntity(SessionAttestationSlot.class, "g").column(property(SessionAttestationSlot.bulletin().id().fromAlias("g")))
                    .where(in(property(SessionAttestationSlot.studentWpe().student().group().id().fromAlias("g")), UniBaseDao.ids(groupList)))
                    .predicate(DQLPredicateType.distinct);

            dql.where(in(property(SessionAttestationBulletin.id().fromAlias("bulletin")), relationBuilder.buildQuery()));
        }

        if (passed != null && passed)
        {
            dql.where(exists(
                    new DQLSelectBuilder().fromEntity(SessionAttestationSlot.class, "slot")
                            .column("slot.id")
                            .joinPath(DQLJoinType.left, SessionAttestationSlot.mark().fromAlias("slot"), "mark")
                            .where(eq(property("slot", SessionAttestationSlot.L_BULLETIN), property("bulletin.id")))
                            .where(eq(DQLFunctions.coalesce(property("mark", SessionAttestationMarkCatalogItem.P_POSITIVE), value(true)), value(true)))
                            .buildQuery()
            ));
        }

        return dql;
    }
}
