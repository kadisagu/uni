/* $Id$ */
package ru.tandemservice.unisession.print;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.rtf.document.RtfDocument;

/**
 * @author Andrey Andreev
 * @since 11.11.2015
 */
public interface ISessionTransferProtocolPrintDao extends INeedPersistenceSupport
{
    RtfDocument printProtocol(Long protocolId);

}