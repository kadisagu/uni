/* $Id$ */
package ru.tandemservice.unisession.dao.retakeDoc;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.entity.dsl.PropertyPath;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unisession.entity.document.SessionObject;
import ru.tandemservice.unisession.entity.document.SessionProjectTheme;
import ru.tandemservice.unisession.entity.document.SessionRetakeDocument;

import java.util.List;

/**
 * @author oleyba
 * @since 6/20/11
 */
public interface ISessionRetakeDocDAO
{
    SpringBeanCache<ISessionRetakeDocDAO> instance = new SpringBeanCache<>(ISessionRetakeDocDAO.class.getName());

    /**
     * Настройки разбиения автоматически формируемых ведомостей пересдач.
     * Если задано разбиение по некоторому параметру, то в одну ведомость пересдач попадут только записи, у которых этот параметр совпадает.
     * В противном случае в одной ведомости могут находиться с различными значениями этого параметра.
     */
    interface GroupingSettings
    {
        enum GroupSplitOption {DO_NOT_SPLIT, SPLIT_BY_GROUP, SPLIT_BY_EPP_GROUP}

        /** Разбиение по учебной или академической группе, либо не производить разбиение по группам вообще. */
        GroupSplitOption getGroupSplitOption();

        /** Разбиение по комиссии (в одну ведомость попадут только те студенты, в записи которых в экзаменационной ведомости была комиссия из одних и тех же ППС). */
        boolean isGroupByCommission();

        /** Разбиение по типу возмещения затрат. */
        boolean isGroupByCompensationType();
    }

    /**
     * Оставить в запросе на записи студентов в УГС только те, МСРП-ФК которых требуют пересдачи (МСРП-ФК только текущей сессии)
     * @param groupRowDql - запрос к записям студента в УГС
     * @param groupRowAlias - алиас записи студента
     * @param sessionObject - текущая сессия
     */
	void filterGroupsNeedRetake(final DQLSelectBuilder groupRowDql, final String groupRowAlias, final SessionObject sessionObject);

    /**
     * Сформировать ведомости пересдач на текущую сессию для заданных УГС
     * @param groupIds - id УГС, для которых нужно сформировать ведомости
     * @param sessionObject - текущая сессия
     * @param inSession - все сформированные ведомости будут иметь признак "в сессию" с указанным значением
     */
    void doAddDocumentsForEduGroups(final List<Long> groupIds, SessionObject sessionObject, boolean inSession);

    /**
     * Сформировать ведомости пересдач на текущую сессию с заданными параметрами разбиения записей студентов на разные ведомости
     * @param sessionObject - текущая сессия
     * @param groupingSettings - параметры разбиения записей студентов на ведомости по указанным признакам
     *                         (если указано разбиение по признаку - в одну ведомость попадут только записи с одинаковым значением этого признака - см. {@link GroupingSettings})
     * @param markList - опциональный список оценок - если не пуст, то в ведомости попадут только МСРП-ФК записей студентов, на которые ссылаются оценки из списка
     */
	void doAutocreateDocuments(final SessionObject sessionObject, final GroupingSettings groupingSettings, final List<Long> markList);

    void doSplitByComission(final Long orgUnitId, final List<Long> selectedIds);

	/**
	 * Объединить несколько ведомостей пересдач в одну. Объединять ведомости можно только если:
	 * <ul>
	 *     <li/> У них совпадают части элемента реестра (мероприятия) и сессии;
	 *     <li/> У них совпадают формы контроля;
	 *     <li/> Они не содержат одинаковых студентов.
	 * </ul>
	 * Старые ведомости будут удалены, будет создана новая ведомость со следующими данными:
	 * <ul>
	 *     <li/> Мероприятие и сессия будут скопированы из старых ведомостей;
	 *     <li/> Дата формирования - текущая дата;
	 *     <li/> Будет назначен следующий номер (по общим правилам задания номера ведомости пересдач);
	 *     <li/> Комиссия, являющаяся объединением комиссий старых ведомостей (комиссий самих ведомостей и их слотов);
	 *     <li/> Слоты старых ведомостей будут ссылаться на новую.
	 * </ul>
	 * @param orgUnitId id подразделения.
	 * @param selectedIds id ведомостей, которые нужно объединить.
	 */
    void doJoin(final Long orgUnitId, final List<Long> selectedIds);

    void doSplit(final Long orgUnitId, final List<Long> selectedIds, final PropertyPath<?> path);

	/**
	 * Название мероприятия ведомости пересдач в формате {@link EppRegistryElementPart#getTitleWithNumber()}, [Названия ФИК через запятую].<br/>
	 * Пример: "Архитектура гражданских и промышленных зданий и сооружений (396 ч., 3/3, Д №2290), Экзамен, Курсовой проект"
	 * @param document Ведопость пересдач.
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	String regElementTitleWithFCATypes(SessionRetakeDocument document);

	/**
	 * Для записей студента в ведомости пересдач создать темы работы в сессии ({@link SessionProjectTheme}) на основе тем записей студента в других документах сессии на то же МСРП-ФК.
	 * Из всех записей студента по МСРП, имеющих темы, берется запись с наибольшей датой формирования.
	 * @param retakeDocument ведомость пересдач, для записей которой нужно заполнить темы
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	void createThemesByLastWpeThemes(SessionRetakeDocument retakeDocument);
}
