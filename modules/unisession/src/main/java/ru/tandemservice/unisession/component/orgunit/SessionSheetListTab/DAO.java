/* $*/

package ru.tandemservice.unisession.component.orgunit.SessionSheetListTab;

import com.google.common.collect.ImmutableList;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.tapsupport.component.selection.CommonMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.DebugUtils;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniedu.base.bo.EducationYear.utils.EducationYearModel;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.ui.EppRegistryElementSelectModel;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionSheetDocument;
import ru.tandemservice.unisession.entity.document.SessionSimpleDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 2/28/11
 */
public class DAO extends UniDao<Model> implements IDAO
{
    protected final DQLOrderDescriptionRegistry order = new DQLOrderDescriptionRegistry(SessionSheetDocument.class, "doc")
            .addAdditionalAlias(SessionDocumentSlot.class, "slot")
            .addAdditionalAlias(SessionMark.class, "mark")
            .setOrders(Student.person().identityCard().fio().s(), new OrderDescription("slot", SessionDocumentSlot.actualStudent().person().identityCard().fullFio().s()));

    @Override
    @SuppressWarnings("unchecked")
    public void prepare(final Model model)
    {
        model.getOrgUnitHolder().refresh();
        model.setSec(new OrgUnitSecModel(model.getOrgUnit()));

        model.setYearModel(new EducationYearModel());
        model.setPartsModel(EducationCatalogsManager.getYearDistributionPartModel());

        model.setRegistryElementModel(new EppRegistryElementSelectModel<EppRegistryElement>(EppRegistryElement.class)
        {
            @Override
            protected DQLSelectBuilder query(final String alias, final String filter)
            {
                final DQLSelectBuilder s = new DQLSelectBuilder().fromEntity(SessionSheetDocument.class, "doc");
                s.where(eq(property(SessionSimpleDocument.orgUnit().fromAlias("doc")), value(model.getOrgUnit())));
                s.joinEntity("doc", DQLJoinType.inner, SessionDocumentSlot.class, "slot", eq(property("doc.id"), property(SessionDocumentSlot.document().id().fromAlias("slot"))));
                s.column(property(SessionDocumentSlot.studentWpeCAction().studentWpe().registryElementPart().registryElement().id().fromAlias("slot")));

                final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppRegistryElement.class, alias);
                dql.where(in(property(alias, "id"), s.buildQuery()));

                if (null != filter)
                {
                    dql.where(this.getFilterCondition(alias, filter));
                }
                dql.order(property(EppRegistryElement.title().fromAlias(alias)));
                dql.order(property(EppRegistryElement.owner().title().fromAlias(alias)));
                return dql;
            }
        });

        model.setCourseCurrentModel(new CommonMultiSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Course.class, "b").column(property("b"))
                        .where(likeUpper(property(Course.title().fromAlias("b")), value(CoreStringUtils.escapeLike(filter, true))))
                        .order(property(Course.title().fromAlias("b")));

                if (set != null)
                    builder.where(in(property(Course.id().fromAlias("b")), set));

                return new DQLListResultBuilder(builder, 50);
            }
        });

        model.setGroupCurrentModel(new CommonMultiSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                final DQLSelectBuilder s = new DQLSelectBuilder().fromEntity(SessionSheetDocument.class, "doc")
                        .column(property(SessionSheetDocument.id().fromAlias("doc")))
                        .where(eq(property(SessionSimpleDocument.orgUnit().fromAlias("doc")), value(model.getOrgUnit())));

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SessionDocumentSlot.class, "a").column(property(SessionDocumentSlot.studentWpeCAction().studentWpe().student().group().fromAlias("a")))
                        .where(in(property(SessionDocumentSlot.document().id().fromAlias("a")), s.buildQuery()))
                        .where(likeUpper(property(SessionDocumentSlot.studentWpeCAction().studentWpe().student().group().title().fromAlias("a")), value(CoreStringUtils.escapeLike(filter, true))))
                        .order(property(SessionDocumentSlot.studentWpeCAction().studentWpe().student().group().title().fromAlias("a")))
                        .predicate(DQLPredicateType.distinct);

                Collection courseCurrentList = model.getSettings().get("courseCurrentList");
                if (courseCurrentList != null && !courseCurrentList.isEmpty())
                    builder.where(in(property(SessionDocumentSlot.studentWpeCAction().studentWpe().student().group().course().fromAlias("a")), courseCurrentList));

                if (set != null)
                    builder.where(in(property(SessionDocumentSlot.studentWpeCAction().studentWpe().student().group().id().fromAlias("a")), set));

                return new DQLListResultBuilder(builder, 50);
            }
        });

        model.setStateModel(new LazySimpleSelectModel<>(ImmutableList.of(
                new IdentifiableWrapper(0L, "Выставлена"),
                new IdentifiableWrapper(1L, "Не выставлена"))));
    }

    @Override
    public void prepareListDataSource(final Model model)
    {
        try
        {

            final Session session = this.getSession();
            final DynamicListDataSource<SessionSheetDocument> dataSource = model.getDataSource();

            final DQLSelectBuilder sheetDQL = new DQLSelectBuilder().fromEntity(SessionSheetDocument.class, "doc");//order.buildDQLSelectBuilder();
            sheetDQL.where(eq(property(SessionSimpleDocument.orgUnit().fromAlias("doc")), value(model.getOrgUnit())));
            sheetDQL.joinEntity("doc", DQLJoinType.inner, SessionDocumentSlot.class, "slot", eq(property("doc.id"), property(SessionDocumentSlot.document().id().fromAlias("slot"))));

            final String markAlias = "mark";
            final IdentifiableWrapper state = model.getSettings().get("state");
            if (null != state)
            {
                if (state.getId().equals(1L))
                {
                    final DQLSelectBuilder markDQL = new DQLSelectBuilder()
                            .fromEntity(SessionMark.class, "mark_ne")
                            .column("mark_ne.id")
                            .where(eq(property("slot.id"), property(SessionMark.slot().id().fromAlias("mark_ne"))));
                    sheetDQL.where(notExists(markDQL.buildQuery()));
                } else
                {
                    sheetDQL.joinEntity("slot", DQLJoinType.inner, SessionMark.class, markAlias, eq(property("slot.id"), property(SessionMark.slot().id().fromAlias(markAlias))));
                }
            }

            FilterUtils.applySelectFilter(sheetDQL, "slot", SessionDocumentSlot.studentWpeCAction().studentWpe().year().educationYear().s(), model.getSettings().get("year"));
            FilterUtils.applySelectFilter(sheetDQL, "slot", SessionDocumentSlot.studentWpeCAction().studentWpe().part().s(), model.getSettings().get("part"));

            FilterUtils.applySelectFilter(sheetDQL, "slot", SessionDocumentSlot.studentWpeCAction().studentWpe().registryElementPart().registryElement(), model.getSettings().get("registryElement"));

            applyFilters(model, sheetDQL, "doc");

            final String docNumber = model.getSettings().get("docNumber");
            if (StringUtils.isNotBlank(docNumber))
            {
                sheetDQL.where(eq(property(SessionSimpleDocument.number().fromAlias("doc")), value(docNumber)));
            }
            FilterUtils.applySimpleLikeFilter(sheetDQL, "slot", SessionDocumentSlot.actualStudent().person().identityCard().lastName().s(), model.getSettings().get("studentLastName"));
            if (!StringUtils.isEmpty(model.getSettings().get("ppsLastName")))
            {
                sheetDQL.joinEntity("slot", DQLJoinType.inner, SessionComissionPps.class, "pps", eq(property(SessionDocumentSlot.commission().id().fromAlias("slot")), property(SessionComissionPps.commission().id().fromAlias("pps"))));
                FilterUtils.applySimpleLikeFilter(sheetDQL, "pps", SessionComissionPps.pps().person().identityCard().lastName().s(), model.getSettings().get("ppsLastName"));
            }

            FilterUtils.applySelectFilter(sheetDQL, "slot", SessionDocumentSlot.actualStudent().course().s(), model.getSettings().get("courseCurrentList"));

            FilterUtils.applySelectFilter(sheetDQL, "slot", SessionDocumentSlot.actualStudent().group().s(), model.getSettings().get("groupCurrentList"));

            FilterUtils.applyBetweenFilter(sheetDQL, "doc", SessionSimpleDocument.formingDate().s(), model.getSettings().get("formedFrom"), (Date) model.getSettings().get("formedTo"));

            final Object performDateFrom = model.getSettings().get("performDateFrom");
            final Object performDateTo = model.getSettings().get("performDateTo");
            if (null != performDateFrom || null != performDateTo)
            {
                if (!sheetDQL.hasAlias(markAlias))
                {
                    sheetDQL.joinEntity("slot", DQLJoinType.inner, SessionMark.class, markAlias, eq(property("slot.id"), property(SessionMark.slot().id().fromAlias(markAlias))));
                }
                FilterUtils.applyBetweenFilter(sheetDQL, markAlias, SessionMark.performDate().s(), model.getSettings().get("formedFrom"), (Date) model.getSettings().get("formedTo"));
            }

            if (!sheetDQL.hasAlias(markAlias))
            {
                sheetDQL.joinEntity("slot", DQLJoinType.left, SessionMark.class, markAlias, eq(property("slot.id"), property(SessionMark.slot().id().fromAlias(markAlias))));
            }

            FilterUtils.applyBetweenFilter(sheetDQL, "doc", SessionSheetDocument.issueDate().s(), model.getSettings().get("formedFrom"), (Date) model.getSettings().get("formedTo"));
            FilterUtils.applyBetweenFilter(sheetDQL, "mark", SessionMark.performDate().s(), model.getSettings().get("performDateFrom"), (Date) model.getSettings().get("performDateTo"));

            if (!dataSource.getEntityOrder().getColumnName().equals(SessionSimpleDocument.number().s()))
            {
                this.order.applyOrder(sheetDQL, dataSource.getEntityOrder());
            }

            DebugUtils.debug(logger, new DebugUtils.Section("session sheet datasource")
            {
                @Override
                public void execute()
                {
                    if (dataSource.getEntityOrder().getColumnName().equals(SessionSimpleDocument.number().s()))
                    {
                        UniBaseUtils.createPageOrderedByNumberAsString(SessionSheetDocument.class, dataSource, sheetDQL, "doc", SessionSheetDocument.number());
                    } else
                    {
                        UniBaseUtils.createPage(dataSource, sheetDQL.column(property("doc")), session);
                    }
                }
            });

            final List<Long> sheetIds = UniBaseUtils.getIdList(model.getDataSource().getEntityList());

            final Map<Long, SessionDocumentSlot> slotMap = new HashMap<>();
            DebugUtils.debug(logger, new DebugUtils.Section("slot map")
            {
                @Override
                public void execute()
                {
                    final DQLSelectBuilder slotDQL = new DQLSelectBuilder()
                            .fromEntity(SessionDocumentSlot.class, "slot")
                            .column("slot")
                            .fetchPath(DQLJoinType.inner, SessionDocumentSlot.document().fromAlias("slot"), "doc")
                            .where(in(property("doc.id"), sheetIds));
                    for (final SessionDocumentSlot slot : slotDQL.createStatement(session).<SessionDocumentSlot>list())
                    {
                        slotMap.put(slot.getDocument().getId(), slot);
                    }
                }
            });


            final Map<Long, SessionMark> markMap = new HashMap<>();
            DebugUtils.debug(logger, new DebugUtils.Section("mark map")
            {
                @Override
                public void execute()
                {
                    final DQLSelectBuilder markDQL = new DQLSelectBuilder()
                            .fromEntity(SessionMark.class, "mark")
                            .joinPath(DQLJoinType.inner, SessionMark.slot().fromAlias("mark"), "slot")
                            .joinEntity("mark", DQLJoinType.inner, SessionSheetDocument.class, "doc", eq(property(SessionDocumentSlot.document().fromAlias("slot")), property("doc")))
                            .where(in(property(SessionSheetDocument.id().fromAlias("doc")), sheetIds))
                            .column(property(SessionSheetDocument.id().fromAlias("doc")))
                            .column("mark");
                    for (Object[] row : markDQL.createStatement(session).<Object[]>list())
                    {
                        markMap.put((Long) row[0], (SessionMark) row[1]);
                    }
                }
            });


            final Map<Long, List<String>> tutorMap = new HashMap<>();
            DebugUtils.debug(logger, new DebugUtils.Section("tutor map")
            {
                @Override
                public void execute()
                {
                    final DQLSelectBuilder tutorDQL = new DQLSelectBuilder()
                            .fromEntity(SessionComissionPps.class, "rel")
                            .column(property(SessionComissionPps.pps().person().fromAlias("rel")))
                            .joinPath(DQLJoinType.inner, SessionComissionPps.commission().fromAlias("rel"), "comm")
                            .joinEntity("comm", DQLJoinType.inner, SessionDocumentSlot.class, "slot", eq(property("comm.id"), property(SessionDocumentSlot.commission().id().fromAlias("slot"))))
                            .column(property(SessionDocumentSlot.document().id().fromAlias("slot")))
                            .joinPath(DQLJoinType.inner, SessionDocumentSlot.document().fromAlias("slot"), "doc")
                            .where(in(property("doc.id"), sheetIds))
                            .order(property(SessionComissionPps.pps().person().identityCard().fullFio().fromAlias("rel")));
                    for (final Object[] row : tutorDQL.createStatement(session).<Object[]>list())
                    {
                        SafeMap.safeGet(tutorMap, (Long) row[1], ArrayList.class).add(((Person) row[0]).getFio());
                    }
                }
            });

            for (final ViewWrapper<SessionSheetDocument> wrapper : ViewWrapper.<SessionSheetDocument>getPatchedList(model.getDataSource()))
            {
                final Long id = wrapper.getEntity().getId();
                final List<String> tutors = tutorMap.get(id);
                final SessionMark mark = markMap.get(id);

                wrapper.setViewProperty("slot", slotMap.get(id));
                wrapper.setViewProperty("mark", mark);
                wrapper.setViewProperty("tutors", tutors);
            }

        } catch (Throwable t)
        {
            throw new RuntimeException(t);
        }
    }

    protected void applyFilters(Model model, DQLSelectBuilder dql, String alias)
    {

    }
}
