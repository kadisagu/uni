package ru.tandemservice.unisession.settings.bo.SessionSettings.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;

import java.util.Map;

/**
 * @author Alexey Lopatin
 * @since 26.05.2015
 */
public interface ISessionSettingsDao extends INeedPersistenceSupport
{
    /**
     * @return Возвращает параметры для настройки «Формирование перезачтений»
     */
    Map<String, Boolean> getSettingsFormationTransferMap();
}
