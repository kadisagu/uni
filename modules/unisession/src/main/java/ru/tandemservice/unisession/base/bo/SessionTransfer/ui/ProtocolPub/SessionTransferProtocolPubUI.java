/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionTransfer.ui.ProtocolPub;

import com.beust.jcommander.internal.Maps;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.config.datasource.column.BlockDSColumn;
import org.tandemframework.caf.ui.config.datasource.column.IHeaderColumnBuilder;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.list.column.IStyleResolver;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import org.tandemframework.shared.commonbase.utils.DQLSimple;
import ru.tandemservice.uniepp.dao.student.EppStudentSlotDAO;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.unisession.base.bo.SessionCustomEduPlan.ui.AddEdit.SessionCustomEduPlanAddEdit;
import ru.tandemservice.unisession.base.bo.SessionCustomEduPlan.ui.AddEdit.SessionCustomEduPlanAddEditUI;
import ru.tandemservice.unisession.base.bo.SessionMark.daemon.SessionMarkDaemonBean;
import ru.tandemservice.unisession.base.bo.SessionTransfer.SessionTransferManager;
import ru.tandemservice.unisession.base.bo.SessionTransfer.logic.SessionTransferProtocolRowDSHandler;
import ru.tandemservice.unisession.base.bo.SessionTransfer.ui.ProtocolAddEdit.SessionTransferProtocolAddEdit;
import ru.tandemservice.unisession.base.bo.SessionTransfer.ui.ProtocolAddEdit.SessionTransferProtocolAddEditUI;
import ru.tandemservice.unisession.base.bo.SessionTransfer.ui.ProtocolRowEdit.SessionTransferProtocolRowEdit;
import ru.tandemservice.unisession.base.bo.SessionTransfer.ui.RequestRowAdd.SessionTransferRequestRowAdd;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionTransferProtocolDocument;
import ru.tandemservice.unisession.entity.document.SessionTransferProtocolMark;
import ru.tandemservice.unisession.entity.document.SessionTransferProtocolRow;

import java.text.MessageFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 14.09.2015
 */
@State({@Bind(key = UIPresenter.PUBLISHER_ID, binding = "protocol.id")})
public class SessionTransferProtocolPubUI extends UIPresenter
{
    public static final String PARAM_PROTOCOL = "protocol";

    public static final String ROW_EDIT_LISTENER = "onClickEditRow";
    public static final String ROW_DELETE_LISTENER = "onClickDeleteRow";

    private SessionTransferProtocolDocument _protocol = new SessionTransferProtocolDocument();

    @Override
    public void onComponentRefresh()
    {
        _protocol = DataAccessServices.dao().getNotNull(_protocol.getId());
        addAdditionalColumns();
    }

    public void addAdditionalColumns()
    {
        IHeaderColumnBuilder controlActions = SessionTransferManager.headerColumn("marksOnControlActions");
        BaseSearchListDataSource ds = getProtocolRowDS();

        ds.doCleanupDataSource();
        ds.addColumn(controlActions);
        ds.addColumn(SessionTransferManager.actionColumn("edit", CommonDefines.ICON_EDIT, ROW_EDIT_LISTENER)
                .permissionKey("editRowSessionTransferProtocolDocument").disabled(_protocol.isLocked()));
        ds.addColumn(SessionTransferManager.actionColumn("delete", CommonDefines.ICON_DELETE, ROW_DELETE_LISTENER)
                        .alert(new FormattedMessage("protocolRowDS.delete.alert", SessionTransferProtocolRow.requestRow().regElementPart().titleWithNumber().s()))
                        .permissionKey("deleteRowSessionTransferProtocolDocument").disabled(_protocol.isLocked()).visible(!_protocol.isApproved())
        );
        for (Map.Entry<String, EppFControlActionType> entry : getUsedFCATypeMap().entrySet()) {
            IStyleResolver resolver = rowEntity -> isHasRegElementPartFCA() && null == getCurrentProtocolRowMark() ? "background-color: #fcebd7;" : "";
            BlockDSColumn column = SessionTransferManager.blockColumn(entry.getKey(), "controlActionBlock").width("1px").styleResolver(resolver).create();
            column.setLabel(entry.getValue().getTitle());
            controlActions.addSubColumn(column).create();
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (SessionTransferProtocolPub.PROTOCOL_ROW_DS.equals(dataSource.getName())) {
            dataSource.put(PARAM_PROTOCOL, _protocol);
        }
    }

    // Listeners

    public void onClickEdit()
    {
        _uiActivation.asRegionDialog(SessionTransferProtocolAddEdit.class)
                .parameter(SessionTransferProtocolAddEditUI.PARAM_STUDENT_ID, _protocol.getRequest().getStudent().getId())
                .parameter(SessionTransferProtocolAddEditUI.PARAM_PROTOCOL_ID, _protocol.getId())
                .activate();
    }

    public void onClickDelete()
    {
        DataAccessServices.dao().delete(_protocol);
        deactivate();
    }

    public void onClickOpen()
    {
        _protocol.setCloseDate(null);
        DataAccessServices.dao().saveOrUpdate(_protocol);
        _uiSupport.doRefresh();
    }

    public void onClickClose()
    {
        if (isCheckMarkEmpty())
            throw new ApplicationException("Необходимо выставить все оценки для строк протокола.");

        if (_protocol.getWorkPlanVersion() == null)
            throw new ApplicationException("Необходимо сформировать версию РУП для протокола.");

        _protocol.setCloseDate(new Date());
        DataAccessServices.dao().saveOrUpdate(_protocol);
        _uiSupport.doRefresh();
    }

    public void onClickApprove()
    {
        if (null == _protocol.getCommission())
            throw new ApplicationException("Невозможно утвердить протокол, в нем не указаны члены комиссии.");
        if (isCheckRowEmpty())
            throw new ApplicationException("Невозможно утвердить протокол, в нем нет ни одного мероприятия.");

        _protocol.setApproved(true);
        DataAccessServices.dao().saveOrUpdate(_protocol);
        _uiSupport.doRefresh();
    }

    public void onClickBackToForming()
    {
        try { SessionTransferManager.instance().dao().onDeleteCustomEduPlan(_protocol.getRequest().getStudent()); } catch (Throwable ignored) {}
        SessionTransferManager.instance().dao().onDeleteWorkPlanVersion(_protocol);
        SessionMarkDaemonBean.DAEMON.wakeUpDaemon();

        _protocol.setApproved(false);
        _protocol.setWorkPlanVersion(null);
        DataAccessServices.dao().saveOrUpdate(_protocol);
        _uiSupport.doRefresh();
    }

    public void onClickWorkPlanForming()
    {
        SessionTransferManager.instance().dao().onCreateWorkPlanVersion(_protocol);
        EppStudentSlotDAO.DAEMON.wakeUpAndWaitDaemon();
        SessionTransferManager.instance().dao().onCreateSessionDocumentSlot(_protocol, null);
        SessionMarkDaemonBean.DAEMON.wakeUpDaemon();
    }

    public void onClickCustomEduPlanForming()
    {
        try { SessionTransferManager.instance().dao().onDeleteCustomEduPlan(_protocol.getRequest().getStudent()); } catch (Throwable ignored) {}
        if (null != _protocol.getWorkPlanVersion())
        {
            _uiActivation.asRegionDialog(SessionCustomEduPlanAddEdit.class).parameter(SessionCustomEduPlanAddEditUI.BIND_PROTOCOL_ID, _protocol.getId()).activate();
        }
    }

    public void onClickEditRow()
    {
        _uiActivation.asRegionDialog(SessionTransferProtocolRowEdit.class).parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong()).activate();
    }

    public void onClickDeleteRow()
    {
        Long rowId = getListenerParameterAsLong();

        if (isCheckMarkSlotNotEmpty(rowId))
            throw new ApplicationException("Нельзя удалить строку протокола, если по ним уже выставлена оценка.");

        DataAccessServices.dao().delete(rowId);
        _uiSupport.doRefresh();
    }

    public void onClickAddRow()
    {
        if (SessionTransferManager.instance().dao().isCheckProtocolRowMiss(_protocol)) {
            ContextLocal.getInfoCollector().add("Все строки заявления добавлены в протокол.");
            return;
        }
        _uiActivation.asRegion(SessionTransferRequestRowAdd.class).parameter(UIPresenter.PUBLISHER_ID, _protocol.getId()).top().activate();
    }

    public void onClickPrint()
    {
        RtfDocument document = SessionTransferManager.instance().protocolPrintDao().printProtocol(_protocol.getId());
        String filename = "Протокол перезачтения от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(_protocol.getProtocolDate())+".rtf";

        if (document != null)
            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().document(document).fileName(filename).rtf(), false);
    }

    // Utils

    public String getTerm()
    {
        EppWorkPlanBase workPlan = _protocol.getWorkPlan();
        return workPlan.getGridTerm().getPart().getTitle() + " " + workPlan.getYear().getEducationYear().getTitle();
    }

    public String getCommission()
    {
        List<SessionComissionPps> commissionPpsList = new DQLSimple<>(SessionComissionPps.class)
                .where(SessionComissionPps.commission(), _protocol.getCommission())
                .order(SessionComissionPps.pps().person().identityCard().lastName())
                .order(SessionComissionPps.pps().person().identityCard().firstName())
                .order(SessionComissionPps.pps().person().identityCard().middleName())
                .list();
        List<String> titles = CommonBaseUtil.getPropertiesList(commissionPpsList, SessionComissionPps.pps().title().s());
        return StringUtils.join(titles, "\n");
    }

    public ParametersMap getParameters()
    {
        return new ParametersMap()
                .add(UIPresenter.PUBLISHER_ID, _protocol.getRequest().getStudent().getId())
                .add("selectedStudentTab", "studentNewSessionTab")
                .add("selectedDataTab", "studentSessionMarkTab");
    }

    public Map<String, EppFControlActionType> getUsedFCATypeMap()
    {
        Map<String, EppFControlActionType> usedFcaTypeMap = Maps.newLinkedHashMap();

        List<SessionTransferProtocolMark> rowMarks = DataAccessServices.dao().getList(SessionTransferProtocolMark.class, SessionTransferProtocolMark.protocolRow().protocol().id(), _protocol.getId(), SessionTransferProtocolMark.controlAction().priority().s());
        for (SessionTransferProtocolMark protocolMark : rowMarks) {
            EppFControlActionType ca = protocolMark.getControlAction();
            if (!usedFcaTypeMap.containsKey(ca.getFullCode()))
                usedFcaTypeMap.put(ca.getFullCode(), ca);
        }
        return usedFcaTypeMap;
    }

    public boolean isCheckRowEmpty()
    {
        return !ISharedBaseDao.instance.get().existsEntity(SessionTransferProtocolRow.class, SessionTransferProtocolRow.protocol().s(), _protocol);
    }

    public boolean isCheckMarkEmpty()
    {
        return ISharedBaseDao.instance.get().existsEntity(
                SessionTransferProtocolMark.class,
                SessionTransferProtocolMark.protocolRow().protocol().s(), _protocol,
                SessionTransferProtocolMark.mark().s(), null
        );
    }

    public boolean isCheckMarkSlotNotEmpty(Long rowId)
    {
        return ISharedBaseDao.instance.get().existsEntityByCondition(SessionTransferProtocolMark.class, "m", and(
                eq(property("m", SessionTransferProtocolMark.protocolRow().id()), value(rowId)),
                isNotNull(property("m", SessionTransferProtocolMark.slot()))
        ));
    }

    public boolean isHasRegElementPartFCA()
    {
        String code = getProtocolRowDS().getCurrentColumn().getName();
        Set<String> hasRegElementPartFCA = getCurrentMarkMap().keySet();
        return hasRegElementPartFCA.contains(code);
    }

    public String getDeleteAlert()
    {
        return MessageFormat.format(getConfig().getProperty("ui.delete.alert"), _protocol.getNumber());
    }

    // Getters & Setters

    public SessionTransferProtocolDocument getProtocol()
    {
        return _protocol;
    }

    public void setProtocol(SessionTransferProtocolDocument protocol)
    {
        _protocol = protocol;
    }

    public BaseSearchListDataSource getProtocolRowDS()
    {
        return getConfig().getDataSource(SessionTransferProtocolPub.PROTOCOL_ROW_DS);
    }

    public DataWrapper getCurrentDS()
    {
        return getProtocolRowDS().getCurrent();
    }

    public SessionTransferProtocolRow getCurrentProtocolRow()
    {
        return getCurrentDS().getWrapped();
    }

    @SuppressWarnings("unchecked")
    public Map<String, SessionMarkGradeValueCatalogItem> getCurrentMarkMap()
    {
        return (Map<String, SessionMarkGradeValueCatalogItem>) getCurrentDS().getProperty(SessionTransferProtocolRowDSHandler.PROP_MARK_MAP);
    }

    public SessionMarkGradeValueCatalogItem getCurrentProtocolRowMark()
    {
        String code = getProtocolRowDS().getCurrentColumn().getName();
        return getCurrentMarkMap().get(code);
    }
}
