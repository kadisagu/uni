/* $Id:$ */
package ru.tandemservice.unisession.brs.dao;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.base.util.Wiki;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionGroup;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.attestation.entity.SessionAttestation;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin;
import ru.tandemservice.unisession.base.bo.SessionMark.logic.ISessionMarkDAO;
import ru.tandemservice.unisession.dao.document.ISessionDocumentBaseDAO;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;
import ru.tandemservice.unisession.entity.document.SessionDocument;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;

import java.util.*;

/**
 * @author oleyba
 * @since 10/9/12
 */
@Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=9865671")
public class SessionBrsDao extends UniBaseDao implements ISessionBrsDao
{
    @Override
    public ISessionBrsSettings getSettings()
    {
        return new ISessionBrsSettings() {
            @Override public boolean isUseCurrentRatingForTransferDocument() { return false;}
            @Override public boolean isCurrentRatingRequiredForTransferDocument() { return false; }
            @Override public boolean isUseCurrentRatingForListDocument() { return false; }
            @Override public boolean isCurrentRatingRequiredForListDocument() { return false; }
        };
    }

    @Override
    public void saveSettings(ISessionBrsSettings settingsWrapper)
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isUseCurrentRating(SessionAttestationBulletin bulletin)
    {
        return false;
    }

    @Override
    public boolean isUseCurrentRating(SessionAttestation attestation)
    {
        return false;
    }

    @Override
    public void doFillAttestationMarkBasedOnCurrentRating(SessionAttestationBulletin bulletin)
    {
        throw new IllegalStateException();
    }

    @Override
    public Map<String, IRatingAdditParamDef> getRatingAdditionalParamDefinitions(ISessionRatingSettingsKey key) {
        return Collections.emptyMap();
    }

    @Override
    public void saveCurrentRating(SessionDocument document)
    {
        throw new IllegalStateException();
    }

    @Override
    public void saveCurrentRating(SessionDocument document, boolean silent)
    {
        saveCurrentRating(document);
    }

    @Override
    public ISessionMarkDAO.MarkData calculateMarkData(Date performDate, SessionDocumentSlot slot, Double points)
    {
        throw new IllegalStateException();
    }

    @Override
    public ISessionRatingSettings getRatingSettings(final ISessionRatingSettingsKey key)
    {
        return getSettingsForDisabledBrs(key);
    }

    @Override
    public Map<ISessionRatingSettingsKey, ISessionRatingSettings> getRatingSettings(Collection<SessionDocumentSlot> slots)
    {
        Map<ISessionRatingSettingsKey, ISessionRatingSettings> map = new HashMap<>();
        for (SessionDocumentSlot slot : slots) {
            ISessionRatingSettingsKey key = key(slot);
            if (null == map.get(key)) {
                ISessionRatingSettings ratingSettings = getRatingSettings(key);
                if (null != ratingSettings)
                    map.put(key, ratingSettings);
            }
        }
        return map;
    }

    @Override
    public Map<ISessionRatingSettingsKey, ISessionRatingSettings> getRatingSettings(SessionDocument document)
    {
        return getRatingSettings(getList(SessionDocumentSlot.class, SessionDocumentSlot.document(), document));
    }

    @Override
    public ISessionRatingSettingsKey key(EppGradeScale scale, EppFControlActionGroup caGroup, EppRegistryElementPart discipline, EppYearPart yearPart, OrgUnit groupOu)
    {
        return new SessionRatingSettingsKey(scale, caGroup, discipline, yearPart, groupOu);
    }

    @Override
    public ISessionRatingSettingsKey key(SessionDocumentSlot slot)
    {
        return SessionRatingSettingsKey.key(slot);
    }

    @Override
    public ISessionRatingSettingsKey key(SessionDocument document, EppStudentWpeCAction slot)
    {
        return SessionRatingSettingsKey.key(document, slot);
    }

    @Override
    public ISessionRatingSettingsKey key(OrgUnit groupOu, EppStudentWpeCAction slot)
    {
        return SessionRatingSettingsKey.key(groupOu, slot);
    }

    protected static class SessionRatingSettingsKey implements ISessionRatingSettingsKey
    {
        private EppGradeScale scale;
        private EppFControlActionGroup caGroup;
        private EppRegistryElementPart discipline;
        private EppYearPart yearPart;
        private OrgUnit groupOu;

        private MultiKey key;

        public SessionRatingSettingsKey(EppGradeScale scale, EppFControlActionGroup caGroup, EppRegistryElementPart discipline, EppYearPart yearPart, OrgUnit groupOu)
        {
            this.scale = scale;
            this.caGroup = caGroup;
            this.discipline = discipline;
            this.yearPart = yearPart;
            this.groupOu = groupOu;
            this.key = new MultiKey(scale.getId(), caGroup.getId(), discipline.getId(), yearPart.getId(), groupOu.getId());
        }

        public static SessionRatingSettingsKey key(SessionDocumentSlot slot)
        {
            EppRegistryElementPart discipline = slot.getDocument() instanceof SessionBulletinDocument ? ((SessionBulletinDocument) slot.getDocument()).getMarkingProperties().getRegistryElementPart() : slot.getStudentWpeCAction().getStudentWpe().getRegistryElementPart() ;
            EppFControlActionType fca = slot.getStudentWpeCAction().getActionType();
            EppGradeScale scale = ISessionDocumentBaseDAO.instance.get().getGradeScale(slot.getDocument(), slot.getStudentWpeCAction());

            OrgUnit groupOu = slot.getDocument().getGroupOu();
            if (null == groupOu)
            {
                Student student = slot.getActualStudent();
                EducationOrgUnit eou = student.getEducationOrgUnit();
                groupOu = eou.getGroupOrgUnit();

                if (null == groupOu)
                    throw new ApplicationException("Работа с документом невозможна: для НПП «" + eou.getTitle() + "», студента «" + student.getFullFio() + "» не указан деканат.");
            }

            EppStudentWorkPlanElement studentWpe = slot.getStudentWpeCAction().getStudentWpe();
            EppYearPart eppYearPart = studentWpe.getEppYearPart();
            if (null == eppYearPart)
                throw new ApplicationException("Работа с документом невозможна: не удалось получить значение части учебного, для пары год «" + studentWpe.getYear().getTitle() + "» и часть «" + studentWpe.getPart().getTitle() + "».");

            return new SessionRatingSettingsKey(scale, fca.getGroup(), discipline, eppYearPart, groupOu);
        }

        public static SessionRatingSettingsKey key(SessionDocument document, EppStudentWpeCAction slot)
        {
            EppRegistryElementPart discipline = document instanceof SessionBulletinDocument ? ((SessionBulletinDocument) document).getMarkingProperties().getRegistryElementPart() : slot.getStudentWpe().getRegistryElementPart() ;
            EppFControlActionType fca = slot.getActionType();
            EppGradeScale scale = ISessionDocumentBaseDAO.instance.get().getGradeScale(document, slot);

            OrgUnit groupOu = document.getGroupOu();
            if (null == groupOu) groupOu = slot.getStudentWpe().getStudent().getEducationOrgUnit().getGroupOrgUnit();

            return new SessionRatingSettingsKey(scale, fca.getGroup(), discipline, slot.getStudentWpe().getEppYearPart(), groupOu);
        }

        public static SessionRatingSettingsKey key(OrgUnit groupOu, EppStudentWpeCAction slot)
        {
            EppRegistryElementPart discipline = slot.getStudentWpe().getRegistryElementPart() ;
            EppFControlActionType fca = slot.getActionType();
            EppGradeScale scale = ISessionDocumentBaseDAO.instance.get().getGradeScale(slot);

            return new SessionRatingSettingsKey(scale, fca.getGroup(), discipline, slot.getStudentWpe().getEppYearPart(), groupOu);
        }

        @Override
        public boolean equals(Object o)
        {
            if (this == o) return true;
            if (!(o instanceof SessionRatingSettingsKey)) return false;
            SessionRatingSettingsKey that = (SessionRatingSettingsKey) o;
            return !(key != null ? !key.equals(that.key) : that.key != null);
        }

        @Override public int hashCode() { return key != null ? key.hashCode() : 0; }
        @Override public EppGradeScale getScale() { return scale; }
        @Override public EppFControlActionGroup getCaGroup() { return caGroup; }
        @Override public EppRegistryElementPart getDiscipline() { return discipline; }
        @Override public EppYearPart getYearPart() { return yearPart; }
        @Override public OrgUnit getGroupOu() { return groupOu; }
        public MultiKey getKey() { return key; }
    }

    public static ISessionRatingSettings getSettingsForDisabledBrs(final ISessionRatingSettingsKey key)
    {
        return new ISessionRatingSettings()
        {
            @Override public ISessionRatingSettingsKey key() { return key; }
            @Override public boolean usePoints() { return false; }
            @Override public boolean useCurrentRating() { return false; }
            @Override public ISessionPointsValidator pointsValidator() { return null; }
            @Override public IBrsCoefficientValue getCoefficientValue(String defCode) { return null; }
        };
    }

}
