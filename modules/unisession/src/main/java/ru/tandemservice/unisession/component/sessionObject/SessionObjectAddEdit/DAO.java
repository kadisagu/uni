// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisession.component.sessionObject.SessionObjectAddEdit;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.commonbase.base.util.INumberQueueDAO;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.base.bo.EducationYear.utils.EducationYearModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unisession.entity.document.SessionObject;

import java.util.Date;

/**
 * @author oleyba
 * @since 2/8/11
 */
public class DAO extends UniBaseDao implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        if (null!=model.getSessionObject().getId()) {
            model.setSessionObject(this.get(model.getSessionObject().getId()));
        } else if (model.getSessionObject().getOrgUnit() == null)
        {   // инициализируем
            model.getSessionObject().setOrgUnit(this.getNotNull(model.getOrgUnitId()));
            if (null != model.getEduYearId())
            {
                model.getSessionObject().setEducationYear(this.getNotNull(EducationYear.class, model.getEduYearId()));
            }
            else
            {
                model.getSessionObject().setEducationYear(EducationYear.getCurrentRequired());
            }
        }
        model.setYearModel(new EducationYearModel());
        model.setPartModel(HierarchyUtil.listHierarchyNodesWithParents(IUniBaseDao.instance.get().getCatalogItemListOrderByCode(YearDistributionPart.class), false));
    }

    @Override
    public void update(final Model model)
    {
        final SessionObject sessionObject = model.getSessionObject();
        final ErrorCollector err = UserContext.getInstance().getErrorCollector();
        if (!sessionObject.getDeadlineDate().after(sessionObject.getStartupDate())) {
            err.add("Дата окончания должна быть позже даты начала", "startupDate", "deadlineDate");
        }
        if (err.hasErrors()) {
            return;
        }
        if(null==sessionObject.getId())
        {
            sessionObject.setNumber(INumberQueueDAO.instance.get().getNextNumber(sessionObject));
            sessionObject.setFormingDate(new Date());
        }
        this.save(sessionObject);
    }
}
