package ru.tandemservice.unisession.base.bo.SessionListDocument.ui.Mark;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unisession.base.bo.SessionListDocument.SessionListDocumentManager;
import ru.tandemservice.unisession.base.bo.SessionListDocument.logic.ISessionListDocumentMarkDAO;
import ru.tandemservice.unisession.base.bo.SessionListDocument.logic.SessionListDocumentMarkCatalogItemHandler;
import ru.tandemservice.unisession.base.bo.SessionListDocument.logic.SessionListDocumentMarkRow;
import ru.tandemservice.unisession.base.bo.SessionListDocument.logic.SessionListDocumentMarksHandler;
import ru.tandemservice.unisession.base.bo.SessionMark.SessionMarkManager;
import ru.tandemservice.unisession.base.bo.SessionMark.logic.ISessionMarkDAO;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.dao.comission.ISessionCommissionDAO;
import ru.tandemservice.unisession.dao.document.ISessionDocumentBaseDAO;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.comission.SessionComission;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionListDocument;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author avedernikov
 * @since 19.07.2016
 */
@State({
		@Bind(key = UIPresenter.PUBLISHER_ID, binding = "listDocument.id")
		})
public class SessionListDocumentMarkUI extends UIPresenter
{
	private SessionListDocument listDocument = new SessionListDocument();

	private List<SessionListDocumentMarkRow> rows = new ArrayList<>();

	@Override
	public void onComponentRefresh()
	{
		ISessionListDocumentMarkDAO dao = SessionListDocumentManager.instance().markDAO();
		listDocument = dao.getNotNull(SessionListDocument.class, listDocument.getId());
		rows = dao.getListDocumentMarkRows(listDocument);
	}

	@Override
	public void onBeforeDataSourceFetch(IUIDataSource dataSource)
	{
		switch (dataSource.getName())
		{
			case SessionListDocumentMark.LIST_DOCUMENT_MARK_DS:
				dataSource.put(SessionListDocumentMarksHandler.PARAM_ROWS, rows);
				break;
			case SessionListDocumentMark.MARK_DS:
				SessionListDocumentMarkRow row = getCurrRow();
				dataSource.put(SessionListDocumentMarkCatalogItemHandler.PARAM_WPE_CACTION, row.getSlot().getStudentWpeCAction());
				dataSource.put(SessionListDocumentMarkCatalogItemHandler.PARAM_CURR_MARK_VALUE, row.getMarkValue());
				break;
		}
	}

	public void onClickApply()
	{
		DataAccessServices.dao().doInTransaction(session ->
												 {
													 for (SessionListDocumentMarkRow row : rows)
													 {
														 SessionDocumentSlot slot = row.getSlot();
														 SessionComission commission = ISessionCommissionDAO.instance.get().saveOrUpdateCommission(slot.getCommission(), row.getPpsList());
														 slot.setCommission(commission);
														 session.saveOrUpdate(slot);

														 SessionMarkManager.instance().regularMarkDao().saveOrUpdateMark(slot, new ISessionMarkDAO.MarkData()
														 {
															 @Override public Date getPerformDate() { return row.getPerformDate(); }
															 @Override public Double getPoints() { return row.getRating(); }
															 @Override public SessionMarkCatalogItem getMarkValue() { return row.getMarkValue(); }
															 @Override public String getComment() { return row.getComment(); }
														 });

														 ISessionDocumentBaseDAO.instance.get().updateProjectTheme(slot, row.getTheme(), null);
													 }
													 return null;
												 });
		deactivate();
	}

	public SessionListDocument getListDocument()
	{
		return listDocument;
	}

	public List<SessionListDocumentMarkRow> getRows()
	{
		return rows;
	}

	public boolean isCurrMarkSelected()
	{
		return getCurrRow().getMarkValue() != null;
	}

	public boolean isRatingVisible()
	{
		return ISessionBrsDao.instance.get().getSettings().isUseCurrentRatingForListDocument();
	}

	public boolean isCurrRatingDisabled()
	{
		return !(getCurrRow().getMarkValue() instanceof SessionMarkGradeValueCatalogItem);
	}

	public boolean isRatingRequired()
	{
		return ISessionBrsDao.instance.get().getSettings().isCurrentRatingRequiredForListDocument();
	}

	public boolean isCurrThemeDisabled()
	{
		return !getCurrRow().isThemeRequired();
	}

	public SessionListDocumentMarkRow getCurrRow()
	{
		return getConfig().getDataSourceCurrentRecord(SessionListDocumentMark.LIST_DOCUMENT_MARK_DS);
	}

	public Long getCurrentSlotId()
	{
		return getCurrRow().getId();
	}

	public boolean isThemeVisible()
	{
		return ISessionDocumentBaseDAO.instance.get().isThemeRequired(getListDocument());
	}

	private String getCurrSelectId(final String selectPrefix)
	{
		return selectPrefix + "_" + getCurrentSlotId();
	}

	public String getCurrRowMarkValueSelectId()
	{
		return getCurrSelectId("markValues");
	}

	public String getCurrRowRatingSelectId()
	{
		return getCurrSelectId("rating");
	}

	public String getCurrRowPpsListSelectId()
	{
		return getCurrSelectId("ppsList");
	}

	public String getCurrRowPerformDateSelectId()
	{
		return getCurrSelectId("performDate");
	}
}
