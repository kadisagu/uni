/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.SummaryBulletinList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.entity.report.UnisessionSummaryBulletinReport;

/**
 * @author oleyba
 * @since 12/16/11
 */
@Configuration
public class SessionReportSummaryBulletinList extends BusinessComponentManager
{
    public static final String DS_REPORTS = "sessionReportSummaryBulletinDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
            .addDataSource(SessionReportManager.instance().eduYearDSConfig())
            .addDataSource(SessionReportManager.instance().yearPartDSConfig())
            .addDataSource(this.searchListDS(DS_REPORTS, this.sessionSummaryBulletinDS(), this.sessionSummaryBulletinDSHandler()))
            .create();
    }

    @Bean
    public ColumnListExtPoint sessionSummaryBulletinDS() {
        return this.columnListExtPointBuilder(DS_REPORTS)
            .addColumn(indicatorColumn("icon").defaultIndicatorItem(new IndicatorColumn.Item("report", "Отчет")).create())
            .addColumn(publisherColumn("date", UnisessionSummaryBulletinReport.formingDate()).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).order().create())
            .addColumn(textColumn("year", UnisessionSummaryBulletinReport.sessionObject().educationYear().title().s()).create())
            .addColumn(textColumn("part", UnisessionSummaryBulletinReport.sessionObject().yearDistributionPart().title().s()).create())
            .addColumn(textColumn("exec", UnisessionSummaryBulletinReport.executor().s()).create())
            .addColumn(textColumn("course", UnisessionSummaryBulletinReport.course().s()).create())
            .addColumn(textColumn("groups", UnisessionSummaryBulletinReport.groups().s()).create())
            .addColumn(actionColumn("print", CommonDefines.ICON_PRINT, "onClickPrint"))
            .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER).permissionKey("ui:deleteStorableReportPermissionKey")
                .alert(FormattedMessage.with().template("sessionReportSummaryBulletinDS.delete.alert").parameter(UnisessionSummaryBulletinReport.formingDateStr().s()).create())
            )
        .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> sessionSummaryBulletinDSHandler() {
        return new SessionSummaryBulletinDSHandler(this.getName());
    }
}
