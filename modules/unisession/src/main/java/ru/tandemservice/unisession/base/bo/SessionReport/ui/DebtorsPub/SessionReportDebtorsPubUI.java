/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.DebtorsPub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport;

/**
 * @author oleyba
 * @since 1/24/12
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "report.id")
})
public class SessionReportDebtorsPubUI extends UIPresenter
{
    private UnisessionDebtorsReport _report = new UnisessionDebtorsReport();
    private CommonPostfixPermissionModelBase sec;

    @Override
    public void onComponentRefresh()
    {
        setReport(DataAccessServices.dao().getNotNull(UnisessionDebtorsReport.class, getReport().getId()));
        setSec(new OrgUnitSecModel(getReport().getSessionObject().getOrgUnit()));
    }

    // Listeners

    public void onClickDelete()
    {
        DataAccessServices.dao().delete(_report.getId());
        deactivate();
    }

    public void onClickPrint()
    {
        getActivationBuilder().asDesktopRoot(IUniComponents.DOWNLOAD_STORABLE_REPORT).parameter("reportId", _report.getId()).parameter("extension", "rtf").parameter("zip", Boolean.FALSE).activate();
    }

    // Getters & Setters


    public UnisessionDebtorsReport getReport()
    {
        return _report;
    }

    public void setReport(UnisessionDebtorsReport report)
    {
        _report = report;
    }

    public CommonPostfixPermissionModelBase getSec()
    {
        return sec;
    }

    public void setSec(CommonPostfixPermissionModelBase sec)
    {
        this.sec = sec;
    }

    public String getDeleteStorableReportPermissionKey(){ return getSec().getPermission("orgUnit_deleteSessionReportResultsList"); }
}
