/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.FinalQualWorkResultAdd.logic;

import jxl.write.*;
import jxl.write.Number;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniepp.entity.catalog.codes.EppRegistryStructureCodes;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.unisession.base.bo.SessionReport.logic.StateFinalAttestationUtil;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.StateFinalAttestationResultAdd.logic.SfaReportInfo;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.StateFinalAttestationResultAdd.logic.StateFinalAttestationResultPrintDAO;
import ru.tandemservice.unisession.entity.catalog.SessionRecommendation4FQW;
import ru.tandemservice.unisession.entity.catalog.SessionSource4FQWTheme;
import ru.tandemservice.unisession.entity.report.StateFinalAttestationResult;
import ru.tandemservice.unisession.entity.stateFinalExam.SessionFQWProtocol;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static ru.tandemservice.unisession.base.bo.SessionReport.logic.StateFinalAttestationUtil.*;

/**
 * @author Andrey Andreev
 * @since 03.11.2016
 */
public class FinalQualWorkResultPrintDAO extends StateFinalAttestationResultPrintDAO<Raw, Row>
{
    @Override
    public ReportInfo initReportInfo(StateFinalAttestationResult report)
    {
        ReportInfo reportInfo = new ReportInfo(report);

        DQLSelectBuilder slotBuilder = getSlotSelectBuilder(reportInfo)
                .where(eq(property(REG_ALIAS, EppRegistryElement.parent().code()), value(EppRegistryStructureCodes.REGISTRY_ATTESTATION_DIPLOMA)));

        slotBuilder.joinEntity(SLOT_ALIAS, DQLJoinType.left, SessionFQWProtocol.class, "ptl",
                               eq(property("ptl", SessionFQWProtocol.documentSlot()), property(SLOT_ALIAS)));

        slotBuilder.column(property(EOU_ALIAS))
                .column(property(WPE_ALIAS))
                .column(property(MARK_ALIAS))
                .column(property("ptl"));

        List<Object[]> slots = getList(slotBuilder);
        if (slots.isEmpty())
            throw new ApplicationException("Нет данных для построения отчета. Нет студентов с выбранными параметрами.");

        reportInfo.setSource4FQWThemeList(getCatalogItemList(SessionSource4FQWTheme.class));
        reportInfo.setRecommendation4FQWList(getCatalogItemList(SessionRecommendation4FQW.class));
        reportInfo.setRaws(StateFinalAttestationUtil.filterRaws(slots.stream().map(Raw::new)));

        return reportInfo;
    }


    @Override
    protected void init() throws WriteException
    {
        super.init();
        _titleColumnDataTableStyle.setAlignment(jxl.format.Alignment.CENTRE);
        _totalTitleColumnDataTableStyle.setAlignment(jxl.format.Alignment.CENTRE);

        _tableWidth = 21;
    }


    @Override
    protected String getFileName(StateFinalAttestationResult report)
    {
        return "Отчет по результатам защиты выпускной квалификационной работы.xls";
    }

    @Override
    protected int printTitle(StateFinalAttestationResult report, WritableSheet sheet)
    {
        int row = 0;

        int indent = 1;

        addTextCell(sheet, indent, row, _tableWidth - indent, 1, "Отчет по результатам защиты выпускной квалификационной работы", _titleStyle);

        return ++row;
    }

    @Override
    protected int getPropertyTitleWidth()
    {
        return 5;
    }

    @Override
    protected int addHeaderDataTable(SfaReportInfo<Raw> sfaReportInfo, WritableSheet sheet, int rowIndex)
    {
        int firstRow = rowIndex;
        int col = 0;

        try
        {
            sheet.setRowView(rowIndex, 700);
            sheet.setRowView(rowIndex + 1, 2000);

            sheet.setColumnView(col, 5);
            addTextCell(sheet, col++, rowIndex, 1, 2, "№", _horHeaderDataTableStyle);

            sheet.setColumnView(col, 20);
            addTextCell(sheet, col++, rowIndex, 1, 2, "Дата проведения", _horHeaderDataTableStyle);

            addTextCell(sheet, col, rowIndex, 4, 1, "Количество выпускников", _horHeaderDataTableStyle);
            sheet.addCell(new Label(col++, rowIndex + 1, "Всего", _verHeaderDataTableStyle));
            sheet.addCell(new Label(col++, rowIndex + 1, "Недопущены", _verHeaderDataTableStyle));
            sheet.addCell(new Label(col++, rowIndex + 1, "Явились на защиту", _verHeaderDataTableStyle));
            sheet.addCell(new Label(col++, rowIndex + 1, "Защитили", _verHeaderDataTableStyle));

            addTextCell(sheet, col++, rowIndex, 1, 2, "Из них (графа 6) \nобучаются на бюджете", _verHeaderDataTableStyle);

            addTextCell(sheet, col, rowIndex, 4, 1, "Из них получили оценки", _horHeaderDataTableStyle);
            sheet.addCell(new Number(col++, rowIndex + 1, 5, _horHeaderDataTableStyle));
            sheet.addCell(new Number(col++, rowIndex + 1, 4, _horHeaderDataTableStyle));
            sheet.addCell(new Number(col++, rowIndex + 1, 3, _horHeaderDataTableStyle));
            sheet.addCell(new Number(col++, rowIndex + 1, 2, _horHeaderDataTableStyle));

            addTextCell(sheet, col++, rowIndex, 1, 2, "Успеваемость, %", _verHeaderDataTableStyle);
            addTextCell(sheet, col++, rowIndex, 1, 2, "Качественный \r\nпоказатель, %", _verHeaderDataTableStyle);
            addTextCell(sheet, col++, rowIndex, 1, 2, "Количество дипломов \r\nс отличием", _verHeaderDataTableStyle);
            addTextCell(sheet, col++, rowIndex, 1, 2, "Качественный показатель \n(дипл. с отл.)", _verHeaderDataTableStyle);

            ReportInfo reportInfo = (ReportInfo) sfaReportInfo;
            List<SessionSource4FQWTheme> source4FQWThemeList = reportInfo.getSource4FQWThemeList();
            sheet.mergeCells(col, rowIndex, col + source4FQWThemeList.size() - 1, rowIndex);
            sheet.addCell(new Label(col, rowIndex, "Количество ВКР выполненных", _horHeaderDataTableStyle));
            for (SessionSource4FQWTheme theme : source4FQWThemeList)
                sheet.addCell(new Label(col++, rowIndex + 1, theme.getTitle(), _verHeaderDataTableStyle));

            List<SessionRecommendation4FQW> recommendation4FQWList = reportInfo.getRecommendation4FQWList();
            sheet.mergeCells(col, rowIndex, col + recommendation4FQWList.size() - 1, rowIndex);
            sheet.addCell(new Label(col, rowIndex, "Количество ВКР рекомендованных", _horHeaderDataTableStyle));
            for (SessionRecommendation4FQW recommendation : recommendation4FQWList)
                sheet.addCell(new Label(col++, rowIndex + 1, recommendation.getTitle(), _verHeaderDataTableStyle));


            _tableWidth = col - 1;

            rowIndex = rowIndex + 2;
            for (; col > 0; col--)// номера колонок
                sheet.addCell(new Number(col - 1, rowIndex, col, _horHeaderBoldDataTableStyle));

            fillArea(sheet, 0, _tableWidth, firstRow - 1, firstRow - 1, _topThickBoardTS);
            fillArea(sheet, _tableWidth + 1, _tableWidth + 1, firstRow, rowIndex, _rightThickTableBoardTS);
        }
        catch (WriteException e)
        {
            e.printStackTrace();
        }

        return ++rowIndex;
    }

    @Override
    protected List<Row> getRows(SfaReportInfo<Raw> sfaReportInfo)
    {
        TreeMap<Date, List<Raw>> rawsByDate = sfaReportInfo.getRaws()
                .stream()
                .filter(raw -> raw.getMark() != null)
                .collect(Collectors.groupingBy(
                        raw -> raw.getMark().getPerformDate(),
                        TreeMap::new,
                        Collectors.mapping(raw -> raw, Collectors.toList())
                ));

        return rawsByDate.entrySet().stream()
                .map(e ->
                     {
                         List<Raw> raw4RegEl = e.getValue();
                         Row row = newRegElRow(sfaReportInfo, DateFormatter.DEFAULT_DATE_FORMATTER.format(e.getKey()));
                         row.addFromRaw(raw4RegEl);
                         return row;
                     })
                .collect(Collectors.toList());
    }

    @Override
    protected void addDataRow(WritableSheet sheet, int startCol, int rowIndex, Row row)
    {
        addIntCell(sheet, startCol++, rowIndex, row.getTotal(), row.getIntStyle());
        addIntCell(sheet, startCol++, rowIndex, row.getNotAllowed(), row.getIntStyle());
        addIntCell(sheet, startCol++, rowIndex, row.getAppeared(), row.getIntStyle());
        addIntCell(sheet, startCol++, rowIndex, row.getPassed(), row.getIntStyle());

        addIntCell(sheet, startCol++, rowIndex, row.getBudget(), row.getIntStyle());

        addIntCell(sheet, startCol++, rowIndex, row.getMark5(), row.getIntStyle());
        addIntCell(sheet, startCol++, rowIndex, row.getMark4(), row.getIntStyle());
        addIntCell(sheet, startCol++, rowIndex, row.getMark3(), row.getIntStyle());
        addIntCell(sheet, startCol++, rowIndex, row.getMark2(), row.getRedIntStyle());

        addDoubleCell(sheet, startCol++, rowIndex, row.getProgress(), row.getDoubleStyle());
        addDoubleCell(sheet, startCol++, rowIndex, row.getGoodProgress(), row.getDoubleStyle());

        addIntCell(sheet, startCol++, rowIndex, row.getHonors(), row.getIntStyle());
        addDoubleCell(sheet, startCol++, rowIndex, row.getHonorsProgress(), row.getDoubleStyle());

        for (int[] value : row.getThemesNumbers().values())
            addIntCell(sheet, startCol++, rowIndex, value[0], row.getIntStyle());

        for (int[] value : row.getRecommendationsNumbers().values())
            addIntCell(sheet, startCol++, rowIndex, value[0], row.getIntStyle());
    }

    @Override
    protected int printNote(SfaReportInfo<Raw> reportInfo, WritableSheet sheet, int row)
    {
        addTextCell(sheet, 0, row++, 2, 1, "* Примечание: ", _noteStyle);
        addTextCell(sheet, 1, row++, 10, 1, "1) Значения в столбцах : \"Успеваемость\", \"Качественный показатель\" и \"Качественный показатель (дипл. с отл.)\" расчитываются автоматически.", _noteStyle);

        return row;
    }

    @Override
    protected Row newRegElRow(SfaReportInfo<Raw> sfaReportInfo, String title)
    {
        ReportInfo reportInfo = (ReportInfo) sfaReportInfo;
        return new Row(title, _titleColumnDataTableStyle, _intColumnDataTableStyle, _intRedColumnDataTableStyle, _doubleColumnDataTableStyle)
                .initProtocolsMap(reportInfo);
    }

    @Override
    protected Row newTotalDirectionRow(SfaReportInfo<Raw> sfaReportInfo, String title)
    {
        ReportInfo reportInfo = (ReportInfo) sfaReportInfo;
        return new Row(title, _totalTitleColumnDataTableStyle, _totalIntColumnDataTableStyle, _totalRedIntColumnDataTableStyle, _totalDoubleColumnDataTableStyle)
                .initProtocolsMap(reportInfo);
    }
}