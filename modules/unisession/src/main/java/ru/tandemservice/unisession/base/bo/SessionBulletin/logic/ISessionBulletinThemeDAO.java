package ru.tandemservice.unisession.base.bo.SessionBulletin.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;

import java.util.List;

/**
 * @author avedernikov
 * @since 08.08.2016
 */
public interface ISessionBulletinThemeDAO extends ICommonDAO, INeedPersistenceSupport
{
	/**
	 * Получить список строк, содержащих информацию о теме работы студента и руководителе ({@link SessionBulletinThemeRow}).
	 * @param bulletin Экзаменационная ведомость, для которой требуется информация о темах и руководителях.
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	List<SessionBulletinThemeRow> getBulletinThemeRows(final SessionBulletinDocument bulletin);

	/**
	 * Обновить данные о ВКР у студента (поля "Тема ВКР", "Руководитель ВКР" и "Руководитель ВКР (текстом)").
	 * @param rows Список строк, содержащих информацию о теме работы студента и руководителе ({@link SessionBulletinThemeRow}) тех студентов, данные которых нужно обновить.
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	void updateFinalQualifyingWorkForStudents(List<SessionBulletinThemeRow> rows);
}
