package ru.tandemservice.unisession.entity.document.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unisession.entity.document.SessionTransferDocument;
import ru.tandemservice.unisession.entity.document.SessionTransferInsideDocument;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Внутреннее перезачтение
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionTransferInsideDocumentGen extends SessionTransferDocument
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.document.SessionTransferInsideDocument";
    public static final String ENTITY_NAME = "sessionTransferInsideDocument";
    public static final int VERSION_HASH = -1755106734;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof SessionTransferInsideDocumentGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionTransferInsideDocumentGen> extends SessionTransferDocument.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionTransferInsideDocument.class;
        }

        public T newInstance()
        {
            return (T) new SessionTransferInsideDocument();
        }
    }
    private static final Path<SessionTransferInsideDocument> _dslPath = new Path<SessionTransferInsideDocument>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionTransferInsideDocument");
    }
            

    public static class Path<E extends SessionTransferInsideDocument> extends SessionTransferDocument.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return SessionTransferInsideDocument.class;
        }

        public String getEntityName()
        {
            return "sessionTransferInsideDocument";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
