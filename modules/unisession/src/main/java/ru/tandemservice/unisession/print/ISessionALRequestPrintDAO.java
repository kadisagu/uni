/* $Id$ */
package ru.tandemservice.unisession.print;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.rtf.document.RtfDocument;

/**
 * @author Andrey Andreev
 * @since 17.11.2015
 */
public interface ISessionALRequestPrintDAO extends INeedPersistenceSupport
{
    RtfDocument printRequest(Long requestId);

}