/* $Id$ */
package ru.tandemservice.unisession.component.orgunit.SessionRetakeDocTab;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.unisession.entity.document.SessionRetakeDocument;

import java.util.Collection;
import java.util.List;

/**
 * @author oleyba
 * @since 6/13/11
 */
@Input({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "orgUnitHolder.id")
})
public class Model
{
    public static final Long BULL_STATUS_EMPTY_ID = 1L;
    public static final Long BULL_STATUS_OPEN_ID = 2L;
    public static final Long BULL_STATUS_CLOSE_ID = 3L;

    private final EntityHolder<OrgUnit> orgUnitHolder = new EntityHolder<OrgUnit>();
    private DynamicListDataSource<SessionRetakeDocument> dataSource;

    private CommonPostfixPermissionModel sec;
    private IDataSettings settings;

    private ISelectModel yearModel;
    private List<HSelectOption> partsList;
    private ISelectModel registryElementModel;
    private ISelectModel groupCurrentModel;
    private ISelectModel bullStatusModel;
    private ISelectModel registryOwnerModel;
    private ISelectModel registryStructureModel;
    private ISelectModel courseCurrentModel;

    public Long getOrgUnitId()
    {
        return this.getOrgUnitHolder().getId();
    }

    public OrgUnit getOrgUnit()
    {
        return this.getOrgUnitHolder().getValue();
    }

    public EducationYear getYear()
    {
        final Object year = this.getSettings().get("year");
        return year instanceof EducationYear ? (EducationYear) year : null;
    }

    public YearDistributionPart getPart()
    {
        final Object part = this.getSettings().get("part");
        return part instanceof YearDistributionPart ? (YearDistributionPart) part : null;
    }

    @SuppressWarnings("unchecked")
    public Collection<EppRegistryElement> getRegistryElements()
    {
        final Object registryElement = this.getSettings().get("registryElement");
        return (registryElement instanceof Collection && ((Collection) registryElement).size() > 0) ? (Collection) registryElement : null;
    }

    @SuppressWarnings("unchecked")
    public Collection<Group> getGroupCurrentList()
    {
        final Object group = this.getSettings().get("groupCurrentList");
        return (group instanceof Collection && ((Collection) group).size() > 0) ? (Collection) group : null;
    }

    @SuppressWarnings("unchecked")
    public String getNumber()
    {
        return StringUtils.trimToNull(this.getSettings().get("docNumber"));
    }

    public DataWrapper getBullStatus()
    {
        final Object bullStatus = getSettings().get("bullStatus");
        return bullStatus instanceof DataWrapper ? (DataWrapper) bullStatus : null;
    }

    @SuppressWarnings("unchecked")
    protected Collection<OrgUnit> getRegistryOwners()
    {
        final Object registryOwner = this.getSettings().get("registryOwner");
        return (registryOwner instanceof Collection && ((Collection) registryOwner).size() > 0) ? (Collection) registryOwner : null;
    }

    @SuppressWarnings("unchecked")
    protected Collection<EppRegistryStructure> getRegistryStructureList()
    {
        final Object bullStatus = this.getSettings().get("registryStructure");
        return (bullStatus instanceof Collection && ((Collection) bullStatus).size() > 0) ? (Collection) bullStatus : null;
    }

    @SuppressWarnings("unchecked")
    protected Collection<Course> getCourseCurrentList()
    {
        final Object courseCurrent = this.getSettings().get("courseCurrentList");
        return (courseCurrent instanceof Collection && ((Collection) courseCurrent).size() > 0) ? (Collection) courseCurrent : null;
    }

    protected String getStudentLastName()
    {
        final Object studLastName = this.getSettings().get("studLastName");
        return (String) studLastName;
    }

    protected String getPpsLastName()
    {
        final Object ppsLastName = this.getSettings().get("ppsLastName");
        return (String) ppsLastName;
    }


    // acessors


    public EntityHolder<OrgUnit> getOrgUnitHolder()
    {
        return this.orgUnitHolder;
    }

    public DynamicListDataSource<SessionRetakeDocument> getDataSource()
    {
        return this.dataSource;
    }

    public void setDataSource(final DynamicListDataSource<SessionRetakeDocument> dataSource)
    {
        this.dataSource = dataSource;
    }

    public CommonPostfixPermissionModel getSec()
    {
        return this.sec;
    }

    public void setSec(final CommonPostfixPermissionModel sec)
    {
        this.sec = sec;
    }

    public IDataSettings getSettings()
    {
        return this.settings;
    }

    public void setSettings(final IDataSettings settings)
    {
        this.settings = settings;
    }

    public ISelectModel getYearModel()
    {
        return this.yearModel;
    }

    public void setYearModel(final ISelectModel yearModel)
    {
        this.yearModel = yearModel;
    }

    public List<HSelectOption> getPartsList()
    {
        return this.partsList;
    }

    public void setPartsList(final List<HSelectOption> partsList)
    {
        this.partsList = partsList;
    }

    public ISelectModel getRegistryElementModel()
    {
        return this.registryElementModel;
    }

    public void setRegistryElementModel(final ISelectModel registryElementModel)
    {
        this.registryElementModel = registryElementModel;
    }

    public ISelectModel getGroupCurrentModel()
    {
        return groupCurrentModel;
    }

    public void setGroupCurrentModel(ISelectModel groupCurrentModel)
    {
        this.groupCurrentModel = groupCurrentModel;
    }

    public ISelectModel getBullStatusModel()
    {
        return bullStatusModel;
    }

    public void setBullStatusModel(ISelectModel bullStatusModel)
    {
        this.bullStatusModel = bullStatusModel;
    }

    public ISelectModel getRegistryOwnerModel()
    {
        return registryOwnerModel;
    }

    public void setRegistryOwnerModel(ISelectModel registryOwnerModel)
    {
        this.registryOwnerModel = registryOwnerModel;
    }

    public ISelectModel getRegistryStructureModel()
    {
        return registryStructureModel;
    }

    public void setRegistryStructureModel(ISelectModel registryStructureModel)
    {
        this.registryStructureModel = registryStructureModel;
    }

    public ISelectModel getCourseCurrentModel()
    {
        return courseCurrentModel;
    }

    public void setCourseCurrentModel(ISelectModel courseCurrentModel)
    {
        this.courseCurrentModel = courseCurrentModel;
    }
}
