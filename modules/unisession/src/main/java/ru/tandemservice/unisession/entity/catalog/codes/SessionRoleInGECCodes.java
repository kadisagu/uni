package ru.tandemservice.unisession.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Роль в составе ГЭК"
 * Имя сущности : sessionRoleInGEC
 * Файл data.xml : session.data.xml
 */
public interface SessionRoleInGECCodes
{
    /** Константа кода (code) элемента : Председатель ГЭК (title) */
    String CHAIRMAN = "session.GECrole.1";
    /** Константа кода (code) элемента : Член ГЭК (title) */
    String MEMBER = "session.GECrole.2";
    /** Константа кода (code) элемента : Секретарь ГЭК (title) */
    String SECRETARY = "session.GECrole.3";

    Set<String> CODES = ImmutableSet.of(CHAIRMAN, MEMBER, SECRETARY);
}
