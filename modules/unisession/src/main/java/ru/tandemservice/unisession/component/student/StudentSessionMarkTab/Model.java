/* $Id$ */
package ru.tandemservice.unisession.component.student.StudentSessionMarkTab;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;

/**
 * @author oleyba
 * @since 4/8/11
 */
@State({@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "student.id")})
public class Model
{
    private Student student = new Student();

    private ISelectModel stateModel;
    private ISelectModel termModel;
    private ISelectModel _showPositiveModel;

    private DynamicListDataSource<EppStudentWpeCAction> dataSource;

    private IDataSettings settings;

    private boolean showCurrentRating;
    private boolean showPoints;



    public Student getStudent()
    {
        return this.student;
    }

    public void setStudent(final Student student)
    {
        this.student = student;
    }

    public ISelectModel getStateModel()
    {
        return this.stateModel;
    }

    public void setStateModel(final ISelectModel stateModel)
    {
        this.stateModel = stateModel;
    }

    public ISelectModel getTermModel()
    {
        return this.termModel;
    }

    public void setTermModel(final ISelectModel termModel)
    {
        this.termModel = termModel;
    }

    public ISelectModel getShowPositiveModel()
    {
        return _showPositiveModel;
    }

    public void setShowPositiveModel(ISelectModel showPositiveModel)
    {
        _showPositiveModel = showPositiveModel;
    }


    public DynamicListDataSource<EppStudentWpeCAction> getDataSource()
    {
        return this.dataSource;
    }

    public void setDataSource(final DynamicListDataSource<EppStudentWpeCAction> dataSource)
    {
        this.dataSource = dataSource;
    }

    public IDataSettings getSettings()
    {
        return this.settings;
    }

    public void setSettings(final IDataSettings settings)
    {
        this.settings = settings;
    }

    public boolean isShowCurrentRating()
    {
        return showCurrentRating;
    }

    public void setShowCurrentRating(boolean showCurrentRating)
    {
        this.showCurrentRating = showCurrentRating;
    }

    public boolean isShowPoints()
    {
        return showPoints;
    }

    public void setShowPoints(boolean showPoints)
    {
        this.showPoints = showPoints;
    }
}
