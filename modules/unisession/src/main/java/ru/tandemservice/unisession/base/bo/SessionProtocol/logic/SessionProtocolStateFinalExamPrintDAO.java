/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionProtocol.logic;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import ru.tandemservice.unisession.entity.catalog.UnisessionCommonTemplate;
import ru.tandemservice.unisession.entity.catalog.codes.UnisessionCommonTemplateCodes;

/**
 * @author Andrey Andreev
 * @since 20.09.2016
 */
public class SessionProtocolStateFinalExamPrintDAO extends AbstractSessionProtocolPrintDAO
{

    @Override
    protected RtfDocument getTemplate()
    {
        UnisessionCommonTemplate templateItem = get(UnisessionCommonTemplate.class, UnisessionCommonTemplate.code().s(), UnisessionCommonTemplateCodes.SESSION_STATE_EXAM_PROTOCOL);
        return new RtfReader().read(templateItem.getContent());
    }
}