/**
 *$Id$
 */
package ru.tandemservice.unisession.base.ext.EppEduGroup.ui.List4StudentIds;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtension;
import org.tandemframework.core.view.formatter.PropertyFormatter;
import org.tandemframework.shared.commonbase.base.util.SimplePublisherLinkResolver;
import ru.tandemservice.uniepp.base.bo.EppEduGroup.ui.List4StudentIds.EppEduGroupList4StudentIds;
import ru.tandemservice.unisession.base.ext.EppEduGroup.logic.EppEduGroupList4StudentIdsExtSessionDSHandler;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;

/**
 * @author Alexander Shaburov
 * @since 17.07.13
 */
@Configuration
public class EppEduGroupList4StudentIdsExt extends BusinessComponentExtensionManager
{
    @Autowired
    public EppEduGroupList4StudentIds _eppEduGroupList4StudentIds;

    @Bean
    public ColumnListExtension eduGroupListDSColumnExtension()
    {
        return columnListExtensionBuilder(_eppEduGroupList4StudentIds.eduGroupListDSColumnExtPoint())
                .addColumn(publisherColumn("bulletin", EppEduGroupList4StudentIdsExtSessionDSHandler.COLUMN_BULLETIN).publisherLinkResolver(new SimplePublisherLinkResolver(EppEduGroupList4StudentIdsExtSessionDSHandler.COLUMN_BULLETIN + ".id")).formatter(new PropertyFormatter(SessionBulletinDocument.typeTitle().s())))
                .create();
    }
}
