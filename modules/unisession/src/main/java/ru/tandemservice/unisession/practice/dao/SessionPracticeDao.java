/* $Id$ */
package ru.tandemservice.unisession.practice.dao;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinMark.SessionMarkableDocHolder;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;

import java.util.Collection;
import java.util.Map;

/**
 * @author Nikolay Fedorovskih
 * @since 05.11.2014
 */
public class SessionPracticeDao extends UniBaseDao implements ISessionPracticeDao
{
    @Override
    public boolean showPracticeTutorMarkColumnInBulletin(SessionMarkableDocHolder.IMarkableDoc document)
    {
        return false;
    }

    @Override
    public Map<Long, IPracticeResult> getPracticeTutorMarks(Collection<Long> studentWpeCActionIds)
    {
        return null;
    }

}