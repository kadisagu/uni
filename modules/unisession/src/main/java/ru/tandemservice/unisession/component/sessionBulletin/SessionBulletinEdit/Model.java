/* $*/

package ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinEdit;

import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.PpsEntry.util.PpsEntrySelectBlockData;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;

import java.util.List;

/**
 * @author oleyba
 * @since 3/3/11
 */
@Input(keys = PublisherActivator.PUBLISHER_ID_KEY, bindings = "bulletin.id")

public class Model
{
    public static final String COMPONENT_NAME = Model.class.getPackage().getName();
    private SessionBulletinDocument bulletin = new SessionBulletinDocument();
    private PpsEntrySelectBlockData ppsData;

    public SessionBulletinDocument getBulletin()
    {
        return this.bulletin;
    }

    public void setBulletin(final SessionBulletinDocument bulletin)
    {
        this.bulletin = bulletin;
    }

    public PpsEntrySelectBlockData getPpsData()
    {
        return ppsData;
    }

    public void setPpsData(PpsEntrySelectBlockData ppsData)
    {
        this.ppsData = ppsData;
    }

    public List<PpsEntry> getPpsList()
    {
        return getPpsData().getSelectedPpsList();
    }

    public void setPpsList(final List<PpsEntry> ppsList)
    {
        getPpsData().setSelectedPps(ppsList);
    }

    public OrgUnit getOrgUnit ()
    {
        return getBulletin().getSessionObject().getOrgUnit();
    }
}
