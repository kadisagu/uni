/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionDocument.ui.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.hibernate.Session;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.catalog.EppWorkPlanRowKind;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.base.bo.SessionDocument.ui.MarkListTab.SessionDocumentMarkListTab;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.*;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.mark.SessionSlotLinkMark;
import ru.tandemservice.unisession.entity.mark.SessionSlotMarkGradeValue;
import ru.tandemservice.unisession.entity.mark.SessionSlotRegularMark;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.unisession.base.bo.SessionDocument.ui.MarkListTab.SessionDocumentMarkListTabUI.*;

/**
 * @author Alexey Lopatin
 * @since 12.05.2015
 */
public class SessionDocumentMarkListDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String PROPERTY_ELEMENT = "element";
    public static final String PROPERTY_ACTION = "action";
    public static final String PROPERTY_GROUP = "group";
    public static final String PROPERTY_EOU = "eou";
    public static final String PROPERTY_SESSION_MARK = "sessionMark";
    public static final String PROPERTY_TOTAL_MARK = "totalMark";
    public static final String PROPERTY_TEACHER = "teacher";
    public static final String PROPERTY_CHECKED_GRADE_BOOK = "checkedGradeBook";
    public static final String PROPERTY_CHECKED_GRADE_BOOK_DISABLED = "checkedGradeBookDisabled";
    public static final String PROPERTY_SHEET_DOCUMENT = "sheetDocument";
    public static final String PROPERTY_RETAKE_DOCUMENT = "retakeDocument";
    public static final String PROPERTY_RETAKE_COUNT = "retakeCount";

    public SessionDocumentMarkListDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, final ExecutionContext context)
    {
        final Long orgUnitId = context.get(PARAM_ORG_UNIT_ID);

        EducationYear year = context.get(PARAM_YEAR);
        YearDistributionPart part = context.get(PARAM_PART);
        final String docNumber = context.get(PARAM_DOC_NUMBER);
        String studentLastName = context.get(PARAM_STUDENT_LAST_NAME);
        String studentFirstName = context.get(PARAM_STUDENT_FIRST_NAME);
        IdentifiableWrapper statusMark = context.get(PARAM_STATUS_MARK);
        List<SessionMarkCatalogItem> sessionMarkList = context.get(PARAM_SESSION_MARK_LIST);
        List<EppWorkPlanRowKind> workPlanRowKindList = context.get(PARAM_WORK_PLAN_ROW_KIND_LIST);
        List<Course> courseList = context.get(PARAM_COURSE_LIST);
        List<Group> groupList = context.get(PARAM_GROUP_LIST);
        DataWrapper chGradeBook = context.get(PARAM_CHECKED_GRADE_BOOK);
        String teacher = context.get(PARAM_TEACHER);

        final List<Object> resultList = Lists.newArrayList();

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EppStudentWorkPlanElement.class, "e")
                .column(property("e"))
                .column(property("a"))
                .column(property("group"))
                .column(property("eou"))
                .joinPath(DQLJoinType.inner, EppStudentWorkPlanElement.student().fromAlias("e"), "st")
                .joinPath(DQLJoinType.inner, Student.group().fromAlias("st"), "group")
                .joinPath(DQLJoinType.inner, Group.educationOrgUnit().fromAlias("group"), "eou")
                .joinEntity("e", DQLJoinType.inner, EppStudentWpeCAction.class, "a", eq(property("e", EppStudentWorkPlanElement.id()), property("a", EppStudentWpeCAction.studentWpe().id())))
                .where(eq(property("eou", EducationOrgUnit.formativeOrgUnit().id()), value(orgUnitId)))
                .where(isNull(property("e", EppStudentWorkPlanElement.removalDate())))
                .where(isNull(property("a", EppStudentWpeCAction.removalDate())));

        FilterUtils.applySelectFilter(builder, "e", EppStudentWorkPlanElement.year().educationYear().s(), year);
        FilterUtils.applySelectFilter(builder, "e", EppStudentWorkPlanElement.part().s(), part);
        FilterUtils.applySimpleLikeFilter(builder, "st", Student.person().identityCard().lastName().s(), studentLastName);
        FilterUtils.applySimpleLikeFilter(builder, "st", Student.person().identityCard().firstName().s(), studentFirstName);
        FilterUtils.applySelectFilter(builder, "e", EppStudentWorkPlanElement.sourceRow().kind().s(), workPlanRowKindList);
        FilterUtils.applySelectFilter(builder, "st", Student.course().s(), courseList);
        FilterUtils.applySelectFilter(builder, "st", Student.group().s(), groupList);

        if (null != statusMark)
        {
            DQLSelectBuilder markDql = getMarkDql("mark")
                    .where(in(property("document", SessionDocument.id()), new DQLSelectBuilder().fromEntity(SessionStudentGradeBookDocument.class, "g").column(property("g", SessionStudentGradeBookDocument.id())).buildQuery()));

            Long statusId = statusMark.getId();
            if (statusId.equals(SessionDocumentMarkListTab.MARK_EMPTY_ITEM.getId()))
            {
                builder.where(notExists(markDql.buildQuery()));
            }
            else if (statusId.equals(SessionDocumentMarkListTab.RETAKE_ITEM.getId()))
            {
                builder.where(exists(markDql
                                             .where(eq(property("docSlot", SessionDocumentSlot.inSession()), value(Boolean.FALSE)))
                                             .where(eq(property("mark", SessionMark.cachedMarkPositiveStatus()), value(Boolean.FALSE)))
                                             .buildQuery()
                ));
            }
            else if (statusId.equals(SessionDocumentMarkListTab.NOT_RETAKE_ITEM.getId()))
            {
                builder.where(exists(markDql
                                             .where(eq(property("docSlot", SessionDocumentSlot.inSession()), value(Boolean.FALSE)))
                                             .where(eq(property("mark", SessionMark.cachedMarkPositiveStatus()), value(Boolean.TRUE)))
                                             .buildQuery()
                ));
            }
            else if (statusId.equals(SessionDocumentMarkListTab.MARK_POSITIVE_ITEM.getId()))
            {
                builder.where(exists(markDql
                                             .where(eq(property("docSlot", SessionDocumentSlot.inSession()), value(Boolean.FALSE)))
                                             .where(eq(property("mark", SessionMark.cachedMarkPositiveStatus()), value(Boolean.TRUE)))
                                             .where(eq(property("mark", SessionMark.cachedMarkValue().cachedPositiveStatus()), value(Boolean.TRUE)))
                                             .buildQuery()
                ));
            }
        }
        if (null != sessionMarkList && !sessionMarkList.isEmpty())
        {
            builder.where(exists(
                    getMarkDql("mark")
                            .where(in(property("document", SessionDocument.id()), new DQLSelectBuilder().fromEntity(SessionStudentGradeBookDocument.class, "g").column(property("g", SessionStudentGradeBookDocument.id())).buildQuery()))
                            .where(eq(property("docSlot", SessionDocumentSlot.inSession()), value(Boolean.FALSE)))
                            .where(in(property("mark", SessionMark.cachedMarkValue()), sessionMarkList))
                            .buildQuery()
            ));
        }
        if (null != chGradeBook)
        {
            boolean value = chGradeBook.getId().equals(TwinComboDataSourceHandler.YES_ID);
            builder.where(exists(
                    getMarkDql("mark")
                            .joinEntity("mark", DQLJoinType.inner, SessionSlotMarkGradeValue.class, "mark_v", eq(property("mark.id"), property("mark_v.id")))
                            .where(eq(property("mark", SessionMark.cachedMarkPositiveStatus()), value(Boolean.TRUE)))
                            .where(eq(property("mark_v", SessionSlotMarkGradeValue.checkedGradeBook()), value(value)))
                            .buildQuery()
            ));

            if (value)
            {
                builder.where(notExists(
                        getMarkDql("mark")
                                .joinEntity("mark", DQLJoinType.inner, SessionSlotMarkGradeValue.class, "mark_v", eq(property("mark.id"), property("mark_v.id")))
                                .where(eq(property("mark", SessionMark.cachedMarkPositiveStatus()), value(Boolean.TRUE)))
                                .where(eq(property("mark_v", SessionSlotMarkGradeValue.checkedGradeBook()), value(Boolean.FALSE)))
                                .buildQuery()
                ));
            }
        }
        if (null != teacher)
        {
            builder.where(exists(
                    getMarkDql("mark")
                            .joinEntity("docSlot", DQLJoinType.inner, SessionComissionPps.class, "c_pps", eq(property("docSlot", SessionDocumentSlot.commission()), property("c_pps", SessionComissionPps.commission())))
                            .where(likeUpper(property("c_pps", SessionComissionPps.pps().person().identityCard().lastName()), value(CoreStringUtils.escapeLike(teacher, true))))
                            .buildQuery()
            ));
        }

        builder
                .order(property("st", Student.course()))
                .order(property("st", Student.group()))
                .order(property("st", Student.person().identityCard().lastName()))
                .order(property("st", Student.person().identityCard().firstName()))
                .order(property("st", Student.person().identityCard().middleName()))
                .order(property("e", EppStudentWorkPlanElement.registryElementPart().registryElement().title()));

        DQLExecutionContext dqlContext = new DQLExecutionContext(context.getSession());
        int totalSize = ((Long) builder.createCountStatement(dqlContext).uniqueResult()).intValue();
        int startRec = input.getStartRecord();
        int countRec = input.getCountRecord();
        if (-1 == startRec || totalSize < startRec)
        {
            startRec = totalSize - (0 != totalSize % countRec ? totalSize % countRec : countRec);
        }

        IDQLStatement statement = builder.createStatement(context.getSession());
        statement.setFirstResult(startRec);
        statement.setMaxResults(countRec);

        List<Object[]> list = statement.list();
        if (list.isEmpty())
            ListOutputBuilder.get(input, Lists.newArrayList()).pageable(true).build();

        final Map<EppStudentWpeCAction, SessionSheetDocument> sheetDocumentMap = Maps.newHashMap();
        final Map<EppStudentWpeCAction, SessionRetakeDocument> retakeDocumentMap = Maps.newHashMap();

        DQLSelectBuilder sheetDql = new DQLSelectBuilder().fromEntity(SessionSheetDocument.class, "doc")
                .column(property("doc"))
                .column(property("docSlot", SessionDocumentSlot.studentWpeCAction()))
                .joinEntity("doc", DQLJoinType.inner, SessionDocumentSlot.class, "docSlot", eq(property("doc.id"), property("docSlot", SessionDocumentSlot.document().id())))
                .where(eq(property("doc", SessionSimpleDocument.orgUnit()), value(orgUnitId)))
                .where(notExists(new DQLSelectBuilder()
                                         .fromEntity(SessionMark.class, "mark_ne").column(property("mark_ne.id"))
                                         .where(eq(property("docSlot.id"), property("mark_ne", SessionMark.slot().id())))
                                         .buildQuery()
                ));

        if (!StringUtils.isEmpty(docNumber))
        {
            sheetDql.where(likeUpper(property("doc", SessionSheetDocument.number()), value(CoreStringUtils.escapeLike(docNumber))));
        }

        DQLSelectBuilder retakeDql = new DQLSelectBuilder().fromEntity(SessionRetakeDocument.class, "doc")
                .column(property("doc"))
                .column(property("docSlot", SessionDocumentSlot.studentWpeCAction()))
                .joinEntity("doc", DQLJoinType.inner, SessionDocumentSlot.class, "docSlot", eq(property("doc.id"), property("docSlot", SessionDocumentSlot.document().id())))
                .where(eq(property("doc", SessionRetakeDocument.sessionObject().orgUnit().id()), value(orgUnitId)))
                .where(isNull(property("doc", SessionRetakeDocument.closeDate())))
                .where(notExists(new DQLSelectBuilder()
                                         .fromEntity(SessionMark.class, "mark").column(property("mark.id"))
                                         .where(eq(property("docSlot.id"), property("mark", SessionMark.slot().id())))
                                         .buildQuery()
                ));

        if (!StringUtils.isEmpty(docNumber))
        {
            retakeDql.where(likeUpper(property("doc", SessionSheetDocument.number()), value(CoreStringUtils.escapeLike(docNumber))));
        }

        for (Object[] obj : sheetDql.createStatement(context.getSession()).<Object[]>list())
        {
            SessionSheetDocument document = (SessionSheetDocument) obj[0];
            EppStudentWpeCAction action = (EppStudentWpeCAction) obj[1];
            sheetDocumentMap.put(action, document);
        }

        for (Object[] obj : retakeDql.createStatement(context.getSession()).<Object[]>list())
        {
            SessionRetakeDocument document = (SessionRetakeDocument) obj[0];
            EppStudentWpeCAction action = (EppStudentWpeCAction) obj[1];
            retakeDocumentMap.put(action, document);
        }

        for (List<Object[]> rows : Lists.partition(list, DQL.MAX_VALUES_ROW_NUMBER)) {

            List<EppStudentWpeCAction> actions = Lists.newArrayList();
            for (Object[] row : rows)
            {
                EppStudentWpeCAction action = (EppStudentWpeCAction) row[1];
                actions.add(action);
            }

            final Session session = context.getSession();
            Map<EppStudentWpeCAction, Long> commissionMap = Maps.newHashMap();
            Map<EppStudentWpeCAction, List<SessionSlotMarkGradeValue>> markGradeValueMap = Maps.newHashMap();
            Map<EppStudentWpeCAction, SessionMark> sessionMarkMap = Maps.newHashMap();
            Map<EppStudentWpeCAction, SessionMark> totalMarkMap = Maps.newHashMap();
            Map<EppStudentWpeCAction, MutableInt> markCountMap = Maps.newHashMap();

            DQLSelectBuilder markDql = new DQLSelectBuilder()
                    .fromEntity(SessionMark.class, "mark").column(property("mark"))
                    .fetchPath(DQLJoinType.inner, SessionMark.slot().fromAlias("mark"), "slot")
                    .fetchPath(DQLJoinType.inner, SessionDocumentSlot.document().fromAlias("slot"), "document")
                    .where(in(property("mark", SessionMark.slot().studentWpeCAction()), actions))
                    .order(property("mark", SessionMark.id()));


            for (SessionMark mark : markDql.createStatement(session).<SessionMark>list())
            {
                SessionDocumentSlot slot = mark.getSlot();
                EppStudentWpeCAction action = slot.getStudentWpeCAction();

                if (slot.getDocument() instanceof SessionStudentGradeBookDocument)
                {
                    // итоговые оценки - в зачетке
                    SessionMark regularMark = mark instanceof SessionSlotLinkMark ? ((SessionSlotLinkMark) mark).getTarget() : mark;
                    if (mark.isInSession())
                        sessionMarkMap.put(action, regularMark);
                    else
                    {
                        totalMarkMap.put(action, regularMark);
                        commissionMap.put(action, slot.getCommission().getId());
                    }
                }
                if (mark instanceof SessionSlotRegularMark)
                {
                    SafeMap.safeGet(markCountMap, action, MutableInt.class).increment();

                    if (mark instanceof SessionSlotMarkGradeValue)
                        SafeMap.safeGet(markGradeValueMap, action, ArrayList.class).add((SessionSlotMarkGradeValue) mark);
                }
            }

            Map<EppStudentWpeCAction, String> commissionPpsMap = Maps.newHashMap();
            if (!commissionMap.isEmpty())
            {
                Map<Long, List<String>> cpMap = Maps.newHashMap();

                new DQLSelectBuilder()
                        .fromEntity(SessionComissionPps.class, "pps").column(property("pps"))
                        .joinPath(DQLJoinType.inner, SessionComissionPps.pps().person().identityCard().fromAlias("pps"), "icard")
                        .where(in(property("pps", SessionComissionPps.commission()), commissionMap.values()))
                        .order(property("pps", SessionComissionPps.commission().id()))
                        .order(property("icard", IdentityCard.lastName()))
                        .order(property("icard", IdentityCard.firstName()))
                        .order(property("icard", IdentityCard.middleName()))
                        .createStatement(session).<SessionComissionPps>list()
                        .forEach(item -> SafeMap.safeGet(cpMap, item.getCommission().getId(), ArrayList.class).add(item.getPps().getFio()));

                commissionPpsMap = commissionMap.entrySet().stream()
                        .collect(Collectors.toMap(Map.Entry::getKey, item -> CommonBaseStringUtil.joinNotEmpty(cpMap.get(item.getValue()), ", ")));
            }

            for (Object[] row : rows)
            {
                EppStudentWpeCAction action = (EppStudentWpeCAction) row[1];

                SessionSheetDocument sheetDoc = sheetDocumentMap.get(action);
                SessionRetakeDocument retakeDoc = retakeDocumentMap.get(action);
                if (null != docNumber)
                {
                    if (null == sheetDoc && null == retakeDoc) continue;
                    if ((null == sheetDoc || !sheetDoc.getNumber().contains(docNumber)) && (null == retakeDoc || !retakeDoc.getNumber().contains(docNumber)))
                        continue;
                }

                List<SessionSlotMarkGradeValue> markGradeValues = markGradeValueMap.getOrDefault(action, Lists.newArrayList());
                boolean checkedGradeBook = !markGradeValues.isEmpty();
                boolean checkedGradeBookDisabled = true;

                for (SessionSlotMarkGradeValue mark : markGradeValues)
                {
                    if (BooleanUtils.isTrue(mark.getCachedMarkPositiveStatus()))
                    {
                        checkedGradeBookDisabled = false;
                        if (!mark.isCheckedGradeBook()) checkedGradeBook = false;
                    }
                    else
                    {
                        checkedGradeBook = false;
                    }
                }

                DataWrapper wrapper = new DataWrapper(action.getId(), "");
                wrapper.setProperty(PROPERTY_ELEMENT, row[0]);
                wrapper.setProperty(PROPERTY_ACTION, action);
                wrapper.setProperty(PROPERTY_GROUP, row[2]);
                wrapper.setProperty(PROPERTY_EOU, row[3]);
                wrapper.setProperty(PROPERTY_SESSION_MARK, sessionMarkMap.get(action));
                wrapper.setProperty(PROPERTY_TOTAL_MARK, totalMarkMap.get(action));
                wrapper.setProperty(PROPERTY_TEACHER, commissionPpsMap.get(action));
                wrapper.setProperty(PROPERTY_CHECKED_GRADE_BOOK, checkedGradeBook);
                wrapper.setProperty(PROPERTY_CHECKED_GRADE_BOOK_DISABLED, checkedGradeBookDisabled);
                wrapper.setProperty(PROPERTY_RETAKE_COUNT, markCountMap.containsKey(action) ? markCountMap.get(action).intValue() : null);
                wrapper.setProperty(PROPERTY_SHEET_DOCUMENT, sheetDoc);
                wrapper.setProperty(PROPERTY_RETAKE_DOCUMENT, retakeDoc);
                resultList.add(wrapper);
            }
            session.flush();
            session.clear();
        }


        DSOutput output = new DSOutput(input);
        output.setRecordList(resultList);
        output.setTotalSize(totalSize);
        return output;
    }

    private DQLSelectBuilder getMarkDql(String alias)
    {
        return new DQLSelectBuilder()
                .fromEntity(SessionMark.class, alias).column(property(alias, "id"))
                .joinEntity(alias, DQLJoinType.left, SessionDocumentSlot.class, "docSlot", eq(property("docSlot", SessionDocumentSlot.id()), property(alias, SessionMark.slot().id())))
                .joinEntity("docSlot", DQLJoinType.inner, SessionDocument.class, "document", eq(property("docSlot", SessionDocumentSlot.document().id()), property("document", SessionDocument.id())))
                .where(eq(property("a.id"), property("docSlot", SessionDocumentSlot.studentWpeCAction().id())));
    }
}
