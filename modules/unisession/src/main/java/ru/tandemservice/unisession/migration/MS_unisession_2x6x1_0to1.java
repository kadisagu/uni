/* $Id$ */
package ru.tandemservice.unisession.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.ISQLTranslator;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLSelectQuery;
import org.tandemframework.hibsupport.EntityIDGenerator;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

/**
 * @author azhebko
 * @since 31.03.2014
 */
public class MS_unisession_2x6x1_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ISQLTranslator translator = tool.getDialect().getSQLTranslator();
//
//        select sdslot.commission_id from session_doc_slot_t as sdslot
//        inner join session_doc_sheet_t as sd
//        on sdslot.document_id = sd.id
//        where exists (
//            select * from session_mark_t as mark
//        inner join session_doc_slot_t as bslot
//        on bslot.id=mark.slot_id
//        inner join session_doc_bulletin_t as b
//        on bslot.document_id = b.id
//        where mark.commission_id=sdslot.commission_id
//        )

        SQLSelectQuery subQuery = new SQLSelectQuery().from(
                SQLFrom.table("session_mark_t", "mark")
                        .innerJoin(SQLFrom.table("session_doc_slot_t", "bslot"), "bslot.id=mark.slot_id")
                        .innerJoin(SQLFrom.table("session_doc_bulletin_t", "b"), "bslot.document_id = b.id"))
                .where("mark.commission_id=sdslot.commission_id");


        SQLSelectQuery selectQuery = new SQLSelectQuery()
                .from(
                        SQLFrom.table("session_doc_slot_t", "sdslot")
                                .innerJoin(SQLFrom.table("session_doc_sheet_t", "sd"), "sdslot.document_id=sd.id"))
                .column("sdslot.id", "id")
                .column("sdslot.commission_id", "commission")
                .where("exists (" + translator.toSql(subQuery) + ")");

        String selectQuerySql = translator.toSql(selectQuery);

        Statement statement = tool.getConnection().createStatement();
        statement.execute(selectQuerySql);

        ResultSet result = statement.getResultSet();

        PreparedStatement updateStatement = tool.prepareStatement("update session_doc_slot_t set commission_id = ? where id = ?");

        while (result.next())
        {
            Long id = result.getLong("id");
            Long commissionId = result.getLong("commission");

            Long newCommissionId = newCommission(commissionId, tool, translator);
            updateStatement.clearParameters();
            updateStatement.setLong(1, newCommissionId);
            updateStatement.setLong(2, id);
            updateStatement.execute();
        }

    }

    private Long newCommission(Long commissionId, DBTool tool, ISQLTranslator translator) throws Exception
    {
        Short commissionCode = tool.entityCodes().get("sessionComission");
        Short ppsCommissionCode = tool.entityCodes().get("sessionComissionPps");

        Long newCommissionId = EntityIDGenerator.generateNewId(commissionCode);

        tool.executeUpdate("insert into session_comission_t (id, discriminator) values (?, ?)", newCommissionId, commissionCode);

        SQLSelectQuery selectQuery = new SQLSelectQuery()
                .from(
                        SQLFrom.table("session_comission_pps_t", "pps"))
                .column("pps.pps_id", "ppsid")
                .where("pps.commission_id=" + commissionId);

        String selectQuerySql = translator.toSql(selectQuery);

        Statement statement = tool.getConnection().createStatement();
        statement.execute(selectQuerySql);

        ResultSet result = statement.getResultSet();

        while (result.next())
        {
            Long ppsId = result.getLong("ppsid");
            Long newCommissionPpsId = EntityIDGenerator.generateNewId(ppsCommissionCode);
            tool.executeUpdate("insert into session_comission_pps_t (id, discriminator, commission_id, pps_id) values (?, ?, ?, ?)", newCommissionPpsId, ppsCommissionCode, newCommissionId, ppsId);
        }

        return newCommissionId;
    }
}