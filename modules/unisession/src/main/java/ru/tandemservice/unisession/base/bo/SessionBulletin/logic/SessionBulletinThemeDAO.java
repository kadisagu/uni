package ru.tandemservice.unisession.base.bo.SessionBulletin.logic;

import org.tandemframework.hibsupport.dao.CommonDAO;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionProjectTheme;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author avedernikov
 * @since 08.08.2016
 */
public class SessionBulletinThemeDAO extends CommonDAO implements ISessionBulletinThemeDAO
{
	@Override
	public List<SessionBulletinThemeRow> getBulletinThemeRows(SessionBulletinDocument bulletin)
	{
		List<SessionDocumentSlot> slots = getList(SessionDocumentSlot.class, SessionDocumentSlot.document(), bulletin,
												  SessionDocumentSlot.studentWpeCAction().studentWpe().student().person().identityCard().fullFio().s());

		List<SessionProjectTheme> projectThemes = getList(SessionProjectTheme.class, SessionProjectTheme.slot(), slots);
		Map<SessionDocumentSlot, String> slot2theme = projectThemes.stream()
				.filter(theme -> theme.getTheme() != null)
				.collect(Collectors.toMap(SessionProjectTheme::getSlot, SessionProjectTheme::getTheme));
		Map<SessionDocumentSlot, PpsEntry> slot2supervisor = projectThemes.stream()
				.filter(theme -> theme.getSupervisor() != null)
				.collect(Collectors.toMap(SessionProjectTheme::getSlot, SessionProjectTheme::getSupervisor));

		if (bulletin.getGroup().getActivityPart().getRegistryElement().getParent().isThemeRequired())
		slots.forEach(slot -> {
				Student student = slot.getActualStudent();
				if (!slot2theme.containsKey(slot))
					slot2theme.put(slot, student.getFinalQualifyingWorkTheme());
				if (!slot2supervisor.containsKey(slot))
					slot2supervisor.put(slot, student.getPpsEntry());
		});
		List<SessionBulletinThemeRow> rows = new ArrayList<>(slots.size());
		slots.forEach(slot -> rows.add(new SessionBulletinThemeRow(slot, slot2theme.get(slot), slot2supervisor.get(slot))));
		return rows;
	}

	@Override
	public void updateFinalQualifyingWorkForStudents(List<SessionBulletinThemeRow> rows)
	{
		for (SessionBulletinThemeRow row : rows)
		{
			PpsEntry supervisor = row.getSupervisor();
			Student student = row.getSlot().getActualStudent();
			student.setFinalQualifyingWorkTheme(row.getTheme());
			student.setPpsEntry(supervisor);
			student.setFinalQualifyingWorkAdvisor(supervisor == null ? null : supervisor.getTitleFioInfoOrgUnitDegreesStatuses());
			update(student);
		}
	}
}
