/* $Id:$ */
package ru.tandemservice.unisession.base.bo.SessionReport.util;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.FilterUtils;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 2/21/12
 */
public abstract class GroupModel extends DQLFullCheckSelectModel
{
    public static IdentifiableWrapper WRAPPER_NO_GROUP = new IdentifiableWrapper(Long.MIN_VALUE, "вне групп");
    
    @Override
    public ListResult findValues(String filter)
    {
        final ListResult groups = super.findValues(filter);

        boolean exists = ISharedBaseDao.instance.get().existsEntity(getStudentDQL().where(isNull(property(Student.group().fromAlias("st")))).column("st.id").buildQuery());

        if (exists && (StringUtils.isBlank(filter) || "вне групп".contains(filter))) {
            List<Object> values = new ArrayList<Object>(groups.getObjects());
            values.add(0, WRAPPER_NO_GROUP);
            values = values.subList(0, Math.min(values.size(), UIDefines.POPUP_LIST_SIZE));
            return new ListResult<>(values, groups.getMaxCount() + 1);
        }

        return groups;
    }

    @Override
    public Object getValue(Object primaryKey)
    {
        if (WRAPPER_NO_GROUP.getId().equals(primaryKey))
            return WRAPPER_NO_GROUP;
        return super.getValue(primaryKey);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List getValues(Set primaryKeys)
    {
        if (CollectionUtils.isEmpty(primaryKeys))
            return Collections.emptyList();
        Set tmp = new HashSet(primaryKeys);
        if (tmp.contains(WRAPPER_NO_GROUP.getId())) {
            tmp.remove(WRAPPER_NO_GROUP.getId());
            final List values = new ArrayList(super.getValues(tmp));
            values.add(0, WRAPPER_NO_GROUP);
            return values;
        }
        return super.getValues(primaryKeys);
    }

    @Override
    protected DQLSelectBuilder query(String alias, String filter)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(Group.class, alias)
            .order(property(Group.title().fromAlias(alias)))
            ;

        DQLSelectBuilder students = getStudentDQL();
        if (null != students)
            dql.where(in(alias, students
                .column(property(Student.group().fromAlias("st")))
                .buildQuery()));
        
        applyGroupConditions(dql, alias);

        FilterUtils.applyLikeFilter(dql, filter,
            Group.title().fromAlias(alias)
        );

        return dql;
    }

    protected void applyGroupConditions(DQLSelectBuilder dql, String alias)
    {
        
    }

    protected DQLSelectBuilder getStudentDQL()
    {
        return null;
    }
}