package ru.tandemservice.unisession.base.bo.StudentPortfolio.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.bean.FastDynaBean;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unisession.base.bo.StudentPortfolio.ui.AchievementList.StudentPortfolioAchievementList;
import ru.tandemservice.unisession.base.bo.StudentPortfolio.ui.AchievementList.StudentPortfolioAchievementListUI;
import ru.tandemservice.unisession.entity.catalog.StudentPortfolioElement;
import ru.tandemservice.unisession.entity.document.SessionStudentGradeBookDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author avedernikov
 * @since 26.11.2015
 */

public class AchievementDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
	public static final String COLUMN_MARKS = "marksColumn";
	public static final String FILE_DISABLED = "fileDisabled";

	public AchievementDSHandler(String ownerId)
	{
		super(ownerId, StudentPortfolioElement.class);
	}

	@Override
	protected DSOutput execute(DSInput input, ExecutionContext context)
	{
		DataWrapper containingWpeFilter = context.get(StudentPortfolioAchievementListUI.CONTAINING_WPE_FILTER);
		Student student = context.getNotNull(StudentPortfolioAchievementListUI.STUDENT_FILTER);

		DSOutput output = achievementOutput(containingWpeFilter, student, input, context);
		List<DataWrapper> wrappers = DataWrapper.wrap(output);
		Collection<Long> cActionIds = wrappers.stream()
				.map(FastDynaBean::<StudentPortfolioElement>getWrapped)
				.filter(achievement -> achievement.getWpeCAction() != null)
				.map(achievement -> achievement.getWpeCAction().getId())
				.collect(Collectors.toList());
		List<SessionMark> marks = getMarks(student, cActionIds, context);
		for (DataWrapper wrapper : wrappers)
		{
			StudentPortfolioElement item = wrapper.getWrapped();
			if (item.getWpeCAction() != null)
				wrapper.put(COLUMN_MARKS, marks.stream()
						.filter(mark -> mark.getSlot().getStudentWpeCAction().getId().equals(item.getWpeCAction().getId()))
						.findFirst().map(SessionMark::getValueTitle).orElse(""));
			wrapper.put(FILE_DISABLED, item.getFile() == null);
		}
		return output;
	}

	private DSOutput achievementOutput(DataWrapper containingWpeFilter, Student student, DSInput input, ExecutionContext context)
	{
		String achievementAlias = "portfolioElem";
		DQLOrderDescriptionRegistry orderDescription = new DQLOrderDescriptionRegistry(StudentPortfolioElement.class, achievementAlias);
		DQLSelectBuilder dql = orderDescription.buildDQLSelectBuilder()
				.column(property(achievementAlias))
				.where(eq(property(achievementAlias, StudentPortfolioElement.student()), value(student)));
		if (containingWpeFilter != null)
		{
			if (containingWpeFilter.getId().equals(StudentPortfolioAchievementList.SHOW_WITH_WPE))
				dql.where(isNotNull(property(achievementAlias, StudentPortfolioElement.wpeCAction())));
			else if (containingWpeFilter.getId().equals(StudentPortfolioAchievementList.SHOW_WITHOUT_WPE))
				dql.where(isNull(property(achievementAlias, StudentPortfolioElement.wpeCAction())));
		}
		return DQLSelectOutputBuilder.get(input, dql, context.getSession()).order(orderDescription).build();
	}

	private List<SessionMark> getMarks(Student student, Collection<Long> cActionIds, ExecutionContext context)
	{
		String markAlias = "mark";
		return new DQLSelectBuilder()
				.fromEntity(SessionMark.class, markAlias)
				.where(in(property(markAlias, SessionMark.slot().studentWpeCAction().id()), cActionIds))
				.where(eq(property(markAlias, SessionMark.slot().studentWpeCAction().studentWpe().student()), value(student)))
				.createStatement(context.getSession()).<SessionMark>list().stream()
					.filter(mark -> (!mark.getSlot().isInSession()) && mark.getSlot().getDocument() instanceof SessionStudentGradeBookDocument)
					.collect(Collectors.toList());
	}
}
