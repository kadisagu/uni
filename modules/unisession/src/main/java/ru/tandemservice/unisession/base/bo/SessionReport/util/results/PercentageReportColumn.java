/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReport.util.results;

import org.tandemframework.core.view.formatter.DoubleFormatter;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.ResultsAdd.ISessionReportResultsDAO;

/**
* @author oleyba
* @since 2/13/12
*/
public class PercentageReportColumn extends DoubleReportColumn
{
    protected static final DoubleFormatter FORMATTER = new DoubleFormatter(1, true);

    private DoubleReportColumn base;
    private DoubleReportColumn value;

    public PercentageReportColumn(DoubleReportColumn base, DoubleReportColumn value)
    {
        this.base = base;
        this.value = value;
    }

    @Override
    public Double getCount(ISessionReportResultsDAO.ISessionResultsReportRow row)
    {
        final Double baseCount = base.getCount(row);
        if (null == baseCount || UniBaseUtils.eq(baseCount, 0))
            return null;
        final Double valueCount = value.getCount(row);
        if (null == valueCount)
            return null;
        return 100 * valueCount / baseCount;
    }

    @Override
    public String cell(ISessionReportResultsDAO.ISessionResultsReportRow row)
    {
        final Double count = getCount(row);
        return null == count ? "" : FORMATTER.format(count);
    }
}
