/* $Id$ */
package ru.tandemservice.unisession.component.orgunit.AllowanceTab;

import org.tandemframework.core.tool.tree.IHierarchyItem;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniedu.base.bo.EducationYear.utils.EducationYearModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unisession.entity.allowance.ISessionStudentNotAllowed;
import ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowed;
import ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowedForBulletin;
import ru.tandemservice.unisession.entity.document.SessionObject;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


/**
 * @author oleyba
 * @since 4/19/11
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.getOrgUnitHolder().refresh();
        model.setSec(new OrgUnitSecModel(model.getOrgUnit()));
        model.setYearModel(new EducationYearModel()
        {
            @Override
            protected DQLSelectBuilder query(final String alias, final String filter)
            {
                final DQLSelectBuilder sessionObjectExists = new DQLSelectBuilder()
                .fromEntity(SessionObject.class, alias)
                .column(property(SessionObject.educationYear().id().fromAlias(alias)))
                .where(eq(property(SessionObject.orgUnit().fromAlias(alias)), value(model.getOrgUnit())));

                final DQLSelectBuilder dql = new DQLSelectBuilder();
                dql.fromEntity(EducationYear.class, alias);
                dql.where(in(property(EducationYear.id().fromAlias(alias)), sessionObjectExists.buildQuery()));
                dql.order(property(EducationYear.intValue().fromAlias(alias)));
                return dql;
            }
        });

        final DQLSelectBuilder partsDQL = new DQLSelectBuilder()
        .fromEntity(SessionObject.class, "obj")
        .column(property(SessionObject.yearDistributionPart().fromAlias("obj")))
        .where(eq(property(SessionObject.orgUnit().fromAlias("obj")), value(model.getOrgUnit())))
        .order(property(SessionObject.yearDistributionPart().code().fromAlias("obj")));
        if (model.getSettings() !=null && model.getSettings().get("eduYear") != null) {
            partsDQL.where(eq(property(SessionObject.educationYear().fromAlias("obj")), commonValue(model.getSettings().get("eduYear"))));
        }
        final Set<YearDistributionPart> parts = new LinkedHashSet<YearDistributionPart>(partsDQL.createStatement(this.getSession()).<YearDistributionPart>list());
        model.setPartsList(HierarchyUtil.listHierarchyNodesWithParents(new ArrayList<IHierarchyItem>(parts), false));

        model.setCourseModel(new DQLFullCheckSelectModel()
        {
            @Override
            protected DQLSelectBuilder query(final String alias, final String filter)
            {
                final DQLSelectBuilder allowance = new DQLSelectBuilder()
                .fromEntity(SessionStudentNotAllowed.class, "entry")
                .column(property(SessionStudentNotAllowed.student().course().id().fromAlias("entry")))
                .where(eq(property(SessionStudentNotAllowed.session().fromAlias("entry")), value(getSessionObject(model))));
                final DQLSelectBuilder bulletinAllowance = new DQLSelectBuilder()
                .fromEntity(SessionStudentNotAllowedForBulletin.class, "entryB")
                .column(property(SessionStudentNotAllowedForBulletin.student().course().id().fromAlias("entryB")))
                .where(eq(property(SessionStudentNotAllowedForBulletin.bulletin().sessionObject().fromAlias("entryB")), value(getSessionObject(model))));
                return new DQLSelectBuilder()
                .fromEntity(Course.class, alias)
                .where(or(
                        in(property(alias + ".id"), allowance.buildQuery()),
                        in(property(alias + ".id"), bulletinAllowance.buildQuery())))
                        .order(property(Course.intValue().fromAlias(alias)));
            }
        });

        model.setGroupModel(new DQLFullCheckSelectModel()
        {
            @Override
            protected DQLSelectBuilder query(final String alias, final String filter)
            {
                final Object sessionObject = getSessionObject(model);
                if (null == sessionObject) {
                    return new DQLSelectBuilder().fromEntity(Group.class, alias).where(isNull(alias + ".id"));
                }

                final DQLSelectBuilder allowance = new DQLSelectBuilder()
                .fromEntity(SessionStudentNotAllowed.class, "entry")
                .column(property(SessionStudentNotAllowed.student().group().id().fromAlias("entry")))
                .where(eq(property(SessionStudentNotAllowed.session().fromAlias("entry")), commonValue(sessionObject)));
                final DQLSelectBuilder bulletinAllowance = new DQLSelectBuilder()
                .fromEntity(SessionStudentNotAllowedForBulletin.class, "entryB")
                .column(property(SessionStudentNotAllowedForBulletin.student().group().id().fromAlias("entryB")))
                .where(eq(property(SessionStudentNotAllowedForBulletin.bulletin().sessionObject().fromAlias("entryB")), commonValue(sessionObject)));

                final DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(Group.class, alias)
                .where(or(
                        in(property(alias + ".id"), allowance.buildQuery()),
                        in(property(alias + ".id"), bulletinAllowance.buildQuery())))
                        .order(property(Group.title().fromAlias(alias)));
                FilterUtils.applySelectFilter(dql, alias, Group.course().s(), model.getSettings().get("course"));
                return dql;
            }
        });
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareListDataSource(final Model model)
    {
        final Object sessionObject = getSessionObject(model);
        final Object group = model.getSettings().get("group");
        final Object course = model.getSettings().get("course");
        final Object personLastName = model.getSettings().get("personLastName");
        final Object personFirstName = model.getSettings().get("personFirstName");

        final DQLSelectBuilder allowance = new DQLSelectBuilder()
        .fromEntity(SessionStudentNotAllowed.class, "entryS")
        .column(property(SessionStudentNotAllowed.id().fromAlias("entryS")))
        .where(eq(property(SessionStudentNotAllowed.session().fromAlias("entryS")), commonValue(sessionObject)))
        .joinPath(DQLJoinType.inner, SessionStudentNotAllowed.student().fromAlias("entryS"), "student");

        final DQLSelectBuilder bulletinAllowance = new DQLSelectBuilder()
        .fromEntity(SessionStudentNotAllowedForBulletin.class, "entryB")
        .column(property(SessionStudentNotAllowedForBulletin.id().fromAlias("entryB")))
        .where(eq(property(SessionStudentNotAllowedForBulletin.bulletin().sessionObject().fromAlias("entryB")), commonValue(sessionObject)))
        .joinPath(DQLJoinType.inner, SessionStudentNotAllowed.student().fromAlias("entryB"), "student");

        for (final DQLSelectBuilder dql : Arrays.asList(allowance, bulletinAllowance))
        {
            FilterUtils.applySelectFilter(dql, "student", Student.group().s(), group);
            FilterUtils.applySelectFilter(dql, "student", Student.course().s(), course);
            FilterUtils.applySimpleLikeFilter(dql, "student", Student.person().identityCard().lastName().s(), (String) personLastName);
            FilterUtils.applySimpleLikeFilter(dql, "student", Student.person().identityCard().firstName().s(), (String) personFirstName);
        }

        final DQLSelectBuilder dql = new DQLSelectBuilder()
        .fromEntity(ru.tandemservice.unisession.entity.allowance.ISessionStudentNotAllowed.class.getCanonicalName(), "entry")
        .column("entry")
        .where(or(
                in(property("entry.id"), allowance.buildQuery()),
                in(property("entry.id"), bulletinAllowance.buildQuery())))
                .order(property(SessionStudentNotAllowed.student().person().identityCard().fullFio().fromAlias("entry")));

        UniBaseUtils.createPage(model.getDataSource(), dql, this.getSession());

        final Map<Long, Object> bulletinColumnValueMap = new HashMap<Long, Object>();
        for (final ISessionStudentNotAllowed row : model.getDataSource().getEntityList()) {
            bulletinColumnValueMap.put(row.getId(), row instanceof SessionStudentNotAllowedForBulletin ? ((SessionStudentNotAllowedForBulletin) row).getBulletin() : "по всем ведомостям");
        }
        ((BlockColumn) model.getDataSource().getColumn("bulletinColumn")).setValueMap(bulletinColumnValueMap);
    }

    @Override
    public void doUnlock(final Long id)
    {
        final ISessionStudentNotAllowed allowance = this.get(id);
        allowance.setRemovalDate(new Date());
        this.update(allowance);
    }

    public SessionObject getSessionObject(Model model)
    {
        final DQLSelectBuilder dql = new DQLSelectBuilder()
        .fromEntity(SessionObject.class, "s")
        .where(eq(property(SessionObject.orgUnit().fromAlias("s")), value(model.getOrgUnit())))
        .order(property(SessionObject.startupDate().fromAlias("s")));

        Object eduYear = model.getSettings().get("eduYear");
        Object part = model.getSettings().get("part");
        if (null != eduYear && null != part) {
            dql.where(eq(property(SessionObject.educationYear().fromAlias("s")), commonValue(eduYear)));
            dql.where(eq(property(SessionObject.yearDistributionPart().fromAlias("s")), commonValue(part)));
        } else {
            dql.where(isNull(property(SessionObject.id().fromAlias("s"))));
        }

        return dql.createStatement(getSession()).uniqueResult();
    }
}
