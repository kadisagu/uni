package ru.tandemservice.unisession.component.catalog.sessionMarkGradeValue.SessionMarkGradeValueAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.Return;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;

/**
 * @author iolshvang
 */
@Input(keys = {"catalogItemId", "catalogCode", "sessionGradeScaleId"}, bindings = {"catalogItemId", "catalogCode", "scale.id"})
@Return({@Bind(key = "createdCatalogItemId", binding = "catalogItem.id")})
public class Model extends DefaultCatalogAddEditModel<SessionMarkGradeValueCatalogItem>
{
    EppGradeScale _scale = new EppGradeScale();
    boolean _editable;
    public static final String COMPONENT_NAME = Model.class.getPackage().getName();
    public boolean isEditable() {
        return this._editable;
    }
    public EppGradeScale getScale() {
        return this._scale;
    }

    public void setScale(final EppGradeScale scale) {
        this._scale = scale;
    }
}
