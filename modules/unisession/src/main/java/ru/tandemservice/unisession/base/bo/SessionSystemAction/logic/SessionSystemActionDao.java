/**
 *$Id$
 */
package ru.tandemservice.unisession.base.bo.SessionSystemAction.logic;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unisession.base.bo.SessionMark.daemon.SessionMarkDaemonBean;

/**
 * @author Alexander Shaburov
 * @since 12.02.13
 */
public class SessionSystemActionDao extends UniBaseDao implements ISessionSystemActionDao
{
    @Override
    public void doRefreshFinalMarks() {
        SessionMarkDaemonBean.DAEMON.registerAfterCompleteWakeUpAndWait();
    }
}
