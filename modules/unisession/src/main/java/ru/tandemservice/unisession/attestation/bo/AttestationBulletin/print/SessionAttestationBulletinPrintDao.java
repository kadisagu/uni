/* $Id:$ */
package ru.tandemservice.unisession.attestation.bo.AttestationBulletin.print;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.bo.Declinable.DeclinableManager;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.ICommonFilterItem;
import org.tandemframework.shared.commonbase.catalog.entity.InflectorVariant;
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.education.StudentCustomState;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeALoad;
import ru.tandemservice.unisession.attestation.entity.SessionAttestation;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlot;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlotAdditionalData;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.base.bo.SessionReport.util.UniAttestationFilterAddon;
import ru.tandemservice.unisession.base.bo.SessionReport.util.UniSessionFilterAddon;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.entity.catalog.UnisessionCommonTemplate;
import ru.tandemservice.unisession.entity.catalog.codes.UnisessionCommonTemplateCodes;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 10/22/12
 */
public class SessionAttestationBulletinPrintDao extends UniBaseDao implements ISessionAttestationBulletinPrintDao
{
    protected static IFormatter<Boolean> yes_no_formatter = source -> {
        if (null == source) return "";
        return Boolean.TRUE.equals(source) ? "атт." : "н/атт.";
    };

    private RtfDocument printBulletin(SessionAttestationBulletin bulletin)
    {
        final boolean useCurrentRating = ISessionBrsDao.instance.get().isUseCurrentRating(bulletin);
        final EppRegistryElementPart discipline = bulletin.getRegistryElementPart();
        List<SessionComissionPps> tutors = getList(SessionComissionPps.class, SessionComissionPps.commission(), bulletin.getCommission(), SessionComissionPps.pps().person().identityCard().fullFio().s());
        SessionAttestation attestation = bulletin.getAttestation();

        final RtfDocument document = getTemplateRtf();

        final RtfInjectModifier modifier = new RtfInjectModifier();
        modifier.put("formativeOrgUnits", bulletin.getOrgUnit().getPrintTitle());
        modifier.put("documentNumber", bulletin.getNumber());
        modifier.put("date", bulletin.getPerformDate() == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(bulletin.getPerformDate()));
        modifier.put("eduYear", attestation.getSessionObject().getEducationYear().getTitle());
        modifier.put("term", StringUtils.trimToEmpty(DeclinableManager.instance().dao().getPropertyValue(attestation.getSessionObject().getYearDistributionPart(), YearDistributionPart.P_SHORT_TITLE, IUniBaseDao.instance.get().getCatalogItem(InflectorVariant.class, InflectorVariantCodes.RU_NOMINATIVE))));
        modifier.modify(document);

        printBulletin(useCurrentRating, discipline, tutors, getStudentData(bulletin), document);

        return document;
    }

    @Override
    public byte[] printBulletin(Long bulletinId)
    {
        final SessionAttestationBulletin bulletin = get(SessionAttestationBulletin.class, bulletinId);
        return RtfUtil.toByteArray(printBulletin(bulletin));
    }

	private List<StudentData> getStudentData(SessionAttestationBulletin bulletin)
	{
		List<SessionAttestationSlot> studentList = getList(SessionAttestationSlot.class, SessionAttestationSlot.bulletin(), bulletin, SessionAttestationSlot.studentWpe().student().person().identityCard().fullFio().s());
		Map<Long, SessionAttestationSlotAdditionalData> dataMap = new HashMap<>();
		for (SessionAttestationSlotAdditionalData data : getList(SessionAttestationSlotAdditionalData.class, SessionAttestationSlotAdditionalData.slot().bulletin(), bulletin))
			dataMap.put(data.getSlot().getId(), data);
		return studentList.stream()
				.map(slot -> new StudentData(slot.getActualStudent(), slot, dataMap.get(slot.getId()), true))
				.collect(Collectors.toList());
	}

	@Override
	public RtfDocument printBulletins(List<Long> bulletinIds)
	{
		final String bulletinAlias = "bulletin";
		List<SessionAttestationBulletin> bulletins = new DQLSelectBuilder()
				.fromEntity(SessionAttestationBulletin.class, bulletinAlias)
				.where(in(property(bulletinAlias, SessionAttestationBulletin.id()), bulletinIds))
				.createStatement(getSession()).list();

		Comparator<MultiKey> bulletinComparator = Comparator.comparing((MultiKey key) -> (String) (key.getKey(1)))
				.thenComparing((MultiKey key) -> ((SessionAttestationBulletin) key.getKey(0)).getRegistryElementPart().getRegistryElement().getTitle())
				.thenComparing((MultiKey key) -> ((SessionAttestationBulletin)key.getKey(0)).getRegistryElementPart().getTitle());
		List<SessionAttestationBulletin> sortedBulletins = bulletins.stream()
				.map(bulletin -> new MultiKey(bulletin, getGroups(getStudentData(bulletin))))
				.sorted(bulletinComparator)
				.map(key -> (SessionAttestationBulletin) key.getKey(0))
				.collect(Collectors.toList());

		RtfDocument document = null;
		for (SessionAttestationBulletin bulletin : sortedBulletins)
		{
			RtfDocument subDocument = printBulletin(bulletin);
			if (document == null)
				document = subDocument;
			else
			{
				RtfUtil.modifySourceList(document.getHeader(), subDocument.getHeader(), subDocument.getElementList());
				document.getElementList().addAll(subDocument.getElementList());
			}
		}
		return document;
	}

    @Override
    public RtfDocument printBulletin(SessionAttestation attestation, UniAttestationFilterAddon addon, EppRegistryElementPart disc)
    {
        final boolean useCurrentRating = ISessionBrsDao.instance.get().isUseCurrentRating(attestation);
        final IdentifiableWrapper group = (IdentifiableWrapper) addon.getFilterItem(UniSessionFilterAddon.SETTINGS_NAME_GROUP_WITH_NOGROUP).getValue();
        List<Student> students = getStudents(group.getId(), addon);
        String orgUnits = UniStringUtils.joinUnique(students, Student.educationOrgUnit().groupOrgUnit().s() + ".printTitle", "; ");

        final DQLSelectBuilder rowsBuilder = new DQLSelectBuilder()
                .fromEntity(SessionAttestationSlot.class, "s")
                .joinEntity("s", DQLJoinType.left, SessionAttestationSlotAdditionalData.class, "d", eq(property(SessionAttestationSlotAdditionalData.slot().fromAlias("d")), property("s")))
                .column("s").column("d")
                .where(eq(property(SessionAttestationSlot.bulletin().attestation().fromAlias("s")), value(attestation)))
                .where(eq(property(SessionAttestationSlot.bulletin().registryElementPart().fromAlias("s")), value(disc)))
                .order(property(SessionAttestationSlot.studentWpe().student().person().identityCard().fullFio().fromAlias("s")));

        addon.applyFilters(rowsBuilder, "s");

        final List<Object[]> rows = rowsBuilder
            .createStatement(getSession()).list();

        final DQLSelectBuilder shouldPassBuilder = new DQLSelectBuilder()
                .fromEntity(EppStudentWpeALoad.class, "s")
                .column(property(EppStudentWpeALoad.studentWpe().student().id().fromAlias("s")))
                .where(eq(property(EppStudentWpeALoad.studentWpe().part().fromAlias("s")), value(attestation.getSessionObject().getYearDistributionPart())))
                .where(eq(property(EppStudentWpeALoad.studentWpe().year().educationYear().fromAlias("s")), value(attestation.getSessionObject().getEducationYear())))
                .where(eq(property(EppStudentWpeALoad.studentWpe().registryElementPart().fromAlias("s")), value(disc)))
                .where(eq(property(EppStudentWpeALoad.studentWpe().student().group().id().fromAlias("s")), value(group.getId())));


        Set<Long> shouldPass = new HashSet<>(shouldPassBuilder
            .createStatement(getSession()).<Long>list());

        final ArrayList<StudentData> data = new ArrayList<>();
        final Set<Long> commissionIds = new HashSet<>();
        for (Object[] row : rows) {
            SessionAttestationSlot slot = (SessionAttestationSlot) row[0];
            data.add(new StudentData(slot.getActualStudent(), slot, (SessionAttestationSlotAdditionalData) row[1], true));
            if (slot.getBulletin().getCommission() != null)
                commissionIds.add(slot.getBulletin().getCommission().getId());
            students.remove(slot.getActualStudent());
        }
        
        final List<SessionComissionPps> tutors = getList(SessionComissionPps.class, SessionComissionPps.commission().id(), commissionIds, SessionComissionPps.pps().person().identityCard().fullFio().s());

        for (Student student : students)
            data.add(new StudentData(student, null, null, shouldPass.contains(student.getId())));
        Collections.sort(data);

        final RtfDocument document = getTemplateRtf();

        final RtfInjectModifier modifier = new RtfInjectModifier();

        SessionReportManager.addOuLeaderData(modifier, attestation.getOrgUnit(), "ouleader", "FIOouleader");
        
        modifier.put("formativeOrgUnits", orgUnits);
        modifier.put("documentNumber", "");
        modifier.put("date", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()));
        modifier.put("eduYear", attestation.getSessionObject().getEducationYear().getTitle());
        modifier.put("term", attestation.getSessionObject().getYearDistributionPart().getShortTitle());
        modifier.modify(document);

        printBulletin(useCurrentRating, disc, tutors, data, document);
        
        return document;    
    }

	private List<Student> getStudents(long groupId, UniAttestationFilterAddon addon)
	{
		ICommonFilterItem customStateFilterItem = addon.getFilterItem(UniAttestationFilterAddon.SETTINGS_NAME_CUSTOM_STATE);
		if (!customStateFilterItem.isFilterItemDisabled())
		{
			Collection<StudentCustomStateCI> customStates = (Collection<StudentCustomStateCI>)(customStateFilterItem.getValue());
			return getStudentsOfState(groupId, customStates);
		}
		return getList(Student.class, Student.group().id(), groupId);
	}

	private List<Student> getStudentsOfState(long groupId, Collection<StudentCustomStateCI> customStates)
	{
		final String studentAlias = "student";
		final String customStateAlias = "customState";
		DQLSelectBuilder customStateDql = new DQLSelectBuilder().fromEntity(StudentCustomState.class, customStateAlias)
				.where(in(property(customStateAlias, StudentCustomState.customState()), customStates))
				.where(eq(property(customStateAlias, StudentCustomState.student()), property(studentAlias)));
		return new DQLSelectBuilder().fromEntity(Student.class, studentAlias)
				.where(eq(property(studentAlias, Student.group()), value(groupId)))
				.where(exists(customStateDql.buildQuery()))
				.createStatement(getSession()).list();
	}

    private void printBulletin(final boolean useCurrentRating, EppRegistryElementPart discipline, List<SessionComissionPps> tutors, List<StudentData> studentList, RtfDocument document)
    {
        final TopOrgUnit academy = TopOrgUnit.getInstance();
        EmployeePost cathedraHead = EmployeeManager.instance().dao().getHead(discipline.getTutorOu());
        RtfString tutorsNewLined = new RtfString();
        for (SessionComissionPps pps: tutors) {
            if (pps != tutors.get(0))
                tutorsNewLined.par();
            tutorsNewLined.append(pps.getPps().getPerson().getIdentityCard().getFullFio());
        }

        final String groups = getGroups(studentList);
        final boolean multiGroup = groups != null && groups.contains(",");

        int i = 1;
        int attestedCount = 0;
        int notAttestedCount = 0;
        int studentCount = 0;
        List<String[]> tableData = new ArrayList<>();
        final List<Integer> archivalFioRowList = new ArrayList<>();
        for (StudentData student : studentList) {
            if (student.slot != null && student.slot.getMark() != null)
            {
                if (student.slot.getMark().isPositive())
                    attestedCount++;
                else
                    notAttestedCount++;
            }
            if (student.slot != null || student.shouldPass)
                studentCount++;

            if (student.student.isArchival() || !student.student.getStatus().isActive())
                archivalFioRowList.add(i);

            tableData.add(getBulletinTableRow(i++, student, useCurrentRating, multiGroup));
        }

        new RtfTableModifier()
            .put("T", tableData.toArray(new String[tableData.size()][]))
            .put("T", new RtfRowIntercepterBase()
            {
                @Override
                public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
                {
                    return SessionAttestationBulletinPrintDao.this.beforeInject(table, row, cell, rowIndex, colIndex, value, archivalFioRowList);
                }

                @Override
                public void beforeModify(final RtfTable table, final int currentRowIndex)
                {
                    SessionAttestationBulletinPrintDao.this.beforeModify(table, currentRowIndex, useCurrentRating, multiGroup);
                }
            })
            .modify(document);

        final RtfInjectModifier modifier = new RtfInjectModifier();
        modifier.put("academyTitle", academy.getPrintTitle());
        modifier.put("discipline", discipline.getTitleWithNumber());
        modifier.put("cathedra", discipline.getTutorOu().getPrintTitle());
        modifier.put("lecturersFIO", UniStringUtils.joinUnique(tutors, SessionComissionPps.pps().person().fio().s(), ", "));
        modifier.put("groups", groups);
        modifier.put("courses", getCourses(studentList));
        modifier.put("attestedNumber", String.valueOf(attestedCount));
        modifier.put("nonAttestedNumber", String.valueOf(notAttestedCount));
        modifier.put("attestedPercent", studentCount == 0 ? "" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(((double) 100 * attestedCount) / studentCount));
        modifier.put("nonAttestedPercent", studentCount == 0 ? "" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(((double) 100 * notAttestedCount) / studentCount));
        modifier.put("lecturersFullFIO", tutorsNewLined);
        modifier.put("cathedraHead", cathedraHead == null ? "" : cathedraHead.getPerson().getFio());
        modifier.modify(document);
    }

    private RtfDocument getTemplateRtf()
    {
        final UnisessionCommonTemplate template = this.get(UnisessionCommonTemplate.class, UnisessionCommonTemplate.code().s(), UnisessionCommonTemplateCodes.ATTESTATION_BULLETIN);
        return new RtfReader().read(template.getContent()).getClone();
    }

    protected void beforeModify(RtfTable table, int currentRowIndex, boolean useCurrentRating, boolean multiGroup)
    {
        if (!useCurrentRating) {
            UniRtfUtil.deleteCell(table.getRowList().get(currentRowIndex), 3, 1); //ячейка в строке с меткой T
            UniRtfUtil.deleteCell(table.getRowList().get(currentRowIndex - 1), 3, 1); //ячейка в шапке
        }
        if (!multiGroup) {
            RtfUtil.deleteCellPrevIncrease(table.getRowList().get(currentRowIndex), 2); //ячейка в строке с меткой T
            RtfUtil.deleteCellPrevIncrease(table.getRowList().get(currentRowIndex - 1), 2); //ячейка в шапке
        }
    }

    protected List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value, List<Integer> italicFioRowList)
    {
        // там где это нужно делаем italic
        if (value != null && colIndex == 1 && italicFioRowList.contains(rowIndex + 1))
        {
            RtfString string = new RtfString();
            string.append(IRtfData.I).append(value).append(IRtfData.I, 0);
            return string.toList();
        }
        return null;
    }

    protected String[] getBulletinTableRow(int number, StudentData student, boolean useCurrentRating, boolean multiGroup)
    {
        List<String> result = new ArrayList<>();
        result.add(String.valueOf(number));
        result.add(student.student.getPerson().getFullFio());
        if (multiGroup)
            result.add(student.student.getGroup() == null ? "" : student.student.getGroup().getTitle());
        if (useCurrentRating)
            result.add(student.slot == null ? (student.shouldPass ? "" : "-") : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(student.slot.getFixedRating()));
        result.add(student.slot == null || student.slot.getMark() == null ? (student.shouldPass ? "" : "-") : yes_no_formatter.format(student.slot.getMark().isPositive()));
        return result.toArray(new String[result.size()]);
    }

    private String getCourses(List<StudentData> studentList)
    {
        List<String> strings = new ArrayList<>();
        for (StudentData student : studentList)
            strings.add(student.slot == null ? "" : student.slot.getStudentWpe().getCourse().getTitle());
        return UniStringUtils.joinUniqueSorted(strings, ", ");
    }

    private String getGroups(List<StudentData> actualStudentList)
    {
        List<String> strings = new ArrayList<>();
        for (StudentData student : actualStudentList)
        {
            if (student.student.getGroup() != null)
                strings.add(student.student.getGroup().getTitle());
            else
                strings.add("вне групп");
        }
        return UniStringUtils.joinUniqueSorted(strings, ", ");
    }

    protected static class StudentData implements Comparable<StudentData>
    {
        Student student;
        SessionAttestationSlot slot;
        SessionAttestationSlotAdditionalData data;
        boolean shouldPass;

        private StudentData(Student student, SessionAttestationSlot slot, SessionAttestationSlotAdditionalData data, boolean shouldPass)
        {
            this.student = student;
            this.slot = slot;
            this.data = data;
            this.shouldPass = shouldPass;
        }

        @Override public int compareTo(StudentData o) {
            int i = CommonCollator.RUSSIAN_COLLATOR.compare(student.getFio(), o.student.getFio());
            if (i != 0) { return i; }
            return Long.compare(student.getId(), o.student.getId());
        }
        public Student getStudent() { return student; }
        public SessionAttestationSlot getSlot() { return slot; }
        public SessionAttestationSlotAdditionalData getData() { return data; }
        public boolean isShouldPass() { return shouldPass; }
    }
}
