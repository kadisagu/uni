/* $Id: $ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.SfeSummaryResultList;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.SfaSummaryResultList.SessionReportSfaSummaryResultListUI;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.SfeSummaryResultAdd.SessionReportSfeSummaryResultAdd;

/**
 * @author Andrey Andreev
 * @since 27.10.2016
 */
@State({@Bind(key = SessionReportManager.BIND_ORG_UNIT, binding = "ouHolder.id")})
public class SessionReportSfeSummaryResultListUI extends SessionReportSfaSummaryResultListUI
{
    // Listeners
    @Override
    public void onClickAdd()
    {
        getActivationBuilder()
                .asDesktopRoot(SessionReportSfeSummaryResultAdd.class)
                .parameter(SessionReportManager.BIND_ORG_UNIT, getOuHolder().getId())
                .activate();
    }

    @Override
    public String getViewPermissionKey()
    {
        return getSec().getPermission(getOuHolder().getId() != null ? "orgUnit_viewStateFinalExamSummaryResultList" : "stateFinalExamSummaryResultReport");
    }

    @Override
    public String getAddPermissionKey()
    {
        return getSec().getPermission(getOuHolder().getId() != null ? "orgUnit_addStateFinalExamSummaryResult" : "addSessionStorableReport");
    }

    @Override
    public String getDeletePermissionKey()
    {
        return getSec().getPermission(getOuHolder().getId() != null ? "orgUnit_deleteStateFinalExamSummaryResult" : "deleteSessionStorableReport");
    }
}
