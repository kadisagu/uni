/* $Id$ */
package ru.tandemservice.unisession.attestation.bo.CurrentMarkScale.logic;

import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unisession.entity.catalog.SessionAttestationMarkCatalogItem;

/**
 * @author Nikolay Fedorovskih
 * @since 07.10.2014
 */
public interface ICurrentMarkScaleDAO extends IUniBaseDao
{
    /**
     * Изменение признака "Положительная" для оценки текущего контроля на противоположный
     *
     * @param currentMarkItemId идентификатор элемента справочника
     */
    void invertMarkPositive(Long currentMarkItemId);

    /**
     * Изменение признака "Активная" для шкалы оценок текущего контроля на противоположный.
     * Т.к. активных шкал не может быть больше одной, если шкала была до этого неактивная, то предыдущая активная делается неактивной.
     * Если изменяемая шкала была активной, а в справочнике было всего 2 шкалы, то активной делается вторая шкала.
     * Если изменяемая шкала была активной, а в справочнике было всего более 2 шкал, то никаких изменений не происходит - изменяемая шкала остается активной.
     *
     * @param currentScaleId идентификатор элемента справочника шкал оценок тек. контроля
     */
    void invertScaleActiveProperty(Long currentScaleId);

    /**
     * Сохранение новой оценки шкалы тек. контроля
     *
     * @param mark оценка
     */
    void saveNewMark(SessionAttestationMarkCatalogItem mark);

    /**
     * Повысить приоритет оценки до максимального
     *
     * @param markId идентификатор оценки из шкалы текущего контроля
     */
    void moveToTopMarkPriority(Long markId);

    /**
     * Повысить приоритет оценки до минимального
     *
     * @param markId идентификатор оценки из шкалы текущего контроля
     */
    void moveToBottomMarkPriority(Long markId);

    /**
     * Повысить приоритет оценки
     *
     * @param markId идентификатор оценки из шкалы текущего контроля
     */
    void moveUpMarkPriority(Long markId);

    /**
     * Понизить приоритет оценки
     *
     * @param markId идентификатор оценки из шкалы текущего контроля
     */
    void moveDownMarkPriority(Long markId);

    /**
     * Удаление шкалы оценок текущего контроля.
     * Если шкала активная, выдается пользовательское сообщение об ошибке.
     *
     * @param scaleId идентификатор шкалы
     */
    void deleteCurrentScale(Long scaleId);
}