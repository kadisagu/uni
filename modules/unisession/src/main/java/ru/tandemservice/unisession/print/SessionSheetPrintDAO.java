/* $*/

package ru.tandemservice.unisession.print;

import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfElementFactory;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.node.IRtfText;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniepp.entity.registry.EppRegistryAttestation;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryPractice;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.entity.catalog.UnisessionCommonTemplate;
import ru.tandemservice.unisession.entity.catalog.codes.UnisessionCommonTemplateCodes;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionProjectTheme;
import ru.tandemservice.unisession.entity.document.SessionSheetDocument;
import ru.tandemservice.unisession.entity.document.SessionSlotRatingData;
import ru.tandemservice.unisession.entity.document.gen.SessionProjectThemeGen;
import ru.tandemservice.unisession.entity.document.gen.SessionSlotRatingDataGen;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.mark.SessionMarkRatingData;
import ru.tandemservice.unisession.entity.mark.SessionSlotRegularMark;
import ru.tandemservice.unisession.entity.mark.gen.SessionMarkRatingDataGen;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * todo оптимизировать массовую печать и вообще число запросов
 *
 * @author oleyba
 * @since 4/5/11
 */
public class SessionSheetPrintDAO extends UniBaseDao implements ISessionSheetPrintDAO
{
    @Override
    public final RtfDocument printSheetList(final Collection<Long> ids)
    {
        final UnisessionCommonTemplate templateItem = this.get(UnisessionCommonTemplate.class, UnisessionCommonTemplate.code().s(), UnisessionCommonTemplateCodes.SHEET_MASS_PRINT_FRAME);
        final RtfDocument template = new RtfReader().read(templateItem.getContent()).getClone();
        final RtfDocument result = template.getClone();

        final String[] tags = new String[] { "A", "B", "C", "D" };
        BatchUtils.execute(ids, tags.length, new BatchUtils.Action<Long>()
                {
            int i = 0;

            @Override
            @SuppressWarnings("deprecation")
            public void execute(final Collection<Long> ids)
            {
                final IRtfElementFactory elementFactory = RtfBean.getElementFactory();
                final IRtfText lineBreak = elementFactory.createRtfText("\n\\par\\pard");
                lineBreak.setRaw(true);
                final IRtfText pageBreak = elementFactory.createRtfText("\n\\sect");
                pageBreak.setRaw(true);

                if (this.i++ > 0)
                {
                    result.getElementList().add(pageBreak);
                    result.getElementList().addAll(template.getClone().getElementList());
                }

                final RtfInjectModifier injectModifier = new RtfInjectModifier();
                final Iterator<Long> iterator = ids.iterator();
                for (final String tag : tags)
                {
                    if (iterator.hasNext())
                    {
                        // todo загрузить все данные массово и вызвать другой метод
                        final RtfDocument page = SessionSheetPrintDAO.this.printSheet(iterator.next());
                        RtfUtil.modifySourceList(result.getHeader(), page.getHeader(), page.getElementList());
                        injectModifier.put(tag, Arrays.asList(lineBreak, getTable(page.getElementList())));
                    } else
                    {
                        injectModifier.put(tag, "");
                    }
                }
                injectModifier.modify(result);
            }
                });

        return result;
    }

    @Override
    public final RtfDocument printSheet(final Long id)
    {
        return this.printSheet(this.get(SessionSheetDocument.class, id), this.get(SessionDocumentSlot.class, SessionDocumentSlot.document().id().s(), id), this.get(SessionMark.class, SessionMark.slot().document().id().s(), id));
    }

    protected RtfDocument printSheet(final SessionSheetDocument sheet, final SessionDocumentSlot slot, final SessionMark mark)
    {
        UnisessionCommonTemplate template;
        ISessionBrsDao.ISessionRatingSettings ratingSettings = ISessionBrsDao.instance.get().getRatingSettings(ISessionBrsDao.instance.get().key(slot));
        if (null != ratingSettings && ratingSettings.useCurrentRating()) {
            template = this.get(UnisessionCommonTemplate.class, UnisessionCommonTemplate.code().s(), UnisessionCommonTemplateCodes.SHEET_WITH_CURRENT_RATING);
        }
        else if (null != ratingSettings && ratingSettings.usePoints()) {
            template = this.get(UnisessionCommonTemplate.class, UnisessionCommonTemplate.code().s(), UnisessionCommonTemplateCodes.SHEET_WITH_POINTS);
        }
        else {
            template = this.get(UnisessionCommonTemplate.class, UnisessionCommonTemplate.code().s(), UnisessionCommonTemplateCodes.SHEET);
        }

        final RtfDocument document = new RtfReader().read(template.getContent()).getClone();

        final TopOrgUnit academy = TopOrgUnit.getInstance();

        // todo массовая загрузка данных - вытащить в параметры метода все, что подгружается тут
        final DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(SessionComissionPps.class, "rel")
            .column(property("rel", SessionComissionPps.pps()))
            .joinPath(DQLJoinType.inner, SessionComissionPps.pps().person().identityCard().fromAlias("rel").s(), "idc")
            .joinEntity("rel", DQLJoinType.inner, SessionDocumentSlot.class, "slot", eq(property(SessionDocumentSlot.commission().id().fromAlias("slot")), property(SessionComissionPps.commission().id().fromAlias("rel"))))
            .where(eq(property("slot.id"), value(slot.getId())))
            .order(property("idc", IdentityCard.lastName()))
            .order(property("idc", IdentityCard.firstName()))
            .order(property("idc", IdentityCard.middleName()));
        final List<PpsEntry> commission = dql.createStatement(this.getSession()).list();
        final EppRegistryElement registryElement = slot.getStudentWpeCAction().getStudentWpe().getRegistryElementPart().getRegistryElement();

        String points = "";
        String currentRating = "";
        String scoredPoints = "";
        SessionSlotRatingData slotRatingData = getByNaturalId(new SessionSlotRatingDataGen.NaturalId(slot));
        if (null != slotRatingData)
            currentRating = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(slotRatingData.getFixedCurrentRating());
        if (mark != null && mark.getPoints() != null) {
            points = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(mark.getPoints());
        }
        if (mark instanceof SessionSlotRegularMark) {
            SessionMarkRatingData markRatingData = getByNaturalId(new SessionMarkRatingDataGen.NaturalId((SessionSlotRegularMark) mark));
            if (null != markRatingData)
                scoredPoints = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(markRatingData.getScoredPoints());
        }

		boolean themeRequired = slot.getStudentWpeCAction().getActionType().isThemeRequired();
		String theme = null;
		if (themeRequired)
		{
			SessionProjectTheme slotTheme = getByNaturalId(new SessionProjectThemeGen.NaturalId(slot));
			theme = (slotTheme == null ? null : slotTheme.getTheme());
		}

		List<String> fieldsToRemove = new ArrayList<>();

        final RtfInjectModifier modifier = new RtfInjectModifier();
        modifier.put("academyTitle", academy.getNominativeCaseTitle() == null ? academy.getTitle() == null ? "" : academy.getTitle() : academy.getNominativeCaseTitle());
        modifier.put("documentNumber", sheet.getNumber());
        modifier.put("studentFio", slot.getActualStudent().getPerson().getFullFio());
        modifier.put("studentEduSubject", slot.getActualStudent().getEducationOrgUnit().getEducationLevelHighSchool().getDisplayableTitle());
        printOuData(modifier, "document", slot.getDocument().getGroupOu());
        printOuData(modifier, "formative", slot.getActualStudent().getEducationOrgUnit().getFormativeOrgUnit());
        printOuData(modifier, "territorial", slot.getActualStudent().getEducationOrgUnit().getTerritorialOrgUnit());
        modifier.put("group", slot.getActualStudent().getGroup() == null ? "" : slot.getActualStudent().getGroup().getTitle());

        if (registryElement instanceof EppRegistryAttestation) {
            modifier.put("regType", "Мероприятие ГИА");
        } else if (registryElement instanceof EppRegistryPractice) {
            modifier.put("regType", "Практика");
        } else {
            modifier.put("regType", "Дисциплина");
        }
        modifier.put("registryElement", registryElement.getTitle() + " (" + slot.getStudentWpeCAction().getType().getTitle() + ")");
        modifier.put("formingDate", sheet.getIssueDate() == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(sheet.getIssueDate()));
        modifier.put("deadlineDate", sheet.getDeadlineDate() == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(sheet.getDeadlineDate()));
        modifier.put("N", slot.getTryNumber() == null ? "" : String.valueOf(slot.getTryNumber()));
        modifier.put("yearPart", slot.getStudentWpeCAction().getStudentWpe().getPart().getTitle());
        modifier.put("term", slot.getStudentWpeCAction().getStudentWpe().getTerm().getTitle());
        modifier.put("caType_N", slot.getStudentWpeCAction().getType().getTitle());
        modifier.put("course", String.valueOf(slot.getStudentWpeCAction().getStudentWpe().getCourse().getIntValue()));
        modifier.put("mark", mark == null ? "" : mark.getValueItem().getPrintTitle());
        modifier.put("points", points);
        modifier.put("currentRating", currentRating);
        modifier.put("scoredPoints", scoredPoints);
        modifier.put("performDate", mark == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(mark.getPerformDate()));
		if (themeRequired)
			modifier.put("theme", theme);
		else
			fieldsToRemove.add("theme");
        modifier.put("tutors", UniStringUtils.join(commission, PpsEntry.person().identityCard().fio().s(), ", "));
        SessionReportManager.addOuLeaderData(modifier, sheet.getOrgUnit(), "leader", "leaderTitle");

        addAdditionalData(modifier, sheet, slot, mark);

		UniRtfUtil.deleteRowsWithLabels(document, fieldsToRemove);

        modifier.modify(document);

        return document;
    }

    protected void printOuData(RtfInjectModifier modifier, String ouKind, OrgUnit ou)
    {
        modifier.put(ouKind + "Ou", ou.getPrintTitle());
        modifier.put(ouKind + "OuHeadIof", "");
        modifier.put(ouKind + "OuHeadFio", "");
        try {
            IdentityCard headIdc = (IdentityCard) ou.getHead().getEmployee().getPerson().getIdentityCard();
            modifier.put(ouKind + "OuHeadIof", headIdc.getIof());
            modifier.put(ouKind + "OuHeadFio", headIdc.getFio()); }
        catch (NullPointerException e) {
            // ну и нету там значит руководителя
        }
    }

    /**
     * Для добавления меток в проектном слое. Переопределите в контроллере печати и добавьте нужные метки в RtfInjectModifier
     * @param modifier RtfInjectModifier
     * @param sheet экз. лист
     * @param slot запись студента в экз. листе
     * @param mark оценка (null, если еще не выставлена)
     */
    protected void addAdditionalData(RtfInjectModifier modifier, SessionSheetDocument sheet, SessionDocumentSlot slot, SessionMark mark)
    {
        //To change body of created methods use File | Settings | File Templates.
    }

    private static RtfTable getTable(final List<IRtfElement> elementList) {
        for (final IRtfElement element : elementList) {
            if (element instanceof IRtfGroup) {
                final RtfTable table = getTable(((IRtfGroup) element).getElementList());
                if (null != table) { return table; }

            } else if (element instanceof RtfTable) {
                return (RtfTable) element;
            }
        }
        return null;
    }
}
