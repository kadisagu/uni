/* $Id: SessionReportResultsListUI.java 21825 2012-02-09 09:08:57Z oleyba $ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.ResultsByDiscList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.ResultsByDiscAdd.SessionReportResultsByDiscAdd;

/**
 * @author oleyba
 * @since 2/7/12
 */
@State
({
    @Bind(key = SessionReportManager.BIND_ORG_UNIT, binding = "ouHolder.id")
})
public class SessionReportResultsByDiscListUI extends UIPresenter
{
    private OrgUnitHolder ouHolder = new OrgUnitHolder();

    // actions

    @Override
    public void onComponentRefresh()
    {
        getOuHolder().refresh();
    }

    public void onSearchParamsChange()
    {
        onComponentRefresh();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(SessionReportManager.PARAM_ORG_UNIT, getOrgUnit());
        dataSource.put(SessionReportManager.PARAM_EDU_YEAR, getSettings().get("year"));
        dataSource.put(SessionReportResultsByDiscDSHandler.PARAM_EDU_YEAR, getSettings().get("year"));
        dataSource.put(SessionReportResultsByDiscDSHandler.PARAM_YEAR_PART, getSettings().get("part"));
    }

    public void onClickAddReport()
    {
        getActivationBuilder().asRegion(SessionReportResultsByDiscAdd.class).parameter(SessionReportManager.BIND_ORG_UNIT, getOuHolder().getId()).activate();
    }

    public void onClickPrint()
    {
        getActivationBuilder().asDesktopRoot(IUniComponents.DOWNLOAD_STORABLE_REPORT).parameter("reportId", getListenerParameterAsLong()).parameter("extension", "rtf").parameter("zip", Boolean.FALSE).activate();
    }

    public void onDeleteEntityFromList()
    {
        IUniBaseDao.instance.get().delete(getListenerParameterAsLong());
    }

    // preseneter

    public OrgUnit getOrgUnit()
    {
        return getOuHolder().getValue();
    }

    @Override
    public ISecured getSecuredObject()
    {
        return null == getOrgUnit() ? super.getSecuredObject() : getOrgUnit();
    }


    public String getViewPermissionKey(){ return getSec().getPermission(getOuHolder().getId() != null ? "orgUnit_viewSessionReportResultsByDiscList" : "sessionResultsByDiscReport"); }

    public String getAddStorableReportPermissionKey(){ return getSec().getPermission(getOuHolder().getId() != null ? "orgUnit_addSessionReportResultsByDiscList" : "addSessionStorableReport"); }

    public String getDeleteStorableReportPermissionKey(){ return getSec().getPermission(getOuHolder().getId() != null ? "orgUnit_deleteSessionReportResultsByDiscList" : "deleteSessionStorableReport"); }



    // getters and setters

    public OrgUnitHolder getOuHolder()
    {
        return ouHolder;
    }

    public CommonPostfixPermissionModelBase getSec()
    {
        return getOuHolder().getSecModel();
    }
}
