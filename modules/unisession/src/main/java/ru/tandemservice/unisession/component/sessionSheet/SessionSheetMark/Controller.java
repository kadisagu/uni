/* $*/

package ru.tandemservice.unisession.component.sessionSheet.SessionSheetMark;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;

/**
 * @author oleyba
 * @since 2/22/11
 */
public class Controller<D extends IDAO<M>, M extends Model> extends AbstractBusinessController<D, M>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final M model = this.getModel(component);
        this.getDao().prepare(model);
        if (model.isNeedToSaveCurrentRatingData()) {
            onClickSaveCurrentRating(component);
        }
    }

    public void onClickApply(final IBusinessComponent component)
    {
        this.getDao().update(this.getModel(component));
        this.deactivate(component);
    }

    public void onClickSaveCurrentRating(final IBusinessComponent component) {
        ISessionBrsDao.instance.get().saveCurrentRating(getModel(component).getSheet());
        this.getDao().prepareCurrentRatingData(this.getModel(component));
    }

}