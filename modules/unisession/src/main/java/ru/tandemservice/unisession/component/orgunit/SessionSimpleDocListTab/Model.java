package ru.tandemservice.unisession.component.orgunit.SessionSimpleDocListTab;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.unisession.entity.document.SessionDocument;

/**
 * @author vdanilov
 */
@Input({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "orgUnitHolder.id")
})
public class Model
{
    private final EntityHolder<OrgUnit> orgUnitHolder = new EntityHolder<OrgUnit>();
    private DynamicListDataSource<SessionDocument> dataSource;

    private String settingsKey;
    private IDataSettings settings;

    private ISelectModel yearModel;
    private ISelectModel partsModel;
    private ISelectModel courseCurrentModel;
    private ISelectModel groupCurrentModel;

    public IEntity getSecuredObject()
    {
        return this.getOrgUnit();
    }

    public Long getOrgUnitId()
    {
        return this.getOrgUnitHolder().getId();
    }

    public OrgUnit getOrgUnit()
    {
        return this.getOrgUnitHolder().getValue();
    }

    // getters and setters

    public EntityHolder<OrgUnit> getOrgUnitHolder()
    {
        return this.orgUnitHolder;
    }

    public IDataSettings getSettings()
    {
        return this.settings;
    }

    public void setSettings(final IDataSettings settings)
    {
        this.settings = settings;
    }

    public DynamicListDataSource<SessionDocument> getDataSource()
    {
        return this.dataSource;
    }

    public void setDataSource(final DynamicListDataSource<SessionDocument> dataSource)
    {
        this.dataSource = dataSource;
    }

    public String getSettingsKey()
    {
        return this.settingsKey;
    }

    public void setSettingsKey(final String settingsKey)
    {
        this.settingsKey = settingsKey;
    }

    public ISelectModel getYearModel()
    {
        return yearModel;
    }

    public void setYearModel(ISelectModel yearModel)
    {
        this.yearModel = yearModel;
    }

    public ISelectModel getPartsModel()
    {
        return partsModel;
    }

    public void setPartsModel(ISelectModel partsModel)
    {
        this.partsModel = partsModel;
    }

    public ISelectModel getCourseCurrentModel()
    {
        return courseCurrentModel;
    }

    public void setCourseCurrentModel(ISelectModel courseCurrentModel)
    {
        this.courseCurrentModel = courseCurrentModel;
    }

    public ISelectModel getGroupCurrentModel()
    {
        return groupCurrentModel;
    }

    public void setGroupCurrentModel(ISelectModel groupCurrentModel)
    {
        this.groupCurrentModel = groupCurrentModel;
    }
}
