package ru.tandemservice.unisession.entity.document.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4ActionType;
import ru.tandemservice.unisession.entity.comission.SessionComission;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;
import ru.tandemservice.unisession.entity.document.SessionDocument;
import ru.tandemservice.unisession.entity.document.SessionObject;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Ведомость
 *
 * Ведомость (формируется на основе учебной группы по видам аудиторной нагрузки)
 * показывает, что учебная группа будет обслуживаться в рамках указанной сессии (семестрового журнала)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionBulletinDocumentGen extends SessionDocument
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.document.SessionBulletinDocument";
    public static final String ENTITY_NAME = "sessionBulletinDocument";
    public static final int VERSION_HASH = 791875662;
    private static IEntityMeta ENTITY_META;

    public static final String L_GROUP = "group";
    public static final String L_SESSION_OBJECT = "sessionObject";
    public static final String P_PERFORM_DATE = "performDate";
    public static final String L_COMMISSION = "commission";
    public static final String P_REGISTRY_ELEMENT_TITLE = "registryElementTitle";
    public static final String P_TITLE = "title";

    private EppRealEduGroup4ActionType _group;     // Группа
    private SessionObject _sessionObject;     // Сессия
    private Date _performDate;     // Дата проведения (фактическая)
    private SessionComission _commission;     // Комиссия

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Группа. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EppRealEduGroup4ActionType getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа. Свойство не может быть null и должно быть уникальным.
     */
    public void setGroup(EppRealEduGroup4ActionType group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Сессия. Свойство не может быть null.
     */
    @NotNull
    public SessionObject getSessionObject()
    {
        return _sessionObject;
    }

    /**
     * @param sessionObject Сессия. Свойство не может быть null.
     */
    public void setSessionObject(SessionObject sessionObject)
    {
        dirty(_sessionObject, sessionObject);
        _sessionObject = sessionObject;
    }

    /**
     * @return Дата проведения (фактическая).
     */
    public Date getPerformDate()
    {
        return _performDate;
    }

    /**
     * @param performDate Дата проведения (фактическая).
     */
    public void setPerformDate(Date performDate)
    {
        dirty(_performDate, performDate);
        _performDate = performDate;
    }

    /**
     * @return Комиссия. Свойство не может быть null.
     */
    @NotNull
    public SessionComission getCommission()
    {
        return _commission;
    }

    /**
     * @param commission Комиссия. Свойство не может быть null.
     */
    public void setCommission(SessionComission commission)
    {
        dirty(_commission, commission);
        _commission = commission;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof SessionBulletinDocumentGen)
        {
            setGroup(((SessionBulletinDocument)another).getGroup());
            setSessionObject(((SessionBulletinDocument)another).getSessionObject());
            setPerformDate(((SessionBulletinDocument)another).getPerformDate());
            setCommission(((SessionBulletinDocument)another).getCommission());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionBulletinDocumentGen> extends SessionDocument.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionBulletinDocument.class;
        }

        public T newInstance()
        {
            return (T) new SessionBulletinDocument();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "group":
                    return obj.getGroup();
                case "sessionObject":
                    return obj.getSessionObject();
                case "performDate":
                    return obj.getPerformDate();
                case "commission":
                    return obj.getCommission();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "group":
                    obj.setGroup((EppRealEduGroup4ActionType) value);
                    return;
                case "sessionObject":
                    obj.setSessionObject((SessionObject) value);
                    return;
                case "performDate":
                    obj.setPerformDate((Date) value);
                    return;
                case "commission":
                    obj.setCommission((SessionComission) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "group":
                        return true;
                case "sessionObject":
                        return true;
                case "performDate":
                        return true;
                case "commission":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "group":
                    return true;
                case "sessionObject":
                    return true;
                case "performDate":
                    return true;
                case "commission":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "group":
                    return EppRealEduGroup4ActionType.class;
                case "sessionObject":
                    return SessionObject.class;
                case "performDate":
                    return Date.class;
                case "commission":
                    return SessionComission.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionBulletinDocument> _dslPath = new Path<SessionBulletinDocument>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionBulletinDocument");
    }
            

    /**
     * @return Группа. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unisession.entity.document.SessionBulletinDocument#getGroup()
     */
    public static EppRealEduGroup4ActionType.Path<EppRealEduGroup4ActionType> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Сессия. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionBulletinDocument#getSessionObject()
     */
    public static SessionObject.Path<SessionObject> sessionObject()
    {
        return _dslPath.sessionObject();
    }

    /**
     * @return Дата проведения (фактическая).
     * @see ru.tandemservice.unisession.entity.document.SessionBulletinDocument#getPerformDate()
     */
    public static PropertyPath<Date> performDate()
    {
        return _dslPath.performDate();
    }

    /**
     * @return Комиссия. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionBulletinDocument#getCommission()
     */
    public static SessionComission.Path<SessionComission> commission()
    {
        return _dslPath.commission();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.entity.document.SessionBulletinDocument#getRegistryElementTitle()
     */
    public static SupportedPropertyPath<String> registryElementTitle()
    {
        return _dslPath.registryElementTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.entity.document.SessionBulletinDocument#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends SessionBulletinDocument> extends SessionDocument.Path<E>
    {
        private EppRealEduGroup4ActionType.Path<EppRealEduGroup4ActionType> _group;
        private SessionObject.Path<SessionObject> _sessionObject;
        private PropertyPath<Date> _performDate;
        private SessionComission.Path<SessionComission> _commission;
        private SupportedPropertyPath<String> _registryElementTitle;
        private SupportedPropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Группа. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unisession.entity.document.SessionBulletinDocument#getGroup()
     */
        public EppRealEduGroup4ActionType.Path<EppRealEduGroup4ActionType> group()
        {
            if(_group == null )
                _group = new EppRealEduGroup4ActionType.Path<EppRealEduGroup4ActionType>(L_GROUP, this);
            return _group;
        }

    /**
     * @return Сессия. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionBulletinDocument#getSessionObject()
     */
        public SessionObject.Path<SessionObject> sessionObject()
        {
            if(_sessionObject == null )
                _sessionObject = new SessionObject.Path<SessionObject>(L_SESSION_OBJECT, this);
            return _sessionObject;
        }

    /**
     * @return Дата проведения (фактическая).
     * @see ru.tandemservice.unisession.entity.document.SessionBulletinDocument#getPerformDate()
     */
        public PropertyPath<Date> performDate()
        {
            if(_performDate == null )
                _performDate = new PropertyPath<Date>(SessionBulletinDocumentGen.P_PERFORM_DATE, this);
            return _performDate;
        }

    /**
     * @return Комиссия. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionBulletinDocument#getCommission()
     */
        public SessionComission.Path<SessionComission> commission()
        {
            if(_commission == null )
                _commission = new SessionComission.Path<SessionComission>(L_COMMISSION, this);
            return _commission;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.entity.document.SessionBulletinDocument#getRegistryElementTitle()
     */
        public SupportedPropertyPath<String> registryElementTitle()
        {
            if(_registryElementTitle == null )
                _registryElementTitle = new SupportedPropertyPath<String>(SessionBulletinDocumentGen.P_REGISTRY_ELEMENT_TITLE, this);
            return _registryElementTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.entity.document.SessionBulletinDocument#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(SessionBulletinDocumentGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return SessionBulletinDocument.class;
        }

        public String getEntityName()
        {
            return "sessionBulletinDocument";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getRegistryElementTitle();

    public abstract String getTitle();
}
