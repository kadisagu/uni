/* $Id: $ */
package ru.tandemservice.unisession.base.bo.SessionReport.logic;


import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.catalog.codes.SessionMarkGradeValueCatalogItemCodes;
import ru.tandemservice.unisession.entity.catalog.codes.SessionMarkStateCatalogItemCodes;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Andrey Andreev
 * @since 02.12.2016
 */
public abstract class AbsStateFinalAttestationRow
{
    protected String _title;                // Название
    protected String _dates;                // Дата проведения
    protected int _total = 0;               // Колонка Всего
    protected int _notAllowed = 0;          // Не допущен
    protected int _vacation = 0;           // В академическом отпуске
    protected int _must = 0;               // Обязаны сдавать
    protected int _delay = 0;              // Отсрочка
    protected int _appeared = 0;            // Явились
    protected int _passed = 0;              // Прошли
    protected int _goodPassed = 0;         // Хорошо Прошли

    protected int _budget = 0;              // Из прошедших обучаются на бюджете

    protected Double _progress;             // Успеваемость, %
    protected Double _goodProgress;         // Качественный показатель, %
    // Оценки
    protected int _mark5 = 0;
    protected int _mark4 = 0;
    protected int _mark3 = 0;
    protected int _mark2 = 0;

    public void addDataFromMarkValue(SessionMarkCatalogItem mark)
    {
        switch (mark.getCode())
        {
            case SessionMarkGradeValueCatalogItemCodes.OTLICHNO:
                _mark5++;
                break;
            case SessionMarkGradeValueCatalogItemCodes.HOROSHO:
                _mark4++;
                break;
            case SessionMarkGradeValueCatalogItemCodes.UDOVLETVORITELNO:
                _mark3++;
                break;
            case SessionMarkGradeValueCatalogItemCodes.NEUDOVLETVORITELNO:
                _mark2++;
                break;
        }
    }

    protected void addDataFromStudent(Student student)
    {
        switch (student.getStatus().getCode())
        {
            case "1":           // активный
            case "7":           // отп.с.посещ
            case "22":          // отп.по.ух.за.реб.с.пос.
                _must++;
                break;
            case "4":           // отложить защиту
                _must++;
                _delay++;
                break;
            case "5":           // отп.акад.б.посещ
            case "6":           // отп.б.посещ.
            case "20":          // отп.по.бер.и.род
            case "21":          // отп.по.ух.за.реб.
            case "23":          // отп.по.ух.за.реб.б.пос/
                _vacation++;
                break;
        }
    }

    public void addFromRaw(Collection<? extends SfaRaw> raws)
    {
        if (raws == null) return;

        List<Date> dates = new ArrayList<>();
        raws.stream()
                .peek(raw ->
                      {
                          if (raw.getMark() != null) dates.add(raw.getMark().getPerformDate());
                      })
                .collect(Collectors.groupingBy(SfaRaw::getStudent, Collectors.mapping(raw -> raw, Collectors.toList())))
                .entrySet().forEach(entry -> addFromRaw(entry.getKey(), entry.getValue()));

        _dates =  StateFinalAttestationUtil.getDatesString(dates);

        calcProgress();
    }

    protected void addFromRaw(Student student, List<? extends SfaRaw> raws)
    {
        addDataFromStudent(student);
        _total++;

        if (raws == null) return;

        boolean appeared = true;
        boolean notAllowed = false;
        boolean passed = true;
        boolean goodProgress = true;
        for (SfaRaw raw : raws)
        {
            SessionMark mark = raw.getMark();
            if (mark == null)
            {
                passed = false;
                appeared = false;
                continue;
            }

            SessionMarkCatalogItem markValue = mark.getValueItem();
            if (SessionMarkStateCatalogItemCodes.NO_ADMISSION.equals(markValue.getCode()))
            {
                notAllowed = true;
                passed = false;
            }

            if (markValue instanceof SessionMarkGradeValueCatalogItem) // Явились на госэкзамен
            {
                if (!((SessionMarkGradeValueCatalogItem) markValue).isPositive())  // Сдали госэкзамен
                    passed = false;

                this.addDataFromMarkValue(markValue);

                if (SessionMarkGradeValueCatalogItemCodes.UDOVLETVORITELNO.equals(markValue.getCode())
                        || SessionMarkGradeValueCatalogItemCodes.NEUDOVLETVORITELNO.equals(markValue.getCode()))
                    goodProgress = false;
            }
            else
            {
                appeared = false;
                passed = false;
            }
        }


        if (appeared) _appeared++;
        if (notAllowed) _notAllowed++;

        if (passed)
        {
            _passed++;
            if (goodProgress)
                _goodPassed++;
            if (student.getCompensationType().isBudget())
                _budget++;
        }
    }

    public void calcProgress()
    {
        _progress = _total > 0 ? 100 * _passed / (double) _total : null;
        _goodProgress = _total > 0 ? 100 * (_mark4 + _mark5) / (double) _total : null;
    }

    public void addFromRows(Collection<? extends AbsStateFinalAttestationRow> rows)
    {
        if (rows == null) return;

        rows.forEach(this::addFromRow);

        calcProgress();
    }

    public <T extends AbsStateFinalAttestationRow> void addFromRow(T row)
    {
        _total += row.getTotal();
        _notAllowed += row.getNotAllowed();
        _appeared += row.getAppeared();
        _passed += row.getPassed();
        _vacation += row.getVacation();
        _must += row.getMust();
        _delay += row.getDelay();

        _mark5 += row.getMark5();
        _mark4 += row.getMark4();
        _mark3 += row.getMark3();
        _mark2 += row.getMark2();

        _goodPassed = _mark4 + _mark5;
    }


    public String getTitle()
    {
        return _title;
    }

    public void setTitle(String title)
    {
        this._title = title;
    }

    public String getDates()
    {
        return _dates;
    }

    public void setDates(String dates)
    {
        this._dates = dates;
    }


    public int getTotal()
    {
        return _total;
    }

    public void setTotal(int total)
    {
        this._total = total;
    }

    public int getAppeared()
    {
        return _appeared;
    }

    public void setAppeared(int appeared)
    {
        this._appeared = appeared;
    }

    public int getDelay()
    {
        return _delay;
    }

    public void setDelay(int delay)
    {
        this._delay = delay;
    }

    public int getMust()
    {
        return _must;
    }

    public void setMust(int must)
    {
        this._must = must;
    }

    public int getVacation()
    {
        return _vacation;
    }

    public void setVacation(int vacation)
    {
        this._vacation = vacation;
    }

    public int getNotAllowed()
    {
        return _notAllowed;
    }

    public void setNotAllowed(int notAllowed)
    {
        this._notAllowed = notAllowed;
    }

    public int getPassed()
    {
        return _passed;
    }

    public void setPassed(int passed)
    {
        this._passed = passed;
    }

    public int getGoodPassed()
    {
        return _goodPassed;
    }

    public void setGoodPassed(int goodPassed)
    {
        _goodPassed = goodPassed;
    }


    public int getBudget()
    {
        return _budget;
    }

    public void setBudget(int budget)
    {
        _budget = budget;
    }


    public int getMark2()
    {
        return _mark2;
    }

    public void setMark2(int mark2)
    {
        this._mark2 = mark2;
    }

    public int getMark3()
    {
        return _mark3;
    }

    public void setMark3(int mark3)
    {
        this._mark3 = mark3;
    }

    public int getMark4()
    {
        return _mark4;
    }

    public void setMark4(int mark4)
    {
        this._mark4 = mark4;
    }

    public int getMark5()
    {
        return _mark5;
    }

    public void setMark5(int mark5)
    {
        this._mark5 = mark5;
    }


    public Double getProgress()
    {
        return _progress;
    }

    public void setProgress(Double progress)
    {
        this._progress = progress;
    }

    public Double getGoodProgress()
    {
        return _goodProgress;
    }

    public void setGoodProgress(Double goodProgress)
    {
        this._goodProgress = goodProgress;
    }
}
