// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisession.component.sessionSheet.SessionSheetPubInline;

import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.dao.document.ISessionDocumentBaseDAO;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionProjectTheme;
import ru.tandemservice.unisession.entity.document.SessionSheetDocument;
import ru.tandemservice.unisession.entity.document.SessionSlotRatingData;
import ru.tandemservice.unisession.entity.document.gen.SessionSlotRatingDataGen;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.mark.SessionMarkRatingData;
import ru.tandemservice.unisession.entity.mark.SessionSlotRegularMark;
import ru.tandemservice.unisession.entity.mark.gen.SessionMarkRatingDataGen;

/**
 * @author oleyba
 * @since 2/16/11
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setSheet(this.getNotNull(SessionSheetDocument.class, model.getSheet().getId()));
        model.setSlot(this.get(SessionDocumentSlot.class, SessionDocumentSlot.document().s(), model.getSheet()));
        model.setMark(this.get(SessionMark.class, SessionMark.slot().s(), model.getSlot()));
        model.setTutors(CommonBaseUtil.<PpsEntry>getPropertiesList(this.getList(SessionComissionPps.class, SessionComissionPps.commission().s(), model.getSlot().getCommission()), SessionComissionPps.pps().s()));

        SessionSlotRatingData slotRatingData = getByNaturalId(new SessionSlotRatingDataGen.NaturalId(model.getSlot()));
        if (null != slotRatingData)
            model.setCurrentRating(slotRatingData.getFixedCurrentRating());
        if (model.getMark() instanceof SessionSlotRegularMark) {
            SessionMarkRatingData markRatingData = getByNaturalId(new SessionMarkRatingDataGen.NaturalId((SessionSlotRegularMark) model.getMark()));
            if (null != markRatingData)
                model.setScoredPoints(markRatingData.getScoredPoints());
        }

        model.setRatingSettings(ISessionBrsDao.instance.get().getRatingSettings(ISessionBrsDao.instance.get().key(model.getSlot())));

		model.setThemeRequired(ISessionDocumentBaseDAO.instance.get().isThemeRequired(model.getSheet()));
		if (model.isThemeRequired())
		{
			SessionProjectTheme theme = getByNaturalId(new SessionProjectTheme.NaturalId(model.getSlot()));
			model.setTheme(theme == null ? null : theme.getTheme());
		}

        initAdditionalMarkData(model);
    }

    protected void initAdditionalMarkData(Model model)
    {
        // для переопределения в проектах
    }
}