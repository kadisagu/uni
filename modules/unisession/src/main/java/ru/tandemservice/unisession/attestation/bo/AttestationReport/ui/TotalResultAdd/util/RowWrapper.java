/**
 *$Id$
 */
package ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.TotalResultAdd.util;

import ru.tandemservice.uni.report.summaryReports.SummaryDataWrapper;

/**
 * @author Alexander Shaburov
 * @since 23.11.12
 */
public class RowWrapper extends SummaryDataWrapper<StudentWrapper>
{
    public RowWrapper(Long groupedEntityId, String groupedTitle)
    {
        setId(groupedEntityId);
        setTitle(groupedTitle);
    }

    // Getters & Setters

    public Long getGroupedId()
    {
        return getId();
    }
}
