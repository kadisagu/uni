package ru.tandemservice.unisession.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unisession_2x8x0_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность unisessionResultsReport

		// создано обязательное свойство targetAdmission
		{

            if (!tool.columnExists("session_rep_results_t", "targetadmission_p"))
            {
                // создать колонку
                tool.createColumn("session_rep_results_t", new DBColumn("targetadmission_p", DBType.BOOLEAN));

                // задать значение по умолчанию
                java.lang.Boolean defaultTargetAdmission = false;
                tool.executeUpdate("update session_rep_results_t set targetadmission_p=? where targetadmission_p is null", defaultTargetAdmission);

                // сделать колонку NOT NULL
                tool.setColumnNullable("session_rep_results_t", "targetadmission_p", false);
            }
		}


    }
}