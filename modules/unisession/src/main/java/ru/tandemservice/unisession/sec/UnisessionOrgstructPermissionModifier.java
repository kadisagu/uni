/* $*/

package ru.tandemservice.unisession.sec;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.ISecurityConfigMetaMapModifier;
import org.tandemframework.sec.meta.*;
import org.tandemframework.shared.commonbase.sec.PermissionMetaUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import ru.tandemservice.unisession.attestation.entity.SessionAttestation;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin;
import ru.tandemservice.unisession.entity.catalog.StudentPortfolioElement;
import ru.tandemservice.unisession.entity.document.*;
import ru.tandemservice.unisession.entity.mark.SessionALRequest;

import java.util.Map;

/**
 * @author oleyba
 * @since 3/30/11
 */
public class UnisessionOrgstructPermissionModifier implements ISecurityConfigMetaMapModifier
{
    @Override
    public void modify(final Map<String, SecurityConfigMeta> securityConfigMetaMap)
    {
        final SecurityConfigMeta config = new SecurityConfigMeta();
        config.setModuleName("uni");
        config.setName("unisession-orgstruct-sec-config");
        config.setTitle("");

        final ModuleGlobalGroupMeta mggOrgstruct = PermissionMetaUtil.createModuleGlobalGroup(config, "orgstructModuleGlobalGroup", "Модуль «Орг. структура»");
        final ModuleLocalGroupMeta mlgOrgstruct = PermissionMetaUtil.createModuleLocalGroup(config, "orgstructLocalGroup", "Модуль «Орг. структура»");

        final ModuleLocalGroupMeta mlgSession = PermissionMetaUtil.createModuleLocalGroup(config, "unisessionLocal", "Модуль «Сессия»");
        final ClassGroupMeta lcgBulletin = PermissionMetaUtil.createClassGroup(mlgSession, "bulletinDocumentLocalClass", "Объект «Ведомость»", SessionBulletinDocument.class.getName());
        final ClassGroupMeta lcgSheet = PermissionMetaUtil.createClassGroup(mlgSession, "sheetDocumentLocalClass", "Объект «Экзаменационный лист»", SessionSheetDocument.class.getName());
        final ClassGroupMeta lcgListDocument = PermissionMetaUtil.createClassGroup(mlgSession, "listDocumentLocalClass", "Объект «Экзаменационная карточка»", SessionListDocument.class.getName());
        final ClassGroupMeta lcgSessionObject = PermissionMetaUtil.createClassGroup(mlgSession, "sessionObjectLocalClass", "Объект «Сессия»", SessionObject.class.getName());
        final ClassGroupMeta lcgRetakeDoc = PermissionMetaUtil.createClassGroup(mlgSession, "retakeDocLocalClass", "Объект «Ведомость пересдач»", SessionRetakeDocument.class.getName());
        final ClassGroupMeta lcgTransferInsideDoc = PermissionMetaUtil.createClassGroup(mlgSession, "transferInsideDocLocalClass", "Объект «Внутреннее перезачтение»", SessionTransferInsideDocument.class.getName());
        final ClassGroupMeta lcgTransferOutsideDoc = PermissionMetaUtil.createClassGroup(mlgSession, "transferOutsideDocLocalClass", "Объект «Внешнее перезачтение»", SessionTransferOutsideDocument.class.getName());
        final ClassGroupMeta lcgAttestation = PermissionMetaUtil.createClassGroup(mlgSession, "sessionAttestationLocalClass", "Объект «Межсессионная аттестация»", SessionAttestation.class.getName());
        final ClassGroupMeta lcgAttBulletin = PermissionMetaUtil.createClassGroup(mlgSession, "sessionAttBulletinLocalClass", "Объект «Аттестационная ведомость»", SessionAttestationBulletin.class.getName());
        final ClassGroupMeta lcgProtocolDoc = PermissionMetaUtil.createClassGroup(mlgSession, "sessionTransferProtocolDocLocalClass", "Объект «Протокол перезачтения и переаттестации»", SessionTransferProtocolDocument.class.getName());
        final ClassGroupMeta lcgALRequest = PermissionMetaUtil.createClassGroup(mlgSession, "sessionALRequestLocalClass", "Объект «Заявление о переводе на ускоренное обучение»", SessionALRequest.class.getName());
        final ClassGroupMeta lcgPortfolioElement = PermissionMetaUtil.createClassGroup(mlgSession, "studentPortfolioLocalClass", "Объект «Элемент портфолио студента»", StudentPortfolioElement.class.getName());

        PermissionMetaUtil.createGroupRelation(config, "unisessionBulletinPG", lcgBulletin.getName());
        PermissionMetaUtil.createGroupRelation(config, "unisessionSheetPG", lcgSheet.getName());
        PermissionMetaUtil.createGroupRelation(config, "unisessionListDocumentPG", lcgListDocument.getName());
        PermissionMetaUtil.createGroupRelation(config, "unisessionSessionObjectPG", lcgSessionObject.getName());
        PermissionMetaUtil.createGroupRelation(config, "unisessionRetakeDocPG", lcgRetakeDoc.getName());
        PermissionMetaUtil.createGroupRelation(config, "unisessionTransferInsideDocumentPG", lcgTransferInsideDoc.getName());
        PermissionMetaUtil.createGroupRelation(config, "unisessionTransferOutsideDocumentPG", lcgTransferOutsideDoc.getName());
        PermissionMetaUtil.createGroupRelation(config, "unisessionAttestationPG", lcgAttestation.getName());
        PermissionMetaUtil.createGroupRelation(config, "unisessionAttBulletinPG", lcgAttBulletin.getName());
        PermissionMetaUtil.createGroupRelation(config, "unisessionTransferProtocolDocumentPG", lcgProtocolDoc.getName());
        PermissionMetaUtil.createGroupRelation(config, "studentSessionReexaminationTabPG", lcgALRequest.getName());
        PermissionMetaUtil.createGroupRelation(config, "studentPortfolioTabPG", lcgPortfolioElement.getName());


        for (final OrgUnitType description : DataAccessServices.dao().getList(OrgUnitType.class, OrgUnitType.P_TITLE))
        {
            final String code = description.getCode();
            final ClassGroupMeta gcg = PermissionMetaUtil.createClassGroup(mggOrgstruct, code + "PermissionClassGroup", "Объект «" + description.getTitle() + "»", OrgUnit.class.getName());
            final ClassGroupMeta lcg = PermissionMetaUtil.createClassGroup(mlgOrgstruct, code + "LocalClassGroup", "Объект «" + description.getTitle() + "»", OrgUnit.class.getName());

            final PermissionGroupMeta pgSessionTab = PermissionMetaUtil.createPermissionGroup(config, code + "SessionTabPermissionGroup", "Вкладка «Сессия»");
            PermissionMetaUtil.createGroupRelation(config, pgSessionTab.getName(), gcg.getName());
            PermissionMetaUtil.createGroupRelation(config, pgSessionTab.getName(), lcg.getName());
            PermissionMetaUtil.createPermission(pgSessionTab, "unisessionTabView_" + code, "Просмотр и печать");

            final PermissionGroupMeta pgSessionObjectTab = PermissionMetaUtil.createPermissionGroup(pgSessionTab, code + "SessionObjectTabPG", "Вкладка «Сессии»");
            PermissionMetaUtil.createPermission(pgSessionObjectTab, "sessionObjectTabView_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(pgSessionObjectTab, "addSessionObject_" + code, "Добавление сессии");
            PermissionMetaUtil.createPermission(pgSessionObjectTab, "addAttestation_" + code, "Добавление межсессионной аттестации");
            PermissionMetaUtil.createPermission(pgSessionObjectTab, "editSessionObject_" + code, "Редактирование сессии");
            PermissionMetaUtil.createPermission(pgSessionObjectTab, "deleteSessionObject_" + code, "Удаление сессии");

            final PermissionGroupMeta pgAttestationTab = PermissionMetaUtil.createPermissionGroup(pgSessionTab, code + "SessionAttestationTabPG", "Вкладка «Аттестации»");
            PermissionMetaUtil.createPermission(pgAttestationTab, "attestationTabView_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(pgAttestationTab, "autoCreateAttestationBulletins_" + code, "Формирование ведомостей");
			PermissionMetaUtil.createPermission(pgAttestationTab, "addAttestationBulletins_" + code, "Добавление ведомостей");
            PermissionMetaUtil.createPermission(pgAttestationTab, "markAttestationBulletin_" + code, "Выставление отметок об аттестации");
            PermissionMetaUtil.createPermission(pgAttestationTab, "printAttestationBulletin_" + code, "Печать ведомостей");
            PermissionMetaUtil.createPermission(pgAttestationTab, "deleteAttestationBulletin_" + code, "Удаление ведомостей");

            final PermissionGroupMeta pgDocumentTab = PermissionMetaUtil.createPermissionGroup(pgSessionTab, code + "SessionDocumentTabPG", "Вкладка «Документы»");
            PermissionMetaUtil.createPermission(pgDocumentTab, "sessionDocumentTabView_" + code, "Просмотр");

            final PermissionGroupMeta pgBulletinTab = PermissionMetaUtil.createPermissionGroup(pgSessionTab, code + "SessionBulletinTabPG", "Вкладка «Ведомости»");
            PermissionMetaUtil.createPermission(pgBulletinTab, "sessionBulletinTabView_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(pgBulletinTab, "autoCreateSessionBulletins_" + code, "Формирование ведомостей");
            PermissionMetaUtil.createPermission(pgBulletinTab, "editSessionBulletin_" + code, "Редактирование ведомости");
            PermissionMetaUtil.createPermission(pgBulletinTab, "deleteSessionBulletin_" + code, "Удаление ведомости");
            PermissionMetaUtil.createPermission(pgBulletinTab, "printSessionBulletin_" + code, "Печать ведомостей");
            PermissionMetaUtil.createPermission(pgBulletinTab, "markSessionBulletin_" + code, "Выставление оценок в ведомости");

            final PermissionGroupMeta pgRetakeDocTab = PermissionMetaUtil.createPermissionGroup(pgSessionTab, code + "SessionRetakeDocTabPG", "Вкладка «Ведомости пересдач»");
            PermissionMetaUtil.createPermission(pgRetakeDocTab, "sessionRetakeDocTabView_" + code, "Просмотр");
			PermissionMetaUtil.createPermission(pgRetakeDocTab, "addSessionRetakeDocs_" + code, "Добавление ведомостей");
            PermissionMetaUtil.createPermission(pgRetakeDocTab, "autoCreateSessionRetakeDocs_" + code, "Формирование ведомостей");
            PermissionMetaUtil.createPermission(pgRetakeDocTab, "editSessionRetakeDoc_" + code, "Редактирование ведомости");
            PermissionMetaUtil.createPermission(pgRetakeDocTab, "deleteSessionRetakeDoc_" + code, "Удаление ведомости");
            PermissionMetaUtil.createPermission(pgRetakeDocTab, "printSessionRetakeDoc_" + code, "Печать ведомостей");
            PermissionMetaUtil.createPermission(pgRetakeDocTab, "markSessionRetakeDoc_" + code, "Выставление оценок в ведомости");

            PermissionMetaUtil.createPermission(pgRetakeDocTab, "joinSessionRetakeDoc_" + code, "Объединение ведомостей");
            PermissionMetaUtil.createPermission(pgRetakeDocTab, "splitSessionRetakeDoc_" + code, "Разбиение ведомостей");

            final PermissionGroupMeta pgSimpleDocTab = PermissionMetaUtil.createPermissionGroup(pgSessionTab, code + "SessionSimpleDocTabPG", "Вкладка «Все документы»");
            PermissionMetaUtil.createPermission(pgSimpleDocTab, "sessionSimpleDocTabView_" + code, "Просмотр");

            final PermissionGroupMeta pgTransferDocTab = PermissionMetaUtil.createPermissionGroup(pgSessionTab, code + "SessionTransferDocTabPG", "Вкладка «Перезачтения»");
            PermissionMetaUtil.createPermission(pgTransferDocTab, "sessionTransferDocTabView_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(pgTransferDocTab, "editSessionTransferDoc_" + code, "Редактирование перезачтения");
            PermissionMetaUtil.createPermission(pgTransferDocTab, "deleteSessionTransferDoc_" + code, "Удаление перезачтения");
            PermissionMetaUtil.createPermission(pgTransferDocTab, "sessionInsideTransferAdd_" + code, "Добавление внутреннего перезачтения");
            PermissionMetaUtil.createPermission(pgTransferDocTab, "sessionOutsideTransferAdd_" + code, "Добавление внешнего перезачтения");

            final PermissionGroupMeta pgSheetTab = PermissionMetaUtil.createPermissionGroup(pgSessionTab, code + "SessionSheetTabPG", "Вкладка «Экзам. листы»");
            PermissionMetaUtil.createPermission(pgSheetTab, "sessionSheetTabView_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(pgSheetTab, "formingSheets_" + code, "Сформировать экз. листы");
            PermissionMetaUtil.createPermission(pgSheetTab, "addSessionSheet_" + code, "Добавление экз. листа");
            PermissionMetaUtil.createPermission(pgSheetTab, "editSessionSheet_" + code, "Редактирование экз. листа");
            PermissionMetaUtil.createPermission(pgSheetTab, "deleteSessionSheet_" + code, "Удаление экз. листа");
            //UniSystemUtils.createPermission(pgSheetTab, "markSessionSheet_" + code, "Выставление оценки в экз. лист");
            PermissionMetaUtil.createPermission(pgSheetTab, "printSessionSheet_" + code, "Печать экз. листов");
            PermissionMetaUtil.createPermission(pgSheetTab, "deleteSelectedSessionSheet_" + code, "Массовое удаление экз. листов");

            final PermissionGroupMeta pgMarkListTab = PermissionMetaUtil.createPermissionGroup(pgSessionTab, code + "SessionMarkListTabPG", "Вкладка «Оценки»");
            PermissionMetaUtil.createPermission(pgMarkListTab, "sessionMarkListTabView_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(pgMarkListTab, "sessionMarkLisSheetForming_" + code, "Формирование экз. листов");
            PermissionMetaUtil.createPermission(pgMarkListTab, "sessionMarkLisBulletinsForming_" + code, "Формирование ведомостей пересдач");
            PermissionMetaUtil.createPermission(pgMarkListTab, "sessionMarkListPrintRetakeCount_" + code, "Просмотр истории сдачи");
            PermissionMetaUtil.createPermission(pgMarkListTab, "sessionMarkListPrintSheetDocument_" + code, "Печать экз. листов");
            PermissionMetaUtil.createPermission(pgMarkListTab, "sessionMarkListPrintRetakeDocument_" + code, "Печать ведомостей пересдач");
            PermissionMetaUtil.createPermission(pgMarkListTab, "sessionMarkListCheckedGradeBook_" + code, "Сверка оценок");

            final PermissionGroupMeta pgJournalTab = PermissionMetaUtil.createPermissionGroup(pgSessionTab, code + "SessionJournalTabPG", "Вкладка «Семестровый журнал»");
            PermissionMetaUtil.createPermission(pgJournalTab, "sessionJournalTabView_" + code, "Просмотр");

            final PermissionGroupMeta pgAllowanceTab = PermissionMetaUtil.createPermissionGroup(pgSessionTab, code + "SessionAllowanceTabPG", "Вкладка «Допуск»");
            PermissionMetaUtil.createPermission(pgAllowanceTab, "sessionAllowanceTabView_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(pgAllowanceTab, "addSessionNotAllowedStudent_" + code, "Добавление недопуска");
            PermissionMetaUtil.createPermission(pgAllowanceTab, "unlockSessionNotAllowedStudent_" + code, "Аннулирование недопуска");
            PermissionMetaUtil.createPermission(pgAllowanceTab, "deleteSessionNotAllowedStudent_" + code, "Удаление информации о недопуске");

            final PermissionGroupMeta pgListDocumentTab = PermissionMetaUtil.createPermissionGroup(pgSessionTab, code + "SessionListDocumentTabPG", "Вкладка «Экзаменационные карточки»");
            PermissionMetaUtil.createPermission(pgListDocumentTab, "sessionListDocumentTabView_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(pgListDocumentTab, "printSessionListDocument_" + code, "Печатать экзам. карточки");
            PermissionMetaUtil.createPermission(pgListDocumentTab, "addSessionListDocument_" + code, "Добавление экзам. карточки");
            PermissionMetaUtil.createPermission(pgListDocumentTab, "editSessionListDocument_" + code, "Редактирование экзам. карточки");
            PermissionMetaUtil.createPermission(pgListDocumentTab, "deleteSessionListDocument_" + code, "Удаление экзам. карточки");




            final PermissionGroupMeta permissionGroupReportsAll = PermissionMetaUtil.createPermissionGroup(config, code + "ReportsTabPermissionGroup", "Вкладка «Отчеты»");
            final PermissionGroupMeta pgReports = PermissionMetaUtil.createPermissionGroup(permissionGroupReportsAll, code + "SessionReportPG", "Отчеты модуля «Сессия»");
            PermissionMetaUtil.createPermission(pgReports, "orgUnit_viewSessionReportSummaryBulletinList_" + code, "Просмотр и печать отчета «Сводная ведомость (на академ. группу)»");
            PermissionMetaUtil.createPermission(pgReports, "orgUnit_addSessionReportSummaryBulletinList_" + code, "Добавление отчета «Сводная ведомость (на академ. группу)»");
            PermissionMetaUtil.createPermission(pgReports, "orgUnit_deleteSessionReportSummaryBulletinList_" + code, "Удаление отчета «Сводная ведомость (на академ. группу)»");
            PermissionMetaUtil.createPermission(pgReports, "orgUnit_viewSessionReportDebtorsList_" + code, "Просмотр и печать отчета «Должники»");
            PermissionMetaUtil.createPermission(pgReports, "orgUnit_addSessionReportDebtorsList_" + code, "Добавление отчета «Должники»");
            PermissionMetaUtil.createPermission(pgReports, "orgUnit_deleteSessionReportDebtorsList_" + code, "Удаление отчета «Должники»");
            PermissionMetaUtil.createPermission(pgReports, "orgUnit_viewSessionReportResultsList_" + code, "Просмотр и печать отчета «Сведения об успеваемости студентов (по курсам, по группам, по направлениям)»");
            PermissionMetaUtil.createPermission(pgReports, "orgUnit_addSessionReportResultsList_" + code, "Добавление отчета «Сведения об успеваемости студентов (по курсам, по группам, по направлениям)»");
            PermissionMetaUtil.createPermission(pgReports, "orgUnit_deleteSessionReportResultsList_" + code, "Удаление отчета «Сведения об успеваемости студентов (по курсам, по группам, по направлениям)»");
            PermissionMetaUtil.createPermission(pgReports, "orgUnit_viewSessionReportResultsByDiscList_" + code, "Просмотр и печать отчета «Сведения об успеваемости студентов (по дисциплинам)»");
            PermissionMetaUtil.createPermission(pgReports, "orgUnit_addSessionReportResultsByDiscList_" + code, "Добавление отчета «Сведения об успеваемости студентов (по дисциплинам)»");
            PermissionMetaUtil.createPermission(pgReports, "orgUnit_deleteSessionReportResultsByDiscList_" + code, "Удаление отчета «Сведения об успеваемости студентов (по дисциплинам)»");
            PermissionMetaUtil.createPermission(pgReports, "orgUnit_viewSessionReportGroupMarksList_" + code, "Просмотр и печать отчета «Сводная ведомость группы (по всем полученным студентами оценкам)»");
            PermissionMetaUtil.createPermission(pgReports, "orgUnit_addSessionReportGroupMarksList_" + code, "Добавление отчета «Сводная ведомость группы (по всем полученным студентами оценкам)»");
            PermissionMetaUtil.createPermission(pgReports, "orgUnit_deleteSessionReportGroupMarksList_" + code, "Удаление отчета «Сводная ведомость группы (по всем полученным студентами оценкам)»");
            PermissionMetaUtil.createPermission(pgReports, "orgUnit_viewSessionReportGroupBulletinListList_" + code, "Просмотр и печать отчета «Сводные данные по ведомостям (на академ. группу)»");
            PermissionMetaUtil.createPermission(pgReports, "orgUnit_addSessionReportGroupBulletinListList_" + code, "Добавление отчета «Сводные данные по ведомостям (на академ. группу)»");
            PermissionMetaUtil.createPermission(pgReports, "orgUnit_deleteSessionReportGroupBulletinListList_" + code, "Удаление отчета «Сводные данные по ведомостям (на академ. группу)»");
            PermissionMetaUtil.createPermission(pgReports, "orgUnit_viewAttestationReportBulletinList_" + code, "Просмотр и печать отчета «Аттестационная ведомость по дисциплине (на академ. группу)»");
            PermissionMetaUtil.createPermission(pgReports, "orgUnit_addAttestationReportBulletinList_" + code, "Добавление отчета «Аттестационная ведомость по дисциплине (на академ. группу)»");
            PermissionMetaUtil.createPermission(pgReports, "orgUnit_deleteAttestationReportBulletinList_" + code, "Удаление отчета «Аттестационная ведомость по дисциплине (на академ. группу)»");
            PermissionMetaUtil.createPermission(pgReports, "orgUnit_viewAttestationReportResultList_" + code, "Просмотр и печать отчета «Сводная ведомость по межсессионной аттестации (на академ. группу)»");
            PermissionMetaUtil.createPermission(pgReports, "orgUnit_addAttestationReportResultList_" + code, "Добавление отчета «Сводная ведомость по межсессионной аттестации (на академ. группу)»");
            PermissionMetaUtil.createPermission(pgReports, "orgUnit_deleteAttestationReportResultList_" + code, "Удаление отчета «Сводная ведомость по межсессионной аттестации (на академ. группу)»");
            PermissionMetaUtil.createPermission(pgReports, "orgUnit_viewAttestationReportTotalResultList_" + code, "Просмотр и печать отчета «Итоги межсессионной аттестации студентов»");
            PermissionMetaUtil.createPermission(pgReports, "orgUnit_addAttestationReportTotalResultList_" + code, "Добавление отчета «Итоги межсессионной аттестации студентов»");
            PermissionMetaUtil.createPermission(pgReports, "orgUnit_deleteAttestationReportTotalResultList_" + code, "Удаление отчета «Итоги межсессионной аттестации студентов»");

            PermissionGroupMeta pgStudentReports = PermissionMetaUtil.createPermissionGroup(permissionGroupReportsAll, code + "StudentReportPermissionGroup", "Отчеты модуля «Студенты»");
            PermissionMetaUtil.createPermission(pgStudentReports, "orgUnit_viewStateFinalExamResultList_" + code, "Просмотр и печать отчета «Результаты государственного экзамена»");
            PermissionMetaUtil.createPermission(pgStudentReports, "orgUnit_addStateFinalExamResult_" + code, "Добавление отчета «Результаты государственного экзамена»");
            PermissionMetaUtil.createPermission(pgStudentReports, "orgUnit_deleteStateFinalExamResult_" + code, "Удаление отчета «Результаты государственного экзамена»");
            PermissionMetaUtil.createPermission(pgStudentReports, "orgUnit_viewFinalQualWorkResultList_" + code, "Просмотр и печать отчета «Результаты защиты выпускной квалификационной работы»");
            PermissionMetaUtil.createPermission(pgStudentReports, "orgUnit_addFinalQualWorkResult_" + code, "Добавление отчета «Результаты защиты выпускной квалификационной работы»");
            PermissionMetaUtil.createPermission(pgStudentReports, "orgUnit_deleteFinalQualWorkResult_" + code, "Удаление отчета «Результаты защиты выпускной квалификационной работы»");

            PermissionMetaUtil.createPermission(pgStudentReports, "orgUnit_viewStateFinalExamSummaryResultList_" + code, "Просмотр и печать отчета «Сводный отчет по результатам государственного экзамена»");
            PermissionMetaUtil.createPermission(pgStudentReports, "orgUnit_addStateFinalExamSummaryResult_" + code, "Добавление отчета «Сводный отчет по результатам государственного экзамена»");
            PermissionMetaUtil.createPermission(pgStudentReports, "orgUnit_deleteStateFinalExamSummaryResult_" + code, "Удаление отчета «Сводный отчет по результатам государственного экзамена»");
            PermissionMetaUtil.createPermission(pgStudentReports, "orgUnit_viewFinalQualWorkSummaryResultList_" + code, "Просмотр и печать отчета «Сводный отчет по результатам защит ВКР»");
            PermissionMetaUtil.createPermission(pgStudentReports, "orgUnit_addFinalQualWorkSummaryResult_" + code, "Добавление отчета «Сводный отчет по результатам защит ВКР»");
            PermissionMetaUtil.createPermission(pgStudentReports, "orgUnit_deleteFinalQualWorkSummaryResult_" + code, "Удаление отчета «Сводный отчет по результатам защит ВКР»");
        }

        securityConfigMetaMap.put(config.getName(), config);
    }
}
