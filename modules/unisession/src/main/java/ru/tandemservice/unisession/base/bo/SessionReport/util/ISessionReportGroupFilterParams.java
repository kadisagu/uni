/* $Id:$ */
package ru.tandemservice.unisession.base.bo.SessionReport.util;

import java.util.Collection;

/**
 * @author oleyba
 * @since 2/24/12
 */
public interface ISessionReportGroupFilterParams
{
    Collection<String> getGroups();
    boolean isIncludeStudentsWithoutGroups();
}
