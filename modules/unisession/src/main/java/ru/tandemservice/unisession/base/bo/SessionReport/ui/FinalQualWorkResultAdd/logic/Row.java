/* $Id: $ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.FinalQualWorkResultAdd.logic;

import jxl.write.WritableCellFormat;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unisession.base.bo.SessionReport.logic.SfaRaw;
import ru.tandemservice.unisession.entity.catalog.SessionRecommendation4FQW;
import ru.tandemservice.unisession.entity.catalog.SessionSource4FQWTheme;
import ru.tandemservice.unisession.entity.stateFinalExam.SessionFQWProtocol;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Andrey Andreev
 * @since 16.12.2016
 */
public class Row extends ru.tandemservice.unisession.base.bo.SessionReport.ui.StateFinalAttestationResultAdd.logic.Row
{

    protected int _honors = 0;                                                   // Количество дипломов с отличием
    protected Double _honorsProgress = 0d;                                       // Качественный показатель (дипл. с отл.)
    protected Map<SessionSource4FQWTheme, int[]> _themesNumbers;                 // Количество ВКР выполненных
    protected Map<SessionRecommendation4FQW, int[]> _recommendationsNumbers;     // Количество ВКР рекомендованных

    public Row(String title, WritableCellFormat titleStyle, WritableCellFormat intStyle, WritableCellFormat redIntStyle, WritableCellFormat doubleStyle)
    {
        super(title, titleStyle, intStyle, redIntStyle, doubleStyle);
    }

    public Row initProtocolsMap(ReportInfo reportInfo)
    {
        _themesNumbers = reportInfo.getSource4FQWThemeList().stream().collect(Collectors.toMap(theme -> theme, themesNumbers -> new int[]{0}));
        _recommendationsNumbers = reportInfo.getRecommendation4FQWList().stream().collect(Collectors.toMap(theme -> theme, themesNumbers -> new int[]{0}));

        return this;
    }

    @Override
    public void calcProgress()
    {
        _progress = _total > 0 ? 100 * _passed / (double) _total : null;
        _goodProgress = _total > 0 ? 100 * (_mark4 + _mark5) / (double) _total : null;
        _honorsProgress = _passed > 0 ? 100 * _honors / (double) _passed : null;
    }

    @Override
    protected void addFromRaw(Student student, List<? extends SfaRaw> raws)
    {
        super.addFromRaw(student, raws);
        if (raws != null)
        {
            raws.forEach(raw ->
                         {

                             SessionFQWProtocol protocol = ((Raw) raw).getProtocol();
                             if (protocol != null)
                             {
                                 if (protocol.isWithHonors())
                                     _honors++;

                                 SessionSource4FQWTheme theme = protocol.getSource4Theme();
                                 if (theme != null) _themesNumbers.get(theme)[0]++;

                                 SessionRecommendation4FQW recommendation = protocol.getRecommendation();
                                 if (recommendation != null) _recommendationsNumbers.get(recommendation)[0]++;
                             }
                         });
        }
    }



    public int getHonors()
    {
        return _honors;
    }

    public void setHonors(int honors)
    {
        this._honors = honors;
    }

    public Double getHonorsProgress()
    {
        return _honorsProgress;
    }

    public void setHonorsProgress(Double honorsProgress)
    {
        this._honorsProgress = honorsProgress;
    }

    public Map<SessionSource4FQWTheme, int[]> getThemesNumbers()
    {
        return _themesNumbers;
    }

    public Map<SessionRecommendation4FQW, int[]> getRecommendationsNumbers()
    {
        return _recommendationsNumbers;
    }
}
