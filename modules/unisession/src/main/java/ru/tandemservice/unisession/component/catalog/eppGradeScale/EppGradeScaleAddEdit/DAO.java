/* $Id: daoAddEdit.vm 6177 2009-01-13 14:09:27Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisession.component.catalog.eppGradeScale.EppGradeScaleAddEdit;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale;

/**
 * @author AutoGenerator
 * Created on 15.02.2011
 */
public class DAO extends DefaultCatalogAddEditDAO<EppGradeScale, Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);
    }

    @Override
    public void update(final Model model)
    {
        super.update(model);
    }

    @Override public Long save(final IEntity entity)
    {
        if (entity instanceof EppGradeScale)
        {
            final Number n = new DQLSelectBuilder()
            .fromEntity(EppGradeScale.class, "s")
            .column(DQLFunctions.max(DQLExpressions.property(EppGradeScale.priority().fromAlias("s"))))
            .createStatement(this.getSession()).uniqueResult();
            ((EppGradeScale)entity).setPriority(1+(null == n ? 0 : n.intValue()));
        }
        return super.save(entity);
    }

}
