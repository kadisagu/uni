package ru.tandemservice.unisession.entity.comission.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unisession.entity.comission.SessionComission;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Преподаватель в комиссии
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionComissionPpsGen extends EntityBase
 implements INaturalIdentifiable<SessionComissionPpsGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.comission.SessionComissionPps";
    public static final String ENTITY_NAME = "sessionComissionPps";
    public static final int VERSION_HASH = -822656137;
    private static IEntityMeta ENTITY_META;

    public static final String L_COMMISSION = "commission";
    public static final String L_PPS = "pps";

    private SessionComission _commission;     // Комиссия
    private PpsEntry _pps;     // Преподаватель

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Комиссия. Свойство не может быть null.
     */
    @NotNull
    public SessionComission getCommission()
    {
        return _commission;
    }

    /**
     * @param commission Комиссия. Свойство не может быть null.
     */
    public void setCommission(SessionComission commission)
    {
        dirty(_commission, commission);
        _commission = commission;
    }

    /**
     * @return Преподаватель. Свойство не может быть null.
     */
    @NotNull
    public PpsEntry getPps()
    {
        return _pps;
    }

    /**
     * @param pps Преподаватель. Свойство не может быть null.
     */
    public void setPps(PpsEntry pps)
    {
        dirty(_pps, pps);
        _pps = pps;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SessionComissionPpsGen)
        {
            if (withNaturalIdProperties)
            {
                setCommission(((SessionComissionPps)another).getCommission());
                setPps(((SessionComissionPps)another).getPps());
            }
        }
    }

    public INaturalId<SessionComissionPpsGen> getNaturalId()
    {
        return new NaturalId(getCommission(), getPps());
    }

    public static class NaturalId extends NaturalIdBase<SessionComissionPpsGen>
    {
        private static final String PROXY_NAME = "SessionComissionPpsNaturalProxy";

        private Long _commission;
        private Long _pps;

        public NaturalId()
        {}

        public NaturalId(SessionComission commission, PpsEntry pps)
        {
            _commission = ((IEntity) commission).getId();
            _pps = ((IEntity) pps).getId();
        }

        public Long getCommission()
        {
            return _commission;
        }

        public void setCommission(Long commission)
        {
            _commission = commission;
        }

        public Long getPps()
        {
            return _pps;
        }

        public void setPps(Long pps)
        {
            _pps = pps;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof SessionComissionPpsGen.NaturalId) ) return false;

            SessionComissionPpsGen.NaturalId that = (NaturalId) o;

            if( !equals(getCommission(), that.getCommission()) ) return false;
            if( !equals(getPps(), that.getPps()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCommission());
            result = hashCode(result, getPps());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCommission());
            sb.append("/");
            sb.append(getPps());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionComissionPpsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionComissionPps.class;
        }

        public T newInstance()
        {
            return (T) new SessionComissionPps();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "commission":
                    return obj.getCommission();
                case "pps":
                    return obj.getPps();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "commission":
                    obj.setCommission((SessionComission) value);
                    return;
                case "pps":
                    obj.setPps((PpsEntry) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "commission":
                        return true;
                case "pps":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "commission":
                    return true;
                case "pps":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "commission":
                    return SessionComission.class;
                case "pps":
                    return PpsEntry.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionComissionPps> _dslPath = new Path<SessionComissionPps>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionComissionPps");
    }
            

    /**
     * @return Комиссия. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.comission.SessionComissionPps#getCommission()
     */
    public static SessionComission.Path<SessionComission> commission()
    {
        return _dslPath.commission();
    }

    /**
     * @return Преподаватель. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.comission.SessionComissionPps#getPps()
     */
    public static PpsEntry.Path<PpsEntry> pps()
    {
        return _dslPath.pps();
    }

    public static class Path<E extends SessionComissionPps> extends EntityPath<E>
    {
        private SessionComission.Path<SessionComission> _commission;
        private PpsEntry.Path<PpsEntry> _pps;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Комиссия. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.comission.SessionComissionPps#getCommission()
     */
        public SessionComission.Path<SessionComission> commission()
        {
            if(_commission == null )
                _commission = new SessionComission.Path<SessionComission>(L_COMMISSION, this);
            return _commission;
        }

    /**
     * @return Преподаватель. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.comission.SessionComissionPps#getPps()
     */
        public PpsEntry.Path<PpsEntry> pps()
        {
            if(_pps == null )
                _pps = new PpsEntry.Path<PpsEntry>(L_PPS, this);
            return _pps;
        }

        public Class getEntityClass()
        {
            return SessionComissionPps.class;
        }

        public String getEntityName()
        {
            return "sessionComissionPps";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
