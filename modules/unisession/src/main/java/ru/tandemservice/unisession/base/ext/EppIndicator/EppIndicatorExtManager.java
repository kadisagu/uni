/* $Id:$ */
package ru.tandemservice.unisession.base.ext.EppIndicator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.SystemActionManager;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.util.SystemActionDefinition;
import ru.tandemservice.uniepp.base.bo.EppIndicator.EppIndicatorManager;
import ru.tandemservice.uniepp.base.bo.EppIndicator.logic.EppIndicator;
import ru.tandemservice.unisession.base.ext.SystemAction.ui.Pub.SystemActionPubExt;

/**
 * @author oleyba
 * @since 9/15/14
 */
@Configuration
public class EppIndicatorExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private EppIndicatorManager _eppIndicatorManager;

    @Bean
    public ItemListExtension<EppIndicator> indicatorListExtPoint()
    {
        return itemListExtension(_eppIndicatorManager.indicatorListExtPoint())
            .add("session-student-without-session-slots", new EppIndicator("session-student-without-session-slots", "SessionIndicatorsStudentsWithoutSessionSlots", "session-session"))
            .add("session-unactual-marks", new EppIndicator("session-unactual-marks", "SessionMarkUnactualList", "session-session"))
            .add("session-student-without-session-bulletin-slots", new EppIndicator("session-student-without-session-bulletin-slots", "SessionIndicatorsStudentWithoutSessBullSlots", "session-session"))
            .create();
    }
}