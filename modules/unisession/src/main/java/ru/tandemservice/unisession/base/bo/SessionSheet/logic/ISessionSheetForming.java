/**
 *$Id$
 */
package ru.tandemservice.unisession.base.bo.SessionSheet.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unisession.entity.catalog.SessionsSimpleDocumentReason;

import java.util.Collection;
import java.util.Date;

/**
 * @author Alexander Shaburov
 * @since 18.02.13
 */
public interface ISessionSheetForming extends INeedPersistenceSupport
{
    void doSaveSessionSheetDocumentList(Collection<Long> sessionMarkIdList, OrgUnit orgUnit, Date issueDate, Date validityDate, SessionsSimpleDocumentReason reason, boolean inSession, boolean printSheetDocuments);
}
