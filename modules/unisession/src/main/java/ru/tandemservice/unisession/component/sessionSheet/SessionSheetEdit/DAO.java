// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisession.component.sessionSheet.SessionSheetEdit;

import com.google.common.collect.ImmutableList;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Session;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uni.base.bo.PpsEntry.util.PpsEntrySelectBlockData;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.base.bo.SessionMark.SessionMarkManager;
import ru.tandemservice.unisession.base.bo.SessionMark.logic.ISessionMarkDAO;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.dao.comission.ISessionCommissionDAO;
import ru.tandemservice.unisession.dao.document.ISessionDocumentBaseDAO;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionsSimpleDocumentReason;
import ru.tandemservice.unisession.entity.comission.SessionComission;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionProjectTheme;
import ru.tandemservice.unisession.entity.document.SessionSheetDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.util.selectModel.SessionTermModel;

import java.util.*;

/**
 * @author oleyba
 * @since 2/17/11
 */
public class DAO<M extends Model> extends UniBaseDao implements IDAO<M>
{
    @Override
    public void prepare(final M model)
    {
        model.setSheet(this.getNotNull(SessionSheetDocument.class, model.getSheet().getId()));
        model.setSlot(this.get(SessionDocumentSlot.class, SessionDocumentSlot.document().s(), model.getSheet()));
        model.setTerm(new SessionTermModel.TermWrapper(model.getSlot()));

        List<PpsEntry> ppsList = CommonBaseUtil.getPropertiesList(this.getList(SessionComissionPps.class, SessionComissionPps.commission().s(), model.getSlot().getCommission()), SessionComissionPps.pps().s());
        model.setPpsData(new PpsEntrySelectBlockData(model.getSlot().getStudentWpeCAction().getTutorOu().getId()).setMultiSelect(true).setInitiallySelectedPps(ppsList));

        if (!model.isInitialized())
        {
            final SessionMark previous = this.getByNaturalId(new SessionMark.NaturalId(model.getSlot()));
            if (null != previous)
            {
                model.setMark(previous.getValueItem());
                model.setPerformDate(previous.getPerformDate());
                initAdditionalMarkData(model, previous);
            }
            model.setInSession(model.getSlot().isInSession());
            model.setInitialized(true);
        }

        model.setStudentModel(new LazySimpleSelectModel<>(ImmutableList.of(model.getSlot().getActualStudent())));
        model.setTermModel(new SessionTermModel()
        {
            @Override
            public Map<EducationYear, Set<YearDistributionPart>> getValueMap()
            {
                return Collections.singletonMap(model.getTerm().getYear(), Collections.singleton(model.getTerm().getPart()));
            }
        });
        model.setControlActionModel(new LazySimpleSelectModel<>(ImmutableList.of(model.getSlot().getStudentWpeCAction()), EppStudentWpeCAction.P_REGISTRY_ELEMENT_TITLE));
        model.setReasonModel(new LazySimpleSelectModel<>(SessionsSimpleDocumentReason.class));

        // настройки БРС - не даем ставить оценку, если используются баллы или тек. рейтинг
        // todo исправить на поддержку выставления в баллах, текущий рейтинг не поддерживать
        ISessionBrsDao.ISessionRatingSettingsKey ratingSettingsKey = ISessionBrsDao.instance.get().key(model.getSlot());
        ISessionBrsDao.ISessionRatingSettings ratingSettings = ISessionBrsDao.instance.get().getRatingSettings(ratingSettingsKey);
        model.setCanMark(ratingSettings == null || (!ratingSettings.useCurrentRating() && !ratingSettings.usePoints()));

        // обновляем список оценок
        if (model.isCanMark())
            model.setMarkModel(SessionMarkManager.instance().regularMarkDao().prepareMarkModel(model.getSheet().getGroupOu(), model.getSlot().getStudentWpeCAction(), model.getMark(), true));

		model.setThemeRequired(model.getSlot().getStudentWpeCAction().getActionType().isThemeRequired());
		if (model.isThemeRequired())
		{
			SessionProjectTheme theme = getByNaturalId(new SessionProjectTheme.NaturalId(model.getSlot()));
			if (theme != null)
				model.setTheme(theme.getTheme());
		}
    }

    @Override
    public void validate(final M model, final ErrorCollector errors)
    {
        if (model.getSheet().getIssueDate() != null && model.getSheet().getDeadlineDate() != null && model.getSheet().getIssueDate().after(model.getSheet().getDeadlineDate())) {
            errors.add("Дата выдачи не может быть позже срока действия.", "issueDate", "deadlineDate");
        }

        if (!model.isCanMark())
            return;

        if (model.getPerformDate() != null && model.getSheet().getDeadlineDate() != null && model.getPerformDate().after(model.getSheet().getDeadlineDate())) {
            errors.add("Дата сдачи не может быть позже срока действия.", "performDate", "deadlineDate");
        }

        if (model.getMark() != null && model.getPerformDate() == null) {
            errors.add("Чтобы выставить оценку, заполните дату сдачи.", "performDate");
        }

        if (model.getMark() != null && CollectionUtils.isEmpty(model.getPpsList())) {
            errors.add("Чтобы выставить оценку, укажите преподавателя.", "pps");
        }
    }

    @Override
    public void update(final M model)
    {
        final Session session = this.getSession();

        final SessionDocumentSlot slot = model.getSlot();

        final boolean slotChanged = slot.isInSession() != model.isInSession();
        final SessionMark oldMark = this.getByNaturalId(new SessionMark.NaturalId(slot));

        final SessionComission commission = ISessionCommissionDAO.instance.get().saveOrUpdateCommission(slot.getCommission(), model.getPpsList());

        final ISessionMarkDAO.MarkData markData = prepareMarkDataForUpdate(model);

        if (slotChanged)
        {
            if (null != oldMark) {
                this.delete(oldMark);
            }
            this.delete(slot);

            session.flush();

            final SessionDocumentSlot newSlot = new SessionDocumentSlot();
            newSlot.update(slot);
            newSlot.setInSession(model.isInSession());
            newSlot.setCommission(commission);
            this.save(newSlot);

            if (model.isCanMark() && null != model.getMark())
			{
                SessionMarkManager.instance().regularMarkDao().saveOrUpdateMark(newSlot, markData);
                saveAdditionalMarkData(model);
            }

			ISessionDocumentBaseDAO.instance.get().updateProjectTheme(slot, model.getTheme(), null);

            return;
        }

        if (model.isCanMark()) {
            if (null != model.getMark()) {
                SessionMarkManager.instance().regularMarkDao().saveOrUpdateMark(slot, markData);
            } else if (null != oldMark) {
                this.delete(oldMark);
            }
        }

        this.update(slot);

        if (model.isCanMark())
            saveAdditionalMarkData(model);

		ISessionDocumentBaseDAO.instance.get().updateProjectTheme(slot, model.getTheme(), null);
    }

    protected void initAdditionalMarkData(M model, SessionMark previous)
    {
        // для переопределения в проектах
    }

    protected ISessionMarkDAO.MarkData prepareMarkDataForUpdate(final M model)
    {
        return new ISessionMarkDAO.MarkData()
        {
            @Override public Date getPerformDate() { return model.getPerformDate(); }
            @Override public Double getPoints() { return null; }
            @Override public SessionMarkCatalogItem getMarkValue() { return model.getMark(); }
            @Override public String getComment() { return null; }
        };
    }

    protected void saveAdditionalMarkData(M model)
    {
        // для переопределения в проектах
    }

    @Override
    public void prepareListDataSource(M m)
    {
        //
    }
}
