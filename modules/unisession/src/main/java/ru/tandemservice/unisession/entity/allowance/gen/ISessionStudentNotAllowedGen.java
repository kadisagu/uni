package ru.tandemservice.unisession.entity.allowance.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unisession.entity.allowance.ISessionStudentNotAllowed;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import ru.tandemservice.unisession.entity.allowance.ISessionStudentNotAllowed;

/**
 * Недопуск студента к сдаче
 *
 * Интерфейс для объектов, которые фиксируют факт недопуска студента до сдачи мероприятий в сессии.
 * Если студент впоследствии был допущен, факт недопуска может быть сохранен с установленной датой снятия недопуска.
 */
@SuppressWarnings({"all", "unchecked", "unused"})
public abstract class ISessionStudentNotAllowedGen extends InterfaceStubBase
 implements ISessionStudentNotAllowed{
    public static final int VERSION_HASH = -478308400;

    public static final String L_STUDENT = "student";
    public static final String P_CREATE_DATE = "createDate";
    public static final String P_REMOVAL_DATE = "removalDate";

    private Student _student;
    private Date _createDate;
    private Date _removalDate;

    @NotNull

    public Student getStudent()
    {
        return _student;
    }

    public void setStudent(Student student)
    {
        _student = student;
    }


    public Date getCreateDate()
    {
        return _createDate;
    }

    public void setCreateDate(Date createDate)
    {
        _createDate = createDate;
    }


    public Date getRemovalDate()
    {
        return _removalDate;
    }

    public void setRemovalDate(Date removalDate)
    {
        _removalDate = removalDate;
    }

    private static final Path<ISessionStudentNotAllowed> _dslPath = new Path<ISessionStudentNotAllowed>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ru.tandemservice.unisession.entity.allowance.ISessionStudentNotAllowed");
    }
            

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.allowance.ISessionStudentNotAllowed#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Дата создания.
     * @see ru.tandemservice.unisession.entity.allowance.ISessionStudentNotAllowed#getCreateDate()
     */
    public static PropertyPath<Date> createDate()
    {
        return _dslPath.createDate();
    }

    /**
     * @return Дата снятия.
     * @see ru.tandemservice.unisession.entity.allowance.ISessionStudentNotAllowed#getRemovalDate()
     */
    public static PropertyPath<Date> removalDate()
    {
        return _dslPath.removalDate();
    }

    public static class Path<E extends ISessionStudentNotAllowed> extends EntityPath<E>
    {
        private Student.Path<Student> _student;
        private PropertyPath<Date> _createDate;
        private PropertyPath<Date> _removalDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.allowance.ISessionStudentNotAllowed#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Дата создания.
     * @see ru.tandemservice.unisession.entity.allowance.ISessionStudentNotAllowed#getCreateDate()
     */
        public PropertyPath<Date> createDate()
        {
            if(_createDate == null )
                _createDate = new PropertyPath<Date>(ISessionStudentNotAllowedGen.P_CREATE_DATE, this);
            return _createDate;
        }

    /**
     * @return Дата снятия.
     * @see ru.tandemservice.unisession.entity.allowance.ISessionStudentNotAllowed#getRemovalDate()
     */
        public PropertyPath<Date> removalDate()
        {
            if(_removalDate == null )
                _removalDate = new PropertyPath<Date>(ISessionStudentNotAllowedGen.P_REMOVAL_DATE, this);
            return _removalDate;
        }

        public Class getEntityClass()
        {
            return ISessionStudentNotAllowed.class;
        }

        public String getEntityName()
        {
            return "ru.tandemservice.unisession.entity.allowance.ISessionStudentNotAllowed";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
