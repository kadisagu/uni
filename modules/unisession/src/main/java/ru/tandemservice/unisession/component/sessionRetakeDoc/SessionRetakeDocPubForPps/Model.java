/* $Id$ */
package ru.tandemservice.unisession.component.sessionRetakeDoc.SessionRetakeDocPubForPps;

/**
 * @author oleyba
 * @since 6/15/11
 */

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.sec.IPrincipalContext;
import ru.tandemservice.unisession.entity.document.SessionRetakeDocument;

@State({
    @Bind(key= PublisherActivator.PUBLISHER_ID_KEY, binding="retakeDoc.id")
})
public class Model
{
    public static final String COMPONENT_NAME = Model.class.getPackage().getName();

    private SessionRetakeDocument retakeDoc = new SessionRetakeDocument();
    private IPrincipalContext _principalContext;

    public SessionRetakeDocument getRetakeDoc()
    {
        return this.retakeDoc;
    }

    public void setRetakeDoc(final SessionRetakeDocument retakeDoc)
    {
        this.retakeDoc = retakeDoc;
    }

    public IPrincipalContext getPrincipalContext()
    {
        return _principalContext;
    }

    public void setPrincipalContext(IPrincipalContext principalContext)
    {
        _principalContext = principalContext;
    }
}
