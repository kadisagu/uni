/* $Id: $ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.FinalQualWorkResultAdd.logic;

import ru.tandemservice.unisession.base.bo.SessionReport.logic.SfaRaw;
import ru.tandemservice.unisession.entity.stateFinalExam.SessionFQWProtocol;


/**
 * @author Andrey Andreev
 * @since 16.12.2016
 */
public class Raw extends SfaRaw
{
    protected SessionFQWProtocol _protocol;

    public Raw(Object[] raw)
    {
        super(raw);
        _protocol = (SessionFQWProtocol) raw[3];
    }

    public SessionFQWProtocol getProtocol()
    {
        return _protocol;
    }

    public void setProtocol(SessionFQWProtocol protocol)
    {
        _protocol = protocol;
    }
}
