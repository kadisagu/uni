/* $Id$ */
package ru.tandemservice.unisession.print;

import java.util.Collection;

import org.tandemframework.rtf.document.RtfDocument;

import org.tandemframework.core.util.cache.SpringBeanCache;

/**
 * @author oleyba
 * @since 6/16/11
 */
public interface ISessionRetakeDocPrintDAO
{
    SpringBeanCache<ISessionRetakeDocPrintDAO> instance = new SpringBeanCache<ISessionRetakeDocPrintDAO>(ISessionRetakeDocPrintDAO.class.getName());

    /**
     * Массовая печать ведомостей в архив, по файлу на каждую
     * @param ids id ведомостей
     * @return контент файла
     */
    byte[] printDocListToZipArchive(Collection<Long> ids);

    /**
     * Массовая печать ведомостей в один rtf-документ
     * @param ids id ведомостей
     * @return rtf
     */
    RtfDocument printDocList(Collection<Long> ids);

    /**
     * Печать ведомости
     * @param id id ведомости
     * @return rtf
     */
    RtfDocument printDoc(Long id);
}
