/**
 *$Id$
 */
package ru.tandemservice.unisession.base.bo.SessionSheet.logic;

import com.google.common.collect.Lists;
import org.hibernate.Session;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.INumberQueueDAO;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.dao.document.ISessionDocumentBaseDAO;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionsSimpleDocumentReason;
import ru.tandemservice.unisession.entity.comission.SessionComission;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionSheetDocument;
import ru.tandemservice.unisession.entity.document.SessionStudentGradeBookDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.print.ISessionSheetPrintDAO;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 18.02.13
 */
public class SessionSheetFormingDao extends UniBaseDao implements ISessionSheetForming
{
    @Override
    public void doSaveSessionSheetDocumentList(Collection<Long> sessionMarkIdList, OrgUnit orgUnit, Date issueDate, Date validityDate, SessionsSimpleDocumentReason reason, boolean inSession, boolean printSheetDocuments)
    {
        List<Long> sheetDocumentIds = Lists.newArrayList();
        final List<SessionMark> markList = getList(SessionMark.class, sessionMarkIdList);

        for (final SessionMark mark : markList)
        {
            final SessionSheetDocument sheet = createSessionSheetDocument(orgUnit, issueDate, validityDate, reason);
            final SessionDocumentSlot slot = createSessionDocumentSlot(sheet, mark, inSession);

            save(sheet);
            save(slot);

			if (slot.getStudentWpeCAction().getActionType().isThemeRequired())
			{
				ISessionDocumentBaseDAO baseDAO = ISessionDocumentBaseDAO.instance.get();
				String theme = baseDAO.themeByLastWpeThemes(slot.getStudentWpeCAction());
				baseDAO.updateProjectTheme(slot, theme, null);
			}

            postprocessDocument(sheet, slot);

            if (printSheetDocuments && !sheetDocumentIds.contains(sheet.getId()))
            {
                sheetDocumentIds.add(sheet.getId());
            }
        }

        if (!sheetDocumentIds.isEmpty())
        {
            String fileName = (sheetDocumentIds.size() > 1 ? "Экзаменационные листы" : "Экзаменационный лист") + ".rtf";
            RtfDocument document = sheetDocumentIds.size() > 1
                    ? ISessionSheetPrintDAO.instance.get().printSheetList(sheetDocumentIds)
                    : ISessionSheetPrintDAO.instance.get().printSheet(sheetDocumentIds.get(0));

            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName(fileName).document(document), true);
        }
    }

    protected SessionSheetDocument createSessionSheetDocument(OrgUnit orgUnit, Date issueDate, Date validityDate, SessionsSimpleDocumentReason reason)
    {
        final SessionSheetDocument sheet = new SessionSheetDocument();
        sheet.setFormingDate(new Date());
        sheet.setIssueDate(issueDate);
        sheet.setDeadlineDate(validityDate);
        sheet.setReason(reason);
        sheet.setOrgUnit(orgUnit);
        sheet.setNumber(sheet.getNumberPrefix() + INumberQueueDAO.instance.get().getNextNumber(sheet));

        return sheet;
    }

    protected SessionDocumentSlot createSessionDocumentSlot(SessionSheetDocument sheet, SessionMark mark, boolean inSession)
    {
        final SessionDocumentSlot slot = new SessionDocumentSlot();
        slot.setInSession(inSession);
        slot.setDocument(sheet);
        slot.setCommission(copyCommission(mark.getCommission()));
        slot.setStudentWpeCAction(mark.getSlot().getStudentWpeCAction());
        slot.setTryNumber(getSlotTryNumber(mark.getSlot().getStudentWpeCAction()));

        return slot;
    }

    private SessionComission copyCommission(SessionComission oldCommission)
    {
        Session session = getSession();
        SessionComission commission = new SessionComission();
        session.save(commission);

        for(SessionComissionPps pps : getList(SessionComissionPps.class, SessionComissionPps.commission().id(), oldCommission.getId()))
        {
            SessionComissionPps newPps = new SessionComissionPps();
            newPps.setCommission(commission);
            newPps.setPps(pps.getPps());
            session.save(newPps);
        }

        return commission;
    }

    /**
     * Для каждой итоговой оценки считаем общее число полученных ранее оценок (не отметок) и добавляем 1.
     */
    protected Integer getSlotTryNumber(EppStudentWpeCAction studentWpCActionSlot)
    {
        Long result = new DQLSelectBuilder().fromEntity(SessionDocumentSlot.class, "d").column(property(SessionDocumentSlot.id().fromAlias("d")))
                .where(eq(property(SessionDocumentSlot.studentWpeCAction().fromAlias("d")), value(studentWpCActionSlot)))
                .where(exists(
                        new DQLSelectBuilder().fromEntity(SessionMark.class, "m").column(property(SessionMark.id().fromAlias("m")))
                                .fetchPath(DQLJoinType.inner, SessionMark.cachedMarkValue().fromAlias("m"), "mv")
                                .fetchPath(DQLJoinType.inner, SessionMark.slot().document().fromAlias("m"), "md")
                                .where(eq(property(SessionMark.slot().fromAlias("m")), property("d")))
                                .where(instanceOf("mv", SessionMarkGradeValueCatalogItem.class))
                                .where(not(instanceOf("md", SessionStudentGradeBookDocument.class)))
                                .buildQuery()))
                .createCountStatement(new DQLExecutionContext(getSession())).<Long>uniqueResult();

        result++;
        return result.intValue();
    }

    /**
     * Метод для пост-обработки документов в проектном слое.
     * @param sheet только что созданный экзам. лист
     * @param slot только что созданный слот для экзам. листа
     */
    protected void postprocessDocument(SessionSheetDocument sheet, SessionDocumentSlot slot)
    {

    }
}
