package ru.tandemservice.unisession.component.orgunit.SessionObjectListTab;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unisession.entity.document.SessionObject;

/**
 * @author vdanilov
 */
@Input({
    @Bind(key=PublisherActivator.PUBLISHER_ID_KEY, binding="orgUnitHolder.id")
})
public class Model {

    private final EntityHolder<OrgUnit> orgUnitHolder = new EntityHolder<OrgUnit>();
    public EntityHolder<OrgUnit> getOrgUnitHolder() { return this.orgUnitHolder; }

    public Long getOrgUnitId() { return this.getOrgUnitHolder().getId(); }
    public OrgUnit getOrgUnit() { return this.getOrgUnitHolder().getValue(); }
    public IEntity getSecuredObject() { return this.getOrgUnit(); }

    private CommonPostfixPermissionModel sec;
    public CommonPostfixPermissionModel getSec() { return this.sec; }
    public void setSec(final CommonPostfixPermissionModel sec) { this.sec = sec; }

    private IDataSettings settings;
    public IDataSettings getSettings() { return this.settings; }
    public void setSettings(final IDataSettings settings) { this.settings = settings; }

    private DynamicListDataSource<SessionObject> dataSource;
    public DynamicListDataSource<SessionObject> getDataSource() { return this.dataSource; }
    public void setDataSource(final DynamicListDataSource<SessionObject> dataSource) { this.dataSource = dataSource; }

}
