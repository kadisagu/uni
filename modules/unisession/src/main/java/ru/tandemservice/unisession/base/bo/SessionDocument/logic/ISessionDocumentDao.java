/* $Id:$ */
package ru.tandemservice.unisession.base.bo.SessionDocument.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;

import java.util.Collection;

/**
 * @author Alexey Lopatin
 * @since 13.11.2016
 */
public interface ISessionDocumentDao extends INeedPersistenceSupport
{
    /**
     * Проставляет значение "Сверена с зачеткой" для оценок студента в сессию по всем выбранным МСРП-ФК
     *
     * @param actionIds МСРП-ФК
     * @param oldValue  старое значение
     */
    void changeCheckedGradeBook(Collection<Long> actionIds, boolean oldValue);
}
