/* $Id$ */
package ru.tandemservice.unisession.component.pps.PpsSessionRetakeList;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.entity.catalog.EppControlActionType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.unisession.entity.document.SessionObject;
import ru.tandemservice.unisession.entity.document.SessionRetakeDocument;

import java.util.Collection;
import java.util.List;

/**
 * @author Vasily Zhukov
 * @since 12.01.2012
 */
public class Model
{
    private IPrincipalContext _principalContext;
    private Person person;
    private DynamicListDataSource<SessionRetakeDocument> dataSource;

    private IDataSettings settings;

    private ISelectModel yearModel;
    private List<HSelectOption> partsList;
    private ISelectModel sessionObjectModel;
    private ISelectModel registryElementModel;
    private ISelectModel controlActionTypeModel;
    private ISelectModel ppsModel;
    private ISelectModel groupModel;

    public EducationYear getYear()
    {
        final Object year = this.getSettings().get("year");
        return year instanceof EducationYear ? (EducationYear) year : null;
    }

    public YearDistributionPart getPart()
    {
        final Object part = this.getSettings().get("part");
        return part instanceof YearDistributionPart ? (YearDistributionPart) part : null;
    }

    public SessionObject getSessionObject()
    {
        final Object sessionObject = this.getSettings().get("sessionObject");
        return sessionObject instanceof SessionObject ? (SessionObject) sessionObject : null;
    }

    @SuppressWarnings("unchecked")
    public Collection<EppRegistryElement> getRegistryElements()
    {
        final Object registryElement = this.getSettings().get("registryElement");
        return (registryElement instanceof Collection && ((Collection) registryElement).size() > 0) ? (Collection) registryElement : null;
    }

    @SuppressWarnings("unchecked")
    public Collection<EppControlActionType> getControlActionTypes()
    {
        final Object controlActionType = this.getSettings().get("controlActionType");
        return (controlActionType instanceof Collection && ((Collection) controlActionType).size() > 0) ? (Collection) controlActionType : null;
    }

    @SuppressWarnings("unchecked")
    public Collection<Group> getGroups()
    {
        final Object group = this.getSettings().get("group");
        return (group instanceof Collection && ((Collection) group).size() > 0) ? (Collection) group : null;
    }

    @SuppressWarnings("unchecked")
    public Collection<PpsEntry> getPpsList()
    {
        final Object pps = this.getSettings().get("pps");
        return (pps instanceof Collection && ((Collection) pps).size() > 0) ? (Collection) pps : null;
    }

    @SuppressWarnings("unchecked")
    public String getNumber()
    {
        return StringUtils.trimToNull(this.getSettings().get("docNumber"));
    }

    // acessors

    public IPrincipalContext getPrincipalContext()
    {
        return _principalContext;
    }

    public void setPrincipalContext(IPrincipalContext principalContext)
    {
        _principalContext = principalContext;
    }

    public Person getPerson()
    {
        return person;
    }

    public void setPerson(Person person)
    {
        this.person = person;
    }

    public DynamicListDataSource<SessionRetakeDocument> getDataSource()
    {
        return this.dataSource;
    }

    public void setDataSource(final DynamicListDataSource<SessionRetakeDocument> dataSource)
    {
        this.dataSource = dataSource;
    }

    public IDataSettings getSettings()
    {
        return this.settings;
    }

    public void setSettings(final IDataSettings settings)
    {
        this.settings = settings;
    }

    public ISelectModel getYearModel()
    {
        return this.yearModel;
    }

    public void setYearModel(final ISelectModel yearModel)
    {
        this.yearModel = yearModel;
    }

    public List<HSelectOption> getPartsList()
    {
        return this.partsList;
    }

    public void setPartsList(final List<HSelectOption> partsList)
    {
        this.partsList = partsList;
    }

    public ISelectModel getSessionObjectModel()
    {
        return this.sessionObjectModel;
    }

    public void setSessionObjectModel(final ISelectModel sessionObjectModel)
    {
        this.sessionObjectModel = sessionObjectModel;
    }

    public ISelectModel getRegistryElementModel()
    {
        return this.registryElementModel;
    }

    public void setRegistryElementModel(final ISelectModel registryElementModel)
    {
        this.registryElementModel = registryElementModel;
    }

    public ISelectModel getControlActionTypeModel()
    {
        return this.controlActionTypeModel;
    }

    public void setControlActionTypeModel(final ISelectModel controlActionTypeModel)
    {
        this.controlActionTypeModel = controlActionTypeModel;
    }

    public ISelectModel getPpsModel()
    {
        return this.ppsModel;
    }

    public void setPpsModel(final ISelectModel ppsModel)
    {
        this.ppsModel = ppsModel;
    }

    public ISelectModel getGroupModel()
    {
        return this.groupModel;
    }

    public void setGroupModel(final ISelectModel groupModel)
    {
        this.groupModel = groupModel;
    }
}
