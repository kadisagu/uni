package ru.tandemservice.unisession.component.sessionListDocument.SessionListDocumentAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.entity.catalog.SessionsSimpleDocumentReason;
import ru.tandemservice.unisession.entity.document.SessionListDocument;
import ru.tandemservice.unisession.util.selectModel.SessionTermModel;

import java.util.Comparator;
import java.util.List;

/**
 * @author iolshvang
 * @since 12.04.2011
 */
@Input({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "orgUnitId"),
    @Bind(key = "cardId", binding = "card.id")
})
public class Model implements SessionTermModel.IStudentBasedDataOwner
{
    public static final String COMPONENT_NAME = Model.class.getPackage().getName();
    private SessionListDocument _card = new SessionListDocument();
    private Long _orgUnitId;
    private OrgUnit _orgUnit;
    private List<SessionsSimpleDocumentReason> _reasonModel;
    private ISelectModel _studentModel;
    private boolean _hasMarks;
    private boolean _addDiscipline;
    private EppStudentWpeCAction _eppSlot;
    private final SessionListDocumentEppSlotAddModel _eppModel = new SessionListDocumentEppSlotAddModel();

    private ISelectModel _termModel;
    private IMultiSelectModel _controlActionModel;

    private Long _currentId;
    private SessionTermModel.TermWrapper _currentTerm;
    private List<EppStudentWpeCAction> _currentEppSlotList;

    public SessionListDocumentEppSlotAddModel getEppModel() { return this._eppModel; }

    public boolean isAddForm() {
        return getCard().getId() == null;
    }

    public boolean isStudentDisabled()
    {
        boolean hasEppSlot = getEppModel().getEppDataSource().getRowList().stream().anyMatch(item -> !item.getEppSlotList().isEmpty());
        return !isAddForm() || hasEppSlot;
    }

    @Override
    public Student getStudent()
    {
        return getCard().getStudent();
    }

    public Long getOrgUnitId()
    {
        return this._orgUnitId;
    }

    public void setOrgUnitId(final Long orgUnitId)
    {
        this._orgUnitId = orgUnitId;
    }

    @Override
    public EppStudentWpeCAction getEppSlot()
    {
        return this._eppSlot;
    }

    public void setEppSlot(final EppStudentWpeCAction eppSlot)
    {
        this._eppSlot = eppSlot;
    }

    public boolean isAddDiscipline()
    {
        return this._addDiscipline;
    }

    public void setAddDiscipline(final boolean addDiscipline)
    {
        this._addDiscipline = addDiscipline;
    }

    public boolean isHasMarks()
    {
        return this._hasMarks;
    }

    public void setHasMarks(final boolean hasMarks)
    {
        this._hasMarks = hasMarks;
    }

    public SessionListDocument getCard()
    {
        return this._card;
    }


    public void setCard(final SessionListDocument card)
    {
        this._card = card;
    }

    public OrgUnit getOrgUnit()
    {
        return this._orgUnit;
    }

    public void setOrgUnit(final OrgUnit orgUnit)
    {
        this._orgUnit = orgUnit;
    }

    public List<SessionsSimpleDocumentReason> getReasonModel()
    {
        return this._reasonModel;
    }

    public void setReasonModel(final List<SessionsSimpleDocumentReason> reasonModel)
    {
        this._reasonModel = reasonModel;
    }

    public ISelectModel getStudentModel()
    {
        return this._studentModel;
    }

    public void setStudentModel(final ISelectModel studentModel)
    {
        this._studentModel = studentModel;
    }

    public static class TermWrapper extends IdentifiableWrapper<Term>
    {
        private static final long serialVersionUID = -6919577577073607505L;
        private final EducationYear _year;
        private final Term _term;

        public TermWrapper(final EducationYear year, final Term term)
        {
            super(term.getId(), term.getTitle() + " " + year.getTitle());
            this._year = year;
            this._term = term;
        }

        public EducationYear getYear()
        {
            return this._year;
        }

        public Term getTerm()
        {
            return this._term;
        }
    }

    public static class TermComparator implements Comparator<TermWrapper>
    {
        public TermComparator()
        {
        }

        @Override
        public int compare(final TermWrapper o1, final TermWrapper o2)
        {
            if (o1.getTerm().equals(o2.getTerm())) {
                return o1.getYear().getIntValue() - o2.getYear().getIntValue();
            }
            return o1.getTerm().getIntValue() - o2.getTerm().getIntValue();
        }
    }

    public TermWithActionSlot getCurrentEntity()
    {
        return null != getEppModel().getEditRow() ? (TermWithActionSlot) getEppModel().getEditRow() : getEppModel().getEppDataSource().getCurrentEntity();
    }

    public boolean isEppSlotListDisabled()
    {
        return getCurrentTerm() == null || isHasMarks();
    }

    public ISelectModel getTermModel()
    {
        return _termModel;
    }

    public void setTermModel(ISelectModel termModel)
    {
        _termModel = termModel;
    }

    public IMultiSelectModel getControlActionModel()
    {
        return _controlActionModel;
    }

    public void setControlActionModel(IMultiSelectModel controlActionModel)
    {
        _controlActionModel = controlActionModel;
    }

    public Long getCurrentId()
    {
        return _currentId;
    }

    public void setCurrentId(Long currentId)
    {
        _currentId = currentId;
    }

    public SessionTermModel.TermWrapper getCurrentTerm()
    {
        return _currentTerm;
    }

    public void setCurrentTerm(SessionTermModel.TermWrapper currentTerm)
    {
        _currentTerm = currentTerm;
    }

    public List<EppStudentWpeCAction> getCurrentEppSlotList()
    {
        return _currentEppSlotList;
    }

    public void setCurrentEppSlotList(List<EppStudentWpeCAction> currentEppSlotList)
    {
        _currentEppSlotList = currentEppSlotList;
    }
}