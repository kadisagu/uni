package ru.tandemservice.unisession.base.bo.SessionListDocument.logic;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.*;

/**
 * Строка, содержащая информацию о выставляемой оценке в экзаменационной карточке.
 * Включает данные об оценке/отметке (запись студента в документе сессии, оценка/отметка, рейтинг, дата выставления, комментарий), состав комиссии и тему работы в сессии.
 * Используется в БК выставления оценок для ввода данных в пределах SearchList-а (элементы ввода в блок-колонках списка забиндены на проперти этой строки).
 * @author avedernikov
 * @since 20.07.2016
 */
public class SessionListDocumentMarkRow extends DataWrapper
{
	public static final String PROP_REG_ELEM = "regElem";

	private final SessionDocumentSlot slot;
	private SessionMarkCatalogItem markValue;
	private Double rating;
	private TreeSet<PpsEntry> pps = new TreeSet<>(Comparator.comparing(PpsEntry::getFullFio));
	private Date performDate;
	private String theme;
	private String comment;
	private final boolean themeRequired;

	public SessionListDocumentMarkRow(SessionDocumentSlot slot, SessionMark mark, Collection<PpsEntry> pps, String theme)
	{
		super(slot);
		put(PROP_REG_ELEM, regElemTitle(slot.getStudentWpeCAction()));

		this.slot = slot;
		if (mark != null)
		{
			this.markValue = mark.getValueItem();
			this.rating = mark.getPoints();
			this.performDate = mark.getPerformDate();
			this.comment = mark.getComment();
		}
		this.pps.addAll(pps);
		this.theme = theme;
		this.themeRequired = slot.getStudentWpeCAction().getActionType().isThemeRequired();
	}

	private static String regElemTitle(EppStudentWpeCAction cAction)
	{
		return cAction.getStudentWpe().getRegistryElementPart().getTitle() + ", " + cAction.getType().getTitle();
	}

	public SessionDocumentSlot getSlot()
	{
		return slot;
	}

	public SessionMarkCatalogItem getMarkValue()
	{
		return markValue;
	}

	public void setMarkValue(final SessionMarkCatalogItem markValue)
	{
		this.markValue = markValue;
	}

	public Double getRating()
	{
		return rating;
	}

	public void setRating(Double rating)
	{
		this.rating = rating;
	}

	/** Состав комиссии; ППС отсортированы по ФИО. */
	public List<PpsEntry> getPpsList()
	{
		return new ArrayList<>(pps);
	}

	public void setPpsList(final List<PpsEntry> ppsList)
	{
		pps.clear();
		pps.addAll(ppsList);
	}

	public Date getPerformDate()
	{
		return performDate;
	}

	public void setPerformDate(final Date performDate)
	{
		this.performDate = performDate;
	}

	public String getTheme()
	{
		return theme;
	}

	public void setTheme(final String theme)
	{
		this.theme = theme;
	}

	public String getComment()
	{
		return comment;
	}

	public void setComment(final String comment)
	{
		this.comment = comment;
	}

	public boolean isThemeRequired()
	{
		return themeRequired;
	}
}
