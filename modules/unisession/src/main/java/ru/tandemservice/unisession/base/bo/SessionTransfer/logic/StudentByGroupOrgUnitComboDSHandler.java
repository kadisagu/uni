/* $Id$ */
// Copyright 2006-2012 Tandem Service Software
package ru.tandemservice.unisession.base.bo.SessionTransfer.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.mq.UniDQLExpressions;

/**
 * @author Vasily Zhukov
 * @since 12.01.2012
 */
public class StudentByGroupOrgUnitComboDSHandler extends DefaultComboDataSourceHandler
{
    public static final String ORG_UNIT = "orgUnit";

    public StudentByGroupOrgUnitComboDSHandler(String ownerId)
    {
        super(ownerId, Student.class);
        setOrderByProperty(Student.person().identityCard().fullFio().s());
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {
        ep.dqlBuilder.where(DQLExpressions.eq(DQLExpressions.property(Student.archival().fromAlias("e")), DQLExpressions.value(Boolean.FALSE)));
        ep.dqlBuilder.where(DQLExpressions.eq(DQLExpressions.property(Student.educationOrgUnit().groupOrgUnit().fromAlias("e")), DQLExpressions.commonValue(ep.context.get(ORG_UNIT))));

        String filter = ep.input.getComboFilterByValue();
        if (StringUtils.isNotEmpty(filter))
        {
            ep.dqlBuilder.joinPath(DQLJoinType.inner, Student.person().identityCard().fromAlias("e"), "c");
            ep.dqlBuilder.joinPath(DQLJoinType.left, Student.group().fromAlias("e"), "g");
            ep.dqlBuilder.where(UniDQLExpressions.likeFioAndGroupTitle("c", "g", filter));
        }
    }
}
