package ru.tandemservice.unisession.entity.document.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unisession.entity.document.SessionDocument;
import ru.tandemservice.unisession.entity.document.SessionDocumentPrintVersion;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Печатная форма закрытого документа сессии
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionDocumentPrintVersionGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.document.SessionDocumentPrintVersion";
    public static final String ENTITY_NAME = "sessionDocumentPrintVersion";
    public static final int VERSION_HASH = -517543381;
    private static IEntityMeta ENTITY_META;

    public static final String L_DOC = "doc";
    public static final String P_CONTENT = "content";

    private SessionDocument _doc;     // Документ сессии
    private byte[] _content;     // Сохраненная печатная форма

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Документ сессии. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public SessionDocument getDoc()
    {
        return _doc;
    }

    /**
     * @param doc Документ сессии. Свойство не может быть null и должно быть уникальным.
     */
    public void setDoc(SessionDocument doc)
    {
        dirty(_doc, doc);
        _doc = doc;
    }

    /**
     * @return Сохраненная печатная форма. Свойство не может быть null.
     */
    @NotNull
    public byte[] getContent()
    {
        return _content;
    }

    /**
     * @param content Сохраненная печатная форма. Свойство не может быть null.
     */
    public void setContent(byte[] content)
    {
        dirty(_content, content);
        _content = content;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SessionDocumentPrintVersionGen)
        {
            setDoc(((SessionDocumentPrintVersion)another).getDoc());
            setContent(((SessionDocumentPrintVersion)another).getContent());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionDocumentPrintVersionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionDocumentPrintVersion.class;
        }

        public T newInstance()
        {
            return (T) new SessionDocumentPrintVersion();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "doc":
                    return obj.getDoc();
                case "content":
                    return obj.getContent();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "doc":
                    obj.setDoc((SessionDocument) value);
                    return;
                case "content":
                    obj.setContent((byte[]) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "doc":
                        return true;
                case "content":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "doc":
                    return true;
                case "content":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "doc":
                    return SessionDocument.class;
                case "content":
                    return byte[].class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionDocumentPrintVersion> _dslPath = new Path<SessionDocumentPrintVersion>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionDocumentPrintVersion");
    }
            

    /**
     * @return Документ сессии. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unisession.entity.document.SessionDocumentPrintVersion#getDoc()
     */
    public static SessionDocument.Path<SessionDocument> doc()
    {
        return _dslPath.doc();
    }

    /**
     * @return Сохраненная печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionDocumentPrintVersion#getContent()
     */
    public static PropertyPath<byte[]> content()
    {
        return _dslPath.content();
    }

    public static class Path<E extends SessionDocumentPrintVersion> extends EntityPath<E>
    {
        private SessionDocument.Path<SessionDocument> _doc;
        private PropertyPath<byte[]> _content;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Документ сессии. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unisession.entity.document.SessionDocumentPrintVersion#getDoc()
     */
        public SessionDocument.Path<SessionDocument> doc()
        {
            if(_doc == null )
                _doc = new SessionDocument.Path<SessionDocument>(L_DOC, this);
            return _doc;
        }

    /**
     * @return Сохраненная печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionDocumentPrintVersion#getContent()
     */
        public PropertyPath<byte[]> content()
        {
            if(_content == null )
                _content = new PropertyPath<byte[]>(SessionDocumentPrintVersionGen.P_CONTENT, this);
            return _content;
        }

        public Class getEntityClass()
        {
            return SessionDocumentPrintVersion.class;
        }

        public String getEntityName()
        {
            return "sessionDocumentPrintVersion";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
