// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisession.component.sessionSheet.SessionSheetAdd;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.IComponentRegion;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.commonbase.tapestry.component.renderComponent.RenderComponent;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unisession.component.student.ControlActionMarkHistory.IControlActionMarkHistoryHandler;
import ru.tandemservice.unisession.util.selectModel.SessionTermModel;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author oleyba
 * @since 2/11/11
 */
public class Controller<D extends IDAO<M>, M extends Model> extends AbstractBusinessController<D, M>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final M model = this.getModel(component);
        this.getDao().prepare(model);
    }

    public void onClickApply(final IBusinessComponent component)
    {
        final M model = this.getModel(component);
        final ErrorCollector errorCollector = component.getUserContext().getErrorCollector();
        this.getDao().validate(model, errorCollector);
        if (errorCollector.hasErrors()) {
            return;
        }
        this.getDao().update(model);
        this.deactivate(component);
    }

    public void onChangeStudent(final IBusinessComponent component)
    {
        M model = getModel(component);
        SessionTermModel termModel = (SessionTermModel) model.getTermModel();
        if (termModel == null) return;

        Map<EducationYear, Set<YearDistributionPart>> yearPartsByYearMap = termModel.getValueMap();
        if (yearPartsByYearMap == null || yearPartsByYearMap.size() != 1) return;


        Map.Entry<EducationYear, Set<YearDistributionPart>> yearSetEntry = yearPartsByYearMap.entrySet().iterator().next();
        if (yearSetEntry.getValue().size() != 1) return;

        EducationYear year = yearSetEntry.getKey();
        YearDistributionPart part = yearSetEntry.getValue().iterator().next();
        SessionTermModel.TermWrapper termWrapper = new SessionTermModel.TermWrapper(year, part);
        model.setTerm(termWrapper);
    }

    public void onChangeControlAction(final IBusinessComponent component)
    {
        final M model = this.getModel(component);
        this.getDao().onChangeControlAction(model);
        if (null == model.getEppSlot()) {
            return;
        }
        final IComponentRegion childComponentRegion = component.getChildRegion(RenderComponent.getRegionName(ru.tandemservice.unisession.component.student.ControlActionMarkHistory.Model.COMPONENT_NAME));
        if (null == childComponentRegion) {
            return;
        }
        final IBusinessComponent childComponent = childComponentRegion.getActiveComponent();
        ((IControlActionMarkHistoryHandler) childComponent.getController()).refresh(model.getEppSlot().getId(), childComponent);
    }

    public void onNeedResize(final IBusinessComponent component)
    {
    }
}