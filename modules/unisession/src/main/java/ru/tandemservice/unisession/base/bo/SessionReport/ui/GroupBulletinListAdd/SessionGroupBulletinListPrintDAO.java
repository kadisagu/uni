/* $Id:$ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.GroupBulletinListAdd;

import com.google.common.collect.Iterables;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfText;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.util.PersonSecurityUtil;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4ActionTypeRow;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.uniepp.entity.student.group.gen.EppRealEduGroup4ActionTypeRowGen;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.base.bo.SessionReport.util.SessionReportUtil;
import ru.tandemservice.unisession.base.bo.SessionReport.util.UniSessionFilterAddon;
import ru.tandemservice.unisession.entity.catalog.UnisessionTemplate;
import ru.tandemservice.unisession.entity.catalog.codes.UnisessionCommonTemplateCodes;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.report.UnisessionGroupBulletinListReport;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 2/24/12
 */
public class SessionGroupBulletinListPrintDAO extends UniBaseDao implements ISessionGroupBulletinListPrintDAO
{
    @Override
    public UnisessionGroupBulletinListReport createStoredReport(SessionReportGroupBulletinListAddUI model)
    {
        UnisessionGroupBulletinListReport report = new UnisessionGroupBulletinListReport();

        DatabaseFile content = new DatabaseFile();
        content.setContent(print(model));
        save(content);
        report.setContent(content);

        report.setFormingDate(new Date());
        report.setExecutor(PersonSecurityUtil.getExecutor());
        report.setSessionObject(model.getSessionObject());

        final UniSessionFilterAddon filterAddon = model.getSessionFilterAddon();

        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_DISC_KIND, UnisessionGroupBulletinListReport.P_DISC_KINDS, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_COURSE, UnisessionGroupBulletinListReport.P_COURSE, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_GROUP_WITH_NOGROUP, UnisessionGroupBulletinListReport.P_GROUPS, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_CONTROL_FORM, UnisessionGroupBulletinListReport.P_CONTROL_ACTION_TYPES, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_REGISTRY_STRUCTURE, UnisessionGroupBulletinListReport.P_REGISTRY_STRUCTURE, "title");

        save(report);
        return report;
    }

    private byte[] print(SessionReportGroupBulletinListAddUI model)
    {
        UnisessionTemplate templateItem = getCatalogItem(UnisessionTemplate.class, UnisessionCommonTemplateCodes.GROUP_BULLETIN_LIST_REPORT);
        if (templateItem == null)
            throw new RuntimeException("Печатный шаблон есть, но не импортирован в справочник шаблонов.");
        RtfDocument template = new RtfReader().read(templateItem.getContent());

        RtfDocument result = null;

        List<ReportTableData> reportTableList = prepareReportData(model);

        for (ReportTableData reportTable : reportTableList)
        {
            RtfDocument document = printReportTable(template.getClone(), model, reportTable);
            if (null == result)
                result = document;
            else if (null != document)
            {
                RtfUtil.modifySourceList(result.getHeader(), document.getHeader(), document.getElementList());
                result.getElementList().addAll(document.getElementList());
            }
        }

        if (null != result)
            return RtfUtil.toByteArray(result);
        else
            throw new ApplicationException("Нет данных для построения отчета.");
    }

    private RtfDocument printReportTable(RtfDocument rtf, SessionReportGroupBulletinListAddUI model, ReportTableData reportTable)
    {
        OrgUnit academy = TopOrgUnit.getInstance();
        OrgUnit ou = model.getSessionObject().getOrgUnit();

        RtfInjectModifier modifier = new RtfInjectModifier();
        modifier.put("formingDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()));
        modifier.put("vuzTitle", academy.getPrintTitle());
        modifier.put("ouTitle", ou.getPrintTitle());
        modifier.put("eduYear", model.getSessionObject().getEducationYear().getTitle());
        modifier.put("groupTitle", StringUtils.isEmpty(reportTable.getGroup()) ? "вне групп" : reportTable.getGroup());
        List<String> terms = new ArrayList<>(reportTable.getTerms());
        Collections.sort(terms);
        modifier.put("term", UniStringUtils.joinWithSeparator(" ",  StringUtils.join(terms, ", "), "(" + model.getSessionObject().getYearDistributionPart().getShortTitle() + ")"));
        modifier.put("executor", PersonSecurityUtil.getExecutor());

        SessionReportManager.addOuLeaderData(modifier, ou, "ouleader", "FIOouleader");
        modifier.modify(rtf);

        final Map<EppFControlActionType, Map<EppRegistryElementPart, Set<SessionBulletinDocument>>> bulletinMap = reportTable.getBulletinMap();
        List<String[]> rtfTable = new ArrayList<>();

        for (Map.Entry<EppFControlActionType, Map<EppRegistryElementPart, Set<SessionBulletinDocument>>> eppFControlActionTypeMapEntry : bulletinMap.entrySet()) {
            List<EppRegistryElementPart> discList = new ArrayList<>(eppFControlActionTypeMapEntry.getValue().keySet());
            Collections.sort(discList, (o1, o2) -> {
                int res = o1.getTitle().compareTo(o2.getRegistryElement().getTitle());
                if (res != 0) return res;
                res = o1.getRegistryElement().getNumber().compareTo(o2.getRegistryElement().getNumber());
                if (res != 0) return res;
                return o1.getNumber() - o2.getNumber();
            });

            int rowNum = 1;
            for (EppRegistryElementPart disc : discList) {
                boolean first = true;
                StringBuilder pps = new StringBuilder();
                StringBuilder caType = new StringBuilder();
                StringBuilder date = new StringBuilder();
                StringBuilder number = new StringBuilder();
                for (SessionBulletinDocument bulletin : eppFControlActionTypeMapEntry.getValue().get(disc)) {
                    if (!first)
                        for (StringBuilder b : new StringBuilder[] {pps, caType, date, number})
                            b.append("\\par ");
                    first = false;
                    List<String> ppsList = reportTable.getPpsMap().get(bulletin);
                    if (null != ppsList)
                        pps.append(StringUtils.join(ppsList, ", "));
                    caType.append(bulletin.getGroup().getType().getTitle());
                    if (bulletin.getPerformDate() != null)
                        date.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(bulletin.getPerformDate()));
                    number.append(bulletin.getNumber());
                }
                rtfTable.add(new String[] {
                        String.valueOf(rowNum++),
                        model.isUseFullDiscTitle() ? disc.getTitleWithNumber() : disc.getRegistryElement().getTitle(),
                        StringUtils.trimToEmpty(disc.getRegistryElement().getShortTitle()),
                        pps.toString(),
                        caType.toString(),
                        date.toString(),
                        number.toString()
                });
            }
        }



        new RtfTableModifier()
            .put("T", rtfTable.toArray(new String[rtfTable.size()][]))
            .put("T", new RtfRowIntercepterBase()
            {
                @Override
                public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
                {
                    List<IRtfElement> list = new ArrayList<>();
                    IRtfText text = RtfBean.getElementFactory().createRtfText(value);
                    text.setRaw(true);
                    list.add(text);
                    return list;
                }
            })
            .modify(rtf);
        
        return rtf;
    }

    private List<ReportTableData> prepareReportData(SessionReportGroupBulletinListAddUI model)
    {
        final Map<String, ReportTableData> tableMap = new HashMap<>();
        final Map<SessionBulletinDocument, List<String>> ppsMap = new HashMap<>();

        final DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(EppStudentWpeCAction.class, "wpca")
            .fromEntity(SessionBulletinDocument.class, "b")
            .fromEntity(SessionDocumentSlot.class, "slot")
            .fromEntity(EppRealEduGroupRow.class, "g_row")
            .fromEntity(EppGroupType.class, "gt")
            .where(eq(property(SessionDocumentSlot.document().fromAlias("slot")), property("b")))
            .where(eq(property(SessionDocumentSlot.studentWpeCAction().fromAlias("slot")), property("wpca")))
            .where(eq(property(EppRealEduGroupRow.studentWpePart().fromAlias("g_row")), property("wpca")))
            .where(eq(property("gt"), property(EppStudentWpeCAction.type().fromAlias("wpca"))))
            ;
        
        dql.where(eq(property(SessionBulletinDocument.sessionObject().fromAlias("b")), value(model.getSessionObject())));

        final DQLSelectBuilder dqlX = new DQLSelectBuilder()
            .fromEntity(EppRealEduGroup4ActionTypeRow.class, "rel")
            .column(property(EppRealEduGroup4ActionTypeRowGen.group().id().fromAlias("rel")));


        dql.where(in(property(EppRealEduGroupRow.group().id().fromAlias("g_row")), dqlX.buildQuery()));

        dql.column("b");
        dql.column(property(EppRealEduGroupRow.studentGroupTitle().fromAlias("g_row")));
        dql.column(property(EppStudentWpeCAction.studentWpe().term().title().fromAlias("wpca")));
        dql.order(property(SessionBulletinDocument.performDate().fromAlias("b")));

        model.getSessionFilterAddon().applyFilters(dql, "wpca");

        for (Object[] row : dql.createStatement(getSession()).<Object[]>list()) {
            SessionBulletinDocument bulletin = (SessionBulletinDocument) row[0];
            String group = (String) row[1];
            String term = (String) row[2];
            ReportTableData reportTable = tableMap.get(group);
            if (null == reportTable) 
                tableMap.put(group, reportTable = new ReportTableData(group, ppsMap));

            EppFControlActionType actionType = get(EppFControlActionType.class, EppFControlActionType.eppGroupType(), bulletin.getGroup().getType());

            final Map<EppRegistryElementPart, Set<SessionBulletinDocument>> partSetMap = SafeMap.safeGet(reportTable.getBulletinMap(), actionType, HashMap.class);
            SafeMap.safeGet(partSetMap, bulletin.getGroup().getActivityPart(), LinkedHashSet.class).add(bulletin);
            reportTable.getTerms().add(term);
            ppsMap.put(bulletin, null);
        }

        for (List<SessionBulletinDocument> elements : Iterables.partition(ppsMap.keySet(), DQL.MAX_VALUES_ROW_NUMBER)) {

            DQLSelectBuilder ppsDQL = new DQLSelectBuilder()
                    .fromEntity(SessionComissionPps.class, "rel")
                    .fromEntity(SessionBulletinDocument.class, "b")
                    .where(eq(property(SessionComissionPps.commission().fromAlias("rel")), property(SessionBulletinDocument.commission().fromAlias("b"))))
                    .where(in(property("b"), elements))
                    .column("b")
                    .column(property(SessionComissionPps.pps().person().identityCard().fullFio().fromAlias("rel")))
                    .order(property(SessionComissionPps.pps().person().identityCard().fullFio().fromAlias("rel")))
                    ;
            for (Object[] row : ppsDQL.createStatement(getSession()).<Object[]>list()) {
                SessionBulletinDocument bulletin = (SessionBulletinDocument) row[0];
                String pps = (String) row[1];
                int li = pps.lastIndexOf(" ");
                if (li != -1)
                    pps = pps.replace(pps.substring(li + 1, pps.length()), pps.charAt(li + 1) + ".");
                li = pps.lastIndexOf(" ");
                int i = pps.indexOf(" ");
                if (i != li)
                    pps = pps.replace(pps.substring(i + 1, li), pps.charAt(i + 1) + ".");
                List<String> ppsList = ppsMap.get(bulletin);
                if (null == ppsList)
                    ppsMap.put(bulletin, ppsList = new ArrayList<>());
                ppsList.add(pps);
            }

        }

        List<ReportTableData> tableList = new ArrayList<>(tableMap.values());
        Collections.sort(tableList);
        return tableList;
    }

    private static class ReportTableData implements Comparable
    {
        private final String group;
        private final Set<String> terms = new HashSet<>();
        private final Map<EppFControlActionType, Map<EppRegistryElementPart, Set<SessionBulletinDocument>>> bulletinMap = new TreeMap<>((o1, o2) -> o1.getPriority() - o2.getPriority());
        private final Map<SessionBulletinDocument, List<String>> ppsMap;

        private ReportTableData(String group, Map<SessionBulletinDocument, List<String>> ppsMap)
        {
            this.group = group;
            this.ppsMap = ppsMap;
        }

        @Override
        public int compareTo(Object o)
        {
            if (!(o instanceof ReportTableData))
                return 0;
            if (this.getGroup() == null)
                return -1;
            if (((ReportTableData) o).getGroup() == null)
                return 1;
            return this.getGroup().compareTo(((ReportTableData) o).getGroup());
        }

        public String getGroup() { return group; }
        public Set<String> getTerms() { return terms; }
        public Map<EppFControlActionType, Map<EppRegistryElementPart, Set<SessionBulletinDocument>>> getBulletinMap() { return bulletinMap; }
        public Map<SessionBulletinDocument, List<String>> getPpsMap() { return ppsMap; }
    }
}
