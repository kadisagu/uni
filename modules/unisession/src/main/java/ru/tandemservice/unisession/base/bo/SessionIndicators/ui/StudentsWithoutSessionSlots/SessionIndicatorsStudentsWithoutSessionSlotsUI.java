/**
 *$Id$
 */
package ru.tandemservice.unisession.base.bo.SessionIndicators.ui.StudentsWithoutSessionSlots;

import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList.AbstractUniStudentListUI;
import ru.tandemservice.uniepp.dao.student.EppStudentSlotDAO;

import java.util.Date;

/**
 * @author Alexander Shaburov
 * @since 20.03.13
 */
public class SessionIndicatorsStudentsWithoutSessionSlotsUI extends AbstractUniStudentListUI
{
    @Override
    public String getSettingsKey()
    {
        return "session.StudentsWithoutSessionSlots.filter";
    }

    public void onClickSync()
    {
        // TODO: call daemon
        onComponentRefresh();
    }

    public String getDaemonStatus(final String message)
    {
        final Long date = EppStudentSlotDAO.DAEMON.getCompleteStatus();
        if (null != date)
            return DateFormatter.DATE_FORMATTER_WITH_TIME.format(new Date(date));

        return message;
    }
}
