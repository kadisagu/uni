/* $Id$ */
package ru.tandemservice.unisession.dao.document;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.entity.document.SessionDocument;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;

/**
 * @author oleyba
 * @since 6/24/11
 */
public interface ISessionDocumentBaseDAO
{
    SpringBeanCache<ISessionDocumentBaseDAO> instance = new SpringBeanCache<>(ISessionDocumentBaseDAO.class.getName());

    /**
     * Закрывает документ, проверяя по выставленным в нем оценкам, можно закрыть, или нет
     * @param document документ
     */
    @Transactional(propagation= Propagation.REQUIRED, readOnly=false)
    void doCloseDocument(SessionDocument document);

    /**
     * Сохраняет тему работы для студента по документу о сдаче мероприятия
     * @param slot - студент в документе
     * @param theme - тема
     * @param comment - комментарий
     */
    @Transactional(propagation= Propagation.REQUIRED, readOnly=false)
    void updateProjectTheme(SessionDocumentSlot slot, String theme, String comment);

	/**
	 * Сохранить тему работы студента по документу о сдаче мероприятия. Если тема и руководитель не заданы (равны null), объект темы удаляется.
	 * @param slot Запись студента в документе сессии.
	 * @param theme Тема
	 * @param comment Комментарий
	 * @param supervisor Руководитель
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	void updateProjectTheme(SessionDocumentSlot slot, String theme, String comment, PpsEntry supervisor);

    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    EppGradeScale getGradeScale(SessionDocument document, EppStudentWpeCAction slot);

    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    EppGradeScale getGradeScale(EppStudentWpeCAction slot);

    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    EppGradeScale getGradeScale(EppRegistryElementPart discipline, EppFControlActionType fca);


    /**
     * @return true, если требуется игнорировать степени готовности УГС при формировании документов
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    boolean isIgnoreRealEduGroupCompleteLevel();

    /**
     * Указывает, требуется ли игнорировать степени готовности УГС при формировании документов
     * @param flag значение признака {@see isIgnoreRealEduGroupCompleteLevel}
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doSetIgnoreRealEduGroupCompleteLevel(boolean flag);

	/**
	 * Построить запрос, фильтрующий записи студента в документе сессии, для которых требуются темы (согласно группам ФИК из МСРП).
	 * Поскольку в документе сессии могут быть записи по разным МСРП, в т.ч. с разными ФИК, то тема требуется только для некоторых из них.
	 * Используется, например, следующим образом:<p/>
	 * {@code new DQLSelectBuilder.fromEntity(SessionDocumentSlot.class, slotAlias).where(exists(fcaTypesRequiresThemeForSlots(slotAlias).buildQuery()))}
	 * @param slotAlias алиас записи мероприятия студента ({@link SessionDocumentSlot}) из основного запроса
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	DQLSelectBuilder fcaTypesRequiresThemeForSlots(final String slotAlias);

	/**
	 * Существуют ли для документа сессии записи мероприятия студента по формам итогового контроля, требующим темы.
	 * @param document документ сессии
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	boolean isThemeRequired(SessionDocument document);

	/**
	 * Тема работы в сессии из последнего по дате формирования документа сессии.
	 * @param wpeCAction МСРП-ФК, для которого ищется тема
	 * @return null, если нет ни одного документа сессии с темой по данному мероприятию; в противном случае - тема из последнего документа сессии по МСРП-ФК, имеющего тему.
	 */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    String themeByLastWpeThemes(EppStudentWpeCAction wpeCAction);
}
