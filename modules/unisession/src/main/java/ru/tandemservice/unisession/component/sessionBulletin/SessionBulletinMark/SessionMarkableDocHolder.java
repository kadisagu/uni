/* $Id$ */
package ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinMark;

import java.util.Date;

import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unisession.entity.comission.SessionComission;
import ru.tandemservice.unisession.entity.document.SessionDocument;
import ru.tandemservice.unisession.entity.document.SessionObject;

/**
 * @author oleyba
 * @since 6/15/11
 */
public class SessionMarkableDocHolder extends EntityHolder<SessionDocument>
{
    private IMarkableDoc doc;

    public interface IMarkableDoc
    {
        IMarkableDocProperties getMarkingProperties();
    }

    public interface IMarkableDocProperties
    {
        Date getPerformDate();

        SessionComission getCommission();

        EppRegistryElementPart getRegistryElementPart();

        EppFControlActionType getCAType();

        SessionObject getSessionObject();
    }

    @Override
    public SessionDocument refresh(final Class<? extends SessionDocument> requireKlass) {
        return this.checkDocumentClass(super.refresh(requireKlass));
    }

    @Override
    public SessionDocument refresh() {
        return this.checkDocumentClass(super.refresh());
    }

    private SessionDocument checkDocumentClass(final SessionDocument value) {
        this.doc = null;
        if (value instanceof IMarkableDoc)
        {
            this.doc = (IMarkableDoc) value;
            return value;
        } else {
            throw new IllegalStateException();
        }
    }

    public IMarkableDocProperties getProperties() {
        if (this.doc == null) {
            return null;
        }
        return this.doc.getMarkingProperties();
    }
}
