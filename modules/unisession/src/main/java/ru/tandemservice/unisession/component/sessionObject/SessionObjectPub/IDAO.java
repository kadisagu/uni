/* $*/

package ru.tandemservice.unisession.component.sessionObject.SessionObjectPub;

import ru.tandemservice.uni.dao.IListDataSourceDao;
import ru.tandemservice.uni.dao.IUpdateable;

/**
 * @author oleyba
 * @since 3/24/11
 */
public interface IDAO extends IUpdateable<Model>, IListDataSourceDao<Model>
{
    void doUnlock(Long id);

    void prepareBulletinDataSource(Model model);

    void prepareSessionDataSource(Model model);

    void delete(Long id);
}
