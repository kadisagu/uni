/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionMark.ui.UnactualList;

import com.google.common.collect.ImmutableList;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unisession.base.bo.SessionMark.SessionMarkManager;
import ru.tandemservice.unisession.base.bo.SessionMark.logic.SessionUnactualMarkDSHandler;

/**
 * @author oleyba
 * @since 10/14/11
 */
public class SessionMarkUnactualListUI extends UIPresenter
{
    public static final long STATE_NOT_TRANSFERRED_ID = 0L;
    public static final long STATE_TRANSFERRED_ID = 1L;

    public static final long EPV_STATE_CURRENT_ID = 0L;
    public static final long EPV_STATE_ALL_ID = 1L;

    private ISelectModel stateModel = new LazySimpleSelectModel<>(ImmutableList.of(
            new IdentifiableWrapper(STATE_NOT_TRANSFERRED_ID, "не перезачтенные"),
            new IdentifiableWrapper(STATE_TRANSFERRED_ID, "перезачтенные"))); // todo
    private ISelectModel epvStateModel = new LazySimpleSelectModel<>(ImmutableList.of(
            new IdentifiableWrapper(EPV_STATE_CURRENT_ID, "только текущая версия УП"),
            new IdentifiableWrapper(EPV_STATE_ALL_ID, "только неактуальные версии УП"))); // todo

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(SessionUnactualMarkDSHandler.PARAM_STUDENT, getSettings().get("student"));
        final IdentifiableWrapper state = getSettings().<IdentifiableWrapper>get("state");
        dataSource.put(SessionUnactualMarkDSHandler.PARAM_TRANSFERRED, state == null ? null : STATE_TRANSFERRED_ID == state.getId());
        final IdentifiableWrapper epvState = getSettings().<IdentifiableWrapper>get("epvState");
        dataSource.put(SessionUnactualMarkDSHandler.PARAM_CURRENT_EPV, epvState == null ? null : EPV_STATE_CURRENT_ID == epvState.getId());
    }

    /* todo
    public void getPrinter()
    {
        return new ExcelListDataSourcePrinter("Почасовики", model.getDataSource()).setupPrintAll()
    }
    */

    public void onDeleteEntityFromList()
    {
        SessionMarkManager.instance().regularMarkDao().deleteUnactualMark(getListenerParameterAsLong());
    }

    public ISelectModel getStateModel()
    {
        return stateModel;
    }

    public ISelectModel getEpvStateModel()
    {
        return epvStateModel;
    }
}
