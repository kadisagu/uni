/* $Id$ */
package ru.tandemservice.unisession.component.pps.PpsSessionRetakeList;

import com.google.common.collect.Lists;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.util.NumberAsStringComparator;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.ui.selectModel.PpsEntryModel;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniedu.base.bo.EducationYear.utils.EducationYearModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.gen.EppRegistryElementGen;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.uniepp.ui.EppRegistryElementSelectModel;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionObject;
import ru.tandemservice.unisession.entity.document.SessionRetakeDocument;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.count;

/**
 * @author Vasily Zhukov
 * @since 12.01.2012
 */
public class DAO extends UniBaseDao implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setPerson(PersonManager.instance().dao().getPerson(model.getPrincipalContext()));

        model.setYearModel(new EducationYearModel()
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                DQLSelectBuilder sessionObjectExists = new DQLSelectBuilder()
                .fromEntity(SessionObject.class, alias)
                .column(property(SessionObject.educationYear().id().fromAlias(alias)))
                //.where(eq(property(SessionObject.orgUnit().fromAlias(alias)), value(model.getOrgUnit())));
                .where(in(DQLExpressions.property(SessionObject.id().fromAlias(alias)), new DQLSelectBuilder().fromEntity(SessionRetakeDocument.class, "retake")
                        .column(property(SessionRetakeDocument.sessionObject().id().fromAlias("retake")))
                        .joinEntity("retake", DQLJoinType.inner, SessionComissionPps.class, "retakePps", eq(property(SessionComissionPps.commission().fromAlias("retakePps")), property(SessionRetakeDocument.commission().fromAlias("retake"))))
                        .where(eq(property(SessionComissionPps.pps().person().fromAlias("retakePps")), value(model.getPerson())))
                        .buildQuery()
                ));

                DQLSelectBuilder dql = new DQLSelectBuilder();
                dql.fromEntity(EducationYear.class, alias);
                dql.where(in(property(EducationYear.id().fromAlias(alias)), sessionObjectExists.buildQuery()));
                dql.order(property(EducationYear.intValue().fromAlias(alias)));
                return dql;
            }
        });

        DQLSelectBuilder partsDQL = new DQLSelectBuilder()
        .fromEntity(SessionObject.class, "obj")
        .column(property(SessionObject.yearDistributionPart().fromAlias("obj")))
        //.where(eq(property(SessionObject.orgUnit().fromAlias("obj")), value(model.getOrgUnit())))
        .where(in(DQLExpressions.property(SessionObject.id().fromAlias("obj")), new DQLSelectBuilder().fromEntity(SessionRetakeDocument.class, "retake")
                .column(property(SessionRetakeDocument.sessionObject().id().fromAlias("retake")))
                .joinEntity("retake", DQLJoinType.inner, SessionComissionPps.class, "retakePps", eq(property(SessionComissionPps.commission().fromAlias("retakePps")), property(SessionRetakeDocument.commission().fromAlias("retake"))))
                .where(eq(property(SessionComissionPps.pps().person().fromAlias("retakePps")), value(model.getPerson())))
                .buildQuery()
        ))
        .order(property(SessionObject.yearDistributionPart().code().fromAlias("obj")));
        if (model.getYear() != null)
        {
            partsDQL.where(eq(property(SessionObject.educationYear().fromAlias("obj")), value(model.getYear())));
        }
        Set<YearDistributionPart> parts = new LinkedHashSet<>(partsDQL.createStatement(this.getSession()).<YearDistributionPart>list());
        model.setPartsList(HierarchyUtil.listHierarchyNodesWithParents(new ArrayList<>(parts), false));

        model.setSessionObjectModel(new DQLFullCheckSelectModel()
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(SessionObject.class, alias)
                //.where(eq(property(SessionObject.orgUnit().fromAlias(alias)), value(model.getOrgUnit())));
                .where(in(DQLExpressions.property(SessionObject.id().fromAlias(alias)), new DQLSelectBuilder().fromEntity(SessionRetakeDocument.class, "retake")
                        .column(property(SessionRetakeDocument.sessionObject().id().fromAlias("retake")))
                        .joinEntity("retake", DQLJoinType.inner, SessionComissionPps.class, "retakePps", eq(property(SessionComissionPps.commission().fromAlias("retakePps")), property(SessionRetakeDocument.commission().fromAlias("retake"))))
                        .where(eq(property(SessionComissionPps.pps().person().fromAlias("retakePps")), value(model.getPerson())))
                        .buildQuery()
                ));
                if (null != model.getYear() && null != model.getPart())
                {
                    dql.where(eq(property(SessionObject.educationYear().fromAlias(alias)), value(model.getYear())));
                    dql.where(eq(property(SessionObject.yearDistributionPart().fromAlias(alias)), value(model.getPart())));
                } else
                {
                    dql.where(isNull(property(SessionObject.id().fromAlias(alias))));
                }
                dql.order(property(SessionObject.startupDate().fromAlias(alias)));
                return dql;
            }
        });

        model.setRegistryElementModel(new EppRegistryElementSelectModel<EppRegistryElement>(EppRegistryElement.class)
                {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                DQLSelectBuilder s = DAO.this.getDocBaseDQL(model);
                s.column(property(SessionRetakeDocument.registryElementPart().registryElement().id().fromAlias("doc")));

                DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppRegistryElement.class, alias);
                dql.where(in(property(alias, "id"), s.buildQuery()));

                if (null != filter)
                {
                    dql.where(this.getFilterCondition(alias, filter));
                }

                dql.order(property(EppRegistryElementGen.title().fromAlias(alias)));
                dql.order(property(EppRegistryElementGen.owner().title().fromAlias(alias)));
                return dql;
            }
                });

        model.setControlActionTypeModel(new LazySimpleSelectModel<>(this.getCatalogItemList(EppFControlActionType.class)));

        model.setPpsModel(new PpsEntryModel()
        {
            @Override
            protected DQLSelectBuilder filter(String alias, DQLSelectBuilder ppsDQL)
            {
                DQLSelectBuilder commDQL = DAO.this.getDocBaseDQL(model);
                commDQL.column(property(SessionRetakeDocument.commission().id().fromAlias("doc")));

                DQLSelectBuilder relDQl = new DQLSelectBuilder()
                .fromEntity(SessionComissionPps.class, "rel")
                .where(in(property(SessionComissionPps.commission().id().fromAlias("rel")), commDQL.buildQuery()))
                .column(property(SessionComissionPps.pps().id().fromAlias("rel")));

                return ppsDQL.where(in(property(PpsEntry.id().fromAlias(alias)), relDQl.buildQuery()));
            }
        });

        model.setGroupModel(new DQLFullCheckSelectModel()
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                DQLSelectBuilder docDQL = DAO.this.getDocBaseDQL(model);
                docDQL.column(property(SessionRetakeDocument.id().fromAlias("doc")));

                DQLSelectBuilder slotDQl = new DQLSelectBuilder()
                .fromEntity(SessionDocumentSlot.class, "slot")
                .where(in(property(SessionDocumentSlot.document().id().fromAlias("slot")), docDQL.buildQuery()))
                .column(property(SessionDocumentSlot.actualStudent().group().id().fromAlias("slot")));

                return new DQLSelectBuilder()
                .fromEntity(Group.class, alias)
                .where(in(property(alias + ".id"), slotDQl.buildQuery()))
                .order(property(Group.title().fromAlias(alias)));
            }
        });
    }

    @Override
    @SuppressWarnings("unchecked")
    public void refreshDataSource(Model model)
    {
        DynamicListDataSource<SessionRetakeDocument> dataSource = model.getDataSource();

        DQLSelectBuilder docDQL = new DQLSelectBuilder().fromEntity(SessionRetakeDocument.class, "doc");
        if (model.getSessionObject() != null)
            docDQL.where(eq(property(SessionRetakeDocument.sessionObject().fromAlias("doc")), value(model.getSessionObject())));
        else
            docDQL.where(isNull(property(SessionRetakeDocument.sessionObject().fromAlias("doc"))));
        docDQL.where(in(property(SessionRetakeDocument.id().fromAlias("doc")), new DQLSelectBuilder().fromEntity(SessionRetakeDocument.class, "retake")
                .column(property(SessionRetakeDocument.id().fromAlias("retake")))
                .joinEntity("retake", DQLJoinType.inner, SessionComissionPps.class, "retakePps", eq(property(SessionComissionPps.commission().fromAlias("retakePps")), property(SessionRetakeDocument.commission().fromAlias("retake"))))
                .where(eq(property(SessionComissionPps.pps().person().fromAlias("retakePps")), value(model.getPerson())))
                .buildQuery()
        ));

        docDQL.joinEntity("doc", DQLJoinType.inner, SessionDocumentSlot.class, "s", eq(
                property(SessionDocumentSlot.document().fromAlias("s")), property("doc")
        ));
        docDQL.joinPath(DQLJoinType.inner, SessionDocumentSlot.actualStudent().fromAlias("s"), "st");
        docDQL.joinEntity("s", model.getGroups() != null ? DQLJoinType.inner : DQLJoinType.left, Group.class, "grp", eq(
                property(Student.group().fromAlias("st")), property("grp")
        ));

        // фильтры
        if (model.getRegistryElements() != null)
        {
            docDQL.where(in(property(SessionRetakeDocument.registryElementPart().registryElement().fromAlias("doc")), model.getRegistryElements()));
        }
        if (model.getControlActionTypes() != null)
        {
            docDQL.where(in(property(SessionDocumentSlot.studentWpeCAction().type().fromAlias("s")), CommonBaseEntityUtil.getPropertiesList(model.getControlActionTypes(), EppFControlActionType.L_EPP_GROUP_TYPE)));
        }
        if (model.getGroups() != null)
        {
            docDQL.where(in(property("grp"), model.getGroups()));
        }
        if (model.getPpsList() != null)
        {
            docDQL.joinEntity("doc", DQLJoinType.inner, SessionComissionPps.class, "pps", eq(
                    property(SessionComissionPps.commission().fromAlias("pps")), property(SessionRetakeDocument.commission().fromAlias("doc"))
            ));
            docDQL.where(in(property(SessionComissionPps.pps().fromAlias("pps")), model.getPpsList()));
        }
        if (model.getNumber() != null)
        {
            docDQL.where(eq(property(SessionRetakeDocument.number().fromAlias("doc")), value(model.getNumber())));
        }

        Number stCountFrom = model.getSettings().get("stCountFrom");
        Number stCountTo = model.getSettings().get("stCountTo");

        if (null != stCountFrom)
        {
            docDQL.where(ge(
                    new DQLSelectBuilder()
                    .fromEntity(SessionDocumentSlot.class, "slot_count")
                    .where(eq(property(SessionDocumentSlot.document().id().fromAlias("slot_count")), property(SessionRetakeDocument.id().fromAlias("doc"))))
                    .column("count(slot_count.id)")
                    .buildQuery(),
                    commonValue(stCountFrom)));
        }
        if (null != stCountTo)
        {
            docDQL.where(le(
                    new DQLSelectBuilder()
                    .fromEntity(SessionDocumentSlot.class, "slot_count")
                    .where(eq(property(SessionDocumentSlot.document().id().fromAlias("slot_count")), property(SessionRetakeDocument.id().fromAlias("doc"))))
                    .column("count(slot_count.id)")
                    .buildQuery(),
                    commonValue(stCountTo)));
        }

        // сортировки
        EntityOrder entityOrder = dataSource.getEntityOrder();
        if ("titleWithCA".equals(entityOrder.getKey()))
        {
            docDQL.column(property("doc.id"));
            docDQL.column(property("doc.number"));
            docDQL.predicate(DQLPredicateType.distinct);
            List rows = docDQL.createStatement(this.getSession()).list();
            Collections.sort(rows, new Comparator<Object[]>() {
                @Override public int compare(Object[] o1, Object[] o2) {
                    return NumberAsStringComparator.INSTANCE.compare((String) o1[1], (String) o2[1]);
                }
            });
            CollectionUtils.transform(rows, input -> ((Object[]) input)[0]);
            createPage(dataSource, rows);
        } else if ("groups".equals(entityOrder.getKey()))
        {
            docDQL.column(property("doc.id"));
            docDQL.column(property("grp.title"));
            docDQL.predicate(DQLPredicateType.distinct);
            List<Long> sortedIds = getSortedIds(docDQL.createStatement(this.getSession()).<Object[]>list(), 1);
            createPage(dataSource, sortedIds);
        } else
        {
            DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(SessionRetakeDocument.class, "rd")
            .where(in("rd.id", docDQL.column("doc.id").buildQuery()));
            new DQLOrderDescriptionRegistry(SessionRetakeDocument.class, "rd").applyOrder(dql, entityOrder);
            UniBaseUtils.createPage(dataSource, dql, getSession());
        }

        // view properties
        for (List<ViewWrapper<SessionRetakeDocument>> documentList : Lists.partition(ViewWrapper.<SessionRetakeDocument>getPatchedList(dataSource), DQL.MAX_VALUES_ROW_NUMBER)) {

            Collection<Long> documentIds = UniBaseUtils.getIdList(documentList);
            Map<Long, Set<String>> groupMap = new HashMap<>();
            {
                DQLSelectBuilder dql = new DQLSelectBuilder();
                dql.fromEntity(SessionDocumentSlot.class, "slot");
                dql.column(DQLExpressions.property(SessionDocumentSlot.document().id().fromAlias("slot")));
                dql.column(DQLExpressions.property(SessionDocumentSlot.actualStudent().group().title().fromAlias("slot")));
                dql.where(DQLExpressions.in(property(SessionDocumentSlot.document().id().fromAlias("slot")), documentIds));
                dql.order(DQLExpressions.property(SessionDocumentSlot.actualStudent().group().title().fromAlias("slot")));

                for (Object[] row : UniBaseDao.scrollRows(dql.createStatement(DAO.this.getSession())))
                {
                    SafeMap.safeGet(groupMap, (Long) row[0], LinkedHashSet.class).add((String) row[1]);
                }
            }

            Map<Long, List<String>> comissionMap = new HashMap<>(documentIds.size());
            {
                DQLSelectBuilder dql = new DQLSelectBuilder();
                dql.fromEntity(SessionComissionPps.class, "pps");
                dql.joinEntity("pps", DQLJoinType.inner, SessionRetakeDocument.class, "doc", DQLExpressions.eq(
                        DQLExpressions.property(SessionComissionPps.commission().id().fromAlias("pps")),
                        DQLExpressions.property(SessionRetakeDocument.commission().id().fromAlias("doc"))
                ));
                dql.column(DQLExpressions.property(SessionRetakeDocument.id().fromAlias("doc")));
                dql.column(DQLExpressions.property(SessionComissionPps.pps().person().identityCard().fromAlias("pps")));
                dql.where(DQLExpressions.in(DQLExpressions.property(SessionRetakeDocument.id().fromAlias("doc")), documentIds));
                dql.order(DQLExpressions.property(SessionComissionPps.pps().person().identityCard().fullFio().fromAlias("pps")));

                for (Object[] row : UniBaseDao.scrollRows(dql.createStatement(DAO.this.getSession())))
                {
                    SafeMap.safeGet(comissionMap, (Long) row[0], ArrayList.class).add(((IdentityCard) row[1]).getFio());
                }
            }

            Map<Long, Number> studentCountMap = new HashMap<>();
            {
                DQLSelectBuilder dql = new DQLSelectBuilder();
                dql.fromEntity(SessionDocumentSlot.class, "slot");
                dql.where(DQLExpressions.in(property(SessionDocumentSlot.document().id().fromAlias("slot")), documentIds));
                dql.group(property(SessionDocumentSlot.document().id().fromAlias("slot")));
                dql.column(property(SessionDocumentSlot.document().id().fromAlias("slot")));
                dql.column(count(SessionDocumentSlot.id().fromAlias("slot")));

                for (Object[] row : UniBaseDao.scrollRows(dql.createStatement(DAO.this.getSession())))
                {
                    studentCountMap.put((Long) row[0], (Number) row[1]);
                }
            }

            Map<Long, Set<String>> caTitleMap = new HashMap<>();
            {
                DQLSelectBuilder dql = new DQLSelectBuilder();
                dql.fromEntity(SessionDocumentSlot.class, "slot");
                dql.where(DQLExpressions.in(property(SessionDocumentSlot.document().id().fromAlias("slot")), documentIds));
                dql.joinPath(DQLJoinType.inner, SessionDocumentSlot.studentWpeCAction().fromAlias("slot"), "wpca");
                dql.joinEntity("wpca", DQLJoinType.inner, EppFControlActionType.class, "ca", eq(property("wpca", EppStudentWpeCAction.type()), property("ca", EppFControlActionType.eppGroupType())));
                dql.column(property(SessionDocumentSlot.document().id().fromAlias("slot")));
                dql.column(property(EppFControlActionType.title().fromAlias("ca")));
                dql.order(property(EppFControlActionType.title().fromAlias("ca")));

                for (Object[] row : UniBaseDao.scrollRows(dql.createStatement(DAO.this.getSession())))
                {
                    SafeMap.safeGet(caTitleMap, (Long) row[0], LinkedHashSet.class).add(StringUtils.uncapitalize((String) row[1]));
                }
            }

            for (ViewWrapper<SessionRetakeDocument> bulletin : documentList)
            {
                Long id = bulletin.getId();
                bulletin.setViewProperty("studentCount", studentCountMap.get(id));
                bulletin.setViewProperty("groups", StringUtils.join(groupMap.get(id), '\n'));
                bulletin.setViewProperty("comission", StringUtils.join(comissionMap.get(id), '\n'));
                bulletin.setViewProperty("titleWithCA", bulletin.getEntity().getTitle() + ", " + StringUtils.join(caTitleMap.get(id), '\n'));
            }
        }
    }

    @SuppressWarnings("unchecked")
    private void createPage(DynamicListDataSource<SessionRetakeDocument> dataSource, List rows)
    {
        if (dataSource.getEntityOrder().getDirection() == OrderDirection.desc)
        {
            Collections.reverse(rows);
        }

        dataSource.setTotalSize(rows.size());
        int startRow = (int) dataSource.getStartRow();
        int countRow = (int) dataSource.getCountRow();
        rows = rows.subList(startRow, Math.min(startRow + countRow, rows.size()));
        dataSource.createPage(sort(this.getList(SessionRetakeDocument.class, "id", rows), rows));
    }

    private DQLSelectBuilder getDocBaseDQL(Model model)
    {
        DQLSelectBuilder rootDql = new DQLSelectBuilder().fromEntity(SessionRetakeDocument.class, "doc");
        if (model.getSessionObject() != null)
        {
            rootDql.where(eq(property(SessionRetakeDocument.sessionObject().fromAlias("doc")), value(model.getSessionObject())));
        }
        return rootDql;
    }
}
