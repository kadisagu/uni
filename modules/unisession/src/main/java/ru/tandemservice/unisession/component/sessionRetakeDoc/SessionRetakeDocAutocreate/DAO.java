/* $Id$ */
package ru.tandemservice.unisession.component.sessionRetakeDoc.SessionRetakeDocAutocreate;

import com.google.common.collect.ImmutableList;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unisession.dao.retakeDoc.ISessionRetakeDocDAO;
import ru.tandemservice.unisession.entity.document.SessionObject;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 6/14/11
 */
public class DAO extends UniBaseDao implements IDAO
{
    private static final long GROUP_BY_GROUP_ID = 1L;
    private static final long GROUP_BY_EPP_GROUP_ID = 2L;

    @Override
    public void prepare(final Model model)
    {
        model.getOrgUnitHolder().refresh();
        model.setSessionObjectModel(new DQLFullCheckSelectModel()
        {
            @Override
            protected DQLSelectBuilder query(final String alias, final String filter)
            {
                final DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(SessionObject.class, alias)
                .where(eq(property(SessionObject.orgUnit().fromAlias(alias)), value(model.getOrgUnit())))
                ;
                FilterUtils.applyLikeFilter(
                        dql, filter,
                        SessionObject.educationYear().title().fromAlias(alias),
                        SessionObject.yearDistributionPart().title().fromAlias(alias)
                );
                return dql;
            }
        });
        model.setGroupSplitModel(new LazySimpleSelectModel<>(ImmutableList.of(
                new IdentifiableWrapper(0L, "нет"),
                new IdentifiableWrapper(DAO.GROUP_BY_GROUP_ID, "по академ. группе"),
                new IdentifiableWrapper(DAO.GROUP_BY_EPP_GROUP_ID, "по учебной группе"))));

        if (null != model.getSessionObject() && null != model.getSessionObject().getId()) {
            model.setSessionObject(this.get(SessionObject.class, model.getSessionObject().getId()));
        } else {
            model.setSessionObject(null);
        }

        // значения по умолчанию
        if (!model.isInitialized())
        {
            model.setGroupByCommission(true);
            model.setGroupSplit(new IdentifiableWrapper(DAO.GROUP_BY_EPP_GROUP_ID, "по учебной группе"));
            model.setInitialized(true);
        }
    }

    @Override
    public void doCreateDocuments(final Model model)
    {
        final ISessionRetakeDocDAO.GroupingSettings groupingSettings = new ISessionRetakeDocDAO.GroupingSettings()
        {
            @Override
            public GroupSplitOption getGroupSplitOption()
            {
                if (model.getGroupSplit() != null && DAO.GROUP_BY_GROUP_ID == model.getGroupSplit().getId()) {
                    return ISessionRetakeDocDAO.GroupingSettings.GroupSplitOption.SPLIT_BY_GROUP;
                }
                if (model.getGroupSplit() != null && DAO.GROUP_BY_EPP_GROUP_ID == model.getGroupSplit().getId()) {
                    return ISessionRetakeDocDAO.GroupingSettings.GroupSplitOption.SPLIT_BY_EPP_GROUP;
                }
                return ISessionRetakeDocDAO.GroupingSettings.GroupSplitOption.DO_NOT_SPLIT;
            }

            @Override
            public boolean isGroupByCommission()
            {
                return model.isGroupByCommission();
            }

            @Override
            public boolean isGroupByCompensationType()
            {
                return model.isGroupByCompensationType();
            }
        };

        ISessionRetakeDocDAO.instance.get().doAutocreateDocuments(model.getSessionObject(), groupingSettings, model.getMarkList());
    }
}
