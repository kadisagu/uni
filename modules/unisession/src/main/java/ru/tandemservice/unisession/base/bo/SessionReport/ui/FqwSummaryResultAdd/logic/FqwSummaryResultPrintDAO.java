/* $Id: $ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.FqwSummaryResultAdd.logic;

import jxl.CellView;
import jxl.write.*;
import jxl.write.Number;
import jxl.write.biff.RowsExceededException;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniepp.entity.catalog.codes.EppRegistryStructureCodes;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.unisession.base.bo.SessionReport.logic.StateFinalAttestationUtil;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.SfaSummaryResultAdd.logic.SfaSumReportInfo;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.SfaSummaryResultAdd.logic.SfaSummaryResultPrintDAO;
import ru.tandemservice.unisession.entity.catalog.SessionRecommendation4FQW;
import ru.tandemservice.unisession.entity.catalog.SessionSource4FQWTheme;
import ru.tandemservice.unisession.entity.report.SfaSummaryResult;
import ru.tandemservice.unisession.entity.stateFinalExam.SessionFQWProtocol;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static ru.tandemservice.unisession.base.bo.SessionReport.logic.StateFinalAttestationUtil.*;

/**
 * @author Andrey Andreev
 * @since 02.11.2016
 */
public class FqwSummaryResultPrintDAO extends SfaSummaryResultPrintDAO<Raw, Row>
{
    @Override
    public SfaSumReportInfo<Raw> initReportInfo(SfaSummaryResult report)
    {
        ReportInfo reportInfo = new ReportInfo(report);

        DQLSelectBuilder eouBuilder = getEducationOrgUnitSelectBuilder(reportInfo);

        DQLSelectBuilder slotBuilder = getSlotSelectBuilder(reportInfo, eouBuilder)
                .where(eq(property(REG_ALIAS, EppRegistryElement.parent().code()), value(EppRegistryStructureCodes.REGISTRY_ATTESTATION_DIPLOMA)));

        slotBuilder.joinEntity(SLOT_ALIAS, DQLJoinType.left, SessionFQWProtocol.class, "ptl",
                               eq(property("ptl", SessionFQWProtocol.documentSlot()), property(SLOT_ALIAS)));

        slotBuilder.column(property(EOU_ALIAS))
                .column(property(WPE_ALIAS))
                .column(property(MARK_ALIAS))
                .column(property("ptl"));

        List<Object[]> slots = getList(slotBuilder);
        if (slots.isEmpty() && report.isSkipEmpty())
            throw new ApplicationException("Нет данных для построения отчета. Нет студентов с выбранными параметрами.");

        List<EducationOrgUnit> educationOrgUnits = getList(eouBuilder);
        if (educationOrgUnits.isEmpty())
            throw new ApplicationException("Нет данных для построения отчета. Нет направлений с выбранными параметрами.");


        reportInfo.setSource4FQWThemeList(getCatalogItemList(SessionSource4FQWTheme.class));
        reportInfo.setRecommendation4FQWList(getCatalogItemList(SessionRecommendation4FQW.class));
        reportInfo.setEducationOrgUnits(educationOrgUnits);
        reportInfo.setRaws(StateFinalAttestationUtil.filterRaws(slots.stream().map(Raw::new)));

        return reportInfo;
    }

    @Override
    protected String getFileName(SfaSummaryResult report)
    {
        return "Сводный отчет по результатам защит ВКР.xls";
    }

    @Override
    protected int printTitle(SfaSummaryResult report, WritableSheet sheet)
    {
        int row = 0;

        addTextCell(sheet, 0, row, 10, 1, "Сводный отчет по результатам защит ВКР на " + report.getEducationYear().getTitle() + " уч. год", _titleStyle);

        return ++row;
    }

    @Override
    protected int printParametersTable(SfaSumReportInfo<Raw> absReportInfo, WritableSheet sheet, int row) throws WriteException
    {
        SfaSummaryResult report = absReportInfo.getReport();

        if (report.getDevelopForm() != null)
            row = addProperty2HeadTable(sheet, row, "Форма освоения", report.getDevelopForm().getTitle(), _parametersTS);

        if (report.getDevelopTech() != null)
            row = addProperty2HeadTable(sheet, row, "Технология освоения", report.getDevelopTech().getTitle(), _parametersTS);

        if (report.getQualification() != null)
            row = addProperty2HeadTable(sheet, row, "Уровень образования", report.getQualification().getTitle(), _parametersTS);

        if (report.getFormativeOrgUnit() != null)
            row = addProperty2HeadTable(sheet, row, "Формирующее подразделение", StringUtils.capitalize(report.getFormativeOrgUnit().getPrintTitle()), _parametersTS);

        if (report.getTerritorialOrgUnit() != null)
            row = addProperty2HeadTable(sheet, row, "Территориальное подразделение", StringUtils.capitalize(report.getTerritorialOrgUnit().getPrintTitle()), _parametersTS);

        if (report.getOwnerOrgUnit() != null)
            row = addProperty2HeadTable(sheet, row, "Выпускающее подразделение", StringUtils.capitalize(report.getOwnerOrgUnit().getPrintTitle()), _parametersTS);

        EducationLevelsHighSchool educationLevelHighSchool = report.getEducationLevelHighSchool();
        if (educationLevelHighSchool != null)
        {
            String programSubjectKindTitle = educationLevelHighSchool.getEducationLevel().getEduProgramSubject().getEduProgramKind().getEduProgramSubjectKindTitle(InflectorVariantCodes.RU_GENITIVE);
            row = addProperty2HeadTable(sheet, row, "Шифр и наименование " + programSubjectKindTitle.toLowerCase(), educationLevelHighSchool.getDisplayableTitle(), _parametersTS);
        }

        if (report.getDevelopCondition() != null)
            row = addProperty2HeadTable(sheet, row, "Условие освоения", report.getDevelopCondition().getTitle(), _parametersTS);

        if (report.getDevelopPeriod() != null)
            row = addProperty2HeadTable(sheet, row, "Нормативный срок освоения", report.getDevelopPeriod().getTitle(), _parametersTS);

        String examsPeriod = report.getExamsPeriod();
        if (!examsPeriod.isEmpty())
            row = addProperty2HeadTable(sheet, row, "Период защит ВКР", examsPeriod, _parametersTS);

        return row;
    }


    @Override
    protected int addHeaderTable(SfaSumReportInfo<Raw> absReportInfo, WritableSheet sheet, int rowIndex)
    {
        ReportInfo info = (ReportInfo) absReportInfo;

        int firstRow = rowIndex;
        int col = 0;

        try
        {
            sheet.setRowView(rowIndex, 700);
            sheet.setRowView(rowIndex + 1, 2000);

            sheet.setColumnView(col, 60);
            addTextCell(sheet, col++, rowIndex, 1, 2, "Структурное подразделение", _horHeaderDataTS);

            addTextCell(sheet, col, rowIndex, 4, 1, "Количество выпускников", _horHeaderDataTS);
            sheet.addCell(new Label(col++, rowIndex + 1, "Всего", _verHeaderDataTS));
            sheet.addCell(new Label(col++, rowIndex + 1, "Недопущены", _verHeaderDataTS));
            sheet.addCell(new Label(col++, rowIndex + 1, "Явились на защиту", _verHeaderDataTS));
            sheet.addCell(new Label(col++, rowIndex + 1, "Защитили", _verHeaderDataTS));

            addTextCell(sheet, col, rowIndex, 4, 1, "Из них получили оценки", _horHeaderDataTS);
            sheet.addCell(new Number(col++, rowIndex + 1, 5, _horHeaderDataTS));
            sheet.addCell(new Number(col++, rowIndex + 1, 4, _horHeaderDataTS));
            sheet.addCell(new Number(col++, rowIndex + 1, 3, _horHeaderDataTS));
            sheet.addCell(new Number(col++, rowIndex + 1, 2, _horHeaderDataTS));

            addTextCell(sheet, col++, rowIndex, 1, 2, "Успеваемость, %", _verHeaderDataTS);
            addTextCell(sheet, col++, rowIndex, 1, 2, "Качественный \r\nпоказатель, %", _verHeaderDataTS);
            addTextCell(sheet, col++, rowIndex, 1, 2, "Количество дипломов \r\nс отличием", _verHeaderDataTS);
            addTextCell(sheet, col++, rowIndex, 1, 2, "Качественный показатель (дипл. с отл.)", _verHeaderDataTS);

            List<SessionSource4FQWTheme> source4FQWThemeList = info.getSource4FQWThemeList();
            sheet.mergeCells(col, rowIndex, col + source4FQWThemeList.size() - 1, rowIndex);
            sheet.addCell(new Label(col, rowIndex, "Количество ВКР выполненных", _horHeaderDataTS));
            for (SessionSource4FQWTheme theme : source4FQWThemeList)
                sheet.addCell(new Label(col++, rowIndex + 1, theme.getTitle(), _verHeaderDataTS));

            List<SessionRecommendation4FQW> recommendation4FQWList = info.getRecommendation4FQWList();
            sheet.mergeCells(col, rowIndex, col + recommendation4FQWList.size() - 1, rowIndex);
            sheet.addCell(new Label(col, rowIndex, "Количество ВКР рекомендованных", _horHeaderDataTS));
            for (SessionRecommendation4FQW recommendation : recommendation4FQWList)
                sheet.addCell(new Label(col++, rowIndex + 1, recommendation.getTitle(), _verHeaderDataTS));


            tableWidth = col - 1;

            rowIndex = rowIndex + 2;
            for (; col > 0; col--)// номера колонок
                sheet.addCell(new Number(col - 1, rowIndex, col, _horHeaderBoldDataTS));

            fillArea(sheet, 0, tableWidth, firstRow - 1, firstRow - 1, _topThickBoardTS);
            fillArea(sheet, tableWidth + 1, tableWidth + 1, firstRow, rowIndex, _rightThickTableBoardTS);
        }
        catch (WriteException e)
        {
            e.printStackTrace();
        }

        return ++rowIndex;
    }

    @Override
    protected int addHeaderTableDetail(SfaSumReportInfo<Raw> absReportInfo, WritableSheet sheet, int rowIndex)
    {
        ReportInfo info = (ReportInfo) absReportInfo;

        int firstRow = rowIndex;
        int col = 0;

        try
        {
            sheet.setRowView(rowIndex, 700);
            sheet.setRowView(rowIndex + 1, 2000);

            addTextCell(sheet, col++, rowIndex, 1, 2, "Поколение перечня", _verHeaderDataTS);
            sheet.setColumnView(col, tableWidth);
            addTextCell(sheet, col++, rowIndex, 1, 2, "Код", _horHeaderDataTS);
            CellView columnView = sheet.getColumnView(col);
            columnView.setAutosize(true);
            sheet.setColumnView(col, columnView);
            addTextCell(sheet, col++, rowIndex, 1, 2, "Специальность/направление", _horHeaderDataTS);
            sheet.setColumnView(col, tableWidth);
            addTextCell(sheet, col++, rowIndex, 1, 2, "Дата проведения", _verHeaderDataTS);

            addTextCell(sheet, col, rowIndex, 4, 1, "Количество выпускников", _horHeaderDataTS);
            sheet.addCell(new Label(col++, rowIndex + 1, "Всего", _verHeaderDataTS));
            sheet.addCell(new Label(col++, rowIndex + 1, "Недопущены", _verHeaderDataTS));
            sheet.addCell(new Label(col++, rowIndex + 1, "Явились на защиту", _verHeaderDataTS));
            sheet.addCell(new Label(col++, rowIndex + 1, "Защитили", _verHeaderDataTS));

            addTextCell(sheet, col, rowIndex, 4, 1, "Из них получили оценки", _horHeaderDataTS);
            sheet.addCell(new Number(col++, rowIndex + 1, 5, _horHeaderDataTS));
            sheet.addCell(new Number(col++, rowIndex + 1, 4, _horHeaderDataTS));
            sheet.addCell(new Number(col++, rowIndex + 1, 3, _horHeaderDataTS));
            sheet.addCell(new Number(col++, rowIndex + 1, 2, _horHeaderDataTS));

            addTextCell(sheet, col++, rowIndex, 1, 2, "Успеваемость, %", _verHeaderDataTS);
            addTextCell(sheet, col++, rowIndex, 1, 2, "Качественный \r\nпоказатель, %", _verHeaderDataTS);
            addTextCell(sheet, col++, rowIndex, 1, 2, "Количество дипломов \r\nс отличием", _verHeaderDataTS);
            addTextCell(sheet, col++, rowIndex, 1, 2, "Качественный показатель (дипл. с отл.)", _verHeaderDataTS);

            List<SessionSource4FQWTheme> source4FQWThemeList = info.getSource4FQWThemeList();
            sheet.mergeCells(col, rowIndex, col + source4FQWThemeList.size() - 1, rowIndex);
            sheet.addCell(new Label(col, rowIndex, "Количество ВКР выполненных", _horHeaderDataTS));
            for (SessionSource4FQWTheme theme : source4FQWThemeList)
                sheet.addCell(new Label(col++, rowIndex + 1, theme.getTitle(), _verHeaderDataTS));

            List<SessionRecommendation4FQW> recommendation4FQWList = info.getRecommendation4FQWList();
            sheet.mergeCells(col, rowIndex, col + recommendation4FQWList.size() - 1, rowIndex);
            sheet.addCell(new Label(col, rowIndex, "Количество ВКР рекомендованных", _horHeaderDataTS));
            for (SessionRecommendation4FQW recommendation : recommendation4FQWList)
                sheet.addCell(new Label(col++, rowIndex + 1, recommendation.getTitle(), _verHeaderDataTS));

            tableWidth = col - 1;

            rowIndex = rowIndex + 2;
            for (; col > 0; col--)
                sheet.addCell(new Number(col - 1, rowIndex, col, _horHeaderBoldDataTS));

            fillArea(sheet, 0, tableWidth, firstRow - 1, firstRow - 1, _topThickBoardTS);
            fillArea(sheet, tableWidth + 1, tableWidth + 1, rowIndex - 2, rowIndex, _rightThickTableBoardTS);
        }
        catch (WriteException e)
        {
            e.printStackTrace();
        }

        return ++rowIndex;
    }


    @Override
    public int addDetailRegElRow2Table(Row row, WritableSheet sheet, int rowIndex)
    {
        int col = 3;

        try
        {
            sheet.addCell(new Label(col++, rowIndex, row.getDates(), row.getIntStyle()));
        }
        catch (WriteException e)
        {
            e.printStackTrace();
        }

        addDataRow(sheet, col, rowIndex, row);

        return ++rowIndex;
    }

    @Override
    public int addDetailTotalRow2Table(Row row, WritableSheet sheet, int rowIndex)
    {
        try
        {
            sheet.setRowView(rowIndex, 350);
        }
        catch (RowsExceededException e)
        {
            e.printStackTrace();
        }

        int col = 4;

        StateFinalAttestationUtil.addTextCell(sheet, 1, rowIndex, 3, 1, row.getTitle(), row.getTitleStyle());

        addDataRow(sheet, col, rowIndex, row);

        return ++rowIndex;
    }

    @Override
    public void addDataRow(WritableSheet sheet, int startCol, int rowIndex, Row row)
    {
        addIntCell(sheet, startCol++, rowIndex, row.getTotal(), row.getIntStyle());
        addIntCell(sheet, startCol++, rowIndex, row.getNotAllowed(), row.getIntStyle());
        addIntCell(sheet, startCol++, rowIndex, row.getAppeared(), row.getIntStyle());
        addIntCell(sheet, startCol++, rowIndex, row.getPassed(), row.getIntStyle());
        addIntCell(sheet, startCol++, rowIndex, row.getMark5(), row.getIntStyle());
        addIntCell(sheet, startCol++, rowIndex, row.getMark4(), row.getIntStyle());
        addIntCell(sheet, startCol++, rowIndex, row.getMark3(), row.getIntStyle());
        addIntCell(sheet, startCol++, rowIndex, row.getMark2(), row.getRedIntStyle());

        addDoubleCell(sheet, startCol++, rowIndex, row.getProgress(), row.getDoubleStyle());
        addDoubleCell(sheet, startCol++, rowIndex, row.getGoodProgress(), row.getDoubleStyle());

        addIntCell(sheet, startCol++, rowIndex, row.getHonors(), row.getIntStyle());
        addDoubleCell(sheet, startCol++, rowIndex, row.getHonorsProgress(), row.getDoubleStyle());

        for (int[] value : row.getThemesNumbers().values())
            addIntCell(sheet, startCol++, rowIndex, value[0], row.getIntStyle());

        for (int[] value : row.getRecommendationsNumbers().values())
            addIntCell(sheet, startCol++, rowIndex, value[0], row.getIntStyle());
    }

    @Override
    protected List<Row> getRegistryElementRows(SfaSumReportInfo<Raw> absReportInfo, List<Raw> raws)
    {
        Row row = newRegElRow(absReportInfo, raws.get(0).getEducationOrgUnit(), null);
        row.addFromRaw(raws);

        return Collections.singletonList(row);
    }


    // Не Детализованные строки
    @Override
    protected Row newFouRow(SfaSumReportInfo<Raw> absReportInfo, String title, boolean first)
    {
        ReportInfo reportInfo = (ReportInfo) absReportInfo;
        return new Row(null, title,
                       first ? _fouTitleTS : _fouSubTitleTS,
                       first ? _fouIntTS : _fouSubIntTS,
                       first ? _fouRedIntTS : _fouSubRedIntTS,
                       first ? _fouDoubleTS : _fouSubDoubleTS)
                .initProtocolsMap(reportInfo.getSource4FQWThemeList(), reportInfo.getRecommendation4FQWList());
    }

    @Override
    protected Row newFinalRow(SfaSumReportInfo<Raw> absReportInfo, String title, boolean first)
    {
        ReportInfo reportInfo = (ReportInfo) absReportInfo;
        return new Row(null, title,
                       first ? _totalTitleTS : _totalSubTitleTS,
                       first ? _totalIntTS : _totalSubIntTS,
                       first ? _totalRedIntTS : _totalSubRedIntTS,
                       first ? _totalDoubleTS : _totalSubDoubleTS)
                .initProtocolsMap(reportInfo.getSource4FQWThemeList(), reportInfo.getRecommendation4FQWList());
    }


    // Детализованные строки
    @Override
    protected Row newRegElRow(SfaSumReportInfo<Raw> absReportInfo, EducationOrgUnit educationOrgUnit, String title)
    {
        ReportInfo reportInfo = (ReportInfo) absReportInfo;
        return new Row(educationOrgUnit, title, _regElTitleDetailTS, _regElDetailIntTS, _regElDetailRedIntTS, _regElDetailDoubleTS)
                .initProtocolsMap(reportInfo.getSource4FQWThemeList(), reportInfo.getRecommendation4FQWList());
    }

    @Override
    protected Row newTotalDirectionRow(SfaSumReportInfo<Raw> absReportInfo)
    {
        ReportInfo reportInfo = (ReportInfo) absReportInfo;
        return new Row(null, "Всего по направлению", _regElTitleDetailTS, _regElDetailIntTS, _regElDetailRedIntTS, _regElDetailDoubleTS)
                .initProtocolsMap(reportInfo.getSource4FQWThemeList(), reportInfo.getRecommendation4FQWList());
    }

    @Override
    protected Row newDetailTouRow(SfaSumReportInfo<Raw> absReportInfo, String title, boolean first)
    {
        ReportInfo reportInfo = (ReportInfo) absReportInfo;
        return new Row(null, title,
                       first ? _touTotalCellThickTopDtlTS : _touTotalCellDtlTS,
                       first ? _touTotalIntThickTopDtlTS : _touTotalIntDtlTS,
                       first ? _touTotalRedIntThickTopDtlTS : _touTotalRedIntDtlTS,
                       first ? _touTotalDoubleThickTopDtlTS : _touTotalDoubleDtlTS)
                .initProtocolsMap(reportInfo.getSource4FQWThemeList(), reportInfo.getRecommendation4FQWList());
    }

    @Override
    protected Row newDetailBranchesRow(SfaSumReportInfo<Raw> absReportInfo, String title, boolean first)
    {
        ReportInfo reportInfo = (ReportInfo) absReportInfo;
        return new Row(null, title,
                       first ? _tousTotalCellThickTopDtlTS : _tousTotalCellDtlTS,
                       first ? _tousTotalIntThickTopDtlTS : _tousTotalIntDtlTS,
                       first ? _tousTotalRedIntThickTopDtlTS : _tousTotalRedIntDtlTS,
                       first ? _tousTotalDoubleThickTopDtlTS : _tousTotalDoubleDtlTS)
                .initProtocolsMap(reportInfo.getSource4FQWThemeList(), reportInfo.getRecommendation4FQWList());
    }

    @Override
    protected Row newDetailFouRow(SfaSumReportInfo<Raw> absReportInfo, String title, boolean first)
    {
        ReportInfo reportInfo = (ReportInfo) absReportInfo;
        return new Row(null, title,
                       first ? _fouTotalCellThickTopDtlTS : _fouTotalCellDtlTS,
                       first ? _fouTotalIntThickTopDtlTS : _fouTotalIntDtlTS,
                       first ? _fouTotalRedIntThickTopDtlTS : _fouTotalRedIntDtlTS,
                       first ? _fouTotalDoubleThickTopDtlTS : _fouTotalDoubleDtlTS)
                .initProtocolsMap(reportInfo.getSource4FQWThemeList(), reportInfo.getRecommendation4FQWList());
    }

    @Override
    protected Row newDetailFinalRow(SfaSumReportInfo<Raw> absReportInfo, String title, boolean first)
    {
        ReportInfo reportInfo = (ReportInfo) absReportInfo;
        return new Row(null, title,
                       first ? _finalTotalCellThickTopDtlTS : _finalTotalCellDtlTS,
                       first ? _finalTotalIntThickTopDtlTS : _finalTotalIntDtlTS,
                       first ? _finalTotalRedIntThickTopDtlTS : _finalTotalRedIntDtlTS,
                       first ? _finalTotalDoubleThickTopDtlTS : _finalTotalDoubleDtlTS)
                .initProtocolsMap(reportInfo.getSource4FQWThemeList(), reportInfo.getRecommendation4FQWList());
    }

    @Override
    protected Row newDetailSvodRow(SfaSumReportInfo<Raw> absReportInfo, String title, boolean first)
    {
        ReportInfo reportInfo = (ReportInfo) absReportInfo;
        return new Row(null, title,
                       first ? _svodTotalCellThickTopDtlTS : _svodTotalCellDtlTS,
                       first ? _svodTotalIntThickTopDtlTS : _svodTotalIntDtlTS,
                       first ? _svodTotalRedIntThickTopDtlTS : _svodTotalRedIntDtlTS,
                       first ? _svodTotalDoubleThickTopDtlTS : _svodTotalDoubleDtlTS)
                .initProtocolsMap(reportInfo.getSource4FQWThemeList(), reportInfo.getRecommendation4FQWList());
    }
}
