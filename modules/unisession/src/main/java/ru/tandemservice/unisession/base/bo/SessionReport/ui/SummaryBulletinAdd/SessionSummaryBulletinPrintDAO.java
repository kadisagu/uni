/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.SummaryBulletinAdd;

import com.google.common.base.Strings;
import com.google.common.collect.*;
import jxl.format.Colour;
import jxl.format.RGB;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.collections15.ListUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableDouble;
import org.apache.commons.lang.mutable.MutableInt;
import org.tandemframework.core.bean.IFastBeanOwner;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.NumberAsStringComparator;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.cell.TextDirection;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.IRtfRowSplitInterceptor;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.ICommonFilterItem;
import org.tandemframework.shared.commonbase.base.util.DelegatePropertyComparator;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.commonbase.base.util.key.TripletKey;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.util.PersonSecurityUtil;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecializationRoot;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.catalog.EppWorkPlanRowKind;
import ru.tandemservice.uniepp.entity.catalog.codes.EppRegistryStructureCodes;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.base.bo.SessionReport.util.SessionReportUtil;
import ru.tandemservice.unisession.base.bo.SessionReport.util.UniSessionFilterAddon;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkStateCatalogItem;
import ru.tandemservice.unisession.entity.catalog.UnisessionTemplate;
import ru.tandemservice.unisession.entity.catalog.codes.UnisessionCommonTemplateCodes;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionStudentGradeBookDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.report.UnisessionSummaryBulletinReport;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Сводная ведомость (на академ. группу)
 * @author oleyba
 * @since 12/20/11
 */
public class SessionSummaryBulletinPrintDAO extends UniBaseDao implements ISessionSummaryBulletinPrintDAO
{
    private static DateFormatter DATE_FORMATTER_DD_MM_YYYY = new DateFormatter("dd.MM.yyyy");

	private static final String TotalByHighSchool = "Всего сдано";
	private static final String TotalByAllHighSchools = "Итоговая сумма";

    @Override
    public byte[] print(SessionReportSummaryBulletinAddUI model, Collection<ISessionSummaryBulletinData> data)
    {
        UnisessionTemplate templateItem = getCatalogItem(UnisessionTemplate.class, UnisessionCommonTemplateCodes.SUMMARY_BULLETIN);
        if (templateItem == null)
            throw new RuntimeException("Печатный шаблон есть, но не импортирован в справочник шаблонов.");
        RtfDocument template = new RtfReader().read(templateItem.getContent());

        RtfDocument result = null;

        for (ISessionSummaryBulletinData bulletin : data)
        {
            RtfDocument document = printBulletin(model, bulletin, template.getClone());
            if (null == result)
                result = document;
            else if (null != document)
            {
                RtfUtil.modifySourceList(result.getHeader(), document.getHeader(), document.getElementList());
                result.getElementList().addAll(document.getElementList());
            }
        }

        if (null != result)
            return RtfUtil.toByteArray(result);
        else
            throw new ApplicationException("Нет данных для построения отчета.");
    }

    @Override
    public UnisessionSummaryBulletinReport createStoredReport(SessionReportSummaryBulletinAddUI model)
    {
        final Collection<ISessionSummaryBulletinData> data = getSessionSummaryBulletinData(model);

        UnisessionSummaryBulletinReport report = new UnisessionSummaryBulletinReport();

        DatabaseFile content = new DatabaseFile();
        content.setContent(print(model, data));
        save(content);
        report.setContent(content);

        final UniSessionFilterAddon filterAddon = model.getSessionFilterAddon();

        report.setFormingDate(new Date());
        report.setExecutor(PersonSecurityUtil.getExecutor());
        report.setSessionObject(model.getSessionObject());
        report.setInSession(model.isInSessionMarksOnly() ? "по результатам в сессию" : "по итоговым результатам");


        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_DISC_KIND, UnisessionSummaryBulletinReport.P_DISC_KINDS, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_GROUP_WITH_NOGROUP, UnisessionSummaryBulletinReport.P_GROUPS, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_COURSE, UnisessionSummaryBulletinReport.P_COURSE, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_CONTROL_FORM, UnisessionSummaryBulletinReport.P_CONTROL_ACTION_TYPES, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_CUSTOM_STATE, UnisessionSummaryBulletinReport.P_CUSTOM_STATE, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_TARGET_ADMISSION, UnisessionSummaryBulletinReport.P_TARGET_ADMISSION, "title");
        report.setShowMarkPointsData(model.isShowMarkPointsData());
		report.setShowPreviousTermPractise(model.isShowPreviousTermPractise());

        save(report);
        return report;
    }

    @Override
    public Collection<ISessionSummaryBulletinData> getSessionSummaryBulletinData(final SessionReportSummaryBulletinAddUI model)
    {

        //final ICommonFilterItem groupFilterItem = model.getSessionFilterAddon().getFilterItem(UniSessionFilterAddon.SETTINGS_NAME_GROUP_WITH_NOGROUP);

        final Map<EppStudentWpeCAction, Set<SessionBulletinDocument>> cAction2bulletins = new HashMap<>();
        final Map<EppStudentWpeCAction, EppRealEduGroupRow> cAction2eduGroupRow = new HashMap<>();

        final DQLSelectBuilder wpcaDQL = getWpSlotDQL(model);
        for (Object[] row : wpcaDQL.createStatement(getSession()).<Object[]>list())
        {
            final EppStudentWpeCAction wpca = (EppStudentWpeCAction) row[0];
            SafeMap.safeGet(cAction2bulletins, wpca, HashSet.class).add((SessionBulletinDocument) row[1]);
            cAction2eduGroupRow.put(wpca, (EppRealEduGroupRow) row[2]);
        }

        final Map<EppStudentWpeCAction, SessionMark> cAction2mark = new HashMap<>();
        BatchUtils.execute(cAction2bulletins.keySet(), 256, elements -> {
			DQLSelectBuilder markDQL = new DQLSelectBuilder()
					.fromEntity(SessionMark.class, "m")
					.fromEntity(SessionStudentGradeBookDocument.class, "d")
					.where(eq(property(SessionMark.slot().document().fromAlias("m")), property("d")))
					.where(in(property(SessionMark.slot().studentWpeCAction().fromAlias("m")), elements))
					.where(eq(property(SessionMark.slot().inSession().fromAlias("m")), value(model.isInSessionMarksOnly())))
					.column(property(SessionMark.slot().studentWpeCAction().fromAlias("m")))
					.column("m");
			for (Object[] row : markDQL.createStatement(getSession()).<Object[]>list())
				cAction2mark.put((EppStudentWpeCAction) row[0], (SessionMark) row[1]);

		});

        Map<EppStudentWpeCAction, EppFControlActionType> cAction2ActionTypeMap = Maps.newHashMap();
        Map<Long, List<PairKey<EppRealEduGroupRow, SessionMark>>> studentAction2MarkMap = Maps.newHashMap();
        Map<String, ISessionSummaryBulletinData> studentGroup2summaryBulletinData = new TreeMap<>();

        for (EppStudentWpeCAction slot : cAction2bulletins.keySet())
        {
            if (slot.getStudentWpe().getSourceRow() == null)
                continue; // не включаем неактуальные МСРП, а для актуальных должна быть строка РУП

            EppRealEduGroupRow row = cAction2eduGroupRow.get(slot);
            SessionMark mark = cAction2mark.get(slot);

            final EppFControlActionType actionType = slot.getActionType();
            final Student st = row.getStudentWpePart().getStudentWpe().getStudent();
            cAction2ActionTypeMap.put(slot, actionType);

            SafeMap.safeGet(studentAction2MarkMap, st.getId(), ArrayList.class).add(PairKey.create(row, mark));
        }

        for (List<PairKey<EppRealEduGroupRow, SessionMark>> list : studentAction2MarkMap.values())
            Collections.sort(list, Comparator.comparing(PairKey::getSecond, Comparator.nullsLast(Comparator.comparing(SessionMark::getPerformDate))));

        for (EppStudentWpeCAction cAction : cAction2bulletins.keySet())
        {
            if (cAction.getStudentWpe().getSourceRow() == null)
                continue; // не включаем неактуальные МСРП, а для актуальных должна быть строка РУП

            final Set<SessionBulletinDocument> bulletins = cAction2bulletins.get(cAction);
            EppRealEduGroupRow row = cAction2eduGroupRow.get(cAction);
            SessionMark mark = cAction2mark.get(cAction);

            final Student st = row.getStudentWpePart().getStudentWpe().getStudent();
            final EppFControlActionType actionType = cAction2ActionTypeMap.get(cAction);
            final EppRegistryElementPart regElementPart = cAction.getStudentWpe().getRegistryElementPart();
            final MultiKey actionKey = SessionSummaryBulletinAction.key(actionType, regElementPart);

            String group = null == row.getStudentGroupTitle()? "" : row.getStudentGroupTitle();

            SessionSummaryBulletinData bulletinData = (SessionSummaryBulletinData) studentGroup2summaryBulletinData.get(group);
            if (null == bulletinData)
            {
                studentGroup2summaryBulletinData.put(group, bulletinData = new SessionSummaryBulletinData(group, model.isShowMarkPointsData()));
            }

            EducationOrgUnit studentEduOu = studentAction2MarkMap.get(st.getId()).get(0).getFirst().getStudentEducationOrgUnit();
            SessionSummaryBulletinStudent student = new SessionSummaryBulletinStudent(row, studentEduOu);
            bulletinData.students.add(student);

            SessionSummaryBulletinAction action = (SessionSummaryBulletinAction) bulletinData.actions.get(actionKey);
            if (null == action)
            {
                bulletinData.actions.put(actionKey, action = new SessionSummaryBulletinAction(actionType, regElementPart));
            }
            action.bulletins.addAll(bulletins);

            final MultiKey key = bulletinData.key(student, action);
            bulletinData.wpRowKindMap.put(key, cAction.getStudentWpe().getSourceRow().getKind());
            bulletinData.markMap.put(key, mark);
        }

	    if (model.isShowPreviousTermPractise())
	    {
		    YearAndPart prevYearAndPart = getPrevYearAndPart(model.getYear(), model.getPart());
		    for (ISessionSummaryBulletinData dataNotCasted : studentGroup2summaryBulletinData.values())
		    {
			    SessionSummaryBulletinData data = (SessionSummaryBulletinData) dataNotCasted;

			    Collection<EppStudentWpeCAction> prevTermPracticeCActions = getPrevTermPracticeCAction(data.getStudents(), prevYearAndPart.year, prevYearAndPart.part, model.getSessionFilterAddon());
			    data.addPrevTermPracticesToWorkPlan(prevTermPracticeCActions);

			    data.getPrevTermPractices().addAll(getPrevTermPracticeColumns(prevTermPracticeCActions));
			    data.addPrevTermPracticeMarks(getMarksForPrevTermPractices(prevTermPracticeCActions, model.isInSessionMarksOnly()));
		    }
	    }

        return studentGroup2summaryBulletinData.values();
    }

	private static class YearAndPart
	{
		public YearAndPart(EducationYear year, YearDistributionPart part)
		{
			this.year = year;
			this.part = part;
		}
		public final EducationYear year;
		public final YearDistributionPart part;
	}

	private YearAndPart getPrevYearAndPart(EducationYear currYear, YearDistributionPart currPart)
	{
		if (currPart.getNumber() > 1)
		{
			final String partAlias = "part";
			YearDistributionPart prevPart = new DQLSelectBuilder().fromEntity(YearDistributionPart.class, partAlias)
					.where(eq(property(partAlias, YearDistributionPart.number()), value(currPart.getNumber() - 1)))
					.where(eq(property(partAlias, YearDistributionPart.yearDistribution()), value(currPart.getYearDistribution())))
					.createStatement(getSession()).uniqueResult();
			return new YearAndPart(currYear, prevPart);
		}
		EducationYear prevYear = get(EducationYear.class, EducationYear.intValue(), currYear.getIntValue() - 1);
		List<YearDistributionPart> partsOfDistribution = getList(YearDistributionPart.class, YearDistributionPart.yearDistribution(), currPart.getYearDistribution(), YearDistributionPart.number().s());
		return new YearAndPart(prevYear, partsOfDistribution.get(partsOfDistribution.size() - 1));
	}

    private DQLSelectBuilder getWpSlotDQL(SessionReportSummaryBulletinAddUI model)
    {
        final DQLSelectBuilder wpcaDQL = new DQLSelectBuilder()
                .fromEntity(EppStudentWpeCAction.class, "wpca").column("wpca")
                .fromEntity(SessionBulletinDocument.class, "b").column("b")
                .fromEntity(SessionDocumentSlot.class, "slot")
                .fromEntity(EppGroupType.class, "gt")
                .fromEntity(EppRealEduGroupRow.class, "g_row").column("g_row")
                .joinPath(DQLJoinType.inner, EppStudentWpeCAction.studentWpe().fromAlias("wpca"), "sWpe")
                .joinEntity("slot", DQLJoinType.left, SessionMark.class, "m", eq(property(SessionMark.slot().fromAlias("m")), property("slot")))
                .where(eq(property(SessionDocumentSlot.document().fromAlias("slot")), property("b")))
                .where(eq(property(SessionDocumentSlot.studentWpeCAction().fromAlias("slot")), property("wpca")))
                .where(eq(property(EppRealEduGroupRow.studentWpePart().fromAlias("g_row")), property("wpca")))
                .where(eq(property(SessionBulletinDocument.sessionObject().fromAlias("b")), value(model.getSessionObject())))
                .where(eq(property("gt"), property(EppStudentWpeCAction.type().fromAlias("wpca"))))
                .where(eq(property("sWpe", EppStudentWorkPlanElement.student().status().active()), value(Boolean.TRUE)))
                .where(eq(property("sWpe", EppStudentWorkPlanElement.student().educationOrgUnit().formativeOrgUnit()), value(model.getOrgUnit())))
                .where(eq(property("sWpe", EppStudentWorkPlanElement.year().educationYear()), value(model.getYear())))
                .where(eq(property("sWpe", EppStudentWorkPlanElement.part()), value(model.getPart())))
                ;

        model.getSessionFilterAddon().applyFilters(wpcaDQL, "wpca");
        wpcaDQL.where(markPerformDateExpr("m", model));


        return wpcaDQL;
    }

	protected IDQLExpression markPerformDateExpr(String markAlias, SessionReportSummaryBulletinAddUI model)
	{
		Date fromDate = model.isPerformDateFromActive() ? model.getPerformDateFrom() : null;
		Date toDate =  model.isPerformDateToActive()? model.getPerformDateTo() : null;
		return betweenDays(SessionMark.performDate().fromAlias(markAlias), fromDate, toDate);
	}

	/**
	 * Относится ли элемент реестра к группе "Практики" или подчиненной ей.
	 * @param regElemAlias Алиас элемента реестра
	 */
	private static IDQLExpression isPractice(final String regElemAlias)
	{
		final String practiceCode = EppRegistryStructureCodes.REGISTRY_PRACTICE;
		final IDQLExpression parentIsPractice = and(isNotNull(property(regElemAlias, EppRegistryElement.parent().parent())),
													eq(property(regElemAlias, EppRegistryElement.parent().parent().code()), value(practiceCode)));
		return or(eq(property(regElemAlias, EppRegistryElement.parent().code()), value(practiceCode)), parentIsPractice);
	}

	/**
	 * Применить фильтры аддона к запросу МСРП-ФК для практик предыдущего семестра, за исключением фильтров "Вид мероприятия реестра" (т.к. нужно брать только практики)
	 * и "Курс" (т.к. нужно брать предыдущие семестры, а они могут оказаться из предыдущего курса).
	 * @param cActionsQuery запрос на МСРП-ФК
	 * @param cActionAlias алиас МСРП-ФК
	 * @param addon аддон, фильтры которого нужно применить
	 */
	private void applyAddonFiltersWithoutTermAndRegStructure(DQLSelectBuilder cActionsQuery, String cActionAlias, UniSessionFilterAddon addon)
	{
		ICommonFilterItem courseItem = null, regStructureItem = null;
		for (ICommonFilterItem item : addon.getFilterItemList())
		{
			if (item.getSettingsName().equals(UniSessionFilterAddon.SETTINGS_NAME_COURSE))
				courseItem = item;
			if (item.getSettingsName().equals(UniSessionFilterAddon.SETTINGS_NAME_REGISTRY_STRUCTURE))
				regStructureItem = item;
		}
		// Запоминаем, выбраны ли эти фильтры, отключаем их, применяем фильтрацию, а затем восстанавливаем прежние значения.
		boolean oldCourseChecked = courseItem.isEnableCheckboxChecked(), oldRegStructureChecked = regStructureItem.isEnableCheckboxChecked();
		courseItem.setEnableCheckboxChecked(false);
		regStructureItem.setEnableCheckboxChecked(false);
		addon.applyFilters(cActionsQuery, cActionAlias);
		courseItem.setEnableCheckboxChecked(oldCourseChecked);
		regStructureItem.setEnableCheckboxChecked(oldRegStructureChecked);
	}

	/**
	 * Получить актуальные МСРП-ФК практик предыдущего семестра для указанных студентов. В целом совпадает с {@link SessionSummaryBulletinPrintDAO#getWpSlotDQL(ru.tandemservice.unisession.base.bo.SessionReport.ui.SummaryBulletinAdd.SessionReportSummaryBulletinAddUI)},
	 * за исключением следующих моментов:
	 * <ul>
	 *     <li/> Не учитываются сессия, вместо нее берутся год и часть года предыдущего семестра.
	 *     <li/> Не учитывается группа элемента реестра (нужно брать только практики) и курс (т.к. для предыдущего семестра курс может быть другой).
	 *     <li/> Не учитывается дата выставления оценок
	 * </ul>
	 * @param studentInfos Студенты, для которых нужно получить практики.
	 * @param prevTermEduYear Учебный год предыдущего семестра.
	 * @param prevTermYearPart Часть учебного года предыдущего семестра. Должен быть на единицу меньше, чем у обычных дисциплин этого студента.
	 * @param filterAddon Аддон с фильтрами для запроса.
	 */
	private Collection<EppStudentWpeCAction> getPrevTermPracticeCAction(Collection<ISessionSummaryBulletinStudent> studentInfos,
	                                                                    EducationYear prevTermEduYear, YearDistributionPart prevTermYearPart, UniSessionFilterAddon filterAddon)
	{
		Collection<Student> students = studentInfos.stream().map(ISessionSummaryBulletinStudent::getStudent).collect(Collectors.toList());
		final String cActionAlias = "cAction";
		final String regElemAlias = "regElem";
		DQLSelectBuilder cActions = new DQLSelectBuilder().fromEntity(EppStudentWpeCAction.class, cActionAlias)
				.column(property(cActionAlias))
				.where(in(property(cActionAlias, EppStudentWpeCAction.studentWpe().student()), students))
				.where(eq(property(cActionAlias, EppStudentWpeCAction.studentWpe().year().educationYear()), value(prevTermEduYear)))
				.where(eq(property(cActionAlias, EppStudentWpeCAction.studentWpe().part()), value(prevTermYearPart)))
				.where(isNull(property(cActionAlias, EppStudentWpeCAction.removalDate())))
				.joinPath(DQLJoinType.inner, EppStudentWpeCAction.studentWpe().registryElementPart().registryElement().fromAlias(cActionAlias), regElemAlias)
				.where(isPractice(regElemAlias));
		applyAddonFiltersWithoutTermAndRegStructure(cActions, cActionAlias, filterAddon);
		return cActions.createStatement(getSession()).list();
	}

	/**
	 * Компаратор ведомостей по номеру. Обычно номер - это просто строковое представление числа; в данный момент (22.08.2016) единственное исключение - в проекте ДВФУ:
	 * номер ведомости имеет формат "Наименование академ. группы-номер ведомости" (см. {@link ru.tandemservice.unifefu.base.util.FefuNumberQueueDAO}, задача DEV-6349).<br/>
	 * В любом случае при разбиении строки на фрагменты чередующихся типов "строка"/"число" будет различие только в одном фрагменте (если МСРП-ФК совпадает, то и группа совпадает),
	 * который является числом; это число на самом деле определяет порядок номера ведомости.
	 */
	private static Comparator<IFastBeanOwner> bulletinByNumberComparator = new DelegatePropertyComparator(NumberAsStringComparator.INSTANCE, SessionBulletinDocument.number().s());

	/**
	 * Получить колонки практик предыдущего семестра по МСРП-ФК. Берем все уникальные пары (дисциплина, ФИК), для каждой находим экз.ведомости и берем ведомость с наименьшим номером.
	 * @param practiceCActions МСРП-ФК, по которым формируем колонки.
	 */
	private Collection<ISessionSummaryBulletinAction> getPrevTermPracticeColumns(Collection<EppStudentWpeCAction> practiceCActions)
	{
		Set<PairKey<EppFControlActionType, EppRegistryElementPart>> practiceCATypeAndDiscipline = practiceCActions.stream()
				.map(cAction -> PairKey.create(cAction.getActionType(), cAction.getStudentWpe().getRegistryElementPart()))
				.collect(Collectors.toSet());

		final String slotAlias = "slot";
		final String bulletinAlias = "bulletin";
		final DQLSelectBuilder bulletinsOfSlots = new DQLSelectBuilder().fromEntity(SessionBulletinDocument.class, bulletinAlias)
				.where(eq(property(slotAlias, SessionDocumentSlot.document()), property(bulletinAlias)));
		Collection<SessionDocumentSlot> prevTermSlots = new DQLSelectBuilder().fromEntity(SessionDocumentSlot.class, slotAlias)
				.where(in(property(slotAlias, SessionDocumentSlot.studentWpeCAction()), practiceCActions))
				.where(exists(bulletinsOfSlots.buildQuery()))
				.createStatement(getSession()).list();

		List<ISessionSummaryBulletinAction> result = new ArrayList<>();
		practiceCATypeAndDiscipline.forEach(key -> {
			SessionBulletinDocument bulletinOfColumn = prevTermSlots.stream()
					.filter(slot -> slot.getStudentWpeCAction().getActionType().equals(key.getFirst()) && slot.getStudentWpeCAction().getStudentWpe().getRegistryElementPart().equals(key.getSecond()))
					.map(slot -> (SessionBulletinDocument)slot.getDocument())
					.min(bulletinByNumberComparator).orElse(null);
			result.add(new PrevTermPracticeColumn(key.getFirst(), key.getSecond(), bulletinOfColumn));
		});
		return result;
	}

	/**
	 * Получить оценки из экз.ведомостей для МСРП-ФК.
	 */
	private Collection<SessionMark> getMarksForPrevTermPractices(Collection<EppStudentWpeCAction> practiceCAction, boolean inSession)
	{
		final String markAlias = "mark";
		final String gradeBookAlias = "gradeBook";
		final DQLSelectBuilder gradeBooksOfMarks = new DQLSelectBuilder().fromEntity(SessionStudentGradeBookDocument.class, gradeBookAlias)
				.where(eq(property(markAlias, SessionMark.slot().document()), property(gradeBookAlias)));
		return new DQLSelectBuilder().fromEntity(SessionMark.class, markAlias)
				.where(in(property(markAlias, SessionMark.slot().studentWpeCAction()), practiceCAction))
				.where(exists(gradeBooksOfMarks.buildQuery()))
				.where(eq(property(markAlias, SessionMark.slot().inSession()), value(inSession)))
				.createStatement(getSession()).list();
	}

    private static class SessionSummaryBulletinData implements ISessionSummaryBulletinData
    {
        private String group;
		/** (Студент, ФИК, Дисциплина) -> вид дисциплины РУП. Если его нет, то контрольного мероприятия в РУП студента нет. Если есть, то хорошо. Больше он ни для чего не нужен. */
        protected Map<MultiKey, EppWorkPlanRowKind> wpRowKindMap = new HashMap<>();
	    /** Набор практик предыдущего семестра в РУП. По наличию практики здесь определяется, ставить ли прочерк вместо оценки студента (если МСРП-ФК нет в рабочем плане). */
	    private Set<TripletKey<Student, EppFControlActionType, EppRegistryElementPart>> prevTermPracticesInWorkPlan = new HashSet<>();
		/** (Студент, ФИК, Дисциплина) -> оценка */
        protected Map<MultiKey, SessionMark> markMap = new HashMap<>();
	    /** (Студент, ФИК, Дисциплина) -> оценка за практику предыдущего семестра */
	    private Map<TripletKey<Student, EppFControlActionType, EppRegistryElementPart>, SessionMark> prevTermPractice2mark = new HashMap<>();
	    /** "Строки студентов" */
        protected Set<ISessionSummaryBulletinStudent> students = new HashSet<>();
		/** (ФИК, дисциплина) -> (ФИК, дисциплина, список ведомостей). Это как бы столбцы дисциплин. */
        protected Map<MultiKey, ISessionSummaryBulletinAction> actions = new HashMap<>();
	    /** Столбцы практик предыдущего семестра. */
	    private List<ISessionSummaryBulletinAction> prevTermPractices = new ArrayList<>();
	    /** Показывать ли данные БРС. */
        protected boolean _showMarkPointsData;

        private SessionSummaryBulletinData(String group, boolean showMarkPointsData)
        {
            this.group = group;
            _showMarkPointsData = showMarkPointsData;
        }

        @Override
        public boolean isShowMarkPointsData()
        {
            return _showMarkPointsData;
        }

        @Override
        public String getGroup()
        {
            return group;
        }

        @Override
        public Collection<ISessionSummaryBulletinStudent> getStudents()
        {
            return students;
        }

        @Override
        public Collection<ISessionSummaryBulletinAction> getActions()
        {
            return actions.values();
        }

        @Override
        public Collection<ISessionSummaryBulletinAction> getPrevTermPractices()
        {
            return prevTermPractices;
        }

        @Override
        public SessionMark getMark(ISessionSummaryBulletinStudent student, ISessionSummaryBulletinAction action)
        {
            return markMap.get(key(student, action));
        }

        @Override
        public SessionMark getPrevTermPracticeMark(EppRegistryElementPart registryElementPart, EppFControlActionType fcaType, Student student)
        {
            return prevTermPractice2mark.get(TripletKey.create(student, fcaType, registryElementPart));
        }

        @Override
        public EppWorkPlanRowKind getWpRowKind(ISessionSummaryBulletinStudent student, ISessionSummaryBulletinAction action)
        {
            return wpRowKindMap.get(key(student, action));
        }

        @Override
        public boolean isPrevTermPracticeInWorkPlan(EppRegistryElementPart registryElementPart, EppFControlActionType fcaType, Student student)
        {
            return prevTermPracticesInWorkPlan.contains(TripletKey.create(student, fcaType, registryElementPart));
        }

        /**
		 * Ключ, состоящий из ID студента, ID ФИК, ID части элемента реестра (дисциплины)
		 * @param student Как бы строка таблицы (нужен только студент)
		 * @param action Столбец таблицы (берем ФИК и дисциплину)
		 */
        protected MultiKey key(ISessionSummaryBulletinStudent student, ISessionSummaryBulletinAction action)
        {
            return new MultiKey(student.getStudent().getId(), action.getControlActionType().getId(), action.getDiscipline().getId());
        }

	    /**
	     * Добавить МСРП-ФК в рабочий план (чтобы потом определять, где ставить прочерк, если МСРП-ФК в РУП нет, а где - оценку или пропуск (если МСРП-ФК в плане есть, но оценка не выставлена))
	     */
	    public void addPrevTermPracticesToWorkPlan(Collection<EppStudentWpeCAction> practiceCActions)
	    {
		    for (EppStudentWpeCAction cAction : practiceCActions)
		    {
			    EppStudentWorkPlanElement wpe = cAction.getStudentWpe();
			    TripletKey<Student, EppFControlActionType, EppRegistryElementPart> key = TripletKey.create(wpe.getStudent(), cAction.getActionType(), wpe.getRegistryElementPart());
			    prevTermPracticesInWorkPlan.add(key);
		    }
	    }

	    /**
	     * Добавить оценки за практики предыдущего семестра.
	     */
	    public void addPrevTermPracticeMarks(Collection<SessionMark> prevTermPracticeMarks)
	    {
		    prevTermPracticeMarks.forEach(mark -> {
			    EppStudentWpeCAction cAction = mark.getSlot().getStudentWpeCAction();
			    EppStudentWorkPlanElement wpe = cAction.getStudentWpe();
			    TripletKey<Student, EppFControlActionType, EppRegistryElementPart> key = TripletKey.create(wpe.getStudent(), cAction.getActionType(), wpe.getRegistryElementPart());
			    prevTermPractice2mark.put(key, mark);
		    });
	    }
    }

    private static class SessionSummaryBulletinStudent extends UniBaseUtils.KeyBase<Student> implements ISessionSummaryBulletinStudent
    {
        private EppRealEduGroupRow row;
        private EducationOrgUnit eduOu;

        private SessionSummaryBulletinStudent(EppRealEduGroupRow row, EducationOrgUnit eduOu)
        {
            this.row = row;
            this.eduOu = eduOu;
        }

        @Override
        public Student getStudent()
        {
            return row.getStudentWpePart().getStudentWpe().getStudent();
        }

        @Override
        public EducationOrgUnit getEduOu()
        {
            return eduOu;
        }

        @Override
        public Course getCourse()
        {
            return row.getStudentWpePart().getStudentWpe().getCourse();
        }

        @Override
        public Term getTerm()
        {
            return row.getStudentWpePart().getStudentWpe().getTerm();
        }

        @Override
        public Student key()
        {
            return getStudent();
        }
    }

	/**
	 * Класс столбцов практик предыдущего семестра.
	 */
    protected static class PrevTermPracticeColumn implements ISessionSummaryBulletinAction
    {
        private final EppFControlActionType fcaType;
        private final EppRegistryElementPart registryElementPart;
	    private final Collection<SessionBulletinDocument> bulletins;

	    // Для практик берем только одну ведомость, если она вообще есть. Потом будем проверять, пуст ли список ведомостей для корректной обработки.
        public PrevTermPracticeColumn(EppFControlActionType fcaType, EppRegistryElementPart registryElementPart, SessionBulletinDocument bulletin)
        {
            this.fcaType = fcaType;
            this.registryElementPart = registryElementPart;
	        this.bulletins = (bulletin == null) ? Collections.emptyList() : Collections.singletonList(bulletin);
        }

        @Override
        public EppFControlActionType getControlActionType()
        {
            return fcaType;
        }

        @Override
        public EppRegistryElementPart getDiscipline()
        {
            return registryElementPart;
        }

        @Override
        public Collection<SessionBulletinDocument> getBulletins()
        {
            return bulletins;
        }
    }

    private static class SessionSummaryBulletinAction extends UniBaseUtils.KeyBase<MultiKey> implements ISessionSummaryBulletinAction
    {
        private EppFControlActionType controlActionType;
        private EppRegistryElementPart discipline;
        private Set<SessionBulletinDocument> bulletins = new HashSet<>();

        private SessionSummaryBulletinAction(EppFControlActionType controlActionType, EppRegistryElementPart discipline)
        {
            this.controlActionType = controlActionType;
            this.discipline = discipline;
        }

		/**
		 * Ключ, состоящий из ФИК и части элемента реестра (дисциплины)
		 */
        private static MultiKey key(EppFControlActionType controlActionType, EppRegistryElementPart discipline)
        {
            return new MultiKey(controlActionType, discipline);
        }

        @Override
        public EppFControlActionType getControlActionType()
        {
            return controlActionType;
        }

        @Override
        public EppRegistryElementPart getDiscipline()
        {
            return discipline;
        }

        @Override
        public Set<SessionBulletinDocument> getBulletins()
        {
            return bulletins;
        }

		/**
		 * Ключ, состоящий из ФИК и части элемента реестра (дисциплины)
		 */
        @Override
        public MultiKey key()
        {
            return key(controlActionType, discipline);
        }
    }

    /**
     * Печать ведомости в rtf по подготовленным данным. Рекомендуется переопределять в проектах только в том случае,
     * если формат таблицы не совпадает с продуктовой - в противном случае лучше пользоваться выделенными методами.
     *
     * @param model   параметры построения отчета
     * @param data     данные сводной ведомости
     * @param document rtf
     * @return rtf
     */
    protected RtfDocument printBulletin(SessionReportSummaryBulletinAddUI model, final ISessionSummaryBulletinData data, final RtfDocument document)
    {
        if (CollectionUtils.isEmpty(data.getActions()))
            return null;

        printAdditionalData(model, data, document);

        RtfInjectModifier modifier = new RtfInjectModifier();
        prepareHeaderInjectModifier(model, data, modifier);
        modifier.modify(document);

        final BulletinTableData tableData = createTableData(data, model);
	    tableData.init();

        RtfTableModifier tableModifier = new RtfTableModifier().put("T", tableData.getCells());
        tableModifier.put("T", createRowInterceptor(tableData, model, document));
        tableModifier.modify(document);

        return document;
    }

	/** Создать обработчик строк таблицы. Для переопределения в проектах (получения наследников {@link SessionSummaryBulletinRowInterceptor} со специализированным поведением). */
	protected SessionSummaryBulletinRowInterceptor createRowInterceptor(BulletinTableData tableData, SessionReportSummaryBulletinAddUI model, RtfDocument document)
	{
		return new SessionSummaryBulletinRowInterceptor(tableData, model.isShowMarkPointsData(), document);
	}

	protected class SessionSummaryBulletinRowInterceptor extends RtfRowIntercepterBase
	{
		protected static final int markDataCellIndex = 3;

		protected final BulletinTableData tableData;
		protected final boolean showMarkPointsData;
		protected final RtfDocument document;

		public SessionSummaryBulletinRowInterceptor(BulletinTableData tableData, boolean showMarkPointsData, RtfDocument document)
		{
			this.tableData = tableData;
			this.showMarkPointsData = showMarkPointsData;
			this.document = document;
		}

		@Override
		public void beforeModify(RtfTable table, int currentRowIndex)
		{
			final int firstLevelTitleRowIndex = currentRowIndex - 5;
			final int secondLevelTitleRowIndex = currentRowIndex - 4;
			final int performDateRowIndex = currentRowIndex - 3;
			final int bulletinRowIndex = currentRowIndex - 2;
			final int markPointsRowIndex = currentRowIndex - 1;

			// разбиваем последнюю ячейку первого ряда шапки для строки с типами контрольных мероприятий
			// заполняем ячейки названиями и выделяем жирным
			RtfUtil.splitRow(table.getRowList().get(firstLevelTitleRowIndex), markDataCellIndex, (newCell, index) -> newCell.getElementList().addAll(new RtfString().boldBegin().append(tableData.getFirstLevelTitle(index)).boldEnd().toList()), tableData.getFirstLevelScales());

			// то же самое для названий контрольных мероприятий
			RtfUtil.splitRow(table.getRowList().get(secondLevelTitleRowIndex), markDataCellIndex, (newCell, index) -> {
				newCell.getElementList().addAll(new RtfString().boldBegin().append(tableData.getSecondLevelTitle(index)).boldEnd().toList());
				newCell.setTextDirection(TextDirection.LR_BT);
			}, tableData.getSecondLevelScales());

			// Аналогично разбиваем ячейку для дат сдачи. Для практик предыдущего семестра в той же ячейке выводим форму контроля (например, "Экзамен\n31.12.2000").
			// Если дата для практики не указана, то выводим только форму контроля в одну строку.
			RtfUtil.splitRow(table.getRowList().get(performDateRowIndex), markDataCellIndex,
					(newCell, index) -> newCell.getElementList().addAll(new RtfString().append(tableData.getPerformDate(index)).toList()),
					tableData.getSecondLevelScales());

			// Аналогично разбиваем ячейку для номеров ведомостей
			RtfUtil.splitRow(table.getRowList().get(bulletinRowIndex), markDataCellIndex, (newCell, index) -> newCell.getElementList().addAll(new RtfString().append(tableData.getBulletins(index)).toList()), tableData.getSecondLevelScales());

			// теперь обработаем строку под формами контроля, и строку с меткой
			RtfRow subCaRow = table.getRowList().get(markPointsRowIndex);
			RtfRow marksRow = table.getRowList().get(currentRowIndex);
			if (showMarkPointsData) // нужно отображать данные по баллам - значит, нужно разбить строку под формами контроля
			{
				// мастштаб для строки под формами контроля
				int[] scales = new int[tableData.getSecondLevelScales().length * 2];
				Arrays.fill(scales, 1);

				// строка под формами контроля
				RtfUtil.splitRow(subCaRow, markDataCellIndex, getMarkDataRowSplitInterceptorForPoints(), scales);
				// строка с меткой - колонку итога поделим пополам, а колонку для вывода оценок и баллов - аналогично строке под формами контроля
				if (marksRow.getCellList().size() > markDataCellIndex + 1)
					RtfUtil.splitRow(marksRow, markDataCellIndex + 1, null, new int[]{1, 1});
				RtfUtil.splitRow(marksRow, markDataCellIndex, null, scales);
			}
			else // не нужно отображать данные по баллам - значит, нужно убрать строку под формами контроля, смерджив ее со строкой с формами контроля
			{
				RtfUtil.splitRow(subCaRow, markDataCellIndex, null, tableData.getSecondLevelScales());

				for (int i = 0; i < tableData.getSecondLevelScales().length; i++)
					RtfUtil.mergeCellsVertical(table, i + markDataCellIndex, markPointsRowIndex - 1, markPointsRowIndex);

				RtfUtil.splitRow(marksRow, markDataCellIndex, null, tableData.getSecondLevelScales());
			}
		}

		@Override
		public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
		{
			// если в ячейке выводится оценка, вызовем метод подсветки (может быть определен в проектном слое)
			if (tableData.containsMark(colIndex, rowIndex))
			{
				SessionMark sessionMark = tableData.getMark(colIndex, rowIndex);
				return highlightMarkCell(document, cell, sessionMark, value);
			}
			// если ряд содержит только ячейку с направлением подготовки -
			if (tableData.getCells()[rowIndex].length == 1)
				return new RtfString().append(IRtfData.QC).boldBegin().append(value).boldEnd().toList();
			if (colIndex == 1 && tableData.isStudentInactive(rowIndex + 1))
				return new RtfString().append(IRtfData.I).append(value).append(IRtfData.I, 0).toList();
			if (printTotals() && isTotalHeaderCell(value))
				return new RtfString().boldBegin().append(value).boldEnd().toList();
			return null;
		}

		protected boolean isTotalHeaderCell(String text)
		{
			return TotalByHighSchool.equals(text) || TotalByAllHighSchools.equals(text);
		}

		@Override
		public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
		{
			for (int rowIndex = 0; rowIndex < tableData.getCells().length; rowIndex++)
			{
				final String[] rowData = tableData.getCells()[rowIndex];
				// если ряд содержит только ячейку с направлением подготовки
				if (rowData.length == 1)
				{
					RtfUtil.unitAllCell(newRowList.get(startIndex + rowIndex), 0);
				}
			}
		}
	}

	/** Сформировать контейнер данных таблицы. Для переопределения в проектах (получения наследников {@link BulletinTableData} с доп. данными). */
	protected BulletinTableData createTableData(ISessionSummaryBulletinData data, SessionReportSummaryBulletinAddUI model)
	{
		return new BulletinTableData(data, model.isShowDisciplineTitleWithNumber());
	}

    protected class BulletinTableData
    {
	    private ISessionSummaryBulletinData data;
	    private boolean showDisciplineTitleWithNumber;

		private String[][] tableCells;

	    /** Формы контроля дисциплин. Нумерация в рамках столбцов первой строки (не подстолбцов дисциплин), начиная с 0 для первой ФИК дисциплин. */
		private String[] disciplineFcaTypeTitles;
	    /** Заголовки дисциплин. Нумерация в рамках столбцов дисциплин (второй строки), начиная с 0 для первой дисциплины (вне зависимости от того, сколько в этой строке еще столбцов практик). */
		private String[] disciplineTitles;

	    /** Заголовки практик предыдущего семестра. */
		private String[] prevTermPracticeTitles;

	    /** Номера экз.ведомостей по номерам столбцов. Пустая строка, если ведомости нет. */
		private String[] bulletins;
	    /** Даты экз.ведомостей по номерам столбцов. Пустая строка, если ведомости нет. */
		private String[] performDates;

		private int[] firstLevelScales;
		private int[] secondLevelScales;
	    /** Индексы строк неактивных студентов (для отображения их особым образом - курсивом) */
		private HashSet<Integer> inactiveStudentRowIndexes;
	    /** Таблица (номер колонки, номер строки) -> оценка (вообще-то в Table нельзя хранить null, поэтому храним Optional) */
		private Table<Integer, Integer, Optional<SessionMark>> marksTable = HashBasedTable.create();

	    /** Число столбцов практик предыдущего семестра */
		private int prevTermPracticeCount;


	    /**
	     * @param showDisciplineTitleWithNumber Отображать полное название частей элементов реестра (если {@code false}, то выводить просто название).
	     */
        public BulletinTableData(ISessionSummaryBulletinData data, boolean showDisciplineTitleWithNumber)
        {
	        this.data = data;
	        this.showDisciplineTitleWithNumber = showDisciplineTitleWithNumber;
        }

	    /** Ячейки таблицы */
        public String[][] getCells()
		{
			return tableCells;
		}

	    /** Заголовок из первой строки по указанному индексу. Если есть практики пред. семестра, то под индексом 0 - соответствующая надпись, начиная с 1 - заголовки ФИК дисциплин. */
		public String getFirstLevelTitle(int columnIndex)
		{
			if (prevTermPracticeCount == 0)
				return disciplineFcaTypeTitles[columnIndex];
			return columnIndex == 0 ? "Практики предыдущего семестра" : disciplineFcaTypeTitles[columnIndex - 1];
		}

	    /** Заголовок из второй строки по указанному индексу. Сначала идут заголовки практик предыдущего семестра, потом - обычных дисциплин. */
		public String getSecondLevelTitle(int columnIndex)
		{
			return (columnIndex < prevTermPracticeCount) ? prevTermPracticeTitles[columnIndex] : disciplineTitles[columnIndex - prevTermPracticeCount];
		}

		public int[] getFirstLevelScales()
		{
			return firstLevelScales;
		}

		public int[] getSecondLevelScales()
		{
			return secondLevelScales;
		}

	    /** Есть ли в таблице оценка (возможно, даже null), т.е. не должен ли там быть прочерк. */
        public boolean containsMark(int columnIndex, int rowIndex)
        {
            return marksTable.contains(columnIndex, rowIndex);
        }

	    /** Получить оценку из соответствующих столбца и строки. */
		public SessionMark getMark(int columnIndex, int rowIndex)
		{
			return marksTable.get(columnIndex, rowIndex).orElse(null);
		}

	    /** Получить строку с номерами ведомости по номеру столбца. Пустая строка, если нет ведомостей. */
		public String getBulletins(int columnIndex)
		{
			return bulletins[columnIndex];
		}

	    /** Получить строку с датами формирования ведомостей по номеру столбца. Пустая строка, если нет ведомостей или дата не выставлена. */
		public String getPerformDate(int columnIndex)
		{
			return performDates[columnIndex];
		}

	    /** Является ли студент в строке с указанным номером неактивным. */
		public boolean isStudentInactive(int rowIndex)
		{
			return inactiveStudentRowIndexes.contains(rowIndex);
		}

	    /** Относится ли номер столбца к практикам предыдущего семестра. Работает не только для заголовка, но и там, где столбец может разбиваться на два - для обычной оценки и БРС. */
	    private boolean indexIsPrevTermPractice(int columnIndex, boolean twoColumnsPerAction)
	    {
		    int multiplier = twoColumnsPerAction ? 2 : 1;
		    return columnIndex < (prevTermPracticeCount * multiplier);
	    }

	    /** Заголовок НПв */
		protected String eduLevelHighSchoolTitle(EducationLevelsHighSchool highSchool)
		{
			EducationLevels eduLevel = highSchool.getEducationLevel();
			EduProgramSpecialization specialization = eduLevel.getEduProgramSpecialization();
			EduProgramSubject subject = eduLevel.getEduProgramSubject();
			String title = eduLevel.getTitleCodePrefix() + " " + (subject != null ? subject.getTitle() : highSchool.getTitle());
			if (null == specialization || (specialization instanceof EduProgramSpecializationRoot))
				return title;
			return title + " | " + specialization.getTitle();
		}

	    private String getRegElemPartTitle(EppRegistryElementPart regElemPart, boolean showDisciplineTitleWithNumber)
	    {
		    return showDisciplineTitleWithNumber ? regElemPart.getTitleWithNumber() : regElemPart.getTitle();
	    }

	    /**
	     * Инициализировать информацию, относящуюся к заголовкам таблицы.
	     * @param actions Упорядоченные столбцы обычных дисциплин.
	     * @param prevTermPractices Упорядоченные столбцы практик предыдущего семестра.
	     */
        protected void initHeaders(List<ISessionSummaryBulletinAction> actions, List<ISessionSummaryBulletinAction> prevTermPractices)
        {
	        // Столбец ФИК дисциплин с подстолбцами дисциплин. Порядок подстолбцов сохраняется.
            Multimap<EppFControlActionType, EppRegistryElementPart> disciplineMultiColumns = LinkedHashMultimap.create();
            actions.forEach(action -> disciplineMultiColumns.put(action.getControlActionType(), action.getDiscipline()));

            disciplineFcaTypeTitles = disciplineMultiColumns.keySet().stream().map(EppFControlActionType::getTitle).toArray(String[]::new);
            disciplineTitles = actions.stream().map(action -> getRegElemPartTitle(action.getDiscipline(), showDisciplineTitleWithNumber)).toArray(String[]::new);

	        // Уникальные пары (дисциплина практики, ФИК), порядок сохраняется.
	        LinkedHashSet<PairKey<EppRegistryElementPart, EppFControlActionType>> prevTermPracticeColumns = prevTermPractices.stream()
			        .map(practice -> PairKey.create(practice.getDiscipline(), practice.getControlActionType()))
			        .collect(Collectors.toCollection(LinkedHashSet::new));

	        prevTermPracticeCount = prevTermPracticeColumns.size();

	        prevTermPracticeTitles = prevTermPracticeColumns.stream().map(key -> getRegElemPartTitle(key.getFirst(), showDisciplineTitleWithNumber)).toArray(String[]::new);

	        Function<ISessionSummaryBulletinAction, String> practiceColumn2bulletinMapper = practice ->
	        {
		        String fcaType = practice.getControlActionType().getShortTitle();
		        String bulletins = formatBulletinsNumber(practice.getBulletins());
		        return Strings.isNullOrEmpty(bulletins) ? fcaType : (fcaType + " № " + bulletins);
	        };
            bulletins = Stream.concat(prevTermPractices.stream().map(practiceColumn2bulletinMapper), actions.stream().map(action -> "№ " + formatBulletinsNumber(action.getBulletins())))
                    .toArray(String[]::new);

            Function<ISessionSummaryBulletinAction, String> disciplineColumn2performDateMapper = action -> action.getBulletins().stream()
                    .filter(b -> b.getPerformDate() != null)
		            .map(b -> DATE_FORMATTER_DD_MM_YYYY.format(b.getPerformDate()))
		            .collect(Collectors.joining(", "));
	        Function<ISessionSummaryBulletinAction, String> practiceColumn2performDateMapper = practice ->
	        {
		        Date performDate = practice.getBulletins().isEmpty() ? null : practice.getBulletins().iterator().next().getPerformDate();
		        return DATE_FORMATTER_DD_MM_YYYY.format(performDate);
	        };
            performDates = Stream.concat(prevTermPractices.stream().map(practiceColumn2performDateMapper), actions.stream().map(disciplineColumn2performDateMapper)).toArray(String[]::new);

	        int[] disciplineFcaScales = disciplineMultiColumns.asMap().values().stream().mapToInt(Collection::size).toArray();
            firstLevelScales = (prevTermPracticeCount == 0) ? ArrayUtils.clone(disciplineFcaScales) : ArrayUtils.addAll(new int[]{prevTermPracticeCount}, disciplineFcaScales);
            secondLevelScales = new int[prevTermPracticeCount + disciplineMultiColumns.size()];
            Arrays.fill(secondLevelScales, 1);
        }

	    /**
	     * Есть ли МСРП-ФК студента в РУП.
	     * @param data Отсюда вытаскиваем фактическую информацию.
	     * @param student Информация о студенте.
	     * @param action Информация о столбце.
	     * @param columnIndex Номер столбца. По нему определяем, является ли это столбец дисциплины или практики предыдущего семестра.
	     * @param twoColumnsPerAction Разбиваются ли столбцы оценок на два - для оценки и БРС. Нужно для корректного определения по номеру столбца.
	     */
        private boolean isStudentActionInWorkPlan(ISessionSummaryBulletinData data, ISessionSummaryBulletinStudent student, ISessionSummaryBulletinAction action, int columnIndex, boolean twoColumnsPerAction)
        {
            if (indexIsPrevTermPractice(columnIndex, twoColumnsPerAction))
                return data.isPrevTermPracticeInWorkPlan(action.getDiscipline(), action.getControlActionType(), student.getStudent());
            return data.getWpRowKind(student, action) != null;
        }

	    /**
	     * Получить оценку студента за мероприятие.
	     * @param data Отсюда вытаскиваем фактическую информацию.
	     * @param student Информация о студенте.
	     * @param action Информация о столбце.
	     * @param columnIndex Номер столбца. По нему определяем, является ли это столбец дисциплины или практики предыдущего семестра.
	     * @param twoColumnsPerAction Разбиваются ли столбцы оценок на два - для оценки и БРС. Нужно для корректного определения по номеру столбца.
	     */
        private SessionMark getMarkFromData(ISessionSummaryBulletinData data, ISessionSummaryBulletinStudent student, ISessionSummaryBulletinAction action, int columnIndex, boolean twoColumnsPerAction)
        {
            if (indexIsPrevTermPractice(columnIndex, twoColumnsPerAction))
                return data.getPrevTermPracticeMark(action.getDiscipline(), action.getControlActionType(), student.getStudent());
            return data.getMark(student, action);
        }

        public void init()
        {
            List<ISessionSummaryBulletinAction> actions = new ArrayList<>(data.getActions());
	        Comparator<EppRegistryElementPart> elemPartFullTitleComparator = Comparator.comparing((EppRegistryElementPart elemPart) -> elemPart.getRegistryElement().getTitle())
			        .thenComparing(elemPart -> elemPart.getRegistryElement().getNumber())
			        .thenComparing(EppRegistryElementPart::getNumber);
	        Comparator<EppRegistryElementPart> elemPartTitleComparator = Comparator.comparing(EppRegistryElementPart::getTitle);
			Collections.sort(actions, Comparator.comparing((ISessionSummaryBulletinAction action) -> action.getControlActionType().getPriority())
					.thenComparing(ISessionSummaryBulletinAction::getDiscipline, showDisciplineTitleWithNumber ? elemPartFullTitleComparator : elemPartTitleComparator));

            Comparator<ISessionSummaryBulletinAction> practiceComparator = Comparator.comparing((ISessionSummaryBulletinAction practice) -> practice.getDiscipline().getTitle())
					.thenComparing(practice -> practice.getDiscipline().getNumber())
					.thenComparing(practice -> practice.getControlActionType().getTitle());
			List<ISessionSummaryBulletinAction> prevTermPractices = data.getPrevTermPractices().stream().sorted(practiceComparator).collect(Collectors.toList());

            initHeaders(actions, prevTermPractices);

            Collection<ISessionSummaryBulletinAction> columns = ListUtils.union(prevTermPractices, actions);

			List<EducationLevelsHighSchool> eduLevelList = data.getStudents().stream()
					.map(s -> s.getEduOu().getEducationLevelHighSchool()).distinct()
					.sorted(Comparator.nullsFirst(Comparator.comparing(EducationLevelsHighSchool::getDisplayableTitle)))
					.collect(Collectors.toList());

            final List<ISessionSummaryBulletinStudent> students = new ArrayList<>(data.getStudents());
            Collections.sort(students, Comparator.comparing(s -> s.getStudent(), Student.FULL_FIO_AND_ID_COMPARATOR));

            int studentNumber = 1;
            int rowNumber = 1;
            List<String[]> tableData = new ArrayList<>();
            inactiveStudentRowIndexes = new HashSet<>();
            Map<ISessionSummaryBulletinAction, MutableInt> positiveMarksEduLevelTotalByCA = SafeMap.get(MutableInt.class);
            Map<ISessionSummaryBulletinAction, MutableDouble> summMarksELTotalByCA = SafeMap.get(MutableDouble.class);
			final boolean multipleHighSchools = eduLevelList.size() > 1;
            for (EducationLevelsHighSchool highSchool : eduLevelList)
            {
                // добавляем строку направления подготовки, если их больше одного
                if (multipleHighSchools)
                {
                    tableData.add(new String[]{eduLevelHighSchoolTitle(highSchool)});
                    rowNumber++;
                }

                Map<ISessionSummaryBulletinAction, MutableInt> positiveMarksTotalbyCA = SafeMap.get(MutableInt.class);
                Map<ISessionSummaryBulletinAction, MutableDouble> summMarksTotalbyCA = SafeMap.get(MutableDouble.class);
                for (ISessionSummaryBulletinStudent student : students)
                {
                    if (!highSchool.getId().equals(student.getEduOu().getEducationLevelHighSchool().getId()))
                        continue;
                    if (!student.getStudent().getStatus().isActive())
                        inactiveStudentRowIndexes.add(rowNumber);
                    rowNumber++;

                    List<String> row = new ArrayList<>();

                    row.addAll(Arrays.asList(printStudentData(studentNumber++, student)));

                    int positiveMarksTotal = 0;
                    double summMarksTotal = 0;
                    // заполняем для этого студента строку оценок
                    // по списку ключей, упорядоченному так же, как колонки таблицы
                    for (ISessionSummaryBulletinAction action : columns)
                    {
	                    // Т.к. тут в номере столбца учитываются еще и первые три (номер, студент, номер зачетки), нужно сделать поправку для номера столбца.
	                    final int studentInfoColumnsCount = 3;
                        // если в РП студента нет этого контрольного мероприятия, ставим прочерк
                        if (!isStudentActionInWorkPlan(data, student, action, row.size() - studentInfoColumnsCount, data.isShowMarkPointsData()))
                        {
                            if (data.isShowMarkPointsData())
                                row.add("-");
                            row.add("-");
                            continue;
                        }

                        final SessionMark sessionMark = getMarkFromData(data, student, action, row.size() - studentInfoColumnsCount, data.isShowMarkPointsData());
                        marksTable.put(row.size(), tableData.size(), Optional.ofNullable(sessionMark));

                        if (sessionMark == null)
                        {
                            if (data.isShowMarkPointsData())
                                row.add("");
                            row.add("");
                            continue;
                        }

                        if (data.isShowMarkPointsData())
                            row.add(sessionMark.getPoints() == null ? "" : DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(sessionMark.getPoints()));
                        summMarksTotal = sessionMark.getPoints() == null ? summMarksTotal : summMarksTotal + sessionMark.getPoints();

                        if (sessionMark.getPoints() != null)
                            summMarksTotalbyCA.get(action).add(sessionMark.getPoints());

                        if (sessionMark.getValueItem() instanceof SessionMarkGradeValueCatalogItem)
                        {
                            final SessionMarkGradeValueCatalogItem markValue = (SessionMarkGradeValueCatalogItem) sessionMark.getValueItem();
                            if (markValue.isPositive())
                            {
                                positiveMarksTotal++;
                                positiveMarksTotalbyCA.get(action).increment();
                            }
                        }

                        row.add(sessionMark.getValueShortTitle());
                    }

                    if (printTotals())
                    {
                        if (data.isShowMarkPointsData())
                        {
                            if (positiveMarksTotal != 0)
                                row.add(DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(summMarksTotal / positiveMarksTotal));
                            else
                                row.add("0");
                        }
                        row.add(String.valueOf(positiveMarksTotal));
                    }

                    tableData.add(row.toArray(new String[row.size()]));
                }

                if (printTotals())
                {
                    List<String> row = new ArrayList<>();
                    row.addAll(Arrays.asList("", TotalByHighSchool, ""));
                    for (ISessionSummaryBulletinAction action : columns)
                    {
                        int positiveTotalMarkValue = positiveMarksTotalbyCA.get(action).intValue();
                        positiveMarksEduLevelTotalByCA.get(action).add(positiveTotalMarkValue);

                        if (data.isShowMarkPointsData())
                        {
                            if (summMarksTotalbyCA.containsKey(action) && positiveTotalMarkValue != 0)
                            {
                                summMarksELTotalByCA.get(action).add(summMarksTotalbyCA.get(action));
                                row.add(DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(summMarksTotalbyCA.get(action).doubleValue() / positiveTotalMarkValue));
                            }
                            else
                                row.add("0");
                        }
                        row.add(String.valueOf(positiveTotalMarkValue));
                    }
                    tableData.add(row.toArray(new String[row.size()]));
                }
            }
			// итог по всем НПП
			if (multipleHighSchools && printTotals())
			{
				List<String> totalRow = new ArrayList<>();
				totalRow.addAll(Arrays.asList("", TotalByAllHighSchools, ""));
				for (ISessionSummaryBulletinAction action : columns)
				{
					int positiveELTotalMarkValue = positiveMarksEduLevelTotalByCA.get(action).intValue();

					if (data.isShowMarkPointsData())
					{
						if (summMarksELTotalByCA.containsKey(action) && positiveELTotalMarkValue != 0)
							totalRow.add(DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(summMarksELTotalByCA.get(action).doubleValue() / positiveELTotalMarkValue));
						else
							totalRow.add("0");
					}
					totalRow.add(String.valueOf(positiveELTotalMarkValue));
				}
				tableData.add(totalRow.toArray(new String[totalRow.size()]));
			}

	        modifyTableRowList(tableData, columns);
            tableCells = tableData.toArray(new String[tableData.size()][]);

	        initAdditionalRows(columns);
        }

	    /**
	     * Модифицировать список строк таблицы (добавить/удалить строки). Для переопределения в проектах.
	     * @param rowList Список строк, подготовленный продуктовой печатью.
	     * @param columns Упорядоченная коллекция колонок.
	     */
	    protected void modifyTableRowList(List<String[]> rowList, Collection<ISessionSummaryBulletinAction> columns)
	    {
	    }

	    /**
	     * Инициализировать дополнительные строки, содержимое которых должно быть согласованы с колонками дисциплин и практик пред. семестра. Для переопределения в проектах.
	     * @param columns Упорядоченная коллекция колонок.
	     */
	    protected void initAdditionalRows(Collection<ISessionSummaryBulletinAction> columns)
	    {
	    }
    }

    /**
     * Вставка меток заголовка. Для переопределения в проектах.
     *
     * @param model   параметры построения отчета
     * @param data     данные отчета
     * @param modifier модификатор
     */
    protected void prepareHeaderInjectModifier(SessionReportSummaryBulletinAddUI model, ISessionSummaryBulletinData data, RtfInjectModifier modifier)
    {
        modifier.put("formingDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()));
        OrgUnit academy = TopOrgUnit.getInstance();
        OrgUnit ou = model.getSessionObject().getOrgUnit();
        modifier.put("vuzTitle", academy.getPrintTitle());
        modifier.put("ouTitle", ou.getPrintTitle());
        modifier.put("educationOrgUnitTitleList", UniStringUtils.joinUnique(Collections2.transform(data.getStudents(), ISessionSummaryBulletinStudent::getStudent), Student.educationOrgUnit().educationLevelHighSchool().title().s(), ", "));
        modifier.put("eduYear", model.getSessionObject().getEducationYear().getTitle());
        modifier.put("groupTitle", StringUtils.isEmpty(data.getGroup()) ? "вне групп" : data.getGroup());

        List<String> terms = new ArrayList<>(new HashSet<>(Collections2.transform(data.getStudents(), input -> input.getTerm().getTitle())));
        Collections.sort(terms);
        modifier.put("term", StringUtils.join(terms, ", "));
        SessionReportManager.addOuLeaderData(modifier, ou, "ouleader", "FIOouleader");
        modifier.put("executor", PersonSecurityUtil.getExecutor());
    }

    /**
     * Для переопределения в проектах.
     * Должен возвращать содержимое строки отчета до колонок с оценками для строки студента.
     * В продуктовой реализации - номер строки, ФИО, номер зачетки.
     *
     * @param studentNumber номер строки отчета
     * @param student       студент
     * @return Должен возвращать содержимое строки отчета до колонок с оценками для строки студента.
     */
    protected String[] printStudentData(int studentNumber, ISessionSummaryBulletinStudent student)
    {
        String number = Integer.toString(studentNumber);
        String fio = student.getStudent().getPerson().getFullFio();
        String bookNumber = student.getStudent().getBookNumber();
        return new String[]{number, fio, bookNumber};
    }

    /**
     * Для переопределения в проектах. Вызывается до обработки меток логикой продукта.
     *
     * @param model ui-компонент
     * @param data     данные отчета
     * @param document rtf
     */
    protected void printAdditionalData(SessionReportSummaryBulletinAddUI model, ISessionSummaryBulletinData data, RtfDocument document)
    {
        //
    }

    /**
     * Для переопределения в проектах.
     *
     * @return Выводить или нет строку с числом положительных оценок под таблицей, и колонку с числом положительных оценок последней в таблице.
     */
    protected boolean printTotals()
    {
        return true;
    }

    /**
     * Для переопределения в проектах.
     *
     * @param bulletins Список ведомостей
     * @return Строка с номерами ведомостей
     */
    protected String formatBulletinsNumber(Collection<SessionBulletinDocument> bulletins)
    {
        return UniStringUtils.join(bulletins, SessionBulletinDocument.number().s(), ", ");
    }

    /**
     * Для переопределения в проектах.
     *
     * @return IRtfRowSplitInterceptor для последней строки шапки (под КМ) для случая, когда выводятся не только оценки, но и баллы.
     */
    protected IRtfRowSplitInterceptor getMarkDataRowSplitInterceptorForPoints()
    {
        return (newCell, index) -> {
            if (index == 0 || index % 2 == 0)
                newCell.addElements(new RtfString().append("балл").toList());
            else
                newCell.addElements(new RtfString().append("отм").toList());
        };
    }

    /**
     * Для переопределения в проектах.
     * Метод позволяет изменить форматирование ячейки с оценкой в зависимости от ее значения.
     *
     * @param document документ (для модификации RtfHeader, когда это нужно)
     * @param cell ячейка
     * @param sessionMark оценка
     * @param value значение для вывода в ячейке
     * @return null, если специальное форматирование не нужно, иначе List<IRtfElement> с нужным форматированием и значением
     */
    protected List<IRtfElement> highlightMarkCell(RtfDocument document, RtfCell cell, SessionMark sessionMark, String value) {
        if (true)
            return null; // код дальше для примера

        if (null == sessionMark)
            return null;

        boolean highlight = false;
        SessionMarkCatalogItem markCatalogItem = sessionMark.getValueItem();
        if (markCatalogItem instanceof SessionMarkGradeValueCatalogItem)
        {
            SessionMarkGradeValueCatalogItem markValue = (SessionMarkGradeValueCatalogItem) markCatalogItem;
            highlight = !markValue.isPositive();
        }
        if (markCatalogItem instanceof SessionMarkStateCatalogItem)
        {
            SessionMarkStateCatalogItem markValue = (SessionMarkStateCatalogItem) markCatalogItem;
            highlight = ! markValue.isRemarkable();
        }

        if (highlight)
        {
	        RGB pinkRgb = Colour.PINK.getDefaultRGB();
            final int backgroundColorIndex = document.getHeader().getColorTable().addColor(pinkRgb.getRed(), pinkRgb.getGreen(), pinkRgb.getBlue());
            RtfString string = new RtfString();
            string.append(value);
            cell.setBackgroundColorIndex(backgroundColorIndex);
            return string.toList();
        }
        return null;
    }
}
