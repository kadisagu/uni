/* $Id:$ */
package ru.tandemservice.unisession.base.bo.SessionReport.util;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.ICommonFilterItem;
import ru.tandemservice.uni.IStorableReport;
import ru.tandemservice.uni.util.UniStringUtils;

import java.util.Collection;
import java.util.Date;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 22.06.2015
 */
public class SessionReportUtil {
    public static void setReportFilterValue(CommonFilterAddon filterAddon, IStorableReport report, String settingName, String reportPropertyName, String titleProperty)
    {
        setReportFilterValue(filterAddon, report, settingName, reportPropertyName, titleProperty, "; ");
    }


    public static void setReportFilterValue(CommonFilterAddon filterAddon, IStorableReport report, String settingName, String reportPropertyName, String titleProperty, String breaker)
    {
        ICommonFilterItem filterItem = filterAddon.getFilterItem(settingName);
        Object filterItemValue = filterItem.getValue();
        if (!filterItem.isEnableCheckboxChecked() || filterItemValue == null) {
            report.setProperty(reportPropertyName, null);
            return;
        }
        if (filterItem.getFormConfig().isMultiSelect()) {
            report.setProperty(reportPropertyName, UniStringUtils.join((Collection) filterItemValue, titleProperty, breaker));
        } else {
            report.setProperty(reportPropertyName, ((IEntity) filterItemValue).getProperty(titleProperty));
        }
    }

}
