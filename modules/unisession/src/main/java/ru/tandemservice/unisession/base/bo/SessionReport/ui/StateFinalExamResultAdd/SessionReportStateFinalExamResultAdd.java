/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.StateFinalExamResultAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uni.base.bo.UniEduProgram.UniEduProgramManager;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.StateFinalAttestationResultAdd.SessionReportStateFinalAttestationResultAdd;

/**
 * @author Andrey Andreev
 * @since 27.10.2016
 */
@Configuration
public class SessionReportStateFinalExamResultAdd extends SessionReportStateFinalAttestationResultAdd
{
    public static final String COMPENSATION_TYPE_DS = "compensationTypeDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addAddon(uiAddon(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME, UniEduProgramEducationOrgUnitAddon.class))
                .addDataSource(SessionReportManager.instance().eduYearDSConfig())
                .addDataSource(selectDS(COMPENSATION_TYPE_DS, compensationTypeDSHandler()).addColumn(CompensationType.shortTitle().s()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler compensationTypeDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), CompensationType.class);
    }

}
