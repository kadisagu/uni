/* $Id$ */
package ru.tandemservice.unisession.component.log.SessionDocumentLogView;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;

/**
 * @author oleyba
 * @since 6/28/11
 */
@State({ @Bind(key= PublisherActivator.PUBLISHER_ID_KEY, binding="entityId") })
public class Model extends ru.tandemservice.uni.component.log.EntityLogViewBase.Model
{
    @Override
    public String getListSettingsKey()
    {
        return "logView.sessionDocument.list.";
    }
}
