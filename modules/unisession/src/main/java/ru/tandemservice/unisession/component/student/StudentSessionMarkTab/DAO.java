/* $Id$ */
package ru.tandemservice.unisession.component.student.StudentSessionMarkTab;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.apache.commons.lang.mutable.MutableInt;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionSlotRatingData;
import ru.tandemservice.unisession.entity.document.SessionStudentGradeBookDocument;
import ru.tandemservice.unisession.entity.mark.*;
import ru.tandemservice.unisession.util.selectModel.SessionTermModel;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 4/8/11
 */
public class DAO extends UniDao<Model> implements IDAO
{
    protected static final long ALL_MARKS_OPTION_ID = 0L;
    protected static final long ACTUAL_MARKS_OPTION_ID = 1L;
    protected static final long UNACTUAL_MARKS_OPTION_ID = 2L;

    @Override
    public void prepare(final Model model)
    {
        final Student student = this.get(Student.class, model.getStudent().getId());
        model.setStudent(student);
        model.setStateModel(new LazySimpleSelectModel<>(ImmutableList.of(
                new IdentifiableWrapper(DAO.ALL_MARKS_OPTION_ID, "Все"),
                new IdentifiableWrapper(DAO.ACTUAL_MARKS_OPTION_ID, "Только актуальные"),
                new IdentifiableWrapper(DAO.UNACTUAL_MARKS_OPTION_ID, "Только неактуальные"))));
        model.setTermModel(SessionTermModel.createForStudent(student, false));
        if (model.getSettings().get("state") == null) {
            model.getSettings().set("state", new IdentifiableWrapper(DAO.ACTUAL_MARKS_OPTION_ID, "Только актуальные"));
        }
        model.setShowCurrentRating(existFixedCurrentRating(model));
        model.setShowPoints(existPoints(model));
        model.setShowPositiveModel(TwinComboDataSourceHandler.getCustomSelectModel("Есть", "Нет"));
    }

    @Override
    public void prepareListDataSource(final Model model)
    {
        final DynamicListDataSource<EppStudentWpeCAction> dataSource = model.getDataSource();

        IDataSettings settings = model.getSettings();
        final IdentifiableWrapper state = settings.get("state");
        final SessionTermModel.TermWrapper term = settings.get("term");
        Boolean showPositive = TwinComboDataSourceHandler.getSelectedValue(settings.get("showPositive"));

        final DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(EppStudentWpeCAction.class, "eppSlot")
            .column("eppSlot")
            .where(eq(property(EppStudentWpeCAction.studentWpe().student().fromAlias("eppSlot")), value(model.getStudent())));
            if (null != state)
            {
                if (state.getId().equals(DAO.ACTUAL_MARKS_OPTION_ID)) {
                    dql.where(isNull(EppStudentWpeCAction.removalDate().fromAlias("eppSlot")));
                } else if (state.getId().equals(DAO.UNACTUAL_MARKS_OPTION_ID)) {
                    dql.where(isNotNull(EppStudentWpeCAction.removalDate().fromAlias("eppSlot")));
                }
            }

        if (showPositive != null)
        {
            IDQLExpression expression = and(eq(property("mp", SessionMark.cachedMarkPositiveStatus()), value(true)),
                                            eq(property("mp", SessionMark.slot().studentWpeCAction()), property("eppSlot")));
            if (showPositive) dql.where(existsByExpr(SessionMark.class, "mp", expression));
            else dql.where(notExistsByExpr(SessionMark.class, "mp", expression));
        }

        if (null != term)
        {
            dql.where(eq(property(EppStudentWpeCAction.studentWpe().year().educationYear().fromAlias("eppSlot")), value(term.getYear())));
            dql.where(eq(property(EppStudentWpeCAction.studentWpe().part().fromAlias("eppSlot")), value(term.getPart())));
        }

        new DQLOrderDescriptionRegistry(EppStudentWpeCAction.class, "eppSlot").applyOrder(dql, dataSource.getEntityOrder());

        UniBaseUtils.createFullPage(dataSource, dql.createStatement(this.getSession()).<EppStudentWpeCAction>list());

        for (List<ViewWrapper<EppStudentWpeCAction>> wrappers : Lists.partition(ViewWrapper.<EppStudentWpeCAction>getPatchedList(dataSource), DQL.MAX_VALUES_ROW_NUMBER)) {

            final Map<EppStudentWpeCAction, SessionMark> sessionMarkMap = new HashMap<>();
            final Map<EppStudentWpeCAction, SessionMark> totalMarkMap = new HashMap<>();
            final Map<EppStudentWpeCAction, MutableInt> markCountMap = new HashMap<>();
            final Map<EppStudentWpeCAction, LinkedHashSet<PpsEntry>> tutorsMap = new HashMap<>();
            final Map<EppStudentWpeCAction, Double> scoredPointsMap = new HashMap<>();
            final Map<EppStudentWpeCAction, Double> fixedCurrentRatingMap = new HashMap<>();

            Set<Long> totalMarkSlotIds = new HashSet<>();

            final DQLSelectBuilder markDql = new DQLSelectBuilder()
                    .fromEntity(SessionMark.class, "mark")
                    .column("mark")
                    .fetchPath(DQLJoinType.inner, SessionMark.slot().fromAlias("mark"), "slot")
                    .fetchPath(DQLJoinType.inner, SessionDocumentSlot.document().fromAlias("slot"), "document")
                    .where(in(property(SessionMark.slot().studentWpeCAction().fromAlias("mark")), ids(wrappers)))
                    .order(property(SessionMark.id().fromAlias("mark")));

            for (final SessionMark mark : markDql.createStatement(DAO.this.getSession()).<SessionMark>list())
            {
                if (mark.getSlot().getDocument() instanceof SessionStudentGradeBookDocument) {
                    // итоговые оценки - в зачетке
                    SessionMark regularMark = mark instanceof SessionSlotLinkMark ? ((SessionSlotLinkMark) mark).getTarget() : mark;
                    if (mark.isInSession()) {
                        sessionMarkMap.put(mark.getSlot().getStudentWpeCAction(), regularMark);
                    }
                    else {
                        totalMarkMap.put(mark.getSlot().getStudentWpeCAction(), regularMark);
                        totalMarkSlotIds.add(regularMark.getSlot().getId());
                    }
                }

                if (mark instanceof SessionSlotRegularMark) {
                    // считаем только регулярные оценки (ссылки не надо учитывать)
                    SafeMap.safeGet(markCountMap, mark.getSlot().getStudentWpeCAction(), MutableInt.class).increment();
                }
            }

            Collection<Long> totalMarkIds = ids(totalMarkMap.values());

            final DQLSelectBuilder ppsDQL = new DQLSelectBuilder()
                    .fromEntity(SessionComissionPps.class, "rel")
                    .column(property(SessionComissionPps.pps().fromAlias("rel")))
                    .joinEntity("rel", DQLJoinType.inner, SessionMark.class, "mark", eq(property(SessionMark.commission().fromAlias("mark")), property(SessionComissionPps.commission().fromAlias("rel"))))
                    .column(property(SessionMark.slot().studentWpeCAction().fromAlias("mark")))
                    .where(in(SessionMark.id().fromAlias("mark"), totalMarkIds));

            for (final Object[] row : ppsDQL.createStatement(DAO.this.getSession()).<Object[]>list()) {
                final PpsEntry pps = (PpsEntry) row[0];
                final EppStudentWpeCAction slot = (EppStudentWpeCAction) row[1];
                SafeMap.safeGet(tutorsMap, slot, LinkedHashSet.class).add(pps);
            }

            for (SessionMarkRatingData data : getList(SessionMarkRatingData.class, SessionMarkRatingData.mark().id(), totalMarkIds)) {
                scoredPointsMap.put(data.getMark().getSlot().getStudentWpeCAction(), data.getScoredPoints());
            }

            for (SessionSlotRatingData data : getList(SessionSlotRatingData.class, SessionSlotRatingData.slot().id(), totalMarkSlotIds)) {
                fixedCurrentRatingMap.put(data.getSlot().getStudentWpeCAction(), data.getFixedCurrentRating());
            }

            for (final ViewWrapper<EppStudentWpeCAction> wrapper : wrappers) {
                SessionMark totalMark = totalMarkMap.get(wrapper.getEntity());
                Double totalMarkPoints = totalMark == null ? null : totalMark.getPoints();

                wrapper.setViewProperty("sessionMark", sessionMarkMap.get(wrapper.getEntity()));
                wrapper.setViewProperty("totalMark", totalMark);
                wrapper.setViewProperty("tutors", tutorsMap.get(wrapper.getEntity()));

                if (totalMark == null) {
                    wrapper.setViewProperty("scoredPoints", null);
                    wrapper.setViewProperty("fixedCurrentRating", null);
                    wrapper.setViewProperty("totalMarkPoints", null);
                }
                else {
                    Double scoredPoints = scoredPointsMap.get(wrapper.getEntity());
                    if (null == scoredPoints) {
                        scoredPoints = totalMarkPoints;
                    }
                    wrapper.setViewProperty("scoredPoints", scoredPoints == null ? "-" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(scoredPoints));

                    Double fixedCurrentRating = fixedCurrentRatingMap.get(wrapper.getEntity());
                    wrapper.setViewProperty("fixedCurrentRating", fixedCurrentRating == null ? "-" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(fixedCurrentRating));

                    wrapper.setViewProperty("totalMarkPoints", totalMarkPoints == null ? "-" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(totalMarkPoints));
                }

                final MutableInt markCount = markCountMap.get(wrapper.getEntity());
                wrapper.setViewProperty("markCount", (null == markCount ? null : markCount.intValue()));
            }
        }
    }
    
    private boolean existPoints(Model model)
    {
        final DQLSelectBuilder markDql = new DQLSelectBuilder()
            .fromEntity(SessionSlotMarkGradeValue.class, "mark")
            .where(eq(property(SessionMark.slot().studentWpeCAction().studentWpe().student().fromAlias("mark")), value(model.getStudent())))
            .where(isNotNull(SessionSlotMarkGradeValue.pointsAsLong().fromAlias("mark")))
            .where(ne(property(SessionSlotMarkGradeValue.pointsAsLong().fromAlias("mark")), value(0)));
        return existsEntity(markDql.buildQuery());
    }

    private boolean existFixedCurrentRating(Model model)
    {
        return existsEntity(SessionMarkRatingData.class, SessionMarkRatingData.mark().slot().studentWpeCAction().studentWpe().student().s(), model.getStudent());
    }
}
