package ru.tandemservice.unisession.component.sessionListDocument.SessionListDocumentPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionListDocument;

/**
 * @author iolshvang
 * @since 13.04.2011
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "card.id"),
    @Bind(key = "selectedTabId", binding = "selectedTabId")
})
public class Model
{
    private SessionListDocument _card = new SessionListDocument();
    private DynamicListDataSource<SessionDocumentSlot> _dataSource;
    private String _title;
    private String _termAndYearTitle;

    private String _selectedTabId;

    public SessionListDocument getCard()
    {
        return this._card;
    }

    public void setCard(final SessionListDocument card)
    {
        this._card = card;
    }

    public DynamicListDataSource<SessionDocumentSlot> getDataSource()
    {
        return this._dataSource;
    }

    public void setDataSource(final DynamicListDataSource<SessionDocumentSlot> dataSource)
    {
        this._dataSource = dataSource;
    }

    public String getTitle()
    {
        return this._title;
    }

    public void setTitle(final String title)
    {
        this._title = title;
    }

    public String getTermAndYearTitle()
    {
        return this._termAndYearTitle;
    }

    public void setTermAndYearTitle(final String termAndYearTitle)
    {
        this._termAndYearTitle = termAndYearTitle;
    }

    public String getSelectedTabId()
    {
        return this._selectedTabId;
    }

    public void setSelectedTabId(final String selectedTabId)
    {
        this._selectedTabId = selectedTabId;
    }
}
