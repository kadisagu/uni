/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReport.util.results;

import org.tandemframework.core.view.formatter.DoubleFormatter;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.ResultsAdd.ISessionReportResultsDAO;

/**
* @author oleyba
* @since 2/13/12
*/
public abstract class DoubleReportColumn implements ISessionReportResultsDAO.ISessionResultsReportColumn
{
    public abstract Double getCount(ISessionReportResultsDAO.ISessionResultsReportRow row);

    @Override
    public String cell(ISessionReportResultsDAO.ISessionResultsReportRow row)
    {
        final Double count = getCount(row);
        return null == count ? "" : DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(count);
    }
}
