/* $*/

package ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinPubForPps;

import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unisession.dao.bulletin.ISessionBulletinDAO;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;

/**
 * @author oleyba
 * @since 2/18/11
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setBulletin(this.getNotNull(SessionBulletinDocument.class, model.getBulletin().getId()));
        model.setCanEditThemes(ISessionBulletinDAO.instance.get().isBulletinRequireTheme(model.getBulletin()));

        final Person person = PersonManager.instance().dao().getPerson(model.getPrincipalContext());
        if (!CollectionUtils.exists(getList(SessionComissionPps.class, SessionComissionPps.commission(), model.getBulletin().getCommission()), sessionComissionPps -> sessionComissionPps.getPps().getPerson().equals(person)))
            throw new ApplicationException("Вы не состоите в коммиссии данной ведомости, поэтому не можете просматривать ее со своей личной страницы.");
    }
}
