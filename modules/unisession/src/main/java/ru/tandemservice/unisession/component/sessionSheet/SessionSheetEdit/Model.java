// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisession.component.sessionSheet.SessionSheetEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.base.bo.PpsEntry.util.PpsEntrySelectBlockData;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionSheetDocument;
import ru.tandemservice.unisession.util.selectModel.SessionTermModel;

import java.util.Date;
import java.util.List;

/**
 * @author oleyba
 * @since 2/17/11
 */
@Input(keys = PublisherActivator.PUBLISHER_ID_KEY, bindings = "sheet.id")
@Output({ @Bind(key="controlActionId", binding="slot.student.id")})
public class Model
{
    public static final String COMPONENT_NAME = Model.class.getPackage().getName();

    private SessionSheetDocument sheet = new SessionSheetDocument();
    private SessionDocumentSlot slot;
    private SessionTermModel.TermWrapper term;
    private PpsEntrySelectBlockData ppsData;
    private SessionMarkCatalogItem mark;
    private Date performDate;

	private boolean themeRequired;
	private String theme;

    private ISelectModel studentModel;
    private ISelectModel termModel;
    private ISelectModel controlActionModel;
    private ISelectModel reasonModel;
    private ISelectModel ppsModel;
    private ISelectModel markModel;

    private boolean inSession;

    private boolean initialized = false;
    private boolean canMark = false;

    public OrgUnit getOrgUnit() {
        return this.getSlot().getActualStudent().getEducationOrgUnit().getGroupOrgUnit();
    }

    public SessionSheetDocument getSheet()
    {
        return this.sheet;
    }

    public void setSheet(final SessionSheetDocument sheet)
    {
        this.sheet = sheet;
    }

    public SessionDocumentSlot getSlot()
    {
        return this.slot;
    }

    public void setSlot(final SessionDocumentSlot slot)
    {
        this.slot = slot;
    }

    public SessionTermModel.TermWrapper getTerm()
    {
        return this.term;
    }

    public void setTerm(final SessionTermModel.TermWrapper term)
    {
        this.term = term;
    }

    public ISelectModel getStudentModel()
    {
        return this.studentModel;
    }

    public void setStudentModel(final ISelectModel studentModel)
    {
        this.studentModel = studentModel;
    }

    public ISelectModel getTermModel()
    {
        return this.termModel;
    }

    public void setTermModel(final ISelectModel termModel)
    {
        this.termModel = termModel;
    }

    public ISelectModel getControlActionModel()
    {
        return this.controlActionModel;
    }

    public void setControlActionModel(final ISelectModel controlActionModel)
    {
        this.controlActionModel = controlActionModel;
    }

    public ISelectModel getReasonModel()
    {
        return this.reasonModel;
    }

    public void setReasonModel(final ISelectModel reasonModel)
    {
        this.reasonModel = reasonModel;
    }

    public PpsEntrySelectBlockData getPpsData()
    {
        return ppsData;
    }

    public void setPpsData(PpsEntrySelectBlockData ppsData)
    {
        this.ppsData = ppsData;
    }

    public List<PpsEntry> getPpsList()
    {
        return getPpsData().getSelectedPpsList();
    }

    public void setPpsList(final List<PpsEntry> ppsList)
    {
        getPpsData().setSelectedPps(ppsList);
    }

    public boolean isInSession()
    {
        return this.inSession;
    }

    public void setInSession(final boolean inSession)
    {
        this.inSession = inSession;
    }

    public SessionMarkCatalogItem getMark()
    {
        return this.mark;
    }

    public void setMark(final SessionMarkCatalogItem mark)
    {
        this.mark = mark;
    }

    public Date getPerformDate()
    {
        return this.performDate;
    }

    public void setPerformDate(final Date performDate)
    {
        this.performDate = performDate;
    }

	public boolean isThemeRequired()
	{
		return themeRequired;
	}

	public void setThemeRequired(final boolean themeRequired)
	{
		this.themeRequired = themeRequired;
	}

	public String getTheme()
	{
		return theme;
	}

	public void setTheme(final String theme)
	{
		this.theme = theme;
	}

    public ISelectModel getMarkModel()
    {
        return this.markModel;
    }

    public void setMarkModel(final ISelectModel markModel)
    {
        this.markModel = markModel;
    }

    public boolean isInitialized()
    {
        return this.initialized;
    }

    public void setInitialized(final boolean initialized)
    {
        this.initialized = initialized;
    }

    public boolean isCanMark()
    {
        return canMark;
    }

    public void setCanMark(boolean canMark)
    {
        this.canMark = canMark;
    }
}
