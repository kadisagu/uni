/* $Id$ */
package ru.tandemservice.unisession.attestation.bo.CurrentMarkScale.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLCaseExpressionBuilder;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.CatalogManager;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unisession.entity.catalog.SessionAttestationMarkCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionCurrentMarkScale;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 07.10.2014
 */
public class CurrentMarkScaleDAO extends UniBaseDao implements ICurrentMarkScaleDAO
{
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    @Override
    public void invertMarkPositive(Long currentMarkItemId)
    {
        SessionAttestationMarkCatalogItem mark = get(SessionAttestationMarkCatalogItem.class, currentMarkItemId);
        mark.setPositive(!mark.isPositive());
        update(mark);
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    @Override
    public void invertScaleActiveProperty(Long currentScaleId)
    {
        SessionCurrentMarkScale current = get(SessionCurrentMarkScale.class, currentScaleId);
        if (current.isActive())
        {
            if (getCount(SessionCurrentMarkScale.class) == 2)
            {
                new DQLUpdateBuilder(SessionCurrentMarkScale.class)
                        .set(SessionCurrentMarkScale.P_ACTIVE,
                             new DQLCaseExpressionBuilder()
                                     .when(eq(property(SessionCurrentMarkScale.P_ACTIVE), value(true)), value(false))
                                     .otherwise(value(true))
                                     .build()
                        )
                        .createStatement(getSession())
                        .execute();
            }
            //else
            // Мы не знаем, какую шкалу делать активной, т.к. шкал больше 2-х (или всего одна), т.к. всегда должна быть активная шкала.
        }
        else
        {
            new DQLUpdateBuilder(SessionCurrentMarkScale.class)
                    .set(SessionCurrentMarkScale.P_ACTIVE, value(false))
                    .createStatement(getSession())
                    .execute();

            current.setActive(true);
            update(current);
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void saveNewMark(SessionAttestationMarkCatalogItem mark)
    {
        final Number maxPriority = new DQLSelectBuilder().fromEntity(SessionAttestationMarkCatalogItem.class, "m")
                .column(DQLFunctions.max(property("m", SessionAttestationMarkCatalogItem.P_PRIORITY)))
                .where(eq(property("m", SessionAttestationMarkCatalogItem.L_SCALE), value(mark.getScale())))
                .createStatement(getSession()).uniqueResult();

        mark.setPriority(maxPriority == null ? 1 : maxPriority.intValue() + 1);
        mark.setCode(DefaultCatalogAddEditDAO.getNewCatalogItemCode(SessionAttestationMarkCatalogItem.class));
        save(mark);
    }

    private List<SessionAttestationMarkCatalogItem> getScaleMarksSorted(Long scaleId)
    {
        return getList(SessionAttestationMarkCatalogItem.class, SessionAttestationMarkCatalogItem.L_SCALE, scaleId, SessionAttestationMarkCatalogItem.P_PRIORITY);
    }

    private void updateScaleMarksPriority(List<SessionAttestationMarkCatalogItem> markList)
    {
        int priority = 1;
        for (SessionAttestationMarkCatalogItem mark : markList)
        {
            mark.setPriority(priority++);
            update(mark);
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void moveToTopMarkPriority(Long markId)
    {
        SessionAttestationMarkCatalogItem mark = getNotNull(SessionAttestationMarkCatalogItem.class, markId);
        List<SessionAttestationMarkCatalogItem> markList = getScaleMarksSorted(mark.getScale().getId());
        markList.remove(mark);
        markList.add(0, mark);
        updateScaleMarksPriority(markList);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void moveToBottomMarkPriority(Long markId)
    {
        SessionAttestationMarkCatalogItem mark = getNotNull(SessionAttestationMarkCatalogItem.class, markId);
        List<SessionAttestationMarkCatalogItem> markList = getScaleMarksSorted(mark.getScale().getId());
        markList.remove(mark);
        markList.add(mark);
        updateScaleMarksPriority(markList);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void moveUpMarkPriority(Long markId)
    {
        SessionAttestationMarkCatalogItem mark = getNotNull(SessionAttestationMarkCatalogItem.class, markId);
        List<SessionAttestationMarkCatalogItem> markList = getScaleMarksSorted(mark.getScale().getId());
        int idx = markList.indexOf(mark);
        if (idx > 0)
        {
            markList.remove(mark);
            markList.add(idx - 1, mark);
            updateScaleMarksPriority(markList);
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void moveDownMarkPriority(Long markId)
    {
        SessionAttestationMarkCatalogItem mark = getNotNull(SessionAttestationMarkCatalogItem.class, markId);
        List<SessionAttestationMarkCatalogItem> markList = getScaleMarksSorted(mark.getScale().getId());
        int idx = markList.indexOf(mark);
        if (idx < markList.size() - 1)
        {
            markList.remove(mark);
            markList.add(idx + 1, mark);
            updateScaleMarksPriority(markList);
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void deleteCurrentScale(Long scaleId)
    {
        SessionCurrentMarkScale scale = refresh(scaleId);
        if (scale.isActive())
            throw new ApplicationException("Нельзя удалять активную шкалу.");

        if (CatalogManager.instance().modifyDao().isCatalogItemSystem(scale))
            throw new IllegalStateException(); // Такого быть не должно

        delete(scale);
    }
}