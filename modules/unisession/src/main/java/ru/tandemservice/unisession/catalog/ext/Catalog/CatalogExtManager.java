/* $Id:$ */
package ru.tandemservice.unisession.catalog.ext.Catalog;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.CatalogManager;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogDesc;
import ru.tandemservice.unisession.entity.catalog.SessionCurrentMarkScale;
import ru.tandemservice.unisession.entity.catalog.SessionMarkStateCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionRoleInGEC;
import ru.tandemservice.unisession.entity.catalog.SessionsSimpleDocumentReason;

/**
 * @author Nikolay Fedorovskih
 * @since 07.10.2014
 */
@Configuration
public class CatalogExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private CatalogManager catalogManager;

    @Bean
    public ItemListExtension<IDynamicCatalogDesc> itemListExtension()
    {
        return itemListExtension(catalogManager.dynamicCatalogsExtPoint())
                .add(SessionCurrentMarkScale.ENTITY_NAME, SessionCurrentMarkScale.getUiDesc())
                .add(SessionRoleInGEC.ENTITY_NAME, SessionRoleInGEC.getUiDesc())
                .add(SessionsSimpleDocumentReason.ENTITY_NAME, SessionsSimpleDocumentReason.getUiDesc())
		        .add(StringUtils.uncapitalize(SessionMarkStateCatalogItem.class.getSimpleName()), SessionMarkStateCatalogItem.getUiDesc())
                .create();
    }
}