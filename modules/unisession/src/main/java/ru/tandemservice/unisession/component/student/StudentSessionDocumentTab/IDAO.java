/* $Id$ */
package ru.tandemservice.unisession.component.student.StudentSessionDocumentTab;

import ru.tandemservice.uni.dao.IListDataSourceDao;
import ru.tandemservice.uni.dao.IUpdateable;

/**
 * @author oleyba
 * @since 4/13/11
 */
public interface IDAO extends IUpdateable<Model>, IListDataSourceDao<Model>
{

}
