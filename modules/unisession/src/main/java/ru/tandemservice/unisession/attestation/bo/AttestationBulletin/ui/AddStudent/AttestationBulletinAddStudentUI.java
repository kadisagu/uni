/**
 *$Id:$
 */
package ru.tandemservice.unisession.attestation.bo.AttestationBulletin.ui.AddStudent;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unisession.attestation.bo.AttestationBulletin.AttestationBulletinManager;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin;

import java.util.List;

/**
 * @author Alexander Shaburov
 * @since 25.10.12
 */
@Input({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "entityId", required = true)
})
public class AttestationBulletinAddStudentUI extends UIPresenter
{
    private long _entityId;
    private SessionAttestationBulletin _entity;

    private List<EppStudentWorkPlanElement> _studentList;

    @Override
    public void onComponentRefresh()
    {
        _entity = DataAccessServices.dao().getNotNull(_entityId);

        _studentList = AttestationBulletinManager.instance().dao().getAttBulletinStudentList(_entity);
    }

    public void onClickApply()
    {
        AttestationBulletinManager.instance().dao().doAddStudents(_entity, _studentList);

        deactivate();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (dataSource.getName().equals(AttestationBulletinAddStudent.STUDENT_DS))
        {
            dataSource.put(AttestationBulletinAddStudent.PROP_BULLETIN, _entity);
        }
    }

    // Getters & Setters

    public long getEntityId()
    {
        return _entityId;
    }

    public void setEntityId(long entityId)
    {
        _entityId = entityId;
    }

    public SessionAttestationBulletin getEntity()
    {
        return _entity;
    }

    public void setEntity(SessionAttestationBulletin entity)
    {
        _entity = entity;
    }

    public List<EppStudentWorkPlanElement> getStudentList()
    {
        return _studentList;
    }

    public void setStudentList(List<EppStudentWorkPlanElement> studentList)
    {
        _studentList = studentList;
    }
}
