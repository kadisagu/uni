package ru.tandemservice.unisession.remoteDocument.ext.RemoteDocument;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.remoteDocument.bo.RemoteDocument.RemoteDocumentManager;

/**
 * @author avedernikov
 * @since 11.12.2015
 */

@Configuration
public class RemoteDocumentExtManager extends BusinessObjectExtensionManager
{
	public static final String REMOTE_DOCUMENT_MODULE = "unisession";

	public static final String STUDENT_PORTFOLIO_ACHIEVEMENT_SERVICE_NAME = "studentPorftolioAchievement";

	@Autowired
	private RemoteDocumentManager _remoteDocumentManager;

	@Bean
	@SuppressWarnings("unchecked")
	public ItemListExtension<String> remoteDocumentTaskExtension()
	{
		return itemListExtension(_remoteDocumentManager.tasksExtPoint())
				.add(STUDENT_PORTFOLIO_ACHIEVEMENT_SERVICE_NAME, "Файлы достижений студентов")
				.create();
	}
}
