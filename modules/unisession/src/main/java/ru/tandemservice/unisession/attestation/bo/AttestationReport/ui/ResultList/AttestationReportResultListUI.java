/**
 *$Id:$
 */
package ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.ResultList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.unisession.attestation.bo.Attestation.AttestationManager;
import ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.ResultAdd.AttestationReportResultAdd;
import ru.tandemservice.unisession.attestation.entity.report.SessionAttestationResultReport;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;

/**
 * @author Alexander Shaburov
 * @since 09.11.12
 */
@State
({
    @Bind(key = SessionReportManager.BIND_ORG_UNIT, binding = "orgUnitHolder.id", required = true)
})
public class AttestationReportResultListUI extends UIPresenter
{
    public static final String PARAM_ATTESTATION = "attestation";

    // fields
    private OrgUnitHolder _orgUnitHolder = new OrgUnitHolder();

    @Override
    public void onComponentRefresh()
    {
        _orgUnitHolder.refresh();
    }

    public void onClickAddReport()
    {
        getActivationBuilder().asRegion(AttestationReportResultAdd.class)
            .parameter(SessionReportManager.BIND_ORG_UNIT, _orgUnitHolder.getId())
            .activate();
    }

    public void onClickPrintReport()
    {
        SessionAttestationResultReport report = DataAccessServices.dao().getNotNull(SessionAttestationResultReport.class, getListenerParameterAsLong());

        byte[] content = report.getContent().getContent();
        if (content == null)
            throw new ApplicationException("Файл печатной формы пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("report.rtf").document(content), false);
    }

    public void onClickDeleteReport()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    @Override
    public ISecured getSecuredObject()
    {
        return _orgUnitHolder.getValue();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(SessionReportManager.PARAM_ORG_UNIT, _orgUnitHolder.getValue());
        dataSource.put(SessionReportManager.PARAM_EDU_YEAR, getSettings().get("educationYear"));
        dataSource.put(AttestationManager.PARAM_ORG_UNIT, _orgUnitHolder.getValue());
        dataSource.put(AttestationManager.PARAM_EDU_YEAR, getSettings().get("educationYear"));
        dataSource.put(AttestationManager.PARAM_YEAR_PART, getSettings().get("yearDistributionPart"));
        dataSource.put(PARAM_ATTESTATION, getSettings().get("attestation"));
    }


    public String getViewPermissionKey(){ return getSec().getPermission("orgUnit_viewAttestationReportResultList"); }

    public String getAddStorableReportPermissionKey(){ return getSec().getPermission("orgUnit_addAttestationReportResultList"); }

    public String getDeleteStorableReportPermissionKey(){ return getSec().getPermission("orgUnit_deleteAttestationReportResultList"); }


    // Getters & Setters

    public OrgUnitHolder getOrgUnitHolder()
    {
        return _orgUnitHolder;
    }

    public CommonPostfixPermissionModelBase getSec()
    {
        return _orgUnitHolder.getSecModel();
    }
}
