/* $*/

package ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinPub;

import org.tandemframework.caf.service.impl.CAFLegacySupportService;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.BooleanColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.transaction.sync.RuntimeTimeoutException;
import org.tandemframework.shared.commonbase.utils.PublisherColumnBuilder;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uniepp.dao.group.EppRealGroupRowDAO;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unisession.base.bo.SessionBulletin.ui.ThemeEdit.SessionBulletinThemeEdit;
import ru.tandemservice.unisession.dao.bulletin.ISessionBulletinDAO;

/**
 * @author oleyba
 * @since 2/18/11
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);

        this.prepareCheckDataSource(component);
    }

    private void prepareCheckDataSource(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        final DynamicListDataSource<EppStudentWorkPlanElement> dataSource = new DynamicListDataSource<>(component, component1 -> {
            Controller.this.getDao().prepareCheckDataSource(model);
        });

        dataSource.addColumn(new PublisherColumnBuilder("ФИО", EppStudentWorkPlanElement.student().person().fullFio(), "studentTab").subTab("studentEppLifecycleTab").tabBind(ru.tandemservice.uni.component.student.StudentPub.Model.SELECTED_TAB_BIND_KEY).build().setOrderable(false));
        dataSource.addColumn(new SimpleColumn("МСРП", "title").setOrderable(false));
        dataSource.addColumn(new BooleanColumn("Включен в ведомость", IDAO.V_BULLETIN).setOrderable(false));
        dataSource.addColumn(new BooleanColumn("Включен в учебную группу", IDAO.V_GROUP).setOrderable(false));
        model.setCheckDataSource(dataSource);
        this.getDao().prepareCheckDataSource(model);
    }

    public void onClickClose(final IBusinessComponent component)
    {
        this.getDao().doCloseBulletin(this.getModel(component));
    }

    public void onClickOpen(final IBusinessComponent component)
    {
        this.getDao().doOpenBulletin(this.getModel(component));
    }

    public void onClickMark(final IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(
                                          ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinMark.Model.COMPONENT_NAME,
                                          new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, this.getModel(component).getBulletin().getId()))
        );
    }

    public void onClickEditThemes(final IBusinessComponent component)
    {
		final Model model = this.getModel(component);
		CAFLegacySupportService.asDesktopRoot(SessionBulletinThemeEdit.class.getSimpleName())
				.parameter(UIPresenter.PUBLISHER_ID, model.getBulletin().getId())
				.activate();
    }

    public void onClickEdit(final IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinEdit.Model.COMPONENT_NAME,
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, this.getModel(component).getBulletin().getId())));
    }

    public void onClickPrint(final IBusinessComponent component)
    {
        this.getDao().printBulletin(this.getModel(component));
    }

    public void onClickSyncWithGroup(final IBusinessComponent component)
    {
        try {
            ISessionBulletinDAO.instance.get().doRefreshBulletin(this.getModel(component).getBulletin().getId());
        } catch (RuntimeTimeoutException e) {
            throw new ApplicationException("Процесс формирования/переформирования ведомостей уже запущен другим пользователем. Попробуйте повторить действие позже.");
        }
    }

    public void onClickSyncEduGroups(final IBusinessComponent component) {
        EppRealGroupRowDAO.DAEMON.wakeUpDaemon();
        this.onRefreshComponent(component);
    }

    public void onClickDelete(final IBusinessComponent component)
    {
        this.getDao().delete(this.getModel(component).getBulletin());
        this.deactivate(component);
    }
}