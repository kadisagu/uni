package ru.tandemservice.unisession.entity.mark;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;

import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.mark.gen.SessionMarkGen;

/**
 * Оценка студента в сессии
 */
public abstract class SessionMark extends SessionMarkGen
{
    public abstract SessionMarkCatalogItem getValueItem();

    public abstract Double getPoints();

    public boolean isInSession() {
        return this.getSlot().isInSession();
    }

    @Override
    @EntityDSLSupport(parts={"value.shortTitle"})
    public String getValueShortTitle() {
        return this.getValueItem().getShortTitle();
    }

    @Override
    @EntityDSLSupport(parts={"value.shortTitle"})
    public String getValueTitle() {
        return this.getValueItem().getTitle();
    }

    @Override public String toString() {
        return this.getClass().getSimpleName()+"@"+this.getId()+"("+this.getSlot().toString()+" -> "+this.getValueTitle()+")";
    }

    /** Отображаемое название: оценка, дата полуцения, название документа. */
    @Override
    @EntityDSLSupport(parts={"value.shortTitle", SessionMark.P_PERFORM_DATE})
    public String getDisplayableTitle()
    {
        return getValueShortTitle() + ", " + DateFormatter.DEFAULT_DATE_FORMATTER.format(this.getPerformDate()) +
                (this.getSlot().getDocument().getTypeShortTitle() != null ? (", " + this.getSlot().getDocument().getTypeShortTitle()) : "");
    }
}