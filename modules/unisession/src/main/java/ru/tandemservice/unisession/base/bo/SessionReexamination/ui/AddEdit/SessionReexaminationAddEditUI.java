/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReexamination.ui.AddEdit;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.info.InfoCollector;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvBlockWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartWrapper;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.unisession.base.bo.SessionReexamination.SessionReexaminationManager;
import ru.tandemservice.unisession.base.bo.SessionReexamination.logic.SessionEpvRegistryRowTermDSHandler;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.mark.SessionALRequest;
import ru.tandemservice.unisession.entity.mark.SessionALRequestRow;
import ru.tandemservice.unisession.entity.mark.SessionALRequestRowMark;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Alexey Lopatin
 * @since 26.08.2015
 */
@State({
        @Bind(key = SessionReexaminationAddEditUI.PARAM_STUDENT_ID, binding = "student.id", required = true),
        @Bind(key = SessionReexaminationAddEditUI.PARAM_REQUEST_ID, binding = "request.id")
})
public class SessionReexaminationAddEditUI extends UIPresenter
{
    public static final String PARAM_STUDENT_ID = "studentId";
    public static final String PARAM_REQUEST_ID = "requestId";
    public static final String PARAM_BLOCK = "block";

    public static final String PARAM_PERSON = "person";

    public static final String PARAM_RE_EXAMINATION_ACTION_LIST = "reExaminationActionList";
    public static final String PARAM_RE_ATTESTATION_ACTION_LIST = "reAttestationActionList";

    private Student _student = new Student();
    private SessionALRequest _request = new SessionALRequest();
    private List<DataWrapper> _rowTermExamList = Lists.newArrayList();
    private List<DataWrapper> _rowTermAttestationList = Lists.newArrayList();
    private Map<PairKey<EppEpvRowTerm, Boolean>, SessionALRequestRow> _requestRowMap = Maps.newLinkedHashMap();
    private Map<PairKey<SessionALRequestRow, EppFControlActionType>, SessionMarkGradeValueCatalogItem> _requestRowMarkMap = Maps.newHashMap();

    private Map<EppRegistryElementPart, List<EppFControlActionType>> _part2fcaMap = Maps.newHashMap();
    private List<EppFControlActionType> _usedFcaTypes = Lists.newArrayList();

    private SessionALRequestRow _currentRequestRow;
    private EppFControlActionType _currentFControlAction;
    private IEppEpvBlockWrapper _blockWrapper;

    @Override
    public void onComponentRefresh()
    {
        _student = DataAccessServices.dao().getNotNull(_student.getId());

        if (isAddForm())
        {
            EppStudent2EduPlanVersion epvRel = IEppEduPlanDAO.instance.get().getActiveStudentEduPlanVersionRelation(_student.getId());
            EppEduPlanVersionBlock block = epvRel == null ? null : epvRel.getBlock();
            if (null != block)
            {
                _blockWrapper = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockData(block.getId(), true);
                _request.setBlock(block);
            }

            _request.setRequestDate(new Date());
            _request.setEduDocument(getStudent().getEduDocument());
            _request.setStudent(getStudent());
        }
        else
        {
            _request = DataAccessServices.dao().getNotNull(_request.getId());
            _blockWrapper = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockData(_request.getBlock().getId(), true);
            fillRowTermList();
        }
        onChangeRequestRows();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        switch (dataSource.getName())
        {
            case SessionReexaminationAddEdit.EDU_DOCUMENT_DS:
            {
                dataSource.put(PARAM_PERSON, getStudent().getPerson());
                break;
            }
            case SessionReexaminationAddEdit.RE_EXAMINATION_ACTION_DS:
            case SessionReexaminationAddEdit.RE_ATTESTATION_ACTION_DS:
            {
                dataSource.put(PARAM_BLOCK, getRequest().getBlock());
                dataSource.put(PARAM_RE_EXAMINATION_ACTION_LIST, _rowTermExamList);
                dataSource.put(PARAM_RE_ATTESTATION_ACTION_LIST, _rowTermAttestationList);
                break;
            }
            case SessionReexaminationManager.MARK_DS:
            {
                dataSource.put(SessionReexaminationManager.PARAM_REG_ELEMENT_PART_ID, _currentRequestRow.getRegElementPart().getId());
                dataSource.put(SessionReexaminationManager.PARAM_FCA_TYPE_ID, _currentFControlAction.getId());
                break;
            }
        }
    }

    public void validate()
    {
        InfoCollector info = ContextLocal.getInfoCollector();
        if (null == _blockWrapper) return;

        Map<PairKey<EppEpvRowTerm, Boolean>, SessionALRequestRow> currentRequestRowMap = Maps.newHashMap();
        currentRequestRowMap.putAll(getRequestRowMap(_rowTermExamList, false));
        currentRequestRowMap.putAll(getRequestRowMap(_rowTermAttestationList, true));

        Map<Long, IEppEpvRowWrapper> rowMap = _blockWrapper.getRowMap();
        Map<Integer, Term> termMap = DevelopGridDAO.getTermMap();
        Map<Long, List<Integer>> row2TermNumberMap = Maps.newHashMap();
        Map<Long, String> termRows = Maps.newHashMap();

        for (PairKey<EppEpvRowTerm, Boolean> keys : currentRequestRowMap.keySet())
        {
            EppEpvRowTerm rowTerm = keys.getFirst();
            SafeMap.safeGet(row2TermNumberMap, rowTerm.getRow().getId(), ArrayList.class).add(rowTerm.getTerm().getIntValue());
        }

        for (Map.Entry<PairKey<EppEpvRowTerm, Boolean>, SessionALRequestRow> entry : currentRequestRowMap.entrySet())
        {
            EppEpvRowTerm rowTerm = entry.getKey().getFirst();
            EppEpvRegistryRow row = (EppEpvRegistryRow) rowTerm.getRow();
            if (termRows.containsKey(row.getId())) continue;

            List<Integer> checkTermList = row2TermNumberMap.get(row.getId());
            IEppEpvRowWrapper rowWrapper = rowMap.get(row.getId());
            SortedSet<Integer> termSet = Sets.newTreeSet(rowWrapper.getActiveTermSet());

            Integer maxCheck = Collections.max(checkTermList);
            termSet.removeIf(term -> checkTermList.contains(term) || term > maxCheck);
            if (termSet.isEmpty()) continue;

            for (Integer term : termSet)
            {
                int termNumber = rowWrapper.getActiveTermNumber(term);
                Long regElementId = row.getRegistryElement().getId();

                IEppRegElWrapper regElWrap = IEppRegistryDAO.instance.get().getRegistryElementDataMap(Collections.singleton(regElementId)).get(regElementId);
                IEppRegElPartWrapper partWrap = regElWrap.getPartMap().get(termNumber);

                DevelopGrid developGrid = row.getOwner().getEduPlanVersion().getDevelopGridTerm().getDevelopGrid();
                DevelopGridTerm gridTerm = DataAccessServices.dao().getByNaturalId(new DevelopGridTerm.NaturalId(developGrid, termMap.get(term)));

                String title = partWrap.getItem().getTitleWithNumber() + " " + term + " " + gridTerm.getPart().getTitle();
                termRows.put(partWrap.getId(), title);
            }
        }

        if (!termRows.isEmpty())
        {
            info.add("Не выбраны мероприятия с меньшим номером семестра: " + (termRows.size() == 1 ? Iterables.get(termRows.values(), 0) : ""));
            if (termRows.size() > 1)
                termRows.values().forEach(info::add);
        }
    }

    public void onClickShow()
    {
        validate();

        fillRequestRow();
        onChangeRequestRows();
    }

    public void onChangeRequestRows()
    {
        _usedFcaTypes = SessionReexaminationManager.instance().dao().getUsedFcaTypes(_request.getBlock(), getRequestRows());
        _part2fcaMap = SessionReexaminationManager.instance().dao().getPart2FCATypeMap(getRequestRows());
    }

    public void onClickApply()
    {
        DataAccessServices.dao().saveOrUpdate(_request);

        List<SessionALRequestRow> requestRows = DataAccessServices.dao().getList(SessionALRequestRow.class, SessionALRequestRow.request().id(), _request.getId());
        List<SessionALRequestRow> mergeRequestRows = SessionReexaminationManager.instance().dao().mergeALRequestRows(_requestRowMap.values(), requestRows, true);
        SessionReexaminationManager.instance().dao().saveALRequestRowMarks(mergeRequestRows, _requestRowMarkMap, _part2fcaMap);
        deactivate();
    }

    // Utils

    private void fillRowTermList()
    {
        _rowTermAttestationList.clear();
        _rowTermExamList.clear();
        _requestRowMarkMap.clear();

        List<SessionALRequestRow> requestRows = DataAccessServices.dao().getList(SessionALRequestRow.class, SessionALRequestRow.request().id(), _request.getId());
        List<SessionALRequestRowMark> requestRowMarks = DataAccessServices.dao().getList(SessionALRequestRowMark.class, SessionALRequestRowMark.requestRow(), requestRows);

        SessionReexaminationManager.instance().dao().sortALRequestRows(requestRows);

        List<Long> regElementIds = requestRows.stream()
                .map(item -> item.getRegElementPart().getRegistryElement().getId())
                .collect(Collectors.toList());

        Map<Long, IEppRegElWrapper> regElementDataMap = IEppRegistryDAO.instance.get().getRegistryElementDataMap(regElementIds);

        for (SessionALRequestRow requestRow : requestRows)
        {
            EppEpvRowTerm rowTerm = requestRow.getRowTerm();
            EppEpvRegistryRow row = (EppEpvRegistryRow) rowTerm.getRow();
            Term term = rowTerm.getTerm();
            DevelopGrid developGrid = row.getOwner().getEduPlanVersion().getDevelopGridTerm().getDevelopGrid();
            DevelopGridTerm gridTerm = DataAccessServices.dao().getByNaturalId(new DevelopGridTerm.NaturalId(developGrid, term));

            IEppEpvRowWrapper rowWrapper = _blockWrapper.getRowMap().get(row.getId());
            int termNumber = rowWrapper.getActiveTermNumber(term.getIntValue());

            IEppRegElWrapper regElWrap = regElementDataMap.get(requestRow.getRegElementPart().getRegistryElement().getId());
            IEppRegElPartWrapper partWrap = regElWrap.getPartMap().get(termNumber);

            EppRegistryElementPart part = partWrap.getItem();
            String rowTermTitle = row.getStoredIndex() + " " + part.getTitleWithNumber() + term.getTitle() + " " + gridTerm.getPart().getTitle();

            DataWrapper wrapper = new DataWrapper(rowTerm.getId(), rowTermTitle, rowTerm);
            wrapper.setProperty(SessionEpvRegistryRowTermDSHandler.ROW_TERM_PART, part);

            if (requestRow.isNeedRetake())
                _rowTermAttestationList.add(wrapper);
            else
                _rowTermExamList.add(wrapper);
        }
        fillRequestRowMap(requestRows);
        for (SessionALRequestRowMark requestRowMark : requestRowMarks)
            _requestRowMarkMap.put(PairKey.create(requestRowMark.getRequestRow(), requestRowMark.getControlAction()), requestRowMark.getMark());
    }

    private void fillRequestRow()
    {
        List<SessionALRequestRow> requestRows;
        Map<PairKey<EppEpvRowTerm, Boolean>, SessionALRequestRow> currentRequestRowMap = Maps.newHashMap();
        currentRequestRowMap.putAll(getRequestRowMap(_rowTermExamList, false));
        currentRequestRowMap.putAll(getRequestRowMap(_rowTermAttestationList, true));

        if (_requestRowMap.isEmpty())
        {
            requestRows = Lists.newArrayList(currentRequestRowMap.values());
            SessionReexaminationManager.instance().dao().sortALRequestRows(requestRows);
        }
        else
            requestRows = SessionReexaminationManager.instance().dao().mergeALRequestRows(currentRequestRowMap.values(), _requestRowMap.values(), false);

        fillRequestRowMap(requestRows);
    }

    private void fillRequestRowMap(List<SessionALRequestRow> requestRow)
    {
        _requestRowMap = Maps.newLinkedHashMap();
        for (SessionALRequestRow row : requestRow)
            _requestRowMap.put(PairKey.create(row.getRowTerm(), row.isNeedRetake()), row);
    }

    private Map<PairKey<EppEpvRowTerm, Boolean>, SessionALRequestRow> getRequestRowMap(List<DataWrapper> wrappers, boolean reAttestation)
    {
        Map<PairKey<EppEpvRowTerm, Boolean>, SessionALRequestRow> requestRowMap = Maps.newHashMap();

        for (DataWrapper wrapper : wrappers)
        {
            EppEpvRowTerm rowTerm = wrapper.getWrapped();
            EppRegistryElementPart part = (EppRegistryElementPart) wrapper.getProperty(SessionEpvRegistryRowTermDSHandler.ROW_TERM_PART);
            PairKey<EppEpvRowTerm, Boolean> keys = PairKey.create(rowTerm, reAttestation);

            if (!requestRowMap.containsKey(keys)) {
                SessionALRequestRow requestRow = new SessionALRequestRow();
                requestRow.setRequest(_request);
                requestRow.setRowTerm(rowTerm);
                requestRow.setRegElementPart(part);
                requestRow.setNeedRetake(reAttestation);

                requestRowMap.put(keys, requestRow);
            }
        }
        return requestRowMap;
    }

    public int getCurrentRequestRowIndex()
    {
        return getRequestRows().indexOf(_currentRequestRow) + 1;
    }

    public String getCurrentRequestRowTitle()
    {
        EppEpvRowTerm rowTerm = _currentRequestRow.getRowTerm();
        EppRegistryElementPart part = _currentRequestRow.getRegElementPart();
        EppEpvTermDistributedRow row = rowTerm.getRow();
        Term term = rowTerm.getTerm();

        DevelopGrid developGrid = row.getOwner().getEduPlanVersion().getDevelopGridTerm().getDevelopGrid();
        DevelopGridTerm gridTerm = DataAccessServices.dao().getByNaturalId(new DevelopGridTerm.NaturalId(developGrid, term));

        return part.getTitleWithNumber() + " " + term.getTitle() + " " + gridTerm.getPart().getTitle();
    }

    public List<SessionALRequestRow> getRequestRows()
    {
        return Lists.newArrayList(_requestRowMap.values());
    }

    public String getCurrentRequestRowAttestationTitle()
    {
        return _currentRequestRow.isNeedRetake() ? "Переаттестация" : "Перезачет";
    }

    public boolean isHasRegElementPartFControlAction()
    {
        List<EppFControlActionType> fcaList = _part2fcaMap.get(_currentRequestRow.getRegElementPart());
        return null != fcaList && fcaList.contains(_currentFControlAction);
    }


    public List<EppFControlActionType> getUsedFcaTypes()
    {
        return _usedFcaTypes;
    }

    public int getActionTableTitleColspan()
    {
        return getUsedFcaTypes().size() + 6;
    }

    public SessionMarkGradeValueCatalogItem getCurrentRequestRowMark()
    {
        return _requestRowMarkMap.get(PairKey.create(_currentRequestRow, _currentFControlAction));
    }

    public void setCurrentRequestRowMark(SessionMarkGradeValueCatalogItem mark)
    {
        _requestRowMarkMap.put(PairKey.create(_currentRequestRow, _currentFControlAction), mark);
    }

    public boolean isAddForm()
    {
        return getRequest().getId() == null;
    }

    // Getters & Setters

    public Student getStudent()
    {
        return _student;
    }

    public void setStudent(Student student)
    {
        _student = student;
    }

    public SessionALRequest getRequest()
    {
        return _request;
    }

    public void setRequest(SessionALRequest request)
    {
        _request = request;
    }

    public List<DataWrapper> getRowTermExamList()
    {
        return _rowTermExamList;
    }

    public void setRowTermExamList(List<DataWrapper> rowTermExamList)
    {
        _rowTermExamList = rowTermExamList;
    }

    public List<DataWrapper> getRowTermAttestationList()
    {
        return _rowTermAttestationList;
    }

    public void setRowTermAttestationList(List<DataWrapper> rowTermAttestationList)
    {
        _rowTermAttestationList = rowTermAttestationList;
    }

    public SessionALRequestRow getCurrentRequestRow()
    {
        return _currentRequestRow;
    }

    public void setCurrentRequestRow(SessionALRequestRow currentRequestRow)
    {
        _currentRequestRow = currentRequestRow;
    }

    public EppFControlActionType getCurrentFControlAction()
    {
        return _currentFControlAction;
    }

    public void setCurrentFControlAction(EppFControlActionType currentFControlAction)
    {
        _currentFControlAction = currentFControlAction;
    }
}
