/* $Id$ */
package ru.tandemservice.unisession.component.student.StudentSessionProjectThemeTab;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.PropertyFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.MultiValuesColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.mark.SessionMark;

/**
 * @author oleyba
 * @since 6/22/11
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        model.setSettings(UniBaseUtils.getDataSettings(component, "StudentSessionMarkTab.filter"));
        this.getDao().prepare(model);

        if (null == model.getDataSource())
        {
            final DynamicListDataSource<SessionDocumentSlot> dataSource = new DynamicListDataSource<>(component, component1 -> {
                Controller.this.getDao().prepareListDataSource(model);
            });

            dataSource.addColumn(new SimpleColumn("Документ", SessionDocumentSlot.document().typeTitle().s()).setClickable(false).setOrderable(false));
            dataSource.addColumn(new SimpleColumn("Мероприятие", SessionDocumentSlot.studentWpeCAction().registryElementTitle().s()).setClickable(false).setOrderable(true));
            //dataSource.addColumn(new SimpleColumn("Форма контроля", SessionDocumentSlot.studentWpeCAction().type().title().s()).setClickable(false).setOrderable(true));
            dataSource.addColumn(new SimpleColumn("Семестр", SessionDocumentSlot.studentWpeCAction().termTitle().s()).setClickable(false).setOrderable(true));
            dataSource.addColumn(new SimpleColumn("Тема", "theme").setClickable(false).setOrderable(false));
            dataSource.addColumn(new SimpleColumn("Оценка", SessionMark.valueTitle().fromAlias("mark").s()).setClickable(false).setOrderable(false));
            dataSource.addColumn(new SimpleColumn("Дата сдачи", SessionMark.performDate().fromAlias("mark").s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false));
            dataSource.addColumn(new MultiValuesColumn("Преподаватель", "tutors").setFormatter(new CollectionFormatter(new PropertyFormatter(PpsEntry.person().identityCard().fio().s()))).setClickable(false).setOrderable(false));
            dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEdit").setPermissionKey("studentSessionProjectThemeEdit"));
            dataSource.setOrder(SessionDocumentSlot.studentWpeCAction().termTitle().s(), OrderDirection.asc);
            model.setDataSource(dataSource);
        }
    }

    public void onClickSearch(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        model.getDataSource().showFirstPage();
        model.getDataSource().refresh();
        DataSettingsFacade.saveSettings(model.getSettings());
    }

    public void onClickClear(final IBusinessComponent context)
    {
        this.getModel(context).getSettings().clear();
        this.onClickSearch(context);
    }

    public void onClickEdit(final IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                ru.tandemservice.unisession.component.sessionTheme.SessionProjectThemeAddEdit.Model.COMPONENT_NAME,
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, component.getListenerParameter())));
    }
}
