/**
 *$Id$
 */
package ru.tandemservice.unisession.base.bo.SessionSheet.ui.Forming;

import com.google.common.collect.Lists;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.base.bo.SessionReport.util.GroupModel;
import ru.tandemservice.unisession.base.bo.SessionSheet.SessionSheetManager;
import ru.tandemservice.unisession.entity.catalog.SessionsSimpleDocumentReason;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionStudentGradeBookDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 15.02.13
 */
@Input({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "orgUnitId", required=true),
        @Bind(key = SessionSheetFormingUI.PARAM_MARK_LIST, binding = "markList")
})
public class SessionSheetFormingUI extends UIPresenter
{
    public static final String PARAM_MARK_LIST = "markList";

    public static final String PARAM_EDU_YEAR_PART = "eduYearPart";
    public static final String PARAM_COURSE_LIST = "courseList";
    public static final String PARAM_GROUP_LIST = "groupList";
    public static final String PARAM_GROUP_ORG_UNIT = "groupOrgUnit";

    private Long _orgUnitId;
    private OrgUnit _orgUnit;

    private CommonPostfixPermissionModel _secModel;

    private Date _issueDate = new Date();
    private Date _validityDate;
    private SessionsSimpleDocumentReason _reason;
    private boolean _inSession;

    private EducationYear _year;
    private YearDistributionPart _yearPart;
    private List<Course> _courseList;

    private IMultiSelectModel _groupModel;
    private List<Group> _groupList;

    private List<Long> _markList;

    @Override
    public void onComponentRefresh()
    {
        _orgUnit = DataAccessServices.dao().getNotNull(OrgUnit.class, _orgUnitId);
        _secModel = new OrgUnitSecModel(_orgUnit);

        _year = EducationYear.getCurrentRequired();

        _groupModel = new GroupModel()
        {
            @Override
            protected DQLSelectBuilder getStudentDQL()
            {
                final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SessionMark.class, "m").column(property(SessionMark.slot().studentWpeCAction().studentWpe().student().id().fromAlias("m")))
                        .where(in(property(SessionMark.slot().document().id().fromAlias("m")),
                                new DQLSelectBuilder().fromEntity(SessionStudentGradeBookDocument.class, "g").column(property(SessionStudentGradeBookDocument.id().fromAlias("g")))
                                        .buildQuery()))
                        .where(eq(property(SessionMark.slot().studentWpeCAction().studentWpe().student().educationOrgUnit().groupOrgUnit().fromAlias("m")), value(_orgUnit)))
                        .where(eq(property(SessionMark.slot().inSession().fromAlias("m")), value(false)))
                        .where(eq(property(SessionMark.slot().studentWpeCAction().studentWpe().part().fromAlias("m")), value(_yearPart)))
                        .where(eq(property(SessionMark.slot().studentWpeCAction().studentWpe().year().educationYear().fromAlias("m")), value(_year)))
                        .where(eq(property(SessionMark.cachedMarkPositiveStatus().fromAlias("m")), value(false)))
                        .where(notExists(
                                new DQLSelectBuilder().fromEntity(SessionDocumentSlot.class, "s").column(property(SessionDocumentSlot.id().fromAlias("s")))
                                        .where(notIn(property(SessionDocumentSlot.document().id().fromAlias("s")),
                                                new DQLSelectBuilder().fromEntity(SessionStudentGradeBookDocument.class, "sg").column(property(SessionStudentGradeBookDocument.id().fromAlias("sg")))
                                                        .buildQuery()))
                                        .where(eq(property(SessionDocumentSlot.studentWpeCAction().fromAlias("s")), property(SessionMark.slot().studentWpeCAction().fromAlias("m"))))
                                        .where(notExists(
                                                new DQLSelectBuilder().fromEntity(SessionMark.class, "slotMark").column(property(SessionMark.id().fromAlias("slotMark")))
                                                        .where(eq(property(SessionMark.slot().fromAlias("slotMark")), property("s")))
                                                        .buildQuery()))
                                        .buildQuery()))
                        .predicate(DQLPredicateType.distinct);


                if (_courseList != null && !_courseList.isEmpty())
                    builder.where(in(property(SessionMark.slot().studentWpeCAction().studentWpe().student().course().fromAlias("m")), _courseList));

                return new DQLSelectBuilder().fromEntity(Student.class, "st").where(in(property(Student.id().fromAlias("st")), builder.buildQuery()));
            }
        };
    }

    public void onClickSearch()
    {

    }

    public void onClickClear()
    {
        _year = null;
        _yearPart = null;
        _courseList = null;
        _groupList = null;
        _markList = null;
    }

    public void onClickApply()
    {
        SessionSheetManager.instance().formingDao().doSaveSessionSheetDocumentList(getSelectedMarkIds(), _orgUnit, _issueDate, _validityDate, _reason, _inSession, false);
        deactivate();
    }

    public void onClickApplyAndPrint()
    {
        SessionSheetManager.instance().formingDao().doSaveSessionSheetDocumentList(getSelectedMarkIds(), _orgUnit, _issueDate, _validityDate, _reason, _inSession, true);
        deactivate();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(SessionReportManager.PARAM_EDU_YEAR_ONLY_CURRENT, false);
        dataSource.put(SessionReportManager.PARAM_EDU_YEAR, _year);
        dataSource.put(SessionReportManager.PARAM_ORG_UNIT, _orgUnit);

        dataSource.put(PARAM_GROUP_ORG_UNIT, _orgUnit);
        dataSource.put(PARAM_EDU_YEAR_PART, _yearPart);
        dataSource.put(PARAM_COURSE_LIST, _courseList);
        dataSource.put(PARAM_GROUP_LIST, _groupList);
        dataSource.put(PARAM_MARK_LIST, _markList);
    }

    // Getters & Setters

    public boolean isEmptyMarkList()
    {
        return _markList == null || _markList.isEmpty();
    }

    private List<Long> getSelectedMarkIds()
    {
        List<Long> result = Lists.newArrayList();
        if (isEmptyMarkList())
        {
            Collection<IEntity> selectedMarkList = getConfig().<BaseSearchListDataSource>getDataSource(SessionSheetForming.DEBTORS_SEARCH_DS).getOptionColumnSelectedObjects("check");
            if (selectedMarkList.isEmpty())
                throw new ApplicationException("Необходимо выбрать хотя бы одного должника из списка.");

            result.addAll(UniBaseDao.ids(selectedMarkList));
        }
        else
            result.addAll(getMarkList());
        return result;
    }

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        _secModel = secModel;
    }

    public Date getIssueDate()
    {
        return _issueDate;
    }

    public void setIssueDate(Date issueDate)
    {
        _issueDate = issueDate;
    }

    public Date getValidityDate()
    {
        return _validityDate;
    }

    public void setValidityDate(Date validityDate)
    {
        _validityDate = validityDate;
    }

    public SessionsSimpleDocumentReason getReason()
    {
        return _reason;
    }

    public void setReason(SessionsSimpleDocumentReason reason)
    {
        _reason = reason;
    }

    public boolean isInSession()
    {
        return _inSession;
    }

    public void setInSession(boolean inSession)
    {
        _inSession = inSession;
    }

    public EducationYear getYear()
    {
        return _year;
    }

    public void setYear(EducationYear year)
    {
        _year = year;
    }

    public YearDistributionPart getYearPart()
    {
        return _yearPart;
    }

    public void setYearPart(YearDistributionPart yearPart)
    {
        _yearPart = yearPart;
    }

    public List<Course> getCourseList()
    {
        return _courseList;
    }

    public void setCourseList(List<Course> courseList)
    {
        _courseList = courseList;
    }

    public List<Group> getGroupList()
    {
        return _groupList;
    }

    public void setGroupList(List<Group> groupList)
    {
        _groupList = groupList;
    }

    public IMultiSelectModel getGroupModel()
    {
        return _groupModel;
    }

    public void setGroupModel(IMultiSelectModel groupModel)
    {
        _groupModel = groupModel;
    }

    public List<Long> getMarkList()
    {
        return _markList;
    }

    public void setMarkList(List<Long> markList)
    {
        _markList = markList;
    }
}
