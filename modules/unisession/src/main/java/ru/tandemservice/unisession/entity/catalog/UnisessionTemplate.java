package ru.tandemservice.unisession.entity.catalog;

import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import ru.tandemservice.unisession.entity.catalog.gen.UnisessionTemplateGen;

/**
 * Печатные шаблоны документов модуля «Сессия»
 */
public class UnisessionTemplate extends UnisessionTemplateGen implements ITemplateDocument
{
    @Override
    public byte[] getContent()
    {
        return CommonBaseUtil.getTemplateContent(this);
    }
}