package ru.tandemservice.unisession.attestation.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unisession.attestation.entity.SessionAttestation;
import ru.tandemservice.unisession.entity.document.SessionObject;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Аттестация
 *
 * Межсессионная аттестация - проводится перед конкретной сессией (в конкретном семестре) и имеет порядковый номер проведения.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionAttestationGen extends EntityBase
 implements INaturalIdentifiable<SessionAttestationGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.attestation.entity.SessionAttestation";
    public static final String ENTITY_NAME = "sessionAttestation";
    public static final int VERSION_HASH = -1578646116;
    private static IEntityMeta ENTITY_META;

    public static final String P_NUMBER = "number";
    public static final String L_SESSION_OBJECT = "sessionObject";
    public static final String P_CLOSE_DATE = "closeDate";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_TITLE = "title";

    private int _number;     // Порядковый номер
    private SessionObject _sessionObject;     // Сессия
    private Date _closeDate;     // Дата закрытия (фактическая)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Порядковый номер. Свойство не может быть null.
     */
    @NotNull
    public int getNumber()
    {
        return _number;
    }

    /**
     * @param number Порядковый номер. Свойство не может быть null.
     */
    public void setNumber(int number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Сессия. Свойство не может быть null.
     */
    @NotNull
    public SessionObject getSessionObject()
    {
        return _sessionObject;
    }

    /**
     * @param sessionObject Сессия. Свойство не может быть null.
     */
    public void setSessionObject(SessionObject sessionObject)
    {
        dirty(_sessionObject, sessionObject);
        _sessionObject = sessionObject;
    }

    /**
     * @return Дата закрытия (фактическая).
     */
    public Date getCloseDate()
    {
        return _closeDate;
    }

    /**
     * @param closeDate Дата закрытия (фактическая).
     */
    public void setCloseDate(Date closeDate)
    {
        dirty(_closeDate, closeDate);
        _closeDate = closeDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SessionAttestationGen)
        {
            if (withNaturalIdProperties)
            {
                setNumber(((SessionAttestation)another).getNumber());
                setSessionObject(((SessionAttestation)another).getSessionObject());
            }
            setCloseDate(((SessionAttestation)another).getCloseDate());
        }
    }

    public INaturalId<SessionAttestationGen> getNaturalId()
    {
        return new NaturalId(getNumber(), getSessionObject());
    }

    public static class NaturalId extends NaturalIdBase<SessionAttestationGen>
    {
        private static final String PROXY_NAME = "SessionAttestationNaturalProxy";

        private int _number;
        private Long _sessionObject;

        public NaturalId()
        {}

        public NaturalId(int number, SessionObject sessionObject)
        {
            _number = number;
            _sessionObject = ((IEntity) sessionObject).getId();
        }

        public int getNumber()
        {
            return _number;
        }

        public void setNumber(int number)
        {
            _number = number;
        }

        public Long getSessionObject()
        {
            return _sessionObject;
        }

        public void setSessionObject(Long sessionObject)
        {
            _sessionObject = sessionObject;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof SessionAttestationGen.NaturalId) ) return false;

            SessionAttestationGen.NaturalId that = (NaturalId) o;

            if( !equals(getNumber(), that.getNumber()) ) return false;
            if( !equals(getSessionObject(), that.getSessionObject()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getNumber());
            result = hashCode(result, getSessionObject());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getNumber());
            sb.append("/");
            sb.append(getSessionObject());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionAttestationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionAttestation.class;
        }

        public T newInstance()
        {
            return (T) new SessionAttestation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "number":
                    return obj.getNumber();
                case "sessionObject":
                    return obj.getSessionObject();
                case "closeDate":
                    return obj.getCloseDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "number":
                    obj.setNumber((Integer) value);
                    return;
                case "sessionObject":
                    obj.setSessionObject((SessionObject) value);
                    return;
                case "closeDate":
                    obj.setCloseDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "number":
                        return true;
                case "sessionObject":
                        return true;
                case "closeDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "number":
                    return true;
                case "sessionObject":
                    return true;
                case "closeDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "number":
                    return Integer.class;
                case "sessionObject":
                    return SessionObject.class;
                case "closeDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionAttestation> _dslPath = new Path<SessionAttestation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionAttestation");
    }
            

    /**
     * @return Порядковый номер. Свойство не может быть null.
     * @see ru.tandemservice.unisession.attestation.entity.SessionAttestation#getNumber()
     */
    public static PropertyPath<Integer> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Сессия. Свойство не может быть null.
     * @see ru.tandemservice.unisession.attestation.entity.SessionAttestation#getSessionObject()
     */
    public static SessionObject.Path<SessionObject> sessionObject()
    {
        return _dslPath.sessionObject();
    }

    /**
     * @return Дата закрытия (фактическая).
     * @see ru.tandemservice.unisession.attestation.entity.SessionAttestation#getCloseDate()
     */
    public static PropertyPath<Date> closeDate()
    {
        return _dslPath.closeDate();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.attestation.entity.SessionAttestation#getShortTitle()
     */
    public static SupportedPropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.attestation.entity.SessionAttestation#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends SessionAttestation> extends EntityPath<E>
    {
        private PropertyPath<Integer> _number;
        private SessionObject.Path<SessionObject> _sessionObject;
        private PropertyPath<Date> _closeDate;
        private SupportedPropertyPath<String> _shortTitle;
        private SupportedPropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Порядковый номер. Свойство не может быть null.
     * @see ru.tandemservice.unisession.attestation.entity.SessionAttestation#getNumber()
     */
        public PropertyPath<Integer> number()
        {
            if(_number == null )
                _number = new PropertyPath<Integer>(SessionAttestationGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Сессия. Свойство не может быть null.
     * @see ru.tandemservice.unisession.attestation.entity.SessionAttestation#getSessionObject()
     */
        public SessionObject.Path<SessionObject> sessionObject()
        {
            if(_sessionObject == null )
                _sessionObject = new SessionObject.Path<SessionObject>(L_SESSION_OBJECT, this);
            return _sessionObject;
        }

    /**
     * @return Дата закрытия (фактическая).
     * @see ru.tandemservice.unisession.attestation.entity.SessionAttestation#getCloseDate()
     */
        public PropertyPath<Date> closeDate()
        {
            if(_closeDate == null )
                _closeDate = new PropertyPath<Date>(SessionAttestationGen.P_CLOSE_DATE, this);
            return _closeDate;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.attestation.entity.SessionAttestation#getShortTitle()
     */
        public SupportedPropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new SupportedPropertyPath<String>(SessionAttestationGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.attestation.entity.SessionAttestation#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(SessionAttestationGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return SessionAttestation.class;
        }

        public String getEntityName()
        {
            return "sessionAttestation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getShortTitle();

    public abstract String getTitle();
}
