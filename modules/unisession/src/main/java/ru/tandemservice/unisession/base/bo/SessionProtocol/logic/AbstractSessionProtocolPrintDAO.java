/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionProtocol.logic;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfElementFactory;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.bo.Person.logic.declination.IDeclinationDao;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unisession.entity.catalog.codes.SessionMarkStateCatalogItemCodes;
import ru.tandemservice.unisession.entity.catalog.codes.SessionRoleInGECCodes;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.stateFinalExam.SessionGECMember;
import ru.tandemservice.unisession.entity.stateFinalExam.SessionGovExamCommission;
import ru.tandemservice.unisession.entity.stateFinalExam.SessionStateFinalExamProtocol;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Andreev
 * @since 20.09.2016
 */
public abstract class AbstractSessionProtocolPrintDAO extends UniBaseDao implements ISessionProtocolPrintDAO
{
    protected static final String EMPTY_LINE_MEMBERS = "__________" + "__________" + "__________" + "__________" + "__________" + "____";

    /**
     * Оценка студента по ID записи студента по мероприятию в документе сессии
     */
    protected Map<Long, SessionMark> _markBySlotMap;
    /**
     * Члены коммисии по коду Роли и ID Коммисии
     */
    protected Map<Long, Map<String, List<SessionGECMember>>> _memberListByRoleByCommissions;


    @Override
    public RtfDocument printProtocols(Collection<Long> protocolIds)
    {
        RtfDocument template = getTemplate();

        initData(protocolIds);

        List<IRtfElement> elementList = new ArrayList<>();
        IRtfElementFactory elementFactory = RtfBean.getElementFactory();
        Iterator<Long> iterator = protocolIds.iterator();
        while (iterator.hasNext())
        {
            elementList.addAll(printProtocol(template.getClone(), iterator.next()).getElementList());
            if (iterator.hasNext())
            {
                elementList.add(elementFactory.createRtfControl(IRtfData.PARD));
                elementList.add(elementFactory.createRtfControl(IRtfData.PAR));

                elementList.add(elementFactory.createRtfControl(IRtfData.PAGE));

                elementList.add(elementFactory.createRtfControl(IRtfData.PARD));
                elementList.add(elementFactory.createRtfControl(IRtfData.PAR));
            }
        }

        RtfDocument document = new RtfDocument();
        document.setSettings(template.getSettings());
        document.setHeader(template.getHeader());
        document.setElementList(elementList);

        return document;
    }

    /**
     * @return шаблон используемый для печати протокола
     */
    protected abstract RtfDocument getTemplate();

    protected void initData(Collection<Long> protocolIds)
    {
        _markBySlotMap = new DQLSelectBuilder()
                .fromEntity(SessionMark.class, "mark")
                .column(property("mark"))
                .where(exists(SessionStateFinalExamProtocol.class,
                              SessionStateFinalExamProtocol.documentSlot().s(), property("mark", SessionMark.slot()),
                              SessionStateFinalExamProtocol.id().s(), protocolIds))
                .createStatement(getSession()).<SessionMark>list()
                .stream().collect(Collectors.toMap(mark -> mark.getSlot().getId(), mark -> mark));

        _memberListByRoleByCommissions = new DQLSelectBuilder()
                .fromEntity(SessionGECMember.class, "mb")
                .column(property("mb"))
                .where(exists(SessionStateFinalExamProtocol.class,
                              SessionStateFinalExamProtocol.id().s(), protocolIds,
                              SessionStateFinalExamProtocol.commission().s(), property("mb", SessionGECMember.commission())))

                .createStatement(getSession()).<SessionGECMember>list()
                .stream()
                .collect(groupingBy(member -> member.getCommission().getId(),
                                    mapping(member -> member, groupingBy(member -> member.getRoleInGEC().getCode(),
                                                                         mapping(member -> member, toList())))
                ));
    }

    protected RtfDocument printProtocol(RtfDocument template, Long protocolId)
    {
        SessionStateFinalExamProtocol protocol = get(SessionStateFinalExamProtocol.class, protocolId);
        Student student = protocol.getDocumentSlot().getStudentWpeCAction().getStudentWpe().getStudent();
        EducationLevelsHighSchool educationLevelHighSchool = student.getEducationOrgUnit().getEducationLevelHighSchool();
        EducationLevels educationLevel = educationLevelHighSchool.getEducationLevel();

        RtfInjectModifier im = new RtfInjectModifier();
        RtfTableModifier tm = new RtfTableModifier();

        TopOrgUnit academy = TopOrgUnit.getInstance();
        im.put("academyTitle", academy.getNominativeCaseTitle() == null ? academy.getTitle() : academy.getNominativeCaseTitle());

        OrgUnit ownerOrgUnit = educationLevelHighSchool.getOrgUnit();
        im.put("ownerOrgUnit", ownerOrgUnit.getNominativeCaseTitle() == null ? ownerOrgUnit.getTitle() : ownerOrgUnit.getNominativeCaseTitle());

        im.put("numProtocol", protocol.getNumber());


        SessionMark mark = _markBySlotMap.get(protocol.getDocumentSlot().getId());
        im.put("date", (mark != null && mark.getPerformDate() != null) ?
                DateFormatter.STRING_MONTHS_AND_QUOTES.format(mark.getPerformDate()) : "«_______» ________________ 20_____ г.");
        String code = mark == null ? null : mark.getValueItem().getCode();
        if (SessionMarkStateCatalogItemCodes.CODES.contains(code))
        {
            tm.remove("presence", 0, 1);
            UniRtfUtil.deleteRowsWithLabels(template, Collections.singletonList("absence"));
        }
        else
        {
            tm.remove("absence", 0, 1);
            UniRtfUtil.deleteRowsWithLabels(template, Collections.singletonList("presence"));
        }


        im.put("exam", protocol.getDocumentSlot().getStudentWpeCAction().getStudentWpe()
                .getRegistryElementPart().getRegistryElement().getTitle());

        EduProgramSubject eduProgramSubject = educationLevel.getEduProgramSubject();
        EduProgramKind eduProgramKind = eduProgramSubject == null ? null : eduProgramSubject.getEduProgramKind();
        String programKind = eduProgramKind == null ? null :
                eduProgramKind.getEduProgramSubjectKindTitle(InflectorVariantCodes.RU_DATIVE).toLowerCase();
        im.put("programKind", programKind);

        im.put("eduProgramSubject", educationLevel.getDisplayableTitle());

        EduProgramQualification eduProgramQualification = educationLevelHighSchool.getAssignedQualification();
        im.put("qualification", eduProgramQualification == null ? null : eduProgramQualification.getTitle());

        printStudent(im, protocol);
        printMembersOfGEC(im, protocol);

        modifyDocument(im, protocol);
        im.modify(template);

        modifyDocument(tm, protocol);
        tm.modify(template);

        return template;
    }

    protected void printStudent(RtfInjectModifier im, SessionStateFinalExamProtocol protocol)
    {
        Student student = protocol.getDocumentSlot().getStudentWpeCAction().getStudentWpe().getStudent();

        im.put("FIO", student.getFio());
        im.put("fullFIO", student.getFullFio());

        IdentityCard identityCard = student.getPerson().getIdentityCard();
        IDeclinationDao declinationDao = PersonManager.instance().declinationDao();
        im.put("genFullFIO", declinationDao.getDeclinationFIO(identityCard, GrammaCase.GENITIVE));
        im.put("instFIO", declinationDao.getDeclinationFIO(identityCard, GrammaCase.INSTRUMENTAL));

        im.put("studentGroup", student.getGroup().getTitle());
        im.put("personalNumber", student.getPersonalNumber());

        SessionMark mark = _markBySlotMap.get(protocol.getDocumentSlot().getId());
        im.put("mark", mark == null ? null : mark.getValueTitle());

        boolean isMale = student.getPerson().isMale();
        im.put("student_A", isMale ? "студента" : "студентки");
        im.put("student_B", isMale ? "студентом" : "студенткой");
        im.put("student_N", isMale ? "студент" : "студентка");
        im.put("student_T", isMale ? "студенту" : "студентке");
        im.put("whom", isMale ? "им" : "ею");
        im.put("toWhom", isMale ? "ему" : "ей");
        im.put("passed", isMale ? "сдал" : "сдала");
        im.put("appearedStudent", isMale ? "явившийся студент" : "явившаяся студентка");
        im.put("appear", isMale ? "явился" : "явилась");
    }

    protected void printMembersOfGEC(RtfInjectModifier im, SessionStateFinalExamProtocol protocol)
    {
        List<String> membersFioString = getMembersFioString(protocol.getCommission(), SessionRoleInGECCodes.CHAIRMAN);
        String chairman = membersFioString == null ? null : CoreStringUtils.join(membersFioString, ", ");
        im.put("presidentStateExamCommission", chairman);
        im.put("signaturePresidentCommission", chairman);

        membersFioString = getMembersFioString(protocol.getCommission(), SessionRoleInGECCodes.MEMBER);
        RtfString membersRtfString = new RtfString();
        if (CollectionUtils.isEmpty(membersFioString))
        {
            membersRtfString.append(EMPTY_LINE_MEMBERS)
                    .append(IRtfData.PAR).append(EMPTY_LINE_MEMBERS)
                    .append(IRtfData.PAR).append(EMPTY_LINE_MEMBERS);
        }
        else
        {
            Iterator<String> iterator = membersFioString.iterator();
            while (iterator.hasNext())
            {
                membersRtfString.append(iterator.next());
                if (iterator.hasNext())
                    membersRtfString.append(IRtfData.PAR);
            }
        }
        im.put("memberStateExamCommission", membersRtfString);

        membersFioString = getMembersFioString(protocol.getCommission(), SessionRoleInGECCodes.SECRETARY);
        String secretary = membersFioString == null ? null : CoreStringUtils.join(membersFioString, ", ");
        im.put("protocolWriter", secretary);
    }


    /**
     * для модификации документа в наследниках по обычным меткам
     */
    protected RtfInjectModifier modifyDocument(RtfInjectModifier modifier, SessionStateFinalExamProtocol protocol)
    {
        return modifier;
    }

    /**
     * для модификации документа в наследниках по табличным меткам
     */
    protected RtfTableModifier modifyDocument(RtfTableModifier modifier, SessionStateFinalExamProtocol protocol)
    {
        return modifier;
    }


    protected List<String> getMembersFioString(SessionGovExamCommission commission, String code)
    {
        if (commission == null) return null;

        Map<String, List<SessionGECMember>> membersByRoleMap = _memberListByRoleByCommissions.get(commission.getId());
        if (membersByRoleMap == null) return null;

        List<SessionGECMember> members = membersByRoleMap.get(code);
        if (members == null) return null;

        List<String> result = new ArrayList<>();
        members.forEach(member -> result.add(member.getPps().getFio()));

        return result;
    }
}