/* $Id: modelItemPub.vm 6177 2009-01-13 14:09:27Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisession.component.catalog.eppGradeScale.EppGradeScaleItemPub;

import java.util.Set;

import org.tandemframework.core.component.Input;
import org.tandemframework.core.runtime.EntityDataRuntime;
import org.tandemframework.core.tool.synchronization.SynchronizeMeta;
import org.tandemframework.core.view.list.column.IEntityHandler;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogItemPub.DefaultCatalogItemPubModel;

import ru.tandemservice.uniepp.entity.catalog.EppGradeScale;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;

/**
 * @author AutoGenerator            100101 2-3 307
 * Created on 15.02.2011
 */
@Input(keys = {"catalogCode"}, bindings = {"catalogCode"})
public class Model extends DefaultCatalogItemPubModel<EppGradeScale>
{

    private DynamicListDataSource<SessionMarkGradeValueCatalogItem> dataSource;
    public DynamicListDataSource<SessionMarkGradeValueCatalogItem> getDataSource() { return this.dataSource; }
    public void setDataSource(final DynamicListDataSource<SessionMarkGradeValueCatalogItem> dataSource) { this.dataSource = dataSource; }
    public IEntityHandler _disabledEntityHandler;
    public Set<Long> _disabledIds;
    public String _catalogCode;

    public String getCatalogCode(){return this._catalogCode;}

    public boolean isSystem()
    {
        return SynchronizeMeta.system == EntityDataRuntime.getEntityPolicy(this.getCatalogItem().getEntityMeta().getName()).getItemPolicy(this.getCatalogItem().getCode()).getSynchronize();
    }

    public IEntityHandler getDisabledEntityHandler()
    {
        return this._disabledEntityHandler;
    }

    public void setDisabledEntityHandler(final IEntityHandler disabledEntityHandler)
    {
        this._disabledEntityHandler = disabledEntityHandler;
    }

    public Set<Long> getDisabledIds()
    {
        return this._disabledIds;
    }

    public void setDisabledIds(final Set<Long> disabledIds)
    {
        this._disabledIds = disabledIds;
    }
}
