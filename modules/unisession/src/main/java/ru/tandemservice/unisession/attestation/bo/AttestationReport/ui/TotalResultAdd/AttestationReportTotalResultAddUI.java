/**
 *$Id$
 */
package ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.TotalResultAdd;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilter;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilterType;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterFormConfig;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unisession.attestation.bo.AttestationReport.AttestationReportManager;
import ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.TotalResultAdd.util.Model;
import ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.TotalResultPub.AttestationReportTotalResultPub;
import ru.tandemservice.unisession.attestation.entity.SessionAttestation;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlot;
import ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.base.bo.SessionReport.util.UniAttestationFilterAddon;

import java.util.List;

/**
 * @author Alexander Shaburov
 * @since 22.11.12
 */
@State
({
    @Bind(key = SessionReportManager.BIND_ORG_UNIT, binding = "orgUnitHolder.id")
})
public class AttestationReportTotalResultAddUI extends UIPresenter
{
    public static final String PARAM_ORG_UNIT = AttestationReportManager.PARAM_ORG_UNIT;

    private OrgUnitHolder _orgUnitHolder = new OrgUnitHolder();

    private boolean _activeFormativeOrgUnitList;

    private EducationYear _eduYear;
    private SessionAttestation _attestation;
    private YearDistributionPart _yearPart;
    private Long _attNumber = 1L;
    private DataWrapper _resultFor;
    private List<OrgUnit> _formativeOrgUnitList;

    @Override
    public void onComponentRefresh()
    {
        _eduYear = EducationYear.getCurrentRequired();
        getOrgUnitHolder().refresh();
        configUtil(getAttestationFilterAddon());

    }

    public void onClickApply()
    {
        Model model = new Model();
        model.setFormativeOrgUnitList(_formativeOrgUnitList);
        model.setAttestation(_attestation);
        model.setAttNumber(_attNumber);
        model.setOrgUnit(_orgUnitHolder.getValue());
        model.setEduYear(_eduYear);
        model.setYearPart(_yearPart);
        model.setFilterAddon(getAttestationFilterAddon());
        model.setResultFor(_resultFor);

        RtfDocument document = AttestationReportManager.instance().attestationTotalResultReportDao().createReportRtfDocument(model);
        SessionAttestationTotalResultReport report = AttestationReportManager.instance().attestationTotalResultReportDao().saveReport(model, document);

        deactivate();
        getActivationBuilder()
                .asDesktopRoot(AttestationReportTotalResultPub.class)
                .parameter(UIPresenter.PUBLISHER_ID, report.getId())
                .activate();
    }

    public boolean isOnOrgUnit()
    {
        return _orgUnitHolder.getId() != null;
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(AttestationReportManager.PARAM_EDU_YEAR_ONLY_CURRENT, true);
        dataSource.put(PARAM_ORG_UNIT, _orgUnitHolder.getValue());
    }

    // Calculate getters

    public UniAttestationFilterAddon getAttestationFilterAddon()
    {
        return (UniAttestationFilterAddon) getConfig().getAddon(CommonFilterAddon.class.getSimpleName());
    }

    @Override
    public ISecured getSecuredObject()
    {
        return _orgUnitHolder.getId() != null ? _orgUnitHolder.getValue() : super.getSecuredObject();
    }

    public CommonPostfixPermissionModelBase getSec()
    {
        return _orgUnitHolder.getId() != null ? _orgUnitHolder.getSecModel() : new CommonPostfixPermissionModel(null);
    }

    public String getAddStorableReportPermissionKey(){ return getSec().getPermission(_orgUnitHolder.getId() != null ? "addStorableReport" : "addSessionStorableReport"); }

    // Getters & Setters

    public OrgUnitHolder getOrgUnitHolder()
    {
        return _orgUnitHolder;
    }

    public SessionAttestation getAttestation()
    {
        return _attestation;
    }

    public void setAttestation(SessionAttestation attestation)
    {
        _attestation = attestation;
    }

    public EducationYear getEduYear()
    {
        return _eduYear;
    }

    public void setEduYear(EducationYear eduYear)
    {
        _eduYear = eduYear;
    }

    public YearDistributionPart getYearPart()
    {
        return _yearPart;
    }

    public void setYearPart(YearDistributionPart yearPart)
    {
        _yearPart = yearPart;
    }

    public Long getAttNumber()
    {
        return _attNumber;
    }

    public void setAttNumber(Long attNumber)
    {
        _attNumber = attNumber;
    }

    public DataWrapper getResultFor()
    {
        return _resultFor;
    }

    public void setResultFor(DataWrapper resultFor)
    {
        _resultFor = resultFor;
    }

    public List<OrgUnit> getFormativeOrgUnitList()
    {
        return _formativeOrgUnitList;
    }

    public void setFormativeOrgUnitList(List<OrgUnit> formativeOrgUnitList)
    {
        _formativeOrgUnitList = formativeOrgUnitList;
    }

    public boolean isActiveFormativeOrgUnitList()
    {
        return _activeFormativeOrgUnitList;
    }

    public void setActiveFormativeOrgUnitList(boolean activeFormativeOrgUnitList)
    {
        _activeFormativeOrgUnitList = activeFormativeOrgUnitList;
    }

    private void configUtil(UniAttestationFilterAddon util)
    {
        util
                .configDoubleWidthFilters(false)
                .configUseEnableCheckbox(true)
                .configSettings(getSettingsKey());


        util.clearFilterItems();

        util

                .addFilterItem(UniAttestationFilterAddon.COURSE, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(UniAttestationFilterAddon.COMPENSATION_TYPE, CommonFilterFormConfig.DEFAULT_CHECKBOX_SELECT_FILTER_CONFIG)
                .addFilterItem(UniAttestationFilterAddon.DEVELOP_FORM, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(UniAttestationFilterAddon.DEVELOP_CONDITION, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(UniAttestationFilterAddon.DEVELOP_TECH, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(UniAttestationFilterAddon.DEVELOP_PERIOD, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(UniAttestationFilterAddon.STUDENT_STATUS, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(UniAttestationFilterAddon.STUDENT_CUSTOM_STATE, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(UniAttestationFilterAddon.TARGET_ADMISSION, CommonFilterFormConfig.DEFAULT_CHECKBOX_SELECT_FILTER_CONFIG);


        configUtilWhere(util);
    }

    public void configUtilWhere(UniAttestationFilterAddon util)
    {
        util.clearWhereFilter();

        util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, SessionAttestationSlot.studentWpe().student().status().active(), true));

        if (null != _attestation) {
            util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, SessionAttestationSlot.bulletin().attestation(), _attestation));
            util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, SessionAttestationSlot.studentWpe().part(), _attestation.getSessionObject().getYearDistributionPart()));
        }
        final boolean onOrgUnit = null != _orgUnitHolder.getValue();
        if (!onOrgUnit)
            util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, SessionAttestationSlot.bulletin().attestation().number(), _attNumber.intValue()));

        if (onOrgUnit)
            util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, SessionAttestationSlot.studentWpe().student().educationOrgUnit().formativeOrgUnit(), getOrgUnitHolder().getValue()));
        if (null != _formativeOrgUnitList && isActiveFormativeOrgUnitList())
            util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.COLLECTION, SessionAttestationSlot.studentWpe().student().educationOrgUnit().formativeOrgUnit(), _formativeOrgUnitList));
        if (null != _eduYear)
            util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, SessionAttestationSlot.studentWpe().year().educationYear(), _eduYear));
        if (null != _yearPart)
            util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, SessionAttestationSlot.studentWpe().part(), _yearPart));

    }

    public void onChangeFilterFields()
    {
        configUtilWhere(getAttestationFilterAddon());
    }




}
