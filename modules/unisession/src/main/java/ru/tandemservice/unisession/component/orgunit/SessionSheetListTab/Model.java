/* $*/

package ru.tandemservice.unisession.component.orgunit.SessionSheetListTab;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.unisession.entity.document.SessionSheetDocument;

/**
 * @author oleyba
 * @since 2/28/11
 */
@Input({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "orgUnitHolder.id")
})
public class Model
{
    private final EntityHolder<OrgUnit> orgUnitHolder = new EntityHolder<OrgUnit>();
    private DynamicListDataSource<SessionSheetDocument> dataSource;

    private String settingsKey;
    private IDataSettings settings;
    private CommonPostfixPermissionModel sec;

    private ISelectModel yearModel;
    private ISelectModel partsModel;
    private ISelectModel stateModel;
    private ISelectModel registryElementModel;
    private ISelectModel courseCurrentModel;
    private ISelectModel groupCurrentModel;

    public IEntity getSecuredObject()
    {
        return this.getOrgUnit();
    }

    public Long getOrgUnitId()
    {
        return this.getOrgUnitHolder().getId();
    }

    public OrgUnit getOrgUnit()
    {
        return this.getOrgUnitHolder().getValue();
    }

    // getters and setters


    public EntityHolder<OrgUnit> getOrgUnitHolder()
    {
        return this.orgUnitHolder;
    }

    public DynamicListDataSource<SessionSheetDocument> getDataSource()
    {
        return this.dataSource;
    }

    public void setDataSource(final DynamicListDataSource<SessionSheetDocument> dataSource)
    {
        this.dataSource = dataSource;
    }

    public String getSettingsKey()
    {
        return this.settingsKey;
    }

    public void setSettingsKey(final String settingsKey)
    {
        this.settingsKey = settingsKey;
    }

    public IDataSettings getSettings()
    {
        return this.settings;
    }

    public void setSettings(final IDataSettings settings)
    {
        this.settings = settings;
    }

    public ISelectModel getStateModel()
    {
        return this.stateModel;
    }

    public void setStateModel(final ISelectModel stateModel)
    {
        this.stateModel = stateModel;
    }

    public CommonPostfixPermissionModel getSec()
    {
        return this.sec;
    }

    public void setSec(final CommonPostfixPermissionModel sec)
    {
        this.sec = sec;
    }

    public ISelectModel getYearModel()
    {
        return yearModel;
    }

    public void setYearModel(ISelectModel yearModel)
    {
        this.yearModel = yearModel;
    }

    public ISelectModel getPartsModel()
    {
        return partsModel;
    }

    public void setPartsModel(ISelectModel partsModel)
    {
        this.partsModel = partsModel;
    }

    public ISelectModel getRegistryElementModel()
    {
        return registryElementModel;
    }

    public void setRegistryElementModel(ISelectModel registryElementModel)
    {
        this.registryElementModel = registryElementModel;
    }

    public ISelectModel getCourseCurrentModel()
    {
        return courseCurrentModel;
    }

    public void setCourseCurrentModel(ISelectModel courseCurrentModel)
    {
        this.courseCurrentModel = courseCurrentModel;
    }

    public ISelectModel getGroupCurrentModel()
    {
        return groupCurrentModel;
    }

    public void setGroupCurrentModel(ISelectModel groupCurrentModel)
    {
        this.groupCurrentModel = groupCurrentModel;
    }
}
