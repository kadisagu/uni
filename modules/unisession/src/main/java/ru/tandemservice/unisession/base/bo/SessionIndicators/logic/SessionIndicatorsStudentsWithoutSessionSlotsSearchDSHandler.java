/**
 *$Id$
 */
package ru.tandemservice.unisession.base.bo.SessionIndicators.logic;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.base.bo.UniStudent.logic.abstractHandler.AbstractStudentSearchListDSHandler;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.gen.EppRegistryElementGen;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow;
import ru.tandemservice.uniepp.entity.workplan.gen.EppWorkPlanRowGen;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 20.03.13
 */
public class SessionIndicatorsStudentsWithoutSessionSlotsSearchDSHandler extends AbstractStudentSearchListDSHandler
{
    public static final String VIEW_PROP_BROKEN_SLOT_LIST = "brokenSlotList";

    private static final String STUDENT_2_SLOT_LIST_MAP = "student2slotListMap";

    public SessionIndicatorsStudentsWithoutSessionSlotsSearchDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected void addAdditionalRestrictions(DQLSelectBuilder builder, String alias, DSInput input, ExecutionContext context)
    {
        super.addAdditionalRestrictions(builder, alias, input, context);

        // только неархивные студенты
        builder.where(eq(property(Student.archival().fromAlias(alias)), value(false)));

        // студенты у которых есть активные МСРП-по-ФК и нет слотов в сессии
        builder.where(exists(
                new DQLSelectBuilder().fromEntity(EppStudentWpeCAction.class, "cas").column(property("cas.id"))
                        .where(eq(property(EppStudentWpeCAction.studentWpe().student().fromAlias("cas")), property(alias)))
                        .where(isNull(property(EppStudentWpeCAction.removalDate().fromAlias("cas"))))
                        .where(notExists(
                                new DQLSelectBuilder()
                                        .fromEntity(SessionDocumentSlot.class, "sds").column(property("sds.id"))
                                        .where(eq(property(SessionDocumentSlot.studentWpeCAction().fromAlias("sds")), property("cas")))
                                        .buildQuery()))
                        .buildQuery()));
    }

    @Override
    protected void prepareWrappData(List<Long> recordIds, DSInput input, ExecutionContext context)
    {
        super.prepareWrappData(recordIds, input, context);

        final Map<Long, List<EppStudentWpeCAction>> student2slotListMap = SafeMap.get(ArrayList.class);

        for (List<Long> elements : Lists.partition(recordIds, DQL.MAX_VALUES_ROW_NUMBER)) {

            final MQBuilder slotBuilder = new MQBuilder(EppStudentWpeCAction.ENTITY_CLASS, "cas");
            slotBuilder.add(MQExpression.isNull("cas", EppStudentWpeCAction.removalDate().s()));
            slotBuilder.add(MQExpression.notIn("cas", "id", new MQBuilder(SessionDocumentSlot.ENTITY_CLASS, "sds", new String[] { SessionDocumentSlot.studentWpeCAction().id().s() })));
            slotBuilder.add(MQExpression.in("cas", EppStudentWpeCAction.studentWpe().student().id().s(), elements));
            slotBuilder.addJoinFetch("cas", EppStudentWpeCAction.studentWpe().s(), "slot");
            slotBuilder.addJoinFetch("slot", EppStudentWorkPlanElement.sourceRow().s(), "source");
            slotBuilder.addJoinFetch("slot", EppStudentWorkPlanElement.registryElementPart().s(), "regelpart");
            slotBuilder.addJoinFetch("regelpart", EppRegistryElementPart.registryElement().s(), "regel");

            slotBuilder.addOrder("slot", EppStudentWorkPlanElement.year().educationYear().intValue().s());
            slotBuilder.addOrder("slot", EppStudentWorkPlanElement.term().intValue().s());
            slotBuilder.addOrder("cas", EppStudentWpeCAction.type().priority().s());
            slotBuilder.addOrder("source", EppWorkPlanRowGen.number().s());
            slotBuilder.addOrder("regel", EppRegistryElementGen.title().s());
            slotBuilder.addOrder("regel", "id");

            slotBuilder.setReadOnly(true);

            for (final EppStudentWpeCAction slot: slotBuilder.<EppStudentWpeCAction>getResultList(context.getSession()))
                student2slotListMap.get(slot.getStudentWpe().getStudent().getId()).add(slot);
        }

        context.put(STUDENT_2_SLOT_LIST_MAP, student2slotListMap);
    }

    @Override
    protected void wrap(DataWrapper wrapper, ExecutionContext context)
    {
        super.wrap(wrapper, context);

        final Map<Long, List<EppStudentWpeCAction>> student2slotListMap = context.get(STUDENT_2_SLOT_LIST_MAP);

        final StringBuilder builder = new StringBuilder();

        long id = 0;
        for (final EppStudentWpeCAction cas: student2slotListMap.get(wrapper.getId()))
        {
            if (id != cas.getStudentWpe().getTerm().getId()) {
                id = cas.getStudentWpe().getTerm().getId();
                if (builder.length() > 0) { builder.append('\n'); }
            }
            builder.append(cas.getStudentWpe().getYear().getEducationYear().getTitle());
            builder.append(' ').append(cas.getStudentWpe().getPart().getShortTitleWithParent());
            builder.append(' ').append(cas.getStudentWpe().getTerm().getIntValue()).append(" с. (").append(cas.getStudentWpe().getCourse().getIntValue()).append(" к.").append(")");
            final EppWorkPlanRow sourceRow = cas.getStudentWpe().getSourceRow();
            if (null != sourceRow) {
                builder.append(' ').append(StringUtils.trimToEmpty(sourceRow.getNumber()));
                final String realTitle = sourceRow.getTitle();
                final int i = realTitle.indexOf(' ', 20);
                final String trimmedTitle = StringUtils.left(realTitle, (i < 1 ? 20 : i));
                if ((trimmedTitle.length()+3) < realTitle.length()) {
                    builder.append(' ').append(trimmedTitle).append("...");
                } else {
                    builder.append(' ').append(realTitle);
                }
            }
            builder.append(" (").append(cas.getStudentWpe().getRegistryElementPart().getNumber()).append("/").append(cas.getStudentWpe().getRegistryElementPart().getRegistryElement().getParts());
            builder.append(", ").append(UniEppUtils.formatLoad(cas.getStudentWpe().getRegistryElementPart().getRegistryElement().getSizeAsDouble(), false));
            builder.append(", ").append(cas.getType().getShortTitle()).append(")");
            builder.append("\n");

        }

        wrapper.setProperty(VIEW_PROP_BROKEN_SLOT_LIST, builder.toString());
    }
}
