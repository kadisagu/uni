/* $Id$ */
package ru.tandemservice.unisession.component.student.StudentSessionMarkTab;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.MultiValuesColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.entity.mark.SessionMark;

/**
 * @author oleyba
 * @since 4/8/11
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        model.setSettings(UniBaseUtils.getDataSettings(component, "StudentSessionMarkTab.filter"));
        this.getDao().prepare(model);

        if (null == model.getDataSource())
        {
            final DynamicListDataSource<EppStudentWpeCAction> dataSource = UniBaseUtils.createDataSource(component, this.getDao());
            dataSource.addColumn(new SimpleColumn("Мероприятие", EppStudentWpeCAction.studentWpe().registryElementPart().titleWithNumber().s()).setClickable(false).setOrderable(true));
            //dataSource.addColumn(new BooleanColumn("Обяз-ное", EppStudentWpeCAction.P_MANDATORY));
            dataSource.addColumn(new SimpleColumn("Форма контроля", EppStudentWpeCAction.type().title().s()).setClickable(false).setOrderable(true));
            dataSource.addColumn(new SimpleColumn("Семестр", EppStudentWpeCAction.termTitle().s()).setClickable(false).setOrderable(true));
            dataSource.addColumn(new SimpleColumn("Оценка в сессию", SessionMark.valueTitle().fromAlias("sessionMark").s()).setClickable(false).setOrderable(false));
            dataSource.addColumn(new SimpleColumn("Итоговая оценка", SessionMark.valueTitle().fromAlias("totalMark").s()).setClickable(false).setOrderable(false));
            dataSource.addColumn(new SimpleColumn("Дата сдачи", SessionMark.performDate().fromAlias("totalMark").s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false));
            addAdditionalMarkColumns(model, dataSource);
            dataSource.addColumn(new MultiValuesColumn("Преподаватель", "tutors").setFormatter(CollectionFormatter.COLLECTION_FORMATTER).setClickable(false).setOrderable(false));
            dataSource.addColumn(new ActionColumn("Всего оценок", "", "onClickShowHistory").setTextKey("markCount").setDisplayHeader(true).setPermissionKey("viewMarkHistoryStudentSessionMarkTab"));
            dataSource.setOrder(EppStudentWpeCAction.termTitle().s(), OrderDirection.asc);
            model.setDataSource(dataSource);
        }
    }

    public void onClickSearch(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        model.getDataSource().showFirstPage();
        model.getDataSource().refresh();
        DataSettingsFacade.saveSettings(model.getSettings());
    }

    public void onClickClear(final IBusinessComponent context)
    {
        this.getModel(context).getSettings().clear();
        this.onClickSearch(context);
    }

    public void onClickShowHistory(final IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                ru.tandemservice.unisession.component.student.ControlActionMarkHistory.Model.COMPONENT_NAME,
                new ParametersMap().add("controlActionId", component.getListenerParameter()).add("showAsSingle", true)));
    }

    protected void addAdditionalMarkColumns(Model model, DynamicListDataSource<EppStudentWpeCAction> dataSource)
    {
        if (model.isShowCurrentRating()) {
            dataSource.addColumn(new SimpleColumn("Балл за сдачу мероприятия", "scoredPoints").setClickable(false).setOrderable(false));
            dataSource.addColumn(new SimpleColumn("Текущий рейтинг", "fixedCurrentRating").setClickable(false).setOrderable(false));
            dataSource.addColumn(new SimpleColumn("Суммарный рейтинг", "totalMarkPoints").setClickable(false).setOrderable(false));
        }
        else if (model.isShowPoints())
            dataSource.addColumn(new SimpleColumn("Суммарный рейтинг", "totalMarkPoints").setClickable(false).setOrderable(false));
    }
}