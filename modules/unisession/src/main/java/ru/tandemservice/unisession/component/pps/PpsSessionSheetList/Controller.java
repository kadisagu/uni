// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisession.component.pps.PpsSessionSheetList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeFCA;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionSheetDocument;
import ru.tandemservice.unisession.entity.document.SessionSimpleDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.HashMap;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author iolshvang
 * @since 11.04.2011
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        model.setPrincipalContext(component.getUserContext().getPrincipalContext());
        this.getDao().prepare(model);

        model.setSettings(component.getSettings());

        final DynamicListDataSource<SessionSheetDocument> dataSource = UniBaseUtils.createDataSource(component, this.getDao());

        dataSource.addColumn(new PublisherLinkColumn("Номер", SessionSimpleDocument.number()).setResolver(new IPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                return new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, entity.getId());
            }

            @Override
            public String getComponentName(IEntity entity)
            {
                return "ru.tandemservice.unisession.component.sessionSheet.SessionSheetPubForPps";
            }
        }));


        final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppFControlActionType.class, "fca")
                .column(property("fca"))
                .column(property("fca", EppFControlActionType.eppGroupType()));

        final List<Object[]> list = DataAccessServices.dao().getList(builder);

        final HashMap<EppGroupTypeFCA, EppFControlActionType> groupTypeToActionType = new HashMap<>();

        for (Object[] objects : list) {
            groupTypeToActionType.put((EppGroupTypeFCA)objects[1], (EppFControlActionType)objects[0]);
        }


        dataSource.addColumn(new SimpleColumn("Мероприятие", "slot." + SessionDocumentSlot.studentWpeCAction(), source -> {
            final EppStudentWpeCAction element = ((EppStudentWpeCAction) source);
            return element.getStudentWpe().getRegistryElementPart().getTitleWithNumber() + ", " +  groupTypeToActionType.get(element.getType()).getTitle().toLowerCase();
        }).setOrderable(true).setClickable(false));


        dataSource.addColumn(new SimpleColumn("Студент", "slot." + SessionDocumentSlot.actualStudent().person().identityCard().fullFio().s()).setOrderable(true).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Группа", "slot." + SessionDocumentSlot.actualStudent().group().title().s()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Семестр",  "slot." + SessionDocumentSlot.termTitle().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата\n выдачи", SessionSimpleDocument.formingDate(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Оценка", "mark." + SessionMark.valueShortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Дата\n сдачи", "mark." + SessionMark.performDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("В который раз сдает", "slot." + SessionDocumentSlot.tryNumber().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Выставить оценку", "edit_mark", "onClickMark"));

        model.setDataSource(dataSource);
    }

    public void onClickSearch(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        model.getDataSource().showFirstPage();
        model.getDataSource().refresh();
        DataSettingsFacade.saveSettings(model.getSettings());
    }

    public void onClickClear(final IBusinessComponent context)
    {
        this.getModel(context).getSettings().clear();
        this.onClickSearch(context);
    }

    public void onClickMark(final IBusinessComponent context)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                ru.tandemservice.unisession.component.sessionSheet.SessionSheetMark.Model.COMPONENT_NAME,
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, context.<Long>getListenerParameter())));
    }
}