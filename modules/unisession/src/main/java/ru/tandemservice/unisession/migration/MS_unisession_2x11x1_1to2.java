package ru.tandemservice.unisession.migration;

import com.google.common.primitives.Ints;
import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLSelectQuery;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;
import ru.tandemservice.uni.migration.MS_uni_2x11x1_1to2;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Поменять приоритет оценок из шкал на противоположный (чтобы наименьшее значение поля соответствовало наивысшему приоритету, как и везде в системе).
 * Значения меняются с некоторого порядка (когда большее значение поля "приоритет" соответствует большему приоритету) на значения {1, 2,... N}, где 1 соответствует самому высокому приоритету,
 * N - самому низкому.
 * Раньше миграция ошибочно была расположена в модуле uni ({@link MS_uni_2x11x1_1to2}), поэтому сначала нужно проверить, не была ли уже осуществлена та миграция.
 * @author avedernikov
 * @since 06.02.2017
 */
@SuppressWarnings({"unused"})
public class MS_unisession_2x11x1_1to2 extends IndependentMigrationScript
{
	@Override
	public ScriptDependency[] getBoundaryDependencies()
	{
		return new ScriptDependency[]
				{
						new ScriptDependency("org.tandemframework", "1.6.18"),
						new ScriptDependency("org.tandemframework.shared", "1.11.1")
				};
	}

	@Override
	public void run(DBTool tool) throws Exception
	{
		if (alreadyMigrated(tool))
			return;
		Collection<Long> scaleIds = getAllMarkScales(tool);
		for (long scaleId : scaleIds)
			invertMarksPriority(scaleId, tool);
	}

	/**
	 * Была ли уже выполнена миграция {@link MS_uni_2x11x1_1to2}. Предполагаем, что пятибальную шкалу и оценки из нее никто не удалил, а также не изменил системный код оценок
	 * (по идее, ничего из этого нельзя сделать из пользовательского интерфейса); а также что никто в здравом уме не сделает оценку Хорошо выше по приоритету, чем Отлично.
	 */
	private static boolean alreadyMigrated(DBTool tool) throws SQLException
	{
		SQLFrom tables = SQLFrom.table("session_c_mark_grade_t", "gradeMark").innerJoin(SQLFrom.table("session_c_mark_base_t", "baseMark"), "gradeMark.id=baseMark.id");
		SQLSelectQuery markQuery = new SQLSelectQuery().from(tables).column("baseMark.code_p").column("gradeMark.priority_p");
		List<Object[]> rows = tool.executeQuery(MigrationUtils.processor(String.class, Integer.class), tool.getDialect().getSQLTranslator().toSql(markQuery));
		int fivePriority = 0, fourPriority = 0;
		for (Object[] row : rows)
		{
			if (row[0].equals("scale5.5"))
				fivePriority = (Integer)row[1];
			if (row[0].equals("scale5.4"))
				fourPriority = (Integer)row[1];
		}
		return fivePriority < fourPriority;
	}

	/** Получить id всех шкал оценок. */
	private static Collection<Long> getAllMarkScales(DBTool tool) throws SQLException
	{
		SQLSelectQuery scaleQuery = new SQLSelectQuery().from(SQLFrom.table("epp_c_gradescale_t", "scale")).column("scale.id");
		List<Object[]> rows = tool.executeQuery(MigrationUtils.processor(Long.class), tool.getDialect().getSQLTranslator().toSql(scaleQuery));
		return rows.stream().map(row -> (Long)row[0]).collect(Collectors.toList());
	}

	/** Инвертировать приоритет оценок для конкретной шкалы (чтобы стало {1, 2, ... N}, где 1 - у наиболее приоритетной, N - у наименее). */
	private static void invertMarksPriority(long scaleId, DBTool tool) throws SQLException
	{
		SQLSelectQuery scaleMarkQuery = new SQLSelectQuery().from(SQLFrom.table("session_c_mark_grade_t", "mark"))
				.column("mark.id").column("mark.priority_p")
				.where("mark.scale_id=?")
				.order("mark.priority_p", false);
		List<Object[]> rows = tool.executeQuery(MigrationUtils.processor(Long.class, Integer.class), tool.getDialect().getSQLTranslator().toSql(scaleMarkQuery), scaleId);

		List<Integer> currPriorities = rows.stream().map((Object[] row) -> (Integer) row[1]).collect(Collectors.toList());
		List<Long> markIds = rows.stream().map((Object[] row) -> (Long) row[0]).collect(Collectors.toList());

		updateMarksPriorityConstraintSafe(markIds, currPriorities, tool);
	}

	/**
	 * Обновить приоритет оценок с учетом констрейнта "session_c_mark_grade_pri" (уникальность пары { шкала, приоритет } ). Сначала выставляются приоритеты, значения которых лежат
	 * в безопасном диапазоне, затем приоритеты выставляются снова, на этот раз как значения { 1, 2,... currPriorities.size }. Безопасный диапазон не должен пересекать ни старый, ни новый,
	 * например - лежать правее обоих.
	 * Оценок в системе немного, поэтому можно выставлять им приоритеты аж по два раза без всяких оптимизаций.
	 * @param markIds id оценок (предполагается, что все эти оценки принадлежат одной шкале, а других оценок из этой шкалы нет).
	 * @param currPriorities Текущие значения приоритетов.
	 * @param tool Тулза для работы с базой.
	 * @throws SQLException
	 */
	private static void updateMarksPriorityConstraintSafe(List<Long> markIds, List<Integer> currPriorities, DBTool tool) throws SQLException
	{
		final int maxPriority = currPriorities.stream().max(Comparator.naturalOrder()).orElse(0);
		final int minPriority = currPriorities.stream().min(Comparator.naturalOrder()).orElse(0);
		final int bigEnoughIncrement = (minPriority >= 0) ? (maxPriority + 1) : (maxPriority - minPriority + 1);

		List<Integer> increasedPriorities = currPriorities.stream().map(p -> p + bigEnoughIncrement).collect(Collectors.toList());
		updateMarksPriority(markIds, increasedPriorities, tool);

		int[] newPriorities = IntStream.rangeClosed(1, currPriorities.size()).toArray();
		updateMarksPriority(markIds, Ints.asList(newPriorities), tool);
	}

	/**
	 * Число такое, что диапазон, в котором лежат числа { currPriorities + x } лежал бы правее, чем диапазон, в котором лежат числа из currPriorities, а также числа из нового диапазона
	 * (т.е. { 1, 2, ... currPriorities.size }).<br/>
	 * Т.е. ((minP + x) > maxP) & ((minP + x) > sz)
	 * <br/> => <br/>
	 * (x > maxP - minP) & (x > sz - minP)
	 * <br/> => <br/>
	 * x > MAX((maxP - minP), (sz - minP))
	 */
	private static int calculateBigEnoughIncrement(List<Integer> currPriorities)
	{
		final int maxPriority = currPriorities.stream().max(Comparator.naturalOrder()).orElse(0);
		final int minPriority = currPriorities.stream().min(Comparator.naturalOrder()).orElse(0);
		final int maxNew = currPriorities.size();
		return Math.max(maxPriority - minPriority, maxNew - minPriority) + 1;
	}

	/**
	 * Выставить оценкам из шкалы новые значения приоритетов (i-й оценке - i-е значение приоритета).
	 * @param markIds id оценок.
	 * @param priorities Новые значения приоритета для каждой оценки.
	 * @param tool Тулза для работы с базой.
	 * @throws SQLException
	 */
	private static void updateMarksPriority(List<Long> markIds, List<Integer> priorities, DBTool tool) throws SQLException
	{
		MigrationUtils.BatchUpdater updater = new MigrationUtils.BatchUpdater("update session_c_mark_grade_t set priority_p=? where id=?", DBType.INTEGER, DBType.LONG);
		for (int i = 0; i < markIds.size(); i++)
			updater.addBatch(priorities.get(i), markIds.get(i));
		updater.executeUpdate(tool);
	}
}
