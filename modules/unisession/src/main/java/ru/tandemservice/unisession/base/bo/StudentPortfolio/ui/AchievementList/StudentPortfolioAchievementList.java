package ru.tandemservice.unisession.base.bo.StudentPortfolio.ui.AchievementList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.util.Icon;
import ru.tandemservice.unisession.base.bo.StudentPortfolio.logic.AchievementDSHandler;
import ru.tandemservice.unisession.entity.catalog.StudentPortfolioElement;

/**
 * @author avedernikov
 * @since 26.11.2015
 */

@Configuration
public class StudentPortfolioAchievementList extends BusinessComponentManager
{
	public static final String CONTAINING_WPE_DS = "containingWpeDS";
	public static final String ACHIEVEMENT_DS = "achievementDS";

	public static final long SHOW_WITHOUT_WPE = 0L;
	public static final long SHOW_WITH_WPE = 1L;

	@Bean
	@Override
	public PresenterExtPoint presenterExtPoint()
	{
		return this.presenterExtPointBuilder()
				.addDataSource(selectDS(CONTAINING_WPE_DS, containigWpeDSHandler()))
				.addDataSource(searchListDS(ACHIEVEMENT_DS, achievementDSColumn(), achievementDSHandler()))
				.create();
	}

	@Bean
	public ColumnListExtPoint achievementDSColumn()
	{
		return columnListExtPointBuilder(ACHIEVEMENT_DS)
				.addColumn(textColumn("title").path(StudentPortfolioElement.title()).order())
				.addColumn(textColumn("wpeCAction").path(StudentPortfolioElement.wpeCAction().type().title()).order())
				.addColumn(textColumn("marks").path(AchievementDSHandler.COLUMN_MARKS))
				.addColumn(actionColumn("downloadFile", new Icon("template_save", "Выгрузка документа"), "onClickDownloadFile").disabled(AchievementDSHandler.FILE_DISABLED))
				.addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).permissionKey("studentPortfolioAchievementListEdit"))
				.addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER)
						.permissionKey("studentPortfolioAchievementListDelete")
						.alert(alert(ACHIEVEMENT_DS + ".delete.alert", StudentPortfolioElement.title())))
				.create();
	}

	@Bean
	public IDefaultComboDataSourceHandler containigWpeDSHandler()
	{
		return new SimpleTitledComboDataSourceHandler(getName())
				.addRecord(SHOW_WITHOUT_WPE, "без мероприятий")
				.addRecord(SHOW_WITH_WPE, "только с мероприятиями");
	}

	@Bean
	public IBusinessHandler<DSInput, DSOutput> achievementDSHandler()
	{
		return new AchievementDSHandler(getName());
	}
}