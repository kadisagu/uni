/* $*/

package ru.tandemservice.unisession.component.sessionSheet.SessionSheetMark;

import ru.tandemservice.uni.dao.IListDataSourceDao;
import ru.tandemservice.uni.dao.IUpdateable;

/**
 * @author oleyba
 * @since 2/22/11
 */
public interface IDAO<M extends Model> extends IUpdateable<M>, IListDataSourceDao<M>
{
    void prepareCurrentRatingData(M model);
}
