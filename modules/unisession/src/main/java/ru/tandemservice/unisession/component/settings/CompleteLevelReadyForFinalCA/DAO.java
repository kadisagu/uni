/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unisession.component.settings.CompleteLevelReadyForFinalCA;


import java.util.List;

import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel;
import ru.tandemservice.unisession.dao.document.ISessionDocumentBaseDAO;

/**
 * @author iolshvang
 * @since 30.05.11 12:05
 */
public class DAO  extends UniDao<Model> implements IDAO
{

    @Override
    public void prepare(Model model)
    {
        model.setFormAnyStates(isFormAnyStates());
    }

    @Override
    public void prepareListDataSource(final Model model)
    {
        final DynamicListDataSource<EppRealEduGroupCompleteLevel> dataSource = model.getDataSource();
        final List<EppRealEduGroupCompleteLevel> list = this.getList(EppRealEduGroupCompleteLevel.class, EppRealEduGroupCompleteLevel.CATALOG_ITEM_CODE);
        dataSource.setCountRow(list.size());
        dataSource.setTotalSize(list.size());
        dataSource.createPage(list);
    }

    @Override
    public void doToggleReadyForFCA(final Long id)
    {
        final EppRealEduGroupCompleteLevel level = this.get(EppRealEduGroupCompleteLevel.class, id);
        level.setReadyForFinalControlAction(!level.isReadyForFinalControlAction());
        this.update(level);
    }

    /**
     * @return  true - формировать ведомости УГС в любых состояниях
     *          false - настройка для этапов
     */
    @Override
    public boolean isFormAnyStates() {
        return ISessionDocumentBaseDAO.instance.get().isIgnoreRealEduGroupCompleteLevel();
    }

    /**
     * @param isFormAnyStates - true - формировать методомости УГС в любых состояниях
     *                          false - настройка для состояний
     */
    @Override
    public void setFormAnyStates(boolean isFormAnyStates) {
        ISessionDocumentBaseDAO.instance.get().doSetIgnoreRealEduGroupCompleteLevel(isFormAnyStates);
    }
}
