/* $Id$ */
package ru.tandemservice.unisession.settings.bo.SessionSettings.ui.FormationTransfer;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import ru.tandemservice.unisession.settings.bo.SessionSettings.SessionSettingsManager;

import java.util.Map;
import java.util.Set;

import static ru.tandemservice.unisession.UnisessionDefines.UNISESSION_SETTINGS;

/**
 * @author Alexey Lopatin
 * @since 25.05.2015
 */
public class SessionSettingsFormationTransferUI extends UIPresenter
{
    private Map<String, Boolean> _settingsMap;
    private String _currentSettingsKey;

    @Override
    public void onComponentRefresh()
    {
        _settingsMap = SessionSettingsManager.instance().dao().getSettingsFormationTransferMap();
    }

    public Set<String> getSettingsKeys()
    {
        return _settingsMap.keySet();
    }

    public Boolean getCurrentSettingsValue()
    {
        return _settingsMap.get(getCurrentSettingsKey());
    }

    public void setCurrentSettingsValue(Boolean value)
    {
        _settingsMap.put(_currentSettingsKey, value);

        IDataSettings settings = DataSettingsFacade.getSettings(UNISESSION_SETTINGS);
        settings.set(_currentSettingsKey, value);
        DataSettingsFacade.saveSettings(settings);
    }

    public String getCurrentSettingsTitle()
    {
        return getConfig().getProperty("ui." + getCurrentSettingsKey());
    }

    public Map<String, Boolean> getSettingsMap()
    {
        return _settingsMap;
    }

    public void setSettingsMap(Map<String, Boolean> settingsMap)
    {
        _settingsMap = settingsMap;
    }

    public String getCurrentSettingsKey()
    {
        return _currentSettingsKey;
    }

    public void setCurrentSettingsKey(String currentSettingsKey)
    {
        _currentSettingsKey = currentSettingsKey;
    }
}
