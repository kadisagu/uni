/* $Id$ */
package ru.tandemservice.unisession.component.sessionRetakeDoc.SessionRetakeDocAutocreate;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.exception.ApplicationException;

import ru.tandemservice.uniepp.dao.student.EppStudentSlotDAO;
import ru.tandemservice.unisession.base.bo.SessionMark.daemon.SessionMarkDaemonBean;

/**
 * @author oleyba
 * @since 6/14/11
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);
    }

    public void onClickApply(final IBusinessComponent component)
    {

        if (null == EppStudentSlotDAO.DAEMON.wakeUpAndWaitDaemon(60)) {
            throw new ApplicationException("Не удается обновить состояние студентов.");
        }

        if (null == SessionMarkDaemonBean.DAEMON.wakeUpAndWaitDaemon(60)) {
            throw new ApplicationException("Не удается обновить оценки студентов.");
        }

        this.getDao().doCreateDocuments(this.getModel(component));
        this.deactivate(component);
    }
}