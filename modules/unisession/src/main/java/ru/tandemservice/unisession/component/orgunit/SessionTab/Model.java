/**
 * $Id: Model.java 10715 2009-11-29 12:54:48Z oleyba $
 */
package ru.tandemservice.unisession.component.orgunit.SessionTab;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

/**
 * @author vdanilov
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "orgUnitHolder.id", required = true),
    @Bind(key = "orgUnitSessionSelectedTab", binding = "orgUnitSessionSelectedTab")
})
@Output({
    @Bind(key = "sessionTabSecModel", binding = "secModel")
})
public class Model
{
    private final EntityHolder<OrgUnit> orgUnitHolder = new EntityHolder<OrgUnit>();
    public EntityHolder<OrgUnit> getOrgUnitHolder() { return this.orgUnitHolder; }
    public OrgUnit getOrgUnit() { return this.getOrgUnitHolder().getValue(); }

    private CommonPostfixPermissionModel secModel;
    public CommonPostfixPermissionModel getSecModel() { return this.secModel; }
    public void setSecModel(final CommonPostfixPermissionModel secModel) { this.secModel = secModel; }

    private String orgUnitSessionSelectedTab = "sessionDocumentTab"; // по умолчанию - документы
    public String getOrgUnitSessionSelectedTab() { return this.orgUnitSessionSelectedTab; }
    public void setOrgUnitSessionSelectedTab(final String orgUnitSessionSelectedTab) { this.orgUnitSessionSelectedTab = orgUnitSessionSelectedTab; }
}