/* $Id:$ */
package ru.tandemservice.unisession.base.bo.SessionReport.util.resultsByDisc;

import ru.tandemservice.unisession.base.bo.SessionReport.util.ISessionReportResultsBaseParams;

/**
 * @author oleyba
 * @since 2/19/12
 */
public interface ISessionReportResultsByDiscParams extends ISessionReportResultsBaseParams
{
    boolean isUseFullDiscTitle();
}
