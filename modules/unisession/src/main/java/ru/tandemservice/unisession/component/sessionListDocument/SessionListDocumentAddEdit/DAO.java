package ru.tandemservice.unisession.component.sessionListDocument.SessionListDocumentAddEdit;

import com.google.common.collect.ImmutableList;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.INumberQueueDAO;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.entity.catalog.SessionsSimpleDocumentReason;
import ru.tandemservice.unisession.entity.comission.SessionComission;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionListDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.util.selectModel.SessionTermModel;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author iolshvang
 * @since 12.04.2011
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setOrgUnit(get(model.getOrgUnitId()));
        final SessionListDocument card = model.getCard();
        if (model.getCard().getId() != null) {
            checkEditAllowed(model);
        }
        final Map<SessionTermModel.TermWrapper, TermWithActionSlot> rowMap = new HashMap<>();
        if (model.getCard().getId() == null)
        {
            card.setOrgUnit(model.getOrgUnit());
            card.setIssueDate(new Date());
            card.setFormingDate(new Date());
            model.setStudentModel(new DQLFullCheckSelectModel(Student.titleWithFio().s())
            {
                @Override
                protected DQLSelectBuilder query(final String alias, final String filter)
                {
                    final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(Student.class, alias)
							.where(eq(property(alias, Student.educationOrgUnit().groupOrgUnit()), value(model.getOrgUnit())))
							.where(eq(property(alias, Student.status().active()), value(Boolean.TRUE)))
							.where(eq(property(alias, Student.archival()), value(Boolean.FALSE)))
							.joinPath(DQLJoinType.inner, Student.person().fromAlias(alias), "person")
							.joinPath(DQLJoinType.inner, Person.identityCard().fromAlias("person"), "idc")
							.order(property("idc", IdentityCard.lastName()))
							.order(property("idc", IdentityCard.firstName()))
							.order(property("idc", IdentityCard.middleName()));
                    if (StringUtils.isNotBlank(filter))
                        dql.where(this.like(IdentityCard.fullFio().fromAlias("idc"), filter));
                    return dql;
                }
            });
        }
        else
        {
            model.setCard(get(model.getCard().getId()));

            final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(SessionDocumentSlot.class, "dsl")
					.joinPath(DQLJoinType.inner, SessionDocumentSlot.studentWpeCAction().fromAlias("dsl"), "epp")
					.where(eq(property("dsl", SessionDocumentSlot.document()), value(model.getCard())))
					.column("epp");
            final List<EppStudentWpeCAction> eppSlotList = dql.createStatement(getSession()).<EppStudentWpeCAction>list();
            model.setStudentModel(new LazySimpleSelectModel<>(ImmutableList.of(model.getCard().getStudent())));

            for (final EppStudentWpeCAction eppActionSlot : eppSlotList)
            {
                final SessionTermModel.TermWrapper term = new SessionTermModel.TermWrapper(eppActionSlot);
                if (!rowMap.containsKey(term))
                    rowMap.put(term, new TermWithActionSlot());
                rowMap.get(term).setTerm(new SessionTermModel.TermWrapper(eppActionSlot));
                rowMap.get(term).getEppSlotList().add(eppActionSlot);
            }
            model.getEppModel().setupRows(rowMap.values());
        }

        model.setReasonModel(getCatalogItemList(SessionsSimpleDocumentReason.class));

        model.setTermModel(new SessionTermModel()
        {
            @Override
            public Map<EducationYear, Set<YearDistributionPart>> getValueMap()
            {
                Student student = model.getStudent();
                if (student == null) return new LinkedHashMap<>();

                SessionsSimpleDocumentReason reason = model.getCard().getReason();
                DQLSelectBuilder wpeCaBuilder = getStudentWpeCActionsBuilder(student, reason != null && reason.isFormOnDebts(), "a")
                        .column(property("a", EppStudentWpeCAction.studentWpe().year().educationYear().id()))
                        .column(property("a", EppStudentWpeCAction.studentWpe().part().id()))
                        .distinct();

                Collection<EppStudentWpeCAction> alreadySelectedWpeCActions = getWpeCActions(model);
                if (CollectionUtils.isNotEmpty(alreadySelectedWpeCActions))
                    wpeCaBuilder.where(notIn(property("a"), alreadySelectedWpeCActions));

                return getYear2YearParts(wpeCaBuilder);
            }
        });
        model.setControlActionModel(getControlActionModel(model));

        final ArrayList<TermWithActionSlot> rows = new ArrayList<>();
        for (final SessionTermModel.TermWrapper term : rowMap.keySet())
        {
            rows.add(rowMap.get(term));
        }

        model.getEppModel().setupRows(rows);

        final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(SessionMark.class, "mark")
				.joinEntity("mark", DQLJoinType.inner, SessionDocumentSlot.class, "slot", eq(property("mark", SessionMark.commission()), property("slot", SessionDocumentSlot.commission())))
				.where(eq(property("slot", SessionDocumentSlot.document()), value(model.getCard())))
				.column("mark");

        model.setHasMarks(dql.createStatement(getSession()).list().size() > 0);

    }

    public List<EppStudentWpeCAction> getWpeCActions(final Model model)
    {
	    Long editRowId = model.getEppModel().getEditRowId();
	    return model.getEppModel().getEppDataSource().getRowList().stream()
			    .filter(row -> !row.getId().equals(editRowId))
			    .flatMap(row -> row.getEppSlotList().stream()).collect(Collectors.toList());
    }

    public List<YearDistributionPart> getTerms(final Model model)
    {
	    Long editRowId = model.getEppModel().getEditRowId();
	    return model.getEppModel().getEppDataSource().getRowList().stream()
			    .filter(row -> !row.getId().equals(editRowId))
			    .map(row -> row.getTerm().getPart()).collect(Collectors.toList());
    }

    private SessionTermModel getTermModel(final Model model)
    {
        return SessionTermModel.create(model);
    }

    private DQLFullCheckSelectModel getControlActionModel(final Model model)
    {
        return new DQLFullCheckSelectModel(EppStudentWpeCAction.P_REGISTRY_ELEMENT_TITLE)
        {
            @Override
            protected DQLSelectBuilder query(final String alias, final String filter)
            {
                SessionTermModel.TermWrapper term = model.getCurrentTerm();
	            Student student = model.getStudent();

                if (student == null || term == null || term.getPart() == null)
                    return new DQLSelectBuilder().fromEntity(EppStudentWpeCAction.class, alias).where(isNull(property(alias, "id")));

                SessionsSimpleDocumentReason reason = model.getCard().getReason();
                DQLSelectBuilder wpeCaBuilder = SessionTermModel.getStudentWpeCActionsBuilder(student, reason != null && reason.isFormOnDebts(), alias);
                String wpeAlias = "wpe";
                wpeCaBuilder
                        .joinPath(DQLJoinType.inner, EppStudentWpeCAction.studentWpe().fromAlias(alias), wpeAlias)
                        .where(eq(property(wpeAlias, EppStudentWorkPlanElement.student()), value(student)))
                        .where(eq(property(wpeAlias, EppStudentWorkPlanElement.year().educationYear()), value(term.getYear())))
                        .where(eq(property(wpeAlias, EppStudentWorkPlanElement.part()), value(term.getPart())))

                        .joinPath(DQLJoinType.inner, EppStudentWorkPlanElement.registryElementPart().registryElement().fromAlias(wpeAlias), "element")
                        .joinEntity(alias, DQLJoinType.inner, EppFControlActionType.class, "type",
                                    eq(property(alias, EppStudentWpeCAction.type()), property("type", EppFControlActionType.eppGroupType())))

                        .order(property("element", EppRegistryElement.title()))
                        .order(property("type", EppFControlActionType.title()));

	            Collection<EppStudentWpeCAction> alreadySelectedWpeCActions = getWpeCActions(model);
	            if (CollectionUtils.isNotEmpty(alreadySelectedWpeCActions))
		            wpeCaBuilder.where(notIn(property(alias), alreadySelectedWpeCActions));

                FilterUtils.applySimpleLikeFilter(wpeCaBuilder, "element", EppRegistryElement.title().s(), filter);

                return wpeCaBuilder;
            }
        };
    }

    @Override
    public void update(final Model model)
    {
        checkEditAllowed(model);
        final ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();
        validate(model, errorCollector);
        if (errorCollector.hasErrors())
            return;

        final SessionListDocument card = model.getCard();
        if (card.getId() == null)
        {
            card.setFormingDate(new Date());
            card.setOrgUnit(model.getOrgUnit());
            card.setNumber(card.getNumberPrefix() + INumberQueueDAO.instance.get().getNextNumber(card));
        }
        save(card);

        final List<TermWithActionSlot> rowList = model.getEppModel().getEppDataSource().getRowList();
        final List<EppStudentWpeCAction> wpeCActions = new ArrayList<>();
        for (final TermWithActionSlot twe : rowList) {
            wpeCActions.addAll(twe.getEppSlotList());
        }

        if (!wpeCActions.isEmpty())
            deleteUnnecessarySlots(card, wpeCActions);

        final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(SessionDocumentSlot.class, "dsl")
				.where(eq(property("dsl", SessionDocumentSlot.document()), value(card)))
				.column(property("dsl", SessionDocumentSlot.studentWpeCAction()));
        if (!wpeCActions.isEmpty())
            dql.where(in(property("dsl", SessionDocumentSlot.studentWpeCAction()), wpeCActions));

        wpeCActions.removeAll(dql.createStatement(getSession()).<EppStudentWpeCAction>list());

        for (final EppStudentWpeCAction wpeCAction : wpeCActions)
	        saveNewSlot(card, wpeCAction);
    }

	private void deleteUnnecessarySlots(SessionListDocument card, Collection<EppStudentWpeCAction> wpeCActionsToRemain)
	{
		final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(SessionDocumentSlot.class, "dsl")
				.where(eq(property("dsl", SessionDocumentSlot.document()), value(card)))
				.where(notIn(property("dsl", SessionDocumentSlot.studentWpeCAction()), wpeCActionsToRemain));
		for (final SessionDocumentSlot slot : dql.createStatement(getSession()).<SessionDocumentSlot>list())
		{
			final SessionComission comission = slot.getCommission();
			delete(slot);
			getSession().flush();
			delete(comission);
		}
	}

	private void saveNewSlot(SessionListDocument card, EppStudentWpeCAction wpeCAction)
	{
		final SessionComission commission = new SessionComission();
		save(commission);
		getSession().flush();

		final SessionDocumentSlot slot = new SessionDocumentSlot();
		slot.setDocument(card);
		slot.setStudentWpeCAction(wpeCAction);
		slot.setCommission(commission);
		slot.setInSession(false);
		save(slot);
	}

    @Override
    public void validate(final Model model, final ErrorCollector errors)
    {
	    Date issuanceDate = model.getCard().getIssueDate(), deadlineDate = model.getCard().getDeadlineDate();
        if ((issuanceDate != null) && (deadlineDate != null) && issuanceDate.after(deadlineDate))
            errors.add("Срок действия должен истекать не раньше даты выдачи.", "issueDate", "deadlineDate");

        StaticListDataSource<TermWithActionSlot> eppDs = model.getEppModel().getEppDataSource();
        if ((eppDs.getCountRow() == 0) || ((eppDs.getRowList().get(0)).getEppSlotList().size() == 0)) {
            errors.add("Необходимо указать хотя бы одну дисциплину.");
        }
    }

    private void checkEditAllowed(final Model model)
    {
        final DQLSelectBuilder check = new DQLSelectBuilder()
            .fromEntity(SessionMark.class, "m")
            .where(eq(property("m", SessionMark.slot().document().id()), value(model.getCard().getId())));
        final Number checkResult = check.createCountStatement(new DQLExecutionContext(getSession())).<Number>uniqueResult();
        if (checkResult != null && checkResult.intValue() > 0)
            throw new ApplicationException("Редактирование невозможно, в карточку уже выставлены оценки.");
    }

    @Override
    public void checkWpeCAction(Model model)
    {
        SessionsSimpleDocumentReason reason = model.getCard().getReason();
        List<Long> wpeCaIdList = SessionTermModel.getStudentWpeCActionsBuilder(model.getStudent(), reason != null && reason.isFormOnDebts(), "ca")
                .column(property("ca", EppStudentWpeCAction.id()))
                .createStatement(getSession()).list();

        SessionListDocumentEppSlotAddModel eppModel = model.getEppModel();
        StaticListDataSource<TermWithActionSlot> eppDataSource = eppModel.getEppDataSource();
        List<TermWithActionSlot> rowList = eppDataSource.getRowList();

        TermWithActionSlot editRow = (TermWithActionSlot) eppModel.getEditRow();

        List<TermWithActionSlot> terms = rowList.stream()
                .filter(row -> !row.equals(editRow))
                .map(TermWithActionSlot::getEppSlotList)
                .filter(CollectionUtils::isNotEmpty)
                .flatMap(Collection::stream)
                .filter(wpeCa -> wpeCaIdList.contains(wpeCa.getId()))
                .collect(Collectors.groupingBy(SessionTermModel.TermWrapper::new, Collectors.mapping(wpeCa -> wpeCa, Collectors.toList())))

                .entrySet().stream()
                .map(entry ->
                     {
                         TermWithActionSlot termWithActionSlot = new TermWithActionSlot();
                         termWithActionSlot.setTerm(entry.getKey());
                         termWithActionSlot.setEppSlotList(entry.getValue());
                         return termWithActionSlot;
                     })
                .collect(Collectors.toList());

        if (editRow != null) terms.add(editRow);

        eppDataSource.setRowList(terms);
    }
}

