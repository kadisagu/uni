package ru.tandemservice.unisession.entity.stateFinalExam.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.stateFinalExam.SessionGovExamCommission;
import ru.tandemservice.unisession.entity.stateFinalExam.SessionStateFinalExamProtocol;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Протокол ГИА
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionStateFinalExamProtocolGen extends EntityBase
 implements INaturalIdentifiable<SessionStateFinalExamProtocolGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.stateFinalExam.SessionStateFinalExamProtocol";
    public static final String ENTITY_NAME = "sessionStateFinalExamProtocol";
    public static final int VERSION_HASH = 472474759;
    private static IEntityMeta ENTITY_META;

    public static final String L_DOCUMENT_SLOT = "documentSlot";
    public static final String L_COMMISSION = "commission";
    public static final String P_NUMBER = "number";
    public static final String P_FORMING_DATE = "formingDate";
    public static final String P_COMMENT = "comment";

    private SessionDocumentSlot _documentSlot;     // Запись студента по мероприятию в документе сессии.
    private SessionGovExamCommission _commission;     // Комиссия
    private String _number;     // Номер
    private Date _formingDate;     // Дата формирования
    private String _comment;     // Комментарий

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Запись студента по мероприятию в документе сессии.. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public SessionDocumentSlot getDocumentSlot()
    {
        return _documentSlot;
    }

    /**
     * @param documentSlot Запись студента по мероприятию в документе сессии.. Свойство не может быть null и должно быть уникальным.
     */
    public void setDocumentSlot(SessionDocumentSlot documentSlot)
    {
        dirty(_documentSlot, documentSlot);
        _documentSlot = documentSlot;
    }

    /**
     * @return Комиссия.
     */
    public SessionGovExamCommission getCommission()
    {
        return _commission;
    }

    /**
     * @param commission Комиссия.
     */
    public void setCommission(SessionGovExamCommission commission)
    {
        dirty(_commission, commission);
        _commission = commission;
    }

    /**
     * @return Номер. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер. Свойство не может быть null.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата формирования. Свойство не может быть null.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    /**
     * @return Комментарий.
     */
    @Length(max=255)
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SessionStateFinalExamProtocolGen)
        {
            if (withNaturalIdProperties)
            {
                setDocumentSlot(((SessionStateFinalExamProtocol)another).getDocumentSlot());
            }
            setCommission(((SessionStateFinalExamProtocol)another).getCommission());
            setNumber(((SessionStateFinalExamProtocol)another).getNumber());
            setFormingDate(((SessionStateFinalExamProtocol)another).getFormingDate());
            setComment(((SessionStateFinalExamProtocol)another).getComment());
        }
    }

    public INaturalId<SessionStateFinalExamProtocolGen> getNaturalId()
    {
        return new NaturalId(getDocumentSlot());
    }

    public static class NaturalId extends NaturalIdBase<SessionStateFinalExamProtocolGen>
    {
        private static final String PROXY_NAME = "SessionStateFinalExamProtocolNaturalProxy";

        private Long _documentSlot;

        public NaturalId()
        {}

        public NaturalId(SessionDocumentSlot documentSlot)
        {
            _documentSlot = ((IEntity) documentSlot).getId();
        }

        public Long getDocumentSlot()
        {
            return _documentSlot;
        }

        public void setDocumentSlot(Long documentSlot)
        {
            _documentSlot = documentSlot;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof SessionStateFinalExamProtocolGen.NaturalId) ) return false;

            SessionStateFinalExamProtocolGen.NaturalId that = (NaturalId) o;

            if( !equals(getDocumentSlot(), that.getDocumentSlot()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getDocumentSlot());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getDocumentSlot());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionStateFinalExamProtocolGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionStateFinalExamProtocol.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("SessionStateFinalExamProtocol is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "documentSlot":
                    return obj.getDocumentSlot();
                case "commission":
                    return obj.getCommission();
                case "number":
                    return obj.getNumber();
                case "formingDate":
                    return obj.getFormingDate();
                case "comment":
                    return obj.getComment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "documentSlot":
                    obj.setDocumentSlot((SessionDocumentSlot) value);
                    return;
                case "commission":
                    obj.setCommission((SessionGovExamCommission) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "documentSlot":
                        return true;
                case "commission":
                        return true;
                case "number":
                        return true;
                case "formingDate":
                        return true;
                case "comment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "documentSlot":
                    return true;
                case "commission":
                    return true;
                case "number":
                    return true;
                case "formingDate":
                    return true;
                case "comment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "documentSlot":
                    return SessionDocumentSlot.class;
                case "commission":
                    return SessionGovExamCommission.class;
                case "number":
                    return String.class;
                case "formingDate":
                    return Date.class;
                case "comment":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionStateFinalExamProtocol> _dslPath = new Path<SessionStateFinalExamProtocol>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionStateFinalExamProtocol");
    }
            

    /**
     * @return Запись студента по мероприятию в документе сессии.. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unisession.entity.stateFinalExam.SessionStateFinalExamProtocol#getDocumentSlot()
     */
    public static SessionDocumentSlot.Path<SessionDocumentSlot> documentSlot()
    {
        return _dslPath.documentSlot();
    }

    /**
     * @return Комиссия.
     * @see ru.tandemservice.unisession.entity.stateFinalExam.SessionStateFinalExamProtocol#getCommission()
     */
    public static SessionGovExamCommission.Path<SessionGovExamCommission> commission()
    {
        return _dslPath.commission();
    }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.stateFinalExam.SessionStateFinalExamProtocol#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.stateFinalExam.SessionStateFinalExamProtocol#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unisession.entity.stateFinalExam.SessionStateFinalExamProtocol#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    public static class Path<E extends SessionStateFinalExamProtocol> extends EntityPath<E>
    {
        private SessionDocumentSlot.Path<SessionDocumentSlot> _documentSlot;
        private SessionGovExamCommission.Path<SessionGovExamCommission> _commission;
        private PropertyPath<String> _number;
        private PropertyPath<Date> _formingDate;
        private PropertyPath<String> _comment;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Запись студента по мероприятию в документе сессии.. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unisession.entity.stateFinalExam.SessionStateFinalExamProtocol#getDocumentSlot()
     */
        public SessionDocumentSlot.Path<SessionDocumentSlot> documentSlot()
        {
            if(_documentSlot == null )
                _documentSlot = new SessionDocumentSlot.Path<SessionDocumentSlot>(L_DOCUMENT_SLOT, this);
            return _documentSlot;
        }

    /**
     * @return Комиссия.
     * @see ru.tandemservice.unisession.entity.stateFinalExam.SessionStateFinalExamProtocol#getCommission()
     */
        public SessionGovExamCommission.Path<SessionGovExamCommission> commission()
        {
            if(_commission == null )
                _commission = new SessionGovExamCommission.Path<SessionGovExamCommission>(L_COMMISSION, this);
            return _commission;
        }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.stateFinalExam.SessionStateFinalExamProtocol#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(SessionStateFinalExamProtocolGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.stateFinalExam.SessionStateFinalExamProtocol#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(SessionStateFinalExamProtocolGen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unisession.entity.stateFinalExam.SessionStateFinalExamProtocol#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(SessionStateFinalExamProtocolGen.P_COMMENT, this);
            return _comment;
        }

        public Class getEntityClass()
        {
            return SessionStateFinalExamProtocol.class;
        }

        public String getEntityName()
        {
            return "sessionStateFinalExamProtocol";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
