package ru.tandemservice.unisession.entity.document.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unisession.entity.document.SessionDocument;
import ru.tandemservice.unisession.entity.document.SessionGlobalDocument;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Документ для массового ввода оценок
 *
 * Автоматически создается при выставлении оценок в рамках семестрового журнала, обеспечивает массовый ввод оценок.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionGlobalDocumentGen extends SessionDocument
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.document.SessionGlobalDocument";
    public static final String ENTITY_NAME = "sessionGlobalDocument";
    public static final int VERSION_HASH = 1152150593;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDUCATION_YEAR = "educationYear";
    public static final String L_YEAR_DISTRIBUTION_PART = "yearDistributionPart";

    private EducationYear _educationYear;     // Учебный год
    private YearDistributionPart _yearDistributionPart;     // Часть года

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     */
    @NotNull
    public EducationYear getEducationYear()
    {
        return _educationYear;
    }

    /**
     * @param educationYear Учебный год. Свойство не может быть null.
     */
    public void setEducationYear(EducationYear educationYear)
    {
        dirty(_educationYear, educationYear);
        _educationYear = educationYear;
    }

    /**
     * @return Часть года. Свойство не может быть null.
     */
    @NotNull
    public YearDistributionPart getYearDistributionPart()
    {
        return _yearDistributionPart;
    }

    /**
     * @param yearDistributionPart Часть года. Свойство не может быть null.
     */
    public void setYearDistributionPart(YearDistributionPart yearDistributionPart)
    {
        dirty(_yearDistributionPart, yearDistributionPart);
        _yearDistributionPart = yearDistributionPart;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof SessionGlobalDocumentGen)
        {
            setEducationYear(((SessionGlobalDocument)another).getEducationYear());
            setYearDistributionPart(((SessionGlobalDocument)another).getYearDistributionPart());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionGlobalDocumentGen> extends SessionDocument.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionGlobalDocument.class;
        }

        public T newInstance()
        {
            return (T) new SessionGlobalDocument();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "educationYear":
                    return obj.getEducationYear();
                case "yearDistributionPart":
                    return obj.getYearDistributionPart();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "educationYear":
                    obj.setEducationYear((EducationYear) value);
                    return;
                case "yearDistributionPart":
                    obj.setYearDistributionPart((YearDistributionPart) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "educationYear":
                        return true;
                case "yearDistributionPart":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "educationYear":
                    return true;
                case "yearDistributionPart":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "educationYear":
                    return EducationYear.class;
                case "yearDistributionPart":
                    return YearDistributionPart.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionGlobalDocument> _dslPath = new Path<SessionGlobalDocument>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionGlobalDocument");
    }
            

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionGlobalDocument#getEducationYear()
     */
    public static EducationYear.Path<EducationYear> educationYear()
    {
        return _dslPath.educationYear();
    }

    /**
     * @return Часть года. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionGlobalDocument#getYearDistributionPart()
     */
    public static YearDistributionPart.Path<YearDistributionPart> yearDistributionPart()
    {
        return _dslPath.yearDistributionPart();
    }

    public static class Path<E extends SessionGlobalDocument> extends SessionDocument.Path<E>
    {
        private EducationYear.Path<EducationYear> _educationYear;
        private YearDistributionPart.Path<YearDistributionPart> _yearDistributionPart;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionGlobalDocument#getEducationYear()
     */
        public EducationYear.Path<EducationYear> educationYear()
        {
            if(_educationYear == null )
                _educationYear = new EducationYear.Path<EducationYear>(L_EDUCATION_YEAR, this);
            return _educationYear;
        }

    /**
     * @return Часть года. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionGlobalDocument#getYearDistributionPart()
     */
        public YearDistributionPart.Path<YearDistributionPart> yearDistributionPart()
        {
            if(_yearDistributionPart == null )
                _yearDistributionPart = new YearDistributionPart.Path<YearDistributionPart>(L_YEAR_DISTRIBUTION_PART, this);
            return _yearDistributionPart;
        }

        public Class getEntityClass()
        {
            return SessionGlobalDocument.class;
        }

        public String getEntityName()
        {
            return "sessionGlobalDocument";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
