/* $Id:$ */
package ru.tandemservice.unisession.attestation.bo.AttestationBulletin.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.INumberGenerationRule;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unisession.attestation.entity.SessionAttestation;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlot;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author oleyba
 * @since 10/9/12
 */
public interface ISessionAttestationBulletinDao extends INeedPersistenceSupport
{
    void changeState(SessionAttestationBulletin bulletin);

    void doGenerateBulletins(SessionAttestation attestation, Collection<Long> eduGroupIds);

    INumberGenerationRule<SessionAttestationBulletin> getNumberGenerationRule();

    EppALoadType getSuitableLoadType(EppRegistryElementPart regElPart);

    /**
     * Обновляет Аттестационную ведомость и список её ППСов.
     * @param entity Аттестационная ведомость, которую редактируем
     * @param ppsList список Преподователей, которые указаны в ведомости
     */
    void doUpdateAttBulletin(SessionAttestationBulletin entity, List<PpsEntry> ppsList);

    void deleteBulletin(Long bulletinId);

    /**
     * @param bulletin Аттестационная ведомость
     * @return список студентов, прикрепленных к Аттестационной ведомости
     */
    List<EppStudentWorkPlanElement> getAttBulletinStudentList(SessionAttestationBulletin bulletin);

    /**
     * Прикрепляет студентов (их МСРП) к Аттестационной ведомости.
     * @param bulletin Аттестационная ведомость
     * @param studentEpvSlotList список МСРП студентов
     */
    void doAddStudents(SessionAttestationBulletin bulletin, List<EppStudentWorkPlanElement> studentEpvSlotList);

    /**
     * Открывает атт.ведомость, удаляет сохраненные печатные формы
     * @param bulletin Атт. ведомость
     */
    void doReopenBulletin(SessionAttestationBulletin bulletin);

    /**
     * Выводит сехраненную печатную форму закрытой атт. ведомости
     * @param bulletin
     */
    void doPrintClosedBulletin(SessionAttestationBulletin bulletin);

    /**
     * Закрывает атт.ведомость, сохраняет печатную форму
     * @param bulletin Атт.ведомость
     */
    void doCloseBulletin(SessionAttestationBulletin bulletin);

    /**
     * Проверяет, можно ли редактировать атт. ведомость
     * @param bulletinId
     */
    void checkEditAllowed(long bulletinId);

    /**
     * Сохраняет список студентов и ведомость при выставлении оценок вручную.
     * Поле для фиксации рейтинга сбрасывается в 0 для тех студентов, кому поставлена отметка да/нет, тем, кому не поставлены отметки, поле для фиксации рейтинга выставляется в null.
     */
    void doSaveManualMark(SessionAttestationBulletin bulletin, List<SessionAttestationSlot> slotList);

    /**
     * Для каждой дисциплиночасти получаем список академ. групп из атт.ведомости
     * { part.id -> group.id }
     */
    Map<Long, Set<Long>> getFoundSlot4GroupMap(SessionAttestation attestation, List<Long> regElementPartIds);

    /**
     * Исключаем уже созданные атт.ведомости для каждой дисциплиночасти академ. группы
     * { part.id -> group.id }
     */
    void filterNotExistsAlreadyCreatedBulletin(String alias, DQLSelectBuilder dql, SessionAttestation attestation);

    /**
     * Выбираем приоритетный вид аудиторной нагрузки для дисциплиночасти
     * приоритет вида: лекции -> практики -> лабораторные
     */
    void filterExistsPriorityLoadType(String alias, DQLSelectBuilder dql);

    /**
     * Отбирает список открытых атт.ведомостей, без оценок
     */
    Set<Long> getBulletinWithoutMarks(Collection<Long> ids);
}
