/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionDocument.ui.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale;
import ru.tandemservice.uniepp.entity.catalog.codes.EppGradeScaleCodes;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkStateCatalogItem;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 29.07.2015
 */
public class SessionMarkDSHandler extends DefaultComboDataSourceHandler
{
    public SessionMarkDSHandler(String ownerId)
    {
        super(ownerId, SessionMarkCatalogItem.class);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        DQLSelectBuilder subBuilder = new DQLSelectBuilder()
                .fromEntity(SessionMarkStateCatalogItem.class, "markState")
                .fromEntity(SessionMarkGradeValueCatalogItem.class, "markGradeValue")
                .where(or(
                        eq(property("m", SessionMarkCatalogItem.id()), property("markState", SessionMarkStateCatalogItem.id())),
                        eq(property("m", SessionMarkCatalogItem.id()), property("markGradeValue", SessionMarkGradeValueCatalogItem.id()))
                ));

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SessionMarkCatalogItem.class, "m").where(exists(subBuilder.buildQuery()));
        FilterUtils.applySelectFilter(builder, "m", SessionMarkCatalogItem.id(), input.getPrimaryKeys());

        String filter = input.getComboFilterByValue();
        if(StringUtils.isNotEmpty(filter))
            builder.where(likeUpper(property("m", SessionMarkCatalogItem.title()), value(CoreStringUtils.escapeLike(filter, true))));

        List<SessionMarkCatalogItem> resultList = builder.createStatement(context.getSession()).list();
        Collections.sort(resultList, COMPARATOR);
        return ListOutputBuilder.get(input, resultList).build();
    }

    private static final Comparator<SessionMarkCatalogItem> COMPARATOR = new Comparator<SessionMarkCatalogItem>()
    {
        private CoreCollectionUtils.Pair<String, String> getItemTitle(SessionMarkCatalogItem item)
        {
            String systemTitle = null, userTitle = null;
            if (item instanceof SessionMarkGradeValueCatalogItem)
            {
                EppGradeScale scale = ((SessionMarkGradeValueCatalogItem) item).getScale();
                if (EppGradeScaleCodes.CODES.contains(scale.getCode())) systemTitle = scale.getTitle();
                else userTitle = scale.getTitle();
            }
            return new CoreCollectionUtils.Pair<>(systemTitle, userTitle);
        }

        @Override
        public int compare(SessionMarkCatalogItem o1, SessionMarkCatalogItem o2)
        {
            int result = 0;
            CoreCollectionUtils.Pair<String, String> item1 = getItemTitle(o1);
            CoreCollectionUtils.Pair<String, String> item2 = getItemTitle(o2);

            for (int i = 0; i < 2; i++)
            {
                String code1 = i == 0 ? item1.getX() : item1.getY();
                String code2 = i == 0 ? item2.getX() : item2.getY();
                if (result == 0)
                {
                    if (null != code1 && null != code2)
                        result = CommonCollator.RUSSIAN_COLLATOR.compare(code1, code2);
                    if (null != code1 && null == code2)
                        result = -1;
                    else if (null != code2 && null == code1)
                        result = 1;
                }
            }
            if (result == 0)
                result = Integer.compare(o1.getPriority(), o2.getPriority());
            return result;
        }
    };
}
