package ru.tandemservice.unisession.component.orgunit.SessionObjectListTab;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unisession.attestation.bo.Attestation.ui.Add.AttestationAdd;
import ru.tandemservice.unisession.entity.document.SessionObject;

/**
 * @author vdanilov
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);

        final String settingsKey = "session.sessionobject." + model.getOrgUnitId();
        model.setSettings(UniBaseUtils.getDataSettings(component, settingsKey));

        final DynamicListDataSource<SessionObject> dataSource = new DynamicListDataSource<>(component, component1 -> {
            Controller.this.getDao().refreshDataSource(Controller.this.getModel(component1));
        });

        dataSource.addColumn(new SimpleColumn("Номер", SessionObject.number()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Учебный год", SessionObject.educationYear().title()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Часть года", SessionObject.yearDistributionPart().titleWithParent()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Дата\n формирования", SessionObject.formingDate(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата\n начала", SessionObject.startupDate(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата\n окончания", SessionObject.deadlineDate(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата\n закрытия", SessionObject.closeDate(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new PublisherLinkColumn("Аттестации", "typeTitle", "attestationList").setOrderable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEdit").setPermissionKey(model.getSec().getPermission("editSessionObject")));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete", "Удалить сессию {0}?", SessionObject.title().s()).setPermissionKey(model.getSec().getPermission("deleteSessionObject")));

        model.setDataSource(dataSource);

    }

    public void onClickEdit(final IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                ru.tandemservice.unisession.component.sessionObject.SessionObjectAddEdit.Model.COMPONENT_NAME,
                new ParametersMap().add("id", component.getListenerParameter())));
    }

    public void onClickDelete(final IBusinessComponent component)
    {
        IUniBaseDao.instance.get().delete((Long) component.getListenerParameter());
    }

    public void onClickSearch(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        model.getDataSource().showFirstPage();
        model.getDataSource().refresh();
        DataSettingsFacade.saveSettings(model.getSettings());
    }

    public void onClickClear(final IBusinessComponent context)
    {
        this.getModel(context).getSettings().clear();
        this.onClickSearch(context);
    }

    public void onClickAdd(final IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                ru.tandemservice.unisession.component.sessionObject.SessionObjectAddEdit.Model.COMPONENT_NAME,
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, this.getModel(component).getOrgUnitId())));
    }

    public void onClickAddAttestation(final IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                AttestationAdd.class.getSimpleName(),
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, this.getModel(component).getOrgUnitId())));
    }
}
