/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionTransfer.ui.ProtocolPub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow;
import ru.tandemservice.unisession.base.bo.SessionTransfer.logic.SessionTransferProtocolRowDSHandler;
import ru.tandemservice.unisession.entity.document.SessionTransferProtocolRow;
import ru.tandemservice.unisession.entity.mark.SessionALRequestRow;

/**
 * @author Alexey Lopatin
 * @since 14.09.2015
 */
@Configuration
public class SessionTransferProtocolPub extends BusinessComponentManager
{
    public static final String PROTOCOL_ROW_DS = "protocolRowDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(PROTOCOL_ROW_DS, protocolRowDS(), protocolRowDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint protocolRowDS()
    {
        return columnListExtPointBuilder(PROTOCOL_ROW_DS)
                .addColumn(textColumn(EppEpvTermDistributedRow.P_STORED_INDEX, SessionTransferProtocolRow.requestRow().rowTerm().row().storedIndex()).width("1px"))
                .addColumn(textColumn(DataWrapper.TITLE, DataWrapper.TITLE))
                .addColumn(textColumn(SessionALRequestRow.P_HOURS_AMOUNT, SessionTransferProtocolRow.requestRow().hoursAmount()).width("1px"))
                .addColumn(textColumn(SessionTransferProtocolRowDSHandler.PROP_RE_EXAM_AND_ATTESTATION, SessionTransferProtocolRowDSHandler.PROP_RE_EXAM_AND_ATTESTATION).width("1px"))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> protocolRowDSHandler()
    {
        return new SessionTransferProtocolRowDSHandler(getName());
    }
}
