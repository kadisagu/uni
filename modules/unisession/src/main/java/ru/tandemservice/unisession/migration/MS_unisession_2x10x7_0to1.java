package ru.tandemservice.unisession.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unisession_2x10x7_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.7")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность unisessionSummaryBulletinReport

		// создано обязательное свойство showPreviousTermPractise
		{
			// создать колонку
			tool.createColumn("session_rep_summary_bull_t", new DBColumn("showprevioustermpractise_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			java.lang.Boolean defaultShowPreviousTermPractise = false;
			tool.executeUpdate("update session_rep_summary_bull_t set showprevioustermpractise_p=? where showprevioustermpractise_p is null", defaultShowPreviousTermPractise);

			// сделать колонку NOT NULL
			tool.setColumnNullable("session_rep_summary_bull_t", "showprevioustermpractise_p", false);

		}


    }
}