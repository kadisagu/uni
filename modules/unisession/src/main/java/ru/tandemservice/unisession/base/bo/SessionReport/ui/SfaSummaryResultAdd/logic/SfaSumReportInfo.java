/* $Id: $ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.SfaSummaryResultAdd.logic;

import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.unisession.base.bo.SessionReport.logic.AbsReportInfo;
import ru.tandemservice.unisession.base.bo.SessionReport.logic.SfaRaw;
import ru.tandemservice.unisession.entity.report.SfaSummaryResult;

import java.util.*;

/**
 * @author Andrey Andreev
 * @since 08.12.2016
 */
public class SfaSumReportInfo<R1 extends SfaRaw> extends AbsReportInfo<SfaSummaryResult>
{
    public SfaSumReportInfo(SfaSummaryResult report)
    {
        super(report);
    }


    protected List<EducationOrgUnit> _educationOrgUnits;

    public List<EducationOrgUnit> getEducationOrgUnits()
    {
        return _educationOrgUnits;
    }

    public void setEducationOrgUnits(List<EducationOrgUnit> educationOrgUnits)
    {
        _educationOrgUnits = educationOrgUnits;
    }


    protected Collection<R1> raws;

    public Collection<R1> getRaws()
    {
        return raws;
    }

    public void setRaws(Collection<R1> raws)
    {
        this.raws = raws;
    }
}