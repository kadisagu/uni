/* $Id:$ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.GroupMarksList;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.entity.report.UnisessionGroupMarksReport;

import java.util.Collections;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 2/21/12
 */
public class SessionGroupMarksDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String PARAM_ORG_UNIT = SessionReportManager.PARAM_ORG_UNIT;
    public static final String PARAM_COURSE = "course";

    private final DQLOrderDescriptionRegistry registry = buildOrderRegistry();

    public SessionGroupMarksDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput dsInput, ExecutionContext context)
    {
        final OrgUnit ou = context.get(PARAM_ORG_UNIT);
        final String course = context.get(PARAM_COURSE);
        if (null == ou || null == course) {
            return ListOutputBuilder.get(dsInput, Collections.emptyList()).build();
        }

        DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(UnisessionGroupMarksReport.class, "r")
            .where(eq(property(UnisessionGroupMarksReport.orgUnit().fromAlias("r")), value(ou)))
            .where(eq(property(UnisessionGroupMarksReport.course().fromAlias("r")), value(course)));

        registry.applyOrder(dql, dsInput.getEntityOrder());

        return DQLSelectOutputBuilder.get(dsInput, dql, context.getSession()).build();
    }

    protected DQLOrderDescriptionRegistry buildOrderRegistry() {
        return new DQLOrderDescriptionRegistry(UnisessionGroupMarksReport.class, "r");
    }


}
