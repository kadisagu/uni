/**
 *$Id:$
 */
package ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.ResultAdd.util;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unisession.attestation.entity.SessionAttestation;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Alexander Shaburov
 * @since 14.11.12
 */
public class Model
{
    //form properties
    private OrgUnit _orgUnit;
    private SessionAttestation _attestation;
    private Course _course;
    private List<Group> _groupList;
    private boolean _disciplineTitleWithLabor = false;

    //print fields
    private List<AttestationReportResultRowWrapper> _rowWrapperList;
    private Set<EppRegistryElementPart> _disciplineSet;
    private EducationYear currentYear = EducationYear.getCurrentRequired();
    private Map<Group, Set<EppRegistryElementPart>> _groupToDisciplineMap;

    // Getters & Setters

    public EducationYear getCurrentYear()
    {
        return currentYear;
    }

    public void setCurrentYear(EducationYear currentYear)
    {
        this.currentYear = currentYear;
    }

    public Set<EppRegistryElementPart> getDisciplineSet()
    {
        return _disciplineSet;
    }

    public void setDisciplineSet(Set<EppRegistryElementPart> disciplineSet)
    {
        _disciplineSet = disciplineSet;
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }

    public SessionAttestation getAttestation()
    {
        return _attestation;
    }

    public void setAttestation(SessionAttestation attestation)
    {
        _attestation = attestation;
    }

    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public List<Group> getGroupList()
    {
        return _groupList;
    }

    public void setGroupList(List<Group> groupList)
    {
        _groupList = groupList;
    }

    public List<AttestationReportResultRowWrapper> getRowWrapperList()
    {
        return _rowWrapperList;
    }

    public void setRowWrapperList(List<AttestationReportResultRowWrapper> rowWrapperList)
    {
        _rowWrapperList = rowWrapperList;
    }

    public boolean isDisciplineTitleWithLabor()
    {
        return _disciplineTitleWithLabor;
    }

    public void setDisciplineTitleWithLabor(boolean disciplineTitleWithLabor)
    {
        _disciplineTitleWithLabor = disciplineTitleWithLabor;
    }

    public Map<Group, Set<EppRegistryElementPart>> getGroupToDisciplineMap()
    {
        return this._groupToDisciplineMap;
    }

    public void setGroupToDisciplineMap(Map<Group, Set<EppRegistryElementPart>> groupToDisciplineMap)
    {
        this._groupToDisciplineMap = groupToDisciplineMap;
    }
}
