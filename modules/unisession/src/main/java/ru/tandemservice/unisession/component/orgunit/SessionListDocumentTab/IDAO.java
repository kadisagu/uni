/* $*/

package ru.tandemservice.unisession.component.orgunit.SessionListDocumentTab;

import ru.tandemservice.uni.dao.IListDataSourceDao;
import ru.tandemservice.uni.dao.IUpdateable;

/**
 * @author iolshvang
 * @since 12.04.2011
 */
public interface IDAO extends IUpdateable<Model>, IListDataSourceDao<Model>
{
	String PROP_TERM = "term";

    void doDeleteCard(Long id);
}
