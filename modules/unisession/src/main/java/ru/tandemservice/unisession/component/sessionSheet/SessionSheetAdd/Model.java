// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisession.component.sessionSheet.SessionSheetAdd;

import com.google.common.collect.ImmutableList;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.base.bo.PpsEntry.util.PpsEntrySelectBlockData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionSheetDocument;
import ru.tandemservice.unisession.util.selectModel.SessionTermModel;

import java.util.Date;
import java.util.List;

/**
 * @author oleyba
 * @since 2/9/11
 */
@Input({
    @Bind(key=PublisherActivator.PUBLISHER_ID_KEY, binding="initOrgUnit.id"),
    @Bind(key="studentId", binding="initStudent.id"),
    @Bind(key="controlActionId", binding="initWpcaSlot.id")
})
@Output({
    @Bind(key="controlActionId", binding="eppSlot.id")
})

public class Model implements SessionTermModel.IStudentBasedDataOwner
{
    public static final String COMPONENT_NAME = Model.class.getPackage().getName();
    private OrgUnit orgUnit;
    private Student student;
    private SessionTermModel.TermWrapper term;
    private EppStudentWpeCAction eppSlot;

    private PpsEntrySelectBlockData ppsData;

    private SessionSheetDocument sheet = new SessionSheetDocument();
    private SessionDocumentSlot slot = new SessionDocumentSlot();
    private SessionMarkCatalogItem mark;
    private Date performDate;

	private boolean themeRequired;
	private String theme;

    private OrgUnitHolder initOrgUnit = new OrgUnitHolder();
    private EntityHolder<Student> initStudent = new EntityHolder<>();
    private EntityHolder<EppStudentWpeCAction> initWpcaSlot = new EntityHolder<>();

    private ISelectModel studentModel;
    private ISelectModel termModel;
    private ISelectModel controlActionModel;
    private ISelectModel reasonModel;
    private ISelectModel markModel = new LazySimpleSelectModel<>(ImmutableList.of());

    private boolean initialized = false;
    private boolean canMark = false;

    public boolean isShowHistory()
    {
        return this.eppSlot != null && this.eppSlot.getId() != null;
    }

    public Long getOrgUnitId()
    {
        return this.getOrgUnit().getId();
    }

    public OrgUnit getOrgUnit()
    {
        return this.orgUnit;
    }

    public void setOrgUnit(final OrgUnit orgUnit)
    {
        this.orgUnit = orgUnit;
    }

    @Override
    public Student getStudent()
    {
        return this.student;
    }

    public void setStudent(final Student student)
    {
        this.student = student;
    }

    public SessionTermModel.TermWrapper getTerm()
    {
        return this.term;
    }

    public void setTerm(final SessionTermModel.TermWrapper term)
    {
        this.term = term;
    }

    @Override
    public EppStudentWpeCAction getEppSlot()
    {
        return this.eppSlot;
    }

    public void setEppSlot(final EppStudentWpeCAction eppSlot)
    {
        this.eppSlot = eppSlot;
    }

    public PpsEntrySelectBlockData getPpsData()
    {
        return ppsData;
    }

    public void setPpsData(PpsEntrySelectBlockData ppsData)
    {
        this.ppsData = ppsData;
    }

    public List<PpsEntry> getPpsList()
    {
        return getPpsData().getSelectedPpsList();
    }

    public void setPpsList(final List<PpsEntry> ppsList)
    {
        getPpsData().setSelectedPps(ppsList);
    }

    public SessionSheetDocument getSheet()
    {
        return this.sheet;
    }

    public void setSheet(final SessionSheetDocument sheet)
    {
        this.sheet = sheet;
    }

    public SessionDocumentSlot getSlot()
    {
        return this.slot;
    }

    public void setSlot(final SessionDocumentSlot slot)
    {
        this.slot = slot;
    }

    public ISelectModel getStudentModel()
    {
        return this.studentModel;
    }

    public void setStudentModel(final ISelectModel studentModel)
    {
        this.studentModel = studentModel;
    }

    public ISelectModel getTermModel()
    {
        return this.termModel;
    }

    public void setTermModel(final ISelectModel termModel)
    {
        this.termModel = termModel;
    }

    public ISelectModel getControlActionModel()
    {
        return this.controlActionModel;
    }

    public void setControlActionModel(final ISelectModel controlActionModel)
    {
        this.controlActionModel = controlActionModel;
    }

    public boolean isInitialized()
    {
        return this.initialized;
    }

    public void setInitialized(final boolean initialized)
    {
        this.initialized = initialized;
    }

    public ISelectModel getReasonModel()
    {
        return this.reasonModel;
    }

    public void setReasonModel(final ISelectModel reasonModel)
    {
        this.reasonModel = reasonModel;
    }

    public SessionMarkCatalogItem getMark()
    {
        return this.mark;
    }

    public void setMark(final SessionMarkCatalogItem mark)
    {
        this.mark = mark;
    }

    public Date getPerformDate()
    {
        return this.performDate;
    }

    public void setPerformDate(final Date performDate)
    {
        this.performDate = performDate;
    }

	public boolean isThemeRequired()
	{
		return themeRequired;
	}

	public void setThemeRequired(final boolean themeRequired)
	{
		this.themeRequired = themeRequired;
	}

	public String getTheme()
	{
		return theme;
	}

	public void setTheme(final String theme)
	{
		this.theme = theme;
	}

    public ISelectModel getMarkModel()
    {
        return this.markModel;
    }

    public void setMarkModel(final ISelectModel markModel)
    {
        this.markModel = markModel;
    }

    public OrgUnitHolder getInitOrgUnit()
    {
        return initOrgUnit;
    }

    public EntityHolder<Student> getInitStudent()
    {
        return initStudent;
    }

    public EntityHolder<EppStudentWpeCAction> getInitWpcaSlot()
    {
        return initWpcaSlot;
    }

    public boolean isCanMark()
    {
        return canMark;
    }

    public void setCanMark(boolean canMark)
    {
        this.canMark = canMark;
    }
}
