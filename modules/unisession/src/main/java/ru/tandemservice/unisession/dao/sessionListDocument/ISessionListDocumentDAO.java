// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisession.dao.sessionListDocument;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import org.tandemframework.shared.commonbase.base.util.INumberGenerationRule;
import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.unisession.entity.document.SessionListDocument;

/**
 * @author iolshvang
 * @since 05.05.2011
 */
public interface ISessionListDocumentDAO
{
    SpringBeanCache<ISessionListDocumentDAO> instance = new SpringBeanCache<ISessionListDocumentDAO>(ISessionListDocumentDAO.class.getName());

    /**
     * Правило генерации номера для экз. карточки при создании новой
     * Логика в проекте - уникальные номера, инкремент с единицы
     * в рамках календарного года даты формирования и подразделения
     * @return правило
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    INumberGenerationRule<SessionListDocument> getNumberGenerationRule();
}
