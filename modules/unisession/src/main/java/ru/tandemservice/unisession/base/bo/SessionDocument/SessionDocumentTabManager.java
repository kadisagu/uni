/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionDocument;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unisession.base.bo.SessionDocument.logic.ISessionDocumentDao;
import ru.tandemservice.unisession.base.bo.SessionDocument.logic.SessionDocumentDao;

/**
 * @author Alexey Lopatin
 * @since 07.05.2015
 */
@Configuration
public class SessionDocumentTabManager extends BusinessObjectManager
{
    public static SessionDocumentTabManager instance()
    {
        return instance(SessionDocumentTabManager.class);
    }

    @Bean
   	public ISessionDocumentDao dao()
   	{
   		return new SessionDocumentDao();
   	}
}
