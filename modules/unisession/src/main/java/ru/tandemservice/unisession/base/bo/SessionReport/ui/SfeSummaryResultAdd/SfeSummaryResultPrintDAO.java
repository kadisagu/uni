/* $Id: $ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.SfeSummaryResultAdd;

import jxl.CellView;
import jxl.write.*;
import jxl.write.Number;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniepp.entity.catalog.codes.EppRegistryStructureCodes;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.unisession.base.bo.SessionReport.logic.SfaRaw;
import ru.tandemservice.unisession.base.bo.SessionReport.logic.StateFinalAttestationUtil;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.SfaSummaryResultAdd.logic.SfaSumReportInfo;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.SfaSummaryResultAdd.logic.Row;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.SfaSummaryResultAdd.logic.SfaSummaryResultPrintDAO;
import ru.tandemservice.unisession.entity.report.SfaSummaryResult;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static ru.tandemservice.unisession.base.bo.SessionReport.logic.StateFinalAttestationUtil.*;

/**
 * @author Andrey Andreev
 * @since 02.11.2016
 */
public class SfeSummaryResultPrintDAO extends SfaSummaryResultPrintDAO<SfaRaw, Row>
{

    @Override
    public SfaSumReportInfo<SfaRaw> initReportInfo(SfaSummaryResult report)
    {
        SfaSumReportInfo<SfaRaw> reportInfo = new SfaSumReportInfo<>(report);

        DQLSelectBuilder eouBuilder = getEducationOrgUnitSelectBuilder(reportInfo);

        DQLSelectBuilder slotBuilder = getSlotSelectBuilder(reportInfo, eouBuilder)
                .where(and(not(eq(property(REG_ALIAS, EppRegistryElement.parent().code()),
                                  value(EppRegistryStructureCodes.REGISTRY_ATTESTATION_DIPLOMA))),
                           eq(property(REG_ALIAS, EppRegistryElement.parent().parent().code()),
                              value(EppRegistryStructureCodes.REGISTRY_ATTESTATION))
                ));

        slotBuilder.column(property(EOU_ALIAS))
                .column(property(WPE_ALIAS))
                .column(property(MARK_ALIAS));

        List<Object[]> slots = getList(slotBuilder);
        if (slots.isEmpty() && report.isSkipEmpty())
            throw new ApplicationException("Нет данных для построения отчета. Нет студентов с выбранными параметрами.");

        List<EducationOrgUnit> educationOrgUnits = getList(eouBuilder);
        if (educationOrgUnits.isEmpty())
            throw new ApplicationException("Нет данных для построения отчета. Нет направлений с выбранными параметрами.");

        reportInfo.setEducationOrgUnits(educationOrgUnits);
        reportInfo.setRaws(StateFinalAttestationUtil.filterRaws(slots.stream().map(SfaRaw::new)));

        return reportInfo;
    }

    @Override
    protected String getFileName(SfaSummaryResult report)
    {
        return "Сводный отчет по результатам государственного экзамена.xls";
    }

    @Override
    protected int printTitle(SfaSummaryResult report, WritableSheet sheet)
    {
        int row = 0;

        addTextCell(sheet, 0, row, 10, 1, "Сводный отчет по результатам государственного экзамена на " + report.getEducationYear().getTitle() + " уч. год", _titleStyle);

        return ++row;
    }


    @Override
    protected int addHeaderTable(SfaSumReportInfo<SfaRaw> reportInfo, WritableSheet sheet, int rowIndex)
    {
        int firstRow = rowIndex;
        int col = 0;

        try
        {
            sheet.setRowView(rowIndex + 1, 2000);

            sheet.setColumnView(col, 60);
            addTextCell(sheet, col++, rowIndex, 1, 2, "Структурное подразделение", _horHeaderDataTS);

            addTextCell(sheet, col, rowIndex, 7, 1, "Количество выпускников", _horHeaderDataTS);
            sheet.addCell(new Label(col++, rowIndex + 1, "Всего", _verHeaderDataTS));
            sheet.addCell(new Label(col++, rowIndex + 1, "В академическом отпуске", _verHeaderDataTS));
            sheet.addCell(new Label(col++, rowIndex + 1, "Обязаны сдавать", _verHeaderDataTS));
            sheet.addCell(new Label(col++, rowIndex + 1, "Отсрочка", _verHeaderDataTS));
            sheet.addCell(new Label(col++, rowIndex + 1, "Недопущены", _verHeaderDataTS));
            sheet.addCell(new Label(col++, rowIndex + 1, "Явились на госэкзамен", _verHeaderDataTS));
            sheet.addCell(new Label(col++, rowIndex + 1, "Сдали госэкзамен", _verHeaderDataTS));

            addTextCell(sheet, col, rowIndex, 4, 1, "Из них получили оценки", _horHeaderDataTS);
            sheet.addCell(new Number(col++, rowIndex + 1, 5, _horHeaderDataTS));
            sheet.addCell(new Number(col++, rowIndex + 1, 4, _horHeaderDataTS));
            sheet.addCell(new Number(col++, rowIndex + 1, 3, _horHeaderDataTS));
            sheet.addCell(new Number(col++, rowIndex + 1, 2, _horHeaderDataTS));

            addTextCell(sheet, col++, rowIndex, 1, 2, "Успеваемость, %", _verHeaderDataTS);
            addTextCell(sheet, col++, rowIndex, 1, 2, "Качественный \r\nпоказатель, %", _verHeaderDataTS);

            tableWidth = col - 1;

            rowIndex = rowIndex + 2;
            for (; col > 0; col--)// номера колонок
                sheet.addCell(new Number(col - 1, rowIndex, col, _horHeaderBoldDataTS));

            fillArea(sheet, 0, tableWidth, firstRow - 1, firstRow - 1, _topThickBoardTS);
            fillArea(sheet, tableWidth + 1, tableWidth + 1, firstRow, rowIndex, _rightThickTableBoardTS);
        }
        catch (WriteException e)
        {
            e.printStackTrace();
        }

        return ++rowIndex;
    }


    @Override
    protected int addHeaderTableDetail(SfaSumReportInfo<SfaRaw> reportInfo, WritableSheet sheet, int rowIndex)
    {
        int firstRow = rowIndex;
        int col = 0;

        try
        {
            sheet.setRowView(rowIndex + 1, 2000);

            addTextCell(sheet, col++, rowIndex, 1, 2, "Поколение перечня", _verHeaderDataTS);
            sheet.setColumnView(col, 13);
            addTextCell(sheet, col++, rowIndex, 1, 2, "Код", _horHeaderDataTS);
            CellView columnView = sheet.getColumnView(col);
            columnView.setAutosize(true);
            sheet.setColumnView(col, columnView);
            addTextCell(sheet, col++, rowIndex, 1, 2, "Специальность/направление", _horHeaderDataTS);
            sheet.setColumnView(col, 50);
            addTextCell(sheet, col++, rowIndex, 1, 2, "Название дисциплины \r\n(в противном случае экзамен междисциплинарный)", _horHeaderDataTS);
            sheet.setColumnView(col, 13);
            addTextCell(sheet, col++, rowIndex, 1, 2, "Дата проведения", _verHeaderDataTS);

            addTextCell(sheet, col, rowIndex, 7, 1, "Количество выпускников", _horHeaderDataTS);
            sheet.addCell(new Label(col++, rowIndex + 1, "Всего", _verHeaderDataTS));
            sheet.addCell(new Label(col++, rowIndex + 1, "В академическом отпуске", _verHeaderDataTS));
            sheet.addCell(new Label(col++, rowIndex + 1, "Обязаны сдавать", _verHeaderDataTS));
            sheet.addCell(new Label(col++, rowIndex + 1, "Отсрочка", _verHeaderDataTS));
            sheet.addCell(new Label(col++, rowIndex + 1, "Недопущены", _verHeaderDataTS));
            sheet.addCell(new Label(col++, rowIndex + 1, "Явились на госэкзамен", _verHeaderDataTS));
            sheet.addCell(new Label(col++, rowIndex + 1, "Сдали госэкзамен", _verHeaderDataTS));

            addTextCell(sheet, col, rowIndex, 4, 1, "Из них получили оценки", _horHeaderDataTS);
            sheet.addCell(new Number(col++, rowIndex + 1, 5, _horHeaderDataTS));
            sheet.addCell(new Number(col++, rowIndex + 1, 4, _horHeaderDataTS));
            sheet.addCell(new Number(col++, rowIndex + 1, 3, _horHeaderDataTS));
            sheet.addCell(new Number(col++, rowIndex + 1, 2, _horHeaderDataTS));

            addTextCell(sheet, col++, rowIndex, 1, 2, "Успеваемость, %", _verHeaderDataTS);
            addTextCell(sheet, col++, rowIndex, 1, 2, "Качественный \r\nпоказатель, %", _verHeaderDataTS);

            tableWidth = col - 1;

            rowIndex = rowIndex + 2;
            for (; col > 0; col--)
                sheet.addCell(new Number(col - 1, rowIndex, col, _horHeaderBoldDataTS));

            fillArea(sheet, 0, tableWidth, firstRow - 1, firstRow - 1, _topThickBoardTS);
            fillArea(sheet, tableWidth + 1, tableWidth + 1, rowIndex - 2, rowIndex, _rightThickTableBoardTS);
        }
        catch (WriteException e)
        {
            e.printStackTrace();
        }

        return ++rowIndex;
    }


    @Override
    public void addDataRow(WritableSheet sheet, int startCol, int rowIndex, Row row)
    {
        addIntCell(sheet, startCol++, rowIndex, row.getTotal(), row.getIntStyle());
        addIntCell(sheet, startCol++, rowIndex, row.getVacation(), row.getIntStyle());
        addIntCell(sheet, startCol++, rowIndex, row.getMust(), row.getIntStyle());
        addIntCell(sheet, startCol++, rowIndex, row.getDelay(), row.getIntStyle());
        addIntCell(sheet, startCol++, rowIndex, row.getNotAllowed(), row.getIntStyle());
        addIntCell(sheet, startCol++, rowIndex, row.getAppeared(), row.getIntStyle());
        addIntCell(sheet, startCol++, rowIndex, row.getPassed(), row.getIntStyle());
        addIntCell(sheet, startCol++, rowIndex, row.getMark5(), row.getIntStyle());
        addIntCell(sheet, startCol++, rowIndex, row.getMark4(), row.getIntStyle());
        addIntCell(sheet, startCol++, rowIndex, row.getMark3(), row.getIntStyle());
        addIntCell(sheet, startCol++, rowIndex, row.getMark2(), row.getRedIntStyle());

        addDoubleCell(sheet, startCol++, rowIndex, row.getProgress(), row.getDoubleStyle());
        addDoubleCell(sheet, startCol, rowIndex, row.getGoodProgress(), row.getDoubleStyle());
    }


    // Не Детализованные строки
    @Override
    protected Row newFouRow(SfaSumReportInfo<SfaRaw> reportInfo, String title, boolean first)
    {
        Row row = new Row(first ? _fouTitleTS : _fouSubTitleTS,
                          first ? _fouIntTS : _fouSubIntTS,
                          first ? _fouRedIntTS : _fouSubRedIntTS,
                          first ? _fouDoubleTS : _fouSubDoubleTS);
        row.setTitle(title);
        return row;
    }

    @Override
    protected Row newFinalRow(SfaSumReportInfo<SfaRaw> reportInfo, String title, boolean first)
    {
        Row row = new Row(first ? _totalTitleTS : _totalSubTitleTS,
                          first ? _totalIntTS : _totalSubIntTS,
                          first ? _totalRedIntTS : _totalSubRedIntTS,
                          first ? _totalDoubleTS : _totalSubDoubleTS);
        row.setTitle(title);
        return row;
    }


    // Детализованные строки
    @Override
    protected Row newRegElRow(SfaSumReportInfo<SfaRaw> reportInfo, EducationOrgUnit educationOrgUnit, String title)
    {
        return new Row(educationOrgUnit, title, _regElTitleDetailTS, _regElDetailIntTS, _regElDetailRedIntTS, _regElDetailDoubleTS);
    }

    @Override
    protected Row newTotalDirectionRow(SfaSumReportInfo<SfaRaw> reportInfo)
    {
        return new Row(null, _touTotalCellDtlTS, _touTotalIntDtlTS, _touTotalRedIntDtlTS, _touTotalDoubleDtlTS);
    }

    @Override
    protected Row newDetailTouRow(SfaSumReportInfo<SfaRaw> reportInfo, String title, boolean first)
    {
        Row row = new Row(null,
                          first ? _touTotalCellThickTopDtlTS : _touTotalCellDtlTS,
                          first ? _touTotalIntThickTopDtlTS : _touTotalIntDtlTS,
                          first ? _touTotalRedIntThickTopDtlTS : _touTotalRedIntDtlTS,
                          first ? _touTotalDoubleThickTopDtlTS : _touTotalDoubleDtlTS);
        row.setTitle(title);
        return row;
    }

    @Override
    protected Row newDetailBranchesRow(SfaSumReportInfo<SfaRaw> reportInfo, String title, boolean first)
    {
        return new Row(null, title,
                       first ? _tousTotalCellThickTopDtlTS : _tousTotalCellDtlTS,
                       first ? _tousTotalIntThickTopDtlTS : _tousTotalIntDtlTS,
                       first ? _tousTotalRedIntThickTopDtlTS : _tousTotalRedIntDtlTS,
                       first ? _tousTotalDoubleThickTopDtlTS : _tousTotalDoubleDtlTS);
    }

    @Override
    protected Row newDetailFouRow(SfaSumReportInfo<SfaRaw> reportInfo, String title, boolean first)
    {
        return new Row(null, title,
                       first ? _fouTotalCellThickTopDtlTS : _fouTotalCellDtlTS,
                       first ? _fouTotalIntThickTopDtlTS : _fouTotalIntDtlTS,
                       first ? _fouTotalRedIntThickTopDtlTS : _fouTotalRedIntDtlTS,
                       first ? _fouTotalDoubleThickTopDtlTS : _fouTotalDoubleDtlTS);
    }

    @Override
    protected Row newDetailFinalRow(SfaSumReportInfo<SfaRaw> reportInfo, String title, boolean first)
    {
        return new Row(null, title,
                       first ? _finalTotalCellThickTopDtlTS : _finalTotalCellDtlTS,
                       first ? _finalTotalIntThickTopDtlTS : _finalTotalIntDtlTS,
                       first ? _finalTotalRedIntThickTopDtlTS : _finalTotalRedIntDtlTS,
                       first ? _finalTotalDoubleThickTopDtlTS : _finalTotalDoubleDtlTS);
    }

    @Override
    protected Row newDetailSvodRow(SfaSumReportInfo<SfaRaw> reportInfo, String title, boolean first)
    {
        return new Row(null, title,
                       first ? _svodTotalCellThickTopDtlTS : _svodTotalCellDtlTS,
                       first ? _svodTotalIntThickTopDtlTS : _svodTotalIntDtlTS,
                       first ? _svodTotalRedIntThickTopDtlTS : _svodTotalRedIntDtlTS,
                       first ? _svodTotalDoubleThickTopDtlTS : _svodTotalDoubleDtlTS);
    }


}
