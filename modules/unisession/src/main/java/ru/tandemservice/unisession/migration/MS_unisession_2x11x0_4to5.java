package ru.tandemservice.unisession.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unisession_2x11x0_4to5 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.11.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность sfaSummaryResult

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("sfa_summary_result_t",
									  new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_sfasummaryresult"),
									  new DBColumn("educationyear_id", DBType.LONG).setNullable(false),
									  new DBColumn("qualification_id", DBType.LONG),
									  new DBColumn("formativeorgunit_id", DBType.LONG),
									  new DBColumn("territorialorgunit_id", DBType.LONG),
									  new DBColumn("ownerorgunit_id", DBType.LONG),
									  new DBColumn("educationlevelhighschool_id", DBType.LONG),
									  new DBColumn("developform_id", DBType.LONG),
									  new DBColumn("developcondition_id", DBType.LONG),
									  new DBColumn("developtech_id", DBType.LONG),
									  new DBColumn("developperiod_id", DBType.LONG),
									  new DBColumn("compensationtype_id", DBType.LONG),
									  new DBColumn("datefrom_p", DBType.TIMESTAMP),
									  new DBColumn("dateto_p", DBType.TIMESTAMP),
									  new DBColumn("detailbydirections_p", DBType.BOOLEAN).setNullable(false),
									  new DBColumn("skipempty_p", DBType.BOOLEAN).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("sfaSummaryResult");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность fqwSummaryResult

		// создана новая сущность
		{
			// у сущности нет своей таблицы - ничего делать не надо
			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("fqwSummaryResult");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность sfeSummaryResult

		// создана новая сущность
		{
			// у сущности нет своей таблицы - ничего делать не надо
			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("sfeSummaryResult");

		}
	}
}