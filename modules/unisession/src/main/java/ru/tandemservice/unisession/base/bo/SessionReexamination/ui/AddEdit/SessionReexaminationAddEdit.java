/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReexamination.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm;
import ru.tandemservice.unisession.base.bo.SessionReexamination.SessionReexaminationManager;
import ru.tandemservice.unisession.base.bo.SessionReexamination.logic.SessionEpvRegistryRowTermDSHandler;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.notIn;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Alexey Lopatin
 * @since 26.08.2015
 */
@Configuration
public class SessionReexaminationAddEdit extends BusinessComponentManager
{
    public static final String EDU_DOCUMENT_DS = "eduDocumentDS";
    public static final String RE_EXAMINATION_ACTION_DS = "reExaminationActionDS";
    public static final String RE_ATTESTATION_ACTION_DS = "reAttestationActionDS";


    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(EDU_DOCUMENT_DS, eduDocumentDS()))
                .addDataSource(selectDS(RE_EXAMINATION_ACTION_DS, reExaminationActionDS()))
                .addDataSource(selectDS(RE_ATTESTATION_ACTION_DS, reAttestationActionDS()))
                .addDataSource(SessionReexaminationManager.instance().markDSConfig())
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> eduDocumentDS()
    {
        return new EntityComboDataSourceHandler(getName(), PersonEduDocument.class)
                .where(PersonEduDocument.person(), SessionReexaminationAddEditUI.PARAM_PERSON)
                .filter(PersonEduDocument.eduDocumentKind().title())
                .filter(PersonEduDocument.seria())
                .filter(PersonEduDocument.number());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> reExaminationActionDS()
    {
        return new SessionEpvRegistryRowTermDSHandler(getName())
        {
            @Override
            public DQLSelectBuilder createBuilder(String alias, ExecutionContext context)
            {
                DQLSelectBuilder builder = super.createBuilder(alias, context);
                List<EppEpvRowTerm> actionList = context.get(SessionReexaminationAddEditUI.PARAM_RE_ATTESTATION_ACTION_LIST);
                if (null != actionList && !actionList.isEmpty())
                    builder.where(notIn(property(alias), actionList));
                return builder;
            }
        };
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> reAttestationActionDS()
    {
        return new SessionEpvRegistryRowTermDSHandler(getName())
        {
            @Override
            public DQLSelectBuilder createBuilder(String alias, ExecutionContext context)
            {
                DQLSelectBuilder builder = super.createBuilder(alias, context);
                List<EppEpvRowTerm> actionList = context.get(SessionReexaminationAddEditUI.PARAM_RE_EXAMINATION_ACTION_LIST);
                if (null != actionList && !actionList.isEmpty())
                    builder.where(notIn(property(alias), actionList));
                return builder;
            }
        };
    }
}
