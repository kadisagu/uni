package ru.tandemservice.unisession.entity.document;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.shared.commonbase.base.util.INumberGenerationRule;
import org.tandemframework.shared.commonbase.base.util.Wiki;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinMark.SessionMarkableDocHolder;
import ru.tandemservice.unisession.dao.bulletin.ISessionBulletinDAO;
import ru.tandemservice.unisession.entity.comission.SessionComission;
import ru.tandemservice.unisession.entity.document.gen.SessionRetakeDocumentGen;
import ru.tandemservice.unisession.sec.ISessionSecDao;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;

/**
 * Ведомость пересдач
 */
public class SessionRetakeDocument extends SessionRetakeDocumentGen implements SessionMarkableDocHolder.IMarkableDoc, SessionObject.ISessionObjectDependentDocument
{
    @Override
    @EntityDSLSupport(parts=SessionDocument.P_NUMBER)
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1064145")
    public String getTypeTitle()
    {
        return "Ведомость пересдач №" + this.getNumber();
    }

    @Override
    @EntityDSLSupport(parts=SessionDocument.P_NUMBER)
    public String getTypeShortTitle()
    {
        return "Вед. пересдач №" + this.getNumber();
    }

    @Override
    @EntityDSLSupport(parts = "number")
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1064145")
    public String getTitle()
    {
        if (getRegistryElementPart() == null) {
            return this.getClass().getSimpleName();
        }
        return this.getTypeTitle() + " " + this.getRegistryElementPart().getTitleWithNumber();
    }

    @Override
    public Collection<IEntity> getSecLocalEntities()
    {
        return ISessionSecDao.instance.get().getSecLocalEntities(this);
    }

    @Override
    public INumberGenerationRule<SessionDocument> getNumberGenerationRule() {
        return ISessionBulletinDAO.instance.get().getNumberGenerationRule();
    }

    public boolean isClosed()
    {
        return this.getCloseDate() != null;
    }

    @Override
    public SessionMarkableDocHolder.IMarkableDocProperties getMarkingProperties() {
        return new SessionMarkableDocHolder.IMarkableDocProperties() {
            @Override
            public Date getPerformDate() {
                return SessionRetakeDocument.this.getPerformDate();
            }

            @Override
            public SessionComission getCommission() {
                return SessionRetakeDocument.this.getCommission();
            }

            @Override
            public EppRegistryElementPart getRegistryElementPart() {
                return SessionRetakeDocument.this.getRegistryElementPart();
            }

            @Override
            public EppFControlActionType getCAType() {
                return null;
            }

            @Override
            public SessionObject getSessionObject() {
                return SessionRetakeDocument.this.getSessionObject();
            }
        };
    }

    @Override
    public OrgUnit getGroupOu()
    {
        return getSessionObject().getGroupOu();
    }
}