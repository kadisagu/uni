/* $Id:$ */
package ru.tandemservice.unisession.brs.util;

import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.formatter.IFormatter;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;

/**
 * @author oleyba
 * @since 11/25/13
 */
public class BrsIRatingValueFormatter implements IFormatter<ISessionBrsDao.IRatingValue>
{
    public static final IFormatter< ISessionBrsDao.IRatingValue> instance = new BrsIRatingValueFormatter();

    @Override
    public String format(ISessionBrsDao.IRatingValue source)
    {
        if (source == null || source.getValue() == null) return "";
        if (source.getValue().isNaN() || source.getValue().isInfinite()) return "";
        return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(source.getValue());
    }
}

