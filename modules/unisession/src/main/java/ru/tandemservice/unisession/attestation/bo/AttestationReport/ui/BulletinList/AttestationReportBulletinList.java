/* $Id: SessionReportSummaryBulletinList.java 22080 2012-02-27 09:41:30Z oleyba $ */
package ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.BulletinList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import ru.tandemservice.unisession.attestation.bo.AttestationReport.AttestationReportManager;
import ru.tandemservice.unisession.attestation.entity.report.SessionAttestationBulletinReport;

/**
 * @author oleyba
 * @since 12/16/11
 */
@Configuration
public class AttestationReportBulletinList extends BusinessComponentManager
{
    public static final String DS_REPORTS = "attestationReportBulletinDS";
    public static final String DS_GROUP = "groupDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
            .addDataSource(AttestationReportManager.instance().attestationDSConfig())
            .addDataSource(this.searchListDS(DS_REPORTS, this.attestationReportBulletinDS(), this.attestationReportBulletinDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint attestationReportBulletinDS() {
        return this.columnListExtPointBuilder(DS_REPORTS)
            .addColumn(indicatorColumn("icon").defaultIndicatorItem(new IndicatorColumn.Item("report", "Отчет")).create())
            .addColumn(publisherColumn("date", SessionAttestationBulletinReport.formingDate()).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).order().create())
            .addColumn(textColumn("attestation", SessionAttestationBulletinReport.attestation().shortTitle().s()).create())
            .addColumn(textColumn("discipline", SessionAttestationBulletinReport.discipline().s()).order().create())
            .addColumn(textColumn("group", SessionAttestationBulletinReport.group().s()).order().create())
            .addColumn(textColumn("exec", SessionAttestationBulletinReport.executor().s()).create())
            .addColumn(actionColumn("print", CommonDefines.ICON_PRINT, "onClickPrint"))
            .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER).permissionKey("ui:deleteStorableReportPermissionKey")
                .alert(FormattedMessage.with().template("attestationReportBulletinDS.delete.alert").parameter(SessionAttestationBulletinReport.formingDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).create())
            )
        .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> attestationReportBulletinDSHandler() {
        return new AttestationReportBulletinDSHandler(this.getName());
    }

}
