package ru.tandemservice.unisession.attestation.bo.Attestation.event;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Created with IntelliJ IDEA.
 * User: makarova
 * Date: 10.12.12
 */
public class SessionAttestationListener  implements IDSetEventListener
{
    public void init()
    {
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, SessionAttestationBulletin.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.beforeUpdate, SessionAttestationBulletin.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.beforeDelete, SessionAttestationBulletin.class, this);
    }

    @Override
    public void onEvent(final DSetEvent event)
    {
        final DQLSelectBuilder closedBull = new DQLSelectBuilder()
                .fromEntity(SessionAttestationBulletin.class, "sess")
                .where(in(property(SessionAttestationBulletin.id().fromAlias("sess")), event.getMultitude().getInExpression()))
                .where(isNotNull(property(SessionAttestationBulletin.attestation().closeDate().fromAlias("sess"))))
                .column(property(SessionAttestationBulletin.id()));

        final Number countClosedBul = closedBull.createCountStatement(event.getContext()).uniqueResult();

        if (countClosedBul != null && countClosedBul.longValue() > 0) {
            throw new ApplicationException("Открытие аттестационной ведомости невозможно, так как аттестация закрыта.", true);
        }
    }
}
