package ru.tandemservice.unisession.entity.catalog;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.BaseDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import ru.tandemservice.unisession.entity.catalog.gen.SessionsSimpleDocumentReasonGen;

/**
 * Причины формирования экзаменационных карточек и листов
 */
public class SessionsSimpleDocumentReason extends SessionsSimpleDocumentReasonGen implements IDynamicCatalogItem
{

    public static IDynamicCatalogDesc getUiDesc()
    {
        return new BaseDynamicCatalogDesc()
        {
            @Override
            public void addAdditionalColumns(DynamicListDataSource<ICatalogItem> dataSource)
            {
                dataSource.getColumns().removeIf(column -> column.getName().equals(SessionsSimpleDocumentReason.P_FORM_ON_DEBTS));
                dataSource.addColumn(
                        new ToggleColumn("Формирование по задолженностям", SessionsSimpleDocumentReason.P_FORM_ON_DEBTS)
                                .setListener(ChangeFormOnDebtsAction.NAME)
                                .setPermissionKey("catalogItem_edit_" + SessionsSimpleDocumentReason.ENTITY_NAME)
                );
            }
        };
    }

    public static class ChangeFormOnDebtsAction extends NamedUIAction
    {
        public static final String NAME = "onChangeFormOnDebts";

        public ChangeFormOnDebtsAction()
        {
            super(NAME);
        }

        @Override
        public void execute(final IUIPresenter presenter)
        {
            ICommonDAO dao = DataAccessServices.dao();
            SessionsSimpleDocumentReason reason = dao.get(SessionsSimpleDocumentReason.class, presenter.getListenerParameterAsLong());
            reason.setFormOnDebts(!reason.isFormOnDebts());
            dao.save(reason);
            presenter.getSupport().doRefresh();
        }
    }
}