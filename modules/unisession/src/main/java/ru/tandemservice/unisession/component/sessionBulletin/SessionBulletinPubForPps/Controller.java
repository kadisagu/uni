/* $*/

package ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinPubForPps;

import org.tandemframework.caf.service.impl.CAFLegacySupportService;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.unisession.base.bo.SessionBulletin.ui.ThemeEdit.SessionBulletinThemeEdit;

/**
 * @author oleyba
 * @since 2/18/11
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        model.setPrincipalContext(component.getUserContext().getPrincipalContext());
        this.getDao().prepare(model);
    }

    public void onClickMark(final IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(
                                          ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinMark.Model.COMPONENT_NAME,
                                          new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, this.getModel(component).getBulletin().getId()))
        );
    }

    public void onClickEditThemes(final IBusinessComponent component)
    {
		final Model model = this.getModel(component);
		CAFLegacySupportService.asDesktopRoot(SessionBulletinThemeEdit.class.getSimpleName())
				.parameter(UIPresenter.PUBLISHER_ID, model.getBulletin().getId())
				.activate();
    }
}