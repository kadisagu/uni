/* $Id: $ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.SfaSummaryResultList;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.IColumnListExtPointBuilder;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon.Filters;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.entity.report.SfaSummaryResult;

import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Andrey Andreev
 * @since 27.10.2016
 */
public abstract class SessionReportSfaSummaryResultList extends BusinessComponentManager
{
    public static final String DS_REPORTS = "stateFinalExamResultDS";
    public static final String EDUCATION_YEAR_PARAM = "educationYear";
    public static final String EDUCATION_ORG_UNIT_UTIL_PARAM = "educationOrgUnitUtil";

    public IColumnListExtPointBuilder addDefaultResultsListColumns(IColumnListExtPointBuilder builder)
    {
        builder.addColumn(publisherColumn("formingDate", SfaSummaryResult.formingDate())
                                  .addParameter(SessionReportManager.BIND_ORG_UNIT, "ui:ouHolder.id")
                                  .formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).order().create())
                .addColumn(textColumn("educationYear", SfaSummaryResult.educationYear().title().s()).order().create())
                .addColumn(booleanColumn("detailByDirections", SfaSummaryResult.detailByDirections().s()).create())

                .addColumn(textColumn("formativeOrgUnit", SfaSummaryResult.formativeOrgUnit().title().s()).create())
                .addColumn(textColumn("territorialOrgUnit", SfaSummaryResult.territorialOrgUnit().title().s()).create())
                .addColumn(textColumn("ownerOrgUnit", SfaSummaryResult.ownerOrgUnit().title().s()).order().create())
                .addColumn(textColumn("educationLevelHighSchool", SfaSummaryResult.educationLevelHighSchool().displayableTitle().s()).order().create())
                .addColumn(textColumn("developForm", SfaSummaryResult.developForm().title().s()).create())
                .addColumn(textColumn("developCondition", SfaSummaryResult.developCondition().title().s()).create())
                .addColumn(textColumn("developTech", SfaSummaryResult.developTech().title().s()).create())

                .addColumn(textColumn("periodStateExams", "")
                                   .formatter(source -> ((SfaSummaryResult) source).getExamsPeriod())
                                   .create());

        customResultsListColumns(builder);

        builder.addColumn(actionColumn("print", CommonDefines.ICON_PRINT, "onClickPrint"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, "onClickDelete")
                                   .permissionKey("ui:deletePermissionKey")
                                   .alert(FormattedMessage.with().template("sfeSummaryResultDS.delete.alert")
                                                  .parameter(SfaSummaryResult.formingDate().s()).create())
                );

        return builder;
    }

    public void customResultsListColumns(IColumnListExtPointBuilder builder)
    {

    }

    public IReadAggregateHandler<DSInput, DSOutput> getSessionReportResultsDSHandler(String ownerId, Class<? extends SfaSummaryResult> reportClass)
    {
        return new DefaultSearchDataSourceHandler(ownerId)
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                String alias = "e";
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(reportClass, alias)
                        .column(property(alias));

                UniEduProgramEducationOrgUnitAddon util = context.get(EDUCATION_ORG_UNIT_UTIL_PARAM);
                if (util != null)
                {
                    FilterUtils.applySelectFilter(builder, alias, SfaSummaryResult.qualification().s(), util.getFilterValue(Filters.QUALIFICATION));
                    FilterUtils.applySelectFilter(builder, alias, SfaSummaryResult.formativeOrgUnit().s(), util.getFilterValue(Filters.FORMATIVE_ORG_UNIT));
                    FilterUtils.applySelectFilter(builder, alias, SfaSummaryResult.territorialOrgUnit().s(), util.getFilterValue(Filters.TERRITORIAL_ORG_UNIT));
                    FilterUtils.applySelectFilter(builder, alias, SfaSummaryResult.ownerOrgUnit().s(), util.getFilterValue(Filters.PRODUCING_ORG_UNIT));
                    FilterUtils.applySelectFilter(builder, alias, SfaSummaryResult.educationLevelHighSchool().s(), util.getFilterValue(Filters.EDUCATION_LEVEL_HIGH_SCHOOL));
                    FilterUtils.applySelectFilter(builder, alias, SfaSummaryResult.developCondition().s(), util.getFilterValue(Filters.DEVELOP_CONDITION));
                    FilterUtils.applySelectFilter(builder, alias, SfaSummaryResult.developForm().s(), util.getFilterValue(Filters.DEVELOP_FORM));
                    FilterUtils.applySelectFilter(builder, alias, SfaSummaryResult.developPeriod().s(), util.getFilterValue(Filters.DEVELOP_PERIOD));
                    FilterUtils.applySelectFilter(builder, alias, SfaSummaryResult.developTech().s(), util.getFilterValue(Filters.DEVELOP_TECH));
                }

                FilterUtils.applySelectFilter(builder, alias, SfaSummaryResult.educationYear(), context.get(EDUCATION_YEAR_PARAM));

                builder.joinPath(DQLJoinType.left, SfaSummaryResult.ownerOrgUnit().fromAlias(alias), "owou");
                builder.joinPath(DQLJoinType.left, SfaSummaryResult.educationLevelHighSchool().fromAlias(alias), "lhs");

                OrderDescription orderByDate = new OrderDescription(SfaSummaryResult.formingDate());
                DQLOrderDescriptionRegistry orderRegistry = new DQLOrderDescriptionRegistry(reportClass, alias);
                orderRegistry
                        .setOrders(SfaSummaryResult.formingDate(), orderByDate)
                        .setOrders(SfaSummaryResult.educationYear().title(), new OrderDescription(SfaSummaryResult.educationYear().title()))
                        .setOrders(SfaSummaryResult.educationLevelHighSchool().displayableTitle(), new OrderDescription("lhs", EducationLevelsHighSchool.displayableTitle()), orderByDate)
                        .setOrders(SfaSummaryResult.ownerOrgUnit().title(), new OrderDescription("owou", OrgUnit.title()), orderByDate);

                orderRegistry.applyOrder(builder, input.getEntityOrder());

                return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build();
            }
        };
    }
}
