package ru.tandemservice.unisession.entity.stateFinalExam.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unisession.entity.catalog.SessionRoleInGEC;
import ru.tandemservice.unisession.entity.stateFinalExam.SessionGECMember;
import ru.tandemservice.unisession.entity.stateFinalExam.SessionGovExamCommission;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Член ГЭК
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionGECMemberGen extends EntityBase
 implements INaturalIdentifiable<SessionGECMemberGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.stateFinalExam.SessionGECMember";
    public static final String ENTITY_NAME = "sessionGECMember";
    public static final int VERSION_HASH = 712009414;
    private static IEntityMeta ENTITY_META;

    public static final String L_COMMISSION = "commission";
    public static final String L_ROLE_IN_G_E_C = "roleInGEC";
    public static final String L_PPS = "pps";

    private SessionGovExamCommission _commission;     // Комиссия
    private SessionRoleInGEC _roleInGEC;     // Роль в комиссии
    private PpsEntry _pps;     // Преподаватель

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Комиссия. Свойство не может быть null.
     */
    @NotNull
    public SessionGovExamCommission getCommission()
    {
        return _commission;
    }

    /**
     * @param commission Комиссия. Свойство не может быть null.
     */
    public void setCommission(SessionGovExamCommission commission)
    {
        dirty(_commission, commission);
        _commission = commission;
    }

    /**
     * @return Роль в комиссии. Свойство не может быть null.
     */
    @NotNull
    public SessionRoleInGEC getRoleInGEC()
    {
        return _roleInGEC;
    }

    /**
     * @param roleInGEC Роль в комиссии. Свойство не может быть null.
     */
    public void setRoleInGEC(SessionRoleInGEC roleInGEC)
    {
        dirty(_roleInGEC, roleInGEC);
        _roleInGEC = roleInGEC;
    }

    /**
     * @return Преподаватель. Свойство не может быть null.
     */
    @NotNull
    public PpsEntry getPps()
    {
        return _pps;
    }

    /**
     * @param pps Преподаватель. Свойство не может быть null.
     */
    public void setPps(PpsEntry pps)
    {
        dirty(_pps, pps);
        _pps = pps;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SessionGECMemberGen)
        {
            if (withNaturalIdProperties)
            {
                setCommission(((SessionGECMember)another).getCommission());
                setRoleInGEC(((SessionGECMember)another).getRoleInGEC());
                setPps(((SessionGECMember)another).getPps());
            }
        }
    }

    public INaturalId<SessionGECMemberGen> getNaturalId()
    {
        return new NaturalId(getCommission(), getRoleInGEC(), getPps());
    }

    public static class NaturalId extends NaturalIdBase<SessionGECMemberGen>
    {
        private static final String PROXY_NAME = "SessionGECMemberNaturalProxy";

        private Long _commission;
        private Long _roleInGEC;
        private Long _pps;

        public NaturalId()
        {}

        public NaturalId(SessionGovExamCommission commission, SessionRoleInGEC roleInGEC, PpsEntry pps)
        {
            _commission = ((IEntity) commission).getId();
            _roleInGEC = ((IEntity) roleInGEC).getId();
            _pps = ((IEntity) pps).getId();
        }

        public Long getCommission()
        {
            return _commission;
        }

        public void setCommission(Long commission)
        {
            _commission = commission;
        }

        public Long getRoleInGEC()
        {
            return _roleInGEC;
        }

        public void setRoleInGEC(Long roleInGEC)
        {
            _roleInGEC = roleInGEC;
        }

        public Long getPps()
        {
            return _pps;
        }

        public void setPps(Long pps)
        {
            _pps = pps;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof SessionGECMemberGen.NaturalId) ) return false;

            SessionGECMemberGen.NaturalId that = (NaturalId) o;

            if( !equals(getCommission(), that.getCommission()) ) return false;
            if( !equals(getRoleInGEC(), that.getRoleInGEC()) ) return false;
            if( !equals(getPps(), that.getPps()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCommission());
            result = hashCode(result, getRoleInGEC());
            result = hashCode(result, getPps());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCommission());
            sb.append("/");
            sb.append(getRoleInGEC());
            sb.append("/");
            sb.append(getPps());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionGECMemberGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionGECMember.class;
        }

        public T newInstance()
        {
            return (T) new SessionGECMember();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "commission":
                    return obj.getCommission();
                case "roleInGEC":
                    return obj.getRoleInGEC();
                case "pps":
                    return obj.getPps();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "commission":
                    obj.setCommission((SessionGovExamCommission) value);
                    return;
                case "roleInGEC":
                    obj.setRoleInGEC((SessionRoleInGEC) value);
                    return;
                case "pps":
                    obj.setPps((PpsEntry) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "commission":
                        return true;
                case "roleInGEC":
                        return true;
                case "pps":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "commission":
                    return true;
                case "roleInGEC":
                    return true;
                case "pps":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "commission":
                    return SessionGovExamCommission.class;
                case "roleInGEC":
                    return SessionRoleInGEC.class;
                case "pps":
                    return PpsEntry.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionGECMember> _dslPath = new Path<SessionGECMember>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionGECMember");
    }
            

    /**
     * @return Комиссия. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.stateFinalExam.SessionGECMember#getCommission()
     */
    public static SessionGovExamCommission.Path<SessionGovExamCommission> commission()
    {
        return _dslPath.commission();
    }

    /**
     * @return Роль в комиссии. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.stateFinalExam.SessionGECMember#getRoleInGEC()
     */
    public static SessionRoleInGEC.Path<SessionRoleInGEC> roleInGEC()
    {
        return _dslPath.roleInGEC();
    }

    /**
     * @return Преподаватель. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.stateFinalExam.SessionGECMember#getPps()
     */
    public static PpsEntry.Path<PpsEntry> pps()
    {
        return _dslPath.pps();
    }

    public static class Path<E extends SessionGECMember> extends EntityPath<E>
    {
        private SessionGovExamCommission.Path<SessionGovExamCommission> _commission;
        private SessionRoleInGEC.Path<SessionRoleInGEC> _roleInGEC;
        private PpsEntry.Path<PpsEntry> _pps;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Комиссия. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.stateFinalExam.SessionGECMember#getCommission()
     */
        public SessionGovExamCommission.Path<SessionGovExamCommission> commission()
        {
            if(_commission == null )
                _commission = new SessionGovExamCommission.Path<SessionGovExamCommission>(L_COMMISSION, this);
            return _commission;
        }

    /**
     * @return Роль в комиссии. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.stateFinalExam.SessionGECMember#getRoleInGEC()
     */
        public SessionRoleInGEC.Path<SessionRoleInGEC> roleInGEC()
        {
            if(_roleInGEC == null )
                _roleInGEC = new SessionRoleInGEC.Path<SessionRoleInGEC>(L_ROLE_IN_G_E_C, this);
            return _roleInGEC;
        }

    /**
     * @return Преподаватель. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.stateFinalExam.SessionGECMember#getPps()
     */
        public PpsEntry.Path<PpsEntry> pps()
        {
            if(_pps == null )
                _pps = new PpsEntry.Path<PpsEntry>(L_PPS, this);
            return _pps;
        }

        public Class getEntityClass()
        {
            return SessionGECMember.class;
        }

        public String getEntityName()
        {
            return "sessionGECMember";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
