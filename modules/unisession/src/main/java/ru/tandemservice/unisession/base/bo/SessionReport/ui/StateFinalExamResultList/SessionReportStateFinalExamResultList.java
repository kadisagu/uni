/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.StateFinalExamResultList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uni.base.bo.UniEduProgram.UniEduProgramManager;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.StateFinalAttestationResultList.SessionReportStateFinalAttestationResultList;
import ru.tandemservice.unisession.entity.report.StateFinalExamResult;

/**
 * @author Andrey Andreev
 * @since 27.10.2016
 */
@Configuration
public class SessionReportStateFinalExamResultList extends SessionReportStateFinalAttestationResultList
{

    public static final String DS_REPORTS = "stateFinalExamResultDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addAddon(uiAddon(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME, UniEduProgramEducationOrgUnitAddon.class))
                .addDataSource(SessionReportManager.instance().eduYearDSConfig())
                .addDataSource(this.searchListDS(DS_REPORTS, sessionReportResultsDS(), sessionReportResultsDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint sessionReportResultsDS()
    {
        return addDefaultResultsListColumns(columnListExtPointBuilder(DS_REPORTS)).create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> sessionReportResultsDSHandler()
    {
        return getSessionReportResultsDSHandler(getName(), StateFinalExamResult.class);
    }
}
