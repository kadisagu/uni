/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReport.util.results;

import org.tandemframework.core.view.formatter.DoubleFormatter;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.ResultsAdd.ISessionReportResultsDAO;

import java.util.HashMap;
import java.util.Map;

/**
* @author oleyba
* @since 2/13/12
*/
public abstract class CalculatedReportColumn extends DoubleReportColumn
{
    protected Map<ISessionReportResultsDAO.ISessionResultsReportRow, Boolean> calculated = new HashMap<ISessionReportResultsDAO.ISessionResultsReportRow, Boolean>();
    private Map<ISessionReportResultsDAO.ISessionResultsReportRow, Double> count = new HashMap<ISessionReportResultsDAO.ISessionResultsReportRow, Double>();

    public abstract Double calculate(ISessionReportResultsDAO.ISessionResultsReportRow row);

    @Override
    public Double getCount(ISessionReportResultsDAO.ISessionResultsReportRow row)
    {
        Double value = count.get(row);
        if (!Boolean.TRUE.equals(calculated.get(row))) {
            count.put(row, value = calculate(row));
            calculated.put(row, Boolean.TRUE);
        }
        return value;
    }

    @Override
    public String cell(ISessionReportResultsDAO.ISessionResultsReportRow row)
    {
        final Double count = getCount(row);
        return null == count ? "" : DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(count);
    }
}
