/* $*/

package ru.tandemservice.unisession.print;

import java.util.Collection;

import org.tandemframework.rtf.document.RtfDocument;

import org.tandemframework.core.util.cache.SpringBeanCache;

/**
 * @author oleyba
 * @since 4/5/11
 */
public interface ISessionSheetPrintDAO
{
    SpringBeanCache<ISessionSheetPrintDAO> instance = new SpringBeanCache<ISessionSheetPrintDAO>(ISessionSheetPrintDAO.class.getName());

    /**
     * массовая печать хвостовок - в один rtf-документ, по четыре на лист
     * @param ids - id хвостовок
     * @return rtf
     */
    RtfDocument printSheetList(Collection<Long> ids);

    /**
     * печать хвостовки - одна на документ, соотв. формат страницы а6
     * @param id - id хвостовки
     * @return rtf
     */
    RtfDocument printSheet(Long id);
}
