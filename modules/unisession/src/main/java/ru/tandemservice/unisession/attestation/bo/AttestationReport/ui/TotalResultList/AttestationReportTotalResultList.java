/**
 *$Id$
 */
package ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.TotalResultList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.core.view.util.Icon;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.unisession.attestation.bo.AttestationReport.AttestationReportManager;
import ru.tandemservice.unisession.attestation.entity.SessionAttestation;
import ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;

import java.util.Collections;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 22.11.12
 */
@Configuration
public class AttestationReportTotalResultList extends BusinessComponentManager
{
    public static final String ATTESTATION_DS = AttestationReportManager.DS_ATTESTATION;
    public static final String EDU_YEAR_DS = SessionReportManager.DS_EDU_YEAR;
    public static final String YEAR_PART_DS = SessionReportManager.DS_YEAR_PART;
    public static final String SESS_ATT_TOTAL_RESULT_REPORT_SEARCH_DS = "sessionAttTotalResultReportSearchDS";


    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(AttestationReportManager.instance().attestationDSConfig())
                .addDataSource(SessionReportManager.instance().eduYearDSConfig())
                .addDataSource(SessionReportManager.instance().yearPartDSConfig())
                .addDataSource(searchListDS(SESS_ATT_TOTAL_RESULT_REPORT_SEARCH_DS, sessionAttTotalResultReportSearchDSColumns(), sessionAttTotalResultReportSearchDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint sessionAttTotalResultReportSearchDSColumns()
    {
        return columnListExtPointBuilder(SESS_ATT_TOTAL_RESULT_REPORT_SEARCH_DS)
                .addColumn(indicatorColumn("report").defaultIndicatorItem(new IndicatorColumn.Item("report")))
                .addColumn(textColumn("formingDate", SessionAttestationTotalResultReport.formingDate()).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).clickable(true).order())
                .addColumn(textColumn("attestation", SessionAttestationTotalResultReport.attestationTitle()))
                .addColumn(textColumn("course", SessionAttestationTotalResultReport.course()))
                .addColumn(actionColumn("printReport", new Icon("printer"), "onClickPrintReport"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, new Icon("delete"), "onClickDeleteReport")
                        .alert(FormattedMessage.with().template("sessionAttTotalResultReportSearchDS.delete.alert").parameter(SessionAttestationTotalResultReport.formingDate().s(), DateFormatter.DATE_FORMATTER_WITH_TIME).create())
                        .permissionKey("ui:deleteStorableReportPermissionKey"))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> sessionAttTotalResultReportSearchDSHandler()
    {
        return new DefaultSearchDataSourceHandler(getName(), SessionAttestationTotalResultReport.class)
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                final OrgUnit orgUnit = context.get(AttestationReportTotalResultListUI.PARAM_ORG_UNIT);
                final EducationYear eduYear = context.get(AttestationReportTotalResultListUI.PARAM_EDU_YEAR);
                final YearDistributionPart yearPart = context.get(AttestationReportTotalResultListUI.PARAM_YEAR_PART);
                final Long attNumber = context.get(AttestationReportTotalResultListUI.PARAM_ATT_NUMBER);
                final SessionAttestation attestation = context.get(AttestationReportTotalResultListUI.PARAM_ATTESTATION);

                if ((null == orgUnit || null == attestation) && (null == eduYear || null == yearPart || null == attNumber)) {
                    return ListOutputBuilder.get(input, Collections.emptyList()).build();
                }

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SessionAttestationTotalResultReport.class, "r").column(property("r"));

                if (orgUnit != null) {
                    builder.where(eq(property(SessionAttestationTotalResultReport.orgUnit().fromAlias("r")), value(orgUnit)));
                    if (attestation != null)
                        builder.where(eq(property(SessionAttestationTotalResultReport.attestation().fromAlias("r")), value(attestation)));
                }
                else {
                    builder.where(isNull(property(SessionAttestationTotalResultReport.orgUnit().fromAlias("r"))));
                    builder.where(eq(property(SessionAttestationTotalResultReport.educationYear().fromAlias("r")), value(eduYear)));
                    builder.where(eq(property(SessionAttestationTotalResultReport.yearDistributionPart().fromAlias("r")), value(yearPart)));
                    builder.where(eq(property(SessionAttestationTotalResultReport.attestationNumber().fromAlias("r")), value(attNumber.intValue())));
                }
                builder.order(property(SessionAttestationTotalResultReport.formingDate().fromAlias("r")), input.getEntityOrder().getDirection());

                return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build();
            }
        };
    }
}
