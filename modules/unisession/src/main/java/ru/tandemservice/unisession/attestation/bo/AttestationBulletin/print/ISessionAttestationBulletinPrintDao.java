/* $Id:$ */
package ru.tandemservice.unisession.attestation.bo.AttestationBulletin.print;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unisession.attestation.entity.SessionAttestation;
import ru.tandemservice.unisession.base.bo.SessionReport.util.UniAttestationFilterAddon;

import java.util.List;

/**
 * @author oleyba
 * @since 10/9/12
 */
public interface ISessionAttestationBulletinPrintDao extends INeedPersistenceSupport
{
    byte[] printBulletin(Long bulletinId);

	/**
	 * Подготовить печатную форму для аттестационных ведомостей (все ведомости в одном файле, каждая ведомость на отдельной странице).
	 * Сортировка ведомостей в файле - по названию группы, затем по названию элемента реестра, затем по номеру части элемента реестра
	 * @param bulletinIds список ID аттестационных ведомостей (неотсортированный)
	 */
	RtfDocument printBulletins(List<Long> bulletinIds);

    RtfDocument printBulletin(SessionAttestation attestation, UniAttestationFilterAddon addon, EppRegistryElementPart disc);
}
