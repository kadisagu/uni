/* $Id:$ */
package ru.tandemservice.unisession.attestation.bo.Attestation.logic;

import org.hibernate.Session;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.log.InsertEntityEvent;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unisession.attestation.bo.Attestation.AttestationManager;
import ru.tandemservice.unisession.attestation.bo.AttestationBulletin.AttestationBulletinManager;
import ru.tandemservice.unisession.attestation.entity.SessionAttestation;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin;
import ru.tandemservice.unisession.entity.document.SessionObject;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
/**
 * @author oleyba
 * @since 10/9/12
 */
public class SessionAttestationDao extends UniBaseDao implements ISessionAttestationDao
{
    @Override
    public SessionAttestation addAttestation(SessionObject sessionObject)
    {
        NamedSyncInTransactionCheckLocker.register(getSession(), "attestation_add_" + sessionObject.getId());

        List<SessionAttestation> attestations = new DQLSelectBuilder()
            .fromEntity(SessionAttestation.class, "s").column("s")
            .where(eq(property(SessionAttestation.sessionObject().fromAlias("s")), value(sessionObject)))
            .order(property(SessionAttestation.number().fromAlias("s")), OrderDirection.desc)
            .createStatement(getSession()).list();

        SessionAttestation attestation = new SessionAttestation();
        attestation.setSessionObject(sessionObject);
        attestation.setNumber(attestations.isEmpty() ? 1 : attestations.get(0).getNumber() + 1);
        save(attestation);
        return attestation;
    }

    @Override
    public void changeState(SessionAttestation attestation)
    {
        if (null == attestation.getCloseDate())
            attestation.setCloseDate(new Date());
        else
            attestation.setCloseDate(null);
        save(attestation);
    }

    public void doClose(SessionAttestation attestation)
    {
        Session session = lockAttestation(attestation);

        final IEventServiceLock eventLock = CoreServices.eventService().lock();

        // получаем все незакрытые Аттестационные ведомости  и закрываем их
        DQLSelectBuilder selectBuilder = new DQLSelectBuilder()
                .fromEntity(SessionAttestationBulletin.class, "bulletin")
                .column(property("bulletin"))
                .where(eq(property(SessionAttestationBulletin.attestation().fromAlias("bulletin")), value(attestation)))
                .where(isNull(property(SessionAttestationBulletin.closeDate().fromAlias("bulletin"))));

        if (selectBuilder != null) {
            List<SessionAttestationBulletin> bulletinList = selectBuilder.createStatement(session).list();

            try {
                for (SessionAttestationBulletin attestationBulletin : bulletinList) {
                    AttestationBulletinManager.instance().dao().doCloseBulletin(attestationBulletin);
                }
            } catch (final Throwable t){
                throw new ApplicationException("Нельзя закрыть аттестацию. " + t.getMessage());
            }
                finally {
                eventLock.release();
                CoreServices.eventService().fireEvent(new InsertEntityEvent("Аттестация закрыта.", attestation));
            }
        }
        session.flush();
        AttestationManager.instance().dao().changeState(attestation);
    }

    private Session lockAttestation(SessionAttestation attestation) {
        Session session = getSession();
        NamedSyncInTransactionCheckLocker.register(session, "attestation_close_" + attestation.getId());
        session.flush();
        return session;
    }
}
