/* $*/

package ru.tandemservice.unisession.component.orgunit.SessionListDocumentTab;

import com.google.common.collect.TreeMultimap;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import org.tandemframework.tapsupport.component.selection.CommonMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniedu.base.bo.EducationYear.utils.EducationYearModel;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionListDocument;
import ru.tandemservice.unisession.entity.document.SessionSimpleDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author iolshvang
 * @since 12.04.2011
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private final DQLOrderDescriptionRegistry order = new DQLOrderDescriptionRegistry(SessionListDocument.class, "doc");


    @Override
    public void prepare(final Model model)
    {
        model.getOrgUnitHolder().refresh();
        model.setSec(new OrgUnitSecModel(model.getOrgUnit()));
        model.setYearModel(new EducationYearModel());
        model.setPartsModel(EducationCatalogsManager.getYearDistributionPartModel());

        model.setCourseCurrentModel(new CommonMultiSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Course.class, "b").column(property("b"))
                        .where(likeUpper(property("b", Course.title()), value(CoreStringUtils.escapeLike(filter, true))))
                        .order(property("b", Course.title()));

                if (set != null)
                    builder.where(in(property("b", Course.id()), set));

                return new DQLListResultBuilder(builder, 50);
            }
        });

        model.setGroupCurrentModel(new CommonMultiSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                final DQLSelectBuilder s = new DQLSelectBuilder().fromEntity(SessionListDocument.class, "doc")
                        .column(property("doc", SessionListDocument.id()))
                        .where(eq(property("doc", SessionListDocument.orgUnit()), value(model.getOrgUnit())));

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SessionDocumentSlot.class, "a")
		                .column(property("a", SessionDocumentSlot.studentWpeCAction().studentWpe().student().group()))
                        .where(in(property("a", SessionDocumentSlot.document().id()), s.buildQuery()))
                        .where(likeUpper(property("a", SessionDocumentSlot.studentWpeCAction().studentWpe().student().group().title()), value(CoreStringUtils.escapeLike(filter, true))))
                        .order(property("a", SessionDocumentSlot.studentWpeCAction().studentWpe().student().group().title()))
                        .distinct();

                Collection courseCurrentList = model.getSettings().get("courseCurrentList");
                if (courseCurrentList != null && !courseCurrentList.isEmpty())
                    builder.where(in(property("a", SessionDocumentSlot.studentWpeCAction().studentWpe().student().group().course()), courseCurrentList));

                if (set != null)
                    builder.where(in(property("a", SessionDocumentSlot.studentWpeCAction().studentWpe().student().group().id()), set));

                return new DQLListResultBuilder(builder, 50);
            }
        });
    }

    @Override
    public void prepareListDataSource(final Model model)
    {
        final Session session = this.getSession();
        final DynamicListDataSource<SessionListDocument> dataSource = model.getDataSource();

	    final String docAlias = "doc";
	    final String slotAlias = "slot";
        final DQLSelectBuilder cardIdDQL = new DQLSelectBuilder().fromEntity(SessionListDocument.class, docAlias)
				.where(eq(property(docAlias, SessionSimpleDocument.orgUnit()), value(model.getOrgUnit())))
				.joinEntity(docAlias, DQLJoinType.left, SessionDocumentSlot.class, slotAlias, eq(property("doc.id"), property(slotAlias, SessionDocumentSlot.document().id())));

        final IDataSettings settings = model.getSettings();
	    applyFilters(cardIdDQL, docAlias, slotAlias, settings);

        EntityOrder entityOrder = dataSource.getEntityOrder();
        if (null != entityOrder && entityOrder.getColumnName().equals(SessionSimpleDocument.P_NUMBER))
        {
            cardIdDQL.predicate(DQLPredicateType.distinct);
            UniBaseUtils.createPageOrderedByNumberAsString(SessionListDocument.class, dataSource, cardIdDQL, "doc", SessionListDocument.number());
        }
        else
        {
            cardIdDQL.column("doc.id");
            final DQLSelectBuilder cardDQL = new DQLSelectBuilder()
					.fromEntity(SessionListDocument.class, "card").column("card")
					.where(in(property("card", SessionListDocument.id()), cardIdDQL.buildQuery()));

            if (null != entityOrder && entityOrder.getColumnName().equals(SessionSimpleDocument.P_FORMING_DATE))
                cardDQL.order(property("card", SessionListDocument.formingDate()), entityOrder.getDirection());
            else
                this.order.applyOrder(cardDQL, entityOrder);
            UniBaseUtils.createPage(dataSource, cardDQL, session);
        }

        final List<Long> sheetIds = UniBaseUtils.getIdList(model.getDataSource().getEntityList());

	    final Comparator<SessionDocumentSlot> slotByTermComparator = Comparator.comparing(slot -> slot.getStudentWpeCAction().getStudentWpe().getTerm().getIntValue());
	    final TreeMultimap<Long, SessionDocumentSlot> docId2TermSlots = TreeMultimap.create(Comparator.<Long>naturalOrder(), slotByTermComparator);
        final DQLSelectBuilder slotDQL = new DQLSelectBuilder()
				.fromEntity(SessionDocumentSlot.class, "slot")
				.column("slot")
				.fetchPath(DQLJoinType.inner, SessionDocumentSlot.document().fromAlias("slot"), "doc")
				.where(in(property("doc.id"), sheetIds));
	    slotDQL.createStatement(getSession()).<SessionDocumentSlot>list().forEach(slot -> docId2TermSlots.put(slot.getDocument().getId(), slot));

        for (final ViewWrapper<SessionListDocument> wrapper : ViewWrapper.<SessionListDocument>getPatchedList(model.getDataSource()))
        {
            final Long id = wrapper.getEntity().getId();
	        String terms = docId2TermSlots.get(id).stream().map(SessionDocumentSlot::getTermTitle).collect(Collectors.joining(", "));
	        wrapper.setViewProperty(PROP_TERM, terms);
        }
    }

	/**
	 * Применить фильтры к запросу экзам. карточек.
	 * @param cardIdDQL Запрос к экзам карточкам и записям студента в документе.
	 * @param docAlias Алиас экзам. карточки {@link SessionListDocument}.
	 * @param slotAlias Алиас записи студента в документе сессии {@link SessionDocumentSlot}.
	 * @param settings Сеттинги с введенными пользователем фильтрами.
	 */
	private void applyFilters(DQLSelectBuilder cardIdDQL, String docAlias, String slotAlias, IDataSettings settings)
	{
		FilterUtils.applySelectFilter(cardIdDQL, slotAlias, SessionDocumentSlot.studentWpeCAction().studentWpe().year().educationYear().s(), settings.get("year"));
		FilterUtils.applySelectFilter(cardIdDQL, slotAlias, SessionDocumentSlot.studentWpeCAction().studentWpe().part().s(), settings.get("part"));
		final String docNumber = settings.get("docNumber");
		if (StringUtils.isNotBlank(docNumber))
		{
		    cardIdDQL.where(eq(property(docAlias, SessionListDocument.number()), value(docNumber)));
		}
		FilterUtils.applySelectFilter(cardIdDQL, slotAlias, SessionDocumentSlot.actualStudent().course().s(), settings.get("courseCurrentList"));
		FilterUtils.applySelectFilter(cardIdDQL, slotAlias, SessionDocumentSlot.actualStudent().group().s(), settings.get("groupCurrentList"));
		FilterUtils.applySimpleLikeFilter(cardIdDQL, slotAlias, SessionDocumentSlot.actualStudent().person().identityCard().lastName().s(), settings.<String>get("studentLastName"));
		FilterUtils.applyBetweenFilter(cardIdDQL, docAlias, SessionListDocument.formingDate().s(), settings.<Date>get("formedFrom"), settings.<Date>get("formedTo"));
	}

	@Override
    public void doDeleteCard(final Long id)
    {
        final DQLSelectBuilder marks4Card = new DQLSelectBuilder()
				.fromEntity(SessionMark.class, "m")
				.where(eq(property("m", SessionMark.slot().document().id()), value(id)));
        final Number checkResult = marks4Card.createCountStatement(new DQLExecutionContext(this.getSession())).<Number>uniqueResult();
        if (checkResult != null && checkResult.intValue() > 0)
            throw new ApplicationException("Нельзя удалить карточку, так как в ней уже выставлены оценки.");
        this.delete(id);
    }
}
