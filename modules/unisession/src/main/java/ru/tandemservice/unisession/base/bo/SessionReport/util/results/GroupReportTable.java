/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReport.util.results;

import com.google.common.collect.Collections2;
import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.ResultsAdd.ISessionReportResultsDAO;

import java.util.*;

/**
* @author oleyba
* @since 2/13/12
*/
public class GroupReportTable extends ReportTable
{
    @Override
    public void modify(RtfDocument rtf)
    {
        RtfInjectModifier modifier = new RtfInjectModifier();
        modifier.put("groupTitle", "Группа");
        modifier.put("Title", "Группа");
        modifier.put("criterion", "группам");
        modifier.modify(rtf);
        SharedRtfUtil.removeParagraphsWithTagsRecursive(rtf, Arrays.asList("eduLevelType", "eduLevelTitle"), false, false);
    }

    public GroupReportTable(Collection<ISessionReportResultsDAO.ISessionResultsReportStudentData> students)
    {
        Set<Group> groups = new HashSet<>(Collections2.transform(students, ISessionReportResultsDAO.ISessionResultsReportStudentData::getGroup));
        if (groups.contains(null)) {
            rowList.add(new ReportRow("Bне групп", CollectionUtils.select(students, input -> input.getGroup() == null)));
            groups.remove(null);
        }
        List<Group> groupList = new ArrayList<>(groups);
        Collections.sort(groupList, ITitled.TITLED_COMPARATOR);
        for (final Group group : groupList) {
            rowList.add(new ReportRow(group.getTitle(), CollectionUtils.select(students, input -> group.equals(input.getGroup()))));
        }
        rowList.add(totalsRow(students));
    }
}
