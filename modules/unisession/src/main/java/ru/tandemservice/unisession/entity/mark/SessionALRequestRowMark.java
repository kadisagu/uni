package ru.tandemservice.unisession.entity.mark;

import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.unisession.entity.mark.gen.*;

/** @see ru.tandemservice.unisession.entity.mark.gen.SessionALRequestRowMarkGen */
public class SessionALRequestRowMark extends SessionALRequestRowMarkGen
{
    public SessionALRequestRowMark()
    {
    }

    public SessionALRequestRowMark(SessionALRequestRow requestRow, EppFControlActionType controlAction)
    {
        setRequestRow(requestRow);
        setControlAction(controlAction);
    }
}