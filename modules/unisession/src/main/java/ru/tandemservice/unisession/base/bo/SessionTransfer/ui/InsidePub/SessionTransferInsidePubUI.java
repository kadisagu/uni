/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionTransfer.ui.InsidePub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeFCA;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.base.bo.SessionTransfer.SessionTransferManager;
import ru.tandemservice.unisession.base.bo.SessionTransfer.ui.InsideAddEdit.SessionTransferInOpWrapper;
import ru.tandemservice.unisession.base.bo.SessionTransfer.ui.InsideAddEdit.SessionTransferInsideAddEdit;
import ru.tandemservice.unisession.base.bo.SessionTransfer.ui.InsideAddEdit.SessionTransferInsideAddEditUI;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.entity.document.SessionProjectTheme;
import ru.tandemservice.unisession.entity.document.SessionTransferInsideDocument;
import ru.tandemservice.unisession.entity.document.SessionTransferInsideOperation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author oleyba
 * @since 10/19/11
 */
@State({
        @Bind(key = IUIPresenter.PUBLISHER_ID, binding = "document.id", required = true)
})
public class SessionTransferInsidePubUI extends UIPresenter
{
    private SessionTransferInsideDocument document = new SessionTransferInsideDocument();

    private List<SessionTransferInOpWrapper> rowList;
    private SessionTransferInOpWrapper currentRow;

    private boolean useCurrentRating;
	private boolean themeColumnVisible;

    // actions

    @Override
    public void onComponentRefresh()
    {
		ICommonDAO dao = DataAccessServices.dao();
        setDocument(dao.get(SessionTransferInsideDocument.class, getDocument().getId()));
        setRowList(new ArrayList<>());
		EppStudentWpeCAction.Path<EppStudentWpeCAction> cActionPath = SessionTransferInsideOperation.targetMark().slot().studentWpeCAction();
        List<SessionTransferInsideOperation> operations = dao.getList(SessionTransferInsideOperation.class,
																	  SessionTransferInsideOperation.targetMark().slot().document().s(), getDocument(),
																	  cActionPath.studentWpe().year().educationYear().intValue().s(),
																	  cActionPath.studentWpe().term().intValue().s(),
																	  cActionPath.studentWpe().registryElementPart().registryElement().title().s(),
																	  cActionPath.type().priority().s()
		);

		List<EppGroupTypeFCA> groupTypesRequiresTheme = dao.getList(EppFControlActionType.class, EppFControlActionType.group().themeRequired(), true)
				.stream().map(EppFControlActionType::getEppGroupType).collect(Collectors.toList());
		themeColumnVisible = operations.stream().anyMatch(op -> groupTypesRequiresTheme.contains(op.getTargetMark().getSlot().getStudentWpeCAction().getType()));

        Map<SessionTransferInsideOperation, SessionProjectTheme> operation2theme = themeColumnVisible ?
				SessionTransferManager.instance().dao().getThemesByTransferOperations(operations) :
				new HashMap<>();

        for (SessionTransferInsideOperation operation : operations)
            getRowList().add(new SessionTransferInOpWrapper(operation, operation2theme.get(operation)));

        useCurrentRating = ISessionBrsDao.instance.get().getSettings().isUseCurrentRatingForTransferDocument();
    }

    public void onClickEdit()
    {
        _uiActivation.asRegion(SessionTransferInsideAddEdit.class)
                .parameter(SessionTransferInsideAddEditUI.DOCUMENT_ID, getDocument().getId())
                .parameter(SessionTransferInsideAddEditUI.STUDENT_ID, null)
                .parameter(SessionTransferInsideAddEditUI.ORG_UNIT_ID, null)
                .activate();
    }

    public void onClickDelete()
    {
        IUniBaseDao.instance.get().delete(getDocument());
        deactivate();
    }

    // presenter

    public Student getStudent()
    {
        return getDocument().getTargetStudent();
    }

    public String getCurrentRowStyle()
    {
        return getRowList().indexOf(getCurrentRow()) % 2 == 0 ? "list-row list-row-odd" : "list-row list-row-even";
    }

    // getters and setters

    public boolean isUseCurrentRating()
    {
        return useCurrentRating;
    }

	public boolean isThemeColumnVisible()
	{
		return themeColumnVisible;
	}

    public Integer getColumnCount()
    {
		int count = 5;
		if (isUseCurrentRating())
			++count;
		if (isThemeColumnVisible())
			++count;
        return count;
    }

    public SessionTransferInsideDocument getDocument()
    {
        return document;
    }

    public void setDocument(SessionTransferInsideDocument document)
    {
        this.document = document;
    }

    public List<SessionTransferInOpWrapper> getRowList()
    {
        return rowList;
    }

    public void setRowList(List<SessionTransferInOpWrapper> rowList)
    {
        this.rowList = rowList;
    }

    public SessionTransferInOpWrapper getCurrentRow()
    {
        return currentRow;
    }

    public void setCurrentRow(SessionTransferInOpWrapper currentRow)
    {
        this.currentRow = currentRow;
    }
}
