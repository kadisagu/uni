package ru.tandemservice.unisession.component.catalog.sessionMarkStateCatalogItem.SessionMarkStateCatalogItemAddEdit;

import java.util.List;

import org.tandemframework.hibsupport.DataAccessServices;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.unisession.entity.catalog.SessionMarkStateCatalogItem;

/**
 * Created by IntelliJ IDEA.
 * User: Tandem
 * Date: 01.03.11
 * Time: 13:17
 * To change this template use File | Settings | File Templates.
 */
public class DAO extends DefaultCatalogAddEditDAO<SessionMarkStateCatalogItem, Model>
{
    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);
        if (model.isAddForm())
        {
            final List<SessionMarkStateCatalogItem> orderedMarks = DataAccessServices.dao().getList(SessionMarkStateCatalogItem.class, SessionMarkStateCatalogItem.P_PRIORITY);
            final int maxPriorityValue = orderedMarks.get(orderedMarks.size() - 1).getPriority();
            model.getCatalogItem().setPriority(maxPriorityValue + 1);
        }
    }
}
