package ru.tandemservice.unisession.base.bo.SessionRetakeDoc.ui.EditThemes;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.formatter.NewLineFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unisession.dao.document.ISessionDocumentBaseDAO;
import ru.tandemservice.unisession.dao.retakeDoc.ISessionRetakeDocDAO;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionProjectTheme;
import ru.tandemservice.unisession.entity.document.SessionRetakeDocument;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author avedernikov
 * @since 14.07.2016
 */
@State({
		@Bind(key = UIPresenter.PUBLISHER_ID, binding = "retakeDoc.id", required = true)
})
public class SessionRetakeDocEditThemesUI extends UIPresenter
{
	private SessionRetakeDocument retakeDoc = new SessionRetakeDocument();

	private Map<SessionDocumentSlot, String> slot2Theme = new HashMap<>();

	@Override
	public void onComponentRefresh()
	{
		retakeDoc = DataAccessServices.dao().getNotNull(retakeDoc.getId());
		DataAccessServices.dao().getList(SessionProjectTheme.class, SessionProjectTheme.slot().document(), retakeDoc).forEach(theme -> slot2Theme.put(theme.getSlot(), theme.getTheme()));
	}

	@Override
	public void onBeforeDataSourceFetch(IUIDataSource dataSource)
	{
		switch (dataSource.getName())
		{
			case SessionRetakeDocEditThemes.THEMES_DS:
				dataSource.put(SessionRetakeDocEditThemes.PARAM_RETAKE_DOC, getRetakeDoc());
				break;
		}
	}

	public SessionRetakeDocument getRetakeDoc()
	{
		return retakeDoc;
	}

	public String getRegistryElementTitle()
	{
		return ISessionRetakeDocDAO.instance.get().regElementTitleWithFCATypes(retakeDoc);
	}

	public String getTutors()
	{
		Set<String> pps = DataAccessServices.dao().getList(SessionComissionPps.class, SessionComissionPps.commission(), retakeDoc.getCommission()).stream()
				.map(comPps -> comPps.getPps().getTitle())
				.collect(Collectors.toSet());
		return String.join(", ", pps);
	}

	public String getStudentGroups()
	{
		Set<Group> groups = DataAccessServices.dao().getList(SessionDocumentSlot.class, SessionDocumentSlot.document(), retakeDoc).stream()
				.map(slot -> slot.getActualStudent().getGroup())
				.collect(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(Group::getTitle).thenComparing(Group::getId))));
		String groupStr = groups.stream().map(Group::getTitle).collect(Collectors.joining("\n"));
		return NewLineFormatter.SIMPLE.format(groupStr);
	}

	private SessionDocumentSlot getCurrentSlot()
	{
		return getConfig().getDataSource(SessionRetakeDocEditThemes.THEMES_DS).getCurrent();
	}

	public String getCurrentSlotTheme()
	{
		return slot2Theme.get(getCurrentSlot());
	}

	public void setCurrentSlotTheme(String theme)
	{
		slot2Theme.put(getCurrentSlot(), theme);
	}

	public void onClickApply()
	{
		ISessionDocumentBaseDAO dao = ISessionDocumentBaseDAO.instance.get();
		slot2Theme.forEach((slot, theme) -> dao.updateProjectTheme(slot, theme, null));
		deactivate();
	}
}
