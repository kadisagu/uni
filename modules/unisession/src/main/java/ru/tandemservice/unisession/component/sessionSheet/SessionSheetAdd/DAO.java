// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisession.component.sessionSheet.SessionSheetAdd;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.INumberQueueDAO;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.base.bo.PpsEntry.util.PpsEntrySelectBlockData;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionGroup;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.base.bo.SessionMark.SessionMarkManager;
import ru.tandemservice.unisession.base.bo.SessionMark.logic.ISessionMarkDAO;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.dao.document.ISessionDocumentBaseDAO;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionsSimpleDocumentReason;
import ru.tandemservice.unisession.entity.comission.SessionComission;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionProjectTheme;
import ru.tandemservice.unisession.entity.document.SessionSheetDocument;
import ru.tandemservice.unisession.entity.document.SessionStudentGradeBookDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.util.selectModel.SessionTermModel;

import java.util.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 2/10/11
 */
public class DAO<M extends Model> extends UniBaseDao implements IDAO<M>
{
    @Override
    public void prepare(final M model)
    {
        model.getInitOrgUnit().refresh();
        model.getInitStudent().refresh();
        model.getInitWpcaSlot().refresh();
        List<PpsEntry> ppsList = CommonBaseUtil.getPropertiesList(this.getList(SessionComissionPps.class, SessionComissionPps.commission().s(), model.getSlot().getCommission()), SessionComissionPps.pps().s());
        model.setPpsData(new PpsEntrySelectBlockData((Collection<Long>) null).setMultiSelect(true).setInitiallySelectedPps(ppsList));
        if (model.getInitWpcaSlot().getId() != null) {
            model.setEppSlot(model.getInitWpcaSlot().getValue());
            model.setTerm(new SessionTermModel.TermWrapper(model.getEppSlot()));
            model.setStudent(model.getEppSlot().getStudentWpe().getStudent());
            model.setOrgUnit(model.getStudent().getEducationOrgUnit().getGroupOrgUnit());
            if (null == model.getOrgUnit()) {
                throw new ApplicationException("Добавление экзаменационного листа для студента невозможно, так как для его направления подготовки не указан деканат.");
            }
            onChangeControlAction(model);
        }
        else if (null != model.getInitStudent().getId())
        {
            model.setStudent(model.getInitStudent().getValue());
            model.setOrgUnit(model.getStudent().getEducationOrgUnit().getGroupOrgUnit());
            if (null == model.getOrgUnit()) {
                throw new ApplicationException("Добавление экзаменационного листа для студента невозможно, так как для его направления подготовки не указан деканат.");
            }
        }
        else if (null != model.getInitOrgUnit().getId()) {
            model.setOrgUnit(model.getInitOrgUnit().getValue());
        }

        if (null != model.getOrgUnitId() && !model.getOrgUnit().getId().equals(model.getOrgUnitId())) {
            throw new ApplicationException("Добавление экзаменационного листа на данном подразделении для выбранного студента невозможно, так как это подразделение не является деканатом по его направлению подготовки.");
        }

        model.setStudentModel(new DQLFullCheckSelectModel(Student.titleWithFio().s())
        {
            @Override
            protected DQLSelectBuilder query(final String alias, final String filter)
            {
                final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(Student.class, alias);
                if (null != model.getInitStudent().getId()) {
                    return dql.where(eq(property(Student.id().fromAlias(alias)), value(model.getInitStudent().getId())));
                }
                dql.where(eq(property(Student.educationOrgUnit().groupOrgUnit().fromAlias(alias)), value(model.getOrgUnit())));
                dql.where(eq(property(Student.status().active().fromAlias(alias)), value(Boolean.TRUE)));
                dql.where(eq(property(Student.archival().fromAlias(alias)), value(Boolean.FALSE)));
                dql.joinPath(DQLJoinType.inner, Student.person().fromAlias(alias), "person");
                dql.joinPath(DQLJoinType.inner, Person.identityCard().fromAlias("person"), "idc");
                if (StringUtils.isNotBlank(filter)) {
                    dql.where(this.like(IdentityCard.fullFio().fromAlias("idc"), filter));
                }
                dql.order(property(IdentityCard.lastName().fromAlias("idc")));
                dql.order(property(IdentityCard.firstName().fromAlias("idc")));
                dql.order(property(IdentityCard.middleName().fromAlias("idc")));
                return dql;
            }
        });

        model.setReasonModel(new LazySimpleSelectModel<>(SessionsSimpleDocumentReason.class));
        model.getSheet().setReason(this.get(SessionsSimpleDocumentReason.class, SessionsSimpleDocumentReason.code().s(), "1"));

        model.setTermModel(new SessionTermModel()
        {
            @Override
            public Map<EducationYear, Set<YearDistributionPart>> getValueMap()
            {
                Student student = model.getStudent();
                if (student == null) return new LinkedHashMap<>();

                DQLSelectBuilder wpeCaBuilder = getStudentWpeCActionsBuilder(student, model.getSheet().getReason().isFormOnDebts(), "a")
                        .column(property("a", EppStudentWpeCAction.studentWpe().year().educationYear().id()))
                        .column(property("a", EppStudentWpeCAction.studentWpe().part().id()))
                        .distinct();

                return getYear2YearParts(wpeCaBuilder);
            }
        });

        model.setControlActionModel(new DQLFullCheckSelectModel(EppStudentWpeCAction.P_REGISTRY_ELEMENT_TITLE)
        {
            @Override
            protected DQLSelectBuilder query(final String alias, final String filter)
            {
                SessionTermModel.TermWrapper term = model.getTerm();

                if (model.getInitWpcaSlot().getId() != null)
                    return new DQLSelectBuilder().fromEntity(EppStudentWpeCAction.class, alias).where(eq(property(alias, "id"), value(model.getInitWpcaSlot().getId())));
                if (model.getStudent() == null || term == null || term.getPart() == null)
                    return new DQLSelectBuilder().fromEntity(EppStudentWpeCAction.class, alias).where(isNull(property(alias, "id")));

                DQLSelectBuilder wpeCaBuilder = SessionTermModel.getStudentWpeCActionsBuilder(model.getStudent(), model.getSheet().getReason().isFormOnDebts(), alias);

                String wpeAlias = "wpe";
                wpeCaBuilder
                        .joinPath(DQLJoinType.inner, EppStudentWpeCAction.studentWpe().fromAlias(alias), wpeAlias)
                        .where(eq(property(wpeAlias, EppStudentWorkPlanElement.student()), value(model.getStudent())))
                        .where(eq(property(wpeAlias, EppStudentWorkPlanElement.year().educationYear()), value(term.getYear())))
                        .where(eq(property(wpeAlias, EppStudentWorkPlanElement.part()), value(term.getPart())))

                        .joinPath(DQLJoinType.inner, EppStudentWorkPlanElement.registryElementPart().registryElement().fromAlias(wpeAlias), "element")
                        .joinEntity(alias, DQLJoinType.inner, EppFControlActionType.class, "type",
                                    eq(property(alias, EppStudentWpeCAction.type()), property("type", EppFControlActionType.eppGroupType())))

                        .order(property("element", EppRegistryElement.title()))
                        .order(property("type", EppFControlActionType.title()));

                FilterUtils.applySimpleLikeFilter(wpeCaBuilder, "element", EppRegistryElement.title().s(), filter);

                return wpeCaBuilder;
            }
        });

        if (null != model.getEppSlot())
            model.setMarkModel(SessionMarkManager.instance().regularMarkDao().prepareMarkModel(model.getOrgUnit(), model.getEppSlot(), null, true));

        if (model.getSheet().getIssueDate() == null) {
            model.getSheet().setIssueDate(new Date());
        }
    }

    @Override
    public void validate(final M model, final ErrorCollector errors)
    {
        if (model.getSheet().getIssueDate() != null && model.getSheet().getDeadlineDate() != null && model.getSheet().getIssueDate().after(model.getSheet().getDeadlineDate())) {
            errors.add("Дата выдачи не может быть позже срока действия.", "issueDate", "deadlineDate");
        }

        if (!model.isCanMark())
            return;

        if (model.getPerformDate() != null && model.getSheet().getDeadlineDate() != null && model.getPerformDate().after(model.getSheet().getDeadlineDate())) {
            errors.add("Дата сдачи не может быть позже срока действия.", "performDate", "deadlineDate");
        }

        if (model.getMark() != null && model.getPerformDate() == null) {
            errors.add("Чтобы выставить оценку, заполните дату сдачи.", "performDate");
        }

        if (model.getMark() != null && CollectionUtils.isEmpty(model.getPpsList())) {
            errors.add("Чтобы выставить оценку, укажите преподавателя.", "pps");
        }
    }

    @Override
    public void update(final M model)
    {
        final SessionSheetDocument sheet = model.getSheet();
        if (null != sheet.getId()) return;

        sheet.setFormingDate(new Date());
        sheet.setOrgUnit(model.getOrgUnit());
        sheet.setNumber(sheet.getNumberPrefix() + INumberQueueDAO.instance.get().getNextNumber(sheet));
        this.save(sheet);

        final SessionComission comission = new SessionComission();
        this.save(comission);

        for (final PpsEntry pps : model.getPpsList())
        {
            final SessionComissionPps rel = new SessionComissionPps();
            rel.setPps(pps);
            rel.setCommission(comission);
            this.save(rel);
        }

        final SessionDocumentSlot slot = model.getSlot();
        slot.setDocument(sheet);
        slot.setStudentWpeCAction(model.getEppSlot());
        slot.setCommission(comission);
        this.save(slot);

        if (model.isCanMark() && null != model.getMark())
		{
            SessionMarkManager.instance().regularMarkDao().saveOrUpdateMark(slot, prepareMarkDataForUpdate(model));
            saveAdditionalMarkData(model);
        }

		ISessionDocumentBaseDAO.instance.get().updateProjectTheme(slot, model.getTheme(), null);
    }

    @Override
    public void onChangeControlAction(final M model)
    {
        // обновляем список преподавателей
        this.refreshPpsListByEppSlot(model);

        model.getSlot().setTryNumber(slotTryNumber(model.getEppSlot()));

		ISessionDocumentBaseDAO baseDAO = ISessionDocumentBaseDAO.instance.get();
        if (null != model.getEppSlot())
		{
            EppGradeScale scale = baseDAO.getGradeScale(model.getEppSlot());
            EppStudentWorkPlanElement studentWpe = model.getEppSlot().getStudentWpe();
            OrgUnit groupOu = studentWpe.getStudent().getEducationOrgUnit().getGroupOrgUnit();
            EppYearPart yearPart = studentWpe.getEppYearPart();
            EppRegistryElementPart discipline = studentWpe.getRegistryElementPart();
            EppFControlActionGroup caGroup = model.getEppSlot().getActionType().getGroup();
            ISessionBrsDao.ISessionRatingSettingsKey ratingSettingsKey = ISessionBrsDao.instance.get().key(scale, caGroup, discipline, yearPart, groupOu);

            // настройки БРС - не даем ставить оценку, если используются баллы или тек. рейтинг
            // todo исправить на поддержку выставления в баллах, текущий рейтинг не поддерживать
            ISessionBrsDao.ISessionRatingSettings ratingSettings = ISessionBrsDao.instance.get().getRatingSettings(ratingSettingsKey);
            model.setCanMark(ratingSettings == null || (!ratingSettings.useCurrentRating() && !ratingSettings.usePoints()));

            // обновляем список оценок
            if (model.isCanMark())
                model.setMarkModel(SessionMarkManager.instance().regularMarkDao().prepareMarkModel(model.getOrgUnit(), model.getEppSlot(), null, true));

			model.setThemeRequired(model.getEppSlot().getActionType().isThemeRequired());
			if (model.isThemeRequired())
			{
				SessionProjectTheme slotTheme = getByNaturalId(new SessionProjectTheme.NaturalId(model.getSlot()));
				String theme = slotTheme != null ? slotTheme.getTheme() : baseDAO.themeByLastWpeThemes(model.getEppSlot());
				model.setTheme(theme);
			}
        }
    }

    /**
     * Для каждой итоговой оценки считаем общее число полученных ранее оценок (не отметок) и добавляем 1.
     */
    protected Integer slotTryNumber(EppStudentWpeCAction studentWpCActionSlot)
    {
        Long result = new DQLSelectBuilder().fromEntity(SessionDocumentSlot.class, "d").column(property(SessionDocumentSlot.id().fromAlias("d")))
                .where(eq(property(SessionDocumentSlot.studentWpeCAction().fromAlias("d")), value(studentWpCActionSlot)))
                .where(exists(
                        new DQLSelectBuilder().fromEntity(SessionMark.class, "m").column(property(SessionMark.id().fromAlias("m")))
                                .fetchPath(DQLJoinType.inner, SessionMark.cachedMarkValue().fromAlias("m"), "mv")
                                .fetchPath(DQLJoinType.inner, SessionMark.slot().document().fromAlias("m"), "md")
                                .where(eq(property(SessionMark.slot().fromAlias("m")), property("d")))
                                .where(instanceOf("mv", SessionMarkGradeValueCatalogItem.class))
                                .where(not(instanceOf("md", SessionStudentGradeBookDocument.class)))
                                .buildQuery()))
                .createCountStatement(new DQLExecutionContext(getSession())).<Long>uniqueResult();

        result++;
        return result.intValue();
    }

    private void refreshPpsListByEppSlot(final M model)
    {
        if (null == model.getEppSlot() || null == model.getEppSlot().getId()) {
            return;
        }

        model.setPpsData(new PpsEntrySelectBlockData(model.getEppSlot().getTutorOu().getId()).setMultiSelect(true));
    }

    protected ISessionMarkDAO.MarkData prepareMarkDataForUpdate(final M model)
    {
        return new ISessionMarkDAO.MarkData()
        {
            @Override public Date getPerformDate() { return model.getPerformDate(); }
            @Override public Double getPoints() { return null; }
            @Override public SessionMarkCatalogItem getMarkValue() { return model.getMark(); }
            @Override public String getComment() { return null; }
        };
    }

    protected void saveAdditionalMarkData(Model model)
    {
        // для переопределения в проектах
    }

    @Override
    public void prepareListDataSource(M m)
    {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
