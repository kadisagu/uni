/* $Id$ */
package ru.tandemservice.unisession.component.sessionRetakeDoc.SessionRetakeDocPubInline;

import ru.tandemservice.uni.dao.IPrepareable;

/**
 * @author oleyba
 * @since 6/15/11
 */
public interface IDAO extends IPrepareable<Model>
{
    String V_MARK = "mark";
    String V_MARK_COUNT = "markCount";
    String PROP_PROJECT_THEME = "projectTheme";

    void prepareListDataSource(Model model);
}
