/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionTransfer.ui.OutsideAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.base.bo.SessionTransfer.logic.StudentByGroupOrgUnitComboDSHandler;

import static org.tandemframework.hibsupport.dql.DQLExpressions.isNull;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author oleyba
 * @since 10/19/11
 */
@Configuration
public class SessionTransferOutsideAddEdit extends BusinessComponentManager
{
    // todo уникальность в пределах чего нужна?

    public static final String DS_EPP_SLOT = "eppSlotDS";
    public static final String STUDENT_DS = "studentDS";

    public static final String KEY_STUDENT = "student";
    public static final String KEY_EDU_YEAR = "eduYear";
    public static final String KEY_YEAR_PART = "yearPart";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(EducationCatalogsManager.instance().eduYearDSConfig())
                .addDataSource(eppSlotDSConfig())
                .addDataSource(selectDS(STUDENT_DS, studentComboDSHandler()).addColumn(Student.titleWithFio().s()))
                .create();
    }

    @Bean
    public UIDataSourceConfig eppSlotDSConfig()
    {
        return SelectDSConfig.with(DS_EPP_SLOT, this.getName())
                .dataSourceClass(SelectDataSource.class)
                .addColumn("title", EppStudentWpeCAction.registryElementTitle().s())
                .handler(this.eppSlotDSHandler())
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> eppSlotDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), EppStudentWpeCAction.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(isNull(property(EppStudentWpeCAction.removalDate().fromAlias(alias))));
                if (!context.hasProperty(KEY_STUDENT) || !context.hasProperty(KEY_EDU_YEAR) || !context.hasProperty(KEY_YEAR_PART))
                    dql.where(isNull(alias + ".id"));
            }
        }
                .where(EppStudentWpeCAction.studentWpe().student(), KEY_STUDENT)
                .where(EppStudentWpeCAction.studentWpe().year().educationYear(), KEY_EDU_YEAR)
                .where(EppStudentWpeCAction.studentWpe().part(), KEY_YEAR_PART)
                .order(EppStudentWpeCAction.studentWpe().registryElementPart().registryElement().title());
    }

    @Bean
    public IDefaultComboDataSourceHandler studentComboDSHandler()
    {
        return new StudentByGroupOrgUnitComboDSHandler(getName());
    }
}
