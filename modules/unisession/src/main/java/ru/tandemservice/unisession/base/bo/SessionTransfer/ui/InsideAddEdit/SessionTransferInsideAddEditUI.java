/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionTransfer.ui.InsideAddEdit;

import com.google.common.collect.ImmutableList;
import org.apache.commons.collections.CollectionUtils;
import org.apache.tapestry.form.validator.BaseValidator;
import org.apache.tapestry.form.validator.Max;
import org.apache.tapestry.form.validator.Min;
import org.apache.tapestry.form.validator.Required;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.base.bo.SessionMark.SessionMarkManager;
import ru.tandemservice.unisession.base.bo.SessionTransfer.SessionTransferManager;
import ru.tandemservice.unisession.base.bo.SessionTransfer.logic.StudentByGroupOrgUnitComboDSHandler;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.dao.document.ISessionDocumentBaseDAO;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.document.SessionProjectTheme;
import ru.tandemservice.unisession.entity.document.SessionTransferInsideDocument;
import ru.tandemservice.unisession.entity.document.SessionTransferInsideOperation;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.mark.SessionSlotRegularMark;
import ru.tandemservice.unisession.util.selectModel.SessionTermModel;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba, vip_delete
 * @since 10/19/11
 */
@Input({
        @Bind(key = SessionTransferInsideAddEditUI.STUDENT_ID, binding = "studentId"),
        @Bind(key = SessionTransferInsideAddEditUI.DOCUMENT_ID, binding = "documentId"),
        @Bind(key = SessionTransferInsideAddEditUI.ORG_UNIT_ID, binding = "orgUnitId"),
        @Bind(key = SessionTransferInsideAddEditUI.AUTO_FORM, binding = "autoForm")
})
public class SessionTransferInsideAddEditUI extends UIPresenter
{
    public static final String STUDENT_ID = "studentId";
    public static final String DOCUMENT_ID = "documentId";
    public static final String ORG_UNIT_ID = PublisherActivator.PUBLISHER_ID_KEY;
    public static final String AUTO_FORM = "autoForm";

    private Long studentId;
    private Long documentId;
    private Long orgUnitId;
    private boolean editForm;

    private Student student;
    private SessionTransferInsideDocument document;
    private OrgUnit orgUnit;

    private List<SessionTransferInOpWrapper> rowList;
    private Date markDate;

    private SessionTransferInOpWrapper currentRow;
    private SessionTransferInOpWrapper editedRow;
    private Integer editedRowIndex;
    boolean rowAdded;
    boolean useCurrentRating;

    private ISelectModel sourceTermModel;
    private ISelectModel sourceEppSlotModel;
    private ISelectModel sourceMarkModel;
    
    private ISelectModel targetTermModel;
    private ISelectModel targetMarkModel;

    private boolean autoForm = false;

    @Override
    public void onComponentRefresh()
    {
        ICommonDAO dao = DataAccessServices.dao();

        setStudent(getStudentId() == null ? null : dao.<Student>getNotNull(getStudentId()));
        setDocument(getDocumentId() == null ? null : dao.<SessionTransferInsideDocument>getNotNull(getDocumentId()));
        setOrgUnit(getOrgUnitId() == null ? null : dao.<OrgUnit>getNotNull(getOrgUnitId()));

        boolean justStarted = getRowList() == null;
        if (justStarted)
            setRowList(new ArrayList<>());

        setEditedRow(null);
        setEditedRowIndex(null);
        setRowAdded(false);

        if (isAddForm())
        {
            // форма добавления внешнего перезачтения
            setEditForm(false);
            setDocument(new SessionTransferInsideDocument());
            getDocument().setFormingDate(new Date());

            if (getStudentId() != null)
            {
                getDocument().setTargetStudent(getStudent());
                getDocument().setOrgUnit(getStudent().getEducationOrgUnit().getGroupOrgUnit());
            }

            if (isAutoForm())
            {
                if (getStudentId() == null)
                    throw new IllegalStateException(); // автоматическое формирование внутреннго перезачтение доступно только в контексте студента

                setRowList(SessionTransferManager.instance().dao().getAutoFormInnerTransfers(getStudentId()));
            }

        } else
        {
            // форма редактирования внешнего перезачтения
            setEditForm(true);
            setStudent(getDocument().getTargetStudent());
            if (justStarted)
			{
                List<SessionTransferInsideOperation> operations = dao.getList(SessionTransferInsideOperation.class,
                        SessionTransferInsideOperation.targetMark().slot().document(), getDocument(),
                        SessionTransferInsideOperation.targetMark().slot().studentWpeCAction().studentWpe().year().educationYear().intValue().s(),
                        SessionTransferInsideOperation.targetMark().slot().studentWpeCAction().studentWpe().term().intValue().s(),
                        SessionTransferInsideOperation.targetMark().slot().studentWpeCAction().studentWpe().registryElementPart().registryElement().title().s(),
                        SessionTransferInsideOperation.targetMark().slot().studentWpeCAction().type().priority().s()
                );

				Map<SessionTransferInsideOperation, SessionProjectTheme> operation2theme = SessionTransferManager.instance().dao().getThemesByTransferOperations(operations);

                for (SessionTransferInsideOperation operation : operations)
				{
                    getRowList().add(new SessionTransferInOpWrapper(operation, operation2theme.get(operation)));
                    setMarkDate(operation.getTargetMark().getPerformDate());
                }
            }
        }

        if (getStudent() != null)
            prepareSelectModelsForStudent();

        useCurrentRating = ISessionBrsDao.instance.get().getSettings().isUseCurrentRatingForTransferDocument();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (SessionTransferInsideAddEdit.DS_TARGET_EPP_SLOT.equals(dataSource.getName())) {
            dataSource.put(SessionTransferInsideAddEdit.KEY_STUDENT, getStudent());
            if (null != getEditedRow() && null != getEditedRow().getTerm())
            {
                dataSource.put(SessionTransferInsideAddEdit.KEY_TARGET_EDU_YEAR, getEditedRow().getTerm().getYear());
                dataSource.put(SessionTransferInsideAddEdit.KEY_TARGET_YEAR_PART, getEditedRow().getTerm().getPart());
            }
        }
        else if (SessionTransferInsideAddEdit.DS_STUDENT.equals(dataSource.getName())) {
            dataSource.put(StudentByGroupOrgUnitComboDSHandler.ORG_UNIT, getOrgUnit());
        }
    }

    // Listeners

    public void onSelectStudent()
    {
        // выбирать студента можно только на форме добавления и при не указанном студенте
        if (!isNeedSelectStudent())
            throw new IllegalStateException("Student changing is not allowed.");

        getDocument().setTargetStudent(getStudent());
        getDocument().setOrgUnit(getStudent().getEducationOrgUnit().getGroupOrgUnit());
        prepareSelectModelsForStudent();
        setRowList(new ArrayList<>());
        setEditedRow(null);
        setEditedRowIndex(null);
    }

    public void onClickApply()
    {
        if (getStudent().getEducationOrgUnit().getGroupOrgUnit() == null) {
            throw new ApplicationException("Нельзя добавить документ о перезачтении студенту, т.к. для его направления подготовки не задан ответственный деканат, который необходим для сохранения документа о перезачтении. Укажите деканат в настройке «Диспетчерские и деканаты».");
        }
        SessionTransferManager.instance().dao().saveOrUpdateInsideTransfer(getDocument(), getMarkDate(), getRowList());
        deactivate();
    }

    public void onClickAddRow()
    {
        if (getEditedRow() != null) return;
        final SessionTransferInOpWrapper newRow = new SessionTransferInOpWrapper();
        setEditedRow(newRow);

        getRowList().add(new SessionTransferInOpWrapper());
        setEditedRowIndex(getRowList().size() - 1);

        setRowAdded(true);
    }

    public void onClickSaveRow()
    {
        if (getEditedRowIndex() != null && getRowList().size() > getEditedRowIndex() && getEditedRowIndex() >= 0)
            getRowList().set(getEditedRowIndex(), getEditedRow());
        setEditedRow(null);
        setRowAdded(false);
    }

    public void onClickCancelEdit()
    {
        if (isRowAdded() && !getRowList().isEmpty())
            getRowList().remove(getRowList().size() - 1);
        setEditedRowIndex(null);
        setEditedRow(null);
        setRowAdded(false);
    }

    public void onClickEditRow()
    {
        if (getEditedRow() != null) return;
        SessionTransferInOpWrapper baseRow = findRow();
        setEditedRow(new SessionTransferInOpWrapper(baseRow));
        setEditedRowIndex(getRowList().indexOf(baseRow));
        prepareTargetMarkModel();
        setRowAdded(false);
    }

    public void onClickDeleteRow()
    {
        SessionTransferInOpWrapper row = findRow();
        if (null != row)
            getRowList().remove(row);
    }

	// При выборе текущего мероприятия при необходимости устанавливаем тему по последнему документу сессии с выбранным текущим мероприятием.
	// Тема берется из этого документа, если ФИК мероприятия требует тему, но в текущей записи студента по перезачтению темы еще нет.
	public void onWpeCActionSelected()
	{
		prepareTargetMarkModel();
		if(!getEditedRow().isThemeExistForDocSlot() && isEditedRowThemeVisible())
			getEditedRow().setTheme(ISessionDocumentBaseDAO.instance.get().themeByLastWpeThemes(getEditedRow().getEppSlot()));
	}

    public void prepareTargetMarkModel()
    {
        setTargetMarkModel(null == getEditedRow() || null == getEditedRow().getEppSlot() ?
                new LazySimpleSelectModel<>(ImmutableList.of()) :
                SessionMarkManager.instance().regularMarkDao().prepareSimpleMarkModel(getEditedRow().getEppSlot(), null, false)
        );
    }

    public void onChangeSourceMark()
    {
        if (getEditedRow() == null)
            return;

        if (getEditedRow().getSourceMark() != null && getEditedRow().getSourceMark().getPoints() != null)
            getEditedRow().setRating(getEditedRow().getSourceMark().getPoints());
    }

    // presenter

    public boolean isNeedSelectStudent()
    {
        // если на форме добавления не указан студент, то его нужно выбрать
        return getDocumentId() == null && getStudentId() == null;
    }

    public boolean isHasStudent()
    {
        return getStudent() != null;
    }

    public boolean isEditMode()
    {
        return getEditedRow() != null;
    }

    public boolean isListEmpty()
    {
        return CollectionUtils.isEmpty(getRowList());
    }

    public boolean isCurrentRowInEditMode()
    {
        return getEditedRow() != null && getEditedRowIndex().equals(getRowList().indexOf(getCurrentRow()));
    }

    public boolean isRatingDisabled()
    {
        return !(getEditedRow() != null && getEditedRow().getMark() instanceof SessionMarkGradeValueCatalogItem);
    }

	public boolean isEditedRowThemeVisible()
	{
		return getEditedRow().getEppSlot() != null && getEditedRow().getEppSlot().getActionType().isThemeRequired();
	}

    // utils

    private boolean isAddForm() {
        return getDocumentId() == null;
    }

    private SessionTransferInOpWrapper findRow()
    {
        Integer id = getListenerParameter();
        SessionTransferInOpWrapper row = null;
        for (SessionTransferInOpWrapper r : getRowList())
            if (r.getId() == id)
                row = r;
        return row;
    }
    
    // data access
    
    private void prepareSelectModelsForStudent() {
        setSourceTermModel(new SessionTermModel() {
            @Override
            public Map<EducationYear, Set<YearDistributionPart>> getValueMap() {
                final DQLSelectBuilder dql = new DQLSelectBuilder()
                        .fromEntity(SessionSlotRegularMark.class, "mark")
                        .predicate(DQLPredicateType.distinct)
                        .joinPath(DQLJoinType.inner, SessionSlotRegularMark.slot().studentWpeCAction().studentWpe().fromAlias("mark"), "slot")
                        .column(property("slot", EppStudentWorkPlanElement.year().educationYear().id()))
                        .column(property("slot", EppStudentWorkPlanElement.part().id()))
                        .where(eq(property("slot", EppStudentWorkPlanElement.student()), value(getStudent())))
                        .where(isNotNull(property(SessionSlotRegularMark.slot().studentWpeCAction().removalDate().fromAlias("mark"))));

                return IUniBaseDao.instance.get().getCalculatedValue(session -> {
                    final Map<EducationYear, Set<YearDistributionPart>> result = new TreeMap<>(Comparator.comparingInt(EducationYear::getIntValue));
                    for (final Object[] row : dql.createStatement(session).<Object[]>list())
                    {
                        final EducationYear year = (EducationYear) session.get(EducationYear.class, (Long) row[0]);
                        final YearDistributionPart yearPart = (YearDistributionPart) session.get(YearDistributionPart.class, (Long) row[1]);
                        SafeMap.safeGet(result, year, key -> new TreeSet<>(Comparator.comparingInt(YearDistributionPart::getNumber))).add(yearPart);
                    }
                    return result;
                });
            }
        });

        setSourceMarkModel(new DQLFullCheckSelectModel() {
            @Override
            protected DQLSelectBuilder query(String alias, String filter) {
                if (getEditedRow() == null || getEditedRow().getSourceEppSlot() == null) {
                    return new DQLSelectBuilder().fromEntity(SessionSlotRegularMark.class, alias).where(isNull(property(alias + ".id")));
                }

                return new DQLSelectBuilder()
                        .fromEntity(SessionSlotRegularMark.class, alias)
                        .where(eq(property(SessionSlotRegularMark.slot().studentWpeCAction().fromAlias(alias)), value(getEditedRow().getSourceEppSlot())))
                        .order(property(SessionSlotRegularMark.slot().document().number().fromAlias(alias)));
            }

            @Override
            public String getLabelFor(Object value, int columnIndex) {
                if (columnIndex == 0 && value instanceof SessionMark) {
                    SessionMark mark = (SessionMark) value;
                    return mark.getValueTitle() + " (" + mark.getSlot().getDocument().getTypeTitle() + ")";
                }
                return "";
            }
        });

        setSourceEppSlotModel(new DQLFullCheckSelectModel(EppStudentWpeCAction.P_REGISTRY_ELEMENT_TITLE) {
            @Override
            protected DQLSelectBuilder query(String alias, String filter) {
                if (getEditedRow() == null || getEditedRow().getSourceTerm() == null || getEditedRow().getSourceTerm().getPart() == null) {
                    return new DQLSelectBuilder().fromEntity(EppStudentWpeCAction.class, alias).where(isNull(property(alias + ".id")));
                }
                SessionTermModel.TermWrapper sourceTerm = getEditedRow().getSourceTerm();
                DQLSelectBuilder marks = new DQLSelectBuilder()
                        .fromEntity(SessionSlotRegularMark.class, "mark")
                        .column(property(SessionSlotRegularMark.slot().studentWpeCAction().id().fromAlias("mark")))
                        .where(eq(property(SessionSlotRegularMark.slot().studentWpeCAction().studentWpe().student().fromAlias("mark")), value(getStudent())))
                        .where(eq(property(SessionSlotRegularMark.slot().studentWpeCAction().studentWpe().part().fromAlias("mark")), value(sourceTerm.getPart())))
                        .where(eq(property(SessionSlotRegularMark.slot().studentWpeCAction().studentWpe().year().educationYear().fromAlias("mark")), value(sourceTerm.getYear())))
                        .where(isNotNull(property(SessionSlotRegularMark.slot().studentWpeCAction().removalDate().fromAlias("mark"))))
                        ;
                return new DQLSelectBuilder().fromEntity(EppStudentWpeCAction.class, alias).where(in(property(alias + ".id"), marks.buildQuery()));
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void sortOptions(List list) {
                Collections.sort((List<EppStudentWpeCAction>) list);
            }
        });

        setTargetTermModel(SessionTermModel.createForStudent(getStudent()));
    }

    // Getters & Setters

    public Integer getColumnCount()
    {
        return isUseCurrentRating() ? 4 : 3;
    }

    public boolean isUseCurrentRating()
    {
        return useCurrentRating;
    }

    public List<BaseValidator> getRatingValidators()
    {
        final List<BaseValidator> validators = new ArrayList<>(3);
        if (ISessionBrsDao.instance.get().getSettings().isCurrentRatingRequiredForTransferDocument())
            validators.add(new Required());
        validators.add(new Min("min=0"));
        validators.add(new Max("max=100"));
        return validators;
    }

    public Long getStudentId()
    {
        return studentId;
    }

    public void setStudentId(Long studentId)
    {
        this.studentId = studentId;
    }

    public Long getDocumentId()
    {
        return documentId;
    }

    public void setDocumentId(Long documentId)
    {
        this.documentId = documentId;
    }

    public Long getOrgUnitId()
    {
        return orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        this.orgUnitId = orgUnitId;
    }

    public boolean isEditForm()
    {
        return editForm;
    }

    public void setEditForm(boolean editForm)
    {
        this.editForm = editForm;
    }

    public Student getStudent()
    {
        return student;
    }

    public void setStudent(Student student)
    {
        this.student = student;
    }

    public SessionTransferInsideDocument getDocument()
    {
        return document;
    }

    public void setDocument(SessionTransferInsideDocument document)
    {
        this.document = document;
    }

    public OrgUnit getOrgUnit()
    {
        return orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        this.orgUnit = orgUnit;
    }

    public List<SessionTransferInOpWrapper> getRowList()
    {
        return rowList;
    }

    public void setRowList(List<SessionTransferInOpWrapper> rowList)
    {
        this.rowList = rowList;
    }

    public Date getMarkDate()
    {
        return markDate;
    }

    public void setMarkDate(Date markDate)
    {
        this.markDate = markDate;
    }

    public SessionTransferInOpWrapper getCurrentRow()
    {
        return currentRow;
    }

    public void setCurrentRow(SessionTransferInOpWrapper currentRow)
    {
        this.currentRow = currentRow;
    }

    public SessionTransferInOpWrapper getEditedRow()
    {
        return editedRow;
    }

    public void setEditedRow(SessionTransferInOpWrapper editedRow)
    {
        this.editedRow = editedRow;
    }

    public Integer getEditedRowIndex()
    {
        return editedRowIndex;
    }

    public void setEditedRowIndex(Integer editedRowIndex)
    {
        this.editedRowIndex = editedRowIndex;
    }

    public boolean isRowAdded() {
        return rowAdded;
    }

    public void setRowAdded(boolean rowAdded) {
        this.rowAdded = rowAdded;
    }

    public ISelectModel getTargetTermModel() {
        return targetTermModel;
    }

    public void setTargetTermModel(ISelectModel targetTermModel) {
        this.targetTermModel = targetTermModel;
    }

    public ISelectModel getTargetMarkModel() {
        return targetMarkModel;
    }

    public void setTargetMarkModel(ISelectModel targetMarkModel) {
        this.targetMarkModel = targetMarkModel;
    }

    public ISelectModel getSourceTermModel() {
        return sourceTermModel;
    }

    public void setSourceTermModel(ISelectModel sourceTermModel) {
        this.sourceTermModel = sourceTermModel;
    }

    public ISelectModel getSourceMarkModel() {
        return sourceMarkModel;
    }

    public void setSourceMarkModel(ISelectModel sourceMarkModel) {
        this.sourceMarkModel = sourceMarkModel;
    }

    public ISelectModel getSourceEppSlotModel() {
        return sourceEppSlotModel;
    }

    public void setSourceEppSlotModel(ISelectModel sourceEppSlotModel) {
        this.sourceEppSlotModel = sourceEppSlotModel;
    }

    public boolean isAutoForm(){ return autoForm; }
    public void setAutoForm(boolean autoForm){ this.autoForm = autoForm; }
}