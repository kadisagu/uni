/* $Id: SessionReportResultsAdd.java 21854 2012-02-10 11:12:10Z oleyba $ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.ResultsByDiscAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.catalog.bo.StudentCatalogs.StudentCatalogsManager;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.base.bo.SessionReport.util.UniSessionFilterAddon;

/**
 * @author oleyba
 * @since 2/7/12
 */
@Configuration
public class SessionReportResultsByDiscAdd extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
            .addAddon(uiAddon(CommonFilterAddon.class.getSimpleName(), UniSessionFilterAddon.class))
            .addDataSource(SessionReportManager.instance().eduYearDSConfig())
            .addDataSource(SessionReportManager.instance().yearPartDSConfig())
            .create();
    }
}