/* $*/

package ru.tandemservice.unisession.print;

import java.util.Collection;

import org.tandemframework.rtf.document.RtfDocument;

import org.tandemframework.core.util.cache.SpringBeanCache;

/**
 * @author oleyba
 * @since 4/2/11
 */
public interface ISessionBulletinPrintDAO
{
    SpringBeanCache<ISessionBulletinPrintDAO> instance = new SpringBeanCache<ISessionBulletinPrintDAO>(ISessionBulletinPrintDAO.class.getName());

    /**
     * Массовая печать ведомостей в архив, по файлу на каждую
     * @param ids id ведомостей
     * @return контент файла
     */
    byte[] printBulletinListToZipArchive(Collection<Long> ids);

    /**
     * Массовая печать ведомостей в один rtf-документ
     * @param ids id ведомостей
     * @return rtf
     */
    RtfDocument printBulletinList(Collection<Long> ids);

    /**
     * Печать ведомости
     * @param id id ведомости
     * @return rtf
     */
    RtfDocument printBulletin(Long id);
}
