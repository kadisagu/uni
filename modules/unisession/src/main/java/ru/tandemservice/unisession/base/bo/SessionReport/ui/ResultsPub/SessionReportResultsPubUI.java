/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.ResultsPub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.entity.report.UnisessionResultsReport;

/**
 * @author oleyba
 * @since 2/6/12
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "report.id")
})
public class SessionReportResultsPubUI extends UIPresenter
{
    private UnisessionResultsReport _report = new UnisessionResultsReport();

    @Override
    public void onComponentRefresh()
    {
        setReport(DataAccessServices.dao().getNotNull(UnisessionResultsReport.class, getReport().getId()));
    }

    public void onClickDelete()
    {
        DataAccessServices.dao().delete(_report.getId());
        deactivate();
    }

    public void onClickPrint()
    {
        getActivationBuilder().asDesktopRoot(IUniComponents.DOWNLOAD_STORABLE_REPORT).parameter("reportId", _report.getId()).parameter("extension", "rtf").parameter("zip", Boolean.FALSE).activate();
    }

    @Override
    public ISecured getSecuredObject()
    {
        return null == getReport().getOrgUnit() ? super.getSecuredObject() : getReport().getOrgUnit();
    }

    // Getters & Setters

    public CommonPostfixPermissionModelBase getSec()
    {
        return new CommonPostfixPermissionModel(getReport().getOrgUnit() != null ? OrgUnitSecModel.getPostfix(getReport().getOrgUnit()) : null);

    }
    public String getViewPermissionKey(){ return getSec().getPermission(getReport().getOrgUnit() != null ? "orgUnit_viewSessionReportResultsList" : "sessionResultsReport"); }

    public String getDeleteStorableReportPermissionKey(){ return getSec().getPermission(getReport().getOrgUnit() != null ? "orgUnit_deleteSessionReportResultsList" : "deleteSessionStorableReport"); }

    public UnisessionResultsReport getReport()
    {
        return _report;
    }

    public void setReport(UnisessionResultsReport report)
    {
        _report = report;
    }
}
