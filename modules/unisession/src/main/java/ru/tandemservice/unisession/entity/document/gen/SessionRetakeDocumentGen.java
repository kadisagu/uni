package ru.tandemservice.unisession.entity.document.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unisession.entity.comission.SessionComission;
import ru.tandemservice.unisession.entity.document.SessionDocument;
import ru.tandemservice.unisession.entity.document.SessionObject;
import ru.tandemservice.unisession.entity.document.SessionRetakeDocument;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Ведомость пересдач
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionRetakeDocumentGen extends SessionDocument
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.document.SessionRetakeDocument";
    public static final String ENTITY_NAME = "sessionRetakeDocument";
    public static final int VERSION_HASH = -1253867498;
    private static IEntityMeta ENTITY_META;

    public static final String L_SESSION_OBJECT = "sessionObject";
    public static final String L_REGISTRY_ELEMENT_PART = "registryElementPart";
    public static final String L_COMMISSION = "commission";
    public static final String P_PERFORM_DATE = "performDate";
    public static final String P_TITLE = "title";

    private SessionObject _sessionObject;     // Сессия
    private EppRegistryElementPart _registryElementPart;     // Мероприятие (элемент реестра), часть
    private SessionComission _commission;     // Комиссия
    private Date _performDate;     // Дата сдачи

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сессия. Свойство не может быть null.
     */
    @NotNull
    public SessionObject getSessionObject()
    {
        return _sessionObject;
    }

    /**
     * @param sessionObject Сессия. Свойство не может быть null.
     */
    public void setSessionObject(SessionObject sessionObject)
    {
        dirty(_sessionObject, sessionObject);
        _sessionObject = sessionObject;
    }

    /**
     * @return Мероприятие (элемент реестра), часть. Свойство не может быть null.
     */
    @NotNull
    public EppRegistryElementPart getRegistryElementPart()
    {
        return _registryElementPart;
    }

    /**
     * @param registryElementPart Мероприятие (элемент реестра), часть. Свойство не может быть null.
     */
    public void setRegistryElementPart(EppRegistryElementPart registryElementPart)
    {
        dirty(_registryElementPart, registryElementPart);
        _registryElementPart = registryElementPart;
    }

    /**
     * @return Комиссия. Свойство не может быть null.
     */
    @NotNull
    public SessionComission getCommission()
    {
        return _commission;
    }

    /**
     * @param commission Комиссия. Свойство не может быть null.
     */
    public void setCommission(SessionComission commission)
    {
        dirty(_commission, commission);
        _commission = commission;
    }

    /**
     * @return Дата сдачи.
     */
    public Date getPerformDate()
    {
        return _performDate;
    }

    /**
     * @param performDate Дата сдачи.
     */
    public void setPerformDate(Date performDate)
    {
        dirty(_performDate, performDate);
        _performDate = performDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof SessionRetakeDocumentGen)
        {
            setSessionObject(((SessionRetakeDocument)another).getSessionObject());
            setRegistryElementPart(((SessionRetakeDocument)another).getRegistryElementPart());
            setCommission(((SessionRetakeDocument)another).getCommission());
            setPerformDate(((SessionRetakeDocument)another).getPerformDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionRetakeDocumentGen> extends SessionDocument.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionRetakeDocument.class;
        }

        public T newInstance()
        {
            return (T) new SessionRetakeDocument();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "sessionObject":
                    return obj.getSessionObject();
                case "registryElementPart":
                    return obj.getRegistryElementPart();
                case "commission":
                    return obj.getCommission();
                case "performDate":
                    return obj.getPerformDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "sessionObject":
                    obj.setSessionObject((SessionObject) value);
                    return;
                case "registryElementPart":
                    obj.setRegistryElementPart((EppRegistryElementPart) value);
                    return;
                case "commission":
                    obj.setCommission((SessionComission) value);
                    return;
                case "performDate":
                    obj.setPerformDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "sessionObject":
                        return true;
                case "registryElementPart":
                        return true;
                case "commission":
                        return true;
                case "performDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "sessionObject":
                    return true;
                case "registryElementPart":
                    return true;
                case "commission":
                    return true;
                case "performDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "sessionObject":
                    return SessionObject.class;
                case "registryElementPart":
                    return EppRegistryElementPart.class;
                case "commission":
                    return SessionComission.class;
                case "performDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionRetakeDocument> _dslPath = new Path<SessionRetakeDocument>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionRetakeDocument");
    }
            

    /**
     * @return Сессия. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionRetakeDocument#getSessionObject()
     */
    public static SessionObject.Path<SessionObject> sessionObject()
    {
        return _dslPath.sessionObject();
    }

    /**
     * @return Мероприятие (элемент реестра), часть. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionRetakeDocument#getRegistryElementPart()
     */
    public static EppRegistryElementPart.Path<EppRegistryElementPart> registryElementPart()
    {
        return _dslPath.registryElementPart();
    }

    /**
     * @return Комиссия. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionRetakeDocument#getCommission()
     */
    public static SessionComission.Path<SessionComission> commission()
    {
        return _dslPath.commission();
    }

    /**
     * @return Дата сдачи.
     * @see ru.tandemservice.unisession.entity.document.SessionRetakeDocument#getPerformDate()
     */
    public static PropertyPath<Date> performDate()
    {
        return _dslPath.performDate();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.entity.document.SessionRetakeDocument#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends SessionRetakeDocument> extends SessionDocument.Path<E>
    {
        private SessionObject.Path<SessionObject> _sessionObject;
        private EppRegistryElementPart.Path<EppRegistryElementPart> _registryElementPart;
        private SessionComission.Path<SessionComission> _commission;
        private PropertyPath<Date> _performDate;
        private SupportedPropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сессия. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionRetakeDocument#getSessionObject()
     */
        public SessionObject.Path<SessionObject> sessionObject()
        {
            if(_sessionObject == null )
                _sessionObject = new SessionObject.Path<SessionObject>(L_SESSION_OBJECT, this);
            return _sessionObject;
        }

    /**
     * @return Мероприятие (элемент реестра), часть. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionRetakeDocument#getRegistryElementPart()
     */
        public EppRegistryElementPart.Path<EppRegistryElementPart> registryElementPart()
        {
            if(_registryElementPart == null )
                _registryElementPart = new EppRegistryElementPart.Path<EppRegistryElementPart>(L_REGISTRY_ELEMENT_PART, this);
            return _registryElementPart;
        }

    /**
     * @return Комиссия. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionRetakeDocument#getCommission()
     */
        public SessionComission.Path<SessionComission> commission()
        {
            if(_commission == null )
                _commission = new SessionComission.Path<SessionComission>(L_COMMISSION, this);
            return _commission;
        }

    /**
     * @return Дата сдачи.
     * @see ru.tandemservice.unisession.entity.document.SessionRetakeDocument#getPerformDate()
     */
        public PropertyPath<Date> performDate()
        {
            if(_performDate == null )
                _performDate = new PropertyPath<Date>(SessionRetakeDocumentGen.P_PERFORM_DATE, this);
            return _performDate;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.entity.document.SessionRetakeDocument#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(SessionRetakeDocumentGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return SessionRetakeDocument.class;
        }

        public String getEntityName()
        {
            return "sessionRetakeDocument";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getTitle();
}
