/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.StateFinalExamResultAdd;


import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.activator.IRootComponentActivationBuilder;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.StateFinalAttestationResultAdd.SessionReportStateFinalAttestationResultAddUI;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.StateFinalExamResultPub.SessionReportStateFinalExamResultPub;
import ru.tandemservice.unisession.entity.report.StateFinalExamResult;


/**
 * @author Andrey Andreev
 * @since 27.10.2016
 */
@State({@Bind(key = SessionReportManager.BIND_ORG_UNIT, binding = "ouHolder.id")})
public class SessionReportStateFinalExamResultAddUI extends SessionReportStateFinalAttestationResultAddUI<StateFinalExamResult>
{


    @Override
    public StateFinalExamResult initReport()
    {
        return new StateFinalExamResult();
    }

    @Override
    public void createStoredReport(StateFinalExamResult report)
    {
        SessionReportManager.instance().stateFinalExamResultPrintDAO().createStoredReport(report, getOuHolder().getId());
    }

    @Override
    public void openReportPub(StateFinalExamResult report)
    {
        IRootComponentActivationBuilder builder = getActivationBuilder()
                .asDesktopRoot(SessionReportStateFinalExamResultPub.class)
                .parameter(UIPresenter.PUBLISHER_ID, report.getId());
        if (getOuHolder().getId() != null)
            builder.parameter(SessionReportManager.BIND_ORG_UNIT, getOuHolder().getId());
        builder.activate();
    }

    @Override
    public String getAddPermissionKey()
    {
        return getSec().getPermission(getOuHolder().getId() != null ? "orgUnit_addStateFinalExamResult" : "addSessionStorableReport");
    }
}
