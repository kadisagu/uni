/**
 *$Id$
 */
package ru.tandemservice.unisession.base.ext.SystemAction.ui.Pub;

import com.healthmarketscience.jackcess.Database;
import org.apache.log4j.Logger;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.unisession.base.bo.SessionSystemAction.SessionSystemActionManager;
import ru.tandemservice.unisession.dao.io.ISessionIODAO;
import ru.tandemservice.unisession.dao.io.SessionIODAO;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;

/**
 * @author Alexander Shaburov
 * @since 12.02.13
 */
public class SessionSystemActionPubAddon extends UIAddon
{
    public SessionSystemActionPubAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public void onClickRefreshTotalMarks()
    {
        SessionSystemActionManager.instance().dao().doRefreshFinalMarks();
    }

    public void onClickExportSessionTemplate()
    {
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer() {
            @Override public void render(OutputStream outputStream) {
                ISessionIODAO.instance.get().exportSessionTemplate(outputStream);
            }
        }.contentType("mdb").fileName("mark-template-" + DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()) + ".mdb"), true);
    }

    public void onClickImportMarks()
    {
        try {
            String path = ApplicationRuntime.getAppInstallPath();
            final File file = new File(path, "data/marks.mdb");
            if (!file.exists()) { throw new ApplicationException("Импорт данных провести не удалось: не найден файл «marks.mdb» с исходными данными в каталоге «${app.install.path}/data/». Разместите файл для импорта с указанным названием на сервере приложения по указанному пути."); }
            if (!file.canRead()) { throw new ApplicationException("Импорт данных провести не удалось: файл «${app.install.path}/data/marks.mdb» не доступен для чтения. Проверьте доступ к файлу на сервере приложения."); }

            try (Database mdb = Database.open(file)) {
                ISessionIODAO.instance.get().doImportMarks(mdb);
            }
        }
        catch (IOException e) {
            Logger.getLogger(SessionIODAO.class).error("Error occured during getting import file, see exception info below. ", e);
            throw CoreExceptionUtils.getRuntimeException(e);
        }
    }
}
