/* $Id$ */
package ru.tandemservice.unisession.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;

/**
 * @author Nikolay Fedorovskih
 * @since 06.10.2014
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unisession_2x6x8_4to5 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.15"),
                        new ScriptDependency("org.tandemframework.shared", "1.6.8")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        Long stateScaleItemId;
        {
            short entityCode = tool.entityCodes().ensure("sessionCurrentMarkScale");
            stateScaleItemId = EntityIDGenerator.generateNewId(entityCode);
            tool.executeUpdate("insert into session_c_cur_mark_scale_t (id, discriminator, code_p, active_p, title_p) values (?,?,?,?,?)",
                               stateScaleItemId, entityCode, "scaleState", true, "Атт./неатт.");
        }

        Long passedItemId, noPassedItemId;
        {
            short entityCode = tool.entityCodes().ensure("sessionCurrentMarkCatalogItem");

            tool.table("session_c_mark_base_t").constraints().clear();

            // Аттестован
            passedItemId = EntityIDGenerator.generateNewId(entityCode);

            tool.executeUpdate("insert into session_c_mark_base_t (id, discriminator, code_p, catalogcode_p, shorttitle_p, printtitle_p, cachedpositivestatus_p, title_p) values (?,?,?,?,?,?,?,?)",
                               passedItemId, entityCode, "passed", "sessionCurrentMarkCatalogItem", "атт.", "аттестован", true, "Аттестован");
            tool.executeUpdate("insert into session_c_cur_mark_t (id, scale_id, priority_p, positive_p) values (?,?,?,?)", passedItemId, stateScaleItemId, 1, true);

            // Не аттестован
            noPassedItemId = EntityIDGenerator.generateNewId(entityCode);
            tool.executeUpdate("insert into session_c_mark_base_t (id, discriminator, code_p, catalogcode_p, shorttitle_p, printtitle_p, cachedpositivestatus_p, title_p) values (?,?,?,?,?,?,?,?)",
                               noPassedItemId, entityCode, "noPassed", "sessionCurrentMarkCatalogItem", "не атт.", "не аттестован", false, "Не аттестован");
            tool.executeUpdate("insert into session_c_cur_mark_t (id, scale_id, priority_p, positive_p) values (?,?,?,?)", noPassedItemId, stateScaleItemId, 2, false);
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность sessionAttestationSlot

        // создано свойство currentMark
        {
            // создать колонку
            tool.createColumn("session_att_slot_t", new DBColumn("currentmark_id", DBType.LONG));

            tool.executeUpdate("update session_att_slot_t set currentmark_id=? where passed_p=?", passedItemId, true);
            tool.executeUpdate("update session_att_slot_t set currentmark_id=? where passed_p=?", noPassedItemId, false);
        }

        // удалено свойство passed
        {
            // удалить колонку
            tool.dropColumn("session_att_slot_t", "passed_p");
        }
    }
}
