/* $Id:$ */
package ru.tandemservice.unisession.base.bo.SessionReport.util.resultsByDisc;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.ResultsByDiscAdd.ISessionReportResultsByDiscDAO;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.ResultsByDiscAdd.SessionReportResultsByDiscAddUI;
import ru.tandemservice.unisession.base.bo.SessionReport.util.SessionReportResultsGrouping;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkStateCatalogItem;
import ru.tandemservice.unisession.entity.catalog.codes.SessionMarkGradeValueCatalogItemCodes;
import ru.tandemservice.unisession.entity.catalog.codes.SessionMarkStateCatalogItemCodes;

import java.util.*;

/**
 * @author oleyba
 * @since 2/19/12
 */
public class SessionReportResultsByDiscUtils
{
    public static List<ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscGrouping> getDefaultGroupingList()
    {
        return Arrays.asList(
            (ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscGrouping) new Grouping(1, "sessionReportResultsByDiscGrouping.byCourse", "по курсам")
            {
                @Override
                public List<ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscRow> getReportRowGroups(SessionReportResultsByDiscAddUI model, final Collection<ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscMarkData> marks)
                {
                    final List<ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscRow> courseRows = new ArrayList<>();
                    for (Course course : DevelopGridDAO.getCourseMap().values()) {

                        Set<ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscMarkData> courseMarks = new HashSet<>();

                        Map<MultiKey, DisciplineRow> discMap = new HashMap<>();
                        for (ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscMarkData mark : marks) {
                            if (!course.equals(mark.getCourse()))
                                continue;
                            MultiKey key = new MultiKey(mark.getDisc().getId(), mark.getTerm().getId());
                            DisciplineRow discRow = discMap.get(key);
                            if (null == discRow)
                                discMap.put(key, discRow = discRow(new MultiKey(course.getIntValue(), mark.getDisc().getId(), mark.getTerm().getId()), mark.getTerm().getIntValue(), model.isUseFullDiscTitle() ? mark.getDisc().getRegistryElement().getEducationElementTitle() : mark.getDisc().getRegistryElement().getTitle(), new ArrayList<ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscMarkData>()));
                            discRow.getMarks().add(mark);
                            courseMarks.add(mark);
                        }

                        List<DisciplineRow> discRows = new ArrayList<>(discMap.values());

                        Collections.sort(discRows);

                        if (!discRows.isEmpty()) {
                            courseRows.add(row(course.getIntValue(), "ИТОГО ПО " + course.getIntValue() + " курсу", courseMarks, discRows));
                        }
                    }
                    
                    ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscRow mainGroup = row(0, "ВСЕГО", marks, courseRows);

                    return Collections.singletonList(mainGroup);
                }

                @Override
                public List<ISessionReportResultsByDiscDAO.ISessionReportResultsColumn> getReportColumns(SessionReportResultsByDiscAddUI model, Collection<ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscMarkData> marks)
                {
                    return SessionReportResultsByDiscUtils.getReportColumns();
                }
            },
            new Grouping(2, "sessionReportResultsByDiscGrouping.byIndex", "по индексам дисциплин")
            {
                @Override
                @SuppressWarnings("unchecked")
                public List<ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscRow> getReportRowGroups(SessionReportResultsByDiscAddUI model, final Collection<ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscMarkData> marks)
                {
                    final List<ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscRow> indexRows = new ArrayList<>();
                    for (IndexKey index : IndexKey.values()) {
                        Set<ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscMarkData> indexMarks = new HashSet<>();

                        Map<MultiKey, DisciplineRow> discMap = new HashMap<>();
                        for (ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscMarkData mark : marks) {
                            if (!index.equals(getIndexKey(mark.getIndex())))
                                continue;
                            MultiKey key = new MultiKey(mark.getDisc().getId(), mark.getTerm().getId());
                            DisciplineRow discRow = discMap.get(key);
                            if (null == discRow)
                                discMap.put(key, discRow = discRow(new MultiKey(index, mark.getDisc().getId(), mark.getTerm().getId()), mark.getTerm().getIntValue(), model.isUseFullDiscTitle() ? mark.getDisc().getRegistryElement().getEducationElementTitle() : mark.getDisc().getRegistryElement().getTitle(), new ArrayList<ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscMarkData>()));
                            discRow.getMarks().add(mark);
                            indexMarks.add(mark);
                        }

                        List<DisciplineRow> discRows = new ArrayList<>(discMap.values());

                        Collections.sort(discRows);

                        if (!discRows.isEmpty()) {
                            indexRows.add(row(index, getIndexTitle(index), indexMarks, discRows));
                        }
                    }

                    ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscRow mainGroup = row(0, "ВСЕГО", marks, indexRows);

                    return Collections.singletonList(mainGroup);
                }

                @Override
                public List<ISessionReportResultsByDiscDAO.ISessionReportResultsColumn> getReportColumns(SessionReportResultsByDiscAddUI model, Collection<ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscMarkData> marks)
                {
                    return SessionReportResultsByDiscUtils.getReportColumns();
                }
            }
        );
    }

    private static ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscRow row(
        final Object key,
        final String title,
        final Collection marks,
        final List subRows)
    {
        return new ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscRow()
        {
            @Override public Object getKey() { return key; }
            @Override public List<String> getTableRowStart() { return Arrays.asList("", title); }
            @Override public Collection<ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscMarkData> getMarks() { return marks; }
            @Override public List<ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscRow> getSubRows() { return subRows; }
        };
    }
    
    private interface DisciplineRow extends ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscRow, Comparable
    {
        int getTerm();
        String getTitle();
    }
    
    private static DisciplineRow discRow(final Object key, final int term, final String title, final Collection marks )
    {
        return new DisciplineRow()
        {
            @Override public Object getKey() { return key; }
            @Override public List<String> getTableRowStart() { return Arrays.asList(String.valueOf(term), title); }
            @Override public Collection<ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscMarkData> getMarks() { return marks; }
            @Override public List<ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscRow> getSubRows() { return null; }
            @Override public int getTerm() { return term; }
            @Override public String getTitle() { return title; }

            @Override
            public int compareTo(Object o)
            {
                if (!(o instanceof DisciplineRow))
                    return 0;
                DisciplineRow other = (DisciplineRow) o;
                int result = this.getTerm() - other.getTerm();
                if (result == 0) result = this.getTitle().compareTo(other.getTitle());
                return result;
            }
        };
    }    

    private static abstract class Grouping extends SessionReportResultsGrouping implements ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscGrouping
    {
        protected Grouping(int num, String key, String title)
        {
            super(num, key, title);
        }
    }    
    
    private enum IndexKey
    {
        KEY_FEDERAL, KEY_REGION, KEY_HIGH_SCHOOL, KEY_OTHERS
    }
    
    private static String getIndexTitle(IndexKey key)
    {
        switch (key)
        {
            case KEY_OTHERS: return "ИТОГО (Прочие дисциплины)";
            case KEY_FEDERAL: return "ИТОГО (Федеральный компонент)";
            case KEY_REGION: return "ИТОГО (Региональный компонент)";
            case KEY_HIGH_SCHOOL: return "ИТОГО (Вузовский компонент)";
        }
        return "ИТОГО (Прочие дисциплины)";
    }

    private static IndexKey getIndexKey(String index)
    {
        if (StringUtils.isBlank(index))
            return IndexKey.KEY_OTHERS;
        else
        {
            final String[] parts = index.split("\\.");
            if (parts.length < 2)
                return IndexKey.KEY_OTHERS;
            else
            {
                final String comp = parts[1];
                if ("Ф".equals(comp))
                    return IndexKey.KEY_FEDERAL;
                else if ("Р".equals(comp))
                    return IndexKey.KEY_REGION;
                else if ("В".equals(comp))
                    return IndexKey.KEY_HIGH_SCHOOL;
                else
                    return IndexKey.KEY_OTHERS;
            }
        }
    }

    private static List<ISessionReportResultsByDiscDAO.ISessionReportResultsColumn> getReportColumns()
    {
        //Количество студентов к началу сессии - количество МСРПпоФИК вошедших в строку
        final PercentageColumn.NumberColumn all = new CountColumn()
        {
            @Override
            public boolean check(ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscMarkData mark)
            {
                return !(mark.getMark() instanceof SessionMarkStateCatalogItem && SessionMarkStateCatalogItemCodes.UNNECESSARY.equals(mark.getMark().getCode()));
            }
        };
        //Допущено к экзаменам - "Количество студентов к началу сессии" минус те МСРПпоФИК у которых итоговая оценка "Не допущен"
        final PercentageColumn.NumberColumn allowed = new CountColumn()
        {
            @Override
            public boolean check(ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscMarkData mark)
            {
                return !(mark.getMark() instanceof SessionMarkStateCatalogItem && (SessionMarkStateCatalogItemCodes.NO_ADMISSION.equals(mark.getMark().getCode()) || SessionMarkStateCatalogItemCodes.UNNECESSARY.equals(mark.getMark().getCode())));
            }
        };
        //Явилось на экзамен - то число МСРПпоФИК строки, у которых итоговой оценкой является именно оценка, а не отметка
        final PercentageColumn.NumberColumn appeared = new CountColumn()
        {
            @Override
            public boolean check(ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscMarkData mark)
            {
                return mark.getMark() instanceof SessionMarkGradeValueCatalogItem;
            }
        };        
        //Не явилось на экзамен - это разница между "Допущено к экзаменам" и "Явилось на экзамен"
        final PercentageColumn.NumberColumn notAppeared = new PercentageColumn.NumberColumn()
        {
            @Override
            public Number getCount(ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscRow row)
            {
                return allowed.getCount(row).doubleValue() - appeared.getCount(row).doubleValue();
            }

            @Override
            public String cell(ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscRow row)
            {
                return String.valueOf(getCount(row).intValue());
            }
        };
        //5 - количество МСРПпоФИК у которых итоговая оценка "5"
        final PercentageColumn.NumberColumn c5 = new CountColumn()
        {
            @Override
            public boolean check(ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscMarkData mark)
            {
                return mark.getMark() instanceof SessionMarkGradeValueCatalogItem && SessionMarkGradeValueCatalogItemCodes.OTLICHNO.equals(mark.getMark().getCode());
            }
        };
        //4 - количество МСРПпоФИК у которых итоговая оценка "4"
        final PercentageColumn.NumberColumn c4 = new CountColumn()
        {
            @Override
            public boolean check(ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscMarkData mark)
            {
                return mark.getMark() instanceof SessionMarkGradeValueCatalogItem && SessionMarkGradeValueCatalogItemCodes.HOROSHO.equals(mark.getMark().getCode());
            }
        };
        //3 - количество МСРПпоФИК у которых итоговая оценка "3"
        final PercentageColumn.NumberColumn c3 = new CountColumn()
        {
            @Override
            public boolean check(ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscMarkData mark)
            {
                return mark.getMark() instanceof SessionMarkGradeValueCatalogItem && SessionMarkGradeValueCatalogItemCodes.UDOVLETVORITELNO.equals(mark.getMark().getCode());
            }
        };
        //2 - количество МСРПпоФИК у которых итоговая оценка "2"
        final PercentageColumn.NumberColumn c2 = new CountColumn()
        {
            @Override
            public boolean check(ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscMarkData mark)
            {
                return mark.getMark() instanceof SessionMarkGradeValueCatalogItem && SessionMarkGradeValueCatalogItemCodes.NEUDOVLETVORITELNO.equals(mark.getMark().getCode());
            }
        };

        return Arrays.asList(
            all,
            allowed,
            appeared,
            notAppeared,
            c5,
            // % считаем от допущено к экзаменам (для строк ниже также от допущено к экзаменам)
            new PercentageColumn(allowed, c5),
            c4,
            new PercentageColumn(allowed, c4),
            c3,
            new PercentageColumn(allowed, c3),
            c2,
            new PercentageColumn(allowed, c2)
            );
    }

}
