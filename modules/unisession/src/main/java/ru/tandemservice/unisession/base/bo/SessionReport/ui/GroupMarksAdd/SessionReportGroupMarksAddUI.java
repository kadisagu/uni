/* $Id: SessionReportGroupMarksAddUI.java 21962 2012-02-19 13:57:27Z oleyba $ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.GroupMarksAdd;

import com.google.common.collect.ImmutableList;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilter;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilterType;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterFormConfig;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uniepp.entity.catalog.EppWorkPlanRowKind;
import ru.tandemservice.uniepp.entity.catalog.codes.EppWorkPlanRowKindCodes;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.GroupMarksPub.SessionReportGroupMarksPub;
import ru.tandemservice.unisession.base.bo.SessionReport.util.UniSessionFilterAddon;
import ru.tandemservice.unisession.entity.report.UnisessionGroupMarksReport;

import java.util.ArrayList;
import java.util.List;

/**
 * @author oleyba
 * @since 12/29/11
 */

@Input
({
    @Bind(key = SessionReportManager.BIND_ORG_UNIT, binding = "ouHolder.id", required = true)
})
public class SessionReportGroupMarksAddUI extends UIPresenter
{
    private OrgUnitHolder ouHolder = new OrgUnitHolder();

    private IdentifiableWrapper discView;

	private boolean disciplineSelected = false;
	private List<EppRegistryElementPart> disciplines = new ArrayList<>();

    public static IdentifiableWrapper DISC_VIEW_BY_TITLE = new IdentifiableWrapper(0L, "по названию");
    public static IdentifiableWrapper DISC_VIEW_BY_TERM = new IdentifiableWrapper(1L, "по семестрам");


    @Override
    public void onComponentActivate()
    {
        getOuHolder().refresh();
    }

    @Override
    public void onComponentRefresh()
    {
        getOuHolder().refresh();
        configUtil(getSessionFilterAddon());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
		switch (dataSource.getName())
		{
			case SessionReportGroupMarksAdd.DISCIPLINE_DS:
				dataSource.put(SessionReportGroupMarksAdd.FILTER_SESSION_ADDON, getSessionFilterAddon());
				dataSource.put(SessionReportGroupMarksAdd.FILTER_ORG_UNIT, getOrgUnit());
				break;
		}
    }

    public void onClickApply() throws Exception
    {
        final ISessionReportGroupMarksDAO dao = ISessionReportGroupMarksDAO.instance.get();
        UnisessionGroupMarksReport report = dao.createStoredReport(this);
        deactivate();
        _uiActivation.asDesktopRoot(SessionReportGroupMarksPub.class)
                .parameter(UIPresenter.PUBLISHER_ID, report.getId())
                .activate();
    }

    // utils

    public LazySimpleSelectModel<IdentifiableWrapper> getDiscViewModel()
    {
        return new LazySimpleSelectModel<>(ImmutableList.of(
                DISC_VIEW_BY_TERM,
                DISC_VIEW_BY_TITLE
        ));
    }

    // getters and setters

    public OrgUnitHolder getOuHolder()
    {
        return ouHolder;
    }

    public OrgUnit getOrgUnit()
    {
        return getOuHolder().getValue();
    }

    public IdentifiableWrapper getDiscView() {
        return discView;
    }

    public void setDiscView(IdentifiableWrapper discView) {
        this.discView = discView;
    }

	public boolean isDisciplineSelected()
	{
		return disciplineSelected;
	}

	public void setDisciplineSelected(boolean disciplineSelected)
	{
		this.disciplineSelected = disciplineSelected;
	}

	public boolean isDisciplineDisabled()
	{
		return ! disciplineSelected;
	}

	public List<EppRegistryElementPart> getDisciplines()
	{
		return disciplines;
	}

	public void setDisciplines(List<EppRegistryElementPart> disciplines)
	{
		this.disciplines = disciplines;
	}

    // for filter addon

    public UniSessionFilterAddon getSessionFilterAddon()
    {
        return (UniSessionFilterAddon) getConfig().getAddon(CommonFilterAddon.class.getSimpleName());
    }


    private void configUtil(UniSessionFilterAddon util)
    {
        util
                .configDoubleWidthFilters(false)
                .configUseEnableCheckbox(true)
                .configSettings(getSettingsKey());


        util.clearFilterItems();

        util
                .addFilterItem(UniSessionFilterAddon.COURSE, new CommonFilterFormConfig(true, false, true, false, true, true))
                .addFilterItem(UniSessionFilterAddon.GROUP_WITH_NO_GROUP_ALT_TITLE, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(UniSessionFilterAddon.STUDENT_CUSTOM_STATE, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(UniSessionFilterAddon.TARGET_ADMISSION, CommonFilterFormConfig.DEFAULT_CHECKBOX_SELECT_FILTER_CONFIG)
                .addFilterItem(UniSessionFilterAddon.DISC_KIND, new CommonFilterFormConfig(true, true, true, false, true, true))
                .addFilterItem(UniSessionFilterAddon.REGISTRY_STRUCTURE, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(UniSessionFilterAddon.CONTROL_FORM, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG);


        final EppWorkPlanRowKind main = DataAccessServices.dao().getByCode(EppWorkPlanRowKind.class, EppWorkPlanRowKindCodes.MAIN);
        final List<EppWorkPlanRowKind> value = new ArrayList<>();
        value.add(main);
        util.getFilterItem(UniSessionFilterAddon.SETTINGS_NAME_DISC_KIND).setValue(value);


        util.saveSettings();

        configUtilWhere(util);
    }

	/**
	 * Применить к утили перманентные фильтры.
	 * NB: если в утили не выбраны академ. группы, то в селекте дисциплин эти условия не срабатывают, поэтому их приходится добавлять в нем вручную.
	 * Т.о. при добавлении фильтров в этом методе нужно будет обязательно продублировать их в {@link SessionReportGroupMarksAdd#disciplineDSHandler}!
	 */
    private void configUtilWhere(UniSessionFilterAddon util)
    {
        util.clearWhereFilter();
        util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EppStudentWpeCAction.studentWpe().student().educationOrgUnit().formativeOrgUnit(), getOrgUnit()));
        util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EppStudentWpeCAction.studentWpe().student().status().active(), true));
		util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EppStudentWpeCAction.removalDate(), null));
    }

}
