/* $Id$ */
package ru.tandemservice.unisession.component.student.StudentSessionProjectThemeTab;

import ru.tandemservice.uni.dao.IPrepareable;

/**
 * @author oleyba
 * @since 6/22/11
 */
public interface IDAO extends IPrepareable<Model>
{
    void prepareListDataSource(Model model);
}
