package ru.tandemservice.unisession.entity.document.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unisession.entity.document.SessionSheetDocument;
import ru.tandemservice.unisession.entity.document.SessionSimpleDocument;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Экзаменационный лист
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionSheetDocumentGen extends SessionSimpleDocument
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.document.SessionSheetDocument";
    public static final String ENTITY_NAME = "sessionSheetDocument";
    public static final int VERSION_HASH = 2052103850;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof SessionSheetDocumentGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionSheetDocumentGen> extends SessionSimpleDocument.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionSheetDocument.class;
        }

        public T newInstance()
        {
            return (T) new SessionSheetDocument();
        }
    }
    private static final Path<SessionSheetDocument> _dslPath = new Path<SessionSheetDocument>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionSheetDocument");
    }
            

    public static class Path<E extends SessionSheetDocument> extends SessionSimpleDocument.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return SessionSheetDocument.class;
        }

        public String getEntityName()
        {
            return "sessionSheetDocument";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
