/* $Id:$ */
package ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinMark;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.common.collect.Table;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.base.bo.SessionMark.SessionMarkManager;
import ru.tandemservice.unisession.base.bo.SessionMark.logic.ISessionMarkDAO;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao.ISessionRatingSettings;
import ru.tandemservice.unisession.brs.util.BrsRatingValueFormatter;
import ru.tandemservice.unisession.dao.comission.ISessionCommissionDAO;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkStateCatalogItem;
import ru.tandemservice.unisession.entity.comission.SessionComission;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionSlotRatingData;
import ru.tandemservice.unisession.entity.document.SessionStudentGradeBookDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.mark.SessionMarkRatingData;
import ru.tandemservice.unisession.entity.mark.SessionSlotLinkMark;
import ru.tandemservice.unisession.entity.mark.SessionSlotRegularMark;
import ru.tandemservice.unisession.practice.dao.ISessionPracticeDao;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 3/2/11
 */
public class DAO extends UniBaseDao implements IDAO
{
    @SuppressWarnings("RedundantCast")
    @Override
    public void prepare(final Model model)
    {
        model.getDocumentHolder().refresh();
        if (model.getDocument().getCloseDate() != null) {
            throw new ApplicationException("Ведомость закрыта, выставление оценок невозможно.");
        }

        prepareRatingSettings(model);

        prepareRegElementTitle(model);

        // Слоты ведомости
        final List<SessionDocumentSlot> slotList = new DQLSelectBuilder().fromEntity(SessionDocumentSlot.class, "slot")
                .column("slot")
                .joinPath(DQLJoinType.inner, SessionDocumentSlot.actualStudent().person().identityCard().fromAlias("slot"), "i")
                .where(eq(property("slot", SessionDocumentSlot.document()), value(model.getDocument())))
                .order(property("i", IdentityCard.lastName()))
                .order(property("i", IdentityCard.firstName()))
                .order(property("i", IdentityCard.middleName()))
                .createStatement(getSession()).list();

        // Формруем строки ведомости
        final Map<SessionDocumentSlot, ISelectModel> modelMap = SessionMarkManager.instance().regularMarkDao().getMarkModelMap(slotList);
        final Date docPerformDate = model.getDocPerformDate(); // Дата сдачи по умолчанию
        final List<BulletinMarkRow> rows = new ArrayList<>(slotList.size());
        final Map<Long, BulletinMarkRow> slotId2rowMap = Maps.newHashMapWithExpectedSize(slotList.size());
        final Map<Long, BulletinMarkRow> studentWpeCActionId2rowMap = Maps.newHashMapWithExpectedSize(slotList.size());
        for (SessionDocumentSlot slot : slotList)
        {
            //final String groupTitle = student2GroupTitleMap.get(slot.getStudentWpeCAction().getId());
            final ISelectModel markSelectModel = modelMap.get(slot);

            final BulletinMarkRow row = new BulletinMarkRow(model, slot, markSelectModel);
            row.setPerformDate(docPerformDate);

            rows.add(row);
            slotId2rowMap.put(slot.getId(), row);
            studentWpeCActionId2rowMap.put(slot.getStudentWpeCAction().getId(), row);
        }
        model.setRowList(rows);

        // Названия УГС студентов
        prepareEppRealEduGroupTitles(model, studentWpeCActionId2rowMap);

        // Ранее полученные оценки
        prepareFinalMarks(model, studentWpeCActionId2rowMap);

        // Оценки, баллы и даты сдачи
        prepareMarkAndPoints(model, slotId2rowMap);

        // Оценки по практикам
        preparePracticeMarks(model, studentWpeCActionId2rowMap);

        // Преподаватели
        prepareCommissions(model);

        // Допуски
        prepareAllowance(model);

        // Тек. рейтинги
        prepareCurrentRatingData(model);

        // Доработки в проектах
        prepareAdditionalMarkData(model);
    }

    protected void prepareEppRealEduGroupTitles(Model model, Map<Long, BulletinMarkRow> studentWpeCActionId2rowMap)
    {
        final List<Object[]> eppRealEduGroupList = new DQLSelectBuilder().fromEntity(EppRealEduGroupRow.class, "eg")
                .column(property("eg", EppRealEduGroupRow.studentWpePart().id()))
                .column(property("eg", EppRealEduGroupRow.studentGroupTitle()))
                .where(in(property("eg", EppRealEduGroupRow.studentWpePart()), studentWpeCActionId2rowMap.keySet()))
                .createStatement(getSession()).list();

        final Set<String> groupTitles = Sets.newHashSet();
        for (Object[] item : eppRealEduGroupList)
        {
            final Long studentWpePartId = (Long) item[0];
            final String title = StringUtils.trimToEmpty((String) item[1]);
            final BulletinMarkRow row = studentWpeCActionId2rowMap.get(studentWpePartId);
            row.setGroupTitle(title);
            groupTitles.add(title);
        }
        // Колонка с названиями групп не нужна, если у всех одна и та же группа
        model.setEppGroupTitleColumnVisible(groupTitles.size() > 1);
    }

    protected void prepareFinalMarks(Model model, Map<Long, BulletinMarkRow> studentWpeCActionId2rowMap)
    {
        final List<Object[]> finalMarkItems = new DQLSelectBuilder()
                .fromEntity(SessionStudentGradeBookDocument.class, "gb")
                .fromEntity(SessionMark.class, "m")
                .column(property("m"))
                .column(property("m", SessionMark.slot().studentWpeCAction().id()))
                .where(eq(property("m", SessionMark.slot().document()), property("gb")))
                .where(in(property("m", SessionMark.slot().studentWpeCAction()), studentWpeCActionId2rowMap.keySet()))
                .order(property("m", SessionMark.performDate()))
                .createStatement(getSession()).list();

        final SessionBulletinDocument bulletin = model.getBulletin();
        final Date bulletinDate = bulletin == null ? model.getDocPerformDate() : bulletin.getPerformDate();
        for (Object[] item : finalMarkItems)
        {
            final SessionMark sessionMark = (SessionMark) item[0];
            final Long studentWpeCActionId = (Long) item[1];
            final SessionSlotRegularMark finalMark = sessionMark instanceof SessionSlotRegularMark ? (SessionSlotRegularMark) sessionMark : ((SessionSlotLinkMark) sessionMark).getTarget();

            final BulletinMarkRow row = studentWpeCActionId2rowMap.get(studentWpeCActionId);
            row.setFinalMark(finalMark != null && (bulletinDate == null || finalMark.getPerformDate().before(bulletinDate)) ? finalMark.getDisplayableTitle() : null);
        }
    }

    protected void preparePracticeMarks(Model model, Map<Long, BulletinMarkRow> studentWpeCActionId2rowMap)
    {
        final SessionBulletinDocument bulletin = model.getBulletin();
        model.setShowPracticeTutorMarkColumn(bulletin != null && ISessionPracticeDao.instance.get().showPracticeTutorMarkColumnInBulletin(bulletin));
        if (model.isShowPracticeTutorMarkColumn())
        {
            final Map<Long, ISessionPracticeDao.IPracticeResult> markMap = ISessionPracticeDao.instance.get().getPracticeTutorMarks(studentWpeCActionId2rowMap.keySet());
            if (markMap == null)
                return;

            for (Map.Entry<Long, ISessionPracticeDao.IPracticeResult> entry : markMap.entrySet())
            {
                final BulletinMarkRow row = studentWpeCActionId2rowMap.get(entry.getKey());
                final SessionMarkGradeValueCatalogItem mark = entry.getValue().getMark();
                final boolean closed = entry.getValue().isClosed();
                row.setPracticeTutorMark(mark != null ? (mark.getTitle() + (closed ? "" : "*")) : "");
            }
        }
    }

    @SuppressWarnings("RedundantCast")
    protected void prepareMarkAndPoints(Model model, Map<Long, BulletinMarkRow> slotId2rowMap)
    {
        for (SessionMark mark : getList(SessionMark.class, SessionMark.slot().document(), model.getDocument()))
        {
            final BulletinMarkRow row = slotId2rowMap.get(mark.getSlot().getId());
            row.setPerformDate(mark.getPerformDate());

            if (!model.isUsePoints() || mark.getValueItem() instanceof SessionMarkStateCatalogItem)
                row.setMark(mark.getValueItem());

            if (!model.isUseCurrentRating() && model.isUsePoints())
                row.setPoints(mark.getPoints());
        }

        if (model.isUseCurrentRating())
        {
            final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(SessionMarkRatingData.class, "r")
                    .column("r")
                    .column(property("r", SessionMarkRatingData.mark().slot().id()))
                    .where(eq(property("r", SessionMarkRatingData.mark().slot().document()), value(model.getDocument())));

            for (Object[] item : dql.createStatement(getSession()).<Object[]>list())
            {
                final SessionMarkRatingData ratingData = (SessionMarkRatingData) item[0];
                final BulletinMarkRow row = slotId2rowMap.get((Long) item[1]);
                row.setPoints(ratingData.getScoredPoints());
            }
        }
    }

    protected void prepareCommissions(Model model)
    {
        final List<PpsEntry> commission = CommonBaseUtil.getPropertiesList(this.getList(SessionComissionPps.class, SessionComissionPps.L_COMMISSION, model.getDocumentHolder().getProperties().getCommission()), SessionComissionPps.L_PPS);
        Collections.sort(commission, CommonCollator.TITLED_WITH_ID_COMPARATOR);
        model.setTutors(commission);

        final DQLSelectBuilder slotTutorsDQL = new DQLSelectBuilder()
                .fromEntity(SessionComissionPps.class, "rel")
                .joinEntity("rel", DQLJoinType.inner, SessionDocumentSlot.class, "slot",
                            eq(property("rel", SessionComissionPps.commission()), property("slot", SessionDocumentSlot.commission())))
                .column("slot.id")
                .column(property("rel", SessionComissionPps.pps().id()))
                .where(eq(property("slot", SessionDocumentSlot.document()), value(model.getDocument())));

        final Table<Long, Long, Boolean> slot2ppsTable = HashBasedTable.create(model.getRowList().size(), commission.size());
        for (final Object[] row : slotTutorsDQL.createStatement(this.getSession()).<Object[]>list())
        {
            final Long slotId = (Long) row[0];
            final Long ppsId = (Long) row[1];
            slot2ppsTable.put(slotId, ppsId, true);
        }
        model.setSlot2ppsTable(slot2ppsTable);
    }

    @Override
    public void prepareRatingSettings(Model model)
    {
        if (!existsEntity(SessionDocumentSlot.class, SessionDocumentSlot.L_DOCUMENT, model.getDocument()))
            throw new ApplicationException("В ведомости отсутствуют студенты, выставление оценок невозможно.");

        model.getDocumentHolder().refresh();
        if (model.getDocument().getCloseDate() != null) {
            throw new ApplicationException("Ведомость закрыта, выставление оценок невозможно.", true);
        }

        final Map<ISessionBrsDao.ISessionRatingSettingsKey, ISessionBrsDao.ISessionRatingSettings> ratingSettings = ISessionBrsDao.instance.get().getRatingSettings(model.getDocument());
        model.setRatingSettings(ratingSettings);

        final Set<Boolean> useCurrentRatingValues = new HashSet<>(CollectionUtils.collect(ratingSettings.values(), ISessionRatingSettings::useCurrentRating));

        if (useCurrentRatingValues.size() > 1) {
            throw new ApplicationException("Выставление оценок невозможно: в ведомость включены студенты с разными формами итогового контроля, при этом для них разные настройки использования БРС при выставлении оценки.");
        }
        model.setUseCurrentRating(!useCurrentRatingValues.isEmpty() && useCurrentRatingValues.iterator().next());

        final Set<Boolean> usePointsValues = new HashSet<>(CollectionUtils.collect(ratingSettings.values(), ISessionRatingSettings::usePoints));

        if (usePointsValues.size() > 1) {
            throw new ApplicationException("Выставление оценок невозможно: в ведомость включены студенты с разными формами итогового контроля, при этом для них разные настройки использования БРС при выставлении оценки.");
        }
        model.setUsePoints(!usePointsValues.isEmpty() && usePointsValues.iterator().next());

        if (model.isUseCurrentRating()) {
            model.setNeedToSaveCurrentRatingData(existsEntity(
                    new DQLSelectBuilder()
                            .fromEntity(SessionDocumentSlot.class, "s").column("s.id")
                            .joinEntity("s", DQLJoinType.left, SessionSlotRatingData.class, "r", eq(property("r", SessionSlotRatingData.slot()), property("s")))
                            .where(eq(property("s", SessionDocumentSlot.document()), value(model.getDocument())))
                            .where(isNull(property("r", SessionSlotRatingData.fixedCurrentRatingAsLong())))
                            .buildQuery()
            ));
        }
    }


    @Override
    public void prepareAllowance(Model model)
    {
        final Set<Long> disallowedStudents = SessionMarkManager.instance().regularMarkDao().getDisallowedStudents(model.getDocument());
        for (BulletinMarkRow row : model.getRowList())
        {
            row.setAllowed(!disallowedStudents.contains(row.getSlot().getActualStudent().getId()));
        }
    }

    @Override
    public boolean validate(Model model)
    {
        ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();
        for (BulletinMarkRow row : model.getRowList()) {
            row.validate(errorCollector);
        }
        return !errorCollector.hasErrors();
    }

    @SuppressWarnings("unchecked")
    @Override
    public void update(final Model model)
    {
        // на изменение комиссий есть листенер, который не дает сохранять оценки с пустой комиссией
        // соответственно, если он сработает, будет исключение, транзакция откатится, а состояние объектов в сессии не обновится
        // потому что refresh не происходит, чтобы данные пользователя с формы не сбросились
        // поэтому при сохранении чистим сессию, грузим все объекты заново из базы, и сеттим данные с формы
        this.getSession().clear();

        model.getDocumentHolder().refresh();
        final Map<SessionDocumentSlot, BulletinMarkRow> slotMap = Maps.newHashMapWithExpectedSize(model.getRowList().size());
        for (BulletinMarkRow row : model.getRowList())
        {
            slotMap.put(this.getNotNull(SessionDocumentSlot.class, row.getSlot().getId()), row);
        }

        NamedSyncInTransactionCheckLocker.register(this.getSession(), String.valueOf(model.getDocument().getId()));

        final Map<Set<Long>, Long> pps2comissionMap = new HashMap<>();
        {
            final Map<Long, Set<Long>> commission2ppsMap = new HashMap<>();
            final DQLSelectBuilder slotComissionDql = new DQLSelectBuilder()
                    .fromEntity(SessionComissionPps.class, "rel")
                    .column(property(SessionComissionPps.commission().id().fromAlias("rel")))
                    .column(property(SessionComissionPps.pps().id().fromAlias("rel")))
                    .where(in(
                            property(SessionComissionPps.commission().fromAlias("rel")),
                            new DQLSelectBuilder()
                                    .fromEntity(SessionDocumentSlot.class, "slot")
                                    .column(property(SessionDocumentSlot.commission().id().fromAlias("slot")))
                                    .where(eq(property(SessionDocumentSlot.document().fromAlias("slot")), value(model.getDocumentHolder().getValue())))
                                    .buildQuery()
                    ));

            for (final Object[] row : scrollRows(slotComissionDql.createStatement(DAO.this.getSession()))) {
                SafeMap.safeGet(commission2ppsMap, (Long)row[0], HashSet.class).add((Long)row[1]);
            }

            for (final Map.Entry<Long, Set<Long>> e: commission2ppsMap.entrySet()) {
                pps2comissionMap.put(e.getValue(), e.getKey()); // здесь может быть не уникальность - это нормально (все неиспользуемые комиссии потом удалятся)
            }
        }

        // сохраним комиссии для слотов
        final Map<Set<Long>, SessionComission> currentCommissionMap = SafeMap.get((Set<Long> key) -> {
            final Long id = pps2comissionMap.get(key);
            if (null != id) { return DAO.this.get(SessionComission.class, id); }
            return ISessionCommissionDAO.instance.get().saveOrUpdateCommission(null, DAO.this.getList(PpsEntry.class, "id", key));
        });

        final Map<Long, Set<Long>> slot2ppsMap = new HashMap<>();
        final Table<Long, Long, Boolean> slot2ppsTable = model.getSlot2ppsTable();

        for (Table.Cell<Long, Long, Boolean> cell : slot2ppsTable.cellSet())
        {
            if (cell.getValue() != null && cell.getValue())
            {
                SafeMap.safeGet(slot2ppsMap, cell.getRowKey(), HashSet.class).add(cell.getColumnKey());
            }
        }

        for (Map.Entry<SessionDocumentSlot, BulletinMarkRow> entry : slotMap.entrySet()) {
            final SessionDocumentSlot slot = entry.getKey();
            final Set<Long> pps = slot2ppsMap.get(slot.getId());
            slot.setCommission(currentCommissionMap.get(pps));
            slot.setInSession(entry.getValue().isInSession());
            this.update(slot);
        }

        // созраняем обновления перед выставлением оценок (иначе cleanup упадет)
        this.getSession().flush();

        // сохраняем оценки для слотов
        for (Map.Entry<SessionDocumentSlot, BulletinMarkRow> entry : slotMap.entrySet()) {
            final SessionDocumentSlot slot = entry.getKey();
            final BulletinMarkRow row = entry.getValue();
            final ISessionMarkDAO.MarkData markData = row.calculateMarkData();
            final SessionMark mark = SessionMarkManager.instance().regularMarkDao().saveOrUpdateMark(slot, markData);
            updateSessionMarkAdditionalInfo(model, mark, row.getPoints());
        }
    }

    @Override
    public void prepareCurrentRatingData(Model model)
    {
        if (model.isUseCurrentRating())
        {
            final List<SessionSlotRatingData> ratingDataList = getList(SessionSlotRatingData.class, SessionSlotRatingData.slot().document(), model.getDocument());
            final Map<SessionDocumentSlot, SessionSlotRatingData> currentRatingMap = Maps.newHashMapWithExpectedSize(ratingDataList.size());
            for (SessionSlotRatingData ratingData : ratingDataList)
            {
                currentRatingMap.put(ratingData.getSlot(), ratingData);
            }

            for (BulletinMarkRow row : model.getRowList())
            {
                final SessionSlotRatingData ratingData = currentRatingMap.get(row.getSlot());
                row.setCurrentRating(ratingData == null ? "" : BrsRatingValueFormatter.instance.get().format(ratingData.getFixedCurrentRating()));
            }
        }
    }

    @SuppressWarnings("UnusedParameters")
    protected void prepareAdditionalMarkData(Model model)
    {
        // для переопределения в проектах
    }

    @SuppressWarnings("unchecked")
    protected void updateSessionMarkAdditionalInfo(Model model, SessionMark mark, Number points)
    {
        if (model.isUseCurrentRating() && mark instanceof SessionSlotRegularMark) {
            SessionMarkRatingData ratingData = get(SessionMarkRatingData.class, SessionMarkRatingData.mark(), (SessionSlotRegularMark) mark);
            if (points != null) {
                if (null == ratingData) {
                    ratingData = new SessionMarkRatingData();
                    ratingData.setMark((SessionSlotRegularMark) mark);
                }
                ratingData.setScoredPoints(points.doubleValue());
                saveOrUpdate(ratingData);
            }
            else if (null != ratingData) {
                delete(ratingData);
            }
        }
        // для переопределения в проектах
    }

    private void prepareRegElementTitle(final Model model)
    {
        final EppFControlActionType caType = model.getDocumentHolder().getProperties().getCAType();
        if (null != caType) {
            model.setRegistryElementTitle(model.getDocumentHolder().getProperties().getRegistryElementPart().getTitle() + ", " + caType.getTitle());
            model.setShowCATypeColumn(false);
            return;
        }

        final DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(SessionDocumentSlot.class, "slot")
                .where(eq(property("slot", SessionDocumentSlot.document()), value(model.getDocumentHolder().getValue())))
                .joinPath(DQLJoinType.inner, SessionDocumentSlot.studentWpeCAction().fromAlias("slot"), "wpe")
                .joinEntity("slot", DQLJoinType.inner, EppFControlActionType.class, "ca", eq(property("wpe", EppStudentWpeCAction.type()), property("ca", EppFControlActionType.eppGroupType())))
                .column(property("ca", EppFControlActionType.title()))
                .predicate(DQLPredicateType.distinct);
        final Set<String> caTypeTitles = new HashSet<>(dql.createStatement(this.getSession()).<String>list());
        model.setRegistryElementTitle(model.getDocumentHolder().getProperties().getRegistryElementPart().getTitle() + ", " + StringUtils.join(caTypeTitles, ", "));
        model.setShowCATypeColumn(caTypeTitles.size() > 1);
    }

}
