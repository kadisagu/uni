/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReport.util.results;

import org.tandemframework.core.common.ITitled;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.ResultsAdd.ISessionReportResultsDAO;

import java.util.Collection;

/**
* @author oleyba
* @since 2/13/12
*/
public class ReportRow implements ISessionReportResultsDAO.ISessionResultsReportRow
{
    private String title;
    private Collection<ISessionReportResultsDAO.ISessionResultsReportStudentData> studentData;

    public ReportRow(String title, Collection<ISessionReportResultsDAO.ISessionResultsReportStudentData> studentData)
    {
        this.title = title;
        this.studentData = studentData;
    }

    @Override public String getTitle() { return title; }
    @Override public Collection<ISessionReportResultsDAO.ISessionResultsReportStudentData> getStudents() { return studentData; }

    @Override
    public int compareTo(Object o)
    {
        if (o instanceof ITitled)
            return ITitled.TITLED_COMPARATOR.compare(this, (ITitled) o);
        return 0;
    }
}
