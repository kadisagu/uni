package ru.tandemservice.unisession.entity.document;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.sec.ISecLocalEntityOwner;
import org.tandemframework.shared.commonbase.base.util.INumberGenerationRule;
import org.tandemframework.shared.commonbase.base.util.INumberObject;
import org.tandemframework.shared.commonbase.base.util.Wiki;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unisession.entity.document.gen.SessionDocumentGen;

import java.util.Collection;

/**
 * Документ сессии
 */
public abstract class SessionDocument extends SessionDocumentGen implements INumberObject, ITitled, ISecLocalEntityOwner
{

    @Override public INumberGenerationRule getNumberGenerationRule() {
        // TODO: reimplement it
        return null;
    }

    public boolean isLocked() { return null != this.getCloseDate(); }
    public boolean isEditable() { return null == this.getCloseDate(); }

    @Override
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1048660")
    public abstract String getTitle();

    @Override
    @EntityDSLSupport(parts=SessionDocument.P_NUMBER)
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1048660")
    public abstract String getTypeTitle();

    @Override
    @EntityDSLSupport(parts=SessionDocument.P_NUMBER)
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1048660")
    public abstract String getTypeShortTitle();

    @Override
    public abstract Collection<IEntity> getSecLocalEntities();

    public abstract OrgUnit getGroupOu();
}