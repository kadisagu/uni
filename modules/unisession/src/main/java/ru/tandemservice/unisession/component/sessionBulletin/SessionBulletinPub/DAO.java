/* $*/

package ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinPub;

import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4ActionTypeRow;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unisession.dao.bulletin.ISessionBulletinDAO;
import ru.tandemservice.unisession.entity.catalog.codes.SessionMarkStateCatalogItemCodes;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;
import ru.tandemservice.unisession.entity.document.SessionDocumentPrintVersion;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.mark.SessionSlotMarkState;
import ru.tandemservice.unisession.print.ISessionBulletinPrintDAO;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.count;
/**
 * @author oleyba
 * @since 2/18/11
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setBulletin(this.getNotNull(SessionBulletinDocument.class, model.getBulletin().getId()));
        final DQLSelectBuilder markDQL = new DQLSelectBuilder()
            .fromEntity(SessionSlotMarkState.class, "m")
            .where(eq(property(SessionSlotMarkState.slot().document().fromAlias("m")), value(model.getBulletin())))
            .where(eq(property(SessionSlotMarkState.value().code().fromAlias("m")), value(SessionMarkStateCatalogItemCodes.NOT_APPEAR_UNKNOWN)))
            .column(count("m.id"));
        if (markDQL.createStatement(this.getSession()).<Number>uniqueResult().longValue()>0) {
            model.setWithUnknownMarks(true);
        } else {
            model.setWithUnknownMarks(false);
        }
        model.setCanEditThemes(ISessionBulletinDAO.instance.get().isBulletinRequireTheme(model.getBulletin()));

        model.setTutors(CommonBaseUtil.<PpsEntry>getPropertiesList(
                this.getList(SessionComissionPps.class, SessionComissionPps.commission().s(), model.getBulletin().getCommission()),
                SessionComissionPps.pps().s()));

        model.setEppRealEduGroupTitles(getStudentGroupTitleList(model));

        model.setStudentGroupList(
                new DQLSelectBuilder()
                        .fromEntity(Group.class, "g")
                        .column(property("g"))
                        .where(exists(SessionDocumentSlot.class,
                                      SessionDocumentSlot.document().s(), model.getBulletin(),
                                      SessionDocumentSlot.actualStudent().group().s(), property("g")))
                        .order(property("g", Group.title()))
                        .createStatement(getSession()).list()
        );

        boolean hasEduGroupRow = ISharedBaseDao.instance.get().existsEntity(EppRealEduGroupRow.class,
                                                                            EppRealEduGroupRow.group().id().s(), model.getBulletin().getGroup().getId(),
                                                                            EppRealEduGroupRow.removalDate().s(), null
        );
        model.setHasEduGroupRow(hasEduGroupRow);
    }

    private String getStudentGroupTitleList(Model model)
    {
        DQLSelectBuilder slotDQL = new DQLSelectBuilder()
                .fromEntity(SessionDocumentSlot.class, "slot")
                .column(property("slot", SessionDocumentSlot.studentWpeCAction().id()))
                .where(eq(property("slot", SessionDocumentSlot.document()), value(model.getBulletin())));

        return new DQLSelectBuilder()
                .fromEntity(EppRealEduGroupRow.class, "eg")
                .column(property("eg", EppRealEduGroupRow.studentGroupTitle()))
                .where(in(property("eg", EppRealEduGroupRow.studentWpePart().id()), slotDQL.buildQuery()))
                .order(property("eg", EppRealEduGroupRow.studentGroupTitle()))
                .distinct()
                .createStatement(getSession()).<String>list()
                .stream().collect(Collectors.joining(", "));
    }

    @Override
    public void prepareCheckDataSource(final Model model)
    {
        final DQLSelectBuilder bulletinDQL = new DQLSelectBuilder()
            .fromEntity(SessionDocumentSlot.class, "slot")
            .column(property(SessionDocumentSlot.studentWpeCAction().studentWpe().fromAlias("slot")))
            .where(eq(property(SessionDocumentSlot.document().fromAlias("slot")), value(model.getBulletin())));

        final Set<EppStudentWorkPlanElement> bulletinStudents = new HashSet<>(bulletinDQL.createStatement(this.getSession()).<EppStudentWorkPlanElement>list());

        final DQLSelectBuilder groupDQL = new DQLSelectBuilder()
            .fromEntity(EppRealEduGroup4ActionTypeRow.class, "row")
            .column(property(EppRealEduGroup4ActionTypeRow.studentWpePart().studentWpe().fromAlias("row")))
            .where(eq(property(EppRealEduGroup4ActionTypeRow.group().fromAlias("row")), value(model.getBulletin().getGroup())))
            .where(isNull(property(EppRealEduGroup4ActionTypeRow.removalDate().fromAlias("row"))));

        final Set<EppStudentWorkPlanElement> groupStudents = new HashSet<>(groupDQL.createStatement(this.getSession()).<EppStudentWorkPlanElement>list());

        final Set<EppStudentWorkPlanElement> students = new HashSet<>();
        students.addAll(groupStudents);
        students.addAll(bulletinStudents);

        final List<EppStudentWorkPlanElement> studentList = new ArrayList<>(students);
        Collections.sort(studentList, EppStudentWorkPlanElement.FULL_FIO_COMPARATOR);

        final DynamicListDataSource<EppStudentWorkPlanElement> dataSource = model.getCheckDataSource();
        UniBaseUtils.createFullPage(dataSource, studentList);

        for (final ViewWrapper<EppStudentWorkPlanElement> wrapper : ViewWrapper.<EppStudentWorkPlanElement>getPatchedList(dataSource))
        {
            wrapper.setViewProperty(IDAO.V_BULLETIN, bulletinStudents.contains(wrapper.getEntity()));
            wrapper.setViewProperty(IDAO.V_GROUP, groupStudents.contains(wrapper.getEntity()));
        }
    }

    @Override
    public void printBulletin(final Model model)
    {
        final SessionBulletinDocument bulletin = model.getBulletin();
        if (bulletin.isClosed())
        {
            final SessionDocumentPrintVersion rel = this.get(SessionDocumentPrintVersion.class, SessionDocumentPrintVersion.doc().id().s(), bulletin.getId());
            if (null != rel)
            {
                final byte[] content = rel.getContent();
                BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("Ведомость.rtf").document(content), true);
                return;
            }
        }
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("Ведомость.rtf").document(ISessionBulletinPrintDAO.instance.get().printBulletin(model.getBulletin().getId())), true);
    }

    @Override
    public void doOpenBulletin(final Model model)
    {
        model.getBulletin().setCloseDate(null);
        this.update(model.getBulletin());
    }

    @Override
    public void doCloseBulletin(final Model model)
    {
        final DQLSelectBuilder emptySlotDQL = new DQLSelectBuilder()
        .fromEntity(SessionDocumentSlot.class, "slot")
        .column("slot.id")
        .where(eq(property(SessionDocumentSlot.document().fromAlias("slot")), value(model.getBulletin())))
        .where(notExists(new DQLSelectBuilder()
        .fromEntity(SessionMark.class, "m")
        .column("m.id")
        .where(eq(property(SessionMark.slot().id().fromAlias("m")), property("slot.id")))
        .buildQuery()));
        final Number count = emptySlotDQL.createCountStatement(new DQLExecutionContext(this.getSession())).uniqueResult();
        if (count != null && count.intValue() > 0) {
            throw new ApplicationException("Нельзя закрыть ведомость, так как не всем студентам в ведомости выставлены оценки.");
        }
        final DQLSelectBuilder markDQL = new DQLSelectBuilder()
        .fromEntity(SessionSlotMarkState.class, "m")
        .where(eq(property(SessionSlotMarkState.slot().document().fromAlias("m")), value(model.getBulletin())))
        .where(eq(property(SessionSlotMarkState.value().code().fromAlias("m")), value(SessionMarkStateCatalogItemCodes.NOT_APPEAR_UNKNOWN)))
        .column(count("m.id"));
        if (markDQL.createStatement(this.getSession()).<Number>uniqueResult().longValue()>0) {
            throw new ApplicationException("Нельзя закрыть ведомость, так как она содержит временные оценки. Укажите причины неявок студентов.");
        }

        //сохраним печатную форму
        //проверяем существование печатной формы
        SessionDocumentPrintVersion rel = this.get(SessionDocumentPrintVersion.class, SessionDocumentPrintVersion.L_DOC, model.getBulletin());
        if (rel == null)
        {
            //если ее нет - создаем
            rel = new SessionDocumentPrintVersion();
            rel.setDoc(model.getBulletin());
        }
        final RtfDocument document = ISessionBulletinPrintDAO.instance.get().printBulletin(model.getBulletin().getId());
        rel.setContent(RtfUtil.toByteArray(document));
        this.getSession().saveOrUpdate(rel);
        model.getBulletin().setCloseDate(new Date());
        this.update(model.getBulletin());
    }
}
