/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unisession.component.settings.CompleteLevelReadyForFinalCA;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel;

/**
 * @author iolshvang
 * @since 30.05.11 12:05
 */
public class Model
{
    private DynamicListDataSource<EppRealEduGroupCompleteLevel> _dataSource;

    private boolean _isFormAnyStates;

    public DynamicListDataSource<EppRealEduGroupCompleteLevel> getDataSource()
    {
        return this._dataSource;
    }

    public void setDataSource(final DynamicListDataSource<EppRealEduGroupCompleteLevel> dataSource)
    {
        this._dataSource = dataSource;
    }

    public boolean isFormAnyStates() {
        return _isFormAnyStates;
    }

    public void setFormAnyStates(boolean isFormAnyStates) {
        _isFormAnyStates = isFormAnyStates;
    }
}
