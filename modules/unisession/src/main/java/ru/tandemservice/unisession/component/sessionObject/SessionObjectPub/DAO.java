/* $*/

package ru.tandemservice.unisession.component.sessionObject.SessionObjectPub;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import java.util.Date;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unisession.entity.allowance.ISessionStudentNotAllowed;
import ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowed;
import ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowedForBulletin;
import ru.tandemservice.unisession.entity.document.SessionObject;

/**
 * @author oleyba
 * @since 3/24/11
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        final Long id = model.getSession().getId();
        model.setSession(this.getNotNull(SessionObject.class, id));
    }

    @Override
    public void doUnlock(final Long id)
    {
        final ISessionStudentNotAllowed allowance = this.get(id);
        allowance.setRemovalDate(new Date());
        this.update(allowance);
    }

    @Override
    public void prepareBulletinDataSource(final Model model)
    {
        final DynamicListDataSource<SessionStudentNotAllowedForBulletin> dataSource = model.getBulletinDataSource();
        final DQLSelectBuilder dql = new DQLSelectBuilder()
        .fromEntity(SessionStudentNotAllowedForBulletin.class, "a")
        .column("a")
        .where(eq(property(SessionStudentNotAllowedForBulletin.bulletin().sessionObject().fromAlias("a")), value(model.getSession())));
        new DQLOrderDescriptionRegistry(SessionStudentNotAllowedForBulletin.class, "a").applyOrder(dql, dataSource.getEntityOrder());
        UniBaseUtils.createPage(dataSource, dql, this.getSession());
    }

    @Override
    public void prepareSessionDataSource(final Model model)
    {
        final DynamicListDataSource<SessionStudentNotAllowed> dataSource = model.getSessionDataSource();
        final DQLSelectBuilder dql = new DQLSelectBuilder()
        .fromEntity(SessionStudentNotAllowed.class, "a")
        .column("a")
        .where(eq(property(SessionStudentNotAllowed.session().fromAlias("a")), value(model.getSession())));
        new DQLOrderDescriptionRegistry(SessionStudentNotAllowed.class, "a").applyOrder(dql, dataSource.getEntityOrder());
        UniBaseUtils.createPage(dataSource, dql, this.getSession());
    }
}
