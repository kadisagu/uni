/**
 *$Id:$
 */
package ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.ResultAdd.util;

import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlot;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Alexander Shaburov
 * @since 15.11.12
 */
public class AttestationReportResultRowWrapper
{
    public AttestationReportResultRowWrapper(Student student)
    {
        _student = student;
    }

    private Student _student;
    private Map<Long, SessionAttestationSlot> _disciplineAttSlotMap = new HashMap<Long, SessionAttestationSlot>();
    private Map<Long, EppStudentWorkPlanElement> _disciplineEpvSlotMap = new HashMap<Long, EppStudentWorkPlanElement>();


    // Calculate

    public Integer getTotalPassed()
    {
        int totalPassed = 0;

        for (Map.Entry<Long, SessionAttestationSlot> entry : _disciplineAttSlotMap.entrySet())
            if (entry.getValue() != null && entry.getValue().getMark() != null && entry.getValue().getMark().isPositive())
                totalPassed++;

        return totalPassed;
    }

    public Integer getTotalUnpassed()
    {
        int totalUnPassed = 0;

        for (Map.Entry<Long, SessionAttestationSlot> entry : _disciplineAttSlotMap.entrySet())
            if (entry.getValue() != null && entry.getValue().getMark() != null && !entry.getValue().getMark().isPositive())
                totalUnPassed++;

        return totalUnPassed;
    }

    public Group getGroup()
    {
        return _student.getGroup();
    }

    public String getFio()
    {
        return _student.getPerson().getFullFio();
    }

    // Getters & Setters

    public Student getStudent()
    {
        return _student;
    }

    public void setStudent(Student student)
    {
        _student = student;
    }

    public Map<Long, SessionAttestationSlot> getDisciplineAttSlotMap()
    {
        return _disciplineAttSlotMap;
    }

    public Map<Long, EppStudentWorkPlanElement> getDisciplineEpvSlotMap()
    {
        return _disciplineEpvSlotMap;
    }
}
