/* $Id$ */
package ru.tandemservice.unisession.component.orgunit.SessionRetakeDocTab;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.meta.entity.access.IEntityAccessSemaphore;
import org.tandemframework.core.util.NumberAsStringComparator;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.gen.OrgUnitGen;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.tapsupport.component.selection.CommonMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniedu.base.bo.EducationYear.utils.EducationYearModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.gen.EppRegistryElementGen;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.uniepp.ui.EppRegistryElementSelectModel;
import ru.tandemservice.unisession.base.bo.SessionMark.daemon.ISessionMarkDaemonBean;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionDocumentPrintVersion;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionObject;
import ru.tandemservice.unisession.entity.document.SessionRetakeDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.print.ISessionRetakeDocPrintDAO;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.count;
import static org.tandemframework.hibsupport.dql.DQLFunctions.upper;

/**
 * @author oleyba
 * @since 6/13/11
 */
public class DAO extends UniBaseDao implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.getOrgUnitHolder().refresh();
        model.setSec(new OrgUnitSecModel(model.getOrgUnit()));
        model.setYearModel(new EducationYearModel()
        {
            @Override
            protected DQLSelectBuilder query(final String alias, final String filter)
            {
                final DQLSelectBuilder sessionObjectExists = new DQLSelectBuilder()
                .fromEntity(SessionObject.class, alias)
                .column(property(SessionObject.educationYear().id().fromAlias(alias)))
                .where(eq(property(SessionObject.orgUnit().fromAlias(alias)), value(model.getOrgUnit())));

                final DQLSelectBuilder dql = new DQLSelectBuilder();
                dql.fromEntity(EducationYear.class, alias);
                dql.where(in(property(EducationYear.id().fromAlias(alias)), sessionObjectExists.buildQuery()));
                dql.order(property(EducationYear.intValue().fromAlias(alias)));
                return dql;
            }
        });

        final DQLSelectBuilder partsDQL = new DQLSelectBuilder()
        .fromEntity(SessionObject.class, "obj")
        .column(property(SessionObject.yearDistributionPart().fromAlias("obj")))
        .where(eq(property(SessionObject.orgUnit().fromAlias("obj")), value(model.getOrgUnit())))
        .order(property(SessionObject.yearDistributionPart().code().fromAlias("obj")));
        if (model.getYear() != null) {
            partsDQL.where(eq(property(SessionObject.educationYear().fromAlias("obj")), value(model.getYear())));
        }
        final Set<YearDistributionPart> parts = new LinkedHashSet<>(partsDQL.createStatement(this.getSession()).<YearDistributionPart>list());
        model.setPartsList(HierarchyUtil.listHierarchyNodesWithParents(new ArrayList<>(parts), false));

        model.setBullStatusModel(new LazySimpleSelectModel<>(ImmutableList.of(
                new DataWrapper(Model.BULL_STATUS_EMPTY_ID, "Незаполненная"),
                new DataWrapper(Model.BULL_STATUS_OPEN_ID, "Открытая"),
                new DataWrapper(Model.BULL_STATUS_CLOSE_ID, "Закрытая")
        )));

        model.setRegistryOwnerModel(new DQLFullCheckSelectModel("fullTitle")
        {
            @Override
            protected DQLSelectBuilder query(final String alias, final String filter)
            {
                final DQLSelectBuilder s = DAO.this.getDocBaseDQL(model);
                s.column(property(SessionRetakeDocument.registryElementPart().registryElement().owner().id().fromAlias("doc")));

                final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(OrgUnit.class, alias);
                dql.where(in(property(alias, "id"), s.buildQuery()));
                if (null != filter)
                {
                    dql.where(or(
                            like(OrgUnit.title().fromAlias(alias), filter),
                            like(OrgUnit.shortTitle().fromAlias(alias), filter)
                    ));
                }

                dql.order(property(OrgUnitGen.shortTitle().fromAlias(alias)));
                dql.order(property(OrgUnitGen.title().fromAlias(alias)));
                return dql;
            }
        });

        model.setRegistryStructureModel(new CommonMultiSelectModel()
        {
            {
                setHierarchical(true);
            }

            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppRegistryStructure.class, "b").column(property("b"))
                        .where(likeUpper(property(EppRegistryStructure.title().fromAlias("b")), value(CoreStringUtils.escapeLike(filter, true))))
                        .order(property(EppRegistryStructure.title().fromAlias("b")));

                if (set != null)
                    builder.where(in(property(EppRegistryStructure.id().fromAlias("b")), set));

                return new DQLListResultBuilder(builder, 50);
            }
        });

        model.setCourseCurrentModel(new CommonMultiSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Course.class, "b").column(property("b"))
                        .where(likeUpper(property(Course.title().fromAlias("b")), value(CoreStringUtils.escapeLike(filter, true))))
                        .order(property(Course.title().fromAlias("b")));

                if (set != null)
                    builder.where(in(property(Course.id().fromAlias("b")), set));

                return new DQLListResultBuilder(builder, 50);
            }
        });

        model.setRegistryElementModel(new EppRegistryElementSelectModel<EppRegistryElement>(EppRegistryElement.class)
                {
            @Override
            protected DQLSelectBuilder query(final String alias, final String filter)
            {
                final DQLSelectBuilder s = DAO.this.getDocBaseDQL(model);
                s.column(property(SessionRetakeDocument.registryElementPart().registryElement().id().fromAlias("doc")));

                final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppRegistryElement.class, alias);
                dql.where(in(property(alias, "id"), s.buildQuery()));

                if (model.getRegistryOwners() != null)
                {
                    dql.where(in(property(EppRegistryElement.owner().fromAlias(alias)), model.getRegistryOwners()));
                }

                if (model.getRegistryStructureList() != null)
                {
                    dql.where(in(property(EppRegistryElement.parent().fromAlias(alias)), model.getRegistryStructureList()));
                }

                if (null != filter) {
                    dql.where(this.getFilterCondition(alias, filter));
                }

                dql.order(property(EppRegistryElementGen.title().fromAlias(alias)));
                dql.order(property(EppRegistryElementGen.owner().title().fromAlias(alias)));
                return dql;
            }
                });

        model.setGroupCurrentModel(new DQLFullCheckSelectModel()
        {
            @Override
            protected DQLSelectBuilder query(final String alias, final String filter)
            {
                final DQLSelectBuilder docDQL = DAO.this.getDocBaseDQL(model);
                docDQL.column(property(SessionRetakeDocument.id().fromAlias("doc")));

                final DQLSelectBuilder slotDQl = new DQLSelectBuilder()
                .fromEntity(SessionDocumentSlot.class, "slot")
                .where(in(property(SessionDocumentSlot.document().id().fromAlias("slot")), docDQL.buildQuery()))
                .column(property(SessionDocumentSlot.actualStudent().group().id().fromAlias("slot")));

                DQLSelectBuilder dql = new DQLSelectBuilder()
                    .fromEntity(Group.class, alias)
                    .where(in(property(alias + ".id"), slotDQl.buildQuery()))
                    .order(property(Group.title().fromAlias(alias)));

                if (model.getCourseCurrentList() != null)
                    dql.where(in(property(Group.course().fromAlias(alias)), model.getCourseCurrentList()));

                FilterUtils.applySimpleLikeFilter(dql, alias, Group.title(), filter);

                return dql;
            }
        });
    }

    @Override
    @SuppressWarnings("unchecked")
    public void refreshDataSource(final Model model)
    {
        final DynamicListDataSource<SessionRetakeDocument> dataSource = model.getDataSource();
        final EntityOrder entityOrder = dataSource.getEntityOrder();

        final DQLSelectBuilder docDQL = new DQLSelectBuilder().fromEntity(SessionRetakeDocument.class, "doc");
        if (getSessionObject(model) == null) {
            // если фильтры сброшены - показываем только с текущего подразделения
            docDQL.where(eq(property(SessionRetakeDocument.sessionObject().orgUnit().fromAlias("doc")), value(model.getOrgUnit())));
        } else {
            docDQL.where(eq(property(SessionRetakeDocument.sessionObject().fromAlias("doc")), value(getSessionObject(model))));
        }

        // Если по студенту добавятся фильтры - не забываем условие сюда дописать
        final boolean needJoinStudent = model.getGroupCurrentList() != null || model.getCourseCurrentList() != null || model.getStudentLastName() != null
                || "groups".equals(entityOrder.getKey());
        if (needJoinStudent) {
            docDQL.joinEntity("doc", DQLJoinType.inner, SessionDocumentSlot.class, "s", eq(
                    property(SessionDocumentSlot.document().fromAlias("s")), property("doc")
            ));
            docDQL.joinPath(DQLJoinType.inner, SessionDocumentSlot.actualStudent().fromAlias("s"), "st");
        }

        // фильтры
        if (model.getRegistryElements() != null) {
            docDQL.where(in(property(SessionRetakeDocument.registryElementPart().registryElement().fromAlias("doc")), model.getRegistryElements()));
        }
        if (model.getGroupCurrentList() != null) {
            if (!needJoinStudent) { throw new IllegalStateException(); }
            docDQL.where(in(property("st", Student.group()), model.getGroupCurrentList()));
        }
        if (model.getNumber() != null) {
            docDQL.where(eq(property(SessionRetakeDocument.number().fromAlias("doc")), value(model.getNumber())));
        }
        if (model.getBullStatus() != null)
        {
            if (model.getBullStatus().getId().equals(Model.BULL_STATUS_EMPTY_ID))
            {
                DQLSelectBuilder slotBuilder = new DQLSelectBuilder().fromEntity(SessionDocumentSlot.class, "slot").column(property(SessionDocumentSlot.id().fromAlias("slot")))
                        .where(eq(property(SessionDocumentSlot.document().id().fromAlias("slot")), property(SessionRetakeDocument.id().fromAlias("doc"))))
                        .where(notExists(
                                new DQLSelectBuilder().fromEntity(SessionMark.class, "mark").column(property(SessionMark.id().fromAlias("mark")))
                                        .where(eq(property(SessionDocumentSlot.id().fromAlias("slot")), property(SessionMark.slot().id().fromAlias("mark"))))
                                        .buildQuery()
                        ));
                docDQL.where(exists(slotBuilder.buildQuery()));
            }
            else if (model.getBullStatus().getId().equals(Model.BULL_STATUS_OPEN_ID))
            {
                docDQL.where(isNull(property(SessionRetakeDocument.closeDate().fromAlias("doc"))));
            }
            else if (model.getBullStatus().getId().equals(Model.BULL_STATUS_CLOSE_ID))
            {
                docDQL.where(isNotNull(property(SessionRetakeDocument.closeDate().fromAlias("doc"))));
            }
        }
        if (model.getRegistryOwners() != null)
        {
            docDQL.where(in(property(SessionRetakeDocument.registryElementPart().registryElement().owner().fromAlias("doc")), model.getRegistryOwners()));
        }
        if (model.getRegistryStructureList() != null)
        {
            docDQL.where(in(property(SessionRetakeDocument.registryElementPart().registryElement().parent().fromAlias("doc")), model.getRegistryStructureList()));
        }
        if (model.getCourseCurrentList() != null) {
            if (!needJoinStudent) { throw new IllegalStateException(); }
            docDQL.where(in(property("st", Student.course()), model.getCourseCurrentList()));
        }
        if (model.getPpsLastName() != null)
        {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SessionComissionPps.class, "pps").column(property(SessionComissionPps.id().fromAlias("pps")))
                    .where(eq(property(SessionComissionPps.commission().id().fromAlias("pps")), property(SessionRetakeDocument.commission().id().fromAlias("doc"))))
                    .where(like(upper(property(SessionComissionPps.pps().person().identityCard().lastName().fromAlias("pps"))), value(CoreStringUtils.escapeLike(model.getPpsLastName(), true))));

            docDQL.where(exists(builder.buildQuery()));
        }

        if (model.getStudentLastName() != null) {
            if (!needJoinStudent) { throw new IllegalStateException(); }
            docDQL.where(likeUpper(property("st", Student.person().identityCard().lastName()), value(CoreStringUtils.escapeLike(model.getStudentLastName(), true))));
        }

        final Number stCountFrom = model.getSettings().get("stCountFrom");
        final Number stCountTo = model.getSettings().get("stCountTo");

        if (null != stCountFrom)
        {
            docDQL.where(ge(
                    new DQLSelectBuilder()
                    .fromEntity(SessionDocumentSlot.class, "slot_count")
                    .where(eq(property(SessionDocumentSlot.document().id().fromAlias("slot_count")), property(SessionRetakeDocument.id().fromAlias("doc"))))
                    .column("count(slot_count.id)")
                    .buildQuery(),
                    commonValue(stCountFrom)));
        }
        if (null != stCountTo)
        {
            docDQL.where(le(
                    new DQLSelectBuilder()
                    .fromEntity(SessionDocumentSlot.class, "slot_count")
                    .where(eq(property(SessionDocumentSlot.document().id().fromAlias("slot_count")), property(SessionRetakeDocument.id().fromAlias("doc"))))
                    .column("count(slot_count.id)")
                    .buildQuery(),
                    commonValue(stCountTo)));
        }

        // сортировки
        if ("titleWithCA".equals(entityOrder.getKey()))
        {
            docDQL.column(property("doc.id"));
            docDQL.column(property("doc.number"));
            docDQL.predicate(DQLPredicateType.distinct);
            final List rows = docDQL.createStatement(this.getSession()).list();
            Collections.sort(rows, new Comparator<Object[]>() { @Override public int compare(final Object[] o1, final Object[] o2) { return NumberAsStringComparator.INSTANCE.compare((String) o1[1], (String) o2[1]); }});
            CollectionUtils.transform(rows, input -> ((Object[]) input)[0]);
            this.createPage(dataSource, rows);
        } else if ("groups".equals(entityOrder.getKey()))
        {
            if (!needJoinStudent) { throw new IllegalStateException(); }
            docDQL.column(property("doc.id"));
            docDQL.column(property("st", Student.group().title()));
            docDQL.predicate(DQLPredicateType.distinct);
            final List<Long> sortedIds = this.getSortedIds(docDQL.createStatement(this.getSession()).<Object[]>list(), 1);
            Set<Long> uniqueIds = new LinkedHashSet<>(sortedIds);
            sortedIds.clear(); sortedIds.addAll(uniqueIds);
            this.createPage(dataSource, sortedIds);
        } else
        {
            final DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(SessionRetakeDocument.class, "rd")
            .where(in("rd.id", docDQL.column("doc.id").buildQuery()));
            new DQLOrderDescriptionRegistry(SessionRetakeDocument.class, "rd").applyOrder(dql, entityOrder);
            UniBaseUtils.createPage(dataSource, dql, this.getSession());
        }

        // view properties
        for (List<ViewWrapper<SessionRetakeDocument>> documentList : Lists.partition(ViewWrapper.<SessionRetakeDocument>getPatchedList(dataSource), DQL.MAX_VALUES_ROW_NUMBER)) {
            final Collection<Long> documentIds = UniBaseUtils.getIdList(documentList);
            final Map<Long, Set<String>> groupMap = new HashMap<>();
            {
                final DQLSelectBuilder dql = new DQLSelectBuilder();
                dql.fromEntity(SessionDocumentSlot.class, "slot");
                dql.column(DQLExpressions.property(SessionDocumentSlot.document().id().fromAlias("slot")));
                dql.column(DQLExpressions.property(SessionDocumentSlot.actualStudent().group().title().fromAlias("slot")));
                dql.where(DQLExpressions.in(property(SessionDocumentSlot.document().id().fromAlias("slot")), documentIds));
                dql.order(DQLExpressions.property(SessionDocumentSlot.actualStudent().group().title().fromAlias("slot")));

                for (final Object[] row : UniBaseDao.scrollRows(dql.createStatement(DAO.this.getSession()))) {
                    SafeMap.safeGet(groupMap, (Long) row[0], LinkedHashSet.class).add((String) row[1]);
                }
            }

            final Map<Long, List<String>> comissionMap = new HashMap<>(documentIds.size());
            {
                final DQLSelectBuilder dql = new DQLSelectBuilder();
                dql.fromEntity(SessionComissionPps.class, "pps");
                dql.joinEntity("pps", DQLJoinType.inner, SessionRetakeDocument.class, "doc", DQLExpressions.eq(
                        DQLExpressions.property(SessionComissionPps.commission().id().fromAlias("pps")),
                        DQLExpressions.property(SessionRetakeDocument.commission().id().fromAlias("doc"))
                ));
                dql.column(DQLExpressions.property(SessionRetakeDocument.id().fromAlias("doc")));
                dql.column(DQLExpressions.property(SessionComissionPps.pps().person().identityCard().fromAlias("pps")));
                dql.where(DQLExpressions.in(DQLExpressions.property(SessionRetakeDocument.id().fromAlias("doc")), documentIds));
                dql.order(DQLExpressions.property(SessionComissionPps.pps().person().identityCard().fullFio().fromAlias("pps")));

                for (final Object[] row : UniBaseDao.scrollRows(dql.createStatement(DAO.this.getSession()))) {
                    SafeMap.safeGet(comissionMap, (Long) row[0], ArrayList.class).add(((IdentityCard) row[1]).getFio());
                }
            }

            final Map<Long, Number> studentCountMap = new HashMap<>();
            {
                final DQLSelectBuilder dql = new DQLSelectBuilder();
                dql.fromEntity(SessionDocumentSlot.class, "slot");
                dql.where(DQLExpressions.in(property(SessionDocumentSlot.document().id().fromAlias("slot")), documentIds));
                dql.group(property(SessionDocumentSlot.document().id().fromAlias("slot")));
                dql.column(property(SessionDocumentSlot.document().id().fromAlias("slot")));
                dql.column(count(SessionDocumentSlot.id().fromAlias("slot")));

                for (final Object[] row : UniBaseDao.scrollRows(dql.createStatement(DAO.this.getSession()))) {
                    studentCountMap.put((Long) row[0], (Number) row[1]);
                }
            }

            final Map<Long, Set<String>> caTitleMap = new HashMap<>();
            {
                final DQLSelectBuilder dql = new DQLSelectBuilder();
                dql.fromEntity(SessionDocumentSlot.class, "slot");
                dql.where(DQLExpressions.in(property(SessionDocumentSlot.document().id().fromAlias("slot")), documentIds));
                dql.joinPath(DQLJoinType.inner, SessionDocumentSlot.studentWpeCAction().fromAlias("slot"), "wpca");
                dql.joinEntity("wpca", DQLJoinType.inner, EppFControlActionType.class, "ca", eq(property("wpca", EppStudentWpeCAction.type()), property("ca", EppFControlActionType.eppGroupType())));
                dql.column(property(SessionDocumentSlot.document().id().fromAlias("slot")));
                dql.column(property(EppFControlActionType.title().fromAlias("ca")));
                dql.order(property(EppFControlActionType.title().fromAlias("ca")));

                for (final Object[] row : UniBaseDao.scrollRows(dql.createStatement(DAO.this.getSession()))) {
                    SafeMap.safeGet(caTitleMap, (Long) row[0], LinkedHashSet.class).add(StringUtils.uncapitalize((String) row[1]));
                }
            }

            for (final ViewWrapper<SessionRetakeDocument> bulletin : documentList)
            {
                final Long id = bulletin.getId();
                bulletin.setViewProperty("studentCount", studentCountMap.get(id));
                bulletin.setViewProperty("groups", StringUtils.join(groupMap.get(id), '\n'));
                bulletin.setViewProperty("comission", StringUtils.join(comissionMap.get(id), '\n'));
                bulletin.setViewProperty("titleWithCA", bulletin.getEntity().getTitle() + ", " + StringUtils.join(caTitleMap.get(id), '\n'));
            }
        }
    }

    @SuppressWarnings("unchecked")
    private void createPage(final DynamicListDataSource<SessionRetakeDocument> dataSource, List rows)
    {
        if (dataSource.getEntityOrder().getDirection() == OrderDirection.desc) {
            Collections.reverse(rows);
        }

        dataSource.setTotalSize(rows.size());
        final int startRow = (int) dataSource.getStartRow();
        final int countRow = (int) dataSource.getCountRow();
        rows = rows.subList(startRow, Math.min(startRow + countRow, rows.size()));
        dataSource.createPage(sort(this.getList(SessionRetakeDocument.class, "id", rows), rows));
    }

    private DQLSelectBuilder getDocBaseDQL(final Model model)
    {
        final DQLSelectBuilder rootDql = new DQLSelectBuilder().fromEntity(SessionRetakeDocument.class, "doc");
        if (getSessionObject(model) != null) {
            rootDql.where(eq(property(SessionRetakeDocument.sessionObject().fromAlias("doc")), value(getSessionObject(model))));
        }
        else {
            rootDql.where(isNotNull("doc.id"));
        }
        return rootDql;
    }

    @Override
    public SessionObject getSessionObject(Model model)
    {
        final DQLSelectBuilder dql = new DQLSelectBuilder()
        .fromEntity(SessionObject.class, "s")
        .where(eq(property(SessionObject.orgUnit().fromAlias("s")), value(model.getOrgUnit())));
        if (null != model.getYear() && null != model.getPart()) {
            dql.where(eq(property(SessionObject.educationYear().fromAlias("s")), value(model.getYear())));
            dql.where(eq(property(SessionObject.yearDistributionPart().fromAlias("s")), value(model.getPart())));
        } else {
            dql.where(isNull(property(SessionObject.id().fromAlias("s"))));
        }
        dql.order(property(SessionObject.startupDate().fromAlias("s")));
        return dql.createStatement(getSession()).uniqueResult();
    }
    
    @Override
    public void doPrintRetakeDoc(final Long id)
    {
        final SessionRetakeDocument bulletin = this.get(SessionRetakeDocument.class, id);
        if (bulletin.isClosed())
        {
            final SessionDocumentPrintVersion rel = this.get(SessionDocumentPrintVersion.class, SessionDocumentPrintVersion.doc().id().s(), id);
            if (null != rel)
            {
                final byte[] content = rel.getContent();
                BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().document(content).fileName("Ведомость.rtf"), true);
                return;
            }
        }
        final RtfDocument document = ISessionRetakeDocPrintDAO.instance.get().printDoc(id);
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("Ведомость.rtf").document(document), true);
    }

    @Override
    public void doRefreshFinalMarks() {

        try( IEntityAccessSemaphore semaphore = CoreServices.entityAccessService().createSemaphore() )
        {
            // разрешаем изменения
            semaphore.allowUpdate(SessionMark.class, true);
            semaphore.allowInsert(SessionMark.class, true);

            ISessionMarkDaemonBean dao = ISessionMarkDaemonBean.instance.get();
            dao.doRefreshStudentGradeBookSlots();
            dao.doRefreshStudentFinalMarks(false);

            getSession().flush();
        }
    }
}
