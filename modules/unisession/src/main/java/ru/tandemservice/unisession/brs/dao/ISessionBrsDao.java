/* $Id:$ */
package ru.tandemservice.unisession.brs.dao;

import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.shared.commonbase.base.util.Wiki;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionGroup;
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.attestation.entity.SessionAttestation;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin;
import ru.tandemservice.unisession.base.bo.SessionMark.logic.ISessionMarkDAO;
import ru.tandemservice.unisession.entity.document.SessionDocument;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;

import java.util.Collection;
import java.util.Date;
import java.util.Map;

/**
 * Коннектор БРС к сессии:
 * осуществляет доступ к скрипту расчета рейтинга и коэффициентам настройки рейтинга,
 * в случае, если подключен модуль «Журналы преподавателей».
 *
 * Если модуль не подключен, дефолтная реализация обеспечивает работу без БРС,
 * т.е. в режиме выставления традиционных оценок.
 *
 * @author oleyba
 * @since 10/9/12
 */
@Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=9865671")
public interface ISessionBrsDao
{
    SpringBeanCache<ISessionBrsDao> instance = new SpringBeanCache<ISessionBrsDao>(ISessionBrsDao.class.getName());

    /**
     * Глобальные настройки БРС
     */
    ISessionBrsSettings getSettings();

    void saveSettings(ISessionBrsSettings settingsWrapper);

    /**
     * Рассчитывать признак аттестации студента в ведомости на основе текущего рейтинга через скрипт расчета рейтинга.
     * @param bulletin аттестационная ведомость
     * @return да/нет
     */
    boolean isUseCurrentRating(SessionAttestationBulletin bulletin);

    /**
     * Рассчитывать признак аттестации студента в ведомости на основе текущего рейтинга через скрипт расчета рейтинга
     * для ведомостей данной аттестации.
     *
     * Может использоваться для проверки доступности массовых операций,
     * с последующей перепроверкой каждой ведомости индивидуальным методом.
     *
     * @param attestation аттестация
     * @return да/нет
     */
    boolean isUseCurrentRating(SessionAttestation attestation);

    /**
     * Метод заполняет отметку о прохождении аттестации на основании текущего рейтинга для всех студентов в ведомости,
     * рассчитывает при этом и сам текущий рейтинг и фиксирует его в документе.
     *
     * Используется скрипт расчета рейтинга.
     *
     * @param bulletin ведомость
     */
    void doFillAttestationMarkBasedOnCurrentRating(SessionAttestationBulletin bulletin);

    /**
     * Метод рассчитывает текущий рейтинг по дисциплине документа для студентов в документе, и сохраняет его.
     * В случае невозможности рассчитать рейтинг метод выдает пользовательское исключение с сообщением о причине.
     *
     * @param document документ сессии
     */
    void saveCurrentRating(SessionDocument document);

    /**
     * Метод рассчитывает текущий рейтинг по дисциплине документа для студентов в документе, и сохраняет его.
     * Добавлен параметр silent:
     * для silent=true метод игнорирует студентов, для которых не получилось рассчитать текущий рейтинг,
     * не обновляя для них данные, т.е. если текущий рейтинг уже был для них сохранен, то эти данные не будут стерты.
     *
     * @param document документ сессии
     * @param silent игнорировать ошибки при расчете рейтинга, пропуская таких студентов
     */
    void saveCurrentRating(SessionDocument document, boolean silent);

    /**
     * Вычисляет оценку по традиционной шкале и ее рейтинговый эквивалент для переданной записи в документе сессии.
     * 
     * Работает в соответствии со скриптом расчета рейтинга.
     * 
     * Если режим работы с документом, настроенный в скрипте, предполагает использование тек. рейтинга, то:
     * 1) если в документе сохранен тек. рейтинг, вычисление использует его.
     * 2) если текущий рейтинг не был зафиксирован на момент обращения к методу, 
     *    то он будет вычислен по данным журналов и сохранен для документа. 
     *    При этом, если вычисление не удалось, метод выдает исключение с пользовательским сообщением о причине.
     * 
     * @param performDate дата сдачи
     * @param slot запись студента в документе сессии
     * @param points выставленный пользователем балл (интерпретируется в соответствии с настроенным в скрипте режимом: либо как рейт. экв. оценки, либо как балл за сдачу мероприятия)
     * @return данные оценки для сохранения в сессии
     */
    ISessionMarkDAO.MarkData calculateMarkData(Date performDate, SessionDocumentSlot slot, Double points);

    /**
     * Метод для получения перечня доп. параметров, которые вычисляет скрипт расчета рейтинга.
     * Перечень передается в виде ключ параметра -> определение параметра.
     *
     * {@link ru.tandemservice.unisession.brs.dao.ISessionBrsDao.IRatingAdditParamDef }
     *
     * @param key ключ настроек рейтинга
     * @return { ключ параметра -> определение параметра }
     */
    Map<String, IRatingAdditParamDef> getRatingAdditionalParamDefinitions(ISessionRatingSettingsKey key);

    /**
     * Настройки работы с рейтингом для переданных записей в документах сессии:
     * {@link ru.tandemservice.unisession.brs.dao.ISessionBrsDao.ISessionRatingSettings }
     *
     * @param slots записи в документах сессии
     * @return настройки работы с рейтингом
     */
    Map<ISessionRatingSettingsKey, ISessionRatingSettings> getRatingSettings(Collection<SessionDocumentSlot> slots);

    /**
     * todo
     *
     * @param document
     * @return
     */
    Map<ISessionRatingSettingsKey, ISessionRatingSettings> getRatingSettings(SessionDocument document);

    /**
     * todo
     *
     * @param key
     * @return
     */
    ISessionRatingSettings getRatingSettings(ISessionRatingSettingsKey key);

    /**
     * Метод для формирования ключа настроек рейтинга по всем значимым для него параметрам:
     * шкала традиц. оценок, группа мер. итогового контроля, дисциплиночасть, уч. год и деканат.
     *
     * @param scale шкала оценок
     * @param caGroup группа мер. итогового контроля
     * @param discipline дисциплиночасть
     * @param yearPart часть уч. года
     * @param groupOu деканат
     * @return ключ настроек рейтинга для получения настроек рейтинга
     */
    ISessionRatingSettingsKey key(EppGradeScale scale, EppFControlActionGroup caGroup, EppRegistryElementPart discipline, EppYearPart yearPart, OrgUnit groupOu);

    /**
     * Метод для формирования ключа настроек рейтинга по записи в документе сессии.
     *
     * @param slot запись
     * @return ключ настроек рейтинга для получения настроек рейтинга
     */
    ISessionRatingSettingsKey key(SessionDocumentSlot slot);

    /**
     * todo
     *
     * @param document
     * @param slot
     * @return
     */
    ISessionRatingSettingsKey key(SessionDocument document, EppStudentWpeCAction slot);

    /**
     * todo
     *
     * @param groupOu
     * @param slot
     * @return
     */
    ISessionRatingSettingsKey key(OrgUnit groupOu, EppStudentWpeCAction slot);

    /**
     * Интерфейс для передачи настроек работы с рейтингом в документе сессии из скрипта расчета рейтинга.
     * Используется для работы БК сессии.
     *
     * Также используется в самом скрипте расчета: в его частях, связанных с вычислением традиционной оценки и ее рейт. эквивалента
     * */
    interface ISessionRatingSettings {
        /**
         * Ключ настроек: описание контекста, для которого должны действовать данные настройки.
         *
         * @return ключ настроек */
        ISessionRatingSettingsKey key();

        /**
         * Разрешать ввод баллов: рейт. экв. либо баллов за сдачу
         * в зависимости от {@link ru.tandemservice.unisession.brs.dao.ISessionBrsDao.ISessionRatingSettings#useCurrentRating()}
         *
         * если false, то работа с документом будет подразумевать только ввод и отображение традиционной оценки
         *
         * @return разрешать ввод баллов */
        boolean usePoints();

        /**
         * Фиксировать и использовать тек. рейтинг.
         *
         * Не имеет значения при отключенном вводе баллов {@link ru.tandemservice.unisession.brs.dao.ISessionBrsDao.ISessionRatingSettings#usePoints()}
         *
         * если true, то введенные пользователем баллы интерпретируются как баллы за сдачу мероприятия
         * если false, то введенные пользователем баллы интерпретируются как рейт. экв. оценки
         *
         * @return использовать тек. рейтинг
         */
        boolean useCurrentRating();

        /**
         * Валидатор для баллов, которые пользователь вводит на форме работы с документом.
         *
         * @return валидатор
         */
        ISessionPointsValidator pointsValidator();

        /**
         * Метод для обращения к коэффициентам БРС
         *
         * (это может быть нужно для работы скриптов далее,
         * при вычислении традиционной оценки и ее рейт. эквивалента).
         *
         * @param defCode пользовательский код коэффициента
         * @return значение коэффициента
         */
        IBrsCoefficientValue getCoefficientValue(String defCode);
    }
    
    interface ISessionRatingSettingsKey {
        OrgUnit getGroupOu();
        EppYearPart getYearPart();

        EppGradeScale getScale();
        EppFControlActionGroup getCaGroup();
        EppRegistryElementPart getDiscipline();
    }
    
    interface IBrsCoefficientValue {
        boolean isBoolValue();
        double getValueAsDouble();
        boolean isValueAsBoolean();
        String getValueStr();
    }

    interface ISessionPointsValidator {
        double min();
        double max();
        int precision();
    }
    
    interface IRatingAdditParamDef {
        String getKey();
        String getTitle();
        String getAbbreviation();
        String getComment();
    }

    interface IStudentCurrentRatingData
    {
        IRatingValue getRatingValue();
        boolean isAllowed();
        ISessionBrsDao.IRatingValue getRatingAdditParam(String key);
    }

    /** Интерфейс для передачи вычисленного рейтинга вместе с текстом ошибки или предупреждения */
    interface IRatingValue
    {
        /** @return Сообщение об ошибке при расчете, предназначенное для вывода в интерфейсе. */
        String getMessage();

        /** @return null, если рейтинг не может быть вычислен из-за ошибки */
        Double getValue();
    }
}
