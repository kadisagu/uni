/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReexamination.ui.Tab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unisession.base.bo.SessionReexamination.ui.Pub.SessionReexaminationPub;
import ru.tandemservice.unisession.entity.mark.SessionALRequest;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 25.08.2015
 */
@Configuration
public class SessionReexaminationTab extends BusinessComponentManager
{
    public static final String REQUEST_DS = "requestDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(REQUEST_DS, requestDS(), requestDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint requestDS()
    {
        return columnListExtPointBuilder(REQUEST_DS)
                .addColumn(publisherColumn(SessionALRequest.P_REQUEST_DATE, SessionALRequest.requestDate()).businessComponent(SessionReexaminationPub.class).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).permissionKey("studentSessionReexaminationPub").required(true).order())
                .addColumn(textColumn(SessionALRequest.L_BLOCK, SessionALRequest.block().titleWithFullNumber()))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).permissionKey("studentSessionReexaminationEdit"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER)
                                   .permissionKey("studentSessionReexaminationDelete")
                                   .alert(alert("requestDS.delete.alert", SessionALRequest.P_REQUEST_DATE, DateFormatter.DEFAULT_DATE_FORMATTER)))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> requestDSHandler()
    {
        return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                Long studentId = context.get(SessionReexaminationTabUI.PARAM_STUDENT_ID);

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SessionALRequest.class, "r")
                        .where(eq(property("r", SessionALRequest.student().id()), value(studentId)))
                        .order(property("r", SessionALRequest.requestDate()), input.getEntityOrder().getDirection());

                return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build();
            }
        };
    }
}
