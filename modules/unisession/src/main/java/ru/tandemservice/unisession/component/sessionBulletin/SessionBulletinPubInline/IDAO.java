/* $*/

package ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinPubInline;

import ru.tandemservice.uni.dao.IListDataSourceDao;
import ru.tandemservice.uni.dao.IUpdateable;

import java.util.List;

/**
 * @author oleyba
 * @since 2/18/11
 */
public interface IDAO extends IUpdateable<Model>, IListDataSourceDao<Model>
{
    String V_CHECKING_DISABLED = "checkingDisabled";
    String V_MARK = "mark";
    String V_MARK_COUNT = "markCount";

    String V_ALLOWED = "allowed";
    String V_ALLOWED_DISABLED = "allowed_disabled";

	String PROP_THEME = "theme";
	String PROP_SUPERVISOR = "supervisor";

    void updateAllowance(Long listenerParameter);

    /**
     * @param model модель
     * @return Список Id SessionDocumentSlot для допущенных студентов среды выбранных чекбоксами среди выбранных
     */
    List<Long> getAllowedStudentSlotIds(Model model);
}
