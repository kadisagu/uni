/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.SummaryBulletinAdd;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilter;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilterType;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterFormConfig;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.catalog.EppWorkPlanRowKind;
import ru.tandemservice.uniepp.entity.catalog.codes.EppWorkPlanRowKindCodes;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.SummaryBulletinPub.SessionReportSummaryBulletinPub;
import ru.tandemservice.unisession.base.bo.SessionReport.util.UniSessionFilterAddon;
import ru.tandemservice.unisession.entity.document.SessionObject;
import ru.tandemservice.unisession.entity.report.UnisessionSummaryBulletinReport;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 12/29/11
 */

@Input
({
    @Bind(key = SessionReportManager.BIND_ORG_UNIT, binding = "ouHolder.id", required = true)
})
public class SessionReportSummaryBulletinAddUI extends UIPresenter
{
    //private static IdentifiableWrapper noGroup = new IdentifiableWrapper(Long.MIN_VALUE, "вне групп");

    private OrgUnitHolder ouHolder = new OrgUnitHolder();

    private ISelectModel groupModel;

    private DataWrapper inSession;
    private boolean _showMarkData;
	private boolean showPreviousTermPractise = false;
	private boolean showDisciplineTitleWithNumber = false;
    private SessionObject sessionObject;

    private Date performDateFrom;
    private Date performDateTo;

    private boolean performDateFromActive;
    private boolean performDateToActive;

    private EducationYear year;
    private YearDistributionPart part;
/*
    Учебный год - селект, обязательный, одинарной ширины, по умолчанию текущий
    Часть года - селект, обязательный, одинарной ширины, справа от учебного года, выводим иерархией разбиение и части года (фактически учебный год и часть года определяют выбор конкретной сессии на данном деканате, по которой будет построен отчет)
    По результатам - селект, обязательный, одинарной ширины, два значения "в сессию" и "итоговым", по умолчанию выбрано "в сессию"
    Включать дисциплины - мультиселект, обязательный (хотя одно значение выбрано должно быть), одинарной ширины, три значения из справочника образовательных программ, по умолчанию выбрано только одно "Основная (обязательная)"
    Курс - селект, обязательный, выбор курса
    Академ. группа - мультиселект, необязательный, одинарной ширины, зависимый от фильтра курс, выводим сохраненные уникальные названия академ. групп (хранятся в "Студент в группе (по форме контроля)", т.е. в строке УГС) на момент проведения выбранной сессии (по ведомостям выбранной годо-части определяем МСРПпоФИК студентов, по ним определяем строки УГС ("Студент в группе (по форме контроля)"), по ним вычисляем уникальные сохраненные названия академ. групп на момент формирования УГС (т.е. какими они были во время этой сессии); если выбрали курс в фильтре выше, то урезается в статистике набор МСРПпоФИК студентов исходя из определения курса студента на момент выбранной годо-части по его сетке (по УП(в)), т.е. берем только МСРПпоФИК студентов соответствующего курса на момент выбранной годо-части (сессии)). Замечу, что если есть студенты вне групп, то в мультиселекте вместо элемента с пустым названием нужно вывести элемент с названием "вне групп", который пойдет первым.

     */

    @Override
    public void onComponentActivate()
    {
        getOuHolder().refresh();

        setInSession(SessionReportManager.instance().getResultDefaultOption());
    }

    @Override
    public void onComponentRefresh()
    {

        configUtil(getSessionFilterAddon());
        getOuHolder().refresh();

        if (null == getYear())
            setYear(EducationYear.getCurrentRequired());


    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(SessionReportManager.PARAM_ORG_UNIT, getOrgUnit());
        dataSource.put(SessionReportManager.PARAM_EDU_YEAR, getYear());
    }


    public void onClickApply()
    {
        validate();
        setSessionObject();
        final ISessionSummaryBulletinPrintDAO dao = ISessionSummaryBulletinPrintDAO.instance.get();
        UnisessionSummaryBulletinReport report= dao.createStoredReport(this);
        deactivate();
        _uiActivation.asDesktopRoot(SessionReportSummaryBulletinPub.class)
                .parameter(UIPresenter.PUBLISHER_ID, report.getId())
                .activate();
    }


    public void doNothing()
    {
    }

    public void validate()
    {
        if (performDateFromActive && performDateToActive && (performDateFrom.getTime() - performDateTo.getTime() > 0))
            _uiSupport.error("Дата, указанная в параметре \"Дата сдачи мероприятия в ведомости с\" не должна быть позже даты в параметре \"Дата сдачи мероприятия в ведомости по\".", "dateFrom");
        if(getUserContext().getErrorCollector().hasErrors())
            throw new ApplicationException();
    }

    public boolean isInSessionMarksOnly()
    {
        return SessionReportManager.RESULT_OPTION_ID_IN_SESSION == this.getInSession().getId();
    }

    public boolean isShowMarkPointsData()
    {
        return _showMarkData;
    }

    public SessionObject getSessionObject()
    {
        return sessionObject;
    }

    private void setSessionObject()
	{
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(SessionObject.class, "obj").column("obj")
                .where(eq(property(SessionObject.orgUnit().fromAlias("obj")), value(SessionReportSummaryBulletinAddUI.this.getOrgUnit())))
                .where(eq(property(SessionObject.yearDistributionPart().fromAlias("obj")), value(part)))
                .where(eq(property(SessionObject.educationYear().fromAlias("obj")), value(year)))
                .order(property(SessionObject.id().fromAlias("obj")));
        final List<SessionObject> list = dql.createStatement(_uiSupport.getSession()).list();
        if (list.isEmpty()) {
            sessionObject = null;
        }
        else if (list.size() != 1)
            throw new IllegalStateException();
        else
            sessionObject =  list.get(0);
    }

    // getters and setters


    public ISelectModel getGroupModel()
    {
        return groupModel;
    }

    public void setGroupModel(ISelectModel groupModel)
    {
        this.groupModel = groupModel;
    }

    public EducationYear getYear()
    {
        return year;
    }

    public void setYear(EducationYear year)
    {
        this.year = year;
    }

    public YearDistributionPart getPart()
    {
        return part;
    }

    public void setPart(YearDistributionPart part)
    {
        this.part = part;
    }

    public DataWrapper getInSession()
    {
        return inSession;
    }

    public void setInSession(DataWrapper inSession)
    {
        this.inSession = inSession;
    }

    public boolean isShowMarkData()
    {
        return _showMarkData;
    }

    public void setShowMarkData(boolean showMarkData)
    {
        _showMarkData = showMarkData;
    }

	public boolean isShowPreviousTermPractise()
	{
		return showPreviousTermPractise;
	}

	public void setShowPreviousTermPractise(boolean showPreviousTermPractise)
	{
		this.showPreviousTermPractise = showPreviousTermPractise;
	}

	public boolean isShowDisciplineTitleWithNumber()
	{
		return showDisciplineTitleWithNumber;
	}

	public void setShowDisciplineTitleWithNumber(boolean showDisciplineTitleWithNumber)
	{
		this.showDisciplineTitleWithNumber = showDisciplineTitleWithNumber;
	}

    public OrgUnitHolder getOuHolder()
    {
        return ouHolder;
    }

    public OrgUnit getOrgUnit()
    {
        return getOuHolder().getValue();
    }

    public boolean isPerformDateToActive() {
        return performDateToActive;
    }

    public void setPerformDateToActive(boolean performDateToActive) {
        this.performDateToActive = performDateToActive;
    }

    public boolean isPerformDateFromActive() {
        return performDateFromActive;
    }

    public void setPerformDateFromActive(boolean performDateFromActive) {
        this.performDateFromActive = performDateFromActive;
    }

    public Date getPerformDateTo() {
        return performDateTo;
    }

    public void setPerformDateTo(Date performDateTo) {
        this.performDateTo = performDateTo;
    }

    public Date getPerformDateFrom() {
        return performDateFrom;
    }

    public void setPerformDateFrom(Date performDateFrom) {
        this.performDateFrom = performDateFrom;
    }

// for filter addon

    public UniSessionFilterAddon getSessionFilterAddon()
    {
        return (UniSessionFilterAddon) getConfig().getAddon(CommonFilterAddon.class.getSimpleName());
    }

    public void onChangeYearOrPart()
    {
        configUtilWhere(getSessionFilterAddon());
    }

    private void configUtil(UniSessionFilterAddon util)
    {
        util
                .configDoubleWidthFilters(false)
                .configUseEnableCheckbox(true)
                .configSettings(getSettingsKey());


        util.clearFilterItems();

        util

                .addFilterItem(UniSessionFilterAddon.DISC_KIND, new CommonFilterFormConfig(true, true, true, false, true, true))
                .addFilterItem(UniSessionFilterAddon.REGISTRY_STRUCTURE, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(UniSessionFilterAddon.CONTROL_FORM, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(UniSessionFilterAddon.COURSE, CommonFilterFormConfig.DEFAULT_CHECKBOX_SELECT_FILTER_CONFIG)
                .addFilterItem(UniSessionFilterAddon.GROUP_WITH_NO_GROUP_ALT_TITLE, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(UniSessionFilterAddon.STUDENT_CUSTOM_STATE, CommonFilterFormConfig.DEFAULT_CHECKBOX_SELECT_FILTER_CONFIG)
                .addFilterItem(UniSessionFilterAddon.TARGET_ADMISSION, CommonFilterFormConfig.DEFAULT_CHECKBOX_SELECT_FILTER_CONFIG);

        util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EppStudentWpeCAction.studentWpe().student().educationOrgUnit().formativeOrgUnit(), getOrgUnit()));
        // дефолтные значения в фильтры
        final EppWorkPlanRowKind main = DataAccessServices.dao().getByCode(EppWorkPlanRowKind.class, EppWorkPlanRowKindCodes.MAIN);
        final List<EppWorkPlanRowKind> value = new ArrayList<>();
        value.add(main);
        util.getFilterItem(UniSessionFilterAddon.SETTINGS_NAME_DISC_KIND).setValue(value);
        util.saveSettings();
        configUtilWhere(util);
    }

    public void configUtilWhere(UniSessionFilterAddon util)
    {
        util.clearWhereFilter();

        util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EppStudentWpeCAction.studentWpe().student().status().active(), true));
        util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EppStudentWpeCAction.studentWpe().student().educationOrgUnit().formativeOrgUnit(), getOrgUnit()));


        if (null != year) util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EppStudentWpeCAction.studentWpe().year().educationYear(), year));
        if (null != part) util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EppStudentWpeCAction.studentWpe().part(), part));
    }
}
