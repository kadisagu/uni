// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.dao.group.EppRealGroupRowDAO;
import ru.tandemservice.uniepp.dao.student.EppStudentSlotDAO;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.catalog.codes.EppRegistryStructureCodes;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unisession.base.bo.SessionProtocol.ui.StateFinalExamList.SessionProtocolStateFinalExamList;
import ru.tandemservice.unisession.base.bo.SessionProtocol.ui.FinalQualWorkList.SessionProtocolFinalQualWorkList;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;

import java.util.*;

/**
 * @author oleyba
 * @since 2/18/11
 */
@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "bulletin.id"),
        @Bind(key = "selectedTabId", binding = "selectedTabId")
})
@Output({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "bulletin.id"),
        @Bind(key = ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinPubInline.Model.BIND_COLUMN_PKEY_HANDLER, binding = "columnPKeyHandler")
})
public class Model
{
    public static final String COMPONENT_NAME = Model.class.getPackage().getName();

    private SessionBulletinDocument bulletin = new SessionBulletinDocument();
    private DynamicListDataSource<EppStudentWorkPlanElement> checkDataSource = new DynamicListDataSource<EppStudentWorkPlanElement>();

    private String _selectedTabId;

    private boolean canEditThemes;
    private boolean withUnknownMarks;

    private String _eppRealEduGroupTitles;

    private List<PpsEntry> tutors;
    private List<Group> _studentGroupList;
    private Group currentGroup;

    private Boolean _isStateFinalExam;
    private boolean _hasEduGroupRow;


    private ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinPubInline.Model.ISessionBulletinColumnPKeyHandler columnPKeyHandler = new ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinPubInline.Model.ISessionBulletinColumnPKeyHandler()
    {
        @Override
        public String getAllowanceColumnPKey()
        {
            return "allowanceEditSessionBulletin";
        }

        @Override
        public String getAddSheetColumnPKey()
        {
            return "addSheetSessionBulletin";
        }
    };

    public String getEduGroupsDaemonStatus(final String message)
    {
        Long date;
        {
            date = EppStudentSlotDAO.DAEMON.getCompleteStatus();
            if (null == date)
            {
                return message;
            }
        }
        {
            date = EppRealGroupRowDAO.DAEMON.getCompleteStatus();
            if (null == date)
            {
                return message;
            }
        }
        return DateFormatter.DATE_FORMATTER_WITH_TIME.format(new Date(date));
    }

    // getters and setters

    public boolean isWithUnknownMarks()
    {
        return this.withUnknownMarks;
    }

    public void setWithUnknownMarks(final boolean withUnknownMarks)
    {
        this.withUnknownMarks = withUnknownMarks;
    }

    public SessionBulletinDocument getBulletin()
    {
        return this.bulletin;
    }

    public void setBulletin(final SessionBulletinDocument bulletin)
    {
        this.bulletin = bulletin;
    }

    public DynamicListDataSource<EppStudentWorkPlanElement> getCheckDataSource()
    {
        return this.checkDataSource;
    }

    public void setCheckDataSource(final DynamicListDataSource<EppStudentWorkPlanElement> checkDataSource)
    {
        this.checkDataSource = checkDataSource;
    }

    public String getSelectedTabId()
    {
        return this._selectedTabId;
    }

    public void setSelectedTabId(final String selectedTabId)
    {
        this._selectedTabId = selectedTabId;
    }

    public boolean isCanEditThemes()
    {
        return this.canEditThemes;
    }

    public void setCanEditThemes(final boolean canEditThemes)
    {
        this.canEditThemes = canEditThemes;
    }

    public ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinPubInline.Model.ISessionBulletinColumnPKeyHandler getColumnPKeyHandler()
    {
        return columnPKeyHandler;
    }

    public List<PpsEntry> getTutors()
    {
        return this.tutors;
    }

    public void setTutors(final List<PpsEntry> tutors)
    {
        this.tutors = tutors;
    }

    public String getEppRealEduGroupTitles()
    {
        return _eppRealEduGroupTitles;
    }

    public void setEppRealEduGroupTitles(String eppRealEduGroupTitles)
    {
        _eppRealEduGroupTitles = eppRealEduGroupTitles;
    }

    public Group getCurrentGroup()
    {
        return currentGroup;
    }

    public void setCurrentGroup(Group currentGroup)
    {
        this.currentGroup = currentGroup;
    }

    public List<Group> getStudentGroupList()
    {
        return _studentGroupList;
    }

    public void setStudentGroupList(List<Group> studentGroupList)
    {
        _studentGroupList = studentGroupList;
    }

    public boolean isStateFinalExam()
    {
        if (_isStateFinalExam == null)
        {
            EppRegistryStructure structure = bulletin.getGroup().getActivityPart().getRegistryElement().getParent().getParent();
            _isStateFinalExam = structure != null && EppRegistryStructureCodes.REGISTRY_ATTESTATION.equals(structure.getCode());
        }

        return _isStateFinalExam;
    }

    public boolean isHasEduGroupRow()
    {
        return _hasEduGroupRow;
    }

    public void setHasEduGroupRow(boolean hasEduGroupRow)
    {
        _hasEduGroupRow = hasEduGroupRow;
    }

    public String getViewProtocolsPermissionKey()
    {
        return EppRegistryStructureCodes.REGISTRY_ATTESTATION_DIPLOMA
                .equals(bulletin.getGroup().getActivityPart().getRegistryElement().getParent().getCode()) ?
                "viewFQWProtocolTab" : "viewStateExamProtocolTab";
    }

    public String getProtocolComponentName()
    {
        String structure = bulletin.getGroup().getActivityPart().getRegistryElement().getParent().getCode();
        if (EppRegistryStructureCodes.REGISTRY_ATTESTATION_DIPLOMA.equals(structure))
            return SessionProtocolFinalQualWorkList.class.getSimpleName();
        else
            return SessionProtocolStateFinalExamList.class.getSimpleName();
    }
}
