package ru.tandemservice.unisession.entity.document.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unisession.entity.document.SessionTransferProtocolDocument;
import ru.tandemservice.unisession.entity.document.SessionTransferProtocolRow;
import ru.tandemservice.unisession.entity.mark.SessionALRequestRow;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Строка протокола перезачтения
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionTransferProtocolRowGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.document.SessionTransferProtocolRow";
    public static final String ENTITY_NAME = "sessionTransferProtocolRow";
    public static final int VERSION_HASH = -1065617490;
    private static IEntityMeta ENTITY_META;

    public static final String L_PROTOCOL = "protocol";
    public static final String L_REQUEST_ROW = "requestRow";
    public static final String P_NEED_RETAKE = "needRetake";

    private SessionTransferProtocolDocument _protocol;     // Протокол
    private SessionALRequestRow _requestRow;     // Строка заявления
    private boolean _needRetake;     // Необходимость пересдачи (true - переаттестация; false - перезачтение)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Протокол. Свойство не может быть null.
     */
    @NotNull
    public SessionTransferProtocolDocument getProtocol()
    {
        return _protocol;
    }

    /**
     * @param protocol Протокол. Свойство не может быть null.
     */
    public void setProtocol(SessionTransferProtocolDocument protocol)
    {
        dirty(_protocol, protocol);
        _protocol = protocol;
    }

    /**
     * @return Строка заявления. Свойство не может быть null.
     */
    @NotNull
    public SessionALRequestRow getRequestRow()
    {
        return _requestRow;
    }

    /**
     * @param requestRow Строка заявления. Свойство не может быть null.
     */
    public void setRequestRow(SessionALRequestRow requestRow)
    {
        dirty(_requestRow, requestRow);
        _requestRow = requestRow;
    }

    /**
     * true  - переаттестация: необходимо пересдавать контрольные мероприятия.
     * false - перезачтение: изучать и сдавать ничего не нужно.
     *
     * @return Необходимость пересдачи (true - переаттестация; false - перезачтение). Свойство не может быть null.
     */
    @NotNull
    public boolean isNeedRetake()
    {
        return _needRetake;
    }

    /**
     * @param needRetake Необходимость пересдачи (true - переаттестация; false - перезачтение). Свойство не может быть null.
     */
    public void setNeedRetake(boolean needRetake)
    {
        dirty(_needRetake, needRetake);
        _needRetake = needRetake;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SessionTransferProtocolRowGen)
        {
            setProtocol(((SessionTransferProtocolRow)another).getProtocol());
            setRequestRow(((SessionTransferProtocolRow)another).getRequestRow());
            setNeedRetake(((SessionTransferProtocolRow)another).isNeedRetake());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionTransferProtocolRowGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionTransferProtocolRow.class;
        }

        public T newInstance()
        {
            return (T) new SessionTransferProtocolRow();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "protocol":
                    return obj.getProtocol();
                case "requestRow":
                    return obj.getRequestRow();
                case "needRetake":
                    return obj.isNeedRetake();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "protocol":
                    obj.setProtocol((SessionTransferProtocolDocument) value);
                    return;
                case "requestRow":
                    obj.setRequestRow((SessionALRequestRow) value);
                    return;
                case "needRetake":
                    obj.setNeedRetake((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "protocol":
                        return true;
                case "requestRow":
                        return true;
                case "needRetake":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "protocol":
                    return true;
                case "requestRow":
                    return true;
                case "needRetake":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "protocol":
                    return SessionTransferProtocolDocument.class;
                case "requestRow":
                    return SessionALRequestRow.class;
                case "needRetake":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionTransferProtocolRow> _dslPath = new Path<SessionTransferProtocolRow>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionTransferProtocolRow");
    }
            

    /**
     * @return Протокол. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionTransferProtocolRow#getProtocol()
     */
    public static SessionTransferProtocolDocument.Path<SessionTransferProtocolDocument> protocol()
    {
        return _dslPath.protocol();
    }

    /**
     * @return Строка заявления. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionTransferProtocolRow#getRequestRow()
     */
    public static SessionALRequestRow.Path<SessionALRequestRow> requestRow()
    {
        return _dslPath.requestRow();
    }

    /**
     * true  - переаттестация: необходимо пересдавать контрольные мероприятия.
     * false - перезачтение: изучать и сдавать ничего не нужно.
     *
     * @return Необходимость пересдачи (true - переаттестация; false - перезачтение). Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionTransferProtocolRow#isNeedRetake()
     */
    public static PropertyPath<Boolean> needRetake()
    {
        return _dslPath.needRetake();
    }

    public static class Path<E extends SessionTransferProtocolRow> extends EntityPath<E>
    {
        private SessionTransferProtocolDocument.Path<SessionTransferProtocolDocument> _protocol;
        private SessionALRequestRow.Path<SessionALRequestRow> _requestRow;
        private PropertyPath<Boolean> _needRetake;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Протокол. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionTransferProtocolRow#getProtocol()
     */
        public SessionTransferProtocolDocument.Path<SessionTransferProtocolDocument> protocol()
        {
            if(_protocol == null )
                _protocol = new SessionTransferProtocolDocument.Path<SessionTransferProtocolDocument>(L_PROTOCOL, this);
            return _protocol;
        }

    /**
     * @return Строка заявления. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionTransferProtocolRow#getRequestRow()
     */
        public SessionALRequestRow.Path<SessionALRequestRow> requestRow()
        {
            if(_requestRow == null )
                _requestRow = new SessionALRequestRow.Path<SessionALRequestRow>(L_REQUEST_ROW, this);
            return _requestRow;
        }

    /**
     * true  - переаттестация: необходимо пересдавать контрольные мероприятия.
     * false - перезачтение: изучать и сдавать ничего не нужно.
     *
     * @return Необходимость пересдачи (true - переаттестация; false - перезачтение). Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionTransferProtocolRow#isNeedRetake()
     */
        public PropertyPath<Boolean> needRetake()
        {
            if(_needRetake == null )
                _needRetake = new PropertyPath<Boolean>(SessionTransferProtocolRowGen.P_NEED_RETAKE, this);
            return _needRetake;
        }

        public Class getEntityClass()
        {
            return SessionTransferProtocolRow.class;
        }

        public String getEntityName()
        {
            return "sessionTransferProtocolRow";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
