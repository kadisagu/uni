/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionProtocol.ui.StateFinalExamList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.IColumnListExtPointBuilder;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.unisession.base.bo.SessionProtocol.SessionProtocolManager;
import ru.tandemservice.unisession.base.bo.SessionProtocol.ui.AbstractList.SessionProtocolAbstractList;

/**
 * @author Andrey Andreev
 * @since 12.09.2016
 */
@Configuration
public class SessionProtocolStateFinalExamList extends SessionProtocolAbstractList
{

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(STATE_FINAL_EXAM_PROTOCOL_DS, stateFinalExamProtocolDSColumnExtPoint(),
                                            SessionProtocolManager.instance().stateFinalExamProtocolDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint stateFinalExamProtocolDSColumnExtPoint()
    {
        IColumnListExtPointBuilder builder = columnListExtPointBuilder(STATE_FINAL_EXAM_PROTOCOL_DS);
        addDefaultProtocolListColumns(builder);
        return builder.create();
    }

    @Override
    public String getDeletePermissionKey()
    {
        return "deleteStateExamProtocol";
    }

    @Override
    public String getPrintPermissionKey()
    {
        return "printStateExamProtocol";
    }
}