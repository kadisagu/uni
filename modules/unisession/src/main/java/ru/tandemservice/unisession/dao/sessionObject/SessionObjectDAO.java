// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisession.dao.sessionObject;

import com.google.common.collect.Iterables;
import org.apache.commons.lang.mutable.MutableInt;
import org.hibernate.criterion.Projections;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.INumberGenerationRule;
import org.tandemframework.shared.commonbase.base.util.SimpleNumberGenerationRule;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unisession.entity.document.SessionObject;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 2/8/11
 */
public class SessionObjectDAO extends UniBaseDao implements ISessionObjectDAO
{
    @Override
    public INumberGenerationRule<SessionObject> getNumberGenerationRule()
    {
        return new SimpleNumberGenerationRule<SessionObject>()
        {
            @Override
            @SuppressWarnings("unchecked")
            public Set<String> getUsedNumbers(final SessionObject object)
            {
                return new HashSet<>(SessionObjectDAO.this.getSession().createCriteria(SessionObject.class).setProjection(Projections.property(SessionObject.number().toString())).list());
            }

            @Override
            public String getNumberQueueName(final SessionObject object)
            {
                return SessionObject.class.getSimpleName();
            }
        };
    }

    @Override
    public Map<Long, Integer> lastTermNumberMap(Collection<Long> studentIds)
    {
        final Map<Long, MutableInt> lastTermNumberMap = SafeMap.get(MutableInt.class);
        for (List<Long> elements : Iterables.partition(studentIds, DQL.MAX_VALUES_ROW_NUMBER)) {

            final List<Object[]> list = new DQLSelectBuilder().fromEntity(SessionMark.class, "m")
                    .joinPath(DQLJoinType.inner, SessionMark.slot().studentWpeCAction().studentWpe().fromAlias("m"), "wpe")
                    .column(property(EppStudentWorkPlanElement.student().id().fromAlias("wpe")))
                    .column(property(EppStudentWorkPlanElement.term().intValue().fromAlias("wpe")))
                    .where(in(property(EppStudentWorkPlanElement.student().id().fromAlias("wpe")), elements))
                    .createStatement(getSession()).list();

            for (final Object[] row : list)
            {
                final Long studentId = (Long) row[0];
                final Integer term = (Integer) row[1];

                final MutableInt i = lastTermNumberMap.get(studentId);
                if (i.intValue() < term)
                    i.setValue(term.intValue());
            }
        }

        final Map<Long, Integer> resultMap = new HashMap<>();
        for (Map.Entry<Long, MutableInt> entry : lastTermNumberMap.entrySet())
            resultMap.put(entry.getKey(), entry.getValue().intValue());

        return resultMap;
    }

    @Override
    public SessionObject getSessionObject(OrgUnit groupOrgUnit, EducationYear year, YearDistributionPart part)
    {
        if (year == null || part == null)
            return null;

        final String alias = "session";
        return new DQLSelectBuilder()
                .fromEntity(SessionObject.class, alias)
                .where(eq(property(SessionObject.orgUnit().fromAlias(alias)), value(groupOrgUnit)))
                .where(eq(property(SessionObject.educationYear().fromAlias(alias)), value(year)))
                .where(eq(property(SessionObject.yearDistributionPart().fromAlias(alias)), value(part)))
                .createStatement(getSession()).uniqueResult();
    }
}
