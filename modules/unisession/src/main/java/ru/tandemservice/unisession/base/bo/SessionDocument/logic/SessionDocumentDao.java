/* $Id:$ */
package ru.tandemservice.unisession.base.bo.SessionDocument.logic;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.meta.entity.access.IEntityAccessSemaphore;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.commonbase.dao.SharedBaseDao;
import ru.tandemservice.unisession.entity.mark.SessionSlotMarkGradeValue;

import java.util.Collection;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 13.11.2016
 */
public class SessionDocumentDao extends SharedBaseDao implements ISessionDocumentDao
{
    @Override
    public void changeCheckedGradeBook(Collection<Long> actionIds, boolean oldValue)
    {
        if (CollectionUtils.isEmpty(actionIds)) return;
        NamedSyncInTransactionCheckLocker.register(getSession(), "SessionSlotMarkGradeValue-changeCheckedGradeBook");

        // усыпляем листенеры проверки консистентности отметок
        IEntityAccessSemaphore semaphore = CoreServices.entityAccessService().createSemaphore();
        try {
            semaphore.allowUpdate(SessionSlotMarkGradeValue.class, true);
            executeAndClear(
                    new DQLUpdateBuilder(SessionSlotMarkGradeValue.class)
                            .fromDataSource(
                                    new DQLSelectBuilder().fromEntity(SessionSlotMarkGradeValue.class, "m")
                                            .column(property("m.id"), "mark_id")
                                            .where(eq(property("m", SessionSlotMarkGradeValue.cachedMarkPositiveStatus()), value(Boolean.TRUE)))
                                            .where(in(property("m", SessionSlotMarkGradeValue.slot().studentWpeCAction().id()), actionIds))
                                            .buildQuery(), "x")
                            .where(eq(property(SessionSlotMarkGradeValue.id()), property("x.mark_id")))
                            .set(SessionSlotMarkGradeValue.checkedGradeBook().s(), value(!oldValue))
            );
        }
        finally {
            // будим обратно листенеры проверки консистентности отметок
            semaphore.release();
        }
    }
}
