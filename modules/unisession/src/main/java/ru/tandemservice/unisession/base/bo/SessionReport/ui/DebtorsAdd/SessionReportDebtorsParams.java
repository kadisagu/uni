/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.DebtorsAdd;

import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.unisession.entity.document.SessionObject;

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * @author oleyba
 * @since 1/24/12
 */
public class SessionReportDebtorsParams implements ISessionReportDebtorsDAO.ISessionReportDebtorsParams
{
    private SessionObject _sessionObject;

    private boolean _educationLevelHighSchoolActive;
    private List<EducationLevelsHighSchool> _educationLevelHighSchoolList;

    private boolean _developFormActive;
    private List<DevelopForm> _developFormList;

    private boolean _developConditionActive;
    private List<DevelopCondition> _developConditionList;

    private boolean _developTechActive;
    private List<DevelopTech> _developTechList;

    private boolean _developPeriodActive;
    private List<DevelopPeriod> _developPeriodList;

    private boolean _courseActive;
    private List<Course> _courseList;

    private boolean _groupActive;
    private boolean includeStudentsWithoutGroups;
    private List<Group> _groupList;

    private boolean _compensationTypeActive;
    private CompensationType _compensationType;

    private boolean _studentStatusActive;
    private List<StudentStatus> _studentStatusList;

    private boolean deadlineCheckActive;
    private Date deadlineCheck;

    private boolean eppFControlActionTypeCheckActive;
    private Collection<EppFControlActionType> eppFControlActionTypes;


    @Override
    public SessionObject getSessionObject()
    {
        return _sessionObject;
    }

    @Override
    public boolean isEducationLevelHighSchoolActive()
    {
        return _educationLevelHighSchoolActive;
    }

    @Override
    public List<EducationLevelsHighSchool> getEducationLevelHighSchoolList()
    {
        return _educationLevelHighSchoolList;
    }

    @Override
    public boolean isDevelopFormActive()
    {
        return _developFormActive;
    }

    @Override
    public List<DevelopForm> getDevelopFormList()
    {
        return _developFormList;
    }

    @Override
    public boolean isDevelopConditionActive()
    {
        return _developConditionActive;
    }

    @Override
    public List<DevelopCondition> getDevelopConditionList()
    {
        return _developConditionList;
    }

    @Override
    public boolean isDevelopTechActive()
    {
        return _developTechActive;
    }

    @Override
    public List<DevelopTech> getDevelopTechList()
    {
        return _developTechList;
    }

    @Override
    public boolean isDevelopPeriodActive()
    {
        return _developPeriodActive;
    }

    @Override
    public List<DevelopPeriod> getDevelopPeriodList()
    {
        return _developPeriodList;
    }

    @Override
    public boolean isCourseActive()
    {
        return _courseActive;
    }

    @Override
    public List<Course> getCourseList()
    {
        return _courseList;
    }

    @Override
    public boolean isGroupActive()
    {
        return _groupActive;
    }

    @Override
    public boolean isIncludeStudentsWithoutGroups()
    {
        return includeStudentsWithoutGroups;
    }

    @Override
    public List<Group> getGroupList()
    {
        return _groupList;
    }

    @Override
    public boolean isCompensationTypeActive()
    {
        return _compensationTypeActive;
    }

    @Override
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    @Override
    public boolean isStudentStatusActive()
    {
        return _studentStatusActive;
    }

    @Override
    public List<StudentStatus> getStudentStatusList()
    {
        return _studentStatusList;
    }

    public void setSessionObject(SessionObject sessionObject)
    {
        _sessionObject = sessionObject;
    }

    public void setEducationLevelHighSchoolActive(boolean educationLevelHighSchoolActive)
    {
        _educationLevelHighSchoolActive = educationLevelHighSchoolActive;
    }

    public void setEducationLevelHighSchoolList(List<EducationLevelsHighSchool> educationLevelHighSchoolList)
    {
        _educationLevelHighSchoolList = educationLevelHighSchoolList;
    }

    public void setDevelopFormActive(boolean developFormActive)
    {
        _developFormActive = developFormActive;
    }

    public void setDevelopFormList(List<DevelopForm> developFormList)
    {
        _developFormList = developFormList;
    }

    public void setDevelopConditionActive(boolean developConditionActive)
    {
        _developConditionActive = developConditionActive;
    }

    public void setDevelopConditionList(List<DevelopCondition> developConditionList)
    {
        _developConditionList = developConditionList;
    }

    public void setDevelopTechActive(boolean developTechActive)
    {
        _developTechActive = developTechActive;
    }

    public void setDevelopTechList(List<DevelopTech> developTechList)
    {
        _developTechList = developTechList;
    }

    public void setDevelopPeriodActive(boolean developPeriodActive)
    {
        _developPeriodActive = developPeriodActive;
    }

    public void setDevelopPeriodList(List<DevelopPeriod> developPeriodList)
    {
        _developPeriodList = developPeriodList;
    }

    public void setCourseActive(boolean courseActive)
    {
        _courseActive = courseActive;
    }

    public void setCourseList(List<Course> courseList)
    {
        _courseList = courseList;
    }

    public void setGroupActive(boolean groupActive)
    {
        _groupActive = groupActive;
    }

    public void setIncludeStudentsWithoutGroups(boolean includeStudentsWithoutGroups)
    {
        this.includeStudentsWithoutGroups = includeStudentsWithoutGroups;
    }

    public void setGroupList(List<Group> groupList)
    {
        _groupList = groupList;
    }

    public void setCompensationTypeActive(boolean compensationTypeActive)
    {
        _compensationTypeActive = compensationTypeActive;
    }

    public void setCompensationType(CompensationType compensationType)
    {
        _compensationType = compensationType;
    }

    public void setStudentStatusActive(boolean studentStatusActive)
    {
        _studentStatusActive = studentStatusActive;
    }

    public void setStudentStatusList(List<StudentStatus> studentStatusList)
    {
        _studentStatusList = studentStatusList;
    }

    @Override
    public Date getDeadlineCheck()
    {
        return deadlineCheck;
    }

    public void setDeadlineCheck(Date deadlineCheck)
    {
        this.deadlineCheck = deadlineCheck;
    }

    @Override
    public boolean isDeadlineCheckActive()
    {
        return deadlineCheckActive;
    }

    public void setDeadlineCheckActive(boolean deadlineCheckActive)
    {
        this.deadlineCheckActive = deadlineCheckActive;
    }

    @Override
    public Collection<EppFControlActionType> getEppFControlActionTypes() {
        return eppFControlActionTypes;
    }

    public void setEppFControlActionTypes(Collection<EppFControlActionType> eppFControlActionTypes) {
        this.eppFControlActionTypes = eppFControlActionTypes;
    }

    @Override
    public boolean isEppFControlActionTypeCheckActive() {
        return eppFControlActionTypeCheckActive;
    }

    public void setEppFControlActionTypeCheckActive(boolean eppFControlActionTypeCheckActive) {
        this.eppFControlActionTypeCheckActive = eppFControlActionTypeCheckActive;
    }
}