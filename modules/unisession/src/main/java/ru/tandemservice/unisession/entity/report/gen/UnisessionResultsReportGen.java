package ru.tandemservice.unisession.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unisession.entity.report.UnisessionResultsReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Отчет «Сведения об успеваемости студентов (по курсам, по группам, по направлениям)»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UnisessionResultsReportGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.report.UnisessionResultsReport";
    public static final String ENTITY_NAME = "unisessionResultsReport";
    public static final int VERSION_HASH = 1002045880;
    private static IEntityMeta ENTITY_META;

    public static final String L_CONTENT = "content";
    public static final String P_FORMING_DATE = "formingDate";
    public static final String P_EXECUTOR = "executor";
    public static final String L_ORG_UNIT = "orgUnit";
    public static final String L_EDUCATION_YEAR = "educationYear";
    public static final String L_YEAR_DISTRIBUTION_PART = "yearDistributionPart";
    public static final String P_PERFORM_DATE_FROM = "performDateFrom";
    public static final String P_PERFORM_DATE_TO = "performDateTo";
    public static final String P_GROUPING = "grouping";
    public static final String P_DISC_KINDS = "discKinds";
    public static final String P_GROUP_ORG_UNIT = "groupOrgUnit";
    public static final String P_TERRITORIAL_ORG_UNIT = "territorialOrgUnit";
    public static final String P_COMPENSATION_TYPE = "compensationType";
    public static final String P_DEVELOP_FORM = "developForm";
    public static final String P_DEVELOP_CONDITION = "developCondition";
    public static final String P_DEVELOP_TECH = "developTech";
    public static final String P_DEVELOP_PERIOD = "developPeriod";
    public static final String P_COURSE = "course";
    public static final String P_STUDENT_STATUS = "studentStatus";
    public static final String P_CUSTOM_STATE = "customState";
    public static final String P_TARGET_ADMISSION = "targetAdmission";
    public static final String P_FORMING_DATE_STR = "formingDateStr";

    private DatabaseFile _content;     // Печатная форма
    private Date _formingDate;     // Дата формирования
    private String _executor;     // Исполнитель
    private OrgUnit _orgUnit;     // Подразделение
    private EducationYear _educationYear;     // Учебный год
    private YearDistributionPart _yearDistributionPart;     // Часть года
    private Date _performDateFrom;     // Дата сдачи мероприятия в ведомости с
    private Date _performDateTo;     // Дата сдачи мероприятия в ведомости по
    private String _grouping;     // Группировка строк отчета
    private String _discKinds;     // Включать дисциплины
    private String _groupOrgUnit;     // Деканат
    private String _territorialOrgUnit;     // Территориальное подр.
    private String _compensationType;     // Вид возмещения затрат
    private String _developForm;     // Форма освоения
    private String _developCondition;     // Условие освоения
    private String _developTech;     // Технология освоения
    private String _developPeriod;     // Срок освоения
    private String _course;     // Курс
    private String _studentStatus;     // Состояние студента
    private String _customState;     // Дополнительный статус
    private String _targetAdmission;     // Целевой прием

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     */
    @NotNull
    public DatabaseFile getContent()
    {
        return _content;
    }

    /**
     * @param content Печатная форма. Свойство не может быть null.
     */
    public void setContent(DatabaseFile content)
    {
        dirty(_content, content);
        _content = content;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата формирования. Свойство не может быть null.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    /**
     * @return Исполнитель.
     */
    @Length(max=255)
    public String getExecutor()
    {
        return _executor;
    }

    /**
     * @param executor Исполнитель.
     */
    public void setExecutor(String executor)
    {
        dirty(_executor, executor);
        _executor = executor;
    }

    /**
     * @return Подразделение.
     */
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     */
    @NotNull
    public EducationYear getEducationYear()
    {
        return _educationYear;
    }

    /**
     * @param educationYear Учебный год. Свойство не может быть null.
     */
    public void setEducationYear(EducationYear educationYear)
    {
        dirty(_educationYear, educationYear);
        _educationYear = educationYear;
    }

    /**
     * @return Часть года. Свойство не может быть null.
     */
    @NotNull
    public YearDistributionPart getYearDistributionPart()
    {
        return _yearDistributionPart;
    }

    /**
     * @param yearDistributionPart Часть года. Свойство не может быть null.
     */
    public void setYearDistributionPart(YearDistributionPart yearDistributionPart)
    {
        dirty(_yearDistributionPart, yearDistributionPart);
        _yearDistributionPart = yearDistributionPart;
    }

    /**
     * @return Дата сдачи мероприятия в ведомости с.
     */
    public Date getPerformDateFrom()
    {
        return _performDateFrom;
    }

    /**
     * @param performDateFrom Дата сдачи мероприятия в ведомости с.
     */
    public void setPerformDateFrom(Date performDateFrom)
    {
        dirty(_performDateFrom, performDateFrom);
        _performDateFrom = performDateFrom;
    }

    /**
     * @return Дата сдачи мероприятия в ведомости по.
     */
    public Date getPerformDateTo()
    {
        return _performDateTo;
    }

    /**
     * @param performDateTo Дата сдачи мероприятия в ведомости по.
     */
    public void setPerformDateTo(Date performDateTo)
    {
        dirty(_performDateTo, performDateTo);
        _performDateTo = performDateTo;
    }

    /**
     * @return Группировка строк отчета.
     */
    public String getGrouping()
    {
        return _grouping;
    }

    /**
     * @param grouping Группировка строк отчета.
     */
    public void setGrouping(String grouping)
    {
        dirty(_grouping, grouping);
        _grouping = grouping;
    }

    /**
     * Названия видов дисциплин РУП, выбранных при построении отчета, через запятую.
     *
     * @return Включать дисциплины. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getDiscKinds()
    {
        return _discKinds;
    }

    /**
     * @param discKinds Включать дисциплины. Свойство не может быть null.
     */
    public void setDiscKinds(String discKinds)
    {
        dirty(_discKinds, discKinds);
        _discKinds = discKinds;
    }

    /**
     * @return Деканат.
     */
    public String getGroupOrgUnit()
    {
        return _groupOrgUnit;
    }

    /**
     * @param groupOrgUnit Деканат.
     */
    public void setGroupOrgUnit(String groupOrgUnit)
    {
        dirty(_groupOrgUnit, groupOrgUnit);
        _groupOrgUnit = groupOrgUnit;
    }

    /**
     * @return Территориальное подр..
     */
    public String getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    /**
     * @param territorialOrgUnit Территориальное подр..
     */
    public void setTerritorialOrgUnit(String territorialOrgUnit)
    {
        dirty(_territorialOrgUnit, territorialOrgUnit);
        _territorialOrgUnit = territorialOrgUnit;
    }

    /**
     * @return Вид возмещения затрат.
     */
    @Length(max=255)
    public String getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат.
     */
    public void setCompensationType(String compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Форма освоения.
     */
    @Length(max=255)
    public String getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения.
     */
    public void setDevelopForm(String developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Условие освоения.
     */
    @Length(max=255)
    public String getDevelopCondition()
    {
        return _developCondition;
    }

    /**
     * @param developCondition Условие освоения.
     */
    public void setDevelopCondition(String developCondition)
    {
        dirty(_developCondition, developCondition);
        _developCondition = developCondition;
    }

    /**
     * @return Технология освоения.
     */
    @Length(max=255)
    public String getDevelopTech()
    {
        return _developTech;
    }

    /**
     * @param developTech Технология освоения.
     */
    public void setDevelopTech(String developTech)
    {
        dirty(_developTech, developTech);
        _developTech = developTech;
    }

    /**
     * @return Срок освоения.
     */
    @Length(max=255)
    public String getDevelopPeriod()
    {
        return _developPeriod;
    }

    /**
     * @param developPeriod Срок освоения.
     */
    public void setDevelopPeriod(String developPeriod)
    {
        dirty(_developPeriod, developPeriod);
        _developPeriod = developPeriod;
    }

    /**
     * @return Курс.
     */
    @Length(max=255)
    public String getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс.
     */
    public void setCourse(String course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Состояние студента.
     */
    @Length(max=255)
    public String getStudentStatus()
    {
        return _studentStatus;
    }

    /**
     * @param studentStatus Состояние студента.
     */
    public void setStudentStatus(String studentStatus)
    {
        dirty(_studentStatus, studentStatus);
        _studentStatus = studentStatus;
    }

    /**
     * @return Дополнительный статус.
     */
    public String getCustomState()
    {
        return _customState;
    }

    /**
     * @param customState Дополнительный статус.
     */
    public void setCustomState(String customState)
    {
        dirty(_customState, customState);
        _customState = customState;
    }

    /**
     * @return Целевой прием.
     */
    @Length(max=255)
    public String getTargetAdmission()
    {
        return _targetAdmission;
    }

    /**
     * @param targetAdmission Целевой прием.
     */
    public void setTargetAdmission(String targetAdmission)
    {
        dirty(_targetAdmission, targetAdmission);
        _targetAdmission = targetAdmission;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UnisessionResultsReportGen)
        {
            setContent(((UnisessionResultsReport)another).getContent());
            setFormingDate(((UnisessionResultsReport)another).getFormingDate());
            setExecutor(((UnisessionResultsReport)another).getExecutor());
            setOrgUnit(((UnisessionResultsReport)another).getOrgUnit());
            setEducationYear(((UnisessionResultsReport)another).getEducationYear());
            setYearDistributionPart(((UnisessionResultsReport)another).getYearDistributionPart());
            setPerformDateFrom(((UnisessionResultsReport)another).getPerformDateFrom());
            setPerformDateTo(((UnisessionResultsReport)another).getPerformDateTo());
            setGrouping(((UnisessionResultsReport)another).getGrouping());
            setDiscKinds(((UnisessionResultsReport)another).getDiscKinds());
            setGroupOrgUnit(((UnisessionResultsReport)another).getGroupOrgUnit());
            setTerritorialOrgUnit(((UnisessionResultsReport)another).getTerritorialOrgUnit());
            setCompensationType(((UnisessionResultsReport)another).getCompensationType());
            setDevelopForm(((UnisessionResultsReport)another).getDevelopForm());
            setDevelopCondition(((UnisessionResultsReport)another).getDevelopCondition());
            setDevelopTech(((UnisessionResultsReport)another).getDevelopTech());
            setDevelopPeriod(((UnisessionResultsReport)another).getDevelopPeriod());
            setCourse(((UnisessionResultsReport)another).getCourse());
            setStudentStatus(((UnisessionResultsReport)another).getStudentStatus());
            setCustomState(((UnisessionResultsReport)another).getCustomState());
            setTargetAdmission(((UnisessionResultsReport)another).getTargetAdmission());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UnisessionResultsReportGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UnisessionResultsReport.class;
        }

        public T newInstance()
        {
            return (T) new UnisessionResultsReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "content":
                    return obj.getContent();
                case "formingDate":
                    return obj.getFormingDate();
                case "executor":
                    return obj.getExecutor();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "educationYear":
                    return obj.getEducationYear();
                case "yearDistributionPart":
                    return obj.getYearDistributionPart();
                case "performDateFrom":
                    return obj.getPerformDateFrom();
                case "performDateTo":
                    return obj.getPerformDateTo();
                case "grouping":
                    return obj.getGrouping();
                case "discKinds":
                    return obj.getDiscKinds();
                case "groupOrgUnit":
                    return obj.getGroupOrgUnit();
                case "territorialOrgUnit":
                    return obj.getTerritorialOrgUnit();
                case "compensationType":
                    return obj.getCompensationType();
                case "developForm":
                    return obj.getDevelopForm();
                case "developCondition":
                    return obj.getDevelopCondition();
                case "developTech":
                    return obj.getDevelopTech();
                case "developPeriod":
                    return obj.getDevelopPeriod();
                case "course":
                    return obj.getCourse();
                case "studentStatus":
                    return obj.getStudentStatus();
                case "customState":
                    return obj.getCustomState();
                case "targetAdmission":
                    return obj.getTargetAdmission();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "content":
                    obj.setContent((DatabaseFile) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
                case "executor":
                    obj.setExecutor((String) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "educationYear":
                    obj.setEducationYear((EducationYear) value);
                    return;
                case "yearDistributionPart":
                    obj.setYearDistributionPart((YearDistributionPart) value);
                    return;
                case "performDateFrom":
                    obj.setPerformDateFrom((Date) value);
                    return;
                case "performDateTo":
                    obj.setPerformDateTo((Date) value);
                    return;
                case "grouping":
                    obj.setGrouping((String) value);
                    return;
                case "discKinds":
                    obj.setDiscKinds((String) value);
                    return;
                case "groupOrgUnit":
                    obj.setGroupOrgUnit((String) value);
                    return;
                case "territorialOrgUnit":
                    obj.setTerritorialOrgUnit((String) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((String) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((String) value);
                    return;
                case "developCondition":
                    obj.setDevelopCondition((String) value);
                    return;
                case "developTech":
                    obj.setDevelopTech((String) value);
                    return;
                case "developPeriod":
                    obj.setDevelopPeriod((String) value);
                    return;
                case "course":
                    obj.setCourse((String) value);
                    return;
                case "studentStatus":
                    obj.setStudentStatus((String) value);
                    return;
                case "customState":
                    obj.setCustomState((String) value);
                    return;
                case "targetAdmission":
                    obj.setTargetAdmission((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "content":
                        return true;
                case "formingDate":
                        return true;
                case "executor":
                        return true;
                case "orgUnit":
                        return true;
                case "educationYear":
                        return true;
                case "yearDistributionPart":
                        return true;
                case "performDateFrom":
                        return true;
                case "performDateTo":
                        return true;
                case "grouping":
                        return true;
                case "discKinds":
                        return true;
                case "groupOrgUnit":
                        return true;
                case "territorialOrgUnit":
                        return true;
                case "compensationType":
                        return true;
                case "developForm":
                        return true;
                case "developCondition":
                        return true;
                case "developTech":
                        return true;
                case "developPeriod":
                        return true;
                case "course":
                        return true;
                case "studentStatus":
                        return true;
                case "customState":
                        return true;
                case "targetAdmission":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "content":
                    return true;
                case "formingDate":
                    return true;
                case "executor":
                    return true;
                case "orgUnit":
                    return true;
                case "educationYear":
                    return true;
                case "yearDistributionPart":
                    return true;
                case "performDateFrom":
                    return true;
                case "performDateTo":
                    return true;
                case "grouping":
                    return true;
                case "discKinds":
                    return true;
                case "groupOrgUnit":
                    return true;
                case "territorialOrgUnit":
                    return true;
                case "compensationType":
                    return true;
                case "developForm":
                    return true;
                case "developCondition":
                    return true;
                case "developTech":
                    return true;
                case "developPeriod":
                    return true;
                case "course":
                    return true;
                case "studentStatus":
                    return true;
                case "customState":
                    return true;
                case "targetAdmission":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "content":
                    return DatabaseFile.class;
                case "formingDate":
                    return Date.class;
                case "executor":
                    return String.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "educationYear":
                    return EducationYear.class;
                case "yearDistributionPart":
                    return YearDistributionPart.class;
                case "performDateFrom":
                    return Date.class;
                case "performDateTo":
                    return Date.class;
                case "grouping":
                    return String.class;
                case "discKinds":
                    return String.class;
                case "groupOrgUnit":
                    return String.class;
                case "territorialOrgUnit":
                    return String.class;
                case "compensationType":
                    return String.class;
                case "developForm":
                    return String.class;
                case "developCondition":
                    return String.class;
                case "developTech":
                    return String.class;
                case "developPeriod":
                    return String.class;
                case "course":
                    return String.class;
                case "studentStatus":
                    return String.class;
                case "customState":
                    return String.class;
                case "targetAdmission":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UnisessionResultsReport> _dslPath = new Path<UnisessionResultsReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UnisessionResultsReport");
    }
            

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.report.UnisessionResultsReport#getContent()
     */
    public static DatabaseFile.Path<DatabaseFile> content()
    {
        return _dslPath.content();
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.report.UnisessionResultsReport#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * @return Исполнитель.
     * @see ru.tandemservice.unisession.entity.report.UnisessionResultsReport#getExecutor()
     */
    public static PropertyPath<String> executor()
    {
        return _dslPath.executor();
    }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.unisession.entity.report.UnisessionResultsReport#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.report.UnisessionResultsReport#getEducationYear()
     */
    public static EducationYear.Path<EducationYear> educationYear()
    {
        return _dslPath.educationYear();
    }

    /**
     * @return Часть года. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.report.UnisessionResultsReport#getYearDistributionPart()
     */
    public static YearDistributionPart.Path<YearDistributionPart> yearDistributionPart()
    {
        return _dslPath.yearDistributionPart();
    }

    /**
     * @return Дата сдачи мероприятия в ведомости с.
     * @see ru.tandemservice.unisession.entity.report.UnisessionResultsReport#getPerformDateFrom()
     */
    public static PropertyPath<Date> performDateFrom()
    {
        return _dslPath.performDateFrom();
    }

    /**
     * @return Дата сдачи мероприятия в ведомости по.
     * @see ru.tandemservice.unisession.entity.report.UnisessionResultsReport#getPerformDateTo()
     */
    public static PropertyPath<Date> performDateTo()
    {
        return _dslPath.performDateTo();
    }

    /**
     * @return Группировка строк отчета.
     * @see ru.tandemservice.unisession.entity.report.UnisessionResultsReport#getGrouping()
     */
    public static PropertyPath<String> grouping()
    {
        return _dslPath.grouping();
    }

    /**
     * Названия видов дисциплин РУП, выбранных при построении отчета, через запятую.
     *
     * @return Включать дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.report.UnisessionResultsReport#getDiscKinds()
     */
    public static PropertyPath<String> discKinds()
    {
        return _dslPath.discKinds();
    }

    /**
     * @return Деканат.
     * @see ru.tandemservice.unisession.entity.report.UnisessionResultsReport#getGroupOrgUnit()
     */
    public static PropertyPath<String> groupOrgUnit()
    {
        return _dslPath.groupOrgUnit();
    }

    /**
     * @return Территориальное подр..
     * @see ru.tandemservice.unisession.entity.report.UnisessionResultsReport#getTerritorialOrgUnit()
     */
    public static PropertyPath<String> territorialOrgUnit()
    {
        return _dslPath.territorialOrgUnit();
    }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.unisession.entity.report.UnisessionResultsReport#getCompensationType()
     */
    public static PropertyPath<String> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.unisession.entity.report.UnisessionResultsReport#getDevelopForm()
     */
    public static PropertyPath<String> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.unisession.entity.report.UnisessionResultsReport#getDevelopCondition()
     */
    public static PropertyPath<String> developCondition()
    {
        return _dslPath.developCondition();
    }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.unisession.entity.report.UnisessionResultsReport#getDevelopTech()
     */
    public static PropertyPath<String> developTech()
    {
        return _dslPath.developTech();
    }

    /**
     * @return Срок освоения.
     * @see ru.tandemservice.unisession.entity.report.UnisessionResultsReport#getDevelopPeriod()
     */
    public static PropertyPath<String> developPeriod()
    {
        return _dslPath.developPeriod();
    }

    /**
     * @return Курс.
     * @see ru.tandemservice.unisession.entity.report.UnisessionResultsReport#getCourse()
     */
    public static PropertyPath<String> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Состояние студента.
     * @see ru.tandemservice.unisession.entity.report.UnisessionResultsReport#getStudentStatus()
     */
    public static PropertyPath<String> studentStatus()
    {
        return _dslPath.studentStatus();
    }

    /**
     * @return Дополнительный статус.
     * @see ru.tandemservice.unisession.entity.report.UnisessionResultsReport#getCustomState()
     */
    public static PropertyPath<String> customState()
    {
        return _dslPath.customState();
    }

    /**
     * @return Целевой прием.
     * @see ru.tandemservice.unisession.entity.report.UnisessionResultsReport#getTargetAdmission()
     */
    public static PropertyPath<String> targetAdmission()
    {
        return _dslPath.targetAdmission();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.entity.report.UnisessionResultsReport#getFormingDateStr()
     */
    public static SupportedPropertyPath<String> formingDateStr()
    {
        return _dslPath.formingDateStr();
    }

    public static class Path<E extends UnisessionResultsReport> extends EntityPath<E>
    {
        private DatabaseFile.Path<DatabaseFile> _content;
        private PropertyPath<Date> _formingDate;
        private PropertyPath<String> _executor;
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private EducationYear.Path<EducationYear> _educationYear;
        private YearDistributionPart.Path<YearDistributionPart> _yearDistributionPart;
        private PropertyPath<Date> _performDateFrom;
        private PropertyPath<Date> _performDateTo;
        private PropertyPath<String> _grouping;
        private PropertyPath<String> _discKinds;
        private PropertyPath<String> _groupOrgUnit;
        private PropertyPath<String> _territorialOrgUnit;
        private PropertyPath<String> _compensationType;
        private PropertyPath<String> _developForm;
        private PropertyPath<String> _developCondition;
        private PropertyPath<String> _developTech;
        private PropertyPath<String> _developPeriod;
        private PropertyPath<String> _course;
        private PropertyPath<String> _studentStatus;
        private PropertyPath<String> _customState;
        private PropertyPath<String> _targetAdmission;
        private SupportedPropertyPath<String> _formingDateStr;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.report.UnisessionResultsReport#getContent()
     */
        public DatabaseFile.Path<DatabaseFile> content()
        {
            if(_content == null )
                _content = new DatabaseFile.Path<DatabaseFile>(L_CONTENT, this);
            return _content;
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.report.UnisessionResultsReport#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(UnisessionResultsReportGen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * @return Исполнитель.
     * @see ru.tandemservice.unisession.entity.report.UnisessionResultsReport#getExecutor()
     */
        public PropertyPath<String> executor()
        {
            if(_executor == null )
                _executor = new PropertyPath<String>(UnisessionResultsReportGen.P_EXECUTOR, this);
            return _executor;
        }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.unisession.entity.report.UnisessionResultsReport#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.report.UnisessionResultsReport#getEducationYear()
     */
        public EducationYear.Path<EducationYear> educationYear()
        {
            if(_educationYear == null )
                _educationYear = new EducationYear.Path<EducationYear>(L_EDUCATION_YEAR, this);
            return _educationYear;
        }

    /**
     * @return Часть года. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.report.UnisessionResultsReport#getYearDistributionPart()
     */
        public YearDistributionPart.Path<YearDistributionPart> yearDistributionPart()
        {
            if(_yearDistributionPart == null )
                _yearDistributionPart = new YearDistributionPart.Path<YearDistributionPart>(L_YEAR_DISTRIBUTION_PART, this);
            return _yearDistributionPart;
        }

    /**
     * @return Дата сдачи мероприятия в ведомости с.
     * @see ru.tandemservice.unisession.entity.report.UnisessionResultsReport#getPerformDateFrom()
     */
        public PropertyPath<Date> performDateFrom()
        {
            if(_performDateFrom == null )
                _performDateFrom = new PropertyPath<Date>(UnisessionResultsReportGen.P_PERFORM_DATE_FROM, this);
            return _performDateFrom;
        }

    /**
     * @return Дата сдачи мероприятия в ведомости по.
     * @see ru.tandemservice.unisession.entity.report.UnisessionResultsReport#getPerformDateTo()
     */
        public PropertyPath<Date> performDateTo()
        {
            if(_performDateTo == null )
                _performDateTo = new PropertyPath<Date>(UnisessionResultsReportGen.P_PERFORM_DATE_TO, this);
            return _performDateTo;
        }

    /**
     * @return Группировка строк отчета.
     * @see ru.tandemservice.unisession.entity.report.UnisessionResultsReport#getGrouping()
     */
        public PropertyPath<String> grouping()
        {
            if(_grouping == null )
                _grouping = new PropertyPath<String>(UnisessionResultsReportGen.P_GROUPING, this);
            return _grouping;
        }

    /**
     * Названия видов дисциплин РУП, выбранных при построении отчета, через запятую.
     *
     * @return Включать дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.report.UnisessionResultsReport#getDiscKinds()
     */
        public PropertyPath<String> discKinds()
        {
            if(_discKinds == null )
                _discKinds = new PropertyPath<String>(UnisessionResultsReportGen.P_DISC_KINDS, this);
            return _discKinds;
        }

    /**
     * @return Деканат.
     * @see ru.tandemservice.unisession.entity.report.UnisessionResultsReport#getGroupOrgUnit()
     */
        public PropertyPath<String> groupOrgUnit()
        {
            if(_groupOrgUnit == null )
                _groupOrgUnit = new PropertyPath<String>(UnisessionResultsReportGen.P_GROUP_ORG_UNIT, this);
            return _groupOrgUnit;
        }

    /**
     * @return Территориальное подр..
     * @see ru.tandemservice.unisession.entity.report.UnisessionResultsReport#getTerritorialOrgUnit()
     */
        public PropertyPath<String> territorialOrgUnit()
        {
            if(_territorialOrgUnit == null )
                _territorialOrgUnit = new PropertyPath<String>(UnisessionResultsReportGen.P_TERRITORIAL_ORG_UNIT, this);
            return _territorialOrgUnit;
        }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.unisession.entity.report.UnisessionResultsReport#getCompensationType()
     */
        public PropertyPath<String> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new PropertyPath<String>(UnisessionResultsReportGen.P_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.unisession.entity.report.UnisessionResultsReport#getDevelopForm()
     */
        public PropertyPath<String> developForm()
        {
            if(_developForm == null )
                _developForm = new PropertyPath<String>(UnisessionResultsReportGen.P_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.unisession.entity.report.UnisessionResultsReport#getDevelopCondition()
     */
        public PropertyPath<String> developCondition()
        {
            if(_developCondition == null )
                _developCondition = new PropertyPath<String>(UnisessionResultsReportGen.P_DEVELOP_CONDITION, this);
            return _developCondition;
        }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.unisession.entity.report.UnisessionResultsReport#getDevelopTech()
     */
        public PropertyPath<String> developTech()
        {
            if(_developTech == null )
                _developTech = new PropertyPath<String>(UnisessionResultsReportGen.P_DEVELOP_TECH, this);
            return _developTech;
        }

    /**
     * @return Срок освоения.
     * @see ru.tandemservice.unisession.entity.report.UnisessionResultsReport#getDevelopPeriod()
     */
        public PropertyPath<String> developPeriod()
        {
            if(_developPeriod == null )
                _developPeriod = new PropertyPath<String>(UnisessionResultsReportGen.P_DEVELOP_PERIOD, this);
            return _developPeriod;
        }

    /**
     * @return Курс.
     * @see ru.tandemservice.unisession.entity.report.UnisessionResultsReport#getCourse()
     */
        public PropertyPath<String> course()
        {
            if(_course == null )
                _course = new PropertyPath<String>(UnisessionResultsReportGen.P_COURSE, this);
            return _course;
        }

    /**
     * @return Состояние студента.
     * @see ru.tandemservice.unisession.entity.report.UnisessionResultsReport#getStudentStatus()
     */
        public PropertyPath<String> studentStatus()
        {
            if(_studentStatus == null )
                _studentStatus = new PropertyPath<String>(UnisessionResultsReportGen.P_STUDENT_STATUS, this);
            return _studentStatus;
        }

    /**
     * @return Дополнительный статус.
     * @see ru.tandemservice.unisession.entity.report.UnisessionResultsReport#getCustomState()
     */
        public PropertyPath<String> customState()
        {
            if(_customState == null )
                _customState = new PropertyPath<String>(UnisessionResultsReportGen.P_CUSTOM_STATE, this);
            return _customState;
        }

    /**
     * @return Целевой прием.
     * @see ru.tandemservice.unisession.entity.report.UnisessionResultsReport#getTargetAdmission()
     */
        public PropertyPath<String> targetAdmission()
        {
            if(_targetAdmission == null )
                _targetAdmission = new PropertyPath<String>(UnisessionResultsReportGen.P_TARGET_ADMISSION, this);
            return _targetAdmission;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.entity.report.UnisessionResultsReport#getFormingDateStr()
     */
        public SupportedPropertyPath<String> formingDateStr()
        {
            if(_formingDateStr == null )
                _formingDateStr = new SupportedPropertyPath<String>(UnisessionResultsReportGen.P_FORMING_DATE_STR, this);
            return _formingDateStr;
        }

        public Class getEntityClass()
        {
            return UnisessionResultsReport.class;
        }

        public String getEntityName()
        {
            return "unisessionResultsReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getFormingDateStr();
}
