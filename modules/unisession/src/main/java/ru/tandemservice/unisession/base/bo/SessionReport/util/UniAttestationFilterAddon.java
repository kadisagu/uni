/* $Id:$ */
package ru.tandemservice.unisession.base.bo.SessionReport.util;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Transformer;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.*;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import org.tandemframework.tapsupport.component.selection.SimpleListResultBuilder;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.StudentCustomState;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlot;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import static java.util.Arrays.asList;
import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 22.07.2015
 */
public class UniAttestationFilterAddon extends CommonFilterAddon<SessionAttestationSlot>  {

    public UniAttestationFilterAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @Override
    public Class<SessionAttestationSlot> getEntityClass() {
        return SessionAttestationSlot
                .class;
    }

    public static final String SETTINGS_NAME_COURSE = "course";
    public static final String SETTINGS_NAME_GROUP_WITH_NOGROUP = "groupsWithNoGroup";
    public static final String SETTINGS_NAME_STUDENT_STATUS = "studentStatus";
    public static final String SETTINGS_NAME_TARGET_ADMISSION = "targetAdmission";
    public static final String SETTINGS_NAME_COMPENSATION_TYPE = "compensationType";
    public static final String SETTINGS_NAME_DEVELOP_FORM = "developForm";
    public static final String SETTINGS_NAME_DEVELOP_CONDITION = "developCondition";
    public static final String SETTINGS_NAME_DEVELOP_PERIOD = "developPeriod";
    public static final String SETTINGS_NAME_DEVELOP_TECH = "developTech";
    public static final String SETTINGS_NAME_CUSTOM_STATE = "customState";
    public static final String SETTINGS_NAME_DISCIPLINE = "discipline";


    public static final CommonFilterContentConfig<Course> COURSE = new CommonFilterContentConfig<>(Course.class, SETTINGS_NAME_COURSE, "Курс", SessionAttestationSlot.studentWpe().student().course(), Course.title(), asList(Course.title()), asList(Course.title()));

    public static final CommonFilterContentConfig<StudentStatus> STUDENT_STATUS = new CommonFilterContentConfig<>(StudentStatus.class, SETTINGS_NAME_STUDENT_STATUS, "Состояние студента", SessionAttestationSlot.studentWpe().student().status(), StudentStatus.title(), asList(StudentStatus.title()), asList(StudentStatus.title()));
    public static final CommonFilterContentConfig<CompensationType> COMPENSATION_TYPE = new CommonFilterContentConfig<>(CompensationType.class, SETTINGS_NAME_COMPENSATION_TYPE, "Вид возмещения затрат", SessionAttestationSlot.studentWpe().student().compensationType(), CompensationType.title(), asList(CompensationType.title()), asList(CompensationType.title()));

    public static final CommonFilterContentConfig<DevelopForm> DEVELOP_FORM = new CommonFilterContentConfig<>(DevelopForm.class, SETTINGS_NAME_DEVELOP_FORM, "Форма освоения", SessionAttestationSlot.studentWpe().student().educationOrgUnit().developForm(), DevelopForm.title(), asList(DevelopForm.title()), asList(DevelopForm.title()));
    public static final CommonFilterContentConfig<DevelopCondition> DEVELOP_CONDITION = new CommonFilterContentConfig<>(DevelopCondition.class, SETTINGS_NAME_DEVELOP_CONDITION, "Условие освоения", SessionAttestationSlot.studentWpe().student().educationOrgUnit().developCondition(), DevelopCondition.title(), asList(DevelopCondition.title()), asList(DevelopCondition.title()));
    public static final CommonFilterContentConfig<DevelopPeriod> DEVELOP_PERIOD = new CommonFilterContentConfig<>(DevelopPeriod.class, SETTINGS_NAME_DEVELOP_PERIOD, "Срок освоения", SessionAttestationSlot.studentWpe().student().educationOrgUnit().developPeriod(), DevelopPeriod.title(), asList(DevelopPeriod.title()), asList(DevelopPeriod.title()));
    public static final CommonFilterContentConfig<DevelopTech> DEVELOP_TECH = new CommonFilterContentConfig<>(DevelopTech.class, SETTINGS_NAME_DEVELOP_TECH, "Технология освоения", SessionAttestationSlot.studentWpe().student().educationOrgUnit().developTech(), DevelopTech.title(), asList(DevelopTech.title()), asList(DevelopTech.title()));


    public static final ICommonFilterFactory<IEntity> STUDENT_CUSTOM_STATE = (StudentCustomStateFilterItem::new);

    public static final ICommonFilterFactory<IEntity> TARGET_ADMISSION = (TargetAdmissionFilterItem::new);

    public static final ICommonFilterFactory<IEntity> GROUP_WITH_NO_GROUP = (GroupsWithNoGroupFilterItem::new);

    public static final ICommonFilterFactory<IEntity> DISCIPLINE = (DisciplineFilterItem::new);

    public static class DisciplineFilterItem extends CustomFilterItem
    {

        public DisciplineFilterItem(CommonFilterFormConfig config) {
            super(config, "Дисциплина", SETTINGS_NAME_DISCIPLINE);
        }

        @Override
        @SuppressWarnings("unchecked")
        public void apply(DQLSelectBuilder builder, String alias) {
            final Object value = getValue();

            if (value instanceof List) {

                builder.where(in(
                        property(alias, SessionAttestationSlot.bulletin().registryElementPart().id()),
                        (CollectionUtils.collect((List<IdentifiableWrapper>) value, IdentifiableWrapper::getId))));

            } else if (value instanceof EppRegistryElementPart)
            {
                builder.where(eq(
                        property(alias, SessionAttestationSlot.bulletin().registryElementPart()),
                        value((EppRegistryElementPart)value)));
            }
        }

        @Override
        public void init(CommonFilterAddon commonFilterAddon, String alias) {
            if (getModel() != null) return;

            setModel(new CommonFilterSelectModel() {
                @Override
                protected IListResultBuilder createBuilder(String filter, Object o) {
                    DQLSelectBuilder builder = commonFilterAddon.prepareEntityBuilder("s", DisciplineFilterItem.this);

                    builder.joinPath(DQLJoinType.inner, SessionAttestationSlot.bulletin().registryElementPart().fromAlias("s"), "disc");

                    if (!StringUtils.isEmpty(filter))
                        builder.where(
                                or(
                                        likeUpper(property("disc", EppRegistryElementPart.registryElement().title()), value(CoreStringUtils.escapeLike(filter.toUpperCase()))),
                                        likeUpper(property("disc", EppRegistryElementPart.number()), value(CoreStringUtils.escapeLike(filter.toUpperCase()))),
                                        likeUpper(property("disc", EppRegistryElementPart.registryElement().size()), value(CoreStringUtils.escapeLike(filter.toUpperCase()))),
                                        likeUpper(property("disc", EppRegistryElementPart.registryElement().parts()), value(CoreStringUtils.escapeLike(filter.toUpperCase())))
                                )
                        );
                    if (o != null) {
                        builder.where(((o instanceof Collection ? CommonFilterAdditionalFilterType.COLLECTION : CommonFilterAdditionalFilterType.EQ).whereExpression("disc", EppRegistryElementPart.id(), o)));
                    }


                    builder
                            .order(property("disc", EppRegistryElementPart.registryElement().title()))
                            .order(property("disc", EppRegistryElementPart.number()))
                            .order(property("disc", EppRegistryElementPart.registryElement().size()))
                            .order(property("disc", EppRegistryElementPart.registryElement().parts()));


                    builder.column(property(SessionAttestationSlot.bulletin().registryElementPart().fromAlias("s")));

                    final List<EppRegistryElementPart> list = builder.createStatement(commonFilterAddon.getSession()).list();

                    Set<DisciplineWrapper> wrappedSet = new TreeSet<>((DisciplineWrapper a, DisciplineWrapper b) ->
                            {
                                int res = a.getTitle().compareToIgnoreCase(b.getTitle());
                                if (res != 0) return res;
                                return a.getId().compareTo(b.getId());
                            }
                    );

                    list.forEach((EppRegistryElementPart element)->wrappedSet.add(new DisciplineWrapper<>(element, element.getTitleWithNumber())));

                    return new SimpleListResultBuilder<>(wrappedSet);
                }
            });
        }
    }


    public static class StudentCustomStateFilterItem extends CustomFilterItem
    {
        public StudentCustomStateFilterItem(CommonFilterFormConfig config) {
            super(config, "Дополнительный статус", SETTINGS_NAME_CUSTOM_STATE);
        }

        @Override
        public void apply(DQLSelectBuilder builder, String alias) {

            final DQLSelectBuilder customStateDQL = new DQLSelectBuilder()
                    .fromEntity(StudentCustomState.class, "scs")
                    .column(value(1))
                    .where(eq(property("scs", StudentCustomState.student()), property(alias, SessionAttestationSlot.studentWpe().student())));

            FilterUtils.applySelectFilter(customStateDQL, "scs", StudentCustomState.customState(), getValue());

            builder.where(exists(customStateDQL.buildQuery()));
        }


        @Override
        public void init(final CommonFilterAddon commonFilterAddon, final String alias) {
            if (getModel() != null) return;

            setModel(new CommonFilterSelectModel() {
                @Override
                protected IListResultBuilder createBuilder(String filter, Object o) {
                    DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentCustomStateCI.class, "s");

                    FilterUtils.applyLikeFilter(builder, filter, StudentCustomStateCI.title().fromAlias("s"));
                    FilterUtils.applySelectFilter(builder, "s", StudentCustomStateCI.id(), o);

                    return new DQLListResultBuilder(builder);
                }
            });

        }
    }


    public static class TargetAdmissionFilterItem extends CustomFilterItem
    {
        public TargetAdmissionFilterItem(CommonFilterFormConfig config) {
            super(config, "Целевой прием", SETTINGS_NAME_TARGET_ADMISSION);
        }

        @Override
        public void apply(DQLSelectBuilder builder, String alias) {
            final Object value = getValue();
            if (value instanceof List) {
                return; //ничего не делаем, мультселект не используется
            } else if (value instanceof DataWrapper)
            {
                final DataWrapper wrapper = (DataWrapper) value;
                boolean targetAdmission = TwinComboDataSourceHandler.getSelectedValueNotNull(wrapper);
                builder.where(eq(
                        property(alias, SessionAttestationSlot.studentWpe().student().targetAdmission()), value(targetAdmission)));
            }
        }

        @Override
        public void init(CommonFilterAddon commonFilterAddon, String alias) {
            if (getModel() != null) return;

            setModel(TwinComboDataSourceHandler.getYesNoDefaultSelectModel());
        }
    }


    public static class GroupsWithNoGroupFilterItem extends CustomFilterItem
    {
        public GroupsWithNoGroupFilterItem(CommonFilterFormConfig config) {
            super(config, "Академическая группа", SETTINGS_NAME_GROUP_WITH_NOGROUP);
        }

        @Override
        public void apply(DQLSelectBuilder builder, String alias) {
            final Object value = getValue();
            if (value instanceof List) {
                if (((List) value).contains(GroupModel.WRAPPER_NO_GROUP))
                    builder.where(or(
                            isNull(property(alias, SessionAttestationSlot.studentWpe().student().group())),
                            in(property(alias, SessionAttestationSlot.studentWpe().student().group().id()), CollectionUtils.collect((List)value, new Transformer<IdentifiableWrapper, Long>() {
                                @Override
                                public Long transform(IdentifiableWrapper identifiableWrapper) {
                                    return identifiableWrapper.getId();
                                }
                            }))
                    ));
                else builder.where(
                        in(property(alias, SessionAttestationSlot.studentWpe().student().group().id()), CollectionUtils.collect((List) value, new Transformer<IdentifiableWrapper, Long>() {
                            @Override
                            public Long transform(IdentifiableWrapper identifiableWrapper) {
                                return identifiableWrapper.getId();
                            }
                        })));
            } else if (value instanceof IdentifiableWrapper)
            {
                if (GroupModel.WRAPPER_NO_GROUP.equals(value))
                {
                    builder.where(isNull(property(alias, SessionAttestationSlot.studentWpe().student().group())));
                } else {
                    builder.where(eq(
                            property(alias, SessionAttestationSlot.studentWpe().student().group().id()), value(((IdentifiableWrapper) value).getId())));
                }
            }
        }

        @Override
        public void init(final CommonFilterAddon commonFilterAddon, String alias) {
            if (getModel() != null) return;

            setModel(new CommonFilterSelectModel() {
                @Override
                protected IListResultBuilder createBuilder(String filter, Object o) {

                    DQLSelectBuilder builder = commonFilterAddon.prepareEntityBuilder("s", GroupsWithNoGroupFilterItem.this);

                    if (!StringUtils.isEmpty(filter))
                        builder.where(likeUpper(property("s", SessionAttestationSlot.studentWpe().student().group().title()), value(CoreStringUtils.escapeLike(filter.toUpperCase()))));
                    if (o != null) {
                        builder.where(((o instanceof Collection ? CommonFilterAdditionalFilterType.COLLECTION : CommonFilterAdditionalFilterType.EQ).whereExpression("s", SessionAttestationSlot.studentWpe().student().group().id(), o)));
                    }

                    builder.column(property("s", SessionAttestationSlot.studentWpe().student().group()))
                            .column(property("s", SessionAttestationSlot.studentWpe().student()));


                    final List<Object[]> list = builder.createStatement(commonFilterAddon.getSession()).list();

                    Set<IdentifiableWrapper> groups = new TreeSet<>((o1, o2) -> {
                        if (o1 == o2)
                            return 0;
                        if (null == o1)
                            return 1;
                        if (null == o2)
                            return -1;

                        int i = o1.getTitle().compareToIgnoreCase(o2.getTitle());
                        if (i != 0) return i;
                        return o1.getId().compareTo(o2.getId());
                    });

                    boolean containsNoGroup = false;

                    for (Object[] objects : list) {
                        groups.add(new IdentifiableWrapper<>((Group)objects[0]));
                        if (null == objects[0])
                            containsNoGroup = true;
                    }
                    if (containsNoGroup)
                    {
                        groups.add(GroupModel.WRAPPER_NO_GROUP);
                    }

                    return new SimpleListResultBuilder<>(groups);
                }
            });
        }
    }

    public static class DisciplineWrapper<T extends EppRegistryElementPart> extends IdentifiableWrapper
    {
        private T value;

        DisciplineWrapper(T entity, String title)
        {
            super(entity, title);
            value = entity;
        }

        public T getValue() {
            return value;
        }
    }

}
