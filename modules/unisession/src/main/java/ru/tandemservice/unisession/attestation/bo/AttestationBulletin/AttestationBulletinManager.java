/* $Id:$ */
package ru.tandemservice.unisession.attestation.bo.AttestationBulletin;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unisession.attestation.bo.Attestation.AttestationManager;
import ru.tandemservice.unisession.attestation.bo.AttestationBulletin.logic.ISessionAttestationBulletinDao;
import ru.tandemservice.unisession.attestation.bo.AttestationBulletin.logic.SessionAttestationBulletinDao;
import ru.tandemservice.unisession.attestation.bo.AttestationBulletin.print.ISessionAttestationBulletinPrintDao;
import ru.tandemservice.unisession.attestation.bo.AttestationBulletin.print.SessionAttestationBulletinPrintDao;
import ru.tandemservice.unisession.attestation.entity.SessionAttestation;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlot;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 10/4/12
 */
@Configuration
public class AttestationBulletinManager extends BusinessObjectManager
{
    public static final String DS_OWNER_ORGUNIT = "ownerOrgUnitDS";
    public static final String DS_DISCIPLINE = "disciplineDS";
    public static final String DS_PPS = "ppsDS";
    public static final String DS_GROUP = "groupDS";
    public static final String DS_BULL_STATUS = "bullStatusDS";
    public static final String DS_REGISTRY_STRYCTURE = "registryStructureDS";

    // filters
    public static final String BIND_PPS_PERSON = "ppsPerson";
    public static final String BIND_ATTESTATION = "attestation";
    public static final String BIND_ATTESTATION_NUMBER = "attestationNumber";
    public static final String BIND_DISCIPLINE_LIST = "disciplineList";
    public static final String BIND_PPS_LIST = "ppsList";
    public static final String BIND_OWNER_ORGUNIT_LIST = "ownerOrgUnitList";
    public static final String BIND_COURSE_LIST = "courseList";
    public static final String BIND_GROUP_LIST = "groupList";
    public static final String BIND_PASSED = "passed";
    public static final String BIND_BULL_STATUS = "bullStatus";
    public static final String BIND_DOCUMENT_NUMBER = "documentNumber";
    public static final String BIND_REGISTRY_STRYCTURE = "registryStructure";
    public static final String BIND_STUDENT_LAST_NAME = "studentLastName";
    public static final String BIND_PPS_LAST_NAME = "ppsLastName";

    public static final Long BULL_STATUS_EMPTY_ID = 1L;
    public static final Long BULL_STATUS_OPEN_ID = 2L;
    public static final Long BULL_STATUS_CLOSE_ID = 3L;


    public static AttestationBulletinManager instance()
    {
        return instance(AttestationBulletinManager.class);
    }

    @Bean
    public ISessionAttestationBulletinDao dao()
    {
        return new SessionAttestationBulletinDao();
    }

    @Bean
    public ISessionAttestationBulletinPrintDao printDao()
    {
        return new SessionAttestationBulletinPrintDao();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> ownerOrgUnitDSHandler()
    {
        EntityComboDataSourceHandler handler = new EntityComboDataSourceHandler(getName(), OrgUnit.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                final SessionAttestation attestation = context.get(AttestationBulletinManager.BIND_ATTESTATION);
                final Person ppsPerson = context.get(AttestationBulletinManager.BIND_PPS_PERSON);
                final EducationYear eduYear = context.get(AttestationManager.PARAM_EDU_YEAR);
                final YearDistributionPart yearPart = context.get(AttestationManager.PARAM_YEAR_PART);

                DQLSelectBuilder subDql = new DQLSelectBuilder().fromEntity(SessionAttestationBulletin.class, "r").column(property(SessionAttestationBulletin.registryElementPart().registryElement().owner().id().fromAlias("r")))
                        .predicate(DQLPredicateType.distinct);

                if (null != ppsPerson)
                {
                    final DQLSelectBuilder tutorDQL = new DQLSelectBuilder().fromEntity(SessionComissionPps.class, "rel").column(property(SessionComissionPps.commission().id().fromAlias("rel")))
                            .where(eq(property(SessionComissionPps.pps().person().fromAlias("rel")), value(ppsPerson)))
                            .predicate(DQLPredicateType.distinct);

                    subDql.where(in(property(SessionAttestationBulletin.commission().id().fromAlias("r")), tutorDQL.buildQuery()));
                }

                if (attestation != null)
                {
                    subDql.where(eq(property(SessionAttestationBulletin.attestation().fromAlias("r")), value(attestation)));
                }
                else
                {
                    subDql
                            .where(eq(property(SessionAttestationBulletin.attestation().sessionObject().educationYear().fromAlias("r")), value(eduYear)))
                            .where(eq(property(SessionAttestationBulletin.attestation().sessionObject().yearDistributionPart().fromAlias("r")), value(yearPart)));
                }

                dql.where(in(
                        property(OrgUnit.id().fromAlias(alias)),
                        subDql.buildQuery()));
            }
        };
        handler.filter(OrgUnit.title());
        handler.order(OrgUnit.title());
        handler.setPageable(true);

        return handler;
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> disciplineDSHandler()
    {
        EntityComboDataSourceHandler handler = new EntityComboDataSourceHandler(getName(), EppRegistryElementPart.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                final EducationYear eduYear = context.get(AttestationManager.PARAM_EDU_YEAR);
                final YearDistributionPart yearPart = context.get(AttestationManager.PARAM_YEAR_PART);
                final Person ppsPerson = context.get(AttestationBulletinManager.BIND_PPS_PERSON);
                final SessionAttestation attestation = context.get(AttestationBulletinManager.BIND_ATTESTATION);
                final List<OrgUnit> orgUnitList = context.get(AttestationBulletinManager.BIND_OWNER_ORGUNIT_LIST);
                final List<EppRegistryStructure> registryStructureList = context.get(AttestationBulletinManager.BIND_REGISTRY_STRYCTURE);

                DQLSelectBuilder includeBuilder = new DQLSelectBuilder().fromEntity(SessionAttestationBulletin.class, "r").column(property(SessionAttestationBulletin.registryElementPart().id().fromAlias("r")))
                    .predicate(DQLPredicateType.distinct);

                if (null != ppsPerson)
                {
                    final DQLSelectBuilder tutorDQL = new DQLSelectBuilder().fromEntity(SessionComissionPps.class, "rel").column(property(SessionComissionPps.commission().id().fromAlias("rel")))
                            .where(eq(property(SessionComissionPps.pps().person().fromAlias("rel")), value(ppsPerson)))
                            .predicate(DQLPredicateType.distinct);

                    includeBuilder.where(in(property(SessionAttestationBulletin.commission().id().fromAlias("r")), tutorDQL.buildQuery()));
                }

                if (attestation != null)
                {
                    includeBuilder.where(eq(property(SessionAttestationBulletin.attestation().fromAlias("r")), value(attestation)));
                }
                else
                {
                    includeBuilder
                            .where(eq(property(SessionAttestationBulletin.attestation().sessionObject().educationYear().fromAlias("r")), value(eduYear)))
                            .where(eq(property(SessionAttestationBulletin.attestation().sessionObject().yearDistributionPart().fromAlias("r")), value(yearPart)));
                }

                if (orgUnitList != null && !orgUnitList.isEmpty()) {
                    includeBuilder.where(in(property(SessionAttestationBulletin.registryElementPart().registryElement().owner().id().fromAlias("r")), UniBaseDao.ids(orgUnitList)));
                }

                if (registryStructureList != null && !registryStructureList.isEmpty())
                {
                    includeBuilder.where(in(property(SessionAttestationBulletin.registryElementPart().registryElement().parent().id().fromAlias("r")), UniBaseDao.ids(registryStructureList)));
                }

                dql.where(in(
                    property(EppRegistryElementPart.id().fromAlias(alias)),
                    includeBuilder.buildQuery()
                ));
            }
        };

        handler.filter(EppRegistryElementPart.registryElement().title());
        handler.order(EppRegistryElementPart.registryElement().title());
        handler.setPageable(true);

        return handler;
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> ppsDSHandler()
    {
        EntityComboDataSourceHandler handler = new EntityComboDataSourceHandler(getName(), PpsEntry.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                final SessionAttestation attestation = context.get(AttestationBulletinManager.BIND_ATTESTATION);
                final List<OrgUnit> orgUnitList = context.get(AttestationBulletinManager.BIND_OWNER_ORGUNIT_LIST);

                DQLSelectBuilder commissionBuilder = new DQLSelectBuilder().fromEntity(SessionAttestationBulletin.class, "r").column(property(SessionAttestationBulletin.commission().id().fromAlias("r")))
                    .where(eq(property(SessionAttestationBulletin.attestation().fromAlias("r")), value(attestation)));

                if ((orgUnitList != null && !orgUnitList.isEmpty())) {
                    commissionBuilder.where(in(property(SessionAttestationBulletin.registryElementPart().registryElement().owner().id().fromAlias("r")), UniBaseDao.ids(orgUnitList)));
                }

                DQLSelectBuilder includeBuilder = new DQLSelectBuilder().fromEntity(SessionComissionPps.class, "b").column(property(SessionComissionPps.pps().id().fromAlias("b")))
                    .where(in(property(SessionComissionPps.commission().id().fromAlias("b")), commissionBuilder.buildQuery()))
                    .predicate(DQLPredicateType.distinct);

                dql.where(in(
                    property(PpsEntry.id().fromAlias(alias)),
                    includeBuilder.buildQuery()
                ));
            }
        };
        handler.order(PpsEntry.person().identityCard().fullFio());
        handler.filter(PpsEntry.person().identityCard().fullFio());
        handler.setPageable(true);

        return handler;
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> groupDSHandler()
    {
        EntityComboDataSourceHandler handler = new EntityComboDataSourceHandler(getName(), Group.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                final EducationYear eduYear = context.get(AttestationManager.PARAM_EDU_YEAR);
                final YearDistributionPart yearPart = context.get(AttestationManager.PARAM_YEAR_PART);
                final Person ppsPerson = context.get(AttestationBulletinManager.BIND_PPS_PERSON);
                final SessionAttestation attestation = context.get(AttestationBulletinManager.BIND_ATTESTATION);
                final List<Course> courseList = context.get(AttestationBulletinManager.BIND_COURSE_LIST);

                DQLSelectBuilder includeBuilder = new DQLSelectBuilder().fromEntity(SessionAttestationSlot.class, "r").column(property(SessionAttestationSlot.studentWpe().student().group().id().fromAlias("r")))
                    .predicate(DQLPredicateType.distinct);

                if (null != ppsPerson)
                {
                    final DQLSelectBuilder tutorDQL = new DQLSelectBuilder().fromEntity(SessionComissionPps.class, "rel").column(property(SessionComissionPps.commission().id().fromAlias("rel")))
                            .where(eq(property(SessionComissionPps.pps().person().fromAlias("rel")), value(ppsPerson)))
                            .predicate(DQLPredicateType.distinct);

                    includeBuilder.where(in(property(SessionAttestationSlot.bulletin().commission().id().fromAlias("r")), tutorDQL.buildQuery()));
                }

                if (attestation != null)
                {
                    includeBuilder.where(eq(property(SessionAttestationSlot.bulletin().attestation().fromAlias("r")), value(attestation)));
                }
                else
                {
                    includeBuilder
                            .where(eq(property(SessionAttestationSlot.bulletin().attestation().sessionObject().educationYear().fromAlias("r")), value(eduYear)))
                            .where(eq(property(SessionAttestationSlot.bulletin().attestation().sessionObject().yearDistributionPart().fromAlias("r")), value(yearPart)));
                }

                if (courseList != null && !courseList.isEmpty()) {
                    includeBuilder.where(in(property(SessionAttestationSlot.studentWpe().student().course().id().fromAlias("r")), UniBaseDao.ids(courseList)));
                }

                dql.where(in(
                    property(Group.id().fromAlias(alias)),
                    includeBuilder.buildQuery()));
            }
        };

        handler.filter(Group.title());
        handler.order(Group.title());
        handler.setPageable(true);

        return handler;
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> bullStatusDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
                .addRecord(BULL_STATUS_EMPTY_ID, "Незаполненная")
                .addRecord(BULL_STATUS_OPEN_ID, "Открытая")
                .addRecord(BULL_STATUS_CLOSE_ID, "Закрытая")
                .filtered(true);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> registryStructureDSHandler()
    {
        EntityComboDataSourceHandler handler = new EntityComboDataSourceHandler(getName(), EppRegistryStructure.class);
        handler.setPageable(true);
        handler.filter(EppRegistryStructure.title());
        handler.order(EppRegistryStructure.title());
        return handler;
    }
}