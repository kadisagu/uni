package ru.tandemservice.unisession.entity.document;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.unisession.entity.document.gen.SessionTransferInsideDocumentGen;

/**
 * Внутреннее перезачтение
 */
public class SessionTransferInsideDocument extends SessionTransferInsideDocumentGen
{
    @Override
    @EntityDSLSupport(parts=SessionDocument.P_NUMBER)
    public String getTypeTitle() { return "Внутреннее перезачтение №" + this.getNumber(); }

    @Override
    @EntityDSLSupport(parts=SessionDocument.P_NUMBER)
    public String getTypeShortTitle()
    {
        return "Внутр. перезачт. №" + this.getNumber();
    }
}