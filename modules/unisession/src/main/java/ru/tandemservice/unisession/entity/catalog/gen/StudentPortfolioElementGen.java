package ru.tandemservice.unisession.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.entity.catalog.StudentPortfolioElement;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Элемент портфолио студента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentPortfolioElementGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.catalog.StudentPortfolioElement";
    public static final String ENTITY_NAME = "studentPortfolioElement";
    public static final int VERSION_HASH = -275551364;
    private static IEntityMeta ENTITY_META;

    public static final String P_TITLE = "title";
    public static final String P_EDIT_DATE = "editDate";
    public static final String L_STUDENT = "student";
    public static final String L_WPE_C_ACTION = "wpeCAction";
    public static final String P_FILE = "file";

    private String _title;     // Название
    private Date _editDate;     // Дата изменения
    private Student _student;     // Студент
    private EppStudentWpeCAction _wpeCAction;     // МСРП-ФК
    private Long _file;     // Файл

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Дата изменения. Свойство не может быть null.
     */
    @NotNull
    public Date getEditDate()
    {
        return _editDate;
    }

    /**
     * @param editDate Дата изменения. Свойство не может быть null.
     */
    public void setEditDate(Date editDate)
    {
        dirty(_editDate, editDate);
        _editDate = editDate;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return МСРП-ФК.
     */
    public EppStudentWpeCAction getWpeCAction()
    {
        return _wpeCAction;
    }

    /**
     * @param wpeCAction МСРП-ФК.
     */
    public void setWpeCAction(EppStudentWpeCAction wpeCAction)
    {
        dirty(_wpeCAction, wpeCAction);
        _wpeCAction = wpeCAction;
    }

    /**
     * @return Файл.
     */
    public Long getFile()
    {
        return _file;
    }

    /**
     * @param file Файл.
     */
    public void setFile(Long file)
    {
        dirty(_file, file);
        _file = file;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StudentPortfolioElementGen)
        {
            setTitle(((StudentPortfolioElement)another).getTitle());
            setEditDate(((StudentPortfolioElement)another).getEditDate());
            setStudent(((StudentPortfolioElement)another).getStudent());
            setWpeCAction(((StudentPortfolioElement)another).getWpeCAction());
            setFile(((StudentPortfolioElement)another).getFile());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentPortfolioElementGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentPortfolioElement.class;
        }

        public T newInstance()
        {
            return (T) new StudentPortfolioElement();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "title":
                    return obj.getTitle();
                case "editDate":
                    return obj.getEditDate();
                case "student":
                    return obj.getStudent();
                case "wpeCAction":
                    return obj.getWpeCAction();
                case "file":
                    return obj.getFile();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "editDate":
                    obj.setEditDate((Date) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "wpeCAction":
                    obj.setWpeCAction((EppStudentWpeCAction) value);
                    return;
                case "file":
                    obj.setFile((Long) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "title":
                        return true;
                case "editDate":
                        return true;
                case "student":
                        return true;
                case "wpeCAction":
                        return true;
                case "file":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "title":
                    return true;
                case "editDate":
                    return true;
                case "student":
                    return true;
                case "wpeCAction":
                    return true;
                case "file":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "title":
                    return String.class;
                case "editDate":
                    return Date.class;
                case "student":
                    return Student.class;
                case "wpeCAction":
                    return EppStudentWpeCAction.class;
                case "file":
                    return Long.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StudentPortfolioElement> _dslPath = new Path<StudentPortfolioElement>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentPortfolioElement");
    }
            

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.catalog.StudentPortfolioElement#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Дата изменения. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.catalog.StudentPortfolioElement#getEditDate()
     */
    public static PropertyPath<Date> editDate()
    {
        return _dslPath.editDate();
    }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.catalog.StudentPortfolioElement#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return МСРП-ФК.
     * @see ru.tandemservice.unisession.entity.catalog.StudentPortfolioElement#getWpeCAction()
     */
    public static EppStudentWpeCAction.Path<EppStudentWpeCAction> wpeCAction()
    {
        return _dslPath.wpeCAction();
    }

    /**
     * @return Файл.
     * @see ru.tandemservice.unisession.entity.catalog.StudentPortfolioElement#getFile()
     */
    public static PropertyPath<Long> file()
    {
        return _dslPath.file();
    }

    public static class Path<E extends StudentPortfolioElement> extends EntityPath<E>
    {
        private PropertyPath<String> _title;
        private PropertyPath<Date> _editDate;
        private Student.Path<Student> _student;
        private EppStudentWpeCAction.Path<EppStudentWpeCAction> _wpeCAction;
        private PropertyPath<Long> _file;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.catalog.StudentPortfolioElement#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(StudentPortfolioElementGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Дата изменения. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.catalog.StudentPortfolioElement#getEditDate()
     */
        public PropertyPath<Date> editDate()
        {
            if(_editDate == null )
                _editDate = new PropertyPath<Date>(StudentPortfolioElementGen.P_EDIT_DATE, this);
            return _editDate;
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.catalog.StudentPortfolioElement#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return МСРП-ФК.
     * @see ru.tandemservice.unisession.entity.catalog.StudentPortfolioElement#getWpeCAction()
     */
        public EppStudentWpeCAction.Path<EppStudentWpeCAction> wpeCAction()
        {
            if(_wpeCAction == null )
                _wpeCAction = new EppStudentWpeCAction.Path<EppStudentWpeCAction>(L_WPE_C_ACTION, this);
            return _wpeCAction;
        }

    /**
     * @return Файл.
     * @see ru.tandemservice.unisession.entity.catalog.StudentPortfolioElement#getFile()
     */
        public PropertyPath<Long> file()
        {
            if(_file == null )
                _file = new PropertyPath<Long>(StudentPortfolioElementGen.P_FILE, this);
            return _file;
        }

        public Class getEntityClass()
        {
            return StudentPortfolioElement.class;
        }

        public String getEntityName()
        {
            return "studentPortfolioElement";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
