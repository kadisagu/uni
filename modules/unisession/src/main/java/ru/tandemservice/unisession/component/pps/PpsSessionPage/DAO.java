/* $Id$ */
package ru.tandemservice.unisession.component.pps.PpsSessionPage;

import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.pps.IPpsEntryDao;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;

import java.util.List;

/**
 * @author oleyba
 * @since 4/21/11
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setPerson(PersonManager.instance().dao().getPerson(model.getPrincipalContext()));
        final List<PpsEntry> ppsList = IPpsEntryDao.instance.get().getPpsList(model.getPrincipalContext());
        model.setPpsFound(!ppsList.isEmpty());
    }
}
