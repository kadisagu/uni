package ru.tandemservice.unisession.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unisession.entity.catalog.UnisessionCommonTemplate;
import ru.tandemservice.unisession.entity.catalog.UnisessionTemplate;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Печатные шаблоны общих документов «Сессия»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UnisessionCommonTemplateGen extends UnisessionTemplate
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.catalog.UnisessionCommonTemplate";
    public static final String ENTITY_NAME = "unisessionCommonTemplate";
    public static final int VERSION_HASH = -2067896044;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof UnisessionCommonTemplateGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UnisessionCommonTemplateGen> extends UnisessionTemplate.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UnisessionCommonTemplate.class;
        }

        public T newInstance()
        {
            return (T) new UnisessionCommonTemplate();
        }
    }
    private static final Path<UnisessionCommonTemplate> _dslPath = new Path<UnisessionCommonTemplate>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UnisessionCommonTemplate");
    }
            

    public static class Path<E extends UnisessionCommonTemplate> extends UnisessionTemplate.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return UnisessionCommonTemplate.class;
        }

        public String getEntityName()
        {
            return "unisessionCommonTemplate";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
