// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisession.component.sessionSheet.SessionSheetEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.info.ErrorCollector;

/**
 * @author oleyba
 * @since 2/17/11
 */
public class Controller<D extends IDAO<M>, M extends Model> extends AbstractBusinessController<D, M>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final M model = this.getModel(component);
        this.getDao().prepare(model);
        ContextLocal.beginPageTitlePart("Редактирование экзаменационного листа №" + model.getSheet().getNumber());
    }

    public void onClickApply(final IBusinessComponent component)
    {
        final M model = this.getModel(component);
        final ErrorCollector errorCollector = component.getUserContext().getErrorCollector();
        this.getDao().validate(model, errorCollector);
        if (errorCollector.hasErrors()) {
            return;
        }
        this.getDao().update(model);
        this.deactivate(component);
    }

    public void onNeedResize(final IBusinessComponent component)
    {
    }
}