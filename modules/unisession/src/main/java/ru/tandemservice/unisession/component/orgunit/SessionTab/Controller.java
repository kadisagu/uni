package ru.tandemservice.unisession.component.orgunit.SessionTab;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override public void onRefreshComponent(final IBusinessComponent component) {
        this.getDao().prepare(this.getModel(component));
    }

    public void onTabChange(final IBusinessComponent component) {
        component.getUserContext().getDesktop().setRefreshScheduled(true);
    }
}
