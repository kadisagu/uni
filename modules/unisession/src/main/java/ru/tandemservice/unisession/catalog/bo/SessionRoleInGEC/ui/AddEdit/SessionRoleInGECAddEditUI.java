/* $Id$ */
package ru.tandemservice.unisession.catalog.bo.SessionRoleInGEC.ui.AddEdit;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.crud.BaseCatalogItemAddEditUI;
import ru.tandemservice.unisession.entity.catalog.SessionRoleInGEC;

/**
 * @author Andrey Andreev
 * @since 12.09.2016
 */
public class SessionRoleInGECAddEditUI extends BaseCatalogItemAddEditUI<SessionRoleInGEC>
{

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();

        if (isAddForm())
        {
            int priority = DataAccessServices.dao().getCount(SessionRoleInGEC.class);
            getCatalogItem().setPriority(priority + 1);
        }
    }
}
