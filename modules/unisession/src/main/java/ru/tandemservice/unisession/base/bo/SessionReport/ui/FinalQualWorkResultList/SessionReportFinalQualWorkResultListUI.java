/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.FinalQualWorkResultList;


import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.sec.ISecured;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.FinalQualWorkResultAdd.SessionReportFinalQualWorkResultAdd;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.StateFinalAttestationResultList.SessionReportStateFinalAttestationResultListUI;

/**
 * @author Andrey Andreev
 * @since 03.11.2016
 */
@State({@Bind(key = SessionReportManager.BIND_ORG_UNIT, binding = "ouHolder.id")})
public class SessionReportFinalQualWorkResultListUI extends SessionReportStateFinalAttestationResultListUI
{
    // Listeners
    @Override
    public void onClickAdd()
    {
        getActivationBuilder()
                .asDesktopRoot(SessionReportFinalQualWorkResultAdd.class)
                .parameter(SessionReportManager.BIND_ORG_UNIT, getOuHolder().getId())
                .activate();
    }

    @Override
    public String getViewPermissionKey()
    {
        return getSec().getPermission(getOuHolder().getId() != null ? "orgUnit_viewFinalQualWorkResultList" : "finalQualWorkResultReport");
    }

    @Override
    public String getAddPermissionKey()
    {
        return getSec().getPermission(getOuHolder().getId() != null ? "orgUnit_addFinalQualWorkResult" : "addSessionStorableReport");
    }

    @Override
    public String getDeletePermissionKey()
    {
        return getSec().getPermission(getOuHolder().getId() != null ? "orgUnit_deleteFinalQualWorkResult" : "deleteSessionStorableReport");
    }
}
