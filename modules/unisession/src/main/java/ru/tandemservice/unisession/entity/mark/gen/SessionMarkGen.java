package ru.tandemservice.unisession.entity.mark.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.comission.SessionComission;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Оценка студента в сессии
 *
 * Показывает, что указанный слот студента по форме контроля получил оценку в рамках некоторого документа (через слот сессии)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionMarkGen extends EntityBase
 implements INaturalIdentifiable<SessionMarkGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.mark.SessionMark";
    public static final String ENTITY_NAME = "sessionMark";
    public static final int VERSION_HASH = 1203847232;
    private static IEntityMeta ENTITY_META;

    public static final String L_SLOT = "slot";
    public static final String P_MODIFICATION_DATE = "modificationDate";
    public static final String P_PERFORM_DATE = "performDate";
    public static final String L_COMMISSION = "commission";
    public static final String P_COMMENT = "comment";
    public static final String L_CACHED_MARK_VALUE = "cachedMarkValue";
    public static final String P_CACHED_MARK_POSITIVE_STATUS = "cachedMarkPositiveStatus";
    public static final String P_DISPLAYABLE_TITLE = "displayableTitle";
    public static final String P_VALUE_SHORT_TITLE = "valueShortTitle";
    public static final String P_VALUE_TITLE = "valueTitle";

    private SessionDocumentSlot _slot;     // Строка в документе сессии
    private Date _modificationDate;     // Дата изменения объекта
    private Date _performDate;     // Дата получения оценки
    private SessionComission _commission;     // Комиссия (фактическая)
    private String _comment;     // Комментарий
    private SessionMarkCatalogItem _cachedMarkValue;     // Оценка (кэш)
    private Boolean _cachedMarkPositiveStatus;     // Положительная оценка (кэш)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Строка в документе сессии. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public SessionDocumentSlot getSlot()
    {
        return _slot;
    }

    /**
     * @param slot Строка в документе сессии. Свойство не может быть null и должно быть уникальным.
     */
    public void setSlot(SessionDocumentSlot slot)
    {
        dirty(_slot, slot);
        _slot = slot;
    }

    /**
     * @return Дата изменения объекта. Свойство не может быть null.
     */
    @NotNull
    public Date getModificationDate()
    {
        return _modificationDate;
    }

    /**
     * @param modificationDate Дата изменения объекта. Свойство не может быть null.
     */
    public void setModificationDate(Date modificationDate)
    {
        dirty(_modificationDate, modificationDate);
        _modificationDate = modificationDate;
    }

    /**
     * @return Дата получения оценки. Свойство не может быть null.
     */
    @NotNull
    public Date getPerformDate()
    {
        return _performDate;
    }

    /**
     * @param performDate Дата получения оценки. Свойство не может быть null.
     */
    public void setPerformDate(Date performDate)
    {
        dirty(_performDate, performDate);
        _performDate = performDate;
    }

    /**
     * @return Комиссия (фактическая). Свойство не может быть null.
     */
    @NotNull
    public SessionComission getCommission()
    {
        return _commission;
    }

    /**
     * @param commission Комиссия (фактическая). Свойство не может быть null.
     */
    public void setCommission(SessionComission commission)
    {
        dirty(_commission, commission);
        _commission = commission;
    }

    /**
     * @return Комментарий.
     */
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    /**
     * Ссылка на базовый класс оценки (вычисляется и обновляется на основе подклассов)
     * если null - ожидает демона
     *
     * @return Оценка (кэш).
     */
    public SessionMarkCatalogItem getCachedMarkValue()
    {
        return _cachedMarkValue;
    }

    /**
     * @param cachedMarkValue Оценка (кэш).
     */
    public void setCachedMarkValue(SessionMarkCatalogItem cachedMarkValue)
    {
        dirty(_cachedMarkValue, cachedMarkValue);
        _cachedMarkValue = cachedMarkValue;
    }

    /**
     * Показывает, что оценка положительная (отметка не требует пересдачи, оценка положительная)
     * обновляется демоном
     * true  - оценка положительная
     * false - оценка не положительная
     * null - ожидает демона
     *
     * @return Положительная оценка (кэш).
     */
    public Boolean getCachedMarkPositiveStatus()
    {
        return _cachedMarkPositiveStatus;
    }

    /**
     * @param cachedMarkPositiveStatus Положительная оценка (кэш).
     */
    public void setCachedMarkPositiveStatus(Boolean cachedMarkPositiveStatus)
    {
        dirty(_cachedMarkPositiveStatus, cachedMarkPositiveStatus);
        _cachedMarkPositiveStatus = cachedMarkPositiveStatus;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SessionMarkGen)
        {
            if (withNaturalIdProperties)
            {
                setSlot(((SessionMark)another).getSlot());
            }
            setModificationDate(((SessionMark)another).getModificationDate());
            setPerformDate(((SessionMark)another).getPerformDate());
            setCommission(((SessionMark)another).getCommission());
            setComment(((SessionMark)another).getComment());
            setCachedMarkValue(((SessionMark)another).getCachedMarkValue());
            setCachedMarkPositiveStatus(((SessionMark)another).getCachedMarkPositiveStatus());
        }
    }

    public INaturalId<SessionMarkGen> getNaturalId()
    {
        return new NaturalId(getSlot());
    }

    public static class NaturalId extends NaturalIdBase<SessionMarkGen>
    {
        private static final String PROXY_NAME = "SessionMarkNaturalProxy";

        private Long _slot;

        public NaturalId()
        {}

        public NaturalId(SessionDocumentSlot slot)
        {
            _slot = ((IEntity) slot).getId();
        }

        public Long getSlot()
        {
            return _slot;
        }

        public void setSlot(Long slot)
        {
            _slot = slot;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof SessionMarkGen.NaturalId) ) return false;

            SessionMarkGen.NaturalId that = (NaturalId) o;

            if( !equals(getSlot(), that.getSlot()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getSlot());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getSlot());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionMarkGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionMark.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("SessionMark is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "slot":
                    return obj.getSlot();
                case "modificationDate":
                    return obj.getModificationDate();
                case "performDate":
                    return obj.getPerformDate();
                case "commission":
                    return obj.getCommission();
                case "comment":
                    return obj.getComment();
                case "cachedMarkValue":
                    return obj.getCachedMarkValue();
                case "cachedMarkPositiveStatus":
                    return obj.getCachedMarkPositiveStatus();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "slot":
                    obj.setSlot((SessionDocumentSlot) value);
                    return;
                case "modificationDate":
                    obj.setModificationDate((Date) value);
                    return;
                case "performDate":
                    obj.setPerformDate((Date) value);
                    return;
                case "commission":
                    obj.setCommission((SessionComission) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
                case "cachedMarkValue":
                    obj.setCachedMarkValue((SessionMarkCatalogItem) value);
                    return;
                case "cachedMarkPositiveStatus":
                    obj.setCachedMarkPositiveStatus((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "slot":
                        return true;
                case "modificationDate":
                        return true;
                case "performDate":
                        return true;
                case "commission":
                        return true;
                case "comment":
                        return true;
                case "cachedMarkValue":
                        return true;
                case "cachedMarkPositiveStatus":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "slot":
                    return true;
                case "modificationDate":
                    return true;
                case "performDate":
                    return true;
                case "commission":
                    return true;
                case "comment":
                    return true;
                case "cachedMarkValue":
                    return true;
                case "cachedMarkPositiveStatus":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "slot":
                    return SessionDocumentSlot.class;
                case "modificationDate":
                    return Date.class;
                case "performDate":
                    return Date.class;
                case "commission":
                    return SessionComission.class;
                case "comment":
                    return String.class;
                case "cachedMarkValue":
                    return SessionMarkCatalogItem.class;
                case "cachedMarkPositiveStatus":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionMark> _dslPath = new Path<SessionMark>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionMark");
    }
            

    /**
     * @return Строка в документе сессии. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unisession.entity.mark.SessionMark#getSlot()
     */
    public static SessionDocumentSlot.Path<SessionDocumentSlot> slot()
    {
        return _dslPath.slot();
    }

    /**
     * @return Дата изменения объекта. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.mark.SessionMark#getModificationDate()
     */
    public static PropertyPath<Date> modificationDate()
    {
        return _dslPath.modificationDate();
    }

    /**
     * @return Дата получения оценки. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.mark.SessionMark#getPerformDate()
     */
    public static PropertyPath<Date> performDate()
    {
        return _dslPath.performDate();
    }

    /**
     * @return Комиссия (фактическая). Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.mark.SessionMark#getCommission()
     */
    public static SessionComission.Path<SessionComission> commission()
    {
        return _dslPath.commission();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unisession.entity.mark.SessionMark#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * Ссылка на базовый класс оценки (вычисляется и обновляется на основе подклассов)
     * если null - ожидает демона
     *
     * @return Оценка (кэш).
     * @see ru.tandemservice.unisession.entity.mark.SessionMark#getCachedMarkValue()
     */
    public static SessionMarkCatalogItem.Path<SessionMarkCatalogItem> cachedMarkValue()
    {
        return _dslPath.cachedMarkValue();
    }

    /**
     * Показывает, что оценка положительная (отметка не требует пересдачи, оценка положительная)
     * обновляется демоном
     * true  - оценка положительная
     * false - оценка не положительная
     * null - ожидает демона
     *
     * @return Положительная оценка (кэш).
     * @see ru.tandemservice.unisession.entity.mark.SessionMark#getCachedMarkPositiveStatus()
     */
    public static PropertyPath<Boolean> cachedMarkPositiveStatus()
    {
        return _dslPath.cachedMarkPositiveStatus();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.entity.mark.SessionMark#getDisplayableTitle()
     */
    public static SupportedPropertyPath<String> displayableTitle()
    {
        return _dslPath.displayableTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.entity.mark.SessionMark#getValueShortTitle()
     */
    public static SupportedPropertyPath<String> valueShortTitle()
    {
        return _dslPath.valueShortTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.entity.mark.SessionMark#getValueTitle()
     */
    public static SupportedPropertyPath<String> valueTitle()
    {
        return _dslPath.valueTitle();
    }

    public static class Path<E extends SessionMark> extends EntityPath<E>
    {
        private SessionDocumentSlot.Path<SessionDocumentSlot> _slot;
        private PropertyPath<Date> _modificationDate;
        private PropertyPath<Date> _performDate;
        private SessionComission.Path<SessionComission> _commission;
        private PropertyPath<String> _comment;
        private SessionMarkCatalogItem.Path<SessionMarkCatalogItem> _cachedMarkValue;
        private PropertyPath<Boolean> _cachedMarkPositiveStatus;
        private SupportedPropertyPath<String> _displayableTitle;
        private SupportedPropertyPath<String> _valueShortTitle;
        private SupportedPropertyPath<String> _valueTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Строка в документе сессии. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unisession.entity.mark.SessionMark#getSlot()
     */
        public SessionDocumentSlot.Path<SessionDocumentSlot> slot()
        {
            if(_slot == null )
                _slot = new SessionDocumentSlot.Path<SessionDocumentSlot>(L_SLOT, this);
            return _slot;
        }

    /**
     * @return Дата изменения объекта. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.mark.SessionMark#getModificationDate()
     */
        public PropertyPath<Date> modificationDate()
        {
            if(_modificationDate == null )
                _modificationDate = new PropertyPath<Date>(SessionMarkGen.P_MODIFICATION_DATE, this);
            return _modificationDate;
        }

    /**
     * @return Дата получения оценки. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.mark.SessionMark#getPerformDate()
     */
        public PropertyPath<Date> performDate()
        {
            if(_performDate == null )
                _performDate = new PropertyPath<Date>(SessionMarkGen.P_PERFORM_DATE, this);
            return _performDate;
        }

    /**
     * @return Комиссия (фактическая). Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.mark.SessionMark#getCommission()
     */
        public SessionComission.Path<SessionComission> commission()
        {
            if(_commission == null )
                _commission = new SessionComission.Path<SessionComission>(L_COMMISSION, this);
            return _commission;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unisession.entity.mark.SessionMark#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(SessionMarkGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * Ссылка на базовый класс оценки (вычисляется и обновляется на основе подклассов)
     * если null - ожидает демона
     *
     * @return Оценка (кэш).
     * @see ru.tandemservice.unisession.entity.mark.SessionMark#getCachedMarkValue()
     */
        public SessionMarkCatalogItem.Path<SessionMarkCatalogItem> cachedMarkValue()
        {
            if(_cachedMarkValue == null )
                _cachedMarkValue = new SessionMarkCatalogItem.Path<SessionMarkCatalogItem>(L_CACHED_MARK_VALUE, this);
            return _cachedMarkValue;
        }

    /**
     * Показывает, что оценка положительная (отметка не требует пересдачи, оценка положительная)
     * обновляется демоном
     * true  - оценка положительная
     * false - оценка не положительная
     * null - ожидает демона
     *
     * @return Положительная оценка (кэш).
     * @see ru.tandemservice.unisession.entity.mark.SessionMark#getCachedMarkPositiveStatus()
     */
        public PropertyPath<Boolean> cachedMarkPositiveStatus()
        {
            if(_cachedMarkPositiveStatus == null )
                _cachedMarkPositiveStatus = new PropertyPath<Boolean>(SessionMarkGen.P_CACHED_MARK_POSITIVE_STATUS, this);
            return _cachedMarkPositiveStatus;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.entity.mark.SessionMark#getDisplayableTitle()
     */
        public SupportedPropertyPath<String> displayableTitle()
        {
            if(_displayableTitle == null )
                _displayableTitle = new SupportedPropertyPath<String>(SessionMarkGen.P_DISPLAYABLE_TITLE, this);
            return _displayableTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.entity.mark.SessionMark#getValueShortTitle()
     */
        public SupportedPropertyPath<String> valueShortTitle()
        {
            if(_valueShortTitle == null )
                _valueShortTitle = new SupportedPropertyPath<String>(SessionMarkGen.P_VALUE_SHORT_TITLE, this);
            return _valueShortTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.entity.mark.SessionMark#getValueTitle()
     */
        public SupportedPropertyPath<String> valueTitle()
        {
            if(_valueTitle == null )
                _valueTitle = new SupportedPropertyPath<String>(SessionMarkGen.P_VALUE_TITLE, this);
            return _valueTitle;
        }

        public Class getEntityClass()
        {
            return SessionMark.class;
        }

        public String getEntityName()
        {
            return "sessionMark";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getDisplayableTitle();

    public abstract String getValueShortTitle();

    public abstract String getValueTitle();
}
