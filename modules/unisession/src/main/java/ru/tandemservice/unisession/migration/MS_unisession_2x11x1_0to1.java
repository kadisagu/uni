package ru.tandemservice.unisession.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unisession_2x11x1_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.11.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность sessionsSimpleDocumentReason

        // создано обязательное свойство formOnDebts
        {
            // создать колонку
            tool.createColumn("sessionssimpledocumentreason_t", new DBColumn("formondebts_p", DBType.BOOLEAN));

            tool.executeUpdate("update sessionssimpledocumentreason_t set formondebts_p=? where code_p = '1' or code_p = '2'", true);
            tool.executeUpdate("update sessionssimpledocumentreason_t set formondebts_p=? where code_p <> '1' and code_p <> '2'", false);

            // сделать колонку NOT NULL
            tool.setColumnNullable("sessionssimpledocumentreason_t", "formondebts_p", false);

        }


    }
}