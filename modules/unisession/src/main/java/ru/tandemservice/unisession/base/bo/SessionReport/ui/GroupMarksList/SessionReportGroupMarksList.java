/* $Id: SessionReportGroupMarksList.java 21825 2012-02-09 09:08:57Z oleyba $ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.GroupMarksList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import ru.tandemservice.unisession.entity.report.UnisessionGroupMarksReport;

/**
 * @author oleyba
 * @since 12/16/11
 */
@Configuration
public class SessionReportGroupMarksList extends BusinessComponentManager
{
    public static final String DS_REPORTS = "sessionReportGroupMarksDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
            .addDataSource(this.searchListDS(DS_REPORTS, this.sessionGroupMarksDS(), this.sessionGroupMarksDSHandler()))
            .create();
    }

    @Bean
    public ColumnListExtPoint sessionGroupMarksDS() {
        return this.columnListExtPointBuilder(DS_REPORTS)
            .addColumn(indicatorColumn("icon").defaultIndicatorItem(new IndicatorColumn.Item("report", "Отчет")).create())
            .addColumn(publisherColumn("date", UnisessionGroupMarksReport.formingDate()).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).order().create())
            .addColumn(textColumn("course", UnisessionGroupMarksReport.course().s()).create())
            .addColumn(textColumn("group", UnisessionGroupMarksReport.groups().s()).create())
            .addColumn(textColumn("exec", UnisessionGroupMarksReport.executor().s()).create())
            .addColumn(actionColumn("print", CommonDefines.ICON_PRINT, "onClickPrint"))
            .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER).permissionKey("ui:deleteStorableReportPermissionKey")
                .alert(FormattedMessage.with().template("sessionReportGroupMarksDS.delete.alert").parameter(UnisessionGroupMarksReport.formingDateStr().s()).create())
            )
        .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> sessionGroupMarksDSHandler() {
        return new SessionGroupMarksDSHandler(this.getName());
    }
}
