package ru.tandemservice.unisession.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unisession_2x9x1_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность sessionTransferProtocolDocument

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("session_doc_tp_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_35b93694"),
				new DBColumn("protocoldate_p", DBType.TIMESTAMP).setNullable(false),
				new DBColumn("request_id", DBType.LONG).setNullable(false),
				new DBColumn("workplan_id", DBType.LONG).setNullable(false),
				new DBColumn("chairman_id", DBType.LONG),
				new DBColumn("commission_id", DBType.LONG),
				new DBColumn("comment_p", DBType.TEXT)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("sessionTransferProtocolDocument");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность sessionTransferProtocolRow

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("session_doc_tp_row_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_sessiontransferprotocolrow"),
				new DBColumn("discriminator", DBType.SHORT).setNullable(false),
				new DBColumn("protocol_id", DBType.LONG).setNullable(false),
				new DBColumn("requestrow_id", DBType.LONG).setNullable(false),
				new DBColumn("reattestation_p", DBType.BOOLEAN).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("sessionTransferProtocolRow");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность sessionTransferProtocolMark

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("session_doc_tp_row_mark_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_sessiontransferprotocolmark"),
				new DBColumn("discriminator", DBType.SHORT).setNullable(false),
				new DBColumn("protocolrow_id", DBType.LONG).setNullable(false),
				new DBColumn("controlaction_id", DBType.LONG).setNullable(false),
				new DBColumn("mark_id", DBType.LONG)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("sessionTransferProtocolMark");
        }
    }
}