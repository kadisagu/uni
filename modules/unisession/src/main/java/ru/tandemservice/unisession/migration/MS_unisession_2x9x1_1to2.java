package ru.tandemservice.unisession.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unisession_2x9x1_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность sessionTransferProtocolDocument

		// создано обязательное свойство approveDate
		{
			// создать колонку
			tool.createColumn("session_doc_tp_t", new DBColumn("approvedate_p", DBType.TIMESTAMP));
		}

		// создано свойство workPlanVersion
		{
			// создать колонку
			tool.createColumn("session_doc_tp_t", new DBColumn("workplanversion_id", DBType.LONG));
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность sessionTransferProtocolMark

		// создано свойство slot
		{
			// создать колонку
			tool.createColumn("session_doc_tp_row_mark_t", new DBColumn("slot_id", DBType.LONG));
		}
    }
}