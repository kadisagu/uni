package ru.tandemservice.unisession.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unisession_2x9x0_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность sessionAttestationBulletinReport

		// создано свойство course
		{
			// создать колонку
			tool.createColumn("session_rep_att_bull_t", new DBColumn("course_p", DBType.createVarchar(255)));

		}

		// создано свойство customState
		{
			// создать колонку
			tool.createColumn("session_rep_att_bull_t", new DBColumn("customstate_p", DBType.TEXT));

		}

		// создано свойство targetAdmission
		{
			// создать колонку
			tool.createColumn("session_rep_att_bull_t", new DBColumn("targetadmission_p", DBType.createVarchar(255)));

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность sessionAttestationTotalResultReport

		// создано свойство customState
		{
			// создать колонку
			tool.createColumn("session_rep_att_total_result_t", new DBColumn("customstate_p", DBType.TEXT));

		}

		// создано свойство targetAdmission
		{
			// создать колонку
			tool.createColumn("session_rep_att_total_result_t", new DBColumn("targetadmission_p", DBType.createVarchar(255)));

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность unisessionDebtorsReport

		// создано свойство dateFrom
		{
			// создать колонку
			tool.createColumn("session_rep_debtors_t", new DBColumn("performdatefrom_p", DBType.DATE));

		}

		// создано свойство dateTo
		{
			// создать колонку
			tool.createColumn("session_rep_debtors_t", new DBColumn("performdateto_p", DBType.DATE));

		}

		// создано свойство registryStructure
		{
			// создать колонку
			tool.createColumn("session_rep_debtors_t", new DBColumn("registrystructure_p", DBType.TEXT));

		}

		// создано свойство targetAdmission
		{
			// создать колонку
			tool.createColumn("session_rep_debtors_t", new DBColumn("targetadmission_p", DBType.createVarchar(255)));

		}

		// создано свойство customState
		{
			// создать колонку
			tool.createColumn("session_rep_debtors_t", new DBColumn("customstate_p", DBType.TEXT));

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность unisessionGroupBulletinListReport

		// создано свойство registryStructure
		{
			// создать колонку
			tool.createColumn("session_rep_grp_bull_list_t", new DBColumn("registrystructure_p", DBType.TEXT));

		}

		// создано свойство performDateFrom
		{
			// создать колонку
			tool.createColumn("session_rep_grp_bull_list_t", new DBColumn("performdatefrom_p", DBType.DATE));

		}

		// создано свойство performDateTo
		{
			// создать колонку
			tool.createColumn("session_rep_grp_bull_list_t", new DBColumn("performdateto_p", DBType.DATE));

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность unisessionGroupMarksReport

		// создано свойство customState
		{
			// создать колонку
			tool.createColumn("session_rep_grp_marks_t", new DBColumn("customstate_p", DBType.TEXT));

		}

		// создано свойство registryStructure
		{
			// создать колонку
			tool.createColumn("session_rep_grp_marks_t", new DBColumn("registrystructure_p", DBType.TEXT));

		}

		// создано свойство controlActionTypes
		{
			// создать колонку
			tool.createColumn("session_rep_grp_marks_t", new DBColumn("controlactiontypes_p", DBType.TEXT));

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность unisessionResultsByDiscReport

		// создано свойство dateFrom
		{
			// создать колонку
			tool.createColumn("session_rep_results_d_t", new DBColumn("performdatefrom_p", DBType.DATE));

		}

		// создано свойство dateTo
		{
			// создать колонку
			tool.createColumn("session_rep_results_d_t", new DBColumn("performdateto_p", DBType.DATE));

		}

		// создано свойство customState
		{
			// создать колонку
			tool.createColumn("session_rep_results_d_t", new DBColumn("customstate_p", DBType.TEXT));

		}

		// создано свойство targetAdmission
		{
			// создать колонку
			tool.createColumn("session_rep_results_d_t", new DBColumn("targetadmission_p", DBType.createVarchar(255)));

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность unisessionResultsReport

		// создано свойство dateFrom
		{
			// создать колонку
			tool.createColumn("session_rep_results_t", new DBColumn("performdatefrom_p", DBType.DATE));

		}

		// создано свойство dateTo
		{
			// создать колонку
			tool.createColumn("session_rep_results_t", new DBColumn("performdateto_p", DBType.DATE));

		}

		// создано свойство customState
		{
			// создать колонку
			tool.createColumn("session_rep_results_t", new DBColumn("customstate_p", DBType.TEXT));

		}

		// изменен тип свойства targetAdmission с миграцией старых данных (было булеан, стало "Да"/"Нет")
		{

			tool.createColumn("session_rep_results_t", new DBColumn("targetadm_p", DBType.createVarchar(255)));

			tool.executeUpdate("update session_rep_results_t set targetadm_p = case when targetadmission_p = ? then ? else ? end", Boolean.TRUE, "Да", "Нет");
			// изменить тип колонки
			tool.dropColumn("session_rep_results_t", "targetadmission_p");
			tool.renameColumn("session_rep_results_t", "targetadm_p", "targetadmission_p");

		}
		
		////////////////////////////////////////////////////////////////////////////////
		// сущность unisessionSummaryBulletinReport

		// создано свойство registryStructure
		{
			// создать колонку
			tool.createColumn("session_rep_summary_bull_t", new DBColumn("registrystructure_p", DBType.TEXT));

		}

		// создано свойство targetAdmission
		{
			// создать колонку
			tool.createColumn("session_rep_summary_bull_t", new DBColumn("targetadmission_p", DBType.createVarchar(255)));

		}

		// создано свойство customState
		{
			// создать колонку
			tool.createColumn("session_rep_summary_bull_t", new DBColumn("customstate_p", DBType.TEXT));

		}

		// создано свойство performDateFrom
		{
			// создать колонку
			tool.createColumn("session_rep_summary_bull_t", new DBColumn("performdatefrom_p", DBType.DATE));

		}

		// создано свойство performDateTo
		{
			// создать колонку
			tool.createColumn("session_rep_summary_bull_t", new DBColumn("performdateto_p", DBType.DATE));

		}

    }
}