/* $Id$ */
package ru.tandemservice.unisession.component.student.StudentSessionProjectThemeTab;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;

/**
 * @author oleyba
 * @since 6/22/11
 */
@State({@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "student.id")})
public class Model
{
    private Student student = new Student();

    private ISelectModel stateModel;
    private ISelectModel termModel;

    private DynamicListDataSource<SessionDocumentSlot> dataSource;

    private IDataSettings settings;

    public Student getStudent()
    {
        return this.student;
    }

    public void setStudent(final Student student)
    {
        this.student = student;
    }

    public ISelectModel getStateModel()
    {
        return this.stateModel;
    }

    public void setStateModel(final ISelectModel stateModel)
    {
        this.stateModel = stateModel;
    }

    public ISelectModel getTermModel()
    {
        return this.termModel;
    }

    public void setTermModel(final ISelectModel termModel)
    {
        this.termModel = termModel;
    }

    public IDataSettings getSettings()
    {
        return this.settings;
    }

    public void setSettings(final IDataSettings settings)
    {
        this.settings = settings;
    }

    public DynamicListDataSource<SessionDocumentSlot> getDataSource()
    {
        return this.dataSource;
    }

    public void setDataSource(final DynamicListDataSource<SessionDocumentSlot> dataSource)
    {
        this.dataSource = dataSource;
    }
}
