/* $Id$ */
package ru.tandemservice.unisession.print;

import com.google.common.collect.ComparisonChain;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.MergeType;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.IRtfRowIntercepter;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.pps.PpsEntryByEmployeePost;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.catalog.UnisessionCommonTemplate;
import ru.tandemservice.unisession.entity.catalog.codes.UnisessionCommonTemplateCodes;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionTransferProtocolDocument;
import ru.tandemservice.unisession.entity.document.SessionTransferProtocolMark;
import ru.tandemservice.unisession.entity.document.SessionTransferProtocolRow;
import ru.tandemservice.unisession.entity.mark.SessionALRequest;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.TreeMap;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Andreev
 * @since 11.11.2015
 */
public class SessionTransferProtocolPrintDao extends UniBaseDao implements ISessionTransferProtocolPrintDao
{
    protected static final String NOT_CELL = "-/-";

    protected SessionTransferProtocolDocument _protocol;

    @Override
    public RtfDocument printProtocol(Long protocolId)
    {
        UnisessionCommonTemplate templateItem = getCatalogItem(UnisessionCommonTemplate.class, UnisessionCommonTemplateCodes.SESSION_TRANSFER_PROTOCOL);
        RtfDocument document = new RtfReader().read(templateItem.getContent());

        _protocol = get(SessionTransferProtocolDocument.class, protocolId);

        modifyText(new RtfInjectModifier()).modify(document);
        modifyTable(new RtfTableModifier()).modify(document);

        return document;
    }

    protected RtfInjectModifier modifyText(RtfInjectModifier modifier)
    {
        SessionALRequest request = _protocol.getRequest();
        Student student = request.getStudent();
        EducationOrgUnit educationOrgUnit = student.getEducationOrgUnit();
        EducationLevels educationLevels = educationOrgUnit.getEducationLevelHighSchool().getEducationLevel();
        EduProgramSubject programSubject = educationLevels.getEduProgramSubject();


        modifier.put("fullTitleOrgUnit", TopOrgUnit.getInstance().getFullTitle())
                .put("formativeOrgUnit", educationOrgUnit.getFormativeOrgUnit().getTitle())
                .put("eduProgramSubject", programSubject != null ? programSubject.getTitleWithCode() : "")
                .put("developForm", StringUtils.lowerCase(educationOrgUnit.getDevelopForm().getTitle()))
                .put("eduOrganization", request.getEduDocument().getEduOrganization())
                .put("chairperson", getPersonTitle(_protocol.getChairman()))
                .put("date", DateFormatter.DEFAULT_DATE_FORMATTER.format(_protocol.getProtocolDate()))
                .put("shortFioStudent", student.getFio())
                .put("year", String.valueOf(request.getEduDocument().getYearEnd()));

        String fioStudent = PersonManager.instance().declinationDao()
                .getDeclinationFIO(student.getPerson().getIdentityCard(), GrammaCase.GENITIVE);
        modifier.put("fioStudent", fioStudent);

        String eduProgramKind = programSubject == null ? "" :
                programSubject.getEduProgramKind().getEduProgramSubjectKindTitle(InflectorVariantCodes.RU_DATIVE);
        modifier.put("eduProgramKind", eduProgramKind);

        String eduProgramSpecialization = (educationLevels.getEduProgramSpecialization() == null ? "" :
                "профиль «" + educationLevels.getEduProgramSpecialization().getTitle()) + "»";
        modifier.put("eduProgramSpecialization", eduProgramSpecialization);

        String eduDocument = (request.getEduDocument().getSeria() != null ? "серия " + request.getEduDocument().getSeria() + " " : "")
                + "№ " + request.getEduDocument().getNumber();
        modifier.put("eduDocument", eduDocument);

        return modifier;
    }

    protected RtfTableModifier modifyTable(RtfTableModifier modifier)
    {
        IRtfRowIntercepter intercepter = new RtfRowIntercepterBase()
        {
            @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                if (value.equals(NOT_CELL))
                    cell.setMergeType(MergeType.VERTICAL_MERGED_NEXT);
                else
                    cell.setMergeType(MergeType.VERTICAL_MERGED_FIRST);
                return null;
            }
        };

        String stpm_alias = "stpm";
        String stpr_alias = "stpr";
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(SessionTransferProtocolRow.class, stpr_alias)
                .column(property(stpr_alias))
                .where(eq(property(stpr_alias, SessionTransferProtocolRow.protocol()), value(_protocol)))
                .joinEntity(stpr_alias, DQLJoinType.left, SessionTransferProtocolMark.class, stpm_alias,
                            eq(property(stpm_alias, SessionTransferProtocolMark.protocolRow()), property(stpr_alias)))
                .column(property(stpm_alias));
        List<Object[]> rawData = getList(dql);

        String[][] rowsWithRetake = getStringRows(rawData, true);
        if (rowsWithRetake.length == 0)
            modifier.remove("T1");
        else
        {
            modifier.put("T1", rowsWithRetake);
            modifier.put("T1", intercepter);
        }

        String[][] rowsWithoutRetake = getStringRows(rawData, false);
        if (rowsWithoutRetake.length == 0)
            modifier.remove("T2");
        else
        {
            modifier.put("T2", rowsWithoutRetake);
            modifier.put("T2", intercepter);
        }

        if (_protocol.getComment() == null)
            modifier.remove("comment");
        else
            modifier.put("comment", new String[][]{{_protocol.getComment()}});

        String[][] commissions = getList(SessionComissionPps.class, SessionComissionPps.commission(), _protocol.getCommission())
                .stream()
                .filter(ssPps -> ssPps.getPps() instanceof PpsEntryByEmployeePost)
                .map(ssPps -> new String[]{getPersonTitle((PpsEntryByEmployeePost) ssPps.getPps())})
                .toArray(String[][]::new);
        modifier.put("comissionPps", commissions);

        return modifier;
    }


    private String[][] getStringRows(List<Object[]> rows, boolean retake)
    {
        Comparator<SessionTransferProtocolRow> rowComparator = (SessionTransferProtocolRow row1, SessionTransferProtocolRow row2) ->
                ComparisonChain.start()
                        .compare(row1.getRequestRow().getRegElementPart().getTitleWithoutBraces(),
                                 row2.getRequestRow().getRegElementPart().getTitleWithoutBraces())
                        .compare(row1.getRequestRow().getRegElementPart().getNumber(),
                                 row2.getRequestRow().getRegElementPart().getNumber())
                        .result();

        final int[] count = {1};

        return rows.stream()
                .filter(row -> ((SessionTransferProtocolRow) row[0]).isNeedRetake() == retake)
                .collect(Collectors.groupingBy(
                        row -> (SessionTransferProtocolRow) row[0],
                        () -> new TreeMap<>(rowComparator),
                        Collectors.mapping(row -> (SessionTransferProtocolMark) row[1], Collectors.toList())))
                .entrySet().stream()
                .map(row -> getStringRowWithoutRetake(row.getKey(), row.getValue(), count))
                .flatMap(Collection::stream)
                .toArray(String[][]::new);
    }

    private List<String[]> getStringRowWithoutRetake(SessionTransferProtocolRow row, List<SessionTransferProtocolMark> marks, int[] count)
    {
        EppRegistryElementPart part = row.getRequestRow().getRegElementPart();

        String[] head = new String[6];
        head[0] = String.valueOf(count[0]++);
        head[1] = part.getTitleWithoutBraces();
        String labor = UniEppUtils.formatLoad(part.getLaborAsDouble(), false) + " з.е.";
        String size = UniEppUtils.formatLoad(part.getSizeAsDouble(), false) + " ч.";
        head[2] = labor + " / " + size;
        head[3] = row.getRequestRow().getHoursAmount() != null ? String.valueOf(row.getRequestRow().getHoursAmount()) : "";

        // Сначала null, затем по убыванию поля priority (т.е. по возрастанию приоритета) - от "неуд." до "отл." (или от "незачет" до "зачет" и т.п.)
        Comparator<SessionMarkGradeValueCatalogItem> markValueComparator = Comparator.nullsFirst(Comparator.comparing((SessionMarkGradeValueCatalogItem mark) -> mark.getPriority()).reversed());
        List<String[]> table = marks.stream()
                .filter(mark -> mark != null)
                .sorted(Comparator.comparing(mark -> mark.getMark(), markValueComparator))
                .map(mark -> {
                    String[] str = new String[6];
                    str[0] = NOT_CELL;
                    str[1] = NOT_CELL;
                    str[2] = NOT_CELL;
                    str[3] = NOT_CELL;
                    str[4] = mark.getControlAction().getTitle();
                    str[5] = mark.getMark() != null ? mark.getMark().getPrintTitle() : "";
                    return str;
                })
                .collect(Collectors.toList());

        if (table.isEmpty())
        {
            head[4] = "отсутствует";
            head[5] = "-";
            table.add(head);
        }
        else
        {
            String[] first = table.get(0);
            first[0] = head[0];
            first[1] = head[1];
            first[2] = head[2];
            first[3] = head[3];
        }
        return table;
    }

    private String getPersonTitle(PpsEntryByEmployeePost person)
    {
        if (person == null) return "";

        StringBuilder builder = new StringBuilder()
                .append(person.getFio())
                .append(", ");

        builder.append(person.getPost().getTitle()).append(" ");
        builder.append(person.getOrgUnit().getShortTitle());

        List<PersonAcademicDegree> personAcademicDegrees = getList(PersonAcademicDegree.class, PersonAcademicDegree.person().id().s(), person.getPerson().getId());
        String degrees = personAcademicDegrees.stream()
                .filter(d -> d.getAcademicDegree() != null)
                .sorted((d1, d2) -> Integer.compare(d1.getAcademicDegree().getType().getLevel(), d2.getAcademicDegree().getType().getLevel()))
                .map(d -> d.getAcademicDegree().getShortTitle())
                .collect(Collectors.joining(", ", "", ""));
        if (degrees.length() > 0)
            builder.append(", ").append(degrees);

        return builder.toString();
    }

}