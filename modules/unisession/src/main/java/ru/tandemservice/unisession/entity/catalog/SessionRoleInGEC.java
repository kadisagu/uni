package ru.tandemservice.unisession.entity.catalog;

import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.BaseDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IPrioritizedCatalogItem;
import ru.tandemservice.unisession.catalog.bo.SessionRoleInGEC.ui.AddEdit.SessionRoleInGECAddEdit;
import ru.tandemservice.unisession.entity.catalog.gen.*;


/**
 * @see ru.tandemservice.unisession.entity.catalog.gen.SessionRoleInGECGen
 */
public class SessionRoleInGEC extends SessionRoleInGECGen implements IDynamicCatalogItem, IPrioritizedCatalogItem
{

    public static IDynamicCatalogDesc getUiDesc()
    {
        return new BaseDynamicCatalogDesc()
        {
            @Override
            public String getAddEditComponentName()
            {
                return SessionRoleInGECAddEdit.class.getSimpleName();
            }
        };
    }
}