package ru.tandemservice.unisession.entity.document.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.entity.comission.SessionComission;
import ru.tandemservice.unisession.entity.document.SessionDocument;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Запись студента по мероприятию в документе сессии
 *
 * Показывает, что указанный слот студента по форме контроля доступен для отметки в рамках указанного документа
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionDocumentSlotGen extends EntityBase
 implements INaturalIdentifiable<SessionDocumentSlotGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.document.SessionDocumentSlot";
    public static final String ENTITY_NAME = "sessionDocumentSlot";
    public static final int VERSION_HASH = -122903871;
    private static IEntityMeta ENTITY_META;

    public static final String L_DOCUMENT = "document";
    public static final String L_STUDENT_WPE_C_ACTION = "studentWpeCAction";
    public static final String P_IN_SESSION = "inSession";
    public static final String L_COMMISSION = "commission";
    public static final String P_TRY_NUMBER = "tryNumber";
    public static final String P_TERM_TITLE = "termTitle";

    private SessionDocument _document;     // Документ
    private EppStudentWpeCAction _studentWpeCAction;     // Форма контроля для сдачи по дисциплине студента
    private boolean _inSession = true;     // В сессию
    private SessionComission _commission;     // Комиссия (назначенная)
    private Integer _tryNumber;     // В который раз сдает

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Документ. Свойство не может быть null.
     */
    @NotNull
    public SessionDocument getDocument()
    {
        return _document;
    }

    /**
     * @param document Документ. Свойство не может быть null.
     */
    public void setDocument(SessionDocument document)
    {
        dirty(_document, document);
        _document = document;
    }

    /**
     * @return Форма контроля для сдачи по дисциплине студента. Свойство не может быть null.
     */
    @NotNull
    public EppStudentWpeCAction getStudentWpeCAction()
    {
        return _studentWpeCAction;
    }

    /**
     * @param studentWpeCAction Форма контроля для сдачи по дисциплине студента. Свойство не может быть null.
     */
    public void setStudentWpeCAction(EppStudentWpeCAction studentWpeCAction)
    {
        dirty(_studentWpeCAction, studentWpeCAction);
        _studentWpeCAction = studentWpeCAction;
    }

    /**
     * @return В сессию. Свойство не может быть null.
     */
    @NotNull
    public boolean isInSession()
    {
        return _inSession;
    }

    /**
     * @param inSession В сессию. Свойство не может быть null.
     */
    public void setInSession(boolean inSession)
    {
        dirty(_inSession, inSession);
        _inSession = inSession;
    }

    /**
     * @return Комиссия (назначенная). Свойство не может быть null.
     */
    @NotNull
    public SessionComission getCommission()
    {
        return _commission;
    }

    /**
     * @param commission Комиссия (назначенная). Свойство не может быть null.
     */
    public void setCommission(SessionComission commission)
    {
        dirty(_commission, commission);
        _commission = commission;
    }

    /**
     * @return В который раз сдает.
     */
    public Integer getTryNumber()
    {
        return _tryNumber;
    }

    /**
     * @param tryNumber В который раз сдает.
     */
    public void setTryNumber(Integer tryNumber)
    {
        dirty(_tryNumber, tryNumber);
        _tryNumber = tryNumber;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SessionDocumentSlotGen)
        {
            if (withNaturalIdProperties)
            {
                setDocument(((SessionDocumentSlot)another).getDocument());
                setStudentWpeCAction(((SessionDocumentSlot)another).getStudentWpeCAction());
                setInSession(((SessionDocumentSlot)another).isInSession());
            }
            setCommission(((SessionDocumentSlot)another).getCommission());
            setTryNumber(((SessionDocumentSlot)another).getTryNumber());
        }
    }

    public INaturalId<SessionDocumentSlotGen> getNaturalId()
    {
        return new NaturalId(getDocument(), getStudentWpeCAction(), isInSession());
    }

    public static class NaturalId extends NaturalIdBase<SessionDocumentSlotGen>
    {
        private static final String PROXY_NAME = "SessionDocumentSlotNaturalProxy";

        private Long _document;
        private Long _studentWpeCAction;
        private boolean _inSession;

        public NaturalId()
        {}

        public NaturalId(SessionDocument document, EppStudentWpeCAction studentWpeCAction, boolean inSession)
        {
            _document = ((IEntity) document).getId();
            _studentWpeCAction = ((IEntity) studentWpeCAction).getId();
            _inSession = inSession;
        }

        public Long getDocument()
        {
            return _document;
        }

        public void setDocument(Long document)
        {
            _document = document;
        }

        public Long getStudentWpeCAction()
        {
            return _studentWpeCAction;
        }

        public void setStudentWpeCAction(Long studentWpeCAction)
        {
            _studentWpeCAction = studentWpeCAction;
        }

        public boolean isInSession()
        {
            return _inSession;
        }

        public void setInSession(boolean inSession)
        {
            _inSession = inSession;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof SessionDocumentSlotGen.NaturalId) ) return false;

            SessionDocumentSlotGen.NaturalId that = (NaturalId) o;

            if( !equals(getDocument(), that.getDocument()) ) return false;
            if( !equals(getStudentWpeCAction(), that.getStudentWpeCAction()) ) return false;
            if( !equals(isInSession(), that.isInSession()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getDocument());
            result = hashCode(result, getStudentWpeCAction());
            result = hashCode(result, isInSession());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getDocument());
            sb.append("/");
            sb.append(getStudentWpeCAction());
            sb.append("/");
            sb.append(isInSession());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionDocumentSlotGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionDocumentSlot.class;
        }

        public T newInstance()
        {
            return (T) new SessionDocumentSlot();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "document":
                    return obj.getDocument();
                case "studentWpeCAction":
                    return obj.getStudentWpeCAction();
                case "inSession":
                    return obj.isInSession();
                case "commission":
                    return obj.getCommission();
                case "tryNumber":
                    return obj.getTryNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "document":
                    obj.setDocument((SessionDocument) value);
                    return;
                case "studentWpeCAction":
                    obj.setStudentWpeCAction((EppStudentWpeCAction) value);
                    return;
                case "inSession":
                    obj.setInSession((Boolean) value);
                    return;
                case "commission":
                    obj.setCommission((SessionComission) value);
                    return;
                case "tryNumber":
                    obj.setTryNumber((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "document":
                        return true;
                case "studentWpeCAction":
                        return true;
                case "inSession":
                        return true;
                case "commission":
                        return true;
                case "tryNumber":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "document":
                    return true;
                case "studentWpeCAction":
                    return true;
                case "inSession":
                    return true;
                case "commission":
                    return true;
                case "tryNumber":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "document":
                    return SessionDocument.class;
                case "studentWpeCAction":
                    return EppStudentWpeCAction.class;
                case "inSession":
                    return Boolean.class;
                case "commission":
                    return SessionComission.class;
                case "tryNumber":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionDocumentSlot> _dslPath = new Path<SessionDocumentSlot>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionDocumentSlot");
    }
            

    /**
     * @return Документ. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionDocumentSlot#getDocument()
     */
    public static SessionDocument.Path<SessionDocument> document()
    {
        return _dslPath.document();
    }

    /**
     * @return Форма контроля для сдачи по дисциплине студента. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionDocumentSlot#getStudentWpeCAction()
     */
    public static EppStudentWpeCAction.Path<EppStudentWpeCAction> studentWpeCAction()
    {
        return _dslPath.studentWpeCAction();
    }

    /**
     * @return В сессию. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionDocumentSlot#isInSession()
     */
    public static PropertyPath<Boolean> inSession()
    {
        return _dslPath.inSession();
    }

    /**
     * @return Комиссия (назначенная). Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionDocumentSlot#getCommission()
     */
    public static SessionComission.Path<SessionComission> commission()
    {
        return _dslPath.commission();
    }

    /**
     * @return В который раз сдает.
     * @see ru.tandemservice.unisession.entity.document.SessionDocumentSlot#getTryNumber()
     */
    public static PropertyPath<Integer> tryNumber()
    {
        return _dslPath.tryNumber();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.entity.document.SessionDocumentSlot#getTermTitle()
     */
    public static SupportedPropertyPath<String> termTitle()
    {
        return _dslPath.termTitle();
    }

    public static class Path<E extends SessionDocumentSlot> extends EntityPath<E>
    {
        private SessionDocument.Path<SessionDocument> _document;
        private EppStudentWpeCAction.Path<EppStudentWpeCAction> _studentWpeCAction;
        private PropertyPath<Boolean> _inSession;
        private SessionComission.Path<SessionComission> _commission;
        private PropertyPath<Integer> _tryNumber;
        private SupportedPropertyPath<String> _termTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Документ. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionDocumentSlot#getDocument()
     */
        public SessionDocument.Path<SessionDocument> document()
        {
            if(_document == null )
                _document = new SessionDocument.Path<SessionDocument>(L_DOCUMENT, this);
            return _document;
        }

    /**
     * @return Форма контроля для сдачи по дисциплине студента. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionDocumentSlot#getStudentWpeCAction()
     */
        public EppStudentWpeCAction.Path<EppStudentWpeCAction> studentWpeCAction()
        {
            if(_studentWpeCAction == null )
                _studentWpeCAction = new EppStudentWpeCAction.Path<EppStudentWpeCAction>(L_STUDENT_WPE_C_ACTION, this);
            return _studentWpeCAction;
        }

    /**
     * @return В сессию. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionDocumentSlot#isInSession()
     */
        public PropertyPath<Boolean> inSession()
        {
            if(_inSession == null )
                _inSession = new PropertyPath<Boolean>(SessionDocumentSlotGen.P_IN_SESSION, this);
            return _inSession;
        }

    /**
     * @return Комиссия (назначенная). Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionDocumentSlot#getCommission()
     */
        public SessionComission.Path<SessionComission> commission()
        {
            if(_commission == null )
                _commission = new SessionComission.Path<SessionComission>(L_COMMISSION, this);
            return _commission;
        }

    /**
     * @return В который раз сдает.
     * @see ru.tandemservice.unisession.entity.document.SessionDocumentSlot#getTryNumber()
     */
        public PropertyPath<Integer> tryNumber()
        {
            if(_tryNumber == null )
                _tryNumber = new PropertyPath<Integer>(SessionDocumentSlotGen.P_TRY_NUMBER, this);
            return _tryNumber;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.entity.document.SessionDocumentSlot#getTermTitle()
     */
        public SupportedPropertyPath<String> termTitle()
        {
            if(_termTitle == null )
                _termTitle = new SupportedPropertyPath<String>(SessionDocumentSlotGen.P_TERM_TITLE, this);
            return _termTitle;
        }

        public Class getEntityClass()
        {
            return SessionDocumentSlot.class;
        }

        public String getEntityName()
        {
            return "sessionDocumentSlot";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getTermTitle();
}
