/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionDocument.ui.Tab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.Output;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.unisession.base.bo.SessionDocument.ui.logic.SessionDocumentDSHandler;

import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 07.05.2015
 */
@Input({@Bind(key = UIPresenter.PUBLISHER_ID, binding = "holder.id")})
@Output({@Bind(key = "sessionTabSecModel", binding = "secModel")})
public class SessionDocumentTabUI extends UIPresenter
{
    public static final String DOCUMENT_REGION_NAME = "documentComponentName";
    public static final String PARAM_DOCUMENT = "document";

    private final EntityHolder<OrgUnit> _holder = new EntityHolder<OrgUnit>();
    private CommonPostfixPermissionModel _secModel;

    @Override
    public void onComponentRefresh()
    {
        OrgUnit ou = getHolder().refresh(OrgUnit.class);
        setSecModel(new OrgUnitSecModel(ou));

        List<SessionDocumentDSHandler.SessionDocumentWrapper> documents = getConfig().getDataSource(SessionDocumentTab.DOCUMENT_DS).getRecords();
        if (null != getDocument() && !documents.contains(getDocument()))
            getSettings().set(PARAM_DOCUMENT, null);

        onChangeDocument();
    }

    public void onChangeDocument()
    {
        getSettings().save();
        if (null != getDocument())
        {
            _uiActivation.asRegion(getDocument().getComponentName(), DOCUMENT_REGION_NAME).activate();
        }
    }

    public String getViewPermissionKey()
    {
        return getDocument() == null ? "" : getSecModel().getPermission(getDocument().getPermissionKey());
    }

    public EntityHolder<OrgUnit> getHolder()
    {
        return _holder;
    }

    public Long getOrgUnitId()
    {
        return getHolder().getId();
    }

    public OrgUnit getOrgUnit()
    {
        return getHolder().getValue();
    }

    public SessionDocumentDSHandler.SessionDocumentWrapper getDocument()
    {
        return getSettings().get(PARAM_DOCUMENT);
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(final CommonPostfixPermissionModel secModel)
    {
        _secModel = secModel;
    }
}
