/**
 *$Id$
 */
package ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.TotalResultAdd.util;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport;

/**
 * @author Alexander Shaburov
 * @since 22.11.12
 */
public interface IAttestationTotalResultReportDao extends INeedPersistenceSupport
{
    /**
     * Создает печатный документ отчета.
     * @return RtfDocument
     */
    <M extends Model> RtfDocument createReportRtfDocument(M model);

    /**
     * Сохраняет отчет и его печатную форму.
     */
    <M extends Model> SessionAttestationTotalResultReport saveReport(M model, RtfDocument document);
}
