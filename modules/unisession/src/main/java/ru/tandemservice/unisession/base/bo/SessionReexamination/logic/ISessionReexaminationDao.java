package ru.tandemservice.unisession.base.bo.SessionReexamination.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.mark.SessionALRequest;
import ru.tandemservice.unisession.entity.mark.SessionALRequestRow;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author Alexey Lopatin
 * @since 31.08.2015
 */
public interface ISessionReexaminationDao extends INeedPersistenceSupport
{
    /**
     * Осуществляет мерж строк заявления о переводе на ускоренное обучение
     *
     * @param currentRows новый набор строк
     * @param oldRows     старый набор строк
     * @param save        сохранять изменения в базу
     * @return итоговый набор
     */
    List<SessionALRequestRow> mergeALRequestRows(Collection<SessionALRequestRow> currentRows, Collection<SessionALRequestRow> oldRows, boolean save);

    /**
     * Осуществляет мерж сохранение заявления о переводе на ускоренное обучение
     *
     * @param requestRows       список строк заявления
     * @param requestRowMarkMap выставленные оценки для перезачтений
     * @param part2fcaMap       мап связи части элемента реестра с ФИК
     */
    void saveALRequestRowMarks(Collection<SessionALRequestRow> requestRows, Map<PairKey<SessionALRequestRow, EppFControlActionType>, SessionMarkGradeValueCatalogItem> requestRowMarkMap, Map<EppRegistryElementPart, List<EppFControlActionType>> part2fcaMap);

    /**
     * Сортирует строки заявления о переводе на ускоренное обучение
     * сначала по индексу элемента реестра, затем по семестру
     */
    void sortALRequestRows(List<SessionALRequestRow> requestRows);

    /**
     * @param requestRows список строк заявления
     * @return Возвращает мап связи части элемента реестра с ФИК
     */
    Map<EppRegistryElementPart, List<EppFControlActionType>> getPart2FCATypeMap(List<SessionALRequestRow> requestRows);

    /**
     * Возвращает уникальный набор ФИК используемых для строк заявления
     *
     * @param block       Блок УПв
     * @param requestRows список строк заявления
     * @return список ФИК
     */
    List<EppFControlActionType> getUsedFcaTypes(EppEduPlanVersionBlock block, List<SessionALRequestRow> requestRows);

    /**
     * Формирует блок колонок для ФИК заявления
     * @param request заявление
     */
    void doCreateControlActionColumns(SessionALRequest request, BaseSearchListDataSource source);
}
