package ru.tandemservice.unisession.base.bo.SessionBulletin.ui.ThemeEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unisession.base.bo.SessionBulletin.logic.SessionBulletinThemesDSHandler;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionProjectTheme;

/**
 * @author avedernikov
 * @since 08.08.2016
 */
@Configuration
public class SessionBulletinThemeEdit extends BusinessComponentManager
{
	public static final String THEMES_DS = "themesDS";
	public static final String SUPERVISOR_DS = "supervisorDS";

	@Bean
	@Override
	public PresenterExtPoint presenterExtPoint()
	{
		return this.presenterExtPointBuilder()
				.addDataSource(simpleSearchListDS(THEMES_DS, themesDSColumn(), themesDSHandler()))
				.addDataSource(CommonBaseStaticSelectDataSource.selectDS(SUPERVISOR_DS, getName(), PpsEntry.defaultSelectDSHandler(getName()))
									   .addColumn(PpsEntry.fio().s())
									   .addColumn(PpsEntry.orgUnit().shortTitle().s())
									   .addColumn(PpsEntry.titlePostOrTimeWorkerData().s()))
				.create();
	}

	@Bean
	public ColumnListExtPoint themesDSColumn()
	{
		return columnListExtPointBuilder(THEMES_DS)
				.addColumn(textColumn(Student.P_BOOK_NUMBER, SessionDocumentSlot.actualStudent().bookNumber()))
				.addColumn(textColumn(Student.P_FULL_FIO, SessionDocumentSlot.actualStudent().fullFio()))
				.addColumn(blockColumn(SessionProjectTheme.P_THEME, "themeBlockColumn"))
				.addColumn(blockColumn(SessionProjectTheme.L_SUPERVISOR, "supervisorBlockColumn"))
				.create();
	}

	@Bean
	public IBusinessHandler<DSInput, DSOutput> themesDSHandler()
	{
		return new SessionBulletinThemesDSHandler(getName());
	}
}
