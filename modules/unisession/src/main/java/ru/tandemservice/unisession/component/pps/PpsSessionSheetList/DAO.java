// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisession.component.pps.PpsSessionSheetList;

import com.google.common.collect.ImmutableList;
import org.hibernate.Session;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniedu.base.bo.EducationYear.utils.EducationYearModel;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionSheetDocument;
import ru.tandemservice.unisession.entity.document.SessionSimpleDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author iolshvang
 * @since 11.04.2011
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private final DQLOrderDescriptionRegistry order = new DQLOrderDescriptionRegistry(SessionSheetDocument.class, "doc")
    .addAdditionalAlias(SessionDocumentSlot.class, "slot")
    .addAdditionalAlias(SessionMark.class, "mark")
    .setOrders(Student.person().identityCard().fio().s(), new OrderDescription("slot", SessionDocumentSlot.actualStudent().person().identityCard().fullFio().s()));

    @Override
    @SuppressWarnings("unchecked")
    public void prepare(final Model model)
    {
        model.setPerson(PersonManager.instance().dao().getPerson(model.getPrincipalContext()));
        model.setEduYearModel(new EducationYearModel());
        model.setCourseModel(new LazySimpleSelectModel<>(DevelopGridDAO.getCourseList()));
        model.setStateModel(new LazySimpleSelectModel<>(ImmutableList.of(
                new IdentifiableWrapper(0L, "Выставлена"),
                new IdentifiableWrapper(1L, "Не выставлена"))));
    }

    @Override
    public void prepareListDataSource(final Model model)
    {
        final Session session = this.getSession();
        final DynamicListDataSource<SessionSheetDocument> dataSource = model.getDataSource();

        final DQLSelectBuilder sheetDQL = new DQLSelectBuilder().fromEntity(SessionSheetDocument.class, "doc");

        final DQLSelectBuilder tutorDQL = new DQLSelectBuilder()
        .fromEntity(SessionComissionPps.class, "rel")
        .where(DQLExpressions.eq(DQLExpressions.property(SessionComissionPps.pps().person().fromAlias("rel")), DQLExpressions.value(model.getPerson())))
        .joinEntity("rel", DQLJoinType.inner, SessionDocumentSlot.class, "slot", eq(property(SessionComissionPps.commission().fromAlias("rel")), property(SessionDocumentSlot.commission().fromAlias("slot"))))
        .column(property(SessionDocumentSlot.document().id().fromAlias("slot")))
        .predicate(DQLPredicateType.distinct);

        sheetDQL.where(in(property(SessionSimpleDocument.id().fromAlias("doc")), tutorDQL.buildQuery()));
        sheetDQL.joinEntity("doc", DQLJoinType.inner, SessionDocumentSlot.class, "slot", eq(property("doc.id"), property(SessionDocumentSlot.document().id().fromAlias("slot"))));

        final String markAlias = "mark";
        final IdentifiableWrapper state = model.getSettings().get("state");
        if (null != state)
        {
            if (state.getId().equals(1L))
            {
                final DQLSelectBuilder markDQL = new DQLSelectBuilder()
                .fromEntity(SessionMark.class, "mark_ne")
                .column("mark_ne.id")
                .where(eq(property("slot.id"), property(SessionMark.slot().id().fromAlias("mark_ne"))));
                sheetDQL.where(notExists(markDQL.buildQuery()));
            }
            else
            {
                sheetDQL.joinEntity("slot", DQLJoinType.inner, SessionMark.class, markAlias, eq(property("slot.id"), property(SessionMark.slot().id().fromAlias(markAlias))));
            }
        }

        ru.tandemservice.unisession.component.orgunit.SessionSimpleDocListTab.DAO.applyFilters(sheetDQL, "slot", markAlias, model.getSettings());

        if (!sheetDQL.hasAlias(markAlias)) {
            sheetDQL.joinEntity("slot", DQLJoinType.left, SessionMark.class, markAlias, eq(property("slot.id"), property(SessionMark.slot().id().fromAlias(markAlias))));
        }

        FilterUtils.applyBetweenFilter(sheetDQL, "doc", SessionSheetDocument.issueDate().s(), model.getSettings().<Date>get("formedFrom"), model.getSettings().<Date>get("formedTo"));
        FilterUtils.applyBetweenFilter(sheetDQL, "mark", SessionMark.performDate().s(), model.getSettings().<Date>get("performDateFrom"), model.getSettings().<Date>get("performDateTo"));

        this.order.applyOrder(sheetDQL, dataSource.getEntityOrder());
        UniBaseUtils.createPage(dataSource, sheetDQL.column(property("doc")), session);

        final List<Long> sheetIds =  UniBaseUtils.getIdList(model.getDataSource().getEntityList());

        final Map<Long, SessionDocumentSlot> slotMap = new HashMap<>();
        final DQLSelectBuilder slotDQL = new DQLSelectBuilder()
        .fromEntity(SessionDocumentSlot.class, "slot")
        .column("slot")
        .fetchPath(DQLJoinType.inner, SessionDocumentSlot.document().fromAlias("slot"), "doc")
        .where(in(property("doc.id"), sheetIds));
        for (final SessionDocumentSlot slot : slotDQL.createStatement(this.getSession()).<SessionDocumentSlot>list()) {
            slotMap.put(slot.getDocument().getId(), slot);
        }

        final Map<Long, SessionMark> markMap = new HashMap<>();
        final DQLSelectBuilder markDQL = new DQLSelectBuilder()
        .fromEntity(SessionMark.class, "mark")
        .column("mark")
        .fetchPath(DQLJoinType.inner, SessionMark.slot().fromAlias("mark"), "slot")
        .fetchPath(DQLJoinType.inner, SessionDocumentSlot.document().fromAlias("slot"), "doc")
        .where(in(property("doc.id"), sheetIds));
        for (final SessionMark mark : markDQL.createStatement(this.getSession()).<SessionMark>list()) {
            markMap.put(mark.getSlot().getDocument().getId(), mark);
        }

        for (final ViewWrapper<SessionSheetDocument> wrapper : ViewWrapper.<SessionSheetDocument>getPatchedList(model.getDataSource()))
        {
            final Long id = wrapper.getEntity().getId();
            final SessionMark mark = markMap.get(id);

            wrapper.setViewProperty("slot", slotMap.get(id));
            wrapper.setViewProperty("mark", mark);
        }
    }
}
