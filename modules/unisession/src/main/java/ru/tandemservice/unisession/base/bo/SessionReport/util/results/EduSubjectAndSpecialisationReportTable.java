/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReport.util.results;

import com.google.common.collect.Collections2;
import com.google.common.collect.Maps;
import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.ResultsAdd.ISessionReportResultsDAO;

import java.util.*;

/**
* @author oleyba
* @since 2/13/12
*/
public class EduSubjectAndSpecialisationReportTable extends ReportTable
{
    @Override
    public void modify(RtfDocument rtf)
    {
        RtfInjectModifier modifier = new RtfInjectModifier();
        modifier.put("groupTitle", "Направление (специальность)");
        modifier.put("Title", "Направление");
        modifier.put("criterion", "направлениям и профилям");
        modifier.modify(rtf);
        SharedRtfUtil.removeParagraphsWithTagsRecursive(rtf, Arrays.asList("eduLevelType", "eduLevelTitle"), false, false);
    }

    public EduSubjectAndSpecialisationReportTable(Collection<ISessionReportResultsDAO.ISessionResultsReportStudentData> students, boolean useFullTitle)
    {
        Set<EducationLevelsHighSchool> levels = new HashSet<>(Collections2.transform(students, ISessionReportResultsDAO.ISessionResultsReportStudentData::getLevel));

        for (EducationLevelsHighSchool level : levels)
        {
            if (level.getEducationLevel().getEduProgramSubject() == null)
                UserContext.getInstance().getErrorCollector().addError("Не найдено сопоставление новому направлению для НПм «" + level.getDisplayableTitle() + "», необходимо заполнить настройку «Связь НПв, НПм с направлениями ОП»");
        }

        if(UserContext.getInstance().getErrorCollector().hasErrors())
            throw new ApplicationException();

        Map<EduProgramSubject, List<EduProgramSpecialization>> subjectListMap = Maps.newHashMap();
        for (EducationLevelsHighSchool level : levels)
        {
            if(!SafeMap.safeGet(subjectListMap, level.getEducationLevel().getEduProgramSubject(), ArrayList.class).contains(level.getEducationLevel().getEduProgramSpecialization()))
                subjectListMap.get(level.getEducationLevel().getEduProgramSubject()).add(level.getEducationLevel().getEduProgramSpecialization());
        }

        for(Map.Entry<EduProgramSubject, List<EduProgramSpecialization>> entry : subjectListMap.entrySet())
        {
            final EduProgramSubject subject = entry.getKey();
            for (EduProgramSpecialization value : entry.getValue())
            {
                final EduProgramSpecialization spec = value;
                String title = useFullTitle ? subject.getTitleWithCode() + (spec != null ? " (" + spec.getDisplayableTitle() + ")" : " (до выбора направленности)") :
                        subject.getShortTitleWithCode() + (spec != null ? " (" + (spec.isRootSpecialization() ? "общ. напр." : spec.getShortTitle()) + ")" : "" );
                rowList.add(new ReportRow(title,
                        CollectionUtils.select(students, input -> {
                            EduProgramSubject inputSubject = input.getLevel().getEducationLevel().getEduProgramSubject();
                            EduProgramSpecialization inputSpec = input.getLevel().getEducationLevel().getEduProgramSpecialization();

                            return Objects.equals(subject, inputSubject) && Objects.equals(spec, inputSpec);
                        })));
            }
        }
        Collections.sort(rowList, ITitled.TITLED_COMPARATOR);
        rowList.add(totalsRow(students));
    }
}
