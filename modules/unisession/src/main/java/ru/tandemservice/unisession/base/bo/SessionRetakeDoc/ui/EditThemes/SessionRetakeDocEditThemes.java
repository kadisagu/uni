package ru.tandemservice.unisession.base.bo.SessionRetakeDoc.ui.EditThemes;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;

/**
 * @author avedernikov
 * @since 14.07.2016
 */
@Configuration
public class SessionRetakeDocEditThemes extends BusinessComponentManager
{
	public static final String THEMES_DS = "themeDS";

	public static final String ALIAS_THEME = "theme";

	public static final String PARAM_RETAKE_DOC = "retakeDoc";

	@Bean
	@Override
	public PresenterExtPoint presenterExtPoint()
	{
		return presenterExtPointBuilder()
				.addDataSource(simpleSearchListDS(THEMES_DS, themesDSColumn(), themesDSHandler()))
				.create();
	}

	@Bean
	public ColumnListExtPoint themesDSColumn()
	{
		return columnListExtPointBuilder(THEMES_DS)
				.addColumn(textColumn(Student.P_BOOK_NUMBER, SessionDocumentSlot.studentWpeCAction().studentWpe().student().bookNumber()))
				.addColumn(textColumn(IdentityCard.P_FULL_FIO, SessionDocumentSlot.studentWpeCAction().studentWpe().student().fullFio()))
				.addColumn(blockColumn(ALIAS_THEME, "themeBlockColumn"))
				.create();
	}

	@Bean
	public IBusinessHandler<DSInput, DSOutput> themesDSHandler()
	{
		return new EntityComboDataSourceHandler(getName(), SessionDocumentSlot.class)
				.where(SessionDocumentSlot.document(), PARAM_RETAKE_DOC)
				.order(SessionDocumentSlot.studentWpeCAction().studentWpe().student().person().identityCard().fullFio());
	}
}
