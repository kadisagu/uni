/**
 *$Id:$
 */
package ru.tandemservice.unisession.attestation.bo.AttestationBulletin.ui.Edit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.uni.base.bo.PpsEntry.util.PpsEntrySelectBlockData;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unisession.attestation.bo.AttestationBulletin.AttestationBulletinManager;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;

import java.util.List;

/**
 * @author Alexander Shaburov
 * @since 16.10.12
 */
@Input({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "entityId", required = true)
})

public class AttestationBulletinEditUI extends UIPresenter
{
    // fields
    private Long _entityId;
    private SessionAttestationBulletin _entity;
    private PpsEntrySelectBlockData ppsData;

    @Override
    public void onComponentActivate()
    {
        AttestationBulletinManager.instance().dao().checkEditAllowed(_entityId);
    }

    @Override
    public void onComponentRefresh()
    {
        _entity = DataAccessServices.dao().getNotNull(_entityId);
        List<PpsEntry> ppsList = CommonBaseUtil.getPropertiesList(IUniBaseDao.instance.get().getList(SessionComissionPps.class, SessionComissionPps.commission().s(), getEntity().getCommission()), SessionComissionPps.pps().s());
        setPpsData(new PpsEntrySelectBlockData(getEntity().getRegistryElementPart().getTutorOu().getId()).setMultiSelect(true).setInitiallySelectedPps(ppsList));
     }

    public void onClickApply()
    {
        AttestationBulletinManager.instance().dao().doUpdateAttBulletin(_entity, this.getPpsList());
        deactivate();
    }

    // Getters & Setters

    public Long getEntityId()
    {
        return _entityId;
    }

    public void setEntityId(Long entityId)
    {
        _entityId = entityId;
    }

    public SessionAttestationBulletin getEntity()
    {
        return _entity;
    }

    public void setEntity(SessionAttestationBulletin entity)
    {
        _entity = entity;
    }

     public PpsEntrySelectBlockData getPpsData()
    {
        return ppsData;
    }

    public void setPpsData(PpsEntrySelectBlockData ppsData)
    {
        this.ppsData = ppsData;
    }

    public List<PpsEntry> getPpsList()
    {
        return getPpsData().getSelectedPpsList();
    }

    public void setPpsList(final List<PpsEntry> ppsList)
    {
        getPpsData().setSelectedPps(ppsList);
    }

}
