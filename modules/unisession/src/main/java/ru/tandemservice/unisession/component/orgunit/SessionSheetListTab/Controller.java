/* $*/

package ru.tandemservice.unisession.component.orgunit.SessionSheetListTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.RowCollectionFormatter;
import org.tandemframework.core.view.formatter.RawFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.MultiValuesColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.utils.PublisherColumnBuilder;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unisession.base.bo.SessionSheet.ui.Forming.SessionSheetForming;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionSheetDocument;
import ru.tandemservice.unisession.entity.document.SessionSimpleDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.print.ISessionSheetPrintDAO;

import java.util.List;

/**
 * @author oleyba
 * @since 2/28/11
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);

        model.setSettingsKey("session.sheet." + model.getOrgUnitId());
        model.setSettings(UniBaseUtils.getDataSettings(component, model.getSettingsKey()));

        final DynamicListDataSource<SessionSheetDocument> dataSource = UniBaseUtils.createDataSource(component, this.getDao());

        dataSource.addColumn(new CheckboxColumn("select"));

        dataSource.addColumn(new SimpleColumn("Номер", SessionSimpleDocument.number()).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Мероприятие", "slot." + SessionDocumentSlot.studentWpeCAction().registryElementTitle().s()).setClickable(false));
        dataSource.addColumn(new PublisherColumnBuilder("Студент", Student.FIO_KEY, "studentNewSessionTab").subTab("studentSessionMarkTab").tabBind(ru.tandemservice.uni.component.student.StudentPub.Model.SELECTED_TAB_BIND_KEY).entity("slot." + SessionDocumentSlot.actualStudent().s()).build());
        dataSource.addColumn(new SimpleColumn("Вид затрат", "slot." + SessionDocumentSlot.actualStudent().compensationType().shortTitle().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Семестр",  "slot." + SessionDocumentSlot.termTitle().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Группа\n (текущая)", "slot." + SessionDocumentSlot.actualStudent().group().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Курс\n (текущий)", "slot." + SessionDocumentSlot.actualStudent().course().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата\n выдачи", SessionSheetDocument.issueDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Оценка", "mark." + SessionMark.valueShortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Дата\n сдачи", "mark." + SessionMark.performDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new MultiValuesColumn("Преподаватель", "tutors").setFormatter(new RowCollectionFormatter(RawFormatter.INSTANCE)).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("В который раз сдает", "slot." + SessionDocumentSlot.tryNumber().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(CommonBaseUtil.getPrintColumn("onClickPrint", "Печать").setPermissionKey(model.getSec().getPermission("printSessionSheet")));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEdit").setPermissionKey(model.getSec().getPermission("editSessionSheet")));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete", "Удалить экз. лист №{0}?", SessionSimpleDocument.number().s()).setPermissionKey(model.getSec().getPermission("deleteSessionSheet")));

        model.setDataSource(dataSource);
    }

    public void onClickFormingSheets(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(SessionSheetForming.class.getSimpleName(),
                new ParametersMap().add(UIPresenter.PUBLISHER_ID, getModel(component).getOrgUnitId())));
    }

    public void onClickSearch(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        model.getDataSource().showFirstPage();
        model.getDataSource().refresh();
        DataSettingsFacade.saveSettings(model.getSettings());
    }

    public void onClickClear(final IBusinessComponent context)
    {
        this.getModel(context).getSettings().clear();
        this.onClickSearch(context);
    }

    public void onClickAddSheet(final IBusinessComponent context)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                ru.tandemservice.unisession.component.sessionSheet.SessionSheetAdd.Model.COMPONENT_NAME,
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, this.getModel(context).getOrgUnitId())));
    }

    public void onClickEdit(final IBusinessComponent context)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                ru.tandemservice.unisession.component.sessionSheet.SessionSheetEdit.Model.COMPONENT_NAME,
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, context.<Long>getListenerParameter())));
    }

    public void onClickDelete(final IBusinessComponent component)
    {
        this.getDao().deleteRow(component);
    }

    public void onClickPrint(final IBusinessComponent component)
    {
        final RtfDocument document = ISessionSheetPrintDAO.instance.get().printSheet(component.<Long>getListenerParameter());
        final byte[] content = RtfUtil.toByteArray(document);
        final Integer temporaryId = PrintReportTemporaryStorage.registerTemporaryPrintForm(content, "Экзаменационный лист.rtf");
        this.activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT, new ParametersMap().add("id", temporaryId).add("zip", Boolean.FALSE)));
    }

    public void onClickPrintSelected(final IBusinessComponent component)
    {
        final List<Long> selectedIds = UniBaseUtils.getIdList(((CheckboxColumn) this.getModel(component).getDataSource().getColumn("select")).getSelectedObjects());
        if (selectedIds.isEmpty()) {
            throw new ApplicationException("Не выбраны экз. листы для печати.");
        }
        final RtfDocument document = ISessionSheetPrintDAO.instance.get().printSheetList(selectedIds);
        final byte[] content = RtfUtil.toByteArray(document);
        final Integer temporaryId = PrintReportTemporaryStorage.registerTemporaryPrintForm(content, "Экзаменационные листы.rtf");
        this.activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT, new ParametersMap().add("id", temporaryId).add("zip", Boolean.FALSE)));
    }

    public void onClickDeletetSelected(final IBusinessComponent component)
    {
        final List<Long> selectedIds = UniBaseUtils.getIdList(((CheckboxColumn) this.getModel(component).getDataSource().getColumn("select")).getSelectedObjects());
        if (selectedIds.isEmpty()) {
            throw new ApplicationException("Не выбраны экз. листы для удаления.");
        }
        int deletedNumber = 0;
        for (final Long id : selectedIds)
        {
            final List<SessionMark> sessionMarkList = IUniBaseDao.instance.get().getList(SessionMark.class, SessionMark.slot().document().id(), id);
            if (sessionMarkList.size()==0)
            {
                this.getDao().delete(id);
                deletedNumber++;
            }
        }
        ContextLocal.getInfoCollector().add("Удалено " + deletedNumber + " экз. листов." + (deletedNumber < selectedIds.size()?" Экз. листы с оценками не были удалены.":""));
    }
}