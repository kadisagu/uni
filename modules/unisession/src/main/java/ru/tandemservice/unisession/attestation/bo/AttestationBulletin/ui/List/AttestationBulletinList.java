/* $Id$ */
package ru.tandemservice.unisession.attestation.bo.AttestationBulletin.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.core.view.util.Icon;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unisession.attestation.bo.Attestation.AttestationManager;
import ru.tandemservice.unisession.attestation.bo.AttestationBulletin.AttestationBulletinManager;
import ru.tandemservice.unisession.attestation.bo.AttestationBulletin.logic.SessionAttestationBulletinDSHandler;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;

import static ru.tandemservice.unisession.attestation.bo.AttestationBulletin.logic.SessionAttestationBulletinDSHandler.*;

/**
 * @author oleyba
 * @since 1/24/12
 */
@Configuration
public class AttestationBulletinList extends BusinessComponentManager
{
    // dataSource
    public static final String DS_BULLETINS = "sessionAttestationBulletinDS";

    public static final String SELECT_COLUMN = "check";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
	{
        return presenterExtPointBuilder()
            .addDataSource(SessionReportManager.instance().eduYearDSConfig())
            .addDataSource(SessionReportManager.instance().yearPartDSConfig())
            .addDataSource(AttestationManager.instance().attestationDSConfig())
            .addDataSource(selectDS(AttestationBulletinManager.DS_REGISTRY_STRYCTURE, AttestationBulletinManager.instance().registryStructureDSHandler()).treeable(true))
            .addDataSource(selectDS(AttestationBulletinManager.DS_BULL_STATUS, AttestationBulletinManager.instance().bullStatusDSHandler()))
            .addDataSource(selectDS(AttestationBulletinManager.DS_OWNER_ORGUNIT, AttestationBulletinManager.instance().ownerOrgUnitDSHandler()).addColumn(OrgUnit.fullTitle().s()))
            .addDataSource(selectDS(AttestationBulletinManager.DS_DISCIPLINE, AttestationBulletinManager.instance().disciplineDSHandler()).addColumn(EppRegistryElementPart.titleWithNumber().s()))
            .addDataSource(selectDS(AttestationBulletinManager.DS_PPS, AttestationBulletinManager.instance().ppsDSHandler()).addColumn(PpsEntry.title().s()))
            .addDataSource(UniStudentManger.instance().courseDSConfig())
            .addDataSource(selectDS(AttestationBulletinManager.DS_GROUP, AttestationBulletinManager.instance().groupDSHandler()).addColumn(Group.title().s()))
            .addDataSource(searchListDS(DS_BULLETINS, sessionAttestationBulletinDS(), sessionAttestationBulletinDSHandler()))
            .create();
    }

    @Bean
    public ColumnListExtPoint sessionAttestationBulletinDS()
    {
        final IFormatter formatter = new CollectionFormatter(source -> {
            if (source == null)
                return "";
            if (source instanceof ITitled)
                return ((ITitled) source).getTitle();
            return source.toString();
        });

        return columnListExtPointBuilder(DS_BULLETINS)
                .addColumn(checkboxColumn(SELECT_COLUMN))
                .addColumn(publisherColumn(WRAPP_PROP_TITLE, SessionAttestationBulletin.title()).order().create())
                .addColumn(textColumn(WRAPP_PROP_GROUP, WRAPP_PROP_GROUP).formatter(formatter))
                .addColumn(textColumn(WRAPP_PROP_STUDENT_COUNT, WRAPP_PROP_STUDENT_COUNT))
                .addColumn(textColumn(WRAPP_PROP_PPS, WRAPP_PROP_PPS).formatter(formatter))
                .addColumn(textColumn(WRAPP_PROP_DISCIPLINE, WRAPP_PROP_DISCIPLINE).order())
                .addColumn(textColumn(WRAPP_PROP_CLOSE_DATE, WRAPP_PROP_CLOSE_DATE))
                .addColumn(actionColumn("automark", new Icon("edit_mark", "Выставить аттестацию на основании рейтинга"), "onClickAutoMark").disabled("ui:attestationClosed").visible("ui:autoMarkEnabled").permissionKey("ui:sec.markAttestationBulletin"))
                .addColumn(actionColumn("print", CommonDefines.ICON_PRINT, "onClickPrint").permissionKey("ui:sec.printAttestationBulletin"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER).disabled("ui:attestationClosed").alert(FormattedMessage.with().template("sessionAttestationBulletinDS.delete.alert").parameter(SessionAttestationBulletin.title().s()).create()).permissionKey("ui:sec.deleteAttestationBulletin"))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> sessionAttestationBulletinDSHandler()
	{
        return new SessionAttestationBulletinDSHandler(getName());
    }
}