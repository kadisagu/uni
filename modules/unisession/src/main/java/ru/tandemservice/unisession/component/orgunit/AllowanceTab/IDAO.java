/* $Id$ */
package ru.tandemservice.unisession.component.orgunit.AllowanceTab;

import ru.tandemservice.uni.dao.IListDataSourceDao;
import ru.tandemservice.uni.dao.IUpdateable;
import ru.tandemservice.unisession.entity.document.SessionObject;

/**
 * @author oleyba
 * @since 4/19/11
 */
public interface IDAO extends IUpdateable<Model>, IListDataSourceDao<Model>
{
    void doUnlock(Long id);
    SessionObject getSessionObject(Model model);
}
