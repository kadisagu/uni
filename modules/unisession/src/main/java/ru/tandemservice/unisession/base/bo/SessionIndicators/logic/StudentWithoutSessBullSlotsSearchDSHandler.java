/**
 *$Id$
 */
package ru.tandemservice.unisession.base.bo.SessionIndicators.logic;

import com.google.common.collect.Lists;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLStatement;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniStudent.logic.abstractHandler.AbstractStudentSearchListDSHandler;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unisession.attestation.entity.SessionAttestation;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlot;
import ru.tandemservice.unisession.base.bo.SessionIndicators.ui.StudentWithoutSessBullSlots.SessionIndicatorsStudentWithoutSessBullSlotsUI;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 05.12.12
 */
public class StudentWithoutSessBullSlotsSearchDSHandler extends AbstractStudentSearchListDSHandler
{
    public static final String STUDENT_REGISTRY_ELEMENT_PART_LIST = "studentRegistryElementPartList";
    private final String EPV_SLOT_STUDENT_MAP = "epvSlotStudentMap";

    public StudentWithoutSessBullSlotsSearchDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected void addAdditionalRestrictions(final DQLSelectBuilder builder, String alias, DSInput input, ExecutionContext context)
    {
        final OrgUnit orgUnit = context.get(SessionIndicatorsStudentWithoutSessBullSlotsUI.PROP_ORG_UNIT);
        final EducationYear eduYear = context.get(SessionIndicatorsStudentWithoutSessBullSlotsUI.PROP_EDU_YEAR);
        final YearDistributionPart yearPart = context.get(SessionIndicatorsStudentWithoutSessBullSlotsUI.PROP_YEAR_PART);
        final Long attNumber = context.get(SessionIndicatorsStudentWithoutSessBullSlotsUI.PROP_ATT_NUMBER);
        final List<EppRegistryStructure> regStructList = context.get(SessionIndicatorsStudentWithoutSessBullSlotsUI.PROP_REGISTRY_STRUCTURE);

        if (eduYear == null || yearPart == null || attNumber == null)
        {
            builder.where(eq(property(Student.id().fromAlias(alias)), value(-1L)));
            return;
        }

        if (!hasSessionAttestation(attNumber, eduYear, yearPart, orgUnit, context))
        {
            builder.where(eq(property(Student.id().fromAlias(alias)), value(-1L)));
            return;
        }

        if (orgUnit != null)
            builder.where(eq(property(Student.educationOrgUnit().groupOrgUnit().fromAlias(alias)), value(orgUnit)));

        final DQLSelectBuilder attSlotExclBuilder = new DQLSelectBuilder().fromEntity(SessionAttestationSlot.class, "slot").column(property(SessionAttestationSlot.studentWpe().id().fromAlias("slot")))
                .where(eq(property(SessionAttestationSlot.bulletin().attestation().number().fromAlias("slot")), value(attNumber)))
                .where(eq(property(SessionAttestationSlot.bulletin().attestation().sessionObject().educationYear().fromAlias("slot")), value(eduYear)))
                .where(eq(property(SessionAttestationSlot.bulletin().attestation().sessionObject().yearDistributionPart().fromAlias("slot")), value(yearPart)));
        if (regStructList != null && !regStructList.isEmpty())
            attSlotExclBuilder.where(notIn(property(SessionAttestationSlot.bulletin().registryElementPart().registryElement().parent().fromAlias("slot")), regStructList));
        if (orgUnit != null)
            attSlotExclBuilder.where(eq(property(SessionAttestationSlot.bulletin().attestation().sessionObject().orgUnit().fromAlias("slot")), value(orgUnit)));

        final DQLSelectBuilder epvSlotInclBuilder = new DQLSelectBuilder().fromEntity(EppStudentWorkPlanElement.class, "epvSlot").column(property(EppStudentWorkPlanElement.student().id().fromAlias("epvSlot")))
                .where(isNull(EppStudentWorkPlanElement.removalDate().fromAlias("epvSlot")))
                .where(eq(property(EppStudentWorkPlanElement.year().educationYear().fromAlias("epvSlot")), value(eduYear)))
                .where(eq(property(EppStudentWorkPlanElement.part().fromAlias("epvSlot")), value(yearPart)))
                .where(notIn(property(EppStudentWorkPlanElement.id().fromAlias("epvSlot")), attSlotExclBuilder.buildQuery()));
        if (regStructList != null && !regStructList.isEmpty())
            epvSlotInclBuilder.where(notIn(property(EppStudentWorkPlanElement.registryElementPart().registryElement().parent().fromAlias("epvSlot")), regStructList));
        if (orgUnit != null)
            epvSlotInclBuilder.where(eq(property(EppStudentWorkPlanElement.student().educationOrgUnit().groupOrgUnit().fromAlias("epvSlot")), value(orgUnit)));

        builder.where(in(property(Student.id().fromAlias(alias)), epvSlotInclBuilder.buildQuery()));

        Map<Long, List<EppRegistryElementPart>> studIdRegElementPartMap = prepareRegistryElementPartMap(builder, attSlotExclBuilder, eduYear, yearPart, regStructList, input.getStartRecord(), input.getCountRecord(), context);

        context.put(EPV_SLOT_STUDENT_MAP, studIdRegElementPartMap);
    }

    @Override
    protected void wrap(DataWrapper wrapper, ExecutionContext context)
    {
        super.wrap(wrapper, context);

        Map<Long, List<EppRegistryElementPart>> studIdRegElementPartMap = context.get(EPV_SLOT_STUDENT_MAP);

        List<EppRegistryElementPart> registryElementPartList = studIdRegElementPartMap.get(wrapper.getId());

        wrapper.setProperty(STUDENT_REGISTRY_ELEMENT_PART_LIST, registryElementPartList);
    }

    /**
     * Есть ли Аттестация с указанными в фильтрах параметрами.
     */
    protected boolean hasSessionAttestation(Long attNumber, EducationYear eduYear, YearDistributionPart yearPart, OrgUnit orgUnit, ExecutionContext context)
    {
        DQLSelectBuilder attBuilder = new DQLSelectBuilder().fromEntity(SessionAttestation.class, "att").column(property(SessionAttestation.id().fromAlias("att")))
                .where(eq(property(SessionAttestation.number().fromAlias("att")), value(attNumber.intValue())))
                .where(eq(property(SessionAttestation.sessionObject().educationYear().fromAlias("att")), value(eduYear)))
                .where(eq(property(SessionAttestation.sessionObject().yearDistributionPart().fromAlias("att")), value(yearPart)));
        if (orgUnit != null)
            attBuilder.where(eq(property(SessionAttestation.sessionObject().orgUnit().fromAlias("att")), value(orgUnit)));

        return attBuilder.createCountStatement(new DQLExecutionContext(getSession())).<Long>uniqueResult() > 0;
    }

    /**
     * Подготавливает мап id студентов на их Дисциплины, по которым студенты не включены в ведомости.
     */
    protected Map<Long, List<EppRegistryElementPart>> prepareRegistryElementPartMap(DQLSelectBuilder studBuilder, final DQLSelectBuilder attSlotExclBuilder, final EducationYear eduYear, final YearDistributionPart yearPart, final List<EppRegistryStructure> regStructList, int start, int max, ExecutionContext context)
    {
        IDQLStatement statement = studBuilder.createStatement(context.getSession());
        statement.setFirstResult(start);
        statement.setMaxResults(max);

        final List<Object[]> registryElementPartList = new ArrayList<>();
        for (List<Student> elements : Lists.partition(statement.<Student>list(), DQL.MAX_VALUES_ROW_NUMBER)) {

            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppStudentWorkPlanElement.class, "slot")
                    .column(property(EppStudentWorkPlanElement.student().id().fromAlias("slot")))
                    .column(property(EppStudentWorkPlanElement.registryElementPart().fromAlias("slot")))
                    .where(in(property(EppStudentWorkPlanElement.student().fromAlias("slot")), elements))
                    .where(isNull(EppStudentWorkPlanElement.removalDate().fromAlias("slot")))
                    .where(eq(property(EppStudentWorkPlanElement.year().educationYear().fromAlias("slot")), value(eduYear)))
                    .where(eq(property(EppStudentWorkPlanElement.part().fromAlias("slot")), value(yearPart)))
                    .where(notIn(property(EppStudentWorkPlanElement.id().fromAlias("slot")), attSlotExclBuilder.buildQuery()));

            if (regStructList != null && !regStructList.isEmpty())
                builder.where(notIn(property(EppStudentWorkPlanElement.registryElementPart().registryElement().parent().fromAlias("slot")), regStructList));

            registryElementPartList.addAll(builder.createStatement(context.getSession()).<Object[]>list());
        }

        Map<Long, List<EppRegistryElementPart>> studIdRegElementPartMap = new HashMap<>();
        for (Object[] objects : registryElementPartList)
        {
            Long studentId = (Long) objects[0];
            EppRegistryElementPart registryElementPart = (EppRegistryElementPart) objects[1];

            List<EppRegistryElementPart> list = studIdRegElementPartMap.get(studentId);
            if (list == null)
                studIdRegElementPartMap.put(studentId, list = new ArrayList<>());
            list.add(registryElementPart);

        }

        return studIdRegElementPartMap;
    }
}
