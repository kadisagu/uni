/* $Id:$ */
package ru.tandemservice.unisession.brs.util;

import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.formatter.IFormatter;

/**
 * @author oleyba
 * @since 10/9/12
 */
public class BrsRatingValueFormatter implements IFormatter<Double>
{
    public static final SpringBeanCache<IFormatter<Double>> instance = new SpringBeanCache<IFormatter<Double>>(BrsRatingValueFormatter.class.getName());

    @Override
    public String format(Double source)
    {
        if (source == null) return "";
        return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(source);
    }
}
