// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisession.component.pps.PpsSessionBulletinList;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.ui.FilterSelectModel;
import ru.tandemservice.uniepp.entity.catalog.EppControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;

import java.util.Collection;

/**
 * @author iolshvang
 * @since 11.04.2011
 */
public class Model implements FilterSelectModel.IDataSettingsProvider
{
    public static final Long BULL_STATUS_EMPTY_ID = 1L;
    public static final Long BULL_STATUS_OPEN_ID = 2L;
    public static final Long BULL_STATUS_CLOSE_ID = 3L;

    private IPrincipalContext _principalContext;
    private Person person;

    private DynamicListDataSource<SessionBulletinDocument> dataSource;

    private IDataSettings settings;

    private ISelectModel yearModel;
    private ISelectModel partsModel;
    private ISelectModel registryOwnerModel;
    private ISelectModel registryElementModel;
    private ISelectModel controlActionTypeModel;
    private ISelectModel groupModel;
    private ISelectModel bullStatusModel;
    private ISelectModel registryStructureModel;
    private ISelectModel courseModel;
    private ISelectModel courseCurrentModel;
    private ISelectModel groupCurrentModel;

    protected EducationYear getYear()
    {
        final Object year = this.getSettings().get("year");
        return year instanceof EducationYear ? (EducationYear) year : null;
    }

    protected void setYear(final EducationYear value)
    {
        this.getSettings().set("year", value);
    }

    protected YearDistributionPart getPart()
    {
        final Object part = this.getSettings().get("part");
        return part instanceof YearDistributionPart ? (YearDistributionPart) part : null;
    }

    protected void setPart(final YearDistributionPart value)
    {
        this.getSettings().set("part", value);
    }

    @SuppressWarnings("unchecked")
    protected Collection<OrgUnit> getRegistryOwners()
    {
        final Object registryOwner = this.getSettings().get("registryOwner");
        return (registryOwner instanceof Collection && ((Collection) registryOwner).size() > 0) ? (Collection) registryOwner : null;
    }

    @SuppressWarnings("unchecked")
    protected Collection<EppRegistryElement> getRegistryElements()
    {
        final Object registryElement = this.getSettings().get("registryElement");
        return (registryElement instanceof Collection && ((Collection) registryElement).size() > 0) ? (Collection) registryElement : null;
    }

    @SuppressWarnings("unchecked")
    protected Collection<EppControlActionType> getControlActionTypes()
    {
        final Object controlActionType = this.getSettings().get("controlActionType");
        return (controlActionType instanceof Collection && ((Collection) controlActionType).size() > 0) ? (Collection) controlActionType : null;
    }

    @SuppressWarnings("unchecked")
    protected Collection<String> getGroups()
    {
        final Object group = this.getSettings().get("group");
        return (group instanceof Collection && ((Collection) group).size() > 0) ? (Collection) group : null;
    }

    protected String getDocumentNumber()
    {
        final Object documentNumber = this.getSettings().get("documentNumber");
        return (String) documentNumber;
    }

    protected DataWrapper getBullStatus()
    {
        final Object bullStatus = this.getSettings().get("bullStatus");
        return (DataWrapper) bullStatus;
    }

    @SuppressWarnings("unchecked")
    protected Collection<EppRegistryStructure> getRegistryStructureList()
    {
        final Object bullStatus = this.getSettings().get("registryStructure");
        return (bullStatus instanceof Collection && ((Collection) bullStatus).size() > 0) ? (Collection) bullStatus : null;
    }

    @SuppressWarnings("unchecked")
    protected Collection<Course> getCourseList()
    {
        final Object course = this.getSettings().get("course");
        return (course instanceof Collection && ((Collection) course).size() > 0) ? (Collection) course : null;
    }

    @SuppressWarnings("unchecked")
    protected Collection<Course> getCourseCurrentList()
    {
        final Object courseCurrent = this.getSettings().get("courseCurrentList");
        return (courseCurrent instanceof Collection && ((Collection) courseCurrent).size() > 0) ? (Collection) courseCurrent : null;
    }

    @SuppressWarnings("unchecked")
    protected Collection<Course> getGroupCurrentList()
    {
        final Object groupCurrent = this.getSettings().get("groupCurrentList");
        return (groupCurrent instanceof Collection && ((Collection) groupCurrent).size() > 0) ? (Collection) groupCurrent : null;
    }

    protected String getStudentLastName()
    {
        final Object studLastName = this.getSettings().get("studLastName");
        return (String) studLastName;
    }

    // accessors

    @Override
    public IDataSettings getSettings()
    {
        return this.settings;
    }

    public void setSettings(final IDataSettings settings)
    {
        this.settings = settings;
    }

    public DynamicListDataSource<SessionBulletinDocument> getDataSource()
    {
        return this.dataSource;
    }

    public void setDataSource(final DynamicListDataSource<SessionBulletinDocument> dataSource)
    {
        this.dataSource = dataSource;
    }

    public ISelectModel getYearModel()
    {
        return this.yearModel;
    }

    public void setYearModel(final ISelectModel yearModel)
    {
        this.yearModel = yearModel;
    }

    public ISelectModel getPartsModel()
    {
        return partsModel;
    }

    public void setPartsModel(ISelectModel partsModel)
    {
        this.partsModel = partsModel;
    }

    public ISelectModel getBullStatusModel()
    {
        return bullStatusModel;
    }

    public void setBullStatusModel(ISelectModel bullStatusModel)
    {
        this.bullStatusModel = bullStatusModel;
    }

    public ISelectModel getRegistryStructureModel()
    {
        return registryStructureModel;
    }

    public void setRegistryStructureModel(ISelectModel registryStructureModel)
    {
        this.registryStructureModel = registryStructureModel;
    }

    public ISelectModel getCourseModel()
    {
        return courseModel;
    }

    public void setCourseModel(ISelectModel courseModel)
    {
        this.courseModel = courseModel;
    }

    public ISelectModel getCourseCurrentModel()
    {
        return courseCurrentModel;
    }

    public void setCourseCurrentModel(ISelectModel courseCurrentModel)
    {
        this.courseCurrentModel = courseCurrentModel;
    }

    public ISelectModel getGroupCurrentModel()
    {
        return groupCurrentModel;
    }

    public void setGroupCurrentModel(ISelectModel groupCurrentModel)
    {
        this.groupCurrentModel = groupCurrentModel;
    }

    public ISelectModel getRegistryOwnerModel()
    {
        return this.registryOwnerModel;
    }

    public void setRegistryOwnerModel(final ISelectModel registryOwnerModel)
    {
        this.registryOwnerModel = registryOwnerModel;
    }

    public ISelectModel getRegistryElementModel()
    {
        return this.registryElementModel;
    }

    public void setRegistryElementModel(final ISelectModel registryElementModel)
    {
        this.registryElementModel = registryElementModel;
    }

    public ISelectModel getControlActionTypeModel()
    {
        return this.controlActionTypeModel;
    }

    public void setControlActionTypeModel(final ISelectModel controlActionTypeModel)
    {
        this.controlActionTypeModel = controlActionTypeModel;
    }

    public ISelectModel getGroupModel()
    {
        return this.groupModel;
    }

    public void setGroupModel(final ISelectModel groupModel)
    {
        this.groupModel = groupModel;
    }

    public Person getPerson()
    {
        return this.person;
    }

    public void setPerson(final Person person)
    {
        this.person = person;
    }

    public IPrincipalContext getPrincipalContext()
    {
        return this._principalContext;
    }

    public void setPrincipalContext(final IPrincipalContext principalContext)
    {
        this._principalContext = principalContext;
    }
}
