package ru.tandemservice.unisession.base.bo.SessionListDocument.logic;

import com.google.common.collect.*;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unisession.dao.document.ISessionDocumentBaseDAO;
import ru.tandemservice.unisession.entity.comission.SessionComission;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionListDocument;
import ru.tandemservice.unisession.entity.document.SessionProjectTheme;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author avedernikov
 * @since 20.07.2016
 */
public class SessionListDocumentMarkDAO extends UniBaseDao implements ISessionListDocumentMarkDAO
{
	@Override
	public List<SessionListDocumentMarkRow> getListDocumentMarkRows(SessionListDocument listDocument)
	{
		List<SessionDocumentSlot> slots = getList(SessionDocumentSlot.class, SessionDocumentSlot.document(), listDocument);
		Collections.sort(slots, Comparator.comparing((SessionDocumentSlot slot) -> slot.getStudentWpeCAction().getRegistryElementTitle()).thenComparing(SessionDocumentSlot::getTermTitle));

		Map<SessionDocumentSlot, SessionMark> slot2mark = getList(SessionMark.class, SessionMark.slot(), slots).stream()
				.collect(Collectors.toMap(SessionMark::getSlot, mark -> mark));

		Multimap<SessionComission, PpsEntry> comission2pps = HashMultimap.create();

		List<SessionComission> comissions = Lists.transform(slots, SessionDocumentSlot::getCommission);
		getList(SessionComissionPps.class, SessionComissionPps.commission(), comissions)
				.forEach(comPps -> comission2pps.put(comPps.getCommission(), comPps.getPps()));

		Map<SessionDocumentSlot, String> slot2theme = getList(SessionProjectTheme.class, SessionProjectTheme.slot(), slots).stream()
				.collect(Collectors.toMap(SessionProjectTheme::getSlot, SessionProjectTheme::getTheme));

		ISessionDocumentBaseDAO baseDAO = ISessionDocumentBaseDAO.instance.get();
		slots.stream()
				.filter(slot -> !slot2theme.containsKey(slot))
				.map(slot -> new HashMap.SimpleEntry<>(slot, baseDAO.themeByLastWpeThemes(slot.getStudentWpeCAction())))
				.filter(entry -> entry.getValue() != null)
				.forEach(entry -> slot2theme.put(entry.getKey(), entry.getValue()));

		List<SessionListDocumentMarkRow> rows = new ArrayList<>();
		slots.forEach(slot -> rows.add(new SessionListDocumentMarkRow(slot, slot2mark.get(slot), comission2pps.get(slot.getCommission()), slot2theme.get(slot))));
		return rows;
	}
}
