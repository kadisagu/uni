/**
 *$Id:$
 */
package ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.ResultPub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.unisession.attestation.entity.report.SessionAttestationResultReport;

/**
 * @author Alexander Shaburov
 * @since 14.11.12
 */
@State
({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "reportId", required = true)
})
public class AttestationReportResultPubUI extends UIPresenter
{
    private Long _reportId;
    private SessionAttestationResultReport _report;

    private CommonPostfixPermissionModelBase _sec;

    @Override
    public void onComponentRefresh()
    {
        _report = DataAccessServices.dao().getNotNull(SessionAttestationResultReport.class, _reportId);
        _sec = new OrgUnitSecModel(getReport().getAttestation().getOrgUnit());
    }

    // Listeners

    public void onClickDelete()
    {
        DataAccessServices.dao().delete(_report.getId());
        deactivate();
    }

    public void onClickPrint()
    {
        byte[] content = _report.getContent().getContent();
        if (content == null)
            throw new ApplicationException("Файл печатной формы пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName(_report.getContent().getFilename()).document(content), true);
    }

    // Getters & Setters

    public CommonPostfixPermissionModelBase getSec()
    {
        return _sec;
    }

    public Long getReportId()
    {
        return _reportId;
    }

    public void setReportId(Long reportId)
    {
        _reportId = reportId;
    }

    public SessionAttestationResultReport getReport()
    {
        return _report;
    }

    public void setReport(SessionAttestationResultReport report)
    {
        _report = report;
    }

    public String getDeleteStorableReportPermissionKey(){ return getSec().getPermission("orgUnit_deleteAttestationReportResultList"); }
}
