/* $Id$ */
package ru.tandemservice.unisession.component.student.StudentSessionTab;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;

import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author oleyba
 * @since 4/7/11
 */
@State( {
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "student.id"),
    @Bind(key = "selectedDataTab", binding = "selectedTab")})
public class Model
{
    private Student student = new Student();

    private String selectedTab;

    public Student getStudent()
    {
        return this.student;
    }

    public void setStudent(final Student student)
    {
        this.student = student;
    }

    public String getSelectedTab()
    {
        return this.selectedTab;
    }

    public void setSelectedTab(final String selectedTab)
    {
        this.selectedTab = selectedTab;
    }
    }
