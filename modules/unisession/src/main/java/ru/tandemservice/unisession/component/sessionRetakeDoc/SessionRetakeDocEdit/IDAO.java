/* $Id$ */
package ru.tandemservice.unisession.component.sessionRetakeDoc.SessionRetakeDocEdit;

import ru.tandemservice.uni.dao.IPrepareable;

/**
 * @author oleyba
 * @since 6/15/11
 */
public interface IDAO extends IPrepareable<Model>
{
    void update(Model model);
}
