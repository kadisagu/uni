/* $Id:$ */
package ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinMark;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.uni.dao.IPrepareable;

/**
 * @author oleyba
 * @since 3/2/11
 */
public interface IDAO extends IPrepareable<Model>
{
    void prepareRatingSettings(Model model);

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void update(Model model);

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    boolean validate(Model model);

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    void prepareAllowance(Model model);

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    void prepareCurrentRatingData(Model model);
}
