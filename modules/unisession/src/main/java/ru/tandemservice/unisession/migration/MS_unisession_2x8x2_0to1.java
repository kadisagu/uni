package ru.tandemservice.unisession.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLUpdateQuery;

/**
 * Автоматически сгенерированная миграция
 */
public class MS_unisession_2x8x2_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[] {
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность sessionCurrentMarkCatalogItem
		// 1. Переименована. 2. Более не является дочерней сущностью.

		// Создаем недостающие колонки
		tool.createColumn("session_c_cur_mark_t", new DBColumn("discriminator", DBType.SHORT));
		tool.createColumn("session_c_cur_mark_t", new DBColumn("code_p", DBType.createVarchar(255)));
		tool.createColumn("session_c_cur_mark_t", new DBColumn("title_p", DBType.createVarchar(1200)));
		tool.createColumn("session_c_cur_mark_t", new DBColumn("shorttitle_p", DBType.createVarchar(255)));
		tool.createColumn("session_c_cur_mark_t", new DBColumn("printtitle_p", DBType.createVarchar(255)));

		// Перемещаем данные из родительской таблицы
		final SQLUpdateQuery upd = new SQLUpdateQuery("session_c_cur_mark_t", "a");
		upd.set("discriminator", "b.discriminator");
		upd.set("code_p", "b.code_p");
		upd.set("title_p", "b.title_p");
		upd.set("shorttitle_p", "b.shorttitle_p");
		upd.set("printtitle_p", "b.printtitle_p");
		upd.from(SQLFrom.table("session_c_mark_base_t", "b"));
		upd.where("a.id=b.id");

		int ret = tool.executeUpdate(tool.getDialect().getSQLTranslator().toSql(upd));

		if (Debug.isEnabled()) {
			System.out.println("MS_unisession_2x8x2_0to1: " + ret + " rows moved.");
		}

		tool.setColumnNullable("session_c_cur_mark_t", "discriminator", false);
		tool.setColumnNullable("session_c_cur_mark_t", "code_p", false);
		tool.setColumnNullable("session_c_cur_mark_t", "shorttitle_p", false);

		// Удаляем строки из родительской таблицы
		tool.deleteRowsFromParentTables("session_c_cur_mark_t", "session_c_mark_base_t");

		// Переименовываем таблицу и сущность
		tool.renameTable("session_c_cur_mark_t", "session_c_att_mark_t");
		tool.entityCodes().delete("sessionAttestationMarkCatalogItem");
		tool.entityCodes().rename("sessionCurrentMarkCatalogItem", "sessionAttestationMarkCatalogItem");


		////////////////////////////////////////////////////////////////////////////////
		// сущность sessionAttestationSlot. Переименовано поле.
		tool.renameColumn("session_att_slot_t", "currentmark_id", "mark_id");
	}
}