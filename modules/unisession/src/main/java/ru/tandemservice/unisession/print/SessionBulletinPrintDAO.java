/*$Id$*/

package ru.tandemservice.unisession.print;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.NumberAsStringComparator;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.DaoCache;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.DelegatePropertyComparator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniepp.entity.catalog.EppControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale;
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionTypeCodes;
import ru.tandemservice.uniepp.entity.catalog.gen.EppGroupTypeGen;
import ru.tandemservice.uniepp.entity.registry.EppRegistryAttestation;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryPractice;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4ActionType;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4ActionTypeRow;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.brs.util.BrsRatingValueFormatter;
import ru.tandemservice.unisession.dao.bulletin.ISessionBulletinDAO;
import ru.tandemservice.unisession.dao.document.ISessionDocumentBaseDAO;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkStateCatalogItem;
import ru.tandemservice.unisession.entity.catalog.UnisessionBulletinTemplate;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.*;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.mark.SessionMarkRatingData;
import ru.tandemservice.unisession.util.SessionBulletinPrintUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 4/2/11
 */
public class SessionBulletinPrintDAO extends UniBaseDao implements ISessionBulletinPrintDAO
{
    protected static final DateFormatter DATE_FORMATTER = new DateFormatter("dd.MM.yy");

    /**
     * Типы колонок ведомости
     */
    protected enum ColumnType
    {
        NUMBER,
        FIO,
        MARK,
        BOOK_NUMBER,
        GROUP,
        THEME,
        EMPTY,
        EMPTY2,
        POINT,
        CURRENT_RATING,
        SCORED_POINTS,
        DATE,
        PPS,
        STUDENT_NUMBER,
    }

    /**
     * Метод для кастомизации в проектах
     * можно переопределить и сделать обработку дополнительных меток,
     * или вообще всю печать заново
     * Вызывается после того, как по шаблону произошла печать дефолтным методом,
     * т.е. меток, которые обрабатывает дефолтная печать, там не будет
     *
     * @param printInfo      печатная форма и данные
     * @param markAmountData количества оценок и отметок, уже выставленных по ведомости в системе
     */
    protected void printAdditionalInfo(final BulletinPrintInfo printInfo, MarkAmountData markAmountData)
    {
    }

    /**
     * Метод для кастомизации в проектах
     * Позволяет переопределить обработку дефолтных меток шаблона
     *
     * @param modifier  - обрабочик, в котором уже есть дефолтные значения по всем дефолтным меткам
     * @param printInfo - печатная форма и данные ведомости
     */
    protected void customizeSimpleTags(final RtfInjectModifier modifier, final BulletinPrintInfo printInfo)
    {
    }

    /**
     * Метод для кастомизации в проектах
     * Позволяет переопределить группировку и сортировку студентов в ведомости
     * При выводе строк в печатной форме они будут выводиться по группам, определенным в этом мапе
     * Если группа не одна, то перед набором студентов группы будет выводиться строка с названием группы
     * При этом строка с названием группы будет болдом и все ячейки смерджены в одну
     *
     * @param printInfo печатная форма и данные ведомости
     * @return {название группы -> отсортированный список студентов этой группы}
     */
    protected TreeMap<String, List<BulletinPrintRow>> getGroupedAndSortedContent(final BulletinPrintInfo printInfo)
    {
        final TreeSet<String> eduTitles = new TreeSet<>();
        for (final EppStudentWpeCAction slot : printInfo.getContentMap().keySet())
        {
            final EppRealEduGroup4ActionTypeRow groupInfo = printInfo.getContentMap().get(slot).getEduGroupRow();
            eduTitles.add(groupInfo == null ? "" : groupInfo.getStudentEducationOrgUnit().getEducationLevelHighSchool().getTitle());
        }

        final TreeMap<String, List<BulletinPrintRow>> result = new TreeMap<>();
        if (eduTitles.size() <= 1)
        {
            result.put("", new ArrayList<>(printInfo.getContentMap().values()));
            return result;
        }

        // группируем
        for (final BulletinPrintRow row : printInfo.getContentMap().values())
        {
            final EppRealEduGroup4ActionTypeRow groupInfo = row.getEduGroupRow();
            final String title = groupInfo == null ? "" : groupInfo.getStudentEducationOrgUnit().getEducationLevelHighSchool().getTitle();
            SafeMap.safeGet(result, title, ArrayList.class).add(row);
        }

        return result;
    }

    /**
     * Метод для кастомизации в проектах
     * Позволяет определить список колонок для печати данной конкретной ведомости
     * Может содержать меньше колонок, чем шаблон, тогда лишние колонки при печати будут удалены
     * Например, если все студенты из одной группы, то можно при печати убрать колонку с группой
     *
     * @param printInfo печатная форма и данные ведомости
     * @return список колонок
     */
    protected List<ColumnType> prepareColumnList(final BulletinPrintInfo printInfo)
    {
        final Set<String> groupTitles = new HashSet<>();
        for (final BulletinPrintRow row : printInfo.getContentMap().values())
        {
            final EppRealEduGroup4ActionTypeRow groupInfo = row.getEduGroupRow();
            groupTitles.add(groupInfo == null ? "" : StringUtils.trimToEmpty(groupInfo.getStudentGroupTitle()));
        }
        final boolean printGroup = groupTitles.size() > 1;
        final boolean printTheme = ISessionBulletinDAO.instance.get().isBulletinRequireTheme(printInfo.getBulletin());

        final List<ColumnType> columns = new ArrayList<>();
        columns.addAll(Arrays.asList(
                ColumnType.NUMBER,
                ColumnType.FIO,
                ColumnType.BOOK_NUMBER));
        if (printGroup)
        {
            columns.add(ColumnType.GROUP);
        }
        if (printTheme && !printInfo.isUseCurrentRating())
        {
            columns.add(ColumnType.THEME);
            if (printInfo.isUsePoints())
            {
                columns.add(ColumnType.POINT);
            }
        } else if (printInfo.isUseCurrentRating())
            columns.addAll(Arrays.asList(
                    ColumnType.CURRENT_RATING,
                    ColumnType.SCORED_POINTS,
                    ColumnType.POINT));
        else if (printInfo.isUsePoints())
        {
            columns.add(ColumnType.POINT);
        }
        columns.add(ColumnType.MARK);
        columns.add(ColumnType.EMPTY);
        return columns;
    }

    /**
     * Метод для кастомизации в проектах
     * Позволяет задать список колонок шаблона ведомости
     * Если в шаблоне проекта используются не те колонки, или не в том порядке, в каком они есть в дефолтном шаблоне
     *
     * @param printInfo ведомость
     * @return список колонок в шаблоне, который будет использован при печати такой ведомости
     */
    protected List<ColumnType> getTemplateColumnList(final BulletinPrintInfo printInfo)
    {
        if (printInfo.isUseCurrentRating())
        {
            return Arrays.asList(
                    ColumnType.NUMBER,
                    ColumnType.FIO,
                    ColumnType.BOOK_NUMBER,
                    ColumnType.GROUP,
                    ColumnType.CURRENT_RATING,
                    ColumnType.SCORED_POINTS,
                    ColumnType.POINT,
                    ColumnType.MARK,
                    ColumnType.EMPTY);
        }
        if (printInfo.isUsePoints())
        {
            return Arrays.asList(
                    ColumnType.NUMBER,
                    ColumnType.FIO,
                    ColumnType.BOOK_NUMBER,
                    ColumnType.GROUP,
                    ColumnType.THEME,
                    ColumnType.POINT,
                    ColumnType.MARK,
                    ColumnType.EMPTY);
        }
        return Arrays.asList(
                ColumnType.NUMBER,
                ColumnType.FIO,
                ColumnType.BOOK_NUMBER,
                ColumnType.GROUP,
                ColumnType.THEME,
                ColumnType.MARK,
                ColumnType.EMPTY);
    }

    /**
     * Метод для кастомизации в проектах
     * Позволяет переопределить печать строчки ведомости
     *
     * @param printRow  данные для печати в строке
     * @param columns   список колонок, в том порядке, в каком надо выводить данные в результирующий массив
     * @param rowNumber номер строки ведомости
     * @return Массив строк с данными по колонкам
     */
    protected String[] fillTableRow(final BulletinPrintRow printRow, final List<ColumnType> columns, final int rowNumber)
    {
        final List<String> result = new ArrayList<>();

        for (final ColumnType column : columns)
        {
            SessionMarkCatalogItem mark = printRow.getMark() == null ? null : printRow.getMark().getValueItem();
            Double points = printRow.getMark() == null ? null : printRow.getMark().getPoints();
            Double currentRating = printRow.getSlotRatingData() == null ? null : printRow.getSlotRatingData().getFixedCurrentRating();
            Double scoredPoints = printRow.getMarkRatingData() == null ? null : printRow.getMarkRatingData().getScoredPoints();

            switch (column)
            {
                case EMPTY:
                    result.add("");
                    break;
                case EMPTY2:
                    result.add("");
                    break;
                case FIO:
                    result.add(printRow.getSlot().getActualStudent().getPerson().getFullFio());
                    break;
                case BOOK_NUMBER:
                    result.add(StringUtils.trimToEmpty(printRow.getSlot().getActualStudent().getBookNumber()));
                    break;
                case GROUP:
                    result.add(printRow.getEduGroupRow() == null ? "" : StringUtils.trimToEmpty(printRow.getEduGroupRow().getStudentGroupTitle()));
                    break;
                case THEME:
                    result.add(printRow.getTheme() == null ? "" : StringUtils.trimToEmpty(printRow.getTheme().getTheme()));
                    break;
                case MARK:
                    result.add(mark == null ? "" : StringUtils.trimToEmpty(mark.getPrintTitle()));
                    break;
                case NUMBER:
                    result.add(String.valueOf(rowNumber));
                    break;
                case CURRENT_RATING:
                    result.add(BrsRatingValueFormatter.instance.get().format(currentRating));
                    break;
                case POINT:
                    result.add(mark instanceof SessionMarkStateCatalogItem ? "-" : BrsRatingValueFormatter.instance.get().format(points));
                    break;
                case SCORED_POINTS:
                    result.add(mark instanceof SessionMarkStateCatalogItem ? "-" : BrsRatingValueFormatter.instance.get().format(scoredPoints));
                    break;
                case DATE:
                    result.add(printRow.getMark() == null || printRow.getMark().getPerformDate() == null ? "" : DATE_FORMATTER.format(printRow.getMark().getPerformDate()));
                    break;
                case PPS:
                    result.add(UniStringUtils.join(printRow.getCommission(), PpsEntry.person().identityCard().fio().s(), ", "));
                    break;
                case STUDENT_NUMBER:
                    result.add(printRow.getSlot().getActualStudent().getPersonalNumber());
                    break;
            }
        }
        return result.toArray(new String[result.size()]);
    }

    /**
     * Метод для кастомизации в проектах
     * Позволяет переопределить определение шаблонов по ведомости
     * Чтобы ведомость печаталась по определенному шаблону,
     * нужно в BulletinPrintInfo инициализировать RtfPrintDocument на clone нужного шаблона
     * Соответственно, в переданном мапе нужно сделать именно это в значениях
     *
     * @param printInfoMap ведомость -> печатная форма с данными ведомости
     */
    protected void initTemplates(final Map<SessionBulletinDocument, BulletinPrintInfo> printInfoMap)
    {
        final Map<MultiKey, RtfDocument> templateMap = new HashMap<>();
        final RtfReader reader = new RtfReader();
        for (final UnisessionBulletinTemplate template : this.getCatalogItemList(UnisessionBulletinTemplate.class))
        {
            if (template.getType() != null)
                templateMap.put(new MultiKey(template.getType().getCode(), template.isPrintCurrentRating(), template.isPrintCurrentRating() || template.isPrintPoints()), reader.read(template.getContent()));
        }

        for (final BulletinPrintInfo printInfo : printInfoMap.values())
        {
            MultiKey key = new MultiKey(printInfo.getBulletin().getGroup().getActionType().getCode(), printInfo.isUseCurrentRating(), printInfo.isUseCurrentRating() || printInfo.isUsePoints());
            RtfDocument template = templateMap.get(key);
            if (null != template)
                printInfo.setDocument(template.getClone());
        }
    }

    // ---- методы ISessionBulletinPrintDAO

    @Override
    public byte[] printBulletinListToZipArchive(final Collection<Long> ids)
    {
        final Map<SessionBulletinDocument, BulletinPrintInfo> bulletinMap = this.prepareData(ids);
        if (bulletinMap.isEmpty())
        {
            throw new ApplicationException("Не выбраны ведомости для печати.");
        }

        ByteArrayOutputStream out;
        byte[] result;
        try
        {
            out = new ByteArrayOutputStream();
            final ZipOutputStream zipOut = new ZipOutputStream(out);
            for (final Map.Entry<SessionBulletinDocument, BulletinPrintInfo> entry : bulletinMap.entrySet())
            {
                final SessionBulletinDocument bulletin = entry.getKey();
                final byte[] content;
                if (bulletin.isClosed())
                {
                    SessionDocumentPrintVersion rel = this.get(SessionDocumentPrintVersion.class, SessionDocumentPrintVersion.doc().id().s(), bulletin.getId());
                    if (rel == null)
                        throw new ApplicationException("Отсутствует печатная форма закрытого документа сессии.");
                    content = rel.getContent();
                } else
                {
                    final RtfDocument document = this.printBulletin(entry.getValue());
                    content = RtfUtil.toByteArray(document);
                }
                zipOut.putNextEntry(new ZipEntry("Vedomost " + CoreStringUtils.transliterate(bulletin.getNumber()) + " " + bulletin.getSessionObject().getEducationYear().getTitle() + ".xls"));
                zipOut.write(content);
                zipOut.closeEntry();
            }
            zipOut.close();
            result = out.toByteArray();
            out.close();
        } catch (final IOException e)
        {
            throw new IllegalStateException(e);
        }

        return result;
    }

    @Override
    public RtfDocument printBulletinList(final Collection<Long> ids)
    {
        final Map<SessionBulletinDocument, BulletinPrintInfo> bulletinMap = this.prepareData(ids);
        if (bulletinMap.isEmpty())
        {
            throw new ApplicationException("Не выбраны ведомости для печати.");
        }

        RtfDocument result = null;

        for (final Map.Entry<SessionBulletinDocument, BulletinPrintInfo> entry : bulletinMap.entrySet())
        {
            final SessionBulletinDocument bulletin = entry.getKey();
            if (bulletin.isClosed() && bulletinMap.entrySet().size() > 1)
            {
                throw new ApplicationException("Массовая печать закрытых ведомостей невозможна. Выберите только открытые ведомости.");
            }
            final RtfDocument document = this.printBulletin(entry.getValue());
            if (null == result)
            {
                result = document;
            } else
            {
                RtfUtil.modifySourceList(result.getHeader(), document.getHeader(), document.getElementList());
                result.getElementList().addAll(document.getElementList());
            }
        }
        if (null == result)
        {
            throw new ApplicationException("Не выбраны ведомости для печати.");
        }
        return result;
    }

    @Override
    public RtfDocument printBulletin(final Long id)
    {
        return this.printBulletinList(Collections.singleton(id));
    }

    // вспомогательное

    private RtfDocument printBulletin(final BulletinPrintInfo printInfo)
    {
        final SessionBulletinDocument bulletin = printInfo.getBulletin();

        if (null == printInfo.getDocument())
        {
            throw new ApplicationException("Для формы контроля и настроек рейтинга ведомости " + bulletin.getTitle() + " не задан печатный шаблон ведомости.");
        }

        final RtfDocument document = printInfo.getDocument();

        // печатаем шапку

        final TopOrgUnit academy = TopOrgUnit.getInstance();
        final OrgUnit orgUnit = bulletin.getSessionObject().getOrgUnit();
        final TreeSet<String> eduTitles = new TreeSet<>();
        final TreeSet<String> groupTitles = new TreeSet<>();
        final TreeSet<String> courseTitles = new TreeSet<>();
        final TreeSet<String> termNumbers = new TreeSet<>();
        for (final EppStudentWpeCAction slot : printInfo.getContentMap().keySet())
        {
            final EppRealEduGroup4ActionTypeRow groupInfo = printInfo.getContentMap().get(slot).getEduGroupRow();
            if (null != groupInfo)
            {
                eduTitles.add(groupInfo.getStudentEducationOrgUnit().getEducationLevelHighSchool().getTitle());
                groupTitles.add(StringUtils.trimToEmpty(groupInfo.getStudentGroupTitle()));
            }
            courseTitles.add(String.valueOf(slot.getStudentWpe().getCourse().getIntValue()));
            termNumbers.add(String.valueOf(slot.getStudentWpe().getTerm().getIntValue()));
        }
        final EppRegistryElementPart registryElementPart = bulletin.getGroup().getActivityPart();
        final EppRegistryElement registryElement = registryElementPart.getRegistryElement();
        List<PpsEntry> commission = printInfo.getCommission();
        if (commission == null)
        {
            commission = Collections.emptyList();
        }
        final int commissionSize = commission.size();

        final RtfInjectModifier modifier = new RtfInjectModifier();
        modifier.put("academyTitle", academy.getPrintTitle());
        modifier.put("ouTitle", orgUnit.getPrintTitle());
        printOuData(modifier, "document", orgUnit);
        modifier.put("documentNumber", bulletin.getNumber());
        modifier.put("educationOrgUnitTitle", StringUtils.join(eduTitles.iterator(), ", "));
        modifier.put("eduYear", bulletin.getSessionObject().getEducationYear().getTitle());
        modifier.put("course", StringUtils.join(courseTitles.iterator(), ", "));
        modifier.put("group", StringUtils.join(groupTitles.iterator(), ", "));
        modifier.put("term", bulletin.getSessionObject().getYearDistributionPart().getTitle());
        modifier.put("termNumber", StringUtils.join(termNumbers.iterator(), ", "));
        if (registryElement instanceof EppRegistryAttestation)
        {
            modifier.put("regType", "Мероприятие ГИА");
        } else if (registryElement instanceof EppRegistryPractice)
        {
            modifier.put("regType", "Практика");
        } else
        {
            modifier.put("regType", "Дисциплина");
        }
        modifier.put("registryElement", registryElement.getTitle());
        modifier.put("totalHours", String.valueOf(registryElementPart.getSize() / 100));
        modifier.put("cathedra", registryElement.getOwner().getPrintTitle());

        final EppControlActionType caType = bulletin.getGroup().getActionType();
        final String caTypes = printInfo.getContentMap().values().stream()
                .map(e -> e.getSlot().getStudentWpeCAction().getType())
                .distinct().sorted(Comparator.comparingInt(EppGroupTypeGen::getPriority))
                .map(EppGroupTypeGen::getTitle)
                .collect(Collectors.joining(", "));
        modifier.put("caType_N", caTypes);

        switch (caType.getCode())
        {
            case EppFControlActionTypeCodes.CONTROL_ACTION_COURSE_PROJECT:
                modifier.put("caType", "Курсовой проект");
                break;
            case EppFControlActionTypeCodes.CONTROL_ACTION_COURSE_WORK:
                modifier.put("caType", "Курсовую работу");
                break;
            case EppFControlActionTypeCodes.CONTROL_ACTION_CONTROL_WORK:
                modifier.put("caType", "Контрольную работу");
                break;
            case EppFControlActionTypeCodes.CONTROL_ACTION_EXAM:
            case EppFControlActionTypeCodes.CONTROL_ACTION_EXAM_ACCUM:
                modifier.put("caType", "Экзамен");
                break;
            case EppFControlActionTypeCodes.CONTROL_ACTION_SETOFF:
            case EppFControlActionTypeCodes.CONTROL_ACTION_SETOFF_DIFF:
                modifier.put("caType", "Зачет");
                break;
            default:
                modifier.put("caType", caType.getTitle());
                break;
        }
        modifier.put("acceptW", commissionSize > 1 ? "приняли" : "принял");

        if (caType.getCode().equals(EppFControlActionTypeCodes.CONTROL_ACTION_EXAM) || caType.getCode().equals(EppFControlActionTypeCodes.CONTROL_ACTION_EXAM_ACCUM))
        {
            modifier.put("documentType", "Экзаменационная ведомость");
        } else
        {
            modifier.put("documentType", "Зачетная ведомость");
        }

        modifier.put("tutors", UniStringUtils.join(commission, PpsEntry.person().identityCard().fio().s(), ", "));
        modifier.put("performDate", bulletin.getPerformDate() == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(bulletin.getPerformDate()));
        modifier.put("formingDate", bulletin.getFormingDate() == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(bulletin.getFormingDate()));
        modifier.put("date", bulletin.getPerformDate() == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(bulletin.getPerformDate()));
        modifier.put("labor", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(registryElementPart.getLaborAsDouble()));

        SessionReportManager.addOuLeaderData(modifier, orgUnit, "ouleader", "FIOouleader");

        // направления подготовки профессионального образования
        SessionBulletinPrintUtils.printEduProgramSubject(modifier, printInfo.getContentMap().keySet());

        // направленности высшего профессионального образования
        SessionBulletinPrintUtils.printEduProgramSpecialization(modifier, printInfo.getContentMap().keySet());

        // уровни образования
        SessionBulletinPrintUtils.printEduLevels(modifier, printInfo.getContentMap().keySet());

        // формы обучения студентов
        SessionBulletinPrintUtils.printDevelopForms(modifier, printInfo.getContentMap().keySet());

        this.customizeSimpleTags(modifier, printInfo);

        modifier.modify(document);

        new RtfTableModifier().put("sign", new String[commissionSize == 0 ? 0 : commissionSize - 1][]).modify(document);

        // печатаем таблицу
        final List<ColumnType> columns = this.prepareColumnList(printInfo);
        final List<ColumnType> templateColumns = this.getTemplateColumnList(printInfo);
        final int fioColumnIndex = templateColumns.indexOf(ColumnType.FIO);
        if (fioColumnIndex == -1)
        {
            throw new IllegalStateException(); // колонка с фио должна быть
        }

        final TreeMap<String, List<BulletinPrintRow>> groupedContentMap = this.getGroupedAndSortedContent(printInfo);
        final boolean useGrouping = groupedContentMap.size() > 1;

        final List<String[]> tableRows = new ArrayList<>();
        int i = 1;

        for (final Map.Entry<String, List<BulletinPrintRow>> entry : groupedContentMap.entrySet())
        {
            if (useGrouping)
            {
                tableRows.add(new String[]{entry.getKey()});
            }
            for (final BulletinPrintRow row : entry.getValue())
            {
                tableRows.add(this.fillTableRow(row, columns, i++));
            }
        }

        final String[][] tableData = tableRows.toArray(new String[tableRows.size()][]);
        final RtfTableModifier tableModifier = new RtfTableModifier().put("T", tableData);
        tableModifier.put("T", new RtfRowIntercepterBase()
        {
            @Override
            public void beforeModify(final RtfTable table, final int currentRowIndex)
            {
                for (int i = templateColumns.size() - 1; i >= 0; i--)
                {
                    if (!columns.contains(templateColumns.get(i)))
                    {
                        deleteCellFioIncrease(table.getRowList().get(currentRowIndex), i); //ячейка в строке с меткой T
                        deleteCellFioIncrease(table.getRowList().get(currentRowIndex - 1), i); //ячейка в шапке
                    }
                }
            }

            private void deleteCellFioIncrease(final RtfRow row, final int cellIndex)
            {
                final RtfCell cell = row.getCellList().get(cellIndex);
                final RtfCell destCell = row.getCellList().get(fioColumnIndex);
                destCell.setWidth(destCell.getWidth() + cell.getWidth());
                row.getCellList().remove(cellIndex);
            }

            @Override
            public List<IRtfElement> beforeInject(final RtfTable table, final RtfRow row, final RtfCell cell, final int rowIndex, final int colIndex, final String value)
            {
                if (useGrouping)
                {
                    if (tableData[rowIndex].length == 1)
                    {
                        return new RtfString().boldBegin().append(value).boldEnd().toList();
                    }
                }
                return null;
            }

            @Override
            public void afterModify(final RtfTable table, final List<RtfRow> newRowList, final int startIndex)
            {
                if (useGrouping)
                {
                    for (int i = 0; i < tableData.length; i++)
                    {
                        if (tableData[i].length == 1)
                        {
                            final RtfRow row = newRowList.get(startIndex + i);
                            RtfUtil.unitAllCell(row, 0);
                        }
                    }
                }
            }
        });
        tableModifier.modify(document);

        // печатаем таблицу с количеством оценок внизу

        final List<SessionMarkGradeValueCatalogItem> markList = this.getGradeScaleMarkList(bulletin);

        final String[][] markTableData = new String[Math.max(3, 1 + markList.size())][4];
        markTableData[0][0] = "Не явилось:";
        markTableData[0][1] = "";
        markTableData[1][0] = "По уважительной причине:";
        markTableData[1][1] = "";
        markTableData[2][0] = "По неуважительной причине:";
        markTableData[2][1] = "";
        markTableData[0][2] = "Получено оценок:";
        markTableData[0][3] = "";

        int row = 1;
        for (final SessionMarkGradeValueCatalogItem mark : markList)
        {
            markTableData[row][2] = mark.getTitle();
            markTableData[row][3] = "";
            row++;
        }

        final MarkAmountData markAmountData = new MarkAmountData(printInfo.getContentMap());
        boolean doPrintMarkAmounts = markAmountData.getTotalMarkAmount() != 0;
        if (doPrintMarkAmounts)
        {

            markTableData[0][1] = String.valueOf(markAmountData.getNotAppearCount());
            markTableData[1][1] = String.valueOf(markAmountData.getNotAppearValid());
            markTableData[2][1] = String.valueOf(markAmountData.getNotAppearInvalid());
            markTableData[0][3] = String.valueOf(markAmountData.getTotalGradeAmount());

            row = 1;
            for (final SessionMarkGradeValueCatalogItem mark : markList)
            {
                markTableData[row][2] = mark.getTitle();
                Integer count = markAmountData.getMarkAmountMap().get(mark);
                markTableData[row][3] = count == null ? "0" : String.valueOf(count);
                row++;
            }
        }

        new RtfTableModifier().put("TB", markTableData).modify(document);

        // печатаем что там еще захочется отдельно допечатать в проектном слое
        this.printAdditionalInfo(printInfo, markAmountData);
        return document;
    }

    protected void printOuData(RtfInjectModifier modifier, String ouKind, OrgUnit ou)
    {
        modifier.put(ouKind + "Ou", ou.getPrintTitle());
        modifier.put(ouKind + "OuHeadIof", "");
        modifier.put(ouKind + "OuHeadFio", "");
        try
        {
            IdentityCard headIdc = (IdentityCard) ou.getHead().getEmployee().getPerson().getIdentityCard();
            modifier.put(ouKind + "OuHeadIof", headIdc.getIof());
            modifier.put(ouKind + "OuHeadFio", headIdc.getFio());
        } catch (NullPointerException e)
        {
            // ну и нету там значит руководителя
        }
    }

    protected List<SessionMarkGradeValueCatalogItem> getGradeScaleMarkList(final SessionBulletinDocument bulletin)
    {
        EppRegistryElementPart discipline = bulletin.getGroup().getActivityPart();
        final EppGradeScale scale = ISessionDocumentBaseDAO.instance.get().getGradeScale(discipline, bulletin.getGroup().getActionType());
        final String key = "SessionMarkGradeValueCatalogItem.list." + scale.getCode();

        List<SessionMarkGradeValueCatalogItem> result = DaoCache.get(key);
        if (null == result)
        {
            List<SessionMarkGradeValueCatalogItem> marks = getList(SessionMarkGradeValueCatalogItem.class, SessionMarkGradeValueCatalogItem.scale(), scale, SessionMarkGradeValueCatalogItem.P_PRIORITY);
            result = Collections.unmodifiableList(new ArrayList<>(Lists.reverse(marks)));
            DaoCache.put(key, result);
        }
        return result;
    }

	/** Заполнение контейнера данных о ведомости. */
    protected Map<SessionBulletinDocument, BulletinPrintInfo> prepareData(final Collection<Long> bulletinIds)
    {
        final List<SessionBulletinDocument> bulletinList = this.getList(SessionBulletinDocument.class, SessionBulletinDocument.id().s(), bulletinIds);
        Collections.sort(bulletinList, new DelegatePropertyComparator(NumberAsStringComparator.INSTANCE, SessionBulletinDocument.P_NUMBER));

        final Map<SessionBulletinDocument, BulletinPrintInfo> result = new LinkedHashMap<>();
        for (final SessionBulletinDocument bulletin : bulletinList)
        {
            BulletinPrintInfo printData = createPrintInfo(bulletin);

            result.put(bulletin, printData);

            Map<ISessionBrsDao.ISessionRatingSettingsKey, ISessionBrsDao.ISessionRatingSettings> ratingSettings = ISessionBrsDao.instance.get().getRatingSettings(bulletin);
            printData.setUseCurrentRating(CollectionUtils.exists(ratingSettings.values(), ISessionBrsDao.ISessionRatingSettings::useCurrentRating));
            printData.setUsePoints(CollectionUtils.exists(ratingSettings.values(), ISessionBrsDao.ISessionRatingSettings::usePoints));
        }

        this.initTemplates(result);
        this.initContentData(result);
        this.initCommissionData(result);
        this.initEppGroupData(result);

        return result;
    }

	/**
	 * Создать пустой контейнер для данных о ведомости. Для переопределения в проектах (создания наследников {@link BulletinPrintInfo} с доп. данными).
	 * Заполнение - в {@link SessionBulletinPrintDAO#prepareData}.
	 */
	protected BulletinPrintInfo createPrintInfo(SessionBulletinDocument bulletin)
	{
		return new BulletinPrintInfo(bulletin);
	}

    private void initEppGroupData(final Map<SessionBulletinDocument, BulletinPrintInfo> bulletinMap)
    {
        final Map<EppRealEduGroup4ActionType, BulletinPrintInfo> groupMap = new HashMap<>();
        for (final Map.Entry<SessionBulletinDocument, BulletinPrintInfo> entry : bulletinMap.entrySet())
        {
            groupMap.put(entry.getKey().getGroup(), entry.getValue());
        }
        for (List<EppRealEduGroup4ActionType> elements : Iterables.partition(groupMap.keySet(), DQL.MAX_VALUES_ROW_NUMBER))
        {
            final DQLSelectBuilder dql = new DQLSelectBuilder()
                    .fromEntity(EppRealEduGroup4ActionTypeRow.class, "rel")
                    .fetchPath(DQLJoinType.inner, EppRealEduGroup4ActionTypeRow.studentWpePart().fromAlias("rel"), "st")
                    .column(property("rel"))
                    .where(in(property(EppRealEduGroup4ActionTypeRow.group().fromAlias("rel")), elements))
                    .where(isNull(EppRealEduGroup4ActionTypeRow.removalDate().fromAlias("rel")));
            for (final EppRealEduGroup4ActionTypeRow rel : dql.createStatement(SessionBulletinPrintDAO.this.getSession()).<EppRealEduGroup4ActionTypeRow>list())
            {
                BulletinPrintRow row = groupMap.get(rel.getGroup()).getContentMap().get(rel.getStudentWpePart());
                if (null != row)
                    row.setEduGroupRow(rel);
            }
        }
    }

    private void initContentData(final Map<SessionBulletinDocument, BulletinPrintInfo> bulletinMap)
    {
        for (List<SessionBulletinDocument> elements : Iterables.partition(bulletinMap.keySet(), DQL.MAX_VALUES_ROW_NUMBER))
        {
            final DQLSelectBuilder dql = new DQLSelectBuilder()
                    .fromEntity(SessionDocumentSlot.class, "slot")
                    .joinEntity("slot", DQLJoinType.left, SessionMark.class, "mark", eq(property(SessionMark.slot().id().fromAlias("mark")), property("slot.id")))
                    .joinEntity("slot", DQLJoinType.left, SessionProjectTheme.class, "theme", eq(property(SessionProjectTheme.slot().fromAlias("theme")), property("slot")))
                    .joinEntity("slot", DQLJoinType.left, SessionSlotRatingData.class, "slotR", eq(property(SessionSlotRatingData.slot().fromAlias("slotR")), property("slot")))
                    .joinEntity("mark", DQLJoinType.left, SessionMarkRatingData.class, "markR", eq(property(SessionMarkRatingData.mark().fromAlias("markR")), property("mark")))
                    .joinPath(DQLJoinType.inner, SessionDocumentSlot.actualStudent().person().identityCard().fromAlias("slot"), "idc")
                    .column("slot")
                    .column("mark")
                    .column("theme")
                    .column("slotR")
                    .column("markR")
                    .where(in(property(SessionDocumentSlot.document().fromAlias("slot")), elements))
                    .order(property("idc", IdentityCard.lastName()))
                    .order(property("idc", IdentityCard.firstName()))
                    .order(property("idc", IdentityCard.middleName()))
                    .order(property(SessionDocumentSlot.actualStudent().educationOrgUnit().educationLevelHighSchool().title().fromAlias("slot")));

            for (final Object[] row : dql.createStatement(SessionBulletinPrintDAO.this.getSession()).<Object[]>list())
            {
                final SessionDocumentSlot slot = (SessionDocumentSlot) row[0];
                final SessionMark mark = (SessionMark) row[1];
                final SessionProjectTheme theme = (SessionProjectTheme) row[2];
                final SessionSlotRatingData slotR = (SessionSlotRatingData) row[3];
                final SessionMarkRatingData markR = (SessionMarkRatingData) row[4];
                final SessionBulletinDocument bulletin = (SessionBulletinDocument) slot.getDocument();
                bulletinMap.get(bulletin).getContentMap().put(slot.getStudentWpeCAction(), new BulletinPrintRow(slot, mark, theme, slotR, markR));
            }
        }
    }

    private void initCommissionData(final Map<SessionBulletinDocument, BulletinPrintInfo> bulletinMap)
    {
        for (List<SessionBulletinDocument> elements : Iterables.partition(bulletinMap.keySet(), DQL.MAX_VALUES_ROW_NUMBER))
        {

            DQLSelectBuilder dql = new DQLSelectBuilder()
                    .fromEntity(SessionComissionPps.class, "rel")
                    .column(SessionComissionPps.pps().fromAlias("rel").s())
                    .joinPath(DQLJoinType.inner, SessionComissionPps.pps().person().identityCard().fromAlias("rel").s(), "idc")
                    .joinEntity("rel", DQLJoinType.inner, SessionBulletinDocument.class, "bulletin", eq(property(SessionBulletinDocument.commission().id().fromAlias("bulletin")), property(SessionComissionPps.commission().id().fromAlias("rel"))))
                    .column(property("bulletin"))
                    .where(in(property("bulletin"), elements))
                    .order(property("idc", IdentityCard.lastName()))
                    .order(property("idc", IdentityCard.firstName()))
                    .order(property("idc", IdentityCard.middleName()));

            for (final Object[] row : dql.createStatement(SessionBulletinPrintDAO.this.getSession()).<Object[]>list())
            {
                bulletinMap.get(row[1]).getCommission().add((PpsEntry) row[0]);
            }

            dql = new DQLSelectBuilder()
                    .fromEntity(SessionComissionPps.class, "rel")
                    .joinPath(DQLJoinType.inner, SessionComissionPps.pps().person().identityCard().fromAlias("rel").s(), "idc")
                    .joinEntity("rel", DQLJoinType.inner, SessionDocumentSlot.class, "slot", eq(property(SessionDocumentSlot.commission().id().fromAlias("slot")), property(SessionComissionPps.commission().id().fromAlias("rel"))))
                    .where(in(property(SessionDocumentSlot.document().fromAlias("slot")), elements))
                    .column(property(SessionComissionPps.pps().fromAlias("rel")))
                    .column(property(SessionDocumentSlot.document().fromAlias("slot")))
                    .column(property(SessionDocumentSlot.studentWpeCAction().fromAlias("slot")))
                    .order(property("idc", IdentityCard.lastName()))
                    .order(property("idc", IdentityCard.firstName()))
                    .order(property("idc", IdentityCard.middleName()));

            for (final Object[] row : dql.createStatement(SessionBulletinPrintDAO.this.getSession()).<Object[]>list())
            {
                bulletinMap.get(row[1]).getContentMap().get(row[2]).getCommission().add((PpsEntry) row[0]);
            }
        }
    }

    protected static class MarkAmountData
    {
        private int notAppearCount = 0, notAppearValid = 0, notAppearInvalid = 0, totalGradeAmount = 0, totalMarkAmount = 0;
        private final Map<SessionMarkGradeValueCatalogItem, Integer> markAmountMap = new HashMap<>();

        public MarkAmountData(final Map<EppStudentWpeCAction, BulletinPrintRow> contentMap)
        {
            for (final BulletinPrintRow row : contentMap.values())
            {
                SessionMark mark = row.getMark();

                if (null == mark)
                {
                    continue;
                }

                totalMarkAmount++;

                final SessionMarkCatalogItem value = mark.getValueItem();
                if (value instanceof SessionMarkGradeValueCatalogItem)
                {
                    final SessionMarkGradeValueCatalogItem gradeValue = (SessionMarkGradeValueCatalogItem) value;
                    this.markAmountMap.put(gradeValue, 1 + SafeMap.safeGet(this.markAmountMap, gradeValue, key -> 0));
                    this.totalGradeAmount++;
                } else if (value instanceof SessionMarkStateCatalogItem)
                {
                    final SessionMarkStateCatalogItem stateValue = (SessionMarkStateCatalogItem) value;
                    this.notAppearCount++;
                    if (stateValue.isValid())
                    {
                        this.notAppearValid++;
                    } else
                    {
                        this.notAppearInvalid++;
                    }
                }
            }
        }

        public int getNotAppearCount()
        {
            return notAppearCount;
        }

        public int getNotAppearValid()
        {
            return notAppearValid;
        }

        public int getNotAppearInvalid()
        {
            return notAppearInvalid;
        }

        public int getTotalGradeAmount()
        {
            return totalGradeAmount;
        }

        public int getTotalMarkAmount()
        {
            return totalMarkAmount;
        }

        public Map<SessionMarkGradeValueCatalogItem, Integer> getMarkAmountMap()
        {
            return markAmountMap;
        }
    }

    /**
     * Печатная форма и данные конкретной ведомости
     */
    protected static class BulletinPrintInfo
    {
        private RtfDocument document;
        private final SessionBulletinDocument bulletin;
        private final List<PpsEntry> commission = new ArrayList<>();
        private final Map<EppStudentWpeCAction, BulletinPrintRow> contentMap = new LinkedHashMap<>();

        private boolean useCurrentRating;
        private boolean usePoints;

        public BulletinPrintInfo(final SessionBulletinDocument bulletin)
        {
            this.bulletin = bulletin;
        }

        /**
         * @return Ведомость
         */
        public SessionBulletinDocument getBulletin()
        {
            return this.bulletin;
        }

        /**
         * @return Собственно печатная форма
         */
        public RtfDocument getDocument()
        {
            return this.document;
        }

        public void setDocument(final RtfDocument document)
        {
            this.document = document;
        }

        /**
         * @return Комиссия
         */
        public List<PpsEntry> getCommission()
        {
            return this.commission;
        }

        public Map<EppStudentWpeCAction, BulletinPrintRow> getContentMap()
        {
            return contentMap;
        }

        public boolean isUseCurrentRating()
        {
            return useCurrentRating;
        }

        public void setUseCurrentRating(boolean useCurrentRating)
        {
            this.useCurrentRating = useCurrentRating;
        }

        public boolean isUsePoints()
        {
            return usePoints;
        }

        public void setUsePoints(boolean usePoints)
        {
            this.usePoints = usePoints;
        }
    }

    protected static class BulletinPrintRow
    {
        private SessionDocumentSlot slot;
        private SessionMark mark;
        private SessionSlotRatingData slotRatingData;
        private SessionMarkRatingData markRatingData;
        private EppRealEduGroup4ActionTypeRow eduGroupRow;
        private SessionProjectTheme theme;
        private List<PpsEntry> commission = new ArrayList<>();

        public BulletinPrintRow(SessionDocumentSlot slot, SessionMark mark, SessionProjectTheme theme, SessionSlotRatingData slotR, SessionMarkRatingData markR)
        {
            this.slot = slot;
            this.mark = mark;
            this.slotRatingData = slotR;
            this.markRatingData = markR;
            this.theme = theme;
        }

        public SessionDocumentSlot getSlot()
        {
            return slot;
        }

        public SessionMark getMark()
        {
            return mark;
        }

        public SessionSlotRatingData getSlotRatingData()
        {
            return slotRatingData;
        }

        public SessionMarkRatingData getMarkRatingData()
        {
            return markRatingData;
        }

        public SessionProjectTheme getTheme()
        {
            return theme;
        }

        public EppRealEduGroup4ActionTypeRow getEduGroupRow()
        {
            return eduGroupRow;
        }

        public void setEduGroupRow(EppRealEduGroup4ActionTypeRow eduGroupRow)
        {
            this.eduGroupRow = eduGroupRow;
        }

        public List<PpsEntry> getCommission()
        {
            return commission;
        }

        public void setCommission(List<PpsEntry> commission)
        {
            this.commission = commission;
        }
    }
}
