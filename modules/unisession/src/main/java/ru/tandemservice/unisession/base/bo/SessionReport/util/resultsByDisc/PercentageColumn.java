/* $Id:$ */
package ru.tandemservice.unisession.base.bo.SessionReport.util.resultsByDisc;

import org.tandemframework.core.view.formatter.DoubleFormatter;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.ResultsByDiscAdd.ISessionReportResultsByDiscDAO;

/**
 * @author oleyba
 * @since 2/20/12
 */
public class PercentageColumn implements ISessionReportResultsByDiscDAO.ISessionReportResultsColumn
{
    public interface NumberColumn extends ISessionReportResultsByDiscDAO.ISessionReportResultsColumn
    {
        Number getCount(ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscRow row);
    }

    private NumberColumn base;
    private NumberColumn value;

    public PercentageColumn(NumberColumn base, NumberColumn value)
    {
        this.base = base;
        this.value = value;
    }

    @Override
    public String cell(ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscRow row)
    {
        Number baseCount = base.getCount(row);
        Number valueCount = value.getCount(row);
        if (null == baseCount || UniBaseUtils.eq(baseCount.doubleValue(), 0) || null == valueCount)
            return "";
        return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(100 * valueCount.doubleValue() / baseCount.doubleValue());
    }
}
