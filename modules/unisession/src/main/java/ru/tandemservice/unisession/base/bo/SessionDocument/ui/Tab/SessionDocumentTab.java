/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionDocument.ui.Tab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.unisession.base.bo.SessionDocument.ui.logic.SessionDocumentDSHandler;

/**
 * @author Alexey Lopatin
 * @since 07.05.2015
 */
@Configuration
public class SessionDocumentTab extends BusinessComponentManager
{
    public static final String DOCUMENT_DS = "documentDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(DOCUMENT_DS, documentDS()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler documentDS()
    {
        return new SessionDocumentDSHandler(getName());
    }
}