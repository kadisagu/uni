package ru.tandemservice.unisession.base.bo.SessionMark.daemon;

import org.apache.commons.lang.mutable.MutableBoolean;
import org.hibernate.Session;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.meta.entity.access.IEntityAccessSemaphore;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SingleValueCache;
import org.tandemframework.core.util.concurrent.semaphore.IThreadSemaphore;
import org.tandemframework.hibsupport.HibSupportUtils;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.hibsupport.transaction.DaoCache;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import org.tandemframework.shared.commonbase.base.util.ThreadCountProperty;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.dao.student.EppStudentSlotDAO;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.base.bo.SessionMark.event.SessionRegularMarkFiledAndCommissionChecker;
import ru.tandemservice.unisession.base.bo.SessionMark.logic.ISessionMarkDAO;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkStateCatalogItem;
import ru.tandemservice.unisession.entity.comission.SessionComission;
import ru.tandemservice.unisession.entity.document.SessionDocument;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionStudentGradeBookDocument;
import ru.tandemservice.unisession.entity.mark.*;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.count;

/**
 * @author vdanilov
 */
public class SessionMarkDaemonBean extends UniBaseDao implements ISessionMarkDaemonBean {

    private final ThreadCountProperty UPDATE_THREAD_COUNT = new ThreadCountProperty(SessionMarkDaemonBean.class.getName()+".threads", 4);

    public static final SyncDaemon DAEMON = new SyncDaemon(SessionMarkDaemonBean.class.getName(), 120, EppStudentSlotDAO.GLOBAL_DAEMON_LOCK, ISessionMarkDAO.LOCK )
    {
        @Override protected void main() {

            IEntityAccessSemaphore semaphore = CoreServices.entityAccessService().createSemaphore();
            try
            {
                semaphore.allowUpdate(SessionMark.class, true);
                semaphore.allowInsert(SessionMark.class, true);

                ISessionMarkDaemonBean dao = ISessionMarkDaemonBean.instance.get();

                try {
                    // обновляет признак положительности регулярных оценок
                    dao.doRefreshRegularMarkCacheFields();
                } catch (final Throwable t) {
                    this.logger.error(t.getMessage(), t);
                    Debug.exception(t.getMessage(), t);
                }

                try {
                    // обновляем набор слотов в зачетках (только на основе их МСРП)
                    dao.doRefreshStudentGradeBookSlots();
                } catch (final Throwable t) {
                    this.logger.error(t.getMessage(), t);
                    Debug.exception(t.getMessage(), t);
                }

                try {
                    // обновляем итоговые оценки (только устаревшие)
                    dao.doRefreshStudentFinalMarks(false);
                } catch (final Throwable t) {
                    this.logger.error(t.getMessage(), t);
                    Debug.exception(t.getMessage(), t);
                }

                try {
                    // обновляет признак положительности оценок-ссылок
                    dao.doRefreshLinkMarkCacheFields();
                } catch (final Throwable t) {
                    this.logger.error(t.getMessage(), t);
                    Debug.exception(t.getMessage(), t);
                }

            }
            finally
            {
                semaphore.release();
                if (this.logger.isInfoEnabled())
                {
                    UniDaoFacade.getCoreDao().getCalculatedValue(session -> {
                        final Number n = new DQLSelectBuilder()
								.fromEntity(SessionMark.class, "mk")
		                        .joinPath(DQLJoinType.inner, SessionMark.slot().fromAlias("mk"), "sl")
								.joinEntity("sl", DQLJoinType.inner, SessionStudentGradeBookDocument.class, "sd", eq(property("sd"), property("sl", SessionDocumentSlot.document())))
								.column(count(property("mk.id"))).createStatement(session).uniqueResult();
                        final Number m = new DQLSelectBuilder()
								.fromEntity(SessionDocumentSlot.class, "sl")
								.joinEntity("sl", DQLJoinType.inner, SessionStudentGradeBookDocument.class, "sd", eq(property("sd"), property("sl", SessionDocumentSlot.document())))
								.column(count(property("sl.id"))).createStatement(session).uniqueResult();
                        final Number k = new DQLSelectBuilder()
								.fromEntity(SessionStudentGradeBookDocument.class, "sd")
								.column(count(property("sd.id"))).createStatement(session).uniqueResult();
                        logger.info("tm/sl/gb=" + ((null == n) ? 0 : n.intValue()) + "/" + ((null == m) ? 0 : m.intValue()) + "/" + ((null == k) ? 0 : k.intValue()));
                        return n;
                    });
                }
            }
        }
    };

    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public boolean doRefreshRegularMarkCacheFields()
    {
        boolean result = false;
        IThreadSemaphore lock = SessionRegularMarkFiledAndCommissionChecker.lock();
        Debug.begin("doRefreshRegularMarkCacheFields");
        try {
            // сначала таблицу с отметками
            {
                int rows = executeAndClear(
		                new DQLUpdateBuilder(SessionSlotMarkState.class)
				                .fromEntity(SessionMarkStateCatalogItem.class, "ci")
				                .where(eq(property("ci"), property(SessionSlotMarkState.value())))
				                .set(SessionSlotMarkState.L_CACHED_MARK_VALUE, property("ci.id"))
				                .set(SessionSlotMarkState.P_CACHED_MARK_POSITIVE_STATUS, property("ci", SessionMarkStateCatalogItem.cachedPositiveStatus()))
				                .where(markCashedValuesNeedsUpdate("ci")));
                if (rows > 0) {
                    result = true;
                    Debug.message("update-rows[state]="+rows);
                }
            }

            // затем таблицу с оценками
            {
	            final DQLUpdateBuilder updateMarkQuery = new DQLUpdateBuilder(SessionSlotMarkGradeValue.class)
						.fromEntity(SessionMarkGradeValueCatalogItem.class, "ci")
						.where(eq(property("ci"), property(SessionSlotMarkGradeValue.value())))
			            .set(SessionSlotMarkGradeValue.L_CACHED_MARK_VALUE, property("ci.id"))
						.set(SessionSlotMarkGradeValue.P_CACHED_MARK_POSITIVE_STATUS, property("ci", SessionMarkGradeValueCatalogItem.cachedPositiveStatus()))
			            .where(markCashedValuesNeedsUpdate("ci"));
	            int rows = executeAndClear(updateMarkQuery);
                if (rows > 0)
                {
                    result = true;
                    Debug.message("update-rows[value]="+rows);
                }
            }
        } finally {
            lock.release();
            Debug.end();
        }
        return result;
    }

	private static IDQLExpression markCashedValuesNeedsUpdate(String markCatalogItemAlias)
	{
		return or(
				isNull(property(SessionMark.L_CACHED_MARK_VALUE)),
				isNull(property(SessionMark.P_CACHED_MARK_POSITIVE_STATUS)),
				ne(property(SessionMark.L_CACHED_MARK_VALUE), property(markCatalogItemAlias, SessionMarkCatalogItem.id())),
				ne(property(SessionMark.P_CACHED_MARK_POSITIVE_STATUS), property(markCatalogItemAlias, SessionMarkCatalogItem.cachedPositiveStatus()))
		);
	}

    @Override
    public boolean doRefreshLinkMarkCacheFields()
    {
        boolean result = false;
        IThreadSemaphore lock = SessionRegularMarkFiledAndCommissionChecker.lock();
        Debug.begin("doRefreshLinkMarkCacheFields");
        try
        {
	        final DQLUpdateBuilder updateMarkQuery = new DQLUpdateBuilder(SessionSlotLinkMark.class)
			        .fromEntity(SessionSlotRegularMark.class, "rm")
			        .where(eq(property("rm"), property(SessionSlotLinkMark.target())))
			        .set(SessionSlotLinkMark.L_CACHED_MARK_VALUE, property("rm", SessionSlotRegularMark.cachedMarkValue().id()))
			        .set(SessionSlotLinkMark.P_CACHED_MARK_POSITIVE_STATUS, property("rm", SessionSlotRegularMark.cachedMarkPositiveStatus()))
			        .where(or(
					        isNull(property(SessionSlotLinkMark.L_CACHED_MARK_VALUE)),
					        isNull(property(SessionSlotLinkMark.P_CACHED_MARK_POSITIVE_STATUS)),
					        ne(property(SessionSlotLinkMark.L_CACHED_MARK_VALUE), property("rm", SessionSlotRegularMark.cachedMarkValue().id())),
					        ne(property(SessionSlotLinkMark.P_CACHED_MARK_POSITIVE_STATUS), property("rm", SessionSlotRegularMark.cachedMarkPositiveStatus()))
			        ));
	        int rows = executeAndClear(updateMarkQuery);
            if (rows > 0)
            {
	            result = true;
                Debug.message("update-rows[link]="+rows);
            }
        }
        finally
        {
            lock.release();
            Debug.end();
        }
        return result;
    }


    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////


    @Override
    public boolean doRefreshStudentGradeBookSlots() {
        final Session session = this.getSession();
        NamedSyncInTransactionCheckLocker.register(session, ISessionMarkDAO.LOCK);

        boolean result = false;
        final Date now = new Date();

        final IEventServiceLock eventLock = CoreServices.eventService().lock();
        Debug.begin("doRefreshStudentGradeBookSlots");
        try
        {
            // удалять зачетки нельзя (в них есть итоговые оценки, пусть они там живут)
            // нужно: создать зачетки, если их нет, актуализировать у них дату закрытия, сформировать в них слоты

            Debug.begin("doRefreshStudentGradeBookSlots.create-gradebooks");
            try
            {
                // создаем недостающие зачетки
                final DQLSelectBuilder dql = new DQLSelectBuilder()
                    .fromEntity(EppStudentWpeCAction.class, "wpca")
                    .joinPath(DQLJoinType.inner, EppStudentWpeCAction.studentWpe().student().fromAlias("wpca"), "st")
                    .column(property("st.id"), "id")
                    .joinEntity("st", DQLJoinType.left, SessionStudentGradeBookDocument.class, "gb", eq(property("gb", SessionStudentGradeBookDocument.student()), property("st")))
                    .where(isNull(property("wpca", EppStudentWpeCAction.removalDate())))
                    .where(isNull(property("gb.id")));

                final Set<Long> list = new HashSet<>(dql.createStatement(session).<Long>list());
                if (list.size() > 0)
                {
                    for (final Long id: list)
                    {
                        final Student student = (Student) session.load(Student.class, id);
                        final SessionStudentGradeBookDocument gb = new SessionStudentGradeBookDocument(student, now);
                        gb.setNumber("gradebook-"+Long.toHexString(student.getId().longValue()));
                        session.save(gb);
                    }
                    session.flush();
                    result = true;
                }

            }
            finally
            {
                Debug.end();
            }

            Debug.begin("doRefreshStudentGradeBookSlots.create-slots");
            try
            {
                // создаем недостающие слоты (только создаем, всегда парами)
                final DQLSelectBuilder dql = new DQLSelectBuilder()
						.fromEntity(EppStudentWpeCAction.class, "wpca")
						.joinPath(DQLJoinType.inner, EppStudentWpeCAction.studentWpe().student().fromAlias("wpca"), "st")
						.where(isNull(property("wpca", EppStudentWpeCAction.removalDate())))

						.joinEntity("st", DQLJoinType.inner, SessionStudentGradeBookDocument.class, "gb", eq(property("st"), property("gb", SessionStudentGradeBookDocument.student())))

                // не понятно как быстрее - not exists или join + is-null
                //                .where(notExists(
                //                        new DQLSelectBuilder()
                //                        .fromEntity(SessionDocumentSlot.class, "sl_gb").addColumn(property("sl_gb.id"))
                //                        .where(eq(property(SessionDocumentSlot.inSession().fromAlias("sl_gb")), value(Boolean.TRUE) /*проверки делем только на слоты в сессию*/))
                //                        .where(eq(property(SessionDocumentSlot.studentWpeCAction().fromAlias("sl_gb")), property("wpca")))
                //                        .where(eq(property(SessionDocumentSlot.document().fromAlias("sl_gb")), property(SessionStudentGradeBookDocument.id().fromAlias("gb"))))
                //                        .buildQuery()
                //                ))
                //                .where(isNull(property(EppStudent2EduPlanVersion.removalDate().fromAlias("s2epv"))))

						.joinEntity("gb", DQLJoinType.left, SessionDocumentSlot.class, "sl_gb", and(
								eq(property("sl_gb", SessionDocumentSlot.inSession()), value(Boolean.TRUE) /*проверки делем только на слоты в сессию*/),
								eq(property("sl_gb", SessionDocumentSlot.studentWpeCAction()), property("wpca")),
								eq(property("sl_gb", SessionDocumentSlot.document()), property("gb", SessionStudentGradeBookDocument.id()))
						))
						.where(isNull(property("sl_gb.id")))

						.distinct()
						.column(property("gb", SessionStudentGradeBookDocument.id()), "gb_id")
						.column(property("wpca", EppStudentWpeCAction.id()), "wpca_id")
						.order(property("gb", SessionStudentGradeBookDocument.id()));

                final List<Object[]> list = dql.createStatement(session).<Object[]>list();
                if (list.size() > 0)
                {
                    this.createNecessarySlotPairs(list);
                    result = true;
                }
            }
            finally
            {
                Debug.end();
            }

        }
        finally
        {
            Debug.end();
            eventLock.release();
        }

        return result;
    }


    private static final Boolean[] IN_SESSION_FLAGS = new Boolean[] { Boolean.FALSE, Boolean.TRUE };

    /**
     * @param rows { [ session-document.id, eppWpCaSlot.id ] }
     */
    protected void createNecessarySlotPairs(final List<Object[]> rows)
    {
        BatchUtils.execute(rows, 256, new BatchUtils.Action<Object[]>()
        {
            final Session session = SessionMarkDaemonBean.this.getSession();
            final SingleValueCache<SessionComission> emptyComission = SessionMarkDaemonBean.this.getEmptyComissionCache(this.session);

            @Override
            public void execute(final Collection<Object[]> rows)
            {
                // локально, на !каждйю порцию!
                final Map<Long, SessionDocument> sessionDocumentCache = SessionMarkDaemonBean.this.getLoadCacheMap(SessionDocument.class);
                for (final Object[] row: rows)
                {
                    final SessionDocument document = sessionDocumentCache.get(row[0]);
                    final EppStudentWpeCAction student = (EppStudentWpeCAction) this.session.load(EppStudentWpeCAction.class, (Long)row[1]);

                    // создаем сразу пару слотов
                    for (final Boolean inSession: SessionMarkDaemonBean.IN_SESSION_FLAGS)
                    {
                        final SessionDocumentSlot slot = new SessionDocumentSlot(document, student, inSession);
                        slot.setCommission(this.emptyComission.get());
                        this.session.save(slot);
                    }
                }

                SessionMarkDaemonBean.this.infoActivity('.');
                this.session.flush();
                this.session.clear();
            }
        });
    }


    private static final String CACHE_KEY_EMPTY_COMISSION = SessionMarkDaemonBean.class.getSimpleName()+".emptyComission";

    protected SingleValueCache<SessionComission> getEmptyComissionCache(final Session session)
    {
        SingleValueCache<SessionComission> cachedValue = DaoCache.get(SessionMarkDaemonBean.CACHE_KEY_EMPTY_COMISSION);
        if (null == cachedValue)
        {
            DaoCache.put(SessionMarkDaemonBean.CACHE_KEY_EMPTY_COMISSION, cachedValue = new SingleValueCache<SessionComission>()
            {
                @Override
                protected SessionComission resolve()
                {
                    // пока создаем новую, но! только если было обращение
                    final SessionComission c = new SessionComission();
                    session.save(c);
                    return c;
                }
            });
        }
        return cachedValue;
    }


    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Более высокий приоритет соответствует меньшему значению поля priority.
    private static final Comparator<SessionSlotRegularMark> MARK_PRIORITY_COMPARATOR = Comparator.comparing(SessionSlotRegularMark::getSessionMarkValuePriority).reversed();
    protected final Comparator<SessionSlotRegularMark> TOTAL_MARK_COMPARATOR = Comparator
		    .comparing(SessionSlotRegularMark::getSessionMarkTypePriority)
		    .thenComparing(MARK_PRIORITY_COMPARATOR)
		    .thenComparing(SessionSlotRegularMark::getComparablePoints)
		    .thenComparing(SessionSlotRegularMark::SessionMarkDocumentPriority)
		    .thenComparing(SessionSlotRegularMark::getPerformDate)
		    .thenComparing(SessionSlotRegularMark::getModificationDate)
		    .thenComparing(SessionSlotRegularMark::getId);

    private static class TotalMarkHolder
    {
        private SessionSlotRegularMark current;

        public void add(final SessionSlotRegularMark element, final Comparator<SessionSlotRegularMark> comparator)
        {
            if ((null == element) || (element == this.current)) { return; }
            if (null == this.current) { this.current = element; return; }

            final int diff = comparator.compare(this.current, element);
            if (diff < 0) { this.current = element; }  // если старое - меньше, то заменяем
        }

        @Override public String toString() {
            return ("TotalMark@"+this.current);
        }
    }


    private static class StudentTotalMark
    {
        private final TotalMarkHolder totalMark[] = { new TotalMarkHolder(), new TotalMarkHolder() };
        private Date lastModificationDate = null;

        public void add(final SessionSlotRegularMark element, final Comparator<SessionSlotRegularMark> comparator)
        {
            if (null == element) { return; }

            this.totalMark[0].add(element, comparator);

            if (element.getSlot().isInSession()) {
                this.totalMark[1].add(element, comparator);
            }

            if ((null == this.lastModificationDate) || this.lastModificationDate.before(element.getModificationDate())) {
                this.lastModificationDate = element.getModificationDate();
            }
        }

        public boolean isEmpty() {
            return (null == this.totalMark[0].current);
        }

        @Override public String toString() {
            return ("TotalMarkHolder@"+Arrays.asList(this.totalMark).toString());
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public boolean doRefreshStudentFinalMarks(final boolean refreshAll)
    {
        final Session session = this.getSession();
        NamedSyncInTransactionCheckLocker.register(session, ISessionMarkDAO.LOCK);

        final MutableBoolean result = new MutableBoolean(false);
        Debug.begin("doRefreshStudentFinalMarks("+refreshAll+")");
        try
        {
            final IEventServiceLock eventLock = CoreServices.eventService().lock();
            final Date now = new Date();
            try
            {
                // берем список всех слотов и уже посчитанными итоговыми оценкамие
                final Map<Long, StudentTotalMark> globalMarkMap = this.getCalculatedTotalMark4Update(refreshAll);
                Debug.message("globalMarkMap.size="+globalMarkMap.size());

                // записываем итоговые значения в зачетки
                BatchUtils.execute(globalMarkMap.keySet(), 256, new BatchUtils.Action<Long>()
                {
                    private Map<Long, StudentTotalMark> local(final Collection<Long> ids)
                    {
                        // для начала из всей кучи оставляем только те, по которым хоть что-то есть из итоговых данных
                        // выделение локального мапа нужно по 2м причинам - быстрый поиск и фильтрация элементов
                        final Map<Long, StudentTotalMark> result = new HashMap<>(ids.size());
                        for (final Long id: ids)
                        {
                            final StudentTotalMark value = globalMarkMap.get(id);
                            if (!value.isEmpty())
	                            result.put(id, value);
                        }
                        return result;
                    }

                    @Override
                    public void execute(final Collection<Long> ids)
                    {
                        final Map<Long, StudentTotalMark> localMarkMap = this.local(ids);
                        if (localMarkMap.isEmpty())
	                        return;

                        // обновляем ВСЕ слоты зачеток, которые прицеплены к студенту в данный момент
                        // новые слоты при этом не создаются (формирование слотов происходит раньше)
                        // в результате этой операции не будет создано ни одной реальной оценки
                        // однако, если реальные оценки там есть - они будут заменены на ссылки, если это возможно

                        final Map<Long, Date> wakeUpModificationDateIds = new HashMap<>();

                        final DQLSelectBuilder dql = new DQLSelectBuilder()
								.fromEntity(SessionStudentGradeBookDocument.class, "gb")
								.joinEntity("gb", DQLJoinType.inner, SessionDocumentSlot.class, "sl", eq(property("sl", SessionDocumentSlot.document()), property("gb")))
								.joinEntity("sl", DQLJoinType.left, SessionMark.class, "xxm", eq(property("sl"), property("xxm", SessionMark.slot())))
								.joinEntity("xxm", DQLJoinType.left, SessionSlotLinkMark.class, "xxl", eq(property("xxm"), property("xxl")))
								.where(in(property("sl", SessionDocumentSlot.studentWpeCAction()), localMarkMap.keySet()))

								/* 0 */ .column(property("gb.id"))
								/* 1 */ .column(property("sl", SessionDocumentSlot.studentWpeCAction().id()))
								/* 2 */ .column(property("sl", SessionDocumentSlot.inSession()))
								/* 3 */ .column(property("sl", SessionDocumentSlot.id()))
								/* 4 */ .column(property("xxm", SessionMark.id()))
								/* 5 */ .column(property("xxl", SessionSlotLinkMark.id()))
								/* 6 */ .column(property("xxl", SessionSlotLinkMark.target().id()));

                        for (final Object[] row: scrollRows(dql.createStatement(session)))
                        {
                            final EppStudentWpeCAction wpcaSlot = (EppStudentWpeCAction)session.load(EppStudentWpeCAction.class, (Long)row[1]);
                            final StudentTotalMark totalMarkState = localMarkMap.get(wpcaSlot.getId());
                            final SessionSlotRegularMark calculatedTotalMark = totalMarkState.totalMark[(Boolean.TRUE.equals(row[2]) ? 1 : 0)].current;

                            if (null == calculatedTotalMark)
                            {
                                // итоговой оценки на текущйи момент нет - осталось добиться чтобы в базе ее тоже не было
                                final Long dbMarkId = (Long)row[4];
                                if (null != dbMarkId) { this.delete(dbMarkId); }

                                continue;
                            }

                            // у нас есть вычисленная итоговая оценка
                            {
                                final Long targetId = (Long)row[6];
                                final Long dbMarkId = (Long)row[4];
                                if (null != dbMarkId)
                                {
                                    if ((calculatedTotalMark.getId().equals(dbMarkId) /* прямо эта оценка (какой-то урод добавил оценку прямо в зачетку) */ ) ||
										(calculatedTotalMark.getId().equals(targetId) /* уже ссылка на нужную оценку*/))
                                    {
                                        // здесь объект доставать нет смысла - нужно просто обновить дату изменения (актуализации)
                                        wakeUpModificationDateIds.put(dbMarkId, totalMarkState.lastModificationDate);
                                        continue;
                                    }

                                    final Long linkId = (Long)row[5];
                                    if (null != linkId)
                                    {
                                        // если это ссылка - то перебрасываем ее на новую оценку
                                        final SessionSlotLinkMark link = (SessionSlotLinkMark)session.get(SessionSlotLinkMark.class, linkId);
                                        this.saveLink(link, calculatedTotalMark, totalMarkState.lastModificationDate);
                                        continue;
                                    }

                                    // если есть оценка (при этом это уже точно не ссылка) - удаляем ее, мы будем сохранять ссылку
                                    this.delete(dbMarkId);
                                    session.flush(); // удаляем прямо сейчас
                                }
                            }

                            final SessionDocumentSlot slot = (SessionDocumentSlot)session.get(SessionDocumentSlot.class, (Long)row[3]);
                            slot.setCommission(calculatedTotalMark.getCommission());
                            slot.setTryNumber(null);
                            session.saveOrUpdate(slot);

                            // сохраняем новую ссылку
                            this.saveLink(new SessionSlotLinkMark(slot, calculatedTotalMark), calculatedTotalMark, totalMarkState.lastModificationDate);
                        }

                        {
                            // обновляем дату изменения, если есть более свежие оценки
                            if (wakeUpModificationDateIds.size() > 0)
                            {
                                new DQLUpdateBuilder(SessionMark.class)
										.set(SessionMark.modificationDate().s(), valueTimestamp(this.max(Collections.max(wakeUpModificationDateIds.values()))))
										.where(DQLExpressions.in(property(SessionMark.id().s()), wakeUpModificationDateIds.keySet()))
										.createStatement(session).execute();
                            }
                        }

                        {
                            // TODO: подумать, что делать, если у студента есть только оценка вне сессии (в сессию нет ниодной оценки)
                            // TODO: надо как-то помеяать таких студентов (чтобы демон их не трогал)
                            // TODO: XXX: UPD: почему возникли эти вопросы?

                        }

                        SessionMarkDaemonBean.this.infoActivity('.');
                        session.flush();
                        session.clear();
                        result.setValue(true);
                    }

                    // !totalMark is not a proxy! //
                    private void saveLink(final SessionSlotLinkMark link, final SessionSlotRegularMark totalMark, final Date lastModificationDate)
                    {
                        link.setTarget(totalMark);
                        link.setCommission(totalMark.getCommission());
                        link.setComment(totalMark.getComment());
                        link.setCachedMarkValue(totalMark.getCachedMarkValue());
                        link.setCachedMarkPositiveStatus(totalMark.getCachedMarkPositiveStatus());
                        link.setPerformDate(totalMark.getPerformDate());
                        link.setModificationDate(this.max(lastModificationDate));
                        session.saveOrUpdate(link);
                    }

                    private void delete(final Long id)
                    {
                        if (null != id)
	                        session.delete(HibSupportUtils.getEntityById(session, id));
                    }

                    private Date max(final Date d)
                    {
                        return now.before(d) ? d : now;
                    }
                });

            } catch (final Throwable t) {
                throw CoreExceptionUtils.getRuntimeException(t);
            } finally {
                eventLock.release();
            }
        } finally {
            Debug.end();
        }
        return result.booleanValue();
    }




    /**
     * вычисляет список eppWpCaSlot по которым необходимо обновить итоговые оценки
     * заполняет значение итоговых оценок в структуре StudentTotalMark
     * @return { eppWpCaSlot.id -> StudentTotalMark (filled) }
     **/
    protected Map<Long, StudentTotalMark> getCalculatedTotalMark4Update(final boolean refreshAll)
    {
        Debug.begin("getCalculatedTotalMark4Update("+refreshAll+")");
        try {
            // сначала ищем все слоты студентов по ФК для которых есть еще не обработанные оценки
            final Map<Long, StudentTotalMark> globalMarkMap = this.getMarkMap4Update(refreshAll);
            if (globalMarkMap.isEmpty()) { return globalMarkMap; }

            // для этих элементов пересчитываем итоговую оценку исходя из всех оценок по слоту
            // делаем в несколько потоков - иначе юзатеся один проц, при этом вся система курит
            final SyncDaemon.ExecutorService executor = DAEMON.getExecutorService(false, this.UPDATE_THREAD_COUNT.get());
            try {
                BatchUtils.execute(globalMarkMap.keySet(), DQL.MAX_VALUES_ROW_NUMBER, idsPart -> {
                    // перед стартом нового потока
                    // обязательно делать локальную копию ids
                    // (т.к. сюда передается один и тот же instance коллекции, но с разными значениями)
                    final Map<Long, StudentTotalMark> localCache = new HashMap<>(idsPart.size());
                    for (final Long id: idsPart) { localCache.put(id, globalMarkMap.get(id)); }

                    // теперь, запускаем поток
                    executor.submit(() -> {

                        // нужно обязательно вызывать UniBaseDao.instance2.get().getCalculatedValue иначе будут лики сессии
                        // ибо оно в новом потоке живет, и, как следствие, будет открывать сессии в обход interceptor-ов

                        IUniBaseDao.instance.get().getCalculatedValue(session -> {
                            // проблема в том, что считать итоговые оценки на sql очень трудоемко
                            // поэтому будем выгружать все, что есть в java, и там уже сортировать
                            // (для этого можно поюзать несколько потоков)

                            // TODO: здесь, наверное, надо юзать stateless-session
                            // TODO: здесь, наверное, надо сделать fetch

                            final DQLSelectBuilder dql = new DQLSelectBuilder()
									.fromEntity(SessionSlotRegularMark.class, "mk")
									.joinPath(DQLJoinType.inner, SessionMark.slot().fromAlias("mk"), "sl")
									.where(in(property("sl", SessionDocumentSlot.studentWpeCAction().id()), localCache.keySet()))
									.column(property("sl", SessionDocumentSlot.studentWpeCAction().id()))
									.column(property("mk"));

                            final Comparator<SessionSlotRegularMark> comparator = SessionMarkDaemonBean.this.TOTAL_MARK_COMPARATOR; // чтение из локальной переменной существенно быстрее, чем из поля класса
                            for (final Object[] row: scrollRows(dql.createStatement(session)))
                            {
	                            // Помним, что в localCache вообще-то те же StudentTotalMark, что и в globalMarkMap, т.е. мы опосредованно меняем объекты в результирующей мапе через такую, блин, левую ссылку.
	                            StudentTotalMark totalMark = localCache.get(row[0]);
	                            totalMark.add((SessionSlotRegularMark) row[1], comparator);
                            }

                            return localCache.size();
                        });
                    });
                });
            } finally {
                executor.close();
            }

            return globalMarkMap;
        } catch (final Exception t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        } finally {
            Debug.end();
        }
    }


    /**
     * вычисляет список eppWpCaSlot по которым необходимо обновить итоговые оценки
     * @return { eppWpCaSlot.id -> StudentTotalMark (empty) }
     **/
    protected Map<Long, StudentTotalMark> getMarkMap4Update(boolean refreshAll)
    {
        final Map<Long, StudentTotalMark> globalMarkMap = new HashMap<>();

        if (refreshAll)
        {
            // ВСЕ активные МСРП-по-ФК, для которых есть слоты в зачетке (без доп. условий)
            // считаем, что к этому моменту у нас УЖЕ есть все слоты в зачетке
            // и обновляем оценки ТОЛЬКО ПО НИМ

            final DQLSelectBuilder dql = new DQLSelectBuilder()
					.fromEntity(SessionDocumentSlot.class, "sl_t")
					.joinEntity("sl_t", DQLJoinType.inner, SessionStudentGradeBookDocument.class, "sd_t", eq(property("sd_t"), property("sl_t", SessionDocumentSlot.document())))
					.joinPath(DQLJoinType.inner, SessionDocumentSlot.studentWpeCAction().fromAlias("sl_t"), "wpca")
					.where(isNull(property("wpca", EppStudentWpeCAction.removalDate())))
					.distinct()
					.column(property("wpca.id"));

            for (final Long id: UniBaseDao.<Long>scroll(dql.createStatement(this.getSession()))) {
                if (null != globalMarkMap.put(id, new StudentTotalMark())) {
                    throw new IllegalStateException();
                }
            }

        }
        else
        {
            // ВСЕ активные МСРП-по-ФК, для которых есть стлоты в зачетке без оценок или с устаревшей (дата модификации) оценкой
            // считаем, что к этому моменту у нас УЖЕ есть все слоты в зачетке
            // и обновляем оценки ТОЛЬКО ПО НИМ

            // TODO: здесь может быть несколько запросов на id

            final DQLSelectBuilder dql = new DQLSelectBuilder()
					.fromEntity(SessionSlotRegularMark.class, "mk_i")
					.joinPath(DQLJoinType.inner, SessionMark.slot().fromAlias("mk_i"), "sl_i")

					.joinEntity("sl_i", DQLJoinType.inner, SessionDocumentSlot.class, "sl_t",
					        eq(property("sl_t", SessionDocumentSlot.studentWpeCAction()), property("sl_i", SessionDocumentSlot.studentWpeCAction())))
					.joinEntity("sl_t", DQLJoinType.inner, SessionStudentGradeBookDocument.class, "sd_t", eq(property("sd_t"), property("sl_t", SessionDocumentSlot.document())))
					.where(or(
					        eq(property("sl_i", SessionDocumentSlot.inSession()), value(Boolean.TRUE)), // либо промежуточная оценка получена в сессию (тогда она влияет на все итоговые)
					        eq(property("sl_i", SessionDocumentSlot.inSession()), property("sl_t", SessionDocumentSlot.inSession())) // либо они имею один признак "в сессию"
					))

					.joinEntity("sl_t", DQLJoinType.left, SessionMark.class, "mk_t", eq(property("sl_t"), property("mk_t", SessionMark.slot())))
					.where(or(
					        isNull(property("mk_t", SessionMark.id())), /* нет итоговой оценки (отдельно проверяем в сессию и вне сессии) */
					        lt(property("mk_t", SessionMark.modificationDate()), property("mk_i", SessionMark.modificationDate())) /* итоговая оценка старее, чем текущая */
					))

					.joinPath(DQLJoinType.inner, SessionDocumentSlot.studentWpeCAction().fromAlias("sl_i" /*? sl_t ?*/), "wpca")
					.where(isNull(property("wpca", EppStudentWpeCAction.removalDate())))
					.distinct()
					.column(property("wpca.id"));

            for (final Long id: UniBaseDao.<Long>scroll(dql.createStatement(this.getSession()))) {
                if (null != globalMarkMap.put(id, new StudentTotalMark())) {
                    throw new IllegalStateException();
                }
            }
        }

        return globalMarkMap;
    }


}
