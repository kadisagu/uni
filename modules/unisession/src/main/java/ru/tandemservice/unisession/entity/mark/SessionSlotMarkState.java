package ru.tandemservice.unisession.entity.mark;

import ru.tandemservice.unisession.entity.catalog.SessionMarkStateCatalogItem;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.mark.gen.SessionSlotMarkStateGen;

/**
 * Оценка студента в сессии (отметка)
 */
public class SessionSlotMarkState extends SessionSlotMarkStateGen
{

    public SessionSlotMarkState() {}
    public SessionSlotMarkState(final SessionDocumentSlot slot, final SessionMarkStateCatalogItem value) {
        this.setSlot(slot);
        this.setValue(value);
    }


    @Override public SessionMarkStateCatalogItem getValueItem() {
        return this.getValue();
    }

    @Override public int getSessionMarkTypePriority() {
        return Short.MIN_VALUE;
    }

    @Override public int getSessionMarkValuePriority() {
        return this.getValue().getPriority();
    }

    @Override public long getComparablePoints() {
        return 0L;
    }

    @Override public Double getPoints() {
        return null;
    }
}