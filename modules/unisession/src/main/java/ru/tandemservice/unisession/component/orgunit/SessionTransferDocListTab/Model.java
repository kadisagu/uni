/* $Id$ */
// Copyright 2006-2012 Tandem Service Software
package ru.tandemservice.unisession.component.orgunit.SessionTransferDocListTab;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.unisession.entity.document.SessionTransferDocument;

/**
 * @author Vasily Zhukov
 * @since 10.01.2012
 */
@Input({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "orgUnitHolder.id")
})
public class Model
{
    private EntityHolder<OrgUnit> orgUnitHolder = new EntityHolder<OrgUnit>();
    private CommonPostfixPermissionModel secModel;
    private DynamicListDataSource<SessionTransferDocument> dataSource;
    private String settingsKey;
    private IDataSettings settings;
    private ISelectModel transferTypeModel;

    public IEntity getSecuredObject()
    {
        return this.getOrgUnit();
    }

    public Long getOrgUnitId()
    {
        return this.getOrgUnitHolder().getId();
    }

    public OrgUnit getOrgUnit()
    {
        return this.getOrgUnitHolder().getValue();
    }

    // Getters & Setters

    public EntityHolder<OrgUnit> getOrgUnitHolder()
    {
        return orgUnitHolder;
    }

    public void setOrgUnitHolder(EntityHolder<OrgUnit> orgUnitHolder)
    {
        this.orgUnitHolder = orgUnitHolder;
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        this.secModel = secModel;
    }

    public DynamicListDataSource<SessionTransferDocument> getDataSource()
    {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<SessionTransferDocument> dataSource)
    {
        this.dataSource = dataSource;
    }

    public String getSettingsKey()
    {
        return settingsKey;
    }

    public void setSettingsKey(String settingsKey)
    {
        this.settingsKey = settingsKey;
    }

    public IDataSettings getSettings()
    {
        return settings;
    }

    public void setSettings(IDataSettings settings)
    {
        this.settings = settings;
    }

    public ISelectModel getTransferTypeModel()
    {
        return transferTypeModel;
    }

    public void setTransferTypeModel(ISelectModel transferTypeModel)
    {
        this.transferTypeModel = transferTypeModel;
    }
}
