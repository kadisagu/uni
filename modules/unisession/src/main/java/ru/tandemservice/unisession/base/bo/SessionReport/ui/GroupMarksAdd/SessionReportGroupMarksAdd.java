/* $Id: SessionReportGroupMarksAdd.java 21825 2012-02-09 09:08:57Z oleyba $ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.GroupMarksAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.base.bo.SessionReport.util.UniSessionFilterAddon;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 12/29/11
 */
@Configuration
public class SessionReportGroupMarksAdd extends BusinessComponentManager
{
	public static final String DISCIPLINE_DS = "disciplineDS";

	public static final String FILTER_SESSION_ADDON = "sessionAddon";
	public static final String FILTER_ORG_UNIT = "orgUnit";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
	{
        return this.presenterExtPointBuilder()
				.addDataSource(CommonBaseStaticSelectDataSource.selectDS(DISCIPLINE_DS, getName(), disciplineDSHandler()))
				.addAddon(uiAddon(CommonFilterAddon.class.getSimpleName(), UniSessionFilterAddon.class))
				.create();
    }

	public EntityComboDataSourceHandler disciplineDSHandler()
	{
		return EppRegistryElementPart.defaultSelectDSHandler(getName())
				.customize((alias, dql, context, filter) ->
				{
					UniSessionFilterAddon sessionAddon = context.getNotNull(FILTER_SESSION_ADDON);
					OrgUnit formOrgUnit = context.getNotNull(FILTER_ORG_UNIT);

					final String wpeCaAlias = "wpeCa";
					DQLSelectBuilder wpeCaDql = new DQLSelectBuilder().fromEntity(EppStudentWpeCAction.class, wpeCaAlias)
							// Если в селектах утили не выбрана Академ.группа, то фильтры из SessionReportGroupMarksAddUI.configUtilWhere не применяются, поэтому нужно указать условия вручную:
							// фильтровать по формирующему подразделению, по активности студента и по актуальности МСРП.
							.where(eq(property(wpeCaAlias, EppStudentWpeCAction.studentWpe().student().educationOrgUnit().formativeOrgUnit()), value(formOrgUnit)))
							.where(eq(property(wpeCaAlias, EppStudentWpeCAction.studentWpe().student().status().active()), value(Boolean.TRUE)))
							.where(isNull(property(wpeCaAlias, EppStudentWpeCAction.removalDate())))

							.where(eq(property(wpeCaAlias, EppStudentWpeCAction.studentWpe().registryElementPart()), property(alias)));
					sessionAddon.applyFilters(wpeCaDql, wpeCaAlias);

					return dql.where(exists(wpeCaDql.buildQuery()));
				});
	}
}