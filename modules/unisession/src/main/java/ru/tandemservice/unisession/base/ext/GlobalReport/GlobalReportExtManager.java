/* $Id$ */
package ru.tandemservice.unisession.base.ext.GlobalReport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.GlobalReport.GlobalReportManager;
import org.tandemframework.shared.commonbase.base.bo.GlobalReport.logic.GlobalReportDefinition;
import ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.TotalResultList.AttestationReportTotalResultList;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.FinalQualWorkResultList.SessionReportFinalQualWorkResultList;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.FqwSummaryResultList.SessionReportFqwSummaryResultList;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.ResultsByDiscList.SessionReportResultsByDiscList;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.ResultsList.SessionReportResultsList;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.SfeSummaryResultList.SessionReportSfeSummaryResultList;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.StateFinalExamResultList.SessionReportStateFinalExamResultList;

/**
 * @author azhebko
 * @since 28.03.2014
 */
@Configuration
public class GlobalReportExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private GlobalReportManager _globalReportManager;

    @Bean
    public ItemListExtension<GlobalReportDefinition> reportListExtension()
    {
        return itemListExtension(_globalReportManager.reportListExtPoint())
                .add("sessionResultsReport", new GlobalReportDefinition("session", "sessionResults", SessionReportResultsList.class.getSimpleName()))
                .add("sessionResultsByDiscReport", new GlobalReportDefinition("session", "sessionResultsByDisc", SessionReportResultsByDiscList.class.getSimpleName()))
                .add("attestationTotalResultReport", new GlobalReportDefinition("session", "attestationTotalResult", AttestationReportTotalResultList.class.getSimpleName()))
                .add("stateFinalExamResultReport", new GlobalReportDefinition("student", "stateFinalExamResult", SessionReportStateFinalExamResultList.class.getSimpleName()))
                .add("finalQualWorkResultReport", new GlobalReportDefinition("student", "finalQualWorkResult", SessionReportFinalQualWorkResultList.class.getSimpleName()))
                .add("stateFinalExamSummaryResultReport", new GlobalReportDefinition("student", "stateFinalExamSummaryResult", SessionReportSfeSummaryResultList.class.getSimpleName()))
                .add("finalQualWorkSummaryResultReport", new GlobalReportDefinition("student", "finalQualWorkSummaryResult", SessionReportFqwSummaryResultList.class.getSimpleName()))
                .create();
    }
}