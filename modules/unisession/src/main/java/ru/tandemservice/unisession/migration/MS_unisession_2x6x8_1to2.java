package ru.tandemservice.unisession.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unisession_2x6x8_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
        {
            new ScriptDependency("org.tandemframework", "1.6.15"),
            new ScriptDependency("org.tandemframework.shared", "1.6.8")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность sessionAttestationBulletin
        tool.renameColumn("session_att_bull_t", "activityelementpart_id", "registryelementpart_id");

        ////////////////////////////////////////////////////////////////////////////////
        // сущность sessionRetakeDocument
        tool.renameColumn("session_doc_retake_t", "activityelementpart_id", "registryelementpart_id");
    }
}