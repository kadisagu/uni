/* $Id$ */
package ru.tandemservice.unisession.component.orgunit.AllowanceTab;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uni.entity.employee.Student;
import org.tandemframework.shared.commonbase.utils.PublisherColumnBuilder;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unisession.entity.allowance.ISessionStudentNotAllowed;
import ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowed;

/**
 * @author oleyba
 * @since 4/19/11
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);

        model.setSettings(UniBaseUtils.getDataSettings(component, model.getSettingsKey()));

        this.prepareSessionListDataSource(component);
    }

    private void prepareSessionListDataSource(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        final DynamicListDataSource<ISessionStudentNotAllowed> dataSource = UniBaseUtils.createDataSource(component, this.getDao());

        dataSource.addColumn(new SimpleColumn("№ зач. книжки", SessionStudentNotAllowed.student().bookNumber().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new PublisherColumnBuilder("ФИО", Student.FIO_KEY, "studentTab").tabBind(ru.tandemservice.uni.component.student.StudentPub.Model.SELECTED_TAB_BIND_KEY).entity(SessionStudentNotAllowed.student().s()).build().setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Вид затрат", SessionStudentNotAllowed.student().compensationType().shortTitle().s()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new BlockColumn("bulletinColumn", "Ведомость", "bulletinColumnBlock").setOrderable(false).setClickable(true));
        dataSource.addColumn(new SimpleColumn("Курс\n (текущий)", SessionStudentNotAllowed.student().course().title().s()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Группа\n (текущая)", SessionStudentNotAllowed.student().group().title().s()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new ActionColumn("Аннулировать недопуск", "unlock_session", "onClickUnlock", "Аннулировать недопуск студента (студентки) «{0}»?", SessionStudentNotAllowed.student().person().fullFio().s()).setDisabledProperty("notActive").setPermissionKey(model.getSec().getPermission("unlockSessionNotAllowedStudent")));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete", "Удалить запись о недопуске студента (студентки) «{0}»?", SessionStudentNotAllowed.student().person().fullFio().s()).setPermissionKey(model.getSec().getPermission("deleteSessionNotAllowedStudent")));

        model.setDataSource(dataSource);
        this.getDao().prepareListDataSource(model);
    }

    public void onClickSearch(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        DataSettingsFacade.saveSettings(model.getSettings());
        this.getDao().prepare(model);
        model.getDataSource().showFirstPage();
        model.getDataSource().refresh();
    }

    public void onClickClear(final IBusinessComponent context)
    {
        this.getModel(context).getSettings().clear();
        this.onClickSearch(context);
    }

    public void onClickUnlock(final IBusinessComponent component)
    {
        this.getDao().doUnlock(component.getListenerParameter());
    }

    public void onClickAddNotAllowed(final IBusinessComponent component)
    {
        final IEntity sessionObject = getDao().getSessionObject(getModel(component));
        if (null == sessionObject) {
            throw new ApplicationException("Для добавления записи о недопуске выберите учебный год и часть.");
        }
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                ru.tandemservice.unisession.component.allowance.SessionAllowanceAdd.Model.COMPONENT_NAME,
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, sessionObject.getId())));
    }

    public void onSearchParamsChange(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        DataSettingsFacade.saveSettings(model.getSettings());
        this.getDao().prepare(model);
    }

    public void onClickDelete(final IBusinessComponent component)
    {
        this.getDao().deleteRow(component);
    }
}