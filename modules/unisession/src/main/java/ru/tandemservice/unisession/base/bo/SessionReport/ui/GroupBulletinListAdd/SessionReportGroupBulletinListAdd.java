/* $Id: SessionReportGroupBulletinListAdd.java 21825 2012-02-09 09:08:57Z oleyba $ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.GroupBulletinListAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import ru.tandemservice.uni.catalog.bo.StudentCatalogs.StudentCatalogsManager;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.base.bo.SessionReport.util.UniSessionFilterAddon;

/**
 * @author oleyba
 * @since 12/29/11
 */
@Configuration
public class SessionReportGroupBulletinListAdd extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
            .addAddon(uiAddon(CommonFilterAddon.class.getSimpleName(), UniSessionFilterAddon.class))
            .addDataSource(SessionReportManager.instance().eduYearDSConfig())
            .addDataSource(SessionReportManager.instance().yearPartDSConfig())
//            .addDataSource(selectDS("courseDS", StudentCatalogsManager.instance().courseDSHandler()))
//            .addDataSource(selectDS("resultDS", SessionReportManager.instance().resultsDSHandler()))
            .create();
    }
}
