/* $Id: $ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.StateFinalAttestationResultAdd.logic;

import ru.tandemservice.unisession.base.bo.SessionReport.logic.AbsReportInfo;
import ru.tandemservice.unisession.base.bo.SessionReport.logic.SfaRaw;
import ru.tandemservice.unisession.entity.report.StateFinalAttestationResult;

import java.util.Collection;

/**
 * @author Andrey Andreev
 * @since 16.12.2016
 */
public class SfaReportInfo<R1 extends SfaRaw> extends AbsReportInfo<StateFinalAttestationResult>
{
    public SfaReportInfo(StateFinalAttestationResult stateFinalAttestationResult)
    {
        super(stateFinalAttestationResult);
    }

    protected Collection<R1> raws;

    public Collection<R1> getRaws()
    {
        return raws;
    }

    public void setRaws(Collection<R1> raws)
    {
        this.raws = raws;
    }
}
