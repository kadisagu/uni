/* $Id: $ */
package ru.tandemservice.unisession.base.bo.SessionReport.logic;

import jxl.format.CellFormat;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableSheet;
import jxl.write.WriteException;
import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Andrey Andreev
 * @since 08.12.2016
 */
public class StateFinalAttestationUtil
{

    /**
     * Заполняет область пустыми значениями с указанным стилем
     * Для рисования рамок и всего такого.
     */
    public static void fillArea(WritableSheet sheet, int colStart, int colFinish, int rowStart, int rowFinish, CellFormat format)
    {
        try
        {
            for (int column = colStart; column <= colFinish; column++)
                for (int row = rowStart; row <= rowFinish; row++)
                    sheet.addCell(new Label(column, row, "", format));
        }
        catch (WriteException e)
        {
            e.printStackTrace();
        }
    }


    /**
     * Добавляет текст в указанную область клеток с указанным стилем
     */
    public static void addTextCell(WritableSheet sheet, int left, int top, int width, int height, String text, CellFormat format)
    {
        try
        {
            if (width > 0 || height > 0)
                sheet.mergeCells(left, top, left + width - 1, top + height - 1);
            sheet.addCell(new Label(left, top, text, format));
        }
        catch (WriteException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Добавляет в ячейку значение если оно не равно нулю, "" если значение равно 0
     */
    public static void addIntCell(WritableSheet sheet, int col, int row, int value, CellFormat format)
    {
        try
        {
            if (value == 0)
                sheet.addCell(new Label(col, row, "", format));
            else sheet.addCell(new Number(col, row, value, format));
        }
        catch (WriteException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Добавляет в ячейку значение если оно не равно нулю, "-" если значение равно 0 и добавляет пустую ячейку если значения нет.
     */
    public static void addDoubleCell(WritableSheet sheet, int col, int row, Double value, CellFormat format)
    {
        try
        {
            if (value == null)
                sheet.addCell(new Label(col, row, "", format));
            else if (value == 0)
                sheet.addCell(new Label(col, row, "-", format));
            else sheet.addCell(new Number(col, row, value, format));
        }
        catch (WriteException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Оставляет для пары Студент-Мероприятие только данные с последними по дате получения оценками
     *
     * @param rawStream данные
     * @param <T>       extends SfaRaw
     */
    public static <T extends SfaRaw> Collection<T> filterRaws(Stream<T> rawStream)
    {
        return rawStream
                .collect(Collectors.groupingBy(
                        raw -> new CoreCollectionUtils.Pair<>(raw.getRegistryElement(), raw.getStudent()),

                        Collectors.mapping(raw -> raw, Collectors.reducing(null, (raw1, raw2) ->
                        {
                            if (raw1 == null) return raw2;
                            if (raw2 == null) return raw1;
                            SessionMark mark1 = raw1.getMark();
                            SessionMark mark2 = raw2.getMark();
                            if (mark1 == null) return raw2;
                            if (mark2 == null) return raw1;
                            if (mark1.getPerformDate().compareTo(mark2.getPerformDate()) > 0)
                                return raw1;
                            else return raw2;
                        }))
                ))
                .values();
    }

    public static String getDatesString(Collection<Date> dates)
    {
        if (CollectionUtils.isEmpty(dates)) return "";

        List<Date> collect = dates.stream().distinct().sorted().map(CoreDateUtils::getDayFirstTimeMoment).collect(Collectors.toList());
        if (collect.size() < 3)
            return collect.stream().map(DateFormatter.DEFAULT_DATE_FORMATTER::format).collect(Collectors.joining(", "));


        StringBuilder result = new StringBuilder();
        String value = DateFormatter.DEFAULT_DATE_FORMATTER.format(collect.get(0));
        result.append(value);

        int i;
        String dash = " - ";
        for (i = 1; collect.size() > i + 1; i++)
        {
            Date prevDate = collect.get(i - 1);
            Date currDate = collect.get(i);
            Date nextDate = collect.get(i + 1);

            boolean prevDiff = CoreDateUtils.getDateDifferenceInDays(currDate, prevDate) <= 1;
            boolean nextDiff = CoreDateUtils.getDateDifferenceInDays(nextDate, currDate) <= 1;

            if (prevDiff && nextDiff)
            {
                if (!value.equals(dash)) result.append(dash);
                value = dash;
                continue;
            }

            if (prevDiff && value.equals(dash))
            {
                value = DateFormatter.DEFAULT_DATE_FORMATTER.format(currDate);
                result.append(value);
                continue;
            }

            value = DateFormatter.DEFAULT_DATE_FORMATTER.format(currDate);
            result.append(", ").append(value);
        }

        if (!value.equals(dash))
            result.append(", ");

        result.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(collect.get(i)));

        return result.toString();
    }
}
