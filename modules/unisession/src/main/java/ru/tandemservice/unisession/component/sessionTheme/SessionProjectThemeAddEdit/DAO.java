/* $Id$ */
package ru.tandemservice.unisession.component.sessionTheme.SessionProjectThemeAddEdit;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unisession.dao.document.ISessionDocumentBaseDAO;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionProjectTheme;

/**
 * @author oleyba
 * @since 6/24/11
 */
public class DAO extends UniBaseDao implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setSlot(this.getNotNull(SessionDocumentSlot.class, model.getSlot().getId()));
        if (!model.isInitialized())
        {
            final SessionProjectTheme theme = this.get(SessionProjectTheme.class, SessionProjectTheme.slot().s(), model.getSlot());
            if (null != theme)
            {
                model.setTheme(theme.getTheme());
                model.setComment(theme.getComment());
            }
            model.setInitialized(true);
        }
    }

    @Override
    public void update(final Model model)
    {
        ISessionDocumentBaseDAO.instance.get().updateProjectTheme(model.getSlot(), model.getTheme(), model.getComment());
    }

}
