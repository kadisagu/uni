package ru.tandemservice.unisession.entity.document;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.unisession.entity.document.gen.SessionStudentGradeBookDocumentGen;
import ru.tandemservice.unisession.sec.ISessionSecDao;

/**
 * Зачетка студента
 *
 * Зачетка студента (живет в рамках его связи с УП(в) и актуальна тогда, когда актуальна связь)
 * хранят итоговые оценки студента как ссылки на оценки, полученные им по другим документам
 */
public class SessionStudentGradeBookDocument extends SessionStudentGradeBookDocumentGen
{

    public SessionStudentGradeBookDocument() {}
    public SessionStudentGradeBookDocument(final Student student, final Date formingDate) {
        this.setStudent(student);
        this.setFormingDate(formingDate);
    }

    @Override
    @EntityDSLSupport(parts = "number")
    public String getTitle() { return this.getNumber(); }

    @Override
    @EntityDSLSupport(parts=SessionDocument.P_NUMBER)
    public String getTypeTitle() {
        return "Электронная зачетная книжка";
    }

    @Override
    @EntityDSLSupport(parts=SessionDocument.P_NUMBER)
    public String getTypeShortTitle()
    {
        return "Зач. книжка";
    }

    @Override
    public Collection<IEntity> getSecLocalEntities() {
        return ISessionSecDao.instance.get().getSecLocalEntities(this);
    }

    @Override
    public OrgUnit getGroupOu()
    {
        return getStudent().getEducationOrgUnit().getGroupOrgUnit();
    }
}