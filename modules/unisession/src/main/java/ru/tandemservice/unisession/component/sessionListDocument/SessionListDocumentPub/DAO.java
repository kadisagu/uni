package ru.tandemservice.unisession.component.sessionListDocument.SessionListDocumentPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionListDocument;
import ru.tandemservice.unisession.entity.document.SessionProjectTheme;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author iolshvang
 * @since 13.04.2011
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        SessionListDocument card = getNotNull(SessionListDocument.class, model.getCard().getId());
        model.setCard(card);
        model.setTitle("№" + card.getNumber());
    }

    private SessionMark getMark(SessionDocumentSlot slot)
    {
        DQLSelectBuilder dqlMark = new DQLSelectBuilder().fromEntity(SessionMark.class, "mark")
                .where(eq(property(SessionMark.slot().fromAlias("mark")), value(slot)))
                .column("mark");
        return ((SessionMark) dqlMark.createStatement(this.getSession()).uniqueResult());
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        DynamicListDataSource<SessionDocumentSlot> dataSource = model.getDataSource();
		EntityOrder order = dataSource.getEntityOrder();
		Comparator<SessionDocumentSlot> slotComparator = getSlotComparator(order.getColumnName(), (order.getDirection() == OrderDirection.desc));

		List<SessionDocumentSlot> slots = getList(SessionDocumentSlot.class, SessionDocumentSlot.document(), model.getCard());
		Collections.sort(slots, slotComparator);

        dataSource.setTotalSize(slots.size());
        dataSource.setCountRow(slots.size());

        if (!slots.isEmpty())
		{
            SessionDocumentSlot slot = slots.get(0);
            model.setTermAndYearTitle(slot.getTermTitle());
        }

        dataSource.createPage(slots);
        //List<SessionComissionPps> sessionComissionPpsList = getList(SessionComissionPps.class, SessionComissionPps.commission(), slot.getCommission());
        List<Long> slotIds = UniBaseUtils.getIdList(model.getDataSource().getEntityList());
        Map<Long, List<String>> tutorMap = new HashMap<>();
        DQLSelectBuilder tutorDQL = new DQLSelectBuilder()
                .fromEntity(SessionComissionPps.class, "rel")
                .column(property(SessionComissionPps.pps().fromAlias("rel")))
                .joinPath(DQLJoinType.inner, SessionComissionPps.commission().fromAlias("rel"), "comm")
                .joinEntity("comm", DQLJoinType.inner, SessionDocumentSlot.class, "slot", eq(property("comm.id"), property(SessionDocumentSlot.commission().id().fromAlias("slot"))))
                .column(property(SessionDocumentSlot.id().fromAlias("slot")))
                .where(in(property("slot.id"), slotIds))
                .order(property(SessionComissionPps.pps().person().identityCard().fullFio().fromAlias("rel")));
        for (Object[] row : tutorDQL.createStatement(this.getSession()).<Object[]>list())
        {
            SafeMap.safeGet(tutorMap, (Long) row[1], ArrayList.class).add(((PpsEntry) row[0]).getPerson().getFio());
        }

		Map<SessionDocumentSlot, String> slot2theme = getList(SessionProjectTheme.class, SessionProjectTheme.slot(), slots).stream()
				.collect(Collectors.toMap(SessionProjectTheme::getSlot, theme -> (theme == null) ? null : theme.getTheme()));

        for (ViewWrapper<SessionDocumentSlot> wrapper : ViewWrapper.<SessionDocumentSlot>getPatchedList(model.getDataSource()))
        {
            Long id = wrapper.getEntity().getId();
            SessionDocumentSlot slot = get(id);
            List<String> tutors = tutorMap.get(id);
            SessionMark mark = getMark(slot);
            wrapper.setViewProperty("markColumn", mark == null ? "" : mark.getValueTitle());
            wrapper.setViewProperty("inSessionColumn", slot.isInSession() ? "Да" : "Нет");
            wrapper.setViewProperty("ratingColumn", mark == null ? null : mark.getPoints());
            wrapper.setViewProperty("ppsColumn", tutors);
			wrapper.setViewProperty(PROP_PROJECT_THEME, slot2theme.get(slot));
            wrapper.setViewProperty("markCommentColumn", mark == null ? "" : mark.getComment());
        }
    }

	private static Comparator<SessionDocumentSlot> getSlotComparator(String columnName, boolean reverseOrder)
	{
		switch (columnName)
		{
			case "studentWpeCAction.registryElementTitle":
				Comparator<SessionDocumentSlot> comparingRegElem = Comparator.comparing(slot -> slot.getStudentWpeCAction().getRegistryElementTitle());
				return (reverseOrder ? comparingRegElem.reversed() : comparingRegElem).thenComparing(slot -> slot.getStudentWpeCAction().getStudentWpe().getTerm().getIntValue());
			case SessionDocumentSlot.P_TERM_TITLE:
				Comparator<SessionDocumentSlot> comparingTerm = Comparator.comparing(slot -> slot.getStudentWpeCAction().getStudentWpe().getTerm().getIntValue());
				return (reverseOrder ? comparingTerm.reversed() : comparingTerm).thenComparing(slot -> slot.getStudentWpeCAction().getRegistryElementTitle());
		}
		return Comparator.comparing(SessionDocumentSlot::getId);
	}

	@Override
    public void deleteSlot(IBusinessComponent context, Model model)
    {
        deleteRow(context);
    }
}
