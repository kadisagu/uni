/* $*/

package ru.tandemservice.unisession.print;

import org.tandemframework.rtf.document.RtfDocument;

import org.tandemframework.core.util.cache.SpringBeanCache;

/**
 * @author iolshvang
 * @since 25.04.2011
 */
public interface ISessionListDocumentPrintDAO
{
    SpringBeanCache<ISessionListDocumentPrintDAO> instance = new SpringBeanCache<ISessionListDocumentPrintDAO>(ISessionListDocumentPrintDAO.class.getName());

    /**
     * печать экз. карточки
     * @param id id карточки
     * @return rtf
     */
    RtfDocument printSessionListDocument(Long id);
}
