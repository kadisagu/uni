package ru.tandemservice.unisession.entity.document;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.shared.commonbase.base.util.INumberGenerationRule;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unisession.dao.sessionListDocument.ISessionListDocumentDAO;
import ru.tandemservice.unisession.entity.document.gen.SessionListDocumentGen;
import ru.tandemservice.unisession.sec.ISessionSecDao;

import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;

/**
 * Экзаменационная карточка
 */
public class SessionListDocument extends SessionListDocumentGen
{
    @Override
    @EntityDSLSupport(parts=SessionDocument.P_NUMBER)
    public String getTypeTitle()
    {
        return "Экзаменационная карточка №" + this.getNumber();
    }

    @Override
    @EntityDSLSupport(parts=SessionDocument.P_NUMBER)
    public String getTypeShortTitle()
    {
        return "Экз. карт. №" + this.getNumber();
    }

    @Override
    public INumberGenerationRule<SessionListDocument> getNumberGenerationRule()
    {
        return ISessionListDocumentDAO.instance.get().getNumberGenerationRule();
    }

    @Override
    public Collection<IEntity> getSecLocalEntities()
    {
        return ISessionSecDao.instance.get().getSecLocalEntities(this);
    }

    public String getNumberPrefix()
    {
        final Calendar formingDate = Calendar.getInstance();
        formingDate.setTime(getFormingDate());
        final int year = formingDate.get(Calendar.YEAR);

        return (year % 100) + ".";
    }

    @Override
    public OrgUnit getGroupOu()
    {
        return getOrgUnit();
    }
}