/**
 *$Id:$
 */
package ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.ResultAdd.util;

import com.google.common.collect.ComparisonChain;
import org.apache.commons.lang.mutable.MutableInt;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.MergeType;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.cell.TextDirection;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfElementFactory;
import org.tandemframework.rtf.util.IRtfRowSplitInterceptor;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlot;
import ru.tandemservice.unisession.attestation.entity.report.SessionAttestationResultReport;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.entity.catalog.UnisessionCommonTemplate;
import ru.tandemservice.unisession.entity.catalog.codes.UnisessionCommonTemplateCodes;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 14.11.12
 */
public class AttestationReportResultDao extends UniBaseDao implements IAttestationReportResultDao
{
    @Override
    public <M extends Model> RtfDocument createReportRtfDocument(M model)
    {
        prepareDataFromDB(model);

        UnisessionCommonTemplate template = getCatalogItem(UnisessionCommonTemplate.class, UnisessionCommonTemplateCodes.ATTESTATION_RESULT_REPORT);

        RtfDocument document = new RtfReader().read(template.getContent());

        document = writeReport(document, model);

        return document;
    }

    @Override
    public <M extends Model> SessionAttestationResultReport saveReport(M model, RtfDocument document)
    {
        SessionAttestationResultReport report = new SessionAttestationResultReport();

        DatabaseFile content = new DatabaseFile();
        content.setContent(RtfUtil.toByteArray(document));
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        content.setFilename("sessionAttestationResultReport_" + new DateFormatter("dd_mm_yyyy").format(new Date()) + ".rtf");

        save(content);

        report.setContent(content);
        report.setFormingDate(new Date());
        report.setCourse(model.getCourse() != null ? model.getCourse().getTitle() : "");
        report.setGroup(model.getGroupList() != null ? UniStringUtils.join(model.getGroupList(), Group.title().s(), ", ") : "");
        report.setAttestation(model.getAttestation());

        save(report);

        return report;
    }

    @Override
    public <M extends Model> ErrorCollector validate(M model)
    {
        ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();

        if (model.getCourse() == null && (model.getGroupList() == null || model.getGroupList().isEmpty()))
            errorCollector.add("Для построения отчета выберите курс или перечень групп.", "course", "group");

        return errorCollector;
    }

    /**
     * Поднимает данные из базы и формирует по ним список врапперов строк отчета.
     */
    protected <M extends Model> void prepareDataFromDB(final M model)
    {
        final List<Student> studentList = new ArrayList<>();
//        final List<SessionAttestationBulletin> bulletinList = new ArrayList<SessionAttestationBulletin>();
        final List<SessionAttestationSlot> attestationSlotList = new ArrayList<>();
        final List<EppStudentWorkPlanElement> epvSlotList = new ArrayList<>();
        final Map<Long, AttestationReportResultRowWrapper> wrapperMap = new HashMap<>();
        final Map<Group, Set<EppRegistryElementPart>> groupToDiscipline = new HashMap<>();

        DQLSelectBuilder groupBuilder = new DQLSelectBuilder().fromEntity(Group.class, "g").column(property(Group.id().fromAlias("g")));
        if (model.getCourse() != null)
            groupBuilder.where(eq(property(Group.course().fromAlias("g")), value(model.getCourse())));
        if (model.getGroupList() != null && !model.getGroupList().isEmpty())
            groupBuilder.where(in(property(Group.id().fromAlias("g")), ids(model.getGroupList())));

        groupBuilder.where(exists(
                new DQLSelectBuilder().fromEntity(Student.class, "s").column(property(Student.id().fromAlias("s")))
                        .where(eq(property(Student.educationOrgUnit().groupOrgUnit().fromAlias("s")), value(model.getOrgUnit())))
                        .where(eq(property(Student.group().id().fromAlias("s")), property(Group.id().fromAlias("g"))))
                        .buildQuery()));

        BatchUtils.execute(groupBuilder.createStatement(getSession()).list(), 128, elements -> studentList.addAll(
                new DQLSelectBuilder().fromEntity(Student.class, "s").column(property("s"))
                        .where(in(property(Student.group().id().fromAlias("s")), elements))
                        .createStatement(getSession()).<Student>list()));

//        DQLSelectBuilder bullBuilder = new DQLSelectBuilder().fromEntity(SessionAttestationBulletin.class, "bull").addColumn(property("bull"))
//                .where(eq(property(SessionAttestationBulletin.attestation().fromAlias("bull")), value(model.getAttestation(), PropertyType.ANY)));
//        bulletinList.addAll(bullBuilder.createStatement(getSession()).<SessionAttestationBulletin>list());

        BatchUtils.execute(studentList, DQL.MAX_VALUES_ROW_NUMBER, elements -> attestationSlotList.addAll(
                new DQLSelectBuilder().fromEntity(SessionAttestationSlot.class, "aSlot").column(property("aSlot"))
                        .where(eq(property(SessionAttestationSlot.bulletin().attestation().fromAlias("aSlot")), value(model.getAttestation())))
                        .where(in(property(SessionAttestationSlot.studentWpe().student().fromAlias("aSlot")), elements))
                        .createStatement(getSession()).<SessionAttestationSlot>list()
        ));

		Set<Student> studentsWithSlots = attestationSlotList.stream().map(slot -> slot.getStudentWpe().getStudent()).collect(Collectors.toSet());

        Comparator<EppRegistryElementPart> eppRegistryElementPartComparator = (o1, o2) -> ComparisonChain.start()
                .compare(o1.getRegistryElement().getTitle(), o2.getRegistryElement().getTitle())
                .compare(o1.getRegistryElement().getNumber(), o2.getRegistryElement().getNumber())
                .compare(o1.getNumber(), o2.getNumber())
                .compare(o1.getId(), o2.getId())
                .result();

        Set<EppRegistryElementPart> disciplineList = attestationSlotList.stream()
                .map(slot -> slot.getBulletin().getRegistryElementPart())
                .collect(Collectors.toCollection(() -> new TreeSet<>(eppRegistryElementPartComparator)
                ));

        for(SessionAttestationSlot slot :attestationSlotList)
        {
            Group group = slot.getStudentWpe().getStudent().getGroup();
            Set<EppRegistryElementPart> set = groupToDiscipline.get(group);
            if (set == null)
            {
                set = new TreeSet<>(eppRegistryElementPartComparator);
                groupToDiscipline.put(group, set);
            }
            set.add(slot.getBulletin().getRegistryElementPart());
        }

        BatchUtils.execute(studentsWithSlots, DQL.MAX_VALUES_ROW_NUMBER, elements -> epvSlotList.addAll(
				new DQLSelectBuilder().fromEntity(EppStudentWorkPlanElement.class, "sSlot").column(property("sSlot"))
						.where(in(property(EppStudentWorkPlanElement.student().fromAlias("sSlot")), elements))
						.where(in(property(EppStudentWorkPlanElement.registryElementPart().fromAlias("sSlot")), disciplineList))
						.where(isNull(property(EppStudentWorkPlanElement.removalDate().fromAlias("sSlot"))))
						.createStatement(getSession()).<EppStudentWorkPlanElement>list()));

		for (Student student : studentsWithSlots)
        {
            wrapperMap.put(student.getId(), new AttestationReportResultRowWrapper(student));
        }

        for (SessionAttestationSlot slot : attestationSlotList)
        {
            AttestationReportResultRowWrapper wrapper = wrapperMap.get(slot.getStudentWpe().getStudent().getId());
            wrapper.getDisciplineAttSlotMap().put(slot.getBulletin().getRegistryElementPart().getId(), slot);
        }

        for (EppStudentWorkPlanElement slot : epvSlotList)
        {
            AttestationReportResultRowWrapper wrapper = wrapperMap.get(slot.getStudent().getId());
            wrapper.getDisciplineEpvSlotMap().put(slot.getRegistryElementPart().getId(), slot);
        }

        model.setDisciplineSet(disciplineList);
        model.setGroupToDisciplineMap(groupToDiscipline);
        model.setRowWrapperList((new ArrayList<>(wrapperMap.values())));
    }

    /**
     * Печатает отчет.
     */
    protected <M extends Model> RtfDocument writeReport(RtfDocument document, final M model)
    {
        final Map<Group, List<AttestationReportResultRowWrapper>> groupWrapperMap = prepareGroupWrapperMap(model);

        writeHeader(document, model);

        if (groupWrapperMap.isEmpty())
        {
            return writeEmptyDocument(document, model);
        }

        return writeDocument(document, groupWrapperMap, model);
    }

    /**
     * Заголовок отчета.
     */
    protected <M extends Model> RtfDocument writeHeader(RtfDocument document, M model)
    {
        RtfInjectModifier injectModifier = new RtfInjectModifier()
            .put("academyTitle", TopOrgUnit.getInstance().getNominativeCaseTitle() != null ? TopOrgUnit.getInstance().getNominativeCaseTitle() : TopOrgUnit.getInstance().getFullTitle())
            .put("formativeOrgUnits", model.getOrgUnit().getNominativeCaseTitle() != null ? model.getOrgUnit().getNominativeCaseTitle() : model.getOrgUnit().getFullTitle())
            .put("executor", UserContext.getInstance().getPrincipalContext().getFio())
            .put("formingDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()));

        SessionReportManager.addOuLeaderData(injectModifier, model.getOrgUnit(), "ouleader", "FIOouleader");

        injectModifier.modify(document);

        return document;
    }

    /**
     * Печатает таблицу с общими данными по группе.
     * @param group текущая группа
     * @param wrapperList список студентов в текущей группе
     */
    protected <M extends Model> RtfDocument writeHeaderGroupTable(RtfDocument document, Group group, List<AttestationReportResultRowWrapper> wrapperList, M model)
    {
        //считаем количество НПП в группе, их будет больше одного, если студенты в таблице имеют разные НПП
        Set<EducationLevelsHighSchool> eduLevels = new HashSet<>();
        for (AttestationReportResultRowWrapper wrapper : wrapperList)
            eduLevels.add(wrapper.getStudent().getEducationOrgUnit().getEducationLevelHighSchool());
        List<EducationLevelsHighSchool> eduLevelList = new ArrayList<>(eduLevels);
        Collections.sort(eduLevelList, new EntityComparator<>(new EntityOrder(EducationLevelsHighSchool.P_DISPLAYABLE_TITLE, OrderDirection.asc)));

        new RtfInjectModifier()
                .put("specialityList", UniStringUtils.join(eduLevelList, EducationLevelsHighSchool.displayableTitle().s(), ", "))
                .put("eduYear", model.getCurrentYear().getTitle())
                .put("group", group.getTitle())
                .put("term", model.getAttestation().getSessionObject().getYearDistributionPart().getTitle())
                .put("data", "")
                .modify(document);

        return document;
    }

    /**
     * Печатает основную таблицу с данными по аттестациям в группе.
     * @param group текущая группа
     * @param wrapperList список студентов в текущей группе
     */
    protected <M extends Model> RtfDocument writeGroupTable(RtfDocument document, Group group, List<AttestationReportResultRowWrapper> wrapperList, final M model)
    {
        final Set<EppRegistryElementPart> disciplineSet =  model.getGroupToDisciplineMap().get(group);
        // группируем строки таблицы(студентов) по их НПП
        final Map<EducationLevelsHighSchool, List<AttestationReportResultRowWrapper>> eduLevelWrapperMap = new TreeMap<>(
                CommonCollator.comparing(EducationLevelsHighSchool::getDisplayableTitle, true)
        );
        for (AttestationReportResultRowWrapper wrapper : wrapperList)
        {
            List<AttestationReportResultRowWrapper> list = eduLevelWrapperMap.get(wrapper.getStudent().getEducationOrgUnit().getEducationLevelHighSchool());
            if (list == null)
                eduLevelWrapperMap.put(wrapper.getStudent().getEducationOrgUnit().getEducationLevelHighSchool(), list = new ArrayList<>());
            list.add(wrapper);
        }

        Comparator<AttestationReportResultRowWrapper> wrapperComparator = CommonCollator.comparing(AttestationReportResultRowWrapper::getFio, true);

        // сортируем студентов по ФИО
        for (List<AttestationReportResultRowWrapper> list : eduLevelWrapperMap.values())
            Collections.sort(list, wrapperComparator);

        int rowIndex = 0;// индекс строки, нужен для того что бы сохранить индексы строк с НПП, что бы знать какие строки мерджить
        final List<Integer> mergeRowIndexList = new ArrayList<>();// список индексов строк, которые надо померджить
        final int columnNumber = 4 + (disciplineSet.isEmpty() ? 1 : disciplineSet.size());
        List<String[]> lineList = new ArrayList<>();
        MutableInt number = new MutableInt(1);// сквозная нумерация студентов внутри группы
        for (Map.Entry<EducationLevelsHighSchool, List<AttestationReportResultRowWrapper>> rowEntry : eduLevelWrapperMap.entrySet())
        {
            // если НПП в группе больше одного, то студенты группируются по своим НПП
            if (eduLevelWrapperMap.size() > 1)
            {
                // выводим строку с названием НПП
                String[] line = new String[columnNumber];
                line[0] = rowEntry.getKey().getDisplayableTitle();
                lineList.add(line);
                mergeRowIndexList.add(rowIndex++);
            }

            for (AttestationReportResultRowWrapper wrapper : rowEntry.getValue())
            {
                String[] line = prepareGroupTableLine(group, wrapper, wrapperList, number, columnNumber, model);
                lineList.add(line);
                rowIndex++;
            }
        }

        RtfRowIntercepterBase rowIntercepter = getRtfRowIntercepter(group, wrapperList, mergeRowIndexList, columnNumber, model);

        new RtfTableModifier()
                .put("T", lineList.toArray(new String[lineList.size()][columnNumber]))
                .put("T", rowIntercepter)
                .modify(document);

        return document;
    }

    /**
     * Заменяет метки пустыми значениями.
     */
    protected <M extends Model> RtfDocument writeEmptyDocument(RtfDocument document, M model)
    {
        new RtfInjectModifier()
                .put("specialityList", "")
                .put("eduYear", model.getCurrentYear().getTitle())
                .put("group", "")
                .put("term", model.getAttestation().getSessionObject().getYearDistributionPart().getTitle())
                .put("data", "")
                .modify(document);

        new RtfTableModifier()
                .put("T", new String[][]{})
                .modify(document);

        return document;
    }

    /**
     * Подготавливает строку для таблицы с данными по аттестации.
     * @param group текущая группа
     * @param wrapper текущий студет
     * @param wrapperList список студентов текущей группы
     * @param number сквозной номре студента
     * @param columnNumber количество колонок с таблице
     */
    protected <M extends Model> String[] prepareGroupTableLine(Group group, AttestationReportResultRowWrapper wrapper, List<AttestationReportResultRowWrapper> wrapperList, MutableInt number, int columnNumber, final M model)
    {
        final Set<EppRegistryElementPart> disciplineSet =  model.getGroupToDisciplineMap().get(group);
        String[] line = new String[columnNumber];
        int i = 0;
        line[i++] = number.toString();
        line[i++] = getFio(wrapper) + (wrapper.getStudent().isArchival() || !wrapper.getStudent().getStatus().isActive() ? "_i_" : ""); // потом в интерцепторе ячейки с _i_ отфарматируются в italic

        if (disciplineSet.isEmpty())
            line[i++] = "";

        for (EppRegistryElementPart discipline : disciplineSet)
        {
            // сокращенное название оценки
            // "-" - если у студента нет актуального МСРП на дисциплину по любому виду ауд. нагрузки, и он не включен ни в одну атт. ведомость
            // пусто во всех остальных случаях
            String result = "-";
            SessionAttestationSlot slot = wrapper.getDisciplineAttSlotMap().get(discipline.getId());
            if (slot != null && slot.getMark() != null)
                result = slot.getMark().getShortTitle();
            else if (wrapper.getDisciplineEpvSlotMap().get(discipline.getId()) != null)
                result = "";
            line[i++] = result;
        }
        line[i++] = wrapper.getTotalPassed().toString();
        line[i] = wrapper.getTotalUnpassed().toString();

        number.increment();

        return line;
    }

    /**
     * Подготавливает интерцептор для основной таблицы с данными по аттестации.
     * @param group текущая группа
     * @param wrapperList список студентов текущей группы
     * @param mergeRowIndexList список индексов строк, колонки которых нужно померджит горизонтально
     * @param columnNumber число колонок таблицы
     */
    protected <M extends Model> RtfRowIntercepterBase getRtfRowIntercepter(Group group, List<AttestationReportResultRowWrapper> wrapperList, final List<Integer> mergeRowIndexList, final int columnNumber, final M model)
    {
        return new RtfRowIntercepterBase()
        {
            @Override
            public void beforeModify(final RtfTable table, int currentRowIndex)
            {
                // тут посплиттим колонку с дисциплинами на число дисциплин и пропишем им названия
                final Set<EppRegistryElementPart> disciplineSet =  model.getGroupToDisciplineMap().get(group);

                if (disciplineSet.isEmpty())
                    return;

                // число колонок дисциплин равно кол-ву дисциплин
                int[] parts = new int[disciplineSet.size()];
                Arrays.fill(parts, 1);
                for (final RtfRow row : table.getRowList())
                {
                    final Iterator<EppRegistryElementPart> iterator = disciplineSet.iterator();

                    // интерцептор для именования колонок с дисциплинами
                    IRtfRowSplitInterceptor headerInterceptor = (newCell, index) -> {
                        if (table.getRowList().indexOf(row) > 0)
                            return;

                        EppRegistryElementPart next = iterator.next();
                        IRtfElement text = RtfBean.getElementFactory().createRtfText(model.isDisciplineTitleWithLabor() ? next.getTitleWithLabor() : next.getTitle());
                        newCell.getElementList().add(text);
						newCell.setTextDirection(TextDirection.LR_BT);
                    };

                    // разбиваем колонку с дисциплинами
                    RtfUtil.splitRow(row, 2, headerInterceptor, parts);
                }
            }

            @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                // там где это нужно делаем italic
                if (value != null && value.contains("_i_"))
                {
                    String s = value.replaceAll("_i_", "");
                    RtfString string = new RtfString();
                    string.append(IRtfData.I).append(s).append(IRtfData.I, 0);
                    return string.toList();
                }
                return null;
            }

            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                // тут померджим строки с названиями НПП, если они есть
                int col1 = 0;
                int col2 = columnNumber - 1;
                for (Integer rowIndex : mergeRowIndexList)
                {
                    int index = rowIndex + startIndex;
                    List<RtfCell> cells = newRowList.get(index).getCellList();
                    cells.get(col1).setMergeType(MergeType.HORIZONTAL_MERGED_FIRST);
                    for (int i = col1 + 1; i <= col2; i++)
                        cells.get(i).setMergeType(MergeType.HORIZONTAL_MERGED_NEXT);
                }
            }
        };
    }

    /**
     * Вставляет разрыв страницы в документ.
     * @param elementList список элементов в документе
     */
    protected void insertPageBreak(List<IRtfElement> elementList, int index)
    {
        IRtfElementFactory elementFactory = RtfBean.getElementFactory();

        elementList.add(index++, elementFactory.createRtfControl(IRtfData.PARD));
//        elementList.add(index++, elementFactory.createRtfControl(IRtfData.PAR));

        elementList.add(index++, elementFactory.createRtfControl(IRtfData.PAGE));

        elementList.add(index++, elementFactory.createRtfControl(IRtfData.PARD));
//        elementList.add(index++, elementFactory.createRtfControl(IRtfData.PAR));
    }

    /**
     * Вставляет пропуск.
     * @param elementList список элементов в документе
     */
    protected void insertBreak(List<IRtfElement> elementList, int index)
    {
        IRtfElementFactory elementFactory = RtfBean.getElementFactory();

        elementList.add(index++, elementFactory.createRtfControl(IRtfData.PARD));
        elementList.add(index++, elementFactory.createRtfControl(IRtfData.PAR));
//        elementList.add(index++, elementFactory.createRtfControl(IRtfData.PARD));
//        elementList.add(index++, elementFactory.createRtfControl(IRtfData.PAR));
    }

    protected String getFio(AttestationReportResultRowWrapper wrapper)
    {
        return wrapper.getFio();
    }

    protected <M extends Model> Map<Group, List<AttestationReportResultRowWrapper>> prepareGroupWrapperMap(M model)
    {
        final Map<Group, List<AttestationReportResultRowWrapper>> groupWrapperMap = new TreeMap<>(CommonCollator.TITLED_WITH_ID_COMPARATOR);
        for (AttestationReportResultRowWrapper wrapper : model.getRowWrapperList())
        {
            List<AttestationReportResultRowWrapper> list = groupWrapperMap.get(wrapper.getGroup());
            if (list == null)
                groupWrapperMap.put(wrapper.getGroup(), list = new ArrayList<>());
            list.add(wrapper);
        }
        return groupWrapperMap;
    }

    protected <M extends Model> RtfDocument writeDocument(RtfDocument document, Map<Group, List<AttestationReportResultRowWrapper>> groupWrapperMap, M model)
    {
        // ищем таблицу в документе
        RtfSearchResult firstTableSearchResult = UniRtfUtil.findRtfTableMark(document, "data");//таблица с общими данными по группе
        RtfSearchResult secondTableSearchResult = UniRtfUtil.findRtfTableMark(document, "T");//таблица с данными по аттестации
        // сохраняем пустой "шаблон" таблиц
        RtfTable firstEmptyTable = (RtfTable) firstTableSearchResult.getElementList().get(firstTableSearchResult.getIndex()).getClone();
        RtfTable secondEmptyTable = (RtfTable) secondTableSearchResult.getElementList().get(secondTableSearchResult.getIndex()).getClone();

        List<IRtfElement> elementList = document.getElementList();

        int index = secondTableSearchResult.getIndex() + 1;//индекс вставки пустых таблиц, нужен для того что бы вставлять таблицы до строки с подписью руководителя

        Iterator<Map.Entry<Group, List<AttestationReportResultRowWrapper>>> iterator = groupWrapperMap.entrySet().iterator();
        for (Map.Entry<Group, List<AttestationReportResultRowWrapper>> entry; iterator.hasNext();)
        {
            entry = iterator.next();

            writeHeaderGroupTable(document, entry.getKey(), entry.getValue(), model);

            writeGroupTable(document, entry.getKey(), entry.getValue(), model);

            // если это не последняя группа
            if (iterator.hasNext())
            {
                // то новая страница и в ней новая таблица
                insertPageBreak(elementList, index);
                index = index + 3;
                elementList.add(index, firstEmptyTable.getClone());
                insertBreak(elementList, index);
                index = index + 3;
                elementList.add(index, secondEmptyTable.getClone());
                insertBreak(elementList, index);
                index = index + 3;
            }

        }

        return document;
    }

}
