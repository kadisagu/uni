/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReexamination.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.caf.ui.config.datasource.column.BlockDSColumn;
import org.tandemframework.caf.ui.config.datasource.column.IHeaderColumnBuilder;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.common.INaturalId;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.commonbase.dao.SharedBaseDao;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvBlockWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionTypeCodes;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;
import ru.tandemservice.unisession.base.bo.SessionReexamination.SessionReexaminationManager;
import ru.tandemservice.unisession.base.bo.SessionTransfer.SessionTransferManager;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.mark.SessionALRequest;
import ru.tandemservice.unisession.entity.mark.SessionALRequestRow;
import ru.tandemservice.unisession.entity.mark.SessionALRequestRowMark;

import java.util.*;

/**
 * @author Alexey Lopatin
 * @since 31.08.2015
 */
public class SessionReexaminationDao extends SharedBaseDao implements ISessionReexaminationDao
{
    @Override
    public List<SessionALRequestRow> mergeALRequestRows(Collection<SessionALRequestRow> currentRows, Collection<SessionALRequestRow> oldRows, boolean save)
    {
        List<SessionALRequestRow> requestRows = Lists.newArrayList();

        new MergeAction.SessionMergeAction<PairKey<EppEpvRowTerm, Boolean>, SessionALRequestRow>()
        {
            @Override
            protected PairKey<EppEpvRowTerm, Boolean> key(final SessionALRequestRow source)
            {
                return new PairKey<>(source.getRowTerm(), source.isNeedRetake());
            }

            @Override
            protected SessionALRequestRow buildRow(final SessionALRequestRow source)
            {
                return source;
            }

            @Override
            protected void fill(final SessionALRequestRow target, final SessionALRequestRow source)
            {
                if (save)
                    target.update(source);
                else
                {
                    target.setRequest(source.getRequest());
                    target.setRowTerm(source.getRowTerm());
                    target.setRegElementPart(source.getRegElementPart());
                    target.setNeedRetake(source.isNeedRetake());
                }
            }

            @Override
            protected void doSaveRecord(SessionALRequestRow databaseRecord)
            {
                if (save)
                    super.doSaveRecord(databaseRecord);
                requestRows.add(databaseRecord);
            }

            @Override
            protected void doDeleteRecord(SessionALRequestRow databaseRecord)
            {
                if (save)
                    super.doDeleteRecord(databaseRecord);
            }
        }.merge(oldRows, currentRows);

        sortALRequestRows(requestRows);
        return requestRows;
    }

    @Override
    public void saveALRequestRowMarks(Collection<SessionALRequestRow> requestRows, Map<PairKey<SessionALRequestRow, EppFControlActionType>, SessionMarkGradeValueCatalogItem> requestRowMarkMap, Map<EppRegistryElementPart, List<EppFControlActionType>> part2fcaMap)
    {
        Map<PairKey<SessionALRequestRow, EppFControlActionType>, SessionALRequestRowMark> currentMap = Maps.newHashMap();

        for (SessionALRequestRow requestRow : requestRows)
        {
            List<EppFControlActionType> fcaList = part2fcaMap.getOrDefault(requestRow.getRegElementPart(), Lists.newArrayList());
            for (EppFControlActionType fca : fcaList)
            {
                SessionMarkGradeValueCatalogItem markValue = null;
                for (Map.Entry<PairKey<SessionALRequestRow, EppFControlActionType>, SessionMarkGradeValueCatalogItem> entry : requestRowMarkMap.entrySet())
                {
                    PairKey<SessionALRequestRow, EppFControlActionType> keys = entry.getKey();
                    if (!requestRow.isNeedRetake() && requestRow.equals(keys.getFirst()) && fca.equals(keys.getSecond()))
                    {
                        markValue = entry.getValue();
                    }
                }

                PairKey<SessionALRequestRow, EppFControlActionType> keys = PairKey.create(requestRow, fca);
                SessionALRequestRowMark mark = new SessionALRequestRowMark(requestRow, fca);
                if (requestRow.isNeedRetake())
                {
                    currentMap.put(keys, mark);
                }
                else if (null != markValue)
                {
                    mark.setMark(markValue);
                    currentMap.put(keys, mark);
                }
            }
        }

        new MergeAction.SessionMergeAction<INaturalId, SessionALRequestRowMark>() {
            @Override protected INaturalId key(final SessionALRequestRowMark source) { return source.getNaturalId(); }
            @Override protected SessionALRequestRowMark buildRow(final SessionALRequestRowMark source) { return new SessionALRequestRowMark(source.getRequestRow(), source.getControlAction()); }
            @Override protected void fill(final SessionALRequestRowMark target, final SessionALRequestRowMark source) { target.update(source, false); }
        }.merge(
                getList(SessionALRequestRowMark.class, SessionALRequestRowMark.requestRow(), requestRows), currentMap.values()
        );
    }

    @Override
    public void sortALRequestRows(List<SessionALRequestRow> requestRows)
    {
        Collections.sort(requestRows, (o1, o2) -> {
            EppEpvRowTerm rt1 = o1.getRowTerm();
            EppEpvRowTerm rt2 = o2.getRowTerm();

            int result = rt1.getRow().getStoredIndex().compareTo(rt2.getRow().getStoredIndex());
            if (result == 0)
                result = Integer.compare(rt1.getTerm().getIntValue(), rt2.getTerm().getIntValue());
            return result;
        });
    }

    @Override
    public Map<EppRegistryElementPart, List<EppFControlActionType>> getPart2FCATypeMap(List<SessionALRequestRow> requestRows)
    {
        Map<EppRegistryElementPart, List<EppFControlActionType>> part2fcaMap = Maps.newHashMap();
        if (requestRows.isEmpty()) return part2fcaMap;

        for (SessionALRequestRowMark rowMark : getList(SessionALRequestRowMark.class, SessionALRequestRowMark.requestRow(), requestRows))
            SafeMap.safeGet(part2fcaMap, rowMark.getRequestRow().getRegElementPart(), ArrayList.class).add(rowMark.getControlAction());

        EppEduPlanVersionBlock block = requestRows.get(0).getRowTerm().getRow().getOwner();
        IEppEpvBlockWrapper blockWrapper = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockData(block.getId(), true);
        Map<Long, IEppEpvRowWrapper> rowMap = blockWrapper.getRowMap();

        Map<EppRegistryElementPart, Long> part2RowMap = Maps.newHashMap();
        for (SessionALRequestRow requestRow : requestRows)
            part2RowMap.put(requestRow.getRegElementPart(), requestRow.getRowTerm().getRow().getId());

        List<EppRegistryElementPartFControlAction> regElement2fcaList = getList(EppRegistryElementPartFControlAction.class, EppRegistryElementPartFControlAction.part(), part2RowMap.keySet());
        Map<Long, Map<Integer, List<String>>> row2fcaTypeMap = Maps.newHashMap();

        // получаем формы итогового контроля для выбранных строк семестра УП
        rowMap.keySet().stream().filter(rowId -> part2RowMap.values().contains(rowId)).forEach(rowId -> {
            IEppEpvRowWrapper rowWrapper = rowMap.get(rowId);
            for (Integer term : rowWrapper.getActiveTermSet())
            {
                for (String code : EppFControlActionTypeCodes.CODES)
                {
                    String fullCode = EppFControlActionType.getFullCode(code);
                    double value = rowWrapper.getActionSize(term, fullCode);
                    if (value != 0)
                    {
                        int activeTerm = rowWrapper.getActiveTermNumber(term);
                        SafeMap.safeGet(row2fcaTypeMap, rowId, HashMap.class).putIfAbsent(activeTerm, Lists.newArrayList());
                        row2fcaTypeMap.get(rowId).get(activeTerm).add(fullCode);
                    }
                }
            }
        });

        // отбираем только те, которые есть в части элемента реестра
        for (EppRegistryElementPartFControlAction action : regElement2fcaList)
        {
            EppRegistryElementPart part = action.getPart();
            EppFControlActionType controlAction = action.getControlAction();

            Long rowId = part2RowMap.get(action.getPart());
            Map<Integer, List<String>> term2ActionMap = row2fcaTypeMap.get(rowId);
            if (null != term2ActionMap)
            {
                List<String> fullCodes = term2ActionMap.get(part.getNumber());
                if (CollectionUtils.isNotEmpty(fullCodes) && fullCodes.contains(controlAction.getFullCode()))
                    SafeMap.safeGet(part2fcaMap, part, ArrayList.class).add(controlAction);
            }
        }
        return part2fcaMap;
    }

    @Override
    public List<EppFControlActionType> getUsedFcaTypes(EppEduPlanVersionBlock block, List<SessionALRequestRow> requestRows)
    {
        List<EppFControlActionType> usedFcaTypes = Lists.newArrayList();
        if (null == block) return usedFcaTypes;

        List<SessionALRequestRowMark> markList = getList(SessionALRequestRowMark.class, SessionALRequestRowMark.requestRow(), requestRows);
        markList.stream()
                .filter(rowMark -> !usedFcaTypes.contains(rowMark.getControlAction()))
                .forEach(rowMark -> usedFcaTypes.add(rowMark.getControlAction()));

        List<EppFControlActionType> fcaTypes = getList(EppFControlActionType.class);
        Map<EppRegistryElementPart, List<EppFControlActionType>> part2fcaMap = getPart2FCATypeMap(requestRows);

        IEppEpvBlockWrapper blockWrapper = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockData(block.getId(), true);
        for (SessionALRequestRow requestRow : requestRows)
        {
            EppEpvRowTerm rowTerm = requestRow.getRowTerm();
            EppEpvTermDistributedRow row = rowTerm.getRow();
            IEppEpvRowWrapper rowWrapper = blockWrapper.getRowMap().get(row.getId());
            List<EppFControlActionType> part2fcaTypes = part2fcaMap.get(requestRow.getRegElementPart());
            if (null != part2fcaTypes)
            {
                Collection<Long> part2fcaTypeIds = ids(part2fcaTypes);
                for (EppFControlActionType fcaType : fcaTypes)
                {
                    Long id = fcaType.getId();
                    int value = rowWrapper.getActionSize(rowTerm.getTerm().getIntValue(), fcaType.getFullCode());
                    if (value > 0 && !usedFcaTypes.contains(fcaType) && part2fcaTypeIds.contains(id))
                    {
                        usedFcaTypes.add(fcaType);
                    }
                }
            }
        }
        Collections.sort(usedFcaTypes, (o1, o2) -> Integer.compare(o1.getPriority(), o2.getPriority()));
        return usedFcaTypes;
    }

    @Override
    public void doCreateControlActionColumns(SessionALRequest request, BaseSearchListDataSource source)
    {
        List<SessionALRequestRow> requestRows = getList(SessionALRequestRow.class, SessionALRequestRow.request().id(), request.getId());
        List<EppFControlActionType> usedFcaTypes = getUsedFcaTypes(request.getBlock(), requestRows);

        source.doCleanupDataSource();

        IHeaderColumnBuilder controlActions = SessionReexaminationManager.headerColumn("controlActions");
        source.addColumn(controlActions);
        for (EppFControlActionType fcaType: usedFcaTypes)
        {
            BlockDSColumn column = SessionTransferManager.blockColumn(fcaType.getFullCode(), "controlActionBlock").width("1px").create();
            column.setLabel(fcaType.getTitle());
            controlActions.addSubColumn(column).create();
        }
    }
}
