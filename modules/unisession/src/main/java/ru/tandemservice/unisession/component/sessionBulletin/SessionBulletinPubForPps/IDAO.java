/* $*/

package ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinPubForPps;

import ru.tandemservice.uni.dao.IListDataSourceDao;
import ru.tandemservice.uni.dao.IUpdateable;

/**
 * @author oleyba
 * @since 2/18/11
 */
public interface IDAO extends IUpdateable<Model>, IListDataSourceDao<Model>
{
}
