/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.StateFinalAttestationResultPub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.unisession.entity.report.StateFinalAttestationResult;

/**
 * @author Andrey Andreev
 * @since 02.11.2016
 */
public abstract class SessionReportStateFinalAttestationResultPubUI<Report extends StateFinalAttestationResult> extends UIPresenter
{
    protected Report _report = initReport();
    protected OrgUnitHolder _ouHolder = new OrgUnitHolder();


    public abstract Report initReport();

    @Override
    public void onComponentRefresh()
    {
        setReport(DataAccessServices.dao().getNotNull(getReport().getId()));
    }

    public void onClickPrint()
    {
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().document(_report.getContent()), false);
    }

    public void onClickDelete()
    {
        DataAccessServices.dao().delete(_report);
        deactivate();
    }


    // Getters & Setters
    public Report getReport()
    {
        return _report;
    }

    public void setReport(Report report)
    {
        _report = report;
    }

    public OrgUnitHolder getOuHolder()
    {
        return _ouHolder;
    }

    public OrgUnit getOrgUnit()
    {
        return getOuHolder().getValue();
    }

    public CommonPostfixPermissionModelBase getSec()
    {
        return getOuHolder().getSecModel();
    }

    @Override
    public ISecured getSecuredObject()
    {
        return getOuHolder().getId() == null ? super.getSecuredObject() : getOuHolder().getValue();
    }

    public abstract String getViewPermissionKey();

    public abstract String getDeletePermissionKey();
}
