/* $Id:$ */
package ru.tandemservice.unisession.attestation.bo.Attestation.ui.Add;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.unisession.attestation.bo.Attestation.AttestationManager;
import ru.tandemservice.unisession.entity.document.SessionObject;

/**
 * @author oleyba
 * @since 10/9/12
 */
@Input({
    @Bind(key= PublisherActivator.PUBLISHER_ID_KEY, binding="holder.id")
})
public class AttestationAddUI extends UIPresenter
{
    private OrgUnitHolder holder = new OrgUnitHolder();
    private SessionObject session;

    @Override
    public void onComponentRefresh()
    {
        getHolder().refresh();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(AttestationAdd.BIND_ORG_UNIT, getHolder().getValue());
    }

    public void onClickApply()
    {
        AttestationManager.instance().dao().addAttestation(getSession());
        deactivate();
    }

    public OrgUnitHolder getHolder()
    {
        return holder;
    }

    public SessionObject getSession()
    {
        return session;
    }

    public void setSession(SessionObject session)
    {
        this.session = session;
    }
}
