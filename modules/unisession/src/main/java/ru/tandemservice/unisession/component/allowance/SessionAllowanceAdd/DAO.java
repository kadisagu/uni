/* $*/

package ru.tandemservice.unisession.component.allowance.SessionAllowanceAdd;

import com.google.common.collect.ImmutableList;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowed;
import ru.tandemservice.unisession.entity.document.SessionObject;

import java.util.Date;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 3/24/11
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        final SessionObject sessionObject = this.getNotNull(SessionObject.class, model.getSession().getId());
        model.setSession(sessionObject);
        model.setSessionObjectModel(new LazySimpleSelectModel<>(ImmutableList.of(model.getSession())));
        model.setStudentModel(new DQLFullCheckSelectModel(Student.person().identityCard().fullFio().s())
        {
            @Override
            protected DQLSelectBuilder query(final String alias, final String filter)
            {
                final DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(Student.class, alias)
                .order(property(Student.person().identityCard().fullFio().fromAlias(alias)))
                .where(eq(property(Student.educationOrgUnit().groupOrgUnit().fromAlias(alias)), value(sessionObject.getOrgUnit())))
                .where(eq(property(Student.status().active().fromAlias(alias)), value(Boolean.TRUE)))
                .where(eq(property(Student.archival().fromAlias(alias)), value(Boolean.FALSE)))
                .where(notExists(new DQLSelectBuilder()
                .fromEntity(SessionStudentNotAllowed.class, "na")
                .column("na")
                .where(eq(property(alias + ".id"), property(SessionStudentNotAllowed.student().id().fromAlias("na"))))
                .where(eq(property(SessionStudentNotAllowed.session().fromAlias("na")), value(sessionObject)))
                .buildQuery()))
                ;
                FilterUtils.applySimpleLikeFilter(builder, alias, Student.person().identityCard().fullFio().s(), filter);
                return builder;
            }
        });
    }

    @Override
    public void update(final Model model)
    {
        final SessionStudentNotAllowed existing = this.getByNaturalId(new SessionStudentNotAllowed.NaturalId(model.getSession(), model.getStudent()));
        if (null != existing)
        {
            existing.setRemovalDate(null);
            this.update(existing);
        }
        else
        {
            final SessionStudentNotAllowed newNotAllowed = new SessionStudentNotAllowed();
            newNotAllowed.setSession(model.getSession());
            newNotAllowed.setStudent(model.getStudent());
            newNotAllowed.setCreateDate(new Date());
            this.save(newNotAllowed);
        }
    }
}
