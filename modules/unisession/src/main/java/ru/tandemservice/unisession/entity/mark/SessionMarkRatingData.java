package ru.tandemservice.unisession.entity.mark;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.unisession.entity.mark.gen.*;

/**
 * Данные рейтинга для оценки в сессии
 */
public class SessionMarkRatingData extends SessionMarkRatingDataGen
{
    @Override
    @EntityDSLSupport(parts = SessionMarkRatingData.P_SCORED_POINTS_AS_LONG)
    public Double getScoredPoints()
    {
        return ((double) getScoredPointsAsLong()) / 100;
    }

    public void setScoredPoints(double points)
    {
        setScoredPointsAsLong(Math.round(points * 100));
    }
}