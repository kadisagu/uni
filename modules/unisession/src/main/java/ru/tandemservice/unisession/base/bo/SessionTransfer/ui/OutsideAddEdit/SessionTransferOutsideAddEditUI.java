/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionTransfer.ui.OutsideAddEdit;

import com.google.common.collect.ImmutableList;
import org.apache.commons.collections.CollectionUtils;
import org.apache.tapestry.form.validator.BaseValidator;
import org.apache.tapestry.form.validator.Max;
import org.apache.tapestry.form.validator.Min;
import org.apache.tapestry.form.validator.Required;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.unisession.base.bo.SessionMark.SessionMarkManager;
import ru.tandemservice.unisession.base.bo.SessionTransfer.SessionTransferManager;
import ru.tandemservice.unisession.base.bo.SessionTransfer.logic.StudentByGroupOrgUnitComboDSHandler;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.dao.document.ISessionDocumentBaseDAO;
import ru.tandemservice.unisession.entity.document.SessionProjectTheme;
import ru.tandemservice.unisession.entity.document.SessionTransferOutsideDocument;
import ru.tandemservice.unisession.entity.document.SessionTransferOutsideOperation;
import ru.tandemservice.unisession.util.selectModel.SessionTermModel;

import java.util.*;

/**
 * @author oleyba, vip_delete
 * @since 10/19/11
 */
@Input({
        @Bind(key = SessionTransferOutsideAddEditUI.STUDENT_ID, binding = "studentId"),
        @Bind(key = SessionTransferOutsideAddEditUI.DOCUMENT_ID, binding = "documentId"),
        @Bind(key = SessionTransferOutsideAddEditUI.ORG_UNIT_ID, binding = "orgUnitId")
})
public class SessionTransferOutsideAddEditUI extends UIPresenter
{
    public static final String STUDENT_ID = "studentId";
    public static final String DOCUMENT_ID = "documentId";
    public static final String ORG_UNIT_ID = PublisherActivator.PUBLISHER_ID_KEY;

    private Long _studentId;
    private Long _documentId;
    private Long _orgUnitId;
    private boolean _editForm;

    private Student _student;
    private SessionTransferOutsideDocument _document;
    private OrgUnit _orgUnit;

    private List<SessionTransferOutOpWrapper> _rowList;
    private Date _markDate;

    private SessionTransferOutOpWrapper _currentRow;
    private SessionTransferOutOpWrapper _editedRow;
    private Integer editedRowIndex;
    boolean rowAdded;
    boolean useCurrentRating;
    
    private ISelectModel _termModel;
    private ISelectModel _markModel;
    private ISelectModel _caModel = new LazySimpleSelectModel<>(EppFControlActionType.class);

    @Override
    public void onComponentRefresh()
    {
        ICommonDAO dao = DataAccessServices.dao();

        setStudent(getStudentId() == null ? null : dao.<Student>getNotNull(getStudentId()));
        setOrgUnit(getOrgUnitId() == null ? null : dao.<OrgUnit>getNotNull(getOrgUnitId()));
        setRowList(new ArrayList<>());

        setEditedRow(null);
        setEditedRowIndex(null);
        setRowAdded(false);

        if (isAddForm())
        {
            // форма добавления внешнего перезачтения
            setEditForm(false);
	        if(getDocument() == null)
		        setDocument(new SessionTransferOutsideDocument());
            getDocument().setFormingDate(new Date());

            if (getStudentId() != null)
            {
                getDocument().setTargetStudent(getStudent());
                getDocument().setOrgUnit(getStudent().getEducationOrgUnit().getGroupOrgUnit());
            }
        } else
        {
	        if(getDocument() == null)
		        setDocument(dao.<SessionTransferOutsideDocument>getNotNull(getDocumentId()));
            // форма редактирования внешнего перезачтения
            setEditForm(true);
            setStudent(getDocument().getTargetStudent());
            List<SessionTransferOutsideOperation> operations = dao.getList(SessionTransferOutsideOperation.class,
                    SessionTransferOutsideOperation.targetMark().slot().document(), getDocument(),
                    SessionTransferOutsideOperation.targetMark().slot().studentWpeCAction().studentWpe().year().educationYear().intValue().s(),
                    SessionTransferOutsideOperation.targetMark().slot().studentWpeCAction().studentWpe().term().intValue().s(),
                    SessionTransferOutsideOperation.targetMark().slot().studentWpeCAction().studentWpe().registryElementPart().registryElement().title().s(),
                    SessionTransferOutsideOperation.targetMark().slot().studentWpeCAction().type().priority().s()
            );

			Map<SessionTransferOutsideOperation, SessionProjectTheme> operation2theme = SessionTransferManager.instance().dao().getThemesByTransferOperations(operations);

            for (SessionTransferOutsideOperation operation : operations)
            {
                getRowList().add(new SessionTransferOutOpWrapper(operation, operation2theme.get(operation)));
                setMarkDate(operation.getTargetMark().getPerformDate());
            }
        }

        if (getStudent() != null)
            setTermModel(SessionTermModel.createForStudent(getStudent()));

        useCurrentRating = ISessionBrsDao.instance.get().getSettings().isUseCurrentRatingForTransferDocument();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (SessionTransferOutsideAddEdit.DS_EPP_SLOT.equals(dataSource.getName()))
        {
            dataSource.put(SessionTransferOutsideAddEdit.KEY_STUDENT, getStudent());
            if (null != getEditedRow() && null != getEditedRow().getTerm())
            {
                dataSource.put(SessionTransferOutsideAddEdit.KEY_EDU_YEAR, getEditedRow().getTerm().getYear());
                dataSource.put(SessionTransferOutsideAddEdit.KEY_YEAR_PART, getEditedRow().getTerm().getPart());
            }
        } else if (SessionTransferOutsideAddEdit.STUDENT_DS.equals(dataSource.getName()))
        {
            dataSource.put(StudentByGroupOrgUnitComboDSHandler.ORG_UNIT, getOrgUnit());
        }
    }

    // Listeners

    public void onSelectStudent()
    {
        // выбирать студента можно только на форме добавления и при не указанном студенте
        if (!isNeedSelectStudent())
            throw new IllegalStateException("Student changing is not allowed.");

        getDocument().setTargetStudent(getStudent());
        getDocument().setOrgUnit(getStudent().getEducationOrgUnit().getGroupOrgUnit());
        setTermModel(SessionTermModel.createForStudent(getStudent()));
        setRowList(new ArrayList<>());
        setEditedRow(null);
        setEditedRowIndex(null);
    }

    public void onClickApply()
    {
        if (getStudent().getEducationOrgUnit().getGroupOrgUnit() == null) {
            throw new ApplicationException("Нельзя добавить документ о перезачтении студенту, т.к. для его направления подготовки не задан ответственный деканат, который необходим для сохранения документа о перезачтении. Укажите деканат в настройке «Диспетчерские и деканаты».");
        }
        SessionTransferManager.instance().dao().saveOrUpdateOutsideTransfer(getDocument(), getMarkDate(), getRowList());
        deactivate();
    }

    public void onClickAddRow()
    {
        if (getEditedRow() != null) return;
        final SessionTransferOutOpWrapper newRow = new SessionTransferOutOpWrapper();
        setEditedRow(newRow);

        getRowList().add(new SessionTransferOutOpWrapper());
        setEditedRowIndex(getRowList().size() - 1);

        setRowAdded(true);
    }

    public void onClickSaveRow()
    {
        if (getEditedRowIndex() != null && getRowList().size() > getEditedRowIndex() && getEditedRowIndex() >= 0)
            getRowList().set(getEditedRowIndex(), getEditedRow());
        setEditedRow(null);
        setRowAdded(false);
    }

    public void onClickCancelEdit()
    {
        if (isRowAdded() && !getRowList().isEmpty())
            getRowList().remove(getRowList().size() - 1);
        setEditedRowIndex(null);
        setEditedRow(null);
        setRowAdded(false);
    }

    public void onClickEditRow()
    {
        if (getEditedRow() != null) return;
        SessionTransferOutOpWrapper baseRow = findRow();
        setEditedRow(new SessionTransferOutOpWrapper(baseRow));
        setEditedRowIndex(getRowList().indexOf(baseRow));
        prepareTargetMarkModel();
        setRowAdded(false);
    }

    public void onClickDeleteRow()
    {
        SessionTransferOutOpWrapper row = findRow();
        if (null != row)
            getRowList().remove(row);
    }

	// При выборе текущего мероприятия при необходимости устанавливаем тему по последнему документу сессии с выбранным текущим мероприятием.
	// Тема берется из этого документа, если ФИК мероприятия требует тему, но в текущей записи студента по перезачтению темы еще нет.
	public void onWpeCActionSelected()
	{
		prepareTargetMarkModel();
		if(!getEditedRow().isThemeExistForDocSlot() && isEditedRowThemeVisible())
			getEditedRow().setTheme(ISessionDocumentBaseDAO.instance.get().themeByLastWpeThemes(getEditedRow().getEppSlot()));
	}

    public void prepareTargetMarkModel()
    {
        setMarkModel(null == getEditedRow() || null == getEditedRow().getEppSlot() ?
                new LazySimpleSelectModel<>(ImmutableList.of()) :
                SessionMarkManager.instance().regularMarkDao().prepareSimpleMarkModel(getEditedRow().getEppSlot(), null, true)
        );
    }

    // Util

    public boolean isAddForm() {
        return getDocumentId() == null;
    }

    public boolean isNeedSelectStudent()
    {
        // если на форме добавления не указан студент, то его нужно выбрать
        return getDocumentId() == null && getStudentId() == null;
    }

    public boolean isHasStudent()
    {
        return getStudent() != null;
    }

    public boolean isEditMode()
    {
        return getEditedRow() != null;
    }

    public boolean isListEmpty()
    {
        return CollectionUtils.isEmpty(getRowList());
    }

    public boolean isCurrentRowInEditMode()
    {
        return getEditedRow() != null && getEditedRowIndex().equals(getRowList().indexOf(getCurrentRow()));
    }

    public SessionTransferOutOpWrapper findRow()
    {
        Integer id = getListenerParameter();
        SessionTransferOutOpWrapper row = null;
        for (SessionTransferOutOpWrapper r : getRowList())
            if (r.getId() == id)
                row = r;
        return row;
    }

	public boolean isEditedRowThemeVisible()
	{
		return getEditedRow().getEppSlot() != null && getEditedRow().getEppSlot().getActionType().isThemeRequired();
	}

    public boolean isRatingDisabled()
    {
        return !(getEditedRow() != null && getEditedRow().getMark() != null);
    }

    // Getters & Setters

    public boolean isUseCurrentRating()
    {
        return useCurrentRating;
    }

    public Integer getColumnCount()
    {
        return isUseCurrentRating() ? 4 : 3;
    }

    public List<BaseValidator> getRatingValidators()
    {
        final List<BaseValidator> validators = new ArrayList<>(3);
        if (ISessionBrsDao.instance.get().getSettings().isCurrentRatingRequiredForTransferDocument())
            validators.add(new Required());
        validators.add(new Min("min=0"));
        validators.add(new Max("max=100"));
        return validators;
    }

    public void setUseCurrentRating(boolean useCurrentRating)
    {
        this.useCurrentRating = useCurrentRating;
    }

    public Long getStudentId()
    {
        return _studentId;
    }

    public void setStudentId(Long studentId)
    {
        _studentId = studentId;
    }

    public Long getDocumentId()
    {
        return _documentId;
    }

    public void setDocumentId(Long documentId)
    {
        _documentId = documentId;
    }

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public boolean isEditForm()
    {
        return _editForm;
    }

    public void setEditForm(boolean editForm)
    {
        _editForm = editForm;
    }

    public Student getStudent()
    {
        return _student;
    }

    public void setStudent(Student student)
    {
        _student = student;
    }

    public SessionTransferOutsideDocument getDocument()
    {
        return _document;
    }

    public void setDocument(SessionTransferOutsideDocument document)
    {
        _document = document;
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }

    public List<SessionTransferOutOpWrapper> getRowList()
    {
        return _rowList;
    }

    public void setRowList(List<SessionTransferOutOpWrapper> rowList)
    {
        _rowList = rowList;
    }

    public Date getMarkDate()
    {
        return _markDate;
    }

    public void setMarkDate(Date markDate)
    {
        _markDate = markDate;
    }

    public SessionTransferOutOpWrapper getCurrentRow()
    {
        return _currentRow;
    }

    public void setCurrentRow(SessionTransferOutOpWrapper currentRow)
    {
        _currentRow = currentRow;
    }

    public SessionTransferOutOpWrapper getEditedRow()
    {
        return _editedRow;
    }

    public void setEditedRow(SessionTransferOutOpWrapper editedRow)
    {
        _editedRow = editedRow;
    }

    public ISelectModel getTermModel()
    {
        return _termModel;
    }

    public void setTermModel(ISelectModel termModel)
    {
        _termModel = termModel;
    }

    public ISelectModel getMarkModel()
    {
        return _markModel;
    }

    public void setMarkModel(ISelectModel markModel)
    {
        _markModel = markModel;
    }

    public ISelectModel getCaModel()
    {
        return _caModel;
    }

    public void setCaModel(ISelectModel caModel)
    {
        _caModel = caModel;
    }

    public Integer getEditedRowIndex()
    {
        return editedRowIndex;
    }

    public void setEditedRowIndex(Integer editedRowIndex)
    {
        this.editedRowIndex = editedRowIndex;
    }

    public boolean isRowAdded() {
        return rowAdded;
    }

    public void setRowAdded(boolean rowAdded) {
        this.rowAdded = rowAdded;
    }
}