package ru.tandemservice.unisession.entity.document;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.shared.commonbase.base.util.INumberGenerationRule;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unisession.dao.transfer.ISessionTransferDAO;
import ru.tandemservice.unisession.entity.document.gen.SessionTransferDocumentGen;
import ru.tandemservice.unisession.sec.ISessionSecDao;

import java.util.Collection;
import java.util.Collections;

/**
 * Перезачтение
 */
public class SessionTransferDocument extends SessionTransferDocumentGen
{
    @Override
    public INumberGenerationRule<SessionTransferDocument> getNumberGenerationRule() {
        return ISessionTransferDAO.instance.get().getNumberGenerationRule();
    }

    @Override
    @EntityDSLSupport(parts = "number")
    public String getTitle() { return this.getNumber(); }

    @Override
    @EntityDSLSupport(parts=SessionDocument.P_NUMBER)
    public String getTypeTitle() { return "Перезачтение №" + this.getNumber(); }

    @Override
    @EntityDSLSupport(parts=SessionDocument.P_NUMBER)
    public String getTypeShortTitle()
    {
        return "Перезачт. №" + this.getNumber();
    }

    @Override
    public Collection<IEntity> getSecLocalEntities() {
        return ISessionSecDao.instance.get().getSecLocalEntities(this);
    }

    @Override
    public OrgUnit getGroupOu() {
        return getOrgUnit();
    }
}