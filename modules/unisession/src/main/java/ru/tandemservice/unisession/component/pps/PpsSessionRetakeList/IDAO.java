/* $Id$ */
package ru.tandemservice.unisession.component.pps.PpsSessionRetakeList;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.uni.dao.IPrepareable;

/**
 * @author Vasily Zhukov
 * @since 12.01.2012
 */
public interface IDAO extends IPrepareable<Model>
{
    @Transactional(propagation= Propagation.SUPPORTS)
    void refreshDataSource(Model model);
}
