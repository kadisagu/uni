/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionTransfer;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unisession.base.bo.SessionTransfer.logic.ISessionTransferDao;
import ru.tandemservice.unisession.print.ISessionTransferProtocolPrintDao;
import ru.tandemservice.unisession.base.bo.SessionTransfer.logic.SessionTransferDao;
import ru.tandemservice.unisession.print.SessionTransferProtocolPrintDao;

/**
 * @author oleyba
 * @since 10/19/11
 */
@Configuration
public class SessionTransferManager extends BusinessObjectManager
{
    public static SessionTransferManager instance()
    {
        return instance(SessionTransferManager.class);
    }

    @Bean
    public ISessionTransferDao dao()
    {
        return new SessionTransferDao();
    }

    @Bean
    public ISessionTransferProtocolPrintDao protocolPrintDao() {return new SessionTransferProtocolPrintDao();}
}
