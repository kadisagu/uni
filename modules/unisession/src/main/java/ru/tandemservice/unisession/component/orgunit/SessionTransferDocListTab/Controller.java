/* $Id$ */
// Copyright 2006-2012 Tandem Service Software
package ru.tandemservice.unisession.component.orgunit.SessionTransferDocListTab;

import org.tandemframework.caf.service.impl.CAFLegacySupportService;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.DefaultPublisherLinkResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unisession.base.bo.SessionTransfer.SessionTransferManager;
import ru.tandemservice.unisession.base.bo.SessionTransfer.ui.InsideAddEdit.SessionTransferInsideAddEdit;
import ru.tandemservice.unisession.base.bo.SessionTransfer.ui.InsideAddEdit.SessionTransferInsideAddEditUI;
import ru.tandemservice.unisession.base.bo.SessionTransfer.ui.OutsideAddEdit.SessionTransferOutsideAddEdit;
import ru.tandemservice.unisession.base.bo.SessionTransfer.ui.OutsideAddEdit.SessionTransferOutsideAddEditUI;
import ru.tandemservice.unisession.entity.document.SessionTransferDocument;
import ru.tandemservice.unisession.entity.document.SessionTransferInsideDocument;
import ru.tandemservice.unisession.entity.document.SessionTransferOutsideDocument;

/**
 * @author Vasily Zhukov
 * @since 10.01.2012
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);
        model.setSecModel(new OrgUnitSecModel(model.getOrgUnit()));

        model.setSettingsKey("session.simpledoc." + model.getOrgUnitId());
        model.setSettings(UniBaseUtils.getDataSettings(component, model.getSettingsKey()));

        DynamicListDataSource<SessionTransferDocument> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().refreshDataSource(getModel(component1));
        });

        dataSource.addColumn(new PublisherLinkColumn("Номер", SessionTransferDocument.number()));
        dataSource.addColumn(new SimpleColumn("Дата формирования", SessionTransferDocument.formingDate(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Тип перезачтения", "docType").setClickable(false).setOrderable(false));
        dataSource.addColumn(new PublisherLinkColumn("ФИО студента", Student.person().identityCard().fullFio(), SessionTransferDocument.L_TARGET_STUDENT).setResolver(new DefaultPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                return new ParametersMap()
                        .add(UIPresenter.PUBLISHER_ID, entity.getId())
                        .add(ru.tandemservice.uni.component.student.StudentPub.Model.SELECTED_TAB_BIND_KEY, "studentNewSessionTab")
                        .add("selectedDataTab", "studentSessionDocumentTab");
            }
        }).setClickable(true));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEdit").setPermissionKey(model.getSecModel().getPermission("editSessionTransferDoc")));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete", "Удалить перезачтение №{0}?", SessionTransferDocument.number().s()).setPermissionKey(model.getSecModel().getPermission("deleteSessionTransferDoc")));

        model.setDataSource(dataSource);
    }

    public void onClickSearch(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.getDataSource().showFirstPage();
        model.getDataSource().refresh();
        DataSettingsFacade.saveSettings(model.getSettings());
    }

    public void onClickClear(IBusinessComponent component)
    {
        getModel(component).getSettings().clear();
        onClickSearch(component);
    }

    public void onClickEdit(IBusinessComponent component)
    {
        SessionTransferDocument document = DataAccessServices.dao().getNotNull(component.<Long>getListenerParameter());
        if (document instanceof SessionTransferOutsideDocument)
            onClickTransferOutsideActivate(SessionTransferOutsideAddEdit.class.getSimpleName(), document.getId(), null);
        else if (document instanceof SessionTransferInsideDocument)
            onClickTransferInsideActivate(SessionTransferInsideAddEdit.class.getSimpleName(), document.getId(), null);
        else
            throw new RuntimeException("Invalid transferDocument type: " + document.getClass());
    }

    public void onClickDelete(IBusinessComponent component)
    {
        SessionTransferManager.instance().dao().deleteSessionTransfer(component.<Long>getListenerParameter());
    }

    public void onClickAddInsideTransfer(IBusinessComponent component)
    {
        onClickTransferInsideActivate(SessionTransferInsideAddEdit.class.getSimpleName(), null, getModel(component).getOrgUnit().getId());
    }

    public void onClickAddOutsideTransfer(IBusinessComponent component)
    {
        onClickTransferOutsideActivate(SessionTransferOutsideAddEdit.class.getSimpleName(), null, getModel(component).getOrgUnit().getId());
    }

    private void onClickTransferInsideActivate(String componentName, Long documentId, Long orgUnitId)
    {
        CAFLegacySupportService.asDesktopRoot(componentName)
                .parameters(new ParametersMap()
                                    .add(SessionTransferInsideAddEditUI.STUDENT_ID, null)
                                    .add(SessionTransferInsideAddEditUI.DOCUMENT_ID, documentId)
                                    .add(SessionTransferInsideAddEditUI.ORG_UNIT_ID, orgUnitId))
                .activate();
    }

    private void onClickTransferOutsideActivate(String componentName, Long documentId, Long orgUnitId)
    {
        CAFLegacySupportService.asDesktopRoot(componentName)
                .parameters(new ParametersMap()
                                    .add(SessionTransferOutsideAddEditUI.STUDENT_ID, null)
                                    .add(SessionTransferOutsideAddEditUI.DOCUMENT_ID, documentId)
                                    .add(SessionTransferOutsideAddEditUI.ORG_UNIT_ID, orgUnitId))
                .activate();
    }
}
