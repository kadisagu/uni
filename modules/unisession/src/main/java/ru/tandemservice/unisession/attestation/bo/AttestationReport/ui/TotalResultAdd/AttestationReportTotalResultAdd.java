/**
 *$Id$
 */
package ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.TotalResultAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniEduProgram.UniEduProgramManager;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.unisession.attestation.bo.AttestationReport.AttestationReportManager;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.base.bo.SessionReport.util.UniAttestationFilterAddon;
import ru.tandemservice.unisession.base.bo.SessionReport.util.UniSessionFilterAddon;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Alexander Shaburov
 * @since 22.11.12
 */
@Configuration
public class AttestationReportTotalResultAdd extends BusinessComponentManager
{
    public static final String ATTESTATION_DS = AttestationReportManager.DS_ATTESTATION;
    public static final String YEAR_PART_DS = SessionReportManager.DS_YEAR_PART;
    public static final String COURSE_DS = UniStudentManger.COURSE_DS;
    public static final String RESULT_FOR_DS = "resultForDS";
    public static final String COMPENSATION_TYPE_DS = UniStudentManger.COMPENSATION_TYPE_DS;
    public static final String STUDENT_STATUS_DS = UniStudentManger.STUDENT_STATUS_DS;
    public static final String FORMATIVE_ORG_UNIT_DS = "formativeOrgUnitDS";

    public static final Long DW_GROUP_ID = 1L;
    public static final Long DW_COURSE_ID = 2L;
    public static final Long DW_DIRECTION_ID = 3L;
    public static final Long DW_DIRECTION_DETAIL_ID = 4L;

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addAddon(uiAddon(CommonFilterAddon.class.getSimpleName(), UniAttestationFilterAddon.class))
                .addDataSource(AttestationReportManager.instance().attestationDSConfig())
                .addDataSource(SessionReportManager.instance().yearPartDSConfig())
                .addDataSource(selectDS(RESULT_FOR_DS, resultForDSHandler()))
                .addDataSource(selectDS(FORMATIVE_ORG_UNIT_DS, formativeOrgUnitDSHandler()).addColumn(OrgUnit.fullTitle().s()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> formativeOrgUnitDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), OrgUnit.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                DQLSelectBuilder inclBuilder = new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, "b").column(property(EducationOrgUnit.formativeOrgUnit().id().fromAlias("b")));

                dql.where(in(property(OrgUnit.id().fromAlias(alias)), inclBuilder.buildQuery()));
            }
        }
                .order(OrgUnit.fullTitle())
                .filter(OrgUnit.fullTitle());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> resultForDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
                .addRecord(DW_GROUP_ID, "по группам")
                .addRecord(DW_COURSE_ID, "по курсам")
                .addRecord(DW_DIRECTION_ID, "по направлениям")
                .addRecord(DW_DIRECTION_DETAIL_ID, "по направлениям (детализация)");
    }
}
