package ru.tandemservice.unisession.component.sessionListDocument.SessionListDocumentAddEdit;

import ru.tandemservice.uni.dao.IListDataSourceDao;
import ru.tandemservice.uni.dao.IUpdateable;

/**
 * @author iolshvang
 * @since 12.04.2011
 */
public interface IDAO extends IUpdateable<Model>, IListDataSourceDao<Model>
{

    /**
     * Выкидывает из выбранных Мероприятий студента строки которых там быть не должно.
     * Например при смене причины.
     */
    void checkWpeCAction(Model model);
}
