/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionTransfer.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Session;
import org.tandemframework.core.common.INaturalId;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.hibsupport.dql.util.DQLSelectColumnNumerator;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.commonbase.base.util.INumberQueueDAO;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import org.tandemframework.shared.commonbase.events.EventListenerLocker;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.pps.PpsEntryByEmployeePost;
import ru.tandemservice.uniepp.base.bo.EppState.logic.EppDSetUpdateCheckEventListener;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartWrapper;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElWrapper;
import ru.tandemservice.uniepp.dao.workplan.EppWorkPlanPartLoadDistributor;
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDAO;
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDataDAO;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeFCA;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.catalog.EppWorkPlanRowKind;
import ru.tandemservice.uniepp.entity.catalog.codes.EppWorkPlanRowKindCodes;
import ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.uniepp.entity.workplan.*;
import ru.tandemservice.unisession.UnisessionDefines;
import ru.tandemservice.unisession.base.bo.SessionMark.SessionMarkManager;
import ru.tandemservice.unisession.base.bo.SessionMark.logic.ISessionMarkDAO;
import ru.tandemservice.unisession.base.bo.SessionTransfer.ui.InsideAddEdit.SessionTransferInOpWrapper;
import ru.tandemservice.unisession.dao.document.ISessionDocumentBaseDAO;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkStateCatalogItem;
import ru.tandemservice.unisession.entity.comission.SessionComission;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.*;
import ru.tandemservice.unisession.entity.mark.*;
import ru.tandemservice.unisession.settings.bo.SessionSettings.SessionSettingsManager;
import ru.tandemservice.unisession.util.selectModel.SessionTermModel;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 10/19/11
 */
public class SessionTransferDao extends UniBaseDao implements ISessionTransferDao
{
    @Override
    public void saveOrUpdateOutsideTransfer(SessionTransferOutsideDocument document, final Date markDate, List<? extends SessionTransferOutsideOperationInfo> rowList)
    {
        final Session session = getSession();

        // удаляем предыдущее содержимое, чтобы не возиться со сверкой
        if (document.getId() != null)
        {
            List<SessionDocumentSlot> slotList = new ArrayList<>();
            for (SessionTransferOperation operation : getList(SessionTransferOperation.class, SessionTransferOperation.targetMark().slot().document().s(), document))
            {
                delete(operation);
                slotList.add(operation.getTargetMark().getSlot());
            }
            session.flush();
            slotList.forEach(this::delete);
            session.flush();
        }

        saveOrUpdate(document);

        SessionComission commission = new SessionComission();
        save(commission);

		ISessionDocumentBaseDAO baseDAO = ISessionDocumentBaseDAO.instance.get();

        // сохраняем заново все мероприятия
        for (final SessionTransferOutsideOperationInfo item : rowList)
        {
            SessionDocumentSlot slot = new SessionDocumentSlot();
            slot.setDocument(document);
            slot.setStudentWpeCAction(item.getEppSlot());
            slot.setCommission(commission);
            slot.setInSession(true);
            save(slot);

            SessionMark mark = SessionMarkManager.instance().regularMarkDao().saveOrUpdateMark(slot, new ISessionMarkDAO.MarkData()
            {
                @Override
                public Date getPerformDate()
                {
                    return markDate;
                }

                @Override
                public Double getPoints()
                {
                    return item.getRating() != null ? item.getRating() : null;
                }

                @Override
                public SessionMarkCatalogItem getMarkValue()
                {
                    return item.getMark();
                }

                @Override
                public String getComment()
                {
                    return null;
                }
            });

			baseDAO.updateProjectTheme(slot, item.getTheme(), null);

            SessionTransferOutsideOperation operation = new SessionTransferOutsideOperation();
            operation.setTargetMark((SessionSlotMarkGradeValue) mark);
            operation.setDiscipline(item.getSourceDiscipline());
            operation.setControlAction(item.getSourceControlAction());
            operation.setMark(item.getSourceMark());
            operation.setComment(item.getComment());
            save(operation);
        }
    }

    @Override
    public void deleteSessionTransfer(Long documentId)
    {
        SessionTransferDocument document = getNotNull(documentId);

        // удаляем все оценки - они удалятся вместе с Перезачтенными мероприятиями
        new DQLDeleteBuilder(SessionTransferOperation.class)
                .where(eq(property(SessionTransferOperation.targetMark().slot().document()), value(document)))
                .createStatement(getSession()).execute();

        // удаляем перезачтение
        delete(document);
    }

    @Override
    public void saveOrUpdateInsideTransfer(SessionTransferInsideDocument document, final Date markDate, List<? extends SessionTransferInsideOperationInfo> rowList)
	{
        final Session session = getSession();

        // удаляем предыдущее содержимое, чтобы не возиться со сверкой
        if (document.getId() != null)
        {
            List<SessionDocumentSlot> slotList = new ArrayList<>();
            for (SessionTransferOperation operation : getList(SessionTransferOperation.class, SessionTransferOperation.targetMark().slot().document().s(), document))
            {
                delete(operation);
                slotList.add(operation.getTargetMark().getSlot());
            }
            session.flush();
            slotList.forEach(this::delete);
            session.flush();
        }

        saveOrUpdate(document);

        SessionComission commission = new SessionComission();
        save(commission);

		ISessionDocumentBaseDAO baseDAO = ISessionDocumentBaseDAO.instance.get();

        // сохраняем заново все мероприятия
        for (final SessionTransferInsideOperationInfo item : rowList)
        {
            SessionDocumentSlot slot = new SessionDocumentSlot();
            slot.setDocument(document);
            slot.setStudentWpeCAction(item.getEppSlot());
            slot.setCommission(commission);
            slot.setInSession(true);
            save(slot);

            SessionMark mark = SessionMarkManager.instance().regularMarkDao().saveOrUpdateMark(slot, new ISessionMarkDAO.MarkData()
            {
                @Override
                public Date getPerformDate()
                {
                    return markDate;
                }

                @Override
                public Double getPoints()
                {
                    return item.getRating() != null ? item.getRating() : null;
                }

                @Override
                public SessionMarkCatalogItem getMarkValue()
                {
                    return item.getMark();
                }

                @Override
                public String getComment()
                {
                    return null;
                }
            });

			baseDAO.updateProjectTheme(slot, item.getTheme(), null);

            SessionTransferInsideOperation operation = new SessionTransferInsideOperation();
            operation.setTargetMark((SessionSlotRegularMark) mark);
            operation.setSourceMark((SessionSlotRegularMark) item.getSourceMark());
            operation.setComment(item.getComment());
            save(operation);
        }
    }

    @Override
    public List<SessionTransferInOpWrapper> getAutoFormInnerTransfers(Long studentId)
    {
        Map<String, Boolean> settingsMap = SessionSettingsManager.instance().dao().getSettingsFormationTransferMap();
        List<SessionTransferInOpWrapper> result = new ArrayList<>();

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(SessionSlotRegularMark.class, "mark")
                /*0*/ /* id оценки, используется для выбора подходящей оценки */
                .column(property("mark", SessionSlotRegularMark.id()))
                /*1*/ /* id документа сессии, используется для отсеивания "Внутренних перезачтений" и "Зачеток студента" */
                .column(property("mark", SessionSlotRegularMark.slot().document().id()))
                .joinPath(DQLJoinType.inner, SessionMark.slot().fromAlias("mark"), "s")
                .joinPath(DQLJoinType.inner, SessionDocumentSlot.studentWpeCAction().studentWpe().fromAlias("s"), "wpe")
                /* неактуальные оценки */
                .where(isNotNull(property(EppStudentWorkPlanElement.removalDate().fromAlias("wpe"))))
                /* оценки данного студента */
                .where(eq(property(SessionDocumentSlot.actualStudent().id().fromAlias("s")), value(studentId)))
                /* кроме тех оценок, на которые есть перезачтения с актуальным МСРП-ФК */
                .where(notIn(property("mark", SessionSlotRegularMark.id()),
                        new DQLSelectBuilder()
                                .fromEntity(SessionTransferInsideOperation.class, "op")
                                .column(property("op", SessionTransferInsideOperation.sourceMark().id()))
                                .where(isNull(property("op", SessionTransferInsideOperation.targetMark().slot().studentWpeCAction().removalDate())))
                                .where(eq(property("op", SessionTransferInsideOperation.targetMark().slot().studentWpeCAction().studentWpe().student().id()), value(studentId)))
                                .buildQuery()))
                /*2*/ /* id подходящего МСРП-ФК для перезачтения: */
                .column(DQLFunctions.coalesce(
                        /* если есть единственное с данной частью дисциплины и формой сдачи, то выбираем его */
                        new DQLSelectBuilder()
                                .fromEntity(EppStudentWpeCAction.class, "ss")
                                .column(DQLFunctions.max(property("ss.id")))
                                .where(eq(property("ss", EppStudentWpeCAction.studentWpe().student().id()), value(studentId)))
                                .where(isNull(property("ss", EppStudentWpeCAction.studentWpe().removalDate())))

                                .group(property("ss", EppStudentWpeCAction.studentWpe().registryElementPart()))
                                .group(property("ss", EppStudentWpeCAction.type()))

                                .having(eq(DQLFunctions.countStar(), value(1)))
                                .having(eq(property("ss", EppStudentWpeCAction.studentWpe().registryElementPart()), property("wpe", EppStudentWorkPlanElement.registryElementPart())))
                                .having(eq(property("ss", EppStudentWpeCAction.type()), property("s", SessionDocumentSlot.studentWpeCAction().type())))
                                .buildQuery(),

                        /* в противном случае берем подходящего по naturalId: студент, год, курс, семестр, часть года, часть дисциплины, форма сдачи */
                        new DQLSelectBuilder()
                                .fromEntity(EppStudentWpeCAction.class, "ss")
                                .column(DQLFunctions.max(property("ss.id")))
                                .joinPath(DQLJoinType.inner, EppStudentWpeCAction.studentWpe().fromAlias("ss"), "s_wpe")
                                .where(isNull(property("ss", EppStudentWpeCAction.studentWpe().removalDate())))
                                .where(eq(property("ss", EppStudentWpeCAction.type()), property("s", SessionDocumentSlot.studentWpeCAction().type())))
                                .where(eq(property("s_wpe", EppStudentWorkPlanElement.student().id()), value(studentId)))
                                .where(eq(property("s_wpe", EppStudentWorkPlanElement.registryElementPart()), property("wpe", EppStudentWorkPlanElement.registryElementPart())))
                                .where(eq(property("s_wpe", EppStudentWorkPlanElement.year()), property("wpe", EppStudentWorkPlanElement.year())))
                                .where(eq(property("s_wpe", EppStudentWorkPlanElement.course()), property("wpe", EppStudentWorkPlanElement.course())))
                                .where(eq(property("s_wpe", EppStudentWorkPlanElement.term()), property("wpe", EppStudentWorkPlanElement.term())))
                                .where(eq(property("s_wpe", EppStudentWorkPlanElement.part()), property("wpe", EppStudentWorkPlanElement.part())))
                                .buildQuery()
                ));

        DQLSelectBuilder markDql = new DQLSelectBuilder().fromEntity(SessionMark.class, "mm").where(eq(property("mark.id"), property("mm", SessionMark.id())));

        Boolean markNegative = settingsMap.get(UnisessionDefines.SETTINGS_MARK_NEGATIVE_ITEM);
        Boolean markRetake = settingsMap.get(UnisessionDefines.SETTINGS_MARK_RETAKE_ITEM);
        Boolean markNotRetake = settingsMap.get(UnisessionDefines.SETTINGS_MARK_NOT_RETAKE_ITEM);

        if (!markRetake && !markNotRetake)
        {
            // выводим только оценки
            markDql.where(instanceOf(SessionMark.cachedMarkValue().fromAlias("mm").s(), SessionMarkGradeValueCatalogItem.class));
            if (!markNegative)
                markDql.where(eq(property("mm", SessionMark.cachedMarkPositiveStatus()), value(Boolean.TRUE)));
        }
        else
        {
            markDql.joinPath(DQLJoinType.left, SessionMark.cachedMarkValue().fromAlias("mm"), "cmv");
            markDql.joinEntity("cmv", DQLJoinType.left, SessionMarkStateCatalogItem.class, "ms", eq(property("cmv.id"), property("ms.id")));
            markDql.where(or(
                    and(
                            instanceOf(SessionMark.cachedMarkValue().fromAlias("mm").s(), SessionMarkGradeValueCatalogItem.class),
                            markNegative ? null : eq(property("mm", SessionMark.cachedMarkPositiveStatus()), value(Boolean.TRUE))),
                    and(
                            instanceOf(SessionMark.cachedMarkValue().fromAlias("mm").s(), SessionMarkStateCatalogItem.class),
                            markRetake && markNotRetake ? null : eq(property("ms", SessionMarkStateCatalogItem.remarkable()), value(markRetake))
                    )
            ));
        }
        builder.where(exists(markDql.buildQuery()));

        for (Object[] row: builder.createStatement(getSession()).<Object[]>list())
        {
            Long markId = (Long) row[0];
            Long sessionDocumentId = (Long) row[1];
            Long slotId = (Long) row[2];

            if (slotId == null)
                /* нечего сопоставлять */
                continue;

            @SuppressWarnings("unchecked")
            Class<? extends SessionDocument> sessionDocumentClass = EntityRuntime.getMeta(sessionDocumentId).getEntityClass();
            if (SessionTransferInsideDocument.class.isAssignableFrom(sessionDocumentClass) || SessionStudentGradeBookDocument.class.isAssignableFrom(sessionDocumentClass))
                /* не подходит тип документа сессии */
                continue;

            SessionSlotRegularMark mark = get(SessionSlotRegularMark.class, markId);
            EppStudentWpeCAction slot = get(EppStudentWpeCAction.class, slotId);

            SessionTransferInOpWrapper wrapper = new SessionTransferInOpWrapper();
            wrapper.setSourceTerm(new SessionTermModel.TermWrapper(mark.getSlot()));
            wrapper.setSourceEppSlot(mark.getSlot().getStudentWpeCAction());
            wrapper.setSourceMark(mark);
            wrapper.setTerm(new SessionTermModel.TermWrapper(slot));
            wrapper.setEppSlot(slot);
            wrapper.setMark(mark.getValueItem());

            result.add(wrapper);
        }

        return result;
    }

    @Override
    public SessionComission saveCommission(SessionComission commission, List<PpsEntryByEmployeePost> commissionPpsList)
    {
        if (CollectionUtils.isEmpty(commissionPpsList)) return null;

        if (null != commission)
        {
            new DQLDeleteBuilder(SessionComissionPps.class)
                    .where(eq(property(SessionComissionPps.commission()), value(commission)))
                    .createStatement(getSession()).execute();
        }
        else
            commission = new SessionComission();

        saveOrUpdate(commission);

        for (PpsEntryByEmployeePost pps : commissionPpsList)
        {
            save(new SessionComissionPps(commission, pps));
        }

        return commission;
    }

    @Override
    public void saveProtocolRowAndMark(SessionTransferProtocolDocument protocol, List<SessionALRequestRow> requestRows)
    {
        if (requestRows.isEmpty()) return;

        Long requestId = protocol.getRequest().getId();

        Map<Long, List<SessionALRequestRowMark>> requestMarksMap = Maps.newHashMap();
        List<SessionALRequestRowMark> requestMarks = new DQLSelectBuilder().fromEntity(SessionALRequestRowMark.class, "m")
                .where(eq(property("m", SessionALRequestRowMark.requestRow().request().id()), value(requestId)))
                .createStatement(getSession()).list();

        for (SessionALRequestRowMark rowMark : requestMarks)
            SafeMap.safeGet(requestMarksMap, rowMark.getRequestRow().getId(), ArrayList.class).add(rowMark);

        List<SessionTransferProtocolRow> protocolRows = Lists.newArrayList();
        List<SessionTransferProtocolMark> protocolMarks = Lists.newArrayList();

        for (SessionALRequestRow requestRow : requestRows)
        {
            SessionTransferProtocolRow row = new SessionTransferProtocolRow();
            row.setProtocol(protocol);
            row.setRequestRow(requestRow);
            row.setNeedRetake(requestRow.isNeedRetake());
            protocolRows.add(row);
        }

        List<SessionTransferProtocolRow> currentProtocolRows = getList(SessionTransferProtocolRow.class, SessionTransferProtocolRow.protocol().id(), protocol.getId());
        protocolMarks.addAll(getList(SessionTransferProtocolMark.class, SessionTransferProtocolMark.protocolRow().protocol().id(), protocol.getId()));
        if (!currentProtocolRows.isEmpty())
            protocolRows.addAll(currentProtocolRows);

        new MergeAction.SessionMergeAction<Long, SessionTransferProtocolRow>() {
            @Override protected Long key(final SessionTransferProtocolRow source) { return source.getRequestRow().getId(); }
            @Override protected SessionTransferProtocolRow buildRow(final SessionTransferProtocolRow source) {
                return new SessionTransferProtocolRow(source.getProtocol(), source.getRequestRow(), source.isNeedRetake());
            }
            @Override protected void fill(final SessionTransferProtocolRow target, final SessionTransferProtocolRow source) { target.update(source, false); }

            @Override
            protected void doSaveRecord(SessionTransferProtocolRow protocolRow)
            {
                super.doSaveRecord(protocolRow);
                if (currentProtocolRows.contains(protocolRow)) return;

                List<SessionALRequestRowMark> rowMarks = requestMarksMap.get(protocolRow.getRequestRow().getId());
                if (CollectionUtils.isNotEmpty(rowMarks))
                {
                    for (SessionALRequestRowMark rowMark : rowMarks)
                    {
                        SessionTransferProtocolMark mark = new SessionTransferProtocolMark();
                        mark.setProtocolRow(protocolRow);
                        mark.setControlAction(rowMark.getControlAction());
                        mark.setMark(rowMark.getMark());
                        protocolMarks.add(mark);
                    }
                }
            }
        }.merge(
                getList(SessionTransferProtocolRow.class, SessionTransferProtocolRow.protocol().id(), protocol.getId()), protocolRows
        );

        new MergeAction.SessionMergeAction<INaturalId, SessionTransferProtocolMark>() {
            @Override protected INaturalId key(final SessionTransferProtocolMark source) { return source.getNaturalId(); }
            @Override protected SessionTransferProtocolMark buildRow(final SessionTransferProtocolMark source) { return new SessionTransferProtocolMark(source.getProtocolRow(), source.getControlAction()); }
            @Override protected void fill(final SessionTransferProtocolMark target, final SessionTransferProtocolMark source) { target.update(source, false); }
        }.merge(
                getList(SessionTransferProtocolMark.class, SessionTransferProtocolMark.protocolRow().protocol().id(), protocol.getId()), protocolMarks
        );
    }

    public void onDeleteWorkPlanVersion(SessionTransferProtocolDocument protocol)
    {
        NamedSyncInTransactionCheckLocker.register(getSession(), String.valueOf(protocol.getId()));
        if (null == protocol.getWorkPlanVersion()) return;

        SessionALRequest request = protocol.getRequest();
        EppStudent2EduPlanVersion s2epv = getByNaturalId(new EppStudent2EduPlanVersion.NaturalId(request.getStudent(), request.getBlock().getEduPlanVersion()));

        if (null == s2epv)
            throw new ApplicationException("Связь с учебный планом студента отсутствует.");

        EppStudent2WorkPlan s2wp = getByNaturalId(new EppStudent2WorkPlan.NaturalId(s2epv, protocol.getWorkPlan()));
        EppStudent2WorkPlan ps2wp = getByNaturalId(new EppStudent2WorkPlan.NaturalId(s2epv, protocol.getWorkPlanVersion()));

        if (null == s2wp || null == ps2wp)
            throw new ApplicationException("Связь с рабочим учебный планом студента отсутствует.");

        s2wp.setRemovalDate(null);
        update(s2wp);

        delete(ps2wp);
        delete(protocol.getWorkPlanVersion());

        List<Long> slotIds = new DQLSelectBuilder().fromEntity(SessionDocumentSlot.class, "slot").column(property("slot.id"))
                .where(eq(property("slot", SessionDocumentSlot.document().id()), value(protocol.getId())))
                .createStatement(getSession()).list();

        List<Long> marksIds = new DQLSelectBuilder().fromEntity(SessionMark.class, "mark").column(property("mark.id"))
                .where(in(property("mark", SessionMark.slot().id()), slotIds))
                .createStatement(getSession()).list();

        new DQLDeleteBuilder(SessionMark.class)
                .where(in(property(SessionMark.id()), marksIds))
                .createStatement(getSession()).execute();

        new DQLUpdateBuilder(SessionTransferProtocolMark.class)
                .set(SessionTransferProtocolMark.slot().s(), nul())
                .where(in(property(SessionTransferProtocolMark.protocolRow().id()),
                          new DQLSelectBuilder().fromEntity(SessionTransferProtocolRow.class, "pr").column(property("pr.id"))
                                  .where(eq(property("pr", SessionTransferProtocolRow.protocol().id()), value(protocol.getId())))
                                  .buildQuery()
                ))
                .createStatement(getSession()).execute();

        new DQLDeleteBuilder(SessionDocumentSlot.class)
                .where(in(property(SessionDocumentSlot.id()), slotIds))
                .createStatement(getSession()).execute();

        getSession().flush();
        getSession().clear();
    }

    @Override
    public void onCreateWorkPlanVersion(SessionTransferProtocolDocument protocol)
    {
        NamedSyncInTransactionCheckLocker.register(getSession(), String.valueOf(protocol.getId()));

        onDeleteWorkPlanVersion(protocol);

        SessionALRequest request = protocol.getRequest();
        EppWorkPlanBase wp = protocol.getWorkPlan();

        final EppStudent2EduPlanVersion s2epv = getByNaturalId(new EppStudent2EduPlanVersion.NaturalId(request.getStudent(), request.getBlock().getEduPlanVersion()));
        if (null == s2epv)
            throw new ApplicationException("Связь с учебный планом студента отсутствует.");

        final EppStudent2WorkPlan s2wp = getByNaturalId(new EppStudent2WorkPlan.NaturalId(s2epv, wp));
        if (null == s2wp)
            throw new ApplicationException("Связь с рабочим учебный планом студента отсутствует.");

        EppState state = getByCode(EppState.class, EppState.STATE_FORMATIVE);

        // создаем версию РУП
        EppWorkPlanVersion wpv = new EppWorkPlanVersion(wp.getWorkPlan());
        String nextNumber = INumberQueueDAO.instance.get().getNextNumber(wpv);
        wpv.setRegistrationNumber(wp.getWorkPlan().getNumber() + "." + nextNumber);
        wpv.setNumber(nextNumber + "*");

        // состояние формируется, чтобы можно было редактировать
        wpv.setState(state);
        wpv.setConfirmDate(new Date());
        save(wpv);

        protocol.setWorkPlanVersion(wpv);
        saveOrUpdate(protocol);

        // заполняем строки версии РУП(в) на основе строк протокола и привязанного РУПа
        doCreateWorkPlanVersionRows(protocol, wpv);

        // создаем новую связь РУПа со студентом, сразу делается неактуальной (чтобы не влиять на процесс до момента утверждения)
        final EppStudent2WorkPlan rel = new EppStudent2WorkPlan();
        rel.update(s2wp);
        rel.setWorkPlan(wpv);
        rel.setRemovalDate(wpv.getConfirmDate());
        save(rel);
        getSession().flush();

        // утверждение версии РУПа
        try (EventListenerLocker.Lock ignored = EppDSetUpdateCheckEventListener.LOCKER.lock(EventListenerLocker.noopHandler()))
        {
            rel.getWorkPlan().setState(getCatalogItem(EppState.class, EppState.STATE_ACCEPTED));  // выставляем сразу согласование
            getSession().flush();
        }

        Long studentEpvId = rel.getStudentEduPlanVersion().getId();
        Set<EppStudent2WorkPlan> student2WorkPlanSet = IEppWorkPlanDAO.instance.get().getStudentEpvWorkPlanMap(Collections.singleton(studentEpvId), true).get(studentEpvId);
        Map<Integer, EppWorkPlanBase> term2WorkPlanMap = new HashMap<>(student2WorkPlanSet.size());
        for (EppStudent2WorkPlan epv2wp : student2WorkPlanSet)
        {
            term2WorkPlanMap.put(epv2wp.getWorkPlan().getTerm().getIntValue(), epv2wp.getWorkPlan());
        }
        term2WorkPlanMap.put(rel.getWorkPlan().getTerm().getIntValue(), rel.getWorkPlan());

        // сохраняем итоговый набор РУП студента
        IEppWorkPlanDAO.instance.get().doUpdateStudentEpvWorkPlan(
                Collections.<Long, Set<EppWorkPlanBase>>singletonMap(studentEpvId, Sets.newHashSet(term2WorkPlanMap.values()))
        );
        IEppWorkPlanDAO.instance.get().doCleanUpWorkPlanTemporaryVersions(s2wp.getWorkPlan().getWorkPlan().getId());
    }

    private void doCreateWorkPlanVersionRows(SessionTransferProtocolDocument protocol, EppWorkPlanVersion wpv)
    {
        Set<Long> regElementIds = Sets.newHashSet();
        Map<Long, Map<Integer, Map<String, EppWorkPlanRowPartLoad>>> loadMap = Maps.newHashMap();
        EppWorkPlanRowKind kindMain = getByCode(EppWorkPlanRowKind.class, EppWorkPlanRowKindCodes.MAIN);

        List<SessionTransferProtocolRow> protocolRows = getList(SessionTransferProtocolRow.class, SessionTransferProtocolRow.protocol(), protocol);
        protocolRows.forEach(protocolRow -> regElementIds.add(protocolRow.getRequestRow().getRegElementPart().getRegistryElement().getId()));

        final List<EppWorkPlanRegistryElementRow> wpRowList = new DQLSelectBuilder().fromEntity(EppWorkPlanRegistryElementRow.class, "r")
                .where(eq(property("r", EppWorkPlanRegistryElementRow.workPlan()), value(protocol.getWorkPlan())))
                .where(notIn(property("r", EppWorkPlanRegistryElementRow.registryElementPart().registryElement().id()), regElementIds))
                .createStatement(getSession()).list();

        wpRowList.forEach(row -> regElementIds.add(row.getRegistryElement().getId()));

        Map<Long, IEppRegElWrapper> regElementDataMap = IEppRegistryDAO.instance.get().getRegistryElementDataMap(regElementIds);
        Map<Long, Map<Integer, Map<String, EppWorkPlanRowPartLoad>>> sourceLoadMap = IEppWorkPlanDataDAO.instance.get().getRowPartLoadDataMap(UniBaseDao.ids(wpRowList));

        // добавляем строки РУПа
        for (EppWorkPlanRegistryElementRow wpRow : wpRowList)
        {
            EppRegistryElementPart part = wpRow.getRegistryElementPart();
            IEppRegElPartWrapper partWrapper = regElementDataMap.get(part.getRegistryElement().getId()).getPartMap().get(part.getNumber());

            if (null != partWrapper)
            {
                EppWorkPlanRegistryElementRow cloneRow = new EppWorkPlanRegistryElementRow();
                cloneRow.update(wpRow);
                cloneRow.setWorkPlan(wpv);
                save(cloneRow);

                // нагрузка
                {
                    Map<Integer, Map<String, EppWorkPlanRowPartLoad>> map = getPartLoadDistributionMap(sourceLoadMap.get(wpRow.getId()), cloneRow);
                    if (null != map)
                        loadMap.put(cloneRow.getId(), map);
                }
            }
        }

        // добавляем строки протокола
        final Map<Integer, IEppWorkPlanPart> sourcePartMap = Maps.newLinkedHashMap();
        List<EppWorkPlanPart> wpPartList = getList(EppWorkPlanPart.class, EppWorkPlanPart.workPlan().id(), protocol.getWorkPlan().getId());
        wpPartList.forEach(part -> sourcePartMap.put(part.getNumber(), part));

        final EppWorkPlanPartLoadDistributor distributor = new EppWorkPlanPartLoadDistributor(sourcePartMap);

        for (SessionTransferProtocolRow protocolRow : protocolRows)
        {
            SessionALRequestRow requestRow = protocolRow.getRequestRow();
            EppRegistryElementPart part = requestRow.getRegElementPart();
            EppRegistryElement regElement = part.getRegistryElement();

            String index = requestRow.getRowTerm().getRow().getStoredIndex();
            final IEppRegElPartWrapper partWrapper = regElementDataMap.get(regElement.getId()).getPartMap().get(part.getNumber());

            if (null != partWrapper)
            {
                final EppWorkPlanRegistryElementRow row = new EppWorkPlanRegistryElementRow();

                row.setRegistryElementPart(requestRow.getRegElementPart());
                row.setWorkPlan(wpv);
                row.setNeedRetake(requestRow.isNeedRetake());
                row.setKind(kindMain);
                row.setNumber(index);
                row.setTitle(regElement.getTitle());
                save(row);

                // нагрузка
                {
                    Map<Integer, Map<String, EppWorkPlanRowPartLoad>> map = distributor.getPartLoadDistributionMap(row, partWrapper);
                    if (null != map)
                        loadMap.put(row.getId(), map);
                }
            }
        }
        IEppWorkPlanDataDAO.instance.get().doUpdateRowPartLoadElementsMap(loadMap);
        IEppWorkPlanDataDAO.instance.get().doUpdateRowPartLoadPeriodsMap(loadMap);
    }

    private Map<Integer, Map<String, EppWorkPlanRowPartLoad>> getPartLoadDistributionMap(Map<Integer, Map<String, EppWorkPlanRowPartLoad>> source, EppWorkPlanRegistryElementRow cloneRow)
    {
        if (null == source) return null;

        Map<Integer, Map<String, EppWorkPlanRowPartLoad>> result = Maps.newHashMap();
        source.entrySet().stream()
                .filter(plMap -> null != plMap.getValue())
                .forEach(plMap -> {
                    Map<String, EppWorkPlanRowPartLoad> cloneLoad = Maps.newHashMap();
                    plMap.getValue().entrySet().forEach(entry -> cloneLoad.put(entry.getKey(), new EppWorkPlanRowPartLoad(cloneRow, entry.getValue())));
                    result.put(plMap.getKey(), cloneLoad);
                });
        return result;
    }

    @Override
    public void onCreateSessionDocumentSlot(SessionTransferProtocolDocument protocol, SessionTransferProtocolRow row)
    {
        if (null == protocol.getWorkPlanVersion()) return;
        NamedSyncInTransactionCheckLocker.register(getSession(), String.valueOf(protocol.getId()));

        List<EppStudentWpeCAction> wpeCAs = Lists.newArrayList();
        Map<EppRegistryElementPart, List<EppGroupTypeFCA>> part2FCAMap = Maps.newHashMap();
        Set<EppRegistryElementPart> parts = Sets.newHashSet();
        List<SessionTransferProtocolRow> protocolRows = row != null ? Lists.newArrayList(row) : getList(SessionTransferProtocolRow.class, SessionTransferProtocolRow.protocol(), protocol);
        protocolRows.forEach(protocolRow -> parts.add(protocolRow.getRequestRow().getRegElementPart()));

        // Отбираем, созданные демоном, список МСРП-ФК
        List<EppStudentWpeCAction> wpeCAList = new DQLSelectBuilder().fromEntity(EppStudentWpeCAction.class, "a").column(property("a"))
                .joinPath(DQLJoinType.inner, EppStudentWpeCAction.studentWpe().fromAlias("a"), "wpe")
                .where(eq(property("wpe", EppStudentWorkPlanElement.student()), value(protocol.getRequest().getStudent())))
                .where(eq(property("wpe", EppStudentWorkPlanElement.year()), value(protocol.getWorkPlanVersion().getYear())))
                .where(eq(property("wpe", EppStudentWorkPlanElement.term()), value(protocol.getWorkPlanVersion().getTerm())))
                .where(in(property("wpe", EppStudentWorkPlanElement.registryElementPart()), parts))
                .createStatement(getSession()).list();

        List<SessionTransferProtocolMark> marks = new DQLSelectBuilder().fromEntity(SessionTransferProtocolMark.class, "m").column(property("m"))
                .where(eq(property("m", SessionTransferProtocolMark.protocolRow().protocol()), value(protocol)))
                .where(isNotNull(property("m", SessionTransferProtocolMark.mark())))
                .where(row == null ? null : eq(property("m", SessionTransferProtocolMark.protocolRow()), value(row)))
                .createStatement(getSession()).list();

        for (SessionTransferProtocolMark protocolMark : marks)
            SafeMap.safeGet(part2FCAMap, protocolMark.getProtocolRow().getRequestRow().getRegElementPart(), ArrayList.class).add(protocolMark.getControlAction().getEppGroupType());

        for (EppStudentWpeCAction wpeCA : wpeCAList)
        {
            EppGroupTypeFCA actionType = wpeCA.getType();
            EppRegistryElementPart part = wpeCA.getStudentWpe().getRegistryElementPart();

            List<EppGroupTypeFCA> markActions = part2FCAMap.get(part);
            if (!CollectionUtils.isEmpty(markActions) && markActions.contains(actionType))
            {
                wpeCAs.add(wpeCA);
            }
        }

        // Создаем записи студента по мероприятию, на основе МСРП-ФК
        for (EppStudentWpeCAction wpeCA : wpeCAs)
        {
            SessionDocumentSlot docSlot = getByNaturalId(new SessionDocumentSlot.NaturalId(protocol, wpeCA, true));
            if (null == docSlot)
            {
                docSlot = new SessionDocumentSlot(protocol, wpeCA, true);
                docSlot.setCommission(protocol.getCommission());
                save(docSlot);
            }
        }
        getSession().flush();

        // Отбираем оценки протокола, которые соответствуют, созданным слотам
        DQLSelectColumnNumerator dql = new DQLSelectColumnNumerator(
                new DQLSelectBuilder().fromEntity(SessionTransferProtocolMark.class, "m")
                        .joinPath(DQLJoinType.inner, SessionTransferProtocolMark.protocolRow().fromAlias("m"), "pr")
                        .joinEntity("pr", DQLJoinType.left, SessionDocumentSlot.class, "slot", eq(property("pr", SessionTransferProtocolRow.protocol()), property("slot", SessionDocumentSlot.document())))
                        .joinPath(DQLJoinType.inner, SessionDocumentSlot.studentWpeCAction().fromAlias("slot"), "wpeSlot")
                        .where(eq(property("pr", SessionTransferProtocolRow.protocol()), value(protocol)))
                        .where(eq(property("slot", SessionDocumentSlot.inSession()), value(Boolean.TRUE)))
                        .where(eq(property("wpeSlot", EppStudentWpeCAction.type()), property("m", SessionTransferProtocolMark.controlAction().eppGroupType())))
                        .where(eq(property("wpeSlot", EppStudentWpeCAction.studentWpe().year()), property("pr", SessionTransferProtocolRow.protocol().workPlanVersion().parent().year())))
                        .where(eq(property("wpeSlot", EppStudentWpeCAction.studentWpe().term()), property("pr", SessionTransferProtocolRow.protocol().workPlanVersion().parent().term())))
                        .where(eq(property("wpeSlot", EppStudentWpeCAction.studentWpe().registryElementPart()), property("pr", SessionTransferProtocolRow.requestRow().regElementPart())))
        );

        int mark_col = dql.column(property("m"));
        int doc_slot_col = dql.column(property("slot"));

        // привязываем слоты к оценкам протокола
        for (Object[] item : dql.getDql().createStatement(getSession()).<Object[]>list())
        {
            SessionTransferProtocolMark mark = (SessionTransferProtocolMark) item[mark_col];
            SessionDocumentSlot docSlot = (SessionDocumentSlot) item[doc_slot_col];

            mark.setSlot(docSlot);
            saveOrUpdate(mark);
        }

        List<SessionTransferProtocolMark> markSlots = new DQLSelectBuilder().fromEntity(SessionTransferProtocolMark.class, "m")
                .where(eq(property("m", SessionTransferProtocolMark.protocolRow().protocol()), value(protocol)))
                .where(isNotNull(property("m", SessionTransferProtocolMark.mark())))
                .where(isNotNull(property("m", SessionTransferProtocolMark.slot())))
                .where(row == null ? null : eq(property("m", SessionTransferProtocolMark.protocolRow()), value(row)))
                .createStatement(getSession()).list();

        // Выставляем оценки
        for (SessionTransferProtocolMark protocolMark : markSlots)
        {
            SessionMarkManager.instance().regularMarkDao().saveOrUpdateMark(protocolMark.getSlot(), new ISessionMarkDAO.MarkData()
            {
                @Override public Date getPerformDate() { return new Date(); }
                @Override public Double getPoints() { return null; }
                @Override public SessionMarkCatalogItem getMarkValue() { return protocolMark.getMark(); }
                @Override public String getComment() { return null; }
            });
        }
    }

    @Override
    public boolean isCheckProtocolRowMiss(SessionTransferProtocolDocument protocol)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SessionALRequestRow.class, "rr")
                .column(property("rr.id"))
                .joinEntity("rr", DQLJoinType.left, SessionTransferProtocolDocument.class, "p", eq(property("p", SessionTransferProtocolDocument.request().id()), property("rr", SessionALRequestRow.request().id())))
                .joinEntity("p", DQLJoinType.left, SessionTransferProtocolRow.class, "pr", and(
                        eq(property("pr", SessionTransferProtocolRow.protocol().id()), property("p.id")),
                        eq(property("pr", SessionTransferProtocolRow.requestRow().id()), property("rr.id"))
                ))
                .where(eq(property("rr", SessionALRequestRow.request().id()), value(protocol.getRequest().getId())))
                .where(eq(property("p.id"), value(protocol.getId())))
                .where(isNull(property("pr.id")));

        return !ISharedBaseDao.instance.get().existsEntityByCondition(SessionALRequestRow.class, "rr", in(property("rr.id"), builder.buildQuery()));
    }

    @Override
    public void onDeleteCustomEduPlan(Student student)
    {
        EppStudent2EduPlanVersion s2epv = IEppEduPlanDAO.instance.get().getActiveStudentEduPlanVersionRelation(student.getId());
        EppCustomEduPlan customEduPlan = s2epv.getCustomEduPlan();

        if (null != customEduPlan)
        {
            s2epv.setCustomEduPlan(null);
            saveOrUpdate(s2epv);

            boolean hasStudentWithCEP = ISharedBaseDao.instance.get().existsEntity(
                    new DQLSelectBuilder()
                            .fromEntity(EppStudent2EduPlanVersion.class, "c")
                            .where(eq(property("c", EppStudent2EduPlanVersion.customEduPlan()), value(customEduPlan)))
                            .where(ne(property("c", EppStudent2EduPlanVersion.id()), value(s2epv.getId())))
                            .buildQuery());

            if (!hasStudentWithCEP)
                delete(customEduPlan);
        }
    }

	@Override
	public <TransferOperation extends SessionTransferOperation> Map<TransferOperation, SessionProjectTheme> getThemesByTransferOperations(List<TransferOperation> operations)
	{
		Map<TransferOperation, SessionProjectTheme> operation2theme = new HashMap<>();
		for (TransferOperation operation : operations)
			operation2theme.put(operation, getByNaturalId(new SessionProjectTheme.NaturalId(operation.getTargetMark().getSlot())));
		return operation2theme;
	}
}