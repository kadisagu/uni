package ru.tandemservice.unisession.component.catalog.sessionMarkGradeValue.SessionMarkGradeValueAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.ContextLocal;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditController;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;

/**
 * @author iolshvang
 */

public class Controller extends DefaultCatalogAddEditController<SessionMarkGradeValueCatalogItem, Model, IDAO>
{

    @Override public void onRefreshComponent(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);
    }

    @Override public void onRenderComponent(final IBusinessComponent component) {
        ContextLocal.beginPageTitlePart(this.getModel(component).getCatalogItem().getId() == null ? "Добавление оценки" : "Редактирование оценки");
    }


}
