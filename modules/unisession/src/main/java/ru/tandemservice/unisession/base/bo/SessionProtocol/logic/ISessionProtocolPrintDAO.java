/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionProtocol.logic;


import org.tandemframework.rtf.document.RtfDocument;

import java.util.Collection;

/**
 * @author Andrey Andreev
 * @since 20.09.2016
 */
public interface ISessionProtocolPrintDAO
{

    RtfDocument printProtocols(Collection<Long> protocolIds);
}
