/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionTransfer.ui.RequestRowAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow;
import ru.tandemservice.unisession.base.bo.SessionReexamination.logic.SessionReexaminationRequestRowDSHandler;
import ru.tandemservice.unisession.entity.document.SessionTransferProtocolDocument;
import ru.tandemservice.unisession.entity.document.SessionTransferProtocolRow;
import ru.tandemservice.unisession.entity.mark.SessionALRequestRow;

import java.util.List;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 22.09.2015
 */
@Configuration
public class SessionTransferRequestRowAdd extends BusinessComponentManager
{
    public static final String REQUEST_ROW_DS = "requestRowDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(REQUEST_ROW_DS, requestRowDS(), requestRowDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint requestRowDS()
    {
        return columnListExtPointBuilder(REQUEST_ROW_DS)
                .addColumn(checkboxColumn("check"))
                .addColumn(textColumn(EppEpvTermDistributedRow.P_STORED_INDEX, SessionALRequestRow.rowTerm().row().storedIndex()).width("1px"))
                .addColumn(textColumn(DataWrapper.TITLE, DataWrapper.TITLE))
                .addColumn(textColumn(SessionALRequestRow.P_HOURS_AMOUNT, SessionALRequestRow.hoursAmount()).width("1px"))
                .addColumn(textColumn(SessionReexaminationRequestRowDSHandler.PROP_RE_EXAM_AND_ATTESTATION, SessionReexaminationRequestRowDSHandler.PROP_RE_EXAM_AND_ATTESTATION).width("1px"))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> requestRowDSHandler()
    {
        return new SessionReexaminationRequestRowDSHandler(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                DSOutput output = super.execute(input, context);

                SessionTransferProtocolDocument protocol = context.get(SessionTransferRequestRowAddUI.PARAM_PROTOCOL);

                List<Long> transferRequestRowIds = new DQLSelectBuilder().fromEntity(SessionTransferProtocolRow.class, "pr")
                        .column(property("pr", SessionTransferProtocolRow.requestRow().id()))
                        .where(eq(property("pr", SessionTransferProtocolRow.protocol()), value(protocol)))
                        .createStatement(context.getSession()).list();

                List<DataWrapper> resultList = DataWrapper.wrap(output).stream().filter(wrapper -> !transferRequestRowIds.contains(wrapper.getId())).collect(Collectors.toList());
                return ListOutputBuilder.get(input, resultList).build();
            }
        };
    }
}
