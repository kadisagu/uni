package ru.tandemservice.unisession.base.ext.Depersonalization;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.Depersonalization.DepersonalizationManager;
import org.tandemframework.shared.commonbase.base.bo.Depersonalization.logic.IDepersonalizationObject;
import org.tandemframework.shared.commonbase.base.bo.Depersonalization.logic.TemplateDocumentResetToDefaultObject;
import ru.tandemservice.unisession.attestation.entity.SessionAttBullletinPrintVersion;
import ru.tandemservice.unisession.entity.catalog.UnisessionTemplate;
import ru.tandemservice.unisession.entity.document.SessionDocumentPrintVersion;

/**
 * @author avedernikov
 * @since 05.10.2015
 */

@Configuration
public class DepersonalizationExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private DepersonalizationManager _depersonalizationManager;

    @Bean
    public ItemListExtension<IDepersonalizationObject> depersonalizationObjectListExtension()
    {
        return _depersonalizationManager.extensionBuilder()
                .add(new TemplateDocumentResetToDefaultObject(UnisessionTemplate.class))
                .add(_depersonalizationManager.multipleDropBuilder()
						.title("Печатные шаблоны закрытых документов сессии")
						.drop(SessionDocumentPrintVersion.class)
						.drop(SessionAttBullletinPrintVersion.class)
                        .optional(true)
						.build())
                .build();
    }
}
