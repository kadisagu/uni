/* $*/

package ru.tandemservice.unisession.dao.comission;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.hibernate.Session;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.util.DQLCanDeleteExpressionBuilder;

import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unisession.entity.comission.SessionComission;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;

/**
 * @author oleyba
 * @since 3/3/11
 */
public class SessionCommissionDAO extends UniBaseDao implements ISessionCommissionDAO
{

    @Override
    public SessionComission saveOrUpdateCommissionIds(SessionComission commission, Collection<Long> ppsListIds)
    {
        // должны быть уникальными (потому что кто-то уже вызывает с неуникальными ключами)
        ppsListIds = ((null == ppsListIds || ppsListIds.isEmpty()) ? Collections.<Long>emptyList() : new HashSet<Long>(ppsListIds));

        Map<Long, PpsEntry> ppsLoadCache = this.getLoadCacheMap(PpsEntry.class);
        if (null == commission)
        {
            // если комиссии нет - то мы ее создаем
            Session session = this.getSession();
            session.save(commission = new SessionComission());

            for (final Long ppsId : ppsListIds) {
                final SessionComissionPps rel = new SessionComissionPps();
                rel.setCommission(commission);
                rel.setPps(ppsLoadCache.get(ppsId));
                session.save(rel);
            }

            session.flush();
            return commission;
        }

        // а вот если комиссия есть - то придется менять
        Session session = this.getSession();
        NamedSyncInTransactionCheckLocker.register(session, String.valueOf(commission.getId()));

        final Map<Long, SessionComissionPps> existing = new HashMap<Long, SessionComissionPps>();
        for (final SessionComissionPps rel : this.getList(SessionComissionPps.class, SessionComissionPps.commission().s(), commission)) {
            existing.put(rel.getPps().getId(), rel);
        }

        for (final Long ppsId : ppsListIds) {
            if (null == existing.remove(ppsId)) {
                final SessionComissionPps rel = new SessionComissionPps();
                rel.setCommission(commission);
                rel.setPps(ppsLoadCache.get(ppsId));
                session.save(rel);
            }
        }

        if (existing.values().size() > 0) {
            for (final SessionComissionPps rel : existing.values()) {
                session.delete(rel);
            }
            // это надо делать сразу после удаления (иначе будет duplicate, если кто-то потом захочет пересоздать)
            session.flush();
        }

        return commission;
    }

    @Override
    public SessionComission saveOrUpdateCommission(SessionComission commission, Collection<PpsEntry> ppsList) {
        return saveOrUpdateCommissionIds(commission, ids(ppsList));
    }

    public static final SyncDaemon DAEMON = new SyncDaemon(SessionCommissionDAO.class.getName(), 120, ISessionCommissionDAO.CLEANUP_LOCKER_NAME)
    {
        @Override protected void main() {
            ISessionCommissionDAO.instance.get().doCleanUp();
            // TODO: merge
        }
    };

    @Override
    public void doCleanUp()
    {
        final DQLCanDeleteExpressionBuilder cde = new DQLCanDeleteExpressionBuilder(SessionComission.class, "id");
        final DQLDeleteBuilder db = new DQLDeleteBuilder(SessionComission.class);
        db.where(cde.getExpression());
        db.createStatement(this.getSession()).execute();
    }
}
