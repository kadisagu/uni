package ru.tandemservice.unisession.entity.document;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniStudent.logic.sessionTransferProtocol.ISessionTransferProtocolProperty;
import ru.tandemservice.unisession.entity.document.gen.*;
import ru.tandemservice.unisession.sec.ISessionSecDao;

import java.util.Collection;

/** @see ru.tandemservice.unisession.entity.document.gen.SessionTransferProtocolDocumentGen */
public class SessionTransferProtocolDocument extends SessionTransferProtocolDocumentGen implements ISessionTransferProtocolProperty
{
    /**
     * @deprecated use {@link SessionTransferProtocolDocumentGen#isApproved()}
     */
    @Deprecated
    public boolean isApprove() { return this.isApproved(); }

    @Override
    public String getTitle()
    {
        return getNumber();
    }

    @Override
    @EntityDSLSupport(parts=SessionDocument.P_NUMBER)
    public String getTypeTitle()
    {
        return "Протокол №" + getNumber();
    }

    @Override
    @EntityDSLSupport(parts=SessionDocument.P_NUMBER)
    public String getTypeShortTitle()
    {
        return "Протокол №" + getNumber();
    }

    @Override
    public Collection<IEntity> getSecLocalEntities()
    {
        return ISessionSecDao.instance.get().getSecLocalEntities(this);
    }

    @Override
    public OrgUnit getGroupOu()
    {
        return getRequest().getStudent().getEducationOrgUnit().getFormativeOrgUnit();
    }
}