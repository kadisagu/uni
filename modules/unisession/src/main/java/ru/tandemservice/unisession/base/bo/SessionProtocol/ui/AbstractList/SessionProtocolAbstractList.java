/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionProtocol.ui.AbstractList;

import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.IColumnListExtPointBuilder;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.util.Icon;
import ru.tandemservice.unisession.entity.stateFinalExam.SessionFQWProtocol;
import ru.tandemservice.unisession.entity.stateFinalExam.SessionStateFinalExamProtocol;


/**
 * @author Andrey Andreev
 * @since 12.09.2016
 */
public abstract class SessionProtocolAbstractList extends BusinessComponentManager
{
    public static final String BULLETIN_ID_PARAM = "bulletinId";
    public static final String STATE_FINAL_EXAM_PROTOCOL_DS = "protocolListDS";

    public static final String SEARCH_LIST_COLUMN_SELECT = "select";
    public static final String SEARCH_LIST_COLUMN_NUMBER = "number";
    public static final String SEARCH_LIST_COLUMN_STUDENT = "student";
    public static final String SEARCH_LIST_COLUMN_GEC = "govExamComm";
    public static final String SEARCH_LIST_COLUMN_COMMENT = "comment";

    public void addDefaultProtocolListColumns(IColumnListExtPointBuilder builder)
    {
        builder.addColumn(checkboxColumn(SEARCH_LIST_COLUMN_SELECT))
                .addColumn(textColumn(SEARCH_LIST_COLUMN_NUMBER, SessionStateFinalExamProtocol.number().s()).order())
                .addColumn(textColumn(SEARCH_LIST_COLUMN_STUDENT, SessionStateFinalExamProtocol.documentSlot().studentWpeCAction().studentWpe().student().fullFio().s()).order())
                .addColumn(blockColumn(SEARCH_LIST_COLUMN_GEC));

        customProtocolListColumns(builder);

        builder.addColumn(textColumn(SEARCH_LIST_COLUMN_COMMENT, SessionFQWProtocol.comment().s()))
                .addColumn(actionColumn("print", new Icon("print"), "onClickPrint").permissionKey(getPrintPermissionKey()))
                .addColumn(actionColumn("delete", new Icon("delete"), "onClickDelete").permissionKey(getDeletePermissionKey())
                                   .alert(FormattedMessage.with().template("protocolListDS.delete.alert")
                                                  .parameter(SessionStateFinalExamProtocol.documentSlot().studentWpeCAction().studentWpe().student().fullFio().s())
                                                  .create()));
    }

    public void customProtocolListColumns(IColumnListExtPointBuilder builder)
    {

    }

    public abstract String getPrintPermissionKey();

    public abstract String getDeletePermissionKey();
}