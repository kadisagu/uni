/* $Id:$ */
package ru.tandemservice.unisession.base.ext.Common;

import org.apache.commons.collections15.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.ModuleStatusReportBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.util.IModuleStatusReporter;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unisession.entity.document.SessionGlobalDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLFunctions.min;

/**
 * @author oleyba
 * @since 2/11/14
 */
@Configuration
public class CommonExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private CommonManager _commonManager;

    @Bean
    public ItemListExtension<IModuleStatusReporter> moduleStatusExtPoint()
    {
        return itemListExtension(_commonManager.moduleStatusExtPoint())
            .add("unisession", () -> {
                final ICommonDAO dao = DataAccessServices.dao();
                List<String> result = new ArrayList<>();

                String alias = "e";

                Map<Long, OrgUnit> ouCache = SafeMap.get(key -> dao.get(OrgUnit.class, key));

                final DQLSelectBuilder docCountMinYearQuery = new DQLSelectBuilder().fromEntity(SessionMark.class, alias)
                    .column(property(alias, SessionMark.slot().document().id()), "doc")
                    .column(property(alias, SessionMark.slot().studentWpeCAction().studentWpe().student().educationOrgUnit().groupOrgUnit().id()), "ou")
                    .column(min(property(alias, SessionMark.slot().studentWpeCAction().studentWpe().year().educationYear().intValue())), "year")
                    .group(property(alias, SessionMark.slot().document().id()))
                    .group(property(alias, SessionMark.slot().studentWpeCAction().studentWpe().student().educationOrgUnit().groupOrgUnit().id()))
                    .distinct();
                List<Object[]> docCountList = dao.getList(docCountMinYearQuery);

                Map<String, Map<String, Set<Long>>> docCountMinYearMap = new TreeMap<>();

                for (Object[] objects : docCountList) {
                    final Long groupOrgUnitId = (Long)objects[1];
                    String yearOuKey = " • " + objects[2] + ", " + (null == groupOrgUnitId ? "Деканат не указан" : ouCache.get(groupOrgUnitId).getTitle());
                    Long document = ((Long) objects[0]);
                    String docClass = EntityRuntime.getMeta(document).getTitle();
                    final Map<String, Set<Long>> stringSetMap = SafeMap.safeGet(docCountMinYearMap, yearOuKey, TreeMap.class);
                    SafeMap.safeGet(stringSetMap, docClass, HashSet.class).add(document);
                }


                final List<Object[]> marksCountInJournalList = dao.getList(new DQLSelectBuilder().fromDataSource(
                    new DQLSelectBuilder()
                        .fromEntity(SessionGlobalDocument.class, alias)
                        .fromEntity(SessionMark.class, "m")
                        .where(eq(property("m", SessionMark.slot().document()), property(alias)))
                        .column(property("m", SessionMark.slot().studentWpeCAction().studentWpe().year().educationYear().intValue()), "year")
                        .column(property("m", SessionMark.slot().studentWpeCAction().studentWpe().student().educationOrgUnit().groupOrgUnit().title()), "ou")
                        .column(DQLFunctions.count(property("m", SessionMark.id())), "count")
                        .group(property("m", SessionMark.slot().studentWpeCAction().studentWpe().year().educationYear().intValue()))
                        .group(property("m", SessionMark.slot().studentWpeCAction().studentWpe().student().educationOrgUnit().groupOrgUnit().title()))
                        .distinct()
                        .buildQuery()
                    , "f")
                    .column(property("f.year"))
                    .column(property("f.ou"))
                    .column(property("f.count"))
                    .order(property("f.year"))
                    .order(property("f.ou")));



                result.add("Число документов (с оценками):");
                for (Map.Entry<String, Map<String, Set<Long>>> stringMapEntry : docCountMinYearMap.entrySet()) {
                    for (Map.Entry<String, Set<Long>> stringSetEntry : stringMapEntry.getValue().entrySet()) {
                        result.add(stringMapEntry.getKey() + ", " + stringSetEntry.getKey() + " - " + stringSetEntry.getValue().size());
                    }
                }

                result.add("Число оценок в семестровом журнале:");
                result.addAll(CollectionUtils.collect(marksCountInJournalList, ModuleStatusReportBuilder.QUERY_RESULT_TRANSFORMER));
                return result;
            })
            .create();
    }
}
