package ru.tandemservice.unisession.attestation.entity.report;

import ru.tandemservice.uni.IStorableReport;
import ru.tandemservice.unisession.attestation.entity.report.gen.SessionAttestationBulletinReportGen;

/**
 * Отчет «Аттестационная ведомость по дисциплине (на академ. группу)»
 */
public class SessionAttestationBulletinReport extends SessionAttestationBulletinReportGen implements IStorableReport
{
}