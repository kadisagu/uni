/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReexamination.ui.Pub;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.unisession.base.bo.SessionReexamination.SessionReexaminationManager;
import ru.tandemservice.unisession.base.bo.SessionReexamination.logic.SessionReexaminationRequestRowDSHandler;
import ru.tandemservice.unisession.base.bo.SessionReexamination.ui.AddEdit.SessionReexaminationAddEdit;
import ru.tandemservice.unisession.base.bo.SessionReexamination.ui.AddEdit.SessionReexaminationAddEditUI;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.mark.SessionALRequest;
import ru.tandemservice.unisession.entity.mark.SessionALRequestRow;

import java.util.Map;

/**
 * @author Alexey Lopatin
 * @since 31.08.2015
 */
@State({@Bind(key = UIPresenter.PUBLISHER_ID, binding = "request.id", required = true)})
public class SessionReexaminationPubUI extends UIPresenter
{
    public static final String PARAM_REQUEST = "request";

    private SessionALRequest _request = new SessionALRequest();

    @Override
    public void onComponentRefresh()
    {
        _request = DataAccessServices.dao().getNotNull(_request.getId());

        BaseSearchListDataSource requestRowDS = getConfig().getDataSource(SessionReexaminationPub.REQUEST_ROW_DS);
        SessionReexaminationManager.instance().dao().doCreateControlActionColumns(_request, requestRowDS);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (SessionReexaminationPub.REQUEST_ROW_DS.equals(dataSource.getName()))
        {
            dataSource.put(PARAM_REQUEST, _request);
        }
    }

    public void onClickEdit()
    {
        _uiActivation.asRegion(SessionReexaminationAddEdit.class)
                .parameter(SessionReexaminationAddEditUI.PARAM_STUDENT_ID, getRequest().getStudent().getId())
                .parameter(SessionReexaminationAddEditUI.PARAM_REQUEST_ID, getRequest().getId())
                .top().activate();
    }

    public void onClickDelete()
    {
        DataAccessServices.dao().delete(_request);
        deactivate();
    }

    public void onClickPrint()
    {
        RtfDocument document = SessionReexaminationManager.instance().sessionALRequestPrintDao().printRequest(_request.getId());
        String filename = "Заявление от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(_request.getRequestDate())+".rtf";

        if (document != null)
            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().document(document).fileName(filename).rtf(), false);
    }

    @SuppressWarnings("unchecked")
    public boolean isHasRegElementPartFControlAction()
    {
        BaseSearchListDataSource requestRowDS = getConfig().getDataSource(SessionReexaminationPub.REQUEST_ROW_DS);
        DataWrapper wrapper = requestRowDS.getCurrent();
        String code = requestRowDS.getCurrentColumn().getName();

        Map<String, Boolean> hasRegElementPartFCAMap = (Map<String, Boolean>) wrapper.getProperty(SessionReexaminationRequestRowDSHandler.PROP_PART_TO_FCA_MAP);
        return hasRegElementPartFCAMap.get(code);
    }

    @SuppressWarnings("unchecked")
    public SessionMarkGradeValueCatalogItem getCurrentRequestRowMark()
    {
        BaseSearchListDataSource requestRowDS = getConfig().getDataSource(SessionReexaminationPub.REQUEST_ROW_DS);
        DataWrapper wrapper = requestRowDS.getCurrent();
        String code = requestRowDS.getCurrentColumn().getName();

        Map<String, SessionMarkGradeValueCatalogItem> reExamMarkMap = (Map<String, SessionMarkGradeValueCatalogItem>) wrapper.getProperty(SessionReexaminationRequestRowDSHandler.PROP_RE_EXAM_MARK_MAP);
        return reExamMarkMap.get(code);
    }

    public SessionALRequestRow getCurrentRequestRow()
    {
        BaseSearchListDataSource requestRowDS = getConfig().getDataSource(SessionReexaminationPub.REQUEST_ROW_DS);
        return ((DataWrapper) requestRowDS.getCurrent()).getWrapped();
    }

    public String getSticker()
    {
        return getConfig().getProperty("ui.sticker") + " " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getRequest().getRequestDate());
    }

    public SessionALRequest getRequest()
    {
        return _request;
    }

    public void setRequest(SessionALRequest request)
    {
        _request = request;
    }
}
