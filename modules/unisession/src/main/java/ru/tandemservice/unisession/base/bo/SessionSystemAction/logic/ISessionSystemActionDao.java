/**
 *$Id$
 */
package ru.tandemservice.unisession.base.bo.SessionSystemAction.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;

/**
 * @author Alexander Shaburov
 * @since 12.02.13
 */
public interface ISessionSystemActionDao extends INeedPersistenceSupport
{
    void doRefreshFinalMarks();
}
