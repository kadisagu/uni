package ru.tandemservice.unisession.component.sessionListDocument.SessionListDocumentAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;

/**
 * @author iolshvang
 * @since 12.04.2011
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{

    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = getModel(component);
        getDao().prepare(model);

        final StaticListDataSource<TermWithActionSlot> ds = model.getEppModel().getEppDataSource();
        ds.clearColumns();
        prepareEppDataSourceColumns(ds);
        if (model.getEppModel().getEppDataSource().getCountRow() == 0)
            addRow(model);
    }

    protected void prepareEppDataSourceColumns(final StaticListDataSource<TermWithActionSlot> ds)
    {
        ds.addColumn(new BlockColumn<>("term", "Семестр", "termColumnBlock"));
        ds.addColumn(new BlockColumn<>("epp", "Мероприятия", "eppColumnBlock"));
        ds.addColumn(new BlockColumn<>("actions", "", "actionsColumnBlock").setWidth(1));
    }

    @Override
    public void onRenderComponent(final IBusinessComponent component)
    {
        ContextLocal.beginPageTitlePart(getModel(component).isAddForm() ? "Добавление экзаменационной карточки" : "Редактирование перечня мероприятий");
    }

    public void onClickApply(final IBusinessComponent component) {
        final Model model = getModel(component);
        getDao().update(model);
        deactivate(component);
    }

    private void edit(final Model model, final Long id)
    {
        final TermWithActionSlot row = model.getEppModel().findRow(id);
        model.getEppModel().setEditRow(row);
    }

    private void addRow(final Model model)
    {
        try
        {
            if (null != model.getEppModel().getEditRowId())
            {
                return;
            }
            final TermWithActionSlot row = new TermWithActionSlot();
            model.setCurrentId(row.getId());

            edit(model, model.getEppModel().addRow(row));
        }
        finally
        {
            model.getEppModel().getEppDataSource().setLastVisitedEntityId(null);
        }
    }

    public void onClickAddEpp(final IBusinessComponent component)
    {
        final Model model = getModel(component);
        onCurrentRowClear(model);
        addRow(model);
    }

    public void onClickEditEpp(final IBusinessComponent component)
    {
        final Model model = getModel(component);
        try {
            model.getEppModel().setEditRow(null);
            edit(model, component.<Long>getListenerParameter());

            TermWithActionSlot row = (TermWithActionSlot) model.getEppModel().getEditRow();
            model.setCurrentId(row.getId());
            model.setCurrentTerm(row.getTerm());
            model.setCurrentEppSlotList(row.getEppSlotList());
        }
        finally
        {
            model.getEppModel().getEppDataSource().setLastVisitedEntityId(null);
        }

    }

    public void onClickChangeTerm(IBusinessComponent component)
    {
        Model model = getModel(component);

        if (null != model.getCurrentId() && model.getCurrentEntity().getId().equals(model.getCurrentId()))
            model.getCurrentEntity().setTerm(model.getCurrentTerm());
    }

    public void onClickChangeEppSlotList(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.getCurrentEntity().setEppSlotList(model.getCurrentEppSlotList());
    }

    public void onClickDeleteEpp(final IBusinessComponent component)
    {
        final Model model = getModel(component);
        try
        {
            model.getEppModel().setEditRow(null);
            model.getEppModel().delete(component.<Long>getListenerParameter());
        }
        finally
        {
            model.getEppModel().getEppDataSource().setLastVisitedEntityId(null);
        }
    }

    public void onClickSaveEpp(final IBusinessComponent component)
    {
        final Model model = getModel(component);
        try
        {
            model.getEppModel().setEditRow(null);
        }
        finally
        {
            model.getEppModel().getEppDataSource().setLastVisitedEntityId(null);
            onCurrentRowClear(model);
        }
    }

    public void onChangeStudent(final IBusinessComponent component)
    {
        final Model model = getModel(component);
        final StaticListDataSource<TermWithActionSlot> ds = model.getEppModel().getEppDataSource();
        if (ds.getCountRow() == 0)
            return;

        ds.clearColumns();
        prepareEppDataSourceColumns(ds);
        addRow(model);
    }

    public void onChangeReason(final IBusinessComponent component)
    {
        getDao().checkWpeCAction(getModel(component));
    }

    public void onCurrentRowClear(Model model)
    {
        model.setCurrentId(null);
        model.setCurrentTerm(null);
        model.setCurrentEppSlotList(null);
    }
}
