/* $Id$ */
package ru.tandemservice.unisession.attestation.bo.CurrentMarkScale.ui.CatalogItemPub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.util.Icon;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDefine;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unisession.entity.catalog.SessionAttestationMarkCatalogItem;

/**
 * @author Nikolay Fedorovskih
 * @since 07.10.2014
 */
@Configuration
public class CurrentMarkScaleCatalogItemPub extends BusinessComponentManager
{
    public static final String MARK_DS = "markDS";
    public static final String MARK_SCALE_PARAM = "scale";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(MARK_DS, markDS(), markDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint markDS()
    {
        return this.columnListExtPointBuilder(MARK_DS)
                .addColumn(textColumn("title", SessionAttestationMarkCatalogItem.P_TITLE))
                .addColumn(textColumn("shortTitle", SessionAttestationMarkCatalogItem.P_SHORT_TITLE))
                .addColumn(textColumn("printTitle", SessionAttestationMarkCatalogItem.P_PRINT_TITLE))
                .addColumn(toggleColumn("positive", SessionAttestationMarkCatalogItem.P_POSITIVE)
                                   .toggleOnListener("onChangePositive").toggleOffListener("onChangePositive"))
                .addColumn(actionColumn("toTop", new Icon(CommonBaseDefine.ICO_VERY_UP), "onClickToTop"))
                .addColumn(actionColumn("up", CommonDefines.ICON_UP, "onClickUp"))
                .addColumn(actionColumn("down", CommonDefines.ICON_DOWN, "onClickDown"))
                .addColumn(actionColumn("toBottom", new Icon(CommonBaseDefine.ICO_VERY_DOWN), "onClickToBottom"))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).permissionKey("ui:sec.edit"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER).disabled("ui:system").permissionKey("ui:sec.delete")
                                   .alert(FormattedMessage.with().template("delete.alert").parameter(SessionAttestationMarkCatalogItem.P_TITLE).create()))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> markDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), SessionAttestationMarkCatalogItem.class)
                .where(SessionAttestationMarkCatalogItem.scale(), MARK_SCALE_PARAM)
                .order(SessionAttestationMarkCatalogItem.priority())
                .filter(SessionAttestationMarkCatalogItem.title());
    }
}