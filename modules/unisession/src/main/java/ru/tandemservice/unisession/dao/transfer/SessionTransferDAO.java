/* $Id$ */
package ru.tandemservice.unisession.dao.transfer;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.util.ClassUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.INumberGenerationRule;
import org.tandemframework.shared.commonbase.base.util.SimpleNumberGenerationRule;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unisession.entity.document.SessionTransferDocument;

/**
 * @author oleyba
 * @since 5/3/11
 */
public class SessionTransferDAO extends UniBaseDao implements ISessionTransferDAO
{
    @Override
    public INumberGenerationRule<SessionTransferDocument> getNumberGenerationRule()
    {
        // реализована сквозная (по всму вузу) нумерация хвостовок в рамках года
        return new SimpleNumberGenerationRule<SessionTransferDocument>()
        {
            @Override
            public Set<String> getUsedNumbers(final SessionTransferDocument sheet)
            {
                final Calendar formingDate = Calendar.getInstance();
                formingDate.setTime(sheet.getFormingDate());
                final int year = formingDate.get(Calendar.YEAR);

                final Calendar beginYear = Calendar.getInstance();
                beginYear.set(year, Calendar.JANUARY, 1, 0, 0, 0);
                beginYear.set(Calendar.MILLISECOND, 0);

                final Calendar endYear = Calendar.getInstance();
                endYear.set(year, Calendar.DECEMBER, 31, 23, 59, 59);
                endYear.set(Calendar.MILLISECOND, 999);


                final List<String> list = new DQLSelectBuilder()
                .fromEntity(ClassUtils.getUserClass(sheet), "sheet")
                .column(property(SessionTransferDocument.number().fromAlias("sheet")))
                .where(ge(DQLExpressions.property(SessionTransferDocument.formingDate().fromAlias("sheet")), valueDate(beginYear.getTime())))
                .where(le(DQLExpressions.property(SessionTransferDocument.formingDate().fromAlias("sheet")), valueDate(endYear.getTime())))
                .where(eq(DQLExpressions.property(SessionTransferDocument.orgUnit().fromAlias("sheet")), value(sheet.getOrgUnit())))
                .predicate(DQLPredicateType.distinct)
                .createStatement(SessionTransferDAO.this.getSession()).list();
                return new HashSet<String>(list);
            }

            @Override
            public String getNumberQueueName(final SessionTransferDocument sheet)
            {
                final Calendar formingDate = Calendar.getInstance();
                formingDate.setTime(sheet.getFormingDate());
                final int year = formingDate.get(Calendar.YEAR);

                return ClassUtils.getUserClass(sheet).getSimpleName() + "." + String.valueOf(year) + "." + sheet.getOrgUnit().getId();
            }
        };
    }
}

