/* $Id$ */
package ru.tandemservice.unisession.attestation.bo.AttestationBulletin.ui.Add;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeALT;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.unisession.attestation.bo.AttestationBulletin.AttestationBulletinManager;
import ru.tandemservice.unisession.attestation.bo.AttestationBulletin.ui.List.AttestationBulletinListUI;
import ru.tandemservice.unisession.attestation.entity.SessionAttestation;
import ru.tandemservice.unisession.base.bo.SessionDocument.util.SessionRealEduGroupFilterAddon;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;

import java.util.Collection;

import static ru.tandemservice.unisession.attestation.bo.Attestation.AttestationManager.*;

/**
 * @author Alexey Lopatin
 * @since 26.11.2015
 */
@State({
        @Bind(key = SessionReportManager.BIND_ORG_UNIT, binding = "holder.id", required = true),
        @Bind(key = AttestationBulletinListUI.BIND_ATTESTATION, binding = "attestation", required = true),
})
public class AttestationBulletinAddUI extends UIPresenter
{
    public static final String PARAM_ATTESTATION = "attestation";

    private OrgUnitHolder _holder = new OrgUnitHolder();
    private EducationYear _eduYear;
    private YearDistributionPart _yearPart;
    private SessionAttestation _attestation;

    @Override
    public void onComponentRefresh()
    {
        getHolder().refresh();
        _eduYear = getAttestation().getSessionObject().getEducationYear();
        _yearPart = getAttestation().getSessionObject().getYearDistributionPart();
        configUtil(getFilterAddon());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(SessionReportManager.PARAM_ORG_UNIT, getOrgUnit());
        dataSource.put(SessionReportManager.PARAM_EDU_YEAR, getEduYear());

        dataSource.put(PARAM_ORG_UNIT, getOrgUnit());
        dataSource.put(PARAM_EDU_YEAR, getEduYear());
        dataSource.put(PARAM_YEAR_PART, getYearPart());
        dataSource.put(PARAM_ATTESTATION, getAttestation());
        dataSource.put(SessionRealEduGroupFilterAddon.UTIL_NAME, getFilterAddon());
    }

    public void onChangeYearOrPart()
    {
        configUtilWhere(getFilterAddon());
    }

    private void configUtil(SessionRealEduGroupFilterAddon util)
    {
        util
                .configDoubleWidthFilters(false)
                .configSettings(getSettingsKey());

        util.clearFilterItems();

        ICommonFilterFactory<IEntity> aLoadType = config -> new SessionRealEduGroupFilterAddon.LoadTypeFilterItem(config, "Вид аудиторной нагрузки", EppGroupTypeALT.class);

        util
                .addFilterItem(SessionRealEduGroupFilterAddon.DEVELOP_FORM, CommonFilterFormConfig.DEFAULT_SELECT_FILTER_CONFIG)
                .addFilterItem(SessionRealEduGroupFilterAddon.DEVELOP_CONDITION, CommonFilterFormConfig.DEFAULT_SELECT_FILTER_CONFIG)
                .addFilterItem(SessionRealEduGroupFilterAddon.EDU_LEVEL, CommonFilterFormConfig.DEFAULT_SELECT_FILTER_CONFIG)
                .addFilterItem(SessionRealEduGroupFilterAddon.TERRITORIAL_ORG_UNIT, CommonFilterFormConfig.DEFAULT_SELECT_FILTER_CONFIG)
                .addFilterItem(SessionRealEduGroupFilterAddon.FORMATIVE_ORG_UNIT, CommonFilterFormConfig.DEFAULT_SELECT_FILTER_CONFIG)
                .addFilterItem(SessionRealEduGroupFilterAddon.COURSE, CommonFilterFormConfig.DEFAULT_SELECT_FILTER_CONFIG)
                .addFilterItem(SessionRealEduGroupFilterAddon.GROUP, CommonFilterFormConfig.DEFAULT_SELECT_FILTER_CONFIG)
                .addFilterItem(SessionRealEduGroupFilterAddon.DISCIPLINE, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(aLoadType, CommonFilterFormConfig.DEFAULT_SELECT_FILTER_CONFIG);

        util.getFilterItem(SessionRealEduGroupFilterAddon.SETTINGS_NAME_DEVELOP_FORM).setNextWithNewLine(false);
        util.getFilterItem(SessionRealEduGroupFilterAddon.SETTINGS_NAME_EDU_LEVEL).setDoubleWidth(true).setColSpan(2);
        util.getFilterItem(SessionRealEduGroupFilterAddon.SETTINGS_NAME_TERRITORIAL_ORG_UNIT).setDoubleWidth(true).setColSpan(2);
        util.getFilterItem(SessionRealEduGroupFilterAddon.SETTINGS_NAME_FORMATIVE_ORG_UNIT).setDoubleWidth(true).setColSpan(2);
        util.getFilterItem(SessionRealEduGroupFilterAddon.SETTINGS_NAME_COURSE).setNextWithNewLine(false);
        util.getFilterItem(SessionRealEduGroupFilterAddon.SETTINGS_NAME_DISCIPLINE).setDoubleWidth(true).setColSpan(2);

        configUtilWhere(util);
    }

    public void configUtilWhere(SessionRealEduGroupFilterAddon util)
    {
        util.clearWhereFilter();
        util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EppRealEduGroupRow.studentWpePart().studentWpe().student().status().active(), true));

        if (null != getOrgUnit())
			util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EppRealEduGroupRow.studentWpePart().studentWpe().student().educationOrgUnit().groupOrgUnit(), getOrgUnit()));
        if (null != _eduYear)
			util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EppRealEduGroupRow.group().summary().yearPart().year().educationYear(), _eduYear));
        if (null != _yearPart)
			util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EppRealEduGroupRow.group().summary().yearPart().part(), _yearPart));
    }

    public SessionRealEduGroupFilterAddon getFilterAddon()
    {
        return ((SessionRealEduGroupFilterAddon) getConfig().getAddon(CommonFilterAddon.class.getSimpleName()))
                .customFilter((dql, alias, filter, presenter) ->
                {
                    SessionAttestation attestation = ((AttestationBulletinAddUI) presenter).getAttestation();
                    AttestationBulletinManager.instance().dao().filterNotExistsAlreadyCreatedBulletin(alias, dql, attestation);
                });
    }

    public void onClickShow()
    {
        configUtilWhere(getFilterAddon());
    }

    public void onClickClear()
    {
        getFilterAddon().clearSettings();
    }

    public void onClickApply()
    {
        Collection<IEntity> selected = getConfig().<BaseSearchListDataSource>getDataSource(AttestationBulletinAdd.DS_EDU_GROUP).getOptionColumnSelectedObjects("check");
        if (selected.isEmpty())
            throw new ApplicationException("Необходимо выбрать хотя бы одну УГС из списка.");

        AttestationBulletinManager.instance().dao().doGenerateBulletins(getAttestation(), UniBaseDao.ids(selected));
        selected.clear();
    }

    public String getSticker()
    {
        return getConfig().getProperty("ui.sticker", getOrgUnit().getFullTitle());
    }

	public String getCheckBoxHint()
	{
		return getConfig().getProperty("ui.checkBoxHint");
	}

    public OrgUnit getOrgUnit()
    {
        return getHolder().getValue();
    }

    // Getters & Setters

    public OrgUnitHolder getHolder()
    {
        return _holder;
    }

    public EducationYear getEduYear()
    {
        return _eduYear;
    }

    public void setEduYear(EducationYear eduYear)
    {
        _eduYear = eduYear;
    }

    public YearDistributionPart getYearPart()
    {
        return _yearPart;
    }

    public void setYearPart(YearDistributionPart yearPart)
    {
        _yearPart = yearPart;
    }

    public SessionAttestation getAttestation()
    {
        return _attestation;
    }

    public void setAttestation(SessionAttestation attestation)
    {
        _attestation = attestation;
    }
}
