/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionProtocol.logic;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.bo.Person.logic.declination.IDeclinationDao;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unisession.entity.catalog.SessionRecommendation4FQW;
import ru.tandemservice.unisession.entity.catalog.UnisessionCommonTemplate;
import ru.tandemservice.unisession.entity.catalog.codes.SessionRoleInGECCodes;
import ru.tandemservice.unisession.entity.catalog.codes.UnisessionCommonTemplateCodes;
import ru.tandemservice.unisession.entity.document.SessionProjectTheme;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.stateFinalExam.SessionFQWProtocol;
import ru.tandemservice.unisession.entity.stateFinalExam.SessionStateFinalExamProtocol;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Andreev
 * @since 21.09.2016
 */
public class SessionProtocolFinalQualWorkPrintDAO extends AbstractSessionProtocolPrintDAO
{
    protected static final String EMPTY_LINE_MEMBERS = "__________" + "__________"
            + "__________" + "__________" + "__________" + "__________" + "______";

    protected static final String EMPTY_LINE_THEME = "__________" + "__________" + "__________"
            + "__________" + "__________" + "__________" + "__________" + "____";

    /**
     * Темы ВКР по ID протокола
     */
    protected Map<Long, SessionProjectTheme> _themeByProtocol;
    /**
     * Данные о последних приказах по ID протокола
     */
    protected Map<Long, OrderData> _orderDataByProtocol;

    @Override
    protected RtfDocument getTemplate()
    {
        UnisessionCommonTemplate templateItem = get(UnisessionCommonTemplate.class, UnisessionCommonTemplate.code().s(), UnisessionCommonTemplateCodes.SESSION_FINAL_QUALIFYING_WORK_PROTOCOL);
        return new RtfReader().read(templateItem.getContent());
    }

    @Override
    protected void initData(Collection<Long> protocolIds)
    {
        super.initData(protocolIds);

        String themeAlias = "theme";
        DQLSelectBuilder themeBuilder = new DQLSelectBuilder()
                .fromEntity(SessionProjectTheme.class, themeAlias)
                .column(property(themeAlias));

        String p1Alias = "prt";
        themeBuilder.joinEntity(themeAlias, DQLJoinType.inner, SessionStateFinalExamProtocol.class, p1Alias,
                                and(eq(property(p1Alias, SessionStateFinalExamProtocol.documentSlot()),
                                       property(themeAlias, SessionProjectTheme.slot())),
                                    in(property(p1Alias, SessionStateFinalExamProtocol.id()), protocolIds)))
                .column(property(p1Alias, SessionStateFinalExamProtocol.id()));

        _themeByProtocol = this.<Object[]>getList(themeBuilder).stream()
                .collect(Collectors.toMap(row -> (Long) row[1], row -> (SessionProjectTheme) row[0]));


        String orderAlias = "order";
        DQLSelectBuilder orderBuilder = new DQLSelectBuilder()
                .fromEntity(OrderData.class, orderAlias)
                .column(property(orderAlias));

        String p2Alias = "prt";
        orderBuilder.joinEntity(orderAlias, DQLJoinType.inner, SessionStateFinalExamProtocol.class, p2Alias,
                                in(property(p2Alias, SessionStateFinalExamProtocol.id()), protocolIds))
                .column(property(p2Alias, SessionStateFinalExamProtocol.id()))
                .where(eq(property(p2Alias, SessionStateFinalExamProtocol.documentSlot().studentWpeCAction().studentWpe().student()),
                          property(orderAlias, OrderData.student())));

        _orderDataByProtocol = this.<Object[]>getList(orderBuilder).stream()
                .collect(Collectors.toMap(row -> (Long) row[1], row -> (OrderData) row[0]));
    }

    @Override
    protected RtfInjectModifier modifyDocument(RtfInjectModifier modifier, SessionStateFinalExamProtocol p)
    {
        SessionFQWProtocol protocol = (SessionFQWProtocol) p;
        Student student = p.getDocumentSlot().getStudentWpeCAction().getStudentWpe().getStudent();

        SessionMark mark = _markBySlotMap.get(p.getDocumentSlot().getId());
        modifier.put("mark", mark == null ? "____________________" : mark.getValueTitle());

        List<String> membersFioString = getMembersFioString(p.getCommission(), SessionRoleInGECCodes.MEMBER);
        RtfString membersRtfString = new RtfString();
        if (CollectionUtils.isEmpty(membersFioString))
        {
            membersRtfString.append(EMPTY_LINE_MEMBERS)
                    .append(IRtfData.PAR).append(EMPTY_LINE_MEMBERS)
                    .append(IRtfData.PAR).append(EMPTY_LINE_MEMBERS);
        }
        else
        {
            Iterator<String> iterator = membersFioString.iterator();
            while (iterator.hasNext())
            {
                membersRtfString.append(iterator.next());
                if (iterator.hasNext())
                    membersRtfString.append(IRtfData.PAR);
            }
        }
        modifier.put("memberStateExamCommission", membersRtfString);


        SessionRecommendation4FQW recommendation = protocol.getRecommendation();
        modifier.put("opinion", recommendation == null ? null : recommendation.getPrintTitle());

        IDeclinationDao declinationDao = PersonManager.instance().declinationDao();

        PpsEntry consultant = protocol.getConsultant();
        modifier.put("consultant", consultant == null ? null :
                declinationDao.getDeclinationFIO(consultant.getPerson().getIdentityCard(), GrammaCase.GENITIVE));

        SessionProjectTheme theme = _themeByProtocol.get(protocol.getId());
        PpsEntry supervisor = theme == null ? null : theme.getSupervisor();
        modifier.put("scientificAdviser", supervisor == null ? null :
                declinationDao.getDeclinationFIO(supervisor.getPerson().getIdentityCard(), GrammaCase.GENITIVE));

        if (theme != null)
            modifier.put("theme", theme.getTheme());
        else
        {
            RtfString empty = new RtfString()
                    .append(EMPTY_LINE_THEME)
                    .append(IRtfData.PAR).append(EMPTY_LINE_THEME)
                    .append(IRtfData.PAR).append(EMPTY_LINE_THEME)
                    .append(IRtfData.PAR).append(EMPTY_LINE_THEME);

            modifier.put("theme", empty);
        }

        OrderData orderData = _orderDataByProtocol.get(protocol.getId());
        Date eduEnrollmentOrderDate = orderData == null ? null : orderData.getEduEnrollmentOrderDate();
        modifier.put("eduEnrollmentOrderDate", eduEnrollmentOrderDate == null ? "___________ 20 __ г." :
                DateFormatter.DEFAULT_DATE_FORMATTER.format(eduEnrollmentOrderDate));

        boolean isMale = student.getPerson().isMale();
        modifier.put("madeAndShowed", isMale ? "выполнил и защитил" : "выполнила и защитила");

        Qualifications qualification = student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getQualification();
        modifier.put("qualificationAndSuccess", qualification == null ? null :
                getQualificationAndSuccess(qualification, protocol.isWithHonors()));

        return modifier;
    }

    protected String getQualificationAndSuccess(Qualifications qualifications, boolean withHonors)
    {
        String result;
        switch (qualifications.getCode())
        {
            case QualificationsCodes.BAKALAVR:
                result = "бакалавра";
                break;
            case QualificationsCodes.MAGISTR:
                result = "магистра";
                break;
            case QualificationsCodes.SPETSIALIST:
                result = "специалиста";
                break;
            default:
                result = "";
        }

        return result + (result.length() > 0 && withHonors ? " " : "") + (withHonors ? "с отличием" : "");
    }
}