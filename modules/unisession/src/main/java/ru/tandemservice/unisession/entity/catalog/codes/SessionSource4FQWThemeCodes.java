package ru.tandemservice.unisession.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Источник темы ВКР"
 * Имя сущности : sessionSource4FQWTheme
 * Файл data.xml : session.data.xml
 */
public interface SessionSource4FQWThemeCodes
{
    /** Константа кода (code) элемента : тема студента (title) */
    String STUDENT_THEME = "session.FOWsc.1";
    /** Константа кода (code) элемента : заявка предприятия (title) */
    String COMPANY_REQUEST = "session.FOWsc.2";
    /** Константа кода (code) элемента : научные исследования (title) */
    String SCIENTIFIC_RESEARCH = "session.FOWsc.3";

    Set<String> CODES = ImmutableSet.of(STUDENT_THEME, COMPANY_REQUEST, SCIENTIFIC_RESEARCH);
}
