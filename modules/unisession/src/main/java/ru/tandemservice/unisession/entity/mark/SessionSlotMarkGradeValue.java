package ru.tandemservice.unisession.entity.mark;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;

import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.mark.gen.SessionSlotMarkGradeValueGen;

/**
 * Оценка студента в сессии (оценка)
 */
public class SessionSlotMarkGradeValue extends SessionSlotMarkGradeValueGen
{

    public SessionSlotMarkGradeValue() {}
    public SessionSlotMarkGradeValue(final SessionDocumentSlot slot, final SessionMarkGradeValueCatalogItem value, final Double points) {
        this.setSlot(slot);
        this.setValue(value);
        this.setPoints(points);
    }

    @Override public SessionMarkGradeValueCatalogItem getValueItem() {
        return this.getValue();
    }

    @Override public int getSessionMarkTypePriority() {
        return this.getValue().getScale().getPriority();
    }

    @Override public int getSessionMarkValuePriority() {
        return this.getValue().getPriority();
    }

    @Override public long getComparablePoints() {
        final Long pointsAsLong = this.getPointsAsLong();
        return (null == pointsAsLong ? 0L : pointsAsLong.longValue());
    }

    @Override
    @EntityDSLSupport(parts = SessionSlotMarkGradeValue.P_POINTS_AS_LONG)
    public Double getPoints()
    {
        final Long pointsAsLong = this.getPointsAsLong();
        return pointsAsLong == null ? null : ((double) pointsAsLong) / 100;
    }

    public void setPoints(final Double points)
    {
        this.setPointsAsLong(null == points ? null : Math.round(points * 100));
    }
}