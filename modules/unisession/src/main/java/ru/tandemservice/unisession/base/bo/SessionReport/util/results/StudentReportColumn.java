/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReport.util.results;

import ru.tandemservice.unisession.base.bo.SessionReport.ui.ResultsAdd.ISessionReportResultsDAO;

import java.util.HashSet;
import java.util.Set;

/**
* @author oleyba
* @since 2/13/12
*/
public abstract class StudentReportColumn extends CalculatedReportColumn
{
    private StudentReportColumn base;
    private Set<Long> students;

    public StudentReportColumn(StudentReportColumn base)
    {
        this.base = base;
    }

    public abstract boolean check(ISessionReportResultsDAO.ISessionResultsReportStudentData student);

    @Override
    public Double calculate(ISessionReportResultsDAO.ISessionResultsReportRow row)
    {
        students = new HashSet<Long>();
        if (null != base && !Boolean.TRUE.equals(base.calculated.get(row)))
            base.calculate(row);
        for (ISessionReportResultsDAO.ISessionResultsReportStudentData student : row.getStudents()) {
            if (null != base && !base.students.contains(student.getStudentId()))
                continue;
            if (check(student))
                students.add(student.getStudentId());
        }
        return (double) students.size();
    }
}
