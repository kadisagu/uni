package ru.tandemservice.unisession.entity.catalog;

import ru.tandemservice.unisession.entity.catalog.gen.SessionMarkCatalogItemGen;

/**
 * Оценка/отметка
 */
public abstract class SessionMarkCatalogItem extends SessionMarkCatalogItemGen
{
    abstract public int getPriority();

    @Override
    public String toString()
    {
        return getClass().getSimpleName() + " " + getId() + " " + getTitle();
    }
}