/* $Id:$ */
package ru.tandemservice.unisession.attestation.bo.Attestation.ui.Add;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unisession.entity.document.SessionObject;

import static org.tandemframework.hibsupport.dql.DQLExpressions.isNull;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author oleyba
 * @since 10/9/12
 */
@Configuration
public class AttestationAdd extends BusinessComponentManager
{
    public static final String BIND_ORG_UNIT = "orgUnit";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
        .addDataSource(selectDS("sessionObjectDS", sessionObjectDSHandler()).addColumn(SessionObject.termTitle().s()))
        .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> sessionObjectDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), SessionObject.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(isNull(property(SessionObject.closeDate().fromAlias(alias))));
            }
        }
        .where(SessionObject.orgUnit(), BIND_ORG_UNIT)
        .order(SessionObject.educationYear().intValue())
        .order(SessionObject.yearDistributionPart().number());
    }
}
