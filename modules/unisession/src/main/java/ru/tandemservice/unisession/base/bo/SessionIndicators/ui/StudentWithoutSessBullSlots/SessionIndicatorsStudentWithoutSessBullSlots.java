/**
 *$Id$
 */
package ru.tandemservice.unisession.base.bo.SessionIndicators.ui.StudentWithoutSessBullSlots;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList.AbstractUniStudentList;
import ru.tandemservice.uniepp.IUniEppDefines;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unisession.base.bo.SessionIndicators.logic.StudentWithoutSessBullSlotsSearchDSHandler;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;

/**
 * @author Alexander Shaburov
 * @since 05.12.12
 */
@Configuration
public class SessionIndicatorsStudentWithoutSessBullSlots extends AbstractUniStudentList
{
    public static final String YEAR_PART_DS = SessionReportManager.DS_YEAR_PART;
    public static final String EDU_YEAR_DS = SessionReportManager.DS_EDU_YEAR;
    public static final String REGISTRY_STRUCTURE_DS = "registryStructureDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return  super.presenterExtPoint(
                presenterExtPointBuilder()
                        .addDataSource(searchListDS(STUDENT_SEARCH_LIST_DS, studentSearchListDSColumnExtPoint(), studentSearchListDSHandler()))
                        .addDataSource(SessionReportManager.instance().eduYearDSConfig())
                        .addDataSource(SessionReportManager.instance().yearPartDSConfig())
                        .addDataSource(selectDS(REGISTRY_STRUCTURE_DS, registryStructureDSHandler()).treeable(true)));
    }

    @Bean
    public ColumnListExtPoint studentSearchListDSColumnExtPoint()
    {
        IPublisherLinkResolver regElementListResolver = new IPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                EppRegistryElementPart registryElementPart = (EppRegistryElementPart) entity;
                return new ParametersMap().add(UIPresenter.PUBLISHER_ID, registryElementPart.getRegistryElement().getId());
            }

            @Override
            public String getComponentName(IEntity entity)
            {
                return IUniEppDefines.EPP_DISCIPLINE_REGISTRY_PUB;
            }
        };
        return createStudentSearchListColumnsBuilder()
                .addColumn(publisherColumn("studentRegistryElementPartList", EppRegistryElementPart.titleWithNumber()).entityListProperty(StudentWithoutSessBullSlotsSearchDSHandler.STUDENT_REGISTRY_ELEMENT_PART_LIST).publisherLinkResolver(regElementListResolver))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> studentSearchListDSHandler()
    {
        return new StudentWithoutSessBullSlotsSearchDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> registryStructureDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EppRegistryStructure.class)
                .order(EppRegistryStructure.code())
                .filter(EppRegistryStructure.title());
    }
}
