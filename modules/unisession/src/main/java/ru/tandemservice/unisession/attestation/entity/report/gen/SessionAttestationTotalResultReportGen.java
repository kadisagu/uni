package ru.tandemservice.unisession.attestation.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unisession.attestation.entity.SessionAttestation;
import ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Отчет «Итоги межсессионной аттестации студентов»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionAttestationTotalResultReportGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport";
    public static final String ENTITY_NAME = "sessionAttestationTotalResultReport";
    public static final int VERSION_HASH = 1057060918;
    private static IEntityMeta ENTITY_META;

    public static final String L_CONTENT = "content";
    public static final String P_FORMING_DATE = "formingDate";
    public static final String L_ORG_UNIT = "orgUnit";
    public static final String L_EDUCATION_YEAR = "educationYear";
    public static final String L_YEAR_DISTRIBUTION_PART = "yearDistributionPart";
    public static final String L_ATTESTATION = "attestation";
    public static final String P_FORMING_ORG_UNITS = "formingOrgUnits";
    public static final String P_ATTESTATION_NUMBER = "attestationNumber";
    public static final String P_COURSE = "course";
    public static final String P_ATTESTATION_RESULT_FOR = "attestationResultFor";
    public static final String P_COMPENSATION_TYPE = "compensationType";
    public static final String P_DEVELOP_FORM = "developForm";
    public static final String P_DEVELOP_CONDITION = "developCondition";
    public static final String P_DEVELOP_TECH = "developTech";
    public static final String P_DEVELOP_PERIOD = "developPeriod";
    public static final String P_STUDENT_STATUS = "studentStatus";
    public static final String P_CUSTOM_STATE = "customState";
    public static final String P_TARGET_ADMISSION = "targetAdmission";
    public static final String P_ATTESTATION_TITLE = "attestationTitle";

    private DatabaseFile _content;     // Печатная форма
    private Date _formingDate;     // Дата формирования
    private OrgUnit _orgUnit;     // Подразделение
    private EducationYear _educationYear;     // Учебный год
    private YearDistributionPart _yearDistributionPart;     // Часть учебного года
    private SessionAttestation _attestation;     // Аттестация
    private String _formingOrgUnits;     // Форм. подразделения
    private Integer _attestationNumber;     // Номер аттестации
    private String _course;     // Курс
    private String _attestationResultFor;     // Выводить итоги аттестации студентов по
    private String _compensationType;     // Вид возмещения затрат
    private String _developForm;     // Форма освоения
    private String _developCondition;     // Условие освоения
    private String _developTech;     // Технология освоения
    private String _developPeriod;     // Срок освоения
    private String _studentStatus;     // Состояние студента
    private String _customState;     // Дополнительный статус
    private String _targetAdmission;     // Целевой прием

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     */
    @NotNull
    public DatabaseFile getContent()
    {
        return _content;
    }

    /**
     * @param content Печатная форма. Свойство не может быть null.
     */
    public void setContent(DatabaseFile content)
    {
        dirty(_content, content);
        _content = content;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата формирования. Свойство не может быть null.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    /**
     * @return Подразделение.
     */
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Учебный год.
     */
    public EducationYear getEducationYear()
    {
        return _educationYear;
    }

    /**
     * @param educationYear Учебный год.
     */
    public void setEducationYear(EducationYear educationYear)
    {
        dirty(_educationYear, educationYear);
        _educationYear = educationYear;
    }

    /**
     * @return Часть учебного года.
     */
    public YearDistributionPart getYearDistributionPart()
    {
        return _yearDistributionPart;
    }

    /**
     * @param yearDistributionPart Часть учебного года.
     */
    public void setYearDistributionPart(YearDistributionPart yearDistributionPart)
    {
        dirty(_yearDistributionPart, yearDistributionPart);
        _yearDistributionPart = yearDistributionPart;
    }

    /**
     * @return Аттестация.
     */
    public SessionAttestation getAttestation()
    {
        return _attestation;
    }

    /**
     * @param attestation Аттестация.
     */
    public void setAttestation(SessionAttestation attestation)
    {
        dirty(_attestation, attestation);
        _attestation = attestation;
    }

    /**
     * @return Форм. подразделения.
     */
    public String getFormingOrgUnits()
    {
        return _formingOrgUnits;
    }

    /**
     * @param formingOrgUnits Форм. подразделения.
     */
    public void setFormingOrgUnits(String formingOrgUnits)
    {
        dirty(_formingOrgUnits, formingOrgUnits);
        _formingOrgUnits = formingOrgUnits;
    }

    /**
     * @return Номер аттестации.
     */
    public Integer getAttestationNumber()
    {
        return _attestationNumber;
    }

    /**
     * @param attestationNumber Номер аттестации.
     */
    public void setAttestationNumber(Integer attestationNumber)
    {
        dirty(_attestationNumber, attestationNumber);
        _attestationNumber = attestationNumber;
    }

    /**
     * @return Курс.
     */
    @Length(max=255)
    public String getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс.
     */
    public void setCourse(String course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Выводить итоги аттестации студентов по.
     */
    @Length(max=255)
    public String getAttestationResultFor()
    {
        return _attestationResultFor;
    }

    /**
     * @param attestationResultFor Выводить итоги аттестации студентов по.
     */
    public void setAttestationResultFor(String attestationResultFor)
    {
        dirty(_attestationResultFor, attestationResultFor);
        _attestationResultFor = attestationResultFor;
    }

    /**
     * @return Вид возмещения затрат.
     */
    @Length(max=255)
    public String getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат.
     */
    public void setCompensationType(String compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Форма освоения.
     */
    @Length(max=255)
    public String getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения.
     */
    public void setDevelopForm(String developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Условие освоения.
     */
    @Length(max=255)
    public String getDevelopCondition()
    {
        return _developCondition;
    }

    /**
     * @param developCondition Условие освоения.
     */
    public void setDevelopCondition(String developCondition)
    {
        dirty(_developCondition, developCondition);
        _developCondition = developCondition;
    }

    /**
     * @return Технология освоения.
     */
    @Length(max=255)
    public String getDevelopTech()
    {
        return _developTech;
    }

    /**
     * @param developTech Технология освоения.
     */
    public void setDevelopTech(String developTech)
    {
        dirty(_developTech, developTech);
        _developTech = developTech;
    }

    /**
     * @return Срок освоения.
     */
    @Length(max=255)
    public String getDevelopPeriod()
    {
        return _developPeriod;
    }

    /**
     * @param developPeriod Срок освоения.
     */
    public void setDevelopPeriod(String developPeriod)
    {
        dirty(_developPeriod, developPeriod);
        _developPeriod = developPeriod;
    }

    /**
     * @return Состояние студента.
     */
    @Length(max=255)
    public String getStudentStatus()
    {
        return _studentStatus;
    }

    /**
     * @param studentStatus Состояние студента.
     */
    public void setStudentStatus(String studentStatus)
    {
        dirty(_studentStatus, studentStatus);
        _studentStatus = studentStatus;
    }

    /**
     * @return Дополнительный статус.
     */
    public String getCustomState()
    {
        return _customState;
    }

    /**
     * @param customState Дополнительный статус.
     */
    public void setCustomState(String customState)
    {
        dirty(_customState, customState);
        _customState = customState;
    }

    /**
     * @return Целевой прием.
     */
    @Length(max=255)
    public String getTargetAdmission()
    {
        return _targetAdmission;
    }

    /**
     * @param targetAdmission Целевой прием.
     */
    public void setTargetAdmission(String targetAdmission)
    {
        dirty(_targetAdmission, targetAdmission);
        _targetAdmission = targetAdmission;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SessionAttestationTotalResultReportGen)
        {
            setContent(((SessionAttestationTotalResultReport)another).getContent());
            setFormingDate(((SessionAttestationTotalResultReport)another).getFormingDate());
            setOrgUnit(((SessionAttestationTotalResultReport)another).getOrgUnit());
            setEducationYear(((SessionAttestationTotalResultReport)another).getEducationYear());
            setYearDistributionPart(((SessionAttestationTotalResultReport)another).getYearDistributionPart());
            setAttestation(((SessionAttestationTotalResultReport)another).getAttestation());
            setFormingOrgUnits(((SessionAttestationTotalResultReport)another).getFormingOrgUnits());
            setAttestationNumber(((SessionAttestationTotalResultReport)another).getAttestationNumber());
            setCourse(((SessionAttestationTotalResultReport)another).getCourse());
            setAttestationResultFor(((SessionAttestationTotalResultReport)another).getAttestationResultFor());
            setCompensationType(((SessionAttestationTotalResultReport)another).getCompensationType());
            setDevelopForm(((SessionAttestationTotalResultReport)another).getDevelopForm());
            setDevelopCondition(((SessionAttestationTotalResultReport)another).getDevelopCondition());
            setDevelopTech(((SessionAttestationTotalResultReport)another).getDevelopTech());
            setDevelopPeriod(((SessionAttestationTotalResultReport)another).getDevelopPeriod());
            setStudentStatus(((SessionAttestationTotalResultReport)another).getStudentStatus());
            setCustomState(((SessionAttestationTotalResultReport)another).getCustomState());
            setTargetAdmission(((SessionAttestationTotalResultReport)another).getTargetAdmission());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionAttestationTotalResultReportGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionAttestationTotalResultReport.class;
        }

        public T newInstance()
        {
            return (T) new SessionAttestationTotalResultReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "content":
                    return obj.getContent();
                case "formingDate":
                    return obj.getFormingDate();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "educationYear":
                    return obj.getEducationYear();
                case "yearDistributionPart":
                    return obj.getYearDistributionPart();
                case "attestation":
                    return obj.getAttestation();
                case "formingOrgUnits":
                    return obj.getFormingOrgUnits();
                case "attestationNumber":
                    return obj.getAttestationNumber();
                case "course":
                    return obj.getCourse();
                case "attestationResultFor":
                    return obj.getAttestationResultFor();
                case "compensationType":
                    return obj.getCompensationType();
                case "developForm":
                    return obj.getDevelopForm();
                case "developCondition":
                    return obj.getDevelopCondition();
                case "developTech":
                    return obj.getDevelopTech();
                case "developPeriod":
                    return obj.getDevelopPeriod();
                case "studentStatus":
                    return obj.getStudentStatus();
                case "customState":
                    return obj.getCustomState();
                case "targetAdmission":
                    return obj.getTargetAdmission();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "content":
                    obj.setContent((DatabaseFile) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "educationYear":
                    obj.setEducationYear((EducationYear) value);
                    return;
                case "yearDistributionPart":
                    obj.setYearDistributionPart((YearDistributionPart) value);
                    return;
                case "attestation":
                    obj.setAttestation((SessionAttestation) value);
                    return;
                case "formingOrgUnits":
                    obj.setFormingOrgUnits((String) value);
                    return;
                case "attestationNumber":
                    obj.setAttestationNumber((Integer) value);
                    return;
                case "course":
                    obj.setCourse((String) value);
                    return;
                case "attestationResultFor":
                    obj.setAttestationResultFor((String) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((String) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((String) value);
                    return;
                case "developCondition":
                    obj.setDevelopCondition((String) value);
                    return;
                case "developTech":
                    obj.setDevelopTech((String) value);
                    return;
                case "developPeriod":
                    obj.setDevelopPeriod((String) value);
                    return;
                case "studentStatus":
                    obj.setStudentStatus((String) value);
                    return;
                case "customState":
                    obj.setCustomState((String) value);
                    return;
                case "targetAdmission":
                    obj.setTargetAdmission((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "content":
                        return true;
                case "formingDate":
                        return true;
                case "orgUnit":
                        return true;
                case "educationYear":
                        return true;
                case "yearDistributionPart":
                        return true;
                case "attestation":
                        return true;
                case "formingOrgUnits":
                        return true;
                case "attestationNumber":
                        return true;
                case "course":
                        return true;
                case "attestationResultFor":
                        return true;
                case "compensationType":
                        return true;
                case "developForm":
                        return true;
                case "developCondition":
                        return true;
                case "developTech":
                        return true;
                case "developPeriod":
                        return true;
                case "studentStatus":
                        return true;
                case "customState":
                        return true;
                case "targetAdmission":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "content":
                    return true;
                case "formingDate":
                    return true;
                case "orgUnit":
                    return true;
                case "educationYear":
                    return true;
                case "yearDistributionPart":
                    return true;
                case "attestation":
                    return true;
                case "formingOrgUnits":
                    return true;
                case "attestationNumber":
                    return true;
                case "course":
                    return true;
                case "attestationResultFor":
                    return true;
                case "compensationType":
                    return true;
                case "developForm":
                    return true;
                case "developCondition":
                    return true;
                case "developTech":
                    return true;
                case "developPeriod":
                    return true;
                case "studentStatus":
                    return true;
                case "customState":
                    return true;
                case "targetAdmission":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "content":
                    return DatabaseFile.class;
                case "formingDate":
                    return Date.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "educationYear":
                    return EducationYear.class;
                case "yearDistributionPart":
                    return YearDistributionPart.class;
                case "attestation":
                    return SessionAttestation.class;
                case "formingOrgUnits":
                    return String.class;
                case "attestationNumber":
                    return Integer.class;
                case "course":
                    return String.class;
                case "attestationResultFor":
                    return String.class;
                case "compensationType":
                    return String.class;
                case "developForm":
                    return String.class;
                case "developCondition":
                    return String.class;
                case "developTech":
                    return String.class;
                case "developPeriod":
                    return String.class;
                case "studentStatus":
                    return String.class;
                case "customState":
                    return String.class;
                case "targetAdmission":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionAttestationTotalResultReport> _dslPath = new Path<SessionAttestationTotalResultReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionAttestationTotalResultReport");
    }
            

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport#getContent()
     */
    public static DatabaseFile.Path<DatabaseFile> content()
    {
        return _dslPath.content();
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Учебный год.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport#getEducationYear()
     */
    public static EducationYear.Path<EducationYear> educationYear()
    {
        return _dslPath.educationYear();
    }

    /**
     * @return Часть учебного года.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport#getYearDistributionPart()
     */
    public static YearDistributionPart.Path<YearDistributionPart> yearDistributionPart()
    {
        return _dslPath.yearDistributionPart();
    }

    /**
     * @return Аттестация.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport#getAttestation()
     */
    public static SessionAttestation.Path<SessionAttestation> attestation()
    {
        return _dslPath.attestation();
    }

    /**
     * @return Форм. подразделения.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport#getFormingOrgUnits()
     */
    public static PropertyPath<String> formingOrgUnits()
    {
        return _dslPath.formingOrgUnits();
    }

    /**
     * @return Номер аттестации.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport#getAttestationNumber()
     */
    public static PropertyPath<Integer> attestationNumber()
    {
        return _dslPath.attestationNumber();
    }

    /**
     * @return Курс.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport#getCourse()
     */
    public static PropertyPath<String> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Выводить итоги аттестации студентов по.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport#getAttestationResultFor()
     */
    public static PropertyPath<String> attestationResultFor()
    {
        return _dslPath.attestationResultFor();
    }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport#getCompensationType()
     */
    public static PropertyPath<String> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport#getDevelopForm()
     */
    public static PropertyPath<String> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport#getDevelopCondition()
     */
    public static PropertyPath<String> developCondition()
    {
        return _dslPath.developCondition();
    }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport#getDevelopTech()
     */
    public static PropertyPath<String> developTech()
    {
        return _dslPath.developTech();
    }

    /**
     * @return Срок освоения.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport#getDevelopPeriod()
     */
    public static PropertyPath<String> developPeriod()
    {
        return _dslPath.developPeriod();
    }

    /**
     * @return Состояние студента.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport#getStudentStatus()
     */
    public static PropertyPath<String> studentStatus()
    {
        return _dslPath.studentStatus();
    }

    /**
     * @return Дополнительный статус.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport#getCustomState()
     */
    public static PropertyPath<String> customState()
    {
        return _dslPath.customState();
    }

    /**
     * @return Целевой прием.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport#getTargetAdmission()
     */
    public static PropertyPath<String> targetAdmission()
    {
        return _dslPath.targetAdmission();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport#getAttestationTitle()
     */
    public static SupportedPropertyPath<String> attestationTitle()
    {
        return _dslPath.attestationTitle();
    }

    public static class Path<E extends SessionAttestationTotalResultReport> extends EntityPath<E>
    {
        private DatabaseFile.Path<DatabaseFile> _content;
        private PropertyPath<Date> _formingDate;
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private EducationYear.Path<EducationYear> _educationYear;
        private YearDistributionPart.Path<YearDistributionPart> _yearDistributionPart;
        private SessionAttestation.Path<SessionAttestation> _attestation;
        private PropertyPath<String> _formingOrgUnits;
        private PropertyPath<Integer> _attestationNumber;
        private PropertyPath<String> _course;
        private PropertyPath<String> _attestationResultFor;
        private PropertyPath<String> _compensationType;
        private PropertyPath<String> _developForm;
        private PropertyPath<String> _developCondition;
        private PropertyPath<String> _developTech;
        private PropertyPath<String> _developPeriod;
        private PropertyPath<String> _studentStatus;
        private PropertyPath<String> _customState;
        private PropertyPath<String> _targetAdmission;
        private SupportedPropertyPath<String> _attestationTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport#getContent()
     */
        public DatabaseFile.Path<DatabaseFile> content()
        {
            if(_content == null )
                _content = new DatabaseFile.Path<DatabaseFile>(L_CONTENT, this);
            return _content;
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(SessionAttestationTotalResultReportGen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Учебный год.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport#getEducationYear()
     */
        public EducationYear.Path<EducationYear> educationYear()
        {
            if(_educationYear == null )
                _educationYear = new EducationYear.Path<EducationYear>(L_EDUCATION_YEAR, this);
            return _educationYear;
        }

    /**
     * @return Часть учебного года.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport#getYearDistributionPart()
     */
        public YearDistributionPart.Path<YearDistributionPart> yearDistributionPart()
        {
            if(_yearDistributionPart == null )
                _yearDistributionPart = new YearDistributionPart.Path<YearDistributionPart>(L_YEAR_DISTRIBUTION_PART, this);
            return _yearDistributionPart;
        }

    /**
     * @return Аттестация.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport#getAttestation()
     */
        public SessionAttestation.Path<SessionAttestation> attestation()
        {
            if(_attestation == null )
                _attestation = new SessionAttestation.Path<SessionAttestation>(L_ATTESTATION, this);
            return _attestation;
        }

    /**
     * @return Форм. подразделения.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport#getFormingOrgUnits()
     */
        public PropertyPath<String> formingOrgUnits()
        {
            if(_formingOrgUnits == null )
                _formingOrgUnits = new PropertyPath<String>(SessionAttestationTotalResultReportGen.P_FORMING_ORG_UNITS, this);
            return _formingOrgUnits;
        }

    /**
     * @return Номер аттестации.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport#getAttestationNumber()
     */
        public PropertyPath<Integer> attestationNumber()
        {
            if(_attestationNumber == null )
                _attestationNumber = new PropertyPath<Integer>(SessionAttestationTotalResultReportGen.P_ATTESTATION_NUMBER, this);
            return _attestationNumber;
        }

    /**
     * @return Курс.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport#getCourse()
     */
        public PropertyPath<String> course()
        {
            if(_course == null )
                _course = new PropertyPath<String>(SessionAttestationTotalResultReportGen.P_COURSE, this);
            return _course;
        }

    /**
     * @return Выводить итоги аттестации студентов по.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport#getAttestationResultFor()
     */
        public PropertyPath<String> attestationResultFor()
        {
            if(_attestationResultFor == null )
                _attestationResultFor = new PropertyPath<String>(SessionAttestationTotalResultReportGen.P_ATTESTATION_RESULT_FOR, this);
            return _attestationResultFor;
        }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport#getCompensationType()
     */
        public PropertyPath<String> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new PropertyPath<String>(SessionAttestationTotalResultReportGen.P_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport#getDevelopForm()
     */
        public PropertyPath<String> developForm()
        {
            if(_developForm == null )
                _developForm = new PropertyPath<String>(SessionAttestationTotalResultReportGen.P_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport#getDevelopCondition()
     */
        public PropertyPath<String> developCondition()
        {
            if(_developCondition == null )
                _developCondition = new PropertyPath<String>(SessionAttestationTotalResultReportGen.P_DEVELOP_CONDITION, this);
            return _developCondition;
        }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport#getDevelopTech()
     */
        public PropertyPath<String> developTech()
        {
            if(_developTech == null )
                _developTech = new PropertyPath<String>(SessionAttestationTotalResultReportGen.P_DEVELOP_TECH, this);
            return _developTech;
        }

    /**
     * @return Срок освоения.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport#getDevelopPeriod()
     */
        public PropertyPath<String> developPeriod()
        {
            if(_developPeriod == null )
                _developPeriod = new PropertyPath<String>(SessionAttestationTotalResultReportGen.P_DEVELOP_PERIOD, this);
            return _developPeriod;
        }

    /**
     * @return Состояние студента.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport#getStudentStatus()
     */
        public PropertyPath<String> studentStatus()
        {
            if(_studentStatus == null )
                _studentStatus = new PropertyPath<String>(SessionAttestationTotalResultReportGen.P_STUDENT_STATUS, this);
            return _studentStatus;
        }

    /**
     * @return Дополнительный статус.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport#getCustomState()
     */
        public PropertyPath<String> customState()
        {
            if(_customState == null )
                _customState = new PropertyPath<String>(SessionAttestationTotalResultReportGen.P_CUSTOM_STATE, this);
            return _customState;
        }

    /**
     * @return Целевой прием.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport#getTargetAdmission()
     */
        public PropertyPath<String> targetAdmission()
        {
            if(_targetAdmission == null )
                _targetAdmission = new PropertyPath<String>(SessionAttestationTotalResultReportGen.P_TARGET_ADMISSION, this);
            return _targetAdmission;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport#getAttestationTitle()
     */
        public SupportedPropertyPath<String> attestationTitle()
        {
            if(_attestationTitle == null )
                _attestationTitle = new SupportedPropertyPath<String>(SessionAttestationTotalResultReportGen.P_ATTESTATION_TITLE, this);
            return _attestationTitle;
        }

        public Class getEntityClass()
        {
            return SessionAttestationTotalResultReport.class;
        }

        public String getEntityName()
        {
            return "sessionAttestationTotalResultReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getAttestationTitle();
}
