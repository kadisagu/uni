/* $Id$ */
// Copyright 2006-2012 Tandem Service Software
package ru.tandemservice.unisession.component.orgunit.SessionTransferDocListTab;

import com.google.common.collect.ImmutableList;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unisession.entity.document.SessionTransferDocument;
import ru.tandemservice.unisession.entity.document.SessionTransferInsideDocument;
import ru.tandemservice.unisession.entity.document.SessionTransferOutsideDocument;

import java.util.Date;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Vasily Zhukov
 * @since 10.01.2012
 */
public class DAO extends UniBaseDao implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.getOrgUnitHolder().refresh();
        model.setTransferTypeModel(new LazySimpleSelectModel<>(ImmutableList.of(
                new DataWrapper(0L, "Внешнее"),
                new DataWrapper(1L, "Внутреннее")
        )));
    }

    @Override
    public void refreshDataSource(Model model)
    {
        final Session session = this.getSession();
        final DynamicListDataSource<SessionTransferDocument> dataSource = model.getDataSource();

        DQLOrderDescriptionRegistry order;
        final DataWrapper type = model.getSettings().get("transferType");
        if (type != null)
            order = new DQLOrderDescriptionRegistry(type.getId().equals(0L) ? SessionTransferOutsideDocument.class : SessionTransferInsideDocument.class, "doc");
        else
            order = new DQLOrderDescriptionRegistry(SessionTransferDocument.class, "doc");

        final DQLSelectBuilder dql = order.buildDQLSelectBuilder();
        dql.where(eq(property(SessionTransferDocument.orgUnit().fromAlias("doc")), value(model.getOrgUnit())));
        applyFilters(dql, "doc", model.getSettings());
        order.applyOrder(dql, dataSource.getEntityOrder());
        UniBaseUtils.createPage(dataSource, dql.column(property("doc")), session);

        for (ViewWrapper wrapper : ViewWrapper.getPatchedList(dataSource)) {
            wrapper.setViewProperty("docType", wrapper.getEntity() instanceof SessionTransferInsideDocument ? "внутреннее" : "внешнее");
        }
    }

    /**
     * Фильтрует по стандартному для документов набору фильтров
     *
     * @param dql      - билдер запроса
     * @param docAlias - алиас документа, должен присутствовать в билдере
     * @param settings - IDataSettings, содержащие значения фильтров
     */
    public void applyFilters(final DQLSelectBuilder dql, final String docAlias, final IDataSettings settings)
    {
        final String docNumber = settings.get("docNumber");
        if (StringUtils.isNotBlank(docNumber))
            dql.where(eq(property(SessionTransferDocument.number().fromAlias(docAlias)), value(docNumber)));

        FilterUtils.applySimpleLikeFilter(dql, docAlias, SessionTransferDocument.targetStudent().person().identityCard().lastName().s(), settings.<String>get("personLastName"));
        FilterUtils.applySimpleLikeFilter(dql, docAlias, SessionTransferDocument.targetStudent().person().identityCard().firstName().s(), settings.<String>get("personFirstName"));
        FilterUtils.applyBetweenFilter(dql, docAlias, SessionTransferDocument.formingDate().s(), settings.<Date>get("formedFrom"), settings.<Date>get("formedTo"));
    }
}
