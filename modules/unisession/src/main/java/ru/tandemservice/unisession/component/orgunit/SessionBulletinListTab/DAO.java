package ru.tandemservice.unisession.component.orgunit.SessionBulletinListTab;

import com.google.common.collect.ImmutableList;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.entity.dsl.PropertyPath;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.util.cache.SingleValueCache;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.gen.OrgUnitGen;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import org.tandemframework.tapsupport.component.selection.CommonMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniedu.base.bo.EducationYear.utils.EducationYearModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.catalog.EppControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.gen.EppRegistryElementGen;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4ActionTypeRow;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.uniepp.entity.student.group.gen.EppRealEduGroup4ActionTypeRowGen;
import ru.tandemservice.uniepp.ui.EppRegistryElementSelectModel;
import ru.tandemservice.unisession.dao.bulletin.ISessionBulletinDAO;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;
import ru.tandemservice.unisession.entity.document.SessionDocumentPrintVersion;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionObject;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.print.ISessionBulletinPrintDAO;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.count;

/**
 * @author vdanilov
 */
public class DAO extends UniBaseDao implements IDAO
{

    private final DQLOrderDescriptionRegistry order = new DQLOrderDescriptionRegistry(SessionBulletinDocument.class, "b");

    @Override
    public void prepare(final Model model)
    {
        model.getOrgUnitHolder().refresh();
        model.setSec(new OrgUnitSecModel(model.getOrgUnit()));

        model.setYearModel(new EducationYearModel()
        {
            @Override
            protected DQLSelectBuilder query(final String alias, final String filter)
            {
                final DQLSelectBuilder sessionObjectExists = new DQLSelectBuilder()
                        .fromEntity(SessionObject.class, alias)
                        .column(property(SessionObject.educationYear().id().fromAlias(alias)))
                        .where(eq(property(SessionObject.orgUnit().fromAlias(alias)), value(model.getOrgUnit())));

                final DQLSelectBuilder dql = new DQLSelectBuilder();
                dql.fromEntity(EducationYear.class, alias);
                dql.where(in(property(EducationYear.id().fromAlias(alias)), sessionObjectExists.buildQuery()));
                dql.order(property(EducationYear.intValue().fromAlias(alias)));
                return dql;
            }
        });

        final DQLSelectBuilder partsDQL = new DQLSelectBuilder()
                .fromEntity(SessionObject.class, "obj")
                .column(property(SessionObject.yearDistributionPart().fromAlias("obj")))
                .where(eq(property(SessionObject.orgUnit().fromAlias("obj")), value(model.getOrgUnit())))
                .order(property(SessionObject.yearDistributionPart().code().fromAlias("obj")));
        if (model.getYear() != null)
        {
            partsDQL.where(eq(property(SessionObject.educationYear().fromAlias("obj")), value(model.getYear())));
        }
        final Set<YearDistributionPart> parts = new LinkedHashSet<>(partsDQL.createStatement(getSession()).<YearDistributionPart>list());
        model.setPartsModel(EducationCatalogsManager.getYearDistributionPartModel(parts));

        model.setBullStatusModel(new LazySimpleSelectModel<>(ImmutableList.of(
                new DataWrapper(Model.BULL_STATUS_EMPTY_ID, "Незаполненная"),
                new DataWrapper(Model.BULL_STATUS_OPEN_ID, "Открытая"),
                new DataWrapper(Model.BULL_STATUS_CLOSE_ID, "Закрытая")
        )));

        model.setControlActionTypeModel(new DQLFullCheckSelectModel("title")
        {
            @Override
            protected DQLSelectBuilder query(final String alias, final String filter)
            {
                final DQLSelectBuilder s = getRootDql(model);
                s.column(property(SessionBulletinDocument.group().type().id().fromAlias("b")));

                final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppFControlActionType.class, alias);
                dql.where(in(property(alias, EppFControlActionType.eppGroupType()), s.buildQuery()));
                if (null != filter)
                {
                    dql.where(or(
                            like(EppControlActionType.title().fromAlias(alias), filter),
                            like(EppControlActionType.shortTitle().fromAlias(alias), filter)
                    ));
                }

                dql.order(property(EppControlActionType.shortTitle().fromAlias(alias)));
                dql.order(property(EppControlActionType.title().fromAlias(alias)));
                return dql;
            }
        });

        model.setProgramKindModel(new DQLFullCheckSelectModel() {
            @Override
            protected DQLSelectBuilder query(String alias, String filter) {
                final String slotAlias = "slot";
                DQLSelectBuilder programKindByBulletins = new DQLSelectBuilder()
                        .fromEntity(SessionDocumentSlot.class, slotAlias)
                        .column(property(slotAlias, SessionDocumentSlot.studentWpeCAction().studentWpe().student()
								.educationOrgUnit().educationLevelHighSchool().educationLevel().eduProgramSubject().subjectIndex().programKind()))
                        .where(in(property(slotAlias, SessionDocumentSlot.document()), getRootDql(model).buildQuery()));
                DQLSelectBuilder dql = new DQLSelectBuilder()
                        .fromEntity(EduProgramKind.class, alias)
                        .order(property(alias, EduProgramKind.priority()))
                        .where(in(property(alias), programKindByBulletins.buildQuery()));
                FilterUtils.applyLikeFilter(dql, filter, EduProgramKind.title().fromAlias(alias));
                return dql;
            }
        });

        model.setRegistryOwnerModel(new DQLFullCheckSelectModel("shortTitle")
        {
            @Override
            protected DQLSelectBuilder query(final String alias, final String filter)
            {
                final DQLSelectBuilder s = getRootDql(model);
                s.column(property(SessionBulletinDocument.group().activityPart().registryElement().owner().id().fromAlias("b")));

                final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(OrgUnit.class, alias);
                dql.where(in(property(alias, "id"), s.buildQuery()));
                if (null != filter)
                {
                    dql.where(or(
                            like(OrgUnitGen.title().fromAlias(alias), filter),
                            like(OrgUnitGen.shortTitle().fromAlias(alias), filter)
                    ));
                }

                dql.order(property(OrgUnitGen.shortTitle().fromAlias(alias)));
                dql.order(property(OrgUnitGen.title().fromAlias(alias)));
                return dql;
            }
        });

        model.setRegistryStructureModel(new CommonMultiSelectModel()
        {
            {
                setHierarchical(true);
            }

            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppRegistryStructure.class, "b").column(property("b"))
                        .where(likeUpper(property(EppRegistryStructure.title().fromAlias("b")), value(CoreStringUtils.escapeLike(filter, true))))
                        .order(property(EppRegistryStructure.title().fromAlias("b")));

                if (set != null)
                    builder.where(in(property(EppRegistryStructure.id().fromAlias("b")), set));

                return new DQLListResultBuilder(builder, 50);
            }
        });

        model.setRegistryElementModel(new EppRegistryElementSelectModel<EppRegistryElement>(EppRegistryElement.class)
        {
            @Override
            protected DQLSelectBuilder query(final String alias, final String filter)
            {
                final DQLSelectBuilder s = getRootDql(model);
                s.column(property(SessionBulletinDocument.group().activityPart().registryElement().id().fromAlias("b")));

                final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppRegistryElement.class, alias);
                dql.where(in(property(alias, "id"), s.buildQuery()));

                {
                    final Collection registryOwners = model.getRegistryOwners();
                    if (null != registryOwners)
                    {
                        dql.where(in(property(EppRegistryElementGen.owner().fromAlias(alias)), registryOwners));
                    }
                }

                if (null != filter)
                {
                    dql.where(getFilterCondition(alias, filter));
                }
                dql.order(property(EppRegistryElementGen.title().fromAlias(alias)));
                dql.order(property(EppRegistryElementGen.owner().title().fromAlias(alias)));
                return dql;
            }
        });

        model.setCourseModel(new CommonMultiSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Course.class, "b").column(property("b"))
                        .where(likeUpper(property(Course.title().fromAlias("b")), value(CoreStringUtils.escapeLike(filter, true))))
                        .order(property(Course.title().fromAlias("b")));

                if (set != null)
                    builder.where(in(property(Course.id().fromAlias("b")), set));

                return new DQLListResultBuilder(builder, 50);
            }
        });

        model.setGroupModel(UniEppUtils.getTitleSelectModel(new SingleValueCache<List<String>>()
        {
            @Override
            protected List<String> resolve()
            {
                final DQLSelectBuilder rootDql = getRootDql(model).column(property("b.id"));
                final DQLSelectBuilder s = joinRelations(order.buildDQLSelectBuilder()).where(in(property("b.id"), rootDql.buildQuery()));
                s.predicate(DQLPredicateType.distinct);
                s.column(property(EppRealEduGroup4ActionTypeRowGen.studentGroupTitle().fromAlias("rel")));
                s.order(property(EppRealEduGroup4ActionTypeRowGen.studentGroupTitle().fromAlias("rel")));
                s.where(isNull(property(EppRealEduGroup4ActionTypeRowGen.removalDate().fromAlias("rel"))));
                return s.createStatement(getSession()).list(); // если надо обновить - нажмут f5
            }
        }));

        model.setCourseCurrentModel(new CommonMultiSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Course.class, "b").column(property("b"))
                        .where(likeUpper(property(Course.title().fromAlias("b")), value(CoreStringUtils.escapeLike(filter, true))))
                        .order(property(Course.title().fromAlias("b")));

                if (set != null)
                    builder.where(in(property(Course.id().fromAlias("b")), set));

                return new DQLListResultBuilder(builder, 50);
            }
        });

        model.setGroupCurrentModel(new CommonMultiSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                DQLSelectBuilder rootDql = getRootDql(model);
                rootDql.column(property(SessionBulletinDocument.id().fromAlias("b")));
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SessionDocumentSlot.class, "a").column(property(SessionDocumentSlot.studentWpeCAction().studentWpe().student().group().fromAlias("a")))
                        .where(in(property(SessionDocumentSlot.document().id().fromAlias("a")), rootDql.buildQuery()))
                        .where(likeUpper(property(SessionDocumentSlot.studentWpeCAction().studentWpe().student().group().title().fromAlias("a")), value(CoreStringUtils.escapeLike(filter, true))))
                        .order(property(SessionDocumentSlot.studentWpeCAction().studentWpe().student().group().title().fromAlias("a")))
                        .predicate(DQLPredicateType.distinct);

                if (model.getCourseCurrentList() != null)
                    builder.where(in(property(SessionDocumentSlot.studentWpeCAction().studentWpe().student().group().course().fromAlias("a")), model.getCourseCurrentList()));

                if (set != null)
                    builder.where(in(property(SessionDocumentSlot.studentWpeCAction().studentWpe().student().group().id().fromAlias("a")), set));

                return new DQLListResultBuilder(builder, 50);
            }
        });
    }

    @Override
    public void refreshDataSource(final Model model)
    {
        final Session session = getSession();
        final DynamicListDataSource<SessionBulletinDocument> dataSource = model.getDataSource();

        final DQLSelectBuilder dql = getRootDql(model);

        applyFilters(model, dql, "b");

        final Collection registryElements = model.getRegistryElements();
        if (null != registryElements)
        {
            dql.where(in(property(SessionBulletinDocument.group().activityPart().registryElement().fromAlias("b")), registryElements));
        } else
        {
            final Collection registryOwners = model.getRegistryOwners();
            if (null != registryOwners)
            {
                dql.where(in(property(SessionBulletinDocument.group().activityPart().registryElement().owner().fromAlias("b")), registryOwners));
            }
        }

        final DQLSelectBuilder realGroupDql = new DQLSelectBuilder()
                .fromEntity(EppRealEduGroup4ActionTypeRow.class, "rel")
                .column(property(EppRealEduGroup4ActionTypeRowGen.group().id().fromAlias("rel")));
        boolean checkGroupDql = false;

        final DQLSelectBuilder slotDql = new DQLSelectBuilder()
                .fromEntity(SessionDocumentSlot.class, "slot")
                .where(eq(property("slot", SessionDocumentSlot.document()), property("b")))
                .column(property(SessionDocumentSlot.id().fromAlias("slot")));
        boolean checkSlotDql = false;

        final Collection groups = model.getGroups();
        if (null != groups)
        {
            checkGroupDql = true;
            if (groups.contains(""))
            {
                realGroupDql.where(or(
                        in(property(EppRealEduGroup4ActionTypeRowGen.studentGroupTitle().fromAlias("rel")), groups),
                        isNull(property(EppRealEduGroup4ActionTypeRowGen.studentGroupTitle().fromAlias("rel")))
                ));
            } else
            {
                realGroupDql.where(in(property(EppRealEduGroup4ActionTypeRowGen.studentGroupTitle().fromAlias("rel")), groups));
            }
        }

        final Collection<EppControlActionType> controlActionTypes = model.getControlActionTypes();
        if (null != controlActionTypes)
        {
            dql.where(in(property(SessionBulletinDocument.group().type().fromAlias("b")), CommonBaseEntityUtil.getPropertiesList(controlActionTypes, EppFControlActionType.eppGroupType())));
        }

        final EduProgramKind programKind = model.getProgramKind();
        if (programKind != null)
        {
            final String slotAlias = "slot";
            DQLSelectBuilder filteredBulletins = new DQLSelectBuilder()
                    .fromEntity(SessionDocumentSlot.class, slotAlias)
                    .column(property(slotAlias, SessionDocumentSlot.document()))
					.where(eq(property(slotAlias, SessionDocumentSlot.studentWpeCAction().studentWpe().student()
							.educationOrgUnit().educationLevelHighSchool().educationLevel().eduProgramSubject().subjectIndex().programKind()), value(programKind)));
            dql.where(in(property("b"), filteredBulletins.buildQuery()));
        }

        final String documentNumber = model.getDocumentNumber();
        if (documentNumber != null)
        {
            dql.where(eq(property(SessionBulletinDocument.number().fromAlias("b")), value(documentNumber)));
        }

        final DataWrapper bullStatus = model.getBullStatus();
        if (bullStatus != null)
        {
            if (bullStatus.getId().equals(Model.BULL_STATUS_EMPTY_ID))
            {
                DQLSelectBuilder markDQL = new DQLSelectBuilder()
                        .fromEntity(SessionMark.class, "mark")
                        .column(property(SessionMark.id().fromAlias("mark")), "m_id")
                        .column(property(SessionMark.slot().document().id().fromAlias("mark")), "b_id");

                dql.joinDataSource("b", DQLJoinType.left, markDQL.buildQuery(), "m", eq(property("b.id"), property("m.b_id")));
                dql.where(isNull("m.m_id"));
            } else if (bullStatus.getId().equals(Model.BULL_STATUS_OPEN_ID))
            {
                dql.where(isNull(property(SessionBulletinDocument.closeDate().fromAlias("b"))));
            } else if (bullStatus.getId().equals(Model.BULL_STATUS_CLOSE_ID))
            {
                dql.where(isNotNull(property(SessionBulletinDocument.closeDate().fromAlias("b"))));
            }
        }

        final Collection<EppRegistryStructure> registryStructureList = model.getRegistryStructureList();
        if (registryStructureList != null)
            dql.where(in(property(SessionBulletinDocument.group().activityPart().registryElement().parent().fromAlias("b")), registryStructureList));

        final Collection<Course> courseList = model.getCourseList();
        if (courseList != null)
        {
            checkSlotDql = true;
            slotDql.where(in(property(SessionDocumentSlot.studentWpeCAction().studentWpe().course().fromAlias("slot")), courseList));
        }

        final Collection<Course> courseCurrentList = model.getCourseCurrentList();
        if (courseCurrentList != null)
        {
            checkSlotDql = true;
            slotDql.where(in(property(SessionDocumentSlot.studentWpeCAction().studentWpe().student().course().fromAlias("slot")), courseCurrentList));
        }

        final Collection<Course> groupCurrentList = model.getGroupCurrentList();
        if (groupCurrentList != null)
        {
            checkSlotDql = true;
            slotDql.where(in(property(SessionDocumentSlot.studentWpeCAction().studentWpe().student().group().fromAlias("slot")), groupCurrentList));
        }

        final String ppsLastName = model.getPpsLastName();
        if (ppsLastName != null)
        {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SessionComissionPps.class, "pps").column(property(SessionComissionPps.id().fromAlias("pps")))
                    .where(eq(property(SessionComissionPps.commission().id().fromAlias("pps")), property(SessionBulletinDocument.commission().id().fromAlias("b"))))
                    .where(likeUpper(property(SessionComissionPps.pps().person().identityCard().lastName().fromAlias("pps")), value(CoreStringUtils.escapeLike(ppsLastName, true))));

            dql.where(exists(builder.buildQuery()));
        }

        final String studentLastName = model.getStudentLastName();
        if (studentLastName != null)
        {
            checkSlotDql = true;
            slotDql.where(likeUpper(property(SessionDocumentSlot.studentWpeCAction().studentWpe().student().person().identityCard().lastName().fromAlias("slot")), value(CoreStringUtils.escapeLike(studentLastName, true))));
        }

        if (checkGroupDql)
        {
            dql.where(in(property(SessionBulletinDocument.group().id().fromAlias("b")), realGroupDql.buildQuery()));
        }

        if (checkSlotDql)
        {
            dql.where(exists(slotDql.buildQuery()));
        }


        final EntityOrder entityOrder = dataSource.getEntityOrder();
        if ("title".equals(entityOrder.getKey()))
        {
            UniBaseUtils.createPageOrderedByNumberAsString(SessionBulletinDocument.class, dataSource, dql, "b", SessionBulletinDocument.number());
        } else if ("course".equals(entityOrder.getKey()))
        {
            createPage(dataSource, dql, EppRealEduGroupRow.studentWpePart().studentWpe().course().intValue());
        } else if ("groups".equals(entityOrder.getKey()))
        {
            createPage(dataSource, dql, EppRealEduGroupRow.studentGroupTitle());
        } else
        {
            order.applyOrder(dql, entityOrder);
            UniBaseUtils.createPage(dataSource, dql.column(property("b")), session);
        }

        final List<ViewWrapper<SessionBulletinDocument>> list = ViewWrapper.getPatchedList(dataSource);
        BatchUtils.execute(list, 64, new BatchUtils.Action<ViewWrapper<SessionBulletinDocument>>()
                {
                    @SuppressWarnings("unchecked")
                    private <T> Map<Long, Set<T>> getRelationMap(final Collection<Long> ids, final PropertyPath<T> relationPath)
                    {
                        final DQLSelectBuilder relDql = joinRelations(order.buildDQLSelectBuilder()).where(in(property("b.id"), ids));
                        relDql.column(property(SessionBulletinDocument.id().fromAlias("b")));
                        relDql.column(property(relationPath.fromAlias("rel")));
                        relDql.predicate(DQLPredicateType.distinct);
                        relDql.order(property(relationPath.fromAlias("rel")));
                        final Map<Long, Set<T>> result = new HashMap<>();
                        for (final Object[] row : relDql.createStatement(session).<Object[]>list())
                        {
                            if (null != row[1])
                            {
                                SafeMap.safeGet(result, (Long) row[0], LinkedHashSet.class).add((T) row[1]);
                            }
                        }
                        return result;
                    }

                    final Map<Long, String> tutorTitleMap = SafeMap.get(key -> ((PpsEntry) session.get(PpsEntry.class, key)).getPerson().getFio());

                    @Override
                    public void execute(final Collection<ViewWrapper<SessionBulletinDocument>> list)
                    {
                        final Collection<Long> ids = UniBaseUtils.getIdList(list);
                        final Map<Long, Set<String>> groupMap = getRelationMap(ids, EppRealEduGroup4ActionTypeRow.studentGroupTitle());
                        final Map<Long, Set<String>> courseMap = getRelationMap(ids, EppRealEduGroup4ActionTypeRow.studentWpePart().studentWpe().course().title());

                        final Map<Long, Set<String>> currGroupMap = new HashMap<>();
                        {
                            final DQLSelectBuilder dql = new DQLSelectBuilder()
                                    .fromEntity(SessionDocumentSlot.class, "slot")
                                    .column(property("slot", SessionDocumentSlot.document().id()))
                                    .column(property("slot", SessionDocumentSlot.actualStudent().group().title()))
                                    .where(in(property("slot", SessionDocumentSlot.document().id()), ids))
                                    .order(property("slot", SessionDocumentSlot.actualStudent().group().title()));

                            for (final Object[] row : UniBaseDao.scrollRows(dql.createStatement(session)))
                            {
                                SafeMap.safeGet(currGroupMap, (Long) row[0], LinkedHashSet.class).add((String) row[1]);
                            }
                        }

                        final Map<Long, List<String>> comissionMap = new HashMap<>(ids.size());
                        {
                            final DQLSelectBuilder dql = new DQLSelectBuilder();
                            dql.fromEntity(SessionComissionPps.class, "pps");
                            dql.joinEntity("pps", DQLJoinType.inner, SessionBulletinDocument.class, "b", eq(
                                    property(SessionComissionPps.commission().id().fromAlias("pps")),
                                    property(SessionBulletinDocument.commission().id().fromAlias("b"))
                            ));
                            dql.column(property(SessionBulletinDocument.id().fromAlias("b")));
                            dql.column(property(SessionComissionPps.pps().id().fromAlias("pps")));
                            dql.where(in(property(SessionBulletinDocument.id().fromAlias("b")), ids));
                            dql.order(property(SessionComissionPps.pps().person().identityCard().fullFio().fromAlias("pps")));

                            for (final Object[] row : UniBaseDao.scrollRows(dql.createStatement(session)))
                            {
                                SafeMap.safeGet(comissionMap, (Long) row[0], ArrayList.class).add(tutorTitleMap.get((Long) row[1]));
                            }
                        }

                        final Map<Long, Number> studentCountMap = new HashMap<>();
                        {
                            final DQLSelectBuilder dql = new DQLSelectBuilder();
                            dql.fromEntity(SessionDocumentSlot.class, "slot");
                            dql.where(in(property(SessionDocumentSlot.document().id().fromAlias("slot")), ids));
                            dql.group(property(SessionDocumentSlot.document().id().fromAlias("slot")));
                            dql.column(property(SessionDocumentSlot.document().id().fromAlias("slot")));
                            dql.column(count(SessionDocumentSlot.id().fromAlias("slot")));

                            for (final Object[] row : UniBaseDao.scrollRows(dql.createStatement(getSession())))
                            {
                                studentCountMap.put((Long) row[0], (Number) row[1]);
                            }
                        }

                        for (final ViewWrapper<SessionBulletinDocument> bulletin : list)
                        {
                            final Long id = bulletin.getId();
                            bulletin.setViewProperty("course", StringUtils.join(courseMap.get(id), '\n'));
                            bulletin.setViewProperty("groups", StringUtils.join(groupMap.get(id), '\n'));
                            bulletin.setViewProperty("currGroups", StringUtils.join(currGroupMap.get(id), '\n'));
                            bulletin.setViewProperty("comission", StringUtils.join(comissionMap.get(id), '\n'));
                            bulletin.setViewProperty("studentCount", studentCountMap.get(id));
                        }

                    }
                }
        );
    }

    protected void applyFilters(Model model, DQLSelectBuilder dql, String alias)
    {

    }


    @Override
    public void deleteBulletin(final Long id)
    {
        if (ISessionBulletinDAO.instance.get().findBulletinsToDelete(Collections.singleton(id)).contains(id))
        {
            delete(id);
        } else
        {
            throw new ApplicationException("Невозможно удалить ведомость (она закрыта, или в ней уже есть оценки).");
        }
    }

    private void createPage(final DynamicListDataSource<SessionBulletinDocument> dataSource, final DQLSelectBuilder dql, final PropertyPath path)
    {
        List<Long> ids = getComparedIds(dql, path);
        if (dataSource.getEntityOrder().getDirection() == OrderDirection.desc)
        {
            Collections.reverse(ids);
        }

        dataSource.setTotalSize(ids.size());

        final int startRow = (int) dataSource.getStartRow();
        final int countRow = (int) dataSource.getCountRow();
        ids = ids.subList(startRow, Math.min(startRow + countRow, ids.size()));

        dataSource.createPage(sort(getList(SessionBulletinDocument.class, "id", ids), ids));
    }

    private List<Long> getComparedIds(DQLSelectBuilder dql, final PropertyPath propertyPath)
    {
        final Session session = getSession();
        dql = joinRelations(dql);
        dql.column(property("b", "id"));
        dql.column(property(propertyPath.fromAlias("rel")));
        dql.predicate(DQLPredicateType.distinct);
        List<Long> sortedIds = getSortedIds(dql.createStatement(session).<Object[]>list(), 1);
        Set<Long> uniqueIds = new LinkedHashSet<>(sortedIds);
        sortedIds.clear();
        sortedIds.addAll(uniqueIds);
        return sortedIds;
    }

    private DQLSelectBuilder getRootDql(final Model model)
    {
        final DQLSelectBuilder rootDql = order.buildDQLSelectBuilder();
        rootDql.where(eq(property(SessionBulletinDocument.sessionObject().orgUnit().fromAlias("b")), value(model.getOrgUnit())));
        rootDql.where(eq(property(SessionBulletinDocument.sessionObject().educationYear().fromAlias("b")), value(model.getYear())));
        rootDql.where(eq(property(SessionBulletinDocument.sessionObject().yearDistributionPart().fromAlias("b")), value(model.getPart())));
        return rootDql;
    }

    protected DQLSelectBuilder joinRelations(final DQLSelectBuilder dql)
    {
        // TODO: переделать

        dql.joinEntity("b", DQLJoinType.left, SessionDocumentSlot.class, "s", eq(
                property(SessionDocumentSlot.document().fromAlias("s")), property("b")
        ));
        dql.joinEntity("s", DQLJoinType.left, EppRealEduGroup4ActionTypeRow.class, "rel", and(
                isNull(property(EppRealEduGroup4ActionTypeRow.removalDate().fromAlias("rel"))),
                eq(property(EppRealEduGroup4ActionTypeRow.studentWpePart().fromAlias("rel")), property(SessionDocumentSlot.studentWpeCAction().id().fromAlias("s")))
        ));
        return dql;
    }

    @Override
    public void doPrintBulletin(final Long id)
    {
        final SessionBulletinDocument bulletin = get(SessionBulletinDocument.class, id);
        if (bulletin.isClosed())
        {
            final SessionDocumentPrintVersion rel = get(SessionDocumentPrintVersion.class, SessionDocumentPrintVersion.doc().id().s(), id);
            if (null != rel)
            {
                final byte[] content = rel.getContent();
                BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().document(content).fileName("Ведомость.rtf"), true);
                return;
            }
        }
        final RtfDocument document = ISessionBulletinPrintDAO.instance.get().printBulletin(id);
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("Ведомость.rtf").document(document), true);
    }
}
