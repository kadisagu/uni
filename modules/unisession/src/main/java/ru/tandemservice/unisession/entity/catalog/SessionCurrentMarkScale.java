package ru.tandemservice.unisession.entity.catalog;

import com.google.common.collect.ImmutableList;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.BaseDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import ru.tandemservice.unisession.attestation.bo.CurrentMarkScale.CurrentMarkScaleManager;
import ru.tandemservice.unisession.attestation.bo.CurrentMarkScale.ui.CatalogItemPub.CurrentMarkScaleCatalogItemPub;
import ru.tandemservice.unisession.entity.catalog.gen.SessionCurrentMarkScaleGen;

import java.util.Collection;

/**
 * Шкала оценок текущего контроля
 */
public class SessionCurrentMarkScale extends SessionCurrentMarkScaleGen implements IDynamicCatalogItem
{
    public static IDynamicCatalogDesc getUiDesc()
    {
        return new BaseDynamicCatalogDesc()
        {
            @Override
            public String getViewComponentName()
            {
                return CurrentMarkScaleCatalogItemPub.class.getSimpleName();
            }

            @Override
            public void addAdditionalColumns(DynamicListDataSource<ICatalogItem> dataSource)
            {
                dataSource.addColumn(
                        new ToggleColumn("Активная", SessionCurrentMarkScale.P_ACTIVE)
                                .toggleOnListener(ChangeActiveScaleAction.NAME)
                                .toggleOffListener(ChangeActiveScaleAction.NAME)
                                .setPermissionKey("catalogItem_edit_" + SessionCurrentMarkScale.ENTITY_NAME)
                );
            }

            @Override
            public Collection<String> getHiddenFields()
            {
                return ImmutableList.of(SessionCurrentMarkScale.P_ACTIVE);
            }

            @Override
            public IDeleteActionListener getDeleteActionListener()
            {
                return catalogItemId -> CurrentMarkScaleManager.instance().dao().deleteCurrentScale(catalogItemId);
            }
        };
    }

    public static class ChangeActiveScaleAction extends NamedUIAction
    {
        public static final String NAME = "onChangeActiveScale";

        public ChangeActiveScaleAction()
        {
            super(NAME);
        }

        @Override
        public void execute(final IUIPresenter presenter)
        {
            CurrentMarkScaleManager.instance().dao().invertScaleActiveProperty(presenter.getListenerParameterAsLong());
            presenter.getSupport().doRefresh();
        }
    }
}