/* $Id: SessionReportResultsAddUI.java 21854 2012-02-10 11:12:10Z oleyba $ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.ResultsByDiscAdd;

import com.google.common.collect.ImmutableList;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilter;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilterType;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterFormConfig;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.catalog.bo.StudentCatalogs.StudentCatalogsManager;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.catalog.EppWorkPlanRowKind;
import ru.tandemservice.uniepp.entity.catalog.codes.EppWorkPlanRowKindCodes;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.ResultsByDiscPub.SessionReportResultsByDiscPub;
import ru.tandemservice.unisession.base.bo.SessionReport.util.UniSessionFilterAddon;
import ru.tandemservice.unisession.entity.report.UnisessionResultsByDiscReport;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @author oleyba
 * @since 2/7/12
 */
@Input
({
    @Bind(key = SessionReportManager.BIND_ORG_UNIT, binding = "ouHolder.id")
})
public class SessionReportResultsByDiscAddUI extends UIPresenter
{
    private OrgUnitHolder ouHolder = new OrgUnitHolder();

    private Date performDateFrom;
    private Date performDateTo;

    private boolean performDateFromActive;
    private boolean performDateToActive;

    private EducationYear year;
    private YearDistributionPart part;

    private GroupingWrapper grouping;

    private boolean useFullDiscTitle;

    private ISelectModel groupingModel = new LazySimpleSelectModel<>(ImmutableList.of(
            new IdentifiableWrapper(0L, "по курсам"),
            new IdentifiableWrapper(1L, "по индексам дисциплин")));
    private ISelectModel territorialOrgUnitModel;

    private boolean studentStatusInitialized;

    @Override
    public void onComponentActivate()
    {
        List<ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscGrouping> groupingList = new ArrayList<>(SessionReportManager.instance().sessionReportResultsByDiscGroupingExtPoint().getItems().values());
        Collections.sort(groupingList);
        List<GroupingWrapper> wrappers = new ArrayList<>();
        for (ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscGrouping g : groupingList)
            wrappers.add(new GroupingWrapper(g));
        setGroupingModel(new LazySimpleSelectModel<>(wrappers));
    }

    @Override
    public void onComponentRefresh()
    {
        getOuHolder().refresh();

        setYear(EducationYear.getCurrentRequired());

        configUtil(getSessionFilterAddon());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(SessionReportManager.PARAM_ORG_UNIT, getOrgUnit());
        dataSource.put(SessionReportManager.PARAM_EDU_YEAR, getYear());
        dataSource.put(SessionReportManager.PARAM_EDU_YEAR_ONLY_CURRENT, Boolean.FALSE);
        dataSource.put(StudentCatalogsManager.PARAM_STUDENT_STATUS_ONLY_ACTIVE, Boolean.TRUE);
    }

    public void onClickApply() throws Exception
    {
        validate();
        if (null == getGrouping())
            throw new ApplicationException("Поле «Сведения об успеваемости» обязательно для заполнения.");

        UnisessionResultsByDiscReport report = ISessionReportResultsByDiscDAO.instance.get().createStoredReport(this, getGrouping().getGrouping());

        this.deactivate();
        _uiActivation.asDesktopRoot(SessionReportResultsByDiscPub.class)
                .parameter(UIPresenter.PUBLISHER_ID, report.getId())
                .activate();
    }

    public void validate()
    {
        if (performDateFromActive && performDateToActive && (performDateFrom.getTime() - performDateTo.getTime() > 0))
            _uiSupport.error("Дата, указанная в параметре \"Дата сдачи мероприятия в ведомости с\" не должна быть позже даты в параметре \"Дата сдачи мероприятия в ведомости по\".", "dateFrom");
        if(getUserContext().getErrorCollector().hasErrors())
            throw new ApplicationException();
    }

    // presenters

    public OrgUnit getOrgUnit()
    {
        return getOuHolder().getValue();
    }

    // getters and setters


    public ISelectModel getGroupingModel()
    {
        return groupingModel;
    }

    public void setGroupingModel(ISelectModel groupingModel)
    {
        this.groupingModel = groupingModel;
    }

    public OrgUnitHolder getOuHolder()
    {
        return ouHolder;
    }

    public boolean isStudentStatusInitialized()
    {
        return studentStatusInitialized;
    }

    public void setStudentStatusInitialized(boolean studentStatusInitialized)
    {
        this.studentStatusInitialized = studentStatusInitialized;
    }

    public GroupingWrapper getGrouping()
    {
        return grouping;
    }

    public void setGrouping(GroupingWrapper grouping)
    {
        this.grouping = grouping;
    }

    public ISelectModel getTerritorialOrgUnitModel()
    {
        return territorialOrgUnitModel;
    }

    public void setTerritorialOrgUnitModel(ISelectModel territorialOrgUnitModel)
    {
        this.territorialOrgUnitModel = territorialOrgUnitModel;
    }

    public boolean isShowYearNotCurrentInfo()
    {
        return getYear().getCurrent() == null || !getYear().getCurrent();
    }

    public EducationYear getYear() {
        return year;
    }

    public void setYear(EducationYear year) {
        this.year = year;
    }

    public YearDistributionPart getPart() {
        return part;
    }

    public void setPart(YearDistributionPart part) {
        this.part = part;
    }

    public boolean isUseFullDiscTitle() {
        return useFullDiscTitle;
    }

    public void setUseFullDiscTitle(boolean useFullDiscTitle) {
        this.useFullDiscTitle = useFullDiscTitle;
    }

    public boolean isPerformDateToActive() {
        return performDateToActive;
    }

    public void setPerformDateToActive(boolean performDateToActive) {
        this.performDateToActive = performDateToActive;
    }

    public boolean isPerformDateFromActive() {
        return performDateFromActive;
    }

    public void setPerformDateFromActive(boolean performDateFromActive) {
        this.performDateFromActive = performDateFromActive;
    }

    public Date getPerformDateTo() {
        return performDateTo;
    }

    public void setPerformDateTo(Date performDateTo) {
        this.performDateTo = performDateTo;
    }

    public Date getPerformDateFrom() {
        return performDateFrom;
    }

    public void setPerformDateFrom(Date performDateFrom) {
        this.performDateFrom = performDateFrom;
    }

    // inner classes

    private static class GroupingWrapper extends IdentifiableWrapper
    {
        private ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscGrouping grouping;

        private GroupingWrapper(ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscGrouping grouping)
        {
            super((long) grouping.getKey().intern().hashCode(), grouping.getTitle());
            this.grouping = grouping;
        }

        public ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscGrouping getGrouping()
        {
            return grouping;
        }
    }


    // for filter addon

    public UniSessionFilterAddon getSessionFilterAddon()
    {
        return (UniSessionFilterAddon) getConfig().getAddon(CommonFilterAddon.class.getSimpleName());
    }

    public void onChangeYearOrPart()
    {
        configUtilWhere(getSessionFilterAddon());
    }


    private void configUtil(UniSessionFilterAddon util)
    {
        util
                .configDoubleWidthFilters(false)
                .configUseEnableCheckbox(true)
                .configSettings(getSettingsKey());


        util.clearFilterItems();

        final boolean hasOu = null != getOrgUnit();

        util

                .addFilterItem(UniSessionFilterAddon.DISC_KIND, new CommonFilterFormConfig(true, true, true, false, true, true))
                .addFilterItem(UniSessionFilterAddon.COMPENSATION_TYPE, CommonFilterFormConfig.DEFAULT_CHECKBOX_SELECT_FILTER_CONFIG)
                .addFilterItem(UniSessionFilterAddon.GROUP_ORG_UNIT, new CommonFilterFormConfig(true, false, true, hasOu, hasOu, hasOu))
                .addFilterItem(UniSessionFilterAddon.TERRITORIAL_ORG_UNIT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(UniSessionFilterAddon.DEVELOP_FORM, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(UniSessionFilterAddon.DEVELOP_CONDITION, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(UniSessionFilterAddon.DEVELOP_TECH, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(UniSessionFilterAddon.DEVELOP_PERIOD, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(UniSessionFilterAddon.COURSE, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(UniSessionFilterAddon.STUDENT_STATUS, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(UniSessionFilterAddon.STUDENT_CUSTOM_STATE, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(UniSessionFilterAddon.TARGET_ADMISSION, CommonFilterFormConfig.DEFAULT_CHECKBOX_SELECT_FILTER_CONFIG);
        if (hasOu) {
            util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EppStudentWpeCAction.studentWpe().student().educationOrgUnit().formativeOrgUnit(), getOrgUnit()));
            util.getFilterItem(UniSessionFilterAddon.SETTINGS_NAME_GROUP_ORG_UNIT).setValue(getOuHolder().getValue());
        }

        // дефолтные значения в фильтры
        final EppWorkPlanRowKind main = DataAccessServices.dao().getByCode(EppWorkPlanRowKind.class, EppWorkPlanRowKindCodes.MAIN);
        final List<EppWorkPlanRowKind> value = new ArrayList<>();
        value.add(main);
        util.getFilterItem(UniSessionFilterAddon.SETTINGS_NAME_DISC_KIND).setValue(value);


        util.saveSettings();
        configUtilWhere(util);
    }

    public void configUtilWhere(UniSessionFilterAddon util)
    {
        util.clearWhereFilter();

        util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EppStudentWpeCAction.studentWpe().student().status().active(), true));
        if (null != getOrgUnit())
            util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EppStudentWpeCAction.studentWpe().student().educationOrgUnit().formativeOrgUnit(), getOrgUnit()));


        if (null != year) util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EppStudentWpeCAction.studentWpe().year().educationYear(), year));
        if (null != part) util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EppStudentWpeCAction.studentWpe().part(), part));
    }

}
