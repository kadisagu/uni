/* $*/

package ru.tandemservice.unisession.base.bo.SessionMark.logic;

import com.google.common.collect.ImmutableList;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.access.IEntityAccessSemaphore;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.util.concurrent.semaphore.IThreadSemaphore;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.DaoCache;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.base.bo.SessionMark.event.SessionRegularMarkFiledAndCommissionChecker;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.brs.dao.SessionBrsDao;
import ru.tandemservice.unisession.dao.document.ISessionDocumentBaseDAO;
import ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowed;
import ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowedForBulletin;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkStateCatalogItem;
import ru.tandemservice.unisession.entity.catalog.codes.SessionMarkStateCatalogItemCodes;
import ru.tandemservice.unisession.entity.catalog.gen.SessionMarkCatalogItemGen;
import ru.tandemservice.unisession.entity.document.*;
import ru.tandemservice.unisession.entity.mark.*;
import ru.tandemservice.unisession.entity.mark.gen.SessionMarkGen;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 2/24/11
 */
public class SessionMarkDAO extends UniBaseDao implements ISessionMarkDAO
{

    @Override
    public Map<SessionDocumentSlot, ISelectModel> getMarkModelMap(Collection<SessionDocumentSlot> slots)
    {
        // разбиваем все слоты на группы по документам
        Map<SessionDocument, Collection<SessionDocumentSlot>> slotFactorMap = new HashMap<>();
        for (SessionDocumentSlot slot : slots)
        {
            SafeMap.safeGet(slotFactorMap, slot.getDocument(), ArrayList.class).add(slot);
        }

        // далее - обрабатываем каждый документ отдельно
        Map<SessionDocumentSlot, ISelectModel> result = new HashMap<>(slots.size());
        for (Map.Entry<SessionDocument, Collection<SessionDocumentSlot>> e : slotFactorMap.entrySet())
        {

            Collection<SessionDocumentSlot> documentSlots = e.getValue();
            Map<Long, SessionMarkCatalogItem> currentValueMap = getCurrentValueMap(documentSlots);
            Set<Long> disallowStudentSet = getDisallowedStudents(e.getKey());
            
            for (SessionDocumentSlot slot : documentSlots)
            {
                boolean disallow = disallowStudentSet.contains(slot.getActualStudent().getId());
                ISessionBrsDao.ISessionRatingSettings ratingSettings = ISessionBrsDao.instance.get().getRatingSettings(ISessionBrsDao.instance.get().key(slot));

                SessionMarkCatalogItem currentValue = currentValueMap.get(slot.getId());
                if (ratingSettings.usePoints() && !(currentValue instanceof SessionMarkStateCatalogItem))
                    currentValue = null;

                ISelectModel model = disallow ? prepareDisallowModel(currentValue) : prepareMarkModel(slot.getStudentWpeCAction(), currentValue, false, ratingSettings, false);
                result.put(slot, model);
            }
        }
        return result;
    }

    @Override
    public Set<Long> getDisallowedStudents(SessionDocument document)
    {
        Set<Long> disallowStudentSet = new HashSet<>();

        // для ведомостей смотрим на недопуск, выставленный в ведомости и на деканате
        if (document instanceof SessionBulletinDocument)
        {
            SessionBulletinDocument bulletin = (SessionBulletinDocument) document;

            // все, кро не допущен в ведомости
            disallowStudentSet.addAll(
                    new DQLSelectBuilder()
                    .fromEntity(SessionStudentNotAllowedForBulletin.class, "x").column(property(SessionStudentNotAllowedForBulletin.student().id().fromAlias("x")))
                    .where(eq(property(SessionStudentNotAllowedForBulletin.bulletin().fromAlias("x")), value(bulletin)))
                    .where(isNull(SessionStudentNotAllowedForBulletin.removalDate().fromAlias("x")))
                    .createStatement(getSession()).<Long>list()
            );
        }

        // все, кто не допущен в сессию
        if (document instanceof SessionObject.ISessionObjectDependentDocument)
        {
            SessionObject.ISessionObjectDependentDocument dependent = (SessionObject.ISessionObjectDependentDocument) document;
            SessionObject sessionObject = dependent.getSessionObject();

            disallowStudentSet.addAll(
                    new DQLSelectBuilder()
                    .fromEntity(SessionStudentNotAllowed.class, "x").column(property(SessionStudentNotAllowed.student().id().fromAlias("x")))
                    .where(eq(property(SessionStudentNotAllowed.session().fromAlias("x")), value(sessionObject)))
                    .where(isNull(SessionStudentNotAllowed.removalDate().fromAlias("x")))
                    .createStatement(getSession()).<Long>list()
            );
        }

        // смотрим на наличие допуска по БРС

        // проверка, что текущий рейтинг уже вычислен вместе с допуском, и объекты SessionSlotRatingData есть,
        // должна быть выполнена перед выставлением оценок в документ в интерфейсе, если документ этого требует

        List<SessionSlotRatingData> ratingDataList = getList(SessionSlotRatingData.class, SessionSlotRatingData.slot().document(), document);
        for (SessionSlotRatingData ratingData : ratingDataList) {
            ISessionBrsDao.ISessionRatingSettings ratingSettings = ISessionBrsDao.instance.get().getRatingSettings(ISessionBrsDao.instance.get().key(ratingData.getSlot()));
            if (!ratingData.isAllowed() && ratingSettings.useCurrentRating())
                disallowStudentSet.add(ratingData.getSlot().getActualStudent().getId());
        }

        return disallowStudentSet;
    }

    @Override
    public ISelectModel prepareSimpleMarkModel(EppStudentWpeCAction slot, SessionMarkCatalogItem currentValue, boolean gradesOnly)
    {
        return prepareMarkModel(slot, currentValue, gradesOnly, SessionBrsDao.getSettingsForDisabledBrs(null), false);
    }

    @Override
    public ISelectModel prepareSimpleMarkModelWithShortTitles(EppStudentWpeCAction slot, SessionMarkCatalogItem currentValue, boolean gradesOnly)
    {
        return prepareMarkModel(slot, currentValue, gradesOnly, SessionBrsDao.getSettingsForDisabledBrs(null), true);
    }

    @Override
    public ISelectModel prepareMarkModel(OrgUnit groupOu, EppStudentWpeCAction slot, SessionMarkCatalogItem currentValue, boolean gradesOnly)
    {
        ISessionBrsDao.ISessionRatingSettings ratingSettings = ISessionBrsDao.instance.get().getRatingSettings(ISessionBrsDao.instance.get().key(groupOu, slot));
        return prepareMarkModel(slot, currentValue, gradesOnly, ratingSettings, false);
    }

    @Override
    @Deprecated
    @SuppressWarnings("deprecation")
    public SessionMark saveOrUpdateMark(SessionDocumentSlot slot, final SessionMarkCatalogItem markValue, final Date performDate)
    {
        return saveOrUpdateMark(slot, new MarkData() {
            @Override public Date getPerformDate() { return performDate; }
            @Override public Double getPoints() { return null; }
            @Override public SessionMarkCatalogItem getMarkValue() { return markValue; }
            @Override public String getComment() { return null; }
        });
    }

    @Override
    public SessionMark saveOrUpdateMark(SessionDocumentSlot slot, MarkData markData)
    {
        SessionMarkCatalogItem markValue = markData.getMarkValue();
        String markComment = markData.getComment();
        Date performDate = markData.getPerformDate();

        if (slot.getDocument() instanceof SessionStudentGradeBookDocument)
        {
            // в зачетки явно выставлять оценки запрещено (там всегда должны быть ссылки)
            throw new IllegalStateException();
        }

        Session session = getSession();
        SessionMark previous = getByNaturalId(new SessionMarkGen.NaturalId(slot));

        if (previous instanceof SessionSlotLinkMark)
        {
            if (previous.getValueItem().equals(markValue))
            {
                return saveMark(slot, previous.getPerformDate(), previous, markComment);
            }
        }

        if (markValue instanceof SessionMarkGradeValueCatalogItem)
        {
            if (previous instanceof SessionSlotMarkGradeValue)
            {
                SessionSlotMarkGradeValue previousGradeValue = (SessionSlotMarkGradeValue) previous;
                previousGradeValue.setValue((SessionMarkGradeValueCatalogItem) markValue);
                previousGradeValue.setPoints(markData.getPoints());
                return saveMark(slot, performDate, previous, markComment);
            }

            if (null != previous)
            {
                session.delete(previous);
                session.flush(); // удаляем прямо сейчас (иначе duplicate-value)
            }
            return saveMark(slot, performDate, new SessionSlotMarkGradeValue(slot, (SessionMarkGradeValueCatalogItem) markValue, markData.getPoints()), markComment);
        }

        if (markValue instanceof SessionMarkStateCatalogItem) {
            if (previous instanceof SessionSlotMarkState)
            {
                ((SessionSlotMarkState) previous).setValue((SessionMarkStateCatalogItem) markValue);
                return saveMark(slot, performDate, previous, markComment);
            }

            if (null != previous)
            {
                session.delete(previous);
                session.flush(); // удаляем прямо сейчас (иначе duplicate-value)
            }
            return saveMark(slot, performDate, new SessionSlotMarkState(slot, (SessionMarkStateCatalogItem) markValue), markComment);
        }

        if (null != previous)
        {
            session.delete(previous);
        }
        return null;
    }

    // TODO: XXX:
    @Override
    public void deleteUnactualMark(Long id)
    {
        IEntity entity = get(id);
        if (!(entity instanceof SessionSlotRegularMark))
            throw new IllegalStateException();

        SessionSlotRegularMark mark = (SessionSlotRegularMark) entity;

        if (!(mark.getSlot().getDocument() instanceof SessionListDocument))
            throw new ApplicationException("Неактуальные оценки можно удалять только из экзаменационных карточек.");

        if (null == mark.getSlot().getStudentWpeCAction().getRemovalDate())
            throw new ApplicationException("Можно удалять, как неактуальные, только оценки по неактуальным МСРП.");

        // усыпляем листенеры проверки консистентности отметок
        final IThreadSemaphore lock = SessionRegularMarkFiledAndCommissionChecker.lock();

        try
        {
            delete(mark);
            getSession().flush();
            delete(mark.getSlot());
        }
        finally
        {
            // будим обратно листенеры проверки консистентности отметок
            lock.release();
        }
    }

    protected SessionMark saveMark(SessionDocumentSlot slot, Date performDate, SessionMark mark, String comment)
    {
        IEntityAccessSemaphore semaphore = CoreServices.entityAccessService().createSemaphore();
        try
        {
            semaphore.allowUpdate(SessionMark.class, true);
            semaphore.allowInsert(SessionMark.class, true);

            ISessionBrsDao.ISessionRatingSettings ratingSettings = ISessionBrsDao.instance.get().getRatingSettings(ISessionBrsDao.instance.get().key(slot));
            if ((ratingSettings.useCurrentRating() || ratingSettings.usePoints()) && !(
                    slot.getDocument() instanceof SessionTransferProtocolDocument ||
                    slot.getDocument() instanceof SessionBulletinDocument ||
                    slot.getDocument() instanceof SessionRetakeDocument ||
                    slot.getDocument() instanceof SessionSheetDocument ||
                    slot.getDocument() instanceof SessionTransferDocument ||
                    slot.getDocument() instanceof SessionListDocument ||
                    slot.getDocument() instanceof SessionGlobalDocument
            ))
                throw new ApplicationException("Выставление оценок в сессии по правилам БРС в данный документ не реализовано. Обратитесь к администратору.");

            mark.setComment(comment);
            mark.setCommission(slot.getCommission());
            mark.setPerformDate(performDate);
            saveOrUpdate(mark);
            getSession().flush();
            return mark;
        } finally
        {
            semaphore.release();
        }
    }
    
    private ISelectModel prepareMarkModel(EppStudentWpeCAction slot, SessionMarkCatalogItem currentValue, boolean gradesOnly, ISessionBrsDao.ISessionRatingSettings ratingSettings, boolean showShortTitle)
    {
        if (gradesOnly && (ratingSettings != null && ratingSettings.usePoints())) {
            return new LazySimpleSelectModel<>(ImmutableList.of());
        }

        if (null == slot || null == slot.getType())
        {
            return new LazySimpleSelectModel<>(ImmutableList.of());
        }

        // сначала все оценки (из шкалы слота)
        EppGradeScale scale = ISessionDocumentBaseDAO.instance.get().getGradeScale(slot);
        List<SessionMarkGradeValueCatalogItem> scaleMarks = getGradeScaleMarkMap().get(scale.getId());
        if (CollectionUtils.isEmpty(scaleMarks)) {
            throw new ApplicationException("В шкале оценок отсутствуют оценки.");
        }

        if (gradesOnly)
        {
            // формируем модель только для оценок
            return new LazySimpleSelectModel<>(scaleMarks);
        }

        // затем к оценкам из шкалы добавляем все отметки о неявке (либо видимые, либо текущее значение)
        List<SessionMarkCatalogItem> marks = (ratingSettings != null && ratingSettings.usePoints()) ? new ArrayList<>() : new ArrayList<>(scaleMarks);
        for (SessionMarkStateCatalogItem item : getStateMarkList())
        {
            if (item.isVisible() || item.equals(currentValue))
            {
                marks.add(item);
            }
        }

        // формируем модель (для оценок и отметок)
        if(showShortTitle)
            return new LazySimpleSelectModel<>(marks, "shortTitle");
        else
            return new LazySimpleSelectModel<>(marks);
    }

    /**
     * cached
     */
    protected Map<Long, List<SessionMarkGradeValueCatalogItem>> getGradeScaleMarkMap()
    {
        String key = "SessionMarkGradeValueCatalogItem.map";
        Map<Long, List<SessionMarkGradeValueCatalogItem>> result = DaoCache.get(key);
        if (null == result)
        {
            result = new HashMap<>(4);
            for (SessionMarkGradeValueCatalogItem item : getList(SessionMarkGradeValueCatalogItem.class, SessionMarkGradeValueCatalogItem.P_PRIORITY))
            {
                SafeMap.safeGet(result, item.getScale().getId(), ArrayList.class).add(item);
            }
            DaoCache.put(key, result = Collections.unmodifiableMap(result));
        }
        return result;
    }

    /**
     * cached
     */
    protected SessionMarkStateCatalogItem getNotAllowedState()
    {
        String key = "SessionMarkStateCatalogItem.not-allowed";
        SessionMarkStateCatalogItem result = DaoCache.get(key);
        if (null == result)
        {
            DaoCache.put(
                    key,
                    result = this.getByNaturalId(
                            new SessionMarkCatalogItemGen.NaturalId(
                                    SessionMarkStateCatalogItemCodes.NO_ADMISSION,
                                    SessionMarkStateCatalogItem.CATALOG_CODE
                            )
                    )
            );
        }
        return result;
    }

    /**
     * cached
     */
    protected List<SessionMarkStateCatalogItem> getStateMarkList()
    {
        String key = "SessionMarkStateCatalogItem.list";
        List<SessionMarkStateCatalogItem> result = DaoCache.get(key);
        if (null == result)
        {
            DaoCache.put(
                    key,
                    result = Collections.unmodifiableList(
                            getList(
                                    SessionMarkStateCatalogItem.class,
                                    SessionMarkStateCatalogItem.P_PRIORITY
                            )
                    )
            );
        }
        return result;
    }

    protected Map<Long, SessionMarkCatalogItem> getCurrentValueMap(Collection<SessionDocumentSlot> documentSlots)
    {
        List<SessionMark> marks = new DQLSelectBuilder()
        .fromEntity(SessionMark.class, "mk").column(property("mk"))
        .where(in(property(SessionMark.slot().fromAlias("mk")), documentSlots))
        .createStatement(getSession()).list();

        Map<Long, SessionMarkCatalogItem> currentValueMap = new HashMap<>(marks.size());
        for (SessionMark mk : marks)
        {
            currentValueMap.put(mk.getSlot().getId(), mk.getValueItem());
        }
        return currentValueMap;
    }

    protected ISelectModel prepareDisallowModel(SessionMarkCatalogItem currentValue)
    {
        SessionMarkStateCatalogItem notAllowed = getNotAllowedState();
        if (null == currentValue)
        {
            return new LazySimpleSelectModel<>(ImmutableList.of(notAllowed));
        }
        return new LazySimpleSelectModel<>(ImmutableList.of(currentValue, notAllowed));
    }
}

