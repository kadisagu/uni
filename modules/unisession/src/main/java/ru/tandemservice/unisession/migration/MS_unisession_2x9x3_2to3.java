package ru.tandemservice.unisession.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unisession_2x9x3_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.3")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность sessionTransferProtocolDocument

		//создаем колонку approved
		tool.createColumn("session_doc_tp_t", new DBColumn("approved_p", DBType.BOOLEAN));

		//заменяем значения, согластно approveDate
		tool.executeUpdate("update session_doc_tp_t set approved_p=? where approvedate_p is not null", true);
		tool.executeUpdate("update session_doc_tp_t set approved_p=? where approved_p is null", false);

		// сделать колонку NOT NULL
		tool.setColumnNullable("session_doc_tp_t", "approved_p", false);

		// удаление колонки approvedate
		tool.dropColumn("session_doc_tp_t", "approvedate_p");

    }
}