/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReport.util.results;

import com.google.common.collect.Collections2;
import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.ResultsAdd.ISessionReportResultsDAO;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
* @author oleyba
* @since 2/13/12
*/
public class EduLevelAndCourseReportTable extends ReportTable
{
    private EduProgramSubject subject;

    public EduLevelAndCourseReportTable(final EduProgramSubject subject, Collection<ISessionReportResultsDAO.ISessionResultsReportStudentData> students)
    {
        this.subject = subject;
        Set<Course> courses = new HashSet<>(Collections2.transform(
                Collections2.filter(students, input -> input.getLevel().getEducationLevel().getEduProgramSubject().equals(subject)),
                ISessionReportResultsDAO.ISessionResultsReportStudentData::getCourse));
        for (final Course course : courses) {
            rowList.add(new ReportRow(course.getTitle(), CollectionUtils.select(students, input -> input.getCourse().equals(course) && input.getLevel().getEducationLevel().getEduProgramSubject().equals(subject))));
        }
        Collections.sort(rowList, ITitled.TITLED_COMPARATOR);
        rowList.add(totalsRow(students));
    }

    @Override
    protected ReportRow totalsRow(Collection<ISessionReportResultsDAO.ISessionResultsReportStudentData> students)
    {
        return new ReportRow("Итого", CollectionUtils.select(students, input -> input.getLevel().getEducationLevel().getEduProgramSubject().equals(subject)));
    }

    @Override
    public void modify(RtfDocument rtf)
    {
        RtfInjectModifier modifier = new RtfInjectModifier();
        modifier.put("groupTitle", "Курс");
        modifier.put("Title", "Курс");
        modifier.put("criterion", "направлениям");
        modifier.put("eduLevelType", "");
        modifier.put("eduLevelTitle", subject.getTitleWithCode());
        modifier.modify(rtf);
    }

    public EduProgramSubject getSubject()
    {
        return subject;
    }
}
