/**
 *$Id$
 */
package ru.tandemservice.unisession.base.bo.SessionSheet;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unisession.base.bo.SessionSheet.logic.ISessionSheetForming;
import ru.tandemservice.unisession.base.bo.SessionSheet.logic.SessionSheetFormingDao;

/**
 * @author Alexander Shaburov
 * @since 15.02.13
 */
@Configuration
public class SessionSheetManager extends BusinessObjectManager
{
    public static SessionSheetManager instance()
    {
        return instance(SessionSheetManager.class);
    }

    @Bean
    public ISessionSheetForming formingDao()
    {
        return new SessionSheetFormingDao();
    }
}
