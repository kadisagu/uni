package ru.tandemservice.unisession.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Оценка (из шкалы оценок)"
 * Имя сущности : sessionMarkGradeValueCatalogItem
 * Файл data.xml : session.data.xml
 */
public interface SessionMarkGradeValueCatalogItemCodes
{
    /** Константа кода (code) элемента : Отлично (title) */
    String OTLICHNO = "scale5.5";
    /** Константа кода (code) элемента : Хорошо (title) */
    String HOROSHO = "scale5.4";
    /** Константа кода (code) элемента : Удовлетворительно (title) */
    String UDOVLETVORITELNO = "scale5.3";
    /** Константа кода (code) элемента : Неудовлетворительно (title) */
    String NEUDOVLETVORITELNO = "scale5.2";
    /** Константа кода (code) элемента : Зачтено (title) */
    String ZACHTENO = "scale2.5";
    /** Константа кода (code) элемента : Не зачтено (title) */
    String NE_ZACHTENO = "scale2.2";

    Set<String> CODES = ImmutableSet.of(OTLICHNO, HOROSHO, UDOVLETVORITELNO, NEUDOVLETVORITELNO, ZACHTENO, NE_ZACHTENO);
}
