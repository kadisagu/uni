/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.FinalQualWorkResultPub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.StateFinalAttestationResultPub.SessionReportStateFinalAttestationResultPubUI;
import ru.tandemservice.unisession.entity.report.FinalQualWorkResult;

/**
 * @author Andrey Andreev
 * @since 03.11.2016
 */
@State({@Bind(key = UIPresenter.PUBLISHER_ID, binding = "report.id"),
        @Bind(key = SessionReportManager.BIND_ORG_UNIT, binding = "ouHolder.id")})
public class SessionReportFinalQualWorkResultPubUI extends SessionReportStateFinalAttestationResultPubUI<FinalQualWorkResult>
{
    @Override
    public FinalQualWorkResult initReport()
    {
        return new FinalQualWorkResult();
    }

    @Override
    public String getViewPermissionKey()
    {
        return getSec().getPermission(getOuHolder().getId() != null ? "orgUnit_viewFinalQualWorkResultList" : "finalQualWorkResultReport");
    }

    @Override
    public String getDeletePermissionKey()
    {
        return getSec().getPermission(getOuHolder().getId() != null ? "orgUnit_deleteFinalQualWorkResult" : "deleteSessionStorableReport");
    }
}
