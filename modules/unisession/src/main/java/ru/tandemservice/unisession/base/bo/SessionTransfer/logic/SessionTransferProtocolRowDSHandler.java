/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionTransfer.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm;
import ru.tandemservice.unisession.base.bo.SessionReexamination.SessionReexaminationManager;
import ru.tandemservice.unisession.base.bo.SessionTransfer.ui.ProtocolPub.SessionTransferProtocolPubUI;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.document.SessionTransferProtocolDocument;
import ru.tandemservice.unisession.entity.document.SessionTransferProtocolMark;
import ru.tandemservice.unisession.entity.document.SessionTransferProtocolRow;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Alexey Lopatin
 * @since 16.09.2015
 */
public class SessionTransferProtocolRowDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String PROP_RE_EXAM_AND_ATTESTATION = "reExamAndAttestation";
    public static final String PROP_MARK_MAP = "markMap";

    public SessionTransferProtocolRowDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        SessionTransferProtocolDocument protocol = context.get(SessionTransferProtocolPubUI.PARAM_PROTOCOL);

        List<DataWrapper> resultList = Lists.newArrayList();
        Map<SessionTransferProtocolRow, Map<EppFControlActionType, SessionMarkGradeValueCatalogItem>> protocolRowMarkMap = Maps.newHashMap();

        List<SessionTransferProtocolRow> protocolRows = DataAccessServices.dao().getList(SessionTransferProtocolRow.class, SessionTransferProtocolRow.protocol().id(), protocol.getId());

        Collections.sort(protocolRows, (o1, o2) -> {
            EppEpvRowTerm rt1 = o1.getRequestRow().getRowTerm();
            EppEpvRowTerm rt2 = o2.getRequestRow().getRowTerm();

            int result = rt1.getRow().getStoredIndex().compareTo(rt2.getRow().getStoredIndex());
            if (result == 0)
                result = Integer.compare(rt1.getTerm().getIntValue(), rt2.getTerm().getIntValue());
            return result;
        });

        List<SessionTransferProtocolMark> protocolRowMarks = DataAccessServices.dao().getList(SessionTransferProtocolMark.class, SessionTransferProtocolMark.protocolRow().protocol().id(), protocol.getId());
        for (SessionTransferProtocolMark protocolMark : protocolRowMarks)
        {
            SafeMap.safeGet(protocolRowMarkMap, protocolMark.getProtocolRow(), HashMap.class).put(protocolMark.getControlAction(), protocolMark.getMark());
        }

        Map<String, DataWrapper> itemMap = SessionReexaminationManager.instance().reexaminationDSExtPoint().getItems();
        for (SessionTransferProtocolRow protocolRow : protocolRows)
        {
            Map<EppFControlActionType, SessionMarkGradeValueCatalogItem> fcaMap = protocolRowMarkMap.get(protocolRow);
            Map<String, SessionMarkGradeValueCatalogItem> markMap = Maps.newHashMap();
            if (null != fcaMap)
            {
                for (EppFControlActionType fcaType : fcaMap.keySet())
                    markMap.put(fcaType.getFullCode(), fcaMap.get(fcaType));
            }

            DataWrapper wrapper = new DataWrapper(protocolRow.getId(), protocolRow.getRequestRow().getRegElementPart().getTitleWithNumber(), protocolRow);
            wrapper.setProperty(PROP_RE_EXAM_AND_ATTESTATION, itemMap.get(String.valueOf(protocolRow.isNeedRetake())).getTitle());
            wrapper.setProperty(PROP_MARK_MAP, markMap);

            resultList.add(wrapper);
        }
        return ListOutputBuilder.get(input, resultList).build();
    }
}