/* $Id: $ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.FqwSummaryResultList;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.FqwSummaryResultAdd.SessionReportFqwSummaryResultAdd;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.SfaSummaryResultList.SessionReportSfaSummaryResultListUI;

/**
 * @author Andrey Andreev
 * @since 27.10.2016
 */
@State({@Bind(key = SessionReportManager.BIND_ORG_UNIT, binding = "ouHolder.id")})
public class SessionReportFqwSummaryResultListUI extends SessionReportSfaSummaryResultListUI
{
    // Listeners
    @Override
    public void onClickAdd()
    {
        getActivationBuilder()
                .asDesktopRoot(SessionReportFqwSummaryResultAdd.class)
                .parameter(SessionReportManager.BIND_ORG_UNIT, getOuHolder().getId())
                .activate();
    }

    @Override
    public String getViewPermissionKey()
    {
        return getSec().getPermission(getOuHolder().getId() != null ? "orgUnit_viewFinalQualWorkSummaryResultList" : "stateFinalExamResultReport");
    }

    @Override
    public String getAddPermissionKey()
    {
        return getSec().getPermission(getOuHolder().getId() != null ? "orgUnit_addFinalQualWorkSummaryResult" : "addSessionStorableReport");
    }

    @Override
    public String getDeletePermissionKey()
    {
        return getSec().getPermission(getOuHolder().getId() != null ? "orgUnit_deleteFinalQualWorkSummaryResult" : "deleteSessionStorableReport");
    }
}
