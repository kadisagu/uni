/* $Id:$ */
package ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.BulletinAdd;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.ICommonFilterItem;
import org.tandemframework.shared.person.base.util.PersonSecurityUtil;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unisession.attestation.bo.AttestationBulletin.AttestationBulletinManager;
import ru.tandemservice.unisession.attestation.entity.report.SessionAttestationBulletinReport;
import ru.tandemservice.unisession.base.bo.SessionReport.util.SessionReportUtil;
import ru.tandemservice.unisession.base.bo.SessionReport.util.UniAttestationFilterAddon;
import ru.tandemservice.unisession.base.bo.SessionReport.util.UniSessionFilterAddon;
import ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport;

import java.util.Date;
import java.util.List;

/**
 * @author oleyba
 * @since 11/8/12
 */
public class AttestationReportBulletinPrintDAO extends UniBaseDao implements IAttestationReportBulletinPrintDAO
{
    @Override
    public Long createStoredReport(AttestationReportBulletinAddUI model)
    {
        SessionAttestationBulletinReport report = new SessionAttestationBulletinReport();
        final UniAttestationFilterAddon filterAddon = model.getAttestationFilterAddon();

        DatabaseFile content = new DatabaseFile();
        content.setContent(print(model));
        save(content);
        report.setContent(content);

        report.setFormingDate(new Date());
        report.setExecutor(PersonSecurityUtil.getExecutor());
        report.setAttestation(model.getAttestation());
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniAttestationFilterAddon.SETTINGS_NAME_COURSE, SessionAttestationBulletinReport.P_COURSE, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniAttestationFilterAddon.SETTINGS_NAME_GROUP_WITH_NOGROUP, SessionAttestationBulletinReport.P_GROUP, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniAttestationFilterAddon.SETTINGS_NAME_CUSTOM_STATE, SessionAttestationBulletinReport.P_CUSTOM_STATE, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniAttestationFilterAddon.SETTINGS_NAME_TARGET_ADMISSION, SessionAttestationBulletinReport.P_TARGET_ADMISSION, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniAttestationFilterAddon.SETTINGS_NAME_DISCIPLINE, SessionAttestationBulletinReport.P_DISCIPLINE, "title", "\n");

        save(report);

        return report.getId();
    }

    private byte[] print(AttestationReportBulletinAddUI model)
    {
        RtfDocument result = null;

        final UniAttestationFilterAddon addon = model.getAttestationFilterAddon();
        final ICommonFilterItem filterItem = addon.getFilterItem(UniAttestationFilterAddon.SETTINGS_NAME_DISCIPLINE);

        final List value = (List) filterItem.getValue();

        for (Object o : value)
        {
            if (o instanceof UniAttestationFilterAddon.DisciplineWrapper) {

                final UniAttestationFilterAddon.DisciplineWrapper wrapper = (UniAttestationFilterAddon.DisciplineWrapper) o;
                EppRegistryElementPart disc = wrapper.getValue();
                RtfDocument document = AttestationBulletinManager.instance().printDao().printBulletin(model.getAttestation(), addon, disc);
                if (null == result)
                    result = document;
                else if (null != document) {
                    RtfUtil.modifySourceList(result.getHeader(), document.getHeader(), document.getElementList());
                    result.getElementList().addAll(document.getElementList());
                }
            }
        }

        if (null != result)
            return RtfUtil.toByteArray(result);
        else
            throw new ApplicationException("Нет данных для построения отчета.");
    }
}
