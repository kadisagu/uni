package ru.tandemservice.unisession.entity.allowance;

import ru.tandemservice.unisession.entity.allowance.gen.SessionStudentNotAllowedForBulletinGen;

/**
 * Недопуск к сдаче по ведомости
 */
public class SessionStudentNotAllowedForBulletin extends SessionStudentNotAllowedForBulletinGen
{
    public boolean isNotActive()
    {
        return null != this.getRemovalDate();
    }

    public boolean isActive()
    {
        return null == this.getRemovalDate();
    }
}