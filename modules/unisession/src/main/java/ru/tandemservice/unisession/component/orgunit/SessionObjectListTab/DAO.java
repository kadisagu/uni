package ru.tandemservice.unisession.component.orgunit.SessionObjectListTab;

import org.hibernate.Session;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unisession.attestation.entity.SessionAttestation;
import ru.tandemservice.unisession.entity.document.SessionObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author vdanilov
 */
public class DAO extends UniBaseDao implements IDAO {

    private final DQLOrderDescriptionRegistry order = new DQLOrderDescriptionRegistry(SessionObject.class, "so");

    @Override
    @SuppressWarnings("unchecked")
    public void prepare(final Model model) {
        model.getOrgUnitHolder().refresh();
        model.setSec(new OrgUnitSecModel(model.getOrgUnit()));
    }

    @Override
    @SuppressWarnings("unchecked")
    public void refreshDataSource(final Model model) {
        final Session session = this.getSession();
        final DynamicListDataSource<SessionObject> dataSource = model.getDataSource();
        {
            final DQLSelectBuilder dql = this.order.buildDQLSelectBuilder();
            dql.where(DQLExpressions.eq(DQLExpressions.property(SessionObject.orgUnit().fromAlias("so")), DQLExpressions.value(model.getOrgUnit())));

            this.order.applyOrder(dql, dataSource.getEntityOrder());
            UniBaseUtils.createPage(dataSource, dql.column(DQLExpressions.property("so")), session);

            Map<SessionObject, List<SessionAttestation>> attMap = new HashMap<SessionObject, List<SessionAttestation>>();
            for (SessionAttestation attestation : getList(SessionAttestation.class, SessionAttestation.sessionObject().orgUnit(), model.getOrgUnit(), SessionAttestation.number().s()))
                SafeMap.safeGet(attMap, attestation.getSessionObject(), ArrayList.class).add(attestation);
            
            for (ViewWrapper<SessionObject> wrapper : ViewWrapper.<SessionObject>getPatchedList(dataSource))
                wrapper.setViewProperty("attestationList", attMap.get(wrapper.getEntity()));
        }
    }
}
