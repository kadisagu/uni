/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.StateFinalExamResultList;


import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.sec.ISecured;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.StateFinalAttestationResultList.SessionReportStateFinalAttestationResultListUI;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.StateFinalExamResultAdd.SessionReportStateFinalExamResultAdd;

/**
 * @author Andrey Andreev
 * @since 27.10.2016
 */
@State({@Bind(key = SessionReportManager.BIND_ORG_UNIT, binding = "ouHolder.id")})
public class SessionReportStateFinalExamResultListUI extends SessionReportStateFinalAttestationResultListUI
{
    // Listeners
    @Override
    public void onClickAdd()
    {
        getActivationBuilder()
                .asDesktopRoot(SessionReportStateFinalExamResultAdd.class)
                .parameter(SessionReportManager.BIND_ORG_UNIT, getOuHolder().getId())
                .activate();
    }

    @Override
    public String getViewPermissionKey()
    {
        return getSec().getPermission(getOuHolder().getId() != null ? "orgUnit_viewStateFinalExamResultList" : "stateFinalExamResultReport");
    }

    @Override
    public String getAddPermissionKey()
    {
        return getSec().getPermission(getOuHolder().getId() != null ? "orgUnit_addStateFinalExamResult" : "addSessionStorableReport");
    }

    @Override
    public String getDeletePermissionKey()
    {
        return getSec().getPermission(getOuHolder().getId() != null ? "orgUnit_deleteStateFinalExamResult" : "deleteSessionStorableReport");
    }
}
