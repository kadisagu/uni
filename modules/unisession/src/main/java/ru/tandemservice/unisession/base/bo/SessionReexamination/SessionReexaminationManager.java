/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReexamination;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;
import ru.tandemservice.unisession.base.bo.SessionReexamination.logic.ISessionReexaminationDao;
import ru.tandemservice.unisession.base.bo.SessionReexamination.logic.SessionReexaminationDao;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.print.ISessionALRequestPrintDAO;
import ru.tandemservice.unisession.print.SessionALRequestPrintDAO;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 25.08.2015
 */
@Configuration
public class SessionReexaminationManager extends BusinessObjectManager
{
    public static final String REEXAMINATION_DS = "reexaminationDS";
    public static final String MARK_DS = "markDS";

    public static final Long ITEM_REEXAMINATION = 0L;
    public static final Long ITEM_REATTESTATION = 1L;

    public static final String PARAM_REG_ELEMENT_PART_ID = "currentRegElementPartId";
    public static final String PARAM_FCA_TYPE_ID = "fcaTypeId";

    public static SessionReexaminationManager instance()
    {
        return instance(SessionReexaminationManager.class);
    }

    @Bean
    public ISessionReexaminationDao dao()
    {
        return new SessionReexaminationDao();
    }

    @Bean
    public UIDataSourceConfig reExaminationDSConfig()
    {
        return SelectDSConfig.with(REEXAMINATION_DS, getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(reexaminationDS())
                .create();
    }

    @Bean
    public UIDataSourceConfig markDSConfig()
    {
        return SelectDSConfig.with(MARK_DS, getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(markDS())
                .addColumn(SessionMarkGradeValueCatalogItem.P_PRINT_TITLE)
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> reexaminationDS()
    {
        return new SimpleTitledComboDataSourceHandler(getName()).addItemList(reexaminationDSExtPoint());
    }

    @Bean
    public ItemListExtPoint<DataWrapper> reexaminationDSExtPoint()
    {
        return itemList(DataWrapper.class)
                .add(Boolean.FALSE.toString(), new DataWrapper(ITEM_REEXAMINATION, "Перезачет"))
                .add(Boolean.TRUE.toString(), new DataWrapper(ITEM_REATTESTATION, "Переаттестация"))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> markDS()
    {
        EntityComboDataSourceHandler handler = new EntityComboDataSourceHandler(getName(), SessionMarkGradeValueCatalogItem.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                Long partId = context.get(PARAM_REG_ELEMENT_PART_ID);
                Long fcaTypeId = context.get(PARAM_FCA_TYPE_ID);

                DQLSelectBuilder subBuilder = new DQLSelectBuilder()
                        .fromEntity(EppRegistryElementPartFControlAction.class, "f")
                        .column(property("f", EppRegistryElementPartFControlAction.gradeScale().id()))
                        .where(eq(property("f", EppRegistryElementPartFControlAction.part().id()), value(partId)))
                        .where(eq(property("f", EppRegistryElementPartFControlAction.controlAction().id()), value(fcaTypeId)));

                dql.where(in(property(alias, SessionMarkGradeValueCatalogItem.scale().id()), subBuilder.buildQuery()));
                dql.where(eq(property(alias, SessionMarkGradeValueCatalogItem.positive()), value(Boolean.TRUE)));
            }
        };
        handler.order(SessionMarkGradeValueCatalogItem.scale().id());
        handler.order(SessionMarkGradeValueCatalogItem.priority());
        return handler;
    }

    @Bean
    public ISessionALRequestPrintDAO sessionALRequestPrintDao() {return new SessionALRequestPrintDAO();}
}
