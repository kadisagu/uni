/* $Id:$ */
package ru.tandemservice.unisession.base.bo.SessionListDocument;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unisession.base.bo.SessionListDocument.logic.ISessionListDocumentMarkDAO;
import ru.tandemservice.unisession.base.bo.SessionListDocument.logic.SessionListDocumentMarkDAO;

/**
 * @author oleyba
 * @since 4/10/15
 */
@Configuration
public class SessionListDocumentManager extends BusinessObjectManager
{
    public static SessionListDocumentManager instance()
    {
        return instance(SessionListDocumentManager.class);
    }

	@Bean
	public ISessionListDocumentMarkDAO markDAO()
	{
		return new SessionListDocumentMarkDAO();
	}
}
