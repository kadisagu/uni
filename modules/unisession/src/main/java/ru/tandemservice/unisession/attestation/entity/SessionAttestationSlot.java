package ru.tandemservice.unisession.attestation.entity;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unisession.attestation.entity.gen.SessionAttestationSlotGen;

/**
 * Запись студента в атт. ведомости
 *
 * Студент, включенный в атт. ведомость для получения отметки о прохождении аттестации.
 */
public class SessionAttestationSlot extends SessionAttestationSlotGen
{

    public SessionAttestationSlot() {}
    public SessionAttestationSlot(SessionAttestationBulletin bulletin, EppStudentWorkPlanElement student) {
        setBulletin(bulletin);
        setStudentWpe(student);
    }

    @Override
    @EntityDSLSupport(parts = SessionAttestationSlot.P_FIXED_RATING_AS_LONG)
    public Double getFixedRating()
    {
        return getFixedRatingAsLong() == null ? null : ((double) getFixedRatingAsLong()) / 100;
    }

    public void setFixedRating(Double grade)
    {
        setFixedRatingAsLong(null == grade ? null : Math.round(grade * 100));
    }

    public Student getActualStudent()
    {
        return getStudentWpe().getStudent();
    }

}