package ru.tandemservice.unisession.entity.document;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.sec.ISecLocalEntityOwner;
import org.tandemframework.shared.commonbase.base.util.INumberGenerationRule;
import org.tandemframework.shared.commonbase.base.util.INumberObject;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.unisession.dao.sessionObject.ISessionObjectDAO;
import ru.tandemservice.unisession.entity.document.gen.SessionObjectGen;
import ru.tandemservice.unisession.sec.ISessionSecDao;

import java.util.Collection;

/**
 * Сессия
 *
 * Сессия (он же семестровый журнал) - документ, определяющий, что на указанном подразделении (деканат)
 * в рамках указанного года (и его части) будет проводится итоговая аттестация студентов.
 * Все итоговые оценки (как максимальные оценки, которые были получены в рамках данной сессии) будут храниться в этом объекте
 */
public class SessionObject extends SessionObjectGen implements INumberObject, ITitled, ISecLocalEntityOwner
{
    /** Для документов, которые могут существовать только в рамках сессии */
    public interface ISessionObjectDependentDocument {
        SessionObject getSessionObject();
    }

    public SessionObject() {}
    public SessionObject(final OrgUnit orgUnit, final EducationYear educationYear, final YearDistributionPart yearDistributionPart) {
        this.setOrgUnit(orgUnit);
        this.setEducationYear(educationYear);
        this.setYearDistributionPart(yearDistributionPart);
    }

    @Override
    public INumberGenerationRule getNumberGenerationRule()
    {
        return ISessionObjectDAO.instance.get().getNumberGenerationRule();
    }

    @Override
    @EntityDSLSupport(parts = {SessionObjectGen.L_EDUCATION_YEAR + "." + EducationYear.P_INT_VALUE, SessionObjectGen.L_YEAR_DISTRIBUTION_PART + "." + YearDistributionPart.P_NUMBER})
    public String getTitle()
    {
        if (getYearDistributionPart() == null) {
            return this.getClass().getSimpleName();
        }
        return getTermTitle();
    }

    @Override
    @EntityDSLSupport(parts = {SessionObjectGen.L_EDUCATION_YEAR + "." + EducationYear.P_INT_VALUE, SessionObjectGen.L_YEAR_DISTRIBUTION_PART + "." + YearDistributionPart.P_NUMBER})
    public String getTermTitle()
    {
        return this.getYearDistributionPart().getTitle() + " " + this.getEducationYear().getTitle();
    }

    @Override
    public Collection<IEntity> getSecLocalEntities()
    {
        return ISessionSecDao.instance.get().getSecLocalEntities(this);
    }

    public OrgUnit getGroupOu()
    {
        return getOrgUnit();
    }
}