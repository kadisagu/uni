/**
 *$Id$
 */
package ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.TotalResultAdd.util;

import org.tandemframework.caf.logic.wrapper.DataWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Alexander Shaburov
 * @since 23.11.12
 */
public class StudentWrapper extends DataWrapper
{
    public StudentWrapper(Long id, Long groupedEntityId)
    {
        setId(id);
        _groupedEntityId = groupedEntityId;
    }

    private Long _groupedEntityId;
    private Map<Long, Boolean> _dicsPassedMap = new HashMap<Long, Boolean>();

    // Getters & Setters

    public Long getGroupedEntityId()
    {
        return _groupedEntityId;
    }

    public void setGroupedEntityId(Long groupedEntityId)
    {
        _groupedEntityId = groupedEntityId;
    }

    public Map<Long, Boolean> getDicsPassedMap()
    {
        return _dicsPassedMap;
    }
}
