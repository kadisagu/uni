/**
 *$Id$
 */
package ru.tandemservice.unisession.base.bo.SessionIndicators.ui.StudentWithoutSessBullSlots;

import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList.AbstractUniStudentListUI;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;

/**
 * @author Alexander Shaburov
 * @since 05.12.12
 */
@State(keys = { "orgUnitId"}, bindings = { "orgUnitId"})
public class SessionIndicatorsStudentWithoutSessBullSlotsUI extends AbstractUniStudentListUI
{
    public static final String PROP_ORG_UNIT = SessionReportManager.PARAM_ORG_UNIT;
    public static final String PROP_EDU_YEAR = SessionReportManager.PARAM_EDU_YEAR;
    public static final String PROP_YEAR_PART = "yearPart";
    public static final String PROP_ATT_NUMBER = "attestationNumber";
    public static final String PROP_REGISTRY_STRUCTURE = "registryStructure";

    private Long _orgUnitId;
    private OrgUnit _orgUnit;
    private final OrgUnitHolder _orgUnitHolder = new OrgUnitHolder();

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();
        if (_orgUnitId != null)
        {
            _orgUnit = DataAccessServices.dao().get(OrgUnit.class, _orgUnitId);
            _orgUnitHolder.setId(_orgUnitId);
            _orgUnitHolder.refresh();
        }
    }

    @Override
    public String getSettingsKey()
    {
        if (_orgUnitId == null)
            return "SessionIndicatorsStudentWithoutSessBullSlotsSearch";
        else
            return "SessionIndicatorsStudentWithoutSessBullSlotsSearch_" + _orgUnitId;
    }

    public String getSticker()
    {
        return getConfig().getProperty("ui.sticker", _orgUnit.getFullTitle());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        super.onBeforeDataSourceFetch(dataSource);

        dataSource.put(PROP_ORG_UNIT, _orgUnit);
        dataSource.put(SessionReportManager.PARAM_EDU_YEAR_ONLY_CURRENT, true);
        dataSource.put(PROP_EDU_YEAR, getSettings().get("educationYear"));
        dataSource.put(PROP_YEAR_PART, getSettings().get(PROP_YEAR_PART));
        dataSource.put(PROP_ATT_NUMBER, getSettings().get(PROP_ATT_NUMBER));
        dataSource.put(PROP_REGISTRY_STRUCTURE, getSettings().get(PROP_REGISTRY_STRUCTURE));
    }

    // Getters & Setters

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }

    public OrgUnitHolder getOrgUnitHolder()
    {
        return _orgUnitHolder;
    }

    public String getViewPermissionKey() {
        if (null != getOrgUnitHolder().getValue()) {
            return getOrgUnitHolder().getSecModel().getPermission("eppGroupOuIndicatorBlockView");
        }
        return "";
    }
}
