/* $Id$ */
package ru.tandemservice.unisession.base.ext.GlobalReport.ui.List;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.TotalResultList.AttestationReportTotalResultList;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.ResultsByDiscList.SessionReportResultsByDiscList;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.ResultsList.SessionReportResultsList;

/**
 * @author azhebko
 * @since 28.03.2014
 */
public class UnisessionGlobalReportLIstAddon extends UIAddon
{
    public static final String NAME = "unisessionGlobalReportLIstAddon";

    public UnisessionGlobalReportLIstAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }
}