package ru.tandemservice.unisession.component.group.JournalTab;

import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.DisplayMode;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.unisession.base.bo.SessionMark.daemon.SessionMarkDaemonBean;

public class Controller extends AbstractBusinessController<IDAO, Model>
{

    @Override
    public void onActivateComponent(final IBusinessComponent component)
    {
        // при открытии компонента обновляем список оценок
        // это гарантирует, что слоты не будут дублироваться в разных сессиях
        // (делать надо в отдельной транзакции)
        // ISessionObjectMarkDAO.instance.get().doRefreshSessionObjectSlots();
    }

    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);

        model.setYearId(component.getSettings().<Long>get("testYear"));
        model.setPartId(component.getSettings().<Long>get("testPart"));

        this.getDao().prepare(model);
        this.onChangeTerm(component);
    }

    public void onChangeTerm(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        if(model.getTerm() != null)
        {
            component.getSettings().set("testYear", model.getTerm().getYear().getId());
            component.getSettings().set("testPart", model.getTerm().getPart().getId());
            component.saveSettings();
        }
        else
        {
            component.getSettings().set("testYear", null);
            component.getSettings().set("testPart", null);
            component.saveSettings();
        }
        this.getDao().prepareDataSource4Term(model);
    }

    public void onClickRevertColumn(final IBusinessComponent component)
    {
        ContextLocal.getDesktop().setRefreshScheduled(true);
    }

    public void onClickEdit(final IBusinessComponent component)
    {
        try {
            final Model model = this.getModel(component);
            model.setMode(DisplayMode.edit);
            final Model.Journal j = model.getJournal();
            if (null != j) {
                this.getDao().prepare4Edit(model);
            }
        } catch (final Throwable t) {
            this.onClickCancel(component);
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    public void onClickSave(final IBusinessComponent component)
    {
        try {
            final Model model = this.getModel(component);
            model.setMode(DisplayMode.view);
            final Model.Journal j = model.getJournal();
            if (null != j) {
                if (this.getDao().saveChanges(model)) {
                    // демон вызывать не надо, т.к. трарзакция уже закрылась
                    // (в ней поменялись оценки - демон вызвался листенером)
                    // но, необходимо дождаться выполнения демона (оценки должны обновиться)
                    SessionMarkDaemonBean.DAEMON.wakeUpAndWaitDaemon(60);
                }
            }
        } finally {
            this.onClickRevertColumn(component);
        }
    }

    public void onClickCancel(final IBusinessComponent component)
    {
        Model model = getModel(component);
        model.setMode(DisplayMode.view);
        ContextLocal.getDesktop().setRefreshScheduled(true);
    }

    public void onClickShowHistory(final IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                ru.tandemservice.unisession.component.student.ControlActionMarkHistory.Model.COMPONENT_NAME,
                new ParametersMap().add("controlActionId", component.getListenerParameter()).add("showAsSingle", true)));
    }

    public void onClickPrint(final IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator("ru.tandemservice.unisession.component.group.JournalTab")
            .setParameters(ParametersMap.createWith(PublisherActivator.PUBLISHER_ID_KEY, getModel(component).getGroup().getId()))
            .setPrintDisplayMode(Boolean.TRUE));
    }

}
