/* $Id$ */
package ru.tandemservice.unisession.component.student.StudentSessionDocumentTab;

import org.tandemframework.caf.service.impl.CAFLegacySupportService;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.component.impl.BusinessComponent;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.PropertyFormatter;
import org.tandemframework.core.view.formatter.RowCollectionFormatter;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.list.column.MultiValuesColumn;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.UnisessionDefines;
import ru.tandemservice.unisession.base.bo.SessionTransfer.ui.InsideAddEdit.SessionTransferInsideAddEdit;
import ru.tandemservice.unisession.base.bo.SessionTransfer.ui.InsideAddEdit.SessionTransferInsideAddEditUI;
import ru.tandemservice.unisession.base.bo.SessionTransfer.ui.OutsideAddEdit.SessionTransferOutsideAddEdit;
import ru.tandemservice.unisession.base.bo.SessionTransfer.ui.OutsideAddEdit.SessionTransferOutsideAddEditUI;
import ru.tandemservice.unisession.base.bo.SessionTransfer.ui.ProtocolAddEdit.SessionTransferProtocolAddEdit;
import ru.tandemservice.unisession.base.bo.SessionTransfer.ui.ProtocolAddEdit.SessionTransferProtocolAddEditUI;
import ru.tandemservice.unisession.base.bo.SessionTransfer.ui.ProtocolPub.SessionTransferProtocolPub;
import ru.tandemservice.unisession.entity.document.SessionDocument;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionGlobalDocument;
import ru.tandemservice.unisession.entity.document.SessionTransferProtocolDocument;

/**
 * @author oleyba
 * @since 4/13/11
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @SuppressWarnings("unchecked")
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        final Model model = getModel(component);
        model.setSettings(UniBaseUtils.getDataSettings(component, "StudentSessionMarkTab.filter"));
        getDao().prepare(model);

        if (null == model.getDataSource())
        {
            DynamicListDataSource<SessionDocument> dataSource = UniBaseUtils.createDataSource(component, getDao());
            dataSource.addColumn(new PublisherLinkColumn("Документ", SessionDocument.typeTitle().s()).setResolver(new IPublisherLinkResolver()
            {
                @Override
                public Object getParameters(IEntity entity)
                {
                    if (((ViewWrapper) entity).getEntity() instanceof SessionGlobalDocument)
                    {
                        return new ParametersMap()
                                .add(PublisherActivator.PUBLISHER_ID_KEY, model.getStudent().getGroup().getId())
                                .add("selectedTabId", "sessionGroupJournalTab");
                    }
                    else
                        return ((ViewWrapper) entity).getEntity().getId();
                }

                @Override
                public String getComponentName(IEntity entity)
                {
                    if (((ViewWrapper)entity).getEntity() instanceof SessionGlobalDocument)
                    {
                        return ru.tandemservice.uni.component.group.GroupPub.Model.class.getPackage().getName();
                    }
                    else if (((ViewWrapper)entity).getEntity() instanceof SessionTransferProtocolDocument)
                        return SessionTransferProtocolPub.class.getSimpleName();
                    else
                        return "";
                }
            }).setClickable(true).setOrderable(false));

            dataSource.addColumn(new MultiValuesColumn("Мероприятие", "slotList").setFormatter(new RowCollectionFormatter(new PropertyFormatter(SessionDocumentSlot.studentWpeCAction().registryElementTitle().s()))).setClickable(false).setOrderable(false));
            dataSource.addColumn(new MultiValuesColumn("Семестр", "slotList").setFormatter(new RowCollectionFormatter(new PropertyFormatter(SessionDocumentSlot.studentWpeCAction().termTitle().s()))).setClickable(false).setOrderable(false));
            dataSource.addColumn(new SimpleColumn("Дата формирования", SessionDocument.formingDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false));
            dataSource.setOrder(EppStudentWpeCAction.termTitle().s(), OrderDirection.asc);
            model.setDataSource(dataSource);
        }
    }

    public void onClickSearch(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.getDataSource().showFirstPage();
        model.getDataSource().refresh();
        DataSettingsFacade.saveSettings(model.getSettings());
    }

    public void onClickClear(IBusinessComponent context)
    {
        getModel(context).getSettings().clear();
        onClickSearch(context);
    }

    public void onClickAddTransferProtocol(IBusinessComponent component)
    {
        CAFLegacySupportService.asRegionDialog(SessionTransferProtocolAddEdit.class.getSimpleName(), (BusinessComponent) component)
                .parameter(SessionTransferProtocolAddEditUI.PARAM_STUDENT_ID, getModel(component).getStudent().getId())
                .activate();
    }

    public void onClickAddInsideTransfer(IBusinessComponent component)
    {
        CAFLegacySupportService.asRegion(SessionTransferInsideAddEdit.class.getSimpleName(), UnisessionDefines.STUDENT_REGION_NAME, (BusinessComponent) component)
                .parameters(new ParametersMap()
                                    .add(SessionTransferInsideAddEditUI.STUDENT_ID, getModel(component).getStudent().getId())
                                    .add(SessionTransferInsideAddEditUI.DOCUMENT_ID, null)
                                    .add(SessionTransferInsideAddEditUI.ORG_UNIT_ID, null))
                .top().activate();
    }

    public void onClickAddOutsideTransfer(IBusinessComponent component)
    {
        CAFLegacySupportService.asRegion(SessionTransferOutsideAddEdit.class.getSimpleName(), UnisessionDefines.STUDENT_REGION_NAME, (BusinessComponent) component)
                .parameters(new ParametersMap()
                                    .add(SessionTransferOutsideAddEditUI.STUDENT_ID, getModel(component).getStudent().getId())
                                    .add(SessionTransferOutsideAddEditUI.DOCUMENT_ID, null)
                                    .add(SessionTransferOutsideAddEditUI.ORG_UNIT_ID, null))
                .top().activate();
    }

    public void onClickInsideTransferAutoForm(IBusinessComponent component)
    {
        CAFLegacySupportService.asRegion(SessionTransferInsideAddEdit.class.getSimpleName(), UnisessionDefines.STUDENT_REGION_NAME, (BusinessComponent) component)
                .parameters(new ParametersMap()
                                    .add(SessionTransferInsideAddEditUI.STUDENT_ID, getModel(component).getStudent().getId())
                                    .add(SessionTransferInsideAddEditUI.DOCUMENT_ID, null)
                                    .add(SessionTransferInsideAddEditUI.ORG_UNIT_ID, null)
                                    .add(SessionTransferInsideAddEditUI.AUTO_FORM, true))
                .top().activate();
    }
}
