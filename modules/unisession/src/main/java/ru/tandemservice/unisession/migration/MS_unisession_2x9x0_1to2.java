package ru.tandemservice.unisession.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unisession_2x9x0_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность sessionALRequest

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("session_al_req_t",
                new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_sessionalrequest"),
                new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                new DBColumn("requestdate_p", DBType.TIMESTAMP).setNullable(false),
                new DBColumn("student_id", DBType.LONG).setNullable(false),
                new DBColumn("edudocument_id", DBType.LONG).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("sessionALRequest");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность sessionALRequestRow

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("session_al_req_row_t",
                new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_sessionalrequestrow"),
                new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                new DBColumn("request_id", DBType.LONG).setNullable(false),
                new DBColumn("rowterm_id", DBType.LONG).setNullable(false),
                new DBColumn("regelementpart_id", DBType.LONG).setNullable(false),
                new DBColumn("reattestation_p", DBType.BOOLEAN).setNullable(false),
                new DBColumn("hoursamount_p", DBType.LONG)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("sessionALRequestRow");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность sessionALRequestRowMark

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("session_al_req_row_mark_t",
                new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_sessionalrequestrowmark"),
                new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                new DBColumn("requestrow_id", DBType.LONG).setNullable(false),
                new DBColumn("controlaction_id", DBType.LONG).setNullable(false),
                new DBColumn("mark_id", DBType.LONG)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("sessionALRequestRowMark");
        }
    }
}