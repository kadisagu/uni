/* $Id$ */
package ru.tandemservice.unisession.component.pps.PpsSessionRetakeList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.unisession.entity.document.SessionRetakeDocument;

/**
 * @author Vasily Zhukov
 * @since 12.01.2012
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.setPrincipalContext(component.getUserContext().getPrincipalContext());
        model.setSettings(component.getSettings());

        getDao().prepare(model);

        DynamicListDataSource<SessionRetakeDocument> dataSource = new DynamicListDataSource<>(component, component1 -> {
            Controller.this.getDao().refreshDataSource(Controller.this.getModel(component1));
        });

        dataSource.addColumn(new SimpleColumn("Дата\n формирования", SessionRetakeDocument.formingDate(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setWidth(1));
        dataSource.addColumn(new PublisherLinkColumn("Ведомость", "titleWithCA").setResolver(new IPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                return new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, entity.getId());
            }

            @Override
            public String getComponentName(IEntity entity)
            {
                return "ru.tandemservice.unisession.component.sessionRetakeDoc.SessionRetakeDocPubForPps";
            }
        }));
        dataSource.addColumn(new MultiValuesColumn("Группа\n (текущая)", "groups").setFormatter(UniEppUtils.NEW_LINE_FORMATTER).setClickable(false).setWidth(1));
        dataSource.addColumn(new SimpleColumn("Числo\n студентов", "studentCount").setClickable(false).setOrderable(false).setWidth(1));
        dataSource.addColumn(new MultiValuesColumn("Преподаватель", "comission").setFormatter(UniEppUtils.NEW_LINE_FORMATTER).setClickable(false).setOrderable(false).setWidth(20));
        dataSource.addColumn(new SimpleColumn("Дисциплина", SessionRetakeDocument.registryElementPart().titleWithNumber()).setClickable(false).setWidth(1));
        dataSource.addColumn(new SimpleColumn("Дата\n закрытия", SessionRetakeDocument.closeDate(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setWidth(1));
        dataSource.addColumn(new ActionColumn("Выставить оценки", "edit_mark", "onClickMark"));
        model.setDataSource(dataSource);
    }

    public void onClickSearch(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);
        model.getDataSource().showFirstPage();
        model.getDataSource().refresh();
        DataSettingsFacade.saveSettings(model.getSettings());
    }

    public void onClickClear(IBusinessComponent context)
    {
        getModel(context).getSettings().clear();
        onClickSearch(context);
    }

    public void onClickMark(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(
                                          ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinMark.Model.COMPONENT_NAME,
                                          new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, component.<Long>getListenerParameter()))
        );
    }

    public void onSearchParamsChange(IBusinessComponent component)
    {
        Model model = getModel(component);
        DataSettingsFacade.saveSettings(model.getSettings());
        getDao().prepare(model);
    }
}
