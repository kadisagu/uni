package ru.tandemservice.unisession.base.bo.StudentPortfolio.ui.AchievementAddEdit;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unisession.base.bo.StudentPortfolio.StudentPortfolioManager;
import ru.tandemservice.unisession.base.bo.StudentPortfolio.logic.IStudentPortfolioDAO;
import ru.tandemservice.unisession.entity.catalog.StudentPortfolioElement;

import java.util.Date;

/**
 * @author avedernikov
 * @since 26.11.2015
 */

@State({
		@Bind(key = StudentPortfolioAchievementAddEditUI.PARAM_STUDENT_ID, binding = "student.id", required = true),
		@Bind(key = StudentPortfolioAchievementAddEditUI.PARAM_ACHIEVEMENT_ID, binding = "achievement.id")
})
public class StudentPortfolioAchievementAddEditUI extends UIPresenter
{
	public static final String PARAM_STUDENT_ID = "studentId";
	public static final String PARAM_ACHIEVEMENT_ID = "achievementId";

	private StudentPortfolioElement achievement = new StudentPortfolioElement();
	private Student student = new Student();
	private IUploadFile file;

	@Override
	public void onComponentRefresh()
	{
		student = DataAccessServices.dao().getNotNull(student.getId());
		if (!isAddForm())
		{
			achievement = DataAccessServices.dao().getNotNull(achievement.getId());
		}
	}

	@Override
	public void onBeforeDataSourceFetch(IUIDataSource dataSource)
	{
		switch (dataSource.getName())
		{
			case StudentPortfolioAchievementAddEdit.WPE_C_ACTION_DS:
			{
				dataSource.put("student", student);
				break;
			}
		}
	}

	public void onClickApply()
	{
		achievement.setEditDate(new Date());
		achievement.setStudent(student);
		IStudentPortfolioDAO dao = StudentPortfolioManager.instance().dao();
		achievement.setFile(dao.createOrUpdateAchievementAttachement(achievement, achievement.getFile(), file));
		dao.saveOrUpdate(achievement);
		deactivate();
	}

	public boolean isAddForm()
	{
		return achievement.getId() == null;
	}

	public void onWpeCActionSelected()
	{
		if (achievement.getTitle() == null && achievement.getWpeCAction() != null)
			achievement.setTitle(achievement.getWpeCAction().getRegistryElementTitle());
	}

	public StudentPortfolioElement getAchievement()
	{
		return this.achievement;
	}

	public void setAchievement(StudentPortfolioElement achievement)
	{
		this.achievement = achievement;
	}

	public IUploadFile getFile()
	{
		return this.file;
	}

	public void setFile(IUploadFile file)
	{
		this.file = file;
	}

	public Student getStudent()
	{
		return student;
	}

	public void setStudent(Student student)
	{
		this.student = student;
	}

}
