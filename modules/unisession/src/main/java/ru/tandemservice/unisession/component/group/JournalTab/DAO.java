package ru.tandemservice.unisession.component.group.JournalTab;

import com.google.common.collect.Iterables;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ObjectUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils.Pair;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.util.cache.SingleValueCache;
import org.tandemframework.core.view.DisplayMode;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.ui.SelectModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.base.bo.SessionMark.SessionMarkManager;
import ru.tandemservice.unisession.base.bo.SessionMark.logic.ISessionMarkDAO.MarkData;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.comission.SessionComission;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionGlobalDocument;
import ru.tandemservice.unisession.entity.document.SessionStudentGradeBookDocument;
import ru.tandemservice.unisession.entity.document.gen.SessionDocumentSlotGen;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.util.selectModel.SessionTermModel;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class DAO extends UniBaseDao implements IDAO
{

    @Override
    public void prepare(final Model model) {
        model.setMode(DisplayMode.view);
        model.getGroupHolder().refresh(Group.class);

        model.setTermModel(new SessionTermModel()
        {
            @Override
            public Map<EducationYear, Set<YearDistributionPart>> getValueMap()
            {
                final Map<EducationYear, Set<YearDistributionPart>> result = new LinkedHashMap<>();

                final DQLSelectBuilder slotDQL = new DQLSelectBuilder()
						.fromEntity(EppStudentWpeCAction.class, "wpcaSlot")

						.joinPath(DQLJoinType.inner, EppStudentWpeCAction.studentWpe().fromAlias("wpcaSlot"), "wpSlot")
						.where(eq(property("wpSlot", EppStudentWorkPlanElement.student().group()), value(model.getGroup())))

						.joinPath(DQLJoinType.inner, EppStudentWorkPlanElement.year().educationYear().fromAlias("wpSlot"), "year")
						.order(property("year", EducationYear.intValue()))
						.column(property("year"))

						.joinPath(DQLJoinType.inner, EppStudentWorkPlanElement.part().fromAlias("wpSlot"), "part")
						.order(property("part", YearDistributionPart.number()))
						.column(property("part"));

                for (final Object[] slot : UniBaseDao.scrollRows(slotDQL.createStatement(DAO.this.getSession()))) {
                    SafeMap.safeGet(result, (EducationYear)slot[0], LinkedHashSet.class).add((YearDistributionPart)slot[1]);
                }
                return result;
            }
        });
        if(model.getYearId() != null && model.getPartId() != null)
        {
            EducationYear year = get(EducationYear.class, model.getYearId());
            YearDistributionPart part = get(YearDistributionPart.class, model.getPartId());

            Map<EducationYear, Set<YearDistributionPart>> valueMap = ((SessionTermModel) model.getTermModel()).getValueMap();

            if(valueMap.get(year) != null && valueMap.get(year).contains(part))
                model.setTerm(new SessionTermModel.TermWrapper(year, part));
        }
        model.setWithoutMSRPFK(model.getTermModel().findValues("").isEmpty());
    }


    @Override
    public void prepareDataSource4Term(final Model model) {
        if (null == model.getTerm()) {
            model.setJournal(null);
            return;
        }
        this.createJournal(model);
    }

    protected Model.Journal createJournal(final Model model) {

        final Session session = this.getSession();
        final Model.Journal journal = new Model.Journal(model.getTerm());

        final Map<EppStudentWpeCAction, Model.Slot> temporaryMap = SafeMap.get(Model.Slot::new);

        // выбираем МСРП-ФК для студентов (то, что они должны сдавать)
        {
            final DQLSelectBuilder dql = new DQLSelectBuilder()
					.fromEntity(EppStudentWpeCAction.class, "wpcaSlot")

					.column(property("wpcaSlot"), "wpcaSlot")
					.fetchPath(DQLJoinType.inner, EppStudentWpeCAction.studentWpe().fromAlias("wpcaSlot"), "wpSlot")

					.fetchPath(DQLJoinType.inner, EppStudentWorkPlanElement.year().fromAlias("wpSlot"), "eppYear")

					.joinPath(DQLJoinType.inner, EppStudentWorkPlanElement.student().fromAlias("wpSlot"), "s")
					.joinPath(DQLJoinType.inner, Student.person().fromAlias("s"), "p")
					.joinPath(DQLJoinType.inner, Person.identityCard().fromAlias("p"), "idc")

					.where(eq(property("s", Student.group()), value(model.getGroup())))
					.where(eq(property("eppYear", EppYearEducationProcess.educationYear()), value(journal.getYear())))
					.where(eq(property("wpSlot", EppStudentWorkPlanElement.part()), value(journal.getPart())))

					.order(property("wpcaSlot", EppStudentWpeCAction.type().priority()))
					.order(property("wpSlot", EppStudentWorkPlanElement.registryElementPart().registryElement().title()))
					.order(property("wpSlot", EppStudentWorkPlanElement.registryElementPart().number()))
					.order(property("wpSlot", EppStudentWorkPlanElement.term().intValue()) /* на случай, если привязали хз что к студентам, сортируем в том числе и по номеру семестра */)
					.order(property("idc", IdentityCard.lastName()))
					.order(property("idc", IdentityCard.firstName()))
					.order(property("idc", IdentityCard.middleName()));

			if (!model.getGroup().isArchival())
			{
				dql
						.where(isNull(property("wpcaSlot", EppStudentWpeCAction.removalDate())))
						.where(eq(property("s", Student.archival()), value(Boolean.FALSE)));
			}

            for (final EppStudentWpeCAction eppSlot: dql.createStatement(session).<EppStudentWpeCAction>list()) {

                EppFControlActionType actionType = eppSlot.getActionType();
                EppRegistryElementPart elementPart = eppSlot.getStudentWpe().getRegistryElementPart();
                EppRegistryStructure registryStructure = elementPart.getRegistryElement().getParent().getRoot();

                final Map<EppFControlActionType, Set<EppRegistryElementPart>> fcaMap = SafeMap.safeGet(journal.getRegistryActionMap(), registryStructure, key -> new TreeMap<>(Comparator.comparingInt(fca -> fca.getPriority())));
                final Set<EppRegistryElementPart> partSet = SafeMap.safeGet(fcaMap, actionType, key -> new LinkedHashSet<>());
                partSet.add(elementPart);

                final Pair<Long, Long> pair = this.getPair(eppSlot);
                final Map<Long, Model.Slot> pairSlotMap = SafeMap.safeGet(journal.getSlotMap(), pair, LinkedHashMap.class);

                final Long relId = eppSlot.getStudentWpe().getStudent().getId();
                if (null != pairSlotMap.put(relId, temporaryMap.get(eppSlot))) {
                    throw new IllegalStateException("duplicate slot: ("+pair.getX()+", "+pair.getY()+") -> {"+relId+" -> "+eppSlot.getId()+"}");
                }
            }
        }


        // выбираем итоговые оценки
        // (оценки, которые можно редактировать будем выбирать отдельно при нажатии на кнопку редактирования колноки)
        {
            for (List<EppStudentWpeCAction> slots : Iterables.partition(temporaryMap.keySet(), DQL.MAX_VALUES_ROW_NUMBER)) {

                final DQLSelectBuilder dql = new DQLSelectBuilder()
                        .fromEntity(SessionMark.class, "mk").column(property("mk"), "mk")
                        .fetchPath(DQLJoinType.inner, SessionMark.slot().fromAlias("mk"), "sl")
                        .joinEntity("sl", DQLJoinType.inner, SessionStudentGradeBookDocument.class, "gb", eq(property("sl", SessionDocumentSlot.document()), property("gb")))
                        .where(in(property("sl", SessionDocumentSlot.studentWpeCAction()), slots))
                        .where(eq(property("sl", SessionDocumentSlot.inSession()), value(Boolean.FALSE)));

                for (final SessionMark mk: UniBaseDao.<SessionMark>scroll(dql.createStatement(session))) {
                    temporaryMap.get(mk.getSlot().getStudentWpeCAction()).setTotalMarkValue(mk.getValueItem());
                }
            }
        }

        // грузим самих студентов
		final DQLSelectBuilder studentDql = new DQLSelectBuilder().fromEntity(Student.class, "st")
				.where(eq(property("st", Student.group()), value(model.getGroup())));
		if (!model.getGroup().isArchival())
			studentDql.where(eq(property("st", Student.archival()), value(false)));
		final List<Student> students = studentDql.createStatement(session).list();

        Collections.sort(students, (s1, s2) -> CommonCollator.RUSSIAN_COLLATOR.compare(s1.getPerson().getFullFio(), s2.getPerson().getFullFio()));

        journal.getStudents().clear();
        journal.getStudents().addAll(students);

        journal.setEducationLevelHighSchoolVisible(
                CollectionUtils.isNotEmpty(students) && students.stream().filter(student -> {
                    EducationLevelsHighSchool highSchool = student.getEducationOrgUnit().getEducationLevelHighSchool();
                    EduProgramSubject programSubject = highSchool == null ? null : highSchool.getEducationLevel().getEduProgramSubject();
                    return null != programSubject && programSubject.getSubjectIndex().getProgramKind().isProgramHigherProf();
                }).count() > 0
        );

        model.setJournal(journal);
        return journal;
    }
//
    protected Pair<Long, Long> getPair(final EppStudentWpeCAction eppSlot) {
        return new Pair<>(
                eppSlot.getStudentWpe().getRegistryElementPart().getId(),
                eppSlot.getType().getId()
        );
    }

    @Override
    public void prepare4Edit(final Model model)
    {
        final Model.Journal j = model.getJournal();
        if (null == j ) { return; }

        for(Map<Long, Model.Slot> column : j.getSlotMap().values())
        {
            final Map<EppStudentWpeCAction, Model.Slot> temporaryMap = new HashMap<>(column.size());
            for (final Model.Slot s: column.values()) {
                temporaryMap.put(s.getWpCASlot(), s);
                s.setJournalMarkSelectModel(
                        new SelectModel<>(SessionMarkManager.instance().regularMarkDao().prepareSimpleMarkModelWithShortTitles(s.getWpCASlot(), null, false))
                );
            }

            // выбираем оценки и слоты по которым в семестровом журнале выставлены оценки
            final DQLSelectBuilder dql = this.getJournalMarksDql(j).where(in(property("sl", SessionDocumentSlot.studentWpeCAction()), temporaryMap.keySet()));
            for (final Object[] row: UniBaseDao.scrollRows(dql.createStatement(this.getSession()))) {
                final SessionDocumentSlot sl = (SessionDocumentSlot)row[0];
                final SessionMark mk = (SessionMark)row[1];

                final Model.Slot slot = temporaryMap.get(sl.getStudentWpeCAction());

                // берем первую (оценки отсортированы в правильном порядке)
                final SelectModel<SessionMarkCatalogItem> journalMarkSelectModel = slot.getJournalMarkSelectModel();
                if (null != mk && null == journalMarkSelectModel.getValue()) {
                    journalMarkSelectModel.setValue(mk.getValueItem());
                }

            }
        }
    }

    // выбираем оценки и слоты по которым в семестровом журнале выставлены оценки
    // оценки всегда вне сессии (inSession=false)
    // оценки всегда с текущего подразделения студента (в данный момент)
    protected DQLSelectBuilder getJournalMarksDql(final Model.Journal j)
    {
        return new DQLSelectBuilder()
        .fromEntity(SessionDocumentSlot.class, "sl")
        .joinEntity("sl", DQLJoinType.inner, SessionGlobalDocument.class, "so", eq(property("sl", SessionDocumentSlot.document()), property("so")))
        .joinEntity("sl", DQLJoinType.inner, SessionMark.class, "mk", eq(property("mk", SessionMark.slot()), property("sl")))

        .column(property("sl"), "sl")
        .column(property("mk"), "mk")

        .where(eq(property("sl", SessionDocumentSlot.inSession()), value(Boolean.FALSE)))
        .where(eq(property("so", SessionGlobalDocument.yearDistributionPart()), value(j.getPart())))
        .where(eq(property("so", SessionGlobalDocument.educationYear()), value(j.getYear())))

        .order(property("mk", SessionMark.performDate()), OrderDirection.desc)
        .order(property("mk", SessionMark.modificationDate()), OrderDirection.desc);

    }

    @Override
    public boolean saveChanges(final Model model)
    {
        final Model.Journal j = model.getJournal();
        if (null == j) { return false; }

        final Session session = this.getSession();
        {
            // надо чистить сессию перед тем, как выполнять операцию,
            // чтобы загрузить объекты в сессию из базы - иначе будут проблемы с кэшем
            session.clear();
        }

        boolean processed = false;
        final SingleValueCache<SessionComission> cachedValue = new SingleValueCache<SessionComission>()
        {
            @Override
            protected SessionComission resolve()
            {
                // TODO: return ISessionCommissionDAO.instance.get().getEmptyComission();

                // пока создаем новую сессию, но! только если было обращение
                final SessionComission c = new SessionComission();
                session.save(c);
                return c;
            }
        };

        final Date now = new Date();

        for(Map<Long, Model.Slot> column : j.getSlotMap().values())
        {
            for (final Model.Slot slot : column.values())
            {

                final SelectModel<SessionMarkCatalogItem> journalMarkSelectModel = slot.getJournalMarkSelectModel();
                final SessionMarkCatalogItem journalMarkValue = (null == journalMarkSelectModel ? null : journalMarkSelectModel.getValue());

                final EppStudentWpeCAction wpCASlot = (EppStudentWpeCAction) session.get(EppStudentWpeCAction.class, slot.getWpCASlot().getId());
                final DQLSelectBuilder dql = this.getJournalMarksDql(j).where(eq(property("sl", SessionDocumentSlot.studentWpeCAction()), value(wpCASlot)));

                final List<Object[]> rows = dql.createStatement(session).list();
                if ((null != journalMarkValue) && rows.isEmpty())
                {
                    // нету слотов (ищем сессии)
                    final Student student = wpCASlot.getStudentWpe().getStudent();
                    final OrgUnit groupOrgUnit = student.getEducationOrgUnit().getGroupOrgUnit();

                    final String message = "Не удалось сохранить оценку студента «" + student.getPerson().getFio() + "»: ";

                    if (null == groupOrgUnit)
                    {
                        ContextLocal.getInfoCollector().add(message + "у направления подготовки студента не указан деканат.");
                        continue;
                    }

                    SessionGlobalDocument document = new DQLSelectBuilder()
                            .fromEntity(SessionGlobalDocument.class, "sd").column(property("sd"), "sd")
                            .where(eq(property("sd", SessionGlobalDocument.yearDistributionPart()), value(j.getPart())))
                            .where(eq(property("sd", SessionGlobalDocument.educationYear()), value(j.getYear())))
                            .createStatement(session).setMaxResults(1).uniqueResult();

                    if(document == null)
                    {
                        document = new SessionGlobalDocument();
                        document.setNumber(j.getYear().getTitle() + ", " + j.getPart().getTitle());
                        document.setFormingDate(now);
                        document.setYearDistributionPart(j.getPart());
                        document.setEducationYear(j.getYear());
                        session.save(document);
                    }

                    SessionDocumentSlot sl = this.getByNaturalId(new SessionDocumentSlotGen.NaturalId(document, wpCASlot, false));
                    if (null == sl)
                    {
                        sl = new SessionDocumentSlot(document, wpCASlot, false);
                        sl.setCommission(cachedValue.get());
                        session.save(sl);
                    }
                    rows.add(new Object[]{sl, null});
                }


                for (final Object[] row : rows)
                {
                    final SessionMark previous = (SessionMark) row[1];
                    if (!ObjectUtils.equals((null == previous ? null : previous.getValueItem()), journalMarkValue))
                    {
                        // заменяем сразу все оценки (на всех семестровых журналах данного подразделения)
                        SessionMarkManager.instance().regularMarkDao().saveOrUpdateMark(
                                (SessionDocumentSlot) row[0],
                                new MarkData()
                                {
                                    @Override
                                    public Date getPerformDate() { return now; }

                                    @Override
                                    public SessionMarkCatalogItem getMarkValue() { return journalMarkValue; }

                                    @Override
                                    public Double getPoints() { return null; }

                                    @Override
                                    public String getComment() { return null; }
                                }
                        );
                        processed = true;
                    }
                }
            }
        }
        return processed;
    }
}
