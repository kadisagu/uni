// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisession.component.sessionSheet.SessionSheetPubForPps;

import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionSheetDocument;

/**
 * @author oleyba
 * @since 2/16/11
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setSheet(this.getNotNull(SessionSheetDocument.class, model.getSheet().getId()));
        SessionDocumentSlot slot = this.get(SessionDocumentSlot.class, SessionDocumentSlot.document().s(), model.getSheet());
        final Person person = PersonManager.instance().dao().getPerson(model.getPrincipalContext());

        if (null == slot || !CollectionUtils.exists(getList(SessionComissionPps.class, SessionComissionPps.commission(), slot.getCommission()), sessionComissionPps -> sessionComissionPps.getPps().getPerson().equals(person)))
            throw new ApplicationException("Вы не состоите в коммиссии данного экзаменационного листа, поэтому не можете просматривать его со своей личной страницы.");
    }
}