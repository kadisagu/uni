package ru.tandemservice.unisession.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Оценка из шкалы текущего контроля"
 * Имя сущности : sessionAttestationMarkCatalogItem
 * Файл data.xml : session.data.xml
 */
public interface SessionAttestationMarkCatalogItemCodes
{
    /** Константа кода (code) элемента : Аттестован (title) */
    String MARK_PASSED = "passed";
    /** Константа кода (code) элемента : Не аттестован (title) */
    String MARK_NO_PASSED = "noPassed";
    /** Константа кода (code) элемента : Отлично (title) */
    String GRADE_5 = "grade5";
    /** Константа кода (code) элемента : Хорошо (title) */
    String GRADE_4 = "grade4";
    /** Константа кода (code) элемента : Удовлетворительно (title) */
    String GRADE_3 = "grade3";
    /** Константа кода (code) элемента : Неудовлетворительно (title) */
    String GRADE_2 = "grade2";
    /** Константа кода (code) элемента : Не аттестован (уваж.) (title) */
    String GRADE_NO_PASSED_GOOD_CAUSE = "gradeNoPassedGoodCause";
    /** Константа кода (code) элемента : Не аттестован (неуваж.) (title) */
    String GRADE_NO_PASSED_UNEXCUSED = "gradeNoPassedUnexcused";

    Set<String> CODES = ImmutableSet.of(MARK_PASSED, MARK_NO_PASSED, GRADE_5, GRADE_4, GRADE_3, GRADE_2, GRADE_NO_PASSED_GOOD_CAUSE, GRADE_NO_PASSED_UNEXCUSED);
}
