/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.StateFinalExamResultAdd;

import jxl.write.*;
import jxl.write.Number;
import jxl.write.biff.RowsExceededException;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import ru.tandemservice.uniepp.entity.catalog.codes.EppRegistryStructureCodes;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.unisession.base.bo.SessionReport.logic.SfaRaw;
import ru.tandemservice.unisession.base.bo.SessionReport.logic.StateFinalAttestationUtil;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.StateFinalAttestationResultAdd.logic.Row;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.StateFinalAttestationResultAdd.logic.SfaReportInfo;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.StateFinalAttestationResultAdd.logic.StateFinalAttestationResultPrintDAO;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.report.StateFinalAttestationResult;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static ru.tandemservice.unisession.base.bo.SessionReport.logic.StateFinalAttestationUtil.*;
import static ru.tandemservice.unisession.base.bo.SessionReport.logic.StateFinalAttestationUtil.addDoubleCell;

/**
 * @author Andrey Andreev
 * @since 02.11.2016
 */
public class StateFinalExamResultPrintDAO extends StateFinalAttestationResultPrintDAO<SfaRaw, Row>
{

    @Override
    public SfaReportInfo<SfaRaw> initReportInfo(StateFinalAttestationResult report)
    {
        SfaReportInfo<SfaRaw> reportInfo = new SfaReportInfo<>(report);

        DQLSelectBuilder slotBuilder = getSlotSelectBuilder(reportInfo)
                .where(and(not(eq(property(REG_ALIAS, EppRegistryElement.parent().code()),
                                  value(EppRegistryStructureCodes.REGISTRY_ATTESTATION_DIPLOMA))),
                           eq(property(REG_ALIAS, EppRegistryElement.parent().parent().code()),
                              value(EppRegistryStructureCodes.REGISTRY_ATTESTATION))
                ));

        slotBuilder.column(property(EOU_ALIAS))
                .column(property(WPE_ALIAS))
                .column(property(MARK_ALIAS));

        List<Object[]> list = getList(slotBuilder);

        if (list.isEmpty())
            throw new ApplicationException("Нет данных для построения отчета. Нет студентов с выбранными параметрами.");

        reportInfo.setRaws(StateFinalAttestationUtil.filterRaws(list.stream().map(SfaRaw::new)));

        return reportInfo;
    }

    @Override
    protected void init() throws WriteException
    {
        super.init();

        _tableWidth = 16;
    }

    @Override
    protected String getFileName(StateFinalAttestationResult report)
    {
        return "Отчет по результатам государственного экзамена.xls";
    }

    @Override
    protected int printTitle(StateFinalAttestationResult report, WritableSheet sheet)
    {
        int row = 0;
        int indent = 1;

        try
        {
            sheet.setRowView(row, 380);
        }
        catch (RowsExceededException e)
        {
            e.printStackTrace();
        }

        addTextCell(sheet, indent, row, _tableWidth - indent, 1, "Отчет по результатам государственного экзамена", _titleStyle);

        return ++row;
    }

    @Override
    protected int getPropertyTitleWidth()
    {
        return 1;
    }

    protected int printParametersTable(SfaReportInfo<SfaRaw> reportInfo, WritableSheet sheet, int row)
    {
        row = super.printParametersTable(reportInfo, sheet, row);

        StateFinalAttestationResult report = reportInfo.getReport();
        if (report.getDateFrom() != null || report.getDateTo() != null)
        {
            String dates = reportInfo.getRaws().stream()
                    .map(SfaRaw::getMark)
                    .filter(mark -> mark != null)
                    .map(SessionMark::getPerformDate)
                    .filter(date -> date != null)
                    .distinct()
                    .sorted()
                    .map(DateFormatter.DEFAULT_DATE_FORMATTER::format)
                    .collect(Collectors.joining(", "));

            row = addProperty2HeadTable(sheet, row, "Дата проведения экзамена", dates, _parametersTableStyle);
        }

        return row;
    }

    protected int addHeaderDataTable(SfaReportInfo<SfaRaw> reportInfo, WritableSheet sheet, int rowIndex)
    {
        int col = 0;
        int firstRow = rowIndex;

        try
        {
            sheet.setRowView(rowIndex + 1, 2000);

            sheet.setColumnView(col, 5);
            addTextCell(sheet, col++, rowIndex, 1, 2, "№ п.п.", _verHeaderDataTableStyle);

            sheet.setColumnView(col, 60);
            addTextCell(sheet, col++, rowIndex, 1, 2, "Название дисциплины", _horHeaderDataTableStyle);

            addTextCell(sheet, col, rowIndex, 7, 1, "Количество выпускников", _horHeaderDataTableStyle);
            sheet.addCell(new Label(col++, rowIndex + 1, "Всего", _verHeaderDataTableStyle));
            sheet.addCell(new Label(col++, rowIndex + 1, "В академическом отпуске", _verHeaderDataTableStyle));
            sheet.addCell(new Label(col++, rowIndex + 1, "Обязаны сдавать", _verHeaderDataTableStyle));
            sheet.addCell(new Label(col++, rowIndex + 1, "Отсрочка", _verHeaderDataTableStyle));
            sheet.addCell(new Label(col++, rowIndex + 1, "Недопущены", _verHeaderDataTableStyle));
            sheet.addCell(new Label(col++, rowIndex + 1, "Явились на госэкзамен", _verHeaderDataTableStyle));
            sheet.addCell(new Label(col++, rowIndex + 1, "Сдали госэкзамен", _verHeaderDataTableStyle));

            addTextCell(sheet, col++, rowIndex, 1, 2, "Из них (графа 9) \nобучаются на бюджете", _verHeaderDataTableStyle);

            addTextCell(sheet, col, rowIndex, 4, 1, "Из них получили оценки", _horHeaderDataTableStyle);
            sheet.addCell(new Number(col++, rowIndex + 1, 5, _horHeaderDataTableStyle));
            sheet.addCell(new Number(col++, rowIndex + 1, 4, _horHeaderDataTableStyle));
            sheet.addCell(new Number(col++, rowIndex + 1, 3, _horHeaderDataTableStyle));
            sheet.addCell(new Number(col++, rowIndex + 1, 2, _horHeaderDataTableStyle));

            addTextCell(sheet, col++, rowIndex, 1, 2, "Успеваемость, %", _verHeaderDataTableStyle);
            addTextCell(sheet, col++, rowIndex, 1, 2, "Качественный \r\nпоказатель, %", _verHeaderDataTableStyle);

            _tableWidth = col - 1;

            rowIndex = rowIndex + 2;
            for (; col > 0; col--)// номера колонок
                sheet.addCell(new Number(col - 1, rowIndex, col, _horHeaderBoldDataTableStyle));

            fillArea(sheet, 0, _tableWidth, firstRow - 1, firstRow - 1, _topThickBoardTS);
            fillArea(sheet, _tableWidth + 1, _tableWidth + 1, firstRow, rowIndex, _rightThickTableBoardTS);
        }
        catch (WriteException e)
        {
            e.printStackTrace();
        }

        return ++rowIndex;
    }

    protected List<Row> getRows(SfaReportInfo<SfaRaw> reportInfo)
    {
        TreeMap<EppRegistryElement, List<SfaRaw>> rawsByRegEl = reportInfo.getRaws().stream().collect(Collectors.groupingBy(
                SfaRaw::getRegistryElement,
                () -> new TreeMap<>(CommonCollator.TITLED_WITH_ID_COMPARATOR),
                Collectors.mapping(raw -> raw, Collectors.toList())
        ));

        return rawsByRegEl.entrySet().stream()
                .map(e ->
                     {
                         List<SfaRaw> raw4RegEl = e.getValue();
                         Row row = newRegElRow(reportInfo, e.getKey().getTitle());
                         row.addFromRaw(raw4RegEl);
                         return row;
                     })
                .collect(Collectors.toList());
    }

    @Override
    protected int printNote(SfaReportInfo<SfaRaw> reportInfo, WritableSheet sheet, int row)
    {
        addTextCell(sheet, 0, row++, 2, 1, "* Примечание: ", _noteStyle);
        addTextCell(sheet, 1, row++, 1, 1, "1) значения в последних двух столбцах расчитываются автоматически.", _noteStyle);
        addTextCell(sheet, 1, row++, 1, 1, "2) таблица заполняется по каждой форме обучения отдельно.", _noteStyle);
        return row;
    }


    @Override
    protected void addDataRow(WritableSheet sheet, int startCol, int rowIndex, Row row)
    {
        addIntCell(sheet, startCol++, rowIndex, row.getTotal(), row.getIntStyle());
        addIntCell(sheet, startCol++, rowIndex, row.getVacation(), row.getIntStyle());
        addIntCell(sheet, startCol++, rowIndex, row.getMust(), row.getIntStyle());
        addIntCell(sheet, startCol++, rowIndex, row.getDelay(), row.getIntStyle());
        addIntCell(sheet, startCol++, rowIndex, row.getNotAllowed(), row.getIntStyle());
        addIntCell(sheet, startCol++, rowIndex, row.getAppeared(), row.getIntStyle());
        addIntCell(sheet, startCol++, rowIndex, row.getPassed(), row.getIntStyle());

        addIntCell(sheet, startCol++, rowIndex, row.getBudget(), row.getIntStyle());

        addIntCell(sheet, startCol++, rowIndex, row.getMark5(), row.getIntStyle());
        addIntCell(sheet, startCol++, rowIndex, row.getMark4(), row.getIntStyle());
        addIntCell(sheet, startCol++, rowIndex, row.getMark3(), row.getIntStyle());
        addIntCell(sheet, startCol++, rowIndex, row.getMark2(), row.getRedIntStyle());

        addDoubleCell(sheet, startCol++, rowIndex, row.getProgress(), row.getDoubleStyle());
        addDoubleCell(sheet, startCol, rowIndex, row.getGoodProgress(), row.getDoubleStyle());
    }

    @Override
    protected Row newRegElRow(SfaReportInfo<SfaRaw> reportInfo, String title)
    {
        return new Row(title, _titleColumnDataTableStyle, _intColumnDataTableStyle, _intRedColumnDataTableStyle, _doubleColumnDataTableStyle);
    }

    @Override
    protected Row newTotalDirectionRow(SfaReportInfo<SfaRaw> reportInfo, String title)
    {
        return new Row(title, _totalTitleColumnDataTableStyle, _totalIntColumnDataTableStyle, _totalRedIntColumnDataTableStyle, _totalDoubleColumnDataTableStyle);
    }
}