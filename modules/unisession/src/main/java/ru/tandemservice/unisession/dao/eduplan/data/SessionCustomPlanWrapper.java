/* $Id$ */
package ru.tandemservice.unisession.dao.eduplan.data;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uni.dao.grid.IDevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.*;
import ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvGroupImRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow;
import ru.tandemservice.uniepp.entity.plan.data.IEppEpvRow;
import ru.tandemservice.unisession.entity.document.SessionTransferProtocolDocument;
import ru.tandemservice.unisession.entity.document.SessionTransferProtocolRow;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Alexey Lopatin
 * @since 25.11.2015
 */
public class SessionCustomPlanWrapper implements IEppCustomPlanWrapper
{
    private final IEppEpvBlockWrapper _blockWrapper;
    private final EppCustomEduPlan _customPlan;
    private final Map<Integer, Collection<IEppCustomPlanRowWrapper>> _rowMap;
    private final int _gridSize;
    private final Map<PairKey<Long, Integer>, IEppCustomPlanRowWrapper> _srcMap; // { [srcRow.id + source term number] -> custom row wrapper }

    public SessionCustomPlanWrapper(EppCustomEduPlan customPlan, SessionTransferProtocolDocument protocol)
    {
        Preconditions.checkNotNull(customPlan);
        Preconditions.checkNotNull(protocol);
        Preconditions.checkNotNull(protocol.getWorkPlanVersion());

        _blockWrapper = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockData(customPlan.getEpvBlock().getId(), true);
        _customPlan = customPlan;

        // Заполняем список семестров, число семестров берется из учебной сетки индивидуального плана.
        // Пусть n - номер семестра из версии РУП.
        // Последний семестр ИУП соответствует последнему семестру исходного УП. Далее декрементируем номер семестра там и там. Продолжаем до (n + 1) семестра включительно.
        // Семестры с номером строго меньше n, остаются с тем же семестром что и были.
        // Все остальные семестры перекидываются в семестр n

        // Номер семестра из версии РУП
        int protocolTermNumber = protocol.getWorkPlanVersion().getTerm().getIntValue();

        // Строки протокола, которые нужно перезачесть/переаттестовать
        List<SessionTransferProtocolRow> rows = DataAccessServices.dao().getList(SessionTransferProtocolRow.class, SessionTransferProtocolRow.protocol().id(), protocol.getId());
        Map<EppEpvRowTerm, SessionTransferProtocolRow> protocolRowMap = rows.stream().collect(Collectors.toMap(row -> row.getRequestRow().getRowTerm(), row -> row));

        Map<PairKey<Long, Integer>, EppEpvRowTerm> protocolRowTerms = Maps.newHashMap();
        protocolRowMap.keySet().forEach(rowTerm -> protocolRowTerms.put(new PairKey<>(rowTerm.getRow().getId(), rowTerm.getTerm().getIntValue()), rowTerm));

        // Количество семестров исходного УП
        final Integer sourceTermsSize = IDevelopGridDAO.instance.get().getDevelopGridSize(_blockWrapper.getVersion().getEduPlan().getDevelopGrid());
        // Количество семестров ИУП
        _gridSize = IDevelopGridDAO.instance.get().getDevelopGridSize(customPlan.getDevelopGrid());
        // Смещение семестров
        final int offset = sourceTermsSize - _gridSize;

        // Строки исходного УП
        final Collection<IEppEpvRowWrapper> srcRows = _blockWrapper.getRowMap().values();
        // Мапа [epv row id] -> [term numbers list]. Для опрпделения индекса семестра, если их несколько, - для вывода названий строк в ИУП.
        final Map<Long, List<Integer>> termIndexMap = new HashMap<>(srcRows.size());

        // Группы ДВ, которым необходимо поменять семестр
        Set<Long> newTermGroupImRows = Sets.newHashSet();

        // Строки ИУП на основе строк УП. [srcRow.id + source term] -> custom row wrapper
        _srcMap = new HashMap<>(srcRows.size());
        for (IEppEpvRowWrapper srcRow : srcRows)
        {
            if (!(srcRow.getRow() instanceof EppEpvTermDistributedRow)) continue; // нужны только строки с нагрузкой
            EppEpvTermDistributedRow distRow = (EppEpvTermDistributedRow) srcRow.getRow();

            final List<Integer> termList = new ArrayList<>(srcRow.getActiveTermSet().size());
            for (Integer term : srcRow.getActiveTermSet())
            {
                Preconditions.checkState(term > 0);
                if (term > sourceTermsSize) continue; // кривой УП

                final PairKey<Long, Integer> key = new PairKey<>(distRow.getId(), term);
                final IEppCustomPlanRowWrapper customRowWrapper = new EppCustomPlanRowWrapper(srcRow, term);

                EppEpvRowTerm rowTerm = protocolRowTerms.get(key);
                if (null != rowTerm)
                {
                    SessionTransferProtocolRow protocolRow = protocolRowMap.get(rowTerm);

                    customRowWrapper.setNewTermNumber(protocolTermNumber);
                    customRowWrapper.setExcluded(false);
                    customRowWrapper.setNeedRetake(protocolRow.isNeedRetake());

                    IEppEpvRow parent = distRow.getParent();
                    if (null != parent && parent instanceof EppEpvGroupImRow)
                    {
                        newTermGroupImRows.add(parent.getId());
                    }
                }
                else
                {
                    if (customRowWrapper.getSourceTermNumber() < protocolTermNumber)
                        customRowWrapper.setNewTermNumber(customRowWrapper.getSourceTermNumber());
                    else
                        customRowWrapper.setNewTermNumber(Math.max(term - offset, protocolTermNumber));

                    customRowWrapper.setExcluded(false);
                    customRowWrapper.setNeedRetake(null);
                }
                _srcMap.put(key, customRowWrapper);
                termList.add(term);
            }
            termIndexMap.put(distRow.getId(), termList);
        }

        // Группы ДВ должны быть в том же семестре, что и сами перезачтенные дисциплины
        _srcMap.entrySet().stream()
                .filter(entry -> newTermGroupImRows.contains(entry.getKey().getFirst()))
                .forEach(entry -> entry.getValue().setNewTermNumber(protocolTermNumber));

        // Расставляем названия (дописываем индекс семестра, если семестров несколько у строки УПв)
        for (Map.Entry<PairKey<Long, Integer>, IEppCustomPlanRowWrapper> entry : _srcMap.entrySet())
        {
            final Long epRowId = entry.getKey().getFirst();
            final Integer sourceTermNumber = entry.getKey().getSecond();
            final List<Integer> termList = termIndexMap.get(epRowId);
            final IEppCustomPlanRowWrapper customRowWrapper = entry.getValue();
            if (termList.size() != 1)
            {
                final int idx = termList.indexOf(sourceTermNumber);
                Preconditions.checkState(idx >= 0);

                final EppCustomPlanRowWrapper wrapper = (EppCustomPlanRowWrapper) customRowWrapper;
                wrapper.setTitle(customRowWrapper.getSourceRow().getTitle() + " (" + (idx + 1) + "/" + termList.size() + ")");
            }
        }

        // Складываем всё в общую мапу.
        _rowMap = new TreeMap<>();
        for (IEppCustomPlanRowWrapper wrapper : _srcMap.values())
        {
            Collection<IEppCustomPlanRowWrapper> set = _rowMap.get(wrapper.getNewTermNumber());
            if (set == null)
                _rowMap.put(wrapper.getNewTermNumber(), set = new TreeSet<>());
            set.add(wrapper);
        }

        // Если в каком-то семестре не оказалось строки, он всё равно должен быть в мапе, хоть и с пустым набором строк
        for (int i = 1; i <= _gridSize; i++)
        {
            if (!_rowMap.containsKey(i))
                _rowMap.put(i, new TreeSet<>());
        }
    }

    @Override
    public IEppEpvBlockWrapper getBlockWrapper()
    {
        return _blockWrapper;
    }

    @Override
    public DevelopGrid getDevelopGrid()
    {
        return _customPlan.getDevelopGrid();
    }

    @Override
    public DevelopCondition getDevelopCondition()
    {
        return _customPlan.getDevelopCondition();
    }

    @Override
    public int getGridSize()
    {
        return _gridSize;
    }

    @Override
    public Map<Integer, Collection<IEppCustomPlanRowWrapper>> getRowMap()
    {
        return _rowMap;
    }

    @Override
    public Map<PairKey<Long, Integer>, IEppCustomPlanRowWrapper> getSourceMap()
    {
        return _srcMap;
    }
}
