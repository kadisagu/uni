/* $Id:$ */
package ru.tandemservice.unisession.base.bo.SessionReport.util.resultsByDisc;

import ru.tandemservice.unisession.base.bo.SessionReport.ui.ResultsByDiscAdd.ISessionReportResultsByDiscDAO;

import java.util.HashMap;
import java.util.Map;

/**
 * @author oleyba
 * @since 2/20/12
 */
public abstract class CountColumn implements ISessionReportResultsByDiscDAO.ISessionReportResultsColumn, PercentageColumn.NumberColumn
{
    private Map<Object, Integer> cache = new HashMap<Object, Integer>();
    
    public abstract boolean check(ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscMarkData mark);

    @Override
    public Number getCount(ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscRow row)
    {
        Integer value = cache.get(row.getKey());
        if (null == value) {
            value = 0;
            for (ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscMarkData mark : row.getMarks())
                if (check(mark))
                    value++;
            cache.put(row.getKey(), value);
        }

        return value;
    }

    @Override
    public String cell(ISessionReportResultsByDiscDAO.ISessionReportResultsByDiscRow row)
    {
        return String.valueOf(getCount(row));
    }
}
