package ru.tandemservice.unisession.entity.document.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unisession.entity.document.SessionObject;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сессия
 *
 * Сессия - определяет, что на указанном подразделении (деканат) в рамках указанного года (и его части) будет проводится промежуточная аттестация студентов.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionObjectGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.document.SessionObject";
    public static final String ENTITY_NAME = "sessionObject";
    public static final int VERSION_HASH = 165807666;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORG_UNIT = "orgUnit";
    public static final String L_EDUCATION_YEAR = "educationYear";
    public static final String L_YEAR_DISTRIBUTION_PART = "yearDistributionPart";
    public static final String P_NUMBER = "number";
    public static final String P_FORMING_DATE = "formingDate";
    public static final String P_CLOSE_DATE = "closeDate";
    public static final String P_STARTUP_DATE = "startupDate";
    public static final String P_DEADLINE_DATE = "deadlineDate";
    public static final String P_COMMENT = "comment";
    public static final String P_TERM_TITLE = "termTitle";
    public static final String P_TITLE = "title";

    private OrgUnit _orgUnit;     // Подразделение
    private EducationYear _educationYear;     // Учебный год
    private YearDistributionPart _yearDistributionPart;     // Часть года
    private String _number;     // Номер документа
    private Date _formingDate;     // Дата формирования (фактическая)
    private Date _closeDate;     // Дата закрытия (фактическая)
    private Date _startupDate;     // Дата начала сессии (фактическая)
    private Date _deadlineDate;     // Дата окончания сессии (фактическая)
    private String _comment;     // Комментарий

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение. Свойство не может быть null.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     */
    @NotNull
    public EducationYear getEducationYear()
    {
        return _educationYear;
    }

    /**
     * @param educationYear Учебный год. Свойство не может быть null.
     */
    public void setEducationYear(EducationYear educationYear)
    {
        dirty(_educationYear, educationYear);
        _educationYear = educationYear;
    }

    /**
     * @return Часть года. Свойство не может быть null.
     */
    @NotNull
    public YearDistributionPart getYearDistributionPart()
    {
        return _yearDistributionPart;
    }

    /**
     * @param yearDistributionPart Часть года. Свойство не может быть null.
     */
    public void setYearDistributionPart(YearDistributionPart yearDistributionPart)
    {
        dirty(_yearDistributionPart, yearDistributionPart);
        _yearDistributionPart = yearDistributionPart;
    }

    /**
     * @return Номер документа. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер документа. Свойство не может быть null.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Дата формирования (фактическая). Свойство не может быть null.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата формирования (фактическая). Свойство не может быть null.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    /**
     * @return Дата закрытия (фактическая).
     */
    public Date getCloseDate()
    {
        return _closeDate;
    }

    /**
     * @param closeDate Дата закрытия (фактическая).
     */
    public void setCloseDate(Date closeDate)
    {
        dirty(_closeDate, closeDate);
        _closeDate = closeDate;
    }

    /**
     * @return Дата начала сессии (фактическая). Свойство не может быть null.
     */
    @NotNull
    public Date getStartupDate()
    {
        return _startupDate;
    }

    /**
     * @param startupDate Дата начала сессии (фактическая). Свойство не может быть null.
     */
    public void setStartupDate(Date startupDate)
    {
        dirty(_startupDate, startupDate);
        _startupDate = startupDate;
    }

    /**
     * @return Дата окончания сессии (фактическая). Свойство не может быть null.
     */
    @NotNull
    public Date getDeadlineDate()
    {
        return _deadlineDate;
    }

    /**
     * @param deadlineDate Дата окончания сессии (фактическая). Свойство не может быть null.
     */
    public void setDeadlineDate(Date deadlineDate)
    {
        dirty(_deadlineDate, deadlineDate);
        _deadlineDate = deadlineDate;
    }

    /**
     * @return Комментарий.
     */
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SessionObjectGen)
        {
            setOrgUnit(((SessionObject)another).getOrgUnit());
            setEducationYear(((SessionObject)another).getEducationYear());
            setYearDistributionPart(((SessionObject)another).getYearDistributionPart());
            setNumber(((SessionObject)another).getNumber());
            setFormingDate(((SessionObject)another).getFormingDate());
            setCloseDate(((SessionObject)another).getCloseDate());
            setStartupDate(((SessionObject)another).getStartupDate());
            setDeadlineDate(((SessionObject)another).getDeadlineDate());
            setComment(((SessionObject)another).getComment());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionObjectGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionObject.class;
        }

        public T newInstance()
        {
            return (T) new SessionObject();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "educationYear":
                    return obj.getEducationYear();
                case "yearDistributionPart":
                    return obj.getYearDistributionPart();
                case "number":
                    return obj.getNumber();
                case "formingDate":
                    return obj.getFormingDate();
                case "closeDate":
                    return obj.getCloseDate();
                case "startupDate":
                    return obj.getStartupDate();
                case "deadlineDate":
                    return obj.getDeadlineDate();
                case "comment":
                    return obj.getComment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "educationYear":
                    obj.setEducationYear((EducationYear) value);
                    return;
                case "yearDistributionPart":
                    obj.setYearDistributionPart((YearDistributionPart) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
                case "closeDate":
                    obj.setCloseDate((Date) value);
                    return;
                case "startupDate":
                    obj.setStartupDate((Date) value);
                    return;
                case "deadlineDate":
                    obj.setDeadlineDate((Date) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "orgUnit":
                        return true;
                case "educationYear":
                        return true;
                case "yearDistributionPart":
                        return true;
                case "number":
                        return true;
                case "formingDate":
                        return true;
                case "closeDate":
                        return true;
                case "startupDate":
                        return true;
                case "deadlineDate":
                        return true;
                case "comment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "orgUnit":
                    return true;
                case "educationYear":
                    return true;
                case "yearDistributionPart":
                    return true;
                case "number":
                    return true;
                case "formingDate":
                    return true;
                case "closeDate":
                    return true;
                case "startupDate":
                    return true;
                case "deadlineDate":
                    return true;
                case "comment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "educationYear":
                    return EducationYear.class;
                case "yearDistributionPart":
                    return YearDistributionPart.class;
                case "number":
                    return String.class;
                case "formingDate":
                    return Date.class;
                case "closeDate":
                    return Date.class;
                case "startupDate":
                    return Date.class;
                case "deadlineDate":
                    return Date.class;
                case "comment":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionObject> _dslPath = new Path<SessionObject>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionObject");
    }
            

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionObject#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionObject#getEducationYear()
     */
    public static EducationYear.Path<EducationYear> educationYear()
    {
        return _dslPath.educationYear();
    }

    /**
     * @return Часть года. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionObject#getYearDistributionPart()
     */
    public static YearDistributionPart.Path<YearDistributionPart> yearDistributionPart()
    {
        return _dslPath.yearDistributionPart();
    }

    /**
     * @return Номер документа. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionObject#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Дата формирования (фактическая). Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionObject#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * @return Дата закрытия (фактическая).
     * @see ru.tandemservice.unisession.entity.document.SessionObject#getCloseDate()
     */
    public static PropertyPath<Date> closeDate()
    {
        return _dslPath.closeDate();
    }

    /**
     * @return Дата начала сессии (фактическая). Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionObject#getStartupDate()
     */
    public static PropertyPath<Date> startupDate()
    {
        return _dslPath.startupDate();
    }

    /**
     * @return Дата окончания сессии (фактическая). Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionObject#getDeadlineDate()
     */
    public static PropertyPath<Date> deadlineDate()
    {
        return _dslPath.deadlineDate();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unisession.entity.document.SessionObject#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.entity.document.SessionObject#getTermTitle()
     */
    public static SupportedPropertyPath<String> termTitle()
    {
        return _dslPath.termTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.entity.document.SessionObject#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends SessionObject> extends EntityPath<E>
    {
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private EducationYear.Path<EducationYear> _educationYear;
        private YearDistributionPart.Path<YearDistributionPart> _yearDistributionPart;
        private PropertyPath<String> _number;
        private PropertyPath<Date> _formingDate;
        private PropertyPath<Date> _closeDate;
        private PropertyPath<Date> _startupDate;
        private PropertyPath<Date> _deadlineDate;
        private PropertyPath<String> _comment;
        private SupportedPropertyPath<String> _termTitle;
        private SupportedPropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionObject#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionObject#getEducationYear()
     */
        public EducationYear.Path<EducationYear> educationYear()
        {
            if(_educationYear == null )
                _educationYear = new EducationYear.Path<EducationYear>(L_EDUCATION_YEAR, this);
            return _educationYear;
        }

    /**
     * @return Часть года. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionObject#getYearDistributionPart()
     */
        public YearDistributionPart.Path<YearDistributionPart> yearDistributionPart()
        {
            if(_yearDistributionPart == null )
                _yearDistributionPart = new YearDistributionPart.Path<YearDistributionPart>(L_YEAR_DISTRIBUTION_PART, this);
            return _yearDistributionPart;
        }

    /**
     * @return Номер документа. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionObject#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(SessionObjectGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Дата формирования (фактическая). Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionObject#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(SessionObjectGen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * @return Дата закрытия (фактическая).
     * @see ru.tandemservice.unisession.entity.document.SessionObject#getCloseDate()
     */
        public PropertyPath<Date> closeDate()
        {
            if(_closeDate == null )
                _closeDate = new PropertyPath<Date>(SessionObjectGen.P_CLOSE_DATE, this);
            return _closeDate;
        }

    /**
     * @return Дата начала сессии (фактическая). Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionObject#getStartupDate()
     */
        public PropertyPath<Date> startupDate()
        {
            if(_startupDate == null )
                _startupDate = new PropertyPath<Date>(SessionObjectGen.P_STARTUP_DATE, this);
            return _startupDate;
        }

    /**
     * @return Дата окончания сессии (фактическая). Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionObject#getDeadlineDate()
     */
        public PropertyPath<Date> deadlineDate()
        {
            if(_deadlineDate == null )
                _deadlineDate = new PropertyPath<Date>(SessionObjectGen.P_DEADLINE_DATE, this);
            return _deadlineDate;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unisession.entity.document.SessionObject#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(SessionObjectGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.entity.document.SessionObject#getTermTitle()
     */
        public SupportedPropertyPath<String> termTitle()
        {
            if(_termTitle == null )
                _termTitle = new SupportedPropertyPath<String>(SessionObjectGen.P_TERM_TITLE, this);
            return _termTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.entity.document.SessionObject#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(SessionObjectGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return SessionObject.class;
        }

        public String getEntityName()
        {
            return "sessionObject";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getTermTitle();

    public abstract String getTitle();
}
