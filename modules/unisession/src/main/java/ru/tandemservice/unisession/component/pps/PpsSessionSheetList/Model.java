// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisession.component.pps.PpsSessionSheetList;

import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.unisession.entity.document.SessionSheetDocument;

/**
 * @author iolshvang
 * @since 11.04.2011
 */

public class Model
{
    private IPrincipalContext _principalContext;
    private Person person;

    private DynamicListDataSource<SessionSheetDocument> dataSource;

    private IDataSettings settings;

    private ISelectModel eduYearModel;
    private ISelectModel courseModel;
    private ISelectModel stateModel;

    public DynamicListDataSource<SessionSheetDocument> getDataSource()
    {
        return this.dataSource;
    }

    public void setDataSource(final DynamicListDataSource<SessionSheetDocument> dataSource)
    {
        this.dataSource = dataSource;
    }

    public IDataSettings getSettings()
    {
        return this.settings;
    }

    public void setSettings(final IDataSettings settings)
    {
        this.settings = settings;
    }

    public ISelectModel getEduYearModel()
    {
        return this.eduYearModel;
    }

    public void setEduYearModel(final ISelectModel eduYearModel)
    {
        this.eduYearModel = eduYearModel;
    }

    public ISelectModel getCourseModel()
    {
        return this.courseModel;
    }

    public void setCourseModel(final ISelectModel courseModel)
    {
        this.courseModel = courseModel;
    }

    public ISelectModel getStateModel()
    {
        return this.stateModel;
    }

    public void setStateModel(final ISelectModel stateModel)
    {
        this.stateModel = stateModel;
    }

    public IPrincipalContext getPrincipalContext()
    {
        return this._principalContext;
    }

    public void setPrincipalContext(final IPrincipalContext principalContext)
    {
        this._principalContext = principalContext;
    }

    public Person getPerson()
    {
        return this.person;
    }

    public void setPerson(final Person person)
    {
        this.person = person;
    }
}

