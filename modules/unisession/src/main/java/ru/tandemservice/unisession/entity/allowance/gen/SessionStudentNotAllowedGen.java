package ru.tandemservice.unisession.entity.allowance.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unisession.entity.allowance.ISessionStudentNotAllowed;
import ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowed;
import ru.tandemservice.unisession.entity.document.SessionObject;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Недопуск студента к сдаче сессии
 *
 * Недопуск студента к сдаче всех мероприятий в данной сессии.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionStudentNotAllowedGen extends EntityBase
 implements ISessionStudentNotAllowed, INaturalIdentifiable<SessionStudentNotAllowedGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowed";
    public static final String ENTITY_NAME = "sessionStudentNotAllowed";
    public static final int VERSION_HASH = 1355597333;
    private static IEntityMeta ENTITY_META;

    public static final String L_SESSION = "session";
    public static final String L_STUDENT = "student";
    public static final String P_CREATE_DATE = "createDate";
    public static final String P_REMOVAL_DATE = "removalDate";

    private SessionObject _session;     // Сессия
    private Student _student;     // Студент
    private Date _createDate;     // Дата создания
    private Date _removalDate;     // Дата снятия

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сессия. Свойство не может быть null.
     */
    @NotNull
    public SessionObject getSession()
    {
        return _session;
    }

    /**
     * @param session Сессия. Свойство не может быть null.
     */
    public void setSession(SessionObject session)
    {
        dirty(_session, session);
        _session = session;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Дата создания.
     */
    public Date getCreateDate()
    {
        return _createDate;
    }

    /**
     * @param createDate Дата создания.
     */
    public void setCreateDate(Date createDate)
    {
        dirty(_createDate, createDate);
        _createDate = createDate;
    }

    /**
     * @return Дата снятия.
     */
    public Date getRemovalDate()
    {
        return _removalDate;
    }

    /**
     * @param removalDate Дата снятия.
     */
    public void setRemovalDate(Date removalDate)
    {
        dirty(_removalDate, removalDate);
        _removalDate = removalDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SessionStudentNotAllowedGen)
        {
            if (withNaturalIdProperties)
            {
                setSession(((SessionStudentNotAllowed)another).getSession());
                setStudent(((SessionStudentNotAllowed)another).getStudent());
            }
            setCreateDate(((SessionStudentNotAllowed)another).getCreateDate());
            setRemovalDate(((SessionStudentNotAllowed)another).getRemovalDate());
        }
    }

    public INaturalId<SessionStudentNotAllowedGen> getNaturalId()
    {
        return new NaturalId(getSession(), getStudent());
    }

    public static class NaturalId extends NaturalIdBase<SessionStudentNotAllowedGen>
    {
        private static final String PROXY_NAME = "SessionStudentNotAllowedNaturalProxy";

        private Long _session;
        private Long _student;

        public NaturalId()
        {}

        public NaturalId(SessionObject session, Student student)
        {
            _session = ((IEntity) session).getId();
            _student = ((IEntity) student).getId();
        }

        public Long getSession()
        {
            return _session;
        }

        public void setSession(Long session)
        {
            _session = session;
        }

        public Long getStudent()
        {
            return _student;
        }

        public void setStudent(Long student)
        {
            _student = student;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof SessionStudentNotAllowedGen.NaturalId) ) return false;

            SessionStudentNotAllowedGen.NaturalId that = (NaturalId) o;

            if( !equals(getSession(), that.getSession()) ) return false;
            if( !equals(getStudent(), that.getStudent()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getSession());
            result = hashCode(result, getStudent());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getSession());
            sb.append("/");
            sb.append(getStudent());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionStudentNotAllowedGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionStudentNotAllowed.class;
        }

        public T newInstance()
        {
            return (T) new SessionStudentNotAllowed();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "session":
                    return obj.getSession();
                case "student":
                    return obj.getStudent();
                case "createDate":
                    return obj.getCreateDate();
                case "removalDate":
                    return obj.getRemovalDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "session":
                    obj.setSession((SessionObject) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "createDate":
                    obj.setCreateDate((Date) value);
                    return;
                case "removalDate":
                    obj.setRemovalDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "session":
                        return true;
                case "student":
                        return true;
                case "createDate":
                        return true;
                case "removalDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "session":
                    return true;
                case "student":
                    return true;
                case "createDate":
                    return true;
                case "removalDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "session":
                    return SessionObject.class;
                case "student":
                    return Student.class;
                case "createDate":
                    return Date.class;
                case "removalDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionStudentNotAllowed> _dslPath = new Path<SessionStudentNotAllowed>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionStudentNotAllowed");
    }
            

    /**
     * @return Сессия. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowed#getSession()
     */
    public static SessionObject.Path<SessionObject> session()
    {
        return _dslPath.session();
    }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowed#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Дата создания.
     * @see ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowed#getCreateDate()
     */
    public static PropertyPath<Date> createDate()
    {
        return _dslPath.createDate();
    }

    /**
     * @return Дата снятия.
     * @see ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowed#getRemovalDate()
     */
    public static PropertyPath<Date> removalDate()
    {
        return _dslPath.removalDate();
    }

    public static class Path<E extends SessionStudentNotAllowed> extends EntityPath<E>
    {
        private SessionObject.Path<SessionObject> _session;
        private Student.Path<Student> _student;
        private PropertyPath<Date> _createDate;
        private PropertyPath<Date> _removalDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сессия. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowed#getSession()
     */
        public SessionObject.Path<SessionObject> session()
        {
            if(_session == null )
                _session = new SessionObject.Path<SessionObject>(L_SESSION, this);
            return _session;
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowed#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Дата создания.
     * @see ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowed#getCreateDate()
     */
        public PropertyPath<Date> createDate()
        {
            if(_createDate == null )
                _createDate = new PropertyPath<Date>(SessionStudentNotAllowedGen.P_CREATE_DATE, this);
            return _createDate;
        }

    /**
     * @return Дата снятия.
     * @see ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowed#getRemovalDate()
     */
        public PropertyPath<Date> removalDate()
        {
            if(_removalDate == null )
                _removalDate = new PropertyPath<Date>(SessionStudentNotAllowedGen.P_REMOVAL_DATE, this);
            return _removalDate;
        }

        public Class getEntityClass()
        {
            return SessionStudentNotAllowed.class;
        }

        public String getEntityName()
        {
            return "sessionStudentNotAllowed";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
