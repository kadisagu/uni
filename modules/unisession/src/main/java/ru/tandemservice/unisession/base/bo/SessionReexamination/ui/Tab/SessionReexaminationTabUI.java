/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReexamination.ui.Tab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unisession.base.bo.SessionReexamination.ui.AddEdit.SessionReexaminationAddEdit;
import ru.tandemservice.unisession.base.bo.SessionReexamination.ui.AddEdit.SessionReexaminationAddEditUI;

/**
 * @author Alexey Lopatin
 * @since 25.08.2015
 */
@State({@Bind(key = UIPresenter.PUBLISHER_ID, binding = "student.id", required = true)})
public class SessionReexaminationTabUI extends UIPresenter
{
    public static final String PARAM_STUDENT_ID = "studentId";

    private Student _student = new Student();

    @Override
    public void onComponentRefresh()
    {
        _student = DataAccessServices.dao().getNotNull(getStudent().getId());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (SessionReexaminationTab.REQUEST_DS.equals(dataSource.getName()))
        {
            dataSource.put(PARAM_STUDENT_ID, getStudent().getId());
        }
    }

    public void onClickRequestAdd()
    {
        _uiActivation.asRegion(SessionReexaminationAddEdit.class)
                .parameter(SessionReexaminationAddEditUI.PARAM_STUDENT_ID, getStudent().getId())
                .top().activate();
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asRegion(SessionReexaminationAddEdit.class)
                .parameter(SessionReexaminationAddEditUI.PARAM_STUDENT_ID, getStudent().getId())
                .parameter(SessionReexaminationAddEditUI.PARAM_REQUEST_ID, getListenerParameterAsLong())
                .top().activate();
    }

    public void onDeleteEntityFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public Student getStudent()
    {
        return _student;
    }

    public void setStudent(Student student)
    {
        _student = student;
    }
}
