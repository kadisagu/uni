/* $*/

package ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinPubInline;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.common.IIdentifiable;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.CollectionFactoryUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.ui.SimpleRowCustomizer;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.brs.util.BrsRatingValueFormatter;
import ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowed;
import ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowedForBulletin;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.catalog.codes.SessionMarkStateCatalogItemCodes;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.*;
import ru.tandemservice.unisession.entity.mark.*;
import ru.tandemservice.unisession.entity.stateFinalExam.SessionStateFinalExamProtocol;
import ru.tandemservice.unisession.practice.dao.ISessionPracticeDao;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 2/18/11
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setBulletin(this.getNotNull(SessionBulletinDocument.class, model.getBulletin().getId()));

        Map<ISessionBrsDao.ISessionRatingSettingsKey, ISessionBrsDao.ISessionRatingSettings> ratingSettings = ISessionBrsDao.instance.get().getRatingSettings(model.getBulletin());
        model.setUseCurrentRating(CollectionUtils.exists(ratingSettings.values(), ISessionBrsDao.ISessionRatingSettings::useCurrentRating));
        model.setUsePoints(CollectionUtils.exists(ratingSettings.values(), ISessionBrsDao.ISessionRatingSettings::usePoints));

        model.setShowPracticeTutorMarkColumn(ISessionPracticeDao.instance.get().showPracticeTutorMarkColumnInBulletin(model.getBulletin()));
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareListDataSource(final Model model)
    {
        final DynamicListDataSource dataSource = model.getDataSource();

        // студенты
        final DQLSelectBuilder slotDQL = new DQLSelectBuilder()
                .fromEntity(SessionDocumentSlot.class, "slot")
                .column("slot");
        this.fetchStudentData(slotDQL, "slot");
        slotDQL.where(eq(property(SessionDocumentSlot.document().fromAlias("slot")), value(model.getBulletin())));
        final DQLOrderDescriptionRegistry order = new DQLOrderDescriptionRegistry(SessionDocumentSlot.class, "slot");
        order.setOrders("person.fullFio", new OrderDescription(SessionDocumentSlot.actualStudent().person().identityCard().fullFio().s()));
        order.applyOrder(slotDQL, dataSource.getEntityOrder());
        final List<SessionDocumentSlot> slotList = slotDQL.createStatement(this.getSession()).list();
        UniBaseUtils.createFullPage(dataSource, slotList);

        // оценки
        final Map<SessionDocumentSlot, SessionMark> markMap = new HashMap<>();
        final Map<SessionDocumentSlot, String> pointsMap = new HashMap<>();
        for (final SessionMark mark : getList(SessionMark.class, SessionMark.slot().document(), model.getBulletin()))
        {
            markMap.put(mark.getSlot(), mark);
            pointsMap.put(mark.getSlot(), BrsRatingValueFormatter.instance.get().format(mark.getPoints()));
        }
        final Map<SessionDocumentSlot, String> scoredPointsMap = new HashMap<>();
        for (final SessionMarkRatingData mark : getList(SessionMarkRatingData.class, SessionMarkRatingData.mark().slot().document(), model.getBulletin()))
        {
            scoredPointsMap.put(mark.getMark().getSlot(), BrsRatingValueFormatter.instance.get().format(mark.getScoredPoints()));
        }
        final Map<SessionDocumentSlot, String> currentRatingMap = new HashMap<>();
        for (final SessionSlotRatingData mark : getList(SessionSlotRatingData.class, SessionSlotRatingData.slot().document(), model.getBulletin()))
        {
            currentRatingMap.put(mark.getSlot(), BrsRatingValueFormatter.instance.get().format(mark.getFixedCurrentRating()));
        }

        dataSource.setRowCustomizer(new SimpleRowCustomizer<ViewWrapper<SessionDocumentSlot>>()
        {
            @Override
            public String getRowStyle(final ViewWrapper<SessionDocumentSlot> row)
            {
                SessionMark mark = markMap.get(row.getEntity());
                if (mark instanceof SessionSlotMarkState && ((SessionSlotMarkState) mark).getValueItem().getCode().equals(SessionMarkStateCatalogItemCodes.NOT_APPEAR_UNKNOWN))
                    return "background-color: " + UniDefines.COLOR_YELLOW + ";";
                return "";
            }
        });

        // число пересдач
        DQLSelectBuilder markDQL = new DQLSelectBuilder()
                .fromEntity(SessionSlotMarkGradeValue.class, "m")
                .fromEntity(SessionDocumentSlot.class, "slot")
                .column("m.id")
                .column("slot")
                .where(eq(property(SessionDocumentSlot.document().fromAlias("slot")), value(model.getBulletin())))
                .where(eq(property(SessionDocumentSlot.studentWpeCAction().fromAlias("slot")), property(SessionSlotMarkGradeValue.slot().studentWpeCAction().fromAlias("m"))));
        final Map<SessionDocumentSlot, Integer> markCountMap = new HashMap<>();
        for (final Object[] row : markDQL.createStatement(this.getSession()).<Object[]>list())
        {
            final SessionDocumentSlot slot = (SessionDocumentSlot) row[1];
            final Integer current = markCountMap.get(slot);
            markCountMap.put(slot, current == null ? 1 : current + 1);
        }

        // допуски
        final List<Student> studentList = CommonBaseUtil.getPropertiesList(slotList, SessionDocumentSlot.actualStudent().s());
        final Collection<Long> studentIds = ids(studentList);

        final DQLSelectBuilder naSessionDQL = new DQLSelectBuilder()
                .fromEntity(SessionStudentNotAllowed.class, "rel")
                .column(property(SessionStudentNotAllowed.student().id().fromAlias("rel")))
                .where(isNull(property(SessionStudentNotAllowed.removalDate().fromAlias("rel"))))
                .where(eq(property(SessionStudentNotAllowed.session().fromAlias("rel")), value(model.getBulletin().getSessionObject())))
                .where(in(property(SessionStudentNotAllowed.student().id().fromAlias("rel")), studentIds));
        final Set<Long> studentsNotAllowedForSession = new HashSet<>(naSessionDQL.createStatement(this.getSession()).<Long>list());

        final DQLSelectBuilder naBulletinDQL = new DQLSelectBuilder()
                .fromEntity(SessionStudentNotAllowedForBulletin.class, "rel")
                .column(property(SessionStudentNotAllowedForBulletin.student().id().fromAlias("rel")))
                .where(isNull(property(SessionStudentNotAllowedForBulletin.removalDate().fromAlias("rel"))))
                .where(eq(property(SessionStudentNotAllowedForBulletin.bulletin().fromAlias("rel")), value(model.getBulletin())));
        final Set<Long> studentsNotAllowedForBulletin = new HashSet<>(naBulletinDQL.createStatement(this.getSession()).<Long>list());

        // преподаватели
        final Map<Long, List<String>> tutorMap = new HashMap<>();
        final DQLSelectBuilder tutorDQL = new DQLSelectBuilder()
                .fromEntity(SessionComissionPps.class, "rel")
                .column(property(SessionComissionPps.pps().person().identityCard().fromAlias("rel")))
                .joinPath(DQLJoinType.inner, SessionComissionPps.commission().fromAlias("rel"), "comm")
                .joinEntity("comm", DQLJoinType.inner, SessionDocumentSlot.class, "slot", eq(property("comm.id"), property(SessionDocumentSlot.commission().id().fromAlias("slot"))))
                .column(property(SessionDocumentSlot.id().fromAlias("slot")))
                .where(eq(property(SessionDocumentSlot.document().fromAlias("slot")), value(model.getBulletin())))
                .order(property(SessionComissionPps.pps().person().identityCard().fullFio().fromAlias("rel")));
        for (final Object[] row : tutorDQL.createStatement(this.getSession()).<Object[]>list())
        {
            SafeMap.safeGet(tutorMap, (Long) row[1], ArrayList.class).add(((IdentityCard) row[0]).getFio());
        }

        // темы работ
        final Map<Long, SessionProjectTheme> slot2theme = new HashMap<>();
        final DQLSelectBuilder themeDQL = new DQLSelectBuilder()
                .fromEntity(SessionProjectTheme.class, "theme")
                .joinPath(DQLJoinType.inner, SessionProjectTheme.slot().fromAlias("theme"), "slot")
                .where(eq(property(SessionDocumentSlot.document().fromAlias("slot")), value(model.getBulletin())))
                .column("slot.id")
                .column(property("theme"));
        for (final Object[] row : themeDQL.createStatement(DAO.this.getSession()).<Object[]>list())
        {
            slot2theme.put((Long) row[0], (SessionProjectTheme) row[1]);
        }

        final List<Long> studentWpeCActionIds = CommonBaseUtil.getPropertiesList(slotList, SessionDocumentSlot.studentWpeCAction().id().s());

        // Академ. группы из строк УГС
        final List<Object[]> eppRealEduGroupList = new DQLSelectBuilder().fromEntity(EppRealEduGroupRow.class, "eg")
                .column(property(EppRealEduGroupRow.studentWpePart().id().fromAlias("eg")))
                .column(property(EppRealEduGroupRow.studentGroupTitle().fromAlias("eg")))
                .where(in(property(EppRealEduGroupRow.studentWpePart().id().fromAlias("eg")), studentWpeCActionIds))
                .createStatement(getSession()).list();
        final Map<Long, String> student2GroupTitleMap = new HashMap<>();
        for (Object[] o : eppRealEduGroupList)
        {
            final Long studentId = (Long) o[0];
            final String title = (String) o[1];

            student2GroupTitleMap.put(studentId, StringUtils.trimToEmpty(title));
        }
        // if all names of groups the same then hide column
        final Set<String> studentGroupTitleList = CollectionFactoryUtils.newSet(student2GroupTitleMap.values());
        if (studentGroupTitleList.size() <= 1)
            dataSource.getColumn("eppRealEduGroup").setVisible(false);

        DQLSelectBuilder finalMarkBuilder = new DQLSelectBuilder()
                .fromEntity(SessionStudentGradeBookDocument.class, "gb")
                .fromEntity(SessionMark.class, "m")
                .column(property("m"))
                .where(eq(property("m", SessionMark.slot().document()), property("gb")))
                .where(in(property("m", SessionMark.slot().studentWpeCAction()), studentWpeCActionIds))
                .order(property("m", SessionMark.performDate()));

        Map<EppStudentWpeCAction, SessionSlotRegularMark> finalMarkMap = new HashMap<>();
        for (SessionMark finalMark : this.<SessionMark>getList(finalMarkBuilder))
        {
            finalMarkMap.put(finalMark.getSlot().getStudentWpeCAction(), finalMark instanceof SessionSlotRegularMark ? (SessionSlotRegularMark) finalMark : ((SessionSlotLinkMark) finalMark).getTarget());
        }

        Map<Long, ISessionPracticeDao.IPracticeResult> practiceMarkMap = null;
        if (model.isShowPracticeTutorMarkColumn())
        {
            practiceMarkMap = ISessionPracticeDao.instance.get().getPracticeTutorMarks(studentWpeCActionIds);
        }

        for (final ViewWrapper<SessionDocumentSlot> wrapper : ViewWrapper.<SessionDocumentSlot>getPatchedList(dataSource))
        {
            final SessionDocumentSlot slot = wrapper.getEntity();
            final Student actualStudent = slot.getActualStudent();
            final boolean allowedForSession = !studentsNotAllowedForSession.contains(actualStudent.getId());
            final boolean allowedForBulletin = !studentsNotAllowedForBulletin.contains(actualStudent.getId());

            SessionMark mark = markMap.get(slot);
            wrapper.setViewProperty(IDAO.V_MARK, mark);
            wrapper.setViewProperty(IDAO.V_CHECKING_DISABLED, null);
            final Integer markCount = markCountMap.get(slot);
            wrapper.setViewProperty(IDAO.V_MARK_COUNT, markCount == null ? "нет" : String.valueOf(markCount - 1));
            wrapper.setViewProperty(IDAO.V_ALLOWED, allowedForSession && allowedForBulletin);
            wrapper.setViewProperty(IDAO.V_ALLOWED_DISABLED, !allowedForSession);
            wrapper.setViewProperty("ppsColumn", StringUtils.join(tutorMap.get(slot.getId()), "\n"));

            Optional<SessionProjectTheme> theme = Optional.ofNullable(slot2theme.get(slot.getId()));
            wrapper.setViewProperty(IDAO.PROP_THEME, theme.map(SessionProjectTheme::getTheme).orElse(null));
            wrapper.setViewProperty(IDAO.PROP_SUPERVISOR, theme.map(SessionProjectTheme::getSupervisor).map(PpsEntry::getTitleFioInfoOrgUnit).orElse(null));

            wrapper.setViewProperty("points", pointsMap.get(slot));
            wrapper.setViewProperty("scoredPoints", scoredPointsMap.get(slot));
            wrapper.setViewProperty("currentRating", currentRatingMap.get(slot));
            wrapper.setViewProperty("eppRealEduGroup", student2GroupTitleMap.get(slot.getStudentWpeCAction().getId()));
            if (model.isShowPracticeTutorMarkColumn() && practiceMarkMap != null)
            {
                final ISessionPracticeDao.IPracticeResult practiceResult = practiceMarkMap.get(slot.getStudentWpeCAction().getId());
                final SessionMarkGradeValueCatalogItem practiceTutorMark = practiceResult != null ? practiceResult.getMark() : null;
                wrapper.setViewProperty("practiceTutorMark", practiceTutorMark != null ? (practiceTutorMark.getTitle() + (practiceResult.isClosed() ? "" : "*")) : "");
            }

            SessionSlotRegularMark finalMark = finalMarkMap.get(slot.getStudentWpeCAction());
            Date date = mark == null ? model.getBulletin().getPerformDate() : mark.getPerformDate();
            wrapper.setViewProperty("finalMark", finalMark != null && (date == null || finalMark.getPerformDate().before(date)) ? finalMark.getDisplayableTitle() : "");
        }
    }

    private void fetchStudentData(final DQLSelectBuilder slotDQL, final String slotAlias)
    {
        slotDQL
                .fetchPath(DQLJoinType.inner, SessionDocumentSlot.studentWpeCAction().fromAlias(slotAlias), "wpca")
                .fetchPath(DQLJoinType.inner, EppStudentWpeCAction.studentWpe().fromAlias("wpca"), "epvSlot")
                .fetchPath(DQLJoinType.inner, EppStudentWorkPlanElement.student().fromAlias("epvSlot"), "student")
                .fetchPath(DQLJoinType.inner, Student.person().fromAlias("student"), "person")
                .fetchPath(DQLJoinType.inner, Person.identityCard().fromAlias("person"), "idc");
    }

    @Override
    public void updateAllowance(final Long id)
    {
        final SessionDocumentSlot slot = this.get(SessionDocumentSlot.class, id);

        final SessionBulletinDocument bulletin = (SessionBulletinDocument) slot.getDocument();
        final Student student = slot.getActualStudent();

        final SessionStudentNotAllowed sessionAllowance = this.getByNaturalId(new SessionStudentNotAllowed.NaturalId(bulletin.getSessionObject(), student));
        SessionStudentNotAllowedForBulletin bulletinAllowance = this.getByNaturalId(new SessionStudentNotAllowedForBulletin.NaturalId(bulletin, student));

        if (sessionAllowance != null)
        {
            throw new ApplicationException("Невозможно отредактировать допуск, так как студент не допущен к сдаче сессии.");
        }

        if (bulletinAllowance != null)
        {
            bulletinAllowance.setRemovalDate(bulletinAllowance.getRemovalDate() == null ? new Date() : null);
            this.update(bulletinAllowance);
        }
        else
        {
            bulletinAllowance = new SessionStudentNotAllowedForBulletin();
            bulletinAllowance.setStudent(student);
            bulletinAllowance.setBulletin(bulletin);
            bulletinAllowance.setCreateDate(new Date());
            this.save(bulletinAllowance);
        }
    }

    @Override
    public List<Long> getAllowedStudentSlotIds(Model model)
    {
        SessionBulletinDocument bulletin = model.getBulletin();

        final Collection<IEntity> selectedObjects = ((CheckboxColumn) model.getDataSource().getColumn("select")).getSelectedObjects();

        if (selectedObjects == null || selectedObjects.isEmpty())
            return null;

        // не допущенные, выбывшие и ненужные студенты
        List<Long> uselessSlots = this.<SessionMark>getList(
                new DQLSelectBuilder()
                        .fromEntity(SessionMark.class, "mark")
                        .column(property("mark"))
                        .where(in(property("mark", SessionMark.slot()), selectedObjects))
        )
                .stream()
                .filter(mark -> SessionMarkStateCatalogItemCodes.NO_ADMISSION.equals(mark.getValueItem().getCode())
                        || SessionMarkStateCatalogItemCodes.UNNECESSARY.equals(mark.getValueItem().getCode())
                        || SessionMarkStateCatalogItemCodes.EXPELLED.equals(mark.getValueItem().getCode()))
                .map(mark -> mark.getSlot().getId())
                .collect(Collectors.toList());

        List<Long> slotIds = selectedObjects.stream()
                .filter(slot -> !uselessSlots.contains(slot.getId()))
                .map(IIdentifiable::getId)
                .collect(Collectors.toList());

        if (slotIds.isEmpty())
            return null;

        String mainAlias = "sds";
        DQLSelectBuilder validSlotIdsBuilder = new DQLSelectBuilder()
                .fromEntity(SessionDocumentSlot.class, mainAlias)
                .column(property(mainAlias, SessionDocumentSlot.id()))
                .where(in(property(mainAlias, SessionDocumentSlot.id()), slotIds));

        // студенты с недопуском на эту ведомость
        String bAlias = "na4b";
        validSlotIdsBuilder.where(notExistsByExpr(SessionStudentNotAllowedForBulletin.class, bAlias, and(
                eq(property(bAlias, SessionStudentNotAllowedForBulletin.bulletin()), value(bulletin)),
                eq(property(bAlias, SessionStudentNotAllowedForBulletin.student()),
                   property(mainAlias, SessionDocumentSlot.studentWpeCAction().studentWpe().student())),
                isNull(property(bAlias, SessionStudentNotAllowedForBulletin.removalDate()))
        )));

        // студенты с недопуском на сессию
        String sAlias = "na4s";
        validSlotIdsBuilder.where(notExistsByExpr(SessionStudentNotAllowed.class, sAlias, and(
                eq(property(sAlias, SessionStudentNotAllowed.session()), value(bulletin.getSessionObject())),
                eq(property(sAlias, SessionStudentNotAllowed.student()),
                   property(mainAlias, SessionDocumentSlot.studentWpeCAction().studentWpe().student())),
                isNull(property(sAlias, SessionStudentNotAllowed.removalDate()))
        )));

        // студенты имеющие протокол по этой ведомости
        validSlotIdsBuilder.where(notExists(SessionStateFinalExamProtocol.class,
                                            SessionStateFinalExamProtocol.documentSlot().s(), property(mainAlias, SessionDocumentSlot.id())
        ));

        return getList(validSlotIdsBuilder);
    }
}
