package ru.tandemservice.unisession.entity.mark.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unisession.entity.mark.SessionALRequest;
import ru.tandemservice.unisession.entity.mark.SessionALRequestRow;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Строка заявления о переводе на ускоренное обучение
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionALRequestRowGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.mark.SessionALRequestRow";
    public static final String ENTITY_NAME = "sessionALRequestRow";
    public static final int VERSION_HASH = -2089689581;
    private static IEntityMeta ENTITY_META;

    public static final String L_REQUEST = "request";
    public static final String L_ROW_TERM = "rowTerm";
    public static final String L_REG_ELEMENT_PART = "regElementPart";
    public static final String P_HOURS_AMOUNT = "hoursAmount";
    public static final String P_NEED_RETAKE = "needRetake";

    private SessionALRequest _request;     // Заявление о переводе на ускоренное обучение
    private EppEpvRowTerm _rowTerm;     // Часть строки УП
    private EppRegistryElementPart _regElementPart;     // Часть элемента реестра
    private Long _hoursAmount;     // Количество часов по приложению
    private boolean _needRetake;     // Необходимость пересдачи (true - переаттестация; false - перезачтение)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Заявление о переводе на ускоренное обучение. Свойство не может быть null.
     */
    @NotNull
    public SessionALRequest getRequest()
    {
        return _request;
    }

    /**
     * @param request Заявление о переводе на ускоренное обучение. Свойство не может быть null.
     */
    public void setRequest(SessionALRequest request)
    {
        dirty(_request, request);
        _request = request;
    }

    /**
     * @return Часть строки УП. Свойство не может быть null.
     */
    @NotNull
    public EppEpvRowTerm getRowTerm()
    {
        return _rowTerm;
    }

    /**
     * @param rowTerm Часть строки УП. Свойство не может быть null.
     */
    public void setRowTerm(EppEpvRowTerm rowTerm)
    {
        dirty(_rowTerm, rowTerm);
        _rowTerm = rowTerm;
    }

    /**
     * @return Часть элемента реестра. Свойство не может быть null.
     */
    @NotNull
    public EppRegistryElementPart getRegElementPart()
    {
        return _regElementPart;
    }

    /**
     * @param regElementPart Часть элемента реестра. Свойство не может быть null.
     */
    public void setRegElementPart(EppRegistryElementPart regElementPart)
    {
        dirty(_regElementPart, regElementPart);
        _regElementPart = regElementPart;
    }

    /**
     * @return Количество часов по приложению.
     */
    public Long getHoursAmount()
    {
        return _hoursAmount;
    }

    /**
     * @param hoursAmount Количество часов по приложению.
     */
    public void setHoursAmount(Long hoursAmount)
    {
        dirty(_hoursAmount, hoursAmount);
        _hoursAmount = hoursAmount;
    }

    /**
     * true  - переаттестация: необходимо пересдавать контрольные мероприятия.
     * false - перезачтение: изучать и сдавать ничего не нужно.
     *
     * @return Необходимость пересдачи (true - переаттестация; false - перезачтение). Свойство не может быть null.
     */
    @NotNull
    public boolean isNeedRetake()
    {
        return _needRetake;
    }

    /**
     * @param needRetake Необходимость пересдачи (true - переаттестация; false - перезачтение). Свойство не может быть null.
     */
    public void setNeedRetake(boolean needRetake)
    {
        dirty(_needRetake, needRetake);
        _needRetake = needRetake;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SessionALRequestRowGen)
        {
            setRequest(((SessionALRequestRow)another).getRequest());
            setRowTerm(((SessionALRequestRow)another).getRowTerm());
            setRegElementPart(((SessionALRequestRow)another).getRegElementPart());
            setHoursAmount(((SessionALRequestRow)another).getHoursAmount());
            setNeedRetake(((SessionALRequestRow)another).isNeedRetake());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionALRequestRowGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionALRequestRow.class;
        }

        public T newInstance()
        {
            return (T) new SessionALRequestRow();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "request":
                    return obj.getRequest();
                case "rowTerm":
                    return obj.getRowTerm();
                case "regElementPart":
                    return obj.getRegElementPart();
                case "hoursAmount":
                    return obj.getHoursAmount();
                case "needRetake":
                    return obj.isNeedRetake();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "request":
                    obj.setRequest((SessionALRequest) value);
                    return;
                case "rowTerm":
                    obj.setRowTerm((EppEpvRowTerm) value);
                    return;
                case "regElementPart":
                    obj.setRegElementPart((EppRegistryElementPart) value);
                    return;
                case "hoursAmount":
                    obj.setHoursAmount((Long) value);
                    return;
                case "needRetake":
                    obj.setNeedRetake((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "request":
                        return true;
                case "rowTerm":
                        return true;
                case "regElementPart":
                        return true;
                case "hoursAmount":
                        return true;
                case "needRetake":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "request":
                    return true;
                case "rowTerm":
                    return true;
                case "regElementPart":
                    return true;
                case "hoursAmount":
                    return true;
                case "needRetake":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "request":
                    return SessionALRequest.class;
                case "rowTerm":
                    return EppEpvRowTerm.class;
                case "regElementPart":
                    return EppRegistryElementPart.class;
                case "hoursAmount":
                    return Long.class;
                case "needRetake":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionALRequestRow> _dslPath = new Path<SessionALRequestRow>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionALRequestRow");
    }
            

    /**
     * @return Заявление о переводе на ускоренное обучение. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.mark.SessionALRequestRow#getRequest()
     */
    public static SessionALRequest.Path<SessionALRequest> request()
    {
        return _dslPath.request();
    }

    /**
     * @return Часть строки УП. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.mark.SessionALRequestRow#getRowTerm()
     */
    public static EppEpvRowTerm.Path<EppEpvRowTerm> rowTerm()
    {
        return _dslPath.rowTerm();
    }

    /**
     * @return Часть элемента реестра. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.mark.SessionALRequestRow#getRegElementPart()
     */
    public static EppRegistryElementPart.Path<EppRegistryElementPart> regElementPart()
    {
        return _dslPath.regElementPart();
    }

    /**
     * @return Количество часов по приложению.
     * @see ru.tandemservice.unisession.entity.mark.SessionALRequestRow#getHoursAmount()
     */
    public static PropertyPath<Long> hoursAmount()
    {
        return _dslPath.hoursAmount();
    }

    /**
     * true  - переаттестация: необходимо пересдавать контрольные мероприятия.
     * false - перезачтение: изучать и сдавать ничего не нужно.
     *
     * @return Необходимость пересдачи (true - переаттестация; false - перезачтение). Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.mark.SessionALRequestRow#isNeedRetake()
     */
    public static PropertyPath<Boolean> needRetake()
    {
        return _dslPath.needRetake();
    }

    public static class Path<E extends SessionALRequestRow> extends EntityPath<E>
    {
        private SessionALRequest.Path<SessionALRequest> _request;
        private EppEpvRowTerm.Path<EppEpvRowTerm> _rowTerm;
        private EppRegistryElementPart.Path<EppRegistryElementPart> _regElementPart;
        private PropertyPath<Long> _hoursAmount;
        private PropertyPath<Boolean> _needRetake;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Заявление о переводе на ускоренное обучение. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.mark.SessionALRequestRow#getRequest()
     */
        public SessionALRequest.Path<SessionALRequest> request()
        {
            if(_request == null )
                _request = new SessionALRequest.Path<SessionALRequest>(L_REQUEST, this);
            return _request;
        }

    /**
     * @return Часть строки УП. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.mark.SessionALRequestRow#getRowTerm()
     */
        public EppEpvRowTerm.Path<EppEpvRowTerm> rowTerm()
        {
            if(_rowTerm == null )
                _rowTerm = new EppEpvRowTerm.Path<EppEpvRowTerm>(L_ROW_TERM, this);
            return _rowTerm;
        }

    /**
     * @return Часть элемента реестра. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.mark.SessionALRequestRow#getRegElementPart()
     */
        public EppRegistryElementPart.Path<EppRegistryElementPart> regElementPart()
        {
            if(_regElementPart == null )
                _regElementPart = new EppRegistryElementPart.Path<EppRegistryElementPart>(L_REG_ELEMENT_PART, this);
            return _regElementPart;
        }

    /**
     * @return Количество часов по приложению.
     * @see ru.tandemservice.unisession.entity.mark.SessionALRequestRow#getHoursAmount()
     */
        public PropertyPath<Long> hoursAmount()
        {
            if(_hoursAmount == null )
                _hoursAmount = new PropertyPath<Long>(SessionALRequestRowGen.P_HOURS_AMOUNT, this);
            return _hoursAmount;
        }

    /**
     * true  - переаттестация: необходимо пересдавать контрольные мероприятия.
     * false - перезачтение: изучать и сдавать ничего не нужно.
     *
     * @return Необходимость пересдачи (true - переаттестация; false - перезачтение). Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.mark.SessionALRequestRow#isNeedRetake()
     */
        public PropertyPath<Boolean> needRetake()
        {
            if(_needRetake == null )
                _needRetake = new PropertyPath<Boolean>(SessionALRequestRowGen.P_NEED_RETAKE, this);
            return _needRetake;
        }

        public Class getEntityClass()
        {
            return SessionALRequestRow.class;
        }

        public String getEntityName()
        {
            return "sessionALRequestRow";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
