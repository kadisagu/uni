/* $*/

package ru.tandemservice.unisession.component.allowance.SessionAllowanceAdd;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unisession.entity.document.SessionObject;

/**
 * @author oleyba
 * @since 3/24/11
 */
@Input({@Bind(key= PublisherActivator.PUBLISHER_ID_KEY, binding = "session.id")})
public class Model
{
    private SessionObject session = new SessionObject();
    private Student student;
    public static final String COMPONENT_NAME = Model.class.getPackage().getName();
    private ISelectModel sessionObjectModel;
    private ISelectModel studentModel;

    public SessionObject getSession()
    {
        return this.session;
    }

    public void setSession(final SessionObject session)
    {
        this.session = session;
    }

    public Student getStudent()
    {
        return this.student;
    }

    public void setStudent(final Student student)
    {
        this.student = student;
    }

    public ISelectModel getStudentModel()
    {
        return this.studentModel;
    }

    public void setStudentModel(final ISelectModel studentModel)
    {
        this.studentModel = studentModel;
    }

    public ISelectModel getSessionObjectModel()
    {
        return this.sessionObjectModel;
    }

    public void setSessionObjectModel(final ISelectModel sessionObjectModel)
    {
        this.sessionObjectModel = sessionObjectModel;
    }
}
