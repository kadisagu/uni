/* $Id: $ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.FqwSummaryResultAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.activator.IRootComponentActivationBuilder;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.FqwSummaryResultPub.SessionReportFqwSummaryResultPub;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.SfaSummaryResultAdd.SessionReportSfaSummaryResultAddUI;
import ru.tandemservice.unisession.entity.report.FqwSummaryResult;

/**
 * @author Andrey Andreev
 * @since 27.10.2016
 */
@State({@Bind(key = SessionReportManager.BIND_ORG_UNIT, binding = "ouHolder.id")})
public class SessionReportFqwSummaryResultAddUI extends SessionReportSfaSummaryResultAddUI<FqwSummaryResult>
{


    @Override
    public FqwSummaryResult initReport()
    {
        return new FqwSummaryResult();
    }

    @Override
    public void createStoredReport(FqwSummaryResult report)
    {
        SessionReportManager.instance().fqwSummaryResultPrintDAO().createStoredReport(report);
    }

    @Override
    public void openReportPub(FqwSummaryResult report)
    {
        IRootComponentActivationBuilder builder = getActivationBuilder()
                .asDesktopRoot(SessionReportFqwSummaryResultPub.class)
                .parameter(UIPresenter.PUBLISHER_ID, report.getId());
        if (getOuHolder().getId() != null)
            builder.parameter(SessionReportManager.BIND_ORG_UNIT, getOuHolder().getId());
        builder.activate();
    }

    @Override
    public String getAddPermissionKey()
    {
        return getSec().getPermission(getOuHolder().getId() != null ? "orgUnit_addFinalQualWorkSummaryResult" : "addSessionStorableReport");
    }
}
