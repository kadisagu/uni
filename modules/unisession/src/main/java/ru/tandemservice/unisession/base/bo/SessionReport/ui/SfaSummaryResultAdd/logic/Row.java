/* $Id: $ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.SfaSummaryResultAdd.logic;

import jxl.write.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.unisession.base.bo.SessionReport.logic.AbsStateFinalAttestationRow;

/**
 * @author Andrey Andreev
 * @since 02.12.2016
 */
public class Row extends AbsStateFinalAttestationRow
{
    final protected WritableCellFormat _titleStyle;
    final protected WritableCellFormat _intStyle;
    final protected WritableCellFormat _redIntStyle;
    final protected WritableCellFormat _doubleStyle;

    protected EducationOrgUnit _educationOrgUnit;

    public Row(WritableCellFormat titleStyle, WritableCellFormat intStyle, WritableCellFormat redIntStyle, WritableCellFormat doubleStyle)
    {
        this(null, null, titleStyle, intStyle, redIntStyle, doubleStyle);
    }

    public Row(EducationOrgUnit educationOrgUnit, WritableCellFormat titleStyle, WritableCellFormat intStyle, WritableCellFormat redIntStyle, WritableCellFormat doubleStyle)
    {
        this(educationOrgUnit, null, titleStyle, intStyle, redIntStyle, doubleStyle);
    }

    public Row(EducationOrgUnit educationOrgUnit, String title, WritableCellFormat titleStyle, WritableCellFormat intStyle, WritableCellFormat redIntStyle, WritableCellFormat doubleStyle)
    {
        _educationOrgUnit = educationOrgUnit;

        _title = title;

        _titleStyle = titleStyle;
        _intStyle = intStyle;
        _redIntStyle = redIntStyle;
        _doubleStyle = doubleStyle;
    }


    public EducationLevels getEducationLevels()
    {
        return _educationOrgUnit == null ? null : _educationOrgUnit.getEducationLevelHighSchool().getEducationLevel();
    }

    public OrgUnit getTerritorialOrgUnit()
    {
        return _educationOrgUnit == null ? null : _educationOrgUnit.getTerritorialOrgUnit();
    }

    public WritableCellFormat getTitleStyle()
    {
        return _titleStyle;
    }

    public WritableCellFormat getIntStyle()
    {
        return _intStyle;
    }

    public WritableCellFormat getRedIntStyle()
    {
        return _redIntStyle;
    }

    public WritableCellFormat getDoubleStyle()
    {
        return _doubleStyle;
    }
}
