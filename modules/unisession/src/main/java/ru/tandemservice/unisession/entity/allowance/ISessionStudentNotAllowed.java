/* $*/

package ru.tandemservice.unisession.entity.allowance;

import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Date;

/**
 * @author oleyba
 * @since 3/24/11
 */
public interface ISessionStudentNotAllowed extends IEntity
{
    Student getStudent();

    Date getCreateDate();

    Date getRemovalDate();

    void setRemovalDate(Date date);
}
