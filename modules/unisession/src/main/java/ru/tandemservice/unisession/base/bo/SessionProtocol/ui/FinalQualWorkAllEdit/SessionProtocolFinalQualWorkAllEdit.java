/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionProtocol.ui.FinalQualWorkAllEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.IColumnListExtPointBuilder;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unisession.base.bo.SessionProtocol.SessionProtocolManager;
import ru.tandemservice.unisession.base.bo.SessionProtocol.ui.AbstractAllEdit.SessionProtocolAbstractAllEdit;
import ru.tandemservice.unisession.entity.catalog.SessionRecommendation4FQW;
import ru.tandemservice.unisession.entity.catalog.SessionSource4FQWTheme;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Andreev
 * @since 12.09.2016
 */
@Configuration
public class SessionProtocolFinalQualWorkAllEdit extends SessionProtocolAbstractAllEdit
{
    public static final String COMMISSION_MEMBERS_DS = "commissionMembersDS";
    public static final String CONSULTANT_DS = "consultantDS";
    public static final String RECOMMENDATION_DS = "recommendationDS";
    public static final String SOURCE_4_THEME_DS = "source4ThemeDS";

    public static final String SEARCH_LIST_COLUMN_CONSULTANT = "consultant";
    public static final String SEARCH_LIST_COLUMN_RECOMMENDATION = "recommendation";
    public static final String SEARCH_LIST_COLUMN_SOURCE_4_THEME = "source4Theme";
    public static final String SEARCH_LIST_COLUMN_WITH_HONORS = "withHonors";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                    .addDataSource(searchListDS(STATE_FINAL_EXAM_PROTOCOL_DS, stateFinalExamProtocolDSColumnExtPoint(),
                                                SessionProtocolManager.instance().stateFinalExamProtocolDSHandler()))
                .addDataSource(selectDS(COMMISSION_MEMBERS_DS, commissionMembersDSHandler()).addColumn(PpsEntry.fio().s()))
                .addDataSource(selectDS(CONSULTANT_DS, consultantDSHandler())
                                       .addColumn(PpsEntry.fio().s())
                                       .addColumn(PpsEntry.orgUnit().shortTitle().s())
                                       .addColumn(PpsEntry.titlePostOrTimeWorkerData().s()))
                .addDataSource(selectDS(RECOMMENDATION_DS, recommendationDSHandler()))
                .addDataSource(selectDS(SOURCE_4_THEME_DS, source4ThemeDSHandler()))
                .create();
    }

    @Override
    public void customProtocolListColumn(IColumnListExtPointBuilder builder)
    {
        builder.addColumn(blockColumn(SEARCH_LIST_COLUMN_CONSULTANT))
                .addColumn(blockColumn(SEARCH_LIST_COLUMN_RECOMMENDATION))
                .addColumn(blockColumn(SEARCH_LIST_COLUMN_SOURCE_4_THEME))
                .addColumn(toggleColumn(SEARCH_LIST_COLUMN_WITH_HONORS, "withHonors")
                                   .toggleOffListener("onClickOffHonors").toggleOnListener("onClickOnHonors"));
    }

    @Bean
    public ColumnListExtPoint stateFinalExamProtocolDSColumnExtPoint()
    {
        IColumnListExtPointBuilder builder = columnListExtPointBuilder(STATE_FINAL_EXAM_PROTOCOL_DS);
        addProtocolListColumns(builder);
        return builder.create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> commissionMembersDSHandler()
    {
        EntityComboDataSourceHandler handler = PpsEntry.defaultSelectDSHandler(getName());

        handler.customize((alias, dql, context, filter) -> {

            SessionBulletinDocument bulletin = context.get(BULLETIN_PARAM);
            dql.where(exists(SessionComissionPps.class,
                             SessionComissionPps.pps().s(), property(alias),
                             SessionComissionPps.commission().s(), bulletin.getCommission()));
            return dql;
        });

        return handler;
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> consultantDSHandler()
    {
        return PpsEntry.defaultSelectDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> recommendationDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), SessionRecommendation4FQW.class);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> source4ThemeDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), SessionSource4FQWTheme.class);
    }
}