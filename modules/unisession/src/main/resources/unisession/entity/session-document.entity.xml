<entity-config name="unisession-document"
               package-name="ru.tandemservice.unisession.entity.document"
               xmlns="http://www.tandemframework.org/meta/entity"
               xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
               xsi:schemaLocation="http://www.tandemframework.org/meta/entity http://www.tandemframework.org/schema/meta/meta-entity.xsd">

    <entity name="sessionObject" title="Сессия" table-name="session_object_t">
        <comment>
            Сессия - определяет, что на указанном подразделении (деканат) в рамках указанного года (и его части) будет проводится промежуточная аттестация студентов.
        </comment>

        <!-- KEY -->
        <many-to-one entity-ref="orgUnit" required="true" title="Подразделение" unique="idx_session_object_uniq"/>
        <many-to-one entity-ref="educationYear" required="true" title="Учебный год" unique="idx_session_object_uniq"/>
        <many-to-one entity-ref="yearDistributionPart" required="true" title="Часть года" unique="idx_session_object_uniq"/>
        <!-- /KEY -->

        <property name="number" type="trimmedstring" required="true" title="Номер документа"/>
        <property name="formingDate" type="date" required="true" title="Дата формирования (фактическая)"/>
        <property name="closeDate" type="date" required="false" title="Дата закрытия (фактическая)"/>

        <property name="startupDate" type="date" required="true" title="Дата начала сессии (фактическая)"/>
        <property name="deadlineDate" type="date" required="true" title="Дата окончания сессии (фактическая)"/>

        <property name="comment" type="text" title="Комментарий" />
    </entity>


	<entity abstract="true" name="sessionDocument" title="Документ сессии" table-name="session_doc_t">
		<comment>
			Документ сессии - абстрактный объект, в котором фиксируются оценки студентов
			После фиксации документа (closeDate != null) любые изменения в документе запрещены,
			его печатная версия (должна быть) сохраняется в нем и существует вечно 	
		</comment>

		<property name="number" type="trimmedstring" required="true" title="Номер документа"/>
		<property name="formingDate" type="date" required="true" title="Дата формирования (фактическая)"/>
		<property name="closeDate" type="date" required="false" title="Дата закрытия (фактическая)"/>

        <child-entity name="sessionGlobalDocument" title="Документ для массового ввода оценок" table-name="session_doc_global_t">
            <comment>
                Автоматически создается при выставлении оценок в рамках семестрового журнала, обеспечивает массовый ввод оценок.
            </comment>

            <many-to-one entity-ref="educationYear" required="true" title="Учебный год" unique="idx_session_doc_global_uniq"/>
            <many-to-one entity-ref="yearDistributionPart" required="true" title="Часть года" unique="idx_session_doc_global_uniq"/>
        </child-entity>

		<child-entity name="sessionBulletinDocument" title="Ведомость" table-name="session_doc_bulletin_t">
			<comment>
				Ведомость (формируется на основе учебной группы по видам аудиторной нагрузки)
				показывает, что учебная группа будет обслуживаться в рамках указанной сессии (семестрового журнала)
			</comment>
			
			<many-to-one name="group" entity-ref="eppRealEduGroup4ActionType" required="true" title="Группа" unique="idx_session_doc_bulletin_uniq"/>
			<many-to-one name="sessionObject" entity-ref="sessionObject" required="true" title="Сессия" />
		
			<property name="performDate" type="date" required="false" title="Дата проведения (фактическая)"/>
			
			<many-to-one name="commission" required="true" entity-ref="sessionComission" title="Комиссия"/>
		</child-entity>

        <child-entity name="sessionRetakeDocument" title="Ведомость пересдач" table-name="session_doc_retake_t">
            <many-to-one name="sessionObject" entity-ref="sessionObject" required="true" title="Сессия" />
            <many-to-one name="registryElementPart" entity-ref="eppRegistryElementPart" required="true" title="Мероприятие (элемент реестра), часть"/>
            <many-to-one name="commission" required="true" entity-ref="sessionComission" title="Комиссия"/>
            <property name="performDate" type="date" title="Дата сдачи" />
        </child-entity>


		<child-entity abstract="true" name="sessionSimpleDocument" title="Списочный документ сессии" table-name="session_doc_simple_t">
			<comment>	
				Списочный (сформированный вне основного процесса) документ
			</comment>
		
			<many-to-one entity-ref="orgUnit" required="true" title="Подразделение"/>
			<many-to-one name="reason" entity-ref="sessionsSimpleDocumentReason" required="true" title="Причина"/>
			
			<property name="issueDate" type="date" required="false" title="Дата выдачи"/>
			<property name="deadlineDate" type="date" required="false" title="Срок действия"/>

			<child-entity name="sessionSheetDocument" title="Экзаменационный лист" table-name="session_doc_sheet_t">
			</child-entity>

			<child-entity name="sessionListDocument" title="Экзаменационная карточка" table-name="session_doc_list_t">
                <many-to-one name="student" entity-ref="student" required="true" title="Студент"/>
			</child-entity>
    	</child-entity>


        <child-entity abstract="true" name="sessionTransferDocument" title="Перезачтение" table-name="session_doc_transfer_t">
			<comment>
				Документ, показывающий перезачтение группы оценок (из старых рп или иного образовательного учреждения)
				в требуемые оценки студента 
			</comment>

            <many-to-one entity-ref="educationYear" required="true" title="Учебный год перезачтения"/>
			<many-to-one entity-ref="orgUnit" required="true" title="Подразделение"/>
            <many-to-one name="targetStudent" entity-ref="student" required="true" title="Студент">
                <comment>
			        Студент, которому перезачитываются оценки
		        </comment>
            </many-to-one>
			
			<property name="eduInstitutionTitle" type="string" title="Учреждение"/>
			<property name="comment" type="text" title="Комментарий" />

            <child-entity name="sessionTransferInsideDocument" title="Внутреннее перезачтение" table-name="session_doc_inside_transfer_t">
            </child-entity>

            <child-entity name="sessionTransferOutsideDocument" title="Внешнее перезачтение" table-name="session_doc_outside_transfer_t">
            </child-entity>

		</child-entity>


		<child-entity name="sessionStudentGradeBookDocument" title="Зачетная книжка студента" table-name="session_doc_stgbook_t">
			<comment>
				Зачетная книжка студента (существует в рамках его связи с УП(в) и актуальна тогда, когда актуальна связь)
				хранит итоговые оценки студента как ссылки на оценки, полученные им по другим документам
			</comment>
			<many-to-one name="student" entity-ref="student" required="true" title="Студент" unique="ids_session_doc_stgbook" backward-cascade="delete"/>
		</child-entity>

        <child-entity name="sessionTransferProtocolDocument" title="Протокол перезачтения" table-name="session_doc_tp_t">
            <property name="protocolDate" type="timestamp" required="true" title="Дата протокола"/>
            <property name="approved" type="boolean" required="true" title="Утвержден" default-value="false"/>
            <many-to-one name="request" entity-ref="sessionALRequest" required="true" title="Заявление о переводе на ускоренное обучение"/>
            <many-to-one name="workPlan" entity-ref="eppWorkPlanBase" required="true" title="РУП"/>
            <many-to-one name="workPlanVersion" entity-ref="eppWorkPlanVersion" backward-cascade="delete-orphan" title="Версия РУП"/>
            <many-to-one name="chairman" entity-ref="ppsEntryByEmployeePost" title="Председатель"/>
            <many-to-one name="commission" entity-ref="sessionComission" title="Комиссия"/>
            <property name="comment" type="text" title="Комментарий"/>
        </child-entity>

	</entity>

    <entity name="sessionTransferProtocolRow" title="Строка протокола перезачтения" table-name="session_doc_tp_row_t">
        <many-to-one name="protocol" entity-ref="sessionTransferProtocolDocument" required="true" backward-cascade="delete" title="Протокол"/>
        <many-to-one name="requestRow" entity-ref="sessionALRequestRow" required="true" title="Строка заявления"/>

        <property name="needRetake" type="boolean" required="true" title="Необходимость пересдачи (true - переаттестация; false - перезачтение)">
            <comment>
                true  - переаттестация: необходимо пересдавать контрольные мероприятия.
                false - перезачтение: изучать и сдавать ничего не нужно.
            </comment>
        </property>
    </entity>

    <entity name="sessionTransferProtocolMark" title="Перезачитываемая оценка протокола" table-name="session_doc_tp_row_mark_t">
        <natural-id>
            <many-to-one name="protocolRow" entity-ref="sessionTransferProtocolRow" required="true" backward-cascade="delete" title="Строка протокола перезачтения"/>
            <many-to-one name="controlAction" entity-ref="eppFControlActionType" required="true" title="Форма итогового контроля"/>
        </natural-id>
        <many-to-one name="mark" entity-ref="sessionMarkGradeValueCatalogItem" title="Оценка (из шкалы оценок)"/>
        <many-to-one name="slot" entity-ref="sessionDocumentSlot" title="Запись студента по мероприятию в документе сессии"/>
    </entity>

	<entity name="sessionDocumentPrintVersion" title="Печатная форма закрытого документа сессии" table-name="session_doc_printForm_t">
        <many-to-one name="doc" entity-ref="sessionDocument" required="true" unique="true" backward-cascade="delete"/>
        <property name="content" type="blob" required="true" title="Сохраненная печатная форма"/>
    </entity>

	<entity name="sessionDocumentSlot" title="Запись студента по мероприятию в документе сессии" table-name="session_doc_slot_t">
		<comment>
			Показывает, что указанный слот студента по форме контроля доступен для отметки в рамках указанного документа
		</comment>
		
		<natural-id mutable="true">
			<!-- мутабельный, потому что есть разбиение и объединение ведомостей пересдач: сохранение слота при смене документа -->
        	<many-to-one name="document" entity-ref="sessionDocument" required="true" backward-cascade="delete" title="Документ"/>
        	<many-to-one name="studentWpeCAction" entity-ref="eppStudentWpeCAction" required="true" title="Форма контроля для сдачи по дисциплине студента"/>
        	<property name="inSession" type="boolean" required="true" title="В сессию" default-value="true"/>
        </natural-id>

        <unique-constraint name="uk_naturalid" message="Одно и то же мероприятие студента (МСРП) не может быть включено в документ сессии более одного раза. Уберите дублирующиеся записи в документе и сохраните снова."/>
        
		<many-to-one name="commission" required="true" entity-ref="sessionComission" title="Комиссия (назначенная)"/>
        <property name="tryNumber" type="integer" required="false" title="В который раз сдает"/>
	</entity>

	<entity abstract="true" name="sessionTransferOperation" title="Перезачтенное мероприятие" table-name="session_transfer_op_t">
		<comment>
			Показывает, что указанная оценка получена студентом в результате перезачтения 
			(фактически определяет откуда именно перезачтена оценка)
			как только связь студента с УП(в) становится неактуальной - closeDate выставляется в removalDate
		</comment>
	
		<natural-id>
			<many-to-one name="targetMark" entity-ref="sessionSlotRegularMark" required="true" forward-cascade="delete" title="Перезачтенная оценка"/>
            <!-- todo constraint на класс документа -->
		</natural-id>
		<property name="comment" type="text" title="Комментарий" />

		<child-entity name="sessionTransferInsideOperation" title="Перезачтенное мероприятие внутри ОУ" table-name="session_transfer_inop_t">
			<many-to-one name="sourceMark" entity-ref="sessionSlotRegularMark" required="true" title="Перезачитываемая оценка"/>
		</child-entity>

		<child-entity name="sessionTransferOutsideOperation" title="Перезачтенное мероприятие из другого ОУ" table-name="session_transfer_outop_t">
			<property name="discipline" type="string" required="true" title="Перезачитываемая дисциплина (по документу)"/>
			<many-to-one name="controlAction" entity-ref="eppFControlActionType" required="true" title="Форма контроля (по документу)"/>
			<property name="mark" type="string" required="true" title="Оценка (по документу)"/>
		</child-entity>
	</entity>

    <entity name="sessionProjectTheme" title="Тема работы студента в сессии" table-name="session_project_theme_t">
        <natural-id>
            <many-to-one name="slot" entity-ref="sessionDocumentSlot" required="true" title="Строка в документе сессии" backward-cascade="delete"/>
        </natural-id>
        <property name="theme" type="text" title="Тема" length="1024"/>
        <property name="comment" type="text" title="Комментарий"/>
		<many-to-one name="supervisor" entity-ref="ppsEntry" title="Руководитель"/>
    </entity>

    <entity name="sessionSlotRatingData" title="Данные рейтинга для записи студента в документе сессии" table-name="session_slot_rating_data_t">
        <natural-id>
            <many-to-one name="slot" entity-ref="sessionDocumentSlot" required="true" title="Запись студента в документе сессии" backward-cascade="delete"/>
        </natural-id>

        <property name="fixedCurrentRatingAsLong" required="false" type="long" title="Текущий рейтинг">
            <comment>
                Содержит рассчитанное и зафиксированное значение текущего рейтинга,
                для использования при выставлении оценки по данной строке.
            </comment>
        </property>

        <property name="allowed" type="boolean" required="true" title="Допуск" default-value="true">
            <comment>
                Допущен ли студент до сдачи мероприятия по правилам БРС.
            </comment>
        </property>
    </entity>

</entity-config>