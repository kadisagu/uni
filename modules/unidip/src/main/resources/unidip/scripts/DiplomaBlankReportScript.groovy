package unidip.scripts

import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.core.util.cache.SafeMap
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLFunctions
import org.tandemframework.hibsupport.dql.DQLJoinType
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.document.text.table.RtfTable
import org.tandemframework.rtf.document.text.table.cell.RtfCell
import org.tandemframework.rtf.document.text.table.row.RtfRow
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil
import org.tandemframework.shared.organization.base.entity.OrgUnit
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import ru.tandemservice.uni.dao.IUniBaseDao
import ru.tandemservice.uni.util.rtf.UniRtfUtil
import ru.tandemservice.unidip.base.entity.catalog.DipScriptItem
import ru.tandemservice.unidip.base.entity.catalog.codes.DipScriptItemCodes
import ru.tandemservice.unidip.bso.bo.DiplomaBlankReport.DiplomaBlankReportManager
import ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank
import ru.tandemservice.unidip.bso.entity.catalog.DipBlankType
import ru.tandemservice.unidip.bso.entity.catalog.codes.DipBlankStateCodes

import static org.tandemframework.hibsupport.dql.DQLExpressions.*

return new DiplomaBlankReportPrint(                // стандартные входные параметры скрипта
        session: session,                          // сессия
        template: template,                        // шаблон
        formDate: formDate,                        // дата формирования шаблона
        formBy: formBy,                            // формировать отчет
        blankTypes: blankTypes,                    // типы бланков
        storageLocations: storageLocations         // место хранения
).print()

/**
 * @author azhebko
 * @since 21.01.2015
 */
class DiplomaBlankReportPrint
{
    Session session
    byte[] template

    Date formDate
    Long formBy
    Collection<DipBlankType> blankTypes
    Collection<OrgUnit> storageLocations

    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()
    def deleteLabels = new ArrayList<>()

    def print()
    {
        im.put('typeReport', DiplomaBlankReportManager.getReportFormByTitle(formBy))
        im.put('fullTitle', TopOrgUnit.instance.fullTitle)
        im.put('dateForm', DateFormatter.DATE_FORMATTER_WITH_TIME.format(formDate))

        if (blankTypes.isEmpty() && storageLocations.isEmpty())
        {
            deleteLabels.add('H')

        } else
        {
            def reportParams = new ArrayList<String[]>()
            if (!blankTypes.isEmpty())
                reportParams.add([
                        'Тип бланка',
                        StringUtils.join(CommonBaseEntityUtil.getPropertiesList(blankTypes, DipBlankType.P_TITLE), '\n')]
                        as String[])

            if (!storageLocations.isEmpty())
                reportParams.add([
                        'Место хранения',
                        StringUtils.join(CommonBaseEntityUtil.getPropertiesList(storageLocations, OrgUnit.P_TITLE), '\n')]
                        as String[])

            tm.put('H', reportParams as String[][])
            tm.put('H', new RtfRowIntercepterBase() {
                @Override
                public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
                {
                    if (colIndex == 1)
                    {
                        Iterator<String> parts = Arrays.asList(value.split('\n')).iterator();
                        RtfString rtfString = new RtfString();
                        while (parts.hasNext())
                        {
                            String part = parts.next();

                            rtfString.append(part);

                            if (parts.hasNext())
                                rtfString.par();
                        }

                        return rtfString.toList();
                    }

                    return null;
                }
            })
        }

        // type -> storage -> state -> count
        Collection<Object[]> rows = new DQLSelectBuilder()
            .fromEntity(DipDiplomaBlank.class, 'b')
            .joinPath(DQLJoinType.inner, DipDiplomaBlank.blankType().fromAlias('b'), 't')
            .joinPath(DQLJoinType.inner, DipDiplomaBlank.blankState().fromAlias('b'), 's')
       /*0*/.column(property('t.code'))
       /*1*/.column(property('b', DipDiplomaBlank.storageLocation().id()))
       /*2*/.column(property('s.code'))
       /*3*/.column(DQLFunctions.count(property('b.id')))
            .group(property('t.code'))
            .group(property('s.code'))
            .group(property('b', DipDiplomaBlank.storageLocation().id()))
            .where(blankTypes.isEmpty() ? null : DQLExpressions.in(property('t'), blankTypes))
            .where(storageLocations.isEmpty() ? null : DQLExpressions.in(property('b', DipDiplomaBlank.storageLocation()), storageLocations))
            .createStatement(session).list()

        final Map<String, Map<Long, Map<String, Long>>> totalCountMap = SafeMap.get(new SafeMap.Callback<String, Map<Long, Map<String, Long>>>() {
            @Override Map<Long, Map<String, Long>> resolve(String key) { return SafeMap.get(HashMap.class) }})

        for (Object[] row: rows)
            totalCountMap.get((String) row[0]).get((Long) row[1]).put((String) row[2], (Long) row[3])

        if (blankTypes.isEmpty()) blankTypes = IUniBaseDao.instance.get().getList(DipBlankType.class, DipBlankType.title().s())
        if (storageLocations.isEmpty())
            storageLocations = new DQLSelectBuilder()
                .fromEntity(OrgUnit.class, 'o')
                .where(exists(new DQLSelectBuilder()
                    .fromEntity(DipDiplomaBlank.class, 'b')
                    .where(eq(property('b', DipDiplomaBlank.storageLocation()), property('o')))
                    .buildQuery()))
                .order(property('o', OrgUnit.title()))
                .createStatement(session)
                .list()

        def totalRows = new ArrayList<String[]>()
        for (DipBlankType blankType: blankTypes)
        {
            Map<Long, Map<String, Long>> countMap = totalCountMap.get(blankType.code)
            Map<String, Long> localCountMap = new HashMap<>()
            for (Map<String, Long> m: countMap.values())
            {
                for (Map.Entry<String, Long> e: m.entrySet())
                {
                    Long value = localCountMap.get(e.key)
                    localCountMap.put(e.key, e.value + (value == null ? 0L : value))
                }
            }

            totalRows.add([
                    blankType.title,
                    count(localCountMap, DipBlankStateCodes.FREE),
                    count(localCountMap, DipBlankStateCodes.LINKED),
                    count(localCountMap, DipBlankStateCodes.RESERVED),
                    count(localCountMap, DipBlankStateCodes.DISPOSAL),
                    count(localCountMap, DipBlankStateCodes.ISSUED)
            ] as String[])
        }

        tm.put('T3', totalRows as String[][])

        RtfDocument document = new RtfReader().read(template);
        RtfDocument inner = null

        switch (formBy)
        {
            case DiplomaBlankReportManager.FORM_BY_TYPE : inner = formByType(blankTypes, storageLocations, totalCountMap); break
            case DiplomaBlankReportManager.FORM_BY_STORAGE_LOCATION : inner = formByStorageLocation(blankTypes, storageLocations, totalCountMap); break

            default : throw new IllegalStateException()
        }

        RtfUtil.modifySourceList(document.header, inner.header, inner.elementList)
        im.put('reportForm', inner.elementList)

        im.modify(document);
        tm.modify(document);
        UniRtfUtil.deleteRowsWithLabels(document, deleteLabels);

        return [document: RtfUtil.toByteArray(document), fileName: 'blank' + DateFormatter.DEFAULT_DATE_FORMATTER.format(formDate) + '.rtf']
    }

    private static String count(Map<String, Long> countMap, String ... states)
    {
        long result = 0L
        for (String state: states)
        {
            Long count = countMap.get(state)
            if (count != null)
                result += count
        }

        return String.valueOf(result)
    }

    private static RtfDocument formByType(Collection<DipBlankType> blankTypes, Collection<OrgUnit> storageLocations, Map<String, Map<Long, Map<String, Long>>> totalCountMap)
    {
        RtfDocument template = new RtfReader().read(IUniBaseDao.instance.get().getCatalogItem(DipScriptItem.class, DipScriptItemCodes.DIPLOMA_BLANK_REPORT_TYPE).currentTemplate)
        RtfDocument result = template.clone
        result.elementList.clear()

        for (DipBlankType blankType: blankTypes)
        {
            RtfDocument clone = template.clone

            new RtfInjectModifier()
                .put('blankType', blankType.title)
                .modify(clone)

            Map<Long, Map<String, Long>> countMap = totalCountMap.get(blankType.code)
            def rows = new ArrayList<String[]>()
            for (OrgUnit storageLocation: storageLocations)
            {
                Map<String, Long> localCountMap = countMap.get(storageLocation.id)
                rows.add([
                        storageLocation.title,
                        count(localCountMap, DipBlankStateCodes.FREE, DipBlankStateCodes.LINKED, DipBlankStateCodes.RESERVED),
                        count(localCountMap, DipBlankStateCodes.FREE),
                        count(localCountMap, DipBlankStateCodes.LINKED),
                        count(localCountMap, DipBlankStateCodes.RESERVED)
                ] as String[])
            }

            new RtfTableModifier()
                .put('T1', rows as String[][])
                .modify(clone)

            result.elementList.addAll(clone.elementList)
        }

        return result
    }

    private static RtfDocument formByStorageLocation(Collection<DipBlankType> blankTypes, Collection<OrgUnit> storageLocations, Map<String, Map<Long, Map<String, Long>>> totalCountMap)
    {
        RtfDocument template = new RtfReader().read(IUniBaseDao.instance.get().getCatalogItem(DipScriptItem.class, DipScriptItemCodes.DIPLOMA_BLANK_REPORT_STORAGE_LOCATION).currentTemplate)
        RtfDocument result = template.clone
        result.elementList.clear()

        for (OrgUnit storageLocation: storageLocations)
        {
            RtfDocument clone = template.clone

            new RtfInjectModifier()
                .put('orgUnit', storageLocation.title)
                .modify(clone)

            def rows = new ArrayList<String[]>()
            for (DipBlankType blankType: blankTypes)
            {
                rows.add([
                        blankType.title,
                        count(totalCountMap.get(blankType.code).get(storageLocation.id), DipBlankStateCodes.FREE, DipBlankStateCodes.LINKED, DipBlankStateCodes.RESERVED),
                        count(totalCountMap.get(blankType.code).get(storageLocation.id), DipBlankStateCodes.FREE),
                        count(totalCountMap.get(blankType.code).get(storageLocation.id), DipBlankStateCodes.LINKED),
                        count(totalCountMap.get(blankType.code).get(storageLocation.id), DipBlankStateCodes.RESERVED)
                ] as String[])
            }

            new RtfTableModifier()
                .put('T2', rows as String[][])
                .modify(clone)

            result.elementList.addAll(clone.elementList)
        }

        return result
    }
}
