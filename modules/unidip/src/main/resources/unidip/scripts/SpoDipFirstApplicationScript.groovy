package unidip.scripts

import org.hibernate.Session
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils
import org.tandemframework.core.view.formatter.DoubleFormatter
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import ru.tandemservice.unidip.base.bo.DipDocument.logic.DipDocumentUtils
import ru.tandemservice.unidip.base.entity.catalog.DipDocTemplateCatalog
import ru.tandemservice.unidip.base.entity.diploma.*
import ru.tandemservice.unidip.settings.entity.DipAssistantManager
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes

/* $Id:$ */

return new SpoDipFistApplicationPrint(               // стандартные входные параметры скрипта
        session: session,                         // сессия
        template: template,                       // шаблон
        diplomaObject: session.get(DiplomaObject.class, diplomaObjectId),   //объект печати
        contentIssuance: contentIssuance,         //Приложение к факту выдачи диплома
        contentIssuanceList: contentIssuanceList, //Список выбранных приложений
        diplomaIssuance: diplomaIssuance          //Факт выдачи диплома
).print()


class SpoDipFistApplicationPrint
{
    Session session
    byte[] template
    DiplomaObject diplomaObject
    DiplomaContentIssuance contentIssuance
    DiplomaIssuance diplomaIssuance
    List<DiplomaContentIssuance> contentIssuanceList


    def print() {
        def im = new RtfInjectModifier()
        def tm = new RtfTableModifier()

        List<DiplomaAcademyRenameData> diplomaAcademyRenameDataList = DataAccessServices.dao().getList(DiplomaAcademyRenameData.class,
                DiplomaAcademyRenameData.L_DIPLOMA_CONTENT, diplomaObject.getContent(), DiplomaAcademyRenameData.academyRename().date().s())

        def academyRename = getAcademyRename(diplomaAcademyRenameDataList)
        im.put("academyRename", academyRename)
        DipAdditionalInformation additionalInformation = DataAccessServices.dao().get(DipAdditionalInformation.class, DipAdditionalInformation.L_DIPLOMA_OBJECT, diplomaObject)
        def information = getSPOInformation(additionalInformation)
        im.put("information", information)

        DipAssistantManager dipAssistantManager = new DQLSelectBuilder().fromEntity(DipAssistantManager.class, "e")
                .createStatement(session).<DipAssistantManager> uniqueResult();
        def topOrgUnit = new DQLSelectBuilder().fromEntity(TopOrgUnit.class, "e")
                .createStatement(session).<TopOrgUnit> uniqueResult()
        if (dipAssistantManager != null)
        {
            im.put("fioRector", dipAssistantManager.employeePost.fio)
            im.put("slash", "/")
        }
        else
        {
            im.put("fioRector", topOrgUnit.head!=null?topOrgUnit.head.employee.person.identityCard.fio:"")
            im.put("slash", "")
        }

        def pages = "4"
        for (DiplomaContentIssuance included : contentIssuanceList) {
            if (included.contentListNumber == 2) {
                pages = "8"
                break;
            }
        }
        im.put("pages", pages)
        im.put("fullNameOrganization", topOrgUnit.getNominativeCaseTitle())
        im.put("cityOrganization", topOrgUnit.getTerritorialFullTitle().replace(".", ""))

        def duplicate = "";
        if (contentIssuance.isDuplicate() || diplomaIssuance.replacedIssuance != null)
		{
            duplicate = "ДУБЛИКАТ"
        }
        im.put("duplicate", duplicate)


        im.put("degreeDifferences", "о среднем профессиональном образовании" + (diplomaObject.content.withSuccess ? " с отличием" : ""))

        im.put("developPeriod", DipDocumentUtils.getDevelopPeriod(diplomaObject))

        def regNumber = contentIssuance.duplicate ? contentIssuance.duplicateRegustrationNumber : diplomaIssuance.registrationNumber;
        im.put("regNumber", regNumber)

        def issuDate = contentIssuance.duplicate ? RussianDateFormatUtils.MONTH_STRING_DATE_FORMAT_NO_QUOTES.format(contentIssuance.duplicateIssuanceDate) :
                RussianDateFormatUtils.MONTH_STRING_DATE_FORMAT_NO_QUOTES.format(diplomaIssuance.issuanceDate)
        im.put("issuDate", issuDate + " года")

        im.put("lastName", diplomaObject.student.person.identityCard.lastName)
        im.put("firstName", diplomaObject.student.person.identityCard.firstName)
        im.put("middleName", diplomaObject.student.person.identityCard.middleName)
        def birthDate = diplomaObject.student.person.identityCard.birthDate != null ?
                RussianDateFormatUtils.MONTH_STRING_DATE_FORMAT_NO_QUOTES.format(diplomaObject.student.person.identityCard.birthDate) + " года" : "";
        im.put("birthDate", birthDate)

        def eduDocumentKind = diplomaObject.student.eduDocument;
        def eduDocumentKindTitle = ""
        def yearEnd = ""
        if (eduDocumentKind != null)
        {
            eduDocumentKindTitle = eduDocumentKind.documentKindTitle.toLowerCase()
            if (!eduDocumentKind.eduOrganizationAddressItem.country.title.equals("Россия"))
            {
                eduDocumentKindTitle += ", " + eduDocumentKind.eduOrganizationAddressItem.country.title;
            }
            yearEnd = String.valueOf(eduDocumentKind.yearEnd) + " год"
        }
        im.put("eduDocumentKind", eduDocumentKindTitle)
        im.put("yearEnd", yearEnd)

        def programSubject = diplomaObject.getContent().getProgramSubject() != null ? diplomaObject.getContent().getProgramSubject().getTitleWithCodeOksoWithoutSpec() : "";
        im.put("programSubject", programSubject)

        def programKind = diplomaObject.content.programSubject.eduProgramKind;
        def isMaster = programKind.code == EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_KVALIFITSIROVANNYH_RABOCHIH_SLUJATSHIH_;
        def programKindWithPO = isMaster ? "по профессии" : "по специальности";
        im.put("programKindWithPO", programKindWithPO);
        def programKindWithoutPO = isMaster ? "профессии" : "специальности";
        im.put("programKindWithoutPO", programKindWithoutPO);

        im.put("programQualification", DipDocumentUtils.getSecondaryProfQualificationTitle(diplomaObject))

        RtfDocument document = new RtfReader().read(template);


        DipDocTemplateCatalog disciplineTemplate = new DipDocTemplateCatalog();
        disciplineTemplate.setTemplatePath("unidip/templates/T_discipline-spo.rtf")
        RtfDocument disciplineDoc = new RtfReader().read(disciplineTemplate.template);
        RtfUtil.modifySourceList(document.header, disciplineDoc.header, disciplineDoc.elementList);




        List<DiplomaContentRow> diplomaContentRowList = DataAccessServices.dao().getList(DiplomaContentRow.class,
                DiplomaContentRow.owner(), diplomaObject.content, DiplomaContentRow.P_NUMBER)

        ArrayList<String[]> disciplineRows = new ArrayList<String[]>()
        ArrayList<String[]> practiceRows = new ArrayList<String[]>()
        double allPracticeWeeksLoad = 0

        ArrayList<String[]> stateExamRows = new ArrayList<String[]>()
        double allStateExamWeeksLoad = 0
        ArrayList<String[]> graduateWorkRows = new ArrayList<String[]>()
        ArrayList<String[]> courseRows = new ArrayList<String[]>()
        double audLoad = 0
        double totalLoad = 0
        boolean printLaborAndAudience = true;
        boolean attestationShowSecond = true
        List<DiplomaContentRow> stateExams = new ArrayList()
        List<DiplomaContentRow> qualifWorks = new ArrayList()

        for (DiplomaContentRow contentRow : diplomaContentRowList)
        {
            if (contentRow instanceof DiplomaQualifWorkRow)
            {
                qualifWorks.add(contentRow)
            }
            if (contentRow instanceof DiplomaStateExamRow)
            {
                if (contentRow.contentListNumber == 1) attestationShowSecond = false
                stateExams.add(contentRow)
            }
            if (contentRow.contentListNumber == 2)
            {
                if (contentRow instanceof DiplomaDisciplineRow)
                {
                    printLaborAndAudience = false
                }
                if (contentRow instanceof DiplomaPracticeRow)
                {
                    allPracticeWeeksLoad += contentRow.weeksAsDouble != null ? contentRow.weeksAsDouble : 0;
                }
                if (contentRow instanceof DiplomaStateExamRow || contentRow instanceof DiplomaQualifWorkRow)
                {
                    allStateExamWeeksLoad += contentRow.weeksAsDouble != null ? contentRow.weeksAsDouble : 0;
                }
            }
            else if (contentRow.contentListNumber == 1)
            {
                if (contentRow instanceof DiplomaDisciplineRow)
                {
                    disciplineRows.add([contentRow.title, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(contentRow.loadAsDouble != null ? contentRow.loadAsDouble : 0), contentRow.mark] as String[])
                    totalLoad += contentRow.loadAsDouble != null ? contentRow.loadAsDouble : 0;
                    audLoad += contentRow.audLoadAsDouble != null ? contentRow.audLoadAsDouble : 0;
                }

                if (contentRow instanceof DiplomaPracticeRow)
                {
                    allPracticeWeeksLoad += contentRow.weeksAsDouble != null ? contentRow.weeksAsDouble : 0;
                    practiceRows.add([contentRow.title, DipDocumentUtils.getWeeksWithUnit(contentRow.weeksAsDouble), contentRow.mark] as String[])
                }
                if (contentRow instanceof DiplomaStateExamRow)
                {
                    allStateExamWeeksLoad += contentRow.weeksAsDouble != null ? contentRow.weeksAsDouble : 0;
                    stateExamRows.add([contentRow.title, "x", contentRow.mark] as String[])
                }
                if (contentRow instanceof DiplomaQualifWorkRow)
                {
                    String title = contentRow.theme != null ? contentRow.title + " «" + contentRow.theme + "»" : contentRow.title
                    graduateWorkRows.add([title, "x", contentRow.mark] as String[])
                    allStateExamWeeksLoad += contentRow.weeksAsDouble != null ? contentRow.weeksAsDouble : 0;
                }
                if (contentRow instanceof DiplomaCourseWorkRow)
                {
                    String title = contentRow.theme != null ? contentRow.title + " «" + contentRow.theme + "»" : contentRow.title
                    courseRows.add([title, contentRow.mark] as String[])
                }


            }

        }

        if (stateExams.empty && !qualifWorks.empty)
            attestationShowSecond = qualifWorks.get(0).contentListNumber == 2

        String practiceLoad = DipDocumentUtils.getWeeksWithUnit(allPracticeWeeksLoad)

        String examLoad =  DipDocumentUtils.getWeeksWithUnit(allStateExamWeeksLoad)



        tm.put("discipline", disciplineRows as String[][])
        fillPractice(diplomaContentRowList, practiceLoad, practiceRows, tm, 1)
        fillSPOStateExam(examLoad, stateExamRows, !attestationShowSecond, tm)
        tm.put("graduateWork", graduateWorkRows as String[][])
        def eduProgramRow = new ArrayList<String[]>()
        def audLoadRow = new ArrayList<String[]>()
        if (printLaborAndAudience)
        {

            eduProgramRow.add(["ВСЕГО часов теоретического обучения:", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(totalLoad) + " час.", "x"] as String[])
            audLoadRow.add(["в том числе аудиторных часов:", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(audLoad) + " час.", "x"] as String[])
        }
        tm.put("labor", eduProgramRow as String[][])
        tm.put("audience", audLoadRow as String[][])

        tm.put("T", courseRows as String[][])

        im.modify(document)
        tm.modify(document)
        return [document: document, fileName: 'diploma.rtf']
    }

    private static void fillPractice(List<DiplomaContentRow> diplomaContentRowList, String practiceRowsLoad, ArrayList<String[]> practiceRows, RtfTableModifier tm, int contentListNumber)
    {

        boolean showTitle = false;
        for (DiplomaContentRow contentRow : diplomaContentRowList)
        {
            if (contentRow instanceof DiplomaPracticeRow) {
                showTitle = contentRow.contentListNumber == contentListNumber;
                break;
            }
        }
        def allRows = new ArrayList<String[]>()

        if (showTitle)
        {
            allRows.add(["Практики", practiceRowsLoad, "x"] as String[])
            allRows.add(["в том числе:"] as String[])
        }
        tm.put("allPractice", allRows as String[][])
        tm.put("practice", practiceRows as String[][])
    }

    private static void fillSPOStateExam(String examRowsLoad, ArrayList<String[]> stateExamRows, boolean showTotalRows, RtfTableModifier tm)
    {
        def allRows = new ArrayList<String[]>()
        if (showTotalRows)
        {
            allRows.add(["Государственная итоговая аттестация", examRowsLoad, "x"] as String[])
            allRows.add(["в том числе:"] as String[])
        }
        tm.put("attestation", allRows as String[][])
        tm.put("exam", stateExamRows as String[][])
    }

    private static RtfString getSPOInformation(DipAdditionalInformation additionalInformation)
    {
        def information = new RtfString();
        if (additionalInformation?.isPassIntenssiveTraining()) information.append("Пройдено ускоренное обучение в пределах образовательной программы среднего профессионального образования.")
        return information
    }

    private static RtfString getAcademyRename(List<DiplomaAcademyRenameData> diplomaAcademyRenameDataList)
    {
        def academyRename = new RtfString();
        for (DiplomaAcademyRenameData renameData : diplomaAcademyRenameDataList)
        {
            if (diplomaAcademyRenameDataList.indexOf(renameData) > 0)
            {
                academyRename.par()
            }
            academyRename.append("Образовательная организация переименована в " + RussianDateFormatUtils.getYearString(renameData.getAcademyRename().getDate(), false) + " году.").par();
            academyRename.append("Старое полное официальное наименование образовательной организации - " + renameData.getAcademyRename().getPreviousFullTitle() + ".");
        }
        return academyRename
    }
}



