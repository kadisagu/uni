package unidip.scripts

import org.hibernate.Session
import org.tandemframework.common.base.entity.IPersistentEmployeePost
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.shared.employeebase.base.entity.EmployeePost
import org.tandemframework.shared.organization.base.entity.OrgUnit
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes
import ru.tandemservice.unidip.base.entity.diploma.DiplomaObject

import static org.tandemframework.hibsupport.dql.DQLExpressions.*

/**
 * @author Andrey Avetisov
 * @since 29.05.2014
 */


return new CertScript(                                                       // стандартные входные параметры скрипта
        session: session,                                                    // сессия
        diplomaObject: session.get(DiplomaObject.class, diplomaObjectId),    //объект печати
        template: template,                                                  // шаблон
).print()

class CertScript
{
    Session session
    byte[] template
    DiplomaObject diplomaObject

    def print()
    {
        def im = new RtfInjectModifier()
        def tm = new RtfTableModifier()

        def topOrgUnit = new DQLSelectBuilder().fromEntity(TopOrgUnit.class, "e")
                .createStatement(session).<TopOrgUnit> uniqueResult()

        im.put("fio", diplomaObject.getStudent().getPerson().getFullFio())
        im.put("course", diplomaObject.getContent().getEducationElementTitle())
        im.put("date", DateFormatter.DEFAULT_DATE_FORMATTER.format(diplomaObject.getRegistrationDate()))
        im.put("academy", topOrgUnit.getPrintTitle())
        im.put("orgUnShort", diplomaObject.getStudent().getEducationOrgUnit().getFormativeOrgUnit().getGenitiveCaseTitle())

        OrgUnit formativeOrgUnit = diplomaObject.getStudent().getEducationOrgUnit().getFormativeOrgUnit()
        String managerPostTitle
        if (formativeOrgUnit.getOrgUnitType().getCode().equals(OrgUnitTypeCodes.INSTITUTE) || formativeOrgUnit.getOrgUnitType().getCode().equals(OrgUnitTypeCodes.BRANCH) || formativeOrgUnit.getOrgUnitType().getCode().equals(OrgUnitTypeCodes.DEPARTMENT))
            managerPostTitle = "Директор "
        else
            managerPostTitle = "Декан "
        im.put("post", managerPostTitle)



        EmployeePost manager = getHead(formativeOrgUnit);
        if (null != manager)
            im.put("dean", manager.getPerson().getIdentityCard().getFio())

        EmployeePost rector = getHead(topOrgUnit)
        if (null != rector)
            im.put("rector", rector.getPerson().getIdentityCard().getFio())

        RtfDocument document = new RtfReader().read(template);
        im.modify(document)
        tm.modify(document)

        return [document: document, fileName: 'document.rtf']
    }

    public EmployeePost getHead(OrgUnit orgUnit)
    {
        IPersistentEmployeePost head = orgUnit.getHead()
        if (head instanceof EmployeePost)
            return (EmployeePost) head

        List<EmployeePost> probablyHeaderPosts = new DQLSelectBuilder()
                .fromEntity(EmployeePost.class, "ep")
                .where(eq(property(EmployeePost.orgUnit().fromAlias("ep")), value(orgUnit)))
                .where(eq(property(EmployeePost.postRelation().headerPost().fromAlias("ep")), value(Boolean.TRUE)))
                .where(eq(property(EmployeePost.employee().archival().fromAlias("ep")), value(Boolean.FALSE)))
                .where(eq(property(EmployeePost.postStatus().active().fromAlias("ep")), value(Boolean.TRUE)))
                .order(property(EmployeePost.person().identityCard().fullFio().fromAlias("ep"))).createStatement(session).list()

        if (probablyHeaderPosts.size() == 1)
            return probablyHeaderPosts.get(0)

        return null
    }
}