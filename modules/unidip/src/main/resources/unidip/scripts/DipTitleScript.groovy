package unidip.scripts
import org.hibernate.Session
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import ru.tandemservice.unidip.base.bo.DipDocument.logic.DipDocumentUtils
import ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance
import ru.tandemservice.unidip.base.entity.diploma.DiplomaObject
import ru.tandemservice.unidip.settings.entity.DipAssistantManager

/**
 * @author Andrey Avetisov
 * @since 15.09.2014
 */

return new DipTitlePrint(                                                   // стандартные входные параметры скрипта
        session: session,                                                   // сессия
        template: template,                                                 // шаблон
        diplomaObject: session.get(DiplomaObject.class, diplomaObjectId),   //Диплом
        diplomaIssuance: diplomaIssuance,                                   //Факт выдачи диплома
).print()

class DipTitlePrint
{
    Session session
    byte[] template
    DiplomaObject diplomaObject
    DiplomaIssuance diplomaIssuance

    def print()
    {
        def im = new RtfInjectModifier()
        def tm = new RtfTableModifier()

        def topOrgUnit = new DQLSelectBuilder().fromEntity(TopOrgUnit.class, "e")
                .createStatement(session).<TopOrgUnit> uniqueResult()

        im.put("fullNameOrganization", topOrgUnit.nominativeCaseTitle)
        im.put("cityOrganization", topOrgUnit.territorialTitle.replace(".", ""))



        RtfDocument document = new RtfReader().read(template);

        if (diplomaIssuance != null && diplomaIssuance.replacedIssuance != null)
        {
            im.put("duplicate", "ДУБЛИКАТ")
        }
        else
        {
            im.put("duplicate", "")
        }

        if (diplomaIssuance != null)
        {
            im.put("registrationNumber", diplomaIssuance.registrationNumber)
            im.put("issuanceDate", RussianDateFormatUtils.MONTH_STRING_DATE_FORMAT_NO_QUOTES.format(diplomaIssuance.issuanceDate)+" года")
        }
        else
        {
            im.put("registrationNumber", "")
            im.put("issuanceDate", "")
        }

		def student = diplomaObject.student

        im.put("lastName", student.person.identityCard.lastName)
        im.put("firstName", student.person.identityCard.firstName)
        im.put("middleName", student.person.identityCard.middleName)
        def programSubject = diplomaObject.content.programSubject!=null?diplomaObject.content.programSubject.titleWithCodeOksoWithoutSpec:"";
        im.put("programSubject", programSubject)

		im.put("programQualification", DipDocumentUtils.getHigherProfQualificationTitle(diplomaObject))

        im.put("protocolNumber", diplomaObject.stateCommissionProtocolNumber != null ? diplomaObject.stateCommissionProtocolNumber + " от" : "")

        if (diplomaObject.getStateCommissionDate() != null)
        {
            im.put("pDay", RussianDateFormatUtils.getDayString(diplomaObject.stateCommissionDate, true))
            im.put("pMonth", RussianDateFormatUtils.getMonthName(diplomaObject.stateCommissionDate, false))
            im.put("pYear", RussianDateFormatUtils.getYearString(diplomaObject.stateCommissionDate, false))
        }
        else
        {
            im.put("pDay", "")
            im.put("pMonth", "")
            im.put("pYear", "")
        }

        if (diplomaObject.stateCommissionChairFio != null)
        {
            im.put("fioGEC", diplomaObject.stateCommissionChairFio)
        }
        else
        {
            im.put("fioGEC", "")
        }
        DipAssistantManager dipAssistantManager = new DQLSelectBuilder().fromEntity(DipAssistantManager.class, "e")
                .createStatement(session).<DipAssistantManager> uniqueResult();
        if (dipAssistantManager != null)
        {
            im.put("fioRector", dipAssistantManager.employeePost.fio)
            im.put("slash", "/")
        }
        else
        {
            im.put("fioRector", topOrgUnit.head!=null?topOrgUnit.head.employee.person.identityCard.fio:"")
            im.put("slash", "")
        }



        im.modify(document)
        tm.modify(document)
        return [document: document, fileName: 'document.rtf']
    }

}