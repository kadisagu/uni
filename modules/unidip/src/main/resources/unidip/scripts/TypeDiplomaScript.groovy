package unidip.scripts

import org.hibernate.Session
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import ru.tandemservice.uni.entity.catalog.EducationLevels
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels
import ru.tandemservice.unidip.base.entity.diploma.*

import java.text.SimpleDateFormat

/**
 * @author Andrey Avetisov
 * @since 27.05.2014
 */
return new DiplomaScript(                                                    // стандартные входные параметры скрипта
        session: session,                                                    // сессия
        diplomaObject: session.get(DiplomaObject.class, diplomaObjectId),    // объект печати
        template: template,                                                  // шаблон
).print()

class DiplomaScript
{
    Session session
    byte[] template
    DiplomaObject diplomaObject

    def print()
    {
        def im = new RtfInjectModifier()
        def tm = new RtfTableModifier()

        def df = new DateFormatter(new SimpleDateFormat("d MMMMM yyyy", RussianDateFormatUtils.DATE_FORMAT_SYMBOLS_RU))
        def academy = new DQLSelectBuilder().fromEntity(TopOrgUnit.class, "e")
                .createStatement(session).<TopOrgUnit>uniqueResult()

        def speciality = diplomaObject.getStudent().getEducationOrgUnit().getEducationLevelHighSchool()
        def mainEduDocument = diplomaObject.getStudent().getPerson().getPersonEduInstitution()

        im.put("academyTitle", academy.getFullTitle())
        im.put("fio", diplomaObject.getStudent().getPerson().getFullFio());
        im.put("birthDateMonthStr", df.format(diplomaObject.getStudent().getPerson().getIdentityCard().getBirthDate()))
        if (null != mainEduDocument)
        {
            im.put("eduDocumentType", mainEduDocument.getDocumentType().getTitle())
            im.put("eduDocYear", String.valueOf(mainEduDocument.getYearEnd()))
        }
        else
        {
            im.put("eduDocumentType", "                                      ")
            im.put("eduDocYear", "    ")
        }


        im.put("passed", diplomaObject.getStudent().getPerson().getIdentityCard().getSex().isMale() ? "прошел" : "прошла")
        im.put("entranceYear", String.valueOf(diplomaObject.getStudent().getEntranceYear()))
        im.put("finishYear", String.valueOf(diplomaObject.getStudent().getFinishYear()).equals(null) ? String.valueOf(diplomaObject.getStudent().getFinishYear()) : "____")
        im.put("developForm", diplomaObject.getStudent().getEducationOrgUnit().getDevelopForm().getTitle().toLowerCase())
        im.put("specialization", speciality.getPrintTitle())
        im.put("speciality", getEducationOrgUnitTitle(speciality))
        im.put("developPeriod", diplomaObject.getStudent().getEducationOrgUnit().getDevelopPeriod().getTitle())
        im.put("academyCity", academy.getAddress().getSettlement().getTitle())
        im.put("documentNum", String.valueOf(diplomaObject.getUniqueNumber()))

        if (null != diplomaObject.getNumber())
            im.put("regNumber", diplomaObject.getNumber())
        else im.put("regNumber", "")
        if (null != diplomaObject.getRegistrationDate())
            im.put("registrationDate", df.format(diplomaObject.getRegistrationDate()))
        else im.put("registrationDate", "")
        if (null != diplomaObject.getIssueDate())
            im.put("issuanceDateMonthStr", df.format(diplomaObject.getIssueDate()))
        else im.put("issuanceDateMonthStr", "")
        im.put("qualification", diplomaObject.getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getQualificationTitleNullSafe().toLowerCase())

        double FullTime = 0
        double FullAudTime = 0
        for (DiplomaDisciplineRow row : DataAccessServices.dao().getList(DiplomaDisciplineRow.class, DiplomaDisciplineRow.owner(), diplomaObject.getContent()))
        {
            FullTime += null == row.getLoadAsDouble() ? 0 : row.getLoadAsDouble()
            FullAudTime += null == row.getAudLoadAsDouble() ? 0 : row.getAudLoadAsDouble()
        }
        im.put("audHours", String.valueOf(FullAudTime))
        im.put("hrInCom", String.valueOf(FullTime))


        int cnt_disc = 0
        int cnt_row = 48
        String str1 = ""
        String str2 = ""

        List<String[]> discDataLines = new ArrayList<String[]>()
        for (DiplomaDisciplineRow row : DataAccessServices.dao().getList(DiplomaDisciplineRow.class, DiplomaDisciplineRow.owner(), diplomaObject.getContent()))
        {
            String[] str = [null, null, null]
            int cnt_cell = 1
            while (row.getTitle().length() / cnt_cell > 76)
            {
                cnt_row--
                cnt_cell++
            }
            if (cnt_disc > cnt_row)
            {
                cnt_row *= 2

                for (int k = 0; k < 5; k++)
                    discDataLines.add(str)
                str1 = "______________________________________________________________________________________"
                str2 = "(продолжение на следующей странице)"
            }
            str = [String.valueOf(++cnt_disc) + ".", row.getTitle(), null == row.getLoadAsDouble() ? "" : String.valueOf(row.getLoadAsDouble()), row.getMark()]
            discDataLines.add(str)
        }

        String[] buf;
        String[][] discArray = discDataLines.toArray()
        if (discArray.length > 0 && null == discArray[discArray.length - 1][1])
        {
            buf = discArray[discArray.length - 1]
            discArray[discArray.length - 1] = discArray[discArray.length - 6]
            discArray[discArray.length - 6] = buf
        }
        tm.put("T_DISC_LIST", discArray)
        im.put("line", str1)
        im.put("continue", str2)

        List<String[]> practDataLines = new ArrayList<String[]>();
        for (DiplomaPracticeRow row : DataAccessServices.dao().getList(DiplomaPracticeRow.class, DiplomaPracticeRow.owner(), diplomaObject.getContent()))
            practDataLines.add([row.getTitle() + ", " + (null == row.getLoadAsDouble() ? "" : String.valueOf(row.getLoadAsDouble().intValue())) + " ч, " + row.getMark()] as String[])

        String[][] resToArray = practDataLines.toArray()
        tm.put("PRACT_T_SHORT", resToArray)


        List<String[]> courseWDataLines = new ArrayList<String[]>();
        for (DiplomaCourseWorkRow row : DataAccessServices.dao().getList(DiplomaCourseWorkRow.class, DiplomaCourseWorkRow.owner(), diplomaObject.getContent()))
            courseWDataLines.add([row.getTitle() + ", " + row.getMark()]);
        resToArray = courseWDataLines.toArray()
        tm.put("COURSE_WT_SHORT", resToArray);

        List<String[]> StateExamDataLines = new ArrayList<String[]>()
        for (DiplomaStateExamRow row : DataAccessServices.dao().getList(DiplomaStateExamRow.class, DiplomaStateExamRow.owner(), diplomaObject.getContent()))
            StateExamDataLines.add([row.getTitle() + ", " + row.getMark()])
        resToArray = StateExamDataLines.toArray()
        tm.put("FINAL_STATE_EXAMS", resToArray)



        List<String[]> qualifWDataLines = new ArrayList<String[]>()
        for (DiplomaQualifWorkRow row : DataAccessServices.dao().getList(DiplomaQualifWorkRow.class, DiplomaQualifWorkRow.owner(), diplomaObject.getContent()))
            qualifWDataLines.add([row.getTitle() + ", " + row.getMark()])
        resToArray = qualifWDataLines.toArray()
        tm.put("GRADUATE_QUALIFI_WORK", resToArray)

        RtfDocument document = new RtfReader().read(template);
        im.modify(document)
        tm.modify(document)

        return [document: document, fileName: 'document.rtf']
    }


    String getEducationOrgUnitTitle(EducationLevelsHighSchool highSchool)
    {
        StructureEducationLevels levelType = highSchool.getEducationLevel().getLevelType();

        if (levelType.isSpecialization())
        {
            EducationLevels educationLevels = highSchool.getEducationLevel();

            while (educationLevels.getParentLevel() != null)
            {
                StructureEducationLevels parentLevelType = educationLevels.getLevelType();
                if (parentLevelType.isSpecialty())
                {
                    return educationLevels.getDisplayableTitle();
                }

                educationLevels = educationLevels.getParentLevel();
            }
        }
        return highSchool.getPrintTitle();
    }


}


