package unidip.scripts

import org.hibernate.Session
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import org.tandemframework.shared.person.base.bo.Person.PersonManager
import org.tandemframework.shared.person.base.entity.IdentityCard
import ru.tandemservice.unidip.base.entity.order.DipStuExcludeExtract
import ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank
import ru.tandemservice.unidip.bso.entity.catalog.DipBlankType
import ru.tandemservice.unidip.bso.entity.catalog.codes.DipBlankStateCodes
import ru.tandemservice.unidip.bso.entity.catalog.codes.DipBlankTypeCodes

return new DisposalActPrint(                        // стандартные входные параметры скрипта
        session: session,                           // сессия
        template: template,                         // шаблон
        blanks: blanks,                             // бланки дипломов и приложений
        blanksToExtracts: blanksToExtracts,         // выписки из приказов об отчислении, соответствующие бланкам
        destructionDate: destructionDate            // дата уничтожения
).print()

/**
 * @author azhebko
 * @since 21.01.2015
 */
class DisposalActPrint
{
    Session session
    byte[] template
    Collection<DipDiplomaBlank> blanks
    Map<DipDiplomaBlank, DipStuExcludeExtract> blanksToExtracts
    Date destructionDate

    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()

    def print()
    {
        String destructionDateStr = DateFormatter.DEFAULT_DATE_FORMATTER.format(destructionDate)
        Collection<String[]> rows = new ArrayList<>()
        for (DipDiplomaBlank blank: blanks) {
            final disposalReason = (blank.blankState.code.equals(DipBlankStateCodes.DISPOSAL)) ?
                    blank.disposalReason.shortTitle :
                    disposalReasonForIssuedBlank(blank)
            rows.add([
                    blank.blankType.title,
                    blank.seria,
                    blank.number,
                    disposalReason,
                    blank.blankState.code.equals(DipBlankStateCodes.ISSUED)? "" : destructionDateStr
            ] as String[])
        }

        tm.put("T", rows as String[][])

        final rector = TopOrgUnit.instance.head
        im.put('fioRector', rector != null ? rector.employee.person.identityCard.fio : '')
        im.put('dataForm', 'от ' + RussianDateFormatUtils.getDateFormattedWithMonthName(new Date()) + ' г.')

        RtfDocument document = new RtfReader().read(template);
        im.modify(document)
        tm.modify(document)

        return [document: RtfUtil.toByteArray(document), fileName: 'disposalAct.rtf']
    }
    def disposalReasonForIssuedBlank(DipDiplomaBlank blank)
    {
        if (!blanksToExtracts.containsKey(blank))
            return "Приказ отсутствует в системе."
        final extract = blanksToExtracts.get(blank)
        final name = declinatedName(extract.entity.person.identityCard)
        final order = extract.paragraph.order
        return "Выдан ${name} (пр. № ${order.number} от ${DateFormatter.DEFAULT_DATE_FORMATTER.format(order.commitDate)})"
    }

    static def declinatedName(IdentityCard identityCard)
    {
        final lastName = PersonManager.instance().declinationDao().getDeclinationLastName(identityCard.lastName, GrammaCase.DATIVE, identityCard.sex.isMale())
        return identityCard.middleName == null ?
                "${lastName} ${identityCard.firstName.charAt(0)}." :
                "${lastName} ${identityCard.firstName.charAt(0)}. ${identityCard.middleName.charAt(0)}.";
    }
}
