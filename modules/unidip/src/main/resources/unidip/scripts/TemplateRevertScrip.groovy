package unidip.scripts

import org.hibernate.Session
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader

/**
 * @author Andrey Avetisov
 * @since 20.05.2014
 */

return new DefaultRevertScript(                          // стандартные входные параметры скрипта
        session: session,                                 // сессия
        template: template,                               // шаблон
).print()

class DefaultRevertScript
{
    Session session
    byte[] template

    def print()
    {
        RtfDocument document = new RtfReader().read(template);
        return [document: document, fileName: 'document.rtf']
    }

}