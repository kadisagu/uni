/* $Id:$ */
package ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.util;

import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.ui.Add.DipBlankRowWrapper;

import java.util.Collection;
import java.util.LinkedHashMap;

/**
 * @author rsizonenko
 * @since 30.12.2014
 */
public class BlankWrapperListEditor
{
    private final LinkedHashMap<Long, DipBlankRowWrapper> _rowWrapperMap = new LinkedHashMap<>();
    private DipBlankRowWrapper _currentRow;

    private Long _editedRowId;
    private DipBlankRowWrapper _backupRow;
    private boolean _newRow;

    public void addRow(OrgUnit ou)
    {
        _editedRowId = generateNextId();

        DipBlankRowWrapper wrapper = new DipBlankRowWrapper(_editedRowId);
        if (null != ou)
            wrapper.setStorageLocation(ou);

        _rowWrapperMap.put(_editedRowId, wrapper);
        _newRow = true;
    }

    public void saveRow()
    {
        DipBlankRowWrapper editedRow = _rowWrapperMap.get(_editedRowId);
        if (editedRow.getFirstNumber().compareTo(editedRow.getLastNumber()) > 0)
        {
            ContextLocal.getErrorCollector().add("Номер первого бланка должен быть не больше номера последнего.", "firstNumber", "lastNumber");
            return;
        }

        _editedRowId = null;
        _newRow = false;
    }

    public void editRow(Long id)
    {
        _editedRowId = id;
        _backupRow = new DipBlankRowWrapper(generateNextId());
        _backupRow.update(_rowWrapperMap.get(_editedRowId));
    }

    public void cancelEdit()
    {
        if (_newRow)
            _rowWrapperMap.remove(_editedRowId);
        else
            _rowWrapperMap.get(_editedRowId).update(_backupRow);

        _editedRowId = null;
        _newRow = false;
    }

    public void deleteRow(Long id)
    {
        _rowWrapperMap.remove(id);
    }

    public DipBlankRowWrapper getCurrentRow() { return _currentRow; }
    public void setCurrentRow(DipBlankRowWrapper currentRow) { _currentRow = currentRow; }

    public Collection<DipBlankRowWrapper> getRows() { return _rowWrapperMap.values(); }

    public boolean isEditMode() { return _editedRowId != null; }

    public boolean isCurrentRowInEditMode()
    {
        return isEditMode() && _editedRowId.equals(getCurrentRow().getId());
    }

    private Long _nextId = 1L;
    private long generateNextId() { return _nextId++; }
}
