/* $Id$ */
package ru.tandemservice.unidip.bso.bo.DiplomaBlankReport.ui.Add;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.DipDiplomaBlankManager;
import ru.tandemservice.unidip.bso.bo.DiplomaBlankReport.DiplomaBlankReportManager;

/**
 * @author azhebko
 * @since 22.01.2015
 */
@Configuration
public class DiplomaBlankReportAdd extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(selectDS("formByDS", DiplomaBlankReportManager.instance().reportFormByDSHandler()))
            .addDataSource(DipDiplomaBlankManager.instance().blankTypeDataSourceConfig())
            .addDataSource(DipDiplomaBlankManager.instance().storageLocationDataSourceConfig())
            .create();
    }
}