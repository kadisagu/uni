/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unidip.base.bo.DiplomaTemplate.ui.Line;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.PageableSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.info.InfoCollector;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unidip.base.bo.DiplomaTemplate.DiplomaTemplateManager;
import ru.tandemservice.unidip.base.bo.DiplomaTemplate.logic.EppRegistryTemplateRowHandler;
import ru.tandemservice.unidip.base.bo.DiplomaTemplate.ui.LineAddEdit.DiplomaTemplateLineAddEdit;
import ru.tandemservice.unidip.base.bo.DiplomaTemplate.ui.LineAddEdit.DiplomaTemplateLineAddEditUI;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaOptDisciplineRow;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaQualifWorkRow;
import ru.tandemservice.unidip.base.entity.eduplan.DiplomaTemplate;
import ru.tandemservice.unidip.settings.bo.DipSettings.logic.DipSettingsDao;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author iolshvang
 * @since 06.08.11 15:40
 */
@State({
               @Bind(key = UIPresenter.PUBLISHER_ID, binding = "diplomaTemplate.id", required = true)
       })
public class DiplomaTemplateLineUI extends UIPresenter
{
    private DiplomaTemplate _diplomaTemplate = new DiplomaTemplate();
    private boolean _hasQualifWork;
    private Multimap<Class<? extends DiplomaContentRow>, DiplomaContentRow> _rowsMap;
    private Map<Class<? extends DiplomaContentRow>, DiplomaContentRow.DiplomaRowConfig> _configMap;
    private Class<? extends DiplomaContentRow> _currentClass;

    @Override
    public void onComponentRefresh()
    {
        _configMap = new LinkedHashMap<>(DiplomaContentRow.DIPLOMA_BLOCK_CLASSES.get());

        // Факультативные дисциплины в шаблоне не добавляются, если выключена настройка
        if(!DipSettingsDao.isDipTemplateCollectOptionalDiscipline()) {
            _configMap.remove(DiplomaOptDisciplineRow.class);
        }

        _diplomaTemplate = DataAccessServices.dao().getNotNull(_diplomaTemplate.getId());
        _hasQualifWork = IUniBaseDao.instance.get().existsEntity(DiplomaQualifWorkRow.class, DiplomaQualifWorkRow.L_OWNER, getDiplomaTemplate().getContent());
        updateDS();
    }

    private void updateDS()
    {
        // Загружаем строки диплома, раскладываем по блокам. Строки сортируем по номеру.
        _rowsMap = ArrayListMultimap.create();
        final List<DiplomaContentRow> rowList = IUniBaseDao.instance.get().getList(DiplomaContentRow.class, DiplomaContentRow.owner(), getDiplomaTemplate().getContent(), DiplomaContentRow.P_NUMBER);
        boolean includeSelectedOrOptionalDisciplines = DipSettingsDao.isDipTemplateCollectOptionalDiscipline();
        for (DiplomaContentRow row : rowList)
        {
            if(!includeSelectedOrOptionalDisciplines && (row instanceof DiplomaOptDisciplineRow)) {
                continue;
            }
            _rowsMap.put(row.getClass(), row);
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (DiplomaTemplateLine.THE_OTHERS_DS.equals(dataSource.getName())) {
            dataSource.put(EppRegistryTemplateRowHandler.DIPLOMA_TEMPLATE, _diplomaTemplate);
        }
        else if (getCurrentClass() != null) {
            dataSource.put(UIDefines.COMBO_OBJECT_LIST, _rowsMap.get(getCurrentClass()));
        }
    }

    public String getLoadColumnTitle()
    {
        return _configMap.get(getCurrentClass()).getLoadColumnTitle(!getDiplomaTemplate().getContent().isLoadInWeeks());
    }

    // Getters & Setters

    public boolean isCurrentAddRowDisabled()
    {
        return getCurrentClass() == DiplomaQualifWorkRow.class && _hasQualifWork;
    }

    public Collection<Class<? extends DiplomaContentRow>> getRowClasses()
    {
        return _configMap.keySet();
    }

    public String getTableTitle()
    {
        return _configMap.get(getCurrentClass()).blockTitle;
    }

    public Class<? extends DiplomaContentRow> getCurrentClass()
    {
        return _currentClass;
    }

    public void setCurrentClass(Class<? extends DiplomaContentRow> currentClass)
    {
        _currentClass = currentClass;
    }

    public DynamicListDataSource getCurrentDS()
    {
        return ((PageableSearchListDataSource) getConfig().getDataSource(getCurrentClass().getSimpleName())).getLegacyDataSource();
    }

    public DiplomaTemplate getDiplomaTemplate()
    {
        return _diplomaTemplate;
    }

    public void setDiplomaTemplate(DiplomaTemplate diplomaTemplate)
    {
        _diplomaTemplate = diplomaTemplate;
    }

    // Listeners

    public void onClickUpdateLine()
    {
        InfoCollector infoCollector = ContextLocal.getInfoCollector();
        DiplomaTemplateManager.instance().dao().createDiplomaContentRows(_diplomaTemplate, infoCollector);
        updateDS();
    }

    public void onClickUp()
    {
        DiplomaTemplateManager.instance().dao().doChangePriorityUp(getListenerParameterAsLong());
        updateDS();
    }

    public void onClickDown()
    {
        DiplomaTemplateManager.instance().dao().doChangePriorityDown(getListenerParameterAsLong());
        updateDS();
    }

    public void onClickAddLine()
    {
        _uiActivation.asRegionDialog(DiplomaTemplateLineAddEdit.class)
                .parameter(DiplomaTemplateLineAddEditUI.DIP_TEMPLATE_ID, getDiplomaTemplate().getId())
                .parameter(DiplomaTemplateLineAddEditUI.ROW_CLASS_BIND, getListenerParameter())
                .activate();
    }

    public void onClickEditRow()
    {
        _uiActivation.asRegionDialog(DiplomaTemplateLineAddEdit.class)
                .parameter(DiplomaTemplateLineAddEditUI.DIP_TEMPLATE_ID, getDiplomaTemplate().getId())
                .parameter(DiplomaTemplateLineAddEditUI.DIP_DOCUMENT_LINE_ID, getListenerParameterAsLong())
                .activate();
    }

    public void onDeleteEntityFromList()
    {
        DiplomaTemplateManager.instance().dao().deleteRow(getListenerParameterAsLong());
        updateDS();
    }
}
