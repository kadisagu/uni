/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.     .
package ru.tandemservice.unidip.base.bo.DipDocument.ui.GlobalList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.unidip.base.bo.DipDocument.logic.DipDocumentStudentDSHandler;
import ru.tandemservice.unidip.base.bo.DipDocument.logic.DipDocumentTypeSelectModel;
import ru.tandemservice.unidip.base.bo.DipDocument.ui.AddEdit.DipDocumentAddEdit;
import ru.tandemservice.unidip.base.bo.DipDocument.ui.AddEdit.DipDocumentAddEditUI;
import ru.tandemservice.unidip.base.bo.DipDocument.ui.Print.DipDocumentPrint;
import ru.tandemservice.unidip.base.bo.DipDocument.ui.Print.DipDocumentPrintUI;
import ru.tandemservice.unidip.base.bo.DiplomaIssuance.ui.AddEdit.DiplomaIssuanceAddEdit;
import ru.tandemservice.unidip.base.bo.DiplomaIssuance.ui.AddEdit.DiplomaIssuanceAddEditUI;

import java.util.Map;

/**
 * @author iolshvang
 * @since 90.09.11 18:39
 */
public class DipDocumentGlobalListUI extends UIPresenter
{
    private ISingleSelectModel _selectModel;

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (DipDocumentGlobalList.DIP_DOCUMENT_DS.equals(dataSource.getName()))
        {
            Map<String, Object> settingMap = _uiSettings.getAsMap(
                    DipDocumentStudentDSHandler.LAST_NAME,
                    DipDocumentStudentDSHandler.FIRST_NAME,
                    DipDocumentStudentDSHandler.REG_NUMBER,
                    DipDocumentStudentDSHandler.ISSUE_DATE_FROM,
                    DipDocumentStudentDSHandler.ISSUE_DATE_TO,
                    DipDocumentStudentDSHandler.DOCUMENT_TYPE,
                    DipDocumentStudentDSHandler.WITH_SUCCESS,
                    DipDocumentStudentDSHandler.BLANK_SERIA,
                    DipDocumentStudentDSHandler.BLANK_NUMBER,
                    DipDocumentStudentDSHandler.ORG_UNIT,
                    DipDocumentStudentDSHandler.PROGRAM_SUBJECT
            );
            dataSource.putAll(settingMap);
        }
    }

    @Override
    public void onComponentRefresh()
    {
        _selectModel = new DipDocumentTypeSelectModel(null, true);
    }

    //Getters & setters


    public ISingleSelectModel getSelectModel()
    {
        return _selectModel;
    }

    public void setSelectModel(ISingleSelectModel selectModel)
    {
        _selectModel = selectModel;
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asRegionDialog(DipDocumentAddEdit.class).parameter(DipDocumentAddEditUI.DIP_DOCUMENT_ID, getListenerParameterAsLong()).activate();
    }

    public void onAddIssunce()
    {
        _uiActivation.asRegionDialog(DiplomaIssuanceAddEdit.class)
                .parameter(DiplomaIssuanceAddEditUI.DIP_DOCUMENT_ID, getListenerParameterAsLong())
                .activate();
    }

    public void onPrintFromGlobalList()
    {
        _uiActivation.asRegionDialog(DipDocumentPrint.class)
                .parameter(DipDocumentPrintUI.DIP_DOCUMENT_ID, getListenerParameterAsLong())
                .activate();
    }
}
