/* $Id$ */
package ru.tandemservice.unidip.base.bo.DipDocument;

import ru.tandemservice.unidip.base.bo.DipDocument.logic.IAggMethodDao;

/**
 * @author Vasily Zhukov
 * @since 05.05.2011
 */
public interface IAggMethodManager
{
    IAggMethodDao aggMethodDao();
}
