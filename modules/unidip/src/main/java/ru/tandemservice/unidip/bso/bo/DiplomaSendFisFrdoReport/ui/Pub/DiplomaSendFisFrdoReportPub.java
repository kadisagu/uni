/* $Id$ */
package ru.tandemservice.unidip.bso.bo.DiplomaSendFisFrdoReport.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author Alexey Lopatin
 * @since 01.04.2015
 */
@Configuration
public class DiplomaSendFisFrdoReportPub extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .create();
    }
}
