/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unidip.base.bo.DipDocument.logic;

import java.util.List;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.hibsupport.DataAccessServices;

import ru.tandemservice.unidip.base.entity.eduplan.DiplomaTemplate;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;

/**
 * @author iolshvang
 * @since 12.09.11 13:29
 */
public class DiplomaTemplateDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String EDUPLAN_VERSION = "eduPlanVersion";

    public DiplomaTemplateDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        EppEduPlanVersion epv = context.get(EDUPLAN_VERSION);
        List<DiplomaTemplate> diplomaTemplateList = DataAccessServices.dao().getList(DiplomaTemplate.class, DiplomaTemplate.eduPlanVersion(), epv);
        return ListOutputBuilder.get(input, diplomaTemplateList).build();
    }
}
