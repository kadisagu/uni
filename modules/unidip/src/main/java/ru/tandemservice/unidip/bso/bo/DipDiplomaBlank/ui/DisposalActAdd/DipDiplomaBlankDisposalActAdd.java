/* $Id$ */
package ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.ui.DisposalActAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.DipDiplomaBlankManager;
import ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank;

import java.util.List;

/**
 * @author azhebko
 * @since 21.01.2015
 */
@Configuration
public class DipDiplomaBlankDisposalActAdd extends BusinessComponentManager
{
    public static final String BLANKS_DS = "blanksDS";
	public static final String DELETE_ALERT = BLANKS_DS + ".delete.alert";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(searchListDS(BLANKS_DS, blanksDSColumns(), blanksDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint blanksDSColumns()
    {
        return columnListExtPointBuilder(BLANKS_DS)
                .addColumn(textColumn(DipDiplomaBlank.L_BLANK_TYPE, DipDiplomaBlank.blankType().shortTitle()))
				.addColumn(textColumn(DipDiplomaBlank.P_SERIA, DipDiplomaBlank.seria()))
				.addColumn(textColumn(DipDiplomaBlank.P_NUMBER, DipDiplomaBlank.number()))
				.addColumn(textColumn(DipDiplomaBlank.L_BLANK_STATE, DipDiplomaBlank.blankState().title()))
				.addColumn(textColumn(DipDiplomaBlank.L_DISPOSAL_REASON, DipDiplomaBlank.disposalReason().shortTitle()))
				.addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, "onClickDelete", alert(DELETE_ALERT)))
				.create();
    }

	@Bean
	public IBusinessHandler<DSInput, DSOutput> blanksDSHandler()
	{
		return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(getName(), DipDiplomaBlank.class)
		{
			@Override
			protected DSOutput execute(DSInput input, ExecutionContext context)
			{
				List<Long> blankIds = context.get(DipDiplomaBlankDisposalActAddUI.PARAM_BLANK_IDS);
				boolean hideIssued = context.getBoolean(DipDiplomaBlankDisposalActAddUI.PARAM_HIDE_ISSUED, true);
				List<DipDiplomaBlank> blanks = DipDiplomaBlankManager.instance().diplomaBlankDao().getShownBlanks(blankIds, hideIssued);
				return ListOutputBuilder.get(input, blanks).pageable(true).build();
			}
		};
	}
}