package ru.tandemservice.unidip.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import ru.tandemservice.unidip.base.entity.catalog.codes.DipDocumentTypeCodes;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unidip_2x6x0_0to1 extends IndependentMigrationScript implements DipDocumentTypeCodes
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {

        ////////////////////////////////////////////////////////////////////////////////
        // сущность dipDocTemplateCatalog

        // создана новая сущность
        if (!tool.tableExists("dip_doc_template_catalog"))
        {
            // создать таблицу
            DBTable dbt = new DBTable("dip_doc_template_catalog",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("withsuccess_p", DBType.BOOLEAN).setNullable(false),
                                      new DBColumn("dipdocumenttype_id", DBType.LONG)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("dipDocTemplateCatalog");

            //копируем данные в dip_doc_template_catalog и scriptItem из dip_doc_template

            tool.table("SCRIPTITEM_T").constraints().clear();
            PreparedStatement insertInCatalog = tool.prepareStatement("insert into dip_doc_template_catalog (id, withsuccess_p, dipdocumenttype_id) values (?, ?, ?)");
            PreparedStatement selectDipTypeID = tool.prepareStatement("select id from DIP_C_TYPE where dipdoctemplate_id=?");
            PreparedStatement insertScript = tool.prepareStatement("insert into SCRIPTITEM_T (id, discriminator ,code_p, catalogcode_p, templatepath_p, scriptpath_p, title_p, usertemplate_p, usertemplateeditdate_p, usertemplatecomment_p) values (?, ?, ?, ?, ?, ?, ?,?, ?, ?)");
            PreparedStatement stmtToTemplate = tool.prepareStatement("select code_p, title_p, path_p, document_p, editdate_p, comment_p, id from dip_doc_template");
            stmtToTemplate.execute();
            ResultSet res = stmtToTemplate.getResultSet();
            while (res.next())
            {
                Long IdItem = EntityIDGenerator.generateNewId(entityCode);
                insertScript.setLong(1, IdItem); //id
                insertScript.setShort(2, entityCode); //discriminator

                insertScript.setObject(3, res.getString(1)); //Code_P to Code_P
                insertScript.setString(4, "unidipScriptItem"); //catalogcode_p

                String templatePath = res.getString(3);
                insertScript.setObject(5, templatePath); //templatepath_p from dip_doc_template.path_p

                if (templatePath.equals("unidip/templates/diploma.rtf"))
                    insertScript.setObject(6, "unidip/scripts/TypeDiplomaScript.groovy"); //scriptpath_p

                else if (templatePath.equals("unidip/templates/acadcert.rtf"))
                    insertScript.setObject(6, "unidip/scripts/TypeAcadCertScript.groovy"); //scriptpath_p

                else if (templatePath.equals("unidip/templates/certificate.rtf"))
                    insertScript.setObject(6, "unidip/scripts/TypeCertScript.groovy"); //scriptpath_p

                else
                    insertScript.setObject(6, "unidip/scripts/TemplateRevertScrip.groovy"); //scriptpath_p


                insertScript.setObject(7, res.getString(2)); //title_p to title_p
                insertScript.setObject(8, res.getObject(4)); //document_p to usertemplate_p
                insertScript.setObject(9, res.getObject(5)); //editdate_p to usertemplateeditdate_p
                insertScript.setObject(10, res.getObject(6)); //comment_p to usertemplatecomment_p
                insertScript.execute();

                insertInCatalog.setLong(1, IdItem); //id
                insertInCatalog.setObject(2, false); //withsuccess_p
                selectDipTypeID.setLong(1, res.getLong(7));
                selectDipTypeID.execute();
                if (selectDipTypeID.getResultSet().next())
                {
                    insertInCatalog.setObject(3, selectDipTypeID.getResultSet().getLong(1)); // в поле dipdocumenttype_id копируем id старого dip_doc_template
                }
                insertInCatalog.execute();

            }

        }


        ////////////////////////////////////////////////////////////////////////////////
        // сущность dipDocumentType

        // удалено свойство businessObjectName
        if (tool.columnExists("dip_c_type", "businessobjectname_p"))
        {
            // удалить колонку
            tool.dropColumn("dip_c_type", "businessobjectname_p");
        }

        // удалено свойство dipDocTemplateCatalog
        if (tool.columnExists("dip_c_type", "dipdoctemplate_id"))
        {
            // удалить колонку
            tool.dropColumn("dip_c_type", "dipdoctemplate_id");
        }


        // создано обязательное свойство childAllowed
        if (!tool.columnExists("dip_c_type", "childallowed_p"))
        {
            // создать колонку
            tool.createColumn("dip_c_type", new DBColumn("childallowed_p", DBType.BOOLEAN));

            // задать значение по умолчанию
            java.lang.Boolean defaultChildAllowed = false;
            tool.executeUpdate("update dip_c_type set childallowed_p=? where childallowed_p is null", defaultChildAllowed);

            // сделать колонку NOT NULL
            tool.setColumnNullable("dip_c_type", "childallowed_p", false);
        }


        // создано обязательное свойство formingDocAllowed
        if (!tool.columnExists("dip_c_type", "formingdocallowed_p"))
        {
            // создать колонку
            tool.createColumn("dip_c_type", new DBColumn("formingdocallowed_p", DBType.BOOLEAN));

            // задать значение по умолчанию
            java.lang.Boolean defaultFormingDocAllowed = true;
            tool.executeUpdate("update dip_c_type set formingdocallowed_p=? where formingdocallowed_p is null", defaultFormingDocAllowed);

            // сделать колонку NOT NULL
            tool.setColumnNullable("dip_c_type", "formingdocallowed_p", false);
        }

        PreparedStatement updateNames = tool.prepareStatement(
                "update dip_c_type set title_p=" + tool.createStringConcatenationSQL("title_p", "' (иной)'") +
                        " where title_p in ('" +
                        CommonBaseStringUtil.joinWithSeparator(
                                "', '",
                                "Справка об обучении",
                                "Диплом о среднем профессиональном образовании",
                                "Диплом бакалавра",
                                "Диплом специалиста",
                                "Диплом магистра",
                                "Диплом об окончании аспирантуры",
                                "Диплом об окончании адъюнктуры",
                                "Удостоверение о повышении квалификации",
                                "Диплом о профессиональной переподготовке",
                                "Иной документ"
                        ) + "')"
        );
        updateNames.executeUpdate();


        //Иной документ
        short entityCode = tool.entityCodes().ensure("dipDocumentType");
        Long othersItemId = EntityIDGenerator.generateNewId(entityCode);

        PreparedStatement insert = tool.prepareStatement("insert into dip_c_type (id, discriminator, code_p, parent_id, shorttitle_p, statedoctype_p, title_p, childAllowed_p, formingDocAllowed_p) values (?, ?, ?, null, ?, ?, ?, ?,?)");
        PreparedStatement isOther = tool.prepareStatement("select* from dip_c_type where code_p=?", OTHER);
        isOther.execute();
        ResultSet res = isOther.getResultSet();
        if (!res.next())
        {
            insert.setLong(1, othersItemId); //id
            insert.setShort(2, entityCode); //discriminator
            insert.setString(3, OTHER); //Code_P
            insert.setString(4, "1"); //shorttitle_p
            insert.setBoolean(5, false); //statedoctype_p
            insert.setString(6, "1"); //title_p
            insert.setBoolean(7, true); //childAllowed_p
            insert.setBoolean(8, false); //formingDocAllowed_p
            insert.execute();
        }
        else
        {
            othersItemId = res.getLong(1);
        }
        tool.executeUpdate("update dip_c_type set parent_id=?, statedoctype_p=?, childAllowed_p=? where id<>?", othersItemId, false, true, othersItemId);

        ////////////////////////////////////////////////////////////////////////////////
        // сущность dipDocTemplate

        // если сущность не была удалена
        if (tool.tableExists("dip_doc_template"))
        {
            //удалить таблицу
            tool.dropTable("dip_doc_template", false /* - не удалять, если есть ссылающиеся таблицы */);
            // удалить код сущности
            tool.entityCodes().delete("dipDocTemplate");
        }

    }
}