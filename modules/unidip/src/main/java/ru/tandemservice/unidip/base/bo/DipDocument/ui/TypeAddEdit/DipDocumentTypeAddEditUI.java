/* $Id$ */
package ru.tandemservice.unidip.base.bo.DipDocument.ui.TypeAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.CatalogManager;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.crud.BaseCatalogItemAddEditUI;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unidip.base.entity.catalog.DipDocumentType;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 16.04.2015
 */
@Input({
        @Bind(key = "catalogItemId", binding = "holder.id"),
        @Bind(key = DefaultCatalogAddEditModel.CATALOG_CODE, binding = "catalogCode")
})
public class DipDocumentTypeAddEditUI extends BaseCatalogItemAddEditUI<DipDocumentType>
{
    private List<DipDocumentType> _parentList;

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();

        _parentList = IUniBaseDao.instance.get().getList(
                new DQLSelectBuilder().fromEntity(DipDocumentType.class, "c")
                        .column(property("c"))
                        .where(isNull(property(DipDocumentType.parent().fromAlias("c"))))
                        .where(eq(property(DipDocumentType.childAllowed().fromAlias("c")), value(true)))
                        .order(property("c", DipDocumentType.title()))
        );
    }

    public boolean isParentDisabled()
    {
        return !isAddForm() && !CatalogManager.instance().modifyDao().isCatalogItemPropertyEditable(getCatalogItem().getId(), DipDocumentType.L_PARENT);
    }

    @Override
    public String getAdditionalPropertiesPageName()
    {
        return this.getClass().getPackage().getName() + ".DipDocumentTypeAdditProps";
    }

    public List<DipDocumentType> getParentList()
    {
        return _parentList;
    }

    @Override
    public void onClickApply()
    {
        getCatalogItem().setFormingDocAllowed(true);
        getCatalogItem().setChildAllowed(false);
        getCatalogItem().setStateDocType(getCatalogItem().getParent().isStateDocType());

        super.onClickApply();
    }
}