package ru.tandemservice.unidip.base.bo.DipDocument.ui.AddEdit.logic;

import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * Хэндлер для селекта направленности ВПО {@link EduProgramSpecialization}. Входные параметры:
 * <ul>
 *     <li/> Версия УП - если указан, то выводятся только направленности, для которых есть блок направленности ({@link EppEduPlanVersionSpecializationBlock}) из данной УПв.
 *     <li/> Направление подготовки проф. образования - если указано, то выводятся только направленности этого направления.
 * </ul>
 * @author avedernikov
 * @since 14.03.2017
 */
public class EduProgramSpecializationDSHandler extends EntityComboDataSourceHandler
{
	public static final String FILTER_EDUPLAN_VERSION = "eduPlanVersion";
	public static final String FILTER_PROGRAM_SUBJECT = "programSubject";

	public EduProgramSpecializationDSHandler(String ownerId)
	{
		super(ownerId, EduProgramSpecialization.class);
	}

	@Override
	protected void applyWhereConditions(final String alias, final DQLSelectBuilder dql, final ExecutionContext context)
	{
		super.applyWhereConditions(alias, dql, context);

		EppEduPlanVersion eduPlanVersion = context.get(FILTER_EDUPLAN_VERSION);
		EduProgramSubject programSubject = context.get(FILTER_PROGRAM_SUBJECT);
		if (eduPlanVersion != null)
		{
			final String epvSpecBlockAlias = "epvSpecBloc";
			DQLSelectBuilder epvSpecBlocDql = new DQLSelectBuilder()
					.fromEntity(EppEduPlanVersionSpecializationBlock.class, epvSpecBlockAlias)
					.where(eq(property(epvSpecBlockAlias, EppEduPlanVersionSpecializationBlock.eduPlanVersion()), value(eduPlanVersion)))
					.where(eq(property(epvSpecBlockAlias, EppEduPlanVersionSpecializationBlock.programSpecialization()), property(alias)));
			dql.where(exists(epvSpecBlocDql.buildQuery()));
		}
		if (programSubject != null)
			dql.where(eq(property(alias, EduProgramSpecialization.programSubject()), value(programSubject)));
	}

	@Override
	protected boolean isExternalSortRequired()
	{
		return true;
	}

	@Override
	protected List<IEntity> sortSelectedValues(List<IEntity> list, Set primaryKeys)
	{
		Collections.sort((List) list);
		return list;
	}
}
