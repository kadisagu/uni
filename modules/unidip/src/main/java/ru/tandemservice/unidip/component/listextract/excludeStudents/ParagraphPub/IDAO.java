/* $Id$ */
package ru.tandemservice.unidip.component.listextract.excludeStudents.ParagraphPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.IAbstractListParagraphPubDAO;
import ru.tandemservice.unidip.base.entity.order.DipStuExcludeExtract;

/**
 * @author Dmitry Seleznev
 * @since 08.06.2011
 */
public interface IDAO extends IAbstractListParagraphPubDAO<DipStuExcludeExtract, Model>
{
}