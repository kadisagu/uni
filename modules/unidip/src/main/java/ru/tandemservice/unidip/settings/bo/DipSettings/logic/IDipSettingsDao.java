/* $Id$ */
package ru.tandemservice.unidip.settings.bo.DipSettings.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaObject;
import ru.tandemservice.unidip.settings.entity.DipFormingRegNumberAlgorithm;
import ru.tandemservice.unidip.settings.entity.DipFormingRowAlgorithm;

/**
 * @author Alexey Lopatin
 * @since 27.05.2015
 */
public interface IDipSettingsDao extends INeedPersistenceSupport
{
    /**
     * Параметр дипломирования "Формировать строки для ДВ(из УПв) и ФТД(из УПв) в шаблоне диплома"
     */
    String DIP_TEMPLATE_COLLECT_OPTIONAL_DISCIPLINE = "DipTemplateCollectOptionalDiscipline";

    /**
     * Метод создает алгоритм "По умолчанию" для тех выпуск. подразделений, у которых его еще нет.
     */
    void createAlgorithm4OwnerOrgUnit();

    /**
     * Метод устанавливает флаг currentAlg для переданного объекта в значение true,
     * для всех остальных это поле становится false.
     *
     * @param algorithm алгоритм пформирования строк в документе об обучении
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void setCurrentRowAlgorithm(DipFormingRowAlgorithm algorithm);

    /**
     * Метод устанавливает флаг current для переданного объекта в значение true,
     * для всех остальных это поле становится false.
     *
     * @param algorithm алгоритм формирования регистрационного номера
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void setCurrentRegNumberAlgorithm(DipFormingRegNumberAlgorithm algorithm);

    /**
     * Возвращает номер диплома по текущему алгоритму из настройки «Параметры дипломирования».
     *
     * @param dipObject диплом
     * @return если алгоритм задан, то возвращает рег. номер диплома, иначе - null
     */
    String getDipIssuanceNextRegNumber(DiplomaObject dipObject);

    /**
     * Формируется регистрационный номер в рамках года.
     * Формат номера «[Г]-[Н]», где Г - год в формате «ГГГГ» из текущей даты, Н - порядковый четырехзначный номер факта выдачи в рамках года.
     * Если в рамках года нет ни одного номера, то выводим НННН = 0001
     *
     * @param dipObject диплом
     * @return номер в формате «ГГГГ-НННН»
     */
    String getDipIssuanceNextRegNumberWithinEduYear(DiplomaObject dipObject);
}
