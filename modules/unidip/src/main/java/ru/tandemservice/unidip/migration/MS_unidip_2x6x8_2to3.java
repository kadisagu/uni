package ru.tandemservice.unidip.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unidip_2x6x8_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.15"),
                        new ScriptDependency("org.tandemframework.shared", "1.6.8")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность dipAdditionalInformation

        // создано обязательное свойство demand
        {
            // создать колонку
            if (!tool.columnExists("dip_additional_information_t", "demand_p"))
            {
                tool.createColumn("dip_additional_information_t", new DBColumn("demand_p", DBType.BOOLEAN));


                // задать значение по умолчанию
                java.lang.Boolean defaultDemand = false;
                tool.executeUpdate("update dip_additional_information_t set demand_p=? where demand_p is null", defaultDemand);

                // сделать колонку NOT NULL
                tool.setColumnNullable("dip_additional_information_t", "demand_p", false);
            }
        }


    }
}