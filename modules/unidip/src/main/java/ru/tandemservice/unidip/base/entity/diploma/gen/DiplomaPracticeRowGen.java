package ru.tandemservice.unidip.base.entity.diploma.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaPracticeRow;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Строка блока практик в документе об обучении
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DiplomaPracticeRowGen extends DiplomaContentRow
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unidip.base.entity.diploma.DiplomaPracticeRow";
    public static final String ENTITY_NAME = "diplomaPracticeRow";
    public static final int VERSION_HASH = -944872614;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof DiplomaPracticeRowGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DiplomaPracticeRowGen> extends DiplomaContentRow.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DiplomaPracticeRow.class;
        }

        public T newInstance()
        {
            return (T) new DiplomaPracticeRow();
        }
    }
    private static final Path<DiplomaPracticeRow> _dslPath = new Path<DiplomaPracticeRow>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DiplomaPracticeRow");
    }
            

    public static class Path<E extends DiplomaPracticeRow> extends DiplomaContentRow.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return DiplomaPracticeRow.class;
        }

        public String getEntityName()
        {
            return "diplomaPracticeRow";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
