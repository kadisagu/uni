/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unidip.base.bo.DiplomaTemplate.ui.LineAddEdit;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.dao.SharedBaseDao;
import ru.tandemservice.unidip.base.bo.DipDocument.logic.DipEpvRegistryRowDSHandler;
import ru.tandemservice.unidip.base.bo.DiplomaTemplate.DiplomaTemplateManager;
import ru.tandemservice.unidip.base.bo.DiplomaTemplate.logic.ControlActionComboBoxDSHandler;
import ru.tandemservice.unidip.base.bo.DiplomaTemplate.logic.RegElemPartComboBoxDSHandler;
import ru.tandemservice.unidip.base.bo.DiplomaTemplate.logic.RegElemPartDSHandler;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRegElPartFControlAction;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow;
import ru.tandemservice.unidip.base.entity.eduplan.DiplomaTemplate;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.catalog.EppGeneration;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanHigherProf;
import ru.tandemservice.uniepp.entity.registry.EppRegistryAction;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author iolshvang
 * @since 08.08.11 13:04
 */
@Input({
               @Bind(key = DiplomaTemplateLineAddEditUI.DIP_TEMPLATE_ID, binding = "diplomaTemplate.id"),
               @Bind(key = DiplomaTemplateLineAddEditUI.ROW_CLASS_BIND, binding = "rowClass"),
               @Bind(key = DiplomaTemplateLineAddEditUI.DIP_DOCUMENT_LINE_ID, binding = "rowId")
       })
public class DiplomaTemplateLineAddEditUI extends UIPresenter
{
    public static final String DIP_TEMPLATE_ID = "diplomaTemplateId";
    public static final String DIP_DOCUMENT_LINE_ID = "dipDocumentLineId";
    public static final String ROW_CLASS_BIND = "rowClass";

    private DiplomaContentRow _row;
    private Long _rowId;
    private DiplomaTemplate _diplomaTemplate = new DiplomaTemplate();
    private boolean _editForm;

    private List<DataWrapper> _regElemWrappedList;
    private DataWrapper _currentEditRow;
    private IUIDataSource _regElemDS;
    private Class<? extends DiplomaContentRow> _rowClass;

    private EppRegistryElementPart _regElem;
    private EppRegistryElementPartFControlAction _partFControlAction;
    private EppRegistryElementPart _regElemOld;
    private EppRegistryElementPartFControlAction _partFControlActionOld;
    private Double _rowLoad;
    private Double _audRowLoad;

    private Long _id = 0L;

    public Long getNextId()
    {
        return _id++;
    }

    @Override
    public void onComponentRefresh()
    {
        setCurrentEditRow(null);
        setRegElemDS(getConfig().getDataSource(DiplomaTemplateLineAddEdit.REG_ELEM_PART_DS));
        _regElemWrappedList = new ArrayList<>();
        _diplomaTemplate = DataAccessServices.dao().getNotNull(DiplomaTemplate.class, DiplomaTemplate.id(), _diplomaTemplate.getId());

        _editForm = null != getRowId();
        if (isEditForm())
        {
            setRow(DataAccessServices.dao().getNotNull(DiplomaContentRow.class, DiplomaContentRow.id(), getRowId()));
            setRowClass(getRow().getClass());

            List<DiplomaContentRegElPartFControlAction> regElemPartList = DataAccessServices.dao().getList(
                    DiplomaContentRegElPartFControlAction.class, DiplomaContentRegElPartFControlAction.row(), getRow(),
                    DiplomaContentRegElPartFControlAction.registryElementPartFControlAction().part().registryElement().title().s(),
                    DiplomaContentRegElPartFControlAction.registryElementPartFControlAction().part().number().s()
            );

            for (DiplomaContentRegElPartFControlAction item : regElemPartList)
            {
                DataWrapper wrapper = new DataWrapper(item);
                wrapper.setId(getNextId());
                _regElemWrappedList.add(wrapper);
            }

        }
        else
        {
            _editForm = false;
            final DiplomaContentRow newRow;
            try
            {
                newRow = getRowClass().newInstance();
                newRow.setOwner(getDiplomaTemplate().getContent());
            }
            catch (InstantiationException | IllegalAccessException e)
            {
                throw CoreExceptionUtils.getRuntimeException(e);
            }
            setRow(newRow);
        }

        setRowLoad(getRow().getCommonLoad());
        setAudRowLoad(_row.getAudLoadAsDouble());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        switch (dataSource.getName())
        {
            case DiplomaTemplateLineAddEdit.REG_ELEM_PART_DS:
                dataSource.put(RegElemPartDSHandler.REG_ELEM_PART_LIST, _regElemWrappedList);
                break;
            case DiplomaTemplateLineAddEdit.REG_ELEM_PART_COMBO_BOX_DS:
                dataSource.put(RegElemPartComboBoxDSHandler.DIPLOMA_TEMPLATE, _diplomaTemplate);
                dataSource.put(RegElemPartComboBoxDSHandler.DIPLOMA_CONTENT_ROW, _row);
                if (!_regElemWrappedList.isEmpty())
                {
                    List<DataWrapper> wrappers = new ArrayList<>(_regElemWrappedList);
                    wrappers.remove(getCurrentRow());
                    dataSource.put(RegElemPartComboBoxDSHandler.REG_ELEM_WRAPPED_LIST, wrappers);
                }
                break;
            case DiplomaTemplateLineAddEdit.CONTROL_ACTION_COMBO_BOX_DS:
                dataSource.put(ControlActionComboBoxDSHandler.REGISTER_ELEMENT_PART, _regElem);
                dataSource.put(ControlActionComboBoxDSHandler.DIPLOMA_TEMPLATE, _diplomaTemplate);
                dataSource.put(ControlActionComboBoxDSHandler.DIPLOMA_CONTENT_ROW, _row);
                if (!_regElemWrappedList.isEmpty())
                {
                    List<DataWrapper> wrappers = new ArrayList<>(_regElemWrappedList);
                    wrappers.remove(getCurrentRow());
                    dataSource.put(ControlActionComboBoxDSHandler.REG_ELEM_WRAPPED_LIST, wrappers);
                }
                break;
            case DiplomaTemplateLineAddEdit.EPV_REGISTRY_ROW_DS:
                dataSource.put(DipEpvRegistryRowDSHandler.EDU_PLAN_VERSION, _diplomaTemplate.getEduPlanVersion().getId());
                dataSource.put(DipEpvRegistryRowDSHandler.REG_STRUCTURE_CODES, getRow().getRelatedRegistryElementTypeCodes());
                break;
        }
    }

    // Getters && Setters

    public EppRegistryElementPart getRegElem()
    {
        return _regElem;
    }

    public void setRegElem(EppRegistryElementPart regElem)
    {
        _regElem = regElem;
    }

    public EppRegistryElementPartFControlAction getPartFControlAction()
    {
        return _partFControlAction;
    }

    public void setPartFControlAction(EppRegistryElementPartFControlAction partFControlAction)
    {
        _partFControlAction = partFControlAction;
    }

    public DiplomaContentRow getRow()
    {
        return _row;
    }

    public void setRow(DiplomaContentRow row)
    {
        _row = row;
    }

    public DiplomaTemplate getDiplomaTemplate()
    {
        return _diplomaTemplate;
    }

    public void setDiplomaTemplate(DiplomaTemplate diplomaTemplate)
    {
        _diplomaTemplate = diplomaTemplate;
    }

    public boolean isEditForm()
    {
        return _editForm;
    }

    public boolean isAddApplicationButtonDisabled()
    {
        return getCurrentEditRow() != null;
    }

    public DataWrapper getCurrentEditRow()
    {
        return _currentEditRow;
    }

    public void setCurrentEditRow(DataWrapper currentEditRow)
    {
        _currentEditRow = currentEditRow;
    }

    public IUIDataSource getRegElemDS()
    {
        return _regElemDS;
    }

    public void setRegElemDS(IUIDataSource regElemDS)
    {
        _regElemDS = regElemDS;
    }

    public DataWrapper getCurrentRow()
    {
        return getRegElemDS().getCurrent();
    }

    public boolean isComponentInEditMode()
    {
        return null != getCurrentEditRow();
    }

    public boolean isCurrentRowInEditMode()
    {
        DataWrapper editRow = getCurrentEditRow();
        return !(null == editRow || null == getCurrentRow()) && _regElemWrappedList.indexOf(editRow) == _regElemWrappedList.indexOf(getCurrentRow());
    }

    public String getRegElemPartTitle()
    {
        String title = "";
        if (null != getCurrentRow())
            title += ((DiplomaContentRegElPartFControlAction) getCurrentRow().getWrapped()).getRegistryElementPartFControlAction().getPart().getTitle()
                    + " [" + ((DiplomaContentRegElPartFControlAction) getCurrentRow().getWrapped()).getRegistryElementPartFControlAction().getControlAction().getTitle() + "]";
        return title;
    }

    public EppEduPlan getEduPlan()
    {
        return getDiplomaTemplate().getEduPlanVersion().getEduPlan();
    }

    public String getRowLoadTitle()
    {
        return getRow().getRowLoadTitle();
    }

    public Double getRowLoad()
    {
        return _rowLoad;
    }

    public void setRowLoad(Double rowLoad)
    {
        _rowLoad = rowLoad;
    }

    public Double getAudRowLoad()
    {
        return _audRowLoad;
    }

    public void setAudRowLoad(Double audRowLoad)
    {
        _audRowLoad = audRowLoad;
    }

    public String getFormattedLoad()
    {
        final EppRegistryElementPart elementPart = ((DiplomaContentRegElPartFControlAction) getCurrentRow().getWrapped()).getRegistryElementPartFControlAction().getPart();
        if (elementPart.getRegistryElement() instanceof EppRegistryAction) {
            return UniEppUtils.formatLoad(elementPart.getWeeksAsDouble(), false) + " нед." + (isShowLabor() ? "/" + UniEppUtils.formatLoad(elementPart.getLaborAsDouble(), false) + " ЗЕТ" : "");
        } else {
            return UniEppUtils.formatLoad(elementPart.getSizeAsDouble(), false) + " ч." + (isShowLabor() ? "/" + UniEppUtils.formatLoad(elementPart.getLaborAsDouble(), false) + " ЗЕТ" : "");
        }
    }

    public boolean isShowLabor()
    {
        return getEduPlan() instanceof EppEduPlanHigherProf && EppGeneration.GENERATION_3.equals(getEduPlan().getGeneration());
    }

    //Listeners
    public void onClickAddRegPart()
    {
        _regElemOld = null;
        _partFControlActionOld = null;

        setRegElem(null);
        setPartFControlAction(null);
        DataWrapper regElemWrapper = new DataWrapper(new DiplomaContentRegElPartFControlAction());
        regElemWrapper.setId(getNextId());
        _regElemWrappedList.add(regElemWrapper);
        setCurrentEditRow(regElemWrapper);
    }

    public void onClickDeleteRowPart()
    {
        if (null != _currentEditRow && null != _regElemOld && null != _partFControlActionOld)
        {
            ((DiplomaContentRegElPartFControlAction) getCurrentEditRow().getWrapped()).getRegistryElementPartFControlAction().setPart(_regElemOld);
            ((DiplomaContentRegElPartFControlAction) getCurrentEditRow().getWrapped()).setRegistryElementPartFControlAction(_partFControlActionOld);
            setCurrentEditRow(null);
        }
        else if (null != _currentEditRow)
        {
            _regElemWrappedList.remove(_regElemWrappedList.size() - 1);
            setCurrentEditRow(null);
        }
        else
        {
            DataWrapper currentWrapper = getListenerParameter();
            _regElemWrappedList.remove(currentWrapper);
            DiplomaContentRegElPartFControlAction item = getCurrentRow().getWrapped();
            if (SharedBaseDao.instance.get().existsEntity(DiplomaContentRegElPartFControlAction.class, DiplomaContentRegElPartFControlAction.P_ID, item.getId()))
                _row.setRowContentUpdateDate(new Date());
            setCurrentEditRow(null);
        }

        _regElemOld = null;
        _partFControlActionOld = null;
    }

    public void onClickSaveRowPart()
    {
        DiplomaContentRegElPartFControlAction rel = _currentEditRow.getWrapped();
        _row.setRowContentUpdateDate(new Date());
        rel.setRow(_row);
        rel.setRegistryElementPartFControlAction(_partFControlAction);
        rel.setOwner(_diplomaTemplate.getContent());
        setCurrentEditRow(null);

        _regElemOld = null;
        _partFControlActionOld = null;
    }

    public void onClickEditRow()
    {
        DataWrapper currentWrapper = getListenerParameter();
        _regElem = ((DiplomaContentRegElPartFControlAction) currentWrapper.getWrapped()).getRegistryElementPartFControlAction().getPart();
        _partFControlAction = ((DiplomaContentRegElPartFControlAction) currentWrapper.getWrapped()).getRegistryElementPartFControlAction();
        _regElemOld = _regElem;
        _partFControlActionOld = _partFControlAction;
        setCurrentEditRow(currentWrapper);
    }

    public void onClickApply()
    {
        getRow().setCommonLoad(_rowLoad);
        _row.setAudLoadAsDouble(_audRowLoad);

        List<DiplomaContentRegElPartFControlAction> regElemPartList = new ArrayList<>();

        for (DataWrapper wrapper : _regElemWrappedList)
        {
            DiplomaContentRegElPartFControlAction item = wrapper.getWrapped();
            regElemPartList.add(item);
        }

        if (null == _row.getOwner())
            _row.setOwner(_diplomaTemplate.getContent());

        if (0 == _row.getContentListNumber())
            _row.setContentListNumber(1);

        if (0 == _row.getNumber())
        {
            int number = DiplomaTemplateManager.instance().dao().getDiplomaContentRowMaxNumber(_row.getOwner());
            _row.setNumber(number + 1);
        }
        DiplomaTemplateManager.instance().dao().saveOrUpdateDiplomaContentRow(regElemPartList, getRow());
        deactivate();
    }

    public void onChangeEpvRegistryRow()
    {
        if(!isEditForm() && _row.getEpvRegistryRow() != null)
            _row.setTitle(_row.getEpvRegistryRow().getTitle());
    }

    public Long getRowId()
    {
        return _rowId;
    }

    public void setRowId(Long rowId)
    {
        _rowId = rowId;
    }

    public Integer getTerm()
    {
        return getRow().getTerm() == 0 ? null : getRow().getTerm();
    }

    public void setTerm(Integer value)
    {
        getRow().setTerm(value == null ? 0 : value);
    }

    public Class<? extends DiplomaContentRow> getRowClass()
    {
        return _rowClass;
    }

    public void setRowClass(Class<? extends DiplomaContentRow> rowClass)
    {
        _rowClass = rowClass;
    }
}
