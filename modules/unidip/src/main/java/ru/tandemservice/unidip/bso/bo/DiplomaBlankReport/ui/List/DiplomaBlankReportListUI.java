/* $Id$ */
package ru.tandemservice.unidip.bso.bo.DiplomaBlankReport.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.BusinessComponentUtils;
import ru.tandemservice.uni.base.bo.UniReport.util.UniReportUtils;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unidip.bso.bo.DiplomaBlankReport.ui.Add.DiplomaBlankReportAdd;

/**
 * @author azhebko
 * @since 22.01.2015
 */
public class DiplomaBlankReportListUI extends UIPresenter
{
    public void onClickAddReport()
    {
        this.getActivationBuilder().asRegion(DiplomaBlankReportAdd.class).activate();
    }

    public void onClickPrint()
    {
        BusinessComponentUtils.downloadDocument(UniReportUtils.createRenderer(this.getListenerParameterAsLong()), true);
    }

    public void onDeleteEntityFromList()
    {
        IUniBaseDao.instance.get().delete(this.getListenerParameterAsLong());
    }
}