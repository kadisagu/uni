/* $Id$ */
package ru.tandemservice.unidip.base.bo.DipDocument.ui.AddInformationAddEdit;

import com.google.common.collect.ImmutableList;
import org.apache.commons.collections.CollectionUtils;
import org.apache.tapestry.form.validator.Required;
import org.apache.tapestry.form.validator.Validator;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import org.tandemframework.tapsupport.component.selection.SingleSelectTextModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.orgstruct.AcademyRename;
import ru.tandemservice.unidip.base.bo.DipDocument.DipDocumentManager;
import ru.tandemservice.unidip.base.bo.DipDocument.logic.DipEduInOtherOrganizationDSHandler;
import ru.tandemservice.unidip.base.bo.DipDocument.logic.DiplomaAcademyRenameDSHandler;
import ru.tandemservice.unidip.base.entity.catalog.codes.DipDocumentTypeCodes;
import ru.tandemservice.unidip.base.entity.diploma.*;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.codes.EduProgramSubjectIndexCodes;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 10.07.2014
 */
@Input({
               @Bind(key = DipDocumentAddInformationAddEditUI.DIP_DOCUMENT_ID, binding = "diplomaObject.id")
       })
public class DipDocumentAddInformationAddEditUI extends UIPresenter
{
    public static final String DIP_DOCUMENT_ID = "dipDocumentId";
    private DiplomaContent _diplomaContent;
    private List<DataWrapper> _diplomaAcademyRenameList;
    private DataWrapper _academyRenameDataWrapper;
    /*
    * сюда копируется _academyRenameDataWrapper после нажатия кнопки edit.
    * Нужно для отмены редактирования.
     */
    private AcademyRename _oldAcademyRename;
    private IUIDataSource _academyRenameListDS;
    private DataWrapper _currentEditAcademyRenameRow;
    private Long _wrapperListId = 0L;

    private DiplomaObject _diplomaObject = new DiplomaObject();
    private DipAdditionalInformation _additionalInformation;
    private DipEduInOtherOrganization _eduInOtherOrganization;
    private boolean _showEduProgramForm;
    private List<EduProgramForm> _eduFormList;
    private DataWrapper _specialization;
    private List<DataWrapper> _eduInOtherOrganizationList;
    private DataWrapper _eduInOtherOrganizationDataWrapper;

    private ISelectModel _eduOrganizationModel;

    private IUIDataSource _eduInOtherOrganizationListDS;
    private DataWrapper _currentEditEduInOtherOrganizationRow;
    private String _oldDiplomaContentRow;
    private Double _oldWeeks;
    private Double _oldCredits;

    @Override
    public void onComponentRefresh()
    {
        _diplomaObject = DataAccessServices.dao().getNotNull(DiplomaObject.class, _diplomaObject.getId());
        _diplomaContent = getDiplomaObject().getContent();
        initAdditionalInformation();
        wrapAcademyRenameList();
        wrapEduInOtherOrganizationList();

        _eduOrganizationModel = new SingleSelectTextModel(){
            @Override
            public ListResult findValues(final String filter){
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DiplomaContentRow.class, "row")
                        .where(likeUpper(DiplomaContentRow.education().fromAlias("row"), value(CoreStringUtils.escapeLike(filter))))
                        .where(eq(property(DiplomaContentRow.owner().fromAlias("row")), value(_diplomaContent)))
                        .column(property(DiplomaContentRow.education().fromAlias("row")));

                return new ListResult<>(IUniBaseDao.instance.get().<String>getList(builder));
            }
        };


        setAcademyRenameListDS(getConfig().getDataSource(DipDocumentAddInformationAddEdit.ACADEMY_RENAME_LIST_DS));
        setEduInOtherOrganizationListDS(getConfig().getDataSource(DipDocumentAddInformationAddEdit.EDU_IN_OTHER_ORGANIZATION_LIST_DS));
        setCurrentEditAcademyRenameRow(null);
        setCurrentEditEduInOtherOrganizationRow(null);
    }

    private void initAdditionalInformation()
    {
        _additionalInformation = DataAccessServices.dao().get(DipAdditionalInformation.class, DipAdditionalInformation.diplomaObject(), getDiplomaObject());
        if (null == getAdditionalInformation())
        {
            setAdditionalInformation(new DipAdditionalInformation());
        }
        else
        {
            List<DipAddInfoEduForm> addInfoEduFormList = DataAccessServices.dao().getList(DipAddInfoEduForm.class, DipAddInfoEduForm.dipAdditionalInformation(), getAdditionalInformation());

            _eduFormList = new ArrayList<>();
            for (DipAddInfoEduForm eduForm : addInfoEduFormList)
            {
                _eduFormList.add(eduForm.getEduProgramForm());
            }
            if (!CollectionUtils.isEmpty(getEduFormList()))
            {
                setShowEduProgramForm(true);
            }

            if (getAdditionalInformation().getSpecialization() != null &&
                    getAdditionalInformation().getSpecialization().equals(DipDocumentAddInformationAddEdit.ORIENTATION.getTitle()))
            {
                setSpecialization(DipDocumentAddInformationAddEdit.ORIENTATION);
            }
            else if (getAdditionalInformation().getSpecialization() != null &&
                    getAdditionalInformation().getSpecialization().equals(DipDocumentAddInformationAddEdit.SPECIALIZATION.getTitle()))
            {
                setSpecialization(DipDocumentAddInformationAddEdit.SPECIALIZATION);
            }
        }
    }

    private void wrapAcademyRenameList()
    {
        List<DiplomaAcademyRenameData> renameDataList = DataAccessServices.dao().getList(DiplomaAcademyRenameData.class, DiplomaAcademyRenameData.diplomaContent(), getDiplomaContent(),
                                                                                         DiplomaAcademyRenameData.academyRename().date().s());
        _diplomaAcademyRenameList = new ArrayList<>();
        for (DiplomaAcademyRenameData renameData : renameDataList)
        {
            DataWrapper wrapper = new DataWrapper(renameData);
            wrapper.getWrapped();
            wrapper.setId(getNextId());
            _diplomaAcademyRenameList.add(wrapper);
        }
    }

    private void wrapEduInOtherOrganizationList()
    {
        List<DipEduInOtherOrganization> otherOrganizationList = DataAccessServices.dao().getList(DipEduInOtherOrganization.class,
                                                                                                 DipEduInOtherOrganization.dipAdditionalInformation(), getAdditionalInformation());
        _eduInOtherOrganizationList = new ArrayList<>();
        for (DipEduInOtherOrganization eduInOtherOrganization : otherOrganizationList)
        {
            DataWrapper wrapper = new DataWrapper(eduInOtherOrganization);
            wrapper.getWrapped();
            wrapper.setId(getNextId());
            _eduInOtherOrganizationList.add(wrapper);
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
		switch (dataSource.getName())
		{
			case DipDocumentAddInformationAddEdit.ACADEMY_RENAME_LIST_DS:
				dataSource.put(DiplomaAcademyRenameDSHandler.DIP_ACADEMY_RENAME_LIST, getDiplomaAcademyRenameList());
				break;
			case DipDocumentAddInformationAddEdit.EDU_IN_OTHER_ORGANIZATION_LIST_DS:
				dataSource.put(DipEduInOtherOrganizationDSHandler.EDU_OTHER_ORGANIZATION_LIST, getEduInOtherOrganizationList());
				break;
			case DipDocumentAddInformationAddEdit.EDU_ORGANIZATION_DS:
				dataSource.put(DipDocumentAddInformationAddEdit.DIPLOMA_CONTENT, getDiplomaContent());
				break;
			case DipDocumentAddInformationAddEdit.ACADEMY_RENAME_COMBO_BOX_DS:
				List<DataWrapper> wrappers = new ArrayList<>(getDiplomaAcademyRenameList());
				wrappers.remove(getCurrentAcademyRenameRow());
				dataSource.put("items", wrappers);
				break;
		}
    }



    public Long getNextId()
    {
        return _wrapperListId++;
    }

    public DiplomaAcademyRenameData getCurrentAcademyRenameData()
    {
        return getCurrentAcademyRenameRow().getWrapped();
    }

    public DipEduInOtherOrganization getCurrentEduInOtherOrganizationData()
    {
        return getCurrentEduInOtherOrganizationRow().getWrapped();
    }

    public DiplomaAcademyRenameData getAcademyRenameData()
    {
        return getAcademyRenameDataWrapper().getWrapped();
    }

    public AcademyRename getOldAcademyRename()
    {
        return _oldAcademyRename;
    }

    public DataWrapper getCurrentAcademyRenameRow()
    {
        return getAcademyRenameListDS().getCurrent();
    }

    public DataWrapper getCurrentEduInOtherOrganizationRow()
    {
        return getEduInOtherOrganizationListDS().getCurrent();
    }

    public ISelectModel getEduOrganizationModel() {
        return _eduOrganizationModel;
    }

    public String getRenameYearTitle()
    {
        return DateFormatter.DATE_FORMATTER_JUST_YEAR.format(getCurrentAcademyRenameData().getAcademyRename().getDate());
    }

    public List<DataWrapper> getDiplomaAcademyRenameList()
    {
        return _diplomaAcademyRenameList;
    }

    public void setDiplomaAcademyRenameList(List<DataWrapper> diplomaAcademyRenameList)
    {
        _diplomaAcademyRenameList = diplomaAcademyRenameList;
    }

    public DiplomaObject getDiplomaObject()
    {
        return _diplomaObject;
    }

    public void setDiplomaObject(DiplomaObject diplomaObject)
    {
        _diplomaObject = diplomaObject;
    }

    public DiplomaContent getDiplomaContent()
    {
        return _diplomaContent;
    }

    public void setDiplomaContent(DiplomaContent diplomaContent)
    {
        _diplomaContent = diplomaContent;
    }

    public DipAdditionalInformation getAdditionalInformation()
    {
        return _additionalInformation;
    }

    public void setAdditionalInformation(DipAdditionalInformation additionalInformation)
    {
        _additionalInformation = additionalInformation;
    }

    public DipEduInOtherOrganization getEduInOtherOrganization()
    {
        return _eduInOtherOrganization;
    }

    public void setEduInOtherOrganization(DipEduInOtherOrganization eduInOtherOrganization)
    {
        _eduInOtherOrganization = eduInOtherOrganization;
    }

    public List<DataWrapper> getEduInOtherOrganizationList()
    {
        return _eduInOtherOrganizationList;
    }

    public boolean isShowEduProgramForm()
    {
        return _showEduProgramForm;
    }

    public void setShowEduProgramForm(boolean showEduProgramForm)
    {
        _showEduProgramForm = showEduProgramForm;
    }

    public List<EduProgramForm> getEduFormList()
    {
        return _eduFormList;
    }

    public void setEduFormList(List<EduProgramForm> eduFormList)
    {
        _eduFormList = eduFormList;
    }

    public DataWrapper getSpecialization()
    {
        return _specialization;
    }

    public void setSpecialization(DataWrapper specialization)
    {
        _specialization = specialization;
    }


    public IUIDataSource getAcademyRenameListDS()
    {
        return _academyRenameListDS;
    }

    public void setAcademyRenameListDS(IUIDataSource academyRenameListDS)
    {
        _academyRenameListDS = academyRenameListDS;
    }

    public IUIDataSource getEduInOtherOrganizationListDS()
    {
        return _eduInOtherOrganizationListDS;
    }

    public void setEduInOtherOrganizationListDS(IUIDataSource eduInOtherOrganizationListDS)
    {
        _eduInOtherOrganizationListDS = eduInOtherOrganizationListDS;
    }

    public DataWrapper getAcademyRenameDataWrapper()
    {
        return _academyRenameDataWrapper;
    }

    public void setAcademyRenameDataWrapper(DataWrapper academyRenameDataWrapper)
    {
        _academyRenameDataWrapper = academyRenameDataWrapper;
    }

    public DataWrapper getCurrentEditAcademyRenameRow()
    {
        return _currentEditAcademyRenameRow;
    }

    public void setCurrentEditAcademyRenameRow(DataWrapper currentEditAcademyRenameRow)
    {
        _currentEditAcademyRenameRow = currentEditAcademyRenameRow;
    }

    public DataWrapper getCurrentEditEduInOtherOrganizationRow()
    {
        return _currentEditEduInOtherOrganizationRow;
    }

    public void setCurrentEditEduInOtherOrganizationRow(DataWrapper currentEditEduInOtherOrganizationRow)
    {
        _currentEditEduInOtherOrganizationRow = currentEditEduInOtherOrganizationRow;
    }

    public boolean isAcademyRenameComponentInEditMode()
    {
        return null != getCurrentEditAcademyRenameRow();
    }

    public boolean isEduInOtherOrganizationComponentInEditMode()
    {
        return null != getCurrentEditEduInOtherOrganizationRow();
    }

    public boolean isCurrentAcademyRenameRowInEditMode()
    {
        DataWrapper editRow = getCurrentEditAcademyRenameRow();
        if (null == editRow || null == getCurrentAcademyRenameRow())
        {
            return false;
        }

        return _diplomaAcademyRenameList.indexOf(editRow) == _diplomaAcademyRenameList.indexOf(getCurrentAcademyRenameRow());
    }

    public boolean isCurrentEduInOtherOrganizationRowInEditMode()
    {
        DataWrapper editRow = getCurrentEditEduInOtherOrganizationRow();
        if (null == editRow || null == getCurrentEduInOtherOrganizationRow())
        {
            return false;
        }

        return _eduInOtherOrganizationList.indexOf(editRow) == _eduInOtherOrganizationList.indexOf(getCurrentEduInOtherOrganizationRow());
    }

    public boolean isBachelorOrMagistrDiplomaOrEducationCert()
    {
		final String dipTypeCode = getDiplomaObject().getContent().getType().getCode();
		final String subjectIndexCode = getProgramSubject().getSubjectIndex().getCode();
		final List<String> correctSubjIndexCodes = ImmutableList.of(EduProgramSubjectIndexCodes.TITLE_2005_62, EduProgramSubjectIndexCodes.TITLE_2005_68,
				EduProgramSubjectIndexCodes.TITLE_2009_62, EduProgramSubjectIndexCodes.TITLE_2009_68, EduProgramSubjectIndexCodes.TITLE_2013_03, EduProgramSubjectIndexCodes.TITLE_2013_04);
		return (dipTypeCode.equals(DipDocumentTypeCodes.BACHELOR_DIPLOMA) || dipTypeCode.equals(DipDocumentTypeCodes.MAGISTR_DIPLOMA) ||
                (dipTypeCode.equals(DipDocumentTypeCodes.EDUCATION_REFERENCE) && (correctSubjIndexCodes.contains(subjectIndexCode))));
    }

    public boolean isSpecialityDiplomaOrEducationCert()
    {
		final String dipTypeCode = getDiplomaObject().getContent().getType().getCode();
		final String subjectIndexCode = getProgramSubject().getSubjectIndex().getCode();
		final List<String> correctSubjIndexCodes = ImmutableList.of(EduProgramSubjectIndexCodes.TITLE_2005_65, EduProgramSubjectIndexCodes.TITLE_2009_65, EduProgramSubjectIndexCodes.TITLE_2013_05);
		return (dipTypeCode.equals(DipDocumentTypeCodes.SPECIALIST_DIPLOMA) ||
                (dipTypeCode.equals(DipDocumentTypeCodes.EDUCATION_REFERENCE) && (correctSubjIndexCodes.contains(subjectIndexCode))));
    }

    private EduProgramSubject getProgramSubject()
    {
        return getDiplomaObject().getContent().getProgramSubject();
    }

    public boolean isEducationReference()
    {
        return getDiplomaObject().getContent().getType().getCode().equals(DipDocumentTypeCodes.EDUCATION_REFERENCE);
    }

    public boolean isSpoDiploma()
    {
        return getDiplomaObject().getContent().getType().getCode().equals(DipDocumentTypeCodes.COLLEGE_DIPLOMA);
    }

    public void onClickAddDipAcadRenameDataList()
    {
        _oldAcademyRename = null;
        _academyRenameDataWrapper = new DataWrapper(new DiplomaAcademyRenameData());
        _academyRenameDataWrapper.setId(getNextId());
        _diplomaAcademyRenameList.add(_academyRenameDataWrapper);
        setCurrentEditAcademyRenameRow(_academyRenameDataWrapper);
    }

    public void onClickAddEduInOtherOrganizationDataList()
    {
        _oldDiplomaContentRow = null;
        _oldWeeks = 0d;
        _oldCredits = 0d;
        DipEduInOtherOrganization eduInOtherOrganization = new DipEduInOtherOrganization();
        eduInOtherOrganization.setDipAdditionalInformation(getAdditionalInformation());

        _eduInOtherOrganizationDataWrapper = new DataWrapper(eduInOtherOrganization);
        _eduInOtherOrganizationDataWrapper.setId(getNextId());
        _eduInOtherOrganizationList.add(_eduInOtherOrganizationDataWrapper);
        setCurrentEditEduInOtherOrganizationRow(_eduInOtherOrganizationDataWrapper);
    }

    public Validator getEduFormValidator()
    {
        return isShowEduProgramForm() ? new Required() : null;
    }

    public void clearEduProgramForm()
    {
        setEduFormList(null);
    }


    public void onClickEditAcademyRenameRow()
    {
        _academyRenameDataWrapper = getListenerParameter();
        _oldAcademyRename = ((DiplomaAcademyRenameData) _academyRenameDataWrapper.getWrapped()).getAcademyRename();
        setCurrentEditAcademyRenameRow(_academyRenameDataWrapper);
    }

    public void onClickEditEduInOtherOrganizationRow()
    {
        _eduInOtherOrganizationDataWrapper = getListenerParameter();
        _oldDiplomaContentRow = ((DipEduInOtherOrganization) _eduInOtherOrganizationDataWrapper.getWrapped()).getRow();
        _oldWeeks = ((DipEduInOtherOrganization) _eduInOtherOrganizationDataWrapper.getWrapped()).getTotalWeeksAsDouble();
        _oldCredits = ((DipEduInOtherOrganization) _eduInOtherOrganizationDataWrapper.getWrapped()).getTotalCreditsAsDouble();
        setCurrentEditEduInOtherOrganizationRow(_eduInOtherOrganizationDataWrapper);
    }

    public void onClickSaveEduInOtherOrganizationRow()
    {

        DataWrapper wrapper = getListenerParameter();
        DipEduInOtherOrganization eduInOtherOrganization = wrapper.getWrapped();

        Long credits = eduInOtherOrganization.getTotalCreditsAsLong();
        Long weeks = eduInOtherOrganization.getTotalWeeksAsLong();


        if ((credits != null && weeks != null) || (credits == null && weeks == null) ||
                ((credits != null && credits == 0L) || (weeks != null && weeks == 0L)))
        {
            throw new ApplicationException("Необходимо заполнить одно из полей: «Зачетные единицы» или «Недели».");
        }
        setCurrentEditEduInOtherOrganizationRow(null);
    }


    public void onClickSaveAcademyRenameRow()
    {
        setCurrentEditAcademyRenameRow(null);
        Collections.sort(_diplomaAcademyRenameList, Comparator.comparing(wrapper -> wrapper.<DiplomaAcademyRenameData>getWrapped().getAcademyRename().getDate()));
    }

    public void onClickDeleteEduInOtherOrganizationRow()
    {
        //если есть старые сохранные значения, то значит нажата кнопка отмена.
        if (null != getCurrentEditEduInOtherOrganizationRow() && null != _oldDiplomaContentRow)
        {
            //Устанавливаем в компоненты старые значения
            ((DipEduInOtherOrganization) _eduInOtherOrganizationDataWrapper.getWrapped()).setRow(_oldDiplomaContentRow);
            ((DipEduInOtherOrganization) _eduInOtherOrganizationDataWrapper.getWrapped()).setTotalWeeksAsDouble(_oldWeeks);
            ((DipEduInOtherOrganization) _eduInOtherOrganizationDataWrapper.getWrapped()).setTotalCreditsAsDouble(_oldCredits);
        }
        //если старых значений нет, но есть новая редактируемая строка, то удаляем эту строку
        else if (null != getCurrentEditEduInOtherOrganizationRow())
        {
            _eduInOtherOrganizationList.remove(_eduInOtherOrganizationList.size() - 1);
        }

        // удаление конкертной строки:
        else
        {
            DataWrapper wrapperForDelete = getListenerParameter();
            _eduInOtherOrganizationList.remove(wrapperForDelete);
            _eduInOtherOrganizationDataWrapper = null;
        }

        setCurrentEditEduInOtherOrganizationRow(null);
    }

    public void onClickDeleteAcademyRenameRow()
    {
        if (null != getCurrentEditAcademyRenameRow() && null != _oldAcademyRename)
        {
            ((DiplomaAcademyRenameData) _academyRenameDataWrapper.getWrapped()).setAcademyRename(_oldAcademyRename);
        }
        else if (null != getCurrentEditAcademyRenameRow())
        {
            _diplomaAcademyRenameList.remove(_diplomaAcademyRenameList.size() - 1);
        }
        else
        {
            DataWrapper wrapperForDelete = getListenerParameter();
            _diplomaAcademyRenameList.remove(wrapperForDelete);
            _academyRenameDataWrapper = null;
        }
        setCurrentEditAcademyRenameRow(null);
    }

    public void onClickApply()
    {
        if (getCurrentEditAcademyRenameRow() != null || getCurrentEditEduInOtherOrganizationRow() != null)
        {
            throw new ApplicationException("Завершите редактирование строк.");
        }

        List<DiplomaAcademyRenameData> diplomaAcademyRenameDataListForUpdate = new ArrayList<>();
        for (DataWrapper wrapper : _diplomaAcademyRenameList)
        {
            diplomaAcademyRenameDataListForUpdate.add(wrapper.getWrapped());
        }

        List<DipEduInOtherOrganization> dipEduInOtherOrganizationList = new ArrayList<>();
        for (DataWrapper wrapper : getEduInOtherOrganizationList())
        {
            dipEduInOtherOrganizationList.add(wrapper.getWrapped());
        }

        getAdditionalInformation().setDiplomaObject(getDiplomaObject());
        getAdditionalInformation().setSpecialization(null == getSpecialization() ? null : getSpecialization().getTitle());

        DipDocumentManager.instance().dao().saveAdditionalDiplomaData(_diplomaContent, diplomaAcademyRenameDataListForUpdate, getAdditionalInformation(),
                                                                      getEduFormList(), dipEduInOtherOrganizationList);
        deactivate();
    }
}
