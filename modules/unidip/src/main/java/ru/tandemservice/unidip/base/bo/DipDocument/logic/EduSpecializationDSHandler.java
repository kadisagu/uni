/* $Id$ */
package ru.tandemservice.unidip.base.bo.DipDocument.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
/**
 * @author Andrey Avetisov
 * @since 09.06.2014
 */
public class EduSpecializationDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String EDUCATION_LEVELS_HIGHSCHOOL = "educationLevels";

    public EduSpecializationDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        EducationLevelsHighSchool educationLevelsHighSchool = context.get(EDUCATION_LEVELS_HIGHSCHOOL);

        DQLSelectBuilder educationLevelsBuilder = new DQLSelectBuilder()
                .fromEntity(EducationLevels.class, "el").column(property("el", EducationLevels.eduProgramSpecialization()), "es")
                .where(or(
                        eq(property("el", EducationLevels.P_ID),
                                          DQLExpressions.value(educationLevelsHighSchool.getEducationLevel().getId())),
                        eq(property("el", EducationLevels.L_PARENT_LEVEL),
                                          value(educationLevelsHighSchool.getEducationLevel().getId()))
                ));


        List<EduProgramSpecialization> educationLevelsList = educationLevelsBuilder
                .createStatement(context.getSession()).list();

        DSOutput output = ListOutputBuilder.get(input, educationLevelsList).build();
        return output;
    }
}
