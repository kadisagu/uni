package ru.tandemservice.unidip.base.entity.order.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaObject;
import ru.tandemservice.unidip.base.entity.order.DipStuExcludeExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из приказа об отчислении
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DipStuExcludeExtractGen extends ListStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unidip.base.entity.order.DipStuExcludeExtract";
    public static final String ENTITY_NAME = "dipStuExcludeExtract";
    public static final int VERSION_HASH = 1362667231;
    private static IEntityMeta ENTITY_META;

    public static final String P_EXCLUDE_DATE = "excludeDate";
    public static final String L_STATUS_OLD = "statusOld";
    public static final String L_STATUS_NEW = "statusNew";
    public static final String L_DIPLOMA = "diploma";
    public static final String L_ISSUANCE = "issuance";
    public static final String P_FINISHED_YEAR = "finishedYear";

    private Date _excludeDate;     // Дата отчисления
    private StudentStatus _statusOld;     // Состояние студента на момент формирования приказа
    private StudentStatus _statusNew;     // Новое состояние студента
    private DiplomaObject _diploma;     // Документ об обучении
    private DiplomaIssuance _issuance;     // Факт выдачи
    private Integer _finishedYear;     // Год окончания на момент проведения приказа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата отчисления. Свойство не может быть null.
     */
    @NotNull
    public Date getExcludeDate()
    {
        return _excludeDate;
    }

    /**
     * @param excludeDate Дата отчисления. Свойство не может быть null.
     */
    public void setExcludeDate(Date excludeDate)
    {
        dirty(_excludeDate, excludeDate);
        _excludeDate = excludeDate;
    }

    /**
     * @return Состояние студента на момент формирования приказа. Свойство не может быть null.
     */
    @NotNull
    public StudentStatus getStatusOld()
    {
        return _statusOld;
    }

    /**
     * @param statusOld Состояние студента на момент формирования приказа. Свойство не может быть null.
     */
    public void setStatusOld(StudentStatus statusOld)
    {
        dirty(_statusOld, statusOld);
        _statusOld = statusOld;
    }

    /**
     * @return Новое состояние студента. Свойство не может быть null.
     */
    @NotNull
    public StudentStatus getStatusNew()
    {
        return _statusNew;
    }

    /**
     * @param statusNew Новое состояние студента. Свойство не может быть null.
     */
    public void setStatusNew(StudentStatus statusNew)
    {
        dirty(_statusNew, statusNew);
        _statusNew = statusNew;
    }

    /**
     * @return Документ об обучении.
     */
    public DiplomaObject getDiploma()
    {
        return _diploma;
    }

    /**
     * @param diploma Документ об обучении.
     */
    public void setDiploma(DiplomaObject diploma)
    {
        dirty(_diploma, diploma);
        _diploma = diploma;
    }

    /**
     * @return Факт выдачи.
     */
    public DiplomaIssuance getIssuance()
    {
        return _issuance;
    }

    /**
     * @param issuance Факт выдачи.
     */
    public void setIssuance(DiplomaIssuance issuance)
    {
        dirty(_issuance, issuance);
        _issuance = issuance;
    }

    /**
     * @return Год окончания на момент проведения приказа.
     */
    public Integer getFinishedYear()
    {
        return _finishedYear;
    }

    /**
     * @param finishedYear Год окончания на момент проведения приказа.
     */
    public void setFinishedYear(Integer finishedYear)
    {
        dirty(_finishedYear, finishedYear);
        _finishedYear = finishedYear;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof DipStuExcludeExtractGen)
        {
            setExcludeDate(((DipStuExcludeExtract)another).getExcludeDate());
            setStatusOld(((DipStuExcludeExtract)another).getStatusOld());
            setStatusNew(((DipStuExcludeExtract)another).getStatusNew());
            setDiploma(((DipStuExcludeExtract)another).getDiploma());
            setIssuance(((DipStuExcludeExtract)another).getIssuance());
            setFinishedYear(((DipStuExcludeExtract)another).getFinishedYear());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DipStuExcludeExtractGen> extends ListStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DipStuExcludeExtract.class;
        }

        public T newInstance()
        {
            return (T) new DipStuExcludeExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "excludeDate":
                    return obj.getExcludeDate();
                case "statusOld":
                    return obj.getStatusOld();
                case "statusNew":
                    return obj.getStatusNew();
                case "diploma":
                    return obj.getDiploma();
                case "issuance":
                    return obj.getIssuance();
                case "finishedYear":
                    return obj.getFinishedYear();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "excludeDate":
                    obj.setExcludeDate((Date) value);
                    return;
                case "statusOld":
                    obj.setStatusOld((StudentStatus) value);
                    return;
                case "statusNew":
                    obj.setStatusNew((StudentStatus) value);
                    return;
                case "diploma":
                    obj.setDiploma((DiplomaObject) value);
                    return;
                case "issuance":
                    obj.setIssuance((DiplomaIssuance) value);
                    return;
                case "finishedYear":
                    obj.setFinishedYear((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "excludeDate":
                        return true;
                case "statusOld":
                        return true;
                case "statusNew":
                        return true;
                case "diploma":
                        return true;
                case "issuance":
                        return true;
                case "finishedYear":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "excludeDate":
                    return true;
                case "statusOld":
                    return true;
                case "statusNew":
                    return true;
                case "diploma":
                    return true;
                case "issuance":
                    return true;
                case "finishedYear":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "excludeDate":
                    return Date.class;
                case "statusOld":
                    return StudentStatus.class;
                case "statusNew":
                    return StudentStatus.class;
                case "diploma":
                    return DiplomaObject.class;
                case "issuance":
                    return DiplomaIssuance.class;
                case "finishedYear":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DipStuExcludeExtract> _dslPath = new Path<DipStuExcludeExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DipStuExcludeExtract");
    }
            

    /**
     * @return Дата отчисления. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.order.DipStuExcludeExtract#getExcludeDate()
     */
    public static PropertyPath<Date> excludeDate()
    {
        return _dslPath.excludeDate();
    }

    /**
     * @return Состояние студента на момент формирования приказа. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.order.DipStuExcludeExtract#getStatusOld()
     */
    public static StudentStatus.Path<StudentStatus> statusOld()
    {
        return _dslPath.statusOld();
    }

    /**
     * @return Новое состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.order.DipStuExcludeExtract#getStatusNew()
     */
    public static StudentStatus.Path<StudentStatus> statusNew()
    {
        return _dslPath.statusNew();
    }

    /**
     * @return Документ об обучении.
     * @see ru.tandemservice.unidip.base.entity.order.DipStuExcludeExtract#getDiploma()
     */
    public static DiplomaObject.Path<DiplomaObject> diploma()
    {
        return _dslPath.diploma();
    }

    /**
     * @return Факт выдачи.
     * @see ru.tandemservice.unidip.base.entity.order.DipStuExcludeExtract#getIssuance()
     */
    public static DiplomaIssuance.Path<DiplomaIssuance> issuance()
    {
        return _dslPath.issuance();
    }

    /**
     * @return Год окончания на момент проведения приказа.
     * @see ru.tandemservice.unidip.base.entity.order.DipStuExcludeExtract#getFinishedYear()
     */
    public static PropertyPath<Integer> finishedYear()
    {
        return _dslPath.finishedYear();
    }

    public static class Path<E extends DipStuExcludeExtract> extends ListStudentExtract.Path<E>
    {
        private PropertyPath<Date> _excludeDate;
        private StudentStatus.Path<StudentStatus> _statusOld;
        private StudentStatus.Path<StudentStatus> _statusNew;
        private DiplomaObject.Path<DiplomaObject> _diploma;
        private DiplomaIssuance.Path<DiplomaIssuance> _issuance;
        private PropertyPath<Integer> _finishedYear;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата отчисления. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.order.DipStuExcludeExtract#getExcludeDate()
     */
        public PropertyPath<Date> excludeDate()
        {
            if(_excludeDate == null )
                _excludeDate = new PropertyPath<Date>(DipStuExcludeExtractGen.P_EXCLUDE_DATE, this);
            return _excludeDate;
        }

    /**
     * @return Состояние студента на момент формирования приказа. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.order.DipStuExcludeExtract#getStatusOld()
     */
        public StudentStatus.Path<StudentStatus> statusOld()
        {
            if(_statusOld == null )
                _statusOld = new StudentStatus.Path<StudentStatus>(L_STATUS_OLD, this);
            return _statusOld;
        }

    /**
     * @return Новое состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.order.DipStuExcludeExtract#getStatusNew()
     */
        public StudentStatus.Path<StudentStatus> statusNew()
        {
            if(_statusNew == null )
                _statusNew = new StudentStatus.Path<StudentStatus>(L_STATUS_NEW, this);
            return _statusNew;
        }

    /**
     * @return Документ об обучении.
     * @see ru.tandemservice.unidip.base.entity.order.DipStuExcludeExtract#getDiploma()
     */
        public DiplomaObject.Path<DiplomaObject> diploma()
        {
            if(_diploma == null )
                _diploma = new DiplomaObject.Path<DiplomaObject>(L_DIPLOMA, this);
            return _diploma;
        }

    /**
     * @return Факт выдачи.
     * @see ru.tandemservice.unidip.base.entity.order.DipStuExcludeExtract#getIssuance()
     */
        public DiplomaIssuance.Path<DiplomaIssuance> issuance()
        {
            if(_issuance == null )
                _issuance = new DiplomaIssuance.Path<DiplomaIssuance>(L_ISSUANCE, this);
            return _issuance;
        }

    /**
     * @return Год окончания на момент проведения приказа.
     * @see ru.tandemservice.unidip.base.entity.order.DipStuExcludeExtract#getFinishedYear()
     */
        public PropertyPath<Integer> finishedYear()
        {
            if(_finishedYear == null )
                _finishedYear = new PropertyPath<Integer>(DipStuExcludeExtractGen.P_FINISHED_YEAR, this);
            return _finishedYear;
        }

        public Class getEntityClass()
        {
            return DipStuExcludeExtract.class;
        }

        public String getEntityName()
        {
            return "dipStuExcludeExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
