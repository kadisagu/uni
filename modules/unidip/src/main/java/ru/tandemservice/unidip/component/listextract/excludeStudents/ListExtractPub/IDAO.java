/* $Id$ */
package ru.tandemservice.unidip.component.listextract.excludeStudents.ListExtractPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.IAbstractListExtractPubDAO;
import ru.tandemservice.unidip.base.entity.order.DipStuExcludeExtract;

/**
 * @author Dmitry Seleznev
 * @since 08.06.2011
 */
public interface IDAO extends IAbstractListExtractPubDAO<DipStuExcludeExtract, Model>
{
}