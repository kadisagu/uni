/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unidip.component.listextract.excludeStudents.ParagraphAddEdit;

import org.hibernate.Session;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.StudentListParagraph;
import ru.tandemservice.movestudent.utils.ExtEducationLevelsHighSchoolSelectModel;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.uni.util.CustomStateUtil;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentIssuance;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance;
import ru.tandemservice.unidip.base.entity.order.DipContentIssuanceToDipExtractRelation;
import ru.tandemservice.unidip.base.entity.order.DipStuExcludeExtract;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.UnimoveDefines;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**``
 * @author iolshvang
 * @since 25.07.11 15:41
 */
public class DAO extends AbstractListParagraphAddEditDAO<DipStuExcludeExtract, Model> implements IDAO
{

    private List<DipContentIssuanceToDipExtractRelation> relationsList = new ArrayList<>();

    @Override
    @SuppressWarnings("unchecked")
    public void prepareColumns(final Model model)
    {
        if (model.getParagraphId() != null)
        {
            Map<Long, DataWrapper> diplomaObjectValueMap = new HashMap<>();
            Map<Long, List<DataWrapper>> relationMap = new HashMap<>();
            for (IAbstractExtract ext : model.getParagraph().getExtractList())
            {
                DipStuExcludeExtract extr = (DipStuExcludeExtract) ext;
                final Long id = extr.getEntity().getId();
                final DiplomaIssuance issuance = extr.getIssuance();

                final DataWrapper wrapper = new DataWrapper(issuance);
                wrapper.setTitle(model.getDiplomaIssuanceTitle(issuance));
                diplomaObjectValueMap.put(id, wrapper);
                List<DipContentIssuanceToDipExtractRelation> relationList = getList(DipContentIssuanceToDipExtractRelation.class, DipContentIssuanceToDipExtractRelation.extract(), extr);
                for (DipContentIssuanceToDipExtractRelation relation : relationList) {
                    final DiplomaContentIssuance contentIssuance = relation.getContentIssuance();
                    final DataWrapper e = new DataWrapper(contentIssuance);
                    StringBuilder titleBuilder = new StringBuilder();
                    titleBuilder.append("№").append(contentIssuance.getContentListNumber())
                            .append(": ")
                            .append(contentIssuance.getBlankSeria())
                            .append(" №")
                            .append(contentIssuance.getBlankNumber());
                    if (contentIssuance.isDuplicate())
                        titleBuilder.append(" Дубликат ").append(contentIssuance.getDuplicateRegustrationNumber())
                                .append(" ")
                                .append(DateFormatter.DEFAULT_DATE_FORMATTER.format(contentIssuance.getDuplicateIssuanceDate()));
                    e.setTitle(titleBuilder.toString());
                    SafeMap.safeGet(relationMap, id, ArrayList.class).add(e);
                }
            }
            ((BlockColumn) model.getDataSource().getColumn("diplomaIssuance")).setValueMap(diplomaObjectValueMap);
            ((BlockColumn) model.getDataSource().getColumn("issuanceContent")).setValueMap(relationMap);
        }
    }

    @Override
    @SuppressWarnings("deprecation")
    public void prepare(final Model model)
    {
        super.prepare(model);
        final OrgUnit curOrgUnit;
        if (null != model.getParagraphId())
            curOrgUnit = model.getParagraph().getOrder().getOrgUnit();
        else
            curOrgUnit = get(StudentListOrder.class, model.getOrderId()).getOrgUnit();

        model.setEducationLevelsListModel(new ExtEducationLevelsHighSchoolSelectModel(null, null, curOrgUnit));

        model.setFormativeOrgUnitListModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING));
        model.setTerritorialOrgUnitListModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL));
        model.setDevelopFormList(EducationCatalogsManager.getDevelopFormSelectModel());
        model.setDevelopConditionList(EducationCatalogsManager.getDevelopConditionSelectModel());
        model.setDevelopTechList(EducationCatalogsManager.getDevelopTechSelectModel());
        model.setDevelopPeriodList(EducationCatalogsManager.getDevelopPeriodSelectModel());
        model.setStudentStatusesList(getCatalogItemList(StudentStatus.class));
        model.setFormative(curOrgUnit);

        // заполняем поля по умолчанию
        if (model.getParagraphId() != null)
        {
            DipStuExcludeExtract extract = (DipStuExcludeExtract) model.getParagraph().getFirstExtract();
            model.setEducationLevel(extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool());
            model.setStudentStatusNew(extract.getStatusNew());
            model.setExcludeDate(extract.getExcludeDate());
            model.setCourse(extract.getEntity().getCourse());
            model.setFormative(extract.getEntity().getEducationOrgUnit().getFormativeOrgUnit());
            model.setTerritorial(extract.getEntity().getEducationOrgUnit().getTerritorialOrgUnit());
            model.setDevelopForm(extract.getEntity().getEducationOrgUnit().getDevelopForm());
            model.setDevelopCondition(extract.getEntity().getEducationOrgUnit().getDevelopCondition());
            model.setDevelopTech(extract.getEntity().getEducationOrgUnit().getDevelopTech());
            model.setDevelopPeriod(extract.getEntity().getEducationOrgUnit().getDevelopPeriod());
        }
        else
        {
            model.setStudentStatusNew(getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_GRADUATED_WITH_DIPLOMA));
        }
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(Student.ENTITY_CLASS, STUDENT_ALIAS);

        CustomStateUtil.addCustomStatesFilter(builder, STUDENT_ALIAS, model.getStudentCustomStateCIs());
        patchListDataSource(builder, model);

        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.P_ARCHIVAL, Boolean.FALSE));

        orderRegistry.applyOrder(builder, model.getDataSource().getEntityOrder());
        List<Student> list = builder.getResultList(getSession());
        model.getDataSource().setCountRow(list.size());

        Map<Long, List<DiplomaIssuance>> diplomaIssuanceMap = new HashMap<>();
        List<DiplomaIssuance> issuanceList = new DQLSelectBuilder().fromEntity(DiplomaIssuance.class, "di")
                .where(in(property("di", DiplomaIssuance.diplomaObject().student()), list))
                .where(isNotNull(property("di", DiplomaIssuance.blankSeria())))
                .where(isNotNull(property("di", DiplomaIssuance.blankNumber())))
                .createStatement(getSession()).list();

        for (DiplomaIssuance issuance : issuanceList)
            SafeMap.safeGet(diplomaIssuanceMap, issuance.getDiplomaObject().getStudent().getId(), ArrayList.class).add(issuance);
        model.setDiplomaIssuanceMap(diplomaIssuanceMap);

        Map<DiplomaIssuance, List<DiplomaContentIssuance>> diplomaContentIssuanceMap = new HashMap<>();
        List<DiplomaContentIssuance> contentIssuanceList = getList(DiplomaContentIssuance.class, DiplomaContentIssuance.diplomaIssuance(), issuanceList);
        for (DiplomaContentIssuance issuanceContent : contentIssuanceList) {
            SafeMap.safeGet(diplomaContentIssuanceMap, issuanceContent.getDiplomaIssuance(), ArrayList.class).add(issuanceContent);
        }

        model.setDiplomaContentIssuanceMap(diplomaContentIssuanceMap);

        UniBaseUtils.createPage(model.getDataSource(), list);

        setAdditionalViewProperties(model, list, true);
    }

    @Override
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.educationOrgUnit().formativeOrgUnit().s(), model.getParagraph().getOrder().getOrgUnit()));
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.educationOrgUnit().territorialOrgUnit().s(), model.getTerritorial()));
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.educationOrgUnit().educationLevelHighSchool().s(), model.getEducationLevel()));
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.course().s(), model.getCourse()));
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.educationOrgUnit().developForm().s(), model.getDevelopForm()));
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.educationOrgUnit().developCondition().s(), model.getDevelopCondition()));
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.educationOrgUnit().developTech().s(), model.getDevelopTech()));
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.educationOrgUnit().developPeriod().s(), model.getDevelopPeriod()));
    }

    @Override
    protected DipStuExcludeExtract createNewInstance(Model model)
    {
        return new DipStuExcludeExtract();
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void fillExtract(DipStuExcludeExtract extract, Student student, Model model) {
        extract.setExcludeDate(model.getExcludeDate());
        extract.setStatusNew(model.getStudentStatusNew());
        extract.setStatusOld(student.getStatus());

        final IValueMapHolder<Object> diplomaIssuanceValueHolder = (IValueMapHolder<Object>) model.getDataSource().getColumn("diplomaIssuance");
        final Map<Long, Object> diplomaIssuanceValueMap = (null == diplomaIssuanceValueHolder ? Collections.<Long, Object>emptyMap() : diplomaIssuanceValueHolder.getValueMap());

        ErrorCollector errs = UserContext.getInstance().getErrorCollector();
        final Long id = student.getId();
        if (!diplomaIssuanceValueMap.containsKey(id))
            errs.add("Поле \"Документ об обучении\" обязательно для заполнения.", "diploma" + id);

        final DataWrapper dataWrapper = (DataWrapper) diplomaIssuanceValueMap.get(id);
        final DiplomaIssuance issuance = dataWrapper.getWrapped();
        extract.setDiploma(issuance.getDiplomaObject());
        extract.setIssuance(issuance);

        final IValueMapHolder<Object> issuanceContentValueHolder = (IValueMapHolder<Object>) model.getDataSource().getColumn("issuanceContent");
        if (issuanceContentValueHolder != null) {
            final Map<Long, Object> issuanceContentValueMap = issuanceContentValueHolder.getValueMap();
            List<DataWrapper> contentIssuanceList = ((List<DataWrapper>) issuanceContentValueMap.get(id));
            for (DataWrapper wrapper : contentIssuanceList) {
                DipContentIssuanceToDipExtractRelation relation = new DipContentIssuanceToDipExtractRelation();
                relation.setContentIssuance(wrapper.getWrapped());
                relation.setExtract(extract);
                relationsList.add(relation);
            }
        }
    }

    @Override
    protected void updateParagraph(Model model, List<Student> studentList) {

        relationsList.clear();

        MergeAction.SessionMergeAction<DiplomaContentIssuance, DipContentIssuanceToDipExtractRelation> merger = new MergeAction.SessionMergeAction<DiplomaContentIssuance, DipContentIssuanceToDipExtractRelation>()
        {
            @Override
            protected DiplomaContentIssuance key(DipContentIssuanceToDipExtractRelation source) {
                return source.getContentIssuance();
            }

            @Override
            protected DipContentIssuanceToDipExtractRelation buildRow(DipContentIssuanceToDipExtractRelation source) {
                return source;
            }

            @Override
            protected void fill(DipContentIssuanceToDipExtractRelation target, DipContentIssuanceToDipExtractRelation source) {
                target.update(source);
            }
        };


        // V A L I D A T E

        final Session session = getSession();

        session.refresh(model.getParagraph().getOrder());
        if (!UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(model.getParagraph().getOrder().getState().getCode()))
            throw new ApplicationException("Нельзя изменять приказ. Изменение приказа возможно только в состоянии «Формируется».");

        if (studentList.size() == 0)
            throw new ApplicationException("Параграф приказа не может быть пустым. При необходимости удалите параграф приказа и создайте новый.");

        studentList.sort(Student.FULL_FIO_AND_ID_COMPARATOR);

        // пока мы были на форме state мог очень сильно поменяться, поэтому надо все данные проверить на актуаность
        for (Student student : studentList)
        {
            // студент не должен быть в другом приказе
            if (!MoveStudentDaoFacade.getMoveStudentDao().isMoveAccessible(student, model.getParagraphId()))
                throw new ApplicationException("Нельзя добавить параграф, так как у студента " + student.getPerson().getFullFio() + " уже есть непроведенные выписки.");
        }

        ErrorCollector errs = UserContext.getInstance().getErrorCollector();

        if (model.isEditForm())
        {
            // E D I T   P A R A G R A P H

            List<DipStuExcludeExtract> extractsList = new ArrayList<>();
            List<DipStuExcludeExtract> extractsToDeleteList = new ArrayList<>();

            // удаляем выписки у студентов, которые не выбранны на форме. тут специально не перенумеровываем выписки,
            // так как потом будет общая перенумерация по ФИО
            for (IAbstractExtract e : model.getParagraph().getExtractList()) {
                final DipStuExcludeExtract extract = (DipStuExcludeExtract) e;
                if (!studentList.remove(extract.getEntity()))
                    extractsToDeleteList.add(extract);
                else
                {
                    model.setExtract(extract);
                    fillExtract(extract, extract.getEntity(), model);
                }
            }

            for (Student student : studentList) {
                // создаем выписку и заполняем в ней стандартные поля
                DipStuExcludeExtract extract = createAndFillExtract(student, model);
                extractsList.add(extract);
            }

            if (errs.hasErrors()) throw new ApplicationException("При заполнении формы допущена ошибка.");

            extractsToDeleteList.forEach(session::delete);
            extractsList.forEach(session::save);
        }
        else
        {
            // C R E A T E   P A R A G R A P H

            StudentListParagraph paragraph = new StudentListParagraph();
            paragraph.setOrder(model.getParagraph().getOrder());
            paragraph.setNumber(model.getParagraph().getOrder().getParagraphCount() + 1);  // номер параграфа с единицы

            model.setParagraph(paragraph);
            List<DipStuExcludeExtract> extractsList = new ArrayList<>();

            for (Student student : studentList)
            {
                // создаем выписку и заполняем в ней стандартные поля
                DipStuExcludeExtract extract = createAndFillExtract(student, model);
                extractsList.add(extract);
            }

            if (errs.hasErrors()) throw new ApplicationException("При заполнении формы допущена ошибка.");

            session.save(paragraph);
            extractsList.forEach(session::save);
        }

        // Перенумеруем все выписки, так чтобы сортировка была по ФИО
        List<ListStudentExtract> extractList = getList(ListStudentExtract.class, IAbstractExtract.L_PARAGRAPH, model.getParagraph());
        extractList.sort(AbstractStudentExtract.FULL_FIO_AND_ID_COMPARATOR);

        Map<DipStuExcludeExtract, List<DipContentIssuanceToDipExtractRelation>> extractToRelMap = new HashMap<>();

        for (DipContentIssuanceToDipExtractRelation relation : relationsList) {
            SafeMap.safeGet(extractToRelMap, relation.getExtract(), ArrayList.class).add(relation);
        }

        // нумеруем выписки с единицы
        int counter = 1;
        for (ListStudentExtract extract : extractList)
        {
            extract.setNumber(counter++);
            List<DipContentIssuanceToDipExtractRelation> source = getList(DipContentIssuanceToDipExtractRelation.class, DipContentIssuanceToDipExtractRelation.extract(), (DipStuExcludeExtract) extract);
            List<DipContentIssuanceToDipExtractRelation> targetRecords = extractToRelMap.get(extract);
            if (source == targetRecords) continue; // если оба нулл, то мерджить не надо.
            if (source == null) source = Collections.emptyList();
            if (targetRecords == null) targetRecords = Collections.emptyList();
            merger.merge(source, targetRecords);
            session.update(extract);
        }
    }
}