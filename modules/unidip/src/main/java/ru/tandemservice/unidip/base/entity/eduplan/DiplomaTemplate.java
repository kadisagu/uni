package ru.tandemservice.unidip.base.entity.eduplan;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.sec.ISecLocalEntityOwner;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContent;
import ru.tandemservice.unidip.base.entity.eduplan.gen.DiplomaTemplateGen;

import java.util.Collection;

/**
 * Шаблон диплома для УП(в)
 *
 * Диплом – документ удостоверяющий получение образования (поведение определяется типом)
 */
public class DiplomaTemplate extends DiplomaTemplateGen implements ISecLocalEntityOwner
{
    @EntityDSLSupport(parts = L_CONTENT + "." + DiplomaContent.P_EDUCATION_ELEMENT_TITLE)
    @Override
    public String getTitle()
    {
        return getContent().getEducationElementTitle();
    }

	@Override
	public Collection<IEntity> getSecLocalEntities()
	{
		return getEduPlanVersion().getSecLocalEntities();
	}
}