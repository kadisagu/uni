/* $Id$ */
package ru.tandemservice.unidip.base.bo.DipDocument.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.info.InfoCollector;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.unidip.base.entity.catalog.DipDocTemplateCatalog;
import ru.tandemservice.unidip.base.entity.catalog.DipDocumentType;
import ru.tandemservice.unidip.base.entity.diploma.*;
import ru.tandemservice.unidip.base.entity.eduplan.DiplomaTemplate;
import ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Vasily Zhukov
 * @since 05.05.2011
 */
public interface IDipDocumentDao extends INeedPersistenceSupport
{
    /**
     * Добавлять/Редактировать документ может только одна транзакция в одно время,
     * так как требуется обеспечить уникальность номера документа
     */
    String DIP_DOCUMENT_ADD_EDIT_SYNC = "dipDocumentAddEditSync";

    /**
     * Метод выдает список печатных шаблонов для конкретного типа документов.
     *
     * @param documentTypeId ID типа документа
     * @return список печатных шаблонов
     */
    List<DipDocTemplateCatalog> getTemplateList(Long documentTypeId);

    /**
     * Метод сохраняет в БД переданный документ, с указанными для него параметрами.
     *
     * @param dipObject   Документ, который необходимо сохранить
     * @param dipTemplate шаблон диплома для УП(в)
     * @return сохраненный документ
     */
    DiplomaObject saveDiplomaObject(DiplomaObject dipObject, DiplomaTemplate dipTemplate);

    /**
     * Метод сохраняет в БД переданный документ, с указанными для него параметрами.
     *
     * @param diplomaObject         Докмент, который необходимо сохранить
     * @param documentType          Тип сохраняемого документа
     * @param programSubject        Направление образовательной программы
     * @param specialization        Направленность образовательной программы
     * @param diplomaTemplate       шаблон диплома для УП(в)
     * @param educationElementTitle Направленность образовательной программы
     * @param withSuccess           Признак "С отличием"
     * @param loadAsDouble          Общая трудоемкость (в неделях или ЗЕ)
     * @return сохраненный докмент
     */
    DiplomaObject saveDiplomaObject(DiplomaObject diplomaObject, DipDocumentType documentType, EduProgramSubject programSubject, EduProgramSpecialization specialization, DiplomaTemplate diplomaTemplate,
                                    String educationElementTitle, Boolean withSuccess, Double loadAsDouble);

    /**
     * Метод производит апдейт переданного документа, в соответсвии с указанными параметрами.
     *
     * @param diplomaObject         Докмент, который необходимо обновить
     * @param educationElementTitle Направленность образовательной программы
     * @param dipDocumentType       Тип документов об обучении
     * @param withSuccess           Признак "С отличием"
     * @param loadAsDouble          Общая трудоемкость (в неделях или ЗЕ)
     */
    void updateDiplomaObject(DiplomaObject diplomaObject, String educationElementTitle, DipDocumentType dipDocumentType, Boolean withSuccess, Double loadAsDouble);

    /**
     * Метод сохраняет в БД список переданных фактов выдачи диплома.
     *  @param diplomaIssuance     Докмент, который необходимо сохранить
     * @param issuanceContentList приложения к факту выдачи
     * @param oldBlankMap старые бланки
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void saveDiplomaIssuance(DiplomaIssuance diplomaIssuance, List<DiplomaContentIssuance> issuanceContentList, Map<Long, DipDiplomaBlank> oldBlankMap);

    /**
     * Метод апдэйтит в БД дополнительную информацию о дипломе
     * для конкретного содрежания документа о полученном образовании.
     *
     * @param content                       Содержание документа о полученном образовании, к которому относитя список переименований организации.
     * @param diplomaAcademyRenameDataList  Список переданных переименований обр. орг. для вывода в дипломе.
     * @param additionalInformation         Дополнительные сведения о дипломе
     * @param eduProgramFormList            Списко форм обучения, для вывода в дополнительных сведениях
     * @param dipEduInOtherOrganizationList Список частей образовательной программы освоенных в других организациях
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void saveAdditionalDiplomaData(DiplomaContent content, List<DiplomaAcademyRenameData> diplomaAcademyRenameDataList,
                                                      DipAdditionalInformation additionalInformation, List<EduProgramForm> eduProgramFormList,
                                                      List<DipEduInOtherOrganization> dipEduInOtherOrganizationList);

    boolean isCanDeleteDipDocument(DiplomaObject diplomaObject);

    void deleteDipDocument(Long dipDocumentId);

    /**
     * В запросе к МСРП по нагрузке для справки об обучении оставить только МСРП, имеющие положительные итоговые оценки.
     * При этом, если статус студента относится к активным, то оставляются только актуальные МСРП. В противном случае - только неактуальные.
     * @param wpePartDql - запрос к МСРП по нагрузке
     * @param wpePartAlias - алиас МСРП по нагрузке
     * @param wpeAlias - алиас МСРП
     * @param studentStatus - статус студента
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    void filterWpePartsAtEducationReference(DQLSelectBuilder wpePartDql, final String wpePartAlias, final String wpeAlias, StudentStatus studentStatus);

    /**
     * Метод создает строки содержания документа об образовании для переданного документа.
     *
     * @param diplomaObject Документ об образовании для которого необходимо сформировать строки.
     */
    void updateLine(DiplomaObject diplomaObject, InfoCollector infoCollector);

	/**
	 * Создать документы об обучении для указанных студентов (кнопка "Сформировать титулы дипломов"). Обработка студентов происходит в алфавитном порядке ФИО.
	 * @param studentIds id студентов.
	 * @param diplomaObject Контейнер данных по ГЭК.
	 * @param withSuccess Признак "с отличием".
	 * @param issuanceDate Дата выдачи - если указана, то формируется факт выдачи документа.
	 * @param useExistingDiplomas Использовать уже существующие документы. Если {@code true}, то если у студента уже есть диплом (не справка об обучении), то вместо создания нового
	 *                            будет происходить заполнение данными (ГЭК, "с отличием") уже существующего (последнего по id, если их несколько); точно так же если задана дата выдачи,
	 *                            то будет изменена дата выдачи у уже существующего факта выдачи (последнего по дате, если их несколько). Если подходящих объектов (документа или факта выдачи)
	 *                            еще нет, то будут созданы новые, как при значении параметра {@code false}.
	 */
	void createDiplomaTitles(Collection<Long> studentIds, DiplomaObject diplomaObject, boolean withSuccess, Date issuanceDate, boolean useExistingDiplomas);

	/**
	 * Создать документы об обучении по шаблону для указанных студентов.
	 * @param studentIds id студентов.
	 * @param diplomaObject Контейнер данных по ГЭК.
	 * @param withSuccess Признак "с отличием".
	 * @param issuanceDate Дата выдачи - если указана, то формируется факт выдачи документа.
     * @param infoCollector Интерфейс выдачи информационных сообщений.
	 */
	void createDiplomaObject(Collection<Long> studentIds, DiplomaObject diplomaObject, boolean withSuccess, Date issuanceDate, InfoCollector infoCollector);

    /**
     * Метод добавляет или обновляет строку содержания документа о полученном образовании, а так же элементы из которых
     * состоит данная строка
     *
     * @param elementPartFControlActionList содержит элементы строки, которые необходимо добавить или обновить
     * @param row                           строка содержания документа о полученном образовании которую необходимо добавить или обновит
     */
    void saveOrUpdateDiplomaContentRow(List<DiplomaContentRegElPartFControlAction> elementPartFControlActionList, DiplomaContentRow row);

    /**
     * Заполняет строки документа об обучении, согласно шаблону
     *
     * @param dipDocument2TemplateMap связь документа об обучении с шаблоном
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void fillDocumentRowByTemplate(Map<DiplomaObject, DiplomaTemplate> dipDocument2TemplateMap);

    /**
     * Возвращает список документов, в которых отброшены все типы, кроме дипломов.
     * Отбрасывает дипломы, на которые ссылается хотя бы одна строка документа об обучении, а также дипломы, у которых не заполнены УП.
     *
     * @param studentIds список студентов
     * @return список дипломов студентов
     */
    List<Long> getDiplomaWithoutRowIds(Collection<Long> studentIds);

    /**
     * Метод устанавливает оценки в строках документа о полученном образовании.
     *
     * @param diplomaContent содержание документа, в строках которого требуется выставить оценки.
     */
    void saveLastMarkInDiplomaContentRow(DiplomaContent diplomaContent);

	/**
	 * Пересчитать общую трудоемкость в содержании документа об обучении. Вычисленная трудоемкость равна сумме трудоемкостей всех строк документа об обучении, относящихся к нему.
	 * @param diplomaContent целевой объект содержания документа об обучении
	 */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	void recalculateDiplomaLabor(DiplomaContent diplomaContent);

    void deleteRow(Long rowId);

    /**
     * Вычисление номера листа для добавляемой в документ строки.
     * По умолчанию, номер листа приложения из последней строки.
     * Если строка первая в блоке, то номер листа берется такой же как номер листа последней строки предыдущего блока.
     * Если строка первая в документе или первая в блоке "Дисциплины", то номер листа 1.
     * Курсовый и НИР печатаются отдельно. Номера листов у них отдельно ото всех
     *
     * @param rowClass класс блока
     * @param content содержимое
     */
    Integer getListNumberForNewRow(Class<? extends DiplomaContentRow> rowClass, DiplomaContent content);

	/**
	 * Относится ли тип документа об обучении к типу "Справка об обучении" или его наследникам (документы такого типа иногда обрабатываются особым образом)
	 * @param documentType тип документа об обучении
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	boolean isEducationReference(DipDocumentType documentType);

	/**
	 * Список кодов указанного типа элемента реестра и всех подчиненных ему
	 * @param parentTypeCode код родительского типа элемента реестра
	 * @return список, включающий parentTypeCode и коды всех подчиненных типов
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	List<String> regElemTypeWithChildrenCodes(final String parentTypeCode);

	/**
	 * Сбросить значение поля "Строка УП (элемент реестра)" ({@link DiplomaContentRow#epvRegistryRow}) у всех строк всех документов об обучении, связанных с определенной версией УП.
	 * @param eduPlanVersion Версия учебного плана.
	 * @return Было ли действие выполнено успешно.
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	boolean resetEpvRegistryRows(EppEduPlanVersion eduPlanVersion);

	/**
	 * Существуют ли в шаблонах диплома ({@link DiplomaTemplate}) данного УПв строки ({@link DiplomaContentRow}), ссылающиеся на строки этого же УПв ({@link EppEpvRegistryRow}).
	 * @param eduPlanVersion Версия учебного плана.
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	boolean isExistsTemplateRowOfEpv(EppEduPlanVersion eduPlanVersion);

	/**
	 * @return факты выдачи с приложениями для диплома
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	Map<DiplomaIssuance, List<DiplomaContentIssuance>> getDiplomaIssuanceList(DiplomaObject diplom);
}
