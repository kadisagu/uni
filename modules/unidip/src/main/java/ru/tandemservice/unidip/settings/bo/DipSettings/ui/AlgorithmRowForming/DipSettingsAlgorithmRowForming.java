/* $Id$ */
package ru.tandemservice.unidip.settings.bo.DipSettings.ui.AlgorithmRowForming;

import com.google.common.collect.Lists;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unidip.settings.entity.DipFormingRowAlgorithm;
import ru.tandemservice.unidip.settings.entity.DipFormingRowAlgorithmOrgUnit;
import ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 02.11.2015
 */
@Configuration
public class DipSettingsAlgorithmRowForming extends BusinessComponentManager
{
    public static final String ALGORITHM_ROW_DS = "algorithmRowDS";
    public static final String ALGORITHM_ROW_BLOCK_DS = "algorithmRowBlockDS";
    public static final String ALGORITHM_ROW_OU_DS = "algorithmRowOuDS";

    public static final DataWrapper ALGORITHM_DEFAULT = new DataWrapper(0, "По умолчанию");

    public static final String PROP_DIP_FORMING_ROW_ALGORITHM = DipFormingRowAlgorithmOrgUnit.L_DIP_FORMING_ROW_ALGORITHM + "Block";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ALGORITHM_ROW_DS, algorithmRowDS()))
                .addDataSource(selectDS(ALGORITHM_ROW_BLOCK_DS, algorithmRowBlockDS()))
                .addDataSource(searchListDS(ALGORITHM_ROW_OU_DS, algorithmRowOuDSColumns(), algorithmRowOuDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> algorithmRowDS()
    {
        return new EntityComboDataSourceHandler(getName(), DipFormingRowAlgorithm.class)
                .filter(DipFormingRowAlgorithm.title())
                .order(DipFormingRowAlgorithm.code());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> algorithmRowBlockDS()
    {
        return new EntityComboDataSourceHandler(getName(), DataWrapper.class)
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DipFormingRowAlgorithm.class, "a")
                        .order(property("a", DipFormingRowAlgorithm.code()));

                String filter = input.getComboFilterByValue();
                if(StringUtils.isNotEmpty(filter))
                    builder.where(likeUpper(property("a", DipFormingRowAlgorithm.title()), value(CoreStringUtils.escapeLike(filter, true))));

                Set key = input.getPrimaryKeys();
                if (CollectionUtils.isNotEmpty(key))
                {
                    if (key.contains(ALGORITHM_DEFAULT.getId()))
                        return ListOutputBuilder.get(input, Lists.newArrayList(ALGORITHM_DEFAULT)).build();
                    else
                    {
                        builder.where(in(property("a.id"), input.getPrimaryKeys()));
                        DipFormingRowAlgorithm algorithm = builder.createStatement(context.getSession()).uniqueResult();
                        return ListOutputBuilder.get(input, Lists.newArrayList(new DataWrapper(algorithm))).build();
                    }
                }

                List<DataWrapper> resultList = Lists.newArrayList();
                List<DipFormingRowAlgorithm> algorithms = builder.createStatement(context.getSession()).list();

                resultList.add(ALGORITHM_DEFAULT);
                resultList.addAll(algorithms.stream().map(DataWrapper::new).collect(Collectors.toList()));

                return ListOutputBuilder.get(input, resultList).pageable(false).build();
            }
        };
    }

    @Bean
    public ColumnListExtPoint algorithmRowOuDSColumns()
    {
        return columnListExtPointBuilder(ALGORITHM_ROW_OU_DS)
                .addColumn(textColumn(EduOwnerOrgUnit.L_ORG_UNIT, EduOwnerOrgUnit.orgUnit().fullTitle()))
                .addColumn(blockColumn(PROP_DIP_FORMING_ROW_ALGORITHM, PROP_DIP_FORMING_ROW_ALGORITHM))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> algorithmRowOuDSHandler()
    {
        return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EduOwnerOrgUnit.class, "o")
                        .joinEntity("o", DQLJoinType.left, DipFormingRowAlgorithmOrgUnit.class, "aou", eq(property("o", EduOwnerOrgUnit.id()), property("aou", DipFormingRowAlgorithmOrgUnit.ownerOrgUnit().id())))
                        .order(property("o", EduOwnerOrgUnit.orgUnit().fullTitle()));

                List<DataWrapper> resultList = Lists.newArrayList();
                for (Object[] item : dql.createStatement(context.getSession()).<Object[]>list())
                {
                    EduOwnerOrgUnit ownerOrgUnit = (EduOwnerOrgUnit) item[0];
                    DipFormingRowAlgorithmOrgUnit algorithm = (DipFormingRowAlgorithmOrgUnit) item[1];

                    DataWrapper wrapper = new DataWrapper(ownerOrgUnit);
                    wrapper.setProperty(PROP_DIP_FORMING_ROW_ALGORITHM, algorithm);
                    resultList.add(wrapper);
                }
                return ListOutputBuilder.get(input, resultList).pageable(false).build();
            }
        };
    }
}
