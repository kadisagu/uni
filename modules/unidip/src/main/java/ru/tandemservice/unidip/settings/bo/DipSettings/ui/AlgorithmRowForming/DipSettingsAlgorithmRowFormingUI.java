/* $Id$ */
package ru.tandemservice.unidip.settings.bo.DipSettings.ui.AlgorithmRowForming;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unidip.settings.bo.DipSettings.DipSettingsManager;
import ru.tandemservice.unidip.settings.entity.DipFormingRowAlgorithm;
import ru.tandemservice.unidip.settings.entity.DipFormingRowAlgorithmOrgUnit;

/**
 * @author Alexey Lopatin
 * @since 02.11.2015
 */
public class DipSettingsAlgorithmRowFormingUI extends UIPresenter
{
    private DipFormingRowAlgorithm _algorithmRow;

    @Override
    public void onComponentRefresh()
    {
        DipSettingsManager.instance().dao().createAlgorithm4OwnerOrgUnit();
        _algorithmRow = DataAccessServices.dao().get(DipFormingRowAlgorithm.class, DipFormingRowAlgorithm.currentAlg(), Boolean.TRUE);
    }

    public void onClickApply()
    {
        DipSettingsManager.instance().dao().setCurrentRowAlgorithm(_algorithmRow);
    }

    public DataWrapper getCurrentAlgorithmRowOu()
    {
        DipFormingRowAlgorithmOrgUnit algorithmOu = getCurrentRow().get(DipSettingsAlgorithmRowForming.PROP_DIP_FORMING_ROW_ALGORITHM);

        DipFormingRowAlgorithm currentAlg = algorithmOu.getDipFormingRowAlgorithm();
        return currentAlg == null ? DipSettingsAlgorithmRowForming.ALGORITHM_DEFAULT : new DataWrapper(currentAlg);
    }

    public void setCurrentAlgorithmRowOu(DataWrapper currentAlgRow)
    {
        DipFormingRowAlgorithmOrgUnit algorithmOu = getCurrentRow().get(DipSettingsAlgorithmRowForming.PROP_DIP_FORMING_ROW_ALGORITHM);
        DipFormingRowAlgorithm dipFormingRowAlgorithm = currentAlgRow == null || null == currentAlgRow.getWrapped() ? null : currentAlgRow.getWrapped();

        algorithmOu.setDipFormingRowAlgorithm(dipFormingRowAlgorithm);
    }

    public DipFormingRowAlgorithm getAlgorithmRow()
    {
        return _algorithmRow;
    }

    public void setAlgorithmRow(DipFormingRowAlgorithm algorithmRow)
    {
        _algorithmRow = algorithmRow;
    }

    public DataWrapper getCurrentRow()
    {
        return getConfig().getDataSourceCurrentRecord(DipSettingsAlgorithmRowForming.ALGORITHM_ROW_OU_DS);
    }
}
