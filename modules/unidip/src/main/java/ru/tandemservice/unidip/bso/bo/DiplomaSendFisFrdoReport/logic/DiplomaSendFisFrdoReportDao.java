/* $Id$ */
package ru.tandemservice.unidip.bso.bo.DiplomaSendFisFrdoReport.logic;

import com.google.common.collect.*;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreCollectionUtils.Pair;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.fias.base.entity.codes.AddressCountryTypeCodes;
import org.tandemframework.shared.person.base.bo.PersonEduDocument.PersonEduDocumentManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import org.tandemframework.shared.person.catalog.entity.EduDocumentKind;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import org.tandemframework.shared.person.catalog.entity.EducationLevelStage;
import org.tandemframework.shared.person.catalog.entity.codes.EduLevelCodes;
import org.tandemframework.shared.person.catalog.entity.codes.EducationLevelStageCodes;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unidip.base.entity.catalog.DipDocumentType;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContent;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaObject;
import ru.tandemservice.unidip.bso.bo.DiplomaSendFisFrdoReport.ui.Add.DiplomaSendFisFrdoReportAdd;
import ru.tandemservice.unidip.bso.entity.blank.DipDiplomaSendFisFrdoReport;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramFormCodes;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2013;
import ru.tandemservice.uniedu.entity.EduProgramSubjectComparison;

import javax.validation.constraints.NotNull;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 01.04.2015
 */
public class DiplomaSendFisFrdoReportDao extends UniBaseDao implements IDiplomaSendFisFrdoReportDao
{
    private static final Long DOCUMENT_STATUS_ORIGINAL = 0L;
    private static final Long DOCUMENT_STATUS_DUPLICATE = 1L;
    private static final Long DOCUMENT_STATUS_LOST = 2L;

    private static final Map<Long, String> DOCUMENT_STATUS_MAP = Maps.newHashMap();

    static
    {
        DOCUMENT_STATUS_MAP.put(DOCUMENT_STATUS_ORIGINAL, "Оригинал");
        DOCUMENT_STATUS_MAP.put(DOCUMENT_STATUS_DUPLICATE, "Дубликат");
        DOCUMENT_STATUS_MAP.put(DOCUMENT_STATUS_LOST, "Утерян");
    }

	private static final ImmutableList<String> highEducationLevel = ImmutableList.of(
			EduLevelCodes.VYSSHEE_OBRAZOVANIE_BAKALAVRIAT,
			EduLevelCodes.VYSSHEE_OBRAZOVANIE_SPETSIALITET_MAGISTRATURA,
			EduLevelCodes.VYSSHEE_OBRAZOVANIE_PODGOTOVKA_KADROV_VYSSHEY_KVALIFIKATSII);

    @Override
    public Long createReport(@NotNull DipDiplomaSendFisFrdoReport report, @NotNull DataWrapper _eduLevel, List<EduProgramKind> programKindList, List<EduProgramSubject> programSubjectList)
    {
		final boolean isEduLevelVo = (_eduLevel.getId() == DiplomaSendFisFrdoReportAdd.eduLevelVoId);
		List<DiplomaIssuance> resultList = getIssuances(report, programKindList, programSubjectList, isEduLevelVo);

		Multimap<DiplomaObject, DiplomaIssuance> diploma2Issuances = Multimaps.index(DataAccessServices.dao().getList(DiplomaIssuance.class), DiplomaIssuance::getDiplomaObject);

        Map<EduProgramSubject, EduProgramSubject2013> programSubject2013Map = Maps.newHashMap();
        List<EduProgramSubjectComparison> subjectComparisonList = DataAccessServices.dao().getList(EduProgramSubjectComparison.class);
        for (EduProgramSubjectComparison subj : subjectComparisonList)
        {
            programSubject2013Map.put(subj.getOldProgramSubject(), subj.getNewProgramSubject());
        }

        try
        {
            byte[] document = getDocument(resultList, diploma2Issuances, programSubject2013Map, isEduLevelVo);

            DatabaseFile content = new DatabaseFile();
            content.setContent(document);
            content.setFilename("Список дипломов.xls");
            save(content);

            report.setFormingDate(new Date());
            report.setContent(content);
            save(report);
        }
        catch (Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
        return report.getId();
    }

	/**
	 * Получить факты выдачи дипломов для отчета.
	 * @param report Отчет. Факты выдачи фильтруются по его полям Тип документа, Дата выдачи с/по и флагу Дубликат документа.
	 * @param programKinds Выбранные виды ОП.
	 * @param programSubjects Выбранные направления подготовки.
	 * @param isEduLevelVo Уровень образования - высшее.
	 */
	private List<DiplomaIssuance> getIssuances(@NotNull DipDiplomaSendFisFrdoReport report, List<EduProgramKind> programKinds, List<EduProgramSubject> programSubjects, boolean isEduLevelVo)
	{
		DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DiplomaIssuance.class, "di").column(property("di"))
				.joinPath(DQLJoinType.inner, DiplomaIssuance.diplomaObject().fromAlias("di"), "do")
				.joinPath(DQLJoinType.inner, DiplomaObject.content().fromAlias("do"), "dc")
				.joinPath(DQLJoinType.inner, DiplomaContent.programSubject().subjectIndex().programKind().fromAlias("dc"), "pk")
				.where(isNotNull(property("di", DiplomaIssuance.blankSeria())))
				.where(isNotNull(property("di", DiplomaIssuance.blankNumber())))
				.order(property("di", DiplomaIssuance.registrationNumber()));

		// filters
		if (CollectionUtils.isNotEmpty(programKinds))
			builder.where(in(property("pk", EduProgramKind.id()), programKinds));
		else if (isEduLevelVo)
			builder.where(in(property("pk", EduProgramKind.eduLevel().code()), highEducationLevel));
		else
			builder.where(eq(property("pk", EduProgramKind.eduLevel().code()), value(EduLevelCodes.SREDNEE_PROFESSIONALNOE_OBRAZOVANIE)));

		if (CollectionUtils.isNotEmpty(programSubjects))
		{
			builder.where(in(property("dc", DiplomaContent.programSubject()), programSubjects));
		}
		if (null != report.getDocumentType())
		{
			builder.where(or(
					eq(property("dc", DiplomaContent.type()), value(report.getDocumentType())),
					eq(property("dc", DiplomaContent.type().parent()), value(report.getDocumentType()))
			));
		}
		if (null != report.getIssuanceDateFrom())
		{
			builder.where(le(property("di", DiplomaIssuance.issuanceDate()), valueDate(report.getIssuanceDateFrom())));
		}
		if (null != report.getIssuanceDateTo())
		{
			builder.where(ge(property("di", DiplomaIssuance.issuanceDate()), valueDate(report.getIssuanceDateTo())));
		}
		if (report.isDuplicateDiplomaObject())
		{
			builder.where(isNotNull(property("di", DiplomaIssuance.replacedIssuance())));
		}

		return builder.createStatement(getSession()).list();
	}

	private byte[] getDocument(List<DiplomaIssuance> resultList, Multimap<DiplomaObject, DiplomaIssuance> diploma2Issuances, Map<EduProgramSubject, EduProgramSubject2013> programSubject2013Map, boolean isEduLevelVo)
			throws WriteException, IOException
    {
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        WritableFont font = new WritableFont(WritableFont.TIMES, 12);

        WritableCellFormat format = getFormat(font, Alignment.CENTRE, VerticalAlignment.CENTRE, null);
        WritableCellFormat formatRose = getFormat(font, Alignment.CENTRE, VerticalAlignment.CENTRE, Colour.ROSE);
        WritableCellFormat formatIvory = getFormat(font, Alignment.CENTRE, VerticalAlignment.CENTRE, Colour.IVORY);

        WritableCellFormat formatLeft = getFormat(font, Alignment.LEFT, null, null);
        WritableCellFormat formatRoseLeft = getFormat(font, Alignment.LEFT, null, Colour.ROSE);
        WritableCellFormat formatIvoryLeft = getFormat(font, Alignment.LEFT, null, Colour.IVORY);
        WritableCellFormat formatYellow = getFormat(font, Alignment.LEFT, null, Colour.YELLOW);

        WritableWorkbook book = Workbook.createWorkbook(out);
        book.setColourRGB(Colour.ROSE, 255, 204, 204);
        WritableSheet sheet = book.createSheet("Шаблон", 0);

		final int firstRowIdx = 0;
        int rowIdx = firstRowIdx;
        sheet.setRowView(firstRowIdx, 70 * 20);
        for (Map.Entry<Pair<String, Integer>, Colour> entry : createColumns(isEduLevelVo).entrySet())
        {
            Pair<String, Integer> key = entry.getKey();
            Colour colour = entry.getValue();

            sheet.addCell(new Label(rowIdx, 0, key.getX(), selectCellFormatByColour(colour, format, formatRose, formatIvory)));
            sheet.setColumnView(rowIdx++, key.getY());
        }

        List<Integer> countryCodeNear = getCountryCodesByType(AddressCountryTypeCodes.NEAR);
        List<Integer> countryCodeFar = getCountryCodesByType(AddressCountryTypeCodes.FAR);

        rowIdx = firstRowIdx;
        for (DiplomaIssuance dip : resultList)
        {
            Long currentId = dip.getId();

            DiplomaIssuance replaced = dip.getReplacedIssuance();
            DiplomaObject dipObject = dip.getDiplomaObject();
            DiplomaContent content = dipObject.getContent();
            DipDocumentType type = content.getType();
            EduProgramSubject programSubject = content.getProgramSubject();

            Student student = dipObject.getStudent();
            IdentityCard iCard = student.getPerson().getIdentityCard();

            String finishYear = new SimpleDateFormat("yyyy").format(dip.getIssuanceDate());

			final boolean isNotReplaced = (replaced == null);
            Long docStatus = isNotReplaced ? null : DOCUMENT_STATUS_DUPLICATE;
            for (DiplomaIssuance di : diploma2Issuances.get(dipObject))
            {
                if (null == docStatus)
                {
                    DiplomaIssuance diReplaced = di.getReplacedIssuance();
                    docStatus = !currentId.equals(di.getId()) && null != diReplaced && dip.equals(diReplaced) ? DOCUMENT_STATUS_LOST : null;
                }
            }
            if (null == docStatus) docStatus = DOCUMENT_STATUS_ORIGINAL;

            rowIdx++;
            int col = 0;
            sheet.addCell(new Label(col++, rowIdx, "Диплом", formatRoseLeft));
            sheet.addCell(new Label(col++, rowIdx, type.getTitle() + (content.isWithSuccess() ? " с отличием" : ""), formatRoseLeft));
            sheet.addCell(new Label(col++, rowIdx, DOCUMENT_STATUS_MAP.get(docStatus), formatLeft));
            sheet.addCell(new Label(col++, rowIdx, docStatus.equals(DOCUMENT_STATUS_LOST) ? "Да" : "", formatLeft));
            sheet.addCell(new Label(col++, rowIdx, docStatus.equals(DOCUMENT_STATUS_DUPLICATE) ? "Да" : "", formatLeft));
            sheet.addCell(new Label(col++, rowIdx, (programSubject == null) ? "" : programSubject.getSubjectIndex().getProgramKind().getEduLevel().getTitle(), formatRoseLeft));
            sheet.addCell(new Label(col++, rowIdx, StringUtils.trimToEmpty(dip.getBlankSeria()), formatRoseLeft));
            sheet.addCell(new Label(col++, rowIdx, StringUtils.trimToEmpty(dip.getBlankNumber()), formatRoseLeft));
            sheet.addCell(new Label(col++, rowIdx, DateFormatter.DEFAULT_DATE_FORMATTER.format(dip.getIssuanceDate()), formatRoseLeft));
            sheet.addCell(new Label(col++, rowIdx, dip.getRegistrationNumber(), formatLeft));


            if (programSubject != null)
            {
				String programSubjectCode = getProgramSubjectCode(programSubject, programSubject2013Map);
				final boolean subjectCodeEmpty = programSubjectCode == null;
				sheet.addCell(new Label(col++, rowIdx, subjectCodeEmpty ? programSubject.getSubjectCode() : programSubjectCode, subjectCodeEmpty ? formatYellow : formatRoseLeft));
            }
            else
                sheet.addCell(new Label(col++, rowIdx, "", formatRoseLeft));

            sheet.addCell(new Label(col++, rowIdx, (programSubject == null) ? "" : programSubject.getTitle(), formatRoseLeft));
            sheet.addCell(new Label(col++, rowIdx, (content.getProgramQualification() == null) ? "" : content.getProgramQualification().getTitle(), formatRoseLeft));
            sheet.addCell(new Label(col++, rowIdx, (content.getEducationElementTitle() == null) ? "" : content.getEducationElementTitle(), formatLeft));
            sheet.addCell(new Label(col++, rowIdx, String.valueOf(student.getEntranceYear()), formatRoseLeft));
            sheet.addCell(new Label(col++, rowIdx, finishYear, formatRoseLeft));

            sheet.addCell(new Label(col++, rowIdx, String.valueOf(Integer.parseInt(finishYear) - student.getEntranceYear()), formatLeft));

            sheet.addCell(new Label(col++, rowIdx, iCard.getLastName(), formatRoseLeft));
            sheet.addCell(new Label(col++, rowIdx, iCard.getFirstName(), formatRoseLeft));
            sheet.addCell(new Label(col++, rowIdx, iCard.getMiddleName() == null ? "" : iCard.getMiddleName(), formatLeft));
            sheet.addCell(new Label(col++, rowIdx, iCard.getBirthDate() == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(iCard.getBirthDate()), formatRoseLeft));
            sheet.addCell(new Label(col++, rowIdx, iCard.getSex().isMale() ? "Муж" : "Жен", formatRoseLeft));
            if (isEduLevelVo)
			{
                sheet.addCell(new Label(col++, rowIdx, getCitizenship(iCard, countryCodeNear, countryCodeFar), formatRoseLeft));
				final EduProgramForm programForm = student.getEducationOrgUnit().getDevelopForm().getProgramForm();
				sheet.addCell(new Label(col++, rowIdx, programForm.getTitle() + (programForm.getCode().equals(EduProgramFormCodes.OCHNO_ZAOCHNAYA) ? " (вечерняя)" : ""), formatRoseLeft));
                sheet.addCell(new Label(col++, rowIdx, getFirstHighEdu(student), formatRoseLeft));
                sheet.addCell(new Label(col++, rowIdx, getCompensation(student), formatRoseLeft));
            }
			sheet.addCell(new Label(col++, rowIdx, isNotReplaced ? "" : "Диплом", formatIvoryLeft));
            sheet.addCell(new Label(col++, rowIdx, isNotReplaced ? "" : StringUtils.trimToEmpty(replaced.getBlankSeria()), formatIvoryLeft));
            sheet.addCell(new Label(col++, rowIdx, isNotReplaced ? "" : StringUtils.trimToEmpty(replaced.getBlankNumber()), formatIvoryLeft));
            sheet.addCell(new Label(col++, rowIdx, isNotReplaced ? "" : replaced.getRegistrationNumber(), formatIvoryLeft));
            sheet.addCell(new Label(col++, rowIdx, isNotReplaced ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(replaced.getIssuanceDate()), formatIvoryLeft));

            IdentityCard iCardReplaced = isNotReplaced ? null : replaced.getDiplomaObject().getStudent().getPerson().getIdentityCard();
            sheet.addCell(new Label(col++, rowIdx, isNotReplaced ? "" : iCardReplaced.getLastName(), formatIvoryLeft));
            sheet.addCell(new Label(col++, rowIdx, isNotReplaced ? "" : iCardReplaced.getFirstName(), formatIvoryLeft));
            sheet.addCell(new Label(col, rowIdx, isNotReplaced ? "" : iCardReplaced.getMiddleName() == null ? "" : iCardReplaced.getMiddleName(), formatIvoryLeft));
        }

        book.write();
        book.close();
        out.close();

        return out.toByteArray();
    }

	private static WritableCellFormat selectCellFormatByColour(Colour colour, WritableCellFormat format, WritableCellFormat formatRose, WritableCellFormat formatIvory)
	{
		if (colour == null)
			return format;
		if (colour.equals(Colour.ROSE))
			return formatRose;
		if (colour.equals(Colour.IVORY))
			return formatIvory;
		return format;
	}

	/**
	 * Список стран заданного типа ("Ближнее/дальнее зарубежье").
	 * @param countryTypeCode Код типа страны.
	 */
	private List<Integer> getCountryCodesByType(int countryTypeCode)
	{
		final String countryAlias = "country";
		return new DQLSelectBuilder().fromEntity(AddressCountry.class, countryAlias)
				.where(eq(property(countryAlias, AddressCountry.countryType().code()), value(countryTypeCode)))
				.column(property(countryAlias, AddressCountry.code()))
				.createStatement(getSession()).list();
	}

	private static String getProgramSubjectCode(EduProgramSubject programSubject, Map<EduProgramSubject, EduProgramSubject2013> programSubject2013Map)
	{
		if (programSubject.is2013())
			return programSubject.getSubjectCode().replace(".", "");

		if (programSubject.is2009() || programSubject.isOKSO())
		{
			EduProgramSubject2013 subject2013 = programSubject2013Map.get(programSubject);
			return (subject2013 == null) ? null : subject2013.getSubjectCode().replace(".", "");
		}
		return null;
	}

	/**
	 * Гражданство по УЛ - РФ, ближнее/дальнее зарубежье, без гражданства.
	 * @param iCard Удостоверение личности.
	 * @param countryCodeNear Коды стран ближнего зарубежья.
	 * @param countryCodeFar Коды стран дальнего зарубежья.
	 */
	private static String getCitizenship(IdentityCard iCard, List<Integer> countryCodeNear, List<Integer> countryCodeFar)
	{
		int code = iCard.getCitizenship().getCode();
		if (code == 0)
			return "РФ";
		if (countryCodeNear.contains(code))
			return "ближнее (из стран СНГ, Балтии, Грузии, Абхазии и Южной Осетии)";
		if (countryCodeFar.contains(code))
			return "дальнее (других иностранных - кроме стран СНГ, Балтии, Грузии, Абхазии и Южной Осетии)";
		return "лица без гражданства";
	}

	private static final String stringYes = "Да";
	private static final String stringNo = "Нет";

	/**
	 * Первое ли это у студента ВО.
	 * @param student Студент.
	 * @return "Да"/"Нет"/"".
	 */
	private static String getFirstHighEdu(Student student)
	{
		if (PersonEduDocumentManager.isShowNewEduDocuments())
			return getFirstHighEduNewDocuments(student);
		if (PersonEduDocumentManager.isShowLegacyEduDocuments())
			return getFirstHighEduLegacy(student);
		return  "";
	}

	/**
	 * Первое ли это у студента ВО (определяется по новому варианту документов об образовании).
	 * @param student Студент.
	 * @return "Да"/"Нет"/"".
	 */
	private static String getFirstHighEduNewDocuments(Student student)
	{
		if (student.getEduDocument() != null)
			return getFirstHighEduNewDocuments(student.getEduDocument());
		if (student.getPerson().getMainEduDocument() != null)
			return getFirstHighEduNewDocuments(student.getPerson().getMainEduDocument());
		return  "";
	}

	/**
	 * Первое ли это у студента ВО согласно документу об образовании.
	 * @param document Док.образ.
	 * @return "Да"/"Нет".
	 */
	private static String getFirstHighEduNewDocuments(PersonEduDocument document)
	{
		EduDocumentKind docKind = document.getEduDocumentKind();
		EduLevel fixEduLevel = docKind.getFixedEducationLevel();

		if (docKind.isCertifyEducationLevel() && (fixEduLevel != null) && highEducationLevel.contains(fixEduLevel.getCode()))
			return stringNo;
		return stringYes;
	}

	/**
	 * Первое ли это у студента ВО (определяется по старому варианту документов об образовании).
	 * @param student Студент.
	 * @return "Да"/"Нет"/"".
	 */
	private static String getFirstHighEduLegacy(Student student)
	{
		@SuppressWarnings("deprecation")
		final PersonEduInstitution eduInstitution = student.getPerson().getPersonEduInstitution();
		if (eduInstitution != null)
		{
			if (isEduLevelStageOrChildHighProf(eduInstitution.getEducationLevelStage()))
				return stringNo;
			return stringYes;
		}
		return  "";
	}

	/** Вид возмещения затрат студента - целевое/бюджет/платное/непонятное. */
	private static String getCompensation(Student student)
	{
		if (student.isTargetAdmission())
			return "Целевое обучение";

		switch (student.getCompensationType().getCode())
		{
			case CompensationTypeCodes.COMPENSATION_TYPE_BUDGET:
				return "Обучение за счет бюджетных ассигнований";
			case CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT:
				return "Платное обучение";
		}
		return "";
	}

	public static boolean isEduLevelStageOrChildHighProf(EducationLevelStage educationLevelStage)
	{
        //рекурсивно смотрим уровень образования и подтипы на принадлежность к HIGH_PROF
        if(educationLevelStage == null)
            return false;
        if(educationLevelStage.getCode().equals(EducationLevelStageCodes.HIGH_PROF))
            return true;
        return isEduLevelStageOrChildHighProf(educationLevelStage.getParent());
    }

    public Map<Pair<String, Integer>, Colour> createColumns(boolean isEduLevelVo)
    {
        Map<Pair<String, Integer>, Colour> columns = Maps.newLinkedHashMap();

        columns.put(new Pair<>("Название документа", 18), Colour.ROSE);
        columns.put(new Pair<>("Вид документа", 36), Colour.ROSE);
        columns.put(new Pair<>("Статус документа", 12), null);
        columns.put(new Pair<>("Подтверждение утраты", 17), null);
        columns.put(new Pair<>("Подтверждение обмена", 17), null);
        columns.put(new Pair<>("Уровень образования", 37), Colour.ROSE);
        columns.put(new Pair<>("Серия документа", 19), Colour.ROSE);
        columns.put(new Pair<>("Номер документа", 18), Colour.ROSE);
        columns.put(new Pair<>("Дата выдачи", 12), Colour.ROSE);
        columns.put(new Pair<>("Регистрационный номер", 19), null);
        columns.put(new Pair<>("Код специальности, направления подготовки", 17), Colour.ROSE);
        columns.put(new Pair<>("Наименование специальности", 31), Colour.ROSE);
        columns.put(new Pair<>("Наименование квалификации", 31), Colour.ROSE);
        columns.put(new Pair<>("Образовательная программа", 25), null);
        columns.put(new Pair<>("Год поступления", 15), Colour.ROSE);
        columns.put(new Pair<>("Год окончания", 14), Colour.ROSE);
        columns.put(new Pair<>("Срок обучения", 12), null);
        columns.put(new Pair<>("Фамилия получателя", 16), Colour.ROSE);
        columns.put(new Pair<>("Имя получателя", 16), Colour.ROSE);
        columns.put(new Pair<>("Отчество получателя", 16), null);
        columns.put(new Pair<>("Дата рождения получателя", 16), Colour.ROSE);
        columns.put(new Pair<>("Пол получателя", 16), Colour.ROSE);
        if (isEduLevelVo)
		{
            columns.put(new Pair<>("Гражданин иностранного государства", 16), Colour.ROSE);
            columns.put(new Pair<>("Форма обучения", 16), Colour.ROSE);
            columns.put(new Pair<>("Высшее образование, получаемое впервые", 16), Colour.ROSE);
            columns.put(new Pair<>("Источник финансирования обучения", 16), Colour.ROSE);
        }
        columns.put(new Pair<>("Наименование документа об образовании (оригинала)", 18), Colour.IVORY);
        columns.put(new Pair<>("Серия (оригинала)", 14), Colour.IVORY);
        columns.put(new Pair<>("Номер (оригинала)", 13), Colour.IVORY);
        columns.put(new Pair<>("Регистрационный N (оригинала)", 13), Colour.IVORY);
        columns.put(new Pair<>("Дата выдачи (оригинала)", 13), Colour.IVORY);
        columns.put(new Pair<>("Фамилия получателя (оригинала)", 15), Colour.IVORY);
        columns.put(new Pair<>("Имя получателя (оригинала)", 13), Colour.IVORY);
        columns.put(new Pair<>("Отчество получателя (оригинала)", 16), Colour.IVORY);

        return columns;
    }

    private WritableCellFormat getFormat(WritableFont font, Alignment alignment, VerticalAlignment verticalAlignment, Colour colour) throws WriteException
    {
        WritableCellFormat format = new WritableCellFormat(font);

        if (null != verticalAlignment)
            format.setVerticalAlignment(verticalAlignment);
        if (null != colour)
            format.setBackground(colour);

        format.setAlignment(alignment);
        format.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.BLACK);

//        if (!alignment.equals(Alignment.LEFT))
            format.setWrap(true);

        return format;
    }
}
