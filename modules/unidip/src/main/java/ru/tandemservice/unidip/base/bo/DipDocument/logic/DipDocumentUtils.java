/* $Id$ */
package ru.tandemservice.unidip.base.bo.DipDocument.logic;

import com.google.common.collect.ImmutableList;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.person.catalog.entity.EducationLevelStage;
import org.tandemframework.shared.person.catalog.entity.codes.EducationLevelStageCodes;
import ru.tandemservice.unidip.base.entity.catalog.codes.DipDocumentTypeCodes;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaObject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2013;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf;
import ru.tandemservice.uniepp.entity.plan.gen.EppEduPlanProfGen;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;

/**
 * @author Andrey Avetisov
 * @since 12.08.2015
 */

public class DipDocumentUtils
{
    /**
     * @param num количество недель.
     * @return Возвращает переданное значение, округленное до второго знака после запятой, и слово "неделя" в соответстующем числу склонении.
     */
    public static String getWeeksWithUnit(Double num)
    {
        if (num == null) {
            num = 0d;
        }
        return CommonBaseStringUtil.numberWithPostfixCase(num, 2, "неделя", "недели", "недель", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS);
    }

    /**
     * @param num количество недель.
     * @return Возвращает переданное значение, округленное до второго знака после запятой, и слово "неделя" в соответстующем числу склонении, в родительном падеже.
     */
    public static String getWeeksWithUnitInGenitive(Double num)
    {
        if (num == null) {
            num = 0d;
        }
        return CommonBaseStringUtil.numberWithPostfixCase(num, 2, "недели", "недель", "недель", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS);
    }

    /**
     * @param num количество зачетных единиц.
     * @return Возвращает переданное значение, округленное до второго знака после запятой, и словосочетание "зачетной единицы"
     * в соответстующем числу склонении, в родительном падеже.
     */
    public static String getLaborWithUnitInGenitive(Double num)
    {
        if (num == null) {
            num = 0d;
        }
        return CommonBaseStringUtil.numberWithPostfixCase(num, 2, "зачетной единицы", "зачетных единиц", "зачетных единиц", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS);
    }

    /**
     * Проверяет необходимо ли для данного диплома показывать трудоемкость.
     *
     * @param diplomaObject диплом
     * @return показывать/скрывать трудоемкость
     */
    public static boolean isShowLabor(DiplomaObject diplomaObject)
    {
        EduProgramSubject programSubject = diplomaObject.getContent().getProgramSubject();
        return programSubject != null && programSubject.isLabor();
    }

    /**
     * Проверяет является ли переданная ступень (или ее parent'ы) образования -  Средним (полным) общим образованием
     *
     * @param eduStage ступень образования
     * @return true - среднее общее образование, false - любой другой уровень образования
     */
    public static boolean isMiddleSchool(EducationLevelStage eduStage)
    {
        return eduStage.getCode().equals(EducationLevelStageCodes.MIDDLE_SCHOOL)
                || eduStage.getParent() != null && isMiddleSchool(eduStage.getParent());
    }

	private static final ImmutableList<String> dipDocTypesForHigherProf = ImmutableList.of(DipDocumentTypeCodes.BACHELOR_DIPLOMA, DipDocumentTypeCodes.SPECIALIST_DIPLOMA, DipDocumentTypeCodes.MAGISTR_DIPLOMA);

	/**
	 * Получить название квалификации ВПО из диплома. Если тип диплома не относится к дипломам бакалавра, спициалиста или магистра, либо квалификация в УП не указано - пустая строка.
	 * @param diplomaObject Документ об обучении.
	 */
	public static String getHigherProfQualificationTitle(DiplomaObject diplomaObject)
	{
		if (!dipDocTypesForHigherProf.contains(diplomaObject.getContent().getType().getCode()))
			return "";

		EduProgramQualification qualification = diplomaObject.getContent().getProgramQualification();
		return (qualification == null) ? "" : qualification.getTitle();
	}

	/**
	 * Получить название квалификации СПО из диплома. Если квалификация не указана - пустая строка.
	 * @param diplomaObject Документ об обучении.
	 */
	public static String getSecondaryProfQualificationTitle(DiplomaObject diplomaObject)
	{
		EduProgramQualification qualification = diplomaObject.getContent().getProgramQualification();
		return (qualification == null) ? "" : qualification.getTitle();
	}

	/**
	 * Получить название квалификации проф. образования из справки об образовании. Если УП - не проф.образования, направление подготовки не указано или оно - классификатора 2013 года,
	 * либо квалификация не указана - пустая строка.
	 * @param diplomaObject Документ об обучении.
	 */
	public static String getEduCertificateQualificationTitle(DiplomaObject diplomaObject)
	{
		if (diplomaObject.getStudentEpv() == null)
		{
			EduProgramQualification qualification = diplomaObject.getContent().getProgramQualification();
			return (qualification == null) ? "" : qualification.getTitle();
		}

		EppEduPlan eduPlan = diplomaObject.getStudentEpv().getEduPlanVersion().getEduPlan();
		EduProgramSubject programSubject = diplomaObject.getContent().getProgramSubject();
		if ((eduPlan instanceof EppEduPlanProf) && (programSubject != null) && (! (programSubject instanceof EduProgramSubject2013)))
		{
			EduProgramQualification qualification = ((EppEduPlanProf)eduPlan).getProgramQualification();
			return (qualification == null) ? "" : qualification.getTitle();
		}
		return "";
	}

	/**
	 * Получить срок освоения (в виде строки) из УПв студента.
	 * @param studentEpv УПв студента (может быть {@code null}).
	 * @return Если УП - проф. образования, то {@link EppEduPlanProf#getStandardProgramDurationStr}, иначе - пустая строка.
	 */
	private static String getDevelopPeriod(EppStudent2EduPlanVersion studentEpv)
	{
		if (studentEpv == null)
			return "";
		EppEduPlan eduPlan = studentEpv.getEduPlanVersion().getEduPlan();
		return (eduPlan instanceof EppEduPlanProf) ? ((EppEduPlanProf)eduPlan).getStandardProgramDurationStr() : "";
	}

	/**
	 * Получить срок освоения (в виде строки) для диплома о высшем образовании.
	 * @param diplomaObject Диплом о ВО.
	 * @return Если в УП ВО из диплома указаны данные о сроке освоения (по крайней мере {@link EppEduPlanProfGen#getStdProgramDurationYears}), то строка вида "X год/года/лет[opt: Y месяц/месяца/месяцев]"
	 * 			(см. {@link EppEduPlanProf#getStandardProgramDurationStr}). Иначе - в зависимости от типа диплома {@link ru.tandemservice.unidip.base.entity.catalog.DipDocumentType}.
	 */
	public static String getHigherDevelopPeriod(DiplomaObject diplomaObject)
	{
		String developPeriod = getDevelopPeriod(diplomaObject.getStudentEpv());
		if (!developPeriod.isEmpty())
			return developPeriod;

		switch (diplomaObject.getContent().getType().getCode())
		{
			case DipDocumentTypeCodes.BACHELOR_DIPLOMA:
				return "4 года";
			case DipDocumentTypeCodes.SPECIALIST_DIPLOMA:
				return "5 лет";
			case DipDocumentTypeCodes.MAGISTR_DIPLOMA:
				return "2 года";
			case DipDocumentTypeCodes.POSTGRADUATE_DIPLOMA:
			case DipDocumentTypeCodes.ADUNCTURE_DIPLOMA_:
				return "3 года";
		}
		return "";
	}

	/**
	 * Получить срок освоения (в виде строки) для диплома НЕ о высшем образовании.
	 * @param diplomaObject Диплом.
	 * @return Если в УП ВО из диплома указаны данные о сроке освоения (по крайней мере {@link EppEduPlanProfGen#getStdProgramDurationYears}), то строка вида "X год/года/лет[opt: Y месяц/месяца/месяцев]"
	 * 			(см. {@link EppEduPlanProf#getStandardProgramDurationStr}). Иначе - пустая строка.
	 */
	public static String getDevelopPeriod(DiplomaObject diplomaObject)
	{
		return getDevelopPeriod(diplomaObject.getStudentEpv());
	}

	/**
	 * Получить значение метки печати {@code "degreeDifferences"} для диплома о ВО.
	 * @param diplomaObject Диплом.
	 * @return В зависимости от типа диплома {@link ru.tandemservice.unidip.base.entity.catalog.DipDocumentType} значения "бакалавра"/"специалиста"/"магистра" (при наличии соотв. признака добавляется также "с отличием"),
	 * 			либо "об окончании аспирантуры"/"об окончании адъюнктуры" для соответствующих типов диплома. Для остальных типов - пустая строка.
	 */
	public static String getHigherDegreeDiff(DiplomaObject diplomaObject)
	{
		final boolean withSuccess = diplomaObject.getContent().isWithSuccess();
		switch (diplomaObject.getContent().getType().getCode())
		{
			case DipDocumentTypeCodes.BACHELOR_DIPLOMA:
				return withSuccess ? "бакалавра с отличием" : "бакалавра";
			case DipDocumentTypeCodes.SPECIALIST_DIPLOMA:
				return withSuccess ? "специалиста с отличием" : "специалиста";
			case DipDocumentTypeCodes.MAGISTR_DIPLOMA:
				return withSuccess ? "магистра с отличием" : "магистра";
			case DipDocumentTypeCodes.POSTGRADUATE_DIPLOMA:
				return "об окончании аспирантуры";
			case DipDocumentTypeCodes.ADUNCTURE_DIPLOMA_:
				return "об окончании адъюнктуры";
		}
		return "";
	}
}
