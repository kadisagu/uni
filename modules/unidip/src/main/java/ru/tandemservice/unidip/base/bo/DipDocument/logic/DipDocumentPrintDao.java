/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unidip.base.bo.DipDocument.logic;

import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.unidip.base.entity.catalog.DipDocTemplateCatalog;
import ru.tandemservice.unidip.base.entity.catalog.DipDocumentType;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaObject;

/**
 * @author iolshvang
 * @since 30.08.11 13:18
 */
public class DipDocumentPrintDao extends CommonDAO implements IDiplomaPrintDao
{
    @Override
    public RtfDocument printDocument(DiplomaObject document)
    {
        return null;
    }
}
