/* $Id$ */
package ru.tandemservice.unidip.base.bo.DipDocument.ui.Print;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.RtfParagraph;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfElementFactory;
import org.tandemframework.rtf.node.RtfControl;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.unidip.base.entity.catalog.DipDocTemplateCatalog;
import ru.tandemservice.unidip.base.entity.catalog.codes.DipDocumentTypeCodes;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentIssuance;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Andrey Avetisov
 * @since 26.05.2014
 */

@Input({
               @Bind(key = DipDocumentPrintUI.DIP_DOCUMENT_ID, binding = "diplomaObject.id"),
               @Bind(key = DipDocumentPrintUI.DIPLOMA_ISSUANCE_ID, binding = "diplomaIssuance.id"),
               @Bind(key = DipDocumentPrintUI.DIPLOMA_ISSUANCE_DISABLED, binding = "diplomaIssuanceDisabled"),

       })

public class DipDocumentPrintUI extends UIPresenter
{
    public static final String DIP_DOCUMENT_ID = "dipDocumentId";
    public static final String DIPLOMA_ISSUANCE_ID = "diplomaIssuanceId";
    public static final String WRAPPED_CONTENT_ISSUANCE = "wrappedContentIssuance";
    public static final String WRAPPED_TEMPLATE = "wrappedTemplate";
    public static final String DIPLOMA_ISSUANCE_DISABLED = "diplomaIssuanceDisabled";

    private DipDocTemplateCatalog _titleTemplate;
    private DiplomaIssuance _diplomaIssuance = new DiplomaIssuance();
    private DiplomaContentIssuance _diplomaContentIssuance;
    private boolean _diplomaIssuanceDisabled;

    private DiplomaObject _diplomaObject = new DiplomaObject();

    private List<DataWrapper> _applicationWrappedList;
    private IUIDataSource _applicationDS;
    private DipDocTemplateCatalog _applicationTemplate;

    private Long _id = 0L;

    public Long getNextId()
    {
        return _id++;
    }

    @Override
    public void onComponentRefresh()
    {
        setDiplomaObject(DataAccessServices.dao().getNotNull(DiplomaObject.class, getDiplomaObject().getId()));
        setApplicationDS(getConfig().getDataSource(DipDocumentPrint.DIP_APP_TEMPLATE_DS));

        setApplicationWrappedList(new ArrayList<>());
        if (getDiplomaIssuance() != null && getDiplomaIssuance().getId() != null)
        {
            _diplomaIssuance = DataAccessServices.dao().get(DiplomaIssuance.class, DiplomaIssuance.P_ID, getDiplomaIssuance().getId());
            List<DiplomaContentIssuance> contentIssuanceList = DataAccessServices.dao().getList(DiplomaContentIssuance.class, DiplomaContentIssuance.L_DIPLOMA_ISSUANCE, getDiplomaIssuance());
            for (DiplomaContentIssuance contentIssuance : contentIssuanceList)
            {
                DataWrapper wrapper = new DataWrapper();
                wrapper.put(WRAPPED_CONTENT_ISSUANCE, contentIssuance);
                wrapper.setId(getNextId());
                getApplicationWrappedList().add(wrapper);
            }

        }
        else
            setDiplomaIssuance(null);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (DipDocumentPrint.TITLE_TEMPLATE_DS.equals(dataSource.getName()) || DipDocumentPrint.APPLICATION_TEMPLATE_DS.equals(dataSource.getName()))
        {
            dataSource.put(DipDocumentPrint.DIP_DOC_TYPE_PARAM, getDiplomaObject().getContent().getType());
            dataSource.put(DipDocumentPrint.WITH_SUCCESS_PARAM, getDiplomaObject().getContent().isWithSuccess());
        }
        if (DipDocumentPrint.ISSUANCE_DS.equals(dataSource.getName()))
        {
            dataSource.put(DipDocumentPrint.DIPLOMA_OBJECT_PARAM, getDiplomaObject().getId());
        }

        if (DipDocumentPrint.DIPLOMA_CONTENT_ISSUANCE_DS.equals(dataSource.getName()))
        {
            dataSource.put(DipDocumentPrint.DIPLOMA_ISSUANCE_PARAM, getDiplomaIssuance());
        }

        if (DipDocumentPrint.DIP_APP_TEMPLATE_DS.equals(dataSource.getName()))
        {
            dataSource.put(DipDocumentPrint.DIP_CONTENT_ISSUANCE_LIST, getApplicationWrappedList());
        }
    }

    public DiplomaIssuance getDiplomaIssuance()
    {
        return _diplomaIssuance;
    }

    public void setDiplomaIssuance(DiplomaIssuance diplomaIssuance)
    {
        _diplomaIssuance = diplomaIssuance;
    }

    public DipDocTemplateCatalog getTitleTemplate()
    {
        return _titleTemplate;
    }

    public void setTitleTemplate(DipDocTemplateCatalog titleTemplate)
    {
        _titleTemplate = titleTemplate;
    }

    public DiplomaObject getDiplomaObject()
    {
        return _diplomaObject;
    }

    public void setDiplomaObject(DiplomaObject diplomaObject)
    {
        _diplomaObject = diplomaObject;
    }

    public DiplomaContentIssuance getDiplomaContentIssuance()
    {
        return _diplomaContentIssuance;
    }

    public void setDiplomaContentIssuance(DiplomaContentIssuance diplomaContentIssuance)
    {
        _diplomaContentIssuance = diplomaContentIssuance;
    }

    public List<DataWrapper> getApplicationWrappedList()
    {
        return _applicationWrappedList;
    }

    public void setApplicationWrappedList(List<DataWrapper> applicationWrappedList)
    {
        _applicationWrappedList = applicationWrappedList;
    }

    public boolean isDiplomaIssuanceDisabled()
    {
        return _diplomaIssuanceDisabled;
    }

    public void setDiplomaIssuanceDisabled(boolean diplomaIssuanceDisabled)
    {
        _diplomaIssuanceDisabled = diplomaIssuanceDisabled;
    }

    public String getSelectTemplateTitle ()
    {
        return getDiplomaObject().getContent().getType().getCode().equals(DipDocumentTypeCodes.EDUCATION_REFERENCE) ? "Печатный шаблон" : "Печатный шаблон титула документа";
    }

    public boolean isEducationReference()
    {
        return getDiplomaObject().getContent().getType().getCode().equals(DipDocumentTypeCodes.EDUCATION_REFERENCE);
    }

    public String getApplicationTitle()
    {
        DataWrapper currentWrapper = getCurrentRow();
        DiplomaContentIssuance currentContentIssuance = currentWrapper.get(WRAPPED_CONTENT_ISSUANCE);
        return diplomaContentIssuanceFormatter().format(currentContentIssuance);

    }

    public DipDocTemplateCatalog getApplicationTemplate()
    {
        return _applicationTemplate;
    }

    public void setApplicationTemplate(DipDocTemplateCatalog applicationTemplate)
    {
        DataWrapper currentWrapper = getApplicationWrappedList().stream().filter(dataWrapper -> dataWrapper.getId().equals(getCurrentRow().getId())).findFirst().get();
        currentWrapper.setProperty(WRAPPED_TEMPLATE, applicationTemplate);
    }

    public String getApplicationTemplateTitle()
    {
        DataWrapper currentWrapper = getCurrentRow();
        DipDocTemplateCatalog templateCatalog = currentWrapper.get(WRAPPED_TEMPLATE);
        if (templateCatalog != null)
            return templateCatalog.getTitle();
        else
            return null;
    }

    public IUIDataSource getApplicationDS()
    {
        return _applicationDS;
    }

    public void setApplicationDS(IUIDataSource applicationDS)
    {
        _applicationDS = applicationDS;
    }

    public DataWrapper getCurrentRow()
    {
        return getApplicationDS().getCurrent();
    }

    public void onClickPrint()
    {
        byte[] userTemplate = null;
        //Проверяем есть ли пользовательский шаблон, если есть, то распечатываем его.
        if (getTitleTemplate() != null && getTitleTemplate().getUserTemplate() != null)
            userTemplate = getTitleTemplate().getUserTemplate();

        List<DiplomaContentIssuance> contentIssuanceList = new ArrayList<>();
        for (DataWrapper wrapper : getApplicationWrappedList())
        {
            contentIssuanceList.add(wrapper.get(WRAPPED_CONTENT_ISSUANCE));
        }
        List<RtfDocument> printDocList = new ArrayList<>();


        if (getTitleTemplate() != null)
        {
            Map<String, Object> titleTemplate = CommonManager.instance().scriptDao().getScriptResult(getTitleTemplate(),
                                                                                                     IScriptExecutor.TEMPLATE_VARIABLE, userTemplate,
                                                                                                     "diplomaObjectId", getDiplomaObject().getId(),
                                                                                                     "diplomaIssuance", getDiplomaIssuance());
            printDocList.add((RtfDocument) titleTemplate.get("document"));
        }
        //Если в searchList выбран шаблон, то распечатываем его в том же документе
        for (DataWrapper wrapper : getApplicationWrappedList())
        {
            if (wrapper.get(WRAPPED_TEMPLATE) != null)
            {
                DipDocTemplateCatalog scriptItem = wrapper.get(WRAPPED_TEMPLATE);
                byte[] content = scriptItem.getUserTemplate() != null ? scriptItem.getUserTemplate() : scriptItem.getContent();

                Map<String, Object> contentIssuanceTemplate = CommonManager.instance().scriptDao().getScriptResult(scriptItem,
                                                                                                                   IScriptExecutor.TEMPLATE_VARIABLE, content,
                                                                                                                   "diplomaObjectId", getDiplomaObject().getId(),
                                                                                                                   "contentIssuance", wrapper.get(WRAPPED_CONTENT_ISSUANCE),
                                                                                                                   "contentIssuanceList", contentIssuanceList,
                                                                                                                   "diplomaIssuance", getDiplomaIssuance());
                printDocList.add((RtfDocument) contentIssuanceTemplate.get("document"));
            }
        }


        for (RtfDocument rtfDocument : printDocList)
        {
            if (printDocList.indexOf(rtfDocument) > 0)
            {
                addList(printDocList.get(0), rtfDocument);
            }
        }


        if (CollectionUtils.isEmpty(printDocList))
        {
            throw new ApplicationException("Необходимо выбрать хотя бы один печатный шаблон: либо титула документа, либо приложения.");
        }
        else
        {
            BusinessComponentUtils.downloadDocument(
                    new CommonBaseRenderer()
                            .document(RtfUtil.toByteArray(printDocList.get(0)))
                            .fileName("document.rtf")
                            .rtf(),
                    false);
        }
    }


    private void addList(RtfDocument titleDoc, RtfDocument applicationDoc)
    {
        IRtfElementFactory elementFactory = RtfBean.getElementFactory();
        IRtfControl startSection = elementFactory.createRtfControl(IRtfData.SECT);
        IRtfControl endSection = elementFactory.createRtfControl(IRtfData.SECTD);
        RtfUtil.modifySourceList(titleDoc.getHeader(), applicationDoc.getHeader(), applicationDoc.getElementList());


        titleDoc.addElement(startSection);
        titleDoc.addElement(endSection);

        int margTop = applicationDoc.getSettings().getMarginTop();
        int margBottom = applicationDoc.getSettings().getMarginBottom();
        int margRight = applicationDoc.getSettings().getMarginRight();
        int margLeft = applicationDoc.getSettings().getMarginLeft();
        int height = applicationDoc.getSettings().getPaperHeight();
        int width = applicationDoc.getSettings().getPaperWidth();

        for (IRtfElement element : applicationDoc.getElementList())
        {
            titleDoc.addElement(element);


            if (element instanceof RtfParagraph)
            {
                for (IRtfElement item : ((RtfParagraph) element).getElementList())
                {
                    if (item instanceof RtfControl)
                    {
                        RtfControl control = (RtfControl) item;
                        if (control.getIndex() == IRtfData.MARGLSXN)
                        {
                            margLeft = control.getValue() > 0 ? control.getValue() : margLeft;
                        }
                        if (control.getIndex() == IRtfData.MARGRSXN)
                        {
                            margRight = control.getValue() > 0 ? control.getValue() : margRight;
                        }
                        if (control.getIndex() == IRtfData.MARGTSXN)
                        {
                            margTop = control.getValue() > 0 ? control.getValue() : margTop;
                        }
                        if (control.getIndex() == IRtfData.MARGBSXN)
                        {
                            margBottom = control.getValue() > 0 ? control.getValue() : margBottom;
                        }

                        if (control.getIndex() == IRtfData.PAPERW)
                        {
                            width = control.getValue() > 0 ? control.getValue() : width;
                        }

                        if (control.getIndex() == IRtfData.PAPERH)
                        {
                            height = control.getValue() > 0 ? control.getValue() : height;
                        }

                    }
                }
            }

            titleDoc.addElement(elementFactory.createRtfControl(IRtfData.PGWSXN, applicationDoc.getSettings().getPaperWidth()));
            titleDoc.addElement(elementFactory.createRtfControl(IRtfData.PGHSXN, applicationDoc.getSettings().getPaperHeight()));

            titleDoc.addElement(elementFactory.createRtfControl(IRtfData.MARGLSXN, margLeft));
            titleDoc.addElement(elementFactory.createRtfControl(IRtfData.MARGRSXN, margRight));
            titleDoc.addElement(elementFactory.createRtfControl(IRtfData.MARGTSXN, margTop));
            titleDoc.addElement(elementFactory.createRtfControl(IRtfData.MARGBSXN, margBottom));


        }
    }

    public void updateApplicationList()
    {
        setApplicationWrappedList(new ArrayList<>());
        if (getDiplomaIssuance() != null)
        {
            List<DiplomaContentIssuance> contentIssuanceList = DataAccessServices.dao().getList(DiplomaContentIssuance.class, DiplomaContentIssuance.L_DIPLOMA_ISSUANCE, getDiplomaIssuance());
            for (DiplomaContentIssuance contentIssuance : contentIssuanceList)
            {
                DataWrapper wrapper = new DataWrapper();
                wrapper.put(WRAPPED_CONTENT_ISSUANCE, contentIssuance);
                wrapper.setId(getNextId());
                getApplicationWrappedList().add(wrapper);
            }
        }
    }

    private IFormatter<DiplomaContentIssuance> diplomaContentIssuanceFormatter()

    {
        return source -> {

            DiplomaContentIssuance diplomaContentIssuance = (DiplomaContentIssuance) source;
            String result = "Приложение №" + diplomaContentIssuance.getContentListNumber() + " " + diplomaContentIssuance.getBlankSeria() +
                    " №" + diplomaContentIssuance.getBlankNumber();

            if (diplomaContentIssuance.isDuplicate())
            {
                result += " (дубликат - рег. №" + diplomaContentIssuance.getDuplicateRegustrationNumber() +
                        " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(diplomaContentIssuance.getDuplicateIssuanceDate()) + ")";
            }
            return result;
        };
    }

}
