/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unidip.component.catalog.dipDocTemplateCatalog.DipDocTemplateCatalogAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.meta.entity.data.IdValue;
import org.tandemframework.core.meta.entity.data.ItemPolicy;
import org.tandemframework.core.runtime.EntityDataRuntime;
import org.tandemframework.core.tool.synchronization.SynchronizeMeta;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.ui.DynamicPub.IDynamicCatalogDefines;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultScriptCatalogAddEdit.DefaultScriptCatalogAddEditModel;
import ru.tandemservice.unidip.base.entity.catalog.DipDocTemplateCatalog;
import ru.tandemservice.unidip.base.entity.catalog.DipDocumentType;

import java.util.List;

/**
 * @author vip_delete
 * @since 13.01.2009
 */
@Input({
        @Bind(key = "useUserTemplate", binding = "useUserTemplate"),
        @Bind(key = "useUserScript", binding = "useUserScript")
})
public class Model extends DefaultScriptCatalogAddEditModel<DipDocTemplateCatalog>
{
    private List<DipDocumentType> _dipDocumentTypeList;
    private List<DipDocTemplateCatalog> _dipDocTemplateCatalogListForCopy;
    private DipDocTemplateCatalog _dipDocTemplateCatalogItemForCopy;

    @Override
    public boolean isTitleDisabled()
    {
        IdValue id = new IdValue();
        id.setPropertyValue(IDynamicCatalogDefines.CATALOG_CODE, DipDocTemplateCatalog.DEFAULT_CATALOG_CODE);
        id.setPropertyValue(DipDocTemplateCatalog.CATALOG_ITEM_CODE, getCatalogItem().getCode());

        ItemPolicy itemPolicy = EntityDataRuntime.getEntityPolicy(DipDocTemplateCatalog.ENTITY_NAME).getItemPolicyById(id);
        return itemPolicy != null && SynchronizeMeta.system == itemPolicy.getPropertyPolicy(DipDocTemplateCatalog.P_TITLE).getSynchronize();
    }

    public boolean isUserScriptAllowed()
    {
        return getDipDocTemplateCatalogItemForCopy() != null || isEditForm();
    }

    public boolean isHasTemplateForCopy()
    {
        return _dipDocTemplateCatalogItemForCopy != null;
    }

    public List<DipDocumentType> getDipDocumentTypeList()
    {
        return _dipDocumentTypeList;
    }

    public void setDipDocumentTypeList(List<DipDocumentType> dipDocumentTypeList)
    {
        _dipDocumentTypeList = dipDocumentTypeList;
    }

    public DipDocTemplateCatalog getDipDocTemplateCatalogItemForCopy()
    {
        return _dipDocTemplateCatalogItemForCopy;
    }

    public void setDipDocTemplateCatalogItemForCopy(DipDocTemplateCatalog dipDocTemplateCatalogItemForCopy)
    {
        _dipDocTemplateCatalogItemForCopy = dipDocTemplateCatalogItemForCopy;
    }

    public List<DipDocTemplateCatalog> getDipDocTemplateCatalogListForCopy()
    {
        return _dipDocTemplateCatalogListForCopy;
    }

    public void setDipDocTemplateCatalogListForCopy(List<DipDocTemplateCatalog> dipDocTemplateCatalogListForCopy)
    {
        _dipDocTemplateCatalogListForCopy = dipDocTemplateCatalogListForCopy;
    }
}