/* $Id$ */
package ru.tandemservice.unidip.base.ext.OrgUnit.ui.View;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.View.OrgUnitView;
import ru.tandemservice.unidip.base.bo.DipOrgUnit.ui.Tab.DipOrgUnitTab;

/**
 * @author Alexey Lopatin
 * @since 29.05.2015
 */
@Configuration
public class OrgUnitViewExt extends BusinessComponentExtensionManager
{
    public static final String TAB_DIP_ORG_UNIT = "dipOrgUnitTab";

    @Autowired
    public OrgUnitView _orgUnitView;

    @Bean
    public TabPanelExtension tabPanelExtension()
    {
        return tabPanelExtensionBuilder(_orgUnitView.orgUnitTabPanelExtPoint())
                .addTab(componentTab(TAB_DIP_ORG_UNIT, DipOrgUnitTab.class).permissionKey("ui:secModel.orgUnit_viewDipTab").parameters("mvel:['publisherId':presenter.orgUnit.id]"))
                .create();
    }
}
