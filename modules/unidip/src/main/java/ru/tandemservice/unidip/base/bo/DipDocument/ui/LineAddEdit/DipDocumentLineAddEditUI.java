/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unidip.base.bo.DipDocument.ui.LineAddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.commonbase.dao.SharedBaseDao;
import org.tandemframework.tapsupport.component.selection.ListResult;
import org.tandemframework.tapsupport.component.selection.SingleSelectTextModel;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unidip.base.bo.DipDocument.DipDocumentManager;
import ru.tandemservice.unidip.base.bo.DipDocument.logic.DipEpvRegistryRowDSHandler;
import ru.tandemservice.unidip.base.bo.DipDocument.logic.RegistryPartComboBoxDSHandler;
import ru.tandemservice.unidip.base.bo.DiplomaTemplate.DiplomaTemplateManager;
import ru.tandemservice.unidip.base.bo.DiplomaTemplate.logic.RegElemPartDSHandler;
import ru.tandemservice.unidip.base.entity.diploma.*;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.registry.EppRegistryAction;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author iolshvang
 * @since 08.08.11 13:04
 */
@Input({
               @Bind(key = DipDocumentLineAddEditUI.DIP_DOCUMENT_ID, binding = "diplomaObject.id"),
               @Bind(key = DipDocumentLineAddEditUI.ROW_CLASS_BIND, binding = "rowClass"),
               @Bind(key = DipDocumentLineAddEditUI.DIP_DOCUMENT_LINE_ID, binding = "rowId")
       })
public class DipDocumentLineAddEditUI extends UIPresenter
{
    public static final String DIP_DOCUMENT_ID = "dipDocumentId";
    public static final String ROW_CLASS_BIND = "rowClass";
    public static final String DIP_DOCUMENT_LINE_ID = "dipDocumentLineId";

    public static final String SLOT_SOURCE = "МСРП студента";
    public static final String REGISTRY_SOURCE = "Реестр дисциплин";

    private String[] _sourceList = new String[]{SLOT_SOURCE, REGISTRY_SOURCE};
    private String _currentSourceValue;

    private boolean _editForm;
    private DiplomaObject _diplomaObject = new DiplomaObject();
    private Long _rowId;
    private DiplomaContentRow _row;
    private Class<? extends DiplomaContentRow> _rowClass;

    private Double _rowLoad;
    private DataWrapper _mark;
    private SingleSelectTextModel _eduOrganizationModel;

    private List<DataWrapper> _regElemWrappedList;
    private DataWrapper _currentEditRow;
    private IUIDataSource _regElemDS;
    private DataWrapper _regElem;
    private DataWrapper _regElemOld;
    private DataWrapper _partFControlAction;
    private DataWrapper _partFControlActionOld;

    private Long _id = 0L;

    public static final String BIND_SOURCE = RegistryPartComboBoxDSHandler.SOURCE;
    public static final String BIND_REGISTRY_PART = RegistryPartComboBoxDSHandler.REGISTRY_PART;
    public static final String BIND_PART_FCA = "elemCA";
    public static final String BIND_REL = "rel";

    public Long getNextId()
    {
        return _id++;
    }

    @Override
    public void onComponentRefresh()
    {
        _diplomaObject = DataAccessServices.dao().getNotNull(DiplomaObject.class, DiplomaObject.id(), _diplomaObject.getId());
        setRegElemDS(getConfig().getDataSource(DipDocumentLineAddEdit.REG_ELEM_PART_DS));
        _regElemWrappedList = new ArrayList<>();
        setCurrentEditRow(null);

        _editForm = null != getRowId();
        if (_editForm)
        {
            setRow(DataAccessServices.dao().getNotNull(DiplomaContentRow.class, DiplomaContentRow.id(), getRowId()));
            setRowClass(getRow().getClass());

            List<DiplomaContentRegElPartFControlAction> regElemPartList = DataAccessServices.dao().getList(
                    DiplomaContentRegElPartFControlAction.class, DiplomaContentRegElPartFControlAction.row(), getRow(),
                    DiplomaContentRegElPartFControlAction.registryElementPartFControlAction().part().registryElement().title().s(),
                    DiplomaContentRegElPartFControlAction.registryElementPartFControlAction().part().number().s()
            );

            for (DiplomaContentRegElPartFControlAction item : regElemPartList)
            {
                DataWrapper wrapper = new DataWrapper(getNextId(), item.getRegistryElementPartFControlAction().getPart().getTitleWithNumber(), item.getRegistryElementPartFControlAction().getPart());
                wrapper.put(BIND_REL, item);
                wrapper.put(BIND_PART_FCA, item.getRegistryElementPartFControlAction());
                _regElemWrappedList.add(wrapper);
            }

            final Long markId = DipDocumentLineAddEdit.MARK_MAP.inverse().get(StringUtils.lowerCase(getRow().getMark()));
            if (markId != null) {
                _mark = new DataWrapper(markId, StringUtils.lowerCase(getRow().getMark()));
            }
        }
        else
        {
            final DiplomaContentRow newRow;
            try
            {
                newRow = getRowClass().newInstance();
                newRow.setContentListNumber(DipDocumentManager.instance().dao().getListNumberForNewRow(getRowClass(), getDiplomaObject().getContent()));
                newRow.setOwner(getDiplomaObject().getContent());
                if (newRow instanceof DiplomaQualifWorkRow) {
                    newRow.setTheme(getDiplomaObject().getStudent().getFinalQualifyingWorkTheme());
                }
            }
            catch (InstantiationException | IllegalAccessException e) {
                throw CoreExceptionUtils.getRuntimeException(e);
            }
            setRow(newRow);
        }

        setRowLoad(getRow().getCommonLoad());
        _eduOrganizationModel = new SingleSelectTextModel()
        {
            @Override
            public ListResult findValues(final String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DiplomaContentRow.class, "r")
                        .predicate(DQLPredicateType.distinct)
                        .column(property("r", DiplomaContentRow.education()))
                        .where(eq(property("r", DiplomaContentRow.owner()), value(getDiplomaObject().getContent())))
                        .order(property("r", DiplomaContentRow.education()));

                FilterUtils.applySimpleLikeFilter(builder, "r", DiplomaContentRow.education(), filter);

                return new DQLListResultBuilder(builder).findOptions();
            }
        };
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(RegistryPartComboBoxDSHandler.DIPLOMA_OBJECT, _diplomaObject);
        switch (dataSource.getName())
        {
            case DipDocumentLineAddEdit.EDU_ORGANIZATION_DS:
                dataSource.put(DipDocumentLineAddEdit.DIPLOMA_CONTENT, _diplomaObject.getContent());
                break;
            case DipDocumentLineAddEdit.REG_ELEM_PART_DS:
                dataSource.put(RegElemPartDSHandler.REG_ELEM_PART_LIST, _regElemWrappedList);
                break;
            case DipDocumentLineAddEdit.REG_ELEM_PART_COMBO_BOX_DS:
                dataSource.put(RegistryPartComboBoxDSHandler.SOURCE, _currentSourceValue);
                dataSource.put(RegistryPartComboBoxDSHandler.CURRENT_PART_FCA, getPartFControlAction() != null ? getPartFControlAction().getWrapped() : null);
                dataSource.put(RegistryPartComboBoxDSHandler.USED_PART_F_CONTROL_ACTION, getUsedElementPartFControlAction());
                break;
            case DipDocumentLineAddEdit.CONTROL_ACTION_COMBO_BOX_DS:
                dataSource.put(RegistryPartComboBoxDSHandler.REGISTRY_PART, getRegElem() != null ? getRegElem().getWrapped() : null);
                dataSource.put(RegistryPartComboBoxDSHandler.SOURCE, _currentSourceValue);
                dataSource.put(RegistryPartComboBoxDSHandler.CURRENT_PART_FCA, getPartFControlAction() != null ? getPartFControlAction().getWrapped() : null);
                dataSource.put(RegistryPartComboBoxDSHandler.USED_PART_F_CONTROL_ACTION, getUsedElementPartFControlAction());
                break;
            case DipDocumentLineAddEdit.EPV_REGISTRY_ROW_DS:
                dataSource.put(DipEpvRegistryRowDSHandler.EDU_PLAN_VERSION, _diplomaObject.getStudentEpv().getEduPlanVersion().getId());
                dataSource.put(DipEpvRegistryRowDSHandler.REG_STRUCTURE_CODES, getRow().getRelatedRegistryElementTypeCodes());
                break;
        }
    }

    //Getters & Setters
    public Integer getTerm()
    {
        return getRow().getTerm() == 0 ? null : getRow().getTerm();
    }

    public void setTerm(Integer value)
    {
        getRow().setTerm(value == null ? 0 : value);
    }

    private List<EppRegistryElementPartFControlAction> getUsedElementPartFControlAction()
    {
        final List<EppRegistryElementPartFControlAction> partFControlActionList = new ArrayList<>();
        for (DataWrapper wrapper : _regElemWrappedList)
        {
            EppRegistryElementPartFControlAction regElPartFControlAction = wrapper.get(BIND_PART_FCA);
            if (regElPartFControlAction != null && !wrapper.getId().equals(getCurrentEditRow().getId()))
                partFControlActionList.add(regElPartFControlAction);
        }
        return partFControlActionList;
    }

    public boolean isEditForm()
    {
        return _editForm;
    }

	public boolean isEmptyEpv()
	{
		return _diplomaObject.getStudentEpv() == null;
	}

    public boolean isAddApplicationButtonDisabled()
    {
        return getCurrentEditRow() != null;
    }

    public DiplomaObject getDiplomaObject()
    {
        return _diplomaObject;
    }

    public void setDiplomaObject(DiplomaObject diplomaObject)
    {
        _diplomaObject = diplomaObject;
    }

    public DiplomaContentRow getRow()
    {
        return _row;
    }

    public void setRow(DiplomaContentRow row)
    {
        _row = row;
    }

    public String getRowLoadTitle()
    {
        return getRow().getRowLoadTitle();
    }

    public Double getRowLoad()
    {
        return _rowLoad;
    }

    public void setRowLoad(Double rowLoad)
    {
        _rowLoad = rowLoad;
    }

    public void setMark(DataWrapper mark)
    {
        _mark = mark;
    }

    public DataWrapper getMark()
    {
        return _mark;
    }

    public boolean isThemeVisible()
    {
        return _row instanceof DiplomaCourseWorkRow || _row instanceof DiplomaQualifWorkRow;
    }

    public SingleSelectTextModel getEduOrganizationModel()
    {
        return _eduOrganizationModel;
    }

    public void setEduOrganizationModel(SingleSelectTextModel eduOrganizationModel)
    {
        _eduOrganizationModel = eduOrganizationModel;
    }

    public String getRegElemPartTitle()
    {
        final EppRegistryElementPartFControlAction fca = getCurrentRow().get(BIND_PART_FCA);
        return fca.getPart().getTitleWithNumber() + " [" + fca.getControlAction().getTitle() + "]";
    }

    public String getFormattedLoad()
    {
        //Считаем нагрузку для частей реестра
        EppRegistryElementPart elementPart = ((EppRegistryElementPartFControlAction) getCurrentRow().get(BIND_PART_FCA)).getPart();

        Double weeksLoad = null;
        if (elementPart.getRegistryElement() instanceof EppRegistryAction) {
            EppRegistryAction registryAction = (EppRegistryAction) elementPart.getRegistryElement();
            weeksLoad = registryAction.getWeeksAsDouble();
        }

        return RegistryPartComboBoxDSHandler.getLoadString(elementPart.getSizeAsDouble(), weeksLoad, elementPart.getLaborAsDouble());
    }

    public String getCurrentMark()
    {
        EppRegistryElementPartFControlAction partFControlAction = getCurrentRow().get(BIND_PART_FCA);
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(SessionMark.class, "e")
                .column(property("e"))
                .where(eqValue(property("e", SessionMark.slot().studentWpeCAction().studentWpe().student()), getDiplomaObject().getStudent()))
                .where(eqValue(property("e", SessionMark.slot().studentWpeCAction().type()), partFControlAction.getControlAction().getEppGroupType()))
                .where(eqValue(property("e", SessionMark.slot().studentWpeCAction().studentWpe().registryElementPart()), partFControlAction.getPart()))
                .order(property("e", SessionMark.performDate()));

        final Set<String> markName = new LinkedHashSet<>();
        for (SessionMark mark : DataAccessServices.dao().<SessionMark>getList(dql)) {
            markName.add(mark.getValueTitle());
        }
        return UniEppUtils.NEW_LINE_FORMATTER.format(CommonBaseStringUtil.joinNotEmpty(markName, "\n"));
    }


    public String getCurrentTutor()
    {
        EppRegistryElementPartFControlAction partFControlAction = getCurrentRow().get(BIND_PART_FCA);
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(SessionDocumentSlot.class, "e")
                .column(property("e", SessionDocumentSlot.commission().id()))
                .where(eqValue(property("e", SessionDocumentSlot.studentWpeCAction().studentWpe().student()), getDiplomaObject().getStudent()))
                .where(eqValue(property("e", SessionDocumentSlot.studentWpeCAction().type()), partFControlAction.getControlAction().getEppGroupType()))
                .where(eqValue(property("e", SessionDocumentSlot.studentWpeCAction().studentWpe().registryElementPart()), partFControlAction.getPart()));

        List<Long> commissionsIds = DataAccessServices.dao().getList(dql);
        List<SessionComissionPps> ppsList = DataAccessServices.dao().getList(SessionComissionPps.class, SessionComissionPps.commission().id(), commissionsIds);

        final Set<String> tutorName = new TreeSet<>();
        for (SessionComissionPps pps : ppsList) {
            tutorName.add(pps.getPps().getPerson().getFio());
        }

        return UniEppUtils.NEW_LINE_FORMATTER.format(CommonBaseStringUtil.joinNotEmpty(tutorName, "\n"));
    }

    public DataWrapper getCurrentEditRow()
    {
        return _currentEditRow;
    }

    public void setCurrentEditRow(DataWrapper currentEditRow)
    {
        _currentEditRow = currentEditRow;
    }

    public DataWrapper getCurrentRow()
    {
        return getRegElemDS().getCurrent();
    }

    public IUIDataSource getRegElemDS()
    {
        return _regElemDS;
    }

    public void setRegElemDS(IUIDataSource regElemDS)
    {
        _regElemDS = regElemDS;
    }

    public DataWrapper getRegElem()
    {
        return _regElem;
    }

    public void setRegElem(DataWrapper regElem)
    {
        _regElem = regElem;
    }

    public boolean isComponentInEditMode()
    {
        return null != getCurrentEditRow();
    }

    public boolean isCurrentRowInEditMode()
    {
        DataWrapper editRow = getCurrentEditRow();
        return !(null == editRow || null == getCurrentRow()) && _regElemWrappedList.indexOf(editRow) == _regElemWrappedList.indexOf(getCurrentRow());
    }

    public String[] getSourceList()
    {
        return _sourceList;
    }

    public void setSourceList(String[] sourceList)
    {
        _sourceList = sourceList;
    }

    public String getCurrentSourceValue()
    {
        return _currentSourceValue;
    }

    public void setCurrentSourceValue(String currentSourceValue)
    {
        _currentSourceValue = currentSourceValue;
    }

    public DataWrapper getPartFControlAction()
    {
        return _partFControlAction;
    }

    public void setPartFControlAction(DataWrapper partFControlAction)
    {
        _partFControlAction = partFControlAction;
    }


    //Listeners
    public void onClickAddRowPart()
    {
        _regElemOld = null;
        _partFControlActionOld = null;
        setRegElem(null);
        setPartFControlAction(null);

        DataWrapper regElemWrapper = new DataWrapper();
        regElemWrapper.setId(getNextId());
        _regElemWrappedList.add(regElemWrapper);
        setCurrentEditRow(regElemWrapper);
        _currentSourceValue = SLOT_SOURCE;
    }

    public void onClickSaveRowPart()
    {
        _row.setRowContentUpdateDate(new Date());

        DiplomaContentRegElPartFControlAction rel = new DiplomaContentRegElPartFControlAction(getRow(), getPartFControlAction().get(BIND_PART_FCA));

        DataWrapper wrapper = getCurrentEditRow();
        wrapper.setTitle(((EppRegistryElementPart) getRegElem().getWrapped()).getRegistryElement().getTitleWithNumber());
        wrapper.put(BIND_REL, rel);
        wrapper.put(BIND_PART_FCA, getPartFControlAction().get(BIND_PART_FCA));
        wrapper.put(BIND_REGISTRY_PART, getRegElem().getWrapped());
        wrapper.put(BIND_SOURCE, getCurrentSourceValue());

        setCurrentEditRow(null);
        _regElem = null;
        _regElemOld = null;
        _partFControlAction = null;
        _partFControlActionOld = null;
    }

    public void onClickEditRow()
    {
        DataWrapper currentWrapper = getListenerParameter();

        if (null == currentWrapper.get(BIND_SOURCE)) {
            _currentSourceValue = REGISTRY_SOURCE;
        }

        final EppRegistryElementPartFControlAction partFCA = currentWrapper.get(BIND_PART_FCA);
        final DataWrapper partWrapper = new DataWrapper(partFCA.getPart().getId(), partFCA.getPart().getTitleWithNumber(), partFCA.getPart());
        partWrapper.put(BIND_PART_FCA, partFCA);
        setRegElem(partWrapper);

        final DataWrapper partFCAWrapper = new DataWrapper(partFCA.getId(), partFCA.getControlAction().getTitle(), partFCA);
        partFCAWrapper.put(BIND_PART_FCA, currentWrapper.get(BIND_PART_FCA));
        partFCAWrapper.put(BIND_REGISTRY_PART, partFCA.getPart());
        setPartFControlAction(partFCAWrapper);

        _regElemOld = getRegElem();
        _partFControlActionOld = getPartFControlAction();
        setCurrentEditRow(currentWrapper);
    }

    public void onClickDeleteRowPart()
    {
        if (null != _currentEditRow && null != _regElemOld && null != _partFControlActionOld) {
            setCurrentEditRow(null);
        }
        else if (null != _currentEditRow) {
            _regElemWrappedList.remove(_regElemWrappedList.size() - 1);
            setCurrentEditRow(null);
        }
        else {
            DataWrapper wrapper = getListenerParameter();
            _regElemWrappedList.remove(wrapper);
            if (SharedBaseDao.instance.get().existsEntity(DiplomaContentRegElPartFControlAction.class, DiplomaContentRegElPartFControlAction.P_ID, getCurrentRow().get(BIND_REL))) {
                _row.setRowContentUpdateDate(new Date());
            }
            setCurrentEditRow(null);
        }

        _regElemOld = null;
        _partFControlActionOld = null;
    }


    public void onClickApply()
    {
        if (getCurrentEditRow() != null) {
            throw new ApplicationException("Завершите редактирование строк.");
        }

        getRow().setCommonLoad(getRowLoad());

        if (null == getRow().getOwner()) {
            getRow().setOwner(getDiplomaObject().getContent());
        }

        if (0 == getRow().getContentListNumber()) {
            getRow().setContentListNumber(1);
        }

        if (0 == getRow().getNumber()) {
            int number = DiplomaTemplateManager.instance().dao().getDiplomaContentRowMaxNumber(getRow().getOwner());
            getRow().setNumber(number + 1);
        }

        _row.setMark(getMark() != null ? getMark().getTitle() : null);

        List<DiplomaContentRegElPartFControlAction> regElemPartList = new ArrayList<>();

        for (DataWrapper wrapper : _regElemWrappedList)
        {
            //Сохраняем в список уже существующие связи
            DiplomaContentRegElPartFControlAction item = wrapper.get(BIND_REL);
            if (item != null) {
                regElemPartList.add(item);
            }
        }

        DipDocumentManager.instance().dao().saveOrUpdateDiplomaContentRow(regElemPartList, getRow());
        deactivate();
    }

    public void onChangeEpvRegistryRow()
    {
        if (!isEditForm() && _row.getEpvRegistryRow() != null) {
            _row.setTitle(_row.getEpvRegistryRow().getTitle());
        }
    }

    public Long getRowId()
    {
        return _rowId;
    }

    public void setRowId(Long rowId)
    {
        _rowId = rowId;
    }

    public Class<? extends DiplomaContentRow> getRowClass()
    {
        return _rowClass;
    }

    public void setRowClass(Class<? extends DiplomaContentRow> rowClass)
    {
        _rowClass = rowClass;
    }
}
