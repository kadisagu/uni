/* $Id$ */
package ru.tandemservice.unidip.base.bo.DipOrgUnit.ui.Tab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import ru.tandemservice.unidip.base.bo.DipDocument.ui.OrgUnitStudentTab.DipDocumentOrgUnitStudentTab;
import ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.ui.List.DipDiplomaBlankList;

/**
 * @author Alexey Lopatin
 * @since 29.05.2015
 */
@Configuration
public class DipOrgUnitTab extends BusinessComponentManager
{
    public static final String DIP_TAB_PANEL = "dipTabPanel";
    public static final String DIP_BLANKS_TAB = "dipBlanksTab";
    public static final String DIP_DIPLOMA_STUDENT_TAB = "dipDiplomasTab";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .create();
    }

    @Bean
    public TabPanelExtPoint dipTabPanelExtPoint()
    {
        return tabPanelExtPointBuilder(DIP_TAB_PANEL)
                .addTab(componentTab(DIP_BLANKS_TAB, DipDiplomaBlankList.class).parameters("mvel:['publisherId':presenter.orgUnit.id]").permissionKey("ui:secModel.orgUnit_viewDipBlanksTab").securedObject("ui:orgUnit"))
                .addTab(componentTab(DIP_DIPLOMA_STUDENT_TAB, DipDocumentOrgUnitStudentTab.class).parameters("mvel:['publisherId':presenter.orgUnit.id]").permissionKey("ui:secModel.orgUnit_viewStudentDiplomaTab").securedObject("ui:orgUnit"))
                .create();
    }
}
