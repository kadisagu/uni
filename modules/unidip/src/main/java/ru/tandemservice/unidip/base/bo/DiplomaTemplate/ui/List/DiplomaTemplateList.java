/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unidip.base.bo.DiplomaTemplate.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.StringLimitFormatter;
import ru.tandemservice.unidip.base.bo.DipDocument.logic.DiplomaTemplateDSHandler;
import ru.tandemservice.unidip.base.entity.eduplan.DiplomaTemplate;

/**
 *
 * @author iolshvang
 * @since 13.07.11 18:38
 */
@Configuration
public class DiplomaTemplateList extends BusinessComponentManager
{
    public static final String DIP_TEMPLATE_DS = "dipTemplateDS";

    @Bean
    public ColumnListExtPoint dipTemplateDS()
    {
        return columnListExtPointBuilder(DIP_TEMPLATE_DS)
                .addColumn(publisherColumn("title", DiplomaTemplate.content().educationElementTitle()).order())
                .addColumn(textColumn("type", DiplomaTemplate.content().type().title()).order())
                .addColumn(textColumn("programSpecialization", DiplomaTemplate.content().programSpecialization().title()).order())
                .addColumn(textColumn("comment", DiplomaTemplate.comment()).formatter(StringLimitFormatter.NON_BROKEN_WORDS_WITH_NEW_LINE_SUPPORT))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).permissionKey("editDipTemplateFromList"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER, alert("dipTemplateDS.delete.alert", DiplomaTemplate.content().educationElementTitle())).permissionKey("deleteDipTemplateFromList"))
                .create();
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(DIP_TEMPLATE_DS).columnListExtPoint(dipTemplateDS()).handler(dipTemplateDSHandler()))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> dipTemplateDSHandler()
    {
        return new DiplomaTemplateDSHandler(getName());
    }
}
