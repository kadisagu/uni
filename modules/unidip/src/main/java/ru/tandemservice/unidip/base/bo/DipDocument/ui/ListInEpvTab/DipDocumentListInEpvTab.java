package ru.tandemservice.unidip.base.bo.DipDocument.ui.ListInEpvTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.NewLineFormatter;
import org.tandemframework.core.view.formatter.StringLimitFormatter;
import org.tandemframework.core.view.list.column.DefaultPublisherLinkResolver;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unidip.base.bo.DipDocument.logic.DipDocumentStudentDSHandler;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaObject;
import ru.tandemservice.unidip.base.entity.order.DipStuExcludeExtract;

/**
 * @author avedernikov
 * @since 29.11.2016
 */
@Configuration
public class DipDocumentListInEpvTab extends BusinessComponentManager
{
	public static final String DIP_DOCUMENT_DS = "dipDocumentDS";
	public static final String ORG_UNIT_DS = "orgUnitDS";

	@Bean
	@Override
	public PresenterExtPoint presenterExtPoint()
	{
		return this.presenterExtPointBuilder()
				.addDataSource(searchListDS(DIP_DOCUMENT_DS, dipDocumentDSColumn(), dipDocumentDSHandler()))
				.addDataSource(CommonManager.instance().yesNoDSConfig())
				.addDataSource(selectDS(ORG_UNIT_DS, orgUnitDSHandler()).addColumn(OrgUnit.P_FULL_TITLE))
				.create();
	}

	@Bean
	public ColumnListExtPoint dipDocumentDSColumn()
	{
		return columnListExtPointBuilder(DIP_DOCUMENT_DS)
				.addColumn(publisherColumn("student", DiplomaObject.student().studentFullFioWithBirthYearGroupAndStatus()).publisherLinkResolver(new DefaultPublisherLinkResolver()
				{
					@Override
					public Object getParameters(final IEntity entity)
					{
						DiplomaObject diplomaObject = ((DataWrapper) entity).getWrapped();
						return new ParametersMap()
								.add(PublisherActivator.PUBLISHER_ID_KEY, diplomaObject.getStudent().getId())
								.add("selectedStudentTab", "studentDipTab");

					}
				}).required(true).order())
				.addColumn(publisherColumn("type", DiplomaObject.content().type().titleWithParent()).required(true).order())
				.addColumn(booleanColumn("withSuccess", DiplomaObject.content().withSuccess()).required(true))
				.addColumn(textColumn(DipDocumentStudentDSHandler.BLANK_SERIA_AND_NUMBER, DipDocumentStudentDSHandler.BLANK_SERIA_AND_NUMBER).formatter(NewLineFormatter.NOBR_IN_LINES).required(true).order())
				.addColumn(textColumn(DipDocumentStudentDSHandler.REGISTRATION_NUMBER, DipDocumentStudentDSHandler.REGISTRATION_NUMBER).formatter(NewLineFormatter.NOBR_IN_LINES).required(true).order())
				.addColumn(textColumn(DipDocumentStudentDSHandler.ISSUANCE_DATE, DipDocumentStudentDSHandler.ISSUANCE_DATE).formatter(NewLineFormatter.NOBR_IN_LINES).required(true).order())
				.addColumn(textColumn(DipDocumentStudentDSHandler.APPLICATION_TO_DIPLOMA, DipDocumentStudentDSHandler.APPLICATION_TO_DIPLOMA).formatter(NewLineFormatter.NOBR_IN_LINES).required(true))
				.addColumn(textColumn("orgUnit", DiplomaObject.student().educationOrgUnit().educationLevelHighSchool().orgUnit().title()))
				.addColumn(publisherColumn(DipDocumentStudentDSHandler.EXCLUDE_ORDER_TITLE, DipDocumentStudentDSHandler.EXCLUDE_ORDER_TITLE)
						.publisherLinkResolver(new DefaultPublisherLinkResolver()
						{
							@Override
							public Object getParameters(IEntity entity)
							{
								DipStuExcludeExtract excludeOrderForDiploma = (DipStuExcludeExtract) entity.getProperty(DipDocumentStudentDSHandler.EXCLUDE_ORDER);
								return excludeOrderForDiploma.getParagraph().getOrder().getId();
							}
						}))
				.addColumn(textColumn("comment", DiplomaObject.comment()).formatter(StringLimitFormatter.NON_BROKEN_WORDS_WITH_NEW_LINE_SUPPORT))
				.create();
	}

	@Bean
	public IBusinessHandler<DSInput, DSOutput> dipDocumentDSHandler()
	{
		return new DipDocumentStudentDSHandler(getName(), true);
	}

	@Bean
	public IBusinessHandler<DSInput, DSOutput> orgUnitDSHandler()
	{
		return new EntityComboDataSourceHandler(getName(), OrgUnit.class)
				.filter(OrgUnit.title())
				.order(OrgUnit.title());
	}
}