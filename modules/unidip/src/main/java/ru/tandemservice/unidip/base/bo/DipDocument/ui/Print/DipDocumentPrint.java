/* $Id$ */
package ru.tandemservice.unidip.base.bo.DipDocument.ui.Print;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unidip.base.entity.catalog.DipDocTemplateCatalog;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentIssuance;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;


/**
 * @author Andrey Avetisov
 * @since 26.05.2014
 */
@Configuration
public class DipDocumentPrint extends BusinessComponentManager
{
    public static final String APPLICATION_TEMPLATE_DS = "applicationTemplateDS";
    public static final String TITLE_TEMPLATE_DS = "titleTemplateDS";
    public static final String ISSUANCE_DS = "issuanceDS";
    public static final String DIP_APP_TEMPLATE_DS = "dipAppTemplateDS";
    public static final String DIPLOMA_CONTENT_ISSUANCE_DS = "diplomaContentIssuanceDS";


    public static final String DIP_DOC_TYPE_PARAM = "docType";
    public static final String WITH_SUCCESS_PARAM = "withSuccess";
    public static final String DIPLOMA_OBJECT_PARAM = "diplomaObject";
    public static final String DIPLOMA_ISSUANCE_PARAM = "diplomaIssuance";
    public static final String DIP_CONTENT_ISSUANCE_LIST = "dipContentIssuanceList";


    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ISSUANCE_DS, dipDocumentIssuanceDSHandler()).addColumn(DiplomaIssuance.P_FULL_TITLE))
                .addDataSource(selectDS(TITLE_TEMPLATE_DS, titleTemplateDSHandler()))
                .addDataSource(searchListDS(DIP_APP_TEMPLATE_DS, dipContentIssuanceDSColumns(), dipApplicationDSHandler()))
                .addDataSource(selectDS(APPLICATION_TEMPLATE_DS, applicationTemplateDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint dipContentIssuanceDSColumns()
    {
        return columnListExtPointBuilder(DIP_APP_TEMPLATE_DS)
                .addColumn(blockColumn("applicationsBlock", "applicationsBlock"))
                .addColumn(blockColumn("applicationTemplateBlock", "applicationTemplateBlock"))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> dipDocumentIssuanceDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), DiplomaIssuance.class)
                .where(DiplomaIssuance.diplomaObject(), DIPLOMA_OBJECT_PARAM)
                .order(DiplomaIssuance.registrationNumber())
                .order(DiplomaIssuance.issuanceDate())
                .order(DiplomaIssuance.blankSeria())
                .order(DiplomaIssuance.blankNumber());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> diplomaContentIssuanceDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), DiplomaContentIssuance.class)
                .where(DiplomaContentIssuance.diplomaIssuance(), DIPLOMA_ISSUANCE_PARAM);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> titleTemplateDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), DipDocTemplateCatalog.class)
                .order(DipDocTemplateCatalog.title())
                .where(DipDocTemplateCatalog.dipDocumentType(), DIP_DOC_TYPE_PARAM)
                .where(DipDocTemplateCatalog.withSuccess(), WITH_SUCCESS_PARAM)
                .where(DipDocTemplateCatalog.application(), false)
				.where(DipDocTemplateCatalog.active(), true)
                .filter(DipDocTemplateCatalog.title());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> applicationTemplateDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), DipDocTemplateCatalog.class)
                .order(DipDocTemplateCatalog.title())
                .where(DipDocTemplateCatalog.dipDocumentType(), DIP_DOC_TYPE_PARAM)
                .where(DipDocTemplateCatalog.withSuccess(), WITH_SUCCESS_PARAM)
                .where(DipDocTemplateCatalog.application(), true)
				.where(DipDocTemplateCatalog.active(), true)
                .filter(DipDocTemplateCatalog.title());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> dipApplicationDSHandler()
    {
        return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                List<DataWrapper> applicationList = context.get(DIP_CONTENT_ISSUANCE_LIST);
                Collections.sort(applicationList, (o1, o2) ->
                    {
                        DiplomaContentIssuance diplomaContentIssuance1 = o1.get(DipDocumentPrintUI.WRAPPED_CONTENT_ISSUANCE);
                        DiplomaContentIssuance diplomaContentIssuance2 = o2.get(DipDocumentPrintUI.WRAPPED_CONTENT_ISSUANCE);
                        if (diplomaContentIssuance1.isDuplicate() && !diplomaContentIssuance2.isDuplicate())
                            return 1;
                        if (!diplomaContentIssuance1.isDuplicate() && diplomaContentIssuance2.isDuplicate())
                            return -1;
                        if (diplomaContentIssuance1.isDuplicate() && diplomaContentIssuance2.isDuplicate() &&
                                diplomaContentIssuance1.getDuplicateIssuanceDate().compareTo(diplomaContentIssuance2.getDuplicateIssuanceDate())!=0 )
                            return diplomaContentIssuance1.getDuplicateIssuanceDate().compareTo(diplomaContentIssuance2.getDuplicateIssuanceDate());
                        return diplomaContentIssuance1.getBlankNumber().compareTo(diplomaContentIssuance2.getBlankNumber());
                    });

                DSOutput output = ListOutputBuilder.get(input, applicationList).build();
                output.setCountRecord(Math.max(output.getTotalSize(), 1));
                return output;
            }
        };
    }
}
