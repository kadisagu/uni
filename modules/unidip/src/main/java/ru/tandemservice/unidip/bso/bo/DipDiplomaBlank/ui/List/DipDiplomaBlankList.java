/* $Id:$ */
package ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseFilterUtil;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.DipDiplomaBlankManager;
import ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank;
import ru.tandemservice.unidip.bso.entity.catalog.DipBlankState;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 24.12.2014
 */
@Configuration
public class DipDiplomaBlankList extends BusinessComponentManager {

    public static final String DIPLOMA_BLANK_LIST_COLUMNS = "diplomaBlankListColumns";
    public static final String DIPLOMA_BLANK_SEARCH_LIST = "diplomaBlankSearchList";
    public static final String BLANK_STATE_DS = "blankStateDS";

    public static final String BLANK_SERIA = "blankSeria";
    public static final String BLANK_NUMBER_FROM = "blankNumberFrom";
    public static final String BLANK_NUMBER_TO = "blankNumberTo";
    public static final String BLANK_TYPE = "blankType";
    public static final String STORAGE_LOCATION = "storageLocation";
    public static final String BLANK_STATE = "blankState";
    public static final String PARAM_ORG_UNIT_ID = "orgUnitId";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(DIPLOMA_BLANK_SEARCH_LIST, diplomaBlankListDSColumns(), diplomaBlankDSHandler()))
                .addDataSource(DipDiplomaBlankManager.instance().storageLocationDataSourceConfig())
                .addDataSource(DipDiplomaBlankManager.instance().blankTypeDataSourceConfig())
                .addDataSource(selectDS(BLANK_STATE_DS, blankStateDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint diplomaBlankListDSColumns()
    {
        return columnListExtPointBuilder(DIPLOMA_BLANK_LIST_COLUMNS)
                .addColumn(checkboxColumn("check"))
                .addColumn(publisherColumn("blankSeriaNumber", DipDiplomaBlank.seriaAndNumber()).order())
                .addColumn(textColumn("blankType", DipDiplomaBlank.blankType().title()).order())
                .addColumn(textColumn("storageLocation", DipDiplomaBlank.storageLocation().fullTitle()).visible("mvel:presenter.orgUnitId == null").order())
                .addColumn(textColumn("state", DipDiplomaBlank.blankState().title()))
                .addColumn(textColumn("disposalReason", DipDiplomaBlank.disposalReason().shortTitle()))
                .addColumn(actionColumn("delete", CommonDefines.ICON_DELETE, "onClickDeleteBlank",
                        FormattedMessage.with().template("diplomaBlankListColumns.deleteAlert")
                                .parameter(DipDiplomaBlank.seria())
                                .parameter(DipDiplomaBlank.number())
                                .parameter(DipDiplomaBlank.blankType().title(), source -> source == null ? null : ((String) source).toLowerCase()).create()
                         )
                        .permissionKey("mvel:presenter.getPermissionKey('deleteDipBlank')")
                        .disabled("ui:blankDeleteDisabled"))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> diplomaBlankDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), DipDiplomaBlank.class) {

            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context) {

                final String alias = "db";
                DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(DipDiplomaBlank.class, alias);
                String blankSeria = context.get(BLANK_SERIA);
                String blankNumberFrom = context.get(BLANK_NUMBER_FROM);
                String blankNumberTo = context.get(BLANK_NUMBER_TO);
                Long orgUnitId = context.get(PARAM_ORG_UNIT_ID);

                CommonBaseFilterUtil.applySelectFilter(dql, alias, DipDiplomaBlank.seria(), blankSeria);
                if (blankNumberFrom != null)
                    dql.where(ge(property(DipDiplomaBlank.number().fromAlias(alias)), value(blankNumberFrom)));
                if (blankNumberTo != null)
                    dql.where(le(property(DipDiplomaBlank.number().fromAlias(alias)), value(blankNumberTo)));

                if (null != orgUnitId){
                    CommonBaseFilterUtil.applySelectFilter(dql, alias, DipDiplomaBlank.storageLocation().id(), orgUnitId);
                }else{
                    CommonBaseFilterUtil.applySelectFilter(dql, alias, DipDiplomaBlank.storageLocation(), context.get(STORAGE_LOCATION));
                }
                CommonBaseFilterUtil.applySelectFilter(dql, alias, DipDiplomaBlank.blankType(), context.get(BLANK_TYPE));
                CommonBaseFilterUtil.applySelectFilter(dql, alias, DipDiplomaBlank.blankState(), context.get(BLANK_STATE));

                DQLOrderDescriptionRegistry registry = new DQLOrderDescriptionRegistry(DipDiplomaBlank.class, alias);

                final EntityOrder entityOrder = input.getEntityOrder();
                OrderDescription seriaOD = new OrderDescription(DipDiplomaBlank.seria());
                OrderDescription numberOD = new OrderDescription(DipDiplomaBlank.number());
                OrderDescription typeOD = new OrderDescription(DipDiplomaBlank.blankType());
                OrderDescription storageLocOD = new OrderDescription(alias, DipDiplomaBlank.storageLocation());

                registry.setOrders(DipDiplomaBlank.seriaAndNumber(), seriaOD, numberOD);
                registry.setOrders(DipDiplomaBlank.blankType().title(), typeOD, seriaOD, numberOD);
                registry.setOrders(DipDiplomaBlank.storageLocation().fullTitle(), storageLocOD, seriaOD, numberOD);

                registry.applyOrder(dql, entityOrder);

                return DQLSelectOutputBuilder.get(input, dql, context.getSession()).build();
            }
        };
    }

    @Bean
    public IDefaultComboDataSourceHandler blankStateDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), DipBlankState.class)
                .order(DipBlankState.title())
                .filter(DipBlankState.title());
    }

}
