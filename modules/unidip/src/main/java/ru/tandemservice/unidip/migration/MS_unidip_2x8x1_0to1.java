package ru.tandemservice.unidip.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unidip_2x8x1_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность diplomaIssuance

		//  свойство blankSeria стало необязательным
		{
			// сделать колонку NULL
			tool.setColumnNullable("dip_issuance_t", "blankseria_p", true);
		}

		//  свойство blankNumber стало необязательным
		{
			// сделать колонку NULL
			tool.setColumnNullable("dip_issuance_t", "blanknumber_p", true);
		}
    }
}