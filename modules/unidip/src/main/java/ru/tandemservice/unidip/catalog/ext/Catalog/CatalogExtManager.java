/* $Id:$ */
package ru.tandemservice.unidip.catalog.ext.Catalog;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.CatalogManager;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogDesc;
import ru.tandemservice.unidip.base.entity.catalog.DipDocumentType;
import ru.tandemservice.unidip.bso.entity.catalog.DipBlankDisposalReason;

/**
 * @author rsizonenko
 * @since 22.12.2014
 */
@Configuration
public class CatalogExtManager extends BusinessObjectExtensionManager {

    @Autowired
    private CatalogManager catalogManager;

    @Bean
    public ItemListExtension<IDynamicCatalogDesc> itemListExtension()
    {
        return itemListExtension(catalogManager.dynamicCatalogsExtPoint())
                .add(StringUtils.uncapitalize(DipBlankDisposalReason.class.getSimpleName()), DipBlankDisposalReason.getUiDesc())
                .add(StringUtils.uncapitalize(DipDocumentType.class.getSimpleName()), DipDocumentType.getUiDesc())
                .create();
    }
}
