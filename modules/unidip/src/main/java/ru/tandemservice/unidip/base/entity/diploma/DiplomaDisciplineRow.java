package ru.tandemservice.unidip.base.entity.diploma;

import ru.tandemservice.unidip.base.bo.DipDocument.DipDocumentManager;
import ru.tandemservice.unidip.base.entity.diploma.gen.DiplomaDisciplineRowGen;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.codes.EppRegistryStructureCodes;
import ru.tandemservice.uniepp.util.EppFControlActionInfo;

import java.util.List;

/**
 * Строка блока Дисциплин в дипломе
 */
public class DiplomaDisciplineRow extends DiplomaDisciplineRowGen
{
    @Override
    protected int getBlockPriority()
    {
        return 0;
    }

    @Override
    protected boolean isLoadInWeeksForOKSO_and_SPO()
    {
        return false;
    }

    @Override
    protected String getPageTitle()
    {
        return "Дисциплины";
    }

	@Override
    public List<String> getRelatedRegistryElementTypeCodes()
    {
		// Здесь могут быть МСРП типа элемента реестра "Дисциплина" и всех подчиненных
		return DipDocumentManager.instance().dao().regElemTypeWithChildrenCodes(EppRegistryStructureCodes.REGISTRY_DISCIPLINE);
    }

    @Override
    public boolean isAllowFCA(EppFControlActionType fca)
    {
        // В блоке дисциплин могут быть формы контроля, определенные в настройке "Связь типов контрольных мероприятий с типами элементов УП / РУП",
        // кроме курсовых: курсовые добавляются в свой блок.
        return fca.isUsedWithDisciplines() && !EppFControlActionInfo.SUPPLIER.get().isCourseWork(fca.getId());
    }
}