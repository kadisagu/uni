/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unidip.component.listextract.excludeStudents.ParagraphAddEdit;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.formatter.NoWrapFormatter;
import org.tandemframework.core.view.formatter.SimpleFormatter;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditController;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.formatters.StudentCustomStateCollectionFormatter;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentIssuance;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance;
import ru.tandemservice.unidip.base.entity.order.DipStuExcludeExtract;

import java.util.*;

/**
 *
 * @author iolshvang
 * @since 25.07.11 15:41
 */
public class Controller extends AbstractListParagraphAddEditController<DipStuExcludeExtract, IDAO, Model>
{
     @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);
        model.setSearchListSettingsKey("StudentListParagraphAddEdit." + model.getParagraphType().getCode() + ".");
        prepareListDataSource(component);
        this.getDao().prepareColumns(model);
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<Student> dataSource = UniBaseUtils.createDataSource(component, getDao());

        CheckboxColumn checkboxColumn = new CheckboxColumn("checkbox", "", true);
        if (model.getSelectedItemList() != null)
            checkboxColumn.setSelectedObjects(model.getSelectedItemList());
        checkboxColumn.setDisableHandler(entity -> {
            final Student student = (Student) ((ViewWrapper) entity).getEntity();
            final List<DiplomaIssuance> diplomaIssuances = model.getDiplomaIssuanceMap().get(student.getId());
            return (diplomaIssuances == null || diplomaIssuances.isEmpty());
        });
        checkboxColumn.setDisplayTotal(true);
        dataSource.addColumn(checkboxColumn);

        prepareListDataSource(component, dataSource);
        model.setDataSource(dataSource);
    }

    // заполняет колонку с приложениями (берутся не-дубликаты и с разными номерами)
    @SuppressWarnings("unchecked")
    public void onChangeIssuance(IBusinessComponent component)
    {
        final Model model = getModel(component);
        final DataWrapper issuanceWrapper = (DataWrapper) ((BlockColumn) model.getDataSource().getColumn("diplomaIssuance")).getValueMap().get(component.getListenerParameter());
        if (issuanceWrapper != null)
        {
            final DiplomaIssuance issuance = issuanceWrapper.getWrapped();

            final BlockColumn column = (BlockColumn) model.getDataSource().getColumn("issuanceContent");
            Map<Long, List<DataWrapper>> issuanceContent = column.getValueMap();
            if (issuanceContent == null) {
                issuanceContent = new HashMap<>();
                column.setValueMap(issuanceContent);
            }

            final List<DiplomaContentIssuance> contentIssuances = DataAccessServices.dao().getList(DiplomaContentIssuance.class, DiplomaContentIssuance.diplomaIssuance(), issuance);

            List<DataWrapper> target = new ArrayList<>();
            Set<Integer> listNumbers = new HashSet<>();

            for (DiplomaContentIssuance contentIssuance : contentIssuances) {
                if (!contentIssuance.isDuplicate() && !listNumbers.contains(contentIssuance.getContentListNumber()))
                {
                    final DataWrapper wrapper = new DataWrapper(contentIssuance);
                    wrapper.setTitle(model.getContentIssuanceTitle(contentIssuance));
                    target.add(wrapper);
                }
            }

            issuanceContent.put((Long) component.getListenerParameter(), target);
        }

    }

    @Override
    protected void prepareListDataSource(IBusinessComponent component, DynamicListDataSource<Student> dataSource)
    {
        dataSource.addColumn(new SimpleColumn("ФИО студента", Student.FIO_KEY, NoWrapFormatter.INSTANCE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дополнительный статус", Model.P_STUDENT_ACTIVE_CUSTOME_STATES, new StudentCustomStateCollectionFormatter()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Пол", Student.person().identityCard().sex().shortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("status", "Состояние", Model.STATUS_KEY, SimpleFormatter.INSTANCE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Курс", Student.course().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Группа", Student.group().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Контракт", Student.compensationType().shortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new BlockColumn("diplomaIssuance", "Документ об обучении"));
        dataSource.addColumn(new BlockColumn("issuanceContent", "Приложения"));
        dataSource.addColumn(new BlockColumn("issuanceRegNumber", "Регистрационный номер"));
        dataSource.addColumn(new BlockColumn("issuanceDate", "Дата выдачи"));
        dataSource.setOrder("status", OrderDirection.asc);
    }
}
