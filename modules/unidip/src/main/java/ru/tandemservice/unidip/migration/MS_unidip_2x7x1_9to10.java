package ru.tandemservice.unidip.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unidip_2x7x1_9to10 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность diplomaObject

		// удалено свойство uniqueNumber
		{
			// удалить колонку
			tool.dropColumn("dip_object_t", "uniquenumber_p");
		}

		// удалено свойство number
		{
			// удалить колонку
			tool.dropColumn("dip_object_t", "number_p");
		}

		// удалено свойство issueDate
		{
			// удалить колонку
			tool.dropColumn("dip_object_t", "issuedate_p");
		}

		// удалено свойство registrationDate
		{
			// удалить колонку
			tool.dropColumn("dip_object_t", "registrationdate_p");
		}
    }
}