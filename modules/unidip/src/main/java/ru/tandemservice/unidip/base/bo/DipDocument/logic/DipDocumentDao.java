/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unidip.base.bo.DipDocument.logic;

import com.google.common.base.Preconditions;
import com.google.common.collect.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.log4j.Logger;
import org.hibernate.FlushMode;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.dsl.PropertyPath;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.InfoCollector;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.EntityIDGenerator;
import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.commonbase.dao.SharedBaseDao;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.AcademyRename;
import ru.tandemservice.unidip.base.bo.DiplomaTemplate.DiplomaTemplateManager;
import ru.tandemservice.unidip.base.bo.DiplomaTemplate.logic.DiplomaTemplateDao;
import ru.tandemservice.unidip.base.bo.DiplomaTemplate.logic.FormingRowUtils;
import ru.tandemservice.unidip.base.entity.catalog.DipAggregationMethod;
import ru.tandemservice.unidip.base.entity.catalog.DipDocTemplateCatalog;
import ru.tandemservice.unidip.base.entity.catalog.DipDocumentType;
import ru.tandemservice.unidip.base.entity.catalog.codes.DipAggregationMethodCodes;
import ru.tandemservice.unidip.base.entity.catalog.codes.DipDocumentTypeCodes;
import ru.tandemservice.unidip.base.entity.diploma.*;
import ru.tandemservice.unidip.base.entity.diploma.gen.DiplomaIssuanceGen;
import ru.tandemservice.unidip.base.entity.eduplan.DiplomaTemplate;
import ru.tandemservice.unidip.base.entity.order.DipStuExcludeExtract;
import ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank;
import ru.tandemservice.unidip.bso.entity.catalog.DipBlankState;
import ru.tandemservice.unidip.bso.entity.catalog.codes.DipBlankStateCodes;
import ru.tandemservice.unidip.settings.bo.DipSettings.DipSettingsManager;
import ru.tandemservice.unidip.settings.entity.DipFormingRowAlgorithm;
import ru.tandemservice.unidip.settings.entity.DipFormingRowAlgorithmOrgUnit;
import ru.tandemservice.unidip.settings.entity.codes.DipFormingRowAlgorithmCodes;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.codes.EduProgramSubjectIndexCodes;
import ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.EppCustomPlanWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppCustomPlanRowWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppCustomPlanWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartWrapper;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppELoadType;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.catalog.codes.EppWorkPlanRowKindCodes;
import ru.tandemservice.uniepp.entity.plan.*;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpePart;
import ru.tandemservice.uniepp.util.EppFControlActionInfo;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionProjectTheme;
import ru.tandemservice.unisession.entity.document.SessionStudentGradeBookDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.mark.SessionSlotLinkMark;

import javax.annotation.Nullable;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.google.common.base.Objects.firstNonNull;
import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.sum;

/**
 * @author iolshvang
 * @since 13.07.11 18:35
 */
public class DipDocumentDao extends SharedBaseDao implements IDipDocumentDao
{
    protected static final Logger log = Logger.getLogger(DipDocumentDao.class);

    @Override
    public List<DipDocTemplateCatalog> getTemplateList(Long documentTypeId)
    {
	    return getList(DipDocTemplateCatalog.class, DipDocTemplateCatalog.dipDocumentType().id(), documentTypeId);
    }

    private interface IDiplomaRowContainer
    {
        DiplomaContentRow getDiplomaRow();
        Long getEppEpvRowId();
        Long getRegElementId();
    }

    private class NewRow implements IDiplomaRowContainer
    {
        // Новая строка. Не обязательно что она будет сохранена в базу - может выступать как контейнер данных, для обновления существующей строки.
        private final DiplomaContentRow row;

        // Элемент реестра, по которому сгрупиирвоаны МСРП
        private final Long regElementId;

        // Враппер строки УПв, найденный по индексу. Может быть null.
        private final IEppEpvRowWrapper epvRowWrapper;

        // { [term] -> [[wpePart]]}. Нужна для того, чтобы вычислять семестр новой строки (по экзаменам и т.п.) и считать суммарную нагрузку по частям.
        private final Map<Integer, Set<EppStudentWpePart>> term2WpeParts;

        // Если нет МСРП по нагрузке, отбираются семестры (и части эл. реестра в рамках них) из МСРП (РУП). Используются только для подсчета трудоемкости.
        private final Map<Integer, Set<EppRegistryElementPart>> terms4EmptyWpe;

        // Формы итогового контроля для частей элеемнта реестра по МСРП
        // partId -> fcaId
        // Теоретически, соответствующей связи ФИК и части может не быть в базе.
        private Set<PairKey<Long, Long>> regElemPart2FcaType;

        public NewRow(DiplomaContentRow row, Map<Integer, Set<EppStudentWpePart>> term2WpeParts, Map<Integer, Set<EppRegistryElementPart>> terms4EmptyWpe, Long regElementId, IEppEpvRowWrapper epvRowWrapper) {
            this.row = row;
            this.term2WpeParts = term2WpeParts;
            this.terms4EmptyWpe = terms4EmptyWpe;
            this.regElementId = regElementId;
            this.epvRowWrapper = epvRowWrapper;
        }

        @Override
        public Long getEppEpvRowId() {
            return epvRowWrapper.getRow().getId();
        }

        @Override
        public Long getRegElementId() {
            return regElementId;
        }

        @Override
        public DiplomaContentRow getDiplomaRow() {
            return this.row;
        }
    }

    private NewRow createOneRow(RowContainer container,
                                Class<? extends DiplomaContentRow> rowClass,
                                IEppEpvRowWrapper eppRowWrapper,
                                Map<Integer, Set<EppStudentWpePart>> term2WpeParts,
                                Integer term)
    {
        Preconditions.checkNotNull(rowClass);
        final DiplomaContentRow newRow;
        try {
            newRow = rowClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw CoreExceptionUtils.getRuntimeException(e);
        }

        newRow.setEpvRegistryRow(eppRowWrapper != null ? (EppEpvRegistryRow) eppRowWrapper.getRow() : null);
        newRow.setTitle(container.title);
        newRow.setTerm(term);
        newRow.setContentListNumber(1);
        newRow.setRowValueUpdateDate(new Date());
        newRow.setRowContentUpdateDate(new Date());

        return new NewRow(newRow, term2WpeParts, container.terms4EmptyWpe, container.regElementId, eppRowWrapper);
    }

	/**
	 * Является ли дисциплина, к которой относится строка, факультативной.
	 * @param container контейнер строки документа об обучении
	 * @return true, если вид дисциплины строки РУП из МСРП «Факультативная»; false если вид дисциплины другой или его невозможно узнать (в строке нет МСРП или строка РУП в МСРП пустая).
	 */
	private boolean isContainerDisciplineOptional(RowContainer container)
	{
		if (container.term2WpeParts.isEmpty())
			return false;
		EppStudentWorkPlanElement sampleWpe = container.term2WpeParts.values().iterator().next().iterator().next().getStudentWpe();
		if (sampleWpe.getSourceRow() == null)
			return false;
		return sampleWpe.getSourceRow().getKind().getCode().equals(EppWorkPlanRowKindCodes.OPTIONAL);
	}

    /**
     * Создание одной или нескольких дисциплин по набору МСРП для элемента реестра. Тут же создаются курсовые.
     */
    private Collection<NewRow> createDisciplineRows(RowContainer container,
                                                    Map<Integer, Set<String>> termActionsMap,
                                                    IEppEpvRowWrapper epvRowWrapper,
                                                    String algCode,
													HashMultimap<Integer, EppStudentWpePart> term2CourseWorkWpeParts)
    {
        // Помещаем в блок с факультативными дисциплинами, если строка РУП первого МСРП имеет вид "Факультативная"
        final Class<? extends DiplomaContentRow> rowClass = isContainerDisciplineOptional(container) ? DiplomaOptDisciplineRow.class : DiplomaDisciplineRow.class;

        final Map<Integer, Set<EppStudentWpePart>> term2NotCourseWorkWpeParts = new HashMap<>();

        container.term2WpeParts.forEach((term, slotSet) -> {
            final Set<EppStudentWpePart> courseSlots = term2CourseWorkWpeParts.get(term);

            // Все слоты, кроме курсовых
            final Set<EppStudentWpePart> slotSetWoCourse = slotSet.stream()
                    .filter(slot -> !courseSlots.contains(slot))
                    .collect(Collectors.toSet());

            term2NotCourseWorkWpeParts.put(term, slotSetWoCourse);
        });

        final List<NewRow> resultList = new ArrayList<>();
        switch (algCode)
        {
            case DipFormingRowAlgorithmCodes.DISC:

                // Алгоритм "Одна дисциплина - одна строка в дипломе". Просто создаем одну дисциплину.
                resultList.add(createOneRow(container, rowClass, epvRowWrapper, term2NotCourseWorkWpeParts, FormingRowUtils.getRowTerm(termActionsMap)));
                break;

            case DipFormingRowAlgorithmCodes.EXAMS:

                // Алгоритм "Одна дисциплина - количество строк в дипломе равно количеству экзаменов".
                FormingRowUtils.splitTermPartWithExams(termActionsMap).forEach((examTerm, innerTerms) -> {

                    // Вычленяем из общей мапы слотов, слоты за семестры, попавшие в строку
                    final Map<Integer, Set<EppStudentWpePart>> term2WpeParts = innerTerms.stream()
                            .collect(Collectors.toMap(Function.<Integer>identity(), term2NotCourseWorkWpeParts::get));

                    resultList.add(createOneRow(container, rowClass, epvRowWrapper, term2WpeParts, examTerm));
                });
                break;

            default:
                throw new UnsupportedOperationException();
        }

        if (!term2CourseWorkWpeParts.isEmpty()) {

            // Создание строк с курсовыми работами / проектами. Эти строки создаются только для дисциплин.
            // Строка с курсовой создается на каждый семестр, где есть курсовая форма контроля. На каждую форму контроля.
            // Т.е. если в одном семестре есть КР и КП, то будет создано 2 строки на этот семестр.
            // Для курсовых нагрузка не заполняется!
			term2CourseWorkWpeParts.asMap().forEach((term, slotSet) -> {

				for (final EppStudentWpePart wpePart : slotSet)
				{

					final NewRow newRow = createOneRow(container, DiplomaCourseWorkRow.class, epvRowWrapper, ImmutableMap.of(term, ImmutableSet.of(wpePart)), term);

					//  Заполняем тему курсовой работы. Берем все записи студента по мероприятию в документе сессии
					//  по МСРП (для которого создается строка), для этих записей подбираем темы работы студента в сессии
					//  с не пустой темой и наибольшей датой формирования (фактическая) документа сессии.

					final String theme = new DQLSelectBuilder().fromEntity(SessionProjectTheme.class, "t")
							.top(1).column(property("t", SessionProjectTheme.theme()))
							.where(isNotNull(property("t", SessionProjectTheme.theme())))
							.where(eq(property("t", SessionProjectTheme.slot().studentWpeCAction()), value(wpePart)))
							.order(property("t", SessionProjectTheme.slot().document().formingDate()), OrderDirection.desc)
							.createStatement(getSession()).uniqueResult();

					newRow.row.setTheme(theme);
					resultList.add(newRow);
				}
			});
        }

        return resultList;
    }

    /**
     * Создание строки диплома по элементу реестра (практика или ИГА).
     */
    private NewRow createOtherRow(RowContainer container, Map<Integer, Set<String>> termActionMap, IEppEpvRowWrapper epvRowWrapper)
    {
        // Определяем блок по типу элемента реестра
        Class<? extends DiplomaContentRow> rowClass = FormingRowUtils.getDiplomaRowClass(container.type);
        if (rowClass == null && epvRowWrapper != null) {
            // Если по типу элемента реестра не удалось понять, в какой блок поместить новую строку (характерно для просто ИГА),
            // то пытаемся определить по типу строки УПв (если она найдена).
            rowClass = FormingRowUtils.getDiplomaRowClass(epvRowWrapper.getType());
        }

        if (rowClass == null) {
            // Всё-таки это просто ИГА - мы так и не поняли, в какой блок поместить строку.
            return null;
        }

        return createOneRow(container, rowClass, epvRowWrapper, container.term2WpeParts, FormingRowUtils.getRowTerm(termActionMap));
    }

	/**
	 * Контейнер для строк. Equatable - сравнение происходит по названию элемента реестра и группе элемента реестра (EppRegistryElement#_parent).
	 */
    private static class RowContainer {
        private final String title;
        private final EppRegistryStructure type;
        private final int hashCode;
        private Long regElementId;

        private Map<Integer, Set<EppStudentWpePart>> term2WpeParts = Maps.newHashMap();
        // когда отсутствует МСРП по нагрузке, отбираем МСРП из РУП - берем все такие семестры (используется только для подсчета трудоемкости)
        private Map<Integer, Set<EppRegistryElementPart>> terms4EmptyWpe = Maps.newHashMap();

        RowContainer(EppRegistryElement regElement) {
            this.title = regElement.getTitle();
            this.type = regElement.getParent();

            this.hashCode = new HashCodeBuilder()
                    .append(this.title.toLowerCase())
                    .append(this.type)
                    .hashCode();
        }

        void addSlots(Integer term, EppRegistryElement registryElement, EppStudentWpePart wpePart) {
            if (term2WpeParts.isEmpty()) {
                term2WpeParts = SafeMap.get(key -> new HashSet<>());
                regElementId = registryElement.getId();
            } else if (regElementId != null && !regElementId.equals(registryElement.getId())) {
                regElementId = null;
            }

            if (!term2WpeParts.get(term).add(wpePart)) {
                throw new IllegalStateException();
            }
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof RowContainer)) {
                throw new IllegalStateException();
            }
            final RowContainer other = (RowContainer) obj;
            return type.getId().equals(other.type.getId()) &&
                    StringUtils.equalsIgnoreCase(title, other.title);
        }

        @Override
        public int hashCode() {
            return this.hashCode;
        }
    }

	/**
	 * Номер последнего семестра, в котором есть положительные итоговые оценки по указанным МСРП
	 * @param wpeParts МСРП по нагрузке, среди которых ищутся оценки
	 * @return номер последнего семестра (нумерация начинается с единицы) с положительной итоговой оценкой, или ноль, если таких оценок нет
	 */
	private int maxTermWithPositiveFinalMark(Collection<EppStudentWpePart> wpeParts)
	{
		final String markAlias = "mark";
		final String wpePartAlias = "wpePart";
		final String docAlias = "doc";
		Integer term = new DQLSelectBuilder().fromEntity(SessionMark.class, markAlias)
				.joinPath(DQLJoinType.inner, SessionMark.slot().studentWpeCAction().fromAlias(markAlias), wpePartAlias)
				.column(property(wpePartAlias, EppStudentWpeCAction.studentWpe().term().intValue())).top(1)
				.where(in(property(wpePartAlias), wpeParts))
				.where(eq(property(markAlias, SessionMark.cachedMarkPositiveStatus()), value(Boolean.TRUE)))
				.where(eq(property(markAlias, SessionMark.slot().inSession()), value(Boolean.FALSE)))
				.joinPath(DQLJoinType.inner, SessionMark.slot().document().fromAlias(markAlias), docAlias)
				.where(instanceOf(docAlias, SessionStudentGradeBookDocument.class))
				.order(property(wpePartAlias, EppStudentWpeCAction.studentWpe().term().intValue()), OrderDirection.desc)
				.createStatement(getSession()).uniqueResult();
		return term == null ? 0 : term;
	}

	/**
	 * Для каждой строки найти хронологически последний семестр с положительной итоговой оценкой и удалить из строки все МСРП после этого семестра.
	 * Строки, по которым нет ни одной положительной оценки, удалить.
	 * @param newRowMap Отсортированные по блокам врапперы новых строк. Ключи в рамках данного метода не важны.
	 */
	private void removeWpePartsAfterLatestPositiveFinalMark(Multimap<Class<? extends DiplomaContentRow>, NewRow> newRowMap)
	{
		Multimap<Class<? extends DiplomaContentRow>, NewRow> rowsWithoutPositiveFinalMarks = HashMultimap.create();
		newRowMap.entries().forEach((rowEntry) ->
				{
					List<EppStudentWpePart> wpeParts = rowEntry.getValue().term2WpeParts.entrySet().stream()
							.flatMap(entry -> entry.getValue().stream())
							.collect(Collectors.toList());
					int maxNeededTerm = maxTermWithPositiveFinalMark(wpeParts);
					if (maxNeededTerm == 0)
					{
						rowsWithoutPositiveFinalMarks.put(rowEntry.getKey(), rowEntry.getValue());
						return;
					}
					List<Integer> termsWithoutMarks = rowEntry.getValue().term2WpeParts.keySet().stream().filter(term -> term > maxNeededTerm).collect(Collectors.toList());
					termsWithoutMarks.forEach(term -> rowEntry.getValue().term2WpeParts.remove(term));
				});
		for (Map.Entry<Class<? extends DiplomaContentRow>, NewRow> entry: rowsWithoutPositiveFinalMarks.entries())
			newRowMap.remove(entry.getKey(), entry.getValue());
	}

    private Multimap<Class<? extends DiplomaContentRow>, NewRow> formingRows(DiplomaObject diplomaObject, String algCode, InfoCollector infoCollector)
    {
        //TODO это пока что ппц. Надо переписать на нормальные интерфейсы и объединить с аналогичным механизмом в шаблоне диплома
        //TODO +1

        // Строки УПв в мапе с ключом по индексу { [epvRow.storedIndex.toLowerCase] -> [epvRowWrapper] }
		final Map<String, List<IEppEpvRowWrapper>> epvIndexMap = SafeMap.get(key -> new ArrayList<>(1));
        DiplomaTemplateManager.instance().dao()
                .getEpvBlockRows(diplomaObject.getStudentEpv().getEduPlanVersion(), diplomaObject.getContent().getProgramSpecialization()).stream()
                .filter(w -> w.getRow() instanceof EppEpvRegistryRow)
                .forEach(w -> {
                    final String index = w.getRow().getStoredIndex();
                    if (StringUtils.isNotEmpty(index)) {
						epvIndexMap.get(index.toLowerCase()).add(w);
                    }
                });

        // Группируем по названию эелемента реестра и семестру.
        // Группировать по названию, а не по элементу реестра, приходится из-за возможного бардака в реестре:
        // у студента может оказаться два МСРП на разные элементы реестра с совпадающими названиями (в САФУ, например)

        // { [RowContainer] -> [RowContainer] }, чтобы иметь сет(!) контейнеров без лишней возни с ключами (ключ реализован в самом контейнере в equals).
        // SafeMap защищает от перезаписи ключа, т.е. контейнер не перезапишется новым объетом, и связи с видами УГС добавятся все в один нужный контейнер.
        final Map<RowContainer, RowContainer> preRows = SafeMap.get(key -> key);
        final Map<Long, Integer> termIdMap = DevelopGridDAO.getIdToTermNumberMap();

        DQLSelectBuilder studentWpeParts = new DQLSelectBuilder().fromEntity(EppStudentWpePart.class, "e")
                .column(property("e"))
                .fetchPath(DQLJoinType.inner, EppStudentWpePart.studentWpe().fromAlias("e"), "wpe")
                .fetchPath(DQLJoinType.inner, EppStudentWorkPlanElement.registryElementPart().fromAlias("wpe"), "regElementPart")
                .fetchPath(DQLJoinType.inner, EppRegistryElementPart.registryElement().fromAlias("regElementPart"), "regElement")
                .where(eq(property("wpe", EppStudentWorkPlanElement.student()), value(diplomaObject.getStudent())));

        DQLSelectBuilder studentWpeBuilder = new DQLSelectBuilder().fromEntity(EppStudentWorkPlanElement.class, "wpe").column(property("wpe"))
                .where(notExists(EppStudentWpePart.class, EppStudentWpePart.studentWpe().s(), property("wpe")))
                .where(eq(property("wpe", EppStudentWorkPlanElement.student()), value(diplomaObject.getStudent())));

        // МСРП в большинстве типов документов оставляем только актуальные,
        // для справок об обучении - особым образом (только с положительными итоговыми оценками, актуальные для активных студентов, все независимо от актуальности для неактивных)
        if (!isEducationReference(diplomaObject.getContent().getType()) || diplomaObject.getStudent().getStatus().isActive())
        {
            studentWpeParts
                    .where(isNull(property("wpe", EppStudentWorkPlanElement.removalDate())))
                    .where(isNull(property("e", EppStudentWpePart.removalDate())));

            studentWpeBuilder.where(isNull(property("wpe", EppStudentWorkPlanElement.removalDate())));
        }

        studentWpeParts.createStatement(getSession()).<EppStudentWpePart>list()
                .forEach(wpePart -> {
                    final EppStudentWorkPlanElement wpe = wpePart.getStudentWpe();
                    final EppRegistryElement registryElement = wpe.getRegistryElementPart().getRegistryElement();
                    final RowContainer row = preRows.get(new RowContainer(registryElement));

                    row.addSlots(termIdMap.get(wpe.getTerm().getId()), registryElement, wpePart);
                });

        studentWpeBuilder.createStatement(getSession()).<EppStudentWorkPlanElement>list()
                .forEach(wpe -> {
                    final EppRegistryElementPart part = wpe.getRegistryElementPart();
                    final EppRegistryElement registryElement = part.getRegistryElement();
                    final RowContainer row = preRows.get(new RowContainer(registryElement));
                    final Integer term = termIdMap.get(wpe.getTerm().getId());
                    SafeMap.safeGet(row.terms4EmptyWpe, term, HashSet.class).add(part);
                });

        // Нагрузку надо брать из строки УПв, если она найдена. Если не найдена - из частей реестра.
        // Врапперы эелементов реестра грузятся скопом, поэтому сначала их собираем, потом считаем нагрузки.
        final List<Long> regElementsForLoad = new ArrayList<>(preRows.size());
        final EppFControlActionInfo fcaInfo = EppFControlActionInfo.SUPPLIER.get();

        // { [row.class] -> [[newRowWrapper]] } - группируем новые строки по блоку, чтобы потом удалить все ВКР из мапы, если их больше одной.
        final Multimap<Class<? extends DiplomaContentRow>, NewRow> newRowMap = ArrayListMultimap.create();

        for (final RowContainer container : preRows.keySet())
		{
            final boolean isDiscipline = container.type.isDisciplineElement();

            // Определяем индекс.
            // Если у всех слотов один индекс, то берем его.
            // Если у некоторых он не указан, а у остальных укзаан один и тот же - берем указанный.
            // Если у некоторых или у всех указан, но отличается у любых двух, то считаем, что индекса нет.
            final Set<String> indexSet = new HashSet<>();

            // Создаем мапу распределения форм контроля по семестрам
            // Даже если ФИК нет в семестре, нагрузку надо считать (и семестр вычислять), поэтому в мапе должен быть семестр (просто с пустым списком ФИК).
            // TreeMap { [term] -> [[fca full code]] }
            final Map<Integer, Set<String>> termActionsMap = new TreeMap<>();

            // { [term] -> [[groupType.id]] } слоты курсовых
			final HashMultimap<Integer, EppStudentWpePart> term2CourseWorkWpeParts = HashMultimap.create();
            final Set<Long> regElementIds = new HashSet<>();

            for (final Map.Entry<Integer, Set<EppStudentWpePart>> termEntry : container.term2WpeParts.entrySet())
			{
                final Integer term = termEntry.getKey();
                Set<String> fcaSet = termActionsMap.get(term);
                if (null == fcaSet)
                    termActionsMap.put(term, fcaSet = new HashSet<>());

                for (final EppStudentWpePart slot : termEntry.getValue())
				{
                    final EppStudentWorkPlanElement wpe = slot.getStudentWpe();

                    regElementIds.add(wpe.getRegistryElementPart().getRegistryElement().getId());
                    final String wpeIndex = wpe.getSourceRow() == null ? null : wpe.getSourceRow().getNumber();
                    if (null != wpeIndex)
                        indexSet.add(wpeIndex.toLowerCase());

                    final String fcaFullCode = fcaInfo.getFullCodeByGroupTypeId(slot.getType().getId());
                    if (null == fcaFullCode)
						continue;

					// Для дисциплин курсовые не считаем за форму контроля, т.к. они вместе со связями идут в отдельный блок.
					// Если у дисциплины в семестре есть только курсовая, то приравниваем такой семестр к семестру без ФИК, чтобы правильно вычислять номер семестра.
                    if (isDiscipline && fcaInfo.isCourseWork(fcaFullCode))
						term2CourseWorkWpeParts.put(term, slot);
                    else
                        fcaSet.add(fcaFullCode);
                }
            }

            // Ищем в УПв строку с тем же индексом, если индекс есть и он один и тот же для всех МСРП из контейнера
			final List<IEppEpvRowWrapper> epvRowWrapperList = indexSet.size() == 1 ? epvIndexMap.get(indexSet.iterator().next()) : null;
			// Если в УПв несколько строк с одним индексом - не берем ни одну (хз какая из них правильная)
			final IEppEpvRowWrapper epvRowWrapper = ((null == epvRowWrapperList) || (epvRowWrapperList.size() != 1)) ? null : epvRowWrapperList.get(0);
            if (null == epvRowWrapper)
			{
                // Для этой строки нагрузку придется брать из частей элемента реестра, т.к. подходящая строка УПв по индексу не найдена
                regElementsForLoad.addAll(regElementIds);
            }

			container.terms4EmptyWpe.values().stream()
					.flatMap(Collection::stream)
					.forEach(regElemPart -> regElementsForLoad.add(regElemPart.getRegistryElement().getId()));

            // Формируем строки диплома
            if (isDiscipline)
			{
				for (final NewRow newRow : createDisciplineRows(container, termActionsMap, epvRowWrapper, algCode, term2CourseWorkWpeParts))
					newRowMap.put(newRow.getDiplomaRow().getClass(), newRow);
			}
            else
			{
                final NewRow newRow = createOtherRow(container, termActionsMap, epvRowWrapper);
                if (null != newRow)
                    newRowMap.put(newRow.getDiplomaRow().getClass(), newRow);
            }
        }

        if (newRowMap.get(DiplomaQualifWorkRow.class).size() > 1)
		{
            infoCollector.add("В документе об обучении может быть указана только одна ВКР.");
            newRowMap.removeAll(DiplomaQualifWorkRow.class);
        }

		// Для справок об обучении удаляем из строк все МСРП после последнего семестра с положительной итоговой оценкой.
		// Строки, где таких оценок вообще нет, удаляем
		if (isEducationReference(diplomaObject.getContent().getType()))
			removeWpePartsAfterLatestPositiveFinalMark(newRowMap);

        // Считаем нагрузки для сформированных строк.
        // Если есть ссылка на строку УПв, то суммируем по семестрам. Иначе - берем из частей эелемента реестра.
        final Map<Long, IEppRegElWrapper> regElemWOEpvRow2RegElemWrapper = !regElementsForLoad.isEmpty()
                ? IEppRegistryDAO.instance.get().getRegistryElementDataMap(regElementsForLoad)
                : ImmutableMap.<Long, IEppRegElWrapper>of();

        // Если есть индивидуальный УП, то надо учитывать перемещения семестров при вычислении нагрузки
        final EppCustomEduPlan customPlan = diplomaObject.getStudentEpv().getCustomEduPlan();
        final IEppCustomPlanWrapper customPlanWrapper = customPlan != null ? new EppCustomPlanWrapper(customPlan) : null;

        final DipAggregationMethod aggregationMethod = getCatalogItem(DipAggregationMethod.class, DipAggregationMethodCodes.POSLEDNYAYA_OTSENKA);
        for (NewRow newRowWrapper : newRowMap.values())
        {
            final DiplomaContentRow dipRow = newRowWrapper.getDiplomaRow();
            dipRow.setOwner(diplomaObject.getContent());
            dipRow.setSourceAggregation(aggregationMethod);

            // Собираем части элементов реестра, которые попали в строку
            final Set<EppRegistryElementPart> parts = new HashSet<>();
            // Формируем связи ФИК с частями элемента реестра, которые должны быть включены в строку. согласно набору МСРП
            newRowWrapper.regElemPart2FcaType = new HashSet<>();
            newRowWrapper.term2WpeParts.values().stream().flatMap(Collection::stream).forEach(wpePart -> {
                final EppRegistryElementPart regPart = wpePart.getStudentWpe().getRegistryElementPart();
                parts.add(regPart);
                if (wpePart instanceof EppStudentWpeCAction)
				{
                    final Long fcaId = fcaInfo.getFcaIdByGroupTypeId(wpePart.getType().getId());
                    newRowWrapper.regElemPart2FcaType.add(new PairKey<>(regPart.getId(), fcaId));
                }
            });
            newRowWrapper.terms4EmptyWpe.values().stream().flatMap(Collection::stream).forEach(parts::add);

            if (!(dipRow instanceof DiplomaCourseWorkRow))
			{
                // Для курсовых нагрузка не заполняется
                if (dipRow.getEpvRegistryRow() != null)
				{
                    // Если найдена строка УПв, то берем нагрузки из неё
                    Set<Integer> terms = Sets.newHashSet();
                    terms.addAll(newRowWrapper.term2WpeParts.keySet());
                    terms.addAll(newRowWrapper.terms4EmptyWpe.keySet());

                    final IEppEpvRowWrapper epvRowWrapper = newRowWrapper.epvRowWrapper;
                    if (customPlanWrapper == null)
					{
                        dipRow.setLoadAsDouble(calcLoad(epvRowWrapper, terms, EppLoadType.FULL_CODE_TOTAL_HOURS));
                        dipRow.setLaborAsDouble(calcLoad(epvRowWrapper, terms, EppLoadType.FULL_CODE_LABOR));
                        dipRow.setWeeksAsDouble(calcLoad(epvRowWrapper, terms, EppLoadType.FULL_CODE_WEEKS));
                        dipRow.setAudLoadAsDouble(calcLoad(epvRowWrapper, terms, EppELoadType.FULL_CODE_AUDIT));
                    }
					else
					{
                        // Есть индивидуальный УП - нагрузки берем через него с учетом перемещений семетров
                        dipRow.setLoadAsDouble(calcLoad(customPlanWrapper, epvRowWrapper.getId(), terms, EppLoadType.FULL_CODE_TOTAL_HOURS));
                        dipRow.setLaborAsDouble(calcLoad(customPlanWrapper, epvRowWrapper.getId(), terms, EppLoadType.FULL_CODE_LABOR));
                        dipRow.setWeeksAsDouble(calcLoad(customPlanWrapper, epvRowWrapper.getId(), terms, EppLoadType.FULL_CODE_WEEKS));
                        dipRow.setAudLoadAsDouble(calcLoad(customPlanWrapper, epvRowWrapper.getId(), terms, EppELoadType.FULL_CODE_AUDIT));
                    }
                }
				else
				{
                    dipRow.setLoadAsDouble(calcLoad(regElemWOEpvRow2RegElemWrapper, parts, EppLoadType.FULL_CODE_TOTAL_HOURS));
                    dipRow.setLaborAsDouble(calcLoad(regElemWOEpvRow2RegElemWrapper, parts, EppLoadType.FULL_CODE_LABOR));
                    if (dipRow instanceof DiplomaQualifWorkRow || dipRow instanceof DiplomaStateExamRow)
					{
                        // Для ИГА берем количество недель из самого элемента реестра, т.к. в частях ИГА недели ввести нельзя

                        // Т.к. мы изначально группировали по названию, а не по самому элементу реестра, элементов реестра для строки
                        // может быть несколько. Просто берем первый попавшийся, ибо какой правильный - не ясно.
                        final IEppRegElWrapper regElWrapper = regElemWOEpvRow2RegElemWrapper.get(parts.iterator().next().getRegistryElement().getId());
                        dipRow.setWeeksAsLong(regElWrapper.getWeeks());
                    }
					else
					{
                        dipRow.setWeeksAsDouble(calcLoad(regElemWOEpvRow2RegElemWrapper, parts, EppLoadType.FULL_CODE_WEEKS));
                    }
                    dipRow.setAudLoadAsDouble(calcLoad(regElemWOEpvRow2RegElemWrapper, parts, EppELoadType.FULL_CODE_AUDIT));
                }
            }
        }

        return newRowMap;
    }

    @Override
    public void filterWpePartsAtEducationReference(DQLSelectBuilder wpePartDql, final String wpePartAlias, final String wpeAlias, StudentStatus studentStatus)
    {
        final String slotAlias = "slot";
        final String gradeBookAlias = "gradeBook";
        final String markAlias = "mark";
		DQLSelectBuilder slotsWithPositiveFinalMarks = new DQLSelectBuilder()
				.fromEntity(SessionDocumentSlot.class, slotAlias)
				.where(eq(property(wpePartAlias), property(slotAlias, SessionDocumentSlot.studentWpeCAction())))
				.joinEntity(slotAlias, DQLJoinType.inner, SessionStudentGradeBookDocument.class, gradeBookAlias, eq(property(gradeBookAlias), property(slotAlias, SessionDocumentSlot.document())))
				.joinEntity(slotAlias, DQLJoinType.inner, SessionMark.class, markAlias, eq(property(slotAlias), property(markAlias, SessionMark.slot())))
				.where(eq(property(markAlias, SessionMark.cachedMarkPositiveStatus()), value(Boolean.TRUE)))
				.where(eq(property(slotAlias, SessionDocumentSlot.inSession()), value(Boolean.FALSE)));
		wpePartDql.where(exists(slotsWithPositiveFinalMarks.buildQuery()));
        if (studentStatus.isActive())
		{
			wpePartDql.where(isNull(property(wpeAlias, EppStudentWorkPlanElement.removalDate())));
			wpePartDql.where(isNull(property(wpePartAlias, EppStudentWpeCAction.removalDate())));
		}
    }

    /**
     * Получение суммы нагрузок нужного типа по строке УПв для набора семестров.
     */
    private double calcLoad(IEppEpvRowWrapper rowWrapper, Set<Integer> terms, String loadFullCode)
    {
        double loadSum = .0d;
        for (final Integer term : terms) {
            loadSum += Math.max(rowWrapper.getTotalInTermLoad(term, loadFullCode), .0d);
        }
        return loadSum;
    }

    /**
     * Получение суммы нагрузок нужного типа по строке индивидуального УП для набора семестров.
     */
    private double calcLoad(IEppCustomPlanWrapper customPlanWrapper, Long epvRowId, Set<Integer> terms, String loadFullCode)
    {
        double loadSum = .0d;
        final Map<PairKey<Long, Integer>, IEppCustomPlanRowWrapper> sourceMap = customPlanWrapper.getSourceMap();
        for (final Integer term : terms) {
            final IEppCustomPlanRowWrapper rowWrapper = sourceMap.get(new PairKey<>(epvRowId, term));
            if (rowWrapper != null && !rowWrapper.isExcluded() && !rowWrapper.getSourceRow().hasParentDistributedRow()) {
                loadSum += rowWrapper.getTotalLoad(loadFullCode);
            }
        }
        return loadSum;
    }

	/**
	 * Получить сумму нагрузок нужного типа по всем частям указанного элемента реестра.
	 * @param regElem2RegElemWrapper Соответствие элементов реестра и их врапперов.
	 * @param parts Все части нужного элемента реестра.
	 * @param loadFullCode Код типа нагрузки.
	 */
    private double calcLoad(Map<Long, IEppRegElWrapper> regElem2RegElemWrapper, Collection<EppRegistryElementPart> parts, String loadFullCode)
    {
        double loadSum = .0d;
        for (final EppRegistryElementPart part : parts)
		{
            final IEppRegElWrapper regElemWrapper = regElem2RegElemWrapper.get(part.getRegistryElement().getId());
			final Map<Integer, IEppRegElPartWrapper> partNumber2PartWrapper = regElemWrapper.getPartMap();
			final IEppRegElPartWrapper elemPartWrapper = partNumber2PartWrapper.get(part.getNumber());
            final double load = elemPartWrapper.getLoadAsDouble(loadFullCode);
            if (load > .0d)
                loadSum += load;
        }
        return loadSum;
    }

    /** Враппер для уже существующей в дипломе строки */
    private class OldRow implements IDiplomaRowContainer
    {
        private final DiplomaContentRow row; // Сама строка
        private final List<PairKey<Long, Long>> regElemPart2FcaType = new ArrayList<>(6); // Связи с ФИК части
        private Long regElementId; // id элемента реестра, если все связи ссылаются на один и тот же эелемент. Иначе - null.

        public OldRow(DiplomaContentRow row) {
            this.row = row;
        }

        private void addRel(Long relElementId, PairKey<Long, Long> relKey)
        {
            if (this.regElemPart2FcaType.isEmpty()) {
                this.regElementId = relElementId;
            } else if (this.regElementId != null && !relElementId.equals(this.regElementId)) {
                this.regElementId = null;
            }
            this.regElemPart2FcaType.add(relKey);
        }

        @Override
        public Long getEppEpvRowId() {
            return row.getEpvRegistryRow() != null ? row.getEpvRegistryRow().getId() : null;
        }

        @Override
        public Long getRegElementId() {
            return regElementId;
        }

        @Override
        public DiplomaContentRow getDiplomaRow() {
            return this.row;
        }
    }

    private interface IRowComparator
    {
        boolean compare(NewRow newRow, OldRow oldRow);
        boolean isUpdateLoad(); // Обновлять нагрузку
        boolean isUpdateTerm(); // Обновлять ли семестр
        boolean isUpdateEpvRow(); // Записывать ли ссылку на строку УПв. Обновляется только если её не было!
    }

    private static final IRowComparator TITLE_AND_OLD_RELS_IN_NEW = new IRowComparator()
    {
        @Override public boolean isUpdateLoad() { return true; }
        @Override public boolean isUpdateTerm() { return true; }
        @Override public boolean isUpdateEpvRow() { return true; }
        @Override public boolean compare(NewRow newRow, OldRow oldRow) {
            return newRow.row.getTitle().equals(oldRow.row.getTitle()) &&
                    ((!oldRow.regElemPart2FcaType.isEmpty() && newRow.regElemPart2FcaType.containsAll(oldRow.regElemPart2FcaType)) || (oldRow.regElemPart2FcaType.isEmpty() && newRow.regElemPart2FcaType.isEmpty()));
        }
    };
    private static final IRowComparator TITLE_AND_TERM = new IRowComparator()
    {
        @Override public boolean isUpdateLoad() { return true; }
        @Override public boolean isUpdateTerm() { return false; }
        @Override public boolean isUpdateEpvRow() { return true; }
        @Override public boolean compare(NewRow newRow, OldRow oldRow) {
            return newRow.row.getTerm() == oldRow.row.getTerm() &&
                    newRow.row.getTitle().equals(oldRow.row.getTitle());
        }
    };
    private static final IRowComparator EPV_ROW_AND_TERM = new IRowComparator()
    {
        @Override public boolean isUpdateLoad() { return true; }
        @Override public boolean isUpdateTerm() { return false; }
        @Override public boolean isUpdateEpvRow() { return false; }
        @Override public boolean compare(NewRow newRow, OldRow oldRow) {
            return newRow.row.getTerm() == oldRow.row.getTerm() &&
                    newRow.epvRowWrapper != null &&
                    newRow.epvRowWrapper.getRow().equals(oldRow.row.getEpvRegistryRow());
        }
    };
    private static final IRowComparator REG_EL_AND_TERM = new IRowComparator()
    {
        @Override public boolean isUpdateLoad() { return true; }
        @Override public boolean isUpdateTerm() { return false; }
        @Override public boolean isUpdateEpvRow() { return true; }
        @Override public boolean compare(NewRow newRow, OldRow oldRow) {
            return newRow.row.getTerm() == oldRow.row.getTerm() &&
                    newRow.getRegElementId() != null && newRow.getRegElementId().equals(oldRow.getRegElementId());
        }
    };
    private static final IRowComparator TITLE_REG_EL_AND_TERM_ZERO = new IRowComparator()
    {
        @Override public boolean isUpdateLoad() { return true; }
        @Override public boolean isUpdateTerm() { return true; }
        @Override public boolean isUpdateEpvRow() { return true; }
        @Override public boolean compare(NewRow newRow, OldRow oldRow) {
            return oldRow.row.getTerm() == 0 &&
                    newRow.row.getTitle().equals(oldRow.row.getTitle()) &&
                    newRow.getRegElementId() != null && newRow.getRegElementId().equals(oldRow.getRegElementId());
        }
    };
    private static final IRowComparator TITLE_EPV_ROW_AND_TERM_ZERO = new IRowComparator()
    {
        @Override public boolean isUpdateLoad() { return true; }
        @Override public boolean isUpdateTerm() { return true; }
        @Override public boolean isUpdateEpvRow() { return false; }
        @Override public boolean compare(NewRow newRow, OldRow oldRow) {
            return oldRow.row.getTerm() == 0 &&
                    newRow.row.getTitle().equals(oldRow.row.getTitle()) &&
                    newRow.epvRowWrapper != null && newRow.epvRowWrapper.getRow().equals(oldRow.row.getEpvRegistryRow());
        }
    };


    private void saveRelations(DiplomaContentRow row, Set<PairKey<Long, Long>> savingRels, Set<PairKey<Long, Long>> oldRels, Map<PairKey<Long, Long>, EppRegistryElementPartFControlAction> allRels)
    {
        // Сохраняем новые связи в базу, если таких еще нет для диплома
        for (PairKey<Long, Long> relItem : savingRels)
        {
            if (oldRels.contains(relItem)) {
                continue; // Уже есть такая связь в этой строке или в какой другой. Перетащить мы её не можем.
            }
            final EppRegistryElementPartFControlAction rel = allRels.get(relItem);
            if (rel == null) {
                // Странная ситуация - для МСРП-ФК нет соответствующей формы контроля в части
                log.warn("EppRegistryElementPartFControlAction not found for EppStudentWpePart. EppRegistryElementPart: " + relItem.getFirst() + ", EppFControlActionType: " + relItem.getSecond());
                continue;
            }
            save(new DiplomaContentRegElPartFControlAction(row, rel));
            oldRels.add(relItem);
        }
    }

    private void mergeRows(NewRow newRow, OldRow oldRow, IRowComparator comparator, Set<PairKey<Long, Long>> oldRels, Map<PairKey<Long, Long>, EppRegistryElementPartFControlAction> allRels)
    {
        final DiplomaContentRow updRow = oldRow.row;
        final DiplomaContentRow srcRow = newRow.row;
        if (comparator.isUpdateLoad()) {
            updRow.setLoadAsLong(srcRow.getLoadAsLong());
            updRow.setLaborAsLong(srcRow.getLaborAsLong());
            updRow.setWeeksAsLong(srcRow.getWeeksAsLong());
            updRow.setAudLoadAsLong(srcRow.getAudLoadAsLong());
        }
        if (comparator.isUpdateTerm()) {
            updRow.setTerm(srcRow.getTerm());
        }
        if (comparator.isUpdateEpvRow() && updRow.getEpvRegistryRow() == null) {
            updRow.setEpvRegistryRow(srcRow.getEpvRegistryRow());
        }
        if (StringUtils.isEmpty(updRow.getTheme())) {
            updRow.setTheme(srcRow.getTheme());
        }
        update(updRow);

        // Создаем недостающие связи с ФИК
        saveRelations(updRow, newRow.regElemPart2FcaType, oldRels, allRels);
    }

    private void mergeRows(List<NewRow> newRows, List<OldRow> oldRows, Set<PairKey<Long, Long>> oldRels, Map<PairKey<Long, Long>, EppRegistryElementPartFControlAction> allRels, IRowComparator comparator)
    {
        // Среди всех новых и существующих строк ищем похожие (по ключу) строки и мержин первые две совпавшие
        // Мерж - перенос нагрузок и номера семестра (если ключ предполагает) из вновь сформированной строки в старую. Также переносятся все связи, которых в старой не хватает.
        final Iterator<NewRow> iterator = newRows.iterator();
        while (iterator.hasNext()) {
            final NewRow newRow = iterator.next();
            final Optional<OldRow> foundResult = oldRows.stream()
                    // Сравнения только в рамках одного блока
                    .filter(existRow -> existRow.row.getClass() == newRow.row.getClass() && comparator.compare(newRow, existRow))
                    .findFirst();

            if (foundResult.isPresent()) {
                mergeRows(newRow, foundResult.get(), comparator, oldRels, allRels);
                iterator.remove();
            }
        }
    }

    private void fixTermCollisions(Collection<NewRow> newRowList, Collection<OldRow> oldRowList,
                                   Predicate<DiplomaContentRow> filter,
                                   Function<DiplomaContentRow, MultiKey> keyFunction)
    {
        // Необходимо обнулить семестр для новых строк, если в рамках блока он одинаков для строк, совпадающих по названию или строке УПв
        // т.к. в базе есть констрейнт на это дело.
        final Multimap<MultiKey, IDiplomaRowContainer> map = ArrayListMultimap.create();

        // Ограничения по номеру семестра действуют для всех блоков, кроме курсовых
        newRowList.stream()
                .filter(w -> !(w.row instanceof DiplomaCourseWorkRow) && filter.test(w.row))
                .forEach(w -> map.put(keyFunction.apply(w.row), w));
        oldRowList.stream()
                .filter(w -> !(w.row instanceof DiplomaCourseWorkRow) && filter.test(w.row))
                .forEach(w -> map.put(keyFunction.apply(w.row), w));

        map.asMap().values().stream().filter(c -> c.size() > 1).flatMap(Collection::stream).forEach(rowContainer -> {
            rowContainer.getDiplomaRow().setTerm(0);
            if (rowContainer instanceof OldRow) {
                getSession().update(rowContainer.getDiplomaRow());
            }
        });
    }

    @Override
    public void updateLine(DiplomaObject diplomaObject, InfoCollector infoCollector)
    {
        final Session session = getSession();
        try // TODO надо переписать механизм мержа новых и существующих строк. Сейчас тут полный шлак.
        {
            session.setFlushMode(FlushMode.MANUAL);

            // Формируем строки диплома
            final DipFormingRowAlgorithm algorithm = getFormingRowAlgorithm(diplomaObject.getStudent());
            final String algCode = algorithm.getCode();
            final Multimap<Class<? extends DiplomaContentRow>, NewRow> newRows = formingRows(diplomaObject, algCode, infoCollector);

            if (!newRows.isEmpty()) {

                // Забираем существующие строки строки документа и свзяи с ФИК
                final Map<Long, OldRow> oldRowMap = new HashMap<>();
                for (DiplomaContentRow row : getList(DiplomaContentRow.class, DiplomaContentRow.owner(), diplomaObject.getContent())) {
                    oldRowMap.put(row.getId(), new OldRow(row));
                }
                final Set<PairKey<Long, Long>> oldRels = new HashSet<>(); // Набор ключей по имеющимся связям
                final List<Object[]> relList = new DQLSelectBuilder().fromEntity(DiplomaContentRegElPartFControlAction.class, "dr")
                        .joinPath(DQLJoinType.inner, DiplomaContentRegElPartFControlAction.registryElementPartFControlAction().fromAlias("dr"), "partFCA")
                        .column(property("dr", DiplomaContentRegElPartFControlAction.row().id()))
                        .column(property("partFCA", EppRegistryElementPartFControlAction.part().registryElement().id()))
                        .column(property("partFCA", EppRegistryElementPartFControlAction.part().id()))
                        .column(property("partFCA", EppRegistryElementPartFControlAction.controlAction().id()))
                        .where(eq(property("dr", DiplomaContentRegElPartFControlAction.owner()), value(diplomaObject.getContent())))
                        .createStatement(getSession()).list();

                for (Object[] rel : relList) {
                    final Long rowId = (Long) rel[0];
                    final Long regElId = (Long) rel[1];
                    final Long partId = (Long) rel[2];
                    final Long fcaId = (Long) rel[3];
                    final PairKey<Long, Long> relKey = new PairKey<>(partId, fcaId);
                    oldRowMap.get(rowId).addRel(regElId, relKey);
                    oldRels.add(relKey);
                }

                // Собираем ФИК частей элементов реестра, имеющиеся в базе
                final Map<PairKey<Long, Long>, EppRegistryElementPartFControlAction> allRels = new HashMap<>();
                final List<EppRegistryElementPart> regParts = new ArrayList<>();
                newRows.values().forEach(w -> w.term2WpeParts.values().stream().flatMap(Collection::stream).forEach(wpePart -> regParts.add(wpePart.getStudentWpe().getRegistryElementPart())));

                for (final EppRegistryElementPartFControlAction rel : getList(EppRegistryElementPartFControlAction.class, EppRegistryElementPartFControlAction.part(), regParts)) {
                    allRels.put(new PairKey<>(rel.getPart().getId(), rel.getControlAction().getId()), rel);
                }

                final List<OldRow> oldRowList = new ArrayList<>(oldRowMap.values());
                final List<NewRow> newRowList = new ArrayList<>(newRows.values());

                // Теперь необходимо смержить новые и существующие строки, если есть совпадения по какому-то из ключей.
                // Если нашелся дубль, новую строку не создаем, а обновляем старую недостающими связями, нагрузкой и иногда семестром

                // 1. если совпадают блок, название строки, связи с ФИК (все связи с ФИК для строки из базы содержатся в только что сформированной строке, либо),
                // то добавляем в существующую строку новые связи, обновляем нагрузки и номер семестра.
                // Если в существующей строке есть связи, которых нет в новой строке, то их не трогаем.
                mergeRows(newRowList, oldRowList, oldRels, allRels, TITLE_AND_OLD_RELS_IN_NEW);

                // 2. если совпадают блок, название строки, элемент реестра, связи с ФИК (все связи с ФИК сформированной строки содержатся в строке из базы),
                // тогда сформированная строка полностью включена в существующую и ее добавлять не нужно.
                // Не применять к строкам блоков «Дисциплины» и «Факультативные дисциплины» при втором алгоритме.
                mergeRows(newRowList, oldRowList, oldRels, allRels, new IRowComparator() {
                    @Override public boolean isUpdateLoad() { return false; }
                    @Override public boolean isUpdateTerm() { return false; }
                    @Override public boolean isUpdateEpvRow() { return false; }
                    @Override public boolean compare(NewRow newRow, OldRow oldRow) {

                        // Для второга алгоритма нельзя этим методом мержить в блоках с дисциплинами и факультативами
                        final boolean disc_opt =
                                (newRow.row instanceof DiplomaDisciplineRow || newRow.row instanceof DiplomaOptDisciplineRow) &&
                                        DipFormingRowAlgorithmCodes.EXAMS.equals(algCode);

                        return !disc_opt && newRow.row.getTitle().equals(oldRow.row.getTitle()) &&
                                ((!oldRow.regElemPart2FcaType.isEmpty() && oldRow.regElemPart2FcaType.containsAll(newRow.regElemPart2FcaType)) || (oldRow.regElemPart2FcaType.isEmpty() && newRow.regElemPart2FcaType.isEmpty()));
                    }
                });

                // 3. если совпадают блок, название строки, номер семестра, то добавляем в существующую строку новые связи, обновляем нагрузки.
                mergeRows(newRowList, oldRowList, oldRels, allRels, TITLE_AND_TERM);

                // 4. если совпадают блок, строка УП, номер семестра, то добавляем в существующую строку новые связи, обновляем нагрузки.
                mergeRows(newRowList, oldRowList, oldRels, allRels, EPV_ROW_AND_TERM);

                // 5. если совпадают блок, номер семестра, элемент реестра (на него ссылаются все связи ФИК, в противном случае элемент не определен),
                // то добавляем в существующую строку новые связи, обновляем нагрузки.
                mergeRows(newRowList, oldRowList, oldRels, allRels, REG_EL_AND_TERM);

                if (DipFormingRowAlgorithmCodes.DISC.equals(algCode)) {

                    // 6. только для первого алгоритма: если совпадают блок, название строки, без номера семестра, элемент реестра,
                    // то добавляем в существующую строку новые связи, обновляем нагрузки и номер семестра.
                    // Если в существующей строке есть связи, которых нет в новой строке, то их не трогаем.
                    mergeRows(newRowList, oldRowList, oldRels, allRels, TITLE_REG_EL_AND_TERM_ZERO);

                    // 7. только для первого алгоритма: если совпадают блок, название строки, строка УП, без номера семестра,
                    // то добавляем в существующую строку новые связи, обновляем нагрузки и номер семестра.
                    // Если в существующей строке есть связи, которых нет в новой строке, то их не трогаем.
                    mergeRows(newRowList, oldRowList, oldRels, allRels, TITLE_EPV_ROW_AND_TERM_ZERO);
                }

                // Исправляем возможные коллизии по номеру семестра

                // Сначала по ключу [блок, название, семестр]
                fixTermCollisions(newRowList, oldRowList,
                                  row -> true, // Для всех строк
                                  row -> new MultiKey(row.getClass(), row.getTitle(), row.getTerm()));
                // Потом по ключу [блок, строка УПв, семестр]
                fixTermCollisions(newRowList, oldRowList,
                                  row -> row.getEpvRegistryRow() != null, // Только для строк, у которых есть ссылка на строку УПв.
                                  row -> new MultiKey(row.getClass(), row.getEpvRegistryRow(), row.getTerm()));

                // Теперь надо сохранить в виде строк то, что осталось
                for (final NewRow newRow : newRowList)
				{
					final DiplomaContentRow dipContentRow = newRow.row;
					if (dipContentRow.getTerm() == 0)
						continue;
                    session.save(dipContentRow);
                    saveRelations(dipContentRow, newRow.regElemPart2FcaType, oldRels, allRels);
                }
                session.flush();
            }

            // Проставляем тему ВКР, если её не было
            for (final DiplomaQualifWorkRow row : getList(DiplomaQualifWorkRow.class, DiplomaQualifWorkRow.owner(), diplomaObject.getContent())) {
                if (StringUtils.isEmpty(row.getTheme())) {
                    row.setTheme(diplomaObject.getStudent().getFinalQualifyingWorkTheme());
                    session.update(row);
                }
            }

            // Пересортируем все строки
            DiplomaTemplateManager.instance().dao().resortAllRowsAsEpv(diplomaObject.getContent(), diplomaObject.getStudentEpv().getEduPlanVersion().getEduPlan());

            session.flush();

        } finally {
            getSession().setFlushMode(FlushMode.AUTO);
        }
    }

	private DipFormingRowAlgorithm getFormingRowAlgorithm(Student student)
	{
		OrgUnit orgUnit = student.getEducationOrgUnit().getEducationLevelHighSchool().getOrgUnit();
		DipFormingRowAlgorithm algorithmByOrgUnit = tryGetFormingRowAlgorithmByOrgUnit(orgUnit);
		if (algorithmByOrgUnit != null)
			return algorithmByOrgUnit;
		return getNotNull(DipFormingRowAlgorithm.class, DipFormingRowAlgorithm.currentAlg(), true);
	}

	private DipFormingRowAlgorithm tryGetFormingRowAlgorithmByOrgUnit(OrgUnit orgUnit)
	{
		EduOwnerOrgUnit eduOwnerOrgUnit = getByNaturalId(new EduOwnerOrgUnit.NaturalId(orgUnit));
		if (eduOwnerOrgUnit == null)
			return null;
		DipFormingRowAlgorithmOrgUnit algorithmOu = getByNaturalId(new DipFormingRowAlgorithmOrgUnit.NaturalId(eduOwnerOrgUnit));
		if (algorithmOu == null)
			return null;
		return algorithmOu.getDipFormingRowAlgorithm();
	}

    @Override
    public DiplomaObject saveDiplomaObject(DiplomaObject dipObject, DiplomaTemplate dipTemplate)
    {
        DiplomaContent content = dipObject.getContent();
        DipDocumentType type = content.getType();
        return saveDiplomaObject(dipObject, type, content.getProgramSubject(), content.getProgramSpecialization(), dipTemplate, type.getTitle(), content.isWithSuccess(), content.getLoadAsDouble());
    }

    @Override
    public DiplomaObject saveDiplomaObject(DiplomaObject diplomaObject, DipDocumentType documentType, EduProgramSubject programSubject, EduProgramSpecialization specialization,
                                           DiplomaTemplate diplomaTemplate, String educationElementTitle, Boolean withSuccess, Double loadAsDouble)
    {
        NamedSyncInTransactionCheckLocker.register(getSession(), DIP_DOCUMENT_ADD_EDIT_SYNC, 300);

        DiplomaContent diplomaContent = new DiplomaContent();
        diplomaContent.setType(documentType);
        save(diplomaContent);

        diplomaObject.setContent(diplomaContent);
        save(diplomaObject);

        saveDiplomaContentRowByTemplate(diplomaTemplate, diplomaContent);

        // Далее заполняем остальные даныне входящими параметрами.
        // Часть из них могла быть записана в saveDiplomaContentRowByTemplate, поэтому заполняем после его вызова.
        diplomaContent.setEducationElementTitle(educationElementTitle);
        diplomaContent.setWithSuccess(withSuccess);
        diplomaContent.setProgramSubject(programSubject);
        diplomaContent.setLoadAsDouble(loadAsDouble);
        diplomaContent.setProgramSpecialization(specialization);
		diplomaContent.setProgramQualification(getQualification(diplomaObject));

        update(diplomaContent);

    /*
    *  При формировании документа о полученном образовании и его содержания.
    *  Проверяется существуют ли Переименования образовательной организации с датой переименования,
    *  попадающей между текущим временем и датой зачисления студента.
    *
    * Если есть такие переименования, то при добавлении документа создаются переименования обр. орг.
    */
        Date enrDate = new DQLSelectBuilder().fromEntity(OrderData.class, "o")
                .column(property("o", OrderData.eduEnrollmentOrderEnrDate()))
                .where(eqValue(property("o", OrderData.student()), diplomaObject.getStudent()))
                .where(isNotNull(property("o", OrderData.P_EDU_ENROLLMENT_ORDER_ENR_DATE)))
                .createStatement(getSession()).uniqueResult();

        if (null != enrDate)
        {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(AcademyRename.class, "a")
                    .column(property("a"))
                    .where(betweenDays(AcademyRename.date().fromAlias("a"), enrDate, new Date()));

            for (AcademyRename academyRename : this.<AcademyRename>getList(builder))
            {
                save(new DiplomaAcademyRenameData(academyRename, diplomaObject.getContent()));
            }
        }
        return diplomaObject;
    }

    private void saveDiplomaContentRowByTemplate(DiplomaTemplate dipTemplate, DiplomaContent dipContent)
    {
        if (null == dipTemplate) return;

        final Map<DiplomaContentRow, List<EppRegistryElementPartFControlAction>> rowMap = SafeMap.get(ArrayList.class);
        for (DiplomaContentRegElPartFControlAction dipFca : getList(DiplomaContentRegElPartFControlAction.class, DiplomaContentRegElPartFControlAction.owner(), dipTemplate.getContent()))
        {
            rowMap.get(dipFca.getRow()).add(dipFca.getRegistryElementPartFControlAction());
        }

        final List<DiplomaContentRow> templateRowList = getList(DiplomaContentRow.class, DiplomaContentRow.L_OWNER, dipTemplate.getContent(), DiplomaContentRow.P_NUMBER);

        DiplomaContent tContent = dipTemplate.getContent();
        dipContent.setProgramSpecialization(tContent.getProgramSpecialization());
        dipContent.setLaborAsLong(tContent.getLaborAsLong());
        dipContent.setWeeksAsLong(tContent.getWeeksAsLong());
        update(tContent);

        DiplomaObject diplomaObject = getNotNull(DiplomaObject.class, DiplomaObject.content(), dipContent);

        for (DiplomaContentRow templateRow : templateRowList)
        {
            final DiplomaContentRow dipRow;
            try
            {
                dipRow = (DiplomaContentRow) templateRow.getEntityMeta().getEntityClass().newInstance();
            }
            catch (InstantiationException | IllegalAccessException e)
            {
                throw CoreExceptionUtils.getRuntimeException(e);
            }

            dipRow.update(templateRow, false);
            dipRow.setOwner(dipContent);
            dipRow.setRowContentUpdateDate(new Date());
            dipRow.setRowValueUpdateDate(new Date());
            dipRow.setMark(null);
            dipRow.setEducation(null);

            if (templateRow instanceof DiplomaQualifWorkRow)
            {
                dipRow.setTheme(diplomaObject.getStudent().getFinalQualifyingWorkTheme());
            }

            save(dipRow);

            getSession().flush();

            final short entityCode = EntityRuntime.getMeta(DiplomaContentRegElPartFControlAction.class).getEntityCode();
            Iterables.partition(rowMap.get(templateRow), DQL.MAX_VALUES_ROW_NUMBER).forEach(elements -> {
                DQLInsertValuesBuilder insertBuilder = new DQLInsertValuesBuilder(DiplomaContentRegElPartFControlAction.class);
                elements.forEach(data -> {
                    insertBuilder.value(DiplomaContentRegElPartFControlAction.P_ID, EntityIDGenerator.generateNewId(entityCode));
                    insertBuilder.value(DiplomaContentRegElPartFControlAction.L_OWNER, dipRow.getOwner());
                    insertBuilder.value(DiplomaContentRegElPartFControlAction.L_REGISTRY_ELEMENT_PART_F_CONTROL_ACTION, data);
                    insertBuilder.value(DiplomaContentRegElPartFControlAction.L_ROW, dipRow);
                    insertBuilder.addBatch();
                });
                insertBuilder.createStatement(getSession()).execute();
            });

            getSession().flush();
            getSession().clear();
        }
    }

	private EduProgramQualification getQualification(DiplomaObject diplomaObject)
	{
		if (diplomaObject.getStudentEpv() == null)
			return diplomaObject.getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getAssignedQualification();

		EppEduPlan eduPlan = diplomaObject.getStudentEpv().getEduPlanVersion().getEduPlan();
		return (eduPlan instanceof EppEduPlanProf) ? ((EppEduPlanProf)eduPlan).getProgramQualification() : null;
	}

    @Override
    public void updateDiplomaObject(DiplomaObject diplomaObject, String educationElementTitle, DipDocumentType dipDocumentType, Boolean withSuccess, Double loadAsDouble)
    {
        update(diplomaObject);
        DiplomaContent content = diplomaObject.getContent();
        content.setEducationElementTitle(educationElementTitle);
        content.setType(dipDocumentType);
        content.setWithSuccess(withSuccess);
        content.setLoadAsDouble(loadAsDouble);
        update(content);
    }

    @Override
    public void saveDiplomaIssuance(DiplomaIssuance diplomaIssuance, List<DiplomaContentIssuance> issuanceContentList, Map<Long, DipDiplomaBlank> oldBlankMap)
    {
        if (diplomaIssuance.getScanCopy() != null)
        {
            saveOrUpdate(diplomaIssuance.getScanCopy());
        }

        updateBlankState(diplomaIssuance.getId() == null ? null : oldBlankMap.get(diplomaIssuance.getId()), diplomaIssuance.getBlank());
        saveOrUpdate(diplomaIssuance);

        if (diplomaIssuance.getId() != null)
        {
            List<DiplomaContentIssuance> existsContents = getList(DiplomaContentIssuance.class, DiplomaContentIssuance.diplomaIssuance(), diplomaIssuance);
            for (DiplomaContentIssuance item : existsContents)
            {
                if (!issuanceContentList.contains(item))
                {
                    updateBlankState(oldBlankMap.get(item.getId()), null);
                    delete(item);
                }
            }

            for (DiplomaContentIssuance item : issuanceContentList)
            {
                item.setDiplomaIssuance(diplomaIssuance);
                updateBlankState(item.getId() == null ? null : oldBlankMap.get(item.getId()), item.getBlank());
                saveOrUpdate(item);
            }
        }
    }

    // старые - свободны, новые - связаны
    private void updateBlankState(@Nullable DipDiplomaBlank oldBlank, @Nullable DipDiplomaBlank newBlank)
    {
        if (oldBlank != null && !oldBlank.equals(newBlank))
        {
            oldBlank.setBlankState(this.getCatalogItem(DipBlankState.class, DipBlankStateCodes.FREE));
            this.update(oldBlank);
        }

        if (newBlank != null && !newBlank.equals(oldBlank))
        {
            newBlank.setBlankState(this.getCatalogItem(DipBlankState.class, DipBlankStateCodes.LINKED));
            this.update(newBlank);
        }
    }

    private void updateDiplomaAcademyRenameDataListForContent(DiplomaContent content, List<DiplomaAcademyRenameData> diplomaAcademyRenameDataList)
    {
        if (null != content.getId())
        {
            List<DiplomaAcademyRenameData> existsDipRenameData = getList(DiplomaAcademyRenameData.class, DiplomaAcademyRenameData.diplomaContent(), content);
            for (DiplomaAcademyRenameData item : existsDipRenameData)
            {
                if (!diplomaAcademyRenameDataList.contains(item))
                    delete(item);
            }
            getSession().flush();
            for (DiplomaAcademyRenameData item : diplomaAcademyRenameDataList)
            {
                item.setDiplomaContent(content);
                saveOrUpdate(item);
            }

            getSession().flush();

        }
    }

    @Override
    public void saveAdditionalDiplomaData(DiplomaContent content, List<DiplomaAcademyRenameData> diplomaAcademyRenameDataList,
                                          DipAdditionalInformation additionalInformation, List<EduProgramForm> eduProgramFormList,
                                          List<DipEduInOtherOrganization> dipEduInOtherOrganizationList)
    {
        updateDiplomaAcademyRenameDataListForContent(content, diplomaAcademyRenameDataList);
        saveOrUpdate(additionalInformation);
        getSession().flush();

        List<DipAddInfoEduForm> existAddInfoEduFormList = getList(DipAddInfoEduForm.class, DipAddInfoEduForm.dipAdditionalInformation(), additionalInformation);
	    existAddInfoEduFormList.forEach(this::delete);

        getSession().flush();

        if (eduProgramFormList != null)
        {
            for (EduProgramForm programForm : eduProgramFormList)
            {
                DipAddInfoEduForm addInfoEduForm = new DipAddInfoEduForm();
                addInfoEduForm.setDipAdditionalInformation(additionalInformation);
                addInfoEduForm.setEduProgramForm(programForm);
                save(addInfoEduForm);
            }
        }

        getSession().flush();

        List<DipEduInOtherOrganization> existsEduInOtherOrganizationList = getList(DipEduInOtherOrganization.class, DipEduInOtherOrganization.dipAdditionalInformation(), additionalInformation);
        for (DipEduInOtherOrganization item : existsEduInOtherOrganizationList)
        {
            if (!dipEduInOtherOrganizationList.contains(item))
                delete(item);
        }
        getSession().flush();

	    dipEduInOtherOrganizationList.forEach(this::saveOrUpdate);
    }

    @Override
    public boolean isCanDeleteDipDocument(DiplomaObject diplomaObject)
    {
        return (!existsEntity(DipStuExcludeExtract.class, DipStuExcludeExtract.L_DIPLOMA, diplomaObject));
    }

    @Override
    public void deleteDipDocument(Long dipDocumentId)
    {
        DiplomaObject diplomaObject = getNotNull(dipDocumentId);
        NamedSyncInTransactionCheckLocker.register(getSession(), DiplomaTemplateDao.LOCK_DIPLOMA_CONTENT_CHANGES + diplomaObject.getContent().getId());
        delete(diplomaObject);
    }

	@Override
	public void createDiplomaTitles(Collection<Long> studentIds, DiplomaObject diplomaObject, boolean withSuccess, Date issuanceDate, boolean useExistingDiplomas)
	{
		boolean diplomaCreated = false;

		for (EppStudent2EduPlanVersion s2epv : getStudentEpvs(studentIds))
		{
			EppEduPlan eduPlan = s2epv.getEduPlanVersion().getEduPlan();
			String programKindCode = eduPlan.getProgramKind().getCode();
			boolean actualWithSuccess = getActualWithSuccess(programKindCode, withSuccess);
			String foundDipTypeCode = getDipTypeCode(programKindCode, eduPlan);

			if (foundDipTypeCode == null)
				continue;

			DipDocumentType type = getByCode(DipDocumentType.class, foundDipTypeCode);
			if (useExistingDiplomas && existDiplomaForStudent(s2epv.getStudent(), type))
			{
				DiplomaObject existingDiploma = findExistingDiploma(s2epv.getStudent(), type);
				fillDiplomaObjectStateCommissionData(existingDiploma, diplomaObject);
				update(existingDiploma);

				existingDiploma.getContent().setWithSuccess(actualWithSuccess);
				update(existingDiploma.getContent());

				if (issuanceDate != null)
				{
					if (existsEntity(DiplomaIssuance.class, DiplomaIssuance.diplomaObject().s(), existingDiploma))
					{
						final String existIssAlias = "existDipIssuance";
						DQLSelectBuilder issuanceDql = new DQLSelectBuilder().fromEntity(DiplomaIssuance.class, existIssAlias)
								.where(eq(property(existIssAlias, DiplomaIssuance.diplomaObject()), value(existingDiploma)));
						DiplomaIssuance existingIssuance = getLastByProperty(issuanceDql, existIssAlias, DiplomaIssuance.issuanceDate(), DiplomaIssuance.id());
						existingIssuance.setIssuanceDate(issuanceDate);
						update(existingIssuance);
					}
					else
						createDiplomaIssuance(issuanceDate, existingDiploma);
				}
			}
			else
			{
				DiplomaObject newDipObject = createAndSaveDiplomaAndContent(diplomaObject, s2epv, eduPlan, actualWithSuccess, type);
				if (issuanceDate != null)
					createDiplomaIssuance(issuanceDate, newDipObject);
			}
			diplomaCreated = true;
		}

		if (!diplomaCreated)
			throw new ApplicationException("Для выбранных студентов не создано ни одного документа об обучении.");
	}

	/** Получить список учебных планов студентов. Сортировка - по полному ФИО. */
	private List<EppStudent2EduPlanVersion> getStudentEpvs(Collection<Long> studentIds)
	{
		// Чтобы не генерить на каждое сравнение при сортировке строку fullFio из трех строк, делаем компаратор по трем полям, а не по fullFio.
		final Comparator<EppStudent2EduPlanVersion> middleNameComparator = Comparator.nullsFirst(Comparator.comparing(stuEpv -> stuEpv.getStudent().getPerson().getIdentityCard().getMiddleName()));
		final Comparator<EppStudent2EduPlanVersion> fioComparator = Comparator.comparing((EppStudent2EduPlanVersion stuEpv) -> stuEpv.getStudent().getPerson().getIdentityCard().getLastName())
				.thenComparing(stuEpv -> stuEpv.getStudent().getPerson().getIdentityCard().getFirstName())
				.thenComparing(middleNameComparator);
		return IEppEduPlanDAO.instance.get().getActiveStudentEduplanVersionRelationMap(studentIds).values().stream()
				.sorted(fioComparator).collect(Collectors.toList());
	}

	/** Значение "с отличием" с поправкой на вид образовательной программы: для программы аспирантуры (адъюнктуры) всегда {@code false}. */
	private static boolean getActualWithSuccess(String programKindCode, boolean withSuccess)
	{
		if (programKindCode.equals(EduProgramKindCodes.PROGRAMMA_ASPIRANTURY_ADYUNKTURY_))
			return false;
		return withSuccess;
	}

	/**
	 * Получить код типа документа об обучении ({@link DipDocumentTypeCodes}) по коду вида образовательной программы ({@link EduProgramKindCodes})
	 * @param programKindCode Вид образовательной программы.
	 * @param eduPlan Учебный план: по коду перечня направлений подготовки из УП уточняется аспирантура/адъюнктура.
	 */
	private static String getDipTypeCode(final String programKindCode, EppEduPlan eduPlan)
	{
		switch (programKindCode)
		{
			case EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_KVALIFITSIROVANNYH_RABOCHIH_SLUJATSHIH_: return DipDocumentTypeCodes.COLLEGE_DIPLOMA;
			case EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV_SREDNEGO_ZVENA: return DipDocumentTypeCodes.COLLEGE_DIPLOMA;
			case EduProgramKindCodes.PROGRAMMA_BAKALAVRIATA: return DipDocumentTypeCodes.BACHELOR_DIPLOMA;
			case EduProgramKindCodes.PROGRAMMA_MAGISTRATURY: return DipDocumentTypeCodes.MAGISTR_DIPLOMA;
			case EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV: return DipDocumentTypeCodes.SPECIALIST_DIPLOMA;
			case EduProgramKindCodes.PROGRAMMA_ASPIRANTURY_ADYUNKTURY_:
			{
				if (!(eduPlan instanceof EppEduPlanProf))
					return null;

				String subjectIndexCode =  ((EppEduPlanProf) eduPlan).getProgramSubject().getSubjectIndex().getCode();
				if (subjectIndexCode.equals(EduProgramSubjectIndexCodes.TITLE_2013_06))
					return DipDocumentTypeCodes.POSTGRADUATE_DIPLOMA;
				if (subjectIndexCode.equals(EduProgramSubjectIndexCodes.TITLE_2013_07))
					return DipDocumentTypeCodes.ADUNCTURE_DIPLOMA_;
			}
			case EduProgramKindCodes.DOPOLNITELNAYA_PROFESSIONALNAYA_PROGRAMMA: return DipDocumentTypeCodes.PROFESSION_RETRAIN_DIPLOMA_;
		}
		return null;
	}

	/** Существует ли документ об обучении указанного типа у студента. */
	private boolean existDiplomaForStudent(Student student, DipDocumentType type)
	{
		return existsEntity(DiplomaObject.class, DiplomaObject.student().s(), student, DiplomaObject.content().type().s(), type);
	}

	/** Найти существующий документ об обучении указанного типа для студента. Если таких документов несколько - вернуть с наибольшим id. */
	private DiplomaObject findExistingDiploma(Student student, DipDocumentType type)
	{
		final String existDiplomaAlias = "existDiploma";
		DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(DiplomaObject.class, existDiplomaAlias)
				.where(eq(property(existDiplomaAlias, DiplomaObject.student()), value(student)))
				.where(eq(property(existDiplomaAlias, DiplomaObject.content().type()), value(type)));
		return getLastByProperty(dql, existDiplomaAlias, DiplomaObject.id());
	}

	/**
	 * Скопировать данные (номер протокола ГЭК {@link DiplomaObject#stateCommissionProtocolNumber}, ФИО председателя ГЭК {@link DiplomaObject#stateCommissionChairFio} и
	 * дату заседания ГЭК {@link DiplomaObject#stateCommissionDate}) из одного документа об обучения в другой.
	 */
	private void fillDiplomaObjectStateCommissionData(DiplomaObject target, DiplomaObject source)
	{
		if (source.getStateCommissionProtocolNumber() != null)
			target.setStateCommissionProtocolNumber(source.getStateCommissionProtocolNumber());
		if (source.getStateCommissionChairFio() != null)
			target.setStateCommissionChairFio(source.getStateCommissionChairFio());
		if (source.getStateCommissionDate() != null)
			target.setStateCommissionDate(source.getStateCommissionDate());
	}

	/**
	 * Получить из базы экземпляр сущности с наибольшим значением какого-либо свойства или комбинации свойств.
	 * @param dql Запрос на сущность (возможно, с фильтрацией, но без сортировки).
	 * @param alias Алиас сущности.
	 * @param propertyPaths Пути к свойству (в порядке приоритета).
	 * @param <T> Тип сущности.
	 * @return {@code null}, если по запросу ничего нет; экземпляр сущности из подходящих под запрос, у которого наибольшее значение свойства.
	 */
	private <T> T getLastByProperty(DQLSelectBuilder dql, String alias, PropertyPath... propertyPaths)
	{
        for (PropertyPath path : propertyPaths)
            dql.order(property(alias, path), OrderDirection.desc);
        return dql.top(1).createStatement(getSession()).uniqueResult();
	}

	/**
	 * Создать и сохранить в базу документ об обучении и содержание документа об обучении.
	 * @param inputDataHolder Ни много ни мало - хранилище целых трех текстовых полей про ГЭК, которые будут внесены в документ об обучении (см. {@link DipDocumentDao#fillDiplomaObjectStateCommissionData}).
	 * @param s2epv Учебный план студента.
	 * @param eduPlan Учебный план - для УП проф. образования в содержание документа записываются трудоемкость (в ЗЕ и неделях) и направление подготовки из УП.
	 * @param withSuccess С отличием.
	 * @param docType Тип документа.
	 * @return Созданный (и уже сохраненный в базу) документ об обучении.
	 */
	private DiplomaObject createAndSaveDiplomaAndContent(DiplomaObject inputDataHolder, EppStudent2EduPlanVersion s2epv, EppEduPlan eduPlan, boolean withSuccess, DipDocumentType docType)
	{
		DiplomaContent diplomaContent = new DiplomaContent();
		if (eduPlan instanceof EppEduPlanProf)
		{
			EppEduPlanProf epProf = (EppEduPlanProf) eduPlan;
			diplomaContent.setLaborAsLong(epProf.getLaborAsLong());
			diplomaContent.setWeeksAsLong(epProf.getWeeksAsLong());
			diplomaContent.setProgramSubject(epProf.getProgramSubject());
			diplomaContent.setProgramQualification(epProf.getProgramQualification());
		}
		if (s2epv.getBlock() instanceof EppEduPlanVersionSpecializationBlock)
			diplomaContent.setProgramSpecialization(((EppEduPlanVersionSpecializationBlock) s2epv.getBlock()).getProgramSpecialization());

		diplomaContent.setType(docType);
		diplomaContent.setWithSuccess(withSuccess);
		save(diplomaContent);

		DiplomaObject dipObject = new DiplomaObject();
		dipObject.setStudentEpv(s2epv);
		dipObject.setStudent(s2epv.getStudent());
		dipObject.setContent(diplomaContent);
		dipObject.setStateCommissionProtocolNumber(inputDataHolder.getStateCommissionProtocolNumber());
		dipObject.setStateCommissionDate(inputDataHolder.getStateCommissionDate());
		dipObject.setStateCommissionChairFio(inputDataHolder.getStateCommissionChairFio());

		save(dipObject);
		return dipObject;
	}

	/**
	 * Создать и сохранить в базу факт выдачи документа об образовании ({@link DiplomaIssuance}).
	 * @param issuanceDate Дата выдачи.
	 * @param dipObject Документ об образовании.
	 */
	private void createDiplomaIssuance(Date issuanceDate, DiplomaObject dipObject)
	{
		String regNumber = DipSettingsManager.instance().dao().getDipIssuanceNextRegNumber(dipObject);
		if (null == regNumber)
			return;

		DiplomaIssuance dipIssuance = new DiplomaIssuance();
		dipIssuance.setDiplomaObject(dipObject);
		dipIssuance.setRegistrationNumber(regNumber);
		dipIssuance.setIssuanceDate(issuanceDate);
		saveOrUpdate(dipIssuance);
	}

	@Override
	public void createDiplomaObject(Collection<Long> studentIds, DiplomaObject diplomaObject, boolean withSuccess, Date issuanceDate, InfoCollector infoCollector)
	{
		HashMap<DiplomaObject, DiplomaTemplate> diploma2Template = new HashMap<>();
        boolean someTemplateNotFound = false;

		for (EppStudent2EduPlanVersion s2epv : getStudentEpvs(studentIds))
		{
			EppEduPlan eduPlan = s2epv.getEduPlanVersion().getEduPlan();
			String programKindCode = eduPlan.getProgramKind().getCode();
			boolean actualWithSuccess = getActualWithSuccess(programKindCode, withSuccess);
			String foundDipTypeCode = getDipTypeCode(programKindCode, eduPlan);

			if (foundDipTypeCode == null)
				continue;

			EduProgramSpecialization specialization = s2epv.getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getEduProgramSpecialization();
			DiplomaTemplate existingDipTemplate = findDiplomaTemplate(s2epv.getEduPlanVersion(), specialization, foundDipTypeCode);
			if (existingDipTemplate == null)
            {
                someTemplateNotFound = true;
                continue;
            }

			DiplomaContent diplomaContent = existingDipTemplate.getContent();
			diplomaContent.setWithSuccess(actualWithSuccess);
			if (eduPlan instanceof EppEduPlanProf)
				diplomaContent.setProgramQualification(((EppEduPlanProf)eduPlan).getProgramQualification());

			DiplomaObject dipObject = new DiplomaObject();
			dipObject.setStudentEpv(s2epv);
			dipObject.setStudent(s2epv.getStudent());
			dipObject.setContent(diplomaContent);
			dipObject.setStateCommissionProtocolNumber(diplomaObject.getStateCommissionProtocolNumber());
			dipObject.setStateCommissionDate(diplomaObject.getStateCommissionDate());
			dipObject.setStateCommissionChairFio(diplomaObject.getStateCommissionChairFio());

			diploma2Template.put(dipObject, existingDipTemplate);
		}

		if (MapUtils.isEmpty(diploma2Template))
		{
			throw new ApplicationException("Для выбранных студентов не создано ни одного документа об обучении, так как не найдены подходящие шаблоны.");
		}
		for (DiplomaObject dipObject : diploma2Template.keySet())
		{
			DiplomaTemplate dipTemplate = diploma2Template.get(dipObject);
			saveDiplomaObject(dipObject, dipTemplate);

			if (issuanceDate != null)
				createDiplomaIssuance(issuanceDate, dipObject);
		}
        if (someTemplateNotFound)
            infoCollector.add("Для некоторых студентов не созданы документы об обучении, так как не найдены подходящие шаблоны.");
	}

	/** Найти шаблон диплома по версии УП, направленности ВПО и типу документа. Если таких шаблонов несколько, взять шаблон с максимальным id. */
	private DiplomaTemplate findDiplomaTemplate(EppEduPlanVersion epv, EduProgramSpecialization specialization, String dipTypeCode)
	{
		final String templateAlias = "template";
		DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(DiplomaTemplate.class, templateAlias)
				.where(eq(property(templateAlias, DiplomaTemplate.eduPlanVersion()), value(epv)))
				.where(eq(property(templateAlias, DiplomaTemplate.content().programSpecialization()), value(specialization)))
				.where(eq(property(templateAlias, DiplomaTemplate.content().type().code()), value(dipTypeCode)));
		return getLastByProperty(dql, templateAlias, DiplomaTemplate.id());
	}

    @Override
    public void saveOrUpdateDiplomaContentRow(List<DiplomaContentRegElPartFControlAction> regElPartFControlActionList, final DiplomaContentRow row)
    {
        row.setRowContentUpdateDate(new Date());
        row.setRowValueUpdateDate(new Date());
        saveOrUpdate(row);

        new MergeAction.SessionMergeAction<DiplomaContentRegElPartFControlAction.NaturalId, DiplomaContentRegElPartFControlAction>() {
            @Override protected DiplomaContentRegElPartFControlAction.NaturalId key(DiplomaContentRegElPartFControlAction source) {
                return new DiplomaContentRegElPartFControlAction.NaturalId(source.getOwner(), source.getRegistryElementPartFControlAction());
            }
            @Override protected DiplomaContentRegElPartFControlAction buildRow(DiplomaContentRegElPartFControlAction source) {
                return new DiplomaContentRegElPartFControlAction(row, source.getRegistryElementPartFControlAction());
            }
            @Override protected void fill(DiplomaContentRegElPartFControlAction target, DiplomaContentRegElPartFControlAction source) {
                target.update(source, false);
            }
        }.merge(
                getList(DiplomaContentRegElPartFControlAction.class, DiplomaContentRegElPartFControlAction.L_ROW, row),
                regElPartFControlActionList
        );

        DiplomaTemplateManager.instance().dao().doNormalizeRowNumbers(row.getClass(), row.getOwner());
    }

    @Override
    public void fillDocumentRowByTemplate(Map<DiplomaObject, DiplomaTemplate> dipDocument2TemplateMap)
    {
        if (dipDocument2TemplateMap.isEmpty()) return;
        for (Map.Entry<DiplomaObject, DiplomaTemplate> entry : dipDocument2TemplateMap.entrySet())
        {
            saveDiplomaContentRowByTemplate(entry.getValue(), entry.getKey().getContent());
        }
    }

    @Override
    public List<Long> getDiplomaWithoutRowIds(Collection<Long> studentIds)
    {
        if (CollectionUtils.isEmpty(studentIds)) return null;

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DiplomaObject.class, "d").column(property("d.id"))
                .where(in(property("d", DiplomaObject.student().id()), studentIds))
                .where(in(property("d", DiplomaObject.content().type().code()), DipDocumentType.ONLY_DIPLOMA_CODES))
				.where(isNotNull(property("d", DiplomaObject.studentEpv())))
                .where(notExists(DiplomaContentRow.class, DiplomaContentRow.owner().s(), property("d", DiplomaObject.content())));

        return builder.createStatement(getSession()).list();
    }

    @Override
    public void saveLastMarkInDiplomaContentRow(DiplomaContent diplomaContent)
    {
        final Session session = getSession();

        List<DiplomaContentRow> rowList = new DQLSelectBuilder().fromEntity(DiplomaContentRow.class, "d")
                .column(property("d"))
                .where(eq(property("d", DiplomaContentRow.owner()), value(diplomaContent)))
                .where(isNull(property("d", DiplomaContentRow.mark())))
                .createStatement(session).list();

        DiplomaObject diplomaObject = getNotNull(DiplomaObject.class, DiplomaObject.content(), diplomaContent);

        for (DiplomaContentRow row : rowList)
        {
            Student student = diplomaObject.getStudent();
            boolean studentActive = student.getStatus().isActive();

            EppRegistryElementPartFControlAction partFCA = new DQLSelectBuilder()
                    .fromEntity(DiplomaContentRegElPartFControlAction.class, "r")
                    .column(property("r", DiplomaContentRegElPartFControlAction.registryElementPartFControlAction()))
                    .top(1)
                    .where(eq(property("r", DiplomaContentRegElPartFControlAction.row()), value(row)))
                    .order(property("r", DiplomaContentRegElPartFControlAction.registryElementPartFControlAction().controlAction().totalMarkPriority()), OrderDirection.asc)
                    .order(property("r", DiplomaContentRegElPartFControlAction.registryElementPartFControlAction().part().number()), OrderDirection.desc)
                    .createStatement(session).uniqueResult();


            if (partFCA != null)
            {
                final DQLSelectBuilder wpSlotDQL = new DQLSelectBuilder()
                        .fromEntity(EppStudentWpeCAction.class, "w")
                        .column(property("w.id")).top(1)
                        .joinPath(DQLJoinType.inner, EppStudentWpeCAction.studentWpe().fromAlias("w"), "wpe")
                        .where(eqValue(property("wpe", EppStudentWorkPlanElement.registryElementPart()), partFCA.getPart()))
                        .where(eqValue(property("w", EppStudentWpeCAction.L_TYPE), partFCA.getControlAction().getEppGroupType()))
                        .where(eqValue(property("wpe", EppStudentWorkPlanElement.student()), student))
                        // Сортируем по семестру
                        .order(property("wpe", EppStudentWorkPlanElement.term().intValue()), OrderDirection.desc)
                        // Сортируем по дате утраты актуальности - активные (без этой даты) самые первые в списке (для этого хитрый coalesce), остальные по дате.
                        .order(DQLFunctions.coalesce(property("w", EppStudentWpeCAction.P_REMOVAL_DATE), valueTimestamp(CoreDateUtils.getNextDayFirstTimeMoment(new Date(), 9000))), OrderDirection.desc);

                if (studentActive)
                    wpSlotDQL.where(isNull(property("wpe", EppStudentWorkPlanElement.removalDate())));
                else
                    wpSlotDQL.where(isNotNull(property("wpe", EppStudentWorkPlanElement.removalDate())));

                final DQLSelectBuilder markDql = new DQLSelectBuilder()
                        .fromEntity(SessionMark.class, "mark")
                        .column(property("mark"))
                        .where(eq(property("mark", SessionMark.slot().studentWpeCAction()), wpSlotDQL.buildQuery()))
                        .order(property("mark", SessionMark.performDate()), OrderDirection.desc);

                for (SessionMark mark : this.<SessionMark>getList(markDql))
                {
                    if (!mark.isInSession() && mark.getSlot().getDocument() instanceof SessionStudentGradeBookDocument) {
                        // итоговые оценки - в зачетке
                        SessionMark regularMark = mark instanceof SessionSlotLinkMark ? ((SessionSlotLinkMark) mark).getTarget() : mark;
                        if (regularMark.getValueItem().isCachedPositiveStatus()) {
                            row.setMark(regularMark.getValueItem().getPrintTitle());
                            session.update(row);
                        }
                        break;
                    }
                }
            }
        }
    }

	@Override
	public void recalculateDiplomaLabor(DiplomaContent diplomaContent)
	{
		final String rowAlias = "row";
		Long totalLabor = new DQLSelectBuilder().fromEntity(DiplomaContentRow.class, rowAlias)
				.where(eq(property(rowAlias, DiplomaContentRow.owner()), value(diplomaContent)))
				.column(sum(property(rowAlias, DiplomaContentRow.laborAsLong())))
				.createStatement(getSession()).uniqueResult();
        diplomaContent.setLaborAsLong(totalLabor == null ? 0 : totalLabor);
        saveOrUpdate(diplomaContent);
	}

    @Override
    public void deleteRow(Long rowId)
    {
        DiplomaContentRow row = get(rowId);
        DiplomaContent content = row.getOwner();
        NamedSyncInTransactionCheckLocker.register(getSession(), DiplomaTemplateDao.LOCK_DIPLOMA_CONTENT_CHANGES + content.getId());
        Class<? extends DiplomaContentRow> rowClass = row.getClass();
        delete(row);
        DiplomaTemplateManager.instance().dao().doNormalizeRowNumbers(rowClass, content);
    }

    @Override
    public Integer getListNumberForNewRow(Class<? extends DiplomaContentRow> rowClass, final DiplomaContent content)
    {
        Preconditions.checkArgument(rowClass != DiplomaContentRow.class); // Нужен конкретный блок

        final Map<Class, Integer> map = SafeMap.get(new SafeMap.Callback<Class, Integer>()
        {
            @Override
            public Integer resolve(Class rowClass)
            {
                return new DQLSelectBuilder().fromEntity(rowClass, "row")
                        .top(1)
                        .column(property("row", DiplomaContentRow.contentListNumber()))
                        .where(eq(property("row", DiplomaContentRow.owner()), value(content)))
                        .order(property("row", DiplomaContentRow.number()), OrderDirection.desc)
                        .createStatement(getSession()).uniqueResult();
            }
        });

        if (rowClass == DiplomaCourseWorkRow.class) {
            // Курсовый и НИР печатаются отдельно. Номера листов у них отдельно ото всех
            return firstNonNull(map.get(rowClass), 1);
        }

        Integer lastNumber = 1;
        for (Class clazz : DiplomaContentRow.DIPLOMA_BLOCK_CLASSES.get().keySet())
        {
            if (rowClass == DiplomaCourseWorkRow.class)
                continue; // Курсовый и НИР печатаются отдельно

            lastNumber = firstNonNull(map.get(clazz), lastNumber);
            if (rowClass == clazz)
                break;
        }
        return lastNumber;
    }

	@Override
	public boolean isEducationReference(DipDocumentType documentType)
	{
		String docTypeCode = (documentType.getParent() == null) ? documentType.getCode() : documentType.getParent().getCode();
		return docTypeCode.equals(DipDocumentTypeCodes.EDUCATION_REFERENCE);
	}

	@Override
	public List<String> regElemTypeWithChildrenCodes(String parentTypeCode)
	{
		ImmutableList.Builder<String> builder = ImmutableList.builder();
		builder.add(parentTypeCode);
		getList(EppRegistryStructure.class).stream()
				.filter(s -> (s.getParent() != null && s.getParent().getCode().equals(parentTypeCode)))
				.forEach(regStr -> builder.add(regStr.getCode()));
		return builder.build();
	}

	@Override
	public boolean resetEpvRegistryRows(EppEduPlanVersion eduPlanVersion)
	{
		final String diplomaObjectAlias = "diplomaObject";
		DQLSelectBuilder dipContentByEpv = new DQLSelectBuilder().fromEntity(DiplomaObject.class, diplomaObjectAlias)
				.where(eq(property(diplomaObjectAlias, DiplomaObject.studentEpv().eduPlanVersion()), value(eduPlanVersion)))
				.column(property(diplomaObjectAlias, DiplomaObject.content()));
		DQLUpdateBuilder updateBuilder = new DQLUpdateBuilder(DiplomaContentRow.class)
				.set(DiplomaContentRow.L_EPV_REGISTRY_ROW, nul())
				.where(in(property(DiplomaContentRow.owner()), dipContentByEpv.buildQuery()));

		boolean success = true;
		try
		{
			executeAndClear(updateBuilder);
		}
		catch (HibernateException e)
		{
			success = false;
		}
		return success;
	}

	@Override
	public boolean isExistsTemplateRowOfEpv(EppEduPlanVersion eduPlanVersion)
	{
		final String diplomaRowAlias = "diplomaRow";
		final String diplomaTemplateAlias = "diplomaTemplate";
		DQLSelectBuilder diplomaTemplateForRows = new DQLSelectBuilder().fromEntity(DiplomaTemplate.class, diplomaTemplateAlias)
				.where(eq(property(diplomaTemplateAlias, DiplomaTemplate.eduPlanVersion()), value(eduPlanVersion)))
				.where(eq(property(diplomaTemplateAlias, DiplomaTemplate.content()), property(diplomaRowAlias, DiplomaContentRow.owner())));
		DQLSelectBuilder rows = new DQLSelectBuilder().fromEntity(DiplomaContentRow.class, diplomaRowAlias)
				.where(exists(diplomaTemplateForRows.buildQuery()))
				.where(eq(property(diplomaRowAlias, DiplomaContentRow.epvRegistryRow().owner().eduPlanVersion()), value(eduPlanVersion)));
		return existsEntity(rows.buildQuery());
	}

    @Override
    public Map<DiplomaIssuance, List<DiplomaContentIssuance>> getDiplomaIssuanceList(DiplomaObject diplom)
    {
        String alias = "di";
        List<Object[]> raws = getList(
                new DQLSelectBuilder()
                        .fromEntity(DiplomaIssuance.class, alias)
                        .column(property(alias))
                        .column(property("dci"))
                        .where(eq(property(alias, DiplomaIssuance.diplomaObject()), value(diplom)))
                        .joinEntity(alias, DQLJoinType.left, DiplomaContentIssuance.class, "dci", eq(property("dci", DiplomaContentIssuance.diplomaIssuance()), property(alias)))
        );

		return raws.stream()
				.collect(Collectors.groupingBy(raw -> (DiplomaIssuance) raw[0],
						() -> new TreeMap<>(Comparator.comparing(DiplomaIssuanceGen::getIssuanceDate)),
						Collectors.mapping(raw -> (DiplomaContentIssuance) raw[1], Collectors.toList())));
	}
}