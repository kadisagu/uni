/* $Id$ */
package ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.ui.ReserveForm;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.DipDiplomaBlankManager;

/**
 * @author azhebko
 * @since 15.01.2015
 */
@Input(@Bind(key = IUIPresenter.PUBLISHER_ID, binding = "blankId", required = true))
public class DipDiplomaBlankReserveFormUI extends UIPresenter
{
    private Long _blankId;
    private String _reservationPurpose;

    public void onClickApply()
    {
        DipDiplomaBlankManager.instance().diplomaBlankDao().doReserveBlank(getBlankId(), getReservationPurpose());
        this.deactivate();
    }

    public Long getBlankId() { return _blankId; }
    public void setBlankId(Long blankId) { _blankId = blankId; }

    public String getReservationPurpose() { return _reservationPurpose; }
    public void setReservationPurpose(String reservationPurpose) { _reservationPurpose = reservationPurpose; }
}