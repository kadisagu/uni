package ru.tandemservice.unidip.bso.entity.catalog;

import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import ru.tandemservice.unidip.bso.entity.catalog.gen.DipBlankStateGen;

/** @see ru.tandemservice.unidip.bso.entity.catalog.gen.DipBlankStateGen */
public class DipBlankState extends DipBlankStateGen implements IDynamicCatalogItem
{
}