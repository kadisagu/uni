/* $Id$ */
package ru.tandemservice.unidip.base.ext.GlobalReport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.GlobalReport.GlobalReportManager;
import org.tandemframework.shared.commonbase.base.bo.GlobalReport.logic.GlobalReportDefinition;
import ru.tandemservice.unidip.bso.bo.DiplomaBlankReport.ui.List.DiplomaBlankReportList;
import ru.tandemservice.unidip.bso.bo.DiplomaSendFisFrdoReport.ui.List.DiplomaSendFisFrdoReportList;

/**
 * @author azhebko
 * @since 22.01.2015
 */
@Configuration
public class GlobalReportExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private GlobalReportManager _globalReportManager;

    @Bean
    public ItemListExtension<GlobalReportDefinition> reportListExtension()
    {
        return this.itemListExtension(_globalReportManager.reportListExtPoint())
            .add("diplomaBlankReport", new GlobalReportDefinition("dip", "diplomaBlank", DiplomaBlankReportList.class.getSimpleName()))
            .add("diplomaSendFisFrdoReport", new GlobalReportDefinition("dip", "diplomaSendFisFrdo", DiplomaSendFisFrdoReportList.class.getSimpleName()))
            .create();
    }
}