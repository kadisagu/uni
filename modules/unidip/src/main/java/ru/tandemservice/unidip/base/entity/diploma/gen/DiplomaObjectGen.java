package ru.tandemservice.unidip.base.entity.diploma.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContent;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaObject;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Документ об обучении
 *
 * Диплом (справка, свидетельство, сертификат) – документ, удостоверяющий получение образования, и (или) квалификации, или проведение обучения по программе.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DiplomaObjectGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unidip.base.entity.diploma.DiplomaObject";
    public static final String ENTITY_NAME = "diplomaObject";
    public static final int VERSION_HASH = -1966861404;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT_EPV = "studentEpv";
    public static final String L_STUDENT = "student";
    public static final String L_CONTENT = "content";
    public static final String P_STATE_COMMISSION_CHAIR_FIO = "stateCommissionChairFio";
    public static final String P_STATE_COMMISSION_PROTOCOL_NUMBER = "stateCommissionProtocolNumber";
    public static final String P_STATE_COMMISSION_DATE = "stateCommissionDate";
    public static final String P_COMMENT = "comment";
    public static final String P_DOCUMENT_TYPE = "documentType";
    public static final String P_ISSUANCE_LIST = "issuanceList";
    public static final String P_ISSUANT_DOCUMENT = "issuantDocument";
    public static final String P_TITLE_WITH_SUCCESS = "titleWithSuccess";

    private EppStudent2EduPlanVersion _studentEpv;     // Связь студента с УП(в)
    private Student _student;     // Студент
    private DiplomaContent _content;     // Содержание
    private String _stateCommissionChairFio;     // ФИО председателя ГЭК
    private String _stateCommissionProtocolNumber;     // Номер протокола ГЭК
    private Date _stateCommissionDate;     // Дата заседания ГЭК
    private String _comment;     // Описание

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Связь студента с УП(в).
     */
    public EppStudent2EduPlanVersion getStudentEpv()
    {
        return _studentEpv;
    }

    /**
     * @param studentEpv Связь студента с УП(в).
     */
    public void setStudentEpv(EppStudent2EduPlanVersion studentEpv)
    {
        dirty(_studentEpv, studentEpv);
        _studentEpv = studentEpv;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Содержание. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public DiplomaContent getContent()
    {
        return _content;
    }

    /**
     * @param content Содержание. Свойство не может быть null и должно быть уникальным.
     */
    public void setContent(DiplomaContent content)
    {
        dirty(_content, content);
        _content = content;
    }

    /**
     * Данные ГЭК о присвоении квалификации.
     *
     * @return ФИО председателя ГЭК.
     */
    @Length(max=255)
    public String getStateCommissionChairFio()
    {
        return _stateCommissionChairFio;
    }

    /**
     * @param stateCommissionChairFio ФИО председателя ГЭК.
     */
    public void setStateCommissionChairFio(String stateCommissionChairFio)
    {
        dirty(_stateCommissionChairFio, stateCommissionChairFio);
        _stateCommissionChairFio = stateCommissionChairFio;
    }

    /**
     * Данные ГЭК о присвоении квалификации.
     *
     * @return Номер протокола ГЭК.
     */
    @Length(max=255)
    public String getStateCommissionProtocolNumber()
    {
        return _stateCommissionProtocolNumber;
    }

    /**
     * @param stateCommissionProtocolNumber Номер протокола ГЭК.
     */
    public void setStateCommissionProtocolNumber(String stateCommissionProtocolNumber)
    {
        dirty(_stateCommissionProtocolNumber, stateCommissionProtocolNumber);
        _stateCommissionProtocolNumber = stateCommissionProtocolNumber;
    }

    /**
     * Данные ГЭК о присвоении квалификации.
     *
     * @return Дата заседания ГЭК.
     */
    public Date getStateCommissionDate()
    {
        return _stateCommissionDate;
    }

    /**
     * @param stateCommissionDate Дата заседания ГЭК.
     */
    public void setStateCommissionDate(Date stateCommissionDate)
    {
        dirty(_stateCommissionDate, stateCommissionDate);
        _stateCommissionDate = stateCommissionDate;
    }

    /**
     * Здесь можно писать комментарий, никакой смысловой нагрузки поле не несет
     *
     * @return Описание.
     */
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Описание.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof DiplomaObjectGen)
        {
            setStudentEpv(((DiplomaObject)another).getStudentEpv());
            setStudent(((DiplomaObject)another).getStudent());
            setContent(((DiplomaObject)another).getContent());
            setStateCommissionChairFio(((DiplomaObject)another).getStateCommissionChairFio());
            setStateCommissionProtocolNumber(((DiplomaObject)another).getStateCommissionProtocolNumber());
            setStateCommissionDate(((DiplomaObject)another).getStateCommissionDate());
            setComment(((DiplomaObject)another).getComment());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DiplomaObjectGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DiplomaObject.class;
        }

        public T newInstance()
        {
            return (T) new DiplomaObject();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "studentEpv":
                    return obj.getStudentEpv();
                case "student":
                    return obj.getStudent();
                case "content":
                    return obj.getContent();
                case "stateCommissionChairFio":
                    return obj.getStateCommissionChairFio();
                case "stateCommissionProtocolNumber":
                    return obj.getStateCommissionProtocolNumber();
                case "stateCommissionDate":
                    return obj.getStateCommissionDate();
                case "comment":
                    return obj.getComment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "studentEpv":
                    obj.setStudentEpv((EppStudent2EduPlanVersion) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "content":
                    obj.setContent((DiplomaContent) value);
                    return;
                case "stateCommissionChairFio":
                    obj.setStateCommissionChairFio((String) value);
                    return;
                case "stateCommissionProtocolNumber":
                    obj.setStateCommissionProtocolNumber((String) value);
                    return;
                case "stateCommissionDate":
                    obj.setStateCommissionDate((Date) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "studentEpv":
                        return true;
                case "student":
                        return true;
                case "content":
                        return true;
                case "stateCommissionChairFio":
                        return true;
                case "stateCommissionProtocolNumber":
                        return true;
                case "stateCommissionDate":
                        return true;
                case "comment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "studentEpv":
                    return true;
                case "student":
                    return true;
                case "content":
                    return true;
                case "stateCommissionChairFio":
                    return true;
                case "stateCommissionProtocolNumber":
                    return true;
                case "stateCommissionDate":
                    return true;
                case "comment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "studentEpv":
                    return EppStudent2EduPlanVersion.class;
                case "student":
                    return Student.class;
                case "content":
                    return DiplomaContent.class;
                case "stateCommissionChairFio":
                    return String.class;
                case "stateCommissionProtocolNumber":
                    return String.class;
                case "stateCommissionDate":
                    return Date.class;
                case "comment":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DiplomaObject> _dslPath = new Path<DiplomaObject>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DiplomaObject");
    }
            

    /**
     * @return Связь студента с УП(в).
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaObject#getStudentEpv()
     */
    public static EppStudent2EduPlanVersion.Path<EppStudent2EduPlanVersion> studentEpv()
    {
        return _dslPath.studentEpv();
    }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaObject#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Содержание. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaObject#getContent()
     */
    public static DiplomaContent.Path<DiplomaContent> content()
    {
        return _dslPath.content();
    }

    /**
     * Данные ГЭК о присвоении квалификации.
     *
     * @return ФИО председателя ГЭК.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaObject#getStateCommissionChairFio()
     */
    public static PropertyPath<String> stateCommissionChairFio()
    {
        return _dslPath.stateCommissionChairFio();
    }

    /**
     * Данные ГЭК о присвоении квалификации.
     *
     * @return Номер протокола ГЭК.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaObject#getStateCommissionProtocolNumber()
     */
    public static PropertyPath<String> stateCommissionProtocolNumber()
    {
        return _dslPath.stateCommissionProtocolNumber();
    }

    /**
     * Данные ГЭК о присвоении квалификации.
     *
     * @return Дата заседания ГЭК.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaObject#getStateCommissionDate()
     */
    public static PropertyPath<Date> stateCommissionDate()
    {
        return _dslPath.stateCommissionDate();
    }

    /**
     * Здесь можно писать комментарий, никакой смысловой нагрузки поле не несет
     *
     * @return Описание.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaObject#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaObject#getDocumentType()
     */
    public static SupportedPropertyPath<String> documentType()
    {
        return _dslPath.documentType();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaObject#getIssuanceList()
     */
    public static SupportedPropertyPath<String> issuanceList()
    {
        return _dslPath.issuanceList();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaObject#getIssuantDocument()
     */
    public static SupportedPropertyPath<String> issuantDocument()
    {
        return _dslPath.issuantDocument();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaObject#getTitleWithSuccess()
     */
    public static SupportedPropertyPath<String> titleWithSuccess()
    {
        return _dslPath.titleWithSuccess();
    }

    public static class Path<E extends DiplomaObject> extends EntityPath<E>
    {
        private EppStudent2EduPlanVersion.Path<EppStudent2EduPlanVersion> _studentEpv;
        private Student.Path<Student> _student;
        private DiplomaContent.Path<DiplomaContent> _content;
        private PropertyPath<String> _stateCommissionChairFio;
        private PropertyPath<String> _stateCommissionProtocolNumber;
        private PropertyPath<Date> _stateCommissionDate;
        private PropertyPath<String> _comment;
        private SupportedPropertyPath<String> _documentType;
        private SupportedPropertyPath<String> _issuanceList;
        private SupportedPropertyPath<String> _issuantDocument;
        private SupportedPropertyPath<String> _titleWithSuccess;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Связь студента с УП(в).
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaObject#getStudentEpv()
     */
        public EppStudent2EduPlanVersion.Path<EppStudent2EduPlanVersion> studentEpv()
        {
            if(_studentEpv == null )
                _studentEpv = new EppStudent2EduPlanVersion.Path<EppStudent2EduPlanVersion>(L_STUDENT_EPV, this);
            return _studentEpv;
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaObject#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Содержание. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaObject#getContent()
     */
        public DiplomaContent.Path<DiplomaContent> content()
        {
            if(_content == null )
                _content = new DiplomaContent.Path<DiplomaContent>(L_CONTENT, this);
            return _content;
        }

    /**
     * Данные ГЭК о присвоении квалификации.
     *
     * @return ФИО председателя ГЭК.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaObject#getStateCommissionChairFio()
     */
        public PropertyPath<String> stateCommissionChairFio()
        {
            if(_stateCommissionChairFio == null )
                _stateCommissionChairFio = new PropertyPath<String>(DiplomaObjectGen.P_STATE_COMMISSION_CHAIR_FIO, this);
            return _stateCommissionChairFio;
        }

    /**
     * Данные ГЭК о присвоении квалификации.
     *
     * @return Номер протокола ГЭК.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaObject#getStateCommissionProtocolNumber()
     */
        public PropertyPath<String> stateCommissionProtocolNumber()
        {
            if(_stateCommissionProtocolNumber == null )
                _stateCommissionProtocolNumber = new PropertyPath<String>(DiplomaObjectGen.P_STATE_COMMISSION_PROTOCOL_NUMBER, this);
            return _stateCommissionProtocolNumber;
        }

    /**
     * Данные ГЭК о присвоении квалификации.
     *
     * @return Дата заседания ГЭК.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaObject#getStateCommissionDate()
     */
        public PropertyPath<Date> stateCommissionDate()
        {
            if(_stateCommissionDate == null )
                _stateCommissionDate = new PropertyPath<Date>(DiplomaObjectGen.P_STATE_COMMISSION_DATE, this);
            return _stateCommissionDate;
        }

    /**
     * Здесь можно писать комментарий, никакой смысловой нагрузки поле не несет
     *
     * @return Описание.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaObject#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(DiplomaObjectGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaObject#getDocumentType()
     */
        public SupportedPropertyPath<String> documentType()
        {
            if(_documentType == null )
                _documentType = new SupportedPropertyPath<String>(DiplomaObjectGen.P_DOCUMENT_TYPE, this);
            return _documentType;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaObject#getIssuanceList()
     */
        public SupportedPropertyPath<String> issuanceList()
        {
            if(_issuanceList == null )
                _issuanceList = new SupportedPropertyPath<String>(DiplomaObjectGen.P_ISSUANCE_LIST, this);
            return _issuanceList;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaObject#getIssuantDocument()
     */
        public SupportedPropertyPath<String> issuantDocument()
        {
            if(_issuantDocument == null )
                _issuantDocument = new SupportedPropertyPath<String>(DiplomaObjectGen.P_ISSUANT_DOCUMENT, this);
            return _issuantDocument;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaObject#getTitleWithSuccess()
     */
        public SupportedPropertyPath<String> titleWithSuccess()
        {
            if(_titleWithSuccess == null )
                _titleWithSuccess = new SupportedPropertyPath<String>(DiplomaObjectGen.P_TITLE_WITH_SUCCESS, this);
            return _titleWithSuccess;
        }

        public Class getEntityClass()
        {
            return DiplomaObject.class;
        }

        public String getEntityName()
        {
            return "diplomaObject";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getDocumentType();

    public abstract String getIssuanceList();

    public abstract String getIssuantDocument();

    public abstract String getTitleWithSuccess();
}
