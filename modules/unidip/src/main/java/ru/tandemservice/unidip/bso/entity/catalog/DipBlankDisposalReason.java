package ru.tandemservice.unidip.bso.entity.catalog;

import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.BaseDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import ru.tandemservice.unidip.bso.entity.catalog.gen.*;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

/** @see ru.tandemservice.unidip.bso.entity.catalog.gen.DipBlankDisposalReasonGen */
public class DipBlankDisposalReason extends DipBlankDisposalReasonGen implements IDynamicCatalogItem
{
    public static IDynamicCatalogDesc getUiDesc()
    {
        return new BaseDynamicCatalogDesc() {

            @Override
            public String getAdditionalPropertiesAddEditPageName() {
                return "ru.tandemservice.unidip.bso.util.additionalPropertiesPages.DipBlankDisposalReasonAdditionalPropertiesAddEdit";
            }
        };
    }
}