package ru.tandemservice.unidip.base.entity.catalog;

import ru.tandemservice.unidip.base.entity.catalog.gen.DipAggregationMethodGen;

/**
 * Алгоритм выставления оценки в диплом
 *
 * указывает класс, который будет обрабатывать строки в УП(в) при выставлении оценок в диплом
 */
public class DipAggregationMethod extends DipAggregationMethodGen
{
}