/* $Id:$ */
package ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unidip.base.entity.catalog.DipDocumentType;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance;
import ru.tandemservice.unidip.base.entity.order.DipStuExcludeExtract;
import ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.ui.Add.DipBlankRowWrapper;
import ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank;
import ru.tandemservice.unidip.bso.entity.catalog.DipBlankDisposalReason;
import ru.tandemservice.unidip.bso.entity.catalog.DipBlankState;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author rsizonenko
 * @since 30.12.2014
 */
public interface IDipDiplomaBlankDao extends INeedPersistenceSupport {

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    Integer saveDiplomaBlanks(@NotNull Collection<DipBlankRowWrapper> rowWrappers);

    /** Меняет состояние бланка на «Зарезервирован», указывает цель резервирования. */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void doReserveBlank(@NotNull Long blankId, String reservationPurpose);

    /** Меняет состояние бланка на «Свободен», сбрасывает цель резервирования. */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void doCancelReserve(@NotNull Long blankId);

    /** Меняет состояние бланков на «Списан», указывает причину и дату списания. */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    Integer doDisposeBlanks(@NotNull Collection<Long> blankIds, DipBlankDisposalReason reason, Date disposalDate);

    /** Меняет состояние бланка на «Свободен», сбрасывает дату и причину списания. */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void doCancelDisposal(@NotNull Long blankId);

	/**
	 * Перевести бланки приложений из состояния «Связан» в состояние «Свободен». Берется факт выдачи документа об образовании, затем все прилинкованные к нему факты выдачи приложений,
	 * после чего бланки из последних переводятся в состояние «Свободен».
	 * @param diplomaIssuance Факт выдачи документа об образовании.
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	void doCancelBlankAppendixesLink(DiplomaIssuance diplomaIssuance);

    /** Меняет место хранения бланков. */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    Integer doChangeStorageLocation(@NotNull Collection<Long> blankIds, @NotNull OrgUnit storageLocation);

    /** Тип бланка, на котором может быть распечатан диплом (приложение). */
    @Nullable
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    String getDiplomaObjectBlankType(@NotNull DipDocumentType documentType, boolean withSuccess, boolean appendix);

    /** Меняет состояние бланков в переданной коллекции на указанное  **/
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void massChangeState(Collection<DipDiplomaBlank> input, DipBlankState state);

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void massDeleteBlanks(Collection<DipDiplomaBlank> input);

    /**
     * Получить бланки по id, при необходимости отфильтровать те, которые находятся в состоянии "Выдан".
     * @param blankIds - id бланков (предполагается, что бланки имеют состояние "Списан" или "Выдан")
     * @param hideIssued - пропустить ли бланки в состоянии "Выдан"
     * @return Список бланков
     */
    List<DipDiplomaBlank> getShownBlanks(Collection<Long> blankIds, boolean hideIssued);

    /**
     * Получить выписки из приказов об отчислении, соответствующие бланкам дипломов.
     * @param blanks Бланки дипломов
     * @return Соответствие бланка и выписки (если выписка не найдена в системе, соответствующий бланк в результате не представлен)
     */
    Map<DipDiplomaBlank, DipStuExcludeExtract> getDiplomaBlanksToExcludeExtracts(List<DipDiplomaBlank> blanks);
}
