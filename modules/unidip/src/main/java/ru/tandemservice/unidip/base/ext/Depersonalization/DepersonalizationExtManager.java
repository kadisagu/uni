package ru.tandemservice.unidip.base.ext.Depersonalization;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.Depersonalization.DepersonalizationManager;
import org.tandemframework.shared.commonbase.base.bo.Depersonalization.logic.IDepersonalizationObject;
import org.tandemframework.shared.commonbase.base.bo.Depersonalization.logic.ScriptItemResetToDefaultObject;
import ru.tandemservice.unidip.base.entity.catalog.DipDocTemplateCatalog;
import ru.tandemservice.unidip.base.entity.catalog.DipScriptItem;

/**
 * @author avedernikov
 * @since 05.10.2015
 */

@Configuration
public class DepersonalizationExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private DepersonalizationManager _depersonalizationManager;

    @Bean
    public ItemListExtension<IDepersonalizationObject> depersonalizationObjectListExtension()
    {
        return _depersonalizationManager.extensionBuilder()
                .add(new ScriptItemResetToDefaultObject(DipDocTemplateCatalog.class))
                .add(new ScriptItemResetToDefaultObject(DipScriptItem.class))
                .build();
    }
}
