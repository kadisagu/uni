/* $Id$ */
package ru.tandemservice.unidip.base.bo.DipDocument.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;

import java.util.List;

/**
 * @author Andrey Avetisov
 * @since 27.08.2014
 */
public class DipEduInOtherOrganizationDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String EDU_OTHER_ORGANIZATION_LIST = "eduOtherOrganizationList";

    public DipEduInOtherOrganizationDSHandler(String ownerId)
    {
        super(ownerId);
    }
    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        List<DataWrapper> eduOtherOrganizationList = context.get(EDU_OTHER_ORGANIZATION_LIST);
        DSOutput output = ListOutputBuilder.get(input, eduOtherOrganizationList).build();
        output.setCountRecord(Math.max(output.getTotalSize(), 1));
        return output;
    }
}
