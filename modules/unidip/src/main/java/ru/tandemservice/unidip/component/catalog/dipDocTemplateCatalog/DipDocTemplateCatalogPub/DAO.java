/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unidip.component.catalog.dipDocTemplateCatalog.DipDocTemplateCatalogPub;

import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultScriptCatalogPub.DefaultScriptCatalogPubDAO;
import ru.tandemservice.unidip.base.entity.catalog.DipDocTemplateCatalog;

/**
 * @author vip_delete
 * @since 13.01.2009
 */
public class DAO extends DefaultScriptCatalogPubDAO<DipDocTemplateCatalog, Model> implements IDAO
{
    @Override
    protected OrderDescriptionRegistry getOrderDescriptionRegistry()
    {
        OrderDescriptionRegistry order = super.getOrderDescriptionRegistry();
        order.setOrders(DipDocTemplateCatalog.dipDocumentType().titleWithParent().s(),
                        new OrderDescription(DipDocTemplateCatalog.dipDocumentType().parent().title()),
                        new OrderDescription(DipDocTemplateCatalog.dipDocumentType().title()),
                        new OrderDescription(DipDocTemplateCatalog.title()));
        return order;
    }

	@Override
	public void doToggleActive(Long templateId)
	{
		DipDocTemplateCatalog template = get(DipDocTemplateCatalog.class, templateId);
		template.setActive(!template.isActive());
		update(template);
		getSession().flush();
	}
}