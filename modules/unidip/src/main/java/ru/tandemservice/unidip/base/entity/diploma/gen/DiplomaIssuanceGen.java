package ru.tandemservice.unidip.base.entity.diploma.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaObject;
import ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Факт выдачи документа об обучении
 *
 * Фиксирует факт выдачи документа и соответствующие ему регистрационные данные.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DiplomaIssuanceGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance";
    public static final String ENTITY_NAME = "diplomaIssuance";
    public static final int VERSION_HASH = 1403649076;
    private static IEntityMeta ENTITY_META;

    public static final String L_DIPLOMA_OBJECT = "diplomaObject";
    public static final String P_REGISTRATION_NUMBER = "registrationNumber";
    public static final String P_ISSUANCE_DATE = "issuanceDate";
    public static final String L_BLANK = "blank";
    public static final String P_BLANK_SERIA = "blankSeria";
    public static final String P_BLANK_NUMBER = "blankNumber";
    public static final String L_REPLACED_ISSUANCE = "replacedIssuance";
    public static final String L_SCAN_COPY = "scanCopy";
    public static final String P_FULL_TITLE = "fullTitle";

    private DiplomaObject _diplomaObject;     // Документ
    private String _registrationNumber;     // Регистрационный номер
    private Date _issuanceDate;     // Дата выдачи
    private DipDiplomaBlank _blank;     // Бланк
    private String _blankSeria;     // Серия бланка
    private String _blankNumber;     // Номер бланка
    private DiplomaIssuance _replacedIssuance;     // Замененный документ
    private DatabaseFile _scanCopy;     // Скан-копия документа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Документ. Свойство не может быть null.
     */
    @NotNull
    public DiplomaObject getDiplomaObject()
    {
        return _diplomaObject;
    }

    /**
     * @param diplomaObject Документ. Свойство не может быть null.
     */
    public void setDiplomaObject(DiplomaObject diplomaObject)
    {
        dirty(_diplomaObject, diplomaObject);
        _diplomaObject = diplomaObject;
    }

    /**
     * Номер по регистрационной книге.
     *
     * @return Регистрационный номер. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getRegistrationNumber()
    {
        return _registrationNumber;
    }

    /**
     * @param registrationNumber Регистрационный номер. Свойство не может быть null.
     */
    public void setRegistrationNumber(String registrationNumber)
    {
        dirty(_registrationNumber, registrationNumber);
        _registrationNumber = registrationNumber;
    }

    /**
     * Дата выдачи по регистрационной книге.
     *
     * @return Дата выдачи. Свойство не может быть null.
     */
    @NotNull
    public Date getIssuanceDate()
    {
        return _issuanceDate;
    }

    /**
     * @param issuanceDate Дата выдачи. Свойство не может быть null.
     */
    public void setIssuanceDate(Date issuanceDate)
    {
        dirty(_issuanceDate, issuanceDate);
        _issuanceDate = issuanceDate;
    }

    /**
     * @return Бланк.
     */
    public DipDiplomaBlank getBlank()
    {
        return _blank;
    }

    /**
     * @param blank Бланк.
     */
    public void setBlank(DipDiplomaBlank blank)
    {
        dirty(_blank, blank);
        _blank = blank;
    }

    /**
     * Серия бланка, на котором напечатан диплом.
     *
     * @return Серия бланка.
     */
    @Length(max=255)
    public String getBlankSeria()
    {
        return _blankSeria;
    }

    /**
     * @param blankSeria Серия бланка.
     */
    public void setBlankSeria(String blankSeria)
    {
        dirty(_blankSeria, blankSeria);
        _blankSeria = blankSeria;
    }

    /**
     * Номер бланка, на котором напечатан диплом.
     *
     * @return Номер бланка.
     */
    @Length(max=255)
    public String getBlankNumber()
    {
        return _blankNumber;
    }

    /**
     * @param blankNumber Номер бланка.
     */
    public void setBlankNumber(String blankNumber)
    {
        dirty(_blankNumber, blankNumber);
        _blankNumber = blankNumber;
    }

    /**
     * Данные о документе, взамен которого выдан этот. Заполняется для документа-дубликата.
     *
     * @return Замененный документ.
     */
    public DiplomaIssuance getReplacedIssuance()
    {
        return _replacedIssuance;
    }

    /**
     * @param replacedIssuance Замененный документ.
     */
    public void setReplacedIssuance(DiplomaIssuance replacedIssuance)
    {
        dirty(_replacedIssuance, replacedIssuance);
        _replacedIssuance = replacedIssuance;
    }

    /**
     * Скан-копия документа об обучении
     *
     * @return Скан-копия документа.
     */
    public DatabaseFile getScanCopy()
    {
        return _scanCopy;
    }

    /**
     * @param scanCopy Скан-копия документа.
     */
    public void setScanCopy(DatabaseFile scanCopy)
    {
        dirty(_scanCopy, scanCopy);
        _scanCopy = scanCopy;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof DiplomaIssuanceGen)
        {
            setDiplomaObject(((DiplomaIssuance)another).getDiplomaObject());
            setRegistrationNumber(((DiplomaIssuance)another).getRegistrationNumber());
            setIssuanceDate(((DiplomaIssuance)another).getIssuanceDate());
            setBlank(((DiplomaIssuance)another).getBlank());
            setBlankSeria(((DiplomaIssuance)another).getBlankSeria());
            setBlankNumber(((DiplomaIssuance)another).getBlankNumber());
            setReplacedIssuance(((DiplomaIssuance)another).getReplacedIssuance());
            setScanCopy(((DiplomaIssuance)another).getScanCopy());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DiplomaIssuanceGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DiplomaIssuance.class;
        }

        public T newInstance()
        {
            return (T) new DiplomaIssuance();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "diplomaObject":
                    return obj.getDiplomaObject();
                case "registrationNumber":
                    return obj.getRegistrationNumber();
                case "issuanceDate":
                    return obj.getIssuanceDate();
                case "blank":
                    return obj.getBlank();
                case "blankSeria":
                    return obj.getBlankSeria();
                case "blankNumber":
                    return obj.getBlankNumber();
                case "replacedIssuance":
                    return obj.getReplacedIssuance();
                case "scanCopy":
                    return obj.getScanCopy();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "diplomaObject":
                    obj.setDiplomaObject((DiplomaObject) value);
                    return;
                case "registrationNumber":
                    obj.setRegistrationNumber((String) value);
                    return;
                case "issuanceDate":
                    obj.setIssuanceDate((Date) value);
                    return;
                case "blank":
                    obj.setBlank((DipDiplomaBlank) value);
                    return;
                case "blankSeria":
                    obj.setBlankSeria((String) value);
                    return;
                case "blankNumber":
                    obj.setBlankNumber((String) value);
                    return;
                case "replacedIssuance":
                    obj.setReplacedIssuance((DiplomaIssuance) value);
                    return;
                case "scanCopy":
                    obj.setScanCopy((DatabaseFile) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "diplomaObject":
                        return true;
                case "registrationNumber":
                        return true;
                case "issuanceDate":
                        return true;
                case "blank":
                        return true;
                case "blankSeria":
                        return true;
                case "blankNumber":
                        return true;
                case "replacedIssuance":
                        return true;
                case "scanCopy":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "diplomaObject":
                    return true;
                case "registrationNumber":
                    return true;
                case "issuanceDate":
                    return true;
                case "blank":
                    return true;
                case "blankSeria":
                    return true;
                case "blankNumber":
                    return true;
                case "replacedIssuance":
                    return true;
                case "scanCopy":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "diplomaObject":
                    return DiplomaObject.class;
                case "registrationNumber":
                    return String.class;
                case "issuanceDate":
                    return Date.class;
                case "blank":
                    return DipDiplomaBlank.class;
                case "blankSeria":
                    return String.class;
                case "blankNumber":
                    return String.class;
                case "replacedIssuance":
                    return DiplomaIssuance.class;
                case "scanCopy":
                    return DatabaseFile.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DiplomaIssuance> _dslPath = new Path<DiplomaIssuance>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DiplomaIssuance");
    }
            

    /**
     * @return Документ. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance#getDiplomaObject()
     */
    public static DiplomaObject.Path<DiplomaObject> diplomaObject()
    {
        return _dslPath.diplomaObject();
    }

    /**
     * Номер по регистрационной книге.
     *
     * @return Регистрационный номер. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance#getRegistrationNumber()
     */
    public static PropertyPath<String> registrationNumber()
    {
        return _dslPath.registrationNumber();
    }

    /**
     * Дата выдачи по регистрационной книге.
     *
     * @return Дата выдачи. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance#getIssuanceDate()
     */
    public static PropertyPath<Date> issuanceDate()
    {
        return _dslPath.issuanceDate();
    }

    /**
     * @return Бланк.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance#getBlank()
     */
    public static DipDiplomaBlank.Path<DipDiplomaBlank> blank()
    {
        return _dslPath.blank();
    }

    /**
     * Серия бланка, на котором напечатан диплом.
     *
     * @return Серия бланка.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance#getBlankSeria()
     */
    public static PropertyPath<String> blankSeria()
    {
        return _dslPath.blankSeria();
    }

    /**
     * Номер бланка, на котором напечатан диплом.
     *
     * @return Номер бланка.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance#getBlankNumber()
     */
    public static PropertyPath<String> blankNumber()
    {
        return _dslPath.blankNumber();
    }

    /**
     * Данные о документе, взамен которого выдан этот. Заполняется для документа-дубликата.
     *
     * @return Замененный документ.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance#getReplacedIssuance()
     */
    public static DiplomaIssuance.Path<DiplomaIssuance> replacedIssuance()
    {
        return _dslPath.replacedIssuance();
    }

    /**
     * Скан-копия документа об обучении
     *
     * @return Скан-копия документа.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance#getScanCopy()
     */
    public static DatabaseFile.Path<DatabaseFile> scanCopy()
    {
        return _dslPath.scanCopy();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance#getFullTitle()
     */
    public static SupportedPropertyPath<String> fullTitle()
    {
        return _dslPath.fullTitle();
    }

    public static class Path<E extends DiplomaIssuance> extends EntityPath<E>
    {
        private DiplomaObject.Path<DiplomaObject> _diplomaObject;
        private PropertyPath<String> _registrationNumber;
        private PropertyPath<Date> _issuanceDate;
        private DipDiplomaBlank.Path<DipDiplomaBlank> _blank;
        private PropertyPath<String> _blankSeria;
        private PropertyPath<String> _blankNumber;
        private DiplomaIssuance.Path<DiplomaIssuance> _replacedIssuance;
        private DatabaseFile.Path<DatabaseFile> _scanCopy;
        private SupportedPropertyPath<String> _fullTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Документ. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance#getDiplomaObject()
     */
        public DiplomaObject.Path<DiplomaObject> diplomaObject()
        {
            if(_diplomaObject == null )
                _diplomaObject = new DiplomaObject.Path<DiplomaObject>(L_DIPLOMA_OBJECT, this);
            return _diplomaObject;
        }

    /**
     * Номер по регистрационной книге.
     *
     * @return Регистрационный номер. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance#getRegistrationNumber()
     */
        public PropertyPath<String> registrationNumber()
        {
            if(_registrationNumber == null )
                _registrationNumber = new PropertyPath<String>(DiplomaIssuanceGen.P_REGISTRATION_NUMBER, this);
            return _registrationNumber;
        }

    /**
     * Дата выдачи по регистрационной книге.
     *
     * @return Дата выдачи. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance#getIssuanceDate()
     */
        public PropertyPath<Date> issuanceDate()
        {
            if(_issuanceDate == null )
                _issuanceDate = new PropertyPath<Date>(DiplomaIssuanceGen.P_ISSUANCE_DATE, this);
            return _issuanceDate;
        }

    /**
     * @return Бланк.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance#getBlank()
     */
        public DipDiplomaBlank.Path<DipDiplomaBlank> blank()
        {
            if(_blank == null )
                _blank = new DipDiplomaBlank.Path<DipDiplomaBlank>(L_BLANK, this);
            return _blank;
        }

    /**
     * Серия бланка, на котором напечатан диплом.
     *
     * @return Серия бланка.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance#getBlankSeria()
     */
        public PropertyPath<String> blankSeria()
        {
            if(_blankSeria == null )
                _blankSeria = new PropertyPath<String>(DiplomaIssuanceGen.P_BLANK_SERIA, this);
            return _blankSeria;
        }

    /**
     * Номер бланка, на котором напечатан диплом.
     *
     * @return Номер бланка.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance#getBlankNumber()
     */
        public PropertyPath<String> blankNumber()
        {
            if(_blankNumber == null )
                _blankNumber = new PropertyPath<String>(DiplomaIssuanceGen.P_BLANK_NUMBER, this);
            return _blankNumber;
        }

    /**
     * Данные о документе, взамен которого выдан этот. Заполняется для документа-дубликата.
     *
     * @return Замененный документ.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance#getReplacedIssuance()
     */
        public DiplomaIssuance.Path<DiplomaIssuance> replacedIssuance()
        {
            if(_replacedIssuance == null )
                _replacedIssuance = new DiplomaIssuance.Path<DiplomaIssuance>(L_REPLACED_ISSUANCE, this);
            return _replacedIssuance;
        }

    /**
     * Скан-копия документа об обучении
     *
     * @return Скан-копия документа.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance#getScanCopy()
     */
        public DatabaseFile.Path<DatabaseFile> scanCopy()
        {
            if(_scanCopy == null )
                _scanCopy = new DatabaseFile.Path<DatabaseFile>(L_SCAN_COPY, this);
            return _scanCopy;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance#getFullTitle()
     */
        public SupportedPropertyPath<String> fullTitle()
        {
            if(_fullTitle == null )
                _fullTitle = new SupportedPropertyPath<String>(DiplomaIssuanceGen.P_FULL_TITLE, this);
            return _fullTitle;
        }

        public Class getEntityClass()
        {
            return DiplomaIssuance.class;
        }

        public String getEntityName()
        {
            return "diplomaIssuance";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getFullTitle();
}
