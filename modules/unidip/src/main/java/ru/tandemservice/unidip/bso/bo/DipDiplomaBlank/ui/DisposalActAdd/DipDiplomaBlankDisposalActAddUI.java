/* $Id$ */
package ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.ui.DisposalActAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unidip.base.entity.catalog.DipScriptItem;
import ru.tandemservice.unidip.base.entity.catalog.codes.DipScriptItemCodes;
import ru.tandemservice.unidip.base.entity.order.DipStuExcludeExtract;
import ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.DipDiplomaBlankManager;
import ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.logic.IDipDiplomaBlankDao;
import ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank;
import ru.tandemservice.unidip.bso.entity.catalog.codes.DipBlankStateCodes;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author azhebko
 * @since 21.01.2015
 */
@Input(@Bind(key = DipDiplomaBlankManager.KEY_BLANKS, binding = "blankIds"))
public class DipDiplomaBlankDisposalActAddUI extends UIPresenter
{
	public static final String PARAM_HIDE_ISSUED = "hideIssued";
	public static final String PARAM_BLANK_IDS = "blankIds";

    private Collection<Long> _blankIds;
    private Date _destructionDate;
    private boolean _hideIssued = true;

    @Override
	public void onBeforeDataSourceFetch(IUIDataSource source)
	{
		source.put(PARAM_BLANK_IDS, _blankIds);
		source.put(PARAM_HIDE_ISSUED, _hideIssued);
	}

    public void onClickPrint()
    {
		IDipDiplomaBlankDao dao = DipDiplomaBlankManager.instance().diplomaBlankDao();
		List<DipDiplomaBlank> blanks = dao.getShownBlanks(_blankIds, _hideIssued);

        Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(
            IUniBaseDao.instance.get().getCatalogItem(DipScriptItem.class, DipScriptItemCodes.BLANK_DISPOSAL_ACT),
            "blanks", blanks,
			"blanksToExtracts", getBlankToExtracts(blanks),
            "destructionDate", getDestructionDate());

        CommonBaseRenderer renderer = new CommonBaseRenderer().rtf().document((byte[]) scriptResult.get(IScriptExecutor.DOCUMENT)).fileName((String) scriptResult.get(IScriptExecutor.FILE_NAME));
        BusinessComponentUtils.downloadDocument(renderer, true);

        this.deactivate();
    }

	private Map<DipDiplomaBlank, DipStuExcludeExtract> getBlankToExtracts(List<DipDiplomaBlank> blanks)
	{
		if (_hideIssued)
			return null;
		List<DipDiplomaBlank> issuedBlanks = blanks.stream().filter(blank -> blank.getBlankState().getCode().equals(DipBlankStateCodes.ISSUED)).collect(Collectors.toList());
        return DipDiplomaBlankManager.instance().diplomaBlankDao().getDiplomaBlanksToExcludeExtracts(issuedBlanks);
	}

	public void onClickDelete()
    {
        _blankIds.remove(this.getListenerParameterAsLong());
    }

    public void setBlankIds(Collection<Long> blankIds) { _blankIds = blankIds; }

    public Collection<Long> getBlankIds()
    {
        return _blankIds;
    }

    public Date getDestructionDate() { return _destructionDate; }
    public void setDestructionDate(Date destructionDate) { _destructionDate = destructionDate; }

    public boolean getHideIssued()
    {
        return  _hideIssued;
    }
    public void setHideIssued(boolean hideIssued)
    {
        _hideIssued = hideIssued;
    }
}