package ru.tandemservice.unidip.base.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.commonbase.catalog.entity.ScriptItem;
import ru.tandemservice.unidip.base.entity.catalog.DipDocTemplateCatalog;
import ru.tandemservice.unidip.base.entity.catalog.DipDocumentType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Печатные шаблоны и скрипты документов об обучении
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DipDocTemplateCatalogGen extends ScriptItem
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unidip.base.entity.catalog.DipDocTemplateCatalog";
    public static final String ENTITY_NAME = "dipDocTemplateCatalog";
    public static final int VERSION_HASH = -1702862102;
    private static IEntityMeta ENTITY_META;

    public static final String P_WITH_SUCCESS = "withSuccess";
    public static final String L_DIP_DOCUMENT_TYPE = "dipDocumentType";
    public static final String P_APPLICATION = "application";
    public static final String P_ACTIVE = "active";

    private boolean _withSuccess;     // С отличием
    private DipDocumentType _dipDocumentType;     // Печатный шаблон
    private boolean _application;     // Приложение
    private boolean _active = true;     // Используется

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return С отличием. Свойство не может быть null.
     */
    @NotNull
    public boolean isWithSuccess()
    {
        return _withSuccess;
    }

    /**
     * @param withSuccess С отличием. Свойство не может быть null.
     */
    public void setWithSuccess(boolean withSuccess)
    {
        dirty(_withSuccess, withSuccess);
        _withSuccess = withSuccess;
    }

    /**
     * @return Печатный шаблон.
     */
    public DipDocumentType getDipDocumentType()
    {
        return _dipDocumentType;
    }

    /**
     * @param dipDocumentType Печатный шаблон.
     */
    public void setDipDocumentType(DipDocumentType dipDocumentType)
    {
        dirty(_dipDocumentType, dipDocumentType);
        _dipDocumentType = dipDocumentType;
    }

    /**
     * @return Приложение. Свойство не может быть null.
     */
    @NotNull
    public boolean isApplication()
    {
        return _application;
    }

    /**
     * @param application Приложение. Свойство не может быть null.
     */
    public void setApplication(boolean application)
    {
        dirty(_application, application);
        _application = application;
    }

    /**
     * @return Используется. Свойство не может быть null.
     */
    @NotNull
    public boolean isActive()
    {
        return _active;
    }

    /**
     * @param active Используется. Свойство не может быть null.
     */
    public void setActive(boolean active)
    {
        dirty(_active, active);
        _active = active;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof DipDocTemplateCatalogGen)
        {
            setWithSuccess(((DipDocTemplateCatalog)another).isWithSuccess());
            setDipDocumentType(((DipDocTemplateCatalog)another).getDipDocumentType());
            setApplication(((DipDocTemplateCatalog)another).isApplication());
            setActive(((DipDocTemplateCatalog)another).isActive());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DipDocTemplateCatalogGen> extends ScriptItem.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DipDocTemplateCatalog.class;
        }

        public T newInstance()
        {
            return (T) new DipDocTemplateCatalog();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "withSuccess":
                    return obj.isWithSuccess();
                case "dipDocumentType":
                    return obj.getDipDocumentType();
                case "application":
                    return obj.isApplication();
                case "active":
                    return obj.isActive();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "withSuccess":
                    obj.setWithSuccess((Boolean) value);
                    return;
                case "dipDocumentType":
                    obj.setDipDocumentType((DipDocumentType) value);
                    return;
                case "application":
                    obj.setApplication((Boolean) value);
                    return;
                case "active":
                    obj.setActive((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "withSuccess":
                        return true;
                case "dipDocumentType":
                        return true;
                case "application":
                        return true;
                case "active":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "withSuccess":
                    return true;
                case "dipDocumentType":
                    return true;
                case "application":
                    return true;
                case "active":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "withSuccess":
                    return Boolean.class;
                case "dipDocumentType":
                    return DipDocumentType.class;
                case "application":
                    return Boolean.class;
                case "active":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DipDocTemplateCatalog> _dslPath = new Path<DipDocTemplateCatalog>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DipDocTemplateCatalog");
    }
            

    /**
     * @return С отличием. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.catalog.DipDocTemplateCatalog#isWithSuccess()
     */
    public static PropertyPath<Boolean> withSuccess()
    {
        return _dslPath.withSuccess();
    }

    /**
     * @return Печатный шаблон.
     * @see ru.tandemservice.unidip.base.entity.catalog.DipDocTemplateCatalog#getDipDocumentType()
     */
    public static DipDocumentType.Path<DipDocumentType> dipDocumentType()
    {
        return _dslPath.dipDocumentType();
    }

    /**
     * @return Приложение. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.catalog.DipDocTemplateCatalog#isApplication()
     */
    public static PropertyPath<Boolean> application()
    {
        return _dslPath.application();
    }

    /**
     * @return Используется. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.catalog.DipDocTemplateCatalog#isActive()
     */
    public static PropertyPath<Boolean> active()
    {
        return _dslPath.active();
    }

    public static class Path<E extends DipDocTemplateCatalog> extends ScriptItem.Path<E>
    {
        private PropertyPath<Boolean> _withSuccess;
        private DipDocumentType.Path<DipDocumentType> _dipDocumentType;
        private PropertyPath<Boolean> _application;
        private PropertyPath<Boolean> _active;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return С отличием. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.catalog.DipDocTemplateCatalog#isWithSuccess()
     */
        public PropertyPath<Boolean> withSuccess()
        {
            if(_withSuccess == null )
                _withSuccess = new PropertyPath<Boolean>(DipDocTemplateCatalogGen.P_WITH_SUCCESS, this);
            return _withSuccess;
        }

    /**
     * @return Печатный шаблон.
     * @see ru.tandemservice.unidip.base.entity.catalog.DipDocTemplateCatalog#getDipDocumentType()
     */
        public DipDocumentType.Path<DipDocumentType> dipDocumentType()
        {
            if(_dipDocumentType == null )
                _dipDocumentType = new DipDocumentType.Path<DipDocumentType>(L_DIP_DOCUMENT_TYPE, this);
            return _dipDocumentType;
        }

    /**
     * @return Приложение. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.catalog.DipDocTemplateCatalog#isApplication()
     */
        public PropertyPath<Boolean> application()
        {
            if(_application == null )
                _application = new PropertyPath<Boolean>(DipDocTemplateCatalogGen.P_APPLICATION, this);
            return _application;
        }

    /**
     * @return Используется. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.catalog.DipDocTemplateCatalog#isActive()
     */
        public PropertyPath<Boolean> active()
        {
            if(_active == null )
                _active = new PropertyPath<Boolean>(DipDocTemplateCatalogGen.P_ACTIVE, this);
            return _active;
        }

        public Class getEntityClass()
        {
            return DipDocTemplateCatalog.class;
        }

        public String getEntityName()
        {
            return "dipDocTemplateCatalog";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
