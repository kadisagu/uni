/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unidip.base.bo.DiplomaTemplate.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import ru.tandemservice.unidip.base.bo.DiplomaTemplate.ui.AddEdit.DiplomaTemplateAddEdit;
import ru.tandemservice.unidip.base.bo.DiplomaTemplate.ui.AddEdit.DiplomaTemplateAddEditUI;
import ru.tandemservice.unidip.base.bo.DiplomaTemplate.ui.Line.DiplomaTemplateLine;
import ru.tandemservice.unidip.base.entity.eduplan.DiplomaTemplate;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectOkso;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;

/**
 * @author iolshvang
 * @since 14.07.11 21:44
 */
@State({
               @Bind(key = UIPresenter.PUBLISHER_ID, binding = "diplomaTemplate.id")
       })
public class DiplomaTemplatePubUI extends UIPresenter
{
    private DiplomaTemplate _diplomaTemplate = new DiplomaTemplate();
    private static final String DIPLOMA_LINE_REGION = "diplomaLineRegion";

    @Override
    public void onComponentRefresh()
    {
        _diplomaTemplate = DataAccessServices.dao().getNotNull(_diplomaTemplate.getId());
        _uiActivation.asRegion(DiplomaTemplateLine.class, DIPLOMA_LINE_REGION).parameter("diplomaTemplate.id", _diplomaTemplate.getId()).activate();
    }

    public String getDeleteDiplomaTemplateAlert()
    {
        return "Удалить шаблон документа «" + _diplomaTemplate.getContent().getEducationElementTitle() + "»?";
    }

    // Getters & Setters

    public DiplomaTemplate getDiplomaTemplate()
    {
        return _diplomaTemplate;
    }

    public void setDiplomaTemplate(DiplomaTemplate diplomaTemplate)
    {
        _diplomaTemplate = diplomaTemplate;
    }

    public String getEduPlanVersionNumber()
    {
        return "№" + getDiplomaTemplate().getEduPlanVersion().getNumber();
    }

    public String getMetaTitle()
    {
        return "Шаблон «" + getDiplomaTemplate().getContent().getEducationElementTitle() + "» документа об обучении версии УП: " + getDiplomaTemplate().getEduPlanVersion().getFullTitle();
    }

    public String getLoadFieldTitle()
    {
        return getDiplomaTemplate().getContent().getLoadTitle();
    }

    // Listeners

    public void onClickEdit()
    {
        _uiActivation.asRegionDialog(DiplomaTemplateAddEdit.class)
                .parameter(DiplomaTemplateAddEditUI.DIP_TEMPLATE_ID, _diplomaTemplate.getId())
                .activate();
    }

    public void onClickDelete()
    {
        DataAccessServices.dao().delete(_diplomaTemplate);
        deactivate();
    }

}
