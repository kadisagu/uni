/* $Id$ */
package ru.tandemservice.unidip.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * @author Andrey Avetisov
 * @since 17.06.2014
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unidip_2x6x0_2to3 extends IndependentMigrationScript
{

    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.15"),
                        new ScriptDependency("org.tandemframework.shared", "1.6.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        /*Данная миграция устанавливает названия (title_p) печатным шаблонам модуля "Дипломирования",
        * если по какой-то причине данное поле не было заполнено ранее.
        * Новое название будет совпадать с названием "типа документа", к которому относится данный шаблон.
        * */

        {
            PreparedStatement selectScripItemEmptyID = tool.prepareStatement("select id from scriptitem_t where title_p is null and catalogCode_p='unidipScriptItem'");
            PreparedStatement selectDipDocumentTypeID = tool.prepareStatement("select dipDocumentType_id from dip_doc_template_catalog where id=?");
            PreparedStatement selectDipDocumentTitle = tool.prepareStatement("select title_p from dip_c_type where id=?");
            PreparedStatement inserScriptItemTitle = tool.prepareStatement("UPDATE  scriptitem_t set title_p=? where id=?");
            selectScripItemEmptyID.execute();

            ResultSet ScripItemIdRes = selectScripItemEmptyID.getResultSet();
            while (ScripItemIdRes.next())
            {
                selectDipDocumentTypeID.setLong(1, ScripItemIdRes.getLong(1));
                selectDipDocumentTypeID.execute();
                ResultSet dipDocumentTypeIdRes = selectDipDocumentTypeID.getResultSet();
                while (dipDocumentTypeIdRes.next())
                {
                    selectDipDocumentTitle.setLong(1, dipDocumentTypeIdRes.getLong(1));
                    selectDipDocumentTitle.execute();
                    ResultSet dipDocumentTitleRes = selectDipDocumentTitle.getResultSet();
                    while (dipDocumentTitleRes.next())
                    {
                        inserScriptItemTitle.setString(1, dipDocumentTitleRes.getString(1));
                        inserScriptItemTitle.setLong(2, ScripItemIdRes.getLong(1));
                        inserScriptItemTitle.execute();
                    }
                }
            }

        }


    }
}
