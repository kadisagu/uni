package ru.tandemservice.unidip.base.entity.order;

import java.util.Date;

import ru.tandemservice.unidip.base.entity.order.gen.DipStuExcludeExtractGen;

/**
 * Выписка из приказа об отчислении
 */
public class DipStuExcludeExtract extends DipStuExcludeExtractGen
{
    @Override
    public Date getBeginDate()
    {
        return getExcludeDate();  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Date getEndDate()
    {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
}