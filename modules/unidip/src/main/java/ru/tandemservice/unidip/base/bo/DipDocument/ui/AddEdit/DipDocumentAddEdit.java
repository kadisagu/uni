/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unidip.base.bo.DipDocument.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unidip.base.bo.DipDocument.ui.AddEdit.logic.EduProgramSpecializationDSHandler;
import ru.tandemservice.unidip.base.entity.catalog.DipDocumentType;
import ru.tandemservice.unidip.base.entity.eduplan.DiplomaTemplate;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author iolshvang
 * @since 14.07.11 11:42
 */
@Configuration
public class DipDocumentAddEdit extends BusinessComponentManager
{
	public static final String EDU_PLAN_VERSION_DS = "eduPlanVersionDS";
    public static final String TEMPLATE_DS = "templateDS";
    public static final String SPECIALIZATION_DS = "specializationDS";

	public static final String FILTER_STUDENT = "student";

    public static final String FILTER_EDUPLAN_VERSION = "eduPlanVersion";
    public static final String FILTER_DOCUMENT_TYPE = "documentType";
    public static final String FILTER_PROGRAM_SPECIALIZATION = "programSpecialization";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
				.addDataSource(selectDS(EDU_PLAN_VERSION_DS, eduPlanVersionDSHandler()))
				.addDataSource(selectDS(SPECIALIZATION_DS, specializationDSHandler()))
                .addDataSource(selectDS(TEMPLATE_DS, templateDSHandler()))
                .create();
    }

	@Bean
	public IReadAggregateHandler<DSInput, DSOutput> eduPlanVersionDSHandler()
	{
		return new EntityComboDataSourceHandler(getName(), EppStudent2EduPlanVersion.class)
				.titleProperty(EppStudent2EduPlanVersion.eduPlanVersion().fullTitleWithEducationsCharacteristics().s())
				.where(EppStudent2EduPlanVersion.student(), FILTER_STUDENT);
	}

	@Bean
	public IReadAggregateHandler<DSInput, DSOutput> specializationDSHandler()
	{
		return new EduProgramSpecializationDSHandler(getName())
				.titleProperty(EduProgramSpecialization.displayableTitle().s())
				.filter(EduProgramSpecialization.title())
				.pageable(false);
	}

    @Bean
    public IDefaultComboDataSourceHandler templateDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), DiplomaTemplate.class)
			.customize((alias, dql, context, filter) ->
			{
				EppEduPlanVersion epv = context.get(FILTER_EDUPLAN_VERSION);
				DipDocumentType type = context.get(FILTER_DOCUMENT_TYPE);
				EduProgramSpecialization specialization = context.get(FILTER_PROGRAM_SPECIALIZATION);

				dql.where(eq(property(alias, DiplomaTemplate.eduPlanVersion()), value(epv)));
				if (null != type)
					dql.where(eq(property(alias, DiplomaTemplate.content().type()), value(type)));
				if (null != specialization)
					dql.where(eq(property(alias, DiplomaTemplate.content().programSpecialization()), value(specialization)));
				return dql;
			});
    }
}