/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unidip.base.bo.DipDocument.logic;

import com.google.common.collect.*;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.common.IIdentifiable;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unidip.base.entity.catalog.DipDocumentType;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentIssuance;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaObject;
import ru.tandemservice.unidip.base.entity.order.DipStuExcludeExtract;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author iolshvang
 * @since 13.07.11 20:41
 */
public class DipDocumentStudentDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String STUDENT = "student";
	public static final String EDU_PLAN_VERSION = "eduPlanVersion";
    public static final String EXCLUDE_ORDER_TITLE = "excludeOrderTitle";
    public static final String IS_ISSUED = "isIssued";
    public static final String LAST_NAME = "lastName";
    public static final String FIRST_NAME = "firstName";
    public static final String REG_NUMBER = "regNumber";

    public static final String ISSUANCE_DATE = "issuanceDate";
    public static final String ISSUE_DATE_FROM = "issueDateFrom";
    public static final String ISSUE_DATE_TO = "issueDateTo";
    public static final String EDIT_DISABLED = "editDisabled";
    public static final String PRINT_DISABLED = "printDisabled";
    public static final String DOCUMENT_TYPE = "docType";

    public static final String REGISTRATION_NUMBER = "registrationNumber";
    public static final String EXCLUDE_ORDER = "excludeOrder";
    public static final String BLANK_SERIA_AND_NUMBER = "blankSeriaAndNumber";
    public static final String BLANK_SERIA = "seria";
    public static final String BLANK_NUMBER = "number";
    public static final String ORG_UNIT = "orgUnit";
    public static final String PROGRAM_SUBJECT = "programSubject";
    public static final String APPLICATION_TO_DIPLOMA = "applicationToDiploma";
    public static final String WITH_SUCCESS = "withSuccess";

	public static final String COLUMN_EDU_PLAN_VERSION = "eduPlanVersion";

    private boolean _pageable;

    public DipDocumentStudentDSHandler(String ownerId, boolean pageable)
    {
        super(ownerId);
        _pageable = pageable;
    }

    private static IDQLExpression getMinDiplomaIssuanceForDiplomaObjectExpression(String diplomaAlias, String orderingProperty, Date minNullDummyObject, Date maxNullDummyObject, OrderDirection dir)
    {
        return DQLFunctions.coalesce(
                new DQLSelectBuilder().fromEntity(DiplomaIssuance.class, "i")
                        .column(dir == OrderDirection.asc ? DQLFunctions.min(property("i", orderingProperty)) : DQLFunctions.max(property("i", orderingProperty)))
                        .where(eq(property(diplomaAlias, DiplomaObject.P_ID), property("i", DiplomaIssuance.L_DIPLOMA_OBJECT)))
                        .buildQuery(),
                valueDate(dir == OrderDirection.asc ? maxNullDummyObject : minNullDummyObject)
        );
    }

    private static IDQLExpression getMinDateDiplomaIssuanceForDiplomaObjectExpression(String diplomaAlias, String orderingProperty, String minNullDummyObject, String maxNullDummyObject, OrderDirection dir)
    {
        return DQLFunctions.coalesce(
                new DQLSelectBuilder().fromEntity(DiplomaIssuance.class, "i")
                        .column(property("i", orderingProperty))
                        .top(1)
                        .where(eq(property(diplomaAlias, DiplomaObject.P_ID), property("i", DiplomaIssuance.L_DIPLOMA_OBJECT)))
                        .order(property("i", DiplomaIssuance.issuanceDate()), OrderDirection.asc)
                        .order(property("i", orderingProperty), OrderDirection.asc)
                        .buildQuery(),
                value(dir == OrderDirection.asc ? maxNullDummyObject : minNullDummyObject)
        );
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(DiplomaObject.class, "d");
        dql.column("d");
        dql.joinPath(DQLJoinType.left, DiplomaObject.content().type().parent().fromAlias("d"), "dp");
        dql.joinPath(DQLJoinType.left, DiplomaObject.student().group().fromAlias("d"), "grp");

		applyFilters(dql, context);
		applyOrder(dql, input.getEntityOrder(), "d", "dp", "grp");

		DSOutput output = DQLSelectOutputBuilder.get(input, dql, context.getSession()).pageable(_pageable).build();
		wrapOutput(output);

        if (!_pageable)
            output.setCountRecord(Math.max(output.getTotalSize(), 5));

        return output;
    }

	private void applyFilters(DQLSelectBuilder dql, ExecutionContext context)
	{
		final Student student = context.get(STUDENT);
		final EppEduPlanVersion eduPlanVersion = context.get(EDU_PLAN_VERSION);
		final String lastName = context.get(LAST_NAME);
		final String firstName = context.get(FIRST_NAME);
		final String regNumber = context.get(REG_NUMBER);
		final Date issueDateFrom = context.get(ISSUE_DATE_FROM);
		final Date issueDateTo = context.get(ISSUE_DATE_TO);
		final DipDocumentType dipDocumentType = context.get(DOCUMENT_TYPE);
		final OrgUnit orgUnit = context.get(ORG_UNIT);
		final String seria = context.get(BLANK_SERIA);
		final String number = context.get(BLANK_NUMBER);
		final EduProgramSubject programSubject = context.get(PROGRAM_SUBJECT);
		final IIdentifiable withSuccess = context.get(WITH_SUCCESS);

		FilterUtils.applySelectFilter(dql, "d", DiplomaObject.content().type(), dipDocumentType);
		FilterUtils.applySelectFilter(dql, "d", DiplomaObject.student().educationOrgUnit().educationLevelHighSchool().orgUnit(), orgUnit);
		FilterUtils.applySelectFilter(dql, "d", DiplomaObject.content().programSubject(), programSubject);
		FilterUtils.applySelectFilter(dql, "d", DiplomaObject.studentEpv().eduPlanVersion(), eduPlanVersion);
		FilterUtils.applySelectFilter(dql, "d", DiplomaObject.student(), student);
		FilterUtils.applySelectFilter(dql, "d", DiplomaObject.content().withSuccess(), TwinComboDataSourceHandler.getSelectedValue(withSuccess));
		FilterUtils.applySimpleLikeFilter(dql, "d", DiplomaObject.student().person().identityCard().lastName(), lastName);
		FilterUtils.applySimpleLikeFilter(dql,  "d", DiplomaObject.student().person().identityCard().firstName(), firstName);

		final DQLSelectBuilder issuanceDQL = new DQLSelectBuilder().fromEntity(DiplomaIssuance.class, "i2");
		issuanceDQL.column("i2.id");
		issuanceDQL.where(eq(property("i2", DiplomaIssuance.L_DIPLOMA_OBJECT), property("d.id")));

		boolean anyIssuanceFilterNotNull;
		anyIssuanceFilterNotNull = FilterUtils.applySimpleLikeFilter(issuanceDQL, "i2", DiplomaIssuance.registrationNumber(), regNumber);
		anyIssuanceFilterNotNull |= FilterUtils.applySimpleLikeFilter(issuanceDQL, "i2", DiplomaIssuance.blankSeria(), seria);
		anyIssuanceFilterNotNull |= FilterUtils.applySimpleLikeFilter(issuanceDQL, "i2", DiplomaIssuance.blankNumber(), number);
		anyIssuanceFilterNotNull |= FilterUtils.applyBetweenFilter(issuanceDQL, "i2", DiplomaIssuance.issuanceDate().s(), issueDateFrom, issueDateTo);

		if (anyIssuanceFilterNotNull)
			dql.where(exists(issuanceDQL.buildQuery()));
	}

	private void applyOrder(DQLSelectBuilder diplomaDql, EntityOrder entityOrder, String dipAlias, String docTypeAlias, String groupAlias)
	{
		final Object orderKey = entityOrder.getKey();
		final OrderDirection direction = entityOrder.getDirection();
		switch ((String)orderKey)
		{
			case REGISTRATION_NUMBER:
				diplomaDql.order(getMinDateDiplomaIssuanceForDiplomaObjectExpression(dipAlias, DiplomaIssuance.P_REGISTRATION_NUMBER, "0", "z", direction), direction);
				return;

			case ISSUANCE_DATE:
				diplomaDql.order(getMinDiplomaIssuanceForDiplomaObjectExpression(dipAlias, DiplomaIssuance.P_ISSUANCE_DATE, CoreDateUtils.getYearFirstTimeMoment(1900), CoreDateUtils.getYearFirstTimeMoment(3000), direction), direction);
				diplomaDql.order(getMinDateDiplomaIssuanceForDiplomaObjectExpression(dipAlias, DiplomaIssuance.P_REGISTRATION_NUMBER, "0", "z", OrderDirection.asc), direction);
				return;

			case BLANK_SERIA_AND_NUMBER:
				diplomaDql.order(getMinDateDiplomaIssuanceForDiplomaObjectExpression(dipAlias, DiplomaIssuance.P_BLANK_SERIA, "0", "z", direction), direction);
				diplomaDql.order(getMinDateDiplomaIssuanceForDiplomaObjectExpression(dipAlias, DiplomaIssuance.P_BLANK_NUMBER, "0", "z", direction), direction);
				diplomaDql.order(getMinDateDiplomaIssuanceForDiplomaObjectExpression(dipAlias, DiplomaIssuance.P_REGISTRATION_NUMBER, "0", "z", direction), direction);
				return;
		}

		final ImmutableList<String> typeProperties = ImmutableList.of(DiplomaObject.content().type().titleWithParent().s(), DiplomaObject.titleWithSuccess().s());
		if (typeProperties.contains(orderKey))
		{
			diplomaDql.order(DQLFunctions.coalesce(property(docTypeAlias, DipDocumentType.P_TITLE), property(dipAlias, DiplomaObject.content().type().title())), direction);
			diplomaDql.order(getMinDateDiplomaIssuanceForDiplomaObjectExpression(dipAlias, DiplomaIssuance.P_REGISTRATION_NUMBER, "0", "z", direction), direction);
		}
		else if (orderKey.equals(DiplomaObject.student().studentFullFioWithBirthYearGroupAndStatus().s()))
		{
			DQLOrderDescriptionRegistry orderDescriptionRegistry = new DQLOrderDescriptionRegistry(DiplomaObject.class, dipAlias);
			orderDescriptionRegistry.setOrders(DiplomaObject.student().studentFullFioWithBirthYearGroupAndStatus(),
					new OrderDescription(DiplomaObject.student().person().identityCard().lastName()),
					new OrderDescription(DiplomaObject.student().person().identityCard().firstName()),
					new OrderDescription(DiplomaObject.student().person().identityCard().middleName()),
					new OrderDescription(groupAlias, Group.P_TITLE));

			orderDescriptionRegistry.applyOrder(diplomaDql, entityOrder);
		}
		else if (orderKey.equals(DiplomaObject.studentEpv().eduPlanVersion().fullTitleWithEducationsCharacteristics().s()))
		{
			applyOrderByEpv(diplomaDql, dipAlias, direction);
		}
		else if (orderKey.equals(DiplomaObject.content().programSubject().titleWithCode().s()))
		{
			applyOrderBySubject(diplomaDql, dipAlias, direction);
		}
	}

	/**
	 * Отсортировать дипломы по нулабельной колонке УПв. При прямой сортировке вначале идут дипломы, в которых УПв null, затем - отсортированные по строке вида "[Номер УП].[Номер версии УП]".
	 * @param diplomaDql Запрос к {@link DiplomaObject}, к которому применяется сортировка.
	 * @param dipAlias Алиас диплома.
	 * @param direction Направление сортировки.
	 */
	private void applyOrderByEpv(DQLSelectBuilder diplomaDql, String dipAlias, OrderDirection direction)
	{
		final String studentEpvAlias = "studentEpv";
		final IDQLExpression epvNumberExpr = DQLFunctions.concat(
				property(studentEpvAlias, EppStudent2EduPlanVersion.eduPlanVersion().eduPlan().number()),
				value("."),
				property(studentEpvAlias, EppStudent2EduPlanVersion.eduPlanVersion().number()));

		DQLSelectBuilder epvDql = new DQLSelectBuilder().fromEntity(EppStudent2EduPlanVersion.class, studentEpvAlias)
				.where(eq(property(dipAlias, DiplomaObject.studentEpv()), property(studentEpvAlias)))
				.column(epvNumberExpr);
		IDQLExpression epvOrderExpr = DQLFunctions.coalesce(epvDql.buildQuery(), value(""));
		diplomaDql.order(epvOrderExpr, direction);
	}

	/**
	 * Отсортировать дипломы по нулабельной колонке Направления подготовки. При прямой сортировке вначале идут дипломы, в которых Направление null, затем - отсортированные по Полному коду направления.
	 * @param diplomaDql Запрос к {@link DiplomaObject}, к которому применяется сортировка.
	 * @param dipAlias Алиас диплома.
	 * @param direction Направление сортировки.
	 */
	private void applyOrderBySubject(DQLSelectBuilder diplomaDql, String dipAlias, OrderDirection direction)
	{
		final String subjectAlias = "subject";
		DQLSelectBuilder subjectDql = new DQLSelectBuilder().fromEntity(EduProgramSubject.class, subjectAlias)
				.where(eq(property(dipAlias, DiplomaObject.content().programSubject()), property(subjectAlias)))
				.column(property(subjectAlias, EduProgramSubject.subjectCode()));
		IDQLExpression subjectOrderExpr = DQLFunctions.coalesce(subjectDql.buildQuery(), value(""));
		diplomaDql.order(subjectOrderExpr, direction);
	}

	private void wrapOutput(DSOutput output)
	{
		ICommonDAO dao = DataAccessServices.dao();

		List<DipStuExcludeExtract> extracts = dao.getList(DipStuExcludeExtract.class, DipStuExcludeExtract.diploma().id(), output.getRecordIds());
		ImmutableMap<Long, DipStuExcludeExtract> diploma2ExcludeExtract = Maps.uniqueIndex(extracts, (DipStuExcludeExtract extract) -> extract.getDiploma().getId());

		List<DiplomaIssuance> issuances = dao.getList(DiplomaIssuance.class, DiplomaIssuance.diplomaObject().id(), output.getRecordIds(),DiplomaIssuance.P_ISSUANCE_DATE, DiplomaIssuance.P_REGISTRATION_NUMBER);
		ImmutableMultimap<Long, DiplomaIssuance> diploma2Issuances = Multimaps.index(issuances, (DiplomaIssuance issuance) -> issuance.getDiplomaObject().getId());

		List<DiplomaContentIssuance> contentIssuances = dao.getList(DiplomaContentIssuance.class, DiplomaContentIssuance.diplomaIssuance().diplomaObject().id(), output.getRecordIds());
		ImmutableMultimap<Long, DiplomaContentIssuance> diploma2ContentIssuances = Multimaps.index(contentIssuances, (DiplomaContentIssuance issuance) -> issuance.getDiplomaIssuance().getDiplomaObject().getId());

		for (DataWrapper wrapper : DataWrapper.wrap(output))
		{
			DipStuExcludeExtract extract = diploma2ExcludeExtract.get(wrapper.getId());
			wrapper.setProperty(EXCLUDE_ORDER, extract);
			wrapper.setProperty(EDIT_DISABLED, extract != null);

			wrapper.setProperty(EXCLUDE_ORDER_TITLE, (extract == null) ? "" : extract.getTitle());
			if (null != extract && extract.isCommitted())
				wrapper.setProperty(EXCLUDE_ORDER_TITLE, (extract.getParagraph() == null) ? "" : ("№" + extract.getParagraph().getOrder().getNumber() + " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getParagraph().getOrder().getCommitDate())));
			else
				wrapper.setProperty(EXCLUDE_ORDER_TITLE, (extract == null || extract.getParagraph() == null) ? "" : (DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getParagraph().getOrder().getCreateDate())));

			Collection<DiplomaIssuance> issuanceList = diploma2Issuances.get(wrapper.getId());

			if (!issuanceList.isEmpty())
			{
				String issuanceDataStr = issuanceList.stream().map(issuance -> DateFormatter.DEFAULT_DATE_FORMATTER.format(issuance.getIssuanceDate())).collect(Collectors.joining(" \n "));
				String regNumberStr = issuanceList.stream().map(DiplomaIssuance::getRegistrationNumber).collect(Collectors.joining(" \n "));
				String seriaAndNumberStr = issuanceList.stream()
						.map(issuance -> StringUtils.trimToEmpty(issuance.getBlankSeria()) + " " + StringUtils.trimToEmpty(issuance.getBlankNumber()))
						.collect(Collectors.joining(" \n "));

				wrapper.setProperty(PRINT_DISABLED, false);
				wrapper.setProperty(ISSUANCE_DATE, issuanceDataStr);
				wrapper.setProperty(REGISTRATION_NUMBER, regNumberStr);
				wrapper.setProperty(BLANK_SERIA_AND_NUMBER, seriaAndNumberStr);
			}
			else
			{
				wrapper.setProperty(PRINT_DISABLED, true);
			}

			Collection<DiplomaContentIssuance> contentIssuancesList = diploma2ContentIssuances.get(wrapper.getId());
			if (!contentIssuancesList.isEmpty())
			{
				StringBuilder contentIssuanceBuilder = new StringBuilder();
				for (DiplomaContentIssuance contentIssuance : contentIssuancesList)
				{
					contentIssuanceBuilder.append(contentIssuanceBuilder.length() > 0 ? "\n" : "")
							.append("№").append(contentIssuance.getContentListNumber()).append(" ").append(contentIssuance.getBlankSeria()).append(" ").append(contentIssuance.getBlankNumber()).append(" ")
							.append(!contentIssuance.isDuplicate() ? "" : "Дубликат")
							.append(null == contentIssuance.getDuplicateRegustrationNumber() ? "" : " " + contentIssuance.getDuplicateRegustrationNumber())
							.append(null == contentIssuance.getDuplicateIssuanceDate() ? " " : " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(contentIssuance.getDuplicateIssuanceDate()));
				}

				wrapper.setProperty(APPLICATION_TO_DIPLOMA, contentIssuanceBuilder.toString());
			}
		}
	}
}