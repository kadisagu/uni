/* $Id$ */
package ru.tandemservice.unidip.base.bo.DipDocument.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.unidip.base.entity.catalog.DipDocTemplateCatalog;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaObject;

/**
 * @author iolshvang
 * @since 29.08.11 16:32
 */
public interface IDiplomaPrintDao extends INeedPersistenceSupport
{
    RtfDocument printDocument(DiplomaObject document);
}
