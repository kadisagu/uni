/* $Id$ */
package ru.tandemservice.unidip.component.listextract.excludeStudents.ParagraphAddEdit;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.IAbstractListParagraphAddEditDAO;
import ru.tandemservice.unidip.base.entity.order.DipStuExcludeExtract;

/**
 * @author Dmitry Seleznev
 * @since 08.06.2011
 */
public interface IDAO extends IAbstractListParagraphAddEditDAO<DipStuExcludeExtract, Model>
{
    void prepareColumns(Model model);
}