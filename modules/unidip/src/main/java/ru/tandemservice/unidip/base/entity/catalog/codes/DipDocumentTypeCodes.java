package ru.tandemservice.unidip.base.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Тип документов об обучении"
 * Имя сущности : dipDocumentType
 * Файл data.xml : unidip-catalogs.data.xml
 */
public interface DipDocumentTypeCodes
{
    /** Константа кода (code) элемента : Иной документ (title) */
    String OTHER = "other";
    /** Константа кода (code) элемента : Справка об обучении (title) */
    String EDUCATION_REFERENCE = "eduRef";
    /** Константа кода (code) элемента : Диплом о среднем профессиональном образовании (title) */
    String COLLEGE_DIPLOMA = "collegeDip";
    /** Константа кода (code) элемента : Диплом бакалавра (title) */
    String BACHELOR_DIPLOMA = "bachelorDip";
    /** Константа кода (code) элемента : Диплом специалиста (title) */
    String SPECIALIST_DIPLOMA = "specialDip";
    /** Константа кода (code) элемента : Диплом магистра (title) */
    String MAGISTR_DIPLOMA = "magistrDip";
    /** Константа кода (code) элемента : Диплом об окончании аспирантуры (title) */
    String POSTGRADUATE_DIPLOMA = "postGradDip";
    /** Константа кода (code) элемента : Диплом об окончании адъюнктуры (title) */
    String ADUNCTURE_DIPLOMA_ = "adunctDip";
    /** Константа кода (code) элемента : Удостоверение о повышении квалификации (title) */
    String UPPER_PROFESSIONAL_CERTIFICATE = "upProfCert";
    /** Константа кода (code) элемента : Диплом о профессиональной переподготовке (title) */
    String PROFESSION_RETRAIN_DIPLOMA_ = "profRetrainDip";

    Set<String> CODES = ImmutableSet.of(OTHER, EDUCATION_REFERENCE, COLLEGE_DIPLOMA, BACHELOR_DIPLOMA, SPECIALIST_DIPLOMA, MAGISTR_DIPLOMA, POSTGRADUATE_DIPLOMA, ADUNCTURE_DIPLOMA_, UPPER_PROFESSIONAL_CERTIFICATE, PROFESSION_RETRAIN_DIPLOMA_);
}
