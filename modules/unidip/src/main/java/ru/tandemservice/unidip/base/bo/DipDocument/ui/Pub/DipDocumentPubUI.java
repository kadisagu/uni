/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unidip.base.bo.DipDocument.ui.Pub;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.uni.component.student.StudentPub.Model;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unidip.base.bo.DipDocument.DipDocumentManager;
import ru.tandemservice.unidip.base.bo.DipDocument.ui.AddEdit.DipDocumentAddEdit;
import ru.tandemservice.unidip.base.bo.DipDocument.ui.AddEdit.DipDocumentAddEditUI;
import ru.tandemservice.unidip.base.bo.DipDocument.ui.AddInformationAddEdit.DipDocumentAddInformationAddEdit;
import ru.tandemservice.unidip.base.bo.DipDocument.ui.AddInformationAddEdit.DipDocumentAddInformationAddEditUI;
import ru.tandemservice.unidip.base.bo.DipDocument.ui.Line.DipDocumentLine;
import ru.tandemservice.unidip.base.bo.DipDocument.ui.Print.DipDocumentPrint;
import ru.tandemservice.unidip.base.bo.DipDocument.ui.Print.DipDocumentPrintUI;
import ru.tandemservice.unidip.base.bo.DipDocument.ui.TemplateSelect.DipDocumentTemplateSelect;
import ru.tandemservice.unidip.base.bo.DipDocument.ui.TemplateSelect.DipDocumentTemplateSelectUI;
import ru.tandemservice.unidip.base.bo.DiplomaIssuance.ui.AddEdit.DiplomaIssuanceAddEdit;
import ru.tandemservice.unidip.base.bo.DiplomaIssuance.ui.AddEdit.DiplomaIssuanceAddEditUI;
import ru.tandemservice.unidip.base.entity.catalog.DipDocumentType;
import ru.tandemservice.unidip.base.entity.diploma.*;
import ru.tandemservice.unidip.base.entity.order.DipContentIssuanceToDipExtractRelation;
import ru.tandemservice.unidip.base.entity.order.DipStuExcludeExtract;
import ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.DipDiplomaBlankManager;
import ru.tandemservice.unidip.bso.entity.catalog.DipBlankState;
import ru.tandemservice.unidip.bso.entity.catalog.codes.DipBlankStateCodes;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;

import java.util.*;

/**
 * @author iolshvang
 * @since 14.07.11 21:44
 */
@State({
               @Bind(key = UIPresenter.PUBLISHER_ID, binding = "dipDocument.id")
       })
public class DipDocumentPubUI extends UIPresenter
{
    private DiplomaObject _dipDocument = new DiplomaObject();
    private static final String DIPLOMA_LINE_REGION = "diplomaLineRegion";
    private boolean _hasExcludeExtract;
    private DiplomaIssuance _diplomaIssuance;
    private List<DiplomaIssuance> _diplomaIssuanceList;


    @Override
    public void onComponentRefresh()
    {
        _dipDocument = DataAccessServices.dao().getNotNull(_dipDocument.getId());
        _uiActivation.asRegion(DipDocumentLine.class, DIPLOMA_LINE_REGION).parameter("dipDocument.id", _dipDocument.getId()).activate();
        _hasExcludeExtract = IUniBaseDao.instance.get().existsEntity(DipStuExcludeExtract.class, DipStuExcludeExtract.L_DIPLOMA, _dipDocument);
        updateIssuanceList();
    }

    public String getDiplomaAcademyRenameTitle()
    {
        String diplomaAcademyRenameTitle = "";
        if (null != getDipDocument())
        {
            List<DiplomaAcademyRenameData> academyRenameDataList = DataAccessServices.dao().getList(DiplomaAcademyRenameData.class, DiplomaAcademyRenameData.diplomaContent(), getDipDocument().getContent(), DiplomaAcademyRenameData.academyRename().date().s());
            for (DiplomaAcademyRenameData renameData : academyRenameDataList)
            {
                diplomaAcademyRenameTitle += "<div>" + renameData.getAcademyRename().getPreviousFullTitle() + ", " + DateFormatter.DATE_FORMATTER_JUST_YEAR.format(renameData.getAcademyRename().getDate()) + "</div>";
            }
        }

        return diplomaAcademyRenameTitle;
    }

    public String getTitle()
    {
		if (_dipDocument == null)
			return "";

		final DiplomaContent content = _dipDocument.getContent();
		return _dipDocument.getTitleWithSuccess() +
                ": " +
				_dipDocument.getStudent().getFio() +
                " " +
                (null != content.getProgramSubject() ? content.getProgramSubject().getTitleWithCode() : "") +
                (null != content.getProgramSpecialization() ? " (" + content.getProgramSpecialization().getTitle() + ")" : "");
    }

    private DataWrapper _currentDiplomaContentIssuance;
    public DataWrapper getCurrentDiplomaContentIssuance() { return _currentDiplomaContentIssuance; }
    public void setCurrentDiplomaContentIssuance(DataWrapper currentDiplomaContentIssuance) { _currentDiplomaContentIssuance = currentDiplomaContentIssuance; }

    public Collection<DataWrapper> getDiplomaContentIssuanceList()
    {
        Collection<DiplomaContentIssuance> contentIssuanceList = IUniBaseDao.instance.get().getList(DiplomaContentIssuance.class, DiplomaContentIssuance.diplomaIssuance(), getDiplomaIssuance(), DiplomaContentIssuance.P_CONTENT_LIST_NUMBER);
        Map<DiplomaContentIssuance, DipContentIssuanceToDipExtractRelation> relMap = new HashMap<>();
        for (DipContentIssuanceToDipExtractRelation rel: IUniBaseDao.instance.get().getList(DipContentIssuanceToDipExtractRelation.class, DipContentIssuanceToDipExtractRelation.contentIssuance(), contentIssuanceList))
            relMap.put(rel.getContentIssuance(), rel);

        DateFormatter f = DateFormatter.DEFAULT_DATE_FORMATTER;
        Collection<DataWrapper> result = new ArrayList<>();
        for (DiplomaContentIssuance contentIssuance: contentIssuanceList)
        {
            DataWrapper wrapper = new DataWrapper(contentIssuance);
            String appendixTitle = "Приложение №" + contentIssuance.getContentListNumber() + " ";
            String appendixExtract = "";
            if (contentIssuance.isDuplicate())
                appendixTitle += " Дубликат " + contentIssuance.getDuplicateRegustrationNumber();
            if (contentIssuance.getDuplicateIssuanceDate() != null)
                appendixTitle += " " + f.format(contentIssuance.getDuplicateIssuanceDate());

            String blank = contentIssuance.getBlankSeria() + " " + contentIssuance.getBlankNumber();
            DipContentIssuanceToDipExtractRelation rel = relMap.get(contentIssuance);

            if (rel != null)
            {
                DipStuExcludeExtract extract = rel.getExtract();
                wrapper.setProperty("extractId", extract.getId());
                appendixExtract = extract.isCommitted() ? ", приказ " : ", приказ от ";

                AbstractStudentOrder order = extract.getParagraph().getOrder();
                wrapper.setProperty("appendixLinkPostfix", extract.isCommitted() ? "№" + order.getNumber() + " от " + f.format(order.getCommitDate()) : f.format(order.getCreateDate()));
            }
            if (null != contentIssuance.getBlank())
            {
                wrapper.setProperty("blankId", contentIssuance.getBlank().getId());
            }

            wrapper.setProperty("appendixTitle", appendixTitle);
            wrapper.setProperty("appendixBlank", blank);
            wrapper.setProperty("appendixExtract", appendixExtract);
            result.add(wrapper);
        }

        return result;
    }

    public String getProtocol()
    {
        return (null != getDipDocument().getStateCommissionProtocolNumber() ? "№" + getDipDocument().getStateCommissionProtocolNumber() : "") +
                (null != getDipDocument().getStateCommissionDate() ? " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDipDocument().getStateCommissionDate()) : "");
    }

    public Long getExtractId()
    {
        return getCurrentExcludeExtract() == null ? null : getCurrentExcludeExtract()./*getParagraph().getOrder().*/getId();
    }

    public String getOrderTitle()
    {
        DipStuExcludeExtract excludeExtract = getCurrentExcludeExtract();
        if (excludeExtract == null || excludeExtract.getParagraph() == null)
            return "";

        if (excludeExtract.isCommitted())
            return "№" + excludeExtract.getParagraph().getOrder().getNumber() + " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(excludeExtract.getParagraph().getOrder().getCommitDate());

        return DateFormatter.DEFAULT_DATE_FORMATTER.format(excludeExtract.getParagraph().getOrder().getCreateDate());
    }

    private Map<DiplomaIssuance, DipStuExcludeExtract> _excludeExtractMap = new HashMap<>();
    private DipStuExcludeExtract getCurrentExcludeExtract()
    {
        return _excludeExtractMap.get(getDiplomaIssuance());
    }

    public String getDeleteDipDocumentAlert()
    {
        return "Удалить " + _dipDocument.getTitleWithSuccess()
                + (null != _dipDocument.getContent().getProgramSubject() ? " " + _dipDocument.getContent().getProgramSubject().getTitleWithCode() : "")
                + (null != _dipDocument.getContent().getProgramSpecialization() ? " (" + _dipDocument.getContent().getProgramSpecialization().getTitle() + ")" : "") + "?";
    }

    public String getDeleteIssuanceAlert()
    {
        return "Удалить регистрационные данные о выдаче документа от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(_diplomaIssuance.getIssuanceDate()) + "?";
    }

    public String getBlankSeriaAndNumber()
    {
        return StringUtils.trimToEmpty(_diplomaIssuance.getBlankSeria()) + " " + StringUtils.trimToEmpty(_diplomaIssuance.getBlankNumber());
    }

    public boolean isShowPrintScanCopy()
    {
        return getDiplomaIssuance().getScanCopy()!=null;
    }

    public boolean isShowAddEditAdditionalInformation()
    {
		final EduProgramSubject programSubject = _dipDocument.getContent().getProgramSubject();
		return (programSubject != null) && programSubject.getSubjectIndex().getProgramKind().isProgramProf();
    }

    public boolean isShowFillByTemplate()
    {
        return DipDocumentType.ONLY_DIPLOMA_CODES.contains(getDipDocument().getContent().getType().getCode());
    }

	public boolean isFillByTemplateDisabled()
	{
		return getDipDocument().getStudentEpv() == null;
	}

    public boolean isRecalculateLaborVisible()
    {
        return ! getDipDocument().getContent().isLoadInWeeks();
    }

    // Getters & Setters

    public DiplomaIssuance getDiplomaIssuance()
    {
        return _diplomaIssuance;
    }

    public void setDiplomaIssuance(DiplomaIssuance diplomaIssuance)
    {
        _diplomaIssuance = diplomaIssuance;
    }

    public List<DiplomaIssuance> getDiplomaIssuanceList()
    {
        return _diplomaIssuanceList;
    }

    public boolean isOrderFinished()
    {
        return _hasExcludeExtract;
    }

    public DiplomaObject getDipDocument()
    {
        return _dipDocument;
    }

    public void setDipDocument(DiplomaObject dipDocument)
    {
        _dipDocument = dipDocument;
    }

    public String getEduPlanVersionNumber()
    {
		if (getDipDocument().getStudentEpv() == null)
			return "";
        return getDipDocument().getStudentEpv().getEduPlanVersion().getTitle();
    }

    public String getLoadFieldTitle() {
        return getDipDocument().getContent().getLoadTitle();
    }

    // Listeners

    public void onClickEdit()
    {
        _uiActivation.asRegionDialog(DipDocumentAddEdit.class)
                .parameter(DipDocumentAddEditUI.DIP_DOCUMENT_ID, _dipDocument.getId())
                .activate();
    }

    public void onClickAddRegIssuanceData()
    {
        _uiActivation.asRegionDialog(DiplomaIssuanceAddEdit.class)
                .parameter(DiplomaIssuanceAddEditUI.DIP_DOCUMENT_ID, _dipDocument.getId())
                .activate();
    }

    public void onClickDelete()
    {
        DipDocumentManager.instance().dao().deleteDipDocument(_dipDocument.getId());
        deactivate();
    }

    public void onClickFillByTemplate()
    {
        boolean hasDiplomaRows = ISharedBaseDao.instance.get().existsEntity(DiplomaContentRow.class, DiplomaContentRow.owner().s(), _dipDocument.getContent());

        if (hasDiplomaRows)
            throw new ApplicationException("Заполнение по шаблону не возможно, т.к. документ содержит строки.");

        _uiActivation.asRegionDialog(DipDocumentTemplateSelect.class).parameter(DipDocumentTemplateSelectUI.PARAM_DOCUMENT_IDS, Lists.newArrayList(_dipDocument.getId())).activate();
    }

    public void onUpdateMark()
    {
        DipDocumentManager.instance().dao().saveLastMarkInDiplomaContentRow(getDipDocument().getContent());
    }

    public void onClickRecalculateLabor()
    {
		DipDocumentManager.instance().dao().recalculateDiplomaLabor(getDipDocument().getContent());
    }

    private void updateIssuanceList()
    {
        _diplomaIssuanceList = DataAccessServices.dao().getList(DiplomaIssuance.class, DiplomaIssuance.diplomaObject(), _dipDocument, DiplomaIssuance.P_ISSUANCE_DATE);
        for (DipStuExcludeExtract e: IUniBaseDao.instance.get().getList(DipStuExcludeExtract.class, DipStuExcludeExtract.issuance(), _diplomaIssuanceList))
            _excludeExtractMap.put(e.getIssuance(), e);
    }

    public void onClickDeleteIssuance()
    {
        Long issuanceId = getListenerParameterAsLong();
        final DiplomaIssuance issuance = DataAccessServices.dao().get(DiplomaIssuance.class, issuanceId);
        if (null != issuance.getBlank())
		{
            issuance.getBlank().setBlankState(DataAccessServices.dao().getByCode(DipBlankState.class, DipBlankStateCodes.FREE));
			DipDiplomaBlankManager.instance().diplomaBlankDao().doCancelBlankAppendixesLink(issuance);
		}
        DataAccessServices.dao().delete(issuanceId);
        updateIssuanceList();
    }

    public void onClickPrintIssuance()
    {
        Long issuanceId = getListenerParameterAsLong();
        _uiActivation.asRegionDialog(DipDocumentPrint.class)
                .parameter(DipDocumentPrintUI.DIP_DOCUMENT_ID, _dipDocument.getId())
                .parameter(DipDocumentPrintUI.DIPLOMA_ISSUANCE_ID, issuanceId)
                .parameter(DipDocumentPrintUI.DIPLOMA_ISSUANCE_DISABLED, true)
                .activate();
    }

    public void onClickEditIssuance()
    {
        Long issuanceId = getListenerParameterAsLong();
        _uiActivation.asRegionDialog(DiplomaIssuanceAddEdit.class)
                .parameter(DiplomaIssuanceAddEditUI.DIP_DOCUMENT_ID, _dipDocument.getId())
                .parameter(DiplomaIssuanceAddEditUI.DIP_ISSUANCE_ID, issuanceId)
                .activate();
    }

    public void onClickPrintScanCopy()
    {
        Long issuanceId = getListenerParameterAsLong();
        DiplomaIssuance issuance = DataAccessServices.dao().get(DiplomaIssuance.class, issuanceId);
        if (issuance.getScanCopy().getContent() == null)
            throw new ApplicationException("Файл скан-копии пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().document(issuance.getScanCopy()), false);
    }

    public void onClickAddEditAdditionalInformation()
    {

        _uiActivation.asRegionDialog(DipDocumentAddInformationAddEdit.class)
                .parameter(DipDocumentAddInformationAddEditUI.DIP_DOCUMENT_ID, _dipDocument.getId())
                .activate();
    }

    public ParametersMap getStudentParameters()
    {
        return new ParametersMap().add(PUBLISHER_ID, getDipDocument().getStudent().getId()).add(Model.SELECTED_TAB_BIND_KEY, "studentTab");
    }
}
