/* $Id:$ */
package ru.tandemservice.unidip.bso.bo.DipDiplomaBlank;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.logic.DipDiplomaBlankDao;
import ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.logic.IDipDiplomaBlankDao;
import ru.tandemservice.unidip.bso.entity.catalog.DipBlankType;

/**
 * @author rsizonenko
 * @since 24.12.2014
 */
@Configuration
public class DipDiplomaBlankManager extends BusinessObjectManager
{
    // binding keys
    public static final String KEY_BLANKS = "blanks";
    public static final String KEY_MASS_ACTION = "massAction";

    public static DipDiplomaBlankManager instance()
    {
        return DipDiplomaBlankManager.instance(DipDiplomaBlankManager.class);
    }

    public static final String BLANK_TYPE_DS = "blankTypeDS";
    public static final String STORAGE_LOCATION_DS = "storageLocationDS";


    @Bean
    public IDefaultComboDataSourceHandler blankTypeDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), DipBlankType.class)
                .order(DipBlankType.title())
                .filter(DipBlankType.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler storageLocationDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), OrgUnit.class)
                .order(OrgUnit.fullTitle())
                .filter(OrgUnit.fullTitle())
                .pageable(true);
    }

    @Bean
    public UIDataSourceConfig blankTypeDataSourceConfig()
    {
        return SelectDSConfig.with(BLANK_TYPE_DS, this.getName()).dataSourceClass(SelectDataSource.class).handler(this.blankTypeDSHandler()).create();
    }

    @Bean
    public UIDataSourceConfig storageLocationDataSourceConfig()
    {
        return SelectDSConfig.with(STORAGE_LOCATION_DS, this.getName())
            .dataSourceClass(SelectDataSource.class)
            .handler(this.storageLocationDSHandler())
            .addColumn(OrgUnit.fullTitle().s())
            .create();
    }

    @Bean
    public IDipDiplomaBlankDao diplomaBlankDao()
    {
        return new DipDiplomaBlankDao();
    }

}
