/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unidip.print.bo.Default.logic;

import org.apache.commons.io.IOUtils;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unidip.base.bo.DipDocument.logic.DipDocumentPrintDao;
import ru.tandemservice.unidip.base.entity.diploma.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author iolshvang
 * @since 29.08.11 16:33
 */
public class DefaultDao extends DipDocumentPrintDao
{
    @Override
    public RtfDocument printDocument(DiplomaObject document)
    {
        InputStream in = UniBaseUtils.class.getClassLoader().getResourceAsStream("unidip/templates/default.rtf");
        byte[] templateContent = null;
        try
        {
            templateContent = IOUtils.toByteArray(in);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        RtfDocument rtfDocument = new RtfReader().read(templateContent).getClone();
        RtfInjectModifier modifier = new RtfInjectModifier();
        modifier.put("documentType", document.getContent().getType().getTitle());
        modifier.put("documentTitle", document.getContent().getEducationElementTitle());
        modifier.put("fio", document.getStudent().getPerson().getFio());
        modifier.modify(rtfDocument);

        RtfTableModifier discTableModifier = new RtfTableModifier();
        List<String[]> discDataLines = new ArrayList<String[]>();
        for (DiplomaDisciplineRow row : getList(DiplomaDisciplineRow.class, DiplomaDisciplineRow.owner(), document.getContent()))
                discDataLines.add(new String[]{row.getTitle(), null == row.getLoadAsDouble() ? "" : String.valueOf(row.getLoadAsDouble()), row.getMark()});
        discTableModifier.put("discT", discDataLines.toArray(new String[][]{}));
        discTableModifier.modify(rtfDocument);

        RtfTableModifier practTableModifier = new RtfTableModifier();
        List<String[]> practDataLines = new ArrayList<String[]>();
        for (DiplomaPracticeRow row : getList(DiplomaPracticeRow.class, DiplomaPracticeRow.owner(), document.getContent()))
                practDataLines.add(new String[] {row.getTitle(), null == row.getLoadAsDouble() ? "" : String.valueOf(row.getLoadAsDouble()), row.getMark()});
        practTableModifier.put("practT", practDataLines.toArray(new String[][]{}));
        practTableModifier.modify(rtfDocument);

        RtfTableModifier courseWTableModifier = new RtfTableModifier();
        List<String[]> courseWDataLines = new ArrayList<String[]>();
        for (DiplomaCourseWorkRow row : getList(DiplomaCourseWorkRow.class, DiplomaCourseWorkRow.owner(), document.getContent()))
                courseWDataLines.add(new String[] {row.getTitle(), null == row.getLoadAsDouble() ? "" : String.valueOf(row.getLoadAsDouble()), row.getMark()});
        courseWTableModifier.put("courseWT", courseWDataLines.toArray(new String[][]{}));
        courseWTableModifier.modify(rtfDocument);

        RtfTableModifier qualifWTableModifier = new RtfTableModifier();
        List<String[]> qualifWDataLines = new ArrayList<String[]>();
        for (DiplomaQualifWorkRow row : getList(DiplomaQualifWorkRow.class, DiplomaQualifWorkRow.owner(), document.getContent()))
                qualifWDataLines.add(new String[] {row.getTitle(), null == row.getLoadAsDouble() ? "" : String.valueOf(row.getLoadAsDouble()), row.getMark()});
        qualifWTableModifier.put("qualifWT", qualifWDataLines.toArray(new String[][]{}));
        qualifWTableModifier.modify(rtfDocument);

        return rtfDocument;
    }
}
