/* $Id$ */
package ru.tandemservice.unidip.dao.mdbio;

import com.google.common.collect.Iterables;
import com.healthmarketscience.jackcess.*;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.apache.commons.lang.mutable.MutableInt;
import org.hibernate.Session;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.catalog.entity.Sex;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.mdbio.BaseIODao;
import ru.tandemservice.uni.dao.mdbio.ICatalogIODao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unidip.base.bo.DipDocument.logic.DipDocumentUtils;
import ru.tandemservice.unidip.base.entity.catalog.DipDocumentType;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContent;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentIssuance;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaObject;
import ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.DipDiplomaBlankManager;
import ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank;
import ru.tandemservice.unidip.bso.entity.catalog.codes.DipBlankStateCodes;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.entity.catalog.EppGeneration;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanHigherProf;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;

import java.io.IOException;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


/**
 * @author Andrey Avetisov
 * @since 18.03.2015
 */
public class DipDocumentIODao extends BaseIODao implements IDipDocumentIODao
{
    public final static String IMPORT_DIPLOMAS_LOG_FILE_NAME = "importDiplomas";

    private static final String PERSON_TABLE = "person_t";
    private static final String PERSON_ID_COLUMN = "id";
    private static final String PERSON_FIRST_NAME_COLUMN = "firstName";
    private static final String PERSON_LAST_NAME_COLUMN = "lastName";
    private static final String PERSON_MIDDLE_NAME_COLUMN = "middleName";
    private static final String PERSON_BIRTHDAY_COLUMN = "birthDate";
    private static final String PERSON_SEX_COLUMN = "sex_p";

    private static final String STUDENT_TABLE = "student_t";
    private static final String STUDENT_ID_COLUMN = "id";
    private static final String STUDENT_PERSON_ID_COLUMN = "person_id";
    private static final String STUDENT_PERSONAL_NUMBER_COLUMN = "student_personal_number ";
    private static final String STUDENT_COURSE_COLUMN = "student_course";
    private static final String STUDENT_GROUP_COLUMN = "student_group";
    private static final String STUDENT_FINISH_YEAR_COLUMN = "finishYear_p";
    private static final String STUDENT_ARCHIVAL_COLUMN = "archival_p";
    private static final String STUDENT_PROGRAM_SUBJECT_COLUMN = "programSubject";
    private static final String STUDENT_PROGRAM_SPECIALIZATION_COLUMN = "programSpecialization";

    private static final String STUDENT_EDU_PLAN_VERSION_TABLE = "epp_student_eduplanversion_t";
    private static final String STUDENT_EDU_PLAN_VERSION_ID_COLUMN = "id";
    private static final String STUDENT_EDU_PLAN_VERSION_STUDENT_ID_COLUMN = "student_id";
    private static final String STUDENT_EDU_PLAN_VERSION_NUMBER_COLUMN = "eduPlanVersion_number";
    private static final String STUDENT_EDU_PLAN_VERSION_EDU_PLAN_NUMBER_COLUMN = "eduPlan_number";
    private static final String STUDENT_EDU_PLAN_VERSION_PROGRAM_SUBJECT_COLUMN = "programSubject";
    private static final String STUDENT_EDU_PLAN_VERSION_PROGRAM_SPECIALIZATION_COLUMN = "programSpecialization";
    private static final String STUDENT_EDU_PLAN_VERSION_REMOVAL_DATE_COLUMN = "removalDate";

    private static final String DIP_OBJECT_TABLE = "dip_object_t";
    private static final String DIP_OBJECT_ID_COLUMN = "id";
    private static final String DIP_OBJECT_TYPE_COLUMN = "type";
    private static final String DIP_OBJECT_WITH_SUCCESS_COLUMN = "withSuccess";
    private static final String DIP_OBJECT_STUDENT_EDUPLAN_VERSION_COLUMN = "epp_student_eduplanversion_id";
    private static final String DIP_OBJECT_PROGRAM_SPECIALIZATION_COLUMN = "programSpecialization";
    private static final String DIP_OBJECT_LABOR_COLUMN = "laborAsLong";
    private static final String DIP_OBJECT_STATE_COMMISSION_CHAIR_FIO_COLUMN = "stateCommissionChairFio";
    private static final String DIP_OBJECT_STATE_COMMISSION_DATE_COLUMN = "stateCommissionDate";
    private static final String DIP_OBJECT_STATE_COMMISSION_PROTOCOL_NUMBER_COLUMN = "stateCommissionProtocolNumber";

    private static final String DIP_ISSUANCE_TABLE = "dip_issuance_t";
    private static final String DIP_ISSUANCE_ID_COLUMN = "id";
    private static final String DIP_ISSUANCE_OBJECT_ID_COLUMN = "object_id";
    private static final String DIP_ISSUANCE_BLANK_SERIA_COLUMN = "blankSeria";
    private static final String DIP_ISSUANCE_BLANK_NUMBER_COLUMN = "blankNumber";
    private static final String DIP_ISSUANCE_DATE_COLUMN = "issuanceDate";
    private static final String DIP_ISSUANCE_REG_NUMBER_COLUMN = "registrationNumber";
    private static final String DIP_ISSUANCE_REPLACED_ISSUANCE_COLUMN = "replacedIssuance";

    private static final String DIP_CONTENT_ISSUANCE_TABLE = "dip_content_issuance_t";
    private static final String DIP_CONTENT_ISSUANCE_ID_COLUMN = "id";
    private static final String DIP_CONTENT_ISSUANCE_ISSUANCE_ID_COLUMN = "issuance_id";
    private static final String DIP_CONTENT_ISSUANCE_BLANK_SERIA_COLUMN = "blankSeria";
    private static final String DIP_CONTENT_ISSUANCE_BLANK_NUMBER_COLUMN = "blankNumber";
    private static final String DIP_CONTENT_ISSUANCE_CONTENT_LIST_NUMBER_COLUMN = "contentListNumber";
    private static final String DIP_CONTENT_ISSUANCE_DUPLICATE_COLUMN = "duplicate";
    private static final String DIP_CONTENT_ISSUANCE_DUPLICATE_ISSUANCE_DATE_ID_COLUMN = "duplicateIssuanceDate";
    private static final String DIP_CONTENT_ISSUANCE_DUPLICATE_REGISTRATION_NUMBER_COLUMN = "duplicateRegistrationNumber";

    @Override
    public void exportDiplomaList(Database mdb) throws Exception
    {
        //экспорт справочников
        ICatalogIODao catalogIO = ICatalogIODao.instance.get();
        Map<Sex, String> sexMap = catalogIO.catalogSex().export(mdb);
        Map<DipDocumentType, String> dipDocumentTypeMap = catalog(DipDocumentType.class, DipDocumentType.P_TITLE).export(mdb);
        Map<EduProgramSubject, String> programSubjectMap = catalog(EduProgramSubject.class, EduProgramSubject.P_TITLE).export(mdb);
        Map<EduProgramSpecialization, String> programSpecializationMap = catalog(EduProgramSpecialization.class, EduProgramSpecialization.P_TITLE).export(mdb);

        //экспорт учебных планов
        Set<Long> studentIds = exportStudentEduPlanVersion(mdb, programSubjectMap, programSpecializationMap);
        //экспорт студентов
        Set<Long> personIds = exportStudents(mdb, programSubjectMap, programSpecializationMap, studentIds);
        //эксопрт персон
        exportPersons(mdb, sexMap, personIds);

        //экспорт дипломов
        exportDipObject(mdb, programSpecializationMap, dipDocumentTypeMap);
        exportDipIssuance(mdb);
        exportDipContentDipIssuance(mdb);
    }

    @Override
    public String importDiplomaList(Database mdb) throws IOException
    {
        if (hasFatalErrorsInTables(mdb))
        {
            throw new ApplicationException("Import was interrupted");
        }
        return importTables(mdb);
    }

    private void exportPersons(Database mdb, final Map<Sex, String> sexMap, Set<Long> personIds) throws Exception
    {
        Table person_t = new TableBuilder(PERSON_TABLE)
                /* [0] */.addColumn(new ColumnBuilder(PERSON_ID_COLUMN, DataType.TEXT).setCompressedUnicode(true).toColumn())
                /* [1] */.addColumn(new ColumnBuilder(PERSON_FIRST_NAME_COLUMN, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                /* [2] */.addColumn(new ColumnBuilder(PERSON_LAST_NAME_COLUMN, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                /* [3] */.addColumn(new ColumnBuilder(PERSON_MIDDLE_NAME_COLUMN, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                /* [4] */.addColumn(new ColumnBuilder(PERSON_BIRTHDAY_COLUMN, DataType.TEXT).setCompressedUnicode(true).toColumn())
                /* [5] */.addColumn(new ColumnBuilder(PERSON_SEX_COLUMN, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .toTable(mdb);

        List<Object[]> rows = new ArrayList<>();
        Session session = getSession();

        for (List<Long> idParts : Iterables.partition(personIds, 500))
        {
            DQLSelectBuilder personDql = new DQLSelectBuilder().fromEntity(Person.class, "p").column("p")
                    .where(in(property("p", Person.P_ID), idParts));
            for (Person person : UniBaseDao.<Person>scroll(personDql.createStatement(session)))
            {
                IdentityCard identityCard = person.getIdentityCard();

                List<Object> row = new ArrayList<>(6);
                        /* [0] */
                row.add(Long.toHexString(person.getId()));
                        /* [1] */
                row.add(StringUtils.trimToNull(identityCard.getFirstName()));
                        /* [2] */
                row.add(StringUtils.trimToNull(identityCard.getLastName()));
                        /* [3] */
                row.add(StringUtils.trimToNull(identityCard.getMiddleName()));
                        /* [4] */
                row.add(DateFormatter.DEFAULT_DATE_FORMATTER.format(identityCard.getBirthDate()));
                        /* [5] */
                row.add(sexMap.get(identityCard.getSex()));
                rows.add(row.toArray());
            }
            session.clear();
        }
        person_t.addRows(rows);
    }

    private Set<Long> exportStudents(Database mdb, Map<EduProgramSubject, String> programSubjectMap, Map<EduProgramSpecialization, String> programSpecializationMap,
                                      Set<Long> studentIds) throws Exception
    {
        Table student_t = new TableBuilder(STUDENT_TABLE)
                /* [0] */.addColumn(new ColumnBuilder(STUDENT_ID_COLUMN, DataType.TEXT).setCompressedUnicode(true).toColumn())
                /* [1] */.addColumn(new ColumnBuilder(STUDENT_PERSON_ID_COLUMN, DataType.TEXT).setCompressedUnicode(true).toColumn())
                /* [2] */.addColumn(new ColumnBuilder(STUDENT_PERSONAL_NUMBER_COLUMN, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                /* [3] */.addColumn(new ColumnBuilder(STUDENT_COURSE_COLUMN, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                /* [4] */.addColumn(new ColumnBuilder(STUDENT_GROUP_COLUMN, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                /* [5] */.addColumn(new ColumnBuilder(STUDENT_FINISH_YEAR_COLUMN, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                /* [6] */.addColumn(new ColumnBuilder(STUDENT_ARCHIVAL_COLUMN, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                /* [7] */.addColumn(new ColumnBuilder(STUDENT_PROGRAM_SUBJECT_COLUMN, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                /* [8] */.addColumn(new ColumnBuilder(STUDENT_PROGRAM_SPECIALIZATION_COLUMN, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .toTable(mdb);

        List<Object[]> rows = new ArrayList<>();
        Set<Long> personIds = new HashSet<>();
        Session session = getSession();

        for (List<Long> idParts : Iterables.partition(studentIds, 500))
        {
            DQLSelectBuilder studentDql = new DQLSelectBuilder().fromEntity(Student.class, "p").column("p")
                    .where(in(property("p", Student.P_ID), idParts));
            for (Student student : UniBaseDao.<Student>scroll(studentDql.createStatement(session)))
            {
                EduProgramSubject programSubject = student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject();
                EduProgramSpecialization programSpecialization = student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getEduProgramSpecialization();

                List<Object> row = new ArrayList<>(9);
                        /* [0] */
                row.add(Long.toHexString(student.getId()));
                        /* [1] */
                row.add(Long.toHexString(student.getPerson().getId()));
                personIds.add(student.getPerson().getId());
                        /* [2] */
                row.add(StringUtils.trimToNull(student.getPersonalNumber()));
                        /* [3] */
                row.add(StringUtils.trimToNull(student.getCourse().getTitle()));
                        /* [4] */
                row.add(StringUtils.trimToNull(student.getGroup() != null ? student.getGroup().getTitle() : ""));
                        /* [5] */
                row.add(student.getFinishYear() != null ? String.valueOf(student.getFinishYear()) : "");
                        /* [6] */
                row.add(student.isArchival() ? "1" : "0");
                        /* [7] */
                row.add(StringUtils.trimToNull(programSubject != null ? programSubjectMap.get(programSubject) : ""));
                        /* [8] */
                row.add(StringUtils.trimToNull(programSpecialization != null ? programSpecializationMap.get(programSpecialization) : ""));

                rows.add(row.toArray());
            }
            session.clear();
        }
        student_t.addRows(rows);
        return personIds;
    }


    private Set<Long> exportStudentEduPlanVersion(Database mdb, Map<EduProgramSubject, String> programSubjectMap, Map<EduProgramSpecialization, String> programSpecializationMap) throws Exception
    {
        Table epp_student_eduplanversion_t = new TableBuilder(STUDENT_EDU_PLAN_VERSION_TABLE)
                /* [0] */.addColumn(new ColumnBuilder(STUDENT_EDU_PLAN_VERSION_ID_COLUMN, DataType.TEXT).setCompressedUnicode(true).toColumn())
                /* [1] */.addColumn(new ColumnBuilder(STUDENT_EDU_PLAN_VERSION_STUDENT_ID_COLUMN, DataType.TEXT).setCompressedUnicode(true).toColumn())
                /* [2] */.addColumn(new ColumnBuilder(STUDENT_EDU_PLAN_VERSION_NUMBER_COLUMN, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                /* [3] */.addColumn(new ColumnBuilder(STUDENT_EDU_PLAN_VERSION_EDU_PLAN_NUMBER_COLUMN, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                /* [4] */.addColumn(new ColumnBuilder(STUDENT_EDU_PLAN_VERSION_PROGRAM_SUBJECT_COLUMN, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                /* [5] */.addColumn(new ColumnBuilder(STUDENT_EDU_PLAN_VERSION_PROGRAM_SPECIALIZATION_COLUMN, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                /* [6] */.addColumn(new ColumnBuilder(STUDENT_EDU_PLAN_VERSION_REMOVAL_DATE_COLUMN, DataType.TEXT).setCompressedUnicode(true).toColumn())
                .toTable(mdb);

        List<Object[]> rows = new ArrayList<>();
        Set<Long> studentIds = new HashSet<>();

        Session session = getSession();
        DQLSelectBuilder epvDql = new DQLSelectBuilder().fromEntity(EppStudent2EduPlanVersion.class, "p").column("p");
        for (List<EppStudent2EduPlanVersion> listPart : Iterables.partition(UniBaseDao.<EppStudent2EduPlanVersion>scroll(epvDql.createStatement(session)), 500))
        {
            for (EppStudent2EduPlanVersion student2EduPlanVersion : listPart)
            {
                EduProgramSubject programSubject = student2EduPlanVersion.getEduPlanVersion().getEduPlan() instanceof EppEduPlanProf ? ((EppEduPlanProf) student2EduPlanVersion.getEduPlanVersion().getEduPlan()).getProgramSubject() : null;
                EduProgramSpecialization programSpecialization = student2EduPlanVersion.getBlock() != null && student2EduPlanVersion.getBlock() instanceof EppEduPlanVersionSpecializationBlock ?
                        ((EppEduPlanVersionSpecializationBlock) student2EduPlanVersion.getBlock()).getProgramSpecialization() : null;

                List<Object> row = new ArrayList<>(7);
                        /* [0] */
                row.add(Long.toHexString(student2EduPlanVersion.getId()));
                        /* [1] */
                row.add(Long.toHexString(student2EduPlanVersion.getStudent().getId()));
                studentIds.add(student2EduPlanVersion.getStudent().getId());
                        /* [2] */
                row.add(StringUtils.trimToNull(student2EduPlanVersion.getEduPlanVersion().getNumber()));
                        /* [3] */
                row.add(StringUtils.trimToNull(student2EduPlanVersion.getEduPlanVersion().getEduPlan().getNumber()));
                        /* [4] */
                row.add(StringUtils.trimToNull(programSubject != null ? programSubjectMap.get(programSubject) : ""));
                        /* [5] */
                row.add(StringUtils.trimToNull(programSpecialization != null ? programSpecializationMap.get(programSpecialization) : ""));
                        /* [6] */
                row.add(StringUtils.trimToNull(DateFormatter.DEFAULT_DATE_FORMATTER.format(student2EduPlanVersion.getRemovalDate())));
                rows.add(row.toArray());
            }
            session.clear();
        }
        epp_student_eduplanversion_t.addRows(rows);

        return studentIds;
    }

    private void exportDipObject(Database mdb, Map<EduProgramSpecialization, String> programSpecializationMap, Map<DipDocumentType, String> dipDocumentTypeMap) throws Exception
    {
        Table dip_object_t = new TableBuilder(DIP_OBJECT_TABLE)
                /* [0] */.addColumn(new ColumnBuilder(DIP_OBJECT_ID_COLUMN, DataType.TEXT).setCompressedUnicode(true).toColumn())
                /* [1] */.addColumn(new ColumnBuilder(DIP_OBJECT_TYPE_COLUMN, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                /* [2] */.addColumn(new ColumnBuilder(DIP_OBJECT_WITH_SUCCESS_COLUMN, DataType.TEXT).setCompressedUnicode(true).toColumn())
                /* [3] */.addColumn(new ColumnBuilder(DIP_OBJECT_STUDENT_EDUPLAN_VERSION_COLUMN, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                /* [4] */.addColumn(new ColumnBuilder(DIP_OBJECT_PROGRAM_SPECIALIZATION_COLUMN, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                /* [5] */.addColumn(new ColumnBuilder(DIP_OBJECT_LABOR_COLUMN, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                /* [6] */.addColumn(new ColumnBuilder(DIP_OBJECT_STATE_COMMISSION_CHAIR_FIO_COLUMN, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                /* [7] */.addColumn(new ColumnBuilder(DIP_OBJECT_STATE_COMMISSION_DATE_COLUMN, DataType.TEXT).setCompressedUnicode(true).toColumn())
                /* [8] */.addColumn(new ColumnBuilder(DIP_OBJECT_STATE_COMMISSION_PROTOCOL_NUMBER_COLUMN, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .toTable(mdb);

        Session session = getSession();
        List<Object[]> rows = new ArrayList<>();
        DQLSelectBuilder diplomaDql = new DQLSelectBuilder().fromEntity(DiplomaObject.class, "p").column("p");

        for (List<DiplomaObject> listPart : Iterables.partition(UniBaseDao.<DiplomaObject>scroll(diplomaDql.createStatement(session)), 500))
        {
            for (DiplomaObject diplomaObject : listPart)
            {
                DiplomaContent diplomaContent = diplomaObject.getContent();
                EduProgramSpecialization programSpecialization = diplomaObject.getContent().getProgramSpecialization();
                String labor = diplomaContent.getLaborAsLong() != null && diplomaContent.getLaborAsLong() >= 0 ? String.valueOf(diplomaContent.getLaborAsLong()) : "";
                String weeks = diplomaContent.getWeeksAsLong() != null && diplomaContent.getWeeksAsLong() >= 0 ? String.valueOf(diplomaContent.getWeeksAsLong()) : "";
                List<Object> row = new ArrayList<>(9);
                        /* [0] */
                row.add(Long.toHexString(diplomaContent.getId()));
                        /* [1] */
                row.add(StringUtils.trimToNull(dipDocumentTypeMap.get(diplomaContent.getType())));
                        /* [2] */
                row.add(diplomaContent.isWithSuccess() ? "1" : "0");
                        /* [3] */
                row.add(Long.toHexString((diplomaObject.getStudentEpv() == null) ? 0 : diplomaObject.getStudentEpv().getId()));
                        /* [4] */
                row.add(StringUtils.trimToNull(programSpecialization != null ? programSpecializationMap.get(programSpecialization) : ""));
                        /* [5] */
                row.add(StringUtils.trimToNull(DipDocumentUtils.isShowLabor(diplomaObject) ? labor : weeks));
                        /* [6] */
                row.add(StringUtils.trimToNull(diplomaObject.getStateCommissionChairFio()));
                        /* [7] */
                row.add(DateFormatter.DEFAULT_DATE_FORMATTER.format(diplomaObject.getStateCommissionDate()));
                        /* [8] */
                row.add(StringUtils.trimToNull(diplomaObject.getStateCommissionProtocolNumber()));

                rows.add(row.toArray());
            }
            session.clear();
        }
        dip_object_t.addRows(rows);
    }

    private void exportDipIssuance(Database mdb) throws Exception
    {
        Table dip_issuance_t = new TableBuilder(DIP_ISSUANCE_TABLE)
                /* [0] */.addColumn(new ColumnBuilder(DIP_ISSUANCE_ID_COLUMN, DataType.TEXT).setCompressedUnicode(true).toColumn())
                /* [1] */.addColumn(new ColumnBuilder(DIP_ISSUANCE_OBJECT_ID_COLUMN, DataType.TEXT).setCompressedUnicode(true).toColumn())
                /* [2] */.addColumn(new ColumnBuilder(DIP_ISSUANCE_BLANK_SERIA_COLUMN, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                /* [3] */.addColumn(new ColumnBuilder(DIP_ISSUANCE_BLANK_NUMBER_COLUMN, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                /* [4] */.addColumn(new ColumnBuilder(DIP_ISSUANCE_DATE_COLUMN, DataType.TEXT).setCompressedUnicode(true).toColumn())
                /* [5] */.addColumn(new ColumnBuilder(DIP_ISSUANCE_REG_NUMBER_COLUMN, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                /* [6] */.addColumn(new ColumnBuilder(DIP_ISSUANCE_REPLACED_ISSUANCE_COLUMN, DataType.TEXT).setCompressedUnicode(true).toColumn())
                .toTable(mdb);

        Session session = getSession();
        List<Object[]> rows = new ArrayList<>();
        DQLSelectBuilder issuanceDql = new DQLSelectBuilder().fromEntity(DiplomaIssuance.class, "p").column("p");


        for (List<DiplomaIssuance> listPart : Iterables.partition(UniBaseDao.<DiplomaIssuance>scroll(issuanceDql.createStatement(session)), 500))
        {
            for (DiplomaIssuance diplomaIssuance : listPart)
            {
                List<Object> row = new ArrayList<>(7);
                        /* [0] */
                row.add(Long.toHexString(diplomaIssuance.getId()));
                        /* [1] */
                row.add(Long.toHexString(diplomaIssuance.getDiplomaObject().getContent().getId()));
                        /* [2] */
                row.add(StringUtils.trimToNull(diplomaIssuance.getBlankSeria()));
                        /* [3] */
                row.add(StringUtils.trimToNull(diplomaIssuance.getBlankNumber()));
                        /* [4] */
                row.add(DateFormatter.DEFAULT_DATE_FORMATTER.format(diplomaIssuance.getIssuanceDate()));
                        /* [5] */
                row.add(StringUtils.trimToNull(diplomaIssuance.getRegistrationNumber()));
                        /* [6] */
                row.add(diplomaIssuance.getReplacedIssuance() != null ? Long.toHexString(diplomaIssuance.getReplacedIssuance().getId()) : "");
                rows.add(row.toArray());
            }
            session.clear();
        }
        dip_issuance_t.addRows(rows);
    }

    private void exportDipContentDipIssuance(Database mdb) throws Exception
    {
        Table dip_content_issuance_t = new TableBuilder(DIP_CONTENT_ISSUANCE_TABLE)
                /* [0] */.addColumn(new ColumnBuilder(DIP_CONTENT_ISSUANCE_ID_COLUMN, DataType.TEXT).setCompressedUnicode(true).toColumn())
                /* [1] */.addColumn(new ColumnBuilder(DIP_CONTENT_ISSUANCE_ISSUANCE_ID_COLUMN, DataType.TEXT).setCompressedUnicode(true).toColumn())
                /* [2] */.addColumn(new ColumnBuilder(DIP_CONTENT_ISSUANCE_BLANK_SERIA_COLUMN, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                /* [3] */.addColumn(new ColumnBuilder(DIP_CONTENT_ISSUANCE_BLANK_NUMBER_COLUMN, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                /* [4] */.addColumn(new ColumnBuilder(DIP_CONTENT_ISSUANCE_CONTENT_LIST_NUMBER_COLUMN, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                /* [5] */.addColumn(new ColumnBuilder(DIP_CONTENT_ISSUANCE_DUPLICATE_COLUMN, DataType.TEXT).setCompressedUnicode(true).toColumn())
                /* [6] */.addColumn(new ColumnBuilder(DIP_CONTENT_ISSUANCE_DUPLICATE_ISSUANCE_DATE_ID_COLUMN, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                /* [7] */.addColumn(new ColumnBuilder(DIP_CONTENT_ISSUANCE_DUPLICATE_REGISTRATION_NUMBER_COLUMN, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .toTable(mdb);

        Session session = getSession();
        List<Object[]> rows = new ArrayList<>();
        DQLSelectBuilder contentIssuanceDql = new DQLSelectBuilder().fromEntity(DiplomaContentIssuance.class, "p").column("p");

        for (List<DiplomaContentIssuance> listPart : Iterables.partition(UniBaseDao.<DiplomaContentIssuance>scroll(contentIssuanceDql.createStatement(session)), 500))
        {
            for (DiplomaContentIssuance contentIssuance : listPart)
            {
                List<Object> row = new ArrayList<>(8);
                        /* [0] */
                row.add(Long.toHexString(contentIssuance.getId()));
                        /* [1] */
                row.add(Long.toHexString(contentIssuance.getDiplomaIssuance().getId()));
                        /* [2] */
                row.add(StringUtils.trimToNull(contentIssuance.getBlankSeria()));
                        /* [3] */
                row.add(StringUtils.trimToNull(contentIssuance.getBlankNumber()));
                        /* [4] */
                row.add(StringUtils.trimToNull(String.valueOf(contentIssuance.getContentListNumber())));
                        /* [5] */
                row.add(contentIssuance.isDuplicate() ? "1" : "0");
                        /* [6] */
                row.add(DateFormatter.DEFAULT_DATE_FORMATTER.format(contentIssuance.getDuplicateIssuanceDate()));
                        /* [7] */
                row.add(StringUtils.trimToNull(contentIssuance.getDuplicateRegustrationNumber()));
                rows.add(row.toArray());
            }
            session.clear();
        }
        dip_content_issuance_t.addRows(rows);
    }

    private String importTables(Database mdb) throws IOException
    {
        final List<String> warningList = new ArrayList<>();
        final Map<String, Long> diplomaIdMap = new HashMap<>();
        final Map<String, Long> issuanceIdMap = new HashMap<>();

        final int diplomasNumber = importDiplomas(mdb, diplomaIdMap, warningList);
        final int issuancesNumber = importIssuances(mdb, diplomaIdMap, issuanceIdMap);
        final int contentIssuancesNumber = importContentIssuances(mdb, issuanceIdMap);

        final StringBuilder out = new StringBuilder();
        out.append("Добавлено: ");
        out.append(CommonBaseStringUtil.numberWithPostfixCase(diplomasNumber, "документ", "документа", "документов")).append(", ");
        out.append(CommonBaseStringUtil.numberWithPostfixCase(issuancesNumber, "факт выдачи", "факта выдачи", "фактов выдачи")).append(", ");
        out.append(CommonBaseStringUtil.numberWithPostfixCase(contentIssuancesNumber, "приложение", "приложения", "приложений")).append(".");
        if (!warningList.isEmpty())
        {
            out.append(" В процессе импорта найдены несоответствия. Обратитесь к логу импорта (файл " + IMPORT_DIPLOMAS_LOG_FILE_NAME + ".log) на сервере приложения для получения детальной информации о несоответствиях.");
        }
        return out.toString();
    }

    private Integer importContentIssuances(Database mdb, final Map<String, Long> issuanceIdMap) throws IOException
    {
        final MutableInt number = new MutableInt(0);
        Table dip_content_issuance_t = mdb.getTable(DIP_CONTENT_ISSUANCE_TABLE);
        execute(dip_content_issuance_t, 500, "Импорт приложений к фактам выдачи документов об образовании " + DIP_ISSUANCE_TABLE, rows -> {
            List<Long> localIds = new ArrayList<>();
            for (final Map<String, Object> row : rows)
            {
                localIds.add(tryParseHexId(String.valueOf(row.get(DIP_CONTENT_ISSUANCE_ID_COLUMN))));
            }
            //id сущностей, уже сохранных в БД:
            Set<Long> dbIds = new HashSet<>(new DQLSelectBuilder().fromEntity(DiplomaContentIssuance.class, "d")
                                                    .column(property("d", DiplomaContentIssuance.P_ID))
                                                    .where(in(property("d", DiplomaContentIssuance.P_ID), localIds))
                                                    .createStatement(getSession()).<Long>list());
            for (final Map<String, Object> row : rows)
            {

                if (dbIds.contains(tryParseHexId((String) row.get(DIP_CONTENT_ISSUANCE_ID_COLUMN))))
                {
                    continue;
                }
                Long diplomaIssuanceId = issuanceIdMap.get(String.valueOf(row.get(DIP_CONTENT_ISSUANCE_ISSUANCE_ID_COLUMN)));
                DiplomaIssuance diplomaIssuance = get(DiplomaIssuance.class, diplomaIssuanceId);
                boolean isDuplicate = "1".equals(row.get(DIP_CONTENT_ISSUANCE_DUPLICATE_COLUMN));
                DiplomaContentIssuance contentIssuance = new DiplomaContentIssuance();
                contentIssuance.setDiplomaIssuance(diplomaIssuance);
                contentIssuance.setBlankSeria((String) row.get(DIP_CONTENT_ISSUANCE_BLANK_SERIA_COLUMN));
                contentIssuance.setBlankNumber((String) row.get(DIP_CONTENT_ISSUANCE_BLANK_NUMBER_COLUMN));
                contentIssuance.setContentListNumber(Integer.parseInt((String) row.get(DIP_CONTENT_ISSUANCE_CONTENT_LIST_NUMBER_COLUMN)));
                contentIssuance.setDuplicate(isDuplicate);
                if (isDuplicate)
                {
                    Date duplicateIssuanceDate = date(row, DIP_CONTENT_ISSUANCE_DUPLICATE_ISSUANCE_DATE_ID_COLUMN, DIP_CONTENT_ISSUANCE_TABLE + "[" + DIP_CONTENT_ISSUANCE_ID_COLUMN + "=" + row.get(DIP_CONTENT_ISSUANCE_ID_COLUMN) + "]", false);
                    String duplicateRegistrationNumber = (String) row.get(DIP_CONTENT_ISSUANCE_DUPLICATE_REGISTRATION_NUMBER_COLUMN);
                    contentIssuance.setDuplicateIssuanceDate(duplicateIssuanceDate);
                    contentIssuance.setDuplicateRegustrationNumber(duplicateRegistrationNumber);
                }

                DipDiplomaBlank blank = getDiplomaBlank(contentIssuance.getDiplomaIssuance().getDiplomaObject(), contentIssuance.getBlankSeria(), contentIssuance.getBlankNumber(), true);
                if (blank!=null)
                {
                    contentIssuance.setBlank(blank);
                }

                save(contentIssuance);
                number.add(1);
            }
        });
        return number.toInteger();
    }

    private Integer importIssuances(Database mdb, final Map<String, Long> diplomaIdMap, final Map<String, Long> issuanceIdMap) throws IOException
    {
        final MutableInt number = new MutableInt(0);
        Table dip_issuance_t = mdb.getTable(DIP_ISSUANCE_TABLE);
        final Map<DiplomaIssuance, String> issuanceWithDuplicate = new HashMap<>();
        execute(dip_issuance_t, 500, "Импорт фактов выдачи документов об образовании " + DIP_ISSUANCE_TABLE, rows -> {

            List<Long> localIds = new ArrayList<>();
            for (final Map<String, Object> row : rows)
            {
                localIds.add(tryParseHexId(String.valueOf(row.get(DIP_ISSUANCE_ID_COLUMN))));
            }
            //id сущностей, уже сохранных в БД:
            Set<Long> dbIds = new HashSet<>(new DQLSelectBuilder().fromEntity(DiplomaIssuance.class, "d")
                                                    .column(property("d", DiplomaIssuance.P_ID))
                                                    .where(in(property("d", DiplomaIssuance.P_ID), localIds))
                                                    .createStatement(getSession()).<Long>list());

            for (final Map<String, Object> row : rows)
            {

                if (dbIds.contains(tryParseHexId((String) row.get(DIP_ISSUANCE_ID_COLUMN))))
                {
                    issuanceIdMap.put((String) row.get(DIP_ISSUANCE_ID_COLUMN), tryParseHexId((String) row.get(DIP_ISSUANCE_ID_COLUMN)));
                    continue;
                }
                Long diplomaContentId = diplomaIdMap.get(String.valueOf(row.get(DIP_ISSUANCE_OBJECT_ID_COLUMN)));
                DiplomaObject diplomaObject = get(DiplomaObject.class, DiplomaObject.content().id(), diplomaContentId);
                String blankSeria = (String) row.get(DIP_ISSUANCE_BLANK_SERIA_COLUMN);
                String blankNumber = (String) row.get(DIP_ISSUANCE_BLANK_NUMBER_COLUMN);
                Date issuanceDate = date(row, DIP_ISSUANCE_DATE_COLUMN, DIP_ISSUANCE_TABLE + "[" + DIP_ISSUANCE_ID_COLUMN + "=" + row.get(DIP_ISSUANCE_ID_COLUMN) + "]", false);
                String registrationNumber = (String) row.get(DIP_ISSUANCE_REG_NUMBER_COLUMN);

                DiplomaIssuance diplomaIssuance = new DiplomaIssuance();
                diplomaIssuance.setDiplomaObject(diplomaObject);
                diplomaIssuance.setBlankSeria(blankSeria);
                diplomaIssuance.setBlankNumber(blankNumber);
                diplomaIssuance.setIssuanceDate(issuanceDate);
                diplomaIssuance.setRegistrationNumber(registrationNumber);

                DipDiplomaBlank blank = getDiplomaBlank(diplomaObject, blankSeria, blankNumber, false);
                if (blank!=null)
                {
                    diplomaIssuance.setBlank(blank);
                }

                save(diplomaIssuance);
                number.add(1);
                issuanceIdMap.put((String) row.get(DIP_ISSUANCE_ID_COLUMN), diplomaIssuance.getId());

                /*  в связи с тем, что дубликат на который ссылается данный факт выдачи,
                  *  может быть сохранен позднее (и его id, который будет сохранен в БД, неизвестен), складываем факты выдачи и их дубликаты в map,
                  *  для сохранения этих данных в отдельном цикле
                 */
                if (!StringUtils.isEmpty((String)row.get(DIP_ISSUANCE_REPLACED_ISSUANCE_COLUMN)))
                {
                    issuanceWithDuplicate.put(diplomaIssuance, (String) row.get(DIP_ISSUANCE_REPLACED_ISSUANCE_COLUMN));
                }

            }
        });

        for (DiplomaIssuance diplomaIssuance : issuanceWithDuplicate.keySet())
        {
            String issuanceHexId = issuanceWithDuplicate.get(diplomaIssuance);
            Long replacedIssuanceId = issuanceIdMap.get(issuanceHexId);
            diplomaIssuance.setReplacedIssuance(get(DiplomaIssuance.class, replacedIssuanceId));
            update(diplomaIssuance);
        }

        return number.toInteger();

    }

    private DipDiplomaBlank getDiplomaBlank(DiplomaObject diplomaObject, String blankSeria, String blankNumber, boolean isApplicationToIssuance)
    {
        String typeCode = DipDiplomaBlankManager.instance().diplomaBlankDao().getDiplomaObjectBlankType(diplomaObject.getContent().getType(), diplomaObject.getContent().isWithSuccess(), isApplicationToIssuance);

        return new DQLSelectBuilder()
                .fromEntity(DipDiplomaBlank.class, "b").column("b")
                .where(eqValue(property("b", DipDiplomaBlank.seria()), blankSeria))
                .where(eqValue(property("b", DipDiplomaBlank.number()), blankNumber))
                .where(eqValue(property("b", DipDiplomaBlank.blankState().code()), DipBlankStateCodes.FREE))
                .where(eqValue(property("b", DipDiplomaBlank.blankType().code()), typeCode))
                .createStatement(getSession()).uniqueResult();
    }
    private Integer importDiplomas(Database mdb, final Map<String, Long> diplomaIdMap, final List<String> warningList) throws IOException
    {

        final MutableInt number = new MutableInt(0);
        Table dip_object_t = mdb.getTable(DIP_OBJECT_TABLE);
        Table student_epv_t = mdb.getTable(STUDENT_EDU_PLAN_VERSION_TABLE);
        final Map<String, DipDocumentType> dipDocumentTypeMap = this.catalog(DipDocumentType.class, DipDocumentType.P_TITLE).lookup(true);
        final Map<String, EduProgramSpecialization> specializationMap = catalog(EduProgramSpecialization.class, EduProgramSpecialization.P_TITLE).lookup(false);
        final Map<String, String> studentEpvSpecializationMap = fetchMapFromTable(student_epv_t, STUDENT_EDU_PLAN_VERSION_ID_COLUMN, STUDENT_PROGRAM_SPECIALIZATION_COLUMN);


        execute(dip_object_t, 500, "Импорт документов об образовании " + DIP_OBJECT_TABLE, rows -> {
            List<Long> localIds = new ArrayList<>();
            for (final Map<String, Object> row : rows)
            {
                diplomaIdMap.put((String) row.get(DIP_OBJECT_ID_COLUMN), tryParseHexId((String) row.get(DIP_OBJECT_ID_COLUMN)));
                localIds.add(tryParseHexId((String) row.get(DIP_OBJECT_ID_COLUMN)));
            }
            //id сущностей, уже сохранных в БД:
            Set<Long> dbIds = new HashSet<>(new DQLSelectBuilder().fromEntity(DiplomaObject.class, "d")
                                                    .column(property("d", DiplomaObject.content().id()))
                                                    .where(in(property("d", DiplomaObject.content().id()), localIds))
                                                    .createStatement(getSession()).<Long>list());

            for (final Map<String, Object> row : rows)
            {
                Long diplomaId = tryParseHexId((String) row.get(DIP_OBJECT_ID_COLUMN));
                //Если запись уже есть в БД, то переходим к следующей итерации
                if (dbIds.contains(diplomaId))
                {
                    continue;
                }

                boolean withSuccess = "1".equals(row.get(DIP_OBJECT_WITH_SUCCESS_COLUMN));
				Long epvId = tryParseHexId((String) row.get(DIP_OBJECT_STUDENT_EDUPLAN_VERSION_COLUMN));
				if ((epvId == null) || (epvId == 0))
					continue;

                EppStudent2EduPlanVersion studentEpv = get(EppStudent2EduPlanVersion.class, epvId);

                String specializationCode = (String) row.get(DIP_OBJECT_PROGRAM_SPECIALIZATION_COLUMN);
                EduProgramSpecialization programSpecialization = specializationMap.get(specializationCode);
                EduProgramSubject programSubject = ((EppEduPlanProf) studentEpv.getEduPlanVersion().getEduPlan()).getProgramSubject();

				boolean isLabor = (programSubject != null) && programSubject.isLabor();
				Long load = row.get(DIP_OBJECT_LABOR_COLUMN)!=null?Long.valueOf((String) row.get(DIP_OBJECT_LABOR_COLUMN)):0;

                String stateCommissionProtocolNumber = (String)row.get(DIP_OBJECT_STATE_COMMISSION_PROTOCOL_NUMBER_COLUMN);
                String stateCommissionChairFio = (String)row.get(DIP_OBJECT_STATE_COMMISSION_CHAIR_FIO_COLUMN);
                Date stateCommissionDate = date(row, DIP_OBJECT_STATE_COMMISSION_DATE_COLUMN, DIP_OBJECT_TABLE + "[" + DIP_OBJECT_ID_COLUMN + "=" + diplomaId + "]", false);

                DipDocumentType dipDocType = dipDocumentTypeMap.get(String.valueOf(row.get(DIP_OBJECT_TYPE_COLUMN)));
                DiplomaContent diplomaContent = new DiplomaContent();
                diplomaContent.setType(dipDocType);
                diplomaContent.setWithSuccess(withSuccess);

                if (isLabor)
                {
                    diplomaContent.setLaborAsLong(load);
                }
                else
                {
                    diplomaContent.setWeeksAsLong(load);
                }
                diplomaContent.setProgramSpecialization(programSpecialization);
                diplomaContent.setProgramSubject(programSubject);
                diplomaContent.setEducationElementTitle((programSubject == null) ? "" : programSubject.getTitleWithCode());

                DiplomaObject diplomaObject = new DiplomaObject();
                diplomaObject.setContent(diplomaContent);
                diplomaObject.setStudentEpv(studentEpv);
				diplomaObject.setStudent(studentEpv.getStudent());
                diplomaObject.setStateCommissionDate(stateCommissionDate);
                diplomaObject.setStateCommissionChairFio(stateCommissionChairFio);
                diplomaObject.setStateCommissionProtocolNumber(stateCommissionProtocolNumber);

                save(diplomaContent);
                save(diplomaObject);
                diplomaIdMap.put((String) row.get(DIP_OBJECT_ID_COLUMN), diplomaObject.getContent().getId());
                if ((!StringUtils.isEmpty(specializationCode ) && !specializationCode.equals(row.get(DIP_OBJECT_STUDENT_EDUPLAN_VERSION_COLUMN)))
                        || (!StringUtils.isEmpty((String)row.get(DIP_OBJECT_STUDENT_EDUPLAN_VERSION_COLUMN)) && (row.get(DIP_OBJECT_STUDENT_EDUPLAN_VERSION_COLUMN)).equals(specializationCode)))
                {
                    warningList.add("For id '" + row.get(DIP_OBJECT_ID_COLUMN) + "' (table " + DIP_OBJECT_TABLE + ") programSpecialization='" + specializationCode + "' does not coincide with the programSpecialization='" + studentEpvSpecializationMap.get(row.get(DIP_OBJECT_STUDENT_EDUPLAN_VERSION_COLUMN)) + "' (table " + STUDENT_EDU_PLAN_VERSION_TABLE + ")");
                    getLog4jLogger().warn("For id '" + row.get(DIP_OBJECT_ID_COLUMN) + "' (table " + DIP_OBJECT_TABLE + ") programSpecialization='" + specializationCode + "' does not coincide with the programSpecialization='" + studentEpvSpecializationMap.get(row.get(DIP_OBJECT_STUDENT_EDUPLAN_VERSION_COLUMN)) + "' (table " + STUDENT_EDU_PLAN_VERSION_TABLE + ")");
                }
                number.add(1);
            }
        });
        return number.toInteger();
    }

    /**
     * Проверяет есть ли в таблицах mdb-файла фатальные ошибки, приводящие к остановке импорта
     *
     * @param mdb проверяемый mdb файл
     * @return true - есть fatal errors, false - нет fatal errors
     * @throws IOException
     */
    private boolean hasFatalErrorsInTables(Database mdb) throws IOException
    {
        final List<String> fatalErrors = new ArrayList<>();
        //Проверяем наличие справочных таблиц:
        getExistTable(mdb, PERSON_TABLE, fatalErrors);
        getExistTable(mdb, STUDENT_TABLE, fatalErrors);
        Table student_edu_plan_ver_t = getExistTable(mdb, STUDENT_EDU_PLAN_VERSION_TABLE, fatalErrors);
        Table doc_type_t = getExistTable(mdb, "catalog_dipDocumentType", fatalErrors);
        Table program_specialization_t = getExistTable(mdb, "catalog_eduProgramSpecialization", fatalErrors);
        getExistTable(mdb, "catalog_eduProgramSubject", fatalErrors);
        getExistTable(mdb, "catalog_sex", fatalErrors);

        //Проверяем наличие таблиц для импорта:
        Table dip_object_t = getExistTable(mdb, DIP_OBJECT_TABLE, fatalErrors);
        Table dip_issuance_t = getExistTable(mdb, DIP_ISSUANCE_TABLE, fatalErrors);
        Table dip_content_issuance_t = getExistTable(mdb, DIP_CONTENT_ISSUANCE_TABLE, fatalErrors);

        List<String> diplomaObjectIds = new ArrayList<>();
        List<String> dipIssuanceIds = new ArrayList<>();
        List<String> dipContentIssuanceIds = new ArrayList<>();


        if (dip_object_t != null)
        {
            List<String> dipDocTypeIds = fetchColumnFromTable(doc_type_t, "text");
            List<String> dipStudentEduPlanVersionIds = fetchColumnFromTable(student_edu_plan_ver_t, STUDENT_EDU_PLAN_VERSION_ID_COLUMN);
            List<String> specializationIds = fetchColumnFromTable(program_specialization_t, "text");
            validateDiplomaObject(dip_object_t, fatalErrors, diplomaObjectIds, dipDocTypeIds, dipStudentEduPlanVersionIds, specializationIds);
        }

        if (dip_issuance_t != null)
        {
            List<String> allIssuanceIds = fetchColumnFromTable(dip_issuance_t, DIP_ISSUANCE_ID_COLUMN);
            validateDiplomaIssuance(dip_issuance_t, fatalErrors, dipIssuanceIds, diplomaObjectIds, allIssuanceIds);
        }
        if (dip_content_issuance_t != null)
        {
            validateDiplomaContentIssuance(dip_content_issuance_t, fatalErrors, dipContentIssuanceIds, dipIssuanceIds);
        }


        for (String fError : fatalErrors)
        {
            getLog4jLogger().fatal(fError);
        }
        return !fatalErrors.isEmpty();
    }


    // Проверяет таблицу с документами о полученном образовании на наличие fatal errors, все найденные ошибки сохраняются в список List<String> fatalErrors
    private void validateDiplomaObject(Table dip_object_t, final List<String> fatalErrors,
                                       final List<String> diplomaObjectIds, final List<String> dipDocTypeIds, final List<String> dipStudentEduPlanVersionIds,
                                       final List<String> specializationIds)
    {

        final MutableBoolean hasNotHexId = new MutableBoolean(false);

        execute(dip_object_t, 500, "Валидация таблицы " + DIP_OBJECT_TABLE, rows -> {
            List<Long> localIds = new ArrayList<>();
            for (final Map<String, Object> row : rows)
            {
                localIds.add(tryParseHexId(String.valueOf(row.get(DIP_OBJECT_ID_COLUMN))));
            }
            //id сущностей, уже сохранных в БД:
            List<Long> dbIds = new DQLSelectBuilder().fromEntity(DiplomaObject.class, "d")
                    .column(property("d", DiplomaObject.P_ID))
                    .where(in(property("d", DiplomaObject.P_ID), localIds))
                    .createStatement(getSession()).list();
            for (final Map<String, Object> row : rows)
            {
                String diplomaId = (String)row.get(DIP_OBJECT_ID_COLUMN);
                //Если запись уже есть в БД, то не валидируем ее
                if (dbIds.contains(tryParseHexId(String.valueOf(row.get(DIP_OBJECT_ID_COLUMN)))))
                {
                    if (!StringUtils.isEmpty(diplomaId))
                        diplomaObjectIds.add(diplomaId);
                    continue;
                }
                String dipDocType = (String) row.get(DIP_OBJECT_TYPE_COLUMN);
                String withSuccess = (String) row.get(DIP_OBJECT_WITH_SUCCESS_COLUMN);
                String studentEduPlanVersion = (String) row.get(DIP_OBJECT_STUDENT_EDUPLAN_VERSION_COLUMN);
                String specialization = (String) row.get(DIP_OBJECT_PROGRAM_SPECIALIZATION_COLUMN);
                String labor = (String) row.get(DIP_OBJECT_LABOR_COLUMN);
                Date stateCommissionDate = date(row, DIP_OBJECT_STATE_COMMISSION_DATE_COLUMN, DIP_OBJECT_TABLE + "[" + DIP_OBJECT_ID_COLUMN + "=" + diplomaId + "]", false);
                //значение id не задано, либо имеет неверные формат (должно быть число в hex-виде).
                if (hasNotHexId.isFalse())
                {
                    hasNotHexId.setValue(hasErrorInRequiredHexField(row, DIP_OBJECT_TABLE, DIP_OBJECT_ID_COLUMN, false, null, fatalErrors));
                }
                //дублирующиеся ID
                checkDuplicatedIds(row, diplomaObjectIds, DIP_OBJECT_TABLE, DIP_OBJECT_ID_COLUMN, fatalErrors);

                //корректность типа документа об образовании
                checkEmptyField(row, DIP_OBJECT_TABLE, DIP_OBJECT_TYPE_COLUMN, diplomaId, fatalErrors);
                checkElemInCatalog(dipDocType, dipDocTypeIds, DIP_OBJECT_TABLE, DIP_OBJECT_TYPE_COLUMN, diplomaId, fatalErrors);

                //корректонсть поля "с отличием"
                checkEmptyField(row, DIP_OBJECT_TABLE, DIP_OBJECT_WITH_SUCCESS_COLUMN, diplomaId, fatalErrors);

                if (!StringUtils.isEmpty(withSuccess)
                        && (!withSuccess.equals("0") && !withSuccess.equals("1")))
                {
                    fatalErrors.add("context: unable to parse value in the field 'withSuccess' table 'dip_object_t'. Error with processing: " + DIP_OBJECT_TABLE + "[" + diplomaId + "].");
                }
                //корректность учебного плана студента
                checkEmptyField(row, DIP_OBJECT_TABLE, DIP_OBJECT_STUDENT_EDUPLAN_VERSION_COLUMN, diplomaId, fatalErrors);

                checkElemInCatalog(studentEduPlanVersion, dipStudentEduPlanVersionIds, DIP_OBJECT_TABLE, DIP_OBJECT_STUDENT_EDUPLAN_VERSION_COLUMN, diplomaId, fatalErrors);

                //корректность направленности образовательной программы
                checkElemInCatalog(specialization, specializationIds, DIP_OBJECT_TABLE, DIP_OBJECT_PROGRAM_SPECIALIZATION_COLUMN, diplomaId, fatalErrors);
                //корректность общей трудоемкости образовательной программы
                checkNumber(labor, DIP_OBJECT_TABLE, DIP_OBJECT_LABOR_COLUMN, diplomaId, fatalErrors);
                //корректность даты заседания ГЭК
                checkDate(stateCommissionDate, DIP_OBJECT_TABLE, DIP_OBJECT_STATE_COMMISSION_DATE_COLUMN, diplomaId, false, fatalErrors);
            }
        });
    }

    // Проверяет таблицу с фактами выдачи документов о полученном образовании на наличие fatal errors, все найденные ошибки сохраняются в список List<String> fatalErrors
    private void validateDiplomaIssuance(Table dip_issuance_t, final List<String> fatalErrors, final List<String> dipIssuanceIds, final List<String> diplomaIds, final List<String> allIssuanceIds)
    {

        final MutableBoolean hasNotHexId = new MutableBoolean(false);
        final List<String> replacedIssuanceList = new ArrayList<>();
        final Map<String, String> issuanceDiplomaMap = fetchMapFromTable(dip_issuance_t, DIP_ISSUANCE_ID_COLUMN, DIP_ISSUANCE_OBJECT_ID_COLUMN);

        execute(dip_issuance_t, 500, "Валидация таблицы " + DIP_ISSUANCE_TABLE, rows -> {
            List<Long> localIds = new ArrayList<>();
            for (final Map<String, Object> row : rows)
            {
                localIds.add(tryParseHexId(String.valueOf(row.get(DIP_ISSUANCE_ID_COLUMN))));
            }
            //id сущностей, уже сохранных в БД:
            List<Long> dbIds = new DQLSelectBuilder().fromEntity(DiplomaIssuance.class, "d")
                    .column(property("d", DiplomaIssuance.P_ID))
                    .where(in(property("d", DiplomaIssuance.P_ID), localIds))
                    .createStatement(getSession()).list();

            for (final Map<String, Object> row : rows)
            {
                String issuanceId = (String) row.get(DIP_ISSUANCE_ID_COLUMN);
                String replacedIssuanceId = (String) row.get(DIP_ISSUANCE_REPLACED_ISSUANCE_COLUMN);

                //Если запись уже есть в БД, то не валидируем ее
                if (dbIds.contains(tryParseHexId((String) row.get(DIP_ISSUANCE_ID_COLUMN))))
                {
                    if (!StringUtils.isEmpty(issuanceId))
                        dipIssuanceIds.add(issuanceId);
                    if (!StringUtils.isEmpty(replacedIssuanceId))
                        replacedIssuanceList.add(replacedIssuanceId);
                    continue;
                }
                String diplomaId = (String) row.get(DIP_ISSUANCE_OBJECT_ID_COLUMN);

                Date issuanceDate = date(row, DIP_ISSUANCE_DATE_COLUMN, DIP_ISSUANCE_TABLE + "[" + DIP_ISSUANCE_ID_COLUMN + "=" + issuanceId + "]", false);
                //значение id не задано, либо имеет неверные формат (должно быть число в hex-виде).
                if (hasNotHexId.isFalse())
                {
                    hasNotHexId.setValue(hasErrorInRequiredHexField(row, DIP_ISSUANCE_TABLE, DIP_ISSUANCE_ID_COLUMN, false, null, fatalErrors));
                }
                checkDuplicatedIds(row, dipIssuanceIds, DIP_ISSUANCE_TABLE, DIP_ISSUANCE_ID_COLUMN, fatalErrors);

                hasErrorInRequiredHexField(row, DIP_ISSUANCE_TABLE, DIP_ISSUANCE_OBJECT_ID_COLUMN, true, issuanceId, fatalErrors);
                checkElemInCatalog(diplomaId, diplomaIds, DIP_ISSUANCE_TABLE, DIP_ISSUANCE_OBJECT_ID_COLUMN, issuanceId, fatalErrors);

                checkEmptyField(row, DIP_ISSUANCE_TABLE, DIP_ISSUANCE_BLANK_SERIA_COLUMN, issuanceId, fatalErrors);
                checkEmptyField(row, DIP_ISSUANCE_TABLE, DIP_ISSUANCE_BLANK_NUMBER_COLUMN, issuanceId, fatalErrors);
                checkEmptyField(row, DIP_ISSUANCE_TABLE, DIP_ISSUANCE_DATE_COLUMN, issuanceId, fatalErrors);
                checkDate(issuanceDate, DIP_ISSUANCE_TABLE, DIP_ISSUANCE_DATE_COLUMN, issuanceId, true, fatalErrors);
                checkEmptyField(row, DIP_ISSUANCE_TABLE, DIP_ISSUANCE_REG_NUMBER_COLUMN, issuanceId, fatalErrors);
                checkElemInCatalog(replacedIssuanceId, allIssuanceIds, DIP_ISSUANCE_TABLE, DIP_ISSUANCE_REPLACED_ISSUANCE_COLUMN, issuanceId, fatalErrors);

                if (!StringUtils.isEmpty(replacedIssuanceId) && replacedIssuanceId.equals(issuanceId))
                {
                    fatalErrors.add("context: in table 'dip_issuance_t' values of fields 'id' and 'replacedIssuance' are equal. Error with processing: " + DIP_ISSUANCE_TABLE + "[" + issuanceId + "].");
                }
                if (!StringUtils.isEmpty(replacedIssuanceId) && replacedIssuanceList.contains(replacedIssuanceId))
                {
                    fatalErrors.add("context: in table 'dip_issuance_t' field 'replacedIssuance' have the same value. Error with processing: " + DIP_ISSUANCE_TABLE + "[" + issuanceId + "].");
                }
                if (!StringUtils.isEmpty(diplomaId) && !StringUtils.isEmpty(replacedIssuanceId) && !diplomaId.equals(issuanceDiplomaMap.get(replacedIssuanceId)))
                {
                    fatalErrors.add("context: in table 'dip_issuance_t' current issuance and replaced issuance are relative to different diplomas. Error with processing: " + DIP_ISSUANCE_TABLE + "[" + issuanceId + "].");
                }
                if (!StringUtils.isEmpty(replacedIssuanceId))
                {
                    replacedIssuanceList.add(replacedIssuanceId);
                }
            }
        });
    }

    // Проверяет таблицу с приложениями к фактам выдачи документов о полученном образовании на наличие fatal errors, все найденные ошибки сохраняются в список List<String> fatalErrors
    private void validateDiplomaContentIssuance(Table dip_content_issuance_t, final List<String> fatalErrors, final List<String> dipContentIssuanceIds, final List<String> dipIssuanceIds)
    {
        final MutableBoolean hasNotHexId = new MutableBoolean(false);
        execute(dip_content_issuance_t, 500, "Валидация таблицы " + DIP_CONTENT_ISSUANCE_TABLE, rows -> {
            List<Long> localIds = new ArrayList<>();
            for (final Map<String, Object> row : rows)
            {
                localIds.add(tryParseHexId((String) row.get(DIP_CONTENT_ISSUANCE_ID_COLUMN)));
            }
            //id сущностей, уже сохранных в БД:
            List<Long> dbIds = new DQLSelectBuilder().fromEntity(DiplomaContentIssuance.class, "d")
                    .column(property("d", DiplomaContentIssuance.P_ID))
                    .where(in(property("d", DiplomaContentIssuance.P_ID), localIds))
                    .createStatement(getSession()).list();

            for (final Map<String, Object> row : rows)
            {
                String contentIssuanceId = (String) row.get(DIP_CONTENT_ISSUANCE_ID_COLUMN);

                //Если запись уже есть в БД, то не валидируем ее
                if (dbIds.contains(tryParseHexId(String.valueOf(row.get(DIP_CONTENT_ISSUANCE_ID_COLUMN)))))
                {
                    if (!StringUtils.isEmpty(contentIssuanceId))
                        dipContentIssuanceIds.add(contentIssuanceId);
                    continue;
                }
                String issuanceId = (String) row.get(DIP_CONTENT_ISSUANCE_ISSUANCE_ID_COLUMN);
                String duplicate = (String) row.get(DIP_CONTENT_ISSUANCE_DUPLICATE_COLUMN);
                Date duplicateIssuanceDate = date(row, DIP_CONTENT_ISSUANCE_DUPLICATE_ISSUANCE_DATE_ID_COLUMN, DIP_CONTENT_ISSUANCE_TABLE + "[" + DIP_CONTENT_ISSUANCE_ID_COLUMN + "=" + contentIssuanceId + "]", false);
                String duplicateRegistrationNumber = (String) row.get(DIP_CONTENT_ISSUANCE_DUPLICATE_REGISTRATION_NUMBER_COLUMN);
                if (hasNotHexId.isFalse())
                {
                    hasNotHexId.setValue(hasErrorInRequiredHexField(row, DIP_CONTENT_ISSUANCE_TABLE, DIP_CONTENT_ISSUANCE_ID_COLUMN, false, null, fatalErrors));
                }
                checkDuplicatedIds(row, dipContentIssuanceIds, DIP_CONTENT_ISSUANCE_TABLE, DIP_CONTENT_ISSUANCE_ID_COLUMN, fatalErrors);

                hasErrorInRequiredHexField(row, DIP_CONTENT_ISSUANCE_TABLE, DIP_CONTENT_ISSUANCE_ISSUANCE_ID_COLUMN, true, contentIssuanceId, fatalErrors);

                checkElemInCatalog(issuanceId, dipIssuanceIds, DIP_CONTENT_ISSUANCE_TABLE, DIP_CONTENT_ISSUANCE_ISSUANCE_ID_COLUMN, contentIssuanceId, fatalErrors);
                checkEmptyField(row, DIP_CONTENT_ISSUANCE_TABLE, DIP_CONTENT_ISSUANCE_BLANK_SERIA_COLUMN, contentIssuanceId, fatalErrors);
                checkEmptyField(row, DIP_CONTENT_ISSUANCE_TABLE, DIP_CONTENT_ISSUANCE_BLANK_NUMBER_COLUMN, contentIssuanceId, fatalErrors);
                checkEmptyField(row, DIP_CONTENT_ISSUANCE_TABLE, DIP_CONTENT_ISSUANCE_BLANK_NUMBER_COLUMN, contentIssuanceId, fatalErrors);
                checkEmptyField(row, DIP_CONTENT_ISSUANCE_TABLE, DIP_CONTENT_ISSUANCE_DUPLICATE_COLUMN, contentIssuanceId, fatalErrors);

                if (!"0".equals(duplicate) && !"1".equals(duplicate))
                {
                    fatalErrors.add("context: unable to parse value in the field 'duplicate' table 'dip_content_issuance_t'. Error with processing: " + DIP_CONTENT_ISSUANCE_TABLE + "[" + issuanceId + "].");
                }
                if ("1".equals(duplicate))
                {
                    checkEmptyField(row, DIP_CONTENT_ISSUANCE_TABLE, DIP_CONTENT_ISSUANCE_DUPLICATE_REGISTRATION_NUMBER_COLUMN, contentIssuanceId, fatalErrors);
                    checkDate(duplicateIssuanceDate, DIP_CONTENT_ISSUANCE_TABLE, DIP_CONTENT_ISSUANCE_DUPLICATE_ISSUANCE_DATE_ID_COLUMN, contentIssuanceId, true, fatalErrors);
                }
                if ("0".equals(duplicate) &&
                        (duplicateIssuanceDate != null || duplicateRegistrationNumber != null))
                {
                    fatalErrors.add("context: Specified fields 'duplicateIssuanceDate' and/or 'duplicateRegistrationNumber' table 'dip_content_issuance_t' without duplicate. Error with processing: " + DIP_CONTENT_ISSUANCE_TABLE + "[" + issuanceId + "].");
                }

            }
        });
    }


    private static List<String> fetchColumnFromTable(Table table, final String column)
    {
        final List<String> columnList = new ArrayList<>();
        if (table != null)
        {
            execute(table, 500, "Валидация", rows -> {
                for (final Map<String, Object> row : rows)
                {
                    if (!StringUtils.isEmpty((String)row.get(column)))
                    {
                        columnList.add(String.valueOf(row.get(column)));
                    }
                }
            });
        }
        return columnList;
    }

    private static Map<String, String> fetchMapFromTable(Table table, final String key, final String value)
    {
        final Map<String, String> columnMap = new HashMap<>();
        if (table != null)
        {
            execute(table, 500, "Валидация", rows -> {
                for (final Map<String, Object> row : rows)
                {
                    if (!StringUtils.isEmpty((String)row.get(key)))
                    {
                        columnMap.put((String) row.get(key), (String) row.get(value));
                    }
                }
            });
        }
        return columnMap;
    }


    //Validators:

    /**
     * Проверяет, что переданная дата (если она не равна null) больше или равна 1900.
     * Если дата меньше 1900 года, то происходит запись в лог
     *
     * @param date        дата, которую необходимо проверить
     * @param table       название таблицы, из которой взята дата (будет выведено в логе)
     * @param field       поле таблицы, в котором содержиться дата (будет выведено в логе)
     * @param id          идентификатор записи, в которой содержиться дата (будет выведено в логе)
     * @param fatalErrors список со строками, в который сохраняем все ошибки
     */
    private static void checkDate(Date date, String table, String field, String id, boolean required, List<String> fatalErrors)
    {

        if ((date == null && required)
                || date != null && CoreDateUtils.getYear(date) <= 1900)
        {
            fatalErrors.add("Table '" + table + "' must contain correct field '" + field + "'. Error with processing: " + table + "[" + id + "].");
        }
    }

    /**
     * Проверяет, что переданный элемент - elem (если он не null) является числом и при этом больше или равно 0.
     * Если условие не выполненно, то происходит запись в лог.
     *
     * @param elem        проверяемый элемент
     * @param table       название таблицы, из которой взят elem (будет выведено в логе)
     * @param field       поле таблицы, в котором содержиться elem (будет выведено в логе)
     * @param id          идентификатор записи, в которой содержиться elem (будет выведено в логе)
     * @param fatalErrors список со строками, в который сохраняем все ошибки
     */
    private static void checkNumber(String elem, String table, String field, String id, List<String> fatalErrors)
    {
        String positiveElem = (!StringUtils.isEmpty(elem) && elem.startsWith("-")) ? elem.replaceFirst("-", "") : elem;

        if (!StringUtils.isEmpty(elem) && (!StringUtils.isNumeric(positiveElem) || Long.parseLong(elem) < 0))
        {
            fatalErrors.add("Table '" + table + "' must contain correct field '" + field + "'. Error with processing: " + table + "[" + id + "].");
        }
    }

    /**
     * Проверяет, что переденный элемент - elem (если он не null), является идентификатором сущности из другой таблицы/справочника.
     * Если условие не выполненно, то происходит запись в лог
     *
     * @param elem        проверяемый элемент
     * @param columnList  список, полученный из сторонней таблицы, в котором должен содержаться elem
     * @param table       название таблицы, из которой взят elem (будет выведено в логе)
     * @param field       поле таблицы, в котором содержиться elem (будет выведено в логе)
     * @param id          идентификатор записи, в которой содержиться elem (будет выведено в логе)
     * @param fatalErrors список со строками, в который сохраняем все ошибк
     */
    private static void checkElemInCatalog(String elem, List<String> columnList, String table, String field, String id, List<String> fatalErrors)
    {
        if (!StringUtils.isEmpty(elem) && !columnList.contains(elem))
        {
            fatalErrors.add("Table '" + table + "' must contain correct field '" + field + "'. Error with processing: " + table + "[" + id + "].");
        }
    }


    /**
     * Проверяет идентификаторы импортируемых сущностей на дублируемость.
     * Если переданная строка, имеет идентификотор, котороый уже был использован ранее (есть в переданном списке), то происходит запись в лог,
     * иначе, идентифакто добавляется в список.
     *
     * @param row         строка таблицы mdb
     * @param idList      список уже проверенныйх идентифкаторов
     * @param table       название таблицы, из которой взята идентфикатор (будет выведено в логе)
     * @param field       поле таблицы, из которого береться текущий идентфикатор
     * @param fatalErrors список со строками, в который сохраняем все ошибк
     */
    private static void checkDuplicatedIds(Map<String, Object> row, List<String> idList, String table, String field, List<String> fatalErrors)
    {
        if (idList.contains(String.valueOf(row.get(field))))
        {
            fatalErrors.add("Table '" + table + "' must contain unique id " + String.valueOf(row.get(field)));
        }
        else if (!StringUtils.isEmpty((String)row.get(field)))
        {
            idList.add(String.valueOf(row.get(field)));
        }
    }

    /**
     * Проверяет является ли поле переданной записи числом, в формате hex.
     *
     * @param row         запись, поле которой нужно проверить
     * @param table       название таблицы, из которой взято поле (будет выведено в логе)
     * @param field       поле, которое необходимо проверить
     * @param showId      выводить ли в логе идентификатор валидируемой записи
     * @param id          идентификатор валидируемой записи
     * @param fatalErrors список со строками, в который сохраняем все ошибк
     * @return является ли запись числом в hex-виде
     */
    private static boolean hasErrorInRequiredHexField(Map<String, Object> row, String table, String field, boolean showId, String id, List<String> fatalErrors)
    {
        if (null == tryParseHexId(String.valueOf(row.get(field))))
        {
            if (showId)
            {
                fatalErrors.add("context: field '" + field + "' in table '" + table + "' must be valid hex number and not null. Error with processing: " + table + "[" + id + "].");
            }
            else
            {
                fatalErrors.add("context: field '" + field + "' in table '" + table + "' must be valid hex number and not null.");
            }
            return true;
        }
        return false;
    }

    /**
     * Проверяет поле записи на null. Если поле равно null, то сохраняет строку для лога
     *
     * @param row         запись, поле которой нужно проверить
     * @param table       название таблицы, из которой взято поле (будет выведено в логе)
     * @param field       поле, которое необходимо проверить
     * @param id          идентификатор валидируемой записи
     * @param fatalErrors список со строками, в который сохраняем все ошибк
     */
    private static void checkEmptyField(Map<String, Object> row, String table, String field, String id, List<String> fatalErrors)
    {
        if (StringUtils.isEmpty((String)row.get(field)))
        {
            fatalErrors.add("Field value for '" + field + "' must not be null. Error with processing: " + table + "[" + id + "].");
        }
    }

    /**
     * Метод ищет таблицу в mdb файле, если ее нет,
     * то возвращает null и делает запись в лог,
     * иначе возвращает найденную таблицу.
     *
     * @param mdb       mdb файл, в котором происходит поиск таблиц
     * @param tableName название таблицы
     * @return если в базе, есть таблица с переданным названием, то возвращает таблицу, иначе null.
     * @throws IOException
     */
    private Table getExistTable(Database mdb, String tableName, List<String> fatalErrors) throws IOException
    {
        Table table = mdb.getTable(tableName);
        if (null == table)
        {
            fatalErrors.add("Table '" + tableName + "' does not exist");
            return null;
        }
        else
            return table;
    }

}
