/* $Id$ */
package ru.tandemservice.unidip.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;

/**
 * @author azhebko
 * @since 17.11.2014
 */
public class MS_unidip_2x7x0_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
            {
                new ScriptDependency("org.tandemframework", "1.6.16"),
                new ScriptDependency("org.tandemframework.shared", "1.7.0")
            };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        Collection<String> scriptPaths = new HashSet<>(Arrays.asList(
            "unidip/scripts/DipFirstApplicationScript.groovy",
            "unidip/scripts/DipSecondApplicationScript.groovy",
            "unidip/scripts/DipTitleScript.groovy",
            "unidip/scripts/EducationCertificateScript.groovy",
            "unidip/scripts/SpoDipFirstApplicationScript.groovy",
            "unidip/scripts/SpoDipSecondApplicationScript.groovy",
            "unidip/scripts/SpoDipTitleScript.groovy",
            "unidip/scripts/TemplateRevertScrip.groovy",
            "unidip/scripts/TypeAcadCertScript.groovy",
            "unidip/scripts/TypeCertScript.groovy",
            "unidip/scripts/TypeDiplomaScript.groovy"));

        Statement statement = tool.getConnection().createStatement();
        statement.execute("select d.id, s.scriptpath_p from dip_c_doc_template_t d inner join scriptitem_t s on s.id = d.id");
        ResultSet resultSet = statement.getResultSet();

        PreparedStatement preparedStatement = tool.prepareStatement("update scriptitem_t set scriptpath_p = 'unidip/scripts/TemplateRevertScrip.groovy' where id = ?");
        while (resultSet.next())
        {
            Long id = resultSet.getLong(1);
            String scriptPath = resultSet.getString(2);
            if (!scriptPaths.contains(scriptPath))
            {
                preparedStatement.setLong(1, id);
                preparedStatement.execute();
            }
        }
    }
}