package ru.tandemservice.unidip.bso.entity.blank;

import ru.tandemservice.uni.IStorableReport;
import ru.tandemservice.unidip.bso.bo.DiplomaBlankReport.DiplomaBlankReportManager;
import ru.tandemservice.unidip.bso.entity.blank.gen.DipDiplomaBlankReportGen;

/** @see ru.tandemservice.unidip.bso.entity.blank.gen.DipDiplomaBlankReportGen */
public class DipDiplomaBlankReport extends DipDiplomaBlankReportGen implements IStorableReport
{
    public String getFormByTitle()
    {
        return DiplomaBlankReportManager.getReportFormByTitle(this.getFormBy());
    }
}