/* $Id$ */
package ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.ui.DisposalForm;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.DipDiplomaBlankManager;
import ru.tandemservice.unidip.bso.entity.catalog.DipBlankDisposalReason;

/**
 * @author azhebko
 * @since 15.01.2015
 */
@Configuration
public class DipDiplomaBlankDisposalForm extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(selectDS("disposalReasonDS", disposalReasonDSHandler()).addColumn(DipBlankDisposalReason.shortTitle().s()))
            .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler disposalReasonDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), DipBlankDisposalReason.class)
            .where(DipBlankDisposalReason.massDisposition(), DipDiplomaBlankManager.KEY_MASS_ACTION, true)
            .filter(DipBlankDisposalReason.shortTitle())
            .order(DipBlankDisposalReason.shortTitle());
    }
}