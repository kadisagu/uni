/* $Id$ */
package ru.tandemservice.unidip.settings.bo.DipSettings.ui.DiplomaParameters;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.base.entity.IPersistentEmployeePost;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.unidip.settings.entity.DipFormingRegNumberAlgorithm;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 26.05.2015
 */
@Configuration
public class DipSettingsDiplomaParameters extends BusinessComponentManager
{
    public static final String ALGORITHM_REG_NUMBER_DS = "algorithmRegNumberDS";
    public static final String ASSISTANT_DS = "assistantDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ALGORITHM_REG_NUMBER_DS, algorithmRegNumberDS()))
                .addDataSource(selectDS(ASSISTANT_DS, assistantManagerDSHandler()).addColumn(EmployeePost.P_FULL_TITLE))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> algorithmRegNumberDS()
    {
        return new EntityComboDataSourceHandler(getName(), DipFormingRegNumberAlgorithm.class)
                .filter(DipFormingRegNumberAlgorithm.title())
                .order(DipFormingRegNumberAlgorithm.code());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> assistantManagerDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EmployeePost.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                IPersistentEmployeePost head = TopOrgUnit.getInstance().getHead();
                if (null != head)
                    dql.where(ne(property(alias, EmployeePost.id()), value(head.getId())));
            }
        }
                .order(EmployeePost.person().identityCard().lastName())
                .order(EmployeePost.person().identityCard().firstName())
                .order(EmployeePost.person().identityCard().middleName())
                .order(EmployeePost.postType().shortTitle())
                .order(EmployeePost.postRelation().postBoundedWithQGandQL().post().title())
                .order(EmployeePost.orgUnit().fullTitle())

                .filter(EmployeePost.person().identityCard().lastName())
                .filter(EmployeePost.person().identityCard().firstName())
                .filter(EmployeePost.person().identityCard().middleName())
                .filter(EmployeePost.postType().shortTitle())
                .filter(EmployeePost.postRelation().postBoundedWithQGandQL().post().title())
                .filter(EmployeePost.orgUnit().fullTitle())
                .pageable(true);
    }
}
