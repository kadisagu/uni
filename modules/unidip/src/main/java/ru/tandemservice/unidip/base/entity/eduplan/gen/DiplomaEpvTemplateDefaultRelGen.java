package ru.tandemservice.unidip.base.entity.eduplan.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unidip.base.entity.eduplan.DiplomaEpvTemplateDefaultRel;
import ru.tandemservice.unidip.base.entity.eduplan.DiplomaTemplate;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Шаблон заполнения документа об обучении по умолчанию для УП(в)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DiplomaEpvTemplateDefaultRelGen extends EntityBase
 implements INaturalIdentifiable<DiplomaEpvTemplateDefaultRelGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unidip.base.entity.eduplan.DiplomaEpvTemplateDefaultRel";
    public static final String ENTITY_NAME = "diplomaEpvTemplateDefaultRel";
    public static final int VERSION_HASH = -1293237654;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDU_PLAN_VERSION = "eduPlanVersion";
    public static final String L_DIPLOMA_TEMPLATE = "diplomaTemplate";

    private EppEduPlanVersion _eduPlanVersion;     // УП(в)
    private DiplomaTemplate _diplomaTemplate;     // Шаблон

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return УП(в). Свойство не может быть null.
     */
    @NotNull
    public EppEduPlanVersion getEduPlanVersion()
    {
        return _eduPlanVersion;
    }

    /**
     * @param eduPlanVersion УП(в). Свойство не может быть null.
     */
    public void setEduPlanVersion(EppEduPlanVersion eduPlanVersion)
    {
        dirty(_eduPlanVersion, eduPlanVersion);
        _eduPlanVersion = eduPlanVersion;
    }

    /**
     * @return Шаблон. Свойство не может быть null.
     */
    @NotNull
    public DiplomaTemplate getDiplomaTemplate()
    {
        return _diplomaTemplate;
    }

    /**
     * @param diplomaTemplate Шаблон. Свойство не может быть null.
     */
    public void setDiplomaTemplate(DiplomaTemplate diplomaTemplate)
    {
        dirty(_diplomaTemplate, diplomaTemplate);
        _diplomaTemplate = diplomaTemplate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof DiplomaEpvTemplateDefaultRelGen)
        {
            if (withNaturalIdProperties)
            {
                setEduPlanVersion(((DiplomaEpvTemplateDefaultRel)another).getEduPlanVersion());
                setDiplomaTemplate(((DiplomaEpvTemplateDefaultRel)another).getDiplomaTemplate());
            }
        }
    }

    public INaturalId<DiplomaEpvTemplateDefaultRelGen> getNaturalId()
    {
        return new NaturalId(getEduPlanVersion(), getDiplomaTemplate());
    }

    public static class NaturalId extends NaturalIdBase<DiplomaEpvTemplateDefaultRelGen>
    {
        private static final String PROXY_NAME = "DiplomaEpvTemplateDefaultRelNaturalProxy";

        private Long _eduPlanVersion;
        private Long _diplomaTemplate;

        public NaturalId()
        {}

        public NaturalId(EppEduPlanVersion eduPlanVersion, DiplomaTemplate diplomaTemplate)
        {
            _eduPlanVersion = ((IEntity) eduPlanVersion).getId();
            _diplomaTemplate = ((IEntity) diplomaTemplate).getId();
        }

        public Long getEduPlanVersion()
        {
            return _eduPlanVersion;
        }

        public void setEduPlanVersion(Long eduPlanVersion)
        {
            _eduPlanVersion = eduPlanVersion;
        }

        public Long getDiplomaTemplate()
        {
            return _diplomaTemplate;
        }

        public void setDiplomaTemplate(Long diplomaTemplate)
        {
            _diplomaTemplate = diplomaTemplate;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof DiplomaEpvTemplateDefaultRelGen.NaturalId) ) return false;

            DiplomaEpvTemplateDefaultRelGen.NaturalId that = (NaturalId) o;

            if( !equals(getEduPlanVersion(), that.getEduPlanVersion()) ) return false;
            if( !equals(getDiplomaTemplate(), that.getDiplomaTemplate()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEduPlanVersion());
            result = hashCode(result, getDiplomaTemplate());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEduPlanVersion());
            sb.append("/");
            sb.append(getDiplomaTemplate());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DiplomaEpvTemplateDefaultRelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DiplomaEpvTemplateDefaultRel.class;
        }

        public T newInstance()
        {
            return (T) new DiplomaEpvTemplateDefaultRel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "eduPlanVersion":
                    return obj.getEduPlanVersion();
                case "diplomaTemplate":
                    return obj.getDiplomaTemplate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "eduPlanVersion":
                    obj.setEduPlanVersion((EppEduPlanVersion) value);
                    return;
                case "diplomaTemplate":
                    obj.setDiplomaTemplate((DiplomaTemplate) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "eduPlanVersion":
                        return true;
                case "diplomaTemplate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "eduPlanVersion":
                    return true;
                case "diplomaTemplate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "eduPlanVersion":
                    return EppEduPlanVersion.class;
                case "diplomaTemplate":
                    return DiplomaTemplate.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DiplomaEpvTemplateDefaultRel> _dslPath = new Path<DiplomaEpvTemplateDefaultRel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DiplomaEpvTemplateDefaultRel");
    }
            

    /**
     * @return УП(в). Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.eduplan.DiplomaEpvTemplateDefaultRel#getEduPlanVersion()
     */
    public static EppEduPlanVersion.Path<EppEduPlanVersion> eduPlanVersion()
    {
        return _dslPath.eduPlanVersion();
    }

    /**
     * @return Шаблон. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.eduplan.DiplomaEpvTemplateDefaultRel#getDiplomaTemplate()
     */
    public static DiplomaTemplate.Path<DiplomaTemplate> diplomaTemplate()
    {
        return _dslPath.diplomaTemplate();
    }

    public static class Path<E extends DiplomaEpvTemplateDefaultRel> extends EntityPath<E>
    {
        private EppEduPlanVersion.Path<EppEduPlanVersion> _eduPlanVersion;
        private DiplomaTemplate.Path<DiplomaTemplate> _diplomaTemplate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return УП(в). Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.eduplan.DiplomaEpvTemplateDefaultRel#getEduPlanVersion()
     */
        public EppEduPlanVersion.Path<EppEduPlanVersion> eduPlanVersion()
        {
            if(_eduPlanVersion == null )
                _eduPlanVersion = new EppEduPlanVersion.Path<EppEduPlanVersion>(L_EDU_PLAN_VERSION, this);
            return _eduPlanVersion;
        }

    /**
     * @return Шаблон. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.eduplan.DiplomaEpvTemplateDefaultRel#getDiplomaTemplate()
     */
        public DiplomaTemplate.Path<DiplomaTemplate> diplomaTemplate()
        {
            if(_diplomaTemplate == null )
                _diplomaTemplate = new DiplomaTemplate.Path<DiplomaTemplate>(L_DIPLOMA_TEMPLATE, this);
            return _diplomaTemplate;
        }

        public Class getEntityClass()
        {
            return DiplomaEpvTemplateDefaultRel.class;
        }

        public String getEntityName()
        {
            return "diplomaEpvTemplateDefaultRel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
