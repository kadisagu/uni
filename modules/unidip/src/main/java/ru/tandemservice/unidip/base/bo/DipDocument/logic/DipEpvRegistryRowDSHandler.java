/* $Id$ */
package ru.tandemservice.unidip.base.bo.DipDocument.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
/**
 * @author nvankov
 * @since 11/21/14
 */
public class DipEpvRegistryRowDSHandler extends DefaultComboDataSourceHandler
{
    public static final String EDU_PLAN_VERSION = "eduPlanVersion";
    public static final String REG_STRUCTURE_CODES = "regStructureCodes";

    public DipEpvRegistryRowDSHandler(String ownerId)
    {
        super(ownerId, EppEpvRegistryRow.class, EppEpvRegistryRow.title());
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {
        super.prepareConditions(ep);

        Long eduPlanVersionId = ep.context.getNotNull(EDU_PLAN_VERSION);
        List<String> regStructureCodes = ep.context.get(REG_STRUCTURE_CODES);

        ep.dqlBuilder.where(eq(property("e", EppEpvRegistryRow.owner().eduPlanVersion().id()), value(eduPlanVersionId)));
        FilterUtils.applySelectFilter(ep.dqlBuilder, "e", EppEpvRegistryRow.registryElementType().code(), regStructureCodes);
    }
}
