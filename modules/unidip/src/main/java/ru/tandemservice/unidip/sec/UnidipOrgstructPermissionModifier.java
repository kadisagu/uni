package ru.tandemservice.unidip.sec;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.ISecurityConfigMetaMapModifier;
import org.tandemframework.sec.meta.*;
import org.tandemframework.shared.commonbase.sec.PermissionMetaUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaObject;
import ru.tandemservice.unidip.base.entity.eduplan.DiplomaTemplate;

import java.util.Map;

/**
 * @author iolshvang
 * @since 03.11.2011
 */
public class UnidipOrgstructPermissionModifier implements ISecurityConfigMetaMapModifier
{
    @Override
    public void modify(final Map<String, SecurityConfigMeta> securityConfigMetaMap)
    {
        final SecurityConfigMeta config = new SecurityConfigMeta();
        config.setModuleName("unidip");
        config.setName("unidip-orgunit-sec-config");
        config.setTitle("");

        final ModuleLocalGroupMeta mlgUnidip = PermissionMetaUtil.createModuleLocalGroup(config, "unidipLocal", "Модуль «Дипломирование»");
        final ClassGroupMeta lcgDiploma = PermissionMetaUtil.createClassGroup(mlgUnidip, "diplomaObjectLocalClass", "Объект «Документ об обучении»", DiplomaObject.class.getName());
	    final ClassGroupMeta lcgTemplate = PermissionMetaUtil.createClassGroup(mlgUnidip, "diplomaTemplateLocalClass", "Объект «Шаблон заполнения документа об обучении»", DiplomaTemplate.class.getName());

	    PermissionMetaUtil.createGroupRelation(config, "unidipDocumentPG", lcgDiploma.getName());
	    PermissionMetaUtil.createGroupRelation(config, "unidipTemplatePG", lcgTemplate.getName());

        // закладки в подразделениях
        ModuleGlobalGroupMeta mggOrgstruct = PermissionMetaUtil.createModuleGlobalGroup(config, "orgstructModuleGlobalGroup", "Модуль «Орг. структура»");
        ModuleLocalGroupMeta mlgOrgstruct = PermissionMetaUtil.createModuleLocalGroup(config, "orgstructLocalGroup", "Модуль «Орг. структура»");


        for (final OrgUnitType description : DataAccessServices.dao().getList(OrgUnitType.class, OrgUnitType.P_TITLE))
        {
            final String code = description.getCode();
            String ou = "orgUnit_";

            ClassGroupMeta gcg = PermissionMetaUtil.createClassGroup(mggOrgstruct, code + "PermissionClassGroup", "Объект «" + description.getTitle() + "»", OrgUnit.class.getName());
            ClassGroupMeta lcg = PermissionMetaUtil.createClassGroup(mlgOrgstruct, code + "LocalClassGroup", "Объект «" + description.getTitle() + "»", OrgUnit.class.getName());

            // "Вкладка «Дипломирование»"
            PermissionGroupMeta pgDipTab = PermissionMetaUtil.createPermissionGroup(config, code + "DipTabPermissionGroup", "Вкладка «Дипломирование»");
            PermissionMetaUtil.createGroupRelation(config, pgDipTab.getName(), gcg.getName());
            PermissionMetaUtil.createGroupRelation(config, pgDipTab.getName(), lcg.getName());
            PermissionMetaUtil.createPermission(pgDipTab, ou + "viewDipTab_" + code, "Просмотр");

            // "Подвкладка «Бланки»"
            PermissionGroupMeta pgBlanks = PermissionMetaUtil.createPermissionGroup(pgDipTab, code + "DipBlanksTabPG", "Подвкладка «Бланки»");
            PermissionMetaUtil.createPermission(pgBlanks, ou + "viewDipBlanksTab_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(pgBlanks, ou + "addDiplomaBlanks_diplomaBlankList_" + code, "Добавление бланков");
            PermissionMetaUtil.createPermission(pgBlanks, ou + "disposeBlanks_diplomaBlankList_" + code, "Списать бланки");
            PermissionMetaUtil.createPermission(pgBlanks, ou + "changeStorageLocation_diplomaBlankList_" + code, "Сменить место хранения");
            PermissionMetaUtil.createPermission(pgBlanks, ou + "printDisposalAct_diplomaBlankList_" + code, "Печатать акт списания");
            PermissionMetaUtil.createPermission(pgBlanks, ou + "deleteDipBlank_diplomaBlankList_" + code, "Удаление бланка");

            // "Подвкладка «Дипломы»"
            PermissionGroupMeta pgDiplomas = PermissionMetaUtil.createPermissionGroup(pgDipTab, code + "DipDiplomasTabPG", "Подвкладка «Дипломы»");
            PermissionMetaUtil.createPermission(pgDiplomas, ou + "viewStudentDiplomaTab_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(pgDiplomas, ou + "generateStudentDiplomas_" + code, "Массовое формирование дипломов по шаблону");
            PermissionMetaUtil.createPermission(pgDiplomas, ou + "generateStudentDiplomaTitles_" + code, "Массовое формирование титулов дипломов");
            PermissionMetaUtil.createPermission(pgDiplomas, ou + "fillByTemplate_" + code, "Массовое заполнение дипломов по шаблону");



            /*
            * permission-group name="studentDipdocTabPG" title="Вкладка «Документы»">
            <permission name="viewStudentDipDocumentTab" title="Просмотр списка документов"/>
	           <permission name="addDipDocument" title="Добавление документа"/>
	           <permission name="editDipDocumentFromList" title="Редактирование документа"/>
	           <permission name="deleteDipDocumentFromList" title="Удаление документа"/>
	           <permission name="printDipDocumentFromList" title="Печать документа"/>
        </permission-group>
            * */
        }

        securityConfigMetaMap.put(config.getName(), config);
    }

}
