/* $Id$ */
package ru.tandemservice.unidip.bso.bo.DiplomaBlankReport.ui.Add;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.common.IIdentifiable;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unidip.bso.bo.DiplomaBlankReport.DiplomaBlankReportManager;
import ru.tandemservice.unidip.bso.bo.DiplomaBlankReport.ui.Pub.DiplomaBlankReportPub;
import ru.tandemservice.unidip.bso.entity.catalog.DipBlankType;

import java.util.Collection;

/**
 * @author azhebko
 * @since 22.01.2015
 */
public class DiplomaBlankReportAddUI extends UIPresenter
{
    public void onClickApply()
    {
        Long reportId = DiplomaBlankReportManager.instance().dao().createDiplomaBlankReport(this.getSettings().<IIdentifiable>get("formBy").getId(), this.getSettings().<Collection<DipBlankType>>get("blankType"), this.getSettings().<Collection<OrgUnit>>get("storageLocation"));
        this.deactivate();
        this.getActivationBuilder().asDesktopRoot(DiplomaBlankReportPub.class).parameter(IUIPresenter.PUBLISHER_ID, reportId).activate();
    }
}