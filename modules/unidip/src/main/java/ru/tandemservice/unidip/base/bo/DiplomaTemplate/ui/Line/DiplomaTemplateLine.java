/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unidip.base.bo.DiplomaTemplate.ui.Line;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.IPresenterExtPointBuilder;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.formatter.LongAsDoubleFormatter;
import org.tandemframework.core.view.formatter.RowCollectionFormatter;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import ru.tandemservice.unidip.base.bo.DiplomaTemplate.logic.EppRegistryTemplateRowHandler;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow;
import ru.tandemservice.uniepp.IUniEppDefines;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;

import java.util.List;

/**
 * @author iolshvang
 * @since 06.08.11 15:37
 */
@Configuration
public class DiplomaTemplateLine extends BusinessComponentManager
{
    public static final String BLOCK_DS = "blockDS";
    public static final String THE_OTHERS_DS = "theOtherDS";

	private static final LongAsDoubleFormatter longAsDouble2DigitsFormatter = new LongAsDoubleFormatter(2);

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        final IPresenterExtPointBuilder extPointBuilder = presenterExtPointBuilder();
        for (Class rowClass : DiplomaContentRow.DIPLOMA_BLOCK_CLASSES.get().keySet()){
            extPointBuilder.addDataSource(searchListDS(rowClass.getSimpleName(), blockDS(), blockDSHandler()));
        }
        extPointBuilder.addDataSource(searchListDS(THE_OTHERS_DS, theOtherDS(), regRowDSHandler()));
        return extPointBuilder.create();
    }

    @Bean
    public ColumnListExtPoint blockDS()
    {
		return columnListExtPointBuilder(BLOCK_DS)
                .addColumn(textColumn("index", DiplomaContentRow.epvRegistryRow().storedIndex()).width("100px"))
                .addColumn(textColumn("discipline", DiplomaContentRow.title()))
                .addColumn(textColumn("term", DiplomaContentRow.termAsString()).width("130px"))
				.addColumn(textColumn("audLoad", DiplomaContentRow.audLoadAsLong()).formatter(longAsDouble2DigitsFormatter).width("130px"))
                .addColumn(textColumn("load", DiplomaContentRow.commonLoad()).formatter(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS).width("130px"))
                .addColumn(actionColumn("priorityUp", CommonDefines.ICON_UP, "onClickUp"))
                .addColumn(actionColumn("priorityDown", CommonDefines.ICON_DOWN, "onClickDown"))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, "onClickEditRow").permissionKey("editDiplomaTemplateLine"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER, alert("delete.alert", DiplomaContentRow.title())).permissionKey("deleteDiplomaTemplateLine"))
                .create();
    }

    @Bean
    public ColumnListExtPoint theOtherDS()
    {
        return columnListExtPointBuilder(THE_OTHERS_DS)
                .addColumn(textColumn("index", EppEpvRegistryRow.storedIndex()))
                .addColumn(textColumn("title", EppEpvRegistryRow.title()))
                .addColumn(publisherColumn("number", EppEpvRegistryRow.registryElement().number())
                        .publisherLinkResolver(new IPublisherLinkResolver()
                        {
                            @Override
                            public Object getParameters(IEntity entity)
                            {
                                return new ParametersMap()
                                        .add(PublisherActivator.PUBLISHER_ID_KEY, ((EppEpvRegistryRow)((DataWrapper) entity).getWrapped()).getRegistryElement().getId());
                            }

                            @Override
                            public String getComponentName(IEntity iEntity)
                            {
                                return IUniEppDefines.EPP_DISCIPLINE_REGISTRY_PUB;
                            }
                        })
                        .create())
                .addColumn(textColumn(EppRegistryTemplateRowHandler.CONTROL_FORM_COLUMN, EppRegistryTemplateRowHandler.CONTROL_FORM_COLUMN).formatter(RowCollectionFormatter.INSTANCE))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> blockDSHandler()
    {
        return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                // Скрестили ежа с носорогом. А, нет. Это я про идеологию передачи списка объектов в хэндлер, если надо таки использовать хендлеры caf...
                final List<DiplomaContentRow> rows = context.get(UIDefines.COMBO_OBJECT_LIST);
                final DSOutput output = ListOutputBuilder.get(input, rows).pageable(false).build();
                output.setCountRecord(Math.max(output.getTotalSize(), 1));
                return output;
            }
        };
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> regRowDSHandler()
    {
        return new EppRegistryTemplateRowHandler(getName());
    }
}