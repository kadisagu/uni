/* $Id$ */
package ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.ui.StorageLocationChangeForm;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

/**
 * @author azhebko
 * @since 20.01.2015
 */
@Configuration
public class DipDiplomaBlankStorageLocationChangeForm extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(selectDS("storageLocationDS", storageLocationDSHandler()).addColumn(OrgUnit.fullTitle().s()))
            .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler storageLocationDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), OrgUnit.class)
            .where(OrgUnit.archival(), Boolean.FALSE)
            .filter(OrgUnit.fullTitle())
            .order(OrgUnit.fullTitle());
    }
}