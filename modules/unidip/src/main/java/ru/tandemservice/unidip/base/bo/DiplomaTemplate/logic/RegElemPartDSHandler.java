/* $Id$ */
package ru.tandemservice.unidip.base.bo.DiplomaTemplate.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import java.util.List;

/**
 * @author Andrey Avetisov
 * @since 21.07.2014
 */
public class RegElemPartDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String REG_ELEM_PART_LIST = "regElemPartList";

    public RegElemPartDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        List<DataWrapper> regElemPartList = context.get(REG_ELEM_PART_LIST);
        DSOutput output = ListOutputBuilder.get(input, regElemPartList).build();
        output.setCountRecord(Math.max(output.getTotalSize(), 1));
        return output;
    }
}
