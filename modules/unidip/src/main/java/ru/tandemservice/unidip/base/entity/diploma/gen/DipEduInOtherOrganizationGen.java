package ru.tandemservice.unidip.base.entity.diploma.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unidip.base.entity.diploma.DipAdditionalInformation;
import ru.tandemservice.unidip.base.entity.diploma.DipEduInOtherOrganization;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Обучение в другой организации
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DipEduInOtherOrganizationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unidip.base.entity.diploma.DipEduInOtherOrganization";
    public static final String ENTITY_NAME = "dipEduInOtherOrganization";
    public static final int VERSION_HASH = -996339578;
    private static IEntityMeta ENTITY_META;

    public static final String L_DIP_ADDITIONAL_INFORMATION = "dipAdditionalInformation";
    public static final String P_TOTAL_CREDITS_AS_LONG = "totalCreditsAsLong";
    public static final String P_TOTAL_WEEKS_AS_LONG = "totalWeeksAsLong";
    public static final String P_ROW = "row";

    private DipAdditionalInformation _dipAdditionalInformation;     // Дополнительные сведения в документе об обучении
    private Long _totalCreditsAsLong;     // Всего зачетных единиц (в сотых долях)
    private Long _totalWeeksAsLong;     // Всего недель (в сотых долях)
    private String _row;     // Образовательная организация

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дополнительные сведения в документе об обучении. Свойство не может быть null.
     */
    @NotNull
    public DipAdditionalInformation getDipAdditionalInformation()
    {
        return _dipAdditionalInformation;
    }

    /**
     * @param dipAdditionalInformation Дополнительные сведения в документе об обучении. Свойство не может быть null.
     */
    public void setDipAdditionalInformation(DipAdditionalInformation dipAdditionalInformation)
    {
        dirty(_dipAdditionalInformation, dipAdditionalInformation);
        _dipAdditionalInformation = dipAdditionalInformation;
    }

    /**
     * Всего зачетных единиц.
     *
     * @return Всего зачетных единиц (в сотых долях).
     */
    public Long getTotalCreditsAsLong()
    {
        return _totalCreditsAsLong;
    }

    /**
     * @param totalCreditsAsLong Всего зачетных единиц (в сотых долях).
     */
    public void setTotalCreditsAsLong(Long totalCreditsAsLong)
    {
        dirty(_totalCreditsAsLong, totalCreditsAsLong);
        _totalCreditsAsLong = totalCreditsAsLong;
    }

    /**
     * Всего недель.
     *
     * @return Всего недель (в сотых долях).
     */
    public Long getTotalWeeksAsLong()
    {
        return _totalWeeksAsLong;
    }

    /**
     * @param totalWeeksAsLong Всего недель (в сотых долях).
     */
    public void setTotalWeeksAsLong(Long totalWeeksAsLong)
    {
        dirty(_totalWeeksAsLong, totalWeeksAsLong);
        _totalWeeksAsLong = totalWeeksAsLong;
    }

    /**
     * @return Образовательная организация. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getRow()
    {
        return _row;
    }

    /**
     * @param row Образовательная организация. Свойство не может быть null.
     */
    public void setRow(String row)
    {
        dirty(_row, row);
        _row = row;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof DipEduInOtherOrganizationGen)
        {
            setDipAdditionalInformation(((DipEduInOtherOrganization)another).getDipAdditionalInformation());
            setTotalCreditsAsLong(((DipEduInOtherOrganization)another).getTotalCreditsAsLong());
            setTotalWeeksAsLong(((DipEduInOtherOrganization)another).getTotalWeeksAsLong());
            setRow(((DipEduInOtherOrganization)another).getRow());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DipEduInOtherOrganizationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DipEduInOtherOrganization.class;
        }

        public T newInstance()
        {
            return (T) new DipEduInOtherOrganization();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "dipAdditionalInformation":
                    return obj.getDipAdditionalInformation();
                case "totalCreditsAsLong":
                    return obj.getTotalCreditsAsLong();
                case "totalWeeksAsLong":
                    return obj.getTotalWeeksAsLong();
                case "row":
                    return obj.getRow();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "dipAdditionalInformation":
                    obj.setDipAdditionalInformation((DipAdditionalInformation) value);
                    return;
                case "totalCreditsAsLong":
                    obj.setTotalCreditsAsLong((Long) value);
                    return;
                case "totalWeeksAsLong":
                    obj.setTotalWeeksAsLong((Long) value);
                    return;
                case "row":
                    obj.setRow((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "dipAdditionalInformation":
                        return true;
                case "totalCreditsAsLong":
                        return true;
                case "totalWeeksAsLong":
                        return true;
                case "row":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "dipAdditionalInformation":
                    return true;
                case "totalCreditsAsLong":
                    return true;
                case "totalWeeksAsLong":
                    return true;
                case "row":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "dipAdditionalInformation":
                    return DipAdditionalInformation.class;
                case "totalCreditsAsLong":
                    return Long.class;
                case "totalWeeksAsLong":
                    return Long.class;
                case "row":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DipEduInOtherOrganization> _dslPath = new Path<DipEduInOtherOrganization>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DipEduInOtherOrganization");
    }
            

    /**
     * @return Дополнительные сведения в документе об обучении. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DipEduInOtherOrganization#getDipAdditionalInformation()
     */
    public static DipAdditionalInformation.Path<DipAdditionalInformation> dipAdditionalInformation()
    {
        return _dslPath.dipAdditionalInformation();
    }

    /**
     * Всего зачетных единиц.
     *
     * @return Всего зачетных единиц (в сотых долях).
     * @see ru.tandemservice.unidip.base.entity.diploma.DipEduInOtherOrganization#getTotalCreditsAsLong()
     */
    public static PropertyPath<Long> totalCreditsAsLong()
    {
        return _dslPath.totalCreditsAsLong();
    }

    /**
     * Всего недель.
     *
     * @return Всего недель (в сотых долях).
     * @see ru.tandemservice.unidip.base.entity.diploma.DipEduInOtherOrganization#getTotalWeeksAsLong()
     */
    public static PropertyPath<Long> totalWeeksAsLong()
    {
        return _dslPath.totalWeeksAsLong();
    }

    /**
     * @return Образовательная организация. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DipEduInOtherOrganization#getRow()
     */
    public static PropertyPath<String> row()
    {
        return _dslPath.row();
    }

    public static class Path<E extends DipEduInOtherOrganization> extends EntityPath<E>
    {
        private DipAdditionalInformation.Path<DipAdditionalInformation> _dipAdditionalInformation;
        private PropertyPath<Long> _totalCreditsAsLong;
        private PropertyPath<Long> _totalWeeksAsLong;
        private PropertyPath<String> _row;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дополнительные сведения в документе об обучении. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DipEduInOtherOrganization#getDipAdditionalInformation()
     */
        public DipAdditionalInformation.Path<DipAdditionalInformation> dipAdditionalInformation()
        {
            if(_dipAdditionalInformation == null )
                _dipAdditionalInformation = new DipAdditionalInformation.Path<DipAdditionalInformation>(L_DIP_ADDITIONAL_INFORMATION, this);
            return _dipAdditionalInformation;
        }

    /**
     * Всего зачетных единиц.
     *
     * @return Всего зачетных единиц (в сотых долях).
     * @see ru.tandemservice.unidip.base.entity.diploma.DipEduInOtherOrganization#getTotalCreditsAsLong()
     */
        public PropertyPath<Long> totalCreditsAsLong()
        {
            if(_totalCreditsAsLong == null )
                _totalCreditsAsLong = new PropertyPath<Long>(DipEduInOtherOrganizationGen.P_TOTAL_CREDITS_AS_LONG, this);
            return _totalCreditsAsLong;
        }

    /**
     * Всего недель.
     *
     * @return Всего недель (в сотых долях).
     * @see ru.tandemservice.unidip.base.entity.diploma.DipEduInOtherOrganization#getTotalWeeksAsLong()
     */
        public PropertyPath<Long> totalWeeksAsLong()
        {
            if(_totalWeeksAsLong == null )
                _totalWeeksAsLong = new PropertyPath<Long>(DipEduInOtherOrganizationGen.P_TOTAL_WEEKS_AS_LONG, this);
            return _totalWeeksAsLong;
        }

    /**
     * @return Образовательная организация. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DipEduInOtherOrganization#getRow()
     */
        public PropertyPath<String> row()
        {
            if(_row == null )
                _row = new PropertyPath<String>(DipEduInOtherOrganizationGen.P_ROW, this);
            return _row;
        }

        public Class getEntityClass()
        {
            return DipEduInOtherOrganization.class;
        }

        public String getEntityName()
        {
            return "dipEduInOtherOrganization";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
