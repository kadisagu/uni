package ru.tandemservice.unidip.bso.entity.catalog;

import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import ru.tandemservice.unidip.bso.entity.catalog.gen.DipBlankTypeGen;

/** @see ru.tandemservice.unidip.bso.entity.catalog.gen.DipBlankTypeGen */
public class DipBlankType extends DipBlankTypeGen implements IDynamicCatalogItem
{
}