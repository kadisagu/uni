package ru.tandemservice.unidip.base.entity.diploma;

import ru.tandemservice.unidip.base.bo.DipDocument.DipDocumentManager;
import ru.tandemservice.unidip.base.entity.diploma.gen.DiplomaPracticeRowGen;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.codes.EppRegistryStructureCodes;

import java.util.List;

/**
 * Строка блока Практики в дипломе
 */
public class DiplomaPracticeRow extends DiplomaPracticeRowGen
{
    @Override
    protected boolean isLoadInWeeksForOKSO_and_SPO()
    {
        return true;
    }

    @Override
    protected int getBlockPriority()
    {
        return 1;
    }

    @Override
    protected String getPageTitle()
    {
        return "Практики";
    }

    @Override
    public List<String> getRelatedRegistryElementTypeCodes()
    {
		// Здесь могут быть МСРП типа элемента реестра "Практика" и всех подчиненных
        return DipDocumentManager.instance().dao().regElemTypeWithChildrenCodes(EppRegistryStructureCodes.REGISTRY_PRACTICE);
    }

    @Override
    public boolean isAllowFCA(EppFControlActionType fca)
    {
        // Доступность ФИК определяется настройкой "Связь типов контрольных мероприятий с типами элементов УП / РУП" - для практик.
        return fca.isUsedWithPractice();
    }
}