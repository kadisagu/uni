/* $Id$ */
package ru.tandemservice.unidip.bso.bo.DiplomaSendFisFrdoReport.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import ru.tandemservice.uni.base.bo.UniReport.util.UniReportUtils;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unidip.bso.entity.blank.DipDiplomaSendFisFrdoReport;

/**
 * @author Alexey Lopatin
 * @since 01.04.2015
 */
@State(@Bind(key = IUIPresenter.PUBLISHER_ID, binding = "report.id", required = true))
public class DiplomaSendFisFrdoReportPubUI extends UIPresenter
{
    private DipDiplomaSendFisFrdoReport _report = new DipDiplomaSendFisFrdoReport();

    @Override
    public void onComponentRefresh()
    {
        _report = IUniBaseDao.instance.get().getNotNull(getReport().getId());
    }

    public void onClickPrint()
    {
        BusinessComponentUtils.downloadDocument(UniReportUtils.createRenderer(getReport().getId()), true);
    }

    public void onClickDelete()
    {
        IUniBaseDao.instance.get().delete(_report.getId());
        deactivate();
    }

    public DipDiplomaSendFisFrdoReport getReport()
    {
        return _report;
    }

    public void setReport(DipDiplomaSendFisFrdoReport report)
    {
        _report = report;
    }
}
