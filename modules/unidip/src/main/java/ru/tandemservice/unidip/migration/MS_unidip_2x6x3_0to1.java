package ru.tandemservice.unidip.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unidip_2x6x3_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.15"),
                        new ScriptDependency("org.tandemframework.shared", "1.6.3")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность diplomaContentRow

        // создано обязательное свойство number
        if (!tool.columnExists("diploma_content_row", "number_p"))
        {
            // создать колонку
            tool.createColumn("diploma_content_row", new DBColumn("number_p", DBType.INTEGER));

            // задать значение по умолчанию
            int i = 1;
            PreparedStatement update = tool.prepareStatement("update diploma_content_row set number_p=? where id=?");
            Statement stmt = tool.getConnection().createStatement();
            ResultSet rsDisc = stmt.executeQuery("select r.id from diploma_content_row r inner join dip_disc_row p on r.id=p.id order by r.title_p");
            while (rsDisc.next())
            {
                Long id = rsDisc.getLong(1);
                update.setLong(1, i);
                update.setLong(2, id);
                update.executeUpdate();
                i++;
            }
            ResultSet rsOptDisc = stmt.executeQuery("select r.id from diploma_content_row r inner join dip_opt_disc_row p on r.id=p.id order by r.title_p");
            while (rsOptDisc.next())
            {
                Long id = rsOptDisc.getLong(1);
                update.setLong(1, i);
                update.setLong(2, id);
                update.executeUpdate();
                i++;
            }
            ResultSet rsPractice = stmt.executeQuery("select r.id from diploma_content_row r inner join dip_practice_row p on r.id=p.id order by r.title_p");
            while (rsPractice.next())
            {
                Long id = rsPractice.getLong(1);
                update.setLong(1, i);
                update.setLong(2, id);
                update.executeUpdate();
                i++;
            }
            ResultSet rsStateExam = stmt.executeQuery("select r.id from diploma_content_row r inner join dip_state_exam_row p on r.id=p.id order by r.title_p");
            while (rsStateExam.next())
            {
                Long id = rsStateExam.getLong(1);
                update.setLong(1, i);
                update.setLong(2, id);
                update.executeUpdate();
                i++;
            }
            ResultSet rsQualifWork = stmt.executeQuery("select r.id from diploma_content_row r inner join dip_qualif_work_row p on r.id=p.id order by r.title_p");
            while (rsQualifWork.next())
            {
                Long id = rsQualifWork.getLong(1);
                update.setLong(1, i);
                update.setLong(2, id);
                update.executeUpdate();
                i++;
            }
            ResultSet rsCourse = stmt.executeQuery("select r.id from diploma_content_row r inner join dip_c_work_row p on r.id=p.id order by r.title_p");
            while (rsCourse.next())
            {
                Long id = rsCourse.getLong(1);
                update.setLong(1, i);
                update.setLong(2, id);
                update.executeUpdate();
                i++;
            }
            //Если есть строки, которые по какой-то причине не относятся ни к одному из блоков:
            ResultSet rs = stmt.executeQuery("select id from diploma_content_row where number_p=null order by title_p");
            while (rs.next())
            {
                Long id = rs.getLong(1);
                update.setLong(1, i);
                update.setLong(2, id);
                update.executeUpdate();
                i++;
            }

            // сделать колонку NOT NULL
            tool.setColumnNullable("diploma_content_row", "number_p", false);

        }


    }
}