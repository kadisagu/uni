/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unidip.base.bo.DiplomaTemplate.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unidip.base.bo.DipDocument.logic.DiplomaTemplateDSHandler;
import ru.tandemservice.unidip.base.bo.DiplomaTemplate.DiplomaTemplateManager;
import ru.tandemservice.unidip.base.bo.DiplomaTemplate.ui.AddEdit.DiplomaTemplateAddEdit;
import ru.tandemservice.unidip.base.bo.DiplomaTemplate.ui.AddEdit.DiplomaTemplateAddEditUI;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;

import java.util.Map;

/**
 * @author iolshvang
 * @since 13.07.11 18:39
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "eduPlanVersion.id")
})
public class DiplomaTemplateListUI extends UIPresenter
{
    private EppEduPlanVersion _eduPlanVersion = new EppEduPlanVersion();

    @Override
    public void onComponentBindReturnParameters(String childRegionName, Map<String, Object> returnedData)
    {
//        if (Boolean.TRUE.equals(returnedData.get(DipDocumentAddEditUI.DIPLOMA_OBJECT_NEW))) {
//            _uiActivation.asDesktopRoot(DipDocumentPub.class).parameter(UIPresenter.PUBLISHER_ID, returnedData.get(DipDocumentAddEditUI.DIPLOMA_OBJECT_ID)).activate();
//        }
    }

    @Override
    public void onComponentRefresh()
    {
        _eduPlanVersion = DataAccessServices.dao().getNotNull(_eduPlanVersion.getId());
    }

     @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (DiplomaTemplateList.DIP_TEMPLATE_DS.equals(dataSource.getName()))
        {
            dataSource.put(DiplomaTemplateDSHandler.EDUPLAN_VERSION, _eduPlanVersion);
        }
    }

    public void onClickAddTemplate()
    {
        _uiActivation.asRegionDialog(DiplomaTemplateAddEdit.class).parameter(DiplomaTemplateAddEditUI.EDUPLAN_VERSION_ID, _eduPlanVersion.getId()).activate();
    }
    public void onEditEntityFromList()
    {
        _uiActivation.asRegionDialog(DiplomaTemplateAddEdit.class).parameter(DiplomaTemplateAddEditUI.DIP_TEMPLATE_ID, getListenerParameterAsLong()).activate();
    }

    public void onDeleteEntityFromList()
    {
        DiplomaTemplateManager.instance().dao().deleteDiplomaTemplate(getListenerParameterAsLong());
    }

    //Getters & setters

    public EppEduPlanVersion getEduPlanVersion()
    {
        return _eduPlanVersion;
    }

    public void setEduPlanVersion(EppEduPlanVersion eduPlanVersion)
    {
        _eduPlanVersion = eduPlanVersion;
    }
}
