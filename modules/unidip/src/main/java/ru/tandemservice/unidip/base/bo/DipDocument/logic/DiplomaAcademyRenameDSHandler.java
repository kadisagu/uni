/* $Id$ */
package ru.tandemservice.unidip.base.bo.DipDocument.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;

import java.util.List;

/**
 * @author Andrey Avetisov
 * @since 10.07.2014
 */
public class DiplomaAcademyRenameDSHandler  extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String DIP_ACADEMY_RENAME_LIST = "dipAcademyRenameList";

    public DiplomaAcademyRenameDSHandler(String ownerId)
    {
        super(ownerId);
    }
    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        List<DataWrapper> academyRenameList = context.get(DIP_ACADEMY_RENAME_LIST);
        DSOutput output = ListOutputBuilder.get(input, academyRenameList).build();
        output.setCountRecord(Math.max(output.getTotalSize(), 1));
        return output;
    }
}
