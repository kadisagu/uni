/* $Id:$ */
package ru.tandemservice.unidip.base.ext.SystemAction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.IItemListExtensionBuilder;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.SystemActionManager;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.util.SystemActionDefinition;
import ru.tandemservice.unidip.base.ext.SystemAction.ui.Pub.SystemActionPubExt;

/**
 * @author rsizonenko
 * @since 27.02.2015
 */
@Configuration
public class SystemActionExtManager extends BusinessObjectExtensionManager {
    @Autowired
    private SystemActionManager _systemActionManager;

    @Bean
    public ItemListExtension<SystemActionDefinition> buttonListExtension() {
        IItemListExtensionBuilder<SystemActionDefinition> itemListExtension = itemListExtension(_systemActionManager.buttonListExtPoint());
        itemListExtension.add("unidip_transferBlanksWithoutOrder", new SystemActionDefinition("unidip", "transferBlanksWithoutOrder", "onClickTransferBlanksWithoutOrder", SystemActionPubExt.ACTION_PUB_ADDON_NAME));
        itemListExtension.add("unidip_transferBlanksFromFreeState", new SystemActionDefinition("unidip", "transferBlanksFromFreeState", "onClickTransferBlanksFromFreeState", SystemActionPubExt.ACTION_PUB_ADDON_NAME));
        itemListExtension.add("unidip_exportDiploma", new SystemActionDefinition("unidip", "exportDiploma", "onClickExportDiploma", SystemActionPubExt.ACTION_PUB_ADDON_NAME));
        itemListExtension.add("unidip_importDiploma", new SystemActionDefinition("unidip", "importDiploma", "onClickImportDiploma", SystemActionPubExt.ACTION_PUB_ADDON_NAME));
        return itemListExtension.create();
    }
}
