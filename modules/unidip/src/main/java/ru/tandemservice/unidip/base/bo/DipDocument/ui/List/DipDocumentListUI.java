/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unidip.base.bo.DipDocument.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.util.zip.ZipCompressionInterface;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unidip.base.bo.DipDocument.DipDocumentManager;
import ru.tandemservice.unidip.base.bo.DipDocument.logic.DipDocumentStudentDSHandler;
import ru.tandemservice.unidip.base.bo.DipDocument.ui.AddEdit.DipDocumentAddEdit;
import ru.tandemservice.unidip.base.bo.DipDocument.ui.AddEdit.DipDocumentAddEditUI;
import ru.tandemservice.unidip.base.bo.DipDocument.ui.Print.DipDocumentPrint;
import ru.tandemservice.unidip.base.bo.DipDocument.ui.Print.DipDocumentPrintUI;
import ru.tandemservice.unidip.base.bo.DipDocument.ui.Pub.DipDocumentPub;
import ru.tandemservice.unidip.base.entity.catalog.DipDocTemplateCatalog;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaObject;

import java.util.*;

/**
 * @author iolshvang
 * @since 13.07.11 18:39
 */
@State({
               @Bind(key = "publisherId", binding = "studentId")
       })
public class DipDocumentListUI extends UIPresenter
{
    private Long studentId;
    private Student student;

    @Override
    public void onComponentBindReturnParameters(String childRegionName, Map<String, Object> returnedData)
    {
        if (Boolean.TRUE.equals(returnedData.get(DipDocumentAddEditUI.DIPLOMA_OBJECT_NEW)))
        {
            _uiActivation.asDesktopRoot(DipDocumentPub.class).parameter(UIPresenter.PUBLISHER_ID, returnedData.get(DipDocumentAddEditUI.DIPLOMA_OBJECT_ID)).activate();
        }
    }

    @Override
    public void onComponentRefresh()
    {
        student = DataAccessServices.dao().getNotNull(studentId);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
		switch (dataSource.getName())
		{
			case DipDocumentList.DIP_DOCUMENT_DS:
				dataSource.put(DipDocumentStudentDSHandler.STUDENT, student);
				break;
		}
    }

    public void onClickAddDocument()
    {
        _uiActivation.asRegionDialog(DipDocumentAddEdit.class).parameter(DipDocumentAddEditUI.STUDENT_ID, student.getId()).activate();
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asRegionDialog(DipDocumentAddEdit.class).parameter(DipDocumentAddEditUI.DIP_DOCUMENT_ID, getListenerParameterAsLong()).activate();
    }

    public void onDeleteEntityFromList()
    {
        DipDocumentManager.instance().dao().deleteDipDocument(getListenerParameterAsLong());
    }

    public void onPrintEntityFromList()
    {
        DiplomaObject dipDocument = DataAccessServices.dao().getNotNull(getListenerParameterAsLong());
        List<DipDocTemplateCatalog> templateList = DipDocumentManager.instance().dao().getTemplateList(dipDocument.getContent().getType().getId());

        // Если шаблона нет, то выводим сообщение
        if (templateList.size() == 0)
            throw new ApplicationException("Для данного типа документа отсутствует печатный шаблон.");

        //Если есть только один шаблон, то распечатываем его
        if (templateList.size() == 1)
        {
            byte[] unzipUserTemplate = null;
			final DipDocTemplateCatalog singleTemplate = templateList.get(0);
			if (singleTemplate.getUserTemplate() != null)
                unzipUserTemplate = ZipCompressionInterface.INSTANCE.decompress(singleTemplate.getUserTemplate());
            CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(singleTemplate,
                                                                              IScriptExecutor.TEMPLATE_VARIABLE, unzipUserTemplate,
                                                                              "diplomaObjectId", dipDocument.getId());
        }
        //иначе открываем окно выбора шаблона
        else
        {
            _uiActivation.asRegionDialog(DipDocumentPrint.class)
                    .parameter(DipDocumentPrintUI.DIP_DOCUMENT_ID, dipDocument.getId())
                    .activate();
        }
    }

    //Getters & setters

    public Student getStudent()
    {
        return student;
    }

    public void setStudent(Student student)
    {
        this.student = student;
    }

    public Long getStudentId()
    {
        return studentId;
    }

    public void setStudentId(Long studentId)
    {
        this.studentId = studentId;
    }
}
