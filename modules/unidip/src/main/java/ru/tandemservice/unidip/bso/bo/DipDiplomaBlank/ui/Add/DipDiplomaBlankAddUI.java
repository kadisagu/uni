/* $Id:$ */
package ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.ui.Add;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import org.tandemframework.tapsupport.component.selection.SingleSelectTextModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.DipDiplomaBlankManager;
import ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.util.BlankWrapperListEditor;
import ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank;

import java.util.Collection;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 25.12.2014
 */
@State({@Bind(key = UIPresenter.PUBLISHER_ID, binding = "orgUnitId")})
public class DipDiplomaBlankAddUI extends UIPresenter
{
    private String _printingOffice;
    private String _invoice;

    private Long _orgUnitId;
    private OrgUnit _orgUnit;

    @Override
    public void onComponentRefresh()
    {
        if (null != _orgUnitId)
            _orgUnit = DataAccessServices.dao().getNotNull(_orgUnitId);
    }

    private final BlankWrapperListEditor _editor = new BlankWrapperListEditor();
    private final ISelectModel _printingOffices =  new SingleSelectTextModel()
    {
        @Override
        public ListResult findValues(final String filter)
        {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DipDiplomaBlank.class, "d")
                .column(property("d", DipDiplomaBlank.printingOffice()))
                .distinct()
                .where(likeUpper(property("d", DipDiplomaBlank.printingOffice()), value(CoreStringUtils.escapeLike(filter))))
                .order(property("d", DipDiplomaBlank.printingOffice()));

            int count = IUniBaseDao.instance.get().getCount(builder);
            if (count > UniDao.MAX_ROWS)
                builder.top(UniDao.MAX_ROWS);

            return new ListResult<>(IUniBaseDao.instance.get().getList(builder), count);
        }
    };

    public void onClickAddRow() { _editor.addRow(getOrgUnit()); }

    public void onClickSaveRow() { _editor.saveRow(); }

    public void onClickEditRow() { _editor.editRow(this.getListenerParameterAsLong()); }

    public void onClickCancelEdit() { _editor.cancelEdit(); }

    public void onClickDeleteRow() { _editor.deleteRow((this.getListenerParameterAsLong())); }

    public void onClickApply()
    {
        Collection<DipBlankRowWrapper> rows = getEditor().getRows();
        for (DipBlankRowWrapper row : rows)
        {
            row.setPrintingOffice(getPrintingOffice());
            row.setInvoice(getInvoice());
        }

        Integer count = DipDiplomaBlankManager.instance().diplomaBlankDao().saveDiplomaBlanks(rows);
        if (count == null)
            return;

        deactivate();
        _uiSupport.info(CommonBaseStringUtil.numberPostfixCase(count, "Добавлен ", "Добавлено ", "Добавлено ") + CommonBaseStringUtil.numberWithPostfixCase(count, "бланк.", "бланка.", "бланков."));
    }

    public BlankWrapperListEditor getEditor() { return _editor; }
    public ISelectModel getPrintingOffices() { return _printingOffices; }

    public Integer getRowsSize()
    {
        int result = 0;
        for (DipBlankRowWrapper row : _editor.getRows())
        {
            Integer firstNumber = row.getFirstNumber();
            Integer lastNumber = row.getLastNumber();
            int numbersCount = null == firstNumber || null == lastNumber ? 0 : lastNumber - firstNumber + 1;
            result += numbersCount;
        }
        return result;
    }

    public String getTotalBlankNumberTitle()
    {
        return "Общее количество бланков: " + getRowsSize();
    }

    public String getPrintingOffice()
    {
        return _printingOffice;
    }

    public void setPrintingOffice(String printingOffice)
    {
        _printingOffice = printingOffice;
    }

    public String getInvoice()
    {
        return _invoice;
    }

    public void setInvoice(String invoice)
    {
        _invoice = invoice;
    }

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }
}