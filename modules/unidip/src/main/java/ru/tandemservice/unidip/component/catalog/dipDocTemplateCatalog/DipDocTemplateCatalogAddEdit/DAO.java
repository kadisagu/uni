/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unidip.component.catalog.dipDocTemplateCatalog.DipDocTemplateCatalogAddEdit;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultScriptCatalogAddEdit.DefaultScriptCatalogAddEditDAO;
import ru.tandemservice.unidip.base.entity.catalog.DipDocTemplateCatalog;
import ru.tandemservice.unidip.base.entity.catalog.DipDocumentType;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vip_delete
 * @since 13.01.2009
 */
public class DAO extends DefaultScriptCatalogAddEditDAO<DipDocTemplateCatalog, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        List<DipDocumentType> dipDocumentTypeList = new DQLSelectBuilder().fromEntity(DipDocumentType.class, "t").column("t")
                .where(eq(property("t", DipDocumentType.P_FORMING_DOC_ALLOWED), value(true)))
                .createStatement(getSession()).list();

        Collections.sort(dipDocumentTypeList, new Comparator<DipDocumentType>()
        {
            @Override
            public int compare(DipDocumentType o1, DipDocumentType o2)
            {
                if (o1.getParent() == null && o2.getParent() == null)
                    return o1.getTitle().compareToIgnoreCase(o2.getTitle());
                if (o1.getParent() != null && o2.getParent() != null)
                {
                    if (o2.getParent().equals(o1.getParent()))
                        return o1.getTitle().compareToIgnoreCase(o2.getTitle());
                    return o1.getParent().getTitle().compareToIgnoreCase(o2.getParent().getTitle());
                }
                if (o1.getParent() == null)
                    return o1.getTitle().compareToIgnoreCase(o2.getParent().getTitle());
                return o1.getParent().getTitle().compareToIgnoreCase(o2.getTitle());
            }
        });
        model.setDipDocumentTypeList(dipDocumentTypeList);

        List<DipDocTemplateCatalog> templateForCopy = new DQLSelectBuilder().fromEntity(DipDocTemplateCatalog.class, "t")
                .where(ne(property(DipDocTemplateCatalog.code().fromAlias("t")), value(model.getCatalogItem().getCode())))
                .order(property("t", DipDocTemplateCatalog.P_TITLE))
                .createStatement(getSession()).list();

        model.setDipDocTemplateCatalogListForCopy(templateForCopy);
    }

    @Override
    public void update(Model model)
    {
        DipDocTemplateCatalog template = model.getCatalogItem();
        if (model.isAddForm())
        {
            template.setCode(getNewCatalogItemCode(DipDocTemplateCatalog.class));
            template.setCatalogCode(DipDocTemplateCatalog.DEFAULT_CATALOG_CODE);
            if (model.isHasTemplateForCopy())
            {
                DipDocTemplateCatalog copy = model.getDipDocTemplateCatalogItemForCopy();

                template.setScriptPath(copy.getScriptPath());
                template.setTemplatePath(copy.getTemplatePath());
                template.setDocument(copy.getDocument());

                model.setUseUserTemplate(true);

                if (null != copy.getUserScript())
                {
                    model.setUseUserScript(true);
                    model.setUserScript(copy.getUserScript());
                }
            }
            else
            {
                if (null == model.getUserTemplateFile())
                    throw new ApplicationException("Необходимо указать шаблон.");
                else
                {
                    template.setScriptPath(DipDocTemplateCatalog.DEFAULT_SCRIPT_PATH);
                    onSaveUserTemplate(model);
                }
                if (null != model.getUseUserScript() && !model.getUseUserScript())
                {
                    model.setUserScript(null);
                }
            }
        }
        else
        {
            String path = template.getTemplatePath();
            if (null == path || null == Thread.currentThread().getContextClassLoader().getResource(path))
            {
                if (null != model.getUseUserTemplate() && !model.getUseUserTemplate())
                    throw new ApplicationException("Необходимо указать шаблон.");

                if (null != model.getUserTemplateFile())
                    onSaveUserTemplate(model);
            }
        }
        super.update(model);
    }

    private void onSaveUserTemplate(Model model)
    {
        try
        {
            DipDocTemplateCatalog template = model.getCatalogItem();
            byte[] content = IOUtils.toByteArray(model.getUserTemplateFile().getStream());

            if (StringUtils.endsWithIgnoreCase(model.getUserTemplateFile().getFileName(), ".rtf") && !RtfReader.checkRtfSignature(content))
                throw new ApplicationException("Загружаемый файл испорчен либо не является файлом формата RTF.");

            template.setTemplatePath(model.getUserTemplateFile().getFilePath());
            template.setDocument(CommonBaseUtil.buildTemplateDocument(content));
        }
        catch (IOException e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    @Override
    protected boolean isAddingScriptAllowed()
    {
        return true;
    }
}