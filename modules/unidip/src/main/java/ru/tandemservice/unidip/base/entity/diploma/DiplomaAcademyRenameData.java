package ru.tandemservice.unidip.base.entity.diploma;

import ru.tandemservice.uni.entity.orgstruct.AcademyRename;
import ru.tandemservice.unidip.base.entity.diploma.gen.*;

/**
 * Переименование обр. орг. для вывода в дипломе
 */
public class DiplomaAcademyRenameData extends DiplomaAcademyRenameDataGen
{
    public DiplomaAcademyRenameData()
    {
    }

    public DiplomaAcademyRenameData(AcademyRename academyRename, DiplomaContent diplomaContent)
    {
        setAcademyRename(academyRename);
        setDiplomaContent(diplomaContent);
    }
}