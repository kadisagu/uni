/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unidip.base.bo.DipDocument.ui.Line;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.*;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.IPresenterExtPointBuilder;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.formatter.LongAsDoubleFormatter;
import org.tandemframework.shared.commonbase.base.util.SimplePublisherLinkResolver;
import ru.tandemservice.unidip.base.bo.DipDocument.logic.DiplomaStudentEpvSlotDSHandler;
import ru.tandemservice.unidip.base.entity.diploma.*;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

import java.util.List;

/**
 * @author iolshvang
 * @since 06.08.11 15:37
 */
@Configuration
public class DipDocumentLine extends BusinessComponentManager
{
    public static final String THE_OTHERS_DS = "theOtherDS";

	private static final LongAsDoubleFormatter longAsDouble2DigitsFormatter = new LongAsDoubleFormatter(2);

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        final IPresenterExtPointBuilder extPointBuilder = presenterExtPointBuilder();
        for (Class clazz : DiplomaContentRow.DIPLOMA_BLOCK_CLASSES.get().keySet())
        {
            extPointBuilder.addDataSource(searchListDS(clazz.getSimpleName(), blockDS(), blockDSHandler()));
        }
        extPointBuilder.addDataSource(searchListDS(THE_OTHERS_DS, theOtherDS(), theOtherDSHandler()));
        return extPointBuilder.create();
    }

    @Bean
    public ColumnListExtPoint blockDS()
    {
        return columnListExtPointBuilder("blockDS")
                .addColumn(textColumn("index", DiplomaContentRow.epvRegistryRow().storedIndex()).width("100px"))
                .addColumn(textColumn("discipline", DiplomaContentRow.titleWithTheme()))
                .addColumn(textColumn("term", DiplomaContentRow.termAsString()).width("90px"))
                .addColumn(textColumn("contentListNumber", DiplomaContentRow.contentListNumber()).width("90px"))
				.addColumn(textColumn("audLoad", DiplomaContentRow.audLoadAsLong()).formatter(longAsDouble2DigitsFormatter).width("130px"))
                .addColumn(textColumn("load", DiplomaContentRow.commonLoad()).width("130px").formatter(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS))
                .addColumn(textColumn("mark", DiplomaContentRow.mark()).width("120px"))
                .addColumn(actionColumn("priorityUp", CommonDefines.ICON_UP, "onClickUp"))
                .addColumn(actionColumn("priorityDown", CommonDefines.ICON_DOWN, "onClickDown"))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, "onEditRow").permissionKey("editDiplomaLine").disabled("ui:existExcludeExtract"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER, alert("delete.alert", DiplomaContentRow.titleWithTheme())).permissionKey("deleteDiplomaLine").disabled("ui:existExcludeExtract"))
                .create();
    }

    @Bean
    public ColumnListExtPoint theOtherDS()
    {
        return columnListExtPointBuilder(THE_OTHERS_DS)
                .addColumn(textColumn("number", DiplomaStudentEpvSlotDSHandler.NUMBER_COLUMN))
                .addColumn(textColumn("discipline", DiplomaStudentEpvSlotDSHandler.OTHER_DISCIPLINES_COLUMN))
                .addColumn(publisherColumn("registryNumber", DiplomaStudentEpvSlotDSHandler.REGISTRY_COLUMN).formatter(reg -> "№" + ((EppRegistryElement) reg).getNumber())
                        .publisherLinkResolver(new SimplePublisherLinkResolver(DiplomaStudentEpvSlotDSHandler.REGISTRY_COLUMN + ".id")))
                .addColumn(textColumn("fc", DiplomaStudentEpvSlotDSHandler.CONTROL_FORM_COLUMN).width("100px"))
                .addColumn(textColumn("term", DiplomaStudentEpvSlotDSHandler.GRID_TERM_COLUMN).width("50px"))
                .addColumn(textColumn("year", DiplomaStudentEpvSlotDSHandler.YEAR_COLUMN).width("100px"))
                .addColumn(textColumn("disciplineKind", DiplomaStudentEpvSlotDSHandler.KIND_COLUMN).width("100px"))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler theOtherDSHandler()
    {
        return new DiplomaStudentEpvSlotDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> blockDSHandler()
    {
        return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                // Скрестили ежа с носорогом. А, нет, с тапком.
                final List<DiplomaContentRow> rows = context.get(UIDefines.COMBO_OBJECT_LIST);
                final DSOutput output = ListOutputBuilder.get(input, rows).pageable(false).build();
                output.setCountRecord(Math.max(output.getTotalSize(), 1));
                return output;
            }
        };
    }
}