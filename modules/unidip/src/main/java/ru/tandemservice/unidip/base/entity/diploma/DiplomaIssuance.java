package ru.tandemservice.unidip.base.entity.diploma;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unidip.base.entity.diploma.gen.DiplomaIssuanceGen;

/**
 * Факт выдачи документа об образовании и(или) квалификации
 *
 * Фиксирует факт выдачи документа и соответствующие ему регистрационные данные.
 */
public class DiplomaIssuance extends DiplomaIssuanceGen
{
    @EntityDSLSupport(parts = {P_REGISTRATION_NUMBER, P_ISSUANCE_DATE, P_BLANK_SERIA, P_BLANK_NUMBER})
    public String getFullTitle()
    {
        String seria = getBlankSeria() == null ? "" : " серия " + getBlankSeria();
        String number = getBlankNumber() == null ? "" : " номер " + getBlankNumber();
        return "Рег. №" + getRegistrationNumber() + " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getIssuanceDate()) + seria + number;
    }
}