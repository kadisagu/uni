/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.     .
package ru.tandemservice.unidip.base.bo.DipDocument.ui.OrgUnitStudentTab;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.PageableSearchListDataSource;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.tool.tree.IHierarchyItem;
import org.tandemframework.core.tool.tree.PlaneTree;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.core.view.list.IViewSelectValueStyle;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import org.tandemframework.tapsupport.component.selection.*;
import ru.tandemservice.uni.base.bo.UniEduProgram.UniEduProgramManager;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unidip.base.bo.DipDocument.DipDocumentManager;
import ru.tandemservice.unidip.base.bo.DipDocument.logic.DipDocumentOrgUnitStudentDSHandler;
import ru.tandemservice.unidip.base.bo.DipDocument.ui.DiplomaObjectDataAdd.DipDocumentDiplomaObjectDataAdd;
import ru.tandemservice.unidip.base.bo.DipDocument.ui.DiplomaObjectDataAdd.DipDocumentDiplomaObjectDataAddUI;
import ru.tandemservice.unidip.base.bo.DipDocument.ui.TemplateSelect.DipDocumentTemplateSelect;
import ru.tandemservice.unidip.base.bo.DipDocument.ui.TemplateSelect.DipDocumentTemplateSelectUI;
import ru.tandemservice.unidip.base.entity.catalog.DipDocumentType;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.unidip.base.bo.DipDocument.ui.OrgUnitStudentTab.DipDocumentOrgUnitStudentTab.*;

/**
 * @author iolshvang
 * @since 90.09.11 18:39
 */
@State({@Bind(key = UIPresenter.PUBLISHER_ID, binding = "orgUnitId")})
public class DipDocumentOrgUnitStudentTabUI extends UIPresenter {
    private OrgUnitSecModel _secModel;
    private Long _orgUnitId;
    private OrgUnit _orgUnit;

    private ISelectModel _dipDocTypeModel;

    @Override
    public void onComponentRefresh() {
        _orgUnit = DataAccessServices.dao().getNotNull(_orgUnitId);
        UniEduProgramEducationOrgUnitAddon util = (UniEduProgramEducationOrgUnitAddon) getConfig().getAddon(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME);
        if (util != null) {
            util.configSettings("menuStudentGroupDiplomaList");
            util.configUseFilters(
                    UniEduProgramEducationOrgUnitAddon.Filters.FORMATIVE_ORG_UNIT,
                    UniEduProgramEducationOrgUnitAddon.Filters.TERRITORIAL_ORG_UNIT,
                    UniEduProgramEducationOrgUnitAddon.Filters.EDUCATION_LEVEL_HIGH_SCHOOL,
                    UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_FORM,
                    UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_CONDITION);
        }
        _secModel = new OrgUnitSecModel(_orgUnit);

        _dipDocTypeModel = new DipDocumentTypeSelectModel();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        dataSource.put(DipDocumentOrgUnitStudentDSHandler.ORG_UNIT_ID, getOrgUnitId());
        dataSource.put(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME, getConfig().getAddon(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME));
        dataSource.put(DipDocumentOrgUnitStudentDSHandler.COURSE, _uiSettings.get(COURSE_PARAM));

        if (DIP_STUDENT_DS.equals(dataSource.getName())) {
            dataSource.put(DipDocumentOrgUnitStudentDSHandler.LAST_NAME, _uiSettings.get(LAST_NAME_PARAM));
            dataSource.put(DipDocumentOrgUnitStudentDSHandler.STUDENT_STATUS, _uiSettings.get(STUDENT_STATUS_PARAM));
            dataSource.put(DipDocumentOrgUnitStudentDSHandler.GROUP, _uiSettings.get(GROUP_PARAM));

            dataSource.put(DipDocumentOrgUnitStudentDSHandler.DOCUMENT_TYPE, _uiSettings.get(DOCUMENT_TYPE_PARAM));

            dataSource.put(DipDocumentOrgUnitStudentDSHandler.ISSUANCE_EMPTY, isIssuanceEmpty());
            dataSource.put(DipDocumentOrgUnitStudentDSHandler.ISSUE_DATE_FROM, _uiSettings.get(ISSUE_DATE_FROM_PARAM));
            dataSource.put(DipDocumentOrgUnitStudentDSHandler.ISSUE_DATE_TO, _uiSettings.get(ISSUE_DATE_TO_PARAM));
            dataSource.put(DipDocumentOrgUnitStudentDSHandler.BLANK_SERIA, createParam(SERIA_PARAM));
            dataSource.put(DipDocumentOrgUnitStudentDSHandler.BLANK_NUMBER, createParam(NUMBER_PARAM));

            dataSource.put(DipDocumentOrgUnitStudentDSHandler.DIPLOMA_CONTENT_ISSUANCE, TwinComboDataSourceHandler.getSelectedValue(_uiSettings.get(DIPLOMA_CONTENT_ISSUANCE_PARAM)));

            dataSource.put(DipDocumentOrgUnitStudentDSHandler.STATE_COMMISSION_DATE_EMPTY, isStateCommissionDateEmpty());
            dataSource.put(DipDocumentOrgUnitStudentDSHandler.STATE_COMMISSION_DATE_FROM, _uiSettings.get(STATE_COMMISSION_DATE_FROM_PARAM));
            dataSource.put(DipDocumentOrgUnitStudentDSHandler.STATE_COMMISSION_DATE_TO, _uiSettings.get(STATE_COMMISSION_DATE_TO_PARAM));
            dataSource.put(DipDocumentOrgUnitStudentDSHandler.STATE_COMMISSION_PROTOCOL_NUMBER, createParam(STATE_COMMISSION_PROTOCOL_NUMBER_PARAM));
            dataSource.put(DipDocumentOrgUnitStudentDSHandler.STATE_COMMISSION_CHAIR_FIO, createParam(STATE_COMMISSION_CHAIR_FIO_PARAM));
        }
    }

    public void onClickGenerateDocuments() {
        final IEventServiceLock eventLock = CoreServices.eventService().lock();
        try {
            Collection<IEntity> selected = getStudentSelected();
            _uiActivation.asRegionDialog(DipDocumentDiplomaObjectDataAdd.class)
                    .parameter(DipDocumentDiplomaObjectDataAddUI.PARAM_STUDENT_IDS, UniBaseDao.ids(selected))
                    .parameter(DipDocumentDiplomaObjectDataAddUI.PARAM_ONLY_TITLE, false)
                    .activate();
            selected.clear();
        } finally {
            eventLock.release();
        }
    }

    public void onClickGenerateDocumentTitles() {
        final IEventServiceLock eventLock = CoreServices.eventService().lock();
        try {
            Collection<IEntity> selected = getStudentSelected();
            _uiActivation.asRegionDialog(DipDocumentDiplomaObjectDataAdd.class)
                    .parameter(DipDocumentDiplomaObjectDataAddUI.PARAM_STUDENT_IDS, UniBaseDao.ids(selected))
                    .parameter(DipDocumentDiplomaObjectDataAddUI.PARAM_ONLY_TITLE, true)
                    .activate();
            selected.clear();
        } finally {
            eventLock.release();
        }
    }

    public void onClickFillByTemplate() {
        final IEventServiceLock eventLock = CoreServices.eventService().lock();
        try {
            Collection<IEntity> selected = getStudentSelected();
            List<Long> documentsIds = DipDocumentManager.instance().dao().getDiplomaWithoutRowIds(UniBaseDao.ids(selected));
            if (CollectionUtils.isNotEmpty(documentsIds))
                _uiActivation.asRegionDialog(DipDocumentTemplateSelect.class).parameter(DipDocumentTemplateSelectUI.PARAM_DOCUMENT_IDS, documentsIds).activate();
            else
                ContextLocal.getInfoCollector().add("Все выбранные дипломы уже заполнены строками.");
            selected.clear();
        } finally {
            eventLock.release();
        }
    }

    public void onChangeIssuanceEmpty() {
        if (getValueFromSetting(ISSUANCE_EMPTY_PARAM, false)) {
            _uiSettings.set(SERIA_PARAM + PARAM_ENABLED, false);
            _uiSettings.set(NUMBER_PARAM + PARAM_ENABLED, false);
        }
    }


    @Override
    public void onComponentBindReturnParameters(String childRegionName, Map<String, Object> returnedData) {
        if (UIDefines.DIALOG_REGION_NAME.equals(childRegionName)) {
            Collection<IEntity> selected = ((PageableSearchListDataSource) _uiConfig.getDataSource(DIP_STUDENT_DS)).getOptionColumnSelectedObjects("checkbox");
            selected.clear();
        }
    }


    private Collection<IEntity> getStudentSelected() {
        Collection<IEntity> selected = ((PageableSearchListDataSource) _uiConfig.getDataSource(DIP_STUDENT_DS)).getOptionColumnSelectedObjects("checkbox");
        if (selected.isEmpty())
            throw new ApplicationException("Не выбран ни один студент.");
        return selected;
    }

    //Getters & setters

    public OrgUnit getOrgUnit() {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit) {
        _orgUnit = orgUnit;
    }

    public Long getOrgUnitId() {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId) {
        _orgUnitId = orgUnitId;
    }

    public OrgUnitSecModel getSecModel() {
        return _secModel;
    }

    public void setSecModel(OrgUnitSecModel _secModel) {
        this._secModel = _secModel;
    }


    public ISelectModel getDipDocTypeModel() {
        return _dipDocTypeModel;
    }

    public Boolean isIssuanceEmpty() {
        return getValueFromSetting(ISSUANCE_EMPTY_PARAM, false);
    }

    public Boolean isStateCommissionDateEmpty() {
        return getValueFromSetting(STATE_COMMISSION_DATE_EMPTY_PARAM, false);
    }

    public Boolean isFilterEnabled(String key) {
        return getValueFromSetting(key + PARAM_ENABLED, false);
    }

    public <T> DipDocumentOrgUnitStudentDSHandler.Param<T> createParam(String key) {
        return new DipDocumentOrgUnitStudentDSHandler.Param<>(isFilterEnabled(key), _uiSettings.get(key + PARAM_VALUE));
    }

    public <T> T getValueFromSetting(String key, T valueIfNull) {
        T value = _uiSettings.get(key);
        if (value == null) return valueIfNull;
        else return value;
    }


    @Override
    public void clearSettings() {
        getSettings().clear();
        UniEduProgramEducationOrgUnitAddon util = (UniEduProgramEducationOrgUnitAddon) getConfig().getAddon(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME);
        if (util != null)
            util.clearFilters();
        saveSettings();
    }

    @Override
    public void saveSettings() {
        getSettings().save();
        UniEduProgramEducationOrgUnitAddon util = (UniEduProgramEducationOrgUnitAddon) getConfig().getAddon(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME);
        if (util != null)
            util.saveSettings();
    }


    protected class DipDocumentTypeSelectModel extends CommonMultiSelectModel
    {
        @Override
        public int getLevel(Object value)
        {
            return ((DataWrapper)value).get("level");
        }

        @Override
        protected IListResultBuilder createBuilder(String filter, Set set)
        {
            DQLSelectBuilder builder = new DQLSelectBuilder()
                    .fromEntity(DipDocumentType.class, "type")
                    .column(property("type"))
                    .where(eq(property("type", DipDocumentType.formingDocAllowed()), value(Boolean.TRUE)));

            FilterUtils.applySimpleLikeFilter(builder, "type", DipDocumentType.title(), filter);
            FilterUtils.applySelectFilter(builder, DipDocumentType.id().fromAlias("type"), set);

            List<DipDocumentType> documentTypeList = DataAccessServices.dao().getList(builder);

            Comparator<DipDocumentType> comparator = HierarchyUtil.getDefaultComparator(documentTypeList);
            PlaneTree planeTree = HierarchyUtil.buildTree(documentTypeList, comparator);
            List<IHierarchyItem> hierarchyItems = planeTree.getFlatTreeObjectsHierarchicalSorted(comparator);

            List<DataWrapper> docTypeList = hierarchyItems.stream()
                    .map(item ->
                         {
                             DipDocumentType type = (DipDocumentType) item;
                             DataWrapper wrapper = new DataWrapper(type.getId(), type.getTitle());
                             wrapper.put("level", planeTree.getLevel(type));
                             wrapper.put("formingDocAllowed", type.isFormingDocAllowed());
                             return wrapper;
                         })
                    .collect(Collectors.toList());

            DataWrapper wrapper = new DataWrapper(0L, "Без документа об обучении");
            wrapper.put("level", 0);
            wrapper.put("formingDocAllowed", true);
            docTypeList.add(0, wrapper);

            return new SimpleListResultBuilder<>(docTypeList);
        }

        @Override
        public IViewSelectValueStyle getValueStyle(Object value)
        {
            if (((DataWrapper) value).get("formingDocAllowed"))
                return super.getValueStyle(value);

            //неактивные типы
            DefaultSelectValueStyle valueStyle = new DefaultSelectValueStyle();
            valueStyle.setDisabled(true);
            valueStyle.setRowStyle("background-color:#D2D2D2");
            return valueStyle;
        }
    }
}
