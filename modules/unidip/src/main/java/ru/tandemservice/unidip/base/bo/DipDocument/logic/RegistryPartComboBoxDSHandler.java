/* $Id$ */
package ru.tandemservice.unidip.base.bo.DipDocument.logic;

import org.apache.commons.lang.text.StrBuilder;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unidip.base.bo.DipDocument.DipDocumentManager;
import ru.tandemservice.unidip.base.bo.DipDocument.ui.LineAddEdit.DipDocumentLineAddEditUI;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRegElPartFControlAction;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaObject;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 19.08.2014
 */
public class RegistryPartComboBoxDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String DIPLOMA_OBJECT = "diplomaObject";
    public static final String USED_PART_F_CONTROL_ACTION = "usedPartFControlAction";
    public static final String CURRENT_PART_FCA = "curRegistryPart";
    public static final String SOURCE = "source";
    public static final String REGISTRY_PART = "registryPart";

    public RegistryPartComboBoxDSHandler(String ownerId)
    {
        super(ownerId);
    }

    public static String getLoadString(Double hours, Double weeks, Double labor)
    {
        final StrBuilder strBuilder = new StrBuilder();
        if (hours != null && hours > 0) {
            strBuilder.appendSeparator(", ");
            strBuilder.append(UniEppUtils.formatLoad(hours, false) + " ч.");
        }

        if (weeks != null && weeks > 0) {
            strBuilder.appendSeparator(", ");
            strBuilder.append(UniEppUtils.formatLoad(weeks, false) + " нед.");
        }

        if (labor != null && labor > 0) {
            strBuilder.appendSeparator(", ");
            strBuilder.append(UniEppUtils.formatLoad(labor, false) + " ЗЕТ");
        }

        return strBuilder.toString();
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        DiplomaObject diplomaObject = context.get(DIPLOMA_OBJECT);
        List<EppRegistryElementPartFControlAction> usedPartFControlActionList = context.get(USED_PART_F_CONTROL_ACTION);
        EppRegistryElementPartFControlAction curPartFCA = context.get(CURRENT_PART_FCA);
        String discSource = context.get(SOURCE);
        input.setCountRecord(50);

        final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppRegistryElementPart.class, "ep");
        dql.column(property("ep"));
        dql.order(property("ep", EppRegistryElementPart.number()));
        dql.order(property("ep", EppRegistryElementPart.registryElement().title()));

        FilterUtils.applySelectFilter(dql, "ep", EppRegistryElementPart.id(), input.getPrimaryKeys());
        FilterUtils.applyLikeFilter(dql, input.getComboFilterByValue(),
                                    EppRegistryElementPart.registryElement().title().fromAlias("ep"),
                                    EppRegistryElementPart.registryElement().number().fromAlias("ep"));

        // Части должны иметь формы контроля...
        final DQLSelectBuilder partFCA_dql = new DQLSelectBuilder()
                .fromEntity(EppRegistryElementPartFControlAction.class, "fca")
                .column(value(1))
                .where(eq(property("fca", EppRegistryElementPartFControlAction.part()), property("ep")));

        // Исключаем уже добавленные на форме, но не добавленные в базу
        if (usedPartFControlActionList != null && !usedPartFControlActionList.isEmpty()) {
            partFCA_dql.where(notIn(property("fca"), usedPartFControlActionList));
        }

        // Либо это редактируемая строка, либо  такого еще не добавлено в дипломе
        partFCA_dql.where(or(
                eq(property("fca"), value(curPartFCA)),
                not(exists(DiplomaContentRegElPartFControlAction.class,
                           DiplomaContentRegElPartFControlAction.owner().s(), diplomaObject.getContent(),
                           DiplomaContentRegElPartFControlAction.registryElementPartFControlAction().s(), property("fca")))
        ));

        dql.where(exists(partFCA_dql.buildQuery()));

        // Если источник данных - МСРП - фильтруем еще и по МСРП
        if (DipDocumentLineAddEditUI.SLOT_SOURCE.equals(discSource))
        {
            DQLSelectBuilder eppSlotDisciplines = new DQLSelectBuilder()
                    .fromEntity(EppStudentWpeCAction.class, "d").column(value(1))
                    .joinPath(DQLJoinType.inner, EppStudentWpeCAction.studentWpe().fromAlias("d"), "slot")
                    .joinEntity("d", DQLJoinType.inner, EppRegistryElementPartFControlAction.class, "p",
                            eq(property("slot", EppStudentWorkPlanElement.registryElementPart()), property("p", EppRegistryElementPartFControlAction.part()))
                    )
                    .where(eq(property("d", EppStudentWpeCAction.type()), property("p", EppRegistryElementPartFControlAction.controlAction().eppGroupType())))
                    .where(eq(property("slot", EppStudentWorkPlanElement.student()), value(diplomaObject.getStudent())))
                    .where(eq(property("slot", EppStudentWorkPlanElement.registryElementPart()), property("ep")));

			// Для большинства документов ищем только актуальные МСРП. Для справок об обучении для ативных студентов - тоже. Для справок для неактивных - любые.
            if ((!DipDocumentManager.instance().dao().isEducationReference(diplomaObject.getContent().getType())) || diplomaObject.getStudent().getStatus().isActive())
				eppSlotDisciplines.where(isNull(property("slot", EppStudentWorkPlanElement.removalDate())));

            dql.where(exists(eppSlotDisciplines.buildQuery()));
        }

        return DQLSelectOutputBuilder.get(input, dql, context.getSession()).pageable(true).build().transform((EppRegistryElementPart part) -> {
            final DataWrapper wrapper = new DataWrapper(part.getId(), part.getTitleWithNumber(), part);
            wrapper.put(REGISTRY_PART, part);
            return wrapper;
        });
    }
}
