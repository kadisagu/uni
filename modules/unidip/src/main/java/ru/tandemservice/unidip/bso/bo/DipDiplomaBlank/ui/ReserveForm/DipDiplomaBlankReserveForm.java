/* $Id$ */
package ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.ui.ReserveForm;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author azhebko
 * @since 15.01.2015
 */
@Configuration
public class DipDiplomaBlankReserveForm extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .create();
    }
}