package ru.tandemservice.unidip.base.entity.eduplan;

import ru.tandemservice.unidip.base.entity.eduplan.gen.DiplomaEpvTemplateDefaultRelGen;

/**
 * Связь УП(в) с шаблоном по умолчанию
 *
 * Задает уникальность шаблона по умолчанию для УП(в)
 */
public class DiplomaEpvTemplateDefaultRel extends DiplomaEpvTemplateDefaultRelGen
{
}