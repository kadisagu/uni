package ru.tandemservice.unidip.migration;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.ISQLTranslator;
import org.tandemframework.dbsupport.sql.SQLSelectQuery;
import ru.tandemservice.unidip.base.entity.diploma.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Map;

import static org.tandemframework.dbsupport.sql.SQLFrom.table;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unidip_2x7x0_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность diplomaContentRow

		ISQLTranslator translator = tool.getDialect().getSQLTranslator();

		PreparedStatement updateRowStatement = tool.prepareStatement("update dip_row_t set number_p = ? where id = ?");

		SQLSelectQuery selectDipRowsQuery = new SQLSelectQuery();
		selectDipRowsQuery.from(table("dip_row_t", "dr"));
		selectDipRowsQuery.column("dr.id", "drid");
		selectDipRowsQuery.column("dr.owner_id", "drownid");
		selectDipRowsQuery.order("dr.number_p");

		String dipRowsSql = translator.toSql(selectDipRowsQuery);

		Statement dipRowsStatement = tool.getConnection().createStatement();
		dipRowsStatement.execute(dipRowsSql);

		ResultSet dipRowsResult = dipRowsStatement.getResultSet();

		// owner -> row
		Map<Long, List<Long>> diplomaDisciplineRowMap = Maps.newHashMap();
		Map<Long, List<Long>> diplomaOptDisciplineRowMap = Maps.newHashMap();
		Map<Long, List<Long>> diplomaPracticeRowMap = Maps.newHashMap();
		Map<Long, List<Long>> diplomaCourseWorkRowMap = Maps.newHashMap();
		Map<Long, List<Long>> diplomaQualifWorkRowMap = Maps.newHashMap();
		Map<Long, List<Long>> diplomaStateExamRowMap = Maps.newHashMap();

		EntityRuntime entityRuntime = EntityRuntime.getInstance();

		while (dipRowsResult.next())
		{
			Long id = dipRowsResult.getLong("drid");
			Long ownerId = dipRowsResult.getLong("drownid");

            IEntityMeta meta = entityRuntime.getMeta(id);
            if (meta == null) {
                continue;
            }
            if(DiplomaDisciplineRow.ENTITY_CLASS.equals(meta.getClassName()))
			{
				putMap(id, ownerId, diplomaDisciplineRowMap);
			}
			else if(DiplomaOptDisciplineRow.ENTITY_CLASS.equals(meta.getClassName()))
			{
				putMap(id, ownerId, diplomaOptDisciplineRowMap);
			}
			else if(DiplomaPracticeRow.ENTITY_CLASS.equals(meta.getClassName()))
			{
				putMap(id, ownerId, diplomaPracticeRowMap);
			}
			else if(DiplomaCourseWorkRow.ENTITY_CLASS.equals(meta.getClassName()))
			{
				putMap(id, ownerId, diplomaCourseWorkRowMap);
			}
			else if(DiplomaQualifWorkRow.ENTITY_CLASS.equals(meta.getClassName()))
			{
				putMap(id, ownerId, diplomaQualifWorkRowMap);
			}
			else if(DiplomaStateExamRow.ENTITY_CLASS.equals(meta.getClassName()))
			{
				putMap(id, ownerId, diplomaStateExamRowMap);
			}
			else
				throw new IllegalStateException();
		}

		tool.dropConstraint("dip_row_t", "uniq_row_number_key_436fa5a3");

		updateRows(diplomaDisciplineRowMap, updateRowStatement);
		updateRows(diplomaOptDisciplineRowMap, updateRowStatement);
		updateRows(diplomaPracticeRowMap, updateRowStatement);
		updateRows(diplomaCourseWorkRowMap, updateRowStatement);
		updateRows(diplomaQualifWorkRowMap, updateRowStatement);
		updateRows(diplomaStateExamRowMap, updateRowStatement);
    }

	private void updateRows(Map<Long, List<Long>> rowMap, PreparedStatement updateRowStatement) throws SQLException
	{
		for(Map.Entry<Long, List<Long>> entry : rowMap.entrySet())
		{
			int i = 1;
			for (Long id : entry.getValue())
			{
				updateRowStatement.clearParameters();
				updateRowStatement.setInt(1, i++);
				updateRowStatement.setLong(2, id);
				updateRowStatement.executeUpdate();
			}
		}
	}

	private void putMap(Long id, Long ownerId, Map<Long, List<Long>> rowMap)
	{
		if(!rowMap.containsKey(ownerId))
			rowMap.put(ownerId, Lists.<Long>newArrayList());
		rowMap.get(ownerId).add(id);
	}
}