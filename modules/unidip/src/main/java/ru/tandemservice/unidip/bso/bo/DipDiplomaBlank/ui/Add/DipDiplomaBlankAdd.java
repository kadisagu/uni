/* $Id:$ */
package ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.ui.Add;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.DipDiplomaBlankManager;

/**
 * @author rsizonenko
 * @since 25.12.2014
 */
@Configuration
public class DipDiplomaBlankAdd extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(DipDiplomaBlankManager.instance().storageLocationDataSourceConfig())
                .addDataSource(DipDiplomaBlankManager.instance().blankTypeDataSourceConfig())
                .create();
    }
}
