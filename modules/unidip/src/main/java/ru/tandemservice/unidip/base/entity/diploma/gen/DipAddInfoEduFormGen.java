package ru.tandemservice.unidip.base.entity.diploma.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unidip.base.entity.diploma.DipAddInfoEduForm;
import ru.tandemservice.unidip.base.entity.diploma.DipAdditionalInformation;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Форма обучения в документе об обучении
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DipAddInfoEduFormGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unidip.base.entity.diploma.DipAddInfoEduForm";
    public static final String ENTITY_NAME = "dipAddInfoEduForm";
    public static final int VERSION_HASH = 112151476;
    private static IEntityMeta ENTITY_META;

    public static final String L_DIP_ADDITIONAL_INFORMATION = "dipAdditionalInformation";
    public static final String L_EDU_PROGRAM_FORM = "eduProgramForm";

    private DipAdditionalInformation _dipAdditionalInformation;     // Дополнительные сведения в документе об обучении
    private EduProgramForm _eduProgramForm;     // Форма обучения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дополнительные сведения в документе об обучении. Свойство не может быть null.
     */
    @NotNull
    public DipAdditionalInformation getDipAdditionalInformation()
    {
        return _dipAdditionalInformation;
    }

    /**
     * @param dipAdditionalInformation Дополнительные сведения в документе об обучении. Свойство не может быть null.
     */
    public void setDipAdditionalInformation(DipAdditionalInformation dipAdditionalInformation)
    {
        dirty(_dipAdditionalInformation, dipAdditionalInformation);
        _dipAdditionalInformation = dipAdditionalInformation;
    }

    /**
     * @return Форма обучения.
     */
    public EduProgramForm getEduProgramForm()
    {
        return _eduProgramForm;
    }

    /**
     * @param eduProgramForm Форма обучения.
     */
    public void setEduProgramForm(EduProgramForm eduProgramForm)
    {
        dirty(_eduProgramForm, eduProgramForm);
        _eduProgramForm = eduProgramForm;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof DipAddInfoEduFormGen)
        {
            setDipAdditionalInformation(((DipAddInfoEduForm)another).getDipAdditionalInformation());
            setEduProgramForm(((DipAddInfoEduForm)another).getEduProgramForm());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DipAddInfoEduFormGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DipAddInfoEduForm.class;
        }

        public T newInstance()
        {
            return (T) new DipAddInfoEduForm();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "dipAdditionalInformation":
                    return obj.getDipAdditionalInformation();
                case "eduProgramForm":
                    return obj.getEduProgramForm();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "dipAdditionalInformation":
                    obj.setDipAdditionalInformation((DipAdditionalInformation) value);
                    return;
                case "eduProgramForm":
                    obj.setEduProgramForm((EduProgramForm) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "dipAdditionalInformation":
                        return true;
                case "eduProgramForm":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "dipAdditionalInformation":
                    return true;
                case "eduProgramForm":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "dipAdditionalInformation":
                    return DipAdditionalInformation.class;
                case "eduProgramForm":
                    return EduProgramForm.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DipAddInfoEduForm> _dslPath = new Path<DipAddInfoEduForm>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DipAddInfoEduForm");
    }
            

    /**
     * @return Дополнительные сведения в документе об обучении. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DipAddInfoEduForm#getDipAdditionalInformation()
     */
    public static DipAdditionalInformation.Path<DipAdditionalInformation> dipAdditionalInformation()
    {
        return _dslPath.dipAdditionalInformation();
    }

    /**
     * @return Форма обучения.
     * @see ru.tandemservice.unidip.base.entity.diploma.DipAddInfoEduForm#getEduProgramForm()
     */
    public static EduProgramForm.Path<EduProgramForm> eduProgramForm()
    {
        return _dslPath.eduProgramForm();
    }

    public static class Path<E extends DipAddInfoEduForm> extends EntityPath<E>
    {
        private DipAdditionalInformation.Path<DipAdditionalInformation> _dipAdditionalInformation;
        private EduProgramForm.Path<EduProgramForm> _eduProgramForm;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дополнительные сведения в документе об обучении. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DipAddInfoEduForm#getDipAdditionalInformation()
     */
        public DipAdditionalInformation.Path<DipAdditionalInformation> dipAdditionalInformation()
        {
            if(_dipAdditionalInformation == null )
                _dipAdditionalInformation = new DipAdditionalInformation.Path<DipAdditionalInformation>(L_DIP_ADDITIONAL_INFORMATION, this);
            return _dipAdditionalInformation;
        }

    /**
     * @return Форма обучения.
     * @see ru.tandemservice.unidip.base.entity.diploma.DipAddInfoEduForm#getEduProgramForm()
     */
        public EduProgramForm.Path<EduProgramForm> eduProgramForm()
        {
            if(_eduProgramForm == null )
                _eduProgramForm = new EduProgramForm.Path<EduProgramForm>(L_EDU_PROGRAM_FORM, this);
            return _eduProgramForm;
        }

        public Class getEntityClass()
        {
            return DipAddInfoEduForm.class;
        }

        public String getEntityName()
        {
            return "dipAddInfoEduForm";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
