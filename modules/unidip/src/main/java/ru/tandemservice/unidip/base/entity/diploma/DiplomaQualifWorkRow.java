package ru.tandemservice.unidip.base.entity.diploma;

import com.google.common.collect.ImmutableList;
import ru.tandemservice.unidip.base.entity.diploma.gen.DiplomaQualifWorkRowGen;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.codes.EppRegistryStructureCodes;

import java.util.List;

/**
 * Строка блока ВКР в дипломе
 */
public class DiplomaQualifWorkRow extends DiplomaQualifWorkRowGen
{
    @Override
    protected boolean isLoadInWeeksForOKSO_and_SPO()
    {
        return true;
    }

    @Override
    protected int getBlockPriority()
    {
        return 3;
    }

    @Override
    protected String getPageTitle()
    {
        return "ВКР";
    }

    @Override
    public List<String> getRelatedRegistryElementTypeCodes()
    {
        return ImmutableList.of(
                EppRegistryStructureCodes.REGISTRY_ATTESTATION,
                EppRegistryStructureCodes.REGISTRY_ATTESTATION_DIPLOMA
        );
    }

    @Override
    public boolean isAllowFCA(EppFControlActionType fca)
    {
        // Доступность ФИК определяется настройкой "Связь типов контрольных мероприятий с типами элементов УП / РУП" - для ИГА.
        return fca.isUsedWithAttestation();
    }
}