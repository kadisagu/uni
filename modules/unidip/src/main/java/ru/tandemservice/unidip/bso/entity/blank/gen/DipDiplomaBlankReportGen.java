package ru.tandemservice.unidip.bso.entity.blank.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlankReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сводный отчет по бланкам дипломов и приложений
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DipDiplomaBlankReportGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlankReport";
    public static final String ENTITY_NAME = "dipDiplomaBlankReport";
    public static final int VERSION_HASH = -411403632;
    private static IEntityMeta ENTITY_META;

    public static final String L_CONTENT = "content";
    public static final String P_FORMING_DATE = "formingDate";
    public static final String P_FORM_BY = "formBy";
    public static final String P_BLANK_TYPE = "blankType";
    public static final String P_STORAGE_LOCATION = "storageLocation";

    private DatabaseFile _content;     // Печатная форма
    private Date _formingDate;     // Дата формирования
    private Long _formBy;     // Формировать по
    private String _blankType;     // Тип бланка
    private String _storageLocation;     // Место хранения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     */
    @NotNull
    public DatabaseFile getContent()
    {
        return _content;
    }

    /**
     * @param content Печатная форма. Свойство не может быть null.
     */
    public void setContent(DatabaseFile content)
    {
        dirty(_content, content);
        _content = content;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата формирования. Свойство не может быть null.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    /**
     * 0 - по типам бланков;
     * 1 - по местам хранения.
     * 
     * См. ru.tandemservice.unidip.bso.bo.DiplomaBlankReport.DiplomaBlankReportManager#reportFormByItems
     *
     * @return Формировать по.
     */
    public Long getFormBy()
    {
        return _formBy;
    }

    /**
     * @param formBy Формировать по.
     */
    public void setFormBy(Long formBy)
    {
        dirty(_formBy, formBy);
        _formBy = formBy;
    }

    /**
     * Названия выбранных типов бланков, разделенных "; ".
     *
     * @return Тип бланка.
     */
    public String getBlankType()
    {
        return _blankType;
    }

    /**
     * @param blankType Тип бланка.
     */
    public void setBlankType(String blankType)
    {
        dirty(_blankType, blankType);
        _blankType = blankType;
    }

    /**
     * Название выбранных подразделений, разделенных "; ".
     *
     * @return Место хранения.
     */
    public String getStorageLocation()
    {
        return _storageLocation;
    }

    /**
     * @param storageLocation Место хранения.
     */
    public void setStorageLocation(String storageLocation)
    {
        dirty(_storageLocation, storageLocation);
        _storageLocation = storageLocation;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof DipDiplomaBlankReportGen)
        {
            setContent(((DipDiplomaBlankReport)another).getContent());
            setFormingDate(((DipDiplomaBlankReport)another).getFormingDate());
            setFormBy(((DipDiplomaBlankReport)another).getFormBy());
            setBlankType(((DipDiplomaBlankReport)another).getBlankType());
            setStorageLocation(((DipDiplomaBlankReport)another).getStorageLocation());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DipDiplomaBlankReportGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DipDiplomaBlankReport.class;
        }

        public T newInstance()
        {
            return (T) new DipDiplomaBlankReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "content":
                    return obj.getContent();
                case "formingDate":
                    return obj.getFormingDate();
                case "formBy":
                    return obj.getFormBy();
                case "blankType":
                    return obj.getBlankType();
                case "storageLocation":
                    return obj.getStorageLocation();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "content":
                    obj.setContent((DatabaseFile) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
                case "formBy":
                    obj.setFormBy((Long) value);
                    return;
                case "blankType":
                    obj.setBlankType((String) value);
                    return;
                case "storageLocation":
                    obj.setStorageLocation((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "content":
                        return true;
                case "formingDate":
                        return true;
                case "formBy":
                        return true;
                case "blankType":
                        return true;
                case "storageLocation":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "content":
                    return true;
                case "formingDate":
                    return true;
                case "formBy":
                    return true;
                case "blankType":
                    return true;
                case "storageLocation":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "content":
                    return DatabaseFile.class;
                case "formingDate":
                    return Date.class;
                case "formBy":
                    return Long.class;
                case "blankType":
                    return String.class;
                case "storageLocation":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DipDiplomaBlankReport> _dslPath = new Path<DipDiplomaBlankReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DipDiplomaBlankReport");
    }
            

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlankReport#getContent()
     */
    public static DatabaseFile.Path<DatabaseFile> content()
    {
        return _dslPath.content();
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlankReport#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * 0 - по типам бланков;
     * 1 - по местам хранения.
     * 
     * См. ru.tandemservice.unidip.bso.bo.DiplomaBlankReport.DiplomaBlankReportManager#reportFormByItems
     *
     * @return Формировать по.
     * @see ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlankReport#getFormBy()
     */
    public static PropertyPath<Long> formBy()
    {
        return _dslPath.formBy();
    }

    /**
     * Названия выбранных типов бланков, разделенных "; ".
     *
     * @return Тип бланка.
     * @see ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlankReport#getBlankType()
     */
    public static PropertyPath<String> blankType()
    {
        return _dslPath.blankType();
    }

    /**
     * Название выбранных подразделений, разделенных "; ".
     *
     * @return Место хранения.
     * @see ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlankReport#getStorageLocation()
     */
    public static PropertyPath<String> storageLocation()
    {
        return _dslPath.storageLocation();
    }

    public static class Path<E extends DipDiplomaBlankReport> extends EntityPath<E>
    {
        private DatabaseFile.Path<DatabaseFile> _content;
        private PropertyPath<Date> _formingDate;
        private PropertyPath<Long> _formBy;
        private PropertyPath<String> _blankType;
        private PropertyPath<String> _storageLocation;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlankReport#getContent()
     */
        public DatabaseFile.Path<DatabaseFile> content()
        {
            if(_content == null )
                _content = new DatabaseFile.Path<DatabaseFile>(L_CONTENT, this);
            return _content;
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlankReport#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(DipDiplomaBlankReportGen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * 0 - по типам бланков;
     * 1 - по местам хранения.
     * 
     * См. ru.tandemservice.unidip.bso.bo.DiplomaBlankReport.DiplomaBlankReportManager#reportFormByItems
     *
     * @return Формировать по.
     * @see ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlankReport#getFormBy()
     */
        public PropertyPath<Long> formBy()
        {
            if(_formBy == null )
                _formBy = new PropertyPath<Long>(DipDiplomaBlankReportGen.P_FORM_BY, this);
            return _formBy;
        }

    /**
     * Названия выбранных типов бланков, разделенных "; ".
     *
     * @return Тип бланка.
     * @see ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlankReport#getBlankType()
     */
        public PropertyPath<String> blankType()
        {
            if(_blankType == null )
                _blankType = new PropertyPath<String>(DipDiplomaBlankReportGen.P_BLANK_TYPE, this);
            return _blankType;
        }

    /**
     * Название выбранных подразделений, разделенных "; ".
     *
     * @return Место хранения.
     * @see ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlankReport#getStorageLocation()
     */
        public PropertyPath<String> storageLocation()
        {
            if(_storageLocation == null )
                _storageLocation = new PropertyPath<String>(DipDiplomaBlankReportGen.P_STORAGE_LOCATION, this);
            return _storageLocation;
        }

        public Class getEntityClass()
        {
            return DipDiplomaBlankReport.class;
        }

        public String getEntityName()
        {
            return "dipDiplomaBlankReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
