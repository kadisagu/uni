package ru.tandemservice.unidip.base.entity.diploma;

import ru.tandemservice.unidip.base.entity.catalog.DipDocumentType;
import ru.tandemservice.unidip.base.entity.diploma.gen.DiplomaContentGen;
import ru.tandemservice.uniepp.UniEppUtils;

/**
 * Вкладыш в диплом
 * <p/>
 * это фактически диплом, за исключением факта его выдачи конкретному человеку
 */
public class DiplomaContent extends DiplomaContentGen
{
    public DiplomaContent()
    {
    }

    public DiplomaContent(DipDocumentType docType, String educationElementTytle)
    {
        setType(docType);
        setEducationElementTitle(educationElementTytle);
    }

    public String getLoadTitle()
    {
        return isLoadInWeeks() ? "Общая трудоемкость (в неделях)" : "Общая трудоемкость (в з.е.)";
    }

    public Double getLoadAsDouble()
    {
        final Long load = isLoadInWeeks() ? this.getWeeksAsLong() : this.getLaborAsLong();
        return UniEppUtils.wrap((null == load || load < 0) ? null : load);
    }

    public boolean isLoadInWeeks()
    {
        // В зачетных единицах (ЗЕ) общая трудоемкость выводится для направлений ВО 2009 и 2013 года. Для остальных - в неделях
        return getProgramSubject() != null && !getProgramSubject().isLabor(); // Для ДПО нет направления, пока показываем недели.
    }

    public void setLoadAsDouble(final Double value)
    {
        final Long load = UniEppUtils.unwrap(value);
        this.setLoadAsLong(null == load ? -1 : load);
    }

    public void setLoadAsLong(final Long value)
    {
        if (isLoadInWeeks()) {
            setWeeksAsLong(value);
        } else {
            setLaborAsLong(value);
        }
    }
}