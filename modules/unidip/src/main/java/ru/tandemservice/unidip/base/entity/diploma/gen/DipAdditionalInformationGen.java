package ru.tandemservice.unidip.base.entity.diploma.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unidip.base.entity.diploma.DipAdditionalInformation;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaObject;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Дополнительные сведения в документе об обучении
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DipAdditionalInformationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unidip.base.entity.diploma.DipAdditionalInformation";
    public static final String ENTITY_NAME = "dipAdditionalInformation";
    public static final int VERSION_HASH = 1731711513;
    private static IEntityMeta ENTITY_META;

    public static final String L_DIPLOMA_OBJECT = "diplomaObject";
    public static final String P_SHOW_ADDITIONAL_DISCIPLINE = "showAdditionalDiscipline";
    public static final String P_SHOW_SELF_EDU_FORM = "showSelfEduForm";
    public static final String P_SHOW_PROGRAM_SPECIALIZATION = "showProgramSpecialization";
    public static final String P_SPECIALIZATION = "specialization";
    public static final String P_PASS_INTENSSIVE_TRAINING = "passIntenssiveTraining";
    public static final String P_DEMAND = "demand";

    private DiplomaObject _diplomaObject;     // Документ об обучении
    private boolean _showAdditionalDiscipline;     // Выводить факультативные дисциплины
    private boolean _showSelfEduForm;     // Выводить форму самообразования
    private boolean _showProgramSpecialization;     // Выводить направленность
    private String _specialization;     // Специализация
    private boolean _passIntenssiveTraining;     // Пройдено ускоренное обучение
    private boolean _demand;     // По требованию

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Документ об обучении. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public DiplomaObject getDiplomaObject()
    {
        return _diplomaObject;
    }

    /**
     * @param diplomaObject Документ об обучении. Свойство не может быть null и должно быть уникальным.
     */
    public void setDiplomaObject(DiplomaObject diplomaObject)
    {
        dirty(_diplomaObject, diplomaObject);
        _diplomaObject = diplomaObject;
    }

    /**
     * Признак - "Выводить факультативные дисциплины"
     *
     * @return Выводить факультативные дисциплины. Свойство не может быть null.
     */
    @NotNull
    public boolean isShowAdditionalDiscipline()
    {
        return _showAdditionalDiscipline;
    }

    /**
     * @param showAdditionalDiscipline Выводить факультативные дисциплины. Свойство не может быть null.
     */
    public void setShowAdditionalDiscipline(boolean showAdditionalDiscipline)
    {
        dirty(_showAdditionalDiscipline, showAdditionalDiscipline);
        _showAdditionalDiscipline = showAdditionalDiscipline;
    }

    /**
     * Признак - "Выводить форму самообразования"
     *
     * @return Выводить форму самообразования. Свойство не может быть null.
     */
    @NotNull
    public boolean isShowSelfEduForm()
    {
        return _showSelfEduForm;
    }

    /**
     * @param showSelfEduForm Выводить форму самообразования. Свойство не может быть null.
     */
    public void setShowSelfEduForm(boolean showSelfEduForm)
    {
        dirty(_showSelfEduForm, showSelfEduForm);
        _showSelfEduForm = showSelfEduForm;
    }

    /**
     * Признак - "Выводить направленность"
     *
     * @return Выводить направленность. Свойство не может быть null.
     */
    @NotNull
    public boolean isShowProgramSpecialization()
    {
        return _showProgramSpecialization;
    }

    /**
     * @param showProgramSpecialization Выводить направленность. Свойство не может быть null.
     */
    public void setShowProgramSpecialization(boolean showProgramSpecialization)
    {
        dirty(_showProgramSpecialization, showProgramSpecialization);
        _showProgramSpecialization = showProgramSpecialization;
    }

    /**
     * Специализация.
     *
     * @return Специализация.
     */
    @Length(max=255)
    public String getSpecialization()
    {
        return _specialization;
    }

    /**
     * @param specialization Специализация.
     */
    public void setSpecialization(String specialization)
    {
        dirty(_specialization, specialization);
        _specialization = specialization;
    }

    /**
     * Признак - "Пройдено ускоренное обучение"
     *
     * @return Пройдено ускоренное обучение. Свойство не может быть null.
     */
    @NotNull
    public boolean isPassIntenssiveTraining()
    {
        return _passIntenssiveTraining;
    }

    /**
     * @param passIntenssiveTraining Пройдено ускоренное обучение. Свойство не может быть null.
     */
    public void setPassIntenssiveTraining(boolean passIntenssiveTraining)
    {
        dirty(_passIntenssiveTraining, passIntenssiveTraining);
        _passIntenssiveTraining = passIntenssiveTraining;
    }

    /**
     * Признак - "Справка выдана по требованию"
     *
     * @return По требованию. Свойство не может быть null.
     */
    @NotNull
    public boolean isDemand()
    {
        return _demand;
    }

    /**
     * @param demand По требованию. Свойство не может быть null.
     */
    public void setDemand(boolean demand)
    {
        dirty(_demand, demand);
        _demand = demand;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof DipAdditionalInformationGen)
        {
            setDiplomaObject(((DipAdditionalInformation)another).getDiplomaObject());
            setShowAdditionalDiscipline(((DipAdditionalInformation)another).isShowAdditionalDiscipline());
            setShowSelfEduForm(((DipAdditionalInformation)another).isShowSelfEduForm());
            setShowProgramSpecialization(((DipAdditionalInformation)another).isShowProgramSpecialization());
            setSpecialization(((DipAdditionalInformation)another).getSpecialization());
            setPassIntenssiveTraining(((DipAdditionalInformation)another).isPassIntenssiveTraining());
            setDemand(((DipAdditionalInformation)another).isDemand());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DipAdditionalInformationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DipAdditionalInformation.class;
        }

        public T newInstance()
        {
            return (T) new DipAdditionalInformation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "diplomaObject":
                    return obj.getDiplomaObject();
                case "showAdditionalDiscipline":
                    return obj.isShowAdditionalDiscipline();
                case "showSelfEduForm":
                    return obj.isShowSelfEduForm();
                case "showProgramSpecialization":
                    return obj.isShowProgramSpecialization();
                case "specialization":
                    return obj.getSpecialization();
                case "passIntenssiveTraining":
                    return obj.isPassIntenssiveTraining();
                case "demand":
                    return obj.isDemand();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "diplomaObject":
                    obj.setDiplomaObject((DiplomaObject) value);
                    return;
                case "showAdditionalDiscipline":
                    obj.setShowAdditionalDiscipline((Boolean) value);
                    return;
                case "showSelfEduForm":
                    obj.setShowSelfEduForm((Boolean) value);
                    return;
                case "showProgramSpecialization":
                    obj.setShowProgramSpecialization((Boolean) value);
                    return;
                case "specialization":
                    obj.setSpecialization((String) value);
                    return;
                case "passIntenssiveTraining":
                    obj.setPassIntenssiveTraining((Boolean) value);
                    return;
                case "demand":
                    obj.setDemand((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "diplomaObject":
                        return true;
                case "showAdditionalDiscipline":
                        return true;
                case "showSelfEduForm":
                        return true;
                case "showProgramSpecialization":
                        return true;
                case "specialization":
                        return true;
                case "passIntenssiveTraining":
                        return true;
                case "demand":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "diplomaObject":
                    return true;
                case "showAdditionalDiscipline":
                    return true;
                case "showSelfEduForm":
                    return true;
                case "showProgramSpecialization":
                    return true;
                case "specialization":
                    return true;
                case "passIntenssiveTraining":
                    return true;
                case "demand":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "diplomaObject":
                    return DiplomaObject.class;
                case "showAdditionalDiscipline":
                    return Boolean.class;
                case "showSelfEduForm":
                    return Boolean.class;
                case "showProgramSpecialization":
                    return Boolean.class;
                case "specialization":
                    return String.class;
                case "passIntenssiveTraining":
                    return Boolean.class;
                case "demand":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DipAdditionalInformation> _dslPath = new Path<DipAdditionalInformation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DipAdditionalInformation");
    }
            

    /**
     * @return Документ об обучении. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unidip.base.entity.diploma.DipAdditionalInformation#getDiplomaObject()
     */
    public static DiplomaObject.Path<DiplomaObject> diplomaObject()
    {
        return _dslPath.diplomaObject();
    }

    /**
     * Признак - "Выводить факультативные дисциплины"
     *
     * @return Выводить факультативные дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DipAdditionalInformation#isShowAdditionalDiscipline()
     */
    public static PropertyPath<Boolean> showAdditionalDiscipline()
    {
        return _dslPath.showAdditionalDiscipline();
    }

    /**
     * Признак - "Выводить форму самообразования"
     *
     * @return Выводить форму самообразования. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DipAdditionalInformation#isShowSelfEduForm()
     */
    public static PropertyPath<Boolean> showSelfEduForm()
    {
        return _dslPath.showSelfEduForm();
    }

    /**
     * Признак - "Выводить направленность"
     *
     * @return Выводить направленность. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DipAdditionalInformation#isShowProgramSpecialization()
     */
    public static PropertyPath<Boolean> showProgramSpecialization()
    {
        return _dslPath.showProgramSpecialization();
    }

    /**
     * Специализация.
     *
     * @return Специализация.
     * @see ru.tandemservice.unidip.base.entity.diploma.DipAdditionalInformation#getSpecialization()
     */
    public static PropertyPath<String> specialization()
    {
        return _dslPath.specialization();
    }

    /**
     * Признак - "Пройдено ускоренное обучение"
     *
     * @return Пройдено ускоренное обучение. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DipAdditionalInformation#isPassIntenssiveTraining()
     */
    public static PropertyPath<Boolean> passIntenssiveTraining()
    {
        return _dslPath.passIntenssiveTraining();
    }

    /**
     * Признак - "Справка выдана по требованию"
     *
     * @return По требованию. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DipAdditionalInformation#isDemand()
     */
    public static PropertyPath<Boolean> demand()
    {
        return _dslPath.demand();
    }

    public static class Path<E extends DipAdditionalInformation> extends EntityPath<E>
    {
        private DiplomaObject.Path<DiplomaObject> _diplomaObject;
        private PropertyPath<Boolean> _showAdditionalDiscipline;
        private PropertyPath<Boolean> _showSelfEduForm;
        private PropertyPath<Boolean> _showProgramSpecialization;
        private PropertyPath<String> _specialization;
        private PropertyPath<Boolean> _passIntenssiveTraining;
        private PropertyPath<Boolean> _demand;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Документ об обучении. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unidip.base.entity.diploma.DipAdditionalInformation#getDiplomaObject()
     */
        public DiplomaObject.Path<DiplomaObject> diplomaObject()
        {
            if(_diplomaObject == null )
                _diplomaObject = new DiplomaObject.Path<DiplomaObject>(L_DIPLOMA_OBJECT, this);
            return _diplomaObject;
        }

    /**
     * Признак - "Выводить факультативные дисциплины"
     *
     * @return Выводить факультативные дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DipAdditionalInformation#isShowAdditionalDiscipline()
     */
        public PropertyPath<Boolean> showAdditionalDiscipline()
        {
            if(_showAdditionalDiscipline == null )
                _showAdditionalDiscipline = new PropertyPath<Boolean>(DipAdditionalInformationGen.P_SHOW_ADDITIONAL_DISCIPLINE, this);
            return _showAdditionalDiscipline;
        }

    /**
     * Признак - "Выводить форму самообразования"
     *
     * @return Выводить форму самообразования. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DipAdditionalInformation#isShowSelfEduForm()
     */
        public PropertyPath<Boolean> showSelfEduForm()
        {
            if(_showSelfEduForm == null )
                _showSelfEduForm = new PropertyPath<Boolean>(DipAdditionalInformationGen.P_SHOW_SELF_EDU_FORM, this);
            return _showSelfEduForm;
        }

    /**
     * Признак - "Выводить направленность"
     *
     * @return Выводить направленность. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DipAdditionalInformation#isShowProgramSpecialization()
     */
        public PropertyPath<Boolean> showProgramSpecialization()
        {
            if(_showProgramSpecialization == null )
                _showProgramSpecialization = new PropertyPath<Boolean>(DipAdditionalInformationGen.P_SHOW_PROGRAM_SPECIALIZATION, this);
            return _showProgramSpecialization;
        }

    /**
     * Специализация.
     *
     * @return Специализация.
     * @see ru.tandemservice.unidip.base.entity.diploma.DipAdditionalInformation#getSpecialization()
     */
        public PropertyPath<String> specialization()
        {
            if(_specialization == null )
                _specialization = new PropertyPath<String>(DipAdditionalInformationGen.P_SPECIALIZATION, this);
            return _specialization;
        }

    /**
     * Признак - "Пройдено ускоренное обучение"
     *
     * @return Пройдено ускоренное обучение. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DipAdditionalInformation#isPassIntenssiveTraining()
     */
        public PropertyPath<Boolean> passIntenssiveTraining()
        {
            if(_passIntenssiveTraining == null )
                _passIntenssiveTraining = new PropertyPath<Boolean>(DipAdditionalInformationGen.P_PASS_INTENSSIVE_TRAINING, this);
            return _passIntenssiveTraining;
        }

    /**
     * Признак - "Справка выдана по требованию"
     *
     * @return По требованию. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DipAdditionalInformation#isDemand()
     */
        public PropertyPath<Boolean> demand()
        {
            if(_demand == null )
                _demand = new PropertyPath<Boolean>(DipAdditionalInformationGen.P_DEMAND, this);
            return _demand;
        }

        public Class getEntityClass()
        {
            return DipAdditionalInformation.class;
        }

        public String getEntityName()
        {
            return "dipAdditionalInformation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
