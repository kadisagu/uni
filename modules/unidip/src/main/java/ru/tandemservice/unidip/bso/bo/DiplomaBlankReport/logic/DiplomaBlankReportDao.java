/* $Id$ */
package ru.tandemservice.unidip.bso.bo.DiplomaBlankReport.logic;

import com.google.common.base.Preconditions;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unidip.base.entity.catalog.DipScriptItem;
import ru.tandemservice.unidip.base.entity.catalog.codes.DipScriptItemCodes;
import ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlankReport;
import ru.tandemservice.unidip.bso.entity.catalog.DipBlankType;

import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

/**
 * @author azhebko
 * @since 23.01.2015
 */
public class DiplomaBlankReportDao extends UniBaseDao implements IDiplomaBlankReportDao
{
    public Long createDiplomaBlankReport(@NotNull Long formBy, @NotNull Collection<DipBlankType> blankTypes, @NotNull Collection<OrgUnit> storageLocations)
    {
        Preconditions.checkNotNull(formBy);
        Preconditions.checkNotNull(blankTypes);
        Preconditions.checkNotNull(storageLocations);

        DipDiplomaBlankReport report = new DipDiplomaBlankReport();
        report.setFormingDate(new Date());

        report.setFormBy(formBy);
        report.setBlankType(StringUtils.join(CommonBaseEntityUtil.getPropertiesList(blankTypes, DipBlankType.P_TITLE), "; "));
        report.setStorageLocation(StringUtils.join(CommonBaseEntityUtil.getPropertiesList(storageLocations, OrgUnit.P_TITLE), "; "));

        Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(this.getCatalogItem(DipScriptItem.class, DipScriptItemCodes.DIPLOMA_BLANK_REPORT),
            "formDate", report.getFormingDate(),
            "formBy", formBy,
            "blankTypes", blankTypes,
            "storageLocations", storageLocations);

        DatabaseFile content = new DatabaseFile();
        content.setContent((byte[]) scriptResult.get(IScriptExecutor.DOCUMENT));
        content.setFilename((String) scriptResult.get(IScriptExecutor.FILE_NAME));
        this.save(content);

        report.setContent(content);
        this.save(report);

        return report.getId();
    }
}