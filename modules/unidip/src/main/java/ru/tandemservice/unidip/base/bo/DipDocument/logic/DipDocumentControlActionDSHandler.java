/* $Id$ */
package ru.tandemservice.unidip.base.bo.DipDocument.logic;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unidip.base.bo.DipDocument.ui.LineAddEdit.DipDocumentLineAddEditUI;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRegElPartFControlAction;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaObject;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 19.08.2014
 */
public class DipDocumentControlActionDSHandler extends DefaultComboDataSourceHandler
{
    public DipDocumentControlActionDSHandler(String ownerId)
    {
        super(ownerId, EppRegistryElementPartFControlAction.class);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final EppRegistryElementPart registryElementPart = context.get(RegistryPartComboBoxDSHandler.REGISTRY_PART);
        if (registryElementPart == null) {
            return new DSOutput(input);
        }

        final DiplomaObject diplomaObject = context.get(RegistryPartComboBoxDSHandler.DIPLOMA_OBJECT);
        final EppRegistryElementPartFControlAction curPartFCA = context.get(RegistryPartComboBoxDSHandler.CURRENT_PART_FCA);
        final List<EppRegistryElementPartFControlAction> alreadyUsedIdsInCurrentRow = context.get(RegistryPartComboBoxDSHandler.USED_PART_F_CONTROL_ACTION);

        final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppRegistryElementPartFControlAction.class, "fca");
        dql.column(property("fca"));
        dql.where(eq(property("fca", EppRegistryElementPartFControlAction.part()), value(registryElementPart)));
        dql.order(property("fca", EppRegistryElementPartFControlAction.controlAction().priority()));

        if (!CollectionUtils.isEmpty(alreadyUsedIdsInCurrentRow)) {
            dql.where(notIn(property("fca"), alreadyUsedIdsInCurrentRow));
        }

        dql.where(or(
                eq(property("fca"), value(curPartFCA)),
                not(exists(DiplomaContentRegElPartFControlAction.class,
                           DiplomaContentRegElPartFControlAction.owner().s(), diplomaObject.getContent(),
                           DiplomaContentRegElPartFControlAction.registryElementPartFControlAction().s(), property("fca")))
        ));

        FilterUtils.applySelectFilter(dql, "fca", EppRegistryElementPart.id(), input.getPrimaryKeys());

        return DQLSelectOutputBuilder.get(input, dql, context.getSession()).pageable(false).build().transform((EppRegistryElementPartFControlAction partFCA) -> {
            final DataWrapper dataWrapper = new DataWrapper(partFCA.getId(), partFCA.getControlAction().getTitle(), partFCA);
            dataWrapper.put(DipDocumentLineAddEditUI.BIND_PART_FCA, partFCA);
            return dataWrapper;
        });
    }
}
