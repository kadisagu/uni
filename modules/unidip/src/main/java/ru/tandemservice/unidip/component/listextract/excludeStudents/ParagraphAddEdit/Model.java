/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unidip.component.listextract.excludeStudents.ParagraphAddEdit;

import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.CommonMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.SimpleListResultBuilder;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentIssuance;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance;
import ru.tandemservice.unidip.base.entity.order.DipStuExcludeExtract;

import java.util.*;

/**
 *
 * @author iolshvang
 * @since 25.07.11 15:41
 */
public class Model extends AbstractListParagraphAddEditModel<DipStuExcludeExtract>
{
    private ISelectModel _educationLevelsListModel;
    private EducationLevelsHighSchool _educationLevel;
    private List<StudentStatus> _studentStatusesList;
    private StudentStatus _studentStatusNew;
    private Date _excludeDate;
    private Map<Long, List<DiplomaIssuance>> _diplomaIssuanceMap = new HashMap<>();
    private Map<DiplomaIssuance, List<DiplomaContentIssuance>> _diplomaContentIssuanceMap = new HashMap<>();
    private ISelectModel _formativeOrgUnitListModel;
    private ISelectModel _territorialOrgUnitListModel;
    private OrgUnit _formative;
    private OrgUnit _territorial;
    private Course _course;
    private DevelopForm _developForm;
    private ISelectModel _developFormList;
    private DevelopCondition _developCondition;
    private ISelectModel _developConditionList;
    private DevelopTech _developTech;
    private ISelectModel _developTechList;
    private DevelopPeriod _developPeriod;
    private ISelectModel _developPeriodList;

    // возвращает список фактов выдачи дипломов. одновременно сеттит значение, если оно всего одно. Если сеттит значение, то сразу заполняет мультиселект приложений.
    public List<DataWrapper> getDiplomaIssuanceList()
    {
        final Long id = getDataSource().getCurrentValueEntity().getId();
        final List<DiplomaIssuance> diplomaIssuances = _diplomaIssuanceMap.get(id);
        if (diplomaIssuances == null || diplomaIssuances.isEmpty())
            return Collections.emptyList();

        if (diplomaIssuances.size() == 1)
        {
            final BlockColumn column = (BlockColumn) getDataSource().getColumn("diplomaIssuance");
            final DiplomaIssuance issuance = diplomaIssuances.get(0);
            final DataWrapper dataWrapper = new DataWrapper(issuance);
            dataWrapper.setTitle(getDiplomaIssuanceTitle(issuance));
            final Object existing = column.getValueMap().get(id);
            if (existing == null)
            {
                column.getValueMap().put(id, dataWrapper);
                final BlockColumn content = (BlockColumn) getDataSource().getColumn("issuanceContent");
                Map<Long, List<DataWrapper>> issuanceContent = content.getValueMap();
                if (issuanceContent == null) {
                    issuanceContent = new HashMap<>();
                    content.setValueMap(issuanceContent);
                }

                final List<DiplomaContentIssuance> contentIssuances = DataAccessServices.dao().getList(DiplomaContentIssuance.class, DiplomaContentIssuance.diplomaIssuance(), issuance);

                List<DataWrapper> target = new ArrayList<>();
                Set<Integer> listNumbers = new HashSet<>();

                for (DiplomaContentIssuance contentIssuance : contentIssuances) {
                    if (!contentIssuance.isDuplicate() && listNumbers.add(contentIssuance.getContentListNumber()))
                    {
                        final DataWrapper wrapper = new DataWrapper(contentIssuance);
                        wrapper.setTitle(getContentIssuanceTitle(contentIssuance));
                        target.add(wrapper);
                    }
                }

                issuanceContent.put(id, target);
            }
        }


        return (List<DataWrapper>) CollectionUtils.collect(diplomaIssuances, diplomaIssuance -> {
            final DataWrapper dataWrapper = new DataWrapper(diplomaIssuance);
            dataWrapper.setTitle(getDiplomaIssuanceTitle(diplomaIssuance));
            return dataWrapper;
        });
    }

    public boolean isCurrentRowChecked()
    {
        final CheckboxColumn checkbox = (CheckboxColumn) getDataSource().getColumn("checkbox");
        return checkbox.isSelected(getDataSource().getCurrentValueEntity().getId());
    }

    public CommonMultiSelectModel getIssuanceContentModel()
    {
        final AbstractColumn column = getDataSource().getColumn("diplomaIssuance");

        final DataWrapper dataWrapper = (DataWrapper) ((IValueMapHolder) column).getValueMap().get(getDataSource().getCurrentValueEntity().getId());
        final DiplomaIssuance issuance = dataWrapper != null ? (DiplomaIssuance)dataWrapper.getWrapped() : null;
        final List<DiplomaContentIssuance> contentIssuanceList = getDiplomaContentIssuanceMap().get(issuance);

        return new CommonMultiSelectModel() {
            @Override
            protected IListResultBuilder createBuilder(String filter, Set set) {
                if (issuance == null || contentIssuanceList == null)
                    return new SimpleListResultBuilder<>(Collections.emptyList());

                final Collection<DataWrapper> wrappedList = CollectionUtils.collect(contentIssuanceList, diplomaContentIssuance -> {
                    final DataWrapper wrapper = new DataWrapper(diplomaContentIssuance);
                    final String title = getContentIssuanceTitle(diplomaContentIssuance);
                    wrapper.setTitle(title);
                    return wrapper;
                });
                return new SimpleListResultBuilder<>(wrappedList);
            }
        };

    }

    public String getDiplomaIssuanceTitle(DiplomaIssuance issuance)
    {
        String title = issuance.getDiplomaObject().getTitleWithSuccess();
        if (null != issuance.getBlankSeria() || null != issuance.getBlankNumber())
        {
            title += ":";
            if (null != issuance.getBlankSeria())
                title += " " + issuance.getBlankSeria();
            if (null != issuance.getBlankNumber())
                title += " №" + issuance.getBlankNumber();
        }
        return title;
    }

    public String getContentIssuanceTitle(DiplomaContentIssuance diplomaContentIssuance) {
        StringBuilder titleBuilder = new StringBuilder();
        titleBuilder.append("№").append(diplomaContentIssuance.getContentListNumber())
                .append(": ")
                .append(diplomaContentIssuance.getBlankSeria())
                .append(" №")
                .append(diplomaContentIssuance.getBlankNumber());
        if (diplomaContentIssuance.isDuplicate())
            titleBuilder.append(" Дубликат ").append(diplomaContentIssuance.getDuplicateRegustrationNumber())
                    .append(" ")
                    .append(DateFormatter.DEFAULT_DATE_FORMATTER.format(diplomaContentIssuance.getDuplicateIssuanceDate()));

        return titleBuilder.toString();
    }

    public boolean isFieldDisabled()
    {
        final List<DiplomaIssuance> diplomaIssuances = getDiplomaIssuanceMap().get(getDataSource().getCurrentValueEntity().getId());
        return (diplomaIssuances == null || diplomaIssuances.isEmpty());
    }


    public String getIssuanceRegNumber()
    {
        final IValueMapHolder column = (IValueMapHolder) getDataSource().getColumn("diplomaIssuance");
        final DataWrapper dataWrapper = (DataWrapper) column.getValueMap().get(getDataSource().getCurrentValueEntity().getId());
        if (dataWrapper == null)
            return null;

        final DiplomaIssuance issuance = dataWrapper.getWrapped();
        return issuance.getRegistrationNumber();
    }

    public String getIssuanceDate()
    {
        final IValueMapHolder column = (IValueMapHolder) getDataSource().getColumn("diplomaIssuance");
        final DataWrapper dataWrapper = (DataWrapper) column.getValueMap().get(getDataSource().getCurrentValueEntity().getId());
        if (dataWrapper == null)
            return null;
        final DiplomaIssuance issuance = dataWrapper.getWrapped();
        return DateFormatter.DEFAULT_DATE_FORMATTER.format(issuance.getIssuanceDate());
    }

    public Map<Long, List<DiplomaIssuance>> getDiplomaIssuanceMap()
    {
        return _diplomaIssuanceMap;
    }

    public void setDiplomaIssuanceMap(Map<Long, List<DiplomaIssuance>> diplomaObjectMap)
    {
        _diplomaIssuanceMap = diplomaObjectMap;
    }

    public ISelectModel getEducationLevelsListModel()
    {
        return _educationLevelsListModel;
    }

    public void setEducationLevelsListModel(ISelectModel educationLevelsListModel)
    {
        _educationLevelsListModel = educationLevelsListModel;
    }

    public EducationLevelsHighSchool getEducationLevel()
    {
        return _educationLevel;
    }

    public void setEducationLevel(EducationLevelsHighSchool educationLevel)
    {
        _educationLevel = educationLevel;
    }

    public List<StudentStatus> getStudentStatusesList()
    {
        return _studentStatusesList;
    }

    public void setStudentStatusesList(List<StudentStatus> studentStatusesList)
    {
        _studentStatusesList = studentStatusesList;
    }

    public StudentStatus getStudentStatusNew()
    {
        return _studentStatusNew;
    }

    public void setStudentStatusNew(StudentStatus studentStatusNew)
    {
        _studentStatusNew = studentStatusNew;
    }

    public Date getExcludeDate()
    {
        return _excludeDate;
    }

    public void setExcludeDate(Date excludeDate)
    {
        _excludeDate = excludeDate;
    }

    public ISelectModel getFormativeOrgUnitListModel()
    {
        return _formativeOrgUnitListModel;
    }

    public void setFormativeOrgUnitListModel(ISelectModel formativeOrgUnitListModel)
    {
        _formativeOrgUnitListModel = formativeOrgUnitListModel;
    }

    public ISelectModel getTerritorialOrgUnitListModel()
    {
        return _territorialOrgUnitListModel;
    }

    public void setTerritorialOrgUnitListModel(ISelectModel territorialOrgUnitListModel)
    {
        _territorialOrgUnitListModel = territorialOrgUnitListModel;
    }

    public OrgUnit getFormative()
    {
        return _formative;
    }

    public void setFormative(OrgUnit formative)
    {
        _formative = formative;
    }

    public OrgUnit getTerritorial()
    {
        return _territorial;
    }

    public void setTerritorial(OrgUnit territorial)
    {
        _territorial = territorial;
    }

    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    public void setDevelopForm(DevelopForm developForm)
    {
        _developForm = developForm;
    }

    public ISelectModel getDevelopFormList() { return _developFormList; }
    public void setDevelopFormList(ISelectModel developFormList) { _developFormList = developFormList; }

    public DevelopCondition getDevelopCondition()
    {
        return _developCondition;
    }

    public void setDevelopCondition(DevelopCondition developCondition)
    {
        _developCondition = developCondition;
    }

    public ISelectModel getDevelopConditionList() { return _developConditionList; }
    public void setDevelopConditionList(ISelectModel developConditionList) { _developConditionList = developConditionList; }

    public DevelopTech getDevelopTech()
    {
        return _developTech;
    }

    public void setDevelopTech(DevelopTech developTech)
    {
        _developTech = developTech;
    }

    public ISelectModel getDevelopTechList() { return _developTechList; }
    public void setDevelopTechList(ISelectModel developTechList) { _developTechList = developTechList; }

    public DevelopPeriod getDevelopPeriod()
    {
        return _developPeriod;
    }

    public void setDevelopPeriod(DevelopPeriod developPeriod)
    {
        _developPeriod = developPeriod;
    }

    public ISelectModel getDevelopPeriodList() { return _developPeriodList; }
    public void setDevelopPeriodList(ISelectModel developPeriodList) { _developPeriodList = developPeriodList; }

    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }


    public Map<DiplomaIssuance, List<DiplomaContentIssuance>> getDiplomaContentIssuanceMap() {
        return _diplomaContentIssuanceMap;
    }

    public void setDiplomaContentIssuanceMap(Map<DiplomaIssuance, List<DiplomaContentIssuance>> diplomaContentIssuanceMap) {
        _diplomaContentIssuanceMap = diplomaContentIssuanceMap;
    }

    public String getIssuanceDependentFields() {
        final Long currentEntityId = ((IEntity) getDataSource().getCurrentEntity()).getId();
        return "issuanceContent" + currentEntityId + ", issuanceRegNumber" + currentEntityId + ", issuanceDate" + currentEntityId;
    }
}