package ru.tandemservice.unidip.base.entity.order.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentIssuance;
import ru.tandemservice.unidip.base.entity.order.DipContentIssuanceToDipExtractRelation;
import ru.tandemservice.unidip.base.entity.order.DipStuExcludeExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь информации о выданном приложении к документу об обучении с выпиской из приказа об отчислении
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DipContentIssuanceToDipExtractRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unidip.base.entity.order.DipContentIssuanceToDipExtractRelation";
    public static final String ENTITY_NAME = "dipContentIssuanceToDipExtractRelation";
    public static final int VERSION_HASH = 1894994645;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXTRACT = "extract";
    public static final String L_CONTENT_ISSUANCE = "contentIssuance";

    private DipStuExcludeExtract _extract;     // Выписка из приказа об отчислении
    private DiplomaContentIssuance _contentIssuance;     // Информация о выданном приложении к документу об обучении

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка из приказа об отчислении. Свойство не может быть null.
     */
    @NotNull
    public DipStuExcludeExtract getExtract()
    {
        return _extract;
    }

    /**
     * @param extract Выписка из приказа об отчислении. Свойство не может быть null.
     */
    public void setExtract(DipStuExcludeExtract extract)
    {
        dirty(_extract, extract);
        _extract = extract;
    }

    /**
     * @return Информация о выданном приложении к документу об обучении. Свойство не может быть null.
     */
    @NotNull
    public DiplomaContentIssuance getContentIssuance()
    {
        return _contentIssuance;
    }

    /**
     * @param contentIssuance Информация о выданном приложении к документу об обучении. Свойство не может быть null.
     */
    public void setContentIssuance(DiplomaContentIssuance contentIssuance)
    {
        dirty(_contentIssuance, contentIssuance);
        _contentIssuance = contentIssuance;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof DipContentIssuanceToDipExtractRelationGen)
        {
            setExtract(((DipContentIssuanceToDipExtractRelation)another).getExtract());
            setContentIssuance(((DipContentIssuanceToDipExtractRelation)another).getContentIssuance());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DipContentIssuanceToDipExtractRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DipContentIssuanceToDipExtractRelation.class;
        }

        public T newInstance()
        {
            return (T) new DipContentIssuanceToDipExtractRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "extract":
                    return obj.getExtract();
                case "contentIssuance":
                    return obj.getContentIssuance();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "extract":
                    obj.setExtract((DipStuExcludeExtract) value);
                    return;
                case "contentIssuance":
                    obj.setContentIssuance((DiplomaContentIssuance) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "extract":
                        return true;
                case "contentIssuance":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "extract":
                    return true;
                case "contentIssuance":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "extract":
                    return DipStuExcludeExtract.class;
                case "contentIssuance":
                    return DiplomaContentIssuance.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DipContentIssuanceToDipExtractRelation> _dslPath = new Path<DipContentIssuanceToDipExtractRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DipContentIssuanceToDipExtractRelation");
    }
            

    /**
     * @return Выписка из приказа об отчислении. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.order.DipContentIssuanceToDipExtractRelation#getExtract()
     */
    public static DipStuExcludeExtract.Path<DipStuExcludeExtract> extract()
    {
        return _dslPath.extract();
    }

    /**
     * @return Информация о выданном приложении к документу об обучении. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.order.DipContentIssuanceToDipExtractRelation#getContentIssuance()
     */
    public static DiplomaContentIssuance.Path<DiplomaContentIssuance> contentIssuance()
    {
        return _dslPath.contentIssuance();
    }

    public static class Path<E extends DipContentIssuanceToDipExtractRelation> extends EntityPath<E>
    {
        private DipStuExcludeExtract.Path<DipStuExcludeExtract> _extract;
        private DiplomaContentIssuance.Path<DiplomaContentIssuance> _contentIssuance;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка из приказа об отчислении. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.order.DipContentIssuanceToDipExtractRelation#getExtract()
     */
        public DipStuExcludeExtract.Path<DipStuExcludeExtract> extract()
        {
            if(_extract == null )
                _extract = new DipStuExcludeExtract.Path<DipStuExcludeExtract>(L_EXTRACT, this);
            return _extract;
        }

    /**
     * @return Информация о выданном приложении к документу об обучении. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.order.DipContentIssuanceToDipExtractRelation#getContentIssuance()
     */
        public DiplomaContentIssuance.Path<DiplomaContentIssuance> contentIssuance()
        {
            if(_contentIssuance == null )
                _contentIssuance = new DiplomaContentIssuance.Path<DiplomaContentIssuance>(L_CONTENT_ISSUANCE, this);
            return _contentIssuance;
        }

        public Class getEntityClass()
        {
            return DipContentIssuanceToDipExtractRelation.class;
        }

        public String getEntityName()
        {
            return "dipContentIssuanceToDipExtractRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
