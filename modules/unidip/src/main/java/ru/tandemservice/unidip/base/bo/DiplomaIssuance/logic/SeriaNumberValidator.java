/* $Id$ */
package ru.tandemservice.unidip.base.bo.DiplomaIssuance.logic;

import org.apache.commons.lang.StringUtils;
import org.apache.tapestry.form.IFormComponent;
import org.apache.tapestry.form.ValidationMessages;
import org.apache.tapestry.form.validator.BaseValidator;
import org.apache.tapestry.valid.ValidationStrings;
import org.apache.tapestry.valid.ValidatorException;

/**
 * @author Andrey Avetisov
 * @since 22.10.2014
 */
public class SeriaNumberValidator extends BaseValidator
{
    public SeriaNumberValidator()
    {
    }

    public SeriaNumberValidator(String initializer)
    {
        super(initializer);
    }

    @Override
    public void validate(IFormComponent field, ValidationMessages messages, Object object) throws ValidatorException
    {
        String input = (String) object;
        if (input != null)
        {
            char[] specChar = {'`', '\'', '~','!', '@', '#', '$', '%', '^', '&', '<', '>'};
            if (StringUtils.containsAny(input, specChar))
            {
                throw new ValidatorException(buildMessage(messages, field));
            }
        }
        else
        {
            throw new ValidatorException(buildEmptyMessage(messages, field));
        }
    }

    private String buildMessage(ValidationMessages messages, IFormComponent field)
    {
        return messages.formatValidationMessage("Поле «{0}» не должно содержать следующие символы: ` ~ ! @ # $ % ^ & < >", ValidationStrings.PATTERN_MISMATCH, new Object[]{field.getDisplayName()});
    }

    private String buildEmptyMessage(ValidationMessages messages, IFormComponent field)
    {
        return messages.formatValidationMessage("Поле «{0}» должно быть заполнено.", ValidationStrings.PATTERN_MISMATCH, new Object[]{field.getDisplayName()});
    }

    @Override
    public boolean isRequired()
    {
        return true;
    }

    @Override
    public boolean getAcceptsNull()
    {
        return true;
    }
}