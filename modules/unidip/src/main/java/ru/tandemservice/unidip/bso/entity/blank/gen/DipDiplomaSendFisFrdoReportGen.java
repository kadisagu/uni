package ru.tandemservice.unidip.bso.entity.blank.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uni.entity.report.StorableReport;
import ru.tandemservice.unidip.base.entity.catalog.DipDocumentType;
import ru.tandemservice.unidip.bso.entity.blank.DipDiplomaSendFisFrdoReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Список дипломов государственного образца для передачи в ФИС ФРДО
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DipDiplomaSendFisFrdoReportGen extends StorableReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unidip.bso.entity.blank.DipDiplomaSendFisFrdoReport";
    public static final String ENTITY_NAME = "dipDiplomaSendFisFrdoReport";
    public static final int VERSION_HASH = -803655451;
    private static IEntityMeta ENTITY_META;

    public static final String L_DOCUMENT_TYPE = "documentType";
    public static final String P_EDU_LEVEL = "eduLevel";
    public static final String P_ISSUANCE_DATE_TO = "issuanceDateTo";
    public static final String P_ISSUANCE_DATE_FROM = "issuanceDateFrom";
    public static final String P_DUPLICATE_DIPLOMA_OBJECT = "duplicateDiplomaObject";

    private DipDocumentType _documentType;     // Тип документа
    private String _eduLevel;     // Уровень образования
    private Date _issuanceDateTo;     // Дата выдачи с
    private Date _issuanceDateFrom;     // Дата выдачи по
    private boolean _duplicateDiplomaObject;     // Дубликат документа об обучении

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Тип документа.
     */
    public DipDocumentType getDocumentType()
    {
        return _documentType;
    }

    /**
     * @param documentType Тип документа.
     */
    public void setDocumentType(DipDocumentType documentType)
    {
        dirty(_documentType, documentType);
        _documentType = documentType;
    }

    /**
     * @return Уровень образования.
     */
    @Length(max=255)
    public String getEduLevel()
    {
        return _eduLevel;
    }

    /**
     * @param eduLevel Уровень образования.
     */
    public void setEduLevel(String eduLevel)
    {
        dirty(_eduLevel, eduLevel);
        _eduLevel = eduLevel;
    }

    /**
     * @return Дата выдачи с.
     */
    public Date getIssuanceDateTo()
    {
        return _issuanceDateTo;
    }

    /**
     * @param issuanceDateTo Дата выдачи с.
     */
    public void setIssuanceDateTo(Date issuanceDateTo)
    {
        dirty(_issuanceDateTo, issuanceDateTo);
        _issuanceDateTo = issuanceDateTo;
    }

    /**
     * @return Дата выдачи по.
     */
    public Date getIssuanceDateFrom()
    {
        return _issuanceDateFrom;
    }

    /**
     * @param issuanceDateFrom Дата выдачи по.
     */
    public void setIssuanceDateFrom(Date issuanceDateFrom)
    {
        dirty(_issuanceDateFrom, issuanceDateFrom);
        _issuanceDateFrom = issuanceDateFrom;
    }

    /**
     * @return Дубликат документа об обучении. Свойство не может быть null.
     */
    @NotNull
    public boolean isDuplicateDiplomaObject()
    {
        return _duplicateDiplomaObject;
    }

    /**
     * @param duplicateDiplomaObject Дубликат документа об обучении. Свойство не может быть null.
     */
    public void setDuplicateDiplomaObject(boolean duplicateDiplomaObject)
    {
        dirty(_duplicateDiplomaObject, duplicateDiplomaObject);
        _duplicateDiplomaObject = duplicateDiplomaObject;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof DipDiplomaSendFisFrdoReportGen)
        {
            setDocumentType(((DipDiplomaSendFisFrdoReport)another).getDocumentType());
            setEduLevel(((DipDiplomaSendFisFrdoReport)another).getEduLevel());
            setIssuanceDateTo(((DipDiplomaSendFisFrdoReport)another).getIssuanceDateTo());
            setIssuanceDateFrom(((DipDiplomaSendFisFrdoReport)another).getIssuanceDateFrom());
            setDuplicateDiplomaObject(((DipDiplomaSendFisFrdoReport)another).isDuplicateDiplomaObject());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DipDiplomaSendFisFrdoReportGen> extends StorableReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DipDiplomaSendFisFrdoReport.class;
        }

        public T newInstance()
        {
            return (T) new DipDiplomaSendFisFrdoReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "documentType":
                    return obj.getDocumentType();
                case "eduLevel":
                    return obj.getEduLevel();
                case "issuanceDateTo":
                    return obj.getIssuanceDateTo();
                case "issuanceDateFrom":
                    return obj.getIssuanceDateFrom();
                case "duplicateDiplomaObject":
                    return obj.isDuplicateDiplomaObject();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "documentType":
                    obj.setDocumentType((DipDocumentType) value);
                    return;
                case "eduLevel":
                    obj.setEduLevel((String) value);
                    return;
                case "issuanceDateTo":
                    obj.setIssuanceDateTo((Date) value);
                    return;
                case "issuanceDateFrom":
                    obj.setIssuanceDateFrom((Date) value);
                    return;
                case "duplicateDiplomaObject":
                    obj.setDuplicateDiplomaObject((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "documentType":
                        return true;
                case "eduLevel":
                        return true;
                case "issuanceDateTo":
                        return true;
                case "issuanceDateFrom":
                        return true;
                case "duplicateDiplomaObject":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "documentType":
                    return true;
                case "eduLevel":
                    return true;
                case "issuanceDateTo":
                    return true;
                case "issuanceDateFrom":
                    return true;
                case "duplicateDiplomaObject":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "documentType":
                    return DipDocumentType.class;
                case "eduLevel":
                    return String.class;
                case "issuanceDateTo":
                    return Date.class;
                case "issuanceDateFrom":
                    return Date.class;
                case "duplicateDiplomaObject":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DipDiplomaSendFisFrdoReport> _dslPath = new Path<DipDiplomaSendFisFrdoReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DipDiplomaSendFisFrdoReport");
    }
            

    /**
     * @return Тип документа.
     * @see ru.tandemservice.unidip.bso.entity.blank.DipDiplomaSendFisFrdoReport#getDocumentType()
     */
    public static DipDocumentType.Path<DipDocumentType> documentType()
    {
        return _dslPath.documentType();
    }

    /**
     * @return Уровень образования.
     * @see ru.tandemservice.unidip.bso.entity.blank.DipDiplomaSendFisFrdoReport#getEduLevel()
     */
    public static PropertyPath<String> eduLevel()
    {
        return _dslPath.eduLevel();
    }

    /**
     * @return Дата выдачи с.
     * @see ru.tandemservice.unidip.bso.entity.blank.DipDiplomaSendFisFrdoReport#getIssuanceDateTo()
     */
    public static PropertyPath<Date> issuanceDateTo()
    {
        return _dslPath.issuanceDateTo();
    }

    /**
     * @return Дата выдачи по.
     * @see ru.tandemservice.unidip.bso.entity.blank.DipDiplomaSendFisFrdoReport#getIssuanceDateFrom()
     */
    public static PropertyPath<Date> issuanceDateFrom()
    {
        return _dslPath.issuanceDateFrom();
    }

    /**
     * @return Дубликат документа об обучении. Свойство не может быть null.
     * @see ru.tandemservice.unidip.bso.entity.blank.DipDiplomaSendFisFrdoReport#isDuplicateDiplomaObject()
     */
    public static PropertyPath<Boolean> duplicateDiplomaObject()
    {
        return _dslPath.duplicateDiplomaObject();
    }

    public static class Path<E extends DipDiplomaSendFisFrdoReport> extends StorableReport.Path<E>
    {
        private DipDocumentType.Path<DipDocumentType> _documentType;
        private PropertyPath<String> _eduLevel;
        private PropertyPath<Date> _issuanceDateTo;
        private PropertyPath<Date> _issuanceDateFrom;
        private PropertyPath<Boolean> _duplicateDiplomaObject;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Тип документа.
     * @see ru.tandemservice.unidip.bso.entity.blank.DipDiplomaSendFisFrdoReport#getDocumentType()
     */
        public DipDocumentType.Path<DipDocumentType> documentType()
        {
            if(_documentType == null )
                _documentType = new DipDocumentType.Path<DipDocumentType>(L_DOCUMENT_TYPE, this);
            return _documentType;
        }

    /**
     * @return Уровень образования.
     * @see ru.tandemservice.unidip.bso.entity.blank.DipDiplomaSendFisFrdoReport#getEduLevel()
     */
        public PropertyPath<String> eduLevel()
        {
            if(_eduLevel == null )
                _eduLevel = new PropertyPath<String>(DipDiplomaSendFisFrdoReportGen.P_EDU_LEVEL, this);
            return _eduLevel;
        }

    /**
     * @return Дата выдачи с.
     * @see ru.tandemservice.unidip.bso.entity.blank.DipDiplomaSendFisFrdoReport#getIssuanceDateTo()
     */
        public PropertyPath<Date> issuanceDateTo()
        {
            if(_issuanceDateTo == null )
                _issuanceDateTo = new PropertyPath<Date>(DipDiplomaSendFisFrdoReportGen.P_ISSUANCE_DATE_TO, this);
            return _issuanceDateTo;
        }

    /**
     * @return Дата выдачи по.
     * @see ru.tandemservice.unidip.bso.entity.blank.DipDiplomaSendFisFrdoReport#getIssuanceDateFrom()
     */
        public PropertyPath<Date> issuanceDateFrom()
        {
            if(_issuanceDateFrom == null )
                _issuanceDateFrom = new PropertyPath<Date>(DipDiplomaSendFisFrdoReportGen.P_ISSUANCE_DATE_FROM, this);
            return _issuanceDateFrom;
        }

    /**
     * @return Дубликат документа об обучении. Свойство не может быть null.
     * @see ru.tandemservice.unidip.bso.entity.blank.DipDiplomaSendFisFrdoReport#isDuplicateDiplomaObject()
     */
        public PropertyPath<Boolean> duplicateDiplomaObject()
        {
            if(_duplicateDiplomaObject == null )
                _duplicateDiplomaObject = new PropertyPath<Boolean>(DipDiplomaSendFisFrdoReportGen.P_DUPLICATE_DIPLOMA_OBJECT, this);
            return _duplicateDiplomaObject;
        }

        public Class getEntityClass()
        {
            return DipDiplomaSendFisFrdoReport.class;
        }

        public String getEntityName()
        {
            return "dipDiplomaSendFisFrdoReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
