/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unidip.base.bo.DiplomaTemplate.ui.AddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.unidip.base.bo.DipDocument.DipDocumentManager;
import ru.tandemservice.unidip.base.bo.DipDocument.logic.DipDocumentTypeSelectModel;
import ru.tandemservice.unidip.base.bo.DiplomaTemplate.DiplomaTemplateManager;
import ru.tandemservice.unidip.base.entity.catalog.DipDocumentType;
import ru.tandemservice.unidip.base.entity.catalog.codes.DipDocumentTypeCodes;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContent;
import ru.tandemservice.unidip.base.entity.eduplan.DiplomaTemplate;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;

import java.util.Collection;

/**
 * @author iolshvang
 * @since 14.07.11 11:42
 */
@Input({
               @Bind(key = DiplomaTemplateAddEditUI.DIP_TEMPLATE_ID, binding = "diplomaTemplate.id"),
               @Bind(key = DiplomaTemplateAddEditUI.EDUPLAN_VERSION_ID, binding = DiplomaTemplateAddEditUI.EDUPLAN_VERSION_ID)
       })
public class DiplomaTemplateAddEditUI extends UIPresenter
{
    public static final String DIP_TEMPLATE_ID = "dipTemplateId";
    public static final String EDUPLAN_VERSION_ID = "eduPlanVersionId";


    private Long _eduPlanVersionId;

    private DiplomaTemplate _diplomaTemplate = new DiplomaTemplate();
    private DiplomaContent _diplomaContent;

    private ISingleSelectModel docTypeModel;
    private boolean _editForm;

    @Override
    public void onComponentRefresh()
    {
        setEditForm(getDiplomaTemplate().getId() != null);
        if (isEditForm())
        {
            // форма редактирования
            _diplomaTemplate = DataAccessServices.dao().getNotNull(DiplomaTemplate.class, _diplomaTemplate.getId());
            _diplomaContent = _diplomaTemplate.getContent();
        }
        else
        {
            _diplomaTemplate.setEduPlanVersion(DataAccessServices.dao().getNotNull(EppEduPlanVersion.class, _eduPlanVersionId));
            _diplomaContent = new DiplomaContent();
            _diplomaTemplate.setContent(_diplomaContent);

            EduProgramKind programKind = getDiplomaTemplate().getEduPlanVersion().getEduPlan().getProgramKind();
            switch (programKind.getCode())
            {
                case EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_KVALIFITSIROVANNYH_RABOCHIH_SLUJATSHIH_:
                case EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV_SREDNEGO_ZVENA:
                    _diplomaContent.setType(DataAccessServices.dao().get(DipDocumentType.class, DipDocumentType.code(), DipDocumentTypeCodes.COLLEGE_DIPLOMA));
                    break;
                case EduProgramKindCodes.PROGRAMMA_BAKALAVRIATA:
                    _diplomaContent.setType(DataAccessServices.dao().get(DipDocumentType.class, DipDocumentType.code(), DipDocumentTypeCodes.BACHELOR_DIPLOMA));
                    break;
                case EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV:
                    _diplomaContent.setType(DataAccessServices.dao().get(DipDocumentType.class, DipDocumentType.code(), DipDocumentTypeCodes.SPECIALIST_DIPLOMA));
                    break;
                case EduProgramKindCodes.PROGRAMMA_MAGISTRATURY:
                    _diplomaContent.setType(DataAccessServices.dao().get(DipDocumentType.class, DipDocumentType.code(), DipDocumentTypeCodes.MAGISTR_DIPLOMA));
                    break;
                case EduProgramKindCodes.PROGRAMMA_ASPIRANTURY_ADYUNKTURY_:
                    _diplomaContent.setType(DataAccessServices.dao().get(DipDocumentType.class, DipDocumentType.code(), DipDocumentTypeCodes.POSTGRADUATE_DIPLOMA));
                    break;
                case EduProgramKindCodes.DOPOLNITELNAYA_PROFESSIONALNAYA_PROGRAMMA:
                    _diplomaContent.setType(DataAccessServices.dao().get(DipDocumentType.class, DipDocumentType.code(), DipDocumentTypeCodes.PROFESSION_RETRAIN_DIPLOMA_));
                    break;
            }

            if (getDiplomaTemplate().getEduPlanVersion().getEduPlan() instanceof EppEduPlanProf)
            {
                EppEduPlanProf eduPlanProf = ((EppEduPlanProf) getDiplomaTemplate().getEduPlanVersion().getEduPlan());
                getDiplomaContent().setProgramSubject(eduPlanProf.getProgramSubject());
                getDiplomaContent().setLoadAsLong(getDiplomaContent().isLoadInWeeks() ? eduPlanProf.getWeeksAsLong() : eduPlanProf.getLaborAsLong());
            }
        }

        EppEduPlan eduPlan = getDiplomaTemplate().getEduPlanVersion().getEduPlan();
        Collection<DipDocumentType> dipDocumentTypes = DipDocumentManager.getDipDocumentTypes(eduPlan.getProgramKind().getCode(), eduPlan instanceof EppEduPlanProf ? ((EppEduPlanProf) eduPlan).getProgramSubject().getSubjectIndex().getCode() : null);
        setDocTypeModel(new DipDocumentTypeSelectModel(dipDocumentTypes, true));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (DiplomaTemplateAddEdit.SPECIALIZATION_DS.equals(dataSource.getName()))
        {
            dataSource.put(DiplomaTemplateAddEdit.BIND_EDUPLAN_VERSION, getDiplomaTemplate().getEduPlanVersion());
        }
    }

    // Getters & Setters


    public Long getEduPlanVersionId()
    {
        return _eduPlanVersionId;
    }

    public void setEduPlanVersionId(Long eduPlanVersionId)
    {
        _eduPlanVersionId = eduPlanVersionId;
    }

    public DiplomaTemplate getDiplomaTemplate()
    {
        return _diplomaTemplate;
    }

    public void setDiplomaTemplate(DiplomaTemplate diplomaTemplate)
    {
        _diplomaTemplate = diplomaTemplate;
    }

    public ISingleSelectModel getDocTypeModel()
    {
        return docTypeModel;
    }

    public void setDocTypeModel(ISingleSelectModel docTypeModel)
    {
        this.docTypeModel = docTypeModel;
    }

    public boolean isEduPlanProf()
    {
        return getDiplomaTemplate().getEduPlanVersion().getEduPlan() instanceof EppEduPlanProf;
    }

    public DiplomaContent getDiplomaContent()
    {
        return _diplomaContent;
    }

    public void setDiplomaContent(DiplomaContent diplomaContent)
    {
        _diplomaContent = diplomaContent;
    }

    public boolean isEditForm()
    {
        return _editForm;
    }

    public void setEditForm(boolean editForm)
    {
        _editForm = editForm;
    }

    public String getLoadFieldTitle()
    {
        return getDiplomaContent().getLoadTitle();
    }

    // Listeners

    public void onClickApply()
    {
        DiplomaTemplateManager.instance().dao().saveOrUpdateDiplomaTemplate(_diplomaTemplate, _diplomaContent);
        deactivate();
    }
}
