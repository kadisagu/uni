/* $Id$ */
package ru.tandemservice.unidip.base.bo.DipOrgUnit;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

/**
 * @author Alexey Lopatin
 * @since 29.05.2015
 */
@Configuration
public class DipOrgUnitManager extends BusinessObjectManager
{
    public static DipOrgUnitManager instance()
    {
        return instance(DipOrgUnitManager.class);
    }
}
