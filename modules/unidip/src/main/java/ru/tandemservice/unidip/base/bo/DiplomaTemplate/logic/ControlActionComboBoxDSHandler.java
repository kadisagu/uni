/* $Id$ */
package ru.tandemservice.unidip.base.bo.DiplomaTemplate.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unidip.base.bo.DiplomaTemplate.DiplomaTemplateManager;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow;
import ru.tandemservice.unidip.base.entity.eduplan.DiplomaTemplate;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Andrey Avetisov
 * @since 21.07.2014
 */
public class ControlActionComboBoxDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String REGISTER_ELEMENT_PART = "registerElementPart";
    public static final String REG_ELEM_WRAPPED_LIST = "regElemWrappedList";
    public static final String DIPLOMA_TEMPLATE = "diplomaTemplate";
    public static final String DIPLOMA_CONTENT_ROW = "diplomaContentRow";

    public ControlActionComboBoxDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final DiplomaTemplate template = context.get(DIPLOMA_TEMPLATE);
        final DiplomaContentRow row = context.get(DIPLOMA_CONTENT_ROW);
        final EppRegistryElementPart part = context.get(REGISTER_ELEMENT_PART);
        final List<DataWrapper> wrappedList = context.get(REG_ELEM_WRAPPED_LIST);

        if (part == null) {
            return new DSOutput(input);
        }

        final DQLSelectBuilder dql = DiplomaTemplateManager.instance().dao().getEppRegistryElementPartFControlActionBuilder("e", template, part, wrappedList, row);
        dql.column(property("e"));

        FilterUtils.applySelectFilter(dql, "e", "id", input.getPrimaryKeys());
        FilterUtils.applyLikeFilter(dql, input.getComboFilterByValue(), EppRegistryElementPartFControlAction.controlAction().title().fromAlias("e"));

        dql.order(property("e", EppRegistryElementPartFControlAction.controlAction().priority()));

        return DQLSelectOutputBuilder.get(input, dql, context.getSession()).pageable(false).build();
    }
}
