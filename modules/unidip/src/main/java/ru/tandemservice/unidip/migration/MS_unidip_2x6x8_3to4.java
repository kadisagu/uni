package ru.tandemservice.unidip.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unidip_2x6x8_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.8")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность dipEduInOtherOrganization

		// переименована колонка свойства row
		// изменился тип данных (LONG -> STRING), может быть это разные свойства? тогда колонку переименовывать не нужно
		{
            tool.createColumn("dip_edu_other_organization_t", new DBColumn("row_p", DBType.createVarchar(255)));

            tool.executeUpdate("UPDATE dip_edu_other_organization_t SET row_p = t1.education_p FROM dip_row_t AS t1 WHERE row_id = t1.id");
			// переименовать колонку
			tool.dropColumn("dip_edu_other_organization_t", "row_id");

		}




    }
}