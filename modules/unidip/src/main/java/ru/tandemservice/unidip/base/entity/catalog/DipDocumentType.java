package ru.tandemservice.unidip.base.entity.catalog;

import com.google.common.collect.ImmutableSet;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.tool.tree.IHierarchyItem;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.BaseDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unidip.base.bo.DipDocument.ui.TypeAddEdit.DipDocumentTypeAddEdit;

import ru.tandemservice.unidip.base.entity.catalog.gen.DipDocumentTypeGen;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static ru.tandemservice.unidip.base.entity.catalog.codes.DipDocumentTypeCodes.*;

/**
 * Тип документа
 * <p/>
 * указывает класс, который будет обрабатывать строки в УП(в) при выставлении оценок в диплом
 */
public class DipDocumentType extends DipDocumentTypeGen implements IHierarchyItem, IDynamicCatalogItem
{
    public static final Set<String> ONLY_DIPLOMA_CODES = ImmutableSet.of(COLLEGE_DIPLOMA, BACHELOR_DIPLOMA, SPECIALIST_DIPLOMA, MAGISTR_DIPLOMA, POSTGRADUATE_DIPLOMA, ADUNCTURE_DIPLOMA_, PROFESSION_RETRAIN_DIPLOMA_);

    @Override
    public IHierarchyItem getHierarhyParent()
    {
        return this.getParent();
    }

    //Если у записи есть элемент 1го уровня, то возвращает title в следующем формате <назв. эл. первого ур.>(<название эл. второго ур.>
    @EntityDSLSupport(parts = {DipDocumentType.L_PARENT+"."+DipDocumentType.P_TITLE, DipDocumentType.P_TITLE})
    public String getTitleWithParent()
    {
        if (null != this.getParent())
            return getParent().getTitle() + " (" + getTitle() + ")";

        return getTitle();
    }

    private static Set<String> HIDDEN_PROPERTIES = ImmutableSet.of(
            DipDocumentType.L_PARENT,
            DipDocumentType.P_CHILD_ALLOWED
    );

    public static IDynamicCatalogDesc getUiDesc() {
        return new BaseDynamicCatalogDesc() {
            @Override public Collection<String> getHiddenFields() { return HIDDEN_PROPERTIES; }
            @Override public String getAddEditComponentName() { return DipDocumentTypeAddEdit.class.getSimpleName(); }
            @Override public Set<Long> getEditDisabledItems(Set<Long> itemIds) {
                return new HashSet<>(IUniBaseDao.instance.get().<Long>getList(
                        new DQLSelectBuilder().fromEntity(DipDocumentType.class, "e")
                                .column(DQLExpressions.property("e.id"))
                                .where(DQLExpressions.isNull(DQLExpressions.property("e", DipDocumentType.parent())))
                ));
            }
        };
    }

}