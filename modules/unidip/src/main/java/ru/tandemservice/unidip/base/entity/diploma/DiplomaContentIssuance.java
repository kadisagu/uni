package ru.tandemservice.unidip.base.entity.diploma;

import ru.tandemservice.unidip.base.entity.diploma.gen.DiplomaContentIssuanceGen;

/**
 * Информация о выданном приложении к документу об образовании
 *
 * Данные о бланке, использованном для выдачи приложения к диплому.
 */
public class DiplomaContentIssuance extends DiplomaContentIssuanceGen
{
}