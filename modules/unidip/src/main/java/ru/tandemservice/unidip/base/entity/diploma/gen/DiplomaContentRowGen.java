package ru.tandemservice.unidip.base.entity.diploma.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unidip.base.entity.catalog.DipAggregationMethod;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContent;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Строка документа об обучении
 *
 * значимая строка в дипломе - за что, сколько часов, что получил, дополнительные данные
 * (данный объект может иметь наследников для кастомной логики печати, например)
 * 
 * важно понимать, что в дипломе не может быть двух строк с одинаковым названием и номером семестра,
 * и название фактически является единственным идентификатором строки
 * ее (строку) иногда невозмо привязать ни к чему более, кроме названия
 * (напрмер, если для конкретного человека дисциплина согласно договору называется не так, как для всех,
 * или, когда выставляется оценка за блок дисциплин и он именуется некоторым названем)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DiplomaContentRowGen extends EntityBase
 implements ILazyFieldOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow";
    public static final String ENTITY_NAME = "diplomaContentRow";
    public static final int VERSION_HASH = 2062950555;
    private static IEntityMeta ENTITY_META;

    public static final String L_OWNER = "owner";
    public static final String P_TITLE = "title";
    public static final String P_TERM = "term";
    public static final String L_SOURCE_AGGREGATION = "sourceAggregation";
    public static final String P_NUMBER = "number";
    public static final String P_ROW_CONTENT_UPDATE_DATE = "rowContentUpdateDate";
    public static final String P_ROW_VALUE_UPDATE_DATE = "rowValueUpdateDate";
    public static final String P_LOAD_AS_LONG = "loadAsLong";
    public static final String P_AUD_LOAD_AS_LONG = "audLoadAsLong";
    public static final String P_WEEKS_AS_LONG = "weeksAsLong";
    public static final String P_LABOR_AS_LONG = "laborAsLong";
    public static final String P_MARK = "mark";
    public static final String P_EDUCATION = "education";
    public static final String P_COMMENT = "comment";
    public static final String P_THEME = "theme";
    public static final String P_TUTOR = "tutor";
    public static final String P_CONTENT_LIST_NUMBER = "contentListNumber";
    public static final String L_EPV_REGISTRY_ROW = "epvRegistryRow";
    public static final String P_COMMON_LOAD = "commonLoad";
    public static final String P_ROW_LOAD_TITLE = "rowLoadTitle";
    public static final String P_TERM_AS_STRING = "termAsString";
    public static final String P_TITLE_WITH_THEME = "titleWithTheme";

    private DiplomaContent _owner;     // Содержание документа об обучении
    private String _title;     // Название
    private int _term;     // Номер семестра
    private DipAggregationMethod _sourceAggregation;     // Алгоритм выставления оценки в диплом
    private int _number;     // Порядковый номер в документе
    private Date _rowContentUpdateDate;     // Дата изменения состава строки
    private Date _rowValueUpdateDate;     // Дата изменения значения строки
    private Long _loadAsLong;     // Нагрузка (в сотых долях часа)
    private Long _audLoadAsLong;     // Аудиторная нагрузка (в сотых долях часа)
    private Long _weeksAsLong;     // Нагрузка в неделях (в сотых долях недель)
    private Long _laborAsLong;     // Трудоемкость (в сотых долях ЗЕ)
    private String _mark;     // Оценка
    private String _education;     // Образовательное учреждение
    private String _comment;     // Описание
    private String _theme;     // Тема
    private String _tutor;     // Преподаватель
    private int _contentListNumber = 1;     // Номер листа приложения
    private EppEpvRegistryRow _epvRegistryRow;     // Строка УП (элемент реестра)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Содержание документа об обучении. Свойство не может быть null.
     */
    @NotNull
    public DiplomaContent getOwner()
    {
        return _owner;
    }

    /**
     * @param owner Содержание документа об обучении. Свойство не может быть null.
     */
    public void setOwner(DiplomaContent owner)
    {
        dirty(_owner, owner);
        _owner = owner;
    }

    /**
     * именно эта строка печатается в диплом
     *
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * Номер семестра, за который выставляется оценка. Должен быть уникален в рамках блока для строк с одинаковым названием или строкой УПв, либо равен нулю (ноль в интерфейсе не выводится).
     *
     * @return Номер семестра. Свойство не может быть null.
     */
    @NotNull
    public int getTerm()
    {
        return _term;
    }

    /**
     * @param term Номер семестра. Свойство не может быть null.
     */
    public void setTerm(int term)
    {
        dirty(_term, term);
        _term = term;
    }

    /**
     * алгоритм показывает как множество МСРП (они прописаны в поле sourceFilter) превратить в одну единственную оценку	с указанием нагрузки
     * при формировании выписки из УП(в) данное поле заполняется значением из соответствующие строки УП(в)
     *
     * @return Алгоритм выставления оценки в диплом.
     */
    public DipAggregationMethod getSourceAggregation()
    {
        return _sourceAggregation;
    }

    /**
     * @param sourceAggregation Алгоритм выставления оценки в диплом.
     */
    public void setSourceAggregation(DipAggregationMethod sourceAggregation)
    {
        dirty(_sourceAggregation, sourceAggregation);
        _sourceAggregation = sourceAggregation;
    }

    /**
     * @return Порядковый номер в документе. Свойство не может быть null.
     */
    @NotNull
    public int getNumber()
    {
        return _number;
    }

    /**
     * @param number Порядковый номер в документе. Свойство не может быть null.
     */
    public void setNumber(int number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Дата изменения состава строки. Свойство не может быть null.
     */
    @NotNull
    public Date getRowContentUpdateDate()
    {
        return _rowContentUpdateDate;
    }

    /**
     * @param rowContentUpdateDate Дата изменения состава строки. Свойство не может быть null.
     */
    public void setRowContentUpdateDate(Date rowContentUpdateDate)
    {
        dirty(_rowContentUpdateDate, rowContentUpdateDate);
        _rowContentUpdateDate = rowContentUpdateDate;
    }

    /**
     * @return Дата изменения значения строки. Свойство не может быть null.
     */
    @NotNull
    public Date getRowValueUpdateDate()
    {
        return _rowValueUpdateDate;
    }

    /**
     * @param rowValueUpdateDate Дата изменения значения строки. Свойство не может быть null.
     */
    public void setRowValueUpdateDate(Date rowValueUpdateDate)
    {
        dirty(_rowValueUpdateDate, rowValueUpdateDate);
        _rowValueUpdateDate = rowValueUpdateDate;
    }

    /**
     * @return Нагрузка (в сотых долях часа).
     */
    public Long getLoadAsLong()
    {
        return _loadAsLong;
    }

    /**
     * @param loadAsLong Нагрузка (в сотых долях часа).
     */
    public void setLoadAsLong(Long loadAsLong)
    {
        dirty(_loadAsLong, loadAsLong);
        _loadAsLong = loadAsLong;
    }

    /**
     * @return Аудиторная нагрузка (в сотых долях часа).
     */
    public Long getAudLoadAsLong()
    {
        return _audLoadAsLong;
    }

    /**
     * @param audLoadAsLong Аудиторная нагрузка (в сотых долях часа).
     */
    public void setAudLoadAsLong(Long audLoadAsLong)
    {
        dirty(_audLoadAsLong, audLoadAsLong);
        _audLoadAsLong = audLoadAsLong;
    }

    /**
     * @return Нагрузка в неделях (в сотых долях недель).
     */
    public Long getWeeksAsLong()
    {
        return _weeksAsLong;
    }

    /**
     * @param weeksAsLong Нагрузка в неделях (в сотых долях недель).
     */
    public void setWeeksAsLong(Long weeksAsLong)
    {
        dirty(_weeksAsLong, weeksAsLong);
        _weeksAsLong = weeksAsLong;
    }

    /**
     * @return Трудоемкость (в сотых долях ЗЕ).
     */
    public Long getLaborAsLong()
    {
        return _laborAsLong;
    }

    /**
     * @param laborAsLong Трудоемкость (в сотых долях ЗЕ).
     */
    public void setLaborAsLong(Long laborAsLong)
    {
        dirty(_laborAsLong, laborAsLong);
        _laborAsLong = laborAsLong;
    }

    /**
     * @return Оценка.
     */
    @Length(max=255)
    public String getMark()
    {
        return _mark;
    }

    /**
     * @param mark Оценка.
     */
    public void setMark(String mark)
    {
        dirty(_mark, mark);
        _mark = mark;
    }

    /**
     * Название ОУ, в котором получена эта оценка. Заполняется для оценок, полученных перезачтением, соответственно, если не заполнено - то в текущем.
     *
     * @return Образовательное учреждение.
     */
    @Length(max=255)
    public String getEducation()
    {
        return _education;
    }

    /**
     * @param education Образовательное учреждение.
     */
    public void setEducation(String education)
    {
        dirty(_education, education);
        _education = education;
    }

    /**
     * @return Описание.
     */
    public String getComment()
    {
        initLazyForGet("comment");
        return _comment;
    }

    /**
     * @param comment Описание.
     */
    public void setComment(String comment)
    {
        initLazyForSet("comment");
        dirty(_comment, comment);
        _comment = comment;
    }

    /**
     * @return Тема.
     */
    public String getTheme()
    {
        return _theme;
    }

    /**
     * @param theme Тема.
     */
    public void setTheme(String theme)
    {
        dirty(_theme, theme);
        _theme = theme;
    }

    /**
     * @return Преподаватель.
     */
    public String getTutor()
    {
        initLazyForGet("tutor");
        return _tutor;
    }

    /**
     * @param tutor Преподаватель.
     */
    public void setTutor(String tutor)
    {
        initLazyForSet("tutor");
        dirty(_tutor, tutor);
        _tutor = tutor;
    }

    /**
     * Номер листа приложения, куда печатать строку.
     *
     * @return Номер листа приложения. Свойство не может быть null.
     */
    @NotNull
    public int getContentListNumber()
    {
        return _contentListNumber;
    }

    /**
     * @param contentListNumber Номер листа приложения. Свойство не может быть null.
     */
    public void setContentListNumber(int contentListNumber)
    {
        dirty(_contentListNumber, contentListNumber);
        _contentListNumber = contentListNumber;
    }

    /**
     * @return Строка УП (элемент реестра).
     */
    public EppEpvRegistryRow getEpvRegistryRow()
    {
        return _epvRegistryRow;
    }

    /**
     * @param epvRegistryRow Строка УП (элемент реестра).
     */
    public void setEpvRegistryRow(EppEpvRegistryRow epvRegistryRow)
    {
        dirty(_epvRegistryRow, epvRegistryRow);
        _epvRegistryRow = epvRegistryRow;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof DiplomaContentRowGen)
        {
            setOwner(((DiplomaContentRow)another).getOwner());
            setTitle(((DiplomaContentRow)another).getTitle());
            setTerm(((DiplomaContentRow)another).getTerm());
            setSourceAggregation(((DiplomaContentRow)another).getSourceAggregation());
            setNumber(((DiplomaContentRow)another).getNumber());
            setRowContentUpdateDate(((DiplomaContentRow)another).getRowContentUpdateDate());
            setRowValueUpdateDate(((DiplomaContentRow)another).getRowValueUpdateDate());
            setLoadAsLong(((DiplomaContentRow)another).getLoadAsLong());
            setAudLoadAsLong(((DiplomaContentRow)another).getAudLoadAsLong());
            setWeeksAsLong(((DiplomaContentRow)another).getWeeksAsLong());
            setLaborAsLong(((DiplomaContentRow)another).getLaborAsLong());
            setMark(((DiplomaContentRow)another).getMark());
            setEducation(((DiplomaContentRow)another).getEducation());
            setComment(((DiplomaContentRow)another).getComment());
            setTheme(((DiplomaContentRow)another).getTheme());
            setTutor(((DiplomaContentRow)another).getTutor());
            setContentListNumber(((DiplomaContentRow)another).getContentListNumber());
            setEpvRegistryRow(((DiplomaContentRow)another).getEpvRegistryRow());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DiplomaContentRowGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DiplomaContentRow.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("DiplomaContentRow is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "owner":
                    return obj.getOwner();
                case "title":
                    return obj.getTitle();
                case "term":
                    return obj.getTerm();
                case "sourceAggregation":
                    return obj.getSourceAggregation();
                case "number":
                    return obj.getNumber();
                case "rowContentUpdateDate":
                    return obj.getRowContentUpdateDate();
                case "rowValueUpdateDate":
                    return obj.getRowValueUpdateDate();
                case "loadAsLong":
                    return obj.getLoadAsLong();
                case "audLoadAsLong":
                    return obj.getAudLoadAsLong();
                case "weeksAsLong":
                    return obj.getWeeksAsLong();
                case "laborAsLong":
                    return obj.getLaborAsLong();
                case "mark":
                    return obj.getMark();
                case "education":
                    return obj.getEducation();
                case "comment":
                    return obj.getComment();
                case "theme":
                    return obj.getTheme();
                case "tutor":
                    return obj.getTutor();
                case "contentListNumber":
                    return obj.getContentListNumber();
                case "epvRegistryRow":
                    return obj.getEpvRegistryRow();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "owner":
                    obj.setOwner((DiplomaContent) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "term":
                    obj.setTerm((Integer) value);
                    return;
                case "sourceAggregation":
                    obj.setSourceAggregation((DipAggregationMethod) value);
                    return;
                case "number":
                    obj.setNumber((Integer) value);
                    return;
                case "rowContentUpdateDate":
                    obj.setRowContentUpdateDate((Date) value);
                    return;
                case "rowValueUpdateDate":
                    obj.setRowValueUpdateDate((Date) value);
                    return;
                case "loadAsLong":
                    obj.setLoadAsLong((Long) value);
                    return;
                case "audLoadAsLong":
                    obj.setAudLoadAsLong((Long) value);
                    return;
                case "weeksAsLong":
                    obj.setWeeksAsLong((Long) value);
                    return;
                case "laborAsLong":
                    obj.setLaborAsLong((Long) value);
                    return;
                case "mark":
                    obj.setMark((String) value);
                    return;
                case "education":
                    obj.setEducation((String) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
                case "theme":
                    obj.setTheme((String) value);
                    return;
                case "tutor":
                    obj.setTutor((String) value);
                    return;
                case "contentListNumber":
                    obj.setContentListNumber((Integer) value);
                    return;
                case "epvRegistryRow":
                    obj.setEpvRegistryRow((EppEpvRegistryRow) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "owner":
                        return true;
                case "title":
                        return true;
                case "term":
                        return true;
                case "sourceAggregation":
                        return true;
                case "number":
                        return true;
                case "rowContentUpdateDate":
                        return true;
                case "rowValueUpdateDate":
                        return true;
                case "loadAsLong":
                        return true;
                case "audLoadAsLong":
                        return true;
                case "weeksAsLong":
                        return true;
                case "laborAsLong":
                        return true;
                case "mark":
                        return true;
                case "education":
                        return true;
                case "comment":
                        return true;
                case "theme":
                        return true;
                case "tutor":
                        return true;
                case "contentListNumber":
                        return true;
                case "epvRegistryRow":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "owner":
                    return true;
                case "title":
                    return true;
                case "term":
                    return true;
                case "sourceAggregation":
                    return true;
                case "number":
                    return true;
                case "rowContentUpdateDate":
                    return true;
                case "rowValueUpdateDate":
                    return true;
                case "loadAsLong":
                    return true;
                case "audLoadAsLong":
                    return true;
                case "weeksAsLong":
                    return true;
                case "laborAsLong":
                    return true;
                case "mark":
                    return true;
                case "education":
                    return true;
                case "comment":
                    return true;
                case "theme":
                    return true;
                case "tutor":
                    return true;
                case "contentListNumber":
                    return true;
                case "epvRegistryRow":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "owner":
                    return DiplomaContent.class;
                case "title":
                    return String.class;
                case "term":
                    return Integer.class;
                case "sourceAggregation":
                    return DipAggregationMethod.class;
                case "number":
                    return Integer.class;
                case "rowContentUpdateDate":
                    return Date.class;
                case "rowValueUpdateDate":
                    return Date.class;
                case "loadAsLong":
                    return Long.class;
                case "audLoadAsLong":
                    return Long.class;
                case "weeksAsLong":
                    return Long.class;
                case "laborAsLong":
                    return Long.class;
                case "mark":
                    return String.class;
                case "education":
                    return String.class;
                case "comment":
                    return String.class;
                case "theme":
                    return String.class;
                case "tutor":
                    return String.class;
                case "contentListNumber":
                    return Integer.class;
                case "epvRegistryRow":
                    return EppEpvRegistryRow.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DiplomaContentRow> _dslPath = new Path<DiplomaContentRow>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DiplomaContentRow");
    }
            

    /**
     * @return Содержание документа об обучении. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow#getOwner()
     */
    public static DiplomaContent.Path<DiplomaContent> owner()
    {
        return _dslPath.owner();
    }

    /**
     * именно эта строка печатается в диплом
     *
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * Номер семестра, за который выставляется оценка. Должен быть уникален в рамках блока для строк с одинаковым названием или строкой УПв, либо равен нулю (ноль в интерфейсе не выводится).
     *
     * @return Номер семестра. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow#getTerm()
     */
    public static PropertyPath<Integer> term()
    {
        return _dslPath.term();
    }

    /**
     * алгоритм показывает как множество МСРП (они прописаны в поле sourceFilter) превратить в одну единственную оценку	с указанием нагрузки
     * при формировании выписки из УП(в) данное поле заполняется значением из соответствующие строки УП(в)
     *
     * @return Алгоритм выставления оценки в диплом.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow#getSourceAggregation()
     */
    public static DipAggregationMethod.Path<DipAggregationMethod> sourceAggregation()
    {
        return _dslPath.sourceAggregation();
    }

    /**
     * @return Порядковый номер в документе. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow#getNumber()
     */
    public static PropertyPath<Integer> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Дата изменения состава строки. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow#getRowContentUpdateDate()
     */
    public static PropertyPath<Date> rowContentUpdateDate()
    {
        return _dslPath.rowContentUpdateDate();
    }

    /**
     * @return Дата изменения значения строки. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow#getRowValueUpdateDate()
     */
    public static PropertyPath<Date> rowValueUpdateDate()
    {
        return _dslPath.rowValueUpdateDate();
    }

    /**
     * @return Нагрузка (в сотых долях часа).
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow#getLoadAsLong()
     */
    public static PropertyPath<Long> loadAsLong()
    {
        return _dslPath.loadAsLong();
    }

    /**
     * @return Аудиторная нагрузка (в сотых долях часа).
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow#getAudLoadAsLong()
     */
    public static PropertyPath<Long> audLoadAsLong()
    {
        return _dslPath.audLoadAsLong();
    }

    /**
     * @return Нагрузка в неделях (в сотых долях недель).
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow#getWeeksAsLong()
     */
    public static PropertyPath<Long> weeksAsLong()
    {
        return _dslPath.weeksAsLong();
    }

    /**
     * @return Трудоемкость (в сотых долях ЗЕ).
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow#getLaborAsLong()
     */
    public static PropertyPath<Long> laborAsLong()
    {
        return _dslPath.laborAsLong();
    }

    /**
     * @return Оценка.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow#getMark()
     */
    public static PropertyPath<String> mark()
    {
        return _dslPath.mark();
    }

    /**
     * Название ОУ, в котором получена эта оценка. Заполняется для оценок, полученных перезачтением, соответственно, если не заполнено - то в текущем.
     *
     * @return Образовательное учреждение.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow#getEducation()
     */
    public static PropertyPath<String> education()
    {
        return _dslPath.education();
    }

    /**
     * @return Описание.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * @return Тема.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow#getTheme()
     */
    public static PropertyPath<String> theme()
    {
        return _dslPath.theme();
    }

    /**
     * @return Преподаватель.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow#getTutor()
     */
    public static PropertyPath<String> tutor()
    {
        return _dslPath.tutor();
    }

    /**
     * Номер листа приложения, куда печатать строку.
     *
     * @return Номер листа приложения. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow#getContentListNumber()
     */
    public static PropertyPath<Integer> contentListNumber()
    {
        return _dslPath.contentListNumber();
    }

    /**
     * @return Строка УП (элемент реестра).
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow#getEpvRegistryRow()
     */
    public static EppEpvRegistryRow.Path<EppEpvRegistryRow> epvRegistryRow()
    {
        return _dslPath.epvRegistryRow();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow#getCommonLoad()
     */
    public static SupportedPropertyPath<Double> commonLoad()
    {
        return _dslPath.commonLoad();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow#getRowLoadTitle()
     */
    public static SupportedPropertyPath<String> rowLoadTitle()
    {
        return _dslPath.rowLoadTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow#getTermAsString()
     */
    public static SupportedPropertyPath<String> termAsString()
    {
        return _dslPath.termAsString();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow#getTitleWithTheme()
     */
    public static SupportedPropertyPath<String> titleWithTheme()
    {
        return _dslPath.titleWithTheme();
    }

    public static class Path<E extends DiplomaContentRow> extends EntityPath<E>
    {
        private DiplomaContent.Path<DiplomaContent> _owner;
        private PropertyPath<String> _title;
        private PropertyPath<Integer> _term;
        private DipAggregationMethod.Path<DipAggregationMethod> _sourceAggregation;
        private PropertyPath<Integer> _number;
        private PropertyPath<Date> _rowContentUpdateDate;
        private PropertyPath<Date> _rowValueUpdateDate;
        private PropertyPath<Long> _loadAsLong;
        private PropertyPath<Long> _audLoadAsLong;
        private PropertyPath<Long> _weeksAsLong;
        private PropertyPath<Long> _laborAsLong;
        private PropertyPath<String> _mark;
        private PropertyPath<String> _education;
        private PropertyPath<String> _comment;
        private PropertyPath<String> _theme;
        private PropertyPath<String> _tutor;
        private PropertyPath<Integer> _contentListNumber;
        private EppEpvRegistryRow.Path<EppEpvRegistryRow> _epvRegistryRow;
        private SupportedPropertyPath<Double> _commonLoad;
        private SupportedPropertyPath<String> _rowLoadTitle;
        private SupportedPropertyPath<String> _termAsString;
        private SupportedPropertyPath<String> _titleWithTheme;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Содержание документа об обучении. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow#getOwner()
     */
        public DiplomaContent.Path<DiplomaContent> owner()
        {
            if(_owner == null )
                _owner = new DiplomaContent.Path<DiplomaContent>(L_OWNER, this);
            return _owner;
        }

    /**
     * именно эта строка печатается в диплом
     *
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(DiplomaContentRowGen.P_TITLE, this);
            return _title;
        }

    /**
     * Номер семестра, за который выставляется оценка. Должен быть уникален в рамках блока для строк с одинаковым названием или строкой УПв, либо равен нулю (ноль в интерфейсе не выводится).
     *
     * @return Номер семестра. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow#getTerm()
     */
        public PropertyPath<Integer> term()
        {
            if(_term == null )
                _term = new PropertyPath<Integer>(DiplomaContentRowGen.P_TERM, this);
            return _term;
        }

    /**
     * алгоритм показывает как множество МСРП (они прописаны в поле sourceFilter) превратить в одну единственную оценку	с указанием нагрузки
     * при формировании выписки из УП(в) данное поле заполняется значением из соответствующие строки УП(в)
     *
     * @return Алгоритм выставления оценки в диплом.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow#getSourceAggregation()
     */
        public DipAggregationMethod.Path<DipAggregationMethod> sourceAggregation()
        {
            if(_sourceAggregation == null )
                _sourceAggregation = new DipAggregationMethod.Path<DipAggregationMethod>(L_SOURCE_AGGREGATION, this);
            return _sourceAggregation;
        }

    /**
     * @return Порядковый номер в документе. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow#getNumber()
     */
        public PropertyPath<Integer> number()
        {
            if(_number == null )
                _number = new PropertyPath<Integer>(DiplomaContentRowGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Дата изменения состава строки. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow#getRowContentUpdateDate()
     */
        public PropertyPath<Date> rowContentUpdateDate()
        {
            if(_rowContentUpdateDate == null )
                _rowContentUpdateDate = new PropertyPath<Date>(DiplomaContentRowGen.P_ROW_CONTENT_UPDATE_DATE, this);
            return _rowContentUpdateDate;
        }

    /**
     * @return Дата изменения значения строки. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow#getRowValueUpdateDate()
     */
        public PropertyPath<Date> rowValueUpdateDate()
        {
            if(_rowValueUpdateDate == null )
                _rowValueUpdateDate = new PropertyPath<Date>(DiplomaContentRowGen.P_ROW_VALUE_UPDATE_DATE, this);
            return _rowValueUpdateDate;
        }

    /**
     * @return Нагрузка (в сотых долях часа).
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow#getLoadAsLong()
     */
        public PropertyPath<Long> loadAsLong()
        {
            if(_loadAsLong == null )
                _loadAsLong = new PropertyPath<Long>(DiplomaContentRowGen.P_LOAD_AS_LONG, this);
            return _loadAsLong;
        }

    /**
     * @return Аудиторная нагрузка (в сотых долях часа).
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow#getAudLoadAsLong()
     */
        public PropertyPath<Long> audLoadAsLong()
        {
            if(_audLoadAsLong == null )
                _audLoadAsLong = new PropertyPath<Long>(DiplomaContentRowGen.P_AUD_LOAD_AS_LONG, this);
            return _audLoadAsLong;
        }

    /**
     * @return Нагрузка в неделях (в сотых долях недель).
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow#getWeeksAsLong()
     */
        public PropertyPath<Long> weeksAsLong()
        {
            if(_weeksAsLong == null )
                _weeksAsLong = new PropertyPath<Long>(DiplomaContentRowGen.P_WEEKS_AS_LONG, this);
            return _weeksAsLong;
        }

    /**
     * @return Трудоемкость (в сотых долях ЗЕ).
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow#getLaborAsLong()
     */
        public PropertyPath<Long> laborAsLong()
        {
            if(_laborAsLong == null )
                _laborAsLong = new PropertyPath<Long>(DiplomaContentRowGen.P_LABOR_AS_LONG, this);
            return _laborAsLong;
        }

    /**
     * @return Оценка.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow#getMark()
     */
        public PropertyPath<String> mark()
        {
            if(_mark == null )
                _mark = new PropertyPath<String>(DiplomaContentRowGen.P_MARK, this);
            return _mark;
        }

    /**
     * Название ОУ, в котором получена эта оценка. Заполняется для оценок, полученных перезачтением, соответственно, если не заполнено - то в текущем.
     *
     * @return Образовательное учреждение.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow#getEducation()
     */
        public PropertyPath<String> education()
        {
            if(_education == null )
                _education = new PropertyPath<String>(DiplomaContentRowGen.P_EDUCATION, this);
            return _education;
        }

    /**
     * @return Описание.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(DiplomaContentRowGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * @return Тема.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow#getTheme()
     */
        public PropertyPath<String> theme()
        {
            if(_theme == null )
                _theme = new PropertyPath<String>(DiplomaContentRowGen.P_THEME, this);
            return _theme;
        }

    /**
     * @return Преподаватель.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow#getTutor()
     */
        public PropertyPath<String> tutor()
        {
            if(_tutor == null )
                _tutor = new PropertyPath<String>(DiplomaContentRowGen.P_TUTOR, this);
            return _tutor;
        }

    /**
     * Номер листа приложения, куда печатать строку.
     *
     * @return Номер листа приложения. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow#getContentListNumber()
     */
        public PropertyPath<Integer> contentListNumber()
        {
            if(_contentListNumber == null )
                _contentListNumber = new PropertyPath<Integer>(DiplomaContentRowGen.P_CONTENT_LIST_NUMBER, this);
            return _contentListNumber;
        }

    /**
     * @return Строка УП (элемент реестра).
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow#getEpvRegistryRow()
     */
        public EppEpvRegistryRow.Path<EppEpvRegistryRow> epvRegistryRow()
        {
            if(_epvRegistryRow == null )
                _epvRegistryRow = new EppEpvRegistryRow.Path<EppEpvRegistryRow>(L_EPV_REGISTRY_ROW, this);
            return _epvRegistryRow;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow#getCommonLoad()
     */
        public SupportedPropertyPath<Double> commonLoad()
        {
            if(_commonLoad == null )
                _commonLoad = new SupportedPropertyPath<Double>(DiplomaContentRowGen.P_COMMON_LOAD, this);
            return _commonLoad;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow#getRowLoadTitle()
     */
        public SupportedPropertyPath<String> rowLoadTitle()
        {
            if(_rowLoadTitle == null )
                _rowLoadTitle = new SupportedPropertyPath<String>(DiplomaContentRowGen.P_ROW_LOAD_TITLE, this);
            return _rowLoadTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow#getTermAsString()
     */
        public SupportedPropertyPath<String> termAsString()
        {
            if(_termAsString == null )
                _termAsString = new SupportedPropertyPath<String>(DiplomaContentRowGen.P_TERM_AS_STRING, this);
            return _termAsString;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow#getTitleWithTheme()
     */
        public SupportedPropertyPath<String> titleWithTheme()
        {
            if(_titleWithTheme == null )
                _titleWithTheme = new SupportedPropertyPath<String>(DiplomaContentRowGen.P_TITLE_WITH_THEME, this);
            return _titleWithTheme;
        }

        public Class getEntityClass()
        {
            return DiplomaContentRow.class;
        }

        public String getEntityName()
        {
            return "diplomaContentRow";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract Double getCommonLoad();

    public abstract String getRowLoadTitle();

    public abstract String getTermAsString();

    public abstract String getTitleWithTheme();
}
