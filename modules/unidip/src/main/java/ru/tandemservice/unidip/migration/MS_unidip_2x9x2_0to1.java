package ru.tandemservice.unidip.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unidip_2x9x2_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность dipFormingRowAlgorithmOrgUnit

        short entityCode;
		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("dip_form_row_algorithm_ou_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_3dfd7880"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("ownerorgunit_id", DBType.LONG).setNullable(false), 
				new DBColumn("dipformingrowalgorithm_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			entityCode = tool.entityCodes().ensure("dipFormingRowAlgorithmOrgUnit");
		}

        PreparedStatement insert = tool.prepareStatement("insert into dip_form_row_algorithm_ou_t (id, discriminator, ownerorgunit_id, dipformingrowalgorithm_id) values (?, ? ,?, ?)");
        Long currentAlgId = (Long) tool.getUniqueResult("select id from dip_form_row_algorithm_t where currentAlg_p = ?", true);

        Statement stmt = tool.getConnection().createStatement();
        stmt.execute("select id from edu_ourole__owner_t");
        ResultSet src = stmt.getResultSet();

        while (src.next())
        {
            Long ownerOuId = src.getLong(1);

            insert.setLong(1, EntityIDGenerator.generateNewId(entityCode));
            insert.setShort(2, entityCode);
            insert.setLong(3, ownerOuId);
            insert.setLong(4, currentAlgId);
            insert.execute();
        }
    }
}