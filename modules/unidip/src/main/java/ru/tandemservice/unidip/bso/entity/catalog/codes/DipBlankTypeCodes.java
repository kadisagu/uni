package ru.tandemservice.unidip.bso.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Тип бланка"
 * Имя сущности : dipBlankType
 * Файл data.xml : unidip-catalogs.data.xml
 */
public interface DipBlankTypeCodes
{
    /** Константа кода (code) элемента : Диплом о среднем профессиональном образовании (title) */
    String SPO_BLANK = "spoBlank";
    /** Константа кода (code) элемента : Диплом о среднем профессиональном образовании с отличием (title) */
    String SPO_WITH_HONOUR_BLANK = "spoWithHonourBlank";
    /** Константа кода (code) элемента : Диплом бакалавра (title) */
    String BACHELOR_BLANK = "bachelorBlank";
    /** Константа кода (code) элемента : Диплом бакалавра с отличием (title) */
    String BACHELOR_WITH_HONOUR_BLANK = "bachelorWithHonourBlank";
    /** Константа кода (code) элемента : Диплом специалиста (title) */
    String SPECIALTY_BLANK = "specialityBlank";
    /** Константа кода (code) элемента : Диплом специалиста с отличием (title) */
    String SPECIALTY_WITH_HONOUR_BLANK = "specialityWithHonourBlank";
    /** Константа кода (code) элемента : Диплом магистра (title) */
    String MASTER_BLANK = "masterBlank";
    /** Константа кода (code) элемента : Диплом магистра с отличием (title) */
    String MASTER_WITH_HONOUR_BLANK = "masterWithHonourBlank";
    /** Константа кода (code) элемента : Диплом об окончании аспирантуры (title) */
    String POST_GRADUATE_BLANK = "postGraduateBlank";
    /** Константа кода (code) элемента : Диплом об окончании адъюнктуры (title) */
    String ADJUNCTURE_BLANK = "adjunctureBlank";
    /** Константа кода (code) элемента : Приложение к диплому о среднем профессиональном образовании (title) */
    String APPENDIX_FOR_SPO_BLANK = "appendixForSpoBlank";
    /** Константа кода (code) элемента : Приложение к диплому бакалавра, диплому бакалавра с отличием, диплому специалиста, диплому специалиста с отличием (title) */
    String APPENDIX_FOR_BACH_SPEC_BLANK = "appendixForBachSpecBlank";
    /** Константа кода (code) элемента : Приложение к диплому магистра, диплому магистра с отличием (title) */
    String APPENDIX_FOR_MASTER_BLANK = "appendixForMasterBlank";
    /** Константа кода (code) элемента : Приложение к диплому об окончании аспирантуры (title) */
    String APPENDIX_FOR_POST_GRAD_BLANK = "appendixForPostGradBlank";
    /** Константа кода (code) элемента : Приложение к диплому об окончании адъюнктуры (title) */
    String APPENDIX_FOR_ADJUNCTURE_BLANK = "appendixForAdjunctureBlank";

    Set<String> CODES = ImmutableSet.of(SPO_BLANK, SPO_WITH_HONOUR_BLANK, BACHELOR_BLANK, BACHELOR_WITH_HONOUR_BLANK, SPECIALTY_BLANK, SPECIALTY_WITH_HONOUR_BLANK, MASTER_BLANK, MASTER_WITH_HONOUR_BLANK, POST_GRADUATE_BLANK, ADJUNCTURE_BLANK, APPENDIX_FOR_SPO_BLANK, APPENDIX_FOR_BACH_SPEC_BLANK, APPENDIX_FOR_MASTER_BLANK, APPENDIX_FOR_POST_GRAD_BLANK, APPENDIX_FOR_ADJUNCTURE_BLANK);
}
