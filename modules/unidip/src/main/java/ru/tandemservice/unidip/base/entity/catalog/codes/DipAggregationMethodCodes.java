package ru.tandemservice.unidip.base.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Алгоритм расчета оценок для документа об обучении"
 * Имя сущности : dipAggregationMethod
 * Файл data.xml : unidip-catalogs.data.xml
 */
public interface DipAggregationMethodCodes
{
    /** Константа кода (code) элемента : Последняя оценка (title) */
    String POSLEDNYAYA_OTSENKA = "2";

    Set<String> CODES = ImmutableSet.of(POSLEDNYAYA_OTSENKA);
}
