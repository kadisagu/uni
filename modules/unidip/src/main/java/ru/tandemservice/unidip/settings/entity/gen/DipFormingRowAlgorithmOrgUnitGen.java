package ru.tandemservice.unidip.settings.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unidip.settings.entity.DipFormingRowAlgorithm;
import ru.tandemservice.unidip.settings.entity.DipFormingRowAlgorithmOrgUnit;
import ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Алгоритм формирования строк в документе об обучении на подразделении
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DipFormingRowAlgorithmOrgUnitGen extends EntityBase
 implements INaturalIdentifiable<DipFormingRowAlgorithmOrgUnitGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unidip.settings.entity.DipFormingRowAlgorithmOrgUnit";
    public static final String ENTITY_NAME = "dipFormingRowAlgorithmOrgUnit";
    public static final int VERSION_HASH = 565481657;
    private static IEntityMeta ENTITY_META;

    public static final String L_OWNER_ORG_UNIT = "ownerOrgUnit";
    public static final String L_DIP_FORMING_ROW_ALGORITHM = "dipFormingRowAlgorithm";

    private EduOwnerOrgUnit _ownerOrgUnit;     // Выпускающее подразделение
    private DipFormingRowAlgorithm _dipFormingRowAlgorithm;     // Алгоритм формирования строк документа об обучении

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выпускающее подразделение. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EduOwnerOrgUnit getOwnerOrgUnit()
    {
        return _ownerOrgUnit;
    }

    /**
     * @param ownerOrgUnit Выпускающее подразделение. Свойство не может быть null и должно быть уникальным.
     */
    public void setOwnerOrgUnit(EduOwnerOrgUnit ownerOrgUnit)
    {
        dirty(_ownerOrgUnit, ownerOrgUnit);
        _ownerOrgUnit = ownerOrgUnit;
    }

    /**
     * @return Алгоритм формирования строк документа об обучении.
     */
    public DipFormingRowAlgorithm getDipFormingRowAlgorithm()
    {
        return _dipFormingRowAlgorithm;
    }

    /**
     * @param dipFormingRowAlgorithm Алгоритм формирования строк документа об обучении.
     */
    public void setDipFormingRowAlgorithm(DipFormingRowAlgorithm dipFormingRowAlgorithm)
    {
        dirty(_dipFormingRowAlgorithm, dipFormingRowAlgorithm);
        _dipFormingRowAlgorithm = dipFormingRowAlgorithm;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof DipFormingRowAlgorithmOrgUnitGen)
        {
            if (withNaturalIdProperties)
            {
                setOwnerOrgUnit(((DipFormingRowAlgorithmOrgUnit)another).getOwnerOrgUnit());
            }
            setDipFormingRowAlgorithm(((DipFormingRowAlgorithmOrgUnit)another).getDipFormingRowAlgorithm());
        }
    }

    public INaturalId<DipFormingRowAlgorithmOrgUnitGen> getNaturalId()
    {
        return new NaturalId(getOwnerOrgUnit());
    }

    public static class NaturalId extends NaturalIdBase<DipFormingRowAlgorithmOrgUnitGen>
    {
        private static final String PROXY_NAME = "DipFormingRowAlgorithmOrgUnitNaturalProxy";

        private Long _ownerOrgUnit;

        public NaturalId()
        {}

        public NaturalId(EduOwnerOrgUnit ownerOrgUnit)
        {
            _ownerOrgUnit = ((IEntity) ownerOrgUnit).getId();
        }

        public Long getOwnerOrgUnit()
        {
            return _ownerOrgUnit;
        }

        public void setOwnerOrgUnit(Long ownerOrgUnit)
        {
            _ownerOrgUnit = ownerOrgUnit;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof DipFormingRowAlgorithmOrgUnitGen.NaturalId) ) return false;

            DipFormingRowAlgorithmOrgUnitGen.NaturalId that = (NaturalId) o;

            if( !equals(getOwnerOrgUnit(), that.getOwnerOrgUnit()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getOwnerOrgUnit());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getOwnerOrgUnit());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DipFormingRowAlgorithmOrgUnitGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DipFormingRowAlgorithmOrgUnit.class;
        }

        public T newInstance()
        {
            return (T) new DipFormingRowAlgorithmOrgUnit();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "ownerOrgUnit":
                    return obj.getOwnerOrgUnit();
                case "dipFormingRowAlgorithm":
                    return obj.getDipFormingRowAlgorithm();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "ownerOrgUnit":
                    obj.setOwnerOrgUnit((EduOwnerOrgUnit) value);
                    return;
                case "dipFormingRowAlgorithm":
                    obj.setDipFormingRowAlgorithm((DipFormingRowAlgorithm) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "ownerOrgUnit":
                        return true;
                case "dipFormingRowAlgorithm":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "ownerOrgUnit":
                    return true;
                case "dipFormingRowAlgorithm":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "ownerOrgUnit":
                    return EduOwnerOrgUnit.class;
                case "dipFormingRowAlgorithm":
                    return DipFormingRowAlgorithm.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DipFormingRowAlgorithmOrgUnit> _dslPath = new Path<DipFormingRowAlgorithmOrgUnit>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DipFormingRowAlgorithmOrgUnit");
    }
            

    /**
     * @return Выпускающее подразделение. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unidip.settings.entity.DipFormingRowAlgorithmOrgUnit#getOwnerOrgUnit()
     */
    public static EduOwnerOrgUnit.Path<EduOwnerOrgUnit> ownerOrgUnit()
    {
        return _dslPath.ownerOrgUnit();
    }

    /**
     * @return Алгоритм формирования строк документа об обучении.
     * @see ru.tandemservice.unidip.settings.entity.DipFormingRowAlgorithmOrgUnit#getDipFormingRowAlgorithm()
     */
    public static DipFormingRowAlgorithm.Path<DipFormingRowAlgorithm> dipFormingRowAlgorithm()
    {
        return _dslPath.dipFormingRowAlgorithm();
    }

    public static class Path<E extends DipFormingRowAlgorithmOrgUnit> extends EntityPath<E>
    {
        private EduOwnerOrgUnit.Path<EduOwnerOrgUnit> _ownerOrgUnit;
        private DipFormingRowAlgorithm.Path<DipFormingRowAlgorithm> _dipFormingRowAlgorithm;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выпускающее подразделение. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unidip.settings.entity.DipFormingRowAlgorithmOrgUnit#getOwnerOrgUnit()
     */
        public EduOwnerOrgUnit.Path<EduOwnerOrgUnit> ownerOrgUnit()
        {
            if(_ownerOrgUnit == null )
                _ownerOrgUnit = new EduOwnerOrgUnit.Path<EduOwnerOrgUnit>(L_OWNER_ORG_UNIT, this);
            return _ownerOrgUnit;
        }

    /**
     * @return Алгоритм формирования строк документа об обучении.
     * @see ru.tandemservice.unidip.settings.entity.DipFormingRowAlgorithmOrgUnit#getDipFormingRowAlgorithm()
     */
        public DipFormingRowAlgorithm.Path<DipFormingRowAlgorithm> dipFormingRowAlgorithm()
        {
            if(_dipFormingRowAlgorithm == null )
                _dipFormingRowAlgorithm = new DipFormingRowAlgorithm.Path<DipFormingRowAlgorithm>(L_DIP_FORMING_ROW_ALGORITHM, this);
            return _dipFormingRowAlgorithm;
        }

        public Class getEntityClass()
        {
            return DipFormingRowAlgorithmOrgUnit.class;
        }

        public String getEntityName()
        {
            return "dipFormingRowAlgorithmOrgUnit";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
