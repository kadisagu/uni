/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unidip.base.bo.DipDocument.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.core.view.formatter.NewLineFormatter;
import org.tandemframework.core.view.list.column.DefaultPublisherLinkResolver;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import ru.tandemservice.unidip.base.bo.DipDocument.logic.DipDocumentStudentDSHandler;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContent;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaObject;
import ru.tandemservice.unidip.base.entity.order.DipStuExcludeExtract;

/**
 * @author iolshvang
 * @since 13.07.11 18:38
 */
@Configuration
public class DipDocumentList extends BusinessComponentManager
{
    public static final String DIP_DOCUMENT_DS = "dipDocumentDS";

	@Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(this.searchListDS(DIP_DOCUMENT_DS, dipDocumentDSColumns(), dipDocumentDSHandler()))
                .create();
    }

	private static final IPublisherLinkResolver excludeOrderLinkResolver = new DefaultPublisherLinkResolver()
	{
		@Override
		public Object getParameters(IEntity entity)
		{
			DipStuExcludeExtract excludeOrderForDiploma = (DipStuExcludeExtract) entity.getProperty(DipDocumentStudentDSHandler.EXCLUDE_ORDER);
			return ParametersMap.createWith(PublisherActivator.PUBLISHER_ID_KEY, excludeOrderForDiploma.getParagraph().getOrder().getId());
		}
	};

	private static final IPublisherLinkResolver epvLinkResolver = new DefaultPublisherLinkResolver()
	{
		@Override
		public Object getParameters(IEntity entity)
		{
			DiplomaObject diplomaObject = ((DataWrapper)entity).getWrapped();
			return (diplomaObject.getStudentEpv() == null) ? null : ParametersMap.createWith(PublisherActivator.PUBLISHER_ID_KEY, diplomaObject.getStudentEpv().getEduPlanVersion().getId());
		}
	};

	private static final IFormatter contentFormatter = source ->
	{
		DiplomaContent content = (DiplomaContent) source;
		if (content.getProgramSubject() == null)
			return "";

		String title = " «" + content.getProgramSubject().getTitleWithCode() + "»";
		if (content.getProgramSpecialization() != null)
			title += " (" + content.getProgramSpecialization().getTitle() + ")";

		return title;
	};

	private static final FormattedMessage deleteAlertMessage = FormattedMessage.with().template("dipDocumentDS.delete.alert")
			.parameter(DiplomaObject.titleWithSuccess())
			.parameter(DiplomaObject.content(), contentFormatter).create();

	@Bean
    public ColumnListExtPoint dipDocumentDSColumns()
    {
		return columnListExtPointBuilder(DIP_DOCUMENT_DS)
                .addColumn(publisherColumn("type", DiplomaObject.titleWithSuccess()).order().width("224px"))
                .addColumn(textColumn(DipDocumentStudentDSHandler.REGISTRATION_NUMBER, DipDocumentStudentDSHandler.REGISTRATION_NUMBER).formatter(NewLineFormatter.NOBR_IN_LINES).width("111px"))
                .addColumn(textColumn(DipDocumentStudentDSHandler.ISSUANCE_DATE, DipDocumentStudentDSHandler.ISSUANCE_DATE).formatter(NewLineFormatter.NOBR_IN_LINES).order().width("111px"))
                .addColumn(textColumn("educationElementTitle", DiplomaObject.content().programSubject().titleWithCode()).order().width("224px"))
                .addColumn(textColumn("programSpecialization", DiplomaObject.content().programSpecialization().title()).width("224px"))
				.addColumn(publisherColumn(DipDocumentStudentDSHandler.COLUMN_EDU_PLAN_VERSION, DiplomaObject.studentEpv().eduPlanVersion().fullTitleWithEducationsCharacteristics()).order().publisherLinkResolver(epvLinkResolver))
                .addColumn(publisherColumn(DipDocumentStudentDSHandler.EXCLUDE_ORDER_TITLE, DipDocumentStudentDSHandler.EXCLUDE_ORDER_TITLE).publisherLinkResolver(excludeOrderLinkResolver).width("111px"))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).disabled(DipDocumentStudentDSHandler.EDIT_DISABLED).permissionKey("editDipDocumentFromList"))
				.addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER, deleteAlertMessage).permissionKey("deleteDipDocumentFromList"))
				.create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> dipDocumentDSHandler()
    {
        return new DipDocumentStudentDSHandler(getName(), false);
    }
}
