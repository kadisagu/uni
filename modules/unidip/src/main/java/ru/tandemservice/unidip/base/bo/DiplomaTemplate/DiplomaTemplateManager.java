/* $Id$ */
package ru.tandemservice.unidip.base.bo.DiplomaTemplate;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;

import ru.tandemservice.unidip.base.bo.DiplomaTemplate.logic.DiplomaTemplateDao;
import ru.tandemservice.unidip.base.bo.DiplomaTemplate.logic.IDiplomaTemplateDao;
import ru.tandemservice.unidip.base.entity.catalog.gen.DipDocumentTypeGen;

/**
 * @author Vasily Zhukov
 * @since 05.05.2011
 */
@Configuration
public class DiplomaTemplateManager extends BusinessObjectManager
{
    @Bean
    public IDefaultComboDataSourceHandler dipDocumentTypeComboDSHandler()
    {
        return (IDefaultComboDataSourceHandler) new SimpleTitledComboDataSourceHandler(getName()).filtered(true).setFilterByProperty(DipDocumentTypeGen.CATALOG_ITEM_TITLE);
    }

    @Bean
    public IDefaultComboDataSourceHandler disciplineComboDSHandler()
    {
        return (IDefaultComboDataSourceHandler) new SimpleTitledComboDataSourceHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler fcaComboDSHandler()
    {
        return (IDefaultComboDataSourceHandler) new SimpleTitledComboDataSourceHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler markComboDSHandler()
    {
        return (IDefaultComboDataSourceHandler) new SimpleTitledComboDataSourceHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler markSourceComboDSHandler()
    {
        return (IDefaultComboDataSourceHandler) new SimpleTitledComboDataSourceHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler dipDocumentLineComboDSHandler()
    {
        return (IDefaultComboDataSourceHandler) new SimpleTitledComboDataSourceHandler(getName());
    }

    public static DiplomaTemplateManager instance()
    {
        return instance(DiplomaTemplateManager.class);
    }

    @Bean
    public IDiplomaTemplateDao dao()
    {
        return new DiplomaTemplateDao();
    }


}
