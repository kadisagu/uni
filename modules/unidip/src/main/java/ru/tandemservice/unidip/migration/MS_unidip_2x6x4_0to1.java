package ru.tandemservice.unidip.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unidip_2x6x4_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.4")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность dipAggregationMethod

		// таблица сущности переименована
		{
			// переименовать таблицу
			tool.renameTable("dip_c_aggregation", "dip_c_aggregation_t");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность dipDocumentType

		// таблица сущности переименована
		{
			// переименовать таблицу
			tool.renameTable("dip_c_type", "dip_c_type_t");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность diplomaAcademyRenameData

		// таблица сущности переименована
		{
			// переименовать таблицу
			tool.renameTable("diploma_academy_rename_data", "dip_academy_rename_data_t");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность diplomaContent

		// таблица сущности переименована
		{
			// переименовать таблицу
			tool.renameTable("diploma_content", "dip_content_t");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность diplomaContentIssuance

		// таблица сущности переименована
		{
			// переименовать таблицу
			tool.renameTable("diploma_content_issuance_t", "dip_content_issuance_t");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность diplomaContentRegElPartFControlAction

		// таблица сущности переименована
		{
			// переименовать таблицу
			tool.renameTable("diploma_rowrepfca", "dip_row_rfca_t");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность diplomaContentRow

		// таблица сущности переименована
		{
			// переименовать таблицу
			tool.renameTable("diploma_content_row", "dip_row_t");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность diplomaCourseWorkRow

		// таблица сущности переименована
		{
			// переименовать таблицу
			tool.renameTable("dip_c_work_row", "dip_row_c_work_t");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность diplomaDisciplineRow

		// таблица сущности переименована
		{
			// переименовать таблицу
			tool.renameTable("dip_disc_row", "dip_row_disc_t");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность diplomaOptDisciplineRow

		// таблица сущности переименована
		{
			// переименовать таблицу
			tool.renameTable("dip_opt_disc_row", "dip_row_opt_t");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность diplomaPracticeRow

		// таблица сущности переименована
		{
			// переименовать таблицу
			tool.renameTable("dip_practice_row", "dip_row_practice_t");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность diplomaQualifWorkRow

		// таблица сущности переименована
		{
			// переименовать таблицу
			tool.renameTable("dip_qualif_work_row", "dip_row_qual_work_t");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность diplomaStateExamRow

		// таблица сущности переименована
		{
			// переименовать таблицу
			tool.renameTable("dip_state_exam_row", "dip_row_state_exam_t");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность diplomaEpvTemplateDefaultRel

		// таблица сущности переименована
		{
			// переименовать таблицу
			tool.renameTable("dip_epv_template_rel", "dip_template_epv_def_t");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность diplomaIssuance

		// таблица сущности переименована
		{
			// переименовать таблицу
			tool.renameTable("diploma_issuance_t", "dip_issuance_t");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность diplomaObject

		// таблица сущности переименована
		{
			// переименовать таблицу
			tool.renameTable("diploma_object", "dip_object_t");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность diplomaTemplate

		// таблица сущности переименована
		{
			// переименовать таблицу
			tool.renameTable("diploma_template", "dip_template_t");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность dipDocTemplateCatalog

		// таблица сущности переименована
		{
			// переименовать таблицу
			tool.renameTable("dip_doc_template_catalog", "dip_c_doc_template_t");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность dipStuExcludeExtract

		// таблица сущности переименована
		{
			// переименовать таблицу
			tool.renameTable("dip_stu_exclude_extract", "dip_stu_exclude_extract_t");

		}


    }
}