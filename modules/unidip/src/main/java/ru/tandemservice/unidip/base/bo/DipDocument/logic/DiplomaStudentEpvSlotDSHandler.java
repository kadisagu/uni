/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unidip.base.bo.DipDocument.logic;

import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Multimap;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unidip.base.bo.DipDocument.DipDocumentManager;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRegElPartFControlAction;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaObject;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Таблица "Дисциплины и мероприятия рабочего учебного плана" на карточке диплома.
 * Строки в большинстве типов документов формируются на основе актуальных МСРП по форме итогового контроля (МСРП-ФК) таких,
 * что УП студента, ссылается на соответствующего студента и версию УП, в которой есть строка, 
 * ссылающаяся на элемент реестра из МСРП, не имеющих связи со строкой документа об обучении.
 * При этом для справок об обучении дополнительно проверяется, что для МСРП-ФК есть положительная итоговая оценка,
 * а для справки об обучении для неактивного студента остаются, напротив, только неактуальные МСРП-ФК
 * (в справках для активных студентов остаются актуальные МСРП-ФК, как и в обычных документах).
 *
 * @author iolshvang
 * @since 06.08.11
 */
public class DiplomaStudentEpvSlotDSHandler extends DefaultSearchDataSourceHandler
{
    public static final String DIPLOMA_OBJECT = "diplomaObject";
    public static final String NUMBER_COLUMN = "numberColumn";
    public static final String REGISTRY_COLUMN = "registryColumn";
    public static final String OTHER_DISCIPLINES_COLUMN = "otherDisciplinesColumn";
    public static final String CONTROL_FORM_COLUMN = "controlFormColumn";
    public static final String GRID_TERM_COLUMN = "gridTermColumn";
    public static final String YEAR_COLUMN = "yearColumn";
    public static final String KIND_COLUMN = "kindColumn";

    public DiplomaStudentEpvSlotDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final DiplomaObject diplomaObject = context.get(DIPLOMA_OBJECT);

        final String wpeCActionAlias = "wpeCAction";
        final String wpeAlias = "wpe";
        final Student student = diplomaObject.getStudent();
        final DQLSelectBuilder otherListBuilder = new DQLSelectBuilder().fromEntity(EppStudentWpeCAction.class, wpeCActionAlias)
                .column(property(wpeCActionAlias))
                .joinPath(DQLJoinType.inner, EppStudentWpeCAction.studentWpe().fromAlias(wpeCActionAlias), wpeAlias)
                .joinEntity(wpeCActionAlias, DQLJoinType.inner, EppRegistryElementPartFControlAction.class, "p",
                        eq(property(wpeAlias, EppStudentWorkPlanElement.registryElementPart()), property("p", EppRegistryElementPartFControlAction.part()))
                )
                .where(eq(property(wpeCActionAlias, EppStudentWpeCAction.type()), property("p", EppRegistryElementPartFControlAction.controlAction().eppGroupType())))
                .where(eq(property(wpeCActionAlias, EppStudentWpeCAction.studentWpe().student()), value(student)))
                .where(not(exists(
                        DiplomaContentRegElPartFControlAction.class,
                        DiplomaContentRegElPartFControlAction.registryElementPartFControlAction().s(), property("p"),
                        DiplomaContentRegElPartFControlAction.row().owner().s(), diplomaObject.getContent()
                )))
                .order(property(wpeCActionAlias, EppStudentWpeCAction.studentWpe().registryElementPart().registryElement().title()))
                .order(property(wpeCActionAlias, EppStudentWpeCAction.type().priority()));

		IDipDocumentDao dao = DipDocumentManager.instance().dao();
        // Для справок об обучении (и их наследников) дополнительно фильтруем по наличию положительных итоговых оценок и актуальности (фильтр по актуальности зависит от активности студента)
        if (dao.isEducationReference(diplomaObject.getContent().getType()))
            dao.filterWpePartsAtEducationReference(otherListBuilder, wpeCActionAlias, wpeAlias, student.getStatus());
        else
            otherListBuilder.where(isNull(property(wpeCActionAlias, EppStudentWpeCAction.studentWpe().removalDate())));


        final Multimap<EppStudentWorkPlanElement, String> controlActionMap = LinkedListMultimap.create();
        for (EppStudentWpeCAction row : otherListBuilder.createStatement(context.getSession()).<EppStudentWpeCAction>list())
        {
            controlActionMap.put(row.getStudentWpe(), row.getType().getTitle());
        }

        final List<DataWrapper> recordList = new ArrayList<>();
        for (Map.Entry<EppStudentWorkPlanElement, Collection<String>> studentEpvSlotEntry : controlActionMap.asMap().entrySet())
        {
            final EppStudentWorkPlanElement slot = studentEpvSlotEntry.getKey();
            final EppWorkPlanRow sourceRow = slot.getSourceRow();

            final DataWrapper record = new DataWrapper(slot);
            record.setProperty(NUMBER_COLUMN, sourceRow == null ? "" : sourceRow.getNumber());
            record.setProperty(OTHER_DISCIPLINES_COLUMN, slot.getRegistryElementPart().getTitle());
            record.setProperty(REGISTRY_COLUMN, slot.getRegistryElementPart().getRegistryElement());
            record.setProperty(CONTROL_FORM_COLUMN, CommonBaseStringUtil.joinUnique(studentEpvSlotEntry.getValue(), ", "));
            record.setProperty(GRID_TERM_COLUMN, slot.getTerm().getIntValue());
            record.setProperty(YEAR_COLUMN, slot.getYear().getTitle());
            record.setProperty(KIND_COLUMN, sourceRow == null ? "" : sourceRow.getKind().getTitle());
            recordList.add(record);
        }

        final DSOutput output = ListOutputBuilder.get(input, recordList).build();
        output.setCountRecord(Math.max(recordList.size(), 1));
        return output;
    }
}