package ru.tandemservice.unidip.base.bo.DipDocument.ui.ListInEpvTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.unidip.base.bo.DipDocument.DipDocumentManager;
import ru.tandemservice.unidip.base.bo.DipDocument.logic.DipDocumentStudentDSHandler;
import ru.tandemservice.unidip.base.bo.DipDocument.logic.DipDocumentTypeSelectModel;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;

import java.util.Map;

/**
 * @author avedernikov
 * @since 29.11.2016
 */
@State({
		@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "eduPlanVersion.id")
})
public class DipDocumentListInEpvTabUI extends UIPresenter
{
	private EppEduPlanVersion eduPlanVersion = new EppEduPlanVersion();

	private ISingleSelectModel documentTypeModel;

	private static final String ResetSuccessfulMessage = "Документы об обучении обновлены.";
	private static final String ResetSuccessfulNeedDeleteTemplateRowsMessage = "Документы об обучении обновлены. Для возможности удаления строки УП удалите строки из шаблона.";
	private static final String ResetFailedMessage = "Автоматическое обновление строк документов об обучении невозможно. Попробуйте вручную.";

	@Override
	public void onBeforeDataSourceFetch(IUIDataSource dataSource)
	{
		switch (dataSource.getName())
		{
			case DipDocumentListInEpvTab.DIP_DOCUMENT_DS:
			{
				Map<String, Object> settingsMap = getSettings().getAsMap(
						DipDocumentStudentDSHandler.LAST_NAME,
						DipDocumentStudentDSHandler.FIRST_NAME,
						DipDocumentStudentDSHandler.REG_NUMBER,
						DipDocumentStudentDSHandler.ISSUE_DATE_FROM,
						DipDocumentStudentDSHandler.ISSUE_DATE_TO,
						DipDocumentStudentDSHandler.DOCUMENT_TYPE,
						DipDocumentStudentDSHandler.WITH_SUCCESS,
						DipDocumentStudentDSHandler.BLANK_SERIA,
						DipDocumentStudentDSHandler.BLANK_NUMBER,
						DipDocumentStudentDSHandler.ORG_UNIT,
						DipDocumentStudentDSHandler.PROGRAM_SUBJECT
				);
				dataSource.putAll(settingsMap);
				dataSource.put(DipDocumentStudentDSHandler.EDU_PLAN_VERSION, eduPlanVersion);
				break;
			}
		}
	}

	@Override
	public void onComponentRefresh()
	{
		eduPlanVersion = DataAccessServices.dao().getNotNull(eduPlanVersion.getId());
		documentTypeModel = new DipDocumentTypeSelectModel(null, true);
	}

	public void onClickResetEpvRegistryRow()
	{
		boolean success = DipDocumentManager.instance().dao().resetEpvRegistryRows(eduPlanVersion);
		ContextLocal.getInfoCollector().add(getAfterResetMessage(success));
	}

	/**
	 * Сообщение, которое выводится после сбрасывания связи строк диплома со строками УПв. Если связи успешно удалены,
	 * то выводится либо {@link DipDocumentListInEpvTabUI#ResetSuccessfulMessage} (если нет строк в шаблонах диплома для УПв),
	 * либо {@link DipDocumentListInEpvTabUI#ResetSuccessfulNeedDeleteTemplateRowsMessage} (если такие строки есть).
	 * Если неуспешно, то {@link DipDocumentListInEpvTabUI#ResetFailedMessage}.
	 * @param resetSuccessful Успешность сброса строк.
	 */
	private String getAfterResetMessage(boolean resetSuccessful)
	{
		if (!resetSuccessful)
			return ResetFailedMessage;

		boolean existsTemplateRowsToDelete = DipDocumentManager.instance().dao().isExistsTemplateRowOfEpv(eduPlanVersion);
		if (existsTemplateRowsToDelete)
			return ResetSuccessfulNeedDeleteTemplateRowsMessage;
		return ResetSuccessfulMessage;
	}

	public EppEduPlanVersion getEduPlanVersion()
	{
		return eduPlanVersion;
	}

	public void setEduPlanVersion(EppEduPlanVersion eduPlanVersion)
	{
		this.eduPlanVersion = eduPlanVersion;
	}

	public ISingleSelectModel getDocumentTypeModel()
	{
		return documentTypeModel;
	}

	public void setDocumentTypeModel(ISingleSelectModel documentTypeModel)
	{
		this.documentTypeModel = documentTypeModel;
	}
}