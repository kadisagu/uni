package ru.tandemservice.unidip.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unidip_2x8x0_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность dipDiplomaSendFisFrdoReport

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("dip_diploma_fis_frdo_report_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_dipdiplomasendfisfrdoreport"), 
				new DBColumn("edulevel_id", DBType.LONG),
				new DBColumn("documenttype_id", DBType.LONG), 
				new DBColumn("issuancedateto_p", DBType.DATE), 
				new DBColumn("issuancedatefrom_p", DBType.DATE), 
				new DBColumn("duplicatediplomaobject_p", DBType.BOOLEAN).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("dipDiplomaSendFisFrdoReport");
		}
    }
}