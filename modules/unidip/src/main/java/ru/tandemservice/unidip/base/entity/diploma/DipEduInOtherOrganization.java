package ru.tandemservice.unidip.base.entity.diploma;

import ru.tandemservice.unidip.base.entity.diploma.gen.*;
import ru.tandemservice.uniepp.UniEppUtils;

/**
 * Обучение в другой организации
 */
public class DipEduInOtherOrganization extends DipEduInOtherOrganizationGen
{
    public Double getTotalCreditsAsDouble()
    {
        Long creditsAsLong = this.getTotalCreditsAsLong();
        return UniEppUtils.wrap((null == creditsAsLong || creditsAsLong < 0) ? null : creditsAsLong);
    }

    public void setTotalCreditsAsDouble(final Double value)
    {
        Long creditsAsLong = UniEppUtils.unwrap(value);
        this.setTotalCreditsAsLong(creditsAsLong);
    }

    public Double getTotalWeeksAsDouble()
    {
        Long totalWeeksAsLong = this.getTotalWeeksAsLong();
        return UniEppUtils.wrap((null == totalWeeksAsLong || totalWeeksAsLong < 0) ? null : totalWeeksAsLong);
    }

    public void setTotalWeeksAsDouble(final Double value)
    {
        Long totalWeeksAsLong = UniEppUtils.unwrap(value);
        this.setTotalWeeksAsLong(totalWeeksAsLong);
    }
}