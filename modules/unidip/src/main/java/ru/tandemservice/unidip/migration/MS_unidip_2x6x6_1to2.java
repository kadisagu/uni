package ru.tandemservice.unidip.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unidip_2x6x6_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.6")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность dipAddInfoEduForm

		// создана новая сущность
        {
            // создать таблицу
            if (!tool.tableExists("dip_add_info_edu_form_t"))
            {
                DBTable dbt = new DBTable("dip_add_info_edu_form_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                          new DBColumn("dipadditionalinformation_id", DBType.LONG).setNullable(false),
                                          new DBColumn("eduprogramform_id", DBType.LONG)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("dipAddInfoEduForm");

            }
        }
		////////////////////////////////////////////////////////////////////////////////
		// сущность dipAdditionalInformation

		// создана новая сущность
		{
			// создать таблицу
            if (!tool.tableExists("dip_additional_information_t"))
            {
                DBTable dbt = new DBTable("dip_additional_information_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                          new DBColumn("diplomaobject_id", DBType.LONG).setNullable(false),
                                          new DBColumn("showadditionaldiscipline_p", DBType.BOOLEAN).setNullable(false),
                                          new DBColumn("showselfeduform_p", DBType.BOOLEAN).setNullable(false),
                                          new DBColumn("showprogramspecialization_p", DBType.BOOLEAN).setNullable(false),
                                          new DBColumn("specialization_p", DBType.createVarchar(255)),
                                          new DBColumn("passintenssivetraining_p", DBType.BOOLEAN).setNullable(false)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("dipAdditionalInformation");
            }
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность dipAssistantManager

		// таблица сущности переименована
		{
			// переименовать таблицу
            if (!tool.tableExists("dip_assistant_manager_t") && tool.tableExists("dip_assistant_manager"))
            {
                tool.renameTable("dip_assistant_manager", "dip_assistant_manager_t");
            }
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность dipEduInOtherOrganization

		// создана новая сущность
		{
			// создать таблицу
            if (!tool.tableExists("dip_edu_other_organization_t"))
            {
                DBTable dbt = new DBTable("dip_edu_other_organization_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                          new DBColumn("dipadditionalinformation_id", DBType.LONG).setNullable(false),
                                          new DBColumn("totalcreditsaslong_p", DBType.LONG),
                                          new DBColumn("totalweeksaslong_p", DBType.LONG),
                                          new DBColumn("row_id", DBType.LONG).setNullable(false)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("dipEduInOtherOrganization");
            }
		}


    }
}