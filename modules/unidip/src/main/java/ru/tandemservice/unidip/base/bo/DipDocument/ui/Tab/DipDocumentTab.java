/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unidip.base.bo.DipDocument.ui.Tab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import ru.tandemservice.unidip.base.bo.DipDocument.ui.List.DipDocumentList;

/**
 *
 * @author iolshvang
 * @since 25.07.11 12:02
 */
@Configuration
public class DipDocumentTab  extends BusinessComponentManager
{
    // tab panel
    public static final String TAB_PANEL = "tabPanel";

    // tab names
    public static final String DOCUMENT_LIST_TAB = "documentListTab";

    @Bean
    public TabPanelExtPoint tabPanelExtPoint()
    {
        return tabPanelExtPointBuilder(TAB_PANEL)
        .addTab(componentTab(DOCUMENT_LIST_TAB, DipDocumentList.class).permissionKey("viewStudentDipDocumentTab").create())
        .create();
    }
}