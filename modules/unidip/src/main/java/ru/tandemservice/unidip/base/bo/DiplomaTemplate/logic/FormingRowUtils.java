/* $Id$ */
package ru.tandemservice.unidip.base.bo.DiplomaTemplate.logic;

import com.google.common.collect.ImmutableMap;
import ru.tandemservice.unidip.base.entity.diploma.*;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.catalog.codes.EppRegistryStructureCodes;
import ru.tandemservice.uniepp.util.EppFControlActionInfo;

import java.util.*;

/**
 * @author Nikolay Fedorovskih
 * @since 04.06.2015
 */
public class FormingRowUtils
{
    private static final Map<String, Class<? extends DiplomaContentRow>> _ROW_CLASS_MAP = ImmutableMap.of(
            EppRegistryStructureCodes.REGISTRY_DISCIPLINE, DiplomaDisciplineRow.class,
            EppRegistryStructureCodes.REGISTRY_PRACTICE, DiplomaPracticeRow.class,
            EppRegistryStructureCodes.REGISTRY_ATTESTATION_EXAM, DiplomaStateExamRow.class,
            EppRegistryStructureCodes.REGISTRY_ATTESTATION_DIPLOMA, DiplomaQualifWorkRow.class
    );

    /** @return тип строки диплома, определенный по типу элемента реестра */
    public static Class<? extends DiplomaContentRow> getDiplomaRowClass(EppRegistryStructure type)
    {
        while (type != null) {
            final Class<? extends DiplomaContentRow> clazz = _ROW_CLASS_MAP.get(type.getCode());
            if (clazz != null) { return clazz; }
            type = type.getParent();
        }
        return null;
    }

    /**
     * Вычисление семестра строки - берется последний, в котором есть экзамен.
     * Если ни одного семестра с экзаменом нет, то берется последний семестр с ФИК.
     * Если вообще ни одного ФИК нет, то берется последний семестр из всех.
     *
     * @param term2fcaFullCodes распределение ФИК по семестрам. В каких-то семестрах ФИКов вполне может не быть.
     */
    public static Integer getRowTerm(Map<Integer, Set<String>> term2fcaFullCodes)
    {
        Integer lastTerm = 0;
        Integer lastFcaTerm = 0;
        Integer lastExamTerm = 0;
        final EppFControlActionInfo fcaInfo = EppFControlActionInfo.SUPPLIER.get();
        for (Map.Entry<Integer, Set<String>> entry : term2fcaFullCodes.entrySet())
        {
            final Integer term = entry.getKey();
            lastTerm = Math.max(lastTerm, term);
            for (String fcaFullCode : entry.getValue())
            {
                lastFcaTerm = Math.max(lastFcaTerm, term);
                if (term > lastExamTerm && fcaInfo.isExam(fcaFullCode)) {
                    lastExamTerm = term;
                }
            }
        }
        return lastExamTerm > 0 ? lastExamTerm : (lastFcaTerm > 0 ? lastFcaTerm : lastTerm);
    }

    /**
     * Разбивание семестров для алгоритма "Одна дисциплина - количество строк в дипломе равно количеству экзаменов"
     * Берутся все семестры, где есть нагрзука/ФИК. Далее на каждый экзамен создается строка в дипломе.
     * Если последним идет семестр без экзамена, то его (часть реестра) связывать со строкой, в которой указан последний экзамен.
     *
     * Для семестров, в которых предусмотрены экзамены по дисциплине, определяем номер части - считаем, что части нумеруются с 1
     * (т.е. если дисциплина читается в семестрах: 2 - зачет, 3 - экзамен, 4 - зачет, 5 - экзамен, 6 - зачет, 7 - зачет,
     * то для дисциплины будут созданы 2 строки с номерами части: 1 - 2,3 семестры; 2 - 4,5,6,7 семестры).
     *
     * @return map[key: семестр строки -> набор семестров, вощедших в строку (если ФИК нет, то пустой!)]
     */
    public static Map<Integer, Set<Integer>> splitTermPartWithExams(Map<Integer, Set<String>> term2fcaFullCodes)
    {
        final EppFControlActionInfo fcaInfo = EppFControlActionInfo.SUPPLIER.get();
        final Map<Integer, Set<Integer>> termMap = new TreeMap<>();
        Set<Integer> pack = new TreeSet<>();
        Integer lastTerm = 0;
        Integer lastFcaTerm = 0;
        Integer lastExamTerm = 0;
        for (Map.Entry<Integer, Set<String>> termEntry : new TreeMap<>(term2fcaFullCodes).entrySet())
        {
            final Integer term = termEntry.getKey();
            pack.add(term);
            lastTerm = Math.max(lastTerm, term);
            if (termEntry.getValue().isEmpty()){
                continue; // Это семестр без ФИК - просто складываем его в набор
            }

            lastFcaTerm = Math.max(term, lastFcaTerm);
            for (String fcaFullCode : termEntry.getValue())
            {
                if (fcaInfo.isExam(fcaFullCode)) {
                    // В семестре есть экзамен
                    termMap.put(term, pack);
                    pack = new TreeSet<>();
                    lastExamTerm = term;
                    break;
                }
            }
        }

        if (!pack.isEmpty()) {
            if (termMap.isEmpty()) {

                // Экзаменов вообще не было - все семестры в одну строку.
                // Номером семестра будет последний семестр с ФИК. Если таких нет, то последний из существующих.
                termMap.put(lastFcaTerm > 0 ? lastFcaTerm : lastTerm, pack);

            } else {

                // В последнем семестре не было экзамена, прикрепляем все последние семестры без экзамена
                // к последнему семестру с экзаменом (а семестр с экзаменом точно уже есть)
                termMap.get(lastExamTerm).addAll(pack);

            }
        }

        return termMap;
    }
}