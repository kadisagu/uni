/* $Id:$ */
package ru.tandemservice.unidip.base.ext.SystemAction.ui.Pub;

import com.healthmarketscience.jackcess.Database;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.process.*;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.uni.dao.mdbio.BaseIODao;
import ru.tandemservice.uni.util.BackgroundProcessHolder;
import ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.ui.TransferToFreeStateForm.DipDiplomaBlankTransferToFreeStateForm;
import ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.ui.TransferWithoutOrderForm.DipDiplomaBlankTransferWithoutOrderForm;
import ru.tandemservice.unidip.dao.mdbio.IDipDocumentIODao;

import java.io.File;
import java.util.Date;

import static ru.tandemservice.unidip.dao.mdbio.DipDocumentIODao.IMPORT_DIPLOMAS_LOG_FILE_NAME;

/**
 * @author rsizonenko
 * @since 27.02.2015
 */
public class DipSystemActionPubAddon extends UIAddon
{
    private File _file;

    @Override
    public void onComponentPrepareRender()
    {
        if (_file != null)
        {
            IDocumentRenderer documentRenderer = new CommonBaseRenderer().document(_file).fileName("diplomas-" + DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()) + ".mdb");
            BusinessComponentUtils.downloadDocument(documentRenderer, true);
            _file = null;
        }
    }

    public DipSystemActionPubAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public void onClickTransferBlanksWithoutOrder()
    {
        getActivationBuilder().asDesktopRoot(DipDiplomaBlankTransferWithoutOrderForm.class).activate();
    }

    public void onClickTransferBlanksFromFreeState()
    {
        getActivationBuilder().asDesktopRoot(DipDiplomaBlankTransferToFreeStateForm.class).activate();
    }

    public void onClickExportDiploma()
    {

        BusinessComponentUtils.runProcess(new BackgroundProcessThread("Экспорт данных документов об обучении", new BackgroundProcessBase()
        {
            @Override
            public ProcessResult run(final ProcessState state)
            {
                getTemplate4Diploma();
                return null;
            }
        }, ProcessDisplayMode.unknown));
    }

    public void onClickImportDiploma()
    {
        final StringBuilder resultMessage = new StringBuilder();
        // пытаемся открыть файл
        final IBackgroundProcess process = new BackgroundProcessBase()
        {
            @Override
            public ProcessResult run(final ProcessState state)
            {

                // предварительно
                String path = ApplicationRuntime.getAppInstallPath();
                final File importFile = new File(path, "data/diplomas.mdb");
                if (!importFile.exists())
                {
                    throw new ApplicationException("Импорт данных провести не удалось: не найден файл «diplomas.mdb» с исходными данными в каталоге «${app.install.path}/data/». Разместите файл для импорта с указанным названием на сервере приложения по указанному пути.");
                }
                if (!importFile.canRead())
                {
                    throw new ApplicationException("Импорт данных провести не удалось: файл «${app.install.path}/data/diplomas.mdb» не доступен для чтения. Проверьте доступ к файлу на сервере приложения.");
                }

                // отключаем логирование в базу и прочие системные штуки
                final IEventServiceLock eventLock = CoreServices.eventService().lock();
                Debug.stopLogging(); /* жрет много памяти, а ее и так не хватает */
                try
                {
                    BaseIODao.setupProcessState(state);
                    BaseIODao.doLoggedAction(() -> {
                        BaseIODao.getLog4jLogger().info("Import started.");
                        try
                        {
                            // открываем файл
                            try (Database mdb = Database.open(importFile))
                            {
                                // запускаем импорт
                                String output = IDipDocumentIODao.instance.get().importDiplomaList(mdb);
                                resultMessage.append(output);
                                BaseIODao.getLog4jLogger().info("Import finished. Done.");
                                return null;
                            }
                            // закрываем файл

                        }
                        catch (Exception t)
                        {
                            BaseIODao.getLog4jLogger().info("Import finished. Error.");
                            throw CoreExceptionUtils.getRuntimeException(t);
                        }
                    }, IMPORT_DIPLOMAS_LOG_FILE_NAME);
                }
                catch (final Exception t)
                {
                    if (t instanceof ApplicationException)
                    {
                        // пробрасываем ApplicationException, как есть и добавляем ошибку в ErrorCollector для отображения сообщения о файле лога
                        UserContext.getInstance().getErrorCollector().add("Импорт данных провести не удалось. Обратитесь к логу импорта (файл " + IMPORT_DIPLOMAS_LOG_FILE_NAME + ".log) на сервере приложения для получения детальной информации об ошибке.");
                        throw (ApplicationException) t;
                    }
                    Debug.exception(t.getMessage(), t);
                    throw new ApplicationException("Импорт данных провести не удалось. Обратитесь к логу импорта (файл " + IMPORT_DIPLOMAS_LOG_FILE_NAME + ".log) на сервере приложения для получения детальной информации об ошибке.", t);

                }
                finally
                {
                    // включаем штуки и логирование обратно
                    Debug.resumeLogging();
                    eventLock.release();
                }

                // если все ок, просто закрываем диалог
                return new ProcessResult(resultMessage.toString());
            }


        };

        new BackgroundProcessHolder().start("Импорт данных документов об обучении", process, ProcessDisplayMode.unknown);
    }


    private void getTemplate4Diploma()
    {
        try
        {
            _file = File.createTempFile("mdb-io-", "mdb");
            Database mdb = Database.create(Database.FileFormat.V2003, _file, false);
            IDipDocumentIODao.instance.get().exportDiplomaList(mdb);
            mdb.close();
        }
        catch (final Exception t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }
}
