/* $Id:$ */
package ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.ui.Add;

import com.google.common.base.Preconditions;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank;
import ru.tandemservice.unidip.bso.entity.catalog.DipBlankType;

import javax.validation.constraints.NotNull;

/**
 * @author rsizonenko
 * @since 30.12.2014
 */
public class DipBlankRowWrapper extends IdentifiableWrapper<DipDiplomaBlank>
{
    public DipBlankRowWrapper(Long id)
    {
        super(id, null);
    }

    private String _seria;
    private Integer _firstNumber;
    private Integer _lastNumber;
    private DipBlankType _type;
    private OrgUnit _storageLocation;
    private String _printingOffice;
    private String _invoice;

    public String getSeria() { return _seria; }
    public void setSeria(String seria) { _seria = seria; }

    public Integer getFirstNumber() { return _firstNumber; }
    public String getStringFirstNumber() { return StringUtils.leftPad(String.valueOf(_firstNumber), 7, "0"); }
    public void setFirstNumber(Integer firstNumber) { _firstNumber = firstNumber; }



    public Integer getLastNumber() { return _lastNumber; }
    public String getStringLastNumber() { return StringUtils.leftPad(String.valueOf(_lastNumber), 7, "0"); }
    public void setLastNumber(Integer lastNumber) { _lastNumber = lastNumber; }

    public DipBlankType getType() { return _type; }
    public void setType(DipBlankType type) { _type = type; }

    public OrgUnit getStorageLocation() { return _storageLocation; }
    public void setStorageLocation(OrgUnit storageLocation) { _storageLocation = storageLocation; }

    public String getPrintingOffice() { return _printingOffice; }
    public void setPrintingOffice(String printingOffice) { this._printingOffice = printingOffice; }

    public String getInvoice() { return _invoice; }
    public void setInvoice(String invoice) { _invoice = invoice; }

    public void update(@NotNull DipBlankRowWrapper sourceRow)
    {
        Preconditions.checkNotNull(sourceRow);
        setType(sourceRow.getType());
        setSeria(sourceRow.getSeria());
        setFirstNumber(sourceRow.getFirstNumber());
        setLastNumber(sourceRow.getLastNumber());
        setStorageLocation(sourceRow.getStorageLocation());
        setPrintingOffice(sourceRow.getPrintingOffice());
        setInvoice(sourceRow.getInvoice());
    }
}