/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unidip.base.bo.DiplomaTemplate.logic;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.unidip.base.bo.DiplomaTemplate.DiplomaTemplateManager;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRegElPartFControlAction;
import ru.tandemservice.unidip.base.entity.eduplan.DiplomaTemplate;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.exists;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author iolshvang
 * @since 06.08.11 16:57
 */
public class EppRegistryTemplateRowHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String DIPLOMA_TEMPLATE = "diplomaObject";
    public static final String CONTROL_FORM_COLUMN = "controlFormColumn";

    public EppRegistryTemplateRowHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final DiplomaTemplate diplomaTemplate = context.get(DIPLOMA_TEMPLATE);

        // Строки УПв, годные  для добавления в шаблон
        final Map<IEppEpvRowWrapper, Map<Integer, Collection<Long>>> epvRows = DiplomaTemplateManager.instance().dao().getEpvRowsForCreation(diplomaTemplate, null);

        // Уже добавленные формы контроля частей элементов реестра
        // { [regEl.id] -> [[fca.id, part.number]] }
        final Multimap<Long, PairKey<Long, Integer>> existsRegElFCA = HashMultimap.create();
        final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppRegistryElementPartFControlAction.class, "fca")
                .column(property("fca", EppRegistryElementPartFControlAction.part().registryElement().id()))
                .column(property("fca", EppRegistryElementPartFControlAction.controlAction().id()))
                .column(property("fca", EppRegistryElementPartFControlAction.part().number()))
                .where(exists(DiplomaContentRegElPartFControlAction.class,
                              DiplomaContentRegElPartFControlAction.registryElementPartFControlAction().s(), property("fca"),
                              DiplomaContentRegElPartFControlAction.owner().s(), diplomaTemplate.getContent()
                ));
        for (Object[] item : dql.createStatement(context.getSession()).<Object[]>list()) {
            existsRegElFCA.put((Long) item[0], new PairKey<>((Long) item[1], (Integer) item[2]));
        }
        final Map<Long, String> fcaTitleMap = new HashMap<>(6);
        for (EppFControlActionType fca : DataAccessServices.dao().getList(EppFControlActionType.class)) {
            fcaTitleMap.put(fca.getId(), fca.getShortTitle());
        }

        final List<DataWrapper> recordList = new ArrayList<>();
        for (Map.Entry<IEppEpvRowWrapper, Map<Integer, Collection<Long>>> entry: epvRows.entrySet())
        {
            final IEppEpvRowWrapper epvRowWrapper = entry.getKey();
            //ФИК по дисциплинам-строкам
            final Collection<String> actionsRow = new ArrayList<>(epvRowWrapper.getActiveTermSet().size());

            int normalizePartNumber = 0;
            for(Integer part : epvRowWrapper.getActiveTermSet()){
                Collection<Long> partEpv = entry.getValue().get(part);
                normalizePartNumber++;
                //семестр активный, но ФИК в нем нет
                if(partEpv == null){
                    continue;
                }
                //ФИК по семестрам дисциплины-строки
                final Set<String> actions = new TreeSet<>();
                StringBuilder property = new StringBuilder();
                for (Long epv : partEpv){
                    if (!existsRegElFCA.containsEntry(((EppEpvRegistryRow) epvRowWrapper.getRow()).getRegistryElement().getId(), new PairKey<>(epv, normalizePartNumber))) {
                        actions.add(fcaTitleMap.get(epv));
                    }
                }
                if (!actions.isEmpty()) {
                    property.append(part).append(" - ");
                    property.append(CommonBaseStringUtil.joinNotEmpty(actions, ", "));
                    actionsRow.add(property.toString());
                }
            }
            if (!actionsRow.isEmpty()) {
                final DataWrapper record = new DataWrapper(epvRowWrapper.getRow());
                record.setProperty(CONTROL_FORM_COLUMN, actionsRow);
                recordList.add(record);
            }
        }
        recordList.sort(Comparator.comparing(o -> ((EppEpvRegistryRow)o.getWrapped()).getStoredIndex()));
        final DSOutput output = ListOutputBuilder.get(input, recordList).build();
        output.setCountRecord(Math.max(recordList.size(), 1));
        return output;
    }
}