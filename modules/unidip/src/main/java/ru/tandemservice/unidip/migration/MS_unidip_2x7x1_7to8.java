/* $Id$ */
package ru.tandemservice.unidip.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

/**
 * @author azhebko
 * @since 22.01.2015
 */
public class MS_unidip_2x7x1_7to8 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        Long issued = null;
        {
            Statement statement = tool.getConnection().createStatement();
            statement.execute("select id from dip_c_blank_state_t where code_p = 'issued'"); // DipBlankStateCodes.ISSUED
            ResultSet resultSet = statement.getResultSet();
            if (resultSet.next())
                issued = resultSet.getLong(1);
        }

        if (issued == null)
            return;

        Collection<Long> blanks = new ArrayList<>();

        {
            Statement statement = tool.getConnection().createStatement();
            statement.execute("" +
                "select distinct b.id, bs.code_p from dip_diploma_blank_t b " +
                "inner join dip_c_blank_state_t bs on bs.id = b.blankstate_id " +
                "inner join dip_issuance_t di on di.blank_id = b.id " +
                "inner join dip_stu_exclude_extract_t ee on ee.issuance_id = di.id " +
                "inner join abstractstudentextract_t ae on ae.id = ee.id " +
                "inner join extractstates_t s on s.id = ae.state_id " +
                "where s.code_p = '6'"); // ExtractStatesCodes.FINISHED

            ResultSet resultSet = statement.getResultSet();
            while (resultSet.next())
            {
                long id = resultSet.getLong(1);
                String state = resultSet.getString(2);
                if (!"linked".equals(state))                     // DipBlankStateCodes.LINKED
                    throw new IllegalStateException("Diploma blank " + id + " has wrong state '" + state + "'");

                blanks.add(id);
            }
        }
        {
            Statement statement = tool.getConnection().createStatement();
            statement.execute("" +
                "select distinct b.id, bs.code_p from dip_diploma_blank_t b " +
                "inner join dip_c_blank_state_t bs on bs.id = b.blankstate_id " +
                "inner join dip_content_issuance_t ci on ci.blank_id = b.id " +
                "inner join dip_iss_cont_to_extract_rel_t cir on cir.contentissuance_id = ci.id " +
                "inner join dip_stu_exclude_extract_t ee on ee.id = cir.extract_id " +
                "inner join abstractstudentextract_t ae on ae.id = ee.id " +
                "inner join extractstates_t s on s.id = ae.state_id " +
                "where s.code_p = '6'"); // ExtractStatesCodes.FINISHED

            ResultSet resultSet = statement.getResultSet();
            while (resultSet.next())
            {
                long id = resultSet.getLong(1);
                String state = resultSet.getString(2);
                if (!"linked".equals(state))                     // DipBlankStateCodes.LINKED
                    throw new IllegalStateException("Diploma blank " + id + " has wrong state '" + state + "'");

                blanks.add(id);
            }
        }

        if (!blanks.isEmpty())
        {
            PreparedStatement preparedStatement = tool.prepareStatement("update dip_diploma_blank_t set blankstate_id = ? where id = ?");
            preparedStatement.setLong(1, issued);
            for (Long id: blanks)
            {
                preparedStatement.setLong(2, id);
                preparedStatement.execute();
            }
        }
    }
}