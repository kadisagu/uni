package ru.tandemservice.unidip.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unidip_2x8x0_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность dipBlankDisposalReason

		// создано обязательное свойство shortTitle
		{
			// создать колонку
			tool.createColumn("dip_c_blank_disposal_reason_t", new DBColumn("shorttitle_p", DBType.createVarchar(255)));

			// задать значение по умолчанию
            String[] codes = {"forceMajeureDestruction", "errorAtFilling", "blankAreOutOfDate", "executiveFaultLossOrDestruction"};
            String[] shortTitles = {"Форс-мажор. обст.", "Ошиб. при запол.", "Устарели", "По вине исполнителя"};

            PreparedStatement update = tool.prepareStatement("update dip_c_blank_disposal_reason_t set shorttitle_p = ? where id = ?");
            Statement stmt = tool.getConnection().createStatement();
            stmt.execute("select id, code_p, title_p from dip_c_blank_disposal_reason_t");

            ResultSet src = stmt.getResultSet();
            while (src.next())
            {
                long id = src.getLong(1);
                String code = src.getString(2);
                String title = src.getString(3);
                boolean found = false;

                for (int i = 0; i < codes.length; i++)
                {
                    if (code.equals(codes[i]))
                    {
                        update.setString(1, shortTitles[i]);
                        update.setLong(2, id);
                        update.execute();
                        found = true;
                    }
                }
                if (!found)
                {
                    update.setString(1, title.substring(0, title.length() < 10 ? title.length() : 10));
                    update.setLong(2, id);
                    update.execute();
                }
            }

			// сделать колонку NOT NULL
			tool.setColumnNullable("dip_c_blank_disposal_reason_t", "shorttitle_p", false);
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность dipDiplomaBlank

		// создано свойство invoice
		{
			// создать колонку
			tool.createColumn("dip_diploma_blank_t", new DBColumn("invoice_p", DBType.createVarchar(255)));
		}
    }
}