/* $Id$ */
package ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.ui.DisposalForm;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.DipDiplomaBlankManager;
import ru.tandemservice.unidip.bso.entity.catalog.DipBlankDisposalReason;

import java.util.Collection;
import java.util.Date;

/**
 * @author azhebko
 * @since 15.01.2015
 */
@Input({
    @Bind(key = DipDiplomaBlankManager.KEY_BLANKS, binding = "blanks", required = true),
    @Bind(key = DipDiplomaBlankManager.KEY_MASS_ACTION, binding = "massDisposal", required = true)})
public class DipDiplomaBlankDisposalFormUI extends UIPresenter
{
    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(DipDiplomaBlankManager.KEY_MASS_ACTION, isMassDisposal() ? true : null);
    }

    public void onClickApply()
    {
        Integer count = DipDiplomaBlankManager.instance().diplomaBlankDao().doDisposeBlanks(getBlanks(), getDisposalReason(), getDisposalDate());
        deactivate();
        if (isMassDisposal())
            this.getSupport().info(CommonBaseStringUtil.numberPostfixCase(count, "Списан ", "Списано ", "Списано ") + CommonBaseStringUtil.numberWithPostfixCase(count, "бланк.", "бланка.", "бланков."));
    }

    private Collection<Long> _blanks;
    private Boolean _massDisposal = true;

    private DipBlankDisposalReason _disposalReason;
    private Date _disposalDate = new Date();

    public Collection<Long> getBlanks() { return _blanks; }
    public void setBlanks(Collection<Long> blanks) { _blanks = blanks; }

    public boolean isMassDisposal() { return _massDisposal; }
    public void setMassDisposal(boolean massDisposal) { _massDisposal = massDisposal; }

    public DipBlankDisposalReason getDisposalReason() { return _disposalReason; }
    public void setDisposalReason(DipBlankDisposalReason disposalReason) { _disposalReason = disposalReason; }

    public Date getDisposalDate() { return _disposalDate; }
    public void setDisposalDate(Date disposalDate) { _disposalDate = disposalDate; }
}