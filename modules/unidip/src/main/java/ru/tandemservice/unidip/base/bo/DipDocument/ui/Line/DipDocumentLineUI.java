/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unidip.base.bo.DipDocument.ui.Line;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.PageableSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.info.InfoCollector;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unidip.base.bo.DipDocument.DipDocumentManager;
import ru.tandemservice.unidip.base.bo.DipDocument.logic.DiplomaStudentEpvSlotDSHandler;
import ru.tandemservice.unidip.base.bo.DipDocument.ui.LineAddEdit.DipDocumentLineAddEdit;
import ru.tandemservice.unidip.base.bo.DipDocument.ui.LineAddEdit.DipDocumentLineAddEditUI;
import ru.tandemservice.unidip.base.bo.DiplomaTemplate.DiplomaTemplateManager;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow.DiplomaRowConfig;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaObject;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaQualifWorkRow;
import ru.tandemservice.unidip.base.entity.order.DipStuExcludeExtract;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author iolshvang
 * @since 06.08.11 15:40
 */
@State({
               @Bind(key = UIPresenter.PUBLISHER_ID, binding = "dipDocumentId", required = true)
       })
public class DipDocumentLineUI extends UIPresenter
{
    private Long _dipDocumentId;
    private DiplomaObject _dipDocument;
    private boolean _hasExcludeExtract;
    private boolean _hasQualifWork;
    private Multimap<Class<? extends DiplomaContentRow>, DiplomaContentRow> _rowsMap;
    private Map<Class<? extends DiplomaContentRow>, DiplomaRowConfig> _configMap = DiplomaContentRow.DIPLOMA_BLOCK_CLASSES.get();
    private Class<? extends DiplomaContentRow> _currentClass;

    @Override
    public void onComponentRefresh()
    {
        _dipDocument = IUniBaseDao.instance.get().getNotNull(_dipDocumentId);
        updateDS();
    }

    private void updateDS()
    {
        _hasExcludeExtract = IUniBaseDao.instance.get().existsEntity(DipStuExcludeExtract.class, DipStuExcludeExtract.L_DIPLOMA, _dipDocument);
        _hasQualifWork = IUniBaseDao.instance.get().existsEntity(DiplomaQualifWorkRow.class, DiplomaQualifWorkRow.L_OWNER, _dipDocument.getContent());

        // Загружаем строки диплома, раскладываем по блокам. Строки сортируем по номеру.
        _rowsMap = ArrayListMultimap.create();
        final List<DiplomaContentRow> rowList = IUniBaseDao.instance.get().getList(DiplomaContentRow.class, DiplomaContentRow.owner(), _dipDocument.getContent(), DiplomaContentRow.P_NUMBER);
        for (DiplomaContentRow row : rowList)
        {
            _rowsMap.put(row.getClass(), row);
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (DipDocumentLine.THE_OTHERS_DS.equals(dataSource.getName())) {
            dataSource.put(DiplomaStudentEpvSlotDSHandler.DIPLOMA_OBJECT, getDipDocument());
        } else if (getCurrentClass() != null) {
            dataSource.put(UIDefines.COMBO_OBJECT_LIST, _rowsMap.get(getCurrentClass()));
        }
    }

    // Getters & Setters

    public boolean isCurrentAddRowDisabled()
    {
        return getCurrentClass() == DiplomaQualifWorkRow.class && _hasQualifWork;
    }

	public boolean isUpdateLineDisabled()
	{
		return _dipDocument.getStudentEpv() == null;
	}

    public Long getDipDocumentId()
    {
        return _dipDocumentId;
    }

    public void setDipDocumentId(Long dipDocumentId)
    {
        _dipDocumentId = dipDocumentId;
    }

    public DiplomaObject getDipDocument()
    {
        return _dipDocument;
    }

    public Collection<Class<? extends DiplomaContentRow>> getRowClasses()
    {
        return _configMap.keySet();
    }

    public String getTableTitle()
    {
        return _configMap.get(getCurrentClass()).blockTitle;
    }

    public String getLoadColumnName()
    {
        return _configMap.get(getCurrentClass()).getLoadColumnTitle(!getDipDocument().getContent().isLoadInWeeks());
    }

    public Class<? extends DiplomaContentRow> getCurrentClass()
    {
        return _currentClass;
    }

    public void setCurrentClass(Class<? extends DiplomaContentRow> currentClass)
    {
        _currentClass = currentClass;
    }

    public DynamicListDataSource getCurrentDS()
    {
        return ((PageableSearchListDataSource) getConfig().getDataSource(getCurrentClass().getSimpleName())).getLegacyDataSource();
    }

    public void setDipDocument(DiplomaObject dipDocument)
    {
        _dipDocument = dipDocument;
    }

    public boolean isExistExcludeExtract()
    {
        return _hasExcludeExtract;
    }

    // Listeners

    public void onClickUp()
    {
        DiplomaTemplateManager.instance().dao().doChangePriorityUp(getListenerParameterAsLong());
        updateDS();
    }

    public void onClickDown()
    {
        DiplomaTemplateManager.instance().dao().doChangePriorityDown(getListenerParameterAsLong());
        updateDS();
    }

    public void onClickUpdateLine()
    {
        try {
            InfoCollector infoCollector = ContextLocal.getInfoCollector();
            DipDocumentManager.instance().dao().updateLine(getDipDocument(), infoCollector);
        } finally {
            // Обновить строки в любом случае надо, чтобы там недосохраненные данные не остались
            updateDS();
        }
    }

    public void onDeleteEntityFromList()
    {
        DipDocumentManager.instance().dao().deleteRow(getListenerParameterAsLong());
        updateDS();
    }

    public void onClickAddLine()
    {
        _uiActivation.asRegionDialog(DipDocumentLineAddEdit.class)
                .parameter(DipDocumentLineAddEditUI.DIP_DOCUMENT_ID, getDipDocumentId())
                .parameter(DipDocumentLineAddEditUI.ROW_CLASS_BIND, this.getListenerParameter())
                .activate();
    }

    public void onEditRow()
    {
        _uiActivation.asRegionDialog(DipDocumentLineAddEdit.class)
                .parameter(DipDocumentLineAddEditUI.DIP_DOCUMENT_ID, getDipDocumentId())
                .parameter(DipDocumentLineAddEditUI.DIP_DOCUMENT_LINE_ID, getListenerParameterAsLong())
                .activate();
    }
}
