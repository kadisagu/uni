package ru.tandemservice.unidip.base.entity.catalog;

import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import ru.tandemservice.unidip.base.entity.catalog.gen.DipDocTemplateCatalogGen;

import java.util.Date;

/**
 * Шаблоны документов о полученном образовании
 */
public class DipDocTemplateCatalog extends DipDocTemplateCatalogGen implements ITemplateDocument
{
    public static final String DEFAULT_SCRIPT_PATH = "unidip/scripts/TemplateRevertScrip.groovy";
    public static final String DEFAULT_CATALOG_CODE = "unidipScriptItem";

    @Override
    public String getPath()
    {
        return getTemplatePath();
    }

    @Override
    public byte[] getDocument()
    {
        return getUserTemplate();
    }

    @Override
    public void setDocument(byte[] document)
    {
        setUserTemplate(document);
    }

    @Override
    public byte[] getContent()
    {
       return CommonBaseUtil.getTemplateContent(this);
    }

    @Override
    public Date getEditDate()
    {
        return getUserTemplateEditDate();
    }

    @Override
    public void setEditDate(Date editDate)
    {
        setUserTemplateEditDate(editDate);
    }

    @Override
    public String getComment()
    {
        return getUserTemplateComment();
    }

    @Override
    public void setComment(String comment)
    {
        setUserTemplateComment(comment);
    }
}