package ru.tandemservice.unidip.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unidip_2x6x8_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.8")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность dipDocTemplateCatalog

		// создано обязательное свойство application
		{
			// создать колонку
            if (!tool.columnExists("dip_c_doc_template_t", "application_p"))
            {
                tool.createColumn("dip_c_doc_template_t", new DBColumn("application_p", DBType.BOOLEAN));

                // задать значение по умолчанию
                java.lang.Boolean defaultApplication = false;
                tool.executeUpdate("update dip_c_doc_template_t set application_p=? where application_p is null", defaultApplication);

                // сделать колонку NOT NULL
                tool.setColumnNullable("dip_c_doc_template_t", "application_p", false);
            }

		}


    }
}