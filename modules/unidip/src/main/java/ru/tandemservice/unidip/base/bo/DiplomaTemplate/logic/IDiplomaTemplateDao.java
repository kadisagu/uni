/* $Id$ */
package ru.tandemservice.unidip.base.bo.DiplomaTemplate.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.info.InfoCollector;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unidip.base.bo.DipDocument.logic.IRowTotalData;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContent;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRegElPartFControlAction;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow;
import ru.tandemservice.unidip.base.entity.eduplan.DiplomaTemplate;
import ru.tandemservice.unidip.settings.entity.DipFormingRowAlgorithm;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author Vasily Zhukov
 * @since 05.05.2011
 */
public interface IDiplomaTemplateDao extends INeedPersistenceSupport
{
    /**
     * Добавлять/Редактировать документ может только одна транзакция в одно время,
     * так как требуется обеспечить уникальность номера документа
     */
    String DIP_TEMPLATE_ADD_EDIT_SYNC = "dipTemplateAddEditSync";

    void doNormalizeRowNumbers(Class<? extends DiplomaContentRow> rowClass, DiplomaContent owner);

    Map<Long, IRowTotalData> getTotalRowData(Collection<Long> rowIds);

    /**
     * Метод сохраняет/апдейтит шаблон диплома для УП(в) и содержание документа о полученном образовании
     *
     * @param diplomaTemplate Шаблон документа о полученном образовании
     * @param diplomaContent  Содержание документа о полученном образовании
     */
    void saveOrUpdateDiplomaTemplate(DiplomaTemplate diplomaTemplate, DiplomaContent diplomaContent);

    /**
     * Метод снижает приоритет строки содержания документа о полученном образовании таким образом,
     * чтобы в одном из блоков шаблона ("Дисциплины", "Практики", "Государственный экзамен", "ВКР", "Курсовые работы (проекты) и НИР")
     * порядковый номер строки снизился на 1
     *
     * @param entityId       Строка приоритет которой нужно снизить
     */
    void doChangePriorityDown(Long entityId);

    /**
     * Метод увеличивает приоритет строки содержания документа о полученном образовании таким образом,
     * чтобы в одном из блоков шаблона ("Дисциплины", "Практики", "Государственный экзамен", "ВКР", "Курсовые работы (проекты) и НИР")
     * порядковый номер строки увеличился на 1
     *
     * @param rowId       Строка приоритет которой нужно повысить
     */
    void doChangePriorityUp(Long rowId);

    /**
     * Метод добавляет или обновляет строку содержания документа о полученном образовании, а так же элементы из которых
     * состоит данная строка
     *
     * @param elementPartFControlActionList содержит элементы строки, которые необходимо добавить или обновить
     * @param row                           строка содержания документа о полученном образовании которую необходимо добавить или обновит
     */
    void saveOrUpdateDiplomaContentRow(List<DiplomaContentRegElPartFControlAction> elementPartFControlActionList, DiplomaContentRow row);

    /**
     * Метод возвращает DQL-запрос для получения связей элемента реестра с формами итоговых мероприятий.
     *
     * @param alias               алиас для EppRegistryElementPartFControlAction
     * @param template            шаблон диплома, для которого надо получить данные
     * @param registryElementPart часть элемента реестра, для которой надо получить связь с формамаи итогового контроля
     * @param regElemWrappedList  список связей диплома с частью элемента реестра, связи обернуты во враппре
     * @param row                 строка диплома
     * @return DQL-запрос для получения элементов класса EppRegistryElementPartFControlAction
     */
    DQLSelectBuilder getEppRegistryElementPartFControlActionBuilder(String alias, DiplomaTemplate template, EppRegistryElementPart registryElementPart,
                                                                    List<DataWrapper> regElemWrappedList, DiplomaContentRow row);

    /**
     * Метод создает строки диплома для указаного шаблона
     *
     * @param template шаблон для содержания, которого необходимо создать строки
     */
    void createDiplomaContentRows(DiplomaTemplate template, InfoCollector infoCollector);

    /**
     * Выводит текущий алгоритм формирования строк в документе об обучении на подразделении
     *
     * По направленности из содержания шаблона определяется блок направленности в рамках версии УП.
     * Получается выпускающее подразделение, по нему находится алгоритм формирования строк документа об обучении на подразделении.
     *
     * @param owner содержание документа
     * @param epv УПв
     * @return текущий алгоритм
     */
    DipFormingRowAlgorithm getCurrentFormingRowAlgorithmOu(DiplomaContent owner, EppEduPlanVersion epv);

    void deleteDiplomaTemplate(Long diplomaTemplateId);

    /** @return врапперы строк блока УПв */
    Collection<IEppEpvRowWrapper> getEpvBlockRows(EppEduPlanVersion eduPlanVersion, EduProgramSpecialization specialization);

    /**
     * Получение строк УПв (блока УПв), пригодных для формирования строк шаблона.
     * Для каждой строки еще получаем набор форм контроля по семестрам, в которых присутствует форма контроля для данной строки.
     * @param template шаблон документа
     * @param infoCollector инфоколлектор для вывода сообщений пользователю. Может быть не указан.
     * @return мапа: [враппер строки УПв] -> [мапа: [номер семестра] -> [формы контроля в семестре]]
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    Map<IEppEpvRowWrapper, Map<Integer, Collection<Long>>> getEpvRowsForCreation(DiplomaTemplate template, InfoCollector infoCollector);

    void deleteRow(Long rowId);

    void resortAllRowsAsEpv(DiplomaContent content, EppEduPlan eduPlan);

    /**
     * Определяет максимальный номер строки в документе
     *
     * @return максимльный номер строки либо 0, если строк нет
     */
    int getDiplomaContentRowMaxNumber(DiplomaContent content);
}
