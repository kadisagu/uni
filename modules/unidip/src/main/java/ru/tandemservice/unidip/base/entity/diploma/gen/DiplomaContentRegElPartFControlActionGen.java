package ru.tandemservice.unidip.base.entity.diploma.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContent;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRegElPartFControlAction;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь строки документа об обучении с реестром мероприятий
 *
 * Определяет перечень МСРП-по-ФК (итоговые формы контроля по дисциплинам), на базе которых будут заполнены итоговая назгрузка и оценка по строке.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DiplomaContentRegElPartFControlActionGen extends EntityBase
 implements INaturalIdentifiable<DiplomaContentRegElPartFControlActionGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRegElPartFControlAction";
    public static final String ENTITY_NAME = "diplomaContentRegElPartFControlAction";
    public static final int VERSION_HASH = -1134924503;
    private static IEntityMeta ENTITY_META;

    public static final String L_OWNER = "owner";
    public static final String L_REGISTRY_ELEMENT_PART_F_CONTROL_ACTION = "registryElementPartFControlAction";
    public static final String L_ROW = "row";

    private DiplomaContent _owner;     // Содержание документа об обучении
    private EppRegistryElementPartFControlAction _registryElementPartFControlAction;     // Форма итогового контроля по части элемента реестра
    private DiplomaContentRow _row;     // Строка

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Содержание документа об обучении. Свойство не может быть null.
     */
    @NotNull
    public DiplomaContent getOwner()
    {
        return _owner;
    }

    /**
     * @param owner Содержание документа об обучении. Свойство не может быть null.
     */
    public void setOwner(DiplomaContent owner)
    {
        dirty(_owner, owner);
        _owner = owner;
    }

    /**
     * @return Форма итогового контроля по части элемента реестра. Свойство не может быть null.
     */
    @NotNull
    public EppRegistryElementPartFControlAction getRegistryElementPartFControlAction()
    {
        return _registryElementPartFControlAction;
    }

    /**
     * @param registryElementPartFControlAction Форма итогового контроля по части элемента реестра. Свойство не может быть null.
     */
    public void setRegistryElementPartFControlAction(EppRegistryElementPartFControlAction registryElementPartFControlAction)
    {
        dirty(_registryElementPartFControlAction, registryElementPartFControlAction);
        _registryElementPartFControlAction = registryElementPartFControlAction;
    }

    /**
     * @return Строка. Свойство не может быть null.
     */
    @NotNull
    public DiplomaContentRow getRow()
    {
        return _row;
    }

    /**
     * @param row Строка. Свойство не может быть null.
     */
    public void setRow(DiplomaContentRow row)
    {
        dirty(_row, row);
        _row = row;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof DiplomaContentRegElPartFControlActionGen)
        {
            if (withNaturalIdProperties)
            {
                setOwner(((DiplomaContentRegElPartFControlAction)another).getOwner());
                setRegistryElementPartFControlAction(((DiplomaContentRegElPartFControlAction)another).getRegistryElementPartFControlAction());
            }
            setRow(((DiplomaContentRegElPartFControlAction)another).getRow());
        }
    }

    public INaturalId<DiplomaContentRegElPartFControlActionGen> getNaturalId()
    {
        return new NaturalId(getOwner(), getRegistryElementPartFControlAction());
    }

    public static class NaturalId extends NaturalIdBase<DiplomaContentRegElPartFControlActionGen>
    {
        private static final String PROXY_NAME = "DiplomaContentRegElPartFControlActionNaturalProxy";

        private Long _owner;
        private Long _registryElementPartFControlAction;

        public NaturalId()
        {}

        public NaturalId(DiplomaContent owner, EppRegistryElementPartFControlAction registryElementPartFControlAction)
        {
            _owner = ((IEntity) owner).getId();
            _registryElementPartFControlAction = ((IEntity) registryElementPartFControlAction).getId();
        }

        public Long getOwner()
        {
            return _owner;
        }

        public void setOwner(Long owner)
        {
            _owner = owner;
        }

        public Long getRegistryElementPartFControlAction()
        {
            return _registryElementPartFControlAction;
        }

        public void setRegistryElementPartFControlAction(Long registryElementPartFControlAction)
        {
            _registryElementPartFControlAction = registryElementPartFControlAction;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof DiplomaContentRegElPartFControlActionGen.NaturalId) ) return false;

            DiplomaContentRegElPartFControlActionGen.NaturalId that = (NaturalId) o;

            if( !equals(getOwner(), that.getOwner()) ) return false;
            if( !equals(getRegistryElementPartFControlAction(), that.getRegistryElementPartFControlAction()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getOwner());
            result = hashCode(result, getRegistryElementPartFControlAction());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getOwner());
            sb.append("/");
            sb.append(getRegistryElementPartFControlAction());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DiplomaContentRegElPartFControlActionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DiplomaContentRegElPartFControlAction.class;
        }

        public T newInstance()
        {
            return (T) new DiplomaContentRegElPartFControlAction();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "owner":
                    return obj.getOwner();
                case "registryElementPartFControlAction":
                    return obj.getRegistryElementPartFControlAction();
                case "row":
                    return obj.getRow();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "owner":
                    obj.setOwner((DiplomaContent) value);
                    return;
                case "registryElementPartFControlAction":
                    obj.setRegistryElementPartFControlAction((EppRegistryElementPartFControlAction) value);
                    return;
                case "row":
                    obj.setRow((DiplomaContentRow) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "owner":
                        return true;
                case "registryElementPartFControlAction":
                        return true;
                case "row":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "owner":
                    return true;
                case "registryElementPartFControlAction":
                    return true;
                case "row":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "owner":
                    return DiplomaContent.class;
                case "registryElementPartFControlAction":
                    return EppRegistryElementPartFControlAction.class;
                case "row":
                    return DiplomaContentRow.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DiplomaContentRegElPartFControlAction> _dslPath = new Path<DiplomaContentRegElPartFControlAction>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DiplomaContentRegElPartFControlAction");
    }
            

    /**
     * @return Содержание документа об обучении. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRegElPartFControlAction#getOwner()
     */
    public static DiplomaContent.Path<DiplomaContent> owner()
    {
        return _dslPath.owner();
    }

    /**
     * @return Форма итогового контроля по части элемента реестра. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRegElPartFControlAction#getRegistryElementPartFControlAction()
     */
    public static EppRegistryElementPartFControlAction.Path<EppRegistryElementPartFControlAction> registryElementPartFControlAction()
    {
        return _dslPath.registryElementPartFControlAction();
    }

    /**
     * @return Строка. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRegElPartFControlAction#getRow()
     */
    public static DiplomaContentRow.Path<DiplomaContentRow> row()
    {
        return _dslPath.row();
    }

    public static class Path<E extends DiplomaContentRegElPartFControlAction> extends EntityPath<E>
    {
        private DiplomaContent.Path<DiplomaContent> _owner;
        private EppRegistryElementPartFControlAction.Path<EppRegistryElementPartFControlAction> _registryElementPartFControlAction;
        private DiplomaContentRow.Path<DiplomaContentRow> _row;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Содержание документа об обучении. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRegElPartFControlAction#getOwner()
     */
        public DiplomaContent.Path<DiplomaContent> owner()
        {
            if(_owner == null )
                _owner = new DiplomaContent.Path<DiplomaContent>(L_OWNER, this);
            return _owner;
        }

    /**
     * @return Форма итогового контроля по части элемента реестра. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRegElPartFControlAction#getRegistryElementPartFControlAction()
     */
        public EppRegistryElementPartFControlAction.Path<EppRegistryElementPartFControlAction> registryElementPartFControlAction()
        {
            if(_registryElementPartFControlAction == null )
                _registryElementPartFControlAction = new EppRegistryElementPartFControlAction.Path<EppRegistryElementPartFControlAction>(L_REGISTRY_ELEMENT_PART_F_CONTROL_ACTION, this);
            return _registryElementPartFControlAction;
        }

    /**
     * @return Строка. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRegElPartFControlAction#getRow()
     */
        public DiplomaContentRow.Path<DiplomaContentRow> row()
        {
            if(_row == null )
                _row = new DiplomaContentRow.Path<DiplomaContentRow>(L_ROW, this);
            return _row;
        }

        public Class getEntityClass()
        {
            return DiplomaContentRegElPartFControlAction.class;
        }

        public String getEntityName()
        {
            return "diplomaContentRegElPartFControlAction";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
