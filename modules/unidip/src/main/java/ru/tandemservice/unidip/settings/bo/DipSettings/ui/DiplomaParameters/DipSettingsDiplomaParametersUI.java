/* $Id$ */
package ru.tandemservice.unidip.settings.bo.DipSettings.ui.DiplomaParameters;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.unidip.settings.bo.DipSettings.DipSettingsManager;
import ru.tandemservice.unidip.settings.bo.DipSettings.logic.DipSettingsDao;
import ru.tandemservice.unidip.settings.entity.DipAssistantManager;
import ru.tandemservice.unidip.settings.entity.DipFormingRegNumberAlgorithm;

import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 26.05.2015
 */
public class DipSettingsDiplomaParametersUI extends UIPresenter
{
    private DipFormingRegNumberAlgorithm _algorithmRegNumber = new DipFormingRegNumberAlgorithm();
    private DipAssistantManager _assistantManager = new DipAssistantManager();
    private Boolean dipTemplateCollectOptionalDiscipline;

    @Override
    public void onComponentRefresh()
    {
        _algorithmRegNumber = DataAccessServices.dao().get(DipFormingRegNumberAlgorithm.class, DipFormingRegNumberAlgorithm.current(), Boolean.TRUE);

        List<DipAssistantManager> assistantManagerList = DataAccessServices.dao().getList(DipAssistantManager.class);
        if (!assistantManagerList.isEmpty())
        {
            _assistantManager = assistantManagerList.get(0);
        }
    }

    public void onClickApply()
    {
        DipSettingsDao.setDipTemplateCollectOptionalDiscipline(dipTemplateCollectOptionalDiscipline);

        if (null == getAssistantManager().getEmployeePost()){
            if (null != _assistantManager.getId())
                DataAccessServices.dao().delete(_assistantManager.getId());
        } else {
            DataAccessServices.dao().saveOrUpdate(_assistantManager);
        }

        DipSettingsManager.instance().dao().setCurrentRegNumberAlgorithm(_algorithmRegNumber);

        deactivate();
    }

    public boolean isEmptyTopOrgUnitHead()
    {
        return TopOrgUnit.getInstance().getHead() == null;
    }

    public DipFormingRegNumberAlgorithm getAlgorithmRegNumber()
    {
        return _algorithmRegNumber;
    }

    public void setAlgorithmRegNumber(DipFormingRegNumberAlgorithm algorithmRegNumber)
    {
        _algorithmRegNumber = algorithmRegNumber;
    }

    public DipAssistantManager getAssistantManager()
    {
        return _assistantManager;
    }

    public void setAssistantManager(DipAssistantManager assistantManager)
    {
        _assistantManager = assistantManager;
    }

    public boolean getDipTemplateCollectOptionalDiscipline(){
        return DipSettingsDao.isDipTemplateCollectOptionalDiscipline();
    }

    public void setDipTemplateCollectOptionalDiscipline(boolean flag){
        dipTemplateCollectOptionalDiscipline = flag;
    }
}
