/* $Id$ */
package ru.tandemservice.unidip.base.bo.DipDocument.ui.TemplateSelect;

import com.google.common.collect.Maps;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaObject;
import ru.tandemservice.unidip.base.entity.eduplan.DiplomaTemplate;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;

import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 28.10.2015
 */
@Configuration
public class DipDocumentTemplateSelect extends BusinessComponentManager
{
    public static final String DIP_DOCUMENT_DS = "dipDocumentDS";
    public static final String DIP_TEMPLATE_DS = "dipTemplateDS";

    public static final String PROP_EDU_PLAN_VERSION = "eduPlanVersion";
    public static final String PROP_PROGRAM_SPECIALIZATION = "programSpecialization";
    public static final String PROP_DIP_TEMPLATE = "dipTemplateBlock";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(DIP_DOCUMENT_DS, documentDSColumns(), documentDSHandler()))
                .addDataSource(selectDS(DIP_TEMPLATE_DS, templateDS()))
                .create();
    }

    @Bean
    public ColumnListExtPoint documentDSColumns()
    {
        IFormatter<EppEduPlanVersion> epvFormatter = epv -> "№" + epv.getFullNumber();
        IFormatter<EduProgramSpecialization> psFormatter = ps -> ps == null ? "" : ps.getDisplayableTitle();

        return columnListExtPointBuilder(DIP_DOCUMENT_DS)
                .addColumn(textColumn(PROP_EDU_PLAN_VERSION, PROP_EDU_PLAN_VERSION).formatter(epvFormatter))
                .addColumn(textColumn(PROP_PROGRAM_SPECIALIZATION, PROP_PROGRAM_SPECIALIZATION).formatter(psFormatter))
                .addColumn(blockColumn(PROP_DIP_TEMPLATE, PROP_DIP_TEMPLATE))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> documentDSHandler()
    {
        return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                List<Long> dipDocumentIds = context.get(UIDefines.COMBO_OBJECT_LIST);

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DiplomaObject.class, "d").column(property("d"))
                        .joinPath(DQLJoinType.inner, DiplomaObject.studentEpv().eduPlanVersion().fromAlias("d"), "epv")
                        .joinPath(DQLJoinType.left, DiplomaObject.content().programSpecialization().fromAlias("d"), "sp")
                        .where(in(property("d.id"), dipDocumentIds))
                        .order(property("epv", EppEduPlanVersion.eduPlan().number()))
                        .order(property("epv", EppEduPlanVersion.number()))
                        .order(property("sp", EduProgramSpecialization.title()));

                Map<MultiKey, DataWrapper> resultMap = Maps.newLinkedHashMap();
                for (DiplomaObject dipDocument : builder.createStatement(context.getSession()).<DiplomaObject>list())
                {
                    EppEduPlanVersion epv = dipDocument.getStudentEpv().getEduPlanVersion();
                    EduProgramSpecialization programSpec = dipDocument.getContent().getProgramSpecialization();

                    MultiKey key = new MultiKey(epv.getId(), programSpec == null ? null : programSpec.getId());
                    if (resultMap.containsKey(key)) continue;

                    DataWrapper wrapper = new DataWrapper(key);
                    wrapper.setProperty(PROP_EDU_PLAN_VERSION, epv);
                    wrapper.setProperty(PROP_PROGRAM_SPECIALIZATION, programSpec);
                    resultMap.put(key, wrapper);
                }
                return ListOutputBuilder.get(input, resultMap.values()).build();
            }
        };
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> templateDS()
    {
        return new EntityComboDataSourceHandler(getName(), DiplomaTemplate.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                EppEduPlanVersion epv = context.get(DipDocumentTemplateSelectUI.PARAM_EPV);
                EduProgramSpecialization specialization = context.get(DipDocumentTemplateSelectUI.PARAM_PROGRAM_SPEC);

                dql.where(eq(property(alias, DiplomaTemplate.eduPlanVersion()), value(epv)));
                FilterUtils.applySelectFilter(dql, alias, DiplomaTemplate.content().programSpecialization(), specialization);
            }
        }
                .filter(DiplomaTemplate.content().educationElementTitle())
                .order(DiplomaTemplate.content().educationElementTitle());
    }
}
