/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unidip.base.bo.DipDocument.ui.OrgUnitStudentTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.base.bo.UniEduProgram.UniEduProgramManager;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unidip.base.bo.DipDocument.logic.DipDocumentOrgUnitStudentDSHandler;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaObject;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.unidip.base.bo.DipDocument.logic.DipDocumentOrgUnitStudentDSHandler.*;

/**
 * @author iolshvang
 * @since 30.09.11 18:38
 */
@Configuration
public class DipDocumentOrgUnitStudentTab extends BusinessComponentManager
{
    public static final String DIP_STUDENT_DS = "dipStudentDS";
    public static final String STUDENT_STATUS_DS = "studentStatusDS";
    public static final String GROUP_DS = "groupDS";

    public static final String PARAM_ENABLED = "Enabled";
    public static final String PARAM_VALUE = "Value";

    public static final String LAST_NAME_PARAM = "lastName";
    public static final String STUDENT_STATUS_PARAM = "studentStatus";
    public static final String COURSE_PARAM = "course";
    public static final String GROUP_PARAM = "group";
    public static final String DOCUMENT_TYPE_PARAM = "documentType";
    public static final String DIPLOMA_CONTENT_ISSUANCE_PARAM = "diplomaContentIssuance";
    public static final String ISSUANCE_EMPTY_PARAM = "issuanceEmpty";
    public static final String ISSUE_DATE_FROM_PARAM = "issueDateFrom";
    public static final String ISSUE_DATE_TO_PARAM = "issueDateTo";
    public static final String SERIA_PARAM = "seria";
    public static final String NUMBER_PARAM = "number";
    public static final String STATE_COMMISSION_DATE_EMPTY_PARAM = "stateCommissionDateEmpty";
    public static final String STATE_COMMISSION_DATE_FROM_PARAM = "stateCommissionDateFrom";
    public static final String STATE_COMMISSION_DATE_TO_PARAM = "stateCommissionDateTo";
    public static final String STATE_COMMISSION_PROTOCOL_NUMBER_PARAM = "stateCommissionProtocolNumber";
    public static final String STATE_COMMISSION_CHAIR_FIO_PARAM = "stateCommissionChairFio";


    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(DIP_STUDENT_DS, dipStudentDSColumns(), dipStudentDSHandler()).create())
                .addAddon(uiAddon(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME, UniEduProgramEducationOrgUnitAddon.class))
                .addDataSource(UniStudentManger.instance().courseDSConfig())
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(STUDENT_STATUS_DS, getName(), StudentStatus.defaultSelectDSHandler(getName())))
                .addDataSource(selectDS(GROUP_DS, groupDSHandler()))
                .addDataSource(selectDS(CommonManager.YES_NO_DS, diplomaContentIssuanceDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint dipStudentDSColumns()
    {
        return columnListExtPointBuilder(DIP_STUDENT_DS)
                .addColumn(checkboxColumn("checkbox"))

                .addColumn(publisherColumn("fio", Student.person().fullFio()).order().required(true))
                .addColumn(textColumn("status", Student.status().title()))
                .addColumn(textColumn("course", Student.course().title()).order())
                .addColumn(textColumn("budget", Student.compensationType().shortTitle()))
                .addColumn(textColumn("group", Student.group().title()).order())
                .addColumn(textColumn("formOU", Student.educationOrgUnit().formativeOrgUnit().title()).order())
                .addColumn(textColumn("terrOU", Student.educationOrgUnit().territorialOrgUnit().title()).order())
                .addColumn(textColumn("eduOU", Student.educationOrgUnit().educationLevelHighSchool().displayableTitle()))
                .addColumn(textColumn("form", Student.educationOrgUnit().developForm().title()).order())
                .addColumn(textColumn("condition", Student.educationOrgUnit().developCondition().title()))
                .addColumn(publisherColumn("diploma", "").entityListProperty(DIPLOMA_COLUMN).formatter(source -> ((DiplomaObject) source).getIssuantDocument()))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> dipStudentDSHandler()
    {
        return new DipDocumentOrgUnitStudentDSHandler(getName());
    }


    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> groupDSHandler()
    {
        EntityComboDataSourceHandler.IQueryCustomizer customizer = (alias, dql, context, filter) ->
        {
            UniEduProgramEducationOrgUnitAddon util = context.get(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME);
            Long orgUnitId = context.get(ORG_UNIT_ID);

            //формируем подзапрос: группы студентов, которые удовлетворяют уже заполненным фильтрам
            String studentAlias = "student";
            DQLSelectBuilder studentDql = new DQLSelectBuilder().fromEntity(Student.class, studentAlias)
                    .column(value(1))
                    .where(eq(property(studentAlias, Student.archival()), value(Boolean.FALSE)))
                    .where(eq(property(studentAlias, Student.group()), property(alias)));
            if (util != null)
                util.applyFilters(studentDql, Student.educationOrgUnit().fromAlias(studentAlias).s());

            //после проверки, что группа принадлежит выпускающему/формирующему подразделению, проверяем, что она находится в списке групп, найденным по условиям фильтров
            dql.joinPath(DQLJoinType.inner, Group.educationOrgUnit().fromAlias(alias), "eou")
                    .where(or(
                            eq(property("eou", EducationOrgUnit.educationLevelHighSchool().orgUnit().id()), value(orgUnitId)),
                            eq(property("eou", EducationOrgUnit.formativeOrgUnit().id()), value(orgUnitId))
                    ))
                    .where(exists(studentDql.buildQuery()))
                    .where(eq(property(alias, Group.archival()), value(Boolean.FALSE)));

            FilterUtils.applySelectFilter(dql, alias, Group.course(), context.<Course>get(COURSE));
            return dql;
        };


        return Group.defaultSelectDSHandler(getName()).customize(customizer);
    }

    @Bean
    protected IDefaultComboDataSourceHandler diplomaContentIssuanceDSHandler()
    {
        return new TwinComboDataSourceHandler(getName())
                .yesTitle("diplomaContentIssuance.yes")
                .noTitle("diplomaContentIssuance.no");
    }
}
