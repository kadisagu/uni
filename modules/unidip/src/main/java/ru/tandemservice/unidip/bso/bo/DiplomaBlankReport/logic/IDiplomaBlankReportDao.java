/* $Id$ */
package ru.tandemservice.unidip.bso.bo.DiplomaBlankReport.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unidip.bso.entity.catalog.DipBlankType;

import javax.validation.constraints.NotNull;
import java.util.Collection;

/**
 * @author azhebko
 * @since 23.01.2015
 */
public interface IDiplomaBlankReportDao extends INeedPersistenceSupport
{
    /** Сохраняет сводный отчет по бланкам дипломов и приложений вместе с печатной формой. */
    public Long createDiplomaBlankReport(@NotNull Long formBy, @NotNull Collection<DipBlankType> blankTypes, @NotNull Collection<OrgUnit> storageLocations);
}