/* $Id$ */
package ru.tandemservice.unidip.base.bo.DipDocument.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaObject;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 16.04.2015
 */
public class DiplomaContentRowDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String DIPLOMA_OBJECT_DS_PARAM = "diplomaObject";

    public DiplomaContentRowDSHandler(String ownerId, Class<? extends DiplomaContentRow> entityClass)
    {
        super(ownerId, entityClass);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final DiplomaObject diplomaObject = context.get(DIPLOMA_OBJECT_DS_PARAM);
        final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(getEntityClass(), "e")
                .column(property("e"))
                .where(eq(property("e", DiplomaContentRow.owner()), value(diplomaObject)));

        final DSOutput output = ListOutputBuilder.get(input, dql.createStatement(context.getSession()).list()).pageable(false).build();
        output.setCountRecord(Math.max(output.getTotalSize(), 1));
        return output;
    }
}