package ru.tandemservice.unidip.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unidip_2x9x3_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.3")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность dipFormingRowAlgorithmOrgUnit

		//  свойство dipFormingRowAlgorithm стало необязательным
        {
            // сделать колонку NULL
            tool.setColumnNullable("dip_form_row_algorithm_ou_t", "dipformingrowalgorithm_id", true);

            // выставляем NULL - если алгоритм совпадает с алгоритмом по умолчанию
            Long currentAlgId = (Long) tool.getUniqueResult("select id from dip_form_row_algorithm_t where currentAlg_p = ?", true);
            tool.executeUpdate("update dip_form_row_algorithm_ou_t set dipFormingRowAlgorithm_id = ? where dipFormingRowAlgorithm_id = ?", null, currentAlgId);
        }
    }
}