package ru.tandemservice.unidip.settings.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Алгоритм формирования строк документа об обучении"
 * Имя сущности : dipFormingRowAlgorithm
 * Файл data.xml : unidip-catalogs.data.xml
 */
public interface DipFormingRowAlgorithmCodes
{
    /** Константа кода (code) элемента : Одна дисциплина - одна строка в дипломе (title) */
    String DISC = "disc";
    /** Константа кода (code) элемента : Одна дисциплина - количество строк в дипломе равно количеству экзаменов (title) */
    String EXAMS = "exams";

    Set<String> CODES = ImmutableSet.of(DISC, EXAMS);
}
