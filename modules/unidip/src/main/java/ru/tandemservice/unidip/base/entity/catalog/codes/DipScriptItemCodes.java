package ru.tandemservice.unidip.base.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Печатные шаблоны и скрипты модуля «Дипломирование»"
 * Имя сущности : dipScriptItem
 * Файл data.xml : unidip-catalogs.data.xml
 */
public interface DipScriptItemCodes
{
    /** Константа кода (code) элемента : Акт списания бланков дипломов и приложений (title) */
    String BLANK_DISPOSAL_ACT = "blankDisposalAct";
    /** Константа кода (code) элемента : Сводный отчет по бланкам дипломов и приложений (title) */
    String DIPLOMA_BLANK_REPORT = "diplomaBlankReport";
    /** Константа кода (code) элемента : Сводный отчет по бланкам дипломов и приложений (по типу) (title) */
    String DIPLOMA_BLANK_REPORT_TYPE = "diplomaBlankReportType";
    /** Константа кода (code) элемента : Сводный отчет по бланкам дипломов и приложений (по месту хранения) (title) */
    String DIPLOMA_BLANK_REPORT_STORAGE_LOCATION = "diplomaBlankReportStorageLocation";

    Set<String> CODES = ImmutableSet.of(BLANK_DISPOSAL_ACT, DIPLOMA_BLANK_REPORT, DIPLOMA_BLANK_REPORT_TYPE, DIPLOMA_BLANK_REPORT_STORAGE_LOCATION);
}
