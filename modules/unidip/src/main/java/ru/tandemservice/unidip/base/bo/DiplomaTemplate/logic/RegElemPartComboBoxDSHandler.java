/* $Id$ */
package ru.tandemservice.unidip.base.bo.DiplomaTemplate.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unidip.base.bo.DiplomaTemplate.DiplomaTemplateManager;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow;
import ru.tandemservice.unidip.base.entity.eduplan.DiplomaTemplate;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 22.07.2014
 */
public class RegElemPartComboBoxDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String DIPLOMA_TEMPLATE = "diplomaTemplate";
    public static final String REG_ELEM_WRAPPED_LIST = "regElemWrappedList";
    public static final String DIPLOMA_CONTENT_ROW = "diplomaContentRow";

    public RegElemPartComboBoxDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final DiplomaTemplate template = context.get(DIPLOMA_TEMPLATE);
        final DiplomaContentRow row = context.get(DIPLOMA_CONTENT_ROW);
        final List<DataWrapper> wrappedList = context.get(REG_ELEM_WRAPPED_LIST);

        final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppRegistryElementPart.class, "p");
        dql.column(property("p"));
        dql.joinPath(DQLJoinType.inner, EppRegistryElementPart.registryElement().fromAlias("p"), "el");

        final DQLSelectBuilder partFCA_dql = DiplomaTemplateManager.instance().dao().getEppRegistryElementPartFControlActionBuilder("e", template, null, wrappedList, row);
        partFCA_dql.column(value(1));
        partFCA_dql.where(eq(property("e", EppRegistryElementPartFControlAction.part()), property("p")));

        dql.where(exists(partFCA_dql.buildQuery()));

        FilterUtils.applySelectFilter(dql, "p", "id", input.getPrimaryKeys());
        FilterUtils.applyLikeFilter(dql, input.getComboFilterByValue(), EppRegistryElement.title().fromAlias("el"), EppRegistryElement.number().fromAlias("el"));

        dql.order(property("el", EppRegistryElement.title()));
        dql.order(property("p", EppRegistryElementPart.number()));

        return DQLSelectOutputBuilder.get(input, dql, context.getSession()).pageable(false).build();
    }
}
