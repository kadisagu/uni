package ru.tandemservice.unidip.base.entity.eduplan.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContent;
import ru.tandemservice.unidip.base.entity.eduplan.DiplomaTemplate;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Шаблон заполнения документа об обучении для УПв
 *
 * Шаблон для формирования новых документов - содержит строки и другие данные для создания на их основе содержимого нового документа.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DiplomaTemplateGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unidip.base.entity.eduplan.DiplomaTemplate";
    public static final String ENTITY_NAME = "diplomaTemplate";
    public static final int VERSION_HASH = -158114304;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDU_PLAN_VERSION = "eduPlanVersion";
    public static final String L_CONTENT = "content";
    public static final String P_COMMENT = "comment";
    public static final String P_TITLE = "title";

    private EppEduPlanVersion _eduPlanVersion;     // УП(в)
    private DiplomaContent _content;     // Содержание документа
    private String _comment;     // Описание

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return УП(в). Свойство не может быть null.
     */
    @NotNull
    public EppEduPlanVersion getEduPlanVersion()
    {
        return _eduPlanVersion;
    }

    /**
     * @param eduPlanVersion УП(в). Свойство не может быть null.
     */
    public void setEduPlanVersion(EppEduPlanVersion eduPlanVersion)
    {
        dirty(_eduPlanVersion, eduPlanVersion);
        _eduPlanVersion = eduPlanVersion;
    }

    /**
     * @return Содержание документа. Свойство не может быть null.
     */
    @NotNull
    public DiplomaContent getContent()
    {
        return _content;
    }

    /**
     * @param content Содержание документа. Свойство не может быть null.
     */
    public void setContent(DiplomaContent content)
    {
        dirty(_content, content);
        _content = content;
    }

    /**
     * @return Описание.
     */
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Описание.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof DiplomaTemplateGen)
        {
            setEduPlanVersion(((DiplomaTemplate)another).getEduPlanVersion());
            setContent(((DiplomaTemplate)another).getContent());
            setComment(((DiplomaTemplate)another).getComment());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DiplomaTemplateGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DiplomaTemplate.class;
        }

        public T newInstance()
        {
            return (T) new DiplomaTemplate();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "eduPlanVersion":
                    return obj.getEduPlanVersion();
                case "content":
                    return obj.getContent();
                case "comment":
                    return obj.getComment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "eduPlanVersion":
                    obj.setEduPlanVersion((EppEduPlanVersion) value);
                    return;
                case "content":
                    obj.setContent((DiplomaContent) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "eduPlanVersion":
                        return true;
                case "content":
                        return true;
                case "comment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "eduPlanVersion":
                    return true;
                case "content":
                    return true;
                case "comment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "eduPlanVersion":
                    return EppEduPlanVersion.class;
                case "content":
                    return DiplomaContent.class;
                case "comment":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DiplomaTemplate> _dslPath = new Path<DiplomaTemplate>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DiplomaTemplate");
    }
            

    /**
     * @return УП(в). Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.eduplan.DiplomaTemplate#getEduPlanVersion()
     */
    public static EppEduPlanVersion.Path<EppEduPlanVersion> eduPlanVersion()
    {
        return _dslPath.eduPlanVersion();
    }

    /**
     * @return Содержание документа. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.eduplan.DiplomaTemplate#getContent()
     */
    public static DiplomaContent.Path<DiplomaContent> content()
    {
        return _dslPath.content();
    }

    /**
     * @return Описание.
     * @see ru.tandemservice.unidip.base.entity.eduplan.DiplomaTemplate#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unidip.base.entity.eduplan.DiplomaTemplate#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends DiplomaTemplate> extends EntityPath<E>
    {
        private EppEduPlanVersion.Path<EppEduPlanVersion> _eduPlanVersion;
        private DiplomaContent.Path<DiplomaContent> _content;
        private PropertyPath<String> _comment;
        private SupportedPropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return УП(в). Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.eduplan.DiplomaTemplate#getEduPlanVersion()
     */
        public EppEduPlanVersion.Path<EppEduPlanVersion> eduPlanVersion()
        {
            if(_eduPlanVersion == null )
                _eduPlanVersion = new EppEduPlanVersion.Path<EppEduPlanVersion>(L_EDU_PLAN_VERSION, this);
            return _eduPlanVersion;
        }

    /**
     * @return Содержание документа. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.eduplan.DiplomaTemplate#getContent()
     */
        public DiplomaContent.Path<DiplomaContent> content()
        {
            if(_content == null )
                _content = new DiplomaContent.Path<DiplomaContent>(L_CONTENT, this);
            return _content;
        }

    /**
     * @return Описание.
     * @see ru.tandemservice.unidip.base.entity.eduplan.DiplomaTemplate#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(DiplomaTemplateGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unidip.base.entity.eduplan.DiplomaTemplate#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(DiplomaTemplateGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return DiplomaTemplate.class;
        }

        public String getEntityName()
        {
            return "diplomaTemplate";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getTitle();
}
