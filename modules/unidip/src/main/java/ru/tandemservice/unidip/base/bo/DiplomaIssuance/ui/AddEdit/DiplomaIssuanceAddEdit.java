/* $Id$ */
package ru.tandemservice.unidip.base.bo.DiplomaIssuance.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.unidip.base.bo.DiplomaIssuance.logic.ContentRowDSHandler;
import ru.tandemservice.unidip.base.bo.DiplomaIssuance.logic.DiplomaContentIssuanceListDSHandler;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentIssuance;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaObject;
import ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.DipDiplomaBlankManager;
import ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank;
import ru.tandemservice.unidip.bso.entity.catalog.codes.DipBlankStateCodes;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 25.06.2014
 */
@Configuration
public class DiplomaIssuanceAddEdit extends BusinessComponentManager
{
    public static final String INSTEAD_DIPLOMA_DS = "insteadDiplomaDS";
    public static final String ISSUANCE_APPLICATION_DS = "issuanceApplicationDS";
    public static final String ISSUANCE_LIST_DS = "issuanceListDS";
    public static final String DS_DIPLOMA_BLANK = "diplomaBlankDS";
    public static final String DS_APPENDIX_BLANK = "appendixBlankDS";

    public static final String DIPLOMA_OBJECT = "diplomaId";
    public static final String ISSUANCE_ID = "issuanceId";

    public static final String BIND_APPENDIX = "appendix";
    public static final String BIND_BLANK = "blank";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        IFormatter<DipDiplomaBlank> blankFormatter = source -> "серия " + source.getSeria() + " номер " + source.getNumber();
        return presenterExtPointBuilder()
                .addDataSource(selectDS(INSTEAD_DIPLOMA_DS, duplicateDiplomaDSHandler()).addColumn(DiplomaIssuance.P_FULL_TITLE))
                .addDataSource(searchListDS(ISSUANCE_APPLICATION_DS, issuanceApplicationDSColumns(), issuanceContentDSHandler()))
                .addDataSource(selectDS(ISSUANCE_LIST_DS, applicationDiplomaDSHandler()))
                .addDataSource(selectDS(DS_DIPLOMA_BLANK, blankDSHandler()).addColumn("seriaNumber", null, blankFormatter))
                .addDataSource(selectDS(DS_APPENDIX_BLANK, blankDSHandler()).addColumn("seriaNumber", null, blankFormatter))
                .create();
    }

    @Bean
    public ColumnListExtPoint issuanceApplicationDSColumns()
    {
        return columnListExtPointBuilder(ISSUANCE_APPLICATION_DS)
                .addColumn(blockColumn("application", "applicationBlock"))
                .addColumn(blockColumn("applicationSeria", "seriaBlock"))
                .addColumn(blockColumn("applicationNumber", "numberBlock"))
                .addColumn(toggleColumn("duplicate", DiplomaContentIssuance.duplicate())
                                   .toggleOnListener("onDuplicate").toggleOnLabel("Будет выдан дубликат приложения без выдачи дубликата документа")
                                   .toggleOffListener("offDuplicate").toggleOffLabel(" ")
                                   .disabled("mvel:!presenter.currentRowInEditMode"))

                .addColumn(blockColumn("duplicateRegNumber", "regNumberBlock"))
                .addColumn(blockColumn("duplicateIssuanceDate", "duplicateIssuanceBlock").hasBlockHeader(true))
                .addColumn(blockColumn("action", "actionBlock").width("1px").hasBlockHeader(true))
                .addColumn(blockColumn("delete", "deleteBlock").width("1px").hasBlockHeader(true))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> issuanceContentDSHandler()
    {
        return new DiplomaContentIssuanceListDSHandler(getName());
    }


    @Bean
    public IBusinessHandler<DSInput, DSOutput> duplicateDiplomaDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), DiplomaIssuance.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                Long id = context.get(ISSUANCE_ID);
                if (null != id)
                    dql.where(DQLExpressions.ne(DQLExpressions.property(DiplomaContentIssuance.id().fromAlias(alias)), DQLExpressions.value(id)));
            }
        }
                .order(DiplomaIssuance.blankNumber())
                .order(DiplomaIssuance.blankSeria())
                .where(DiplomaIssuance.diplomaObject(), DIPLOMA_OBJECT);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> applicationDiplomaDSHandler()
    {
        return new ContentRowDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler blankDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), DipDiplomaBlank.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                final DiplomaObject diplomaObject = context.get(DIPLOMA_OBJECT);
                final boolean appendix = context.get(BIND_APPENDIX);

                final String blankType = DipDiplomaBlankManager.instance().diplomaBlankDao().getDiplomaObjectBlankType(diplomaObject.getContent().getType(), diplomaObject.getContent().isWithSuccess(), appendix);
                if (blankType == null) {
                    dql.where(nothing());
                }
                else
                {
                    final DipDiplomaBlank blank = context.get(BIND_BLANK);
                    final EducationOrgUnit eduOu = diplomaObject.getStudent().getEducationOrgUnit();
                    final OrgUnit eduOrgUnit = eduOu.getEducationLevelHighSchool().getOrgUnit();
                    final OrgUnit formativeOrgUnit = eduOu.getFormativeOrgUnit();

                    dql.where(eq(property(alias, DipDiplomaBlank.blankType().code()), value(blankType)));
                    dql.where(in(property(alias, DipDiplomaBlank.storageLocation()), eduOrgUnit, formativeOrgUnit));
                    dql.where(or(
                            blank == null ? null : eq(property(alias), value(blank)),
                            eq(property(alias, DipDiplomaBlank.blankState().code()), value(DipBlankStateCodes.FREE))
                    ));
                }
            }
        }
            .filter(DipDiplomaBlank.seria())
            .filter(DipDiplomaBlank.number())
            .order(DipDiplomaBlank.seria())
            .order(DipDiplomaBlank.number())
            .pageable(true);
    }
}