package ru.tandemservice.unidip.base.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Печатные шаблоны и скрипты документов об обучении"
 * Имя сущности : dipDocTemplateCatalog
 * Файл data.xml : unidip-catalogs.data.xml
 */
public interface DipDocTemplateCatalogCodes
{
    /** Константа кода (code) элемента : Диплом бакалавра - титул (title) */
    String TITLE_BACHELOR = "titleBachelor";
    /** Константа кода (code) элемента : Диплом бакалавра - приложение 1 (title) */
    String APPLICATION_BACHELOR_1 = "applicationBachelor.1";
    /** Константа кода (code) элемента : Диплом бакалавра - приложение 2 (title) */
    String APPLICATION_BACHELOR_2 = "applicationBachelor.2";
    /** Константа кода (code) элемента : Диплом специалиста - титул (title) */
    String TITLE_SPECIAL = "titleSpecial";
    /** Константа кода (code) элемента : Диплом специалиста - приложение 1 (title) */
    String APPLICATION_SPECIAL_1 = "applicationSpecial.1";
    /** Константа кода (code) элемента : Диплом специалиста - приложение 2 (title) */
    String APPLICATION_SPECIAL_2 = "applicationSpecial.2";
    /** Константа кода (code) элемента : Диплом магистра - титул (title) */
    String TITLE_MAGISTR = "titleMagistr";
    /** Константа кода (code) элемента : Диплом магистра - приложение 1 (title) */
    String APPLICATION_MAGISTR_1 = "applicationMagistr.1";
    /** Константа кода (code) элемента : Диплом магистра - приложение 2 (title) */
    String APPLICATION_MAGISTR_2 = "applicationMagistr.2";
    /** Константа кода (code) элемента : Диплом об окончании аспирантуры - титул (title) */
    String TITLE_POSTGRADUATE = "titlePostGrad";
    /** Константа кода (code) элемента : Диплом об окончании аспирантуры - приложение 1 (title) */
    String APPLICATION_POSTGRADUATE_1 = "applicationPostGrad.1";
    /** Константа кода (code) элемента : Диплом об окончании аспирантуры - приложение 2 (title) */
    String APPLICATION_POSTGRADUATE_2 = "applicationPostGrad.2";
    /** Константа кода (code) элемента : Диплом об окончании адъюнктуры - титул (title) */
    String TITLE_ADUNCTURE = "titleAdunct";
    /** Константа кода (code) элемента : Диплом об окончании адъюнктуры - приложение 1 (title) */
    String APPLICATION_ADUNCTURE_1 = "applicationAdunct.1";
    /** Константа кода (code) элемента : Диплом об окончании адъюнктуры - приложение 2 (title) */
    String APPLICATION_ADUNCTURE_2 = "applicationAdunct.2";
    /** Константа кода (code) элемента : Справка об обучении (title) */
    String EDUCATION_CERTIFICATE = "educationCertificate";
    /** Константа кода (code) элемента : Диплом о среднем профессиональном образовании - титул (title) */
    String TITLE_COLLEGE = "titleCollege";
    /** Константа кода (code) элемента : Диплом о среднем профессиональном образовании - приложение 1 (title) */
    String COLLEGE_APPLICATION_1 = "collegeApplication.1";
    /** Константа кода (code) элемента : Диплом о среднем профессиональном образовании - приложение 2 (title) */
    String COLLEGE_APPLICATION_2 = "collegeApplication.2";

    Set<String> CODES = ImmutableSet.of(TITLE_BACHELOR, APPLICATION_BACHELOR_1, APPLICATION_BACHELOR_2, TITLE_SPECIAL, APPLICATION_SPECIAL_1, APPLICATION_SPECIAL_2, TITLE_MAGISTR, APPLICATION_MAGISTR_1, APPLICATION_MAGISTR_2, TITLE_POSTGRADUATE, APPLICATION_POSTGRADUATE_1, APPLICATION_POSTGRADUATE_2, TITLE_ADUNCTURE, APPLICATION_ADUNCTURE_1, APPLICATION_ADUNCTURE_2, EDUCATION_CERTIFICATE, TITLE_COLLEGE, COLLEGE_APPLICATION_1, COLLEGE_APPLICATION_2);
}
