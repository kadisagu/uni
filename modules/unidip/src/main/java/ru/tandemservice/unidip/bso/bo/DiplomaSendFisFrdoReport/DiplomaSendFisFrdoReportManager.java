/* $Id$ */
package ru.tandemservice.unidip.bso.bo.DiplomaSendFisFrdoReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unidip.bso.bo.DiplomaSendFisFrdoReport.logic.DiplomaSendFisFrdoReportDao;
import ru.tandemservice.unidip.bso.bo.DiplomaSendFisFrdoReport.logic.IDiplomaSendFisFrdoReportDao;

/**
 * @author Alexey Lopatin
 * @since 01.04.2015
 */
@Configuration
public class DiplomaSendFisFrdoReportManager extends BusinessObjectManager
{
    public static DiplomaSendFisFrdoReportManager instance()
    {
        return BusinessObjectManager.instance(DiplomaSendFisFrdoReportManager.class);
    }

    @Bean
    public IDiplomaSendFisFrdoReportDao dao()
    {
        return new DiplomaSendFisFrdoReportDao();
    }
}
