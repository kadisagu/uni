/* $Id$ */
package ru.tandemservice.unidip.bso.bo.DiplomaSendFisFrdoReport.ui.Add;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import ru.tandemservice.unidip.bso.bo.DiplomaSendFisFrdoReport.DiplomaSendFisFrdoReportManager;
import ru.tandemservice.unidip.bso.bo.DiplomaSendFisFrdoReport.ui.Pub.DiplomaSendFisFrdoReportPub;
import ru.tandemservice.unidip.bso.entity.blank.DipDiplomaSendFisFrdoReport;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;

import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 01.04.2015
 */
public class DiplomaSendFisFrdoReportAddUI extends UIPresenter
{
    public static final String PARAM_EDU_LEVEL = "eduLevel";
    public static final String PARAM_PROGRAM_SUBJECT_LIST = "programSubjectList";
    public static final String PARAM_PROGRAM_KIND_LIST = "programKindList";

    private DipDiplomaSendFisFrdoReport _report = new DipDiplomaSendFisFrdoReport();

    @Override
    public void onComponentRefresh()
    {
        if(getSettings().get(PARAM_EDU_LEVEL) == null)
            getSettings().set(PARAM_EDU_LEVEL, new DataWrapper(DiplomaSendFisFrdoReportAdd.eduLevelVoId, "Высшее образование"));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(PARAM_EDU_LEVEL, getSettings().get(PARAM_EDU_LEVEL));
        if (DiplomaSendFisFrdoReportAdd.PROGRAM_SUBJECT_DS.equals(dataSource.getName())) {
            dataSource.put(PARAM_PROGRAM_KIND_LIST, getSettings().get(PARAM_PROGRAM_KIND_LIST));
        }
    }

    public void onClickApply()
    {
        List<EduProgramSubject> programSubjectList = getSettings().get(PARAM_PROGRAM_SUBJECT_LIST);
        List<EduProgramKind> programKindList = getSettings().get(PARAM_PROGRAM_KIND_LIST);
        DataWrapper eduLevel = getSettings().get(PARAM_EDU_LEVEL);
        if (eduLevel.getId() == DiplomaSendFisFrdoReportAdd.eduLevelVoId) {
            getReport().setEduLevel("Высшее образование");
        } else {
            getReport().setEduLevel("Среднее профессиональное образование");
        }
        Long reportId = DiplomaSendFisFrdoReportManager.instance().dao().createReport(getReport(), eduLevel, programKindList, programSubjectList);
        deactivate();
        _uiActivation.asDesktopRoot(DiplomaSendFisFrdoReportPub.class).parameter(IUIPresenter.PUBLISHER_ID, reportId).activate();
    }

    public DipDiplomaSendFisFrdoReport getReport()
    {
        return _report;
    }

    public void setReport(DipDiplomaSendFisFrdoReport report)
    {
        _report = report;
    }
}
