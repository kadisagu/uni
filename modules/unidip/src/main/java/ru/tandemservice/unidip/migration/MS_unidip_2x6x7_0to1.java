package ru.tandemservice.unidip.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unidip_2x6x7_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.15"),
                        new ScriptDependency("org.tandemframework.shared", "1.6.7")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность dipFormingRowAlgorithm

        // создана новая сущность
        {
            // создать таблицу
            if (!tool.tableExists("dip_form_row_algorithm_t"))
            {
                DBTable dbt = new DBTable("dip_form_row_algorithm_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                          new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
                                          new DBColumn("description_p", DBType.createVarchar(1200)).setNullable(false),
                                          new DBColumn("currentalg_p", DBType.BOOLEAN).setNullable(false),
                                          new DBColumn("title_p", DBType.createVarchar(1200))
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("dipFormingRowAlgorithm");
            }
        }


    }
}