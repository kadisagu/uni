/* $Id$ */
package ru.tandemservice.unidip.base.bo.DipDocument.logic;

import java.util.Collection;
import java.util.Map;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;

/**
 * @author iolshvang
 * @since 29.08.11 16:32
 */
public interface IAggMethodDao extends INeedPersistenceSupport
{
    Map<Long, IRowTotalData> getTotalRowData(Collection<Long> rowIds);
}
