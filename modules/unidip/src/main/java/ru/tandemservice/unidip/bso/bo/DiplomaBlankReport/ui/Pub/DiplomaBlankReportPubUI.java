/* $Id$ */
package ru.tandemservice.unidip.bso.bo.DiplomaBlankReport.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import ru.tandemservice.uni.base.bo.UniReport.util.UniReportUtils;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlankReport;

/**
 * @author azhebko
 * @since 22.01.2015
 */
@State(@Bind(key = IUIPresenter.PUBLISHER_ID, binding = "report.id", required = true))
public class DiplomaBlankReportPubUI extends UIPresenter
{
    private DipDiplomaBlankReport _report = new DipDiplomaBlankReport();

    @Override
    public void onComponentRefresh()
    {
        _report = IUniBaseDao.instance.get().getNotNull(getReport().getId());
    }

    public void onClickDelete()
    {
        IUniBaseDao.instance.get().delete(_report.getId());
        this.deactivate();
    }

    public void onClickPrint()
    {
        BusinessComponentUtils.downloadDocument(UniReportUtils.createRenderer(getReport().getId()), true);
    }

    public DipDiplomaBlankReport getReport() { return _report;}
}