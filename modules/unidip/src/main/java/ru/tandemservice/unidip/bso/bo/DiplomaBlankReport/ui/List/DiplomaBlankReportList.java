/* $Id$ */
package ru.tandemservice.unidip.bso.bo.DiplomaBlankReport.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlankReport;

/**
 * @author azhebko
 * @since 22.01.2015
 */
@Configuration
public class DiplomaBlankReportList extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(searchListDS("diplomaBlankReportDS", diplomaBlankDSColumns(), diplomaBlankDSHandler()))
            .create();
    }

    @Bean
    public ColumnListExtPoint diplomaBlankDSColumns()
    {
        return columnListExtPointBuilder("diplomaBlankReportDS")
            .addColumn(indicatorColumn("icon").defaultIndicatorItem(new IndicatorColumn.Item("report", "Отчет")).create())
            .addColumn(publisherColumn("date", DipDiplomaBlankReport.formingDate()).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).width("200px").create())
            .addColumn(textColumn("blankType", DipDiplomaBlankReport.blankType().s()).width("500px").create())
            .addColumn(textColumn("storageLocation", DipDiplomaBlankReport.storageLocation().s()).create())
            .addColumn(actionColumn("print", CommonDefines.ICON_PRINT, "onClickPrint").permissionKey("printDipStorableReport"))
            .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER)
                .alert(FormattedMessage.with().template("diplomaBlankReportDS.delete.alert").parameter(DipDiplomaBlankReport.formingDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).create())
                .permissionKey("deleteDipStorableReport"))
            .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler diplomaBlankDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), DipDiplomaBlankReport.class)
            .order(DipDiplomaBlankReport.formingDate())
            .pageable(true);
    }
}