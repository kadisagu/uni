package ru.tandemservice.unidip.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unidip_2x7x1_5to6 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность diplomaContentIssuance

		// создано свойство blank
		{
			// создать колонку
			tool.createColumn("dip_content_issuance_t", new DBColumn("blank_id", DBType.LONG));

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность diplomaIssuance

		// создано свойство blank
		{
			// создать колонку
			tool.createColumn("dip_issuance_t", new DBColumn("blank_id", DBType.LONG));

		}
    }
}