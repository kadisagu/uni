/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unidip.component.listextract.excludeStudents;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.RtfRowIntercepterRawText;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.IListParagraphPrintFormCreator;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentIssuance;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance;
import ru.tandemservice.unidip.base.entity.order.DipContentIssuanceToDipExtractRelation;
import ru.tandemservice.unidip.base.entity.order.DipStuExcludeExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

import java.util.ArrayList;
import java.util.List;

/**
 * @author iolshvang
 * @since 25.07.11 15:51
 */
public class DipStuExcludeExtractPrint implements IPrintFormCreator<DipStuExcludeExtract>, IListParagraphPrintFormCreator<DipStuExcludeExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, DipStuExcludeExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = createParagraphInjectModifier(extract.getParagraph(), extract);
        CommonListExtractPrint.injectCommonListExtractData(modifier, extract);

        modifier.put("excludeDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getExcludeDate()));
        modifier.put("diploma", null != extract.getDiploma() ? StringUtils.lowerCase(extract.getDiploma().getTitleWithSuccess()) : "");

        final List<DipContentIssuanceToDipExtractRelation> contentIssuanceList = DataAccessServices.dao().getList(DipContentIssuanceToDipExtractRelation.class, DipContentIssuanceToDipExtractRelation.extract(), extract);
        StringBuilder diplomaContent = new StringBuilder();

        if (!contentIssuanceList.isEmpty()){
            diplomaContent.append(" (");
            boolean firstIteration = true;
            for (DipContentIssuanceToDipExtractRelation relation : contentIssuanceList) {
                if (!firstIteration)
                    diplomaContent.append(", ");
                else firstIteration = false;

                DiplomaContentIssuance contentIssuance = relation.getContentIssuance();
                diplomaContent.append("приложение №").append(contentIssuance.getContentListNumber())
                        .append(" ")
                        .append(contentIssuance.getBlankSeria())
                        .append(" №")
                        .append(contentIssuance.getBlankNumber());
                if (contentIssuance.isDuplicate())
                    diplomaContent.append(" дубликат ").append(contentIssuance.getDuplicateRegustrationNumber())
                            .append(" ")
                            .append(DateFormatter.DEFAULT_DATE_FORMATTER.format(contentIssuance.getDuplicateIssuanceDate()));
            }
            diplomaContent.append(")");
        }

        modifier.put("diplomaContent", diplomaContent.toString());

        modifier.modify(document);
        return document;
    }

    @Override
    public void modifyOrderTemplate(RtfInjectModifier modifier, IAbstractOrder order, DipStuExcludeExtract firstExtract)
    {
        String orgUnit_G = firstExtract.getEntity().getEducationOrgUnit().getFormativeOrgUnit().getGenitiveCaseTitle();
        modifier.put("orgUnit_G", orgUnit_G != null ? orgUnit_G : firstExtract.getEntity().getEducationOrgUnit().getFormativeOrgUnit().getTitle());
    }

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, DipStuExcludeExtract firstExtract)
    {
        return CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraph, firstExtract)
                .put("excludeDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(firstExtract.getExcludeDate()))
                .put("educationLevelHighSchoolStr", firstExtract.getEducationLevelHighSchoolStr());
    }

    @Override
    public RtfTableModifier createParagraphTableModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, DipStuExcludeExtract firstExtract)
    {
        RtfTableModifier tableModifier = new RtfTableModifier();

        int cnt = 1;
        List<String[]> paragraphDataLines = new ArrayList<>();
        for (Object ext : paragraph.getExtractList())
        {
            final DipStuExcludeExtract extract = (DipStuExcludeExtract) ext;

            StringBuilder diplomaBuilder = new StringBuilder();
            if (null != extract.getDiploma())
                diplomaBuilder.append(StringUtils.lowerCase(extract.getDiploma().getTitleWithSuccess()));
            final DiplomaIssuance issuance = extract.getIssuance();
            if (null != issuance)
            {
                if (null != issuance.getBlankSeria())
                    diplomaBuilder.append(" ").append(issuance.getBlankSeria());
                if (null != issuance.getBlankNumber())
                    diplomaBuilder.append(" №").append(issuance.getBlankNumber());
            }

            final List<DipContentIssuanceToDipExtractRelation> contentIssuanceList = DataAccessServices.dao().getList(DipContentIssuanceToDipExtractRelation.class, DipContentIssuanceToDipExtractRelation.extract(), extract);

            if (!contentIssuanceList.isEmpty()){
                for (DipContentIssuanceToDipExtractRelation relation : contentIssuanceList) {
                    diplomaBuilder.append("\\line");
                    DiplomaContentIssuance contentIssuance = relation.getContentIssuance();
                    diplomaBuilder.append("приложение №").append(contentIssuance.getContentListNumber())
                            .append(": ")
                            .append(contentIssuance.getBlankSeria())
                            .append(" №")
                            .append(contentIssuance.getBlankNumber());
                    if (contentIssuance.isDuplicate())
                        diplomaBuilder.append(" дубликат ").append(contentIssuance.getDuplicateRegustrationNumber())
                                .append(" ")
                                .append(DateFormatter.DEFAULT_DATE_FORMATTER.format(contentIssuance.getDuplicateIssuanceDate()));
                }
            }

            paragraphDataLines.add(new String[]{String.valueOf(cnt++) + ".", extract.getEntity().getPerson().getFullFio(), diplomaBuilder.toString()});
        }

        tableModifier.put("T", paragraphDataLines.toArray(new String[][]{}));
        tableModifier.put("T", new RtfRowIntercepterRawText());

        return tableModifier;
    }
}