/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unidip.base.bo.DiplomaTemplate.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author iolshvang
 * @since 14.07.11 11:42
 */
@Configuration
public class DiplomaTemplateAddEdit extends BusinessComponentManager
{
    public static final String SPECIALIZATION_DS = "specializationDS";
    public static final String BIND_EDUPLAN_VERSION = "eduplanVersion";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(SPECIALIZATION_DS, specializationDSHandler()))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> specializationDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSpecialization.class) {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(exists(EppEduPlanVersionSpecializationBlock.class,
                                 EppEduPlanVersionSpecializationBlock.eduPlanVersion().s(), context.get(BIND_EDUPLAN_VERSION),
                                 EppEduPlanVersionSpecializationBlock.programSpecialization().s(), property(alias)
                ));
            }
            @Override protected boolean isExternalSortRequired() { return true; }
            @Override protected List<IEntity> sortSelectedValues(List<IEntity> list, Set primaryKeys) { Collections.sort((List) list); return list; }
        }
            .filter(EduProgramSpecialization.title())
            .pageable(false);
    }
}