package ru.tandemservice.unidip.settings.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unidip.settings.entity.DipFormingRegNumberAlgorithm;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Алгоритм формирования номеров документа об обучении
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DipFormingRegNumberAlgorithmGen extends EntityBase
 implements INaturalIdentifiable<DipFormingRegNumberAlgorithmGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unidip.settings.entity.DipFormingRegNumberAlgorithm";
    public static final String ENTITY_NAME = "dipFormingRegNumberAlgorithm";
    public static final int VERSION_HASH = -1907020867;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_DESCRIPTION = "description";
    public static final String P_CURRENT = "current";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private String _description;     // Описание
    private boolean _current;     // Текущий алгоритм
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Описание. Свойство не может быть null.
     */
    @NotNull
    @Length(max=1200)
    public String getDescription()
    {
        return _description;
    }

    /**
     * @param description Описание. Свойство не может быть null.
     */
    public void setDescription(String description)
    {
        dirty(_description, description);
        _description = description;
    }

    /**
     * @return Текущий алгоритм. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public boolean isCurrent()
    {
        return _current;
    }

    /**
     * @param current Текущий алгоритм. Свойство не может быть null и должно быть уникальным.
     */
    public void setCurrent(boolean current)
    {
        dirty(_current, current);
        _current = current;
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof DipFormingRegNumberAlgorithmGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((DipFormingRegNumberAlgorithm)another).getCode());
            }
            setDescription(((DipFormingRegNumberAlgorithm)another).getDescription());
            setCurrent(((DipFormingRegNumberAlgorithm)another).isCurrent());
            setTitle(((DipFormingRegNumberAlgorithm)another).getTitle());
        }
    }

    public INaturalId<DipFormingRegNumberAlgorithmGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<DipFormingRegNumberAlgorithmGen>
    {
        private static final String PROXY_NAME = "DipFormingRegNumberAlgorithmNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof DipFormingRegNumberAlgorithmGen.NaturalId) ) return false;

            DipFormingRegNumberAlgorithmGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DipFormingRegNumberAlgorithmGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DipFormingRegNumberAlgorithm.class;
        }

        public T newInstance()
        {
            return (T) new DipFormingRegNumberAlgorithm();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "description":
                    return obj.getDescription();
                case "current":
                    return obj.isCurrent();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "description":
                    obj.setDescription((String) value);
                    return;
                case "current":
                    obj.setCurrent((Boolean) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "description":
                        return true;
                case "current":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "description":
                    return true;
                case "current":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "description":
                    return String.class;
                case "current":
                    return Boolean.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DipFormingRegNumberAlgorithm> _dslPath = new Path<DipFormingRegNumberAlgorithm>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DipFormingRegNumberAlgorithm");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unidip.settings.entity.DipFormingRegNumberAlgorithm#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Описание. Свойство не может быть null.
     * @see ru.tandemservice.unidip.settings.entity.DipFormingRegNumberAlgorithm#getDescription()
     */
    public static PropertyPath<String> description()
    {
        return _dslPath.description();
    }

    /**
     * @return Текущий алгоритм. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unidip.settings.entity.DipFormingRegNumberAlgorithm#isCurrent()
     */
    public static PropertyPath<Boolean> current()
    {
        return _dslPath.current();
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.unidip.settings.entity.DipFormingRegNumberAlgorithm#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends DipFormingRegNumberAlgorithm> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _description;
        private PropertyPath<Boolean> _current;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unidip.settings.entity.DipFormingRegNumberAlgorithm#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(DipFormingRegNumberAlgorithmGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Описание. Свойство не может быть null.
     * @see ru.tandemservice.unidip.settings.entity.DipFormingRegNumberAlgorithm#getDescription()
     */
        public PropertyPath<String> description()
        {
            if(_description == null )
                _description = new PropertyPath<String>(DipFormingRegNumberAlgorithmGen.P_DESCRIPTION, this);
            return _description;
        }

    /**
     * @return Текущий алгоритм. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unidip.settings.entity.DipFormingRegNumberAlgorithm#isCurrent()
     */
        public PropertyPath<Boolean> current()
        {
            if(_current == null )
                _current = new PropertyPath<Boolean>(DipFormingRegNumberAlgorithmGen.P_CURRENT, this);
            return _current;
        }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.unidip.settings.entity.DipFormingRegNumberAlgorithm#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(DipFormingRegNumberAlgorithmGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return DipFormingRegNumberAlgorithm.class;
        }

        public String getEntityName()
        {
            return "dipFormingRegNumberAlgorithm";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
