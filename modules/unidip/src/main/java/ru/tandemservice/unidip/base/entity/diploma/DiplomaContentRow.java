package ru.tandemservice.unidip.base.entity.diploma;

import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.google.common.collect.ComparisonChain;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Ordering;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import ru.tandemservice.unidip.base.entity.diploma.gen.DiplomaContentRowGen;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;

import java.util.*;

/**
 * Запись в дипломе
 * <p/>
 * значимая строка в дипломе - за что, сколько часов, что получил, дополнительные данные
 * (данный объект может иметь наследников для кастомной логики печати, например)
 */
public abstract class DiplomaContentRow extends DiplomaContentRowGen implements ITitled
{
    protected enum ROW_LOAD_TYPE
    {
        LOAD_TYPE_LABOR,
        LOAD_TYPE_HOURS,
        LOAD_TYPE_WEEKS
    }

    /** Приоритет блока (для сортировки в рамках карточки диплома) */
    protected abstract int getBlockPriority();

    /** В неделях ли турдоемкость для направлений, не относящихся к ВО 2009 и 2013 года */
    protected abstract boolean isLoadInWeeksForOKSO_and_SPO();

    /** Заголовок блока со строками */
    protected abstract String getPageTitle();

    /** Коды типов элементов реестра, на которые может ссылаться строка */
    public abstract List<String> getRelatedRegistryElementTypeCodes();

    public static class DiplomaRowConfig {
        public final IEntityMeta meta;
        public final boolean oldLoadInWeeks;
        public final String blockTitle;

        public String getLoadColumnTitle(boolean labor) {
            if (labor) return "Трудоемкость (в з.е.)";
            return oldLoadInWeeks ? "Нагрузка (в неделях)" : "Нагрузка (в часах)";
        }
        public DiplomaRowConfig(IEntityMeta meta, boolean oldLoadInWeeks, String blockTitle) {
            this.meta = meta;
            this.oldLoadInWeeks = oldLoadInWeeks;
            this.blockTitle = blockTitle;
        }
    }

    // Собираем все дочерние классы, сортируем по приоритету
    @SuppressWarnings("unchecked")
    public static final Supplier<Map<Class<? extends DiplomaContentRow>, DiplomaRowConfig>> DIPLOMA_BLOCK_CLASSES = Suppliers.memoize(() -> {
        final Map<Integer, DiplomaRowConfig> map = new TreeMap<>(); // Сортируем блоки по приоритету
        try
        {
            for (IEntityMeta childMeta : CommonBaseUtil.getChildMetaSupplier(DiplomaContentRow.class, true).get())
            {
                // Чтобы не юзать рефлекшн и что-то по имени доставать, просто создаем инстанс и юзаем абстрактный метод
                final DiplomaContentRow fakeRow = (DiplomaContentRow) childMeta.getEntityClass().newInstance();
                final DiplomaRowConfig config = new DiplomaRowConfig(childMeta, fakeRow.isLoadInWeeksForOKSO_and_SPO(), fakeRow.getPageTitle());
                if (map.put(fakeRow.getBlockPriority(), config) != null) {
                    throw new IllegalStateException("Block priority must be unique");
                }
            }
        }
        catch (InstantiationException | IllegalAccessException e) {
            throw CoreExceptionUtils.getRuntimeException(e);
        }

        final ImmutableMap.Builder<Class<? extends DiplomaContentRow>, DiplomaRowConfig> builder = ImmutableMap.builder();
        for (DiplomaRowConfig config : map.values()) {
            builder.put(config.meta.getEntityClass(), config);
        }
        return builder.build();
    });

    @EntityDSLSupport(parts = {DiplomaContentRow.P_TITLE, DiplomaContentRow.P_THEME})
    @Override
    public String getTitleWithTheme()
    {
        if (null != this.getTheme() && !this.getTheme().isEmpty())
            return getTitle() + " «" + getTheme() + "»";

        return getTitle();
    }

    /**
     * Номер семестра для отображения в интерфейсе и при печати. Вместо нуля выводится пустая строка.
     */
    @EntityDSLSupport(parts = {DiplomaContentRow.P_TERM})
    public String getTermAsString()
    {
        return getTerm() == 0 ? StringUtils.EMPTY : String.valueOf(getTerm());
    }

    public Double getLoadAsDouble()
    {
        return UniEppUtils.wrap(this.getLoadAsLong());
    }

    public void setLoadAsDouble(final Double value)
    {
        final Long load = UniEppUtils.unwrap(value);
        this.setLoadAsLong(load);
    }

    public Double getAudLoadAsDouble()
    {
        return UniEppUtils.wrap(this.getAudLoadAsLong());
    }

    public void setAudLoadAsDouble(final Double value)
    {
        final Long load = UniEppUtils.unwrap(value);
        this.setAudLoadAsLong(load);
    }

    public Double getWeeksAsDouble()
    {
        Long weeks = this.getWeeksAsLong();
        return UniEppUtils.wrap((null == weeks || weeks < 0) ? null : weeks);
    }

    public void setWeeksAsDouble(final Double value)
    {
        Long weeks = UniEppUtils.unwrap(value);
        this.setWeeksAsLong(weeks);
    }

    public Double getLaborAsDouble()
    {
        Long labor = this.getLaborAsLong();
        return UniEppUtils.wrap((null == labor || labor < 0) ? null : labor);
    }

    public void setLaborAsDouble(final Double value)
    {
        Long labor = UniEppUtils.unwrap(value);
        this.setLaborAsLong(labor);
    }

    @EntityDSLSupport
    @Override
    public Double getCommonLoad()
    {
        switch (getRowLoadType())
        {
            case LOAD_TYPE_LABOR:
                return getLaborAsDouble();
            case LOAD_TYPE_HOURS:
                return getLoadAsDouble();
            case LOAD_TYPE_WEEKS:
                return getWeeksAsDouble();
        }
        throw new IllegalStateException();
    }

    public void setCommonLoad(Double value)
    {
        switch (getRowLoadType())
        {
            case LOAD_TYPE_LABOR:
                setLaborAsDouble(value);
                break;
            case LOAD_TYPE_HOURS:
                setLoadAsDouble(value);
                break;
            case LOAD_TYPE_WEEKS:
                setWeeksAsDouble(value);
                break;
        }
    }

    @EntityDSLSupport
    @Override
    public String getRowLoadTitle()
    {
        switch (getRowLoadType())
        {
            case LOAD_TYPE_LABOR:
                return "Трудоемкость (в з.е.)";
            case LOAD_TYPE_HOURS:
                return "Нагрузка (в часах)";
            case LOAD_TYPE_WEEKS:
                return "Нагрузка (в неделях)";
        }
        throw new IllegalStateException();
    }

    private ROW_LOAD_TYPE _row_load_type_cached;
    private ROW_LOAD_TYPE getRowLoadType()
    {
        if (_row_load_type_cached == null)
        {
            // Если в документе (шаблоне) часы в ЗЕ, то и в строке будут ЗЕ
            if (!getOwner().isLoadInWeeks()) {
                _row_load_type_cached = ROW_LOAD_TYPE.LOAD_TYPE_LABOR;
            }
            else {
                // Если в документе (шаблоне) часы в неделях, то и в строке будут часы или недели (для практик и ИГА)
                _row_load_type_cached = isLoadInWeeksForOKSO_and_SPO() ? ROW_LOAD_TYPE.LOAD_TYPE_WEEKS : ROW_LOAD_TYPE.LOAD_TYPE_HOURS;
            }
        }
        return _row_load_type_cached;
    }

    /** @return доступна ли форма итогового контроля для данной строки (блока). */
    public abstract boolean isAllowFCA(EppFControlActionType fca);

    public static final Comparator<DiplomaContentRow> TITLE_AND_TERM_COMPARATOR = (DiplomaContentRow o1, DiplomaContentRow o2) -> ComparisonChain.start()
            .compare(o1, o2, CommonCollator.TITLED_COMPARATOR)
            .compare(o1.getTerm(), o2.getTerm())
            .compare(o1.getId(), o2.getId(), Ordering.natural().nullsFirst())
            .result();
}