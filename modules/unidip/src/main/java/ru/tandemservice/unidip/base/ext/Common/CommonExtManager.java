/* $Id:$ */
package ru.tandemservice.unidip.base.ext.Common;

import org.apache.commons.collections15.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLSelectableQuery;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.ModuleStatusReportBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.util.IModuleStatusReporter;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentIssuance;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaObject;
import ru.tandemservice.unidip.base.entity.eduplan.DiplomaEpvTemplateDefaultRel;
import ru.tandemservice.unidip.base.entity.order.DipStuExcludeExtract;
import ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank;
import ru.tandemservice.unidip.bso.entity.catalog.codes.DipBlankStateCodes;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unimove.entity.catalog.codes.ExtractStatesCodes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 2/11/14
 */
@Configuration
public class CommonExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private CommonManager _commonManager;

    @Bean
    public ItemListExtension<IModuleStatusReporter> moduleStatusExtPoint()
    {
        return itemListExtension(_commonManager.moduleStatusExtPoint())
            .add("unidip", () -> {
                IUniBaseDao dao = IUniBaseDao.instance.get();
                List<String> result = new ArrayList<>();
                String alias = "a";

                Date date = new Date();
                Integer cYear = CoreDateUtils.getYear(date);

                final List<Object[]> templateCountList = dao.getList(new DQLSelectBuilder().fromDataSource(new DQLSelectBuilder().fromEntity(DiplomaEpvTemplateDefaultRel.class, alias)
                        .column(property(alias, DiplomaEpvTemplateDefaultRel.eduPlanVersion().number()), "num")
                        .column(DQLFunctions.count(property(alias, DiplomaEpvTemplateDefaultRel.id())), "c")
                        .group(property(alias, DiplomaEpvTemplateDefaultRel.eduPlanVersion().number())).buildQuery(), "f")
                        .column(property("f.num"))
                        .column(property("f.c"))
                        .top(3)
                        .order(property("f.c"), OrderDirection.desc));

                final List<Long> averageTempCountList = dao.getList(new DQLSelectBuilder().fromEntity(DiplomaEpvTemplateDefaultRel.class, alias)
                        .column(DQLFunctions.count(property(alias, DiplomaEpvTemplateDefaultRel.id())))
                        .group(property(alias, DiplomaEpvTemplateDefaultRel.eduPlanVersion().number())));

                Long averageTempCount = 0L;
                for (Long value: averageTempCountList) {
                    averageTempCount += value;
                }


                List<String> yearAndNppList = new ArrayList<>();

                final List<Integer> years = dao.getList(new DQLSelectBuilder().fromEntity(DiplomaObject.class, alias)
                        .joinEntity(alias, DQLJoinType.inner, EppStudentWorkPlanElement.class, "b",
                                eq(property(alias, DiplomaObject.studentEpv().id()), property("b", EppStudentWorkPlanElement.studentEduPlanVersion().id())))
                        .column(DQLFunctions.max(DQLFunctions.year(property("b", EppStudentWorkPlanElement.modificationDate()))))
                        .group(property(alias, DiplomaObject.id()))
                        .distinct());

                Collections.sort(years);

                final List<OrgUnit> grOrgUnits = dao.getList(new DQLSelectBuilder().fromEntity(DiplomaObject.class, alias)
                        .column(property(alias, DiplomaObject.student().educationOrgUnit().groupOrgUnit()))
                        .order(property(alias, DiplomaObject.student().educationOrgUnit().groupOrgUnit().title()))
                        .distinct());


                final IDQLSelectableQuery dipQuery = new DQLSelectBuilder().fromEntity(DiplomaObject.class, alias)
                        .joinEntity(alias, DQLJoinType.inner, EppStudentWorkPlanElement.class, "b",
                                eq(property(alias, DiplomaObject.studentEpv().id()), property("b", EppStudentWorkPlanElement.studentEduPlanVersion().id())))
                        .group(property(alias, DiplomaObject.id()))
                        .column(property(alias), "diploma")
                        .column(DQLFunctions.max(DQLFunctions.year(property("b", EppStudentWorkPlanElement.modificationDate()))), "year")
                        .buildQuery();

                for (Integer year : years) {


                    for (OrgUnit orgUnit : grOrgUnits) {

                        yearAndNppList.add(year + ", " + orgUnit.getTitle());

                        yearAndNppList.add(" • число документов об обучении: " + dao.getCount(
                                new DQLSelectBuilder().fromDataSource(dipQuery, "dip")
                                        .where(eq(property("dip.diploma", DiplomaObject.student().educationOrgUnit().groupOrgUnit().id()), value(orgUnit.getId())))
                                        .where(eq(property("dip.year"), value(year)))
                                        .column(property("dip.diploma.id"))
                        ));
                        yearAndNppList.add(" • число документов об обучении с фактами выдачи: " + dao.getCount(
                                new DQLSelectBuilder().fromDataSource(dipQuery, "dip")
                                        .where(eq(property("dip.diploma", DiplomaObject.student().educationOrgUnit().groupOrgUnit().id()), value(orgUnit.getId())))
                                        .where(eq(property("dip.year"), value(year)))
                                        .joinEntity("dip", DQLJoinType.inner, DiplomaIssuance.class, "c", eq(property("dip.diploma", DiplomaObject.id()), property("c", DiplomaIssuance.diplomaObject().id())))
                                        .column(property("dip.diploma.id"))
                        ));
                        yearAndNppList.add(" • число документов об обучении с несколькими фактами выдачи: " + dao.getCount(
                                        new DQLSelectBuilder().fromDataSource(new DQLSelectBuilder().fromDataSource(dipQuery, "dip")
                                        .where(eq(property("dip.diploma", DiplomaObject.student().educationOrgUnit().groupOrgUnit().id()), value(orgUnit.getId())))
                                        .where(eq(property("dip.year"), value(year)))
                                        .joinEntity("dip", DQLJoinType.inner, DiplomaIssuance.class, "c", eq(property("dip.diploma", DiplomaObject.id()), property("c", DiplomaIssuance.diplomaObject().id())))
                                        .column(property("dip.diploma.id"), "id")
                                        .group(property("dip.diploma.id"))
                                        .column(DQLFunctions.count(property("c")), "count").buildQuery(), "f")
                                .where(gt(property("f.count"), value(1)))
                                .column(property("f.id"))
                                .distinct()

                        ));
                        yearAndNppList.add(" • число документов об обучении со вторым и более приложениями: " + dao.getCount(
                                new DQLSelectBuilder().fromDataSource(new DQLSelectBuilder().fromDataSource(dipQuery, "dip")
                                            .where(eq(property("dip.diploma", DiplomaObject.student().educationOrgUnit().groupOrgUnit().id()), value(orgUnit.getId())))
                                            .where(eq(property("dip.year"), value(year)))
                                            .joinEntity("dip", DQLJoinType.inner, DiplomaIssuance.class, "c", eq(property("dip.diploma", DiplomaObject.id()), property("c", DiplomaIssuance.diplomaObject().id())))
                                            .joinEntity("c", DQLJoinType.inner, DiplomaContentIssuance.class, "d", eq(property("c", DiplomaIssuance.id()), property("d", DiplomaContentIssuance.diplomaIssuance().id())))
                                            .column(property("dip.diploma.id"), "id")
                                            .group(property("dip.diploma.id"))
                                            .column(DQLFunctions.count(property("d")), "count").buildQuery(), "f")
                                        .where(gt(property("f.count"), value(1)))
                                        .column(property("f.id"))
                                        .distinct()
                        ));
                        yearAndNppList.add(" • число документов об обучении со скан-копиями: " + dao.getCount(
                                new DQLSelectBuilder().fromDataSource(dipQuery, "dip")
                                        .where(eq(property("dip.diploma", DiplomaObject.student().educationOrgUnit().groupOrgUnit().id()), value(orgUnit.getId())))
                                        .where(eq(property("dip.year"), value(year)))
                                        .joinEntity("dip", DQLJoinType.inner, DiplomaIssuance.class, "c", eq(property("dip.diploma", DiplomaObject.id()), property("c", DiplomaIssuance.diplomaObject().id())))
                                        .where(isNotNull(property("c", DiplomaIssuance.scanCopy())))
                                        .column(property("dip.diploma.id"))
                        ));

                    }

                }


                result.add("Число бланков дипломов и приложений:");
                result.add(" • свободные - " + dao.getCount(new DQLSelectBuilder().fromEntity(DipDiplomaBlank.class, alias)
                        .where(eq(property(alias, DipDiplomaBlank.blankState().code()), value(DipBlankStateCodes.FREE)))));
                result.add(" • зарезервированные - " + dao.getCount(new DQLSelectBuilder().fromEntity(DipDiplomaBlank.class, alias)
                        .where(eq(property(alias, DipDiplomaBlank.blankState().code()), value(DipBlankStateCodes.RESERVED)))));
                result.add(" • связанные - " + dao.getCount(new DQLSelectBuilder().fromEntity(DipDiplomaBlank.class, alias)
                        .where(eq(property(alias, DipDiplomaBlank.blankState().code()), value(DipBlankStateCodes.LINKED)))));
                result.add(" • выданные - " + dao.getCount(new DQLSelectBuilder().fromEntity(DipDiplomaBlank.class, alias)
                        .where(eq(property(alias, DipDiplomaBlank.blankState().code()), value(DipBlankStateCodes.ISSUED)))));
                result.add(" • списанные - " + dao.getCount(new DQLSelectBuilder().fromEntity(DipDiplomaBlank.class, alias)
                        .where(eq(property(alias, DipDiplomaBlank.blankState().code()), value(DipBlankStateCodes.DISPOSAL)))));
                result.add("Число шаблонов дипломов для одной версии УП (топ-3):");
                result.addAll(CollectionUtils.collect(templateCountList, ModuleStatusReportBuilder.QUERY_RESULT_TRANSFORMER));
                result.add("Среднее число шаблонов дипломов для одной версии УП: " + averageTempCount);
                result.addAll(yearAndNppList);

                // todo Число документов об обучении с видом ОП

                result.add("Число выписок из приказов (согласованных): " + dao.getCount(new DQLSelectBuilder().fromEntity(DipStuExcludeExtract.class, alias)
                        .where(eq(property(alias, DipStuExcludeExtract.state().code()), value(ExtractStatesCodes.ACCEPTED)))
                        .where(eq(DQLFunctions.year(property(alias, DipStuExcludeExtract.createDate())), value(cYear)))
                ));
                result.add("Число выписок из приказов (проведенных): " + dao.getCount(new DQLSelectBuilder().fromEntity(DipStuExcludeExtract.class, alias)
                        .where(eq(property(alias, DipStuExcludeExtract.state().code()), value(ExtractStatesCodes.FINISHED)))
                        .where(eq(DQLFunctions.year(property(alias, DipStuExcludeExtract.createDate())), value(cYear)))
                ));

                return result;
            })
            .create();
    }
}
