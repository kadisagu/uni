/* $Id$ */
package ru.tandemservice.unidip.base.bo.DiplomaIssuance.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentIssuance;

import java.util.List;

/**
 * @author Andrey Avetisov
 * @since 02.07.2014
 */
public class DiplomaContentIssuanceListDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String CONTENT_ISSUANCE_LIST = "contentIssuanceList";

    public DiplomaContentIssuanceListDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        List<DataWrapper> issuanceList = context.get(CONTENT_ISSUANCE_LIST);
        DSOutput output = ListOutputBuilder.get(input, issuanceList).build();
        output.setCountRecord(Math.max(output.getTotalSize(), 1));
        return output;
    }
}
