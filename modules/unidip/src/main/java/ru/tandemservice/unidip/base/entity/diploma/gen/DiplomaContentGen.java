package ru.tandemservice.unidip.base.entity.diploma.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unidip.base.entity.catalog.DipDocumentType;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContent;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Содержание документа об обучении
 *
 * Определяет содержание документа - характеристики самого документа и содержание приложения к диплому,
 * в частности, перечень пройденных мероприятий (дисциплин, практик, защиты ВКР и пр.), объем дисциплины, оценки, названия работ.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DiplomaContentGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unidip.base.entity.diploma.DiplomaContent";
    public static final String ENTITY_NAME = "diplomaContent";
    public static final int VERSION_HASH = -1665738551;
    private static IEntityMeta ENTITY_META;

    public static final String L_TYPE = "type";
    public static final String P_WEEKS_AS_LONG = "weeksAsLong";
    public static final String P_LABOR_AS_LONG = "laborAsLong";
    public static final String L_PROGRAM_SUBJECT = "programSubject";
    public static final String L_PROGRAM_SPECIALIZATION = "programSpecialization";
    public static final String L_PROGRAM_QUALIFICATION = "programQualification";
    public static final String P_EDUCATION_ELEMENT_TITLE = "educationElementTitle";
    public static final String P_WITH_SUCCESS = "withSuccess";

    private DipDocumentType _type;     // Тип документа
    private Long _weeksAsLong;     // Общая трудоемкость образовательной программы (в сотых долях недель)
    private Long _laborAsLong;     // Общая трудоемкость образовательной программы (в сотых долях ЗЕ)
    private EduProgramSubject _programSubject;     // Направление подготовки
    private EduProgramSpecialization _programSpecialization;     // Направленность образовательной программы
    private EduProgramQualification _programQualification;     // Квалификация
    private String _educationElementTitle;     // Название (программы, направления)
    private boolean _withSuccess;     // С отличием

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Тип документа. Свойство не может быть null.
     */
    @NotNull
    public DipDocumentType getType()
    {
        return _type;
    }

    /**
     * @param type Тип документа. Свойство не может быть null.
     */
    public void setType(DipDocumentType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Общая трудоемкость образовательной программы (в сотых долях недель).
     */
    public Long getWeeksAsLong()
    {
        return _weeksAsLong;
    }

    /**
     * @param weeksAsLong Общая трудоемкость образовательной программы (в сотых долях недель).
     */
    public void setWeeksAsLong(Long weeksAsLong)
    {
        dirty(_weeksAsLong, weeksAsLong);
        _weeksAsLong = weeksAsLong;
    }

    /**
     * @return Общая трудоемкость образовательной программы (в сотых долях ЗЕ).
     */
    public Long getLaborAsLong()
    {
        return _laborAsLong;
    }

    /**
     * @param laborAsLong Общая трудоемкость образовательной программы (в сотых долях ЗЕ).
     */
    public void setLaborAsLong(Long laborAsLong)
    {
        dirty(_laborAsLong, laborAsLong);
        _laborAsLong = laborAsLong;
    }

    /**
     * @return Направление подготовки.
     */
    public EduProgramSubject getProgramSubject()
    {
        return _programSubject;
    }

    /**
     * @param programSubject Направление подготовки.
     */
    public void setProgramSubject(EduProgramSubject programSubject)
    {
        dirty(_programSubject, programSubject);
        _programSubject = programSubject;
    }

    /**
     * @return Направленность образовательной программы.
     */
    public EduProgramSpecialization getProgramSpecialization()
    {
        return _programSpecialization;
    }

    /**
     * @param programSpecialization Направленность образовательной программы.
     */
    public void setProgramSpecialization(EduProgramSpecialization programSpecialization)
    {
        dirty(_programSpecialization, programSpecialization);
        _programSpecialization = programSpecialization;
    }

    /**
     * @return Квалификация.
     */
    public EduProgramQualification getProgramQualification()
    {
        return _programQualification;
    }

    /**
     * @param programQualification Квалификация.
     */
    public void setProgramQualification(EduProgramQualification programQualification)
    {
        dirty(_programQualification, programQualification);
        _programQualification = programQualification;
    }

    /**
     * @return Название (программы, направления).
     */
    @Length(max=255)
    public String getEducationElementTitle()
    {
        return _educationElementTitle;
    }

    /**
     * @param educationElementTitle Название (программы, направления).
     */
    public void setEducationElementTitle(String educationElementTitle)
    {
        dirty(_educationElementTitle, educationElementTitle);
        _educationElementTitle = educationElementTitle;
    }

    /**
     * Признак - "С отличием"
     *
     * @return С отличием. Свойство не может быть null.
     */
    @NotNull
    public boolean isWithSuccess()
    {
        return _withSuccess;
    }

    /**
     * @param withSuccess С отличием. Свойство не может быть null.
     */
    public void setWithSuccess(boolean withSuccess)
    {
        dirty(_withSuccess, withSuccess);
        _withSuccess = withSuccess;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof DiplomaContentGen)
        {
            setType(((DiplomaContent)another).getType());
            setWeeksAsLong(((DiplomaContent)another).getWeeksAsLong());
            setLaborAsLong(((DiplomaContent)another).getLaborAsLong());
            setProgramSubject(((DiplomaContent)another).getProgramSubject());
            setProgramSpecialization(((DiplomaContent)another).getProgramSpecialization());
            setProgramQualification(((DiplomaContent)another).getProgramQualification());
            setEducationElementTitle(((DiplomaContent)another).getEducationElementTitle());
            setWithSuccess(((DiplomaContent)another).isWithSuccess());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DiplomaContentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DiplomaContent.class;
        }

        public T newInstance()
        {
            return (T) new DiplomaContent();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "type":
                    return obj.getType();
                case "weeksAsLong":
                    return obj.getWeeksAsLong();
                case "laborAsLong":
                    return obj.getLaborAsLong();
                case "programSubject":
                    return obj.getProgramSubject();
                case "programSpecialization":
                    return obj.getProgramSpecialization();
                case "programQualification":
                    return obj.getProgramQualification();
                case "educationElementTitle":
                    return obj.getEducationElementTitle();
                case "withSuccess":
                    return obj.isWithSuccess();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "type":
                    obj.setType((DipDocumentType) value);
                    return;
                case "weeksAsLong":
                    obj.setWeeksAsLong((Long) value);
                    return;
                case "laborAsLong":
                    obj.setLaborAsLong((Long) value);
                    return;
                case "programSubject":
                    obj.setProgramSubject((EduProgramSubject) value);
                    return;
                case "programSpecialization":
                    obj.setProgramSpecialization((EduProgramSpecialization) value);
                    return;
                case "programQualification":
                    obj.setProgramQualification((EduProgramQualification) value);
                    return;
                case "educationElementTitle":
                    obj.setEducationElementTitle((String) value);
                    return;
                case "withSuccess":
                    obj.setWithSuccess((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "type":
                        return true;
                case "weeksAsLong":
                        return true;
                case "laborAsLong":
                        return true;
                case "programSubject":
                        return true;
                case "programSpecialization":
                        return true;
                case "programQualification":
                        return true;
                case "educationElementTitle":
                        return true;
                case "withSuccess":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "type":
                    return true;
                case "weeksAsLong":
                    return true;
                case "laborAsLong":
                    return true;
                case "programSubject":
                    return true;
                case "programSpecialization":
                    return true;
                case "programQualification":
                    return true;
                case "educationElementTitle":
                    return true;
                case "withSuccess":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "type":
                    return DipDocumentType.class;
                case "weeksAsLong":
                    return Long.class;
                case "laborAsLong":
                    return Long.class;
                case "programSubject":
                    return EduProgramSubject.class;
                case "programSpecialization":
                    return EduProgramSpecialization.class;
                case "programQualification":
                    return EduProgramQualification.class;
                case "educationElementTitle":
                    return String.class;
                case "withSuccess":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DiplomaContent> _dslPath = new Path<DiplomaContent>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DiplomaContent");
    }
            

    /**
     * @return Тип документа. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContent#getType()
     */
    public static DipDocumentType.Path<DipDocumentType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Общая трудоемкость образовательной программы (в сотых долях недель).
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContent#getWeeksAsLong()
     */
    public static PropertyPath<Long> weeksAsLong()
    {
        return _dslPath.weeksAsLong();
    }

    /**
     * @return Общая трудоемкость образовательной программы (в сотых долях ЗЕ).
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContent#getLaborAsLong()
     */
    public static PropertyPath<Long> laborAsLong()
    {
        return _dslPath.laborAsLong();
    }

    /**
     * @return Направление подготовки.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContent#getProgramSubject()
     */
    public static EduProgramSubject.Path<EduProgramSubject> programSubject()
    {
        return _dslPath.programSubject();
    }

    /**
     * @return Направленность образовательной программы.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContent#getProgramSpecialization()
     */
    public static EduProgramSpecialization.Path<EduProgramSpecialization> programSpecialization()
    {
        return _dslPath.programSpecialization();
    }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContent#getProgramQualification()
     */
    public static EduProgramQualification.Path<EduProgramQualification> programQualification()
    {
        return _dslPath.programQualification();
    }

    /**
     * @return Название (программы, направления).
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContent#getEducationElementTitle()
     */
    public static PropertyPath<String> educationElementTitle()
    {
        return _dslPath.educationElementTitle();
    }

    /**
     * Признак - "С отличием"
     *
     * @return С отличием. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContent#isWithSuccess()
     */
    public static PropertyPath<Boolean> withSuccess()
    {
        return _dslPath.withSuccess();
    }

    public static class Path<E extends DiplomaContent> extends EntityPath<E>
    {
        private DipDocumentType.Path<DipDocumentType> _type;
        private PropertyPath<Long> _weeksAsLong;
        private PropertyPath<Long> _laborAsLong;
        private EduProgramSubject.Path<EduProgramSubject> _programSubject;
        private EduProgramSpecialization.Path<EduProgramSpecialization> _programSpecialization;
        private EduProgramQualification.Path<EduProgramQualification> _programQualification;
        private PropertyPath<String> _educationElementTitle;
        private PropertyPath<Boolean> _withSuccess;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Тип документа. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContent#getType()
     */
        public DipDocumentType.Path<DipDocumentType> type()
        {
            if(_type == null )
                _type = new DipDocumentType.Path<DipDocumentType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Общая трудоемкость образовательной программы (в сотых долях недель).
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContent#getWeeksAsLong()
     */
        public PropertyPath<Long> weeksAsLong()
        {
            if(_weeksAsLong == null )
                _weeksAsLong = new PropertyPath<Long>(DiplomaContentGen.P_WEEKS_AS_LONG, this);
            return _weeksAsLong;
        }

    /**
     * @return Общая трудоемкость образовательной программы (в сотых долях ЗЕ).
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContent#getLaborAsLong()
     */
        public PropertyPath<Long> laborAsLong()
        {
            if(_laborAsLong == null )
                _laborAsLong = new PropertyPath<Long>(DiplomaContentGen.P_LABOR_AS_LONG, this);
            return _laborAsLong;
        }

    /**
     * @return Направление подготовки.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContent#getProgramSubject()
     */
        public EduProgramSubject.Path<EduProgramSubject> programSubject()
        {
            if(_programSubject == null )
                _programSubject = new EduProgramSubject.Path<EduProgramSubject>(L_PROGRAM_SUBJECT, this);
            return _programSubject;
        }

    /**
     * @return Направленность образовательной программы.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContent#getProgramSpecialization()
     */
        public EduProgramSpecialization.Path<EduProgramSpecialization> programSpecialization()
        {
            if(_programSpecialization == null )
                _programSpecialization = new EduProgramSpecialization.Path<EduProgramSpecialization>(L_PROGRAM_SPECIALIZATION, this);
            return _programSpecialization;
        }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContent#getProgramQualification()
     */
        public EduProgramQualification.Path<EduProgramQualification> programQualification()
        {
            if(_programQualification == null )
                _programQualification = new EduProgramQualification.Path<EduProgramQualification>(L_PROGRAM_QUALIFICATION, this);
            return _programQualification;
        }

    /**
     * @return Название (программы, направления).
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContent#getEducationElementTitle()
     */
        public PropertyPath<String> educationElementTitle()
        {
            if(_educationElementTitle == null )
                _educationElementTitle = new PropertyPath<String>(DiplomaContentGen.P_EDUCATION_ELEMENT_TITLE, this);
            return _educationElementTitle;
        }

    /**
     * Признак - "С отличием"
     *
     * @return С отличием. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContent#isWithSuccess()
     */
        public PropertyPath<Boolean> withSuccess()
        {
            if(_withSuccess == null )
                _withSuccess = new PropertyPath<Boolean>(DiplomaContentGen.P_WITH_SUCCESS, this);
            return _withSuccess;
        }

        public Class getEntityClass()
        {
            return DiplomaContent.class;
        }

        public String getEntityName()
        {
            return "diplomaContent";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
