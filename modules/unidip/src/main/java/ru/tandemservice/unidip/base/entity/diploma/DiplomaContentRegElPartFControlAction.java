package ru.tandemservice.unidip.base.entity.diploma;

import com.google.common.base.Preconditions;
import ru.tandemservice.unidip.base.entity.diploma.gen.DiplomaContentRegElPartFControlActionGen;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;

/**
 * Связь записи диплома с частью элемента реестра
 *
 * Определяет перечень МСРП-по-ФК (итоговые формы контроля по дисциплинам), на базе которых строится итоговые назгрузка и оценка по строке диплома
 */
public class DiplomaContentRegElPartFControlAction extends DiplomaContentRegElPartFControlActionGen
{
    public DiplomaContentRegElPartFControlAction() {}

    public DiplomaContentRegElPartFControlAction(DiplomaContentRow row, EppRegistryElementPartFControlAction registryElementPartFControlAction)
    {
        Preconditions.checkNotNull(row.getOwner());

        setOwner(row.getOwner());
        setRegistryElementPartFControlAction(registryElementPartFControlAction);
        setRow(row);
    }
}