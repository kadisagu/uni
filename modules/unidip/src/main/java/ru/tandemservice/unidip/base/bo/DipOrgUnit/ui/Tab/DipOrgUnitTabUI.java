/* $Id$ */
package ru.tandemservice.unidip.base.bo.DipOrgUnit.ui.Tab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;

/**
 * @author Alexey Lopatin
 * @since 29.05.2015
 */
@State({@Bind(key = UIPresenter.PUBLISHER_ID, binding = "orgUnit.id", required = true)})
public class DipOrgUnitTabUI extends UIPresenter
{
    private OrgUnit _orgUnit = new OrgUnit();
    private CommonPostfixPermissionModel _secModel;

    @Override
    public void onComponentRefresh()
    {
        setOrgUnit(DataAccessServices.dao().getNotNull(OrgUnit.class, _orgUnit.getId()));
        setSecModel(new OrgUnitSecModel(getOrgUnit()));
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        _secModel = secModel;
    }
}
