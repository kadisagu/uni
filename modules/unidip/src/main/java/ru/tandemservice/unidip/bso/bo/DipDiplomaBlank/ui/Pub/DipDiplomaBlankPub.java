/* $Id:$ */
package ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.button.ButtonListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author rsizonenko
 * @since 27.12.2014
 */
@Configuration
public class DipDiplomaBlankPub extends BusinessComponentManager {

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .create();
    }

    @Bean
    public ButtonListExtPoint blankActionsButtons()
    {
        return buttonListExtPointBuilder("blankActions")
				.addButton(submitButton("reserve").listener("onClickReserve").permissionKey("reserve_diplomaBlankPub").visible("ui:free"))
				.addButton(submitButton("cancelReserve").listener("onClickCancelReserve").permissionKey("cancelReserve_diplomaBlankPub").visible("ui:reserved"))
				.addButton(submitButton("dispose").listener("onClickDispose").permissionKey("dispose_diplomaBlankPub").visible("ui:free"))
				.addButton(submitButton("cancelDisposal").listener("onClickCancelDisposal").permissionKey("cancelDisposal_diplomaBlankPub").visible("ui:disposed"))
				.addButton(submitButton("changeStorageLocation").listener("onClickChangeStorageLocation").permissionKey("changeStorageLocation_diplomaBlankPub").visible("ui:free"))
				.addButton(submitButton("cancelLink").listener("onClickCancelLink").permissionKey("cancelLink_diplomaBlankPub").visible("ui:possibleCancelLink").alert("message:blankActions.cancelLink.alert"))
				.create();
    }
}
