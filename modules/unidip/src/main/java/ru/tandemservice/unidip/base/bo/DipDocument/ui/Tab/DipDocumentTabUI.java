/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unidip.base.bo.DipDocument.ui.Tab;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;

import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.employee.Student;

/**
 *
 * @author iolshvang
 * @since 25.07.11 12:02
 */
@State({
    @Bind(key = "publisherId", binding = "studentId")
})
public class DipDocumentTabUI extends UIPresenter
{
    public static final String SELECTED_TAB = "selectedTab";

    private Long _studentId;
    private String _selectedTab;
     private Student _student;
    private CommonPostfixPermissionModel _secModel;
    private Map<String, Object> _tabParameter;

    @Override
    public void onComponentRefresh()
    {
        _student = DataAccessServices.dao().getNotNull(_studentId);

        StringUtils.uncapitalize(OrgUnit.class.getSimpleName());

        _secModel = new CommonPostfixPermissionModel("stu");

    }

    // Getters & Setters

    public String getSelectedTab()
    {
        return _selectedTab;
    }

    public void setSelectedTab(String selectedTab)
    {
        _selectedTab = selectedTab;
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        _secModel = secModel;
    }

    public Map<String, Object> getTabParameter()
    {
        return _tabParameter;
    }

    public void setTabParameter(Map<String, Object> tabParameter)
    {
        _tabParameter = tabParameter;
    }

    public Long getStudentId()
    {
        return _studentId;
    }

    public void setStudentId(Long studentId)
    {
        _studentId = studentId;
    }

    public Student getStudent()
    {
        return _student;
    }

    public void setStudent(Student student)
    {
        _student = student;
    }
}
