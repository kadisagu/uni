/* $Id$ */
package ru.tandemservice.unidip.base.bo.DipDocument.ui.TemplateSelect;

import com.google.common.collect.Maps;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unidip.base.bo.DipDocument.DipDocumentManager;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaObject;
import ru.tandemservice.unidip.base.entity.eduplan.DiplomaTemplate;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;

import java.util.List;
import java.util.Map;

import static ru.tandemservice.unidip.base.bo.DipDocument.ui.TemplateSelect.DipDocumentTemplateSelect.*;

/**
 * @author Alexey Lopatin
 * @since 28.10.2015
 */
@State({@Bind(key = DipDocumentTemplateSelectUI.PARAM_DOCUMENT_IDS, binding = "documentIds")})
public class DipDocumentTemplateSelectUI extends UIPresenter
{
    public static final String PARAM_DOCUMENT_IDS = "documentIds";
    public static final String PARAM_EPV = "eduPlanVersion";
    public static final String PARAM_PROGRAM_SPEC = "programSpecialization";

    private List<Long> _documentIds;

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(UIDefines.COMBO_OBJECT_LIST, _documentIds);

        if (dataSource.getName().equals(DIP_TEMPLATE_DS))
        {
            dataSource.put(PARAM_EPV, getCurrentRow().getProperty(PROP_EDU_PLAN_VERSION));
            dataSource.put(PARAM_PROGRAM_SPEC, getCurrentRow().getProperty(PROP_PROGRAM_SPECIALIZATION));
        }
    }

    public void onClickApply()
    {
        IUIDataSource source = getConfig().getDataSource(DIP_DOCUMENT_DS);

        List<DiplomaObject> documents = DataAccessServices.dao().getList(DiplomaObject.class, _documentIds);
        Map<DiplomaObject, DiplomaTemplate> dipDocument2TemplateMap = Maps.newHashMap();
        for (DiplomaObject document : documents)
        {
            EduProgramSpecialization programSpec = document.getContent().getProgramSpecialization();
            MultiKey key = new MultiKey(document.getStudentEpv().getEduPlanVersion().getId(), programSpec == null ? null : programSpec.getId());
            DataWrapper wrapper = getCurrentWrapper(source, key);

            if (null != wrapper)
            {
                DiplomaTemplate dipTemplate = wrapper.get(PROP_DIP_TEMPLATE);
                if (null != dipTemplate)
                    dipDocument2TemplateMap.put(document, dipTemplate);
            }
        }
        DipDocumentManager.instance().dao().fillDocumentRowByTemplate(dipDocument2TemplateMap);
        deactivate();
    }

    private DataWrapper getCurrentWrapper(IUIDataSource source, MultiKey key)
    {
        List<DataWrapper> wrappers = source.getRecords();
        for (DataWrapper wrapper : wrappers)
        {
            if (key.equals(wrapper.getWrapped())) return wrapper;
        }
        return null;
    }

    public DataWrapper getCurrentRow()
    {
        return getConfig().getDataSourceCurrentRecord(DIP_DOCUMENT_DS);
    }

    public DiplomaTemplate getDipTemplate()
    {
        return getCurrentRow().get(PROP_DIP_TEMPLATE);
    }

    public void setDipTemplate(DiplomaTemplate dipTemplate)
    {
        getCurrentRow().set(PROP_DIP_TEMPLATE, dipTemplate);
    }

    public List<Long> getDocumentIds()
    {
        return _documentIds;
    }

    public void setDocumentIds(List<Long> documentIds)
    {
        _documentIds = documentIds;
    }
}
