package ru.tandemservice.unidip.bso.entity.blank;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.unidip.bso.entity.blank.gen.*;

/** @see ru.tandemservice.unidip.bso.entity.blank.gen.DipDiplomaBlankGen */
public class DipDiplomaBlank extends DipDiplomaBlankGen
{
    @EntityDSLSupport(parts = { DipDiplomaBlank.P_SERIA, DipDiplomaBlank.P_NUMBER})
    public String getSeriaAndNumber()
    {
        return getSeria() + " " + getNumber();
    }
}