/* $Id$ */
package ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.ui.StorageLocationChangeForm;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.DipDiplomaBlankManager;
import ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank;

import java.util.Collection;

/**
 * @author azhebko
 * @since 20.01.2015
 */
@Input({
    @Bind(key = DipDiplomaBlankManager.KEY_BLANKS, binding = "blanks", required = true),
    @Bind(key = DipDiplomaBlankManager.KEY_MASS_ACTION, binding = "massRelocation", required = true)})
public class DipDiplomaBlankStorageLocationChangeFormUI extends UIPresenter
{
    private Collection<Long> _blanks;
    private boolean _massRelocation;
    private OrgUnit _storageLocation;

    @Override
    public void onComponentRefresh()
    {
        setStorageLocation(IUniBaseDao.instance.get().<DipDiplomaBlank>getNotNull(_blanks.iterator().next()).getStorageLocation());
    }

    public void onClickApply()
    {
        Integer count = DipDiplomaBlankManager.instance().diplomaBlankDao().doChangeStorageLocation(getBlanks(), getStorageLocation());
        this.deactivate();
        if (isMassRelocation())
            this.getSupport().info("Сменили место хранения " + CommonBaseStringUtil.numberWithPostfixCase(count, "бланка.", "бланков.", "бланков."));
    }

    public Collection<Long> getBlanks() { return _blanks; }
    public void setBlanks(Collection<Long> blanks) { _blanks = blanks; }

    public boolean isMassRelocation() { return _massRelocation; }
    public void setMassRelocation(boolean massRelocation) { _massRelocation = massRelocation; }

    public OrgUnit getStorageLocation() { return _storageLocation; }
    public void setStorageLocation(OrgUnit storageLocation) { _storageLocation = storageLocation; }
}