/* $Id$ */
package ru.tandemservice.unidip.settings.bo.DipSettings.logic;

import com.google.common.collect.Iterables;
import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.EntityIDGenerator;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaObject;
import ru.tandemservice.unidip.settings.entity.DipFormingRegNumberAlgorithm;
import ru.tandemservice.unidip.settings.entity.DipFormingRowAlgorithm;
import ru.tandemservice.unidip.settings.entity.DipFormingRowAlgorithmOrgUnit;
import ru.tandemservice.unidip.settings.entity.codes.DipFormingRegNumberAlgorithmCodes;
import ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit;

import java.util.Comparator;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.substring;

/**
 * @author Alexey Lopatin
 * @since 27.05.2015
 */
@Transactional
public class DipSettingsDao extends UniBaseDao implements IDipSettingsDao
{
    public static final String DIPLOMA_REG_NUMBER_LOCK = "DiplomaRegNumberLock-";

    @Override
    public void createAlgorithm4OwnerOrgUnit()
    {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .column(property("own.id"))
                .fromEntity(EduOwnerOrgUnit.class, "own")
                .joinEntity("own", DQLJoinType.left, DipFormingRowAlgorithmOrgUnit.class, "alg", eq(property("own.id"), property("alg", DipFormingRowAlgorithmOrgUnit.ownerOrgUnit().id())))
                .where(isNull("alg.id"));

        final short entityCode = EntityRuntime.getMeta(DipFormingRowAlgorithmOrgUnit.class).getEntityCode();
        Iterables.partition(builder.createStatement(getSession()).list(), DQL.MAX_VALUES_ROW_NUMBER).forEach(elements -> {
            DQLInsertValuesBuilder insertBuilder = new DQLInsertValuesBuilder(DipFormingRowAlgorithmOrgUnit.class);
            elements.forEach(owner -> {
                insertBuilder.value(DipFormingRowAlgorithmOrgUnit.P_ID, EntityIDGenerator.generateNewId(entityCode));
                insertBuilder.value(DipFormingRowAlgorithmOrgUnit.L_OWNER_ORG_UNIT, owner);
                insertBuilder.addBatch();
            });
            insertBuilder.createStatement(getSession()).execute();
        });
    }

    @Override
    public void setCurrentRowAlgorithm(DipFormingRowAlgorithm algorithm)
    {
        new DQLUpdateBuilder(DipFormingRowAlgorithm.class)
                .set(DipFormingRowAlgorithm.P_CURRENT_ALG, value(Boolean.FALSE))
                .where(eq(property(DipFormingRowAlgorithm.P_CURRENT_ALG), value(Boolean.TRUE)))
                .createStatement(getSession()).execute();

        new DQLUpdateBuilder(DipFormingRowAlgorithm.class)
                .set(DipFormingRowAlgorithm.P_CURRENT_ALG, value(Boolean.TRUE))
                .where(eq(property(DipFormingRowAlgorithm.P_CODE), value(algorithm.getCode())))
                .createStatement(getSession()).execute();
    }

    @Override
    public void setCurrentRegNumberAlgorithm(DipFormingRegNumberAlgorithm algorithm)
    {
        new DQLUpdateBuilder(DipFormingRegNumberAlgorithm.class)
                .set(DipFormingRegNumberAlgorithm.P_CURRENT, value(Boolean.FALSE))
                .where(eq(property(DipFormingRegNumberAlgorithm.P_CURRENT), value(Boolean.TRUE)))
                .createStatement(getSession()).execute();

        if (null != algorithm && null != algorithm.getCode())
        {
            new DQLUpdateBuilder(DipFormingRegNumberAlgorithm.class)
                    .set(DipFormingRegNumberAlgorithm.P_CURRENT, value(Boolean.TRUE))
                    .where(eq(property(DipFormingRegNumberAlgorithm.P_CODE), value(algorithm.getCode())))
                    .createStatement(getSession()).execute();
        }
    }

    @Override
    public String getDipIssuanceNextRegNumber(DiplomaObject dipObject)
    {
        DipFormingRegNumberAlgorithm algorithmRegNumber = DataAccessServices.dao().get(DipFormingRegNumberAlgorithm.class, DipFormingRegNumberAlgorithm.current(), Boolean.TRUE);
        if (null == algorithmRegNumber) return null;

        String regNumber;
        String code = algorithmRegNumber.getCode();

        if (code.equals(DipFormingRegNumberAlgorithmCodes.NUMBER_WITHIN_EDU_YEAR))
            regNumber = getDipIssuanceNextRegNumberWithinEduYear(dipObject);
        else
            throw new IllegalStateException("Unknown algorithm.");

        return regNumber;
    }

    @Override
    public String getDipIssuanceNextRegNumberWithinEduYear(DiplomaObject dipObject)
    {
        final int numberLen = 4;
        final int currentYear = UniBaseUtils.getCurrentYear();

        // Блокировка для префикса
        NamedSyncInTransactionCheckLocker.register(getSession(), DIPLOMA_REG_NUMBER_LOCK + currentYear);

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(DiplomaIssuance.class, "di")
                .column(substring(property("di", DiplomaIssuance.registrationNumber()), 6, numberLen))
                .where(like(property("di", DiplomaIssuance.registrationNumber()), value(currentYear + "-____")));

        final Integer maxNumber = dql.createStatement(getSession()).<String>list().stream()
                .filter(StringUtils::isNumeric).map(Integer::valueOf)
                .max(Comparator.<Integer>naturalOrder())
                .orElse(0);

        if (maxNumber >= ((int) Math.pow(10, numberLen) - 1))
        {
            ContextLocal.getInfoCollector().add("Для " + currentYear + " года не осталось свободных регистрационных номеров.");
            return null;
        }

        return currentYear + "-" + StringUtils.leftPad(String.valueOf(maxNumber + 1), numberLen, '0');
    }

    public static boolean isDipTemplateCollectOptionalDiscipline(){
        IDataSettings data = DataSettingsFacade.getSettings(DipSettingsDao.class.getSimpleName());
        return Boolean.TRUE.equals(data.get(DIP_TEMPLATE_COLLECT_OPTIONAL_DISCIPLINE));
    }

    public static void setDipTemplateCollectOptionalDiscipline(boolean flag){
        IDataSettings data = DataSettingsFacade.getSettings(DipSettingsDao.class.getSimpleName());
        data.set(DIP_TEMPLATE_COLLECT_OPTIONAL_DISCIPLINE, flag);
        DataSettingsFacade.saveSettings(data);
    }
}
