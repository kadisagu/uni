/* $Id$ */
package ru.tandemservice.unidip.bso.bo.DiplomaSendFisFrdoReport.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.BusinessComponentUtils;
import ru.tandemservice.uni.base.bo.UniReport.util.UniReportUtils;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unidip.bso.bo.DiplomaSendFisFrdoReport.ui.Add.DiplomaSendFisFrdoReportAdd;

/**
 * @author Alexey Lopatin
 * @since 01.04.2015
 */
public class DiplomaSendFisFrdoReportListUI extends UIPresenter
{
    public void onClickAddReport()
    {
        getActivationBuilder().asRegion(DiplomaSendFisFrdoReportAdd.class).activate();
    }

    public void onClickPrint()
    {
        BusinessComponentUtils.downloadDocument(UniReportUtils.createRenderer(getListenerParameterAsLong()), true);
    }

    public void onDeleteEntityFromList()
    {
        IUniBaseDao.instance.get().delete(getListenerParameterAsLong());
    }
}
