package ru.tandemservice.unidip.bso.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Состояние бланка"
 * Имя сущности : dipBlankState
 * Файл data.xml : unidip-catalogs.data.xml
 */
public interface DipBlankStateCodes
{
    /** Константа кода (code) элемента : Свободен (title) */
    String FREE = "free";
    /** Константа кода (code) элемента : Зарезервирован (title) */
    String RESERVED = "reserved";
    /** Константа кода (code) элемента : Списан (title) */
    String DISPOSAL = "disposal";
    /** Константа кода (code) элемента : Связан (title) */
    String LINKED = "linked";
    /** Константа кода (code) элемента : Выдан (title) */
    String ISSUED = "issued";

    Set<String> CODES = ImmutableSet.of(FREE, RESERVED, DISPOSAL, LINKED, ISSUED);
}
