/* $Id:$ */
package ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import org.tandemframework.tapsupport.TapSupportUtils;
import org.tandemframework.tapsupport.confirm.ClickButtonAction;
import org.tandemframework.tapsupport.confirm.ConfirmInfo;
import ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.DipDiplomaBlankManager;
import ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.ui.Add.DipDiplomaBlankAdd;
import ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.ui.DisposalActAdd.DipDiplomaBlankDisposalActAdd;
import ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.ui.DisposalForm.DipDiplomaBlankDisposalForm;
import ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.ui.StorageLocationChangeForm.DipDiplomaBlankStorageLocationChangeForm;
import ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank;
import ru.tandemservice.unidip.bso.entity.catalog.DipBlankState;
import ru.tandemservice.unidip.bso.entity.catalog.codes.DipBlankStateCodes;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author rsizonenko
 * @since 24.12.2014
 */
@State({@Bind(key = UIPresenter.PUBLISHER_ID, binding = "orgUnitId")})
public class DipDiplomaBlankListUI extends UIPresenter
{
    private Long _orgUnitId;
    private OrgUnit _orgUnit;

    @Override
    public void onComponentRefresh()
    {
        if (null != _orgUnitId)
            _orgUnit = DataAccessServices.dao().getNotNull(_orgUnitId);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        dataSource.put(DipDiplomaBlankList.BLANK_SERIA, getSettings().get(DipDiplomaBlankList.BLANK_SERIA));
        dataSource.put(DipDiplomaBlankList.BLANK_NUMBER_FROM, getSettings().get(DipDiplomaBlankList.BLANK_NUMBER_FROM));
        dataSource.put(DipDiplomaBlankList.BLANK_NUMBER_TO, getSettings().get(DipDiplomaBlankList.BLANK_NUMBER_TO));
        dataSource.put(DipDiplomaBlankList.BLANK_TYPE, getSettings().get(DipDiplomaBlankList.BLANK_TYPE));
        dataSource.put(DipDiplomaBlankList.STORAGE_LOCATION, getSettings().get(DipDiplomaBlankList.STORAGE_LOCATION));
        dataSource.put(DipDiplomaBlankList.BLANK_STATE, getSettings().get(DipDiplomaBlankList.BLANK_STATE));
        dataSource.put(DipDiplomaBlankList.PARAM_ORG_UNIT_ID, _orgUnitId);
    }

    @Override
    public void onComponentBindReturnParameters(String childRegionName, Map<String, Object> returnedData) {
        if (UIDefines.DIALOG_REGION_NAME.equals(childRegionName)) {
            Collection<IEntity> selected = ((BaseSearchListDataSource) this.getConfig().getDataSource(DipDiplomaBlankList.DIPLOMA_BLANK_SEARCH_LIST)).getOptionColumnSelectedObjects("check");
            selected.clear();
        }
    }

    public void onClickAddBlanks()
    {
        _uiActivation.asRegion(DipDiplomaBlankAdd.class).parameter(UIPresenter.PUBLISHER_ID, _orgUnitId).activate();
    }

    public void onClickDisposeBlanks()
    {
        @SuppressWarnings("unchecked")
        Collection<DipDiplomaBlank> check = (Collection) ((BaseSearchListDataSource) this.getConfig().getDataSource(DipDiplomaBlankList.DIPLOMA_BLANK_SEARCH_LIST)).getOptionColumnSelectedObjects("check");
        if (check.isEmpty())
            throw new ApplicationException("Выберите бланки для списания.");

        Set<DipBlankState> states = new HashSet<>();
        for (DipDiplomaBlank blank: check)
            states.add(blank.getBlankState());

        if (states.size() > 1 || !states.iterator().next().getCode().equals(DipBlankStateCodes.FREE))
            throw new ApplicationException("Нужно выбрать бланки в состоянии «Свободен».");

        this.getActivationBuilder()
            .asRegionDialog(DipDiplomaBlankDisposalForm.class)
            .parameter(DipDiplomaBlankManager.KEY_BLANKS, CommonDAO.ids(check))
            .parameter(DipDiplomaBlankManager.KEY_MASS_ACTION, true)
            .activate();
    }

    public void onClickChangeStorageLocation()
    {
        @SuppressWarnings("unchecked")
        Collection<DipDiplomaBlank> check = (Collection) ((BaseSearchListDataSource) this.getConfig().getDataSource(DipDiplomaBlankList.DIPLOMA_BLANK_SEARCH_LIST)).getOptionColumnSelectedObjects("check");
        if (check.isEmpty())
            throw new ApplicationException("Выберите бланки, находящиеся на одном месте хранения.");

        Set<Long> storageLocations = new HashSet<>();
        Set<DipBlankState> states = new HashSet<>();
        for (DipDiplomaBlank blank: check)
        {
            storageLocations.add(blank.getStorageLocation() == null ? null : blank.getStorageLocation().getId());
            states.add(blank.getBlankState());
        }

        if (states.size() > 1 || !states.iterator().next().getCode().equals(DipBlankStateCodes.FREE))
            throw new ApplicationException("Нужно выбрать бланки в состоянии «Свободен».");

        if (storageLocations.size() > 1)
            throw new ApplicationException("Невозможно сменить место хранения. Выбранные бланки находятся на разных местах хранения.");

        this.getActivationBuilder()
            .asRegionDialog(DipDiplomaBlankStorageLocationChangeForm.class)
            .parameter(DipDiplomaBlankManager.KEY_BLANKS, CommonDAO.ids(check))
            .parameter(DipDiplomaBlankManager.KEY_MASS_ACTION, true)
            .activate();
    }

    public void onClickPrintDisposalAct()
    {
        @SuppressWarnings("unchecked")
        Collection<DipDiplomaBlank> check = (Collection) ((BaseSearchListDataSource) this.getConfig().getDataSource(DipDiplomaBlankList.DIPLOMA_BLANK_SEARCH_LIST)).getOptionColumnSelectedObjects("check");
        check = check.stream()
				.filter(blank -> blank.getBlankState().getCode().equals(DipBlankStateCodes.DISPOSAL) || blank.getBlankState().getCode().equals(DipBlankStateCodes.ISSUED))
				.collect(Collectors.toList());
        if (check.isEmpty())
            throw new ApplicationException("Выберите бланки, имеющие состояние «Списан» или «Выдан».");

        this.getActivationBuilder()
            .asRegionDialog(DipDiplomaBlankDisposalActAdd.class)
            .parameter(DipDiplomaBlankManager.KEY_BLANKS, CommonDAO.ids(check))
            .activate();
    }

    public void onClickDeleteBlank()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public void onClickMassDeleteBlanks()
    {
        @SuppressWarnings("unchecked")
        Collection<DipDiplomaBlank> check = (Collection) ((BaseSearchListDataSource) this.getConfig().getDataSource(DipDiplomaBlankList.DIPLOMA_BLANK_SEARCH_LIST)).getOptionColumnSelectedObjects("check");

        if (check.isEmpty())
        {
            throw new ApplicationException("Выберите бланки для удаления.");
        }

		List<DipDiplomaBlank> freeBlanks = check.stream()
				.filter(blank -> blank.getBlankState().getCode().equals(DipBlankStateCodes.FREE))
				.collect(Collectors.toList());


        if (freeBlanks.isEmpty())
        {
            throw new ApplicationException("Выберите  хотя бы один бланк c состоянием «Свободен».");
        }

        int count = freeBlanks.size();

        if (!"ok".equals(_uiSupport.getClientParameter()))
        {
            final String alert = "Удалить " + CommonBaseStringUtil.numberWithPostfixCase(count, "бланк?", "бланка?", "бланков?");
            ConfirmInfo confirm = new ConfirmInfo(alert, new ClickButtonAction("massDelete", "ok", true));
            TapSupportUtils.displayConfirm(confirm);

            return;
        }

        DipDiplomaBlankManager.instance().diplomaBlankDao().massDeleteBlanks(freeBlanks);
    }


    public boolean isBlankDeleteDisabled()
    {
        DipDiplomaBlank blank = _uiSupport.getDataSourceCurrentValue(DipDiplomaBlankList.DIPLOMA_BLANK_SEARCH_LIST);
        return  (!blank.getBlankState().getCode().equals(DipBlankStateCodes.FREE));
    }

    public String getPermissionKeyView()
    {
        return _orgUnitId == null ? "menuDiplomaBlankRegistry" : new OrgUnitSecModel(getOrgUnit()).getPermission("orgUnit_viewDipBlanksTab");
    }

    public String getPermissionKey(String key)
    {
        String permissionKey = key + "_diplomaBlankList";
        return getOrgUnitId() == null ? permissionKey : new OrgUnitSecModel(getOrgUnit()).getPermission("orgUnit_" + permissionKey);
    }

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }
}