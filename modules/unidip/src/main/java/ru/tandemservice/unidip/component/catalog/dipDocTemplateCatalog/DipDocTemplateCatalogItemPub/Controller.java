/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unidip.component.catalog.dipDocTemplateCatalog.DipDocTemplateCatalogItemPub;

import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultScriptCatalogItemPub.DefaultScriptCatalogItemPubController;
import ru.tandemservice.unidip.base.entity.catalog.DipDocTemplateCatalog;

/**
 * @author vip_delete
 * @since 13.01.2009
 */
public class Controller extends DefaultScriptCatalogItemPubController<DipDocTemplateCatalog, Model, IDAO>
{
    public void onClickDownloadUserTemplate(final IBusinessComponent component) throws Exception
    {
        final DipDocTemplateCatalog item = getModel(component).getCatalogItem();
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName(item.getTemplateFileName() != null ? item.getTemplateFileName() : "title.rtf").document(item.getUserTemplate()), false);
    }
}