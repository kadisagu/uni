/* $Id$ */
package ru.tandemservice.unidip.base.bo.DipDocument;

import com.google.common.base.Preconditions;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;

import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unidip.base.bo.DipDocument.logic.DipDocumentDao;
import ru.tandemservice.unidip.base.bo.DipDocument.logic.DipEpvRegistryRowDSHandler;
import ru.tandemservice.unidip.base.bo.DipDocument.logic.IDipDocumentDao;
import ru.tandemservice.unidip.base.entity.catalog.DipAggregationMethod;
import ru.tandemservice.unidip.base.entity.catalog.DipDocumentType;
import ru.tandemservice.unidip.base.entity.catalog.codes.DipDocumentTypeCodes;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes;
import ru.tandemservice.uniedu.catalog.entity.subjects.codes.EduProgramSubjectIndexCodes;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.Collection;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Vasily Zhukov
 * @since 05.05.2011
 */
@Configuration
public class DipDocumentManager extends BusinessObjectManager
{
    @Bean
    public IDefaultComboDataSourceHandler dipAlgorithmComboDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), DipAggregationMethod.class);
    }

    @Bean
    public IDefaultComboDataSourceHandler markSourceComboDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName());
    }

    public static DipDocumentManager instance()
    {
        return instance(DipDocumentManager.class);
    }

    @Bean
    public IDipDocumentDao dao()
    {
        return new DipDocumentDao();
    }

    @Bean
    public IDefaultComboDataSourceHandler epvRegRowDSHandler()
    {
        return new DipEpvRegistryRowDSHandler(getName());
    }

	/**
	 * Типы выдаваемых документов об обучении, соответствующие данному виду ОП.
	 * @param programKindCode Код вида ОП.
	 * @param subjectIndexCode Код перечня направлений подготовки проф. образования (требуется чтобы отличать аспирантуру от адъюнктуры - см. {@link EduProgramKindCodes#PROGRAMMA_ASPIRANTURY_ADYUNKTURY_}).
	 */
    public static @NotNull Collection<DipDocumentType> getDipDocumentTypes(@NotNull String programKindCode, @Nullable String subjectIndexCode)
    {
        Preconditions.checkNotNull(programKindCode);
        Collection<String> parentDipDocumentTypeCodes = getParentDipDocumentTypeCodes(programKindCode, subjectIndexCode);
		final DQLSelectBuilder dipTypeDql = new DQLSelectBuilder()
				.fromEntity(DipDocumentType.class, "t")
				.column(property("t"))
				.joinPath(DQLJoinType.left, DipDocumentType.parent().fromAlias("t"), "pt")
				.where(or(
						in(property("t", DipDocumentType.code()), parentDipDocumentTypeCodes),
						in(property("pt", DipDocumentType.code()), parentDipDocumentTypeCodes)));
		return IUniBaseDao.instance.get().getList(dipTypeDql);
    }

	/**
	 * Коды типа документов об обучении по виду ОП.
	 * @param programKindCode Код вида ОП.
	 * @param subjectIndexCode Код перечня направлений подготовки проф. образования (требуется чтобы отличать аспирантуру от адъюнктуры - см. {@link EduProgramKindCodes#PROGRAMMA_ASPIRANTURY_ADYUNKTURY_}).
	 */
    private static @NotNull Collection<String> getParentDipDocumentTypeCodes(@NotNull String programKindCode, @Nullable String subjectIndexCode)
    {
        Preconditions.checkNotNull(programKindCode);
        switch (programKindCode)
        {
            case EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_KVALIFITSIROVANNYH_RABOCHIH_SLUJATSHIH_ : return Arrays.asList(DipDocumentTypeCodes.COLLEGE_DIPLOMA, DipDocumentTypeCodes.OTHER, DipDocumentTypeCodes.EDUCATION_REFERENCE);
            case EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV_SREDNEGO_ZVENA : return Arrays.asList(DipDocumentTypeCodes.COLLEGE_DIPLOMA, DipDocumentTypeCodes.OTHER, DipDocumentTypeCodes.EDUCATION_REFERENCE);
            case EduProgramKindCodes.PROGRAMMA_BAKALAVRIATA : return Arrays.asList(DipDocumentTypeCodes.BACHELOR_DIPLOMA, DipDocumentTypeCodes.OTHER, DipDocumentTypeCodes.EDUCATION_REFERENCE);
            case EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV : return Arrays.asList(DipDocumentTypeCodes.SPECIALIST_DIPLOMA, DipDocumentTypeCodes.OTHER, DipDocumentTypeCodes.EDUCATION_REFERENCE);
            case EduProgramKindCodes.PROGRAMMA_MAGISTRATURY : return Arrays.asList(DipDocumentTypeCodes.MAGISTR_DIPLOMA, DipDocumentTypeCodes.OTHER, DipDocumentTypeCodes.EDUCATION_REFERENCE);
            case EduProgramKindCodes.PROGRAMMA_ASPIRANTURY_ADYUNKTURY_ :
            {
                if (subjectIndexCode == null || !(EduProgramSubjectIndexCodes.TITLE_2013_07.equals(subjectIndexCode) || EduProgramSubjectIndexCodes.TITLE_2013_06.equals(subjectIndexCode)))
                    throw new IllegalStateException();

				final String postgraduateOrAdjuncture = EduProgramSubjectIndexCodes.TITLE_2013_07.equals(subjectIndexCode) ? DipDocumentTypeCodes.ADUNCTURE_DIPLOMA_ : DipDocumentTypeCodes.POSTGRADUATE_DIPLOMA;
				return Arrays.asList(postgraduateOrAdjuncture, DipDocumentTypeCodes.OTHER, DipDocumentTypeCodes.EDUCATION_REFERENCE);
            }

            case EduProgramKindCodes.PROGRAMMA_ORDINATURY : return Arrays.asList(DipDocumentTypeCodes.OTHER, DipDocumentTypeCodes.EDUCATION_REFERENCE);
            case EduProgramKindCodes.DOPOLNITELNAYA_PROFESSIONALNAYA_PROGRAMMA :
                return Arrays.asList(DipDocumentTypeCodes.PROFESSION_RETRAIN_DIPLOMA_, DipDocumentTypeCodes.UPPER_PROFESSIONAL_CERTIFICATE, DipDocumentTypeCodes.OTHER, DipDocumentTypeCodes.EDUCATION_REFERENCE);

            default : throw new IllegalStateException(); // неизвестный  вид ОП
        }
    }
}
