/* $Id$ */
package ru.tandemservice.unidip.bso.bo.DiplomaSendFisFrdoReport.ui.Add;

import com.google.common.collect.ImmutableList;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractReadAggregateHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unidip.base.entity.catalog.DipDocumentType;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 01.04.2015
 */
@Configuration
public class DiplomaSendFisFrdoReportAdd extends BusinessComponentManager
{
    public static final String EDU_LEVEL_DS = "eduLevelDS";
    public static final String PROGRAM_SUBJECT_DS = "programSubjectDS";
    public static final String DOCUMENT_TYPE_DS = "documentTypeDS";
    public static final String PROGRAM_KIND_DS = "programKindDS";

    static final ImmutableList<String> VO = ImmutableList.of(EduProgramKindCodes.PROGRAMMA_BAKALAVRIATA, EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV, EduProgramKindCodes.PROGRAMMA_MAGISTRATURY,
			EduProgramKindCodes.PROGRAMMA_ASPIRANTURY_ADYUNKTURY_, EduProgramKindCodes.PROGRAMMA_ORDINATURY, EduProgramKindCodes.PROGRAMMA_INTERNATURY);
    static final ImmutableList<String> SPO = ImmutableList.of(EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_KVALIFITSIROVANNYH_RABOCHIH_SLUJATSHIH_, EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV_SREDNEGO_ZVENA);

	public static final long eduLevelVoId = 0L;
	public static final long eduLevelSpoId = 1L;

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(eduLevelDSConfig())
                .addDataSource(documentTypeDSConfig())
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(PROGRAM_KIND_DS, getName(), EduProgramKind.defaultSelectDSHandler(getName())
						.customize((alias, dql, context, filter) ->
						{
							final DataWrapper eduLevelWrapper = context.get(DiplomaSendFisFrdoReportAddUI.PARAM_EDU_LEVEL);
							if (eduLevelWrapper.getId() == eduLevelVoId)
								return dql.where(in(property(alias, EduProgramKind.code()), VO));
							return dql.where(in(property(alias, EduProgramKind.code()), SPO));
						})))
                .addDataSource(selectDS(PROGRAM_SUBJECT_DS, programSubjectDS()).addColumn(EduProgramSubject.titleWithCode().s()))
                .create();
    }

    @Bean
    public UIDataSourceConfig eduLevelDSConfig()
    {
        return SelectDSConfig.with(EDU_LEVEL_DS, getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(eduLevelDSHandler())
                .treeable(true)
                .create();
    }

    @Bean
    public UIDataSourceConfig documentTypeDSConfig()
    {
        return SelectDSConfig.with(DOCUMENT_TYPE_DS, getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(documentTypeDSHandler())
                .treeable(true)
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> eduLevelDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
				.addRecord(eduLevelVoId, "Высшее образование")
				.addRecord(eduLevelSpoId, "Среднее профессиональное образование");
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> documentTypeDSHandler()
    {
        return new AbstractReadAggregateHandler<DSInput, DSOutput>(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DipDocumentType.class, "d").column("d")
                        .where(eq(property("d", DipDocumentType.stateDocType()), value(Boolean.TRUE)));

                if (!CollectionUtils.isEmpty(input.getPrimaryKeys()))
                {
                    builder.where(in(property("d.id"), input.getPrimaryKeys()));
                    return ListOutputBuilder.get(input, builder.createStatement(context.getSession()).list()).build();
                }

                String filter = input.getComboFilterByValue();
                if (StringUtils.isNotEmpty(filter))
                {
                    builder.where(likeUpper(property("d", DipDocumentType.title()), value(CoreStringUtils.escapeLike(filter, true))));
                }

                Set<DipDocumentType> parts = new LinkedHashSet<>(builder.createStatement(context.getSession()).<DipDocumentType>list());
                return ListOutputBuilder.get(input, HierarchyUtil.listHierarchyItemsWithParents(new ArrayList<>(parts))).build();
            }
        };
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> programSubjectDS()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSubject.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                DataWrapper eduLevel = context.get(DiplomaSendFisFrdoReportAddUI.PARAM_EDU_LEVEL);
                List<EduProgramKind> programKindList = context.get(DiplomaSendFisFrdoReportAddUI.PARAM_PROGRAM_KIND_LIST);

                if (CollectionUtils.isNotEmpty(programKindList))
                    dql.where(in(property(EduProgramSubject.subjectIndex().programKind().id().fromAlias(alias)), programKindList));

                if (eduLevel != null)
				{
                    if (eduLevel.getId() == eduLevelVoId)
                        dql.where(in(property(alias, EduProgramSubject.subjectIndex().programKind().code()), VO));
                    else if (eduLevel.getId() == eduLevelSpoId)
                        dql.where(in(property(alias, EduProgramSubject.subjectIndex().programKind().code()), SPO));
                }
            }
        }
                .filter(EduProgramSubject.subjectCode())
                .filter(EduProgramSubject.title())
                .order(EduProgramSubject.subjectCode())
                .order(EduProgramSubject.title())
                .pageable(true);
    }
}
