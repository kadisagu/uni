/* $Id:$ */
package ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.ui.TransferToFreeStateForm;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.DipDiplomaBlankManager;
import ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.ui.List.DipDiplomaBlankList;
import ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank;
import ru.tandemservice.unidip.bso.entity.catalog.DipBlankState;
import ru.tandemservice.unidip.bso.entity.catalog.codes.DipBlankStateCodes;

import java.util.Collection;

/**
 * @author rsizonenko
 * @since 02.03.2015
 */
public class DipDiplomaBlankTransferToFreeStateFormUI extends UIPresenter {
    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        dataSource.put(DipDiplomaBlankList.BLANK_SERIA, getSettings().get(DipDiplomaBlankList.BLANK_SERIA));
        dataSource.put(DipDiplomaBlankList.BLANK_NUMBER_FROM, getSettings().get(DipDiplomaBlankList.BLANK_NUMBER_FROM));
        dataSource.put(DipDiplomaBlankList.BLANK_NUMBER_TO, getSettings().get(DipDiplomaBlankList.BLANK_NUMBER_TO));
        dataSource.put(DipDiplomaBlankList.BLANK_TYPE, getSettings().get(DipDiplomaBlankList.BLANK_TYPE));
        dataSource.put(DipDiplomaBlankList.STORAGE_LOCATION, getSettings().get(DipDiplomaBlankList.STORAGE_LOCATION));
    }

    public void onClickChangeState()
    {
        @SuppressWarnings("unchecked")
        Collection<DipDiplomaBlank> check = (Collection) ((BaseSearchListDataSource) this.getConfig().getDataSource(DipDiplomaBlankList.DIPLOMA_BLANK_SEARCH_LIST)).getOptionColumnSelectedObjects("check");
        if (check.isEmpty())
            throw new ApplicationException("Необходимо выбрать хотя бы один бланк.");

        final DipBlankState issuedState = DataAccessServices.dao().getByCode(DipBlankState.class, DipBlankStateCodes.ISSUED);

        DipDiplomaBlankManager.instance().diplomaBlankDao().massChangeState(check, issuedState);

    }
}