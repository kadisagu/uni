package ru.tandemservice.unidip.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLUpdateQuery;

import java.sql.SQLException;

/**
 * Сделать поле "УПв студента" в дипломе необязательным.
 * Чтобы получать данные, которые раньше брали из УПв, добавить в содержание диплома поле "квалификация" (поле "направление подготовки" там уже есть).
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unidip_2x11x3_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.11.3")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность diplomaContent
		createColumnQualification(tool);

		////////////////////////////////////////////////////////////////////////////////
		// сущность diplomaObject

		//  свойство studentEpv стало необязательным
		{
			// сделать колонку NULL
			tool.setColumnNullable("dip_object_t", "studentepv_id", true);
		}


    }

	/** Создать кололнку programQualification в сущности diplomaContent и инициализировать ее из УП проф. образования диплома (если у этого диплома УП такой). */
	public static void createColumnQualification(DBTool tool) throws SQLException
	{
		tool.createColumn("dip_content_t", new DBColumn("programqualification_id", DBType.LONG));

		SQLUpdateQuery setColumnsQuery = new SQLUpdateQuery("dip_content_t", "dipContent");
		joinColumnSource(setColumnsQuery.getUpdatedTableFrom(), "dipContent", "eduPlanProf");
		setColumnsQuery.set("programqualification_id", "eduPlanProf.programqualification_id");
		tool.executeUpdate(tool.getDialect().getSQLTranslator().toSql(setColumnsQuery));
	}

	/**
	 * Приджойнить к таблице DiplomaContent таблицу УП проф. образования (через диплом и версию УП студента).
	 * @param dipContentTable Таблица содержания диплома.
	 * @param dipContentAlias Алиас содержания диплома.
	 * @param eduPlanProfAlias Требуемый алиас УП проф. образования.
	 */
	private static void joinColumnSource(final SQLFrom dipContentTable, final String dipContentAlias, final String eduPlanProfAlias)
	{
		dipContentTable
				.innerJoin(SQLFrom.table("dip_object_t", "diploma"), ("diploma.content_id = " + dipContentAlias + ".id"))
				.innerJoin(SQLFrom.table("epp_student_eduplanversion_t", "studentEpv"), "diploma.studentepv_id = studentEpv.id")
				.innerJoin(SQLFrom.table("epp_eduplan_ver_t", "epv"), "studentEpv.eduplanversion_id = epv.id")
				.innerJoin(SQLFrom.table("epp_eduplan_prof_t", eduPlanProfAlias), ("epv.eduplan_id = " + eduPlanProfAlias + ".id"));
	}
}