/* $Id$ */
package ru.tandemservice.unidip.dao.mdbio;

import com.healthmarketscience.jackcess.Database;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;

import java.io.IOException;

/**
 * @author Andrey Avetisov
 * @since 18.03.2015
 */
public interface IDipDocumentIODao
{
    SpringBeanCache<IDipDocumentIODao> instance = new SpringBeanCache<>(IDipDocumentIODao.class.getName());

    /**
     * Экспорт шаблона с существующими дипломами
     * @param mdb файл для экспорта
     * @throws Exception
     */
    @Transactional(propagation= Propagation.SUPPORTS, readOnly=true)
    void exportDiplomaList(Database mdb) throws Exception;

    /** @return { mdb.student_id -> student.id } */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    String importDiplomaList(Database mdb) throws IOException;
}
