/* $Id$ */
package ru.tandemservice.unidip.migration;

import com.google.common.collect.*;
import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.core.common.DBType;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;
import org.tandemframework.shared.commonbase.utils.MigrationUtils.BatchUpdater;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaCourseWorkRow;

import java.util.*;

/**
 * Простите меня если сможете, Мне за мои дела достанется...
 *
 * @author Nikolay Fedorovskih
 * @since 28.05.2015
 */
public class MS_unidip_2x8x1_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[] {
                new ScriptDependency("org.tandemframework", "1.6.17"),
                new ScriptDependency("org.tandemframework.shared", "1.8.1")
        };
    }

    class RowWrapper implements Comparable
    {
        final Long id;
        final Long epvRowId;
        final Integer number;
        final String title;

        // Набор ФИК строки диплома. PairKey([Номер части элемента реестра], [идентификатор ФИК])
        final Set<PairKey<Integer, Long>> fcaSet = new TreeSet<>(new Comparator<PairKey<Integer, Long>>() {
            @Override public int compare(PairKey<Integer, Long> p1, PairKey<Integer, Long> p2) {
                return ComparisonChain.start()
                        .compare(p1.getFirst(), p2.getFirst())
                        .compare(p1.getSecond(), p2.getSecond())
                        .result();
            }
        });

        public RowWrapper(Long id, Long epvRowId, Integer number, String title) {
            this.id = id;
            this.epvRowId = epvRowId;
            this.number = number;
            this.title = title;
        }
        @Override
        public boolean equals(Object obj) {
            return id.equals(((RowWrapper) obj).id);
        }
        @Override
        public int hashCode() {
            return id.hashCode();
        }
        @Override
        public int compareTo(Object o) {
            if (o == this) return 0;
            final RowWrapper w = (RowWrapper) o;
            int i;
            if ((i = Integer.compare(this.number, w.number)) != 0) return i;
            if ((i = Long.compare(this.id, w.id)) != 0) return i;
            return 0;
        }
    }

    private Map<Long, Map<Integer, Set<Long>>> _epvRowsMap;

    private Map<Integer, Set<Long>> getEprTermMap(DBTool tool, Long epvRowId) throws Exception
    {
        if (_epvRowsMap == null) // lazy
        {
            final List<Object[]> items = tool.executeQuery(
                    MigrationUtils.processor(Long.class, Integer.class, Long.class),
                    "select r.id, t.intvalue_p, rca.controlactiontype_id " +
                            "from epp_epvrow_distr_t r " +
                            "join epp_epvrowterm_t rt on rt.row_id=r.id " +
                            "join term_t t on t.id=rt.term_id " +
                            "left join epp_epvrowterm_caction_t rca on rca.rowterm_id=rt.id " +
                            "where exists(select 1 from dip_row_t dr where dr.epvregistryrow_id=r.id)"
            );

            _epvRowsMap = SafeMap.get(TreeMap.class);
            for (Object[] item : items)
            {
                final Long id = (Long) item[0];
                final Integer term = (Integer) item[1];
                final Long caId = (Long) item[2];

                final Map<Integer, Set<Long>> map = _epvRowsMap.get(id);
                Set<Long> caIdSet = map.get(term);
                if (caIdSet == null) {
                    map.put(term, caIdSet = new HashSet<>());
                }
                caIdSet.add(caId);
            }
        }
        return _epvRowsMap.get(epvRowId);
    }

    private Integer getTermForPart(DBTool tool, RowWrapper wrapper, Set<Long> main_fca_set) throws Exception
    {
        // Берем максимальный номер части с экзаменом (курсачом). Если экзаменов (курсачей) нет, берем максимальный номер части для указанных ФИК.
        int max_part_number = 0;
        int max_main_part_number = 0; // экзамен / курсач
        final Map<Integer, Set<Long>> part_fca_map = SafeMap.get(HashSet.class);
        for (PairKey<Integer, Long> fcaEntry : wrapper.fcaSet)
        {
            final Integer partNumber = fcaEntry.getFirst();
            final Long fcaId = fcaEntry.getSecond();
            if (main_fca_set.contains(fcaId) && partNumber > max_main_part_number) {
                max_main_part_number = partNumber;
            }
            if (partNumber > max_part_number) {
                max_part_number = partNumber;
            }
            part_fca_map.get(partNumber).add(fcaId);
        }
        if (max_main_part_number > 0) {
            max_part_number = max_main_part_number;
        }
        else if (max_part_number <= 0) {
            return null; // wtf?
        }

        int normalizedTermNumber = 1;
        for (Map.Entry<Integer, Set<Long>> entry : getEprTermMap(tool, wrapper.epvRowId).entrySet())
        {
            if (normalizedTermNumber == max_part_number) {
                // Проверяем, что в семестре строки УПв есть какая-либо из форм контроля ФИК строки диплома
                final Set<Long> epv_fca_set = entry.getValue();
                final Set<Long> part_fca_set = part_fca_map.get(normalizedTermNumber);
                if (CollectionUtils.containsAny(epv_fca_set, part_fca_set)) {
                    return entry.getKey(); // Теперь мы знаем семестр, который можно записать в строку диплома
                }
                break;
            }
            normalizedTermNumber++;
        }
        return null;
    }

    private Integer getTermForEpv(DBTool tool, RowWrapper wrapper, Set<Long> main_fca_set, Set<Long> all_fca_set) throws Exception
    {
        // Берем максимальный номер семестра строки УПв, в котором есть экзамен (курсовая) либо максимальный семестр, в котором есть ФИК, и записываем номер семестра в строку диплома.
        int max_main_term = 0;
        int max_fca_term = 0;
        for (Map.Entry<Integer, Set<Long>> entry : getEprTermMap(tool, wrapper.epvRowId).entrySet())
        {
            final Set<Long> epvActions = entry.getValue();
            final int term = entry.getKey();
            if (CollectionUtils.containsAny(epvActions, all_fca_set)) {
                if (CollectionUtils.containsAny(epvActions, main_fca_set) && term > max_main_term) {
                    max_main_term = term;
                }
                if (term > max_fca_term) {
                    max_fca_term = term;
                }
            }
        }
        if (max_main_term > 0) {
            max_fca_term = max_main_term;
        }
        if (max_fca_term > 0) {
            return max_fca_term;
        }
        return null;
    }

    private void fillTermsForExamAlg(DBTool tool, Collection<RowWrapper> wrappers, Set<Long> main_fca_set, Map<RowWrapper, Integer> finalMap) throws Exception
    {
        // Предполагаем, что это строки созданные по алгоритму один экзамен - одна строка в дипломе. Расставляем семестры соответственно экзаменам (курсовым) в строке УПв.
        // Берем все семестры строки УПв, в которых есть экзамен (либо курсовые, если это блок курсовых).
        // Если количество таких семестров отличается от количества строк в группе, то пропускаем группу строк, проставляем им ноль. Иначе сортируем строки по номеру и присваиваем номера семестров по порядку следования.

        final Long epvRowId = wrappers.iterator().next().epvRowId;
        final List<Integer> mainTerms = new ArrayList<>();
        for (Map.Entry<Integer, Set<Long>> entry : getEprTermMap(tool, epvRowId).entrySet()) {
            if (CollectionUtils.containsAny(entry.getValue(), main_fca_set)) {
                mainTerms.add(entry.getKey());
            }
        }
        if (mainTerms.size() != wrappers.size()) {
            return; // Что-то не сошлось. Алгоритм не подходит. Семестр не определить.
        }

        final List<RowWrapper> sortedList = new ArrayList<>(wrappers);
        Collections.sort(sortedList);
        for (int i = 0; i < sortedList.size(); i++) {
            finalMap.put(sortedList.get(i), mainTerms.get(i));
            if (!sortedList.get(i).epvRowId.equals(epvRowId)) {
                throw new IllegalArgumentException();
            }
        }
    }

    private void fixCollisionsInBlock(Map<RowWrapper, Integer> map, Set<Long> main_fca_set)
    {
        // Устранение коллизий

        // 1. В блоке диплома не должно быть двух строк, ссылающихся на одну строку УПв и имеющих одинаковый номер семестра
        final Multimap<PairKey<Long, Integer>, RowWrapper> epvRowMap = HashMultimap.create();
        for (Map.Entry<RowWrapper, Integer> entry : map.entrySet()) {
            epvRowMap.put(new PairKey<>(entry.getKey().epvRowId, entry.getValue()), entry.getKey());
        }

        for (Collection<RowWrapper> rows : epvRowMap.asMap().values()) {
            if (rows.size() <= 1) {
                continue; // everything is ok
            }

            // Если для двух строк проставился один и тот же номер семестра, то сравниваем приоритеты группы ФИК (экзамен, потом зачет).
            // Если они совпадают, проставляем таким строкам ноль.
            // Если разные, то строке с наименьшим численным приоритетом (экзамен) выставляем номер семестра, а другой строке 0.
            final Set<RowWrapper> wrappersWithMainFCA = new HashSet<>();
            for (RowWrapper wrapper : rows) {
                for (PairKey<Integer, Long> row_fca : wrapper.fcaSet) {
                    if (main_fca_set.contains(row_fca.getSecond())) {
                        wrappersWithMainFCA.add(wrapper);
                    }
                }
            }

            for (RowWrapper wrapper : rows) {
                if (wrappersWithMainFCA.size() == 1 && wrappersWithMainFCA.contains(wrapper)) {
                    continue;
                }
                map.remove(wrapper);
            }
        }

        // 2. В блоке диплома не должно быть двух строк с одинаковым названием и номером семестра.
        // Всем совпадающим ставим ноль, т.к. невозможно сказать точно, где какой семестр.

        final Multimap<PairKey<String, Integer>, RowWrapper> titleMap = HashMultimap.create();
        for (Map.Entry<RowWrapper, Integer> entry : map.entrySet()) {
            titleMap.put(new PairKey<>(entry.getKey().title, entry.getValue()), entry.getKey());
        }

        for (Collection<RowWrapper> rows : titleMap.asMap().values()) {
            if (rows.size() > 1) {
                for (RowWrapper wrapper : rows) {
                    map.remove(wrapper);
                }
            }
        }
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        {
            // Таблицы для дисциплин по блокам не нужны - своих полей нет у этих сущностей
            tool.dropTable("dip_row_state_exam_t", true);
            tool.dropTable("dip_row_qual_work_t", true);
            tool.dropTable("dip_row_practice_t", true);
            tool.dropTable("dip_row_opt_t", true);
            tool.dropTable("dip_row_disc_t", true);
            tool.dropTable("dip_row_c_work_t", true);
        }

        // сущность diplomaContentRow - свойство partNumber переименовано в term и стало обязательным

        if (tool.columnExists("dip_row_t", "term_p")) {
            return; // миграция уже отработала
        }
        tool.renameColumn("dip_row_t", "partnumber_p", "term_p");

        // Сначала всем проставим ноль
        tool.executeUpdate("update dip_row_t set term_p=?", 0);

        // сделать колонку NOT NULL
        tool.setColumnNullable("dip_row_t", "term_p", false);

        // Теперь надо проставить семестр тем строкам, для которых это возможно.
        // А возможно это только если у строки диплома есть ссылка на строку УПв.

        // Получаем [id контента диплома], [discriminator], [id строки диплома], [название строки], [id строки УПв], [id ФИК, если есть], [номер части, если есть], [номер строки диплома], [название строки диплома]
        final List<Object[]> items = tool.executeQuery(
                MigrationUtils.processor(Long.class, Short.class, Long.class, Long.class, Long.class, Integer.class, Integer.class, String.class),
                "select r.owner_id, r.discriminator, r.id, epv_r.id, part_fca.controlaction_id, part.number_p, r.number_p, r.title_p\n" +
                        "from dip_row_t r\n" +
                        "inner join epp_epvrow_regel_t epv_r on epv_r.id = r.epvregistryrow_id\n" +
                        "left join dip_row_rfca_t part_fca_rel on part_fca_rel.row_id = r.id\n" +
                        "left join epp_reg_element_part_fca_t part_fca on part_fca.id = part_fca_rel.rgstryelmntprtfcntrlactn_id\n" +
                        "left join epp_reg_element_part_t part on part.id = part_fca.part_id"
        );

        if (items.isEmpty()) {
            return;
        }

        final Map<String, Long> caCodeMap = MigrationUtils.getCatalogCode2IdMap(tool, "epp_c_cactiontype_t");
        final Set<Long> exams = ImmutableSet.of(caCodeMap.get("01"), caCodeMap.get("06")); // Экзамены
        final Set<Long> examsAndSets = ImmutableSet.of(caCodeMap.get("01"), caCodeMap.get("06"), caCodeMap.get("02"), caCodeMap.get("03")); // Экзамены и зачеты
        final Set<Long> courses = ImmutableSet.of(caCodeMap.get("04"), caCodeMap.get("05")); // Курсовые

        // Группируем по блоку
        final Map<PairKey<Long, Short>, Map<Long, RowWrapper>> wrappersMap = SafeMap.get(HashMap.class);
        for (Object[] item : items)
        {
            final Long ownerId = (Long) item[0];
            final Short discriminator = (Short) item[1];
            final Long id = (Long) item[2];
            final Long epvRowId = (Long) item[3];
            final Long fcaId = (Long) item[4];
            final Integer partNumber = (Integer) item[5];
            final Integer rowNumber = (Integer) item[6];
            final String title = (String) item[7];

            final PairKey<Long, Short> key = new PairKey<>(ownerId, discriminator);
            final Map<Long, RowWrapper> blockMap = wrappersMap.get(key);
            RowWrapper wrapper = blockMap.get(id);
            if (wrapper == null) {
                blockMap.put(id, wrapper = new RowWrapper(id, epvRowId, rowNumber, title));
            }
            if (fcaId != null) {
                wrapper.fcaSet.add(new PairKey<>(partNumber, fcaId));
            }
        }

        // Обрабатываем блоки
        final Map<RowWrapper, Integer> finalMap = new HashMap<>();
        final Map<RowWrapper, Integer> blockFinalMap = new HashMap<>();
        for (Map.Entry<PairKey<Long, Short>, Map<Long, RowWrapper>> blockEntry : wrappersMap.entrySet())
        {
            blockFinalMap.clear();

            // Отделаем курсовые от прочих
            final Short blockDiscriminator = blockEntry.getKey().getSecond();
            final boolean courseWork = EntityRuntime.getMeta(DiplomaCourseWorkRow.class).getEntityCode().equals(blockDiscriminator);
            final Set<Long> main_fca_set = courseWork ? courses : exams; // приоритетная форма контроля (экзамены или курсовые)
            final Set<Long> all_fca_set = courseWork ? courses : examsAndSets; // все формы контроля доступные для данной строки (для курсачей - только курсачи, для остальный - зачеты и экзамены)

            // Группируем по строке УПв
            final Multimap<Long, RowWrapper> epvMap = ArrayListMultimap.create();
            for (RowWrapper wrapper : blockEntry.getValue().values()) {
                epvMap.put(wrapper.epvRowId, wrapper);
            }

            // Разбаем полученные группы
            for (Collection<RowWrapper> wrapperGroup : epvMap.asMap().values())
            {
                if (wrapperGroup.size() == 1) {
                    // 1. Если в группе оказалась одна строка диплома.

                    final RowWrapper wrapper = wrapperGroup.iterator().next();
                    final Integer term = !wrapper.fcaSet.isEmpty() ?
                            getTermForPart(tool, wrapper, main_fca_set) : // 1.1. Если у строки диплома есть ФИК.
                            getTermForEpv(tool, wrapper, main_fca_set, all_fca_set); // 1.2. Если у строки диплома нет ФИК.
                    if (term != null) {
                        blockFinalMap.put(wrapper, term);
                    }
                } else {
                    // 2. Если в группе оказалась несколько строк диплома.

                    // Собираем строки с ФИК
                    final Set<RowWrapper> wrappersWithFCA = new HashSet<>();
                    for (RowWrapper wrapper : wrapperGroup) {
                        if (!wrapper.fcaSet.isEmpty()) {
                            wrappersWithFCA.add(wrapper);
                        }
                    }
                    if (!wrappersWithFCA.isEmpty()) {
                        // Если в группе есть строки с ФИК, заполняем только их
                        for (RowWrapper wrapper : wrappersWithFCA) {
                            final Integer term = getTermForPart(tool, wrapper, main_fca_set);
                            if (term != null) {
                                blockFinalMap.put(wrapper, term);
                            }
                        }
                    } else {
                        // Если у всех строк нет ФИК.
                        fillTermsForExamAlg(tool, wrapperGroup, main_fca_set, blockFinalMap);
                    }
                }
            }

            // Теперь надо устранить возможные коллизии: не может быть двух строк с одним названием и семестром в блоке
            // и не может быть двух строк с одной строкой УПв и семестром в блоке
            fixCollisionsInBlock(blockFinalMap, main_fca_set);
            finalMap.putAll(blockFinalMap);
        }

        if (finalMap.isEmpty()) {
            return;
        }

        // Записываем полученные семестры в базу
        final BatchUpdater updater = new BatchUpdater("update dip_row_t set term_p=? where id=?", DBType.INTEGER, DBType.LONG);
        for (Map.Entry<RowWrapper, Integer> entry : finalMap.entrySet()) {
            updater.addBatch(entry.getValue(), entry.getKey().id);
        }
        updater.executeUpdate(tool);

    }
}