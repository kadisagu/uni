package ru.tandemservice.unidip.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;
import org.tandemframework.shared.person.catalog.entity.codes.EduLevelCodes;

import java.util.ArrayList;
import java.util.List;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unidip_2x10x2_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.10.2")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность dipDiplomaSendFisFrdoReport

        if (!tool.columnExists("dip_diploma_fis_frdo_report_t", "edulevel_p")) {

            List<String> highEducationLevel = new ArrayList<>();
            highEducationLevel.add(EduLevelCodes.VYSSHEE_OBRAZOVANIE_BAKALAVRIAT);
            highEducationLevel.add(EduLevelCodes.VYSSHEE_OBRAZOVANIE_SPETSIALITET_MAGISTRATURA);
            highEducationLevel.add(EduLevelCodes.VYSSHEE_OBRAZOVANIE_PODGOTOVKA_KADROV_VYSSHEY_KVALIFIKATSII);

            List<Object[]> reports = tool.executeQuery(MigrationUtils.processor(Long.class, String.class, String.class), "select r.id, l.code_p, l.title_p from dip_diploma_fis_frdo_report_t r " +
                    "left join c_edu_level_t as l on l.id = r.edulevel_id");

            tool.renameColumn("dip_diploma_fis_frdo_report_t", "edulevel_id", "edulevel_p");
            tool.changeColumnType("dip_diploma_fis_frdo_report_t", "edulevel_p", DBType.createVarchar(255));

            final MigrationUtils.BatchUpdater updater = new MigrationUtils.BatchUpdater("update dip_diploma_fis_frdo_report_t set edulevel_p=? where id=?", DBType.createVarchar(255), DBType.LONG);
            reports.stream().forEach(objects -> {
                Long id = (Long) objects[0];
                String code = (String) objects[1];
                String title = (String) objects[2];
                if (highEducationLevel.contains(code)) {
                    updater.addBatch("Высшее образование", id);
                } else {
                    updater.addBatch(title, id);
                }
            });
            updater.executeUpdate(tool);
        }
    }
}