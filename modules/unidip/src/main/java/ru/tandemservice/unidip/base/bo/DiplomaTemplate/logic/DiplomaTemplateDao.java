/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unidip.base.bo.DiplomaTemplate.logic;

import com.google.common.base.Preconditions;
import com.google.common.collect.*;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Session;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.InfoCollector;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.commonbase.base.util.key.TripletKey;
import org.tandemframework.shared.commonbase.dao.SharedBaseDao;
import ru.tandemservice.unidip.base.bo.DipDocument.IAggMethodManager;
import ru.tandemservice.unidip.base.bo.DipDocument.logic.IRowTotalData;
import ru.tandemservice.unidip.base.entity.catalog.DipAggregationMethod;
import ru.tandemservice.unidip.base.entity.catalog.codes.DipAggregationMethodCodes;
import ru.tandemservice.unidip.base.entity.catalog.codes.DipDocumentTypeCodes;
import ru.tandemservice.unidip.base.entity.diploma.*;
import ru.tandemservice.unidip.base.entity.eduplan.DiplomaEpvTemplateDefaultRel;
import ru.tandemservice.unidip.base.entity.eduplan.DiplomaTemplate;
import ru.tandemservice.unidip.settings.bo.DipSettings.logic.DipSettingsDao;
import ru.tandemservice.unidip.settings.entity.DipFormingRowAlgorithm;
import ru.tandemservice.unidip.settings.entity.DipFormingRowAlgorithmOrgUnit;
import ru.tandemservice.unidip.settings.entity.codes.DipFormingRowAlgorithmCodes;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit;
import ru.tandemservice.uniepp.dao.eduplan.EppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.entity.catalog.*;
import ru.tandemservice.uniepp.entity.catalog.codes.EppPlanStructureCodes;
import ru.tandemservice.uniepp.entity.plan.*;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvStructureRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;
import ru.tandemservice.uniepp.util.EppFControlActionInfo;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author iolshvang
 * @since 13.07.11 18:35
 */
public class DiplomaTemplateDao extends SharedBaseDao implements IDiplomaTemplateDao
{
    public static final String LOCK_DIPLOMA_CONTENT_CHANGES = "LockDipContentChanges.";

    @Override
    public void deleteRow(Long rowId)
    {
        DiplomaContentRow row = get(rowId);
        DiplomaContent content = row.getOwner();
        NamedSyncInTransactionCheckLocker.register(getSession(), LOCK_DIPLOMA_CONTENT_CHANGES + content.getId());
        Class<? extends DiplomaContentRow> rowClass = row.getClass();
        delete(row);
        doNormalizeRowNumbers(rowClass, content);
    }

    @Override
    public void doNormalizeRowNumbers(Class<? extends DiplomaContentRow> rowClass, DiplomaContent owner)
    {
        NamedSyncInTransactionCheckLocker.register(getSession(), LOCK_DIPLOMA_CONTENT_CHANGES + owner.getId());
        CommonManager.instance().commonPriorityDao().doNormalizePriority(rowClass, DiplomaContentRow.number().s(), DiplomaContentRow.owner(), owner);
    }

    @Override
    public Map<Long, IRowTotalData> getTotalRowData(Collection<Long> rowIds)
    {
        Map<DipAggregationMethod, List<Long>> aggMethodMap = new HashMap<>();
        for (DiplomaContentRow row : getList(DiplomaContentRow.class, rowIds))
        {
            SafeMap.safeGet(aggMethodMap, row.getSourceAggregation(), ArrayList.class).add(row.getId());
        }
        Map<Long, IRowTotalData> rowDataMap = new HashMap<>();
        for (DipAggregationMethod method : aggMethodMap.keySet())
        {
            String boName = method.getBusinessObjectName();
            rowDataMap.putAll(((IAggMethodManager) BusinessObjectManager.instance(boName)).aggMethodDao().getTotalRowData(aggMethodMap.get(method)));
        }
        return rowDataMap;
    }

    @Override
    public void saveOrUpdateDiplomaTemplate(DiplomaTemplate diplomaTemplate, DiplomaContent diplomaContent)
    {
        NamedSyncInTransactionCheckLocker.register(getSession(), DIP_TEMPLATE_ADD_EDIT_SYNC);

        saveOrUpdate(diplomaContent);
        getSession().flush();
        saveOrUpdate(diplomaTemplate);
        getSession().flush();

        DiplomaEpvTemplateDefaultRel rel = new DiplomaEpvTemplateDefaultRel();
        rel.setEduPlanVersion(diplomaTemplate.getEduPlanVersion());
        rel.setDiplomaTemplate(diplomaTemplate);

        if (!existsEntity(DiplomaEpvTemplateDefaultRel.class, DiplomaEpvTemplateDefaultRel.L_DIPLOMA_TEMPLATE, rel.getDiplomaTemplate(), DiplomaEpvTemplateDefaultRel.L_EDU_PLAN_VERSION, rel.getEduPlanVersion()))
            save(rel);
    }

    @Override
    public void deleteDiplomaTemplate(Long diplomaTemplateId)
    {
        DiplomaTemplate diplomaTemplate = get(DiplomaTemplate.class, diplomaTemplateId);
        NamedSyncInTransactionCheckLocker.register(getSession(), LOCK_DIPLOMA_CONTENT_CHANGES + diplomaTemplate.getContent().getId());
        delete(diplomaTemplateId);
    }

    @Override
    public void doChangePriorityDown(Long entityId)
    {
        DiplomaContentRow item = getNotNull(DiplomaContentRow.class, entityId);
        NamedSyncInTransactionCheckLocker.register(getSession(), LOCK_DIPLOMA_CONTENT_CHANGES + item.getOwner().getId());
        CommonManager.instance().commonPriorityDao().doChangePriorityDown(item, item.getClass(), DiplomaContentRow.number().s(), DiplomaContentRow.owner());
    }

    @Override
    public void doChangePriorityUp(Long rowId)
    {
        DiplomaContentRow item = getNotNull(DiplomaContentRow.class, rowId);
        NamedSyncInTransactionCheckLocker.register(getSession(), LOCK_DIPLOMA_CONTENT_CHANGES + item.getOwner().getId());
        CommonManager.instance().commonPriorityDao().doChangePriorityUp(item, item.getClass(), DiplomaContentRow.number().s(), DiplomaContentRow.owner());
    }

    @Override
    public void saveOrUpdateDiplomaContentRow(List<DiplomaContentRegElPartFControlAction> elementPartFControlActionList, DiplomaContentRow row)
    {
        NamedSyncInTransactionCheckLocker.register(getSession(), LOCK_DIPLOMA_CONTENT_CHANGES + row.getOwner().getId());

        if (null == row.getRowContentUpdateDate())
            row.setRowContentUpdateDate(new Date());
        row.setRowValueUpdateDate(new Date());
        saveOrUpdate(row);


        for (DiplomaContentRegElPartFControlAction item : elementPartFControlActionList)
        {
            if (null == item.getOwner() || null == item.getRegistryElementPartFControlAction())
                throw new ApplicationException("Завершите редактирование строк.");
        }

        final List<DiplomaContentRegElPartFControlAction> databaseRecords = getList(DiplomaContentRegElPartFControlAction.class, DiplomaContentRegElPartFControlAction.L_ROW, row);
        new MergeAction.SessionMergeAction<DiplomaContentRegElPartFControlAction.NaturalId, DiplomaContentRegElPartFControlAction>() {
            @Override protected DiplomaContentRegElPartFControlAction.NaturalId key(DiplomaContentRegElPartFControlAction source) { return new DiplomaContentRegElPartFControlAction.NaturalId(source.getOwner(), source.getRegistryElementPartFControlAction()); }
            @Override protected DiplomaContentRegElPartFControlAction buildRow(DiplomaContentRegElPartFControlAction source) { return new DiplomaContentRegElPartFControlAction(source.getRow(), source.getRegistryElementPartFControlAction()); }
            @Override protected void fill(DiplomaContentRegElPartFControlAction target, DiplomaContentRegElPartFControlAction source) { target.update(source, false); }
        }.merge(databaseRecords, elementPartFControlActionList);

        doNormalizeRowNumbers(row.getClass(), row.getOwner());
    }

    @Override
    public DQLSelectBuilder getEppRegistryElementPartFControlActionBuilder(String alias, DiplomaTemplate template, EppRegistryElementPart registryElementPart, List<DataWrapper> regElemWrappedList, DiplomaContentRow row)
    {
        final DQLSelectBuilder partDQLBuilder = new DQLSelectBuilder()
                .fromEntity(EppRegistryElementPartFControlAction.class, alias)
                .joinPath(DQLJoinType.inner, EppRegistryElementPartFControlAction.part().fromAlias(alias), "part")
                .joinPath(DQLJoinType.inner, EppRegistryElementPart.registryElement().fromAlias("part"), "regEl");

        // фильтруем по наличию строки УПв

        final DQLSelectBuilder eppEpvRowListBuilder = new DQLSelectBuilder()
                .fromEntity(EppEpvRegistryRow.class, "r").column(property("r.id"))
                .joinPath(DQLJoinType.inner, EppEpvRegistryRow.owner().fromAlias("r"), "b")
                .where(eq(property("b", EppEduPlanVersionBlock.eduPlanVersion()), value(template.getEduPlanVersion())))
                .where(eq(property("r", EppEpvRegistryRow.registryElement()), property("regEl")));

        final EduProgramSpecialization specialization = template.getContent().getProgramSpecialization();
        if (specialization != null) {
            eppEpvRowListBuilder.joinEntity("b", DQLJoinType.left, EppEduPlanVersionSpecializationBlock.class, "spb", eq("b", "spb"));
            eppEpvRowListBuilder.where(or(
                    instanceOf("b", EppEduPlanVersionRootBlock.class),
                    eq(property("spb", EppEduPlanVersionSpecializationBlock.programSpecialization()), value(specialization))
            ));
        } else {
            eppEpvRowListBuilder.where(instanceOf("b", EppEduPlanVersionRootBlock.class));
        }

        partDQLBuilder.where(exists(eppEpvRowListBuilder.buildQuery()));


        // Убираем уже добавленные в шаблон формы контроля частей реестра
        final DQLSelectBuilder usedFcaDQL = new DQLSelectBuilder()
                .fromEntity(DiplomaContentRegElPartFControlAction.class, "r_fca")
                .column(value(1))
                .joinPath(DQLJoinType.inner, DiplomaContentRegElPartFControlAction.row().fromAlias("r_fca"), "r")
                .where(eq(property("r", DiplomaContentRow.owner()), value(template.getContent())))
                .where(eq(property("r_fca", DiplomaContentRegElPartFControlAction.registryElementPartFControlAction()), property(alias)));

        if (null != row) {
            usedFcaDQL.where(ne(property("r"), value(row)));
        }
        partDQLBuilder.where(notExists(usedFcaDQL.buildQuery()));


        // Убираем добавленные на форме формы контроля частей реестра
        if (CollectionUtils.isNotEmpty(regElemWrappedList))
        {
            final List<EppRegistryElementPartFControlAction> partFCAs = regElemWrappedList.stream().map(w -> w.<DiplomaContentRegElPartFControlAction>getWrapped().getRegistryElementPartFControlAction()).collect(Collectors.toList());
            partDQLBuilder.where(notIn(property(alias), partFCAs));

            // если на форме уже добавляли что-то в строку - берем только кусочки дисциплины, которую выбрали первой
            partFCAs.stream().filter(p_fca -> p_fca != null).findFirst().ifPresent(
                    item -> partDQLBuilder.where(eq(property("regEl"), value(item.getPart().getRegistryElement())))
            );
        }

        // и фильтруем по части дисциплины реестра, если пришла
        if (registryElementPart != null) {
            partDQLBuilder.where(eq(property("part"), value(registryElementPart)));
        }

        return partDQLBuilder;
    }

    @Override
    public Collection<IEppEpvRowWrapper> getEpvBlockRows(EppEduPlanVersion eduPlanVersion, EduProgramSpecialization specialization)
    {
        // Определяем блок УПв
        EppEduPlanVersionBlock block = null;
        if (specialization != null){
            // Если направленность указана, ищем блок с этой направленностью
            block = new DQLSelectBuilder()
                    .fromEntity(EppEduPlanVersionSpecializationBlock.class, "s")
                    .column(property("s"))
                    .where(eq(property("s", EppEduPlanVersionSpecializationBlock.eduPlanVersion()), value(eduPlanVersion)))
                    .where(eq(property("s", EppEduPlanVersionSpecializationBlock.programSpecialization()), value(specialization)))
                    .createStatement(this.getSession()).uniqueResult();
        }

        if (block == null) {
            // Если направленность не указана или нет блока направленности, берем общий блок
            block = this.get(EppEduPlanVersionRootBlock.class, EppEduPlanVersionRootBlock.eduPlanVersion(), eduPlanVersion);
            if (block == null) {
                return ImmutableList.of();
            }
        }

        return IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockRowMap(block.getId(), true).values();
    }

    @Override
    public Map<IEppEpvRowWrapper, Map<Integer, Collection<Long>>> getEpvRowsForCreation(DiplomaTemplate template, InfoCollector infoCollector)
    {
        // Строки шаблона формируются на основе строк УП(в)
        final Collection<IEppEpvRowWrapper> allRows = getEpvBlockRows(template.getEduPlanVersion(), template.getContent().getProgramSpecialization());
        final Multimap<EppRegistryElement, IEppEpvRowWrapper> regMap = HashMultimap.create();

        // Фильтруем полученные строки блока УПв по ряду параметров

        DiplomaQualifWorkRow tempDipQualifWorkRow = new DiplomaQualifWorkRow();

        final List<String> rowsWoReg = new ArrayList<>(); // названия строк без ссылки на элемент реестра
        final List<IEppEpvRowWrapper> delQualifWorkRows = Lists.newArrayList();
        final Map<IEppEpvRowWrapper, Map<Integer,Collection<Long>>> resultMap = Maps.newLinkedHashMap();

        final EppFControlActionInfo fcaInfo = EppFControlActionInfo.SUPPLIER.get();

        boolean includeSelectedOrOptionalDisciplines = DipSettingsDao.isDipTemplateCollectOptionalDiscipline();

        for (final IEppEpvRowWrapper rowWrapper : allRows)
        {
            // Эта строка не дисциплина - не нужна, пропускаем
            if (!(rowWrapper.getRow() instanceof EppEpvRegistryRow)) {
                continue;
            }

            // Если не выбран чекбокс добавления ДВ и ФКД, то пропускаем ДВ и ФКД
            if (!includeSelectedOrOptionalDisciplines && (rowWrapper.isSelectedItem() || rowWrapper.isOptionalItem())) {
                continue;
            }

            // Проверяем, что есть блок в дипломе, куда помещается строка УПв //((EppEpvRegistryRow)rowWrapper.getRow()).getParent() ?? faculty
            if (FormingRowUtils.getDiplomaRowClass(rowWrapper.getType()) == null) {
                continue; // Видимо, это просто ИГА или ещё что-то, для которых строка в дипломе (шаблоне) не создается
            }

            final EppRegistryElement registryElement = ((EppEpvRegistryRow) rowWrapper.getRow()).getRegistryElement();
            if (registryElement == null) {
                // Элемент реестра не указан, строку не создаем, выводим название в предупреждении
                rowsWoReg.add(rowWrapper.getTitleWithIndex());
            } else {
                // Собираем ФИКи строки
                final Map<Integer, Collection<Long>> fcaIds = Maps.newHashMap();
                for (Map.Entry<Long, String> fcaEntry : fcaInfo.getIdToFullCodeMap().entrySet())
                {
                    if (!rowWrapper.getActionTermSet(fcaEntry.getValue()).isEmpty()) {
                        // Добавляем ФИК соответсвующему реестру
                        for (Integer part : rowWrapper.getActionTermSet(fcaEntry.getValue())){
                            SafeMap.safeGet(fcaIds, part, ArrayList.class).add(fcaEntry.getKey());
                        }
                    }
                }
                if (fcaIds.isEmpty()) {
                    // Если в строке УПв нет ФИК, пропускаем её.
                    continue;
                }

                if (tempDipQualifWorkRow.getRelatedRegistryElementTypeCodes().contains(rowWrapper.getType().getCode()))
                {
                    delQualifWorkRows.add(rowWrapper);
                }

                // Элемент реестра указан, нагрузка и/или контроль есть, кладем в мультимапу - если элемент реестра вдруг повторится для каки-то строк УПв,
                // для этих строк создвать строку шаблона не будем, а выведем сообщение с предупреждением.
                regMap.put(registryElement, rowWrapper);
                resultMap.put(rowWrapper, fcaIds);
            }
        }

        if (null != infoCollector)
        {
            if (!rowsWoReg.isEmpty())
                infoCollector.add("Для строк учебного плана (" + CommonBaseStringUtil.joinUniqueSorted(rowsWoReg, ", ") + ") не созданы строки документа, т.к. они не ссылаются на элемент реестра.");
            if (delQualifWorkRows.size() > 1)
            {
                infoCollector.add("В документе об обучении может быть указана только одна ВКР.");
                delQualifWorkRows.forEach(resultMap::remove);
            }
        }

        // Для тех элементы реестра, для которых есть более одной строки УПв, подходящей для добавления в шаблон, исключаем строки УПв из списка на формирвоание строк шаблона
        // и выводим в пользовательское предупреждение, мол у вас ошибочка в плане, добавле строки руками
        for (Map.Entry<EppRegistryElement, Collection<IEppEpvRowWrapper>> regMapEntry : regMap.asMap().entrySet())
        {
            if (regMapEntry.getValue().size() <= 1) {
                continue; // Всё хорошо
            }

            // Не всё хорошо - на один элемент реестра оказалось несколько ссылок. Собираем ссылки и ругаемся в варнинге.
            final List<String> rowTitles = new ArrayList<>(regMapEntry.getValue().size());
            for (IEppEpvRowWrapper rowWrapper : regMapEntry.getValue())
            {
                rowTitles.add(rowWrapper.getTitleWithIndex());
                resultMap.remove(rowWrapper);
            }

            if (infoCollector != null) {
                infoCollector.add("Для строк учебного плана (" + CommonBaseStringUtil.joinUniqueSorted(rowTitles, ", ") + ") не созданы строки документа, т.к. они ссылаются на один и тот же элемент реестра.");
            }
        }

        return resultMap;
    }

    private class RowCreationWrapper
    {
        private final DiplomaContentRow row;
        private final IEppEpvRowWrapper epvRowWrapper;
        private final Set<Integer> termSet; // Семестры, которые охватывает строка. Если не указан, то все семестры.
        private Long fcaId; // Форма контроля, для которой создана строка. Исользуется для курсовых, у которых для каждой ФИК своя строка.

        public RowCreationWrapper(DiplomaContentRow row, IEppEpvRowWrapper epvRowWrapper, Set<Integer> termSet)
        {
            this.row = row;
            this.epvRowWrapper = epvRowWrapper;
            this.termSet = termSet;
        }
    }

    /**
     * Получение суммы нагрузок нужного типа по строке УПв для набора семестров.
     * Если набор семестров не задан, вернется общая по строке нагрузка.
     */
    private double calcLoad(IEppEpvRowWrapper rowWrapper, Set<Integer> terms, String loadFullCode)
    {
        if (terms == null || terms.isEmpty()) {
            if (!EppLoadType.FULL_CODE_WEEKS.equals(loadFullCode)) {
                return Math.max(rowWrapper.getTotalLoad(0, loadFullCode), .0d); // общая для строки нагрузка (не путать с суммой)
            } else {
                return Math.max(rowWrapper.getTotalLoad(null, EppLoadType.FULL_CODE_WEEKS), .0d); // Недели надо всегда суммировать, т.к. их сумма отдельно не хранится пока
            }
        }
        double loadSum = .0d;
        for (final Integer term : terms) {
            loadSum += Math.max(rowWrapper.getTotalInTermLoad(term, loadFullCode), .0d);
        }
        return loadSum;
    }

    private RowCreationWrapper createRow(IEppEpvRowWrapper epvRowWrapper, Set<Integer> termSet, Class<? extends DiplomaContentRow> rowClazz, Integer term, boolean fillLoad)
    {
        Preconditions.checkNotNull(rowClazz);
        final DiplomaContentRow newRow;
        try {
            newRow = rowClazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw CoreExceptionUtils.getRuntimeException(e);
        }
        newRow.setEpvRegistryRow((EppEpvRegistryRow) epvRowWrapper.getRow());
        newRow.setTitle(epvRowWrapper.getTitle());
        newRow.setTerm(term);
        newRow.setContentListNumber(1);
        newRow.setRowValueUpdateDate(new Date());
        newRow.setRowContentUpdateDate(new Date());

        if (fillLoad) {
            // Считаем нагрузку по строке. Когда termSet == null, берется общая нагрузка по строке УПв.
            newRow.setLoadAsDouble(calcLoad(epvRowWrapper, termSet, EppLoadType.FULL_CODE_TOTAL_HOURS));
            newRow.setLaborAsDouble(calcLoad(epvRowWrapper, termSet, EppLoadType.FULL_CODE_LABOR));
            newRow.setWeeksAsDouble(calcLoad(epvRowWrapper, termSet, EppLoadType.FULL_CODE_WEEKS));
            newRow.setAudLoadAsDouble(calcLoad(epvRowWrapper, termSet, EppELoadType.FULL_CODE_AUDIT));
        }
        return new RowCreationWrapper(newRow, epvRowWrapper, termSet);
    }

    /**
     * Скорее всего, в коде учтено больше развилок, чем в постановке.
     * Поэтому, внимательно читайте комментарии.
     */
    private List<PairKey<DiplomaContentRow, Collection<DiplomaContentRegElPartFControlAction>>> formingRows(Collection<IEppEpvRowWrapper> epvRowWrappers, DiplomaTemplate template)
    {
        DiplomaContent owner = template.getContent();
        final String algCode = getCurrentFormingRowAlgorithmOu(owner, template.getEduPlanVersion()).getCode();
        final EppFControlActionInfo fcaInfo = EppFControlActionInfo.SUPPLIER.get();
        // Новые строки шаблона
        final List<RowCreationWrapper> newRows = new ArrayList<>(epvRowWrappers.size());
        for (final IEppEpvRowWrapper epvRowWrapper : epvRowWrappers)
        {
            final boolean isDiscipline = epvRowWrapper.getType().isDisciplineElement();
            // Создаем мапу распределения форм контроля по семестрам
            // Даже если ФИК нет в семестре, нагрузку надо считать (и семестр вычислять), поэтому в мапе должен быть семестр (просто с пустым списком ФИК).
            final Map<Integer, Set<String>> termActionsMap = SafeMap.get(HashSet.class);
            final List<PairKey<Integer, String>> courseList = new ArrayList<>();
            for (Integer term : epvRowWrapper.getActiveTermSet())
            {
                final Set<String> fcaSet = termActionsMap.get(term);  // get => put !!!
                for (String fcaFullCode : fcaInfo.getIdToFullCodeMap().values()) {
                    if (epvRowWrapper.getActionSize(term, fcaFullCode) > 0) {
                        if (isDiscipline && fcaInfo.isCourseWork(fcaFullCode)) {
                            // Для дисциплин курсовые не считаем за форму контроля, т.к. они вместе со связями пойдут идут в одтельный блок.
                            // Если у дисциплине в семестре есть только курсовая, то приравниваем такой семестр к семестру без ФИК, чтобы правильно вычислять номер семестра.
                            courseList.add(new PairKey<>(term, fcaFullCode));
                            continue;
                        }
                        fcaSet.add(fcaFullCode);
                    }
                }
            }

            if (isDiscipline && DipFormingRowAlgorithmCodes.EXAMS.equals(algCode)) {
                // Это дисциплина и алгоритм - "Одна дисциплина - количество строк в дипломе равно количеству экзаменов"
                for (Map.Entry<Integer, Set<Integer>> termEntry : FormingRowUtils.splitTermPartWithExams(termActionsMap).entrySet()) {
                    final RowCreationWrapper wrapper = createRow(epvRowWrapper, termEntry.getValue(), epvRowWrapper.isOptionalItem()?DiplomaOptDisciplineRow.class:DiplomaDisciplineRow.class, termEntry.getKey(), true);
                    newRows.add(wrapper);
                }
            } else {
                // Это практика, госэкзамен или ВКР. Либо это дисциплина и алгоритм "Одна дисциплина - одна строка в дипломе". Просто создаем одну строку.
                final RowCreationWrapper wrapper = createRow(epvRowWrapper, null, epvRowWrapper.isOptionalItem()?DiplomaOptDisciplineRow.class:FormingRowUtils.getDiplomaRowClass(epvRowWrapper.getType()), FormingRowUtils.getRowTerm(termActionsMap), true);
                newRows.add(wrapper);
            }

            if (isDiscipline && !courseList.isEmpty()) {
                // Создание строк с курсовыми работами / проектами. Эти строки создаются только для дисциплин
                // Строка с курсовой создается на каждый семестр, где есть курсовая форма контроля. На каждую форму контроля/
                // Т.е. если в одном семестре есть КР и КП, то будет создано 2 строки на этот семестр.
                // Для курсовых нагрузка не заполняется!
                for (PairKey<Integer, String> entry : courseList)
                {
                    final RowCreationWrapper courseRow = createRow(epvRowWrapper, ImmutableSet.of(entry.getFirst()), DiplomaCourseWorkRow.class, entry.getFirst(), false);
                    courseRow.fcaId = fcaInfo.getId(entry.getSecond());
                    newRows.add(courseRow);
                }
            }
        }

        // Проставляем ссылку на шаблон и метод вычисления оценки
        final DipAggregationMethod aggregationMethod = getCatalogItem(DipAggregationMethod.class, DipAggregationMethodCodes.POSLEDNYAYA_OTSENKA);
        for (RowCreationWrapper wrapper : newRows) {
            wrapper.row.setOwner(owner);
            wrapper.row.setSourceAggregation(aggregationMethod);
        }

        return attachRowActions(newRows); // Создаем связи с ФИК частей элементов реестра
    }

    @Override
    public DipFormingRowAlgorithm getCurrentFormingRowAlgorithmOu(DiplomaContent owner, EppEduPlanVersion epv)
    {
        EduOwnerOrgUnit ownerOu = null;
        if (null != owner.getProgramSpecialization())
        {
            ownerOu = new DQLSelectBuilder().fromEntity(EppEduPlanVersionSpecializationBlock.class, "b")
                    .column(property("b", EppEduPlanVersionSpecializationBlock.ownerOrgUnit()))
                    .where(eq(property("b", EppEduPlanVersionSpecializationBlock.eduPlanVersion()), value(epv)))
                    .where(eq(property("b", EppEduPlanVersionSpecializationBlock.programSpecialization()), value(owner.getProgramSpecialization())))
                    .createStatement(getSession()).uniqueResult();
        }
        else
        {
            String typeCode = owner.getType().getCode();
            if (typeCode.equals(DipDocumentTypeCodes.COLLEGE_DIPLOMA) && epv.getEduPlan() instanceof EppEduPlanSecondaryProf)
            {
                EppEduPlanSecondaryProf eduProf = (EppEduPlanSecondaryProf) epv.getEduPlan();
                ownerOu = eduProf.getOwnerOrgUnit();
            }
        }
        if (null != ownerOu)
        {
            DipFormingRowAlgorithmOrgUnit algorithmOu = getByNaturalId(new DipFormingRowAlgorithmOrgUnit.NaturalId(ownerOu));
            if (null != algorithmOu && null != algorithmOu.getDipFormingRowAlgorithm())
                return algorithmOu.getDipFormingRowAlgorithm();
        }
        return getNotNull(DipFormingRowAlgorithm.class, DipFormingRowAlgorithm.currentAlg(), true);
    }

    private List<PairKey<DiplomaContentRow, Collection<DiplomaContentRegElPartFControlAction>>> attachRowActions(Collection<RowCreationWrapper> rowCreationWrappers)
    {
        // Создаем связи строк шаблона с формами итогового контроля частей реестра
        // Для строки УПв ищутся соответствующие части элемента реестра по нормализованным номерам семестров, участвующих в формировании строки шаблона.
        // Далее берутся формы итогового контроля этих частей, и на основе них создаются связи со строкой шаблона.

        final List<Long> regElementIds = new ArrayList<>(rowCreationWrappers.size());
        for (RowCreationWrapper wrapper  : rowCreationWrappers) {
            regElementIds.add(wrapper.row.getEpvRegistryRow().getRegistryElement().getId());
        }

        // Получаем все элементы реестра для созданных строк вместе с номерами всех их частей (для правильного сопоставления семестра - части) и формами итогового контроля
        final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppRegistryElementPart.class, "e")
                .column(property("e", EppRegistryElementPart.registryElement().id()))
                .column(property("e", EppRegistryElementPart.number()))
                .column(property("part_fca.id"))
                .column(property("part_fca", EppRegistryElementPartFControlAction.controlAction().id()))
                .joinEntity("e", DQLJoinType.left, EppRegistryElementPartFControlAction.class, "part_fca", eq(property("e"), property("part_fca", EppRegistryElementPartFControlAction.part())))
                .where(in(property("e", EppRegistryElementPart.registryElement()), regElementIds));

        // regEl.id -> [part.number - > [[fcaPart.id, fca.id]]]
        final Map<Long, Map<Integer, Set<PairKey<Long, Long>>>> regMap = SafeMap.get(TreeMap.class);
        for (Object[] item : this.<Object[]>getList(dql)) {
            final Long regElId = (Long) item[0];
            final Integer partNumber = (Integer) item[1];
            final Long partFcaId = (Long) item[2];
            final Long fcaId = (Long) item[3];

            final Map<Integer, Set<PairKey<Long, Long>>> partMap = regMap.get(regElId);
            final Set<PairKey<Long, Long>> part_fca_set = SafeMap.safeGet(partMap, partNumber, HashSet.class);
            if (partFcaId != null) {
                part_fca_set.add(new PairKey<>(partFcaId, fcaId));
            }
        }

        // Получаем ФИК для проверки их доступности для практик/ИГА/дисциплин
        final Map<String, EppControlActionType> actionTypeMap = EppEduPlanVersionDataDAO.getActionTypeMap();
        final EppFControlActionInfo fcaInfo = EppFControlActionInfo.SUPPLIER.get();
        final List<PairKey<DiplomaContentRow, Collection<DiplomaContentRegElPartFControlAction>>> resultList = new ArrayList<>();
        final Session session = getSession();

        for (RowCreationWrapper wrapper : rowCreationWrappers)
        {
            final Map<Integer, Set<PairKey<Long, Long>>> partMap = regMap.get(wrapper.row.getEpvRegistryRow().getRegistryElement().getId());

            // Первый по порядку семестр, имеющий нагрузку или контроль, строки УПв соответствует первой части в элементе реестра. И так далее.
            final Iterator<Map.Entry<Integer, Set<PairKey<Long, Long>>>> partEntryIterator = partMap.entrySet().iterator();
            final Iterator<Integer> epvRowTermIterator = wrapper.epvRowWrapper.getActiveTermSet().iterator();

            final Collection<DiplomaContentRegElPartFControlAction> row_fca_list = new ArrayList<>();
            while (partEntryIterator.hasNext() && epvRowTermIterator.hasNext())
            {
                final Integer epvRowTerm = epvRowTermIterator.next();
                final Map.Entry<Integer, Set<PairKey<Long, Long>>> partEntry = partEntryIterator.next();

                if (wrapper.termSet != null && !wrapper.termSet.contains(epvRowTerm)) {
                    continue; // Пропускаем часть, т.к. соответствующий семестр не из этой строки
                }

                for (PairKey<Long, Long> partFCA : partEntry.getValue()) {
                    final Long fcaId = partFCA.getSecond();
                    final EppFControlActionType fca = (EppFControlActionType) actionTypeMap.get(fcaInfo.getFullCode(fcaId));

                    if (wrapper.fcaId != null && !wrapper.fcaId.equals(fcaId)) {
                        continue; // Если для враппера указана ФИК, то видимо это курсовая, для которой на каждый ФИК отдельная строка - необходимо сверять ФИК с тем, который во враппере.
                    }
                    if (!wrapper.row.isAllowFCA(fca)) {
                        continue; // Для строки недоступна эта форма контроля
                    }
                    if (wrapper.epvRowWrapper.getActionSize(epvRowTerm, fcaInfo.getFullCode(fcaId)) <= 0) {
                        continue; // Такой ФИК нет в семестре строки УПв
                    }

                    final EppRegistryElementPartFControlAction part_fca = (EppRegistryElementPartFControlAction) session.load(EppRegistryElementPartFControlAction.class, partFCA.getFirst());
                    row_fca_list.add(new DiplomaContentRegElPartFControlAction(wrapper.row, part_fca));
                }
            }
            resultList.add(new PairKey<>(wrapper.row, row_fca_list));
        }
        return resultList;
    }

    @Override
    public void createDiplomaContentRows(DiplomaTemplate template, InfoCollector infoCollector)
    {
        NamedSyncInTransactionCheckLocker.register(getSession(), LOCK_DIPLOMA_CONTENT_CHANGES + template.getContent().getId());

        final Session session = getSession();
        final Map<IEppEpvRowWrapper, Map<Integer, Collection<Long>>> epvRows = getEpvRowsForCreation(template, infoCollector);

        if (!epvRows.isEmpty()) {
            // Формируем строки
            final List<PairKey<DiplomaContentRow, Collection<DiplomaContentRegElPartFControlAction>>> newRows = formingRows(epvRows.keySet(), template);
            if (!newRows.isEmpty()) {
                // Получаем строки, уже добавленные в шаблон, чтобы заменить совпадающие по названию и семестру вновь сформированными
                final Multimap<TripletKey<Class, String, Integer>, DiplomaContentRow> currentMapByTitle = ArrayListMultimap.create();
                final Multimap<TripletKey<Class, EppEpvRegistryRow, Integer>, DiplomaContentRow> currentMapByRegEl = ArrayListMultimap.create();
                final Multimap<TripletKey<Class, EppEpvRegistryRow, String>, DiplomaContentRow> currentMapByTitleAndRegEl = ArrayListMultimap.create();
                for (DiplomaContentRow row : this.getList(DiplomaContentRow.class, DiplomaContentRow.owner(), template.getContent())) {
                    currentMapByTitle.put(new TripletKey<>(row.getClass(), row.getTitle(), row.getTerm()), row);
                    currentMapByRegEl.put(new TripletKey<>(row.getClass(), row.getEpvRegistryRow(), row.getTerm()), row);
                    currentMapByTitleAndRegEl.put(new TripletKey<>(row.getClass(), row.getEpvRegistryRow(), row.getTitle()), row);
                }
                // Получаем существующие ФИК. Если будут совпадать, придется удалить.
                // MergeAction тут не подходит..
                final Map<EppRegistryElementPartFControlAction, DiplomaContentRegElPartFControlAction> current_fca_map = new HashMap<>();
                for (DiplomaContentRegElPartFControlAction fca : this.getList(DiplomaContentRegElPartFControlAction.class, DiplomaContentRegElPartFControlAction.owner(), template.getContent())) {
                    current_fca_map.put(fca.getRegistryElementPartFControlAction(), fca);
                }

                final List<DiplomaContentRow> rows2save = new ArrayList<>();
                final Set<DiplomaContentRow> rows2del = new HashSet<>();
                final Set<Long> rowsFCA2del = new HashSet<>();
                final List<DiplomaContentRegElPartFControlAction> rowsFCA2save = new ArrayList<>();
                final Multimap<TripletKey<Class, String, Integer>, DiplomaContentRow> mapByTitle = ArrayListMultimap.create();
                final Multimap<TripletKey<Class, EppEpvRegistryRow, Integer>, DiplomaContentRow> mapByRegElKey = ArrayListMultimap.create();
                // Проставляем ссылку на шаблон, метод вычисления оценки и сохраняем строки
                for (PairKey<DiplomaContentRow, Collection<DiplomaContentRegElPartFControlAction>> newRowEntry : newRows) {
                    final DiplomaContentRow newRow = newRowEntry.getFirst();

                    final TripletKey<Class, String, Integer> keyByTitle = new TripletKey<>(newRow.getClass(), newRow.getTitle(), newRow.getTerm());
                    final TripletKey<Class, EppEpvRegistryRow, Integer> keyByRegEl = new TripletKey<>(newRow.getClass(), newRow.getEpvRegistryRow(), newRow.getTerm());

                    rows2del.addAll(currentMapByTitle.get(keyByTitle));
                    rows2del.addAll(currentMapByRegEl.get(keyByRegEl));

                    if (newRow.getTerm() > 0)
                    {
                        // Если семестр указан, надо разобраться с дублями:
                        // - не может быть двух строк в одном блоке, имеющих одно название и семестр
                        // - не может быть двух строк в одном блоке, имеющих один элемент реестра и семестр
                        mapByTitle.put(keyByTitle, newRow);
                        mapByRegElKey.put(keyByRegEl, newRow);

                        // Строки без семестра удаляем
                        rows2del.addAll(currentMapByTitle.get(new TripletKey<>(newRow.getClass(), newRow.getTitle(), 0)));
                        rows2del.addAll(currentMapByRegEl.get(new TripletKey<>(newRow.getClass(), newRow.getEpvRegistryRow(), 0)));
                    }
                    // Удаляем также совпадения по названию и элементу реестра
                    rows2del.addAll(currentMapByTitleAndRegEl.get(new TripletKey<>(newRow.getClass(), newRow.getEpvRegistryRow(), newRow.getTitle())));

                    rows2save.add(newRow);

                    // Формы контроля
                    for (DiplomaContentRegElPartFControlAction fca : newRowEntry.getSecond()) {
                        final DiplomaContentRegElPartFControlAction currentFCA = current_fca_map.get(fca.getRegistryElementPartFControlAction());
                        if (currentFCA != null) {
                            rowsFCA2del.add(currentFCA.getId()); // Удаляем совпадающие ФИК из шаблона
                        }
                        rowsFCA2save.add(fca);
                    }
                }

                if (!rowsFCA2del.isEmpty()) {
                    new DQLDeleteBuilder(DiplomaContentRegElPartFControlAction.class).where(in(property("id"), rowsFCA2del)).createStatement(session).execute();
                }
                if (!rows2del.isEmpty()) {
                    new DQLDeleteBuilder(DiplomaContentRow.class).where(in(property("id"), rows2del)).createStatement(session).execute();
                }
                session.flush();

                // Убираем коллизии в новых строках - просто сбрасываем номер семестра, пусть вручную разруливают и расставляют если им нужно.
                // Блок с курсовыми пропускаем, т.к. там ограничение не действует.
                for (Map.Entry<TripletKey<Class, EppEpvRegistryRow, Integer>, Collection<DiplomaContentRow>> oneKeyRows : mapByRegElKey.asMap().entrySet())
                {
                    if (oneKeyRows.getValue().size() > 1 && oneKeyRows.getKey().getFirst() != DiplomaCourseWorkRow.class) {
                        for (DiplomaContentRow row : oneKeyRows.getValue()) {
                            row.setTerm(0);
                        }
                    }
                }
                for (Map.Entry<TripletKey<Class, String, Integer>, Collection<DiplomaContentRow>> oneKeyRows : mapByTitle.asMap().entrySet())
                {
                    if (oneKeyRows.getValue().size() > 1 && oneKeyRows.getKey().getFirst() != DiplomaCourseWorkRow.class) {
                        for (DiplomaContentRow row : oneKeyRows.getValue()) {
                            row.setTerm(0);
                        }
                    }
                }

                for (DiplomaContentRow row : rows2save) {
                    session.save(row);
                }
                for (DiplomaContentRegElPartFControlAction fca : rowsFCA2save) {
                    session.save(fca);
                }
            }
        }

        // Пересортировать существующие строки
        session.flush();
        resortAllRowsAsEpv(template.getContent(), template.getEduPlanVersion().getEduPlan());
    }

    @Override
    public void resortAllRowsAsEpv(DiplomaContent content, EppEduPlan eduPlan)
    {
        // Проходим по всем блокам содержимого диплома (шаблона) и пересортируем
        // Номера строк сразу нормализуются (с 1)

        final Map<Class, List<DiplomaContentRow>> map = SafeMap.get(ArrayList.class);
        for (DiplomaContentRow row : getList(DiplomaContentRow.class, DiplomaContentRow.owner(), content)) {
            map.get(row.getClass()).add(row);
        }

        for (List<DiplomaContentRow> blockRows : map.values())
        {
            if (blockRows.isEmpty()) {
                continue;
            }

            final List<DiplomaContentRow> resultRowList;
            if (eduPlan instanceof EppEduPlanProf)
            {
                resultRowList = new ArrayList<>(blockRows.size());
                // для УП проф. образования строки сортируются так

                // Строки диплома, ссылающиеся на строку УПв, сортируются в соответствии с поколением ГОС.
                // Для 2009 и 2013 - как в УПв, для 2005 - как в УПв, только сначала по компонентам, потом по циклам.
                // В рамках одной строки УПв сортируем, как обычно (по названию и номеру части)
                // Строки без ссылок на строку УПв соритруются, как обычно (по названию и номеру части)
                final Comparator<EppEpvRow> epvRowComparator = ((EppEduPlanProf) eduPlan).getProgramSubject().isOKSO() ? OLD_GOS_ROW_COMPARATOR : EppEpvRow.GLOBAL_ROW_COMPARATOR;
                final Multimap<EppEpvRow, DiplomaContentRow> registryRowMap = TreeMultimap.create(epvRowComparator, DiplomaContentRow.TITLE_AND_TERM_COMPARATOR);
                for (DiplomaContentRow row : blockRows)
                {
                    if (row.getEpvRegistryRow() != null) {
                        registryRowMap.put(row.getEpvRegistryRow(), row);
                    } else {
                        resultRowList.add(row);
                    }
                }

                // Сортируем те, у которых нет ссылок
                Collections.sort(resultRowList, DiplomaContentRow.TITLE_AND_TERM_COMPARATOR);

                // Добавляем в начало все, у которых есть ссылки, уже отсортированные
                resultRowList.addAll(0, registryRowMap.values());
            }
            else
            {
                // СПО и ДПО и что-то еще доселе неизвестное сортируем по названию и номеру части
                resultRowList = blockRows;
                Collections.sort(resultRowList, DiplomaContentRow.TITLE_AND_TERM_COMPARATOR);
            }

            // Перенумеровываем отсортированный список
            for (int i = 0; i < resultRowList.size(); i++)
            {
                DiplomaContentRow row = resultRowList.get(i);
                row.setNumber(i+1);
                update(row);
            }
        }
    }

    // ГОС (перечень 2005) - сначала дисциплины по компонентам, внутри по циклам, сортируем строки УП
    private static final Comparator<EppEpvRow> OLD_GOS_ROW_COMPARATOR = (o1, o2) -> {
        EppPlanStructure o1Component = null;
        EppPlanStructure o1Cycle = null;
        EppEpvRow o1Parent = o1.getHierarhyParent();
        while (o1Parent != null)
        {
            if (o1Parent instanceof EppEpvStructureRow)
            {
                EppPlanStructure struct = ((EppEpvStructureRow) o1Parent).getValue().getParent();
                if (struct != null)
                {
                    if (EppPlanStructureCodes.GOS_CYCLES.equals(struct.getCode()))
                        o1Cycle = ((EppEpvStructureRow) o1Parent).getValue();
                    else if (EppPlanStructureCodes.GOS_COMPONENTS.equals(struct.getCode()))
                        o1Component = ((EppEpvStructureRow) o1Parent).getValue();
                }
            }
            o1Parent = o1Parent.getHierarhyParent();
        }

        EppPlanStructure o2Component = null;
        EppPlanStructure o2Cycle = null;
        EppEpvRow o2Parent = o2.getHierarhyParent();
        while (o2Parent != null)
        {
            if (o2Parent instanceof EppEpvStructureRow)
            {
                EppPlanStructure struct = ((EppEpvStructureRow) o2Parent).getValue().getParent();
                if (struct != null)
                {
                    if (EppPlanStructureCodes.GOS_CYCLES.equals(struct.getCode()))
                        o2Cycle = ((EppEpvStructureRow) o2Parent).getValue();
                    else if (EppPlanStructureCodes.GOS_COMPONENTS.equals(struct.getCode()))
                        o2Component = ((EppEpvStructureRow) o2Parent).getValue();
                }
            }
            o2Parent = o2Parent.getHierarhyParent();
        }

        return ComparisonChain.start()
                .compare(o1Component, o2Component, CommonBaseUtil.CODE_SIMPLE_SAFE_COMPARATOR) // по возрастанию кода компоненты структурной строки УП
                .compare(o1Cycle, o2Cycle, CommonBaseUtil.PRIORITY_SIMPLE_SAFE_COMPARATOR) // по приоритету циклов (2g.index.) структуры ГОС/УП структурных строк УП
                .compare(o1, o2, EppEpvRow.GLOBAL_ROW_COMPARATOR) // В рамках компонента-цикла сортируем как в УП
                .result();
    };

    @Override
    public int getDiplomaContentRowMaxNumber(DiplomaContent content)
    {
        final Number maxRowNumber = new DQLSelectBuilder().fromEntity(DiplomaContentRow.class, "d")
                .column(DQLFunctions.max(property("d", DiplomaContentRow.P_NUMBER)))
                .where(eq(property("d", DiplomaContentRow.owner()), value(content)))
                .createStatement(getSession()).uniqueResult();

        return maxRowNumber == null ? 0 : maxRowNumber.intValue();
    }


}
