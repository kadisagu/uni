/* $Id$ */
package ru.tandemservice.unidip.bso.bo.DiplomaSendFisFrdoReport.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.SimplePublisherLinkResolver;
import ru.tandemservice.unidip.bso.bo.DiplomaSendFisFrdoReport.ui.Pub.DiplomaSendFisFrdoReportPub;
import ru.tandemservice.unidip.bso.entity.blank.DipDiplomaSendFisFrdoReport;

import java.util.HashMap;

import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Alexey Lopatin
 * @since 01.04.2015
 */
@Configuration
public class DiplomaSendFisFrdoReportList extends BusinessComponentManager
{
    public static final String DIPLOMA_SEND_FIS_FRDO_REPORT_DS = "diplomaSendFisFrdoReportDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(DIPLOMA_SEND_FIS_FRDO_REPORT_DS, diplomaSendFisFrdoReportDS(), diplomaSendFisFrdoReportDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint diplomaSendFisFrdoReportDS()
    {
        return columnListExtPointBuilder(DIPLOMA_SEND_FIS_FRDO_REPORT_DS)
                .addColumn(indicatorColumn("report").defaultIndicatorItem(new IndicatorColumn.Item("report")))
                .addColumn(publisherColumn(DipDiplomaSendFisFrdoReport.P_FORMING_DATE, DipDiplomaSendFisFrdoReport.formingDate())
                                   .publisherLinkResolver(new SimplePublisherLinkResolver(DipDiplomaSendFisFrdoReport.id())
                                                                  .setComponentName(DiplomaSendFisFrdoReportPub.class.getSimpleName())
                                                                  .params(new HashMap<String, Object>()
                                                                  {{
                                                                          put(IUIPresenter.PUBLISHER_ID, DipDiplomaSendFisFrdoReport.id());
                                                                      }})
                                   )
                                   .formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).width("200px").order())
                .addColumn(textColumn(DipDiplomaSendFisFrdoReport.P_EDU_LEVEL, DipDiplomaSendFisFrdoReport.eduLevel()))
                .addColumn(textColumn(DipDiplomaSendFisFrdoReport.L_DOCUMENT_TYPE, DipDiplomaSendFisFrdoReport.documentType().title()))
                .addColumn(dateColumn(DipDiplomaSendFisFrdoReport.P_ISSUANCE_DATE_FROM, DipDiplomaSendFisFrdoReport.issuanceDateFrom()).width("150px").formatter(DateFormatter.DEFAULT_DATE_FORMATTER))
                .addColumn(dateColumn(DipDiplomaSendFisFrdoReport.P_ISSUANCE_DATE_TO, DipDiplomaSendFisFrdoReport.issuanceDateTo()).width("150px").formatter(DateFormatter.DEFAULT_DATE_FORMATTER))
                .addColumn(booleanColumn(DipDiplomaSendFisFrdoReport.P_DUPLICATE_DIPLOMA_OBJECT, DipDiplomaSendFisFrdoReport.duplicateDiplomaObject()))
                .addColumn(actionColumn("print", CommonDefines.ICON_PRINT, "onClickPrint").permissionKey("diplomaSendFisFrdoReport_print"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER)
                                   .alert(FormattedMessage.with().template("diplomaSendFisFrdoReportDS.delete.alert").parameter(DipDiplomaSendFisFrdoReport.P_FORMING_DATE, DateFormatter.DATE_FORMATTER_WITH_TIME).create())
                                   .permissionKey("diplomaSendFisFrdoReport_delete"))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> diplomaSendFisFrdoReportDSHandler()
    {
        return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DipDiplomaSendFisFrdoReport.class, "r");
                builder.order(property("r", DipDiplomaSendFisFrdoReport.P_FORMING_DATE), input.getEntityOrder().getDirection());
                return ListOutputBuilder.get(input, builder.createStatement(context.getSession()).list()).pageable(true).build();
            }
        };
    }
}
