/* $Id$ */
package ru.tandemservice.unidip.bso.bo.DiplomaBlankReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import ru.tandemservice.unidip.bso.bo.DiplomaBlankReport.logic.DiplomaBlankReportDao;
import ru.tandemservice.unidip.bso.bo.DiplomaBlankReport.logic.IDiplomaBlankReportDao;

/**
 * @author azhebko
 * @since 22.01.2015
 */
@Configuration
public class DiplomaBlankReportManager extends BusinessObjectManager
{
    public static final long FORM_BY_TYPE = 0L;
    public static final long FORM_BY_STORAGE_LOCATION = 1L;

    public static DiplomaBlankReportManager instance()
    {
        return BusinessObjectManager.instance(DiplomaBlankReportManager.class);
    }

    @Bean
    public IDiplomaBlankReportDao dao()
    {
        return new DiplomaBlankReportDao();
    }

    @Bean
    public ItemListExtPoint<ReportFormByItem> reportFormByItems()
    {
        return this.itemList(ReportFormByItem.class)
            .add(String.valueOf(FORM_BY_TYPE), new ReportFormByItem(FORM_BY_TYPE, "reportFormBy.blankType.title"))
            .add(String.valueOf(FORM_BY_STORAGE_LOCATION), new ReportFormByItem(FORM_BY_STORAGE_LOCATION, "reportFormBy.storageLocation.title"))
            .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler reportFormByDSHandler()
    {
        SimpleTitledComboDataSourceHandler dsHandler = new SimpleTitledComboDataSourceHandler(this.getName());
        for (ReportFormByItem item: reportFormByItems().getItems().values())
            dsHandler.addRecord(item.getId(), item.getTitle());

        return dsHandler;
    }

    public static class ReportFormByItem
    {
        private final Long _id;
        private final String _title;

        public ReportFormByItem(Long id, String title)
        {
            _id = id;
            _title = title;
        }

        public Long getId() { return _id; }
        public String getTitle() { return _title; }
    }

    public static String getReportFormByTitle(Long id)
    {
        return instance().getProperty(instance().reportFormByItems().getItem(String.valueOf(id)).getTitle());
    }
}