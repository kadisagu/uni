/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unidip.aggregationMethod.bo.AggMethodManual.logic;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.tandemframework.hibsupport.dao.CommonDAO;

import ru.tandemservice.unidip.base.bo.DipDocument.logic.IAggMethodDao;
import ru.tandemservice.unidip.base.bo.DipDocument.logic.IRowTotalData;

/**
 * @author iolshvang
 * @since 07.09.11 16:22
 */
public class AggMethodManualDao extends CommonDAO implements IAggMethodDao
{
    @Override
    public Map<Long, IRowTotalData> getTotalRowData(Collection<Long> rowIds)
    {
        Map<Long, IRowTotalData> totalDataMap = new HashMap<Long, IRowTotalData>();
        for (Long rowId : rowIds)
            totalDataMap.put(rowId, new IRowTotalData()
            {
                @Override
                public Long getAudLoadAsLong()
                {
                    return null;
                }

                @Override
                public Long getLoadAsLong()
                {
                    return null;
                }

                @Override
                public String getMark()
                {
                    return null;
                }
            });
        return totalDataMap;
    }
}
