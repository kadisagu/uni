/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unidip.base.bo.DipDocument.ui.AddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unidip.base.bo.DipDocument.DipDocumentManager;
import ru.tandemservice.unidip.base.bo.DipDocument.logic.DipDocumentTypeSelectModel;
import ru.tandemservice.unidip.base.bo.DipDocument.ui.AddEdit.logic.EduProgramSpecializationDSHandler;
import ru.tandemservice.unidip.base.entity.catalog.DipDocumentType;
import ru.tandemservice.unidip.base.entity.catalog.codes.DipDocumentTypeCodes;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContent;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentIssuance;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaObject;
import ru.tandemservice.unidip.base.entity.eduplan.DiplomaTemplate;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;

import java.util.Collection;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author iolshvang
 * @since 14.07.11 11:42
 */
@Input({
               @Bind(key = DipDocumentAddEditUI.DIP_DOCUMENT_ID, binding = "diplomaObject.id"),
               @Bind(key = DipDocumentAddEditUI.STUDENT_ID, binding = "student.id")
       })
public class DipDocumentAddEditUI extends UIPresenter
{
    public static final String DIP_DOCUMENT_ID = "dipDocumentId";
    public static final String STUDENT_ID = "studentId";
    public static final String DIPLOMA_OBJECT_NEW = "diplomaObject.newObject";
    public static final String DIPLOMA_OBJECT_ID = "diplomaObject.id";

	private boolean editForm;

	private DiplomaObject diplomaObject = new DiplomaObject();
	private Student student = new Student();
	private EppStudent2EduPlanVersion eppStudent2EduPlanVersion;
    private EduProgramSpecialization specialization;
    private String educationElementTitle;
    private DipDocumentType documentType;
    private DiplomaTemplate diplomaTemplate;
	private boolean withSuccessDisabled = true;
    private boolean withSuccess;
	private boolean issued;
    private Double loadAsDouble;
    private EppEduPlanProf eduPlanProf;

	private ISingleSelectModel docTypeModel;

    @Override
    public void onComponentRefresh()
    {
        editForm = (diplomaObject.getId() != null);

        if (editForm)
			initEditForm();
        else
			initAddForm();

        docTypeModel = new DipDocumentTypeSelectModel(getAppropriateDocumentTypes(), true);
    }

	@Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        switch (dataSource.getName())
        {
			case DipDocumentAddEdit.EDU_PLAN_VERSION_DS:
				dataSource.put(DipDocumentAddEdit.FILTER_STUDENT, student);
				break;
            case DipDocumentAddEdit.SPECIALIZATION_DS:
				if (eppStudent2EduPlanVersion == null)
					dataSource.put(EduProgramSpecializationDSHandler.FILTER_PROGRAM_SUBJECT, getSubject());
				else
					dataSource.put(EduProgramSpecializationDSHandler.FILTER_EDUPLAN_VERSION, eppStudent2EduPlanVersion.getEduPlanVersion());
				break;
            case DipDocumentAddEdit.TEMPLATE_DS:
                dataSource.put(DipDocumentAddEdit.FILTER_EDUPLAN_VERSION, eppStudent2EduPlanVersion.getEduPlanVersion());
                dataSource.put(DipDocumentAddEdit.FILTER_DOCUMENT_TYPE, documentType);
                dataSource.put(DipDocumentAddEdit.FILTER_PROGRAM_SPECIALIZATION, specialization);
				break;
        }
    }

	private void initEditForm()
	{
		diplomaObject = DataAccessServices.dao().getNotNull(DiplomaObject.class, diplomaObject.getId());
		student = diplomaObject.getStudent();
		eppStudent2EduPlanVersion = diplomaObject.getStudentEpv();

		issued = isDiplomaOrContentIssued(diplomaObject);

		DiplomaContent content = diplomaObject.getContent();
		documentType = content.getType();
		onChangeType();	// Зависит от issued и documentType
		withSuccess = content.isWithSuccess();
		loadAsDouble = content.getLoadAsDouble();
		specialization = content.getProgramSpecialization();
		educationElementTitle = content.getEducationElementTitle();
	}

	/** Есть ли факт выдачи для диплома или содержимого диплома, содержащий непустой бланк.*/
	private static boolean isDiplomaOrContentIssued(DiplomaObject diplomaObject)
	{
		DQLSelectBuilder diplomaIssued = new DQLSelectBuilder()
				.fromEntity(DiplomaIssuance.class, "i")
				.where(eq(property("i", DiplomaIssuance.diplomaObject()), value(diplomaObject)))
				.where(isNotNull(property("i", DiplomaIssuance.blank())));

		DQLSelectBuilder diplomaContentIssued = new DQLSelectBuilder()
				.fromEntity(DiplomaContentIssuance.class, "i")
				.where(eq(property("i", DiplomaContentIssuance.diplomaIssuance().diplomaObject()), value(diplomaObject)))
				.where(isNotNull(property("i", DiplomaContentIssuance.blank())));

		final IUniBaseDao dao = IUniBaseDao.instance.get();
		return dao.existsEntity(diplomaIssued.buildQuery()) || dao.existsEntity(diplomaContentIssued.buildQuery());
	}

	private void initAddForm()
	{
		issued = false;

		if (student.getId() == null)
			throw new IllegalArgumentException();

		diplomaObject.setContent(new DiplomaContent());
		student = DataAccessServices.dao().getNotNull(Student.class, student.getId());
		diplomaObject.setStudent(student);
		eppStudent2EduPlanVersion = IEppEduPlanDAO.instance.get().getActiveStudentEduPlanVersionRelation(student.getId());
		onChangeEpv();

		if (isEduPlanProf())
		{
			eduPlanProf = (EppEduPlanProf) eppStudent2EduPlanVersion.getEduPlanVersion().getEduPlan();
			if (eduPlanProf.getProgramSubject().isLabor())
				loadAsDouble = eduPlanProf.getLaborAsDouble();
			else
				loadAsDouble = eduPlanProf.getWeekAsDouble();
		}
	}

	/** Получить типы документов, разрешенные для вида ОП из активного УПв студента. Если активного УПв у него нет, то вид ОП берется из НПП. */
	private Collection<DipDocumentType> getAppropriateDocumentTypes()
	{
		String programKindCode, subjectIndexCode;
		final EppStudent2EduPlanVersion actualStudentEpv = IEppEduPlanDAO.instance.get().getActiveStudentEduPlanVersionRelation(student.getId());
		if (actualStudentEpv != null)
		{
			programKindCode = actualStudentEpv.getEduPlanVersion().getEduPlan().getProgramKind().getCode();

			EppEduPlan eduPlan = actualStudentEpv.getEduPlanVersion().getEduPlan();
			subjectIndexCode = (eduPlan instanceof EppEduPlanProf) ? ((EppEduPlanProf)eduPlan).getProgramSubject().getCode() : null;
		}
		else
		{
			EduProgramSubjectIndex subjectIndex = student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject().getSubjectIndex();
			programKindCode = subjectIndex.getProgramKind().getCode();
			subjectIndexCode = subjectIndex.getCode();
		}
		return DipDocumentManager.getDipDocumentTypes(programKindCode, subjectIndexCode);
	}

	private EduProgramSubject getSubject()
	{
		if (isEduPlanProf())
		{
			EppEduPlan eduPlan = eppStudent2EduPlanVersion.getEduPlanVersion().getEduPlan();
			return ((EppEduPlanProf)eduPlan).getProgramSubject();
		}
		return student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject();
	}


    // Listeners

	/**
	 * При смене типа документа нужно менять чекбокс "С отличием" (дизаблить, если тип не выбран или не относится к дипломам, либо если есть факт выдачи с бланком;
	 * если тип не выбран, то чекбокс сбрасывается).
	 */
	public void onChangeType()
	{
		if (documentType == null)
		{
			withSuccessDisabled = true;
			withSuccess = false;
		}
		else
		{
			boolean wrongType = ! isDiplomaType(documentType);
			withSuccessDisabled = wrongType || issued;
		}
	}

	public void onChangeEpv()
	{
		EduProgramSubject subject = getSubject();
		if (subject != null)
		{
			diplomaObject.getContent().setProgramSubject(subject);
			educationElementTitle = subject.getTitleWithCode();
		}
	}

    public void onChangeTemplate()
    {
        if (diplomaTemplate != null)
            loadAsDouble = diplomaTemplate.getContent().getLoadAsDouble();
    }

    public void onClickApply()
    {
        ParametersMap result = new ParametersMap().add(DIPLOMA_OBJECT_NEW, (!editForm));

        if (!editForm)
        {
			diplomaObject.setStudentEpv(eppStudent2EduPlanVersion);
            DiplomaObject savedDiplomaObject = DipDocumentManager.instance().dao().saveDiplomaObject(
					diplomaObject, documentType, diplomaObject.getContent().getProgramSubject(), specialization, diplomaTemplate,
					educationElementTitle, withSuccess, loadAsDouble);

            diplomaObject.setId(savedDiplomaObject.getId());
        }
        else
        {
            DipDocumentManager.instance().dao().updateDiplomaObject(diplomaObject, educationElementTitle, documentType, withSuccess, loadAsDouble);
        }
        deactivate(result.add(DIPLOMA_OBJECT_ID, diplomaObject.getId()));
    }

	/** Является ли тип одним из следующих: диплом бакалавра, дисплом специалиста, диплом магистра, диплом СПО. */
	private boolean isDiplomaType(DipDocumentType type)
	{
		switch (type.getCode())
		{
			case DipDocumentTypeCodes.BACHELOR_DIPLOMA: case DipDocumentTypeCodes.SPECIALIST_DIPLOMA:
			case DipDocumentTypeCodes.MAGISTR_DIPLOMA: case DipDocumentTypeCodes.COLLEGE_DIPLOMA:
				return true;
		}
		return false;
	}

	public boolean isProgramKindProf()
	{
		final EduProgramSubject programSubject = diplomaObject.getContent().getProgramSubject();
		return (programSubject != null) && programSubject.getSubjectIndex().getProgramKind().isProgramProf();
	}

	public boolean isEduPlanProf()
	{
		return (eppStudent2EduPlanVersion != null) && (eppStudent2EduPlanVersion.getEduPlanVersion().getEduPlan() instanceof EppEduPlanProf);
	}

	public boolean isTemplateVisible()
	{
		return (!editForm) && (eppStudent2EduPlanVersion != null);
	}

    // Getters & Setters

    public DiplomaObject getDiplomaObject()
    {
        return diplomaObject;
    }

    public void setDiplomaObject(DiplomaObject diplomaObject)
    {
        this.diplomaObject = diplomaObject;
    }

	public Student getStudent()
	{
		return student;
	}

    public String getEducationElementTitle()
    {
        return educationElementTitle;
    }

    public void setEducationElementTitle(String educationElementTitle)
    {
        this.educationElementTitle = educationElementTitle;
    }

    public void setEditForm(boolean editForm)
    {
        this.editForm = editForm;
    }

    public boolean isEditForm()
    {
        return editForm;
    }

    public EppStudent2EduPlanVersion getEppStudent2EduPlanVersion()
    {
        return eppStudent2EduPlanVersion;
    }

    public void setEppStudent2EduPlanVersion(EppStudent2EduPlanVersion eppStudent2EduPlanVersion)
    {
        this.eppStudent2EduPlanVersion = eppStudent2EduPlanVersion;
    }

    public DipDocumentType getDocumentType()
    {
        return documentType;
    }

    public void setDocumentType(DipDocumentType documentType)
    {
        this.documentType = documentType;
    }

    public boolean isWithSuccess()
    {
        return withSuccess;
    }

    public void setWithSuccess(boolean withSuccess)
    {
        this.withSuccess = withSuccess;
    }

    public EduProgramSpecialization getSpecialization()
    {
        return specialization;
    }

    public void setSpecialization(EduProgramSpecialization specialization)
    {
        this.specialization = specialization;
    }

    public EppEduPlanProf getEduPlanProf()
    {
        return eduPlanProf;
    }

    public void setEduPlanProf(EppEduPlanProf eduPlanProf)
    {
        this.eduPlanProf = eduPlanProf;
    }

	public boolean isWithSuccessDisabled()
	{
		return withSuccessDisabled;
	}

	public boolean isIssued()
	{
		return issued;
	}

    public Double getLoadAsDouble()
    {
        return loadAsDouble;
    }

    public void setLoadAsDouble(Double loadAsDouble)
    {
        this.loadAsDouble = loadAsDouble;
    }

    public DiplomaTemplate getDiplomaTemplate()
	{
        return diplomaTemplate;
    }

    public void setDiplomaTemplate(DiplomaTemplate diplomaTemplate)
	{
        this.diplomaTemplate = diplomaTemplate;
    }

    public ISingleSelectModel getDocTypeModel()
    {
        return docTypeModel;
    }

    public String getLoadFieldTitle()
    {
        return diplomaObject.getContent().getLoadTitle();
    }
}
