/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unidip.base.bo.DiplomaTemplate.ui.LineAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.IFormatter;
import ru.tandemservice.unidip.base.bo.DipDocument.DipDocumentManager;
import ru.tandemservice.unidip.base.bo.DiplomaTemplate.logic.ControlActionComboBoxDSHandler;
import ru.tandemservice.unidip.base.bo.DiplomaTemplate.logic.RegElemPartComboBoxDSHandler;
import ru.tandemservice.unidip.base.bo.DiplomaTemplate.logic.RegElemPartDSHandler;
import ru.tandemservice.unidip.base.entity.catalog.DipAggregationMethod;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;


/**
 * @author iolshvang
 * @since 08.08.11 13:03
 */
@Configuration
public class DiplomaTemplateLineAddEdit extends BusinessComponentManager
{
    public static final String REG_ELEM_PART_DS = "regElemPartDS";
    public static final String REG_ELEM_PART_COMBO_BOX_DS = "regElemPartComboBoxDS";
    public static final String CONTROL_ACTION_COMBO_BOX_DS = "controlActionComboBoxDS";
    public static final String ALGORITHM_DS = "algorithmDS";
    public static final String MARK_SOURCE_DS = "markSourceDS";
    public static final String EPV_REGISTRY_ROW_DS = "epvRegistryRowDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(REG_ELEM_PART_DS, regElemPartDSColumns(), regElemPartDSHandler()))
                .addDataSource(selectDS(REG_ELEM_PART_COMBO_BOX_DS).handler(regElemPartComboBoxDSHandler()).addColumn(EppRegistryElementPart.titleWithNumber().s()))
                .addDataSource(selectDS(CONTROL_ACTION_COMBO_BOX_DS).handler(controlActionComboBoxDSHandler()).addColumn(EppRegistryElementPartFControlAction.controlAction().title().s()))
                .addDataSource(selectDS(ALGORITHM_DS).handler(DipDocumentManager.instance().dipAlgorithmComboDSHandler()).addColumn(DipAggregationMethod.P_TITLE))
                .addDataSource(selectDS(MARK_SOURCE_DS, DipDocumentManager.instance().markSourceComboDSHandler()))
                .addDataSource(selectDS(EPV_REGISTRY_ROW_DS, DipDocumentManager.instance().epvRegRowDSHandler()).addColumn(EppEpvRegistryRow.storedIndex().s()).addColumn(EppEpvRegistryRow.title().s()))
                .create();
    }

    @Bean
    public ColumnListExtPoint regElemPartDSColumns()
    {
        return columnListExtPointBuilder(REG_ELEM_PART_DS)
                .addColumn(blockColumn("regElemTitleBlock", "regElemTitleBlock"))
                .addColumn(blockColumn("regElemLoadBlock", "regElemLoadBlock"))
                .addColumn(blockColumn("edit", "actionBlock").width("1px").hasBlockHeader(true).required(true))
                .addColumn(blockColumn("deleteCancelBlock", "deleteCancelBlock").hint(" ").width("1px").hasBlockHeader(true).required(true))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> regElemPartDSHandler()
    {
        return new RegElemPartDSHandler(getName());
    }


    @Bean
    public IBusinessHandler<DSInput, DSOutput> regElemPartComboBoxDSHandler()
    {
        return new RegElemPartComboBoxDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> controlActionComboBoxDSHandler()
    {
        return new ControlActionComboBoxDSHandler(getName());
    }

}