package ru.tandemservice.unidip.base.entity.diploma;

import ru.tandemservice.unidip.base.bo.DipDocument.DipDocumentManager;
import ru.tandemservice.unidip.base.entity.diploma.gen.DiplomaOptDisciplineRowGen;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.codes.EppRegistryStructureCodes;

import java.util.List;

/**
 * Строка блока факультативных дисциплин в дипломе
 */
public class DiplomaOptDisciplineRow extends DiplomaOptDisciplineRowGen
{
    @Override
    protected int getBlockPriority()
    {
        return 5;
    }

    @Override
    protected boolean isLoadInWeeksForOKSO_and_SPO()
    {
        return false;
    }

    @Override
    protected String getPageTitle()
    {
        return "Факультативные дисциплины";
    }

    @Override
    public List<String> getRelatedRegistryElementTypeCodes()
    {
		// Здесь могут быть МСРП типа элемента реестра "Дисциплина" и всех подчиненных
        return DipDocumentManager.instance().dao().regElemTypeWithChildrenCodes(EppRegistryStructureCodes.REGISTRY_DISCIPLINE);
    }

    @Override
    public boolean isAllowFCA(EppFControlActionType fca)
    {
        // Для блока дисциплин по выбору доступны все формы контроля (настройки никакой нет)
        return true;
    }
}