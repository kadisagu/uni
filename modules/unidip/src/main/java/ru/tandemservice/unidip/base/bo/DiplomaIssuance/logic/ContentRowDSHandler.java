/* $Id$ */
package ru.tandemservice.unidip.base.bo.DiplomaIssuance.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContent;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow;

import java.util.ArrayList;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;

/**
 * @author Andrey Avetisov
 * @since 30.06.2014
 */
public class ContentRowDSHandler extends SimpleTitledComboDataSourceHandler
{
    public static final String DIPLOMA_CONTENT = "diplomaContent";

    public ContentRowDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        DiplomaContent content = context.get(DIPLOMA_CONTENT);

        List<Integer> numbers = new DQLSelectBuilder().fromEntity(DiplomaContentRow.class, "cr")
                .predicate(DQLPredicateType.distinct)
                .column(property("cr", DiplomaContentRow.contentListNumber()))
                .where(eq(property("cr", DiplomaContentRow.owner()), value(content)))
                .order(property("cr", DiplomaContentRow.contentListNumber()))
                .createStatement(context.getSession()).list();

        List<DataWrapper> wrappers = new ArrayList<>(numbers.size());
        for (Integer number : numbers)
        {
            wrappers.add(new DataWrapper((long) number, "Приложение №" + number));
        }

        context.put(UIDefines.COMBO_OBJECT_LIST, wrappers);
        return super.execute(input, context);
    }
}
