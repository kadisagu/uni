/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unidip.aggregationMethod.bo.AggMethodManual;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

import ru.tandemservice.unidip.aggregationMethod.bo.AggMethodManual.logic.AggMethodManualDao;
import ru.tandemservice.unidip.base.bo.DipDocument.IAggMethodManager;
import ru.tandemservice.unidip.base.bo.DipDocument.logic.IAggMethodDao;

/**
 * @author iolshvang
 * @since 07.09.11 16:22
 */
@Configuration
public class AggMethodManualManager extends BusinessObjectManager implements IAggMethodManager
{
    public static AggMethodManualManager instance()
    {
        return instance(AggMethodManualManager.class);
    }

    @Bean
    @Override
    public IAggMethodDao aggMethodDao()
    {
        return new AggMethodManualDao();
    }
}
