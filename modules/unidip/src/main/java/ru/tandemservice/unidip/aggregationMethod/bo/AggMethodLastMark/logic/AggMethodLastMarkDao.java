/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unidip.aggregationMethod.bo.AggMethodLastMark.logic;

import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unidip.base.bo.DipDocument.logic.IAggMethodDao;
import ru.tandemservice.unidip.base.bo.DipDocument.logic.IRowTotalData;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRegElPartFControlAction;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaObject;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartModule;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModuleALoad;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionStudentGradeBookDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.mark.SessionSlotLinkMark;
import ru.tandemservice.unisession.entity.mark.SessionSlotRegularMark;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.sum;

/**
 * @author iolshvang
 * @since 07.09.11 16:22
 */
public class AggMethodLastMarkDao extends CommonDAO implements IAggMethodDao
{
    private IRowTotalData getAggData(final List<SessionMark> marks, final List<EppRegistryElementPart> regPartList)
    {
           return new IRowTotalData()
           {
               @Override
               public Long getAudLoadAsLong()
               {
                   Long load = 0L;
                   for (EppRegistryElementPart part : regPartList)
                   {
                       load += part.getRegistryElement().getSize();
                   }
                   return load;
               }

               @Override
               public Long getLoadAsLong()
               {
                   return new DQLSelectBuilder()
                           .fromEntity(EppRegistryElementPartModule.class, "rel")
                           .where(in(EppRegistryElementPartModule.part().id().fromAlias("rel"), ids(regPartList)))
                           .joinEntity("rel", DQLJoinType.inner, EppRegistryModuleALoad.class, "aload", eq(property(EppRegistryModuleALoad.module().fromAlias("aload")), property(EppRegistryElementPartModule.module().fromAlias("rel"))))
                           .column(sum(property("aload.load")))
                           .createStatement(getSession()).<Long>uniqueResult();
               }

               @Override
               public String getMark()
               {
                   Comparator<SessionSlotRegularMark> regMarkComparator = Comparator.comparing(SessionSlotRegularMark::getSessionMarkTypePriority);
                   Comparator<SessionMarkCatalogItem> catalogMarkComparator = Comparator.comparing(SessionMarkCatalogItem::getPriority).reversed();
                   Comparator<SessionMark> priorityComparator = Comparator.comparing((SessionMark mark) -> mark.getSlot().getStudentWpeCAction().getType().getPriority())
                           .thenComparing(mark -> (mark instanceof SessionSlotLinkMark) ? ((SessionSlotLinkMark) mark).getTarget() : (SessionSlotRegularMark) mark, regMarkComparator)
                           .thenComparing(SessionMark::getValueItem, catalogMarkComparator);
                   return Collections.max(marks, (o1, o2) -> {

                       int term1 = o1.getSlot().getStudentWpeCAction().getStudentWpe().getTerm().getIntValue();
                       int term2 = o2.getSlot().getStudentWpeCAction().getStudentWpe().getTerm().getIntValue();

                       if (term1==term2)
                       {
                           if ((o1.getSlot().getStudentWpeCAction().getStudentWpe().getRegistryElementPart().getHierarhyParent().getId().equals(o2.getSlot().getStudentWpeCAction().getStudentWpe().getRegistryElementPart().getHierarhyParent().getId())))
                               return Integer.valueOf(o1.getSlot().getStudentWpeCAction().getStudentWpe().getRegistryElementPart().getNumber()).compareTo(o2.getSlot().getStudentWpeCAction().getStudentWpe().getRegistryElementPart().getNumber());
                           return priorityComparator.compare(o1, o2);

                       }
                       return Integer.valueOf(term1).compareTo(term2);
                   }).getValueTitle();
               }
           };
    }

    @Override
    public Map<Long, IRowTotalData> getTotalRowData(Collection<Long> rowIds)
    {
        Map<Long, List<SessionMark>> rowMarkMap = new HashMap<>();

        DQLSelectBuilder markBuilder = new DQLSelectBuilder()
                .fromEntity(DiplomaContentRow.class, "r")
                .where(in(property(DiplomaContentRow.id().fromAlias("r")), rowIds))
                .joinEntity("r", DQLJoinType.left, DiplomaObject.class, "doc", eq(property(DiplomaObject.content().fromAlias("doc")), property(DiplomaContentRow.owner().fromAlias("r"))))
                .joinEntity("doc", DQLJoinType.inner, EppStudentWorkPlanElement.class, "epvSlot", eq(property(DiplomaObject.student().fromAlias("doc")), property(EppStudentWorkPlanElement.student().fromAlias("epvSlot"))))
                .joinEntity("epvSlot", DQLJoinType.inner, EppStudentWpeCAction.class, "wpcaSlot", eq(property("epvSlot"), property(EppStudentWpeCAction.studentWpe().fromAlias("wpcaSlot"))))
                .joinEntity("wpcaSlot", DQLJoinType.inner, SessionDocumentSlot.class, "sdSlot", eq(property("wpcaSlot"), property(SessionDocumentSlot.studentWpeCAction().fromAlias("sdSlot"))))
                .joinEntity("sdSlot", DQLJoinType.inner, SessionMark.class, "mark", eq(property("sdSlot"), property(SessionMark.slot().fromAlias("mark"))))
                .joinEntity("r", DQLJoinType.inner, DiplomaContentRegElPartFControlAction.class, "rowRel", eq(property(DiplomaContentRegElPartFControlAction.row().fromAlias("rowRel")), property("r")))

                .joinPath(DQLJoinType.inner, SessionMark.slot().fromAlias("mark"), "slot")
                .where(eq(property(SessionDocumentSlot.inSession().fromAlias("slot")), value(false)))
                .joinEntity("mark", DQLJoinType.inner, SessionStudentGradeBookDocument.class, "book", eq(property(SessionDocumentSlot.document().fromAlias("slot")), property("book")))
                .joinPath(DQLJoinType.inner, SessionDocumentSlot.studentWpeCAction().fromAlias("slot"), "caSlot")
                .joinPath(DQLJoinType.inner, EppStudentWpeCAction.studentWpe().fromAlias("caSlot"), "eppSlot")
                .joinEntity("slot", DQLJoinType.inner, EppRegistryElementPartFControlAction.class, "rel",
                            eq(property(EppStudentWorkPlanElement.registryElementPart().fromAlias("eppSlot")), property("rel.part"))
                )
                .where(eq(property(EppStudentWpeCAction.type().fromAlias("caSlot")), property("rel", EppRegistryElementPartFControlAction.controlAction().eppGroupType())))
                .where(eq(property("rel"), property(DiplomaContentRegElPartFControlAction.registryElementPartFControlAction().fromAlias("rowRel"))))
                .column("r.id")
                .column("mark");

            for (Object res : markBuilder.createStatement(getSession()).list())
            {
                SafeMap.safeGet(rowMarkMap, (Long) ((Object[]) res)[0], ArrayList.class).add((SessionMark) ((Object[]) res)[1]);
            }
            
            Map<Long, IRowTotalData> totalDataMap = new HashMap<>();
            for (Long rowId : rowMarkMap.keySet())
            {
                totalDataMap.put(rowId, getAggData(rowMarkMap.get(rowId),
                    new DQLSelectBuilder().fromEntity(DiplomaContentRegElPartFControlAction.class, "r")
                       .where(eq(property(DiplomaContentRegElPartFControlAction.row().id().fromAlias("r")), value(rowId)))
                       .column("r.registryElementPartFControlAction.part").createStatement(getSession()).<EppRegistryElementPart>list()));
            }
        return totalDataMap;
    }
}
