package ru.tandemservice.unidip.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.PreparedStatement;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unidip_2x6x2_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность diplomaContentRow

		// создано обязательное свойство contentListNumber
		{
			// создать колонку
            if(!tool.columnExists("diploma_content_row", "contentlistnumber_p"))
            {
                tool.createColumn("diploma_content_row", new DBColumn("contentlistnumber_p", DBType.INTEGER));

                // задать значение по умолчанию
                java.lang.Integer defaultContentListNumber = 1;
                tool.executeUpdate("update diploma_content_row set contentlistnumber_p=? where contentlistnumber_p is null", defaultContentListNumber);

                // сделать колонку NOT NULL
                tool.setColumnNullable("diploma_content_row", "contentlistnumber_p", false);
            }

		}

        ////////////////////////////////////////////////////////////////////////////////
        // сущность dipDocumentType
        //так как список двухуровневый, то дочернии элементы, не должны иметь своих наследников
        {
            tool.executeUpdate("UPDATE dip_c_type set childallowed_p=? where parent_id is not null", false);
        }
    }
}