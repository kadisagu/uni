package ru.tandemservice.unidip.bso.entity.blank.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank;
import ru.tandemservice.unidip.bso.entity.catalog.DipBlankDisposalReason;
import ru.tandemservice.unidip.bso.entity.catalog.DipBlankState;
import ru.tandemservice.unidip.bso.entity.catalog.DipBlankType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Бланк диплома (приложения)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DipDiplomaBlankGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank";
    public static final String ENTITY_NAME = "dipDiplomaBlank";
    public static final int VERSION_HASH = 85639707;
    private static IEntityMeta ENTITY_META;

    public static final String L_BLANK_TYPE = "blankType";
    public static final String L_BLANK_STATE = "blankState";
    public static final String P_SERIA = "seria";
    public static final String P_NUMBER = "number";
    public static final String L_STORAGE_LOCATION = "storageLocation";
    public static final String L_DISPOSAL_REASON = "disposalReason";
    public static final String P_DISPOSAL_DATE = "disposalDate";
    public static final String P_PRINTING_OFFICE = "printingOffice";
    public static final String P_INVOICE = "invoice";
    public static final String P_RESERVATION_PURPOSE = "reservationPurpose";
    public static final String P_SERIA_AND_NUMBER = "seriaAndNumber";

    private DipBlankType _blankType;     // Тип бланка
    private DipBlankState _blankState;     // Состояние
    private String _seria;     // Серия бланка
    private String _number;     // Номер бланка
    private OrgUnit _storageLocation;     // Место хранения
    private DipBlankDisposalReason _disposalReason;     // Причина списания
    private Date _disposalDate;     // Дата списания
    private String _printingOffice;     // Типография
    private String _invoice;     // Накладная
    private String _reservationPurpose;     // Цель резервирования

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Тип бланка. Свойство не может быть null.
     */
    @NotNull
    public DipBlankType getBlankType()
    {
        return _blankType;
    }

    /**
     * @param blankType Тип бланка. Свойство не может быть null.
     */
    public void setBlankType(DipBlankType blankType)
    {
        dirty(_blankType, blankType);
        _blankType = blankType;
    }

    /**
     * @return Состояние. Свойство не может быть null.
     */
    @NotNull
    public DipBlankState getBlankState()
    {
        return _blankState;
    }

    /**
     * @param blankState Состояние. Свойство не может быть null.
     */
    public void setBlankState(DipBlankState blankState)
    {
        dirty(_blankState, blankState);
        _blankState = blankState;
    }

    /**
     * @return Серия бланка. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getSeria()
    {
        return _seria;
    }

    /**
     * @param seria Серия бланка. Свойство не может быть null.
     */
    public void setSeria(String seria)
    {
        dirty(_seria, seria);
        _seria = seria;
    }

    /**
     * @return Номер бланка. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер бланка. Свойство не может быть null.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Место хранения.
     */
    public OrgUnit getStorageLocation()
    {
        return _storageLocation;
    }

    /**
     * @param storageLocation Место хранения.
     */
    public void setStorageLocation(OrgUnit storageLocation)
    {
        dirty(_storageLocation, storageLocation);
        _storageLocation = storageLocation;
    }

    /**
     * @return Причина списания.
     */
    public DipBlankDisposalReason getDisposalReason()
    {
        return _disposalReason;
    }

    /**
     * @param disposalReason Причина списания.
     */
    public void setDisposalReason(DipBlankDisposalReason disposalReason)
    {
        dirty(_disposalReason, disposalReason);
        _disposalReason = disposalReason;
    }

    /**
     * @return Дата списания.
     */
    public Date getDisposalDate()
    {
        return _disposalDate;
    }

    /**
     * @param disposalDate Дата списания.
     */
    public void setDisposalDate(Date disposalDate)
    {
        dirty(_disposalDate, disposalDate);
        _disposalDate = disposalDate;
    }

    /**
     * @return Типография.
     */
    @Length(max=255)
    public String getPrintingOffice()
    {
        return _printingOffice;
    }

    /**
     * @param printingOffice Типография.
     */
    public void setPrintingOffice(String printingOffice)
    {
        dirty(_printingOffice, printingOffice);
        _printingOffice = printingOffice;
    }

    /**
     * @return Накладная.
     */
    @Length(max=255)
    public String getInvoice()
    {
        return _invoice;
    }

    /**
     * @param invoice Накладная.
     */
    public void setInvoice(String invoice)
    {
        dirty(_invoice, invoice);
        _invoice = invoice;
    }

    /**
     * @return Цель резервирования.
     */
    @Length(max=255)
    public String getReservationPurpose()
    {
        return _reservationPurpose;
    }

    /**
     * @param reservationPurpose Цель резервирования.
     */
    public void setReservationPurpose(String reservationPurpose)
    {
        dirty(_reservationPurpose, reservationPurpose);
        _reservationPurpose = reservationPurpose;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof DipDiplomaBlankGen)
        {
            setBlankType(((DipDiplomaBlank)another).getBlankType());
            setBlankState(((DipDiplomaBlank)another).getBlankState());
            setSeria(((DipDiplomaBlank)another).getSeria());
            setNumber(((DipDiplomaBlank)another).getNumber());
            setStorageLocation(((DipDiplomaBlank)another).getStorageLocation());
            setDisposalReason(((DipDiplomaBlank)another).getDisposalReason());
            setDisposalDate(((DipDiplomaBlank)another).getDisposalDate());
            setPrintingOffice(((DipDiplomaBlank)another).getPrintingOffice());
            setInvoice(((DipDiplomaBlank)another).getInvoice());
            setReservationPurpose(((DipDiplomaBlank)another).getReservationPurpose());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DipDiplomaBlankGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DipDiplomaBlank.class;
        }

        public T newInstance()
        {
            return (T) new DipDiplomaBlank();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "blankType":
                    return obj.getBlankType();
                case "blankState":
                    return obj.getBlankState();
                case "seria":
                    return obj.getSeria();
                case "number":
                    return obj.getNumber();
                case "storageLocation":
                    return obj.getStorageLocation();
                case "disposalReason":
                    return obj.getDisposalReason();
                case "disposalDate":
                    return obj.getDisposalDate();
                case "printingOffice":
                    return obj.getPrintingOffice();
                case "invoice":
                    return obj.getInvoice();
                case "reservationPurpose":
                    return obj.getReservationPurpose();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "blankType":
                    obj.setBlankType((DipBlankType) value);
                    return;
                case "blankState":
                    obj.setBlankState((DipBlankState) value);
                    return;
                case "seria":
                    obj.setSeria((String) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "storageLocation":
                    obj.setStorageLocation((OrgUnit) value);
                    return;
                case "disposalReason":
                    obj.setDisposalReason((DipBlankDisposalReason) value);
                    return;
                case "disposalDate":
                    obj.setDisposalDate((Date) value);
                    return;
                case "printingOffice":
                    obj.setPrintingOffice((String) value);
                    return;
                case "invoice":
                    obj.setInvoice((String) value);
                    return;
                case "reservationPurpose":
                    obj.setReservationPurpose((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "blankType":
                        return true;
                case "blankState":
                        return true;
                case "seria":
                        return true;
                case "number":
                        return true;
                case "storageLocation":
                        return true;
                case "disposalReason":
                        return true;
                case "disposalDate":
                        return true;
                case "printingOffice":
                        return true;
                case "invoice":
                        return true;
                case "reservationPurpose":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "blankType":
                    return true;
                case "blankState":
                    return true;
                case "seria":
                    return true;
                case "number":
                    return true;
                case "storageLocation":
                    return true;
                case "disposalReason":
                    return true;
                case "disposalDate":
                    return true;
                case "printingOffice":
                    return true;
                case "invoice":
                    return true;
                case "reservationPurpose":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "blankType":
                    return DipBlankType.class;
                case "blankState":
                    return DipBlankState.class;
                case "seria":
                    return String.class;
                case "number":
                    return String.class;
                case "storageLocation":
                    return OrgUnit.class;
                case "disposalReason":
                    return DipBlankDisposalReason.class;
                case "disposalDate":
                    return Date.class;
                case "printingOffice":
                    return String.class;
                case "invoice":
                    return String.class;
                case "reservationPurpose":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DipDiplomaBlank> _dslPath = new Path<DipDiplomaBlank>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DipDiplomaBlank");
    }
            

    /**
     * @return Тип бланка. Свойство не может быть null.
     * @see ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank#getBlankType()
     */
    public static DipBlankType.Path<DipBlankType> blankType()
    {
        return _dslPath.blankType();
    }

    /**
     * @return Состояние. Свойство не может быть null.
     * @see ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank#getBlankState()
     */
    public static DipBlankState.Path<DipBlankState> blankState()
    {
        return _dslPath.blankState();
    }

    /**
     * @return Серия бланка. Свойство не может быть null.
     * @see ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank#getSeria()
     */
    public static PropertyPath<String> seria()
    {
        return _dslPath.seria();
    }

    /**
     * @return Номер бланка. Свойство не может быть null.
     * @see ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Место хранения.
     * @see ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank#getStorageLocation()
     */
    public static OrgUnit.Path<OrgUnit> storageLocation()
    {
        return _dslPath.storageLocation();
    }

    /**
     * @return Причина списания.
     * @see ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank#getDisposalReason()
     */
    public static DipBlankDisposalReason.Path<DipBlankDisposalReason> disposalReason()
    {
        return _dslPath.disposalReason();
    }

    /**
     * @return Дата списания.
     * @see ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank#getDisposalDate()
     */
    public static PropertyPath<Date> disposalDate()
    {
        return _dslPath.disposalDate();
    }

    /**
     * @return Типография.
     * @see ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank#getPrintingOffice()
     */
    public static PropertyPath<String> printingOffice()
    {
        return _dslPath.printingOffice();
    }

    /**
     * @return Накладная.
     * @see ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank#getInvoice()
     */
    public static PropertyPath<String> invoice()
    {
        return _dslPath.invoice();
    }

    /**
     * @return Цель резервирования.
     * @see ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank#getReservationPurpose()
     */
    public static PropertyPath<String> reservationPurpose()
    {
        return _dslPath.reservationPurpose();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank#getSeriaAndNumber()
     */
    public static SupportedPropertyPath<String> seriaAndNumber()
    {
        return _dslPath.seriaAndNumber();
    }

    public static class Path<E extends DipDiplomaBlank> extends EntityPath<E>
    {
        private DipBlankType.Path<DipBlankType> _blankType;
        private DipBlankState.Path<DipBlankState> _blankState;
        private PropertyPath<String> _seria;
        private PropertyPath<String> _number;
        private OrgUnit.Path<OrgUnit> _storageLocation;
        private DipBlankDisposalReason.Path<DipBlankDisposalReason> _disposalReason;
        private PropertyPath<Date> _disposalDate;
        private PropertyPath<String> _printingOffice;
        private PropertyPath<String> _invoice;
        private PropertyPath<String> _reservationPurpose;
        private SupportedPropertyPath<String> _seriaAndNumber;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Тип бланка. Свойство не может быть null.
     * @see ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank#getBlankType()
     */
        public DipBlankType.Path<DipBlankType> blankType()
        {
            if(_blankType == null )
                _blankType = new DipBlankType.Path<DipBlankType>(L_BLANK_TYPE, this);
            return _blankType;
        }

    /**
     * @return Состояние. Свойство не может быть null.
     * @see ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank#getBlankState()
     */
        public DipBlankState.Path<DipBlankState> blankState()
        {
            if(_blankState == null )
                _blankState = new DipBlankState.Path<DipBlankState>(L_BLANK_STATE, this);
            return _blankState;
        }

    /**
     * @return Серия бланка. Свойство не может быть null.
     * @see ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank#getSeria()
     */
        public PropertyPath<String> seria()
        {
            if(_seria == null )
                _seria = new PropertyPath<String>(DipDiplomaBlankGen.P_SERIA, this);
            return _seria;
        }

    /**
     * @return Номер бланка. Свойство не может быть null.
     * @see ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(DipDiplomaBlankGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Место хранения.
     * @see ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank#getStorageLocation()
     */
        public OrgUnit.Path<OrgUnit> storageLocation()
        {
            if(_storageLocation == null )
                _storageLocation = new OrgUnit.Path<OrgUnit>(L_STORAGE_LOCATION, this);
            return _storageLocation;
        }

    /**
     * @return Причина списания.
     * @see ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank#getDisposalReason()
     */
        public DipBlankDisposalReason.Path<DipBlankDisposalReason> disposalReason()
        {
            if(_disposalReason == null )
                _disposalReason = new DipBlankDisposalReason.Path<DipBlankDisposalReason>(L_DISPOSAL_REASON, this);
            return _disposalReason;
        }

    /**
     * @return Дата списания.
     * @see ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank#getDisposalDate()
     */
        public PropertyPath<Date> disposalDate()
        {
            if(_disposalDate == null )
                _disposalDate = new PropertyPath<Date>(DipDiplomaBlankGen.P_DISPOSAL_DATE, this);
            return _disposalDate;
        }

    /**
     * @return Типография.
     * @see ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank#getPrintingOffice()
     */
        public PropertyPath<String> printingOffice()
        {
            if(_printingOffice == null )
                _printingOffice = new PropertyPath<String>(DipDiplomaBlankGen.P_PRINTING_OFFICE, this);
            return _printingOffice;
        }

    /**
     * @return Накладная.
     * @see ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank#getInvoice()
     */
        public PropertyPath<String> invoice()
        {
            if(_invoice == null )
                _invoice = new PropertyPath<String>(DipDiplomaBlankGen.P_INVOICE, this);
            return _invoice;
        }

    /**
     * @return Цель резервирования.
     * @see ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank#getReservationPurpose()
     */
        public PropertyPath<String> reservationPurpose()
        {
            if(_reservationPurpose == null )
                _reservationPurpose = new PropertyPath<String>(DipDiplomaBlankGen.P_RESERVATION_PURPOSE, this);
            return _reservationPurpose;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank#getSeriaAndNumber()
     */
        public SupportedPropertyPath<String> seriaAndNumber()
        {
            if(_seriaAndNumber == null )
                _seriaAndNumber = new SupportedPropertyPath<String>(DipDiplomaBlankGen.P_SERIA_AND_NUMBER, this);
            return _seriaAndNumber;
        }

        public Class getEntityClass()
        {
            return DipDiplomaBlank.class;
        }

        public String getEntityName()
        {
            return "dipDiplomaBlank";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getSeriaAndNumber();
}
