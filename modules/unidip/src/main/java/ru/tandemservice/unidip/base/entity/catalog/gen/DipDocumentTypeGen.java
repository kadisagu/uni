package ru.tandemservice.unidip.base.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unidip.base.entity.catalog.DipDocumentType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Тип документов об обучении
 *
 * Тип документов, выдаваемых студентам по окончанию обучения.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DipDocumentTypeGen extends EntityBase
 implements INaturalIdentifiable<DipDocumentTypeGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unidip.base.entity.catalog.DipDocumentType";
    public static final String ENTITY_NAME = "dipDocumentType";
    public static final int VERSION_HASH = 903984504;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String L_PARENT = "parent";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_STATE_DOC_TYPE = "stateDocType";
    public static final String P_CHILD_ALLOWED = "childAllowed";
    public static final String P_FORMING_DOC_ALLOWED = "formingDocAllowed";
    public static final String P_TITLE = "title";
    public static final String P_TITLE_WITH_PARENT = "titleWithParent";

    private String _code;     // Системный код
    private DipDocumentType _parent;     // Тип документов об обучении
    private String _shortTitle;     // Сокращенное название
    private boolean _stateDocType;     // Государственного образца
    private boolean _childAllowed;     // Разрешено добавление дочерних элементов
    private boolean _formingDocAllowed = true;     // Разрешено формирование документов
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Тип документов об обучении.
     */
    public DipDocumentType getParent()
    {
        return _parent;
    }

    /**
     * @param parent Тип документов об обучении.
     */
    public void setParent(DipDocumentType parent)
    {
        dirty(_parent, parent);
        _parent = parent;
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название. Свойство не может быть null.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return Государственного образца. Свойство не может быть null.
     */
    @NotNull
    public boolean isStateDocType()
    {
        return _stateDocType;
    }

    /**
     * @param stateDocType Государственного образца. Свойство не может быть null.
     */
    public void setStateDocType(boolean stateDocType)
    {
        dirty(_stateDocType, stateDocType);
        _stateDocType = stateDocType;
    }

    /**
     * @return Разрешено добавление дочерних элементов. Свойство не может быть null.
     */
    @NotNull
    public boolean isChildAllowed()
    {
        return _childAllowed;
    }

    /**
     * @param childAllowed Разрешено добавление дочерних элементов. Свойство не может быть null.
     */
    public void setChildAllowed(boolean childAllowed)
    {
        dirty(_childAllowed, childAllowed);
        _childAllowed = childAllowed;
    }

    /**
     * @return Разрешено формирование документов. Свойство не может быть null.
     */
    @NotNull
    public boolean isFormingDocAllowed()
    {
        return _formingDocAllowed;
    }

    /**
     * @param formingDocAllowed Разрешено формирование документов. Свойство не может быть null.
     */
    public void setFormingDocAllowed(boolean formingDocAllowed)
    {
        dirty(_formingDocAllowed, formingDocAllowed);
        _formingDocAllowed = formingDocAllowed;
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof DipDocumentTypeGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((DipDocumentType)another).getCode());
            }
            setParent(((DipDocumentType)another).getParent());
            setShortTitle(((DipDocumentType)another).getShortTitle());
            setStateDocType(((DipDocumentType)another).isStateDocType());
            setChildAllowed(((DipDocumentType)another).isChildAllowed());
            setFormingDocAllowed(((DipDocumentType)another).isFormingDocAllowed());
            setTitle(((DipDocumentType)another).getTitle());
        }
    }

    public INaturalId<DipDocumentTypeGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<DipDocumentTypeGen>
    {
        private static final String PROXY_NAME = "DipDocumentTypeNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof DipDocumentTypeGen.NaturalId) ) return false;

            DipDocumentTypeGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DipDocumentTypeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DipDocumentType.class;
        }

        public T newInstance()
        {
            return (T) new DipDocumentType();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "parent":
                    return obj.getParent();
                case "shortTitle":
                    return obj.getShortTitle();
                case "stateDocType":
                    return obj.isStateDocType();
                case "childAllowed":
                    return obj.isChildAllowed();
                case "formingDocAllowed":
                    return obj.isFormingDocAllowed();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "parent":
                    obj.setParent((DipDocumentType) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "stateDocType":
                    obj.setStateDocType((Boolean) value);
                    return;
                case "childAllowed":
                    obj.setChildAllowed((Boolean) value);
                    return;
                case "formingDocAllowed":
                    obj.setFormingDocAllowed((Boolean) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "parent":
                        return true;
                case "shortTitle":
                        return true;
                case "stateDocType":
                        return true;
                case "childAllowed":
                        return true;
                case "formingDocAllowed":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "parent":
                    return true;
                case "shortTitle":
                    return true;
                case "stateDocType":
                    return true;
                case "childAllowed":
                    return true;
                case "formingDocAllowed":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "parent":
                    return DipDocumentType.class;
                case "shortTitle":
                    return String.class;
                case "stateDocType":
                    return Boolean.class;
                case "childAllowed":
                    return Boolean.class;
                case "formingDocAllowed":
                    return Boolean.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DipDocumentType> _dslPath = new Path<DipDocumentType>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DipDocumentType");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unidip.base.entity.catalog.DipDocumentType#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Тип документов об обучении.
     * @see ru.tandemservice.unidip.base.entity.catalog.DipDocumentType#getParent()
     */
    public static DipDocumentType.Path<DipDocumentType> parent()
    {
        return _dslPath.parent();
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.catalog.DipDocumentType#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return Государственного образца. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.catalog.DipDocumentType#isStateDocType()
     */
    public static PropertyPath<Boolean> stateDocType()
    {
        return _dslPath.stateDocType();
    }

    /**
     * @return Разрешено добавление дочерних элементов. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.catalog.DipDocumentType#isChildAllowed()
     */
    public static PropertyPath<Boolean> childAllowed()
    {
        return _dslPath.childAllowed();
    }

    /**
     * @return Разрешено формирование документов. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.catalog.DipDocumentType#isFormingDocAllowed()
     */
    public static PropertyPath<Boolean> formingDocAllowed()
    {
        return _dslPath.formingDocAllowed();
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.unidip.base.entity.catalog.DipDocumentType#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unidip.base.entity.catalog.DipDocumentType#getTitleWithParent()
     */
    public static SupportedPropertyPath<String> titleWithParent()
    {
        return _dslPath.titleWithParent();
    }

    public static class Path<E extends DipDocumentType> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private DipDocumentType.Path<DipDocumentType> _parent;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<Boolean> _stateDocType;
        private PropertyPath<Boolean> _childAllowed;
        private PropertyPath<Boolean> _formingDocAllowed;
        private PropertyPath<String> _title;
        private SupportedPropertyPath<String> _titleWithParent;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unidip.base.entity.catalog.DipDocumentType#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(DipDocumentTypeGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Тип документов об обучении.
     * @see ru.tandemservice.unidip.base.entity.catalog.DipDocumentType#getParent()
     */
        public DipDocumentType.Path<DipDocumentType> parent()
        {
            if(_parent == null )
                _parent = new DipDocumentType.Path<DipDocumentType>(L_PARENT, this);
            return _parent;
        }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.catalog.DipDocumentType#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(DipDocumentTypeGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return Государственного образца. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.catalog.DipDocumentType#isStateDocType()
     */
        public PropertyPath<Boolean> stateDocType()
        {
            if(_stateDocType == null )
                _stateDocType = new PropertyPath<Boolean>(DipDocumentTypeGen.P_STATE_DOC_TYPE, this);
            return _stateDocType;
        }

    /**
     * @return Разрешено добавление дочерних элементов. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.catalog.DipDocumentType#isChildAllowed()
     */
        public PropertyPath<Boolean> childAllowed()
        {
            if(_childAllowed == null )
                _childAllowed = new PropertyPath<Boolean>(DipDocumentTypeGen.P_CHILD_ALLOWED, this);
            return _childAllowed;
        }

    /**
     * @return Разрешено формирование документов. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.catalog.DipDocumentType#isFormingDocAllowed()
     */
        public PropertyPath<Boolean> formingDocAllowed()
        {
            if(_formingDocAllowed == null )
                _formingDocAllowed = new PropertyPath<Boolean>(DipDocumentTypeGen.P_FORMING_DOC_ALLOWED, this);
            return _formingDocAllowed;
        }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.unidip.base.entity.catalog.DipDocumentType#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(DipDocumentTypeGen.P_TITLE, this);
            return _title;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unidip.base.entity.catalog.DipDocumentType#getTitleWithParent()
     */
        public SupportedPropertyPath<String> titleWithParent()
        {
            if(_titleWithParent == null )
                _titleWithParent = new SupportedPropertyPath<String>(DipDocumentTypeGen.P_TITLE_WITH_PARENT, this);
            return _titleWithParent;
        }

        public Class getEntityClass()
        {
            return DipDocumentType.class;
        }

        public String getEntityName()
        {
            return "dipDocumentType";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getTitleWithParent();
}
