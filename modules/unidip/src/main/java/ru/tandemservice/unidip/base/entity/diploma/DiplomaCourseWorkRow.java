package ru.tandemservice.unidip.base.entity.diploma;

import com.google.common.collect.ImmutableList;
import ru.tandemservice.unidip.base.entity.diploma.gen.DiplomaCourseWorkRowGen;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.codes.EppRegistryStructureCodes;
import ru.tandemservice.uniepp.util.EppFControlActionInfo;

import java.util.List;

/**
 * Строка блока Курсовые работы в дипломе
 */
public class DiplomaCourseWorkRow extends DiplomaCourseWorkRowGen
{
    @Override
    protected int getBlockPriority()
    {
        return 4;
    }

    @Override
    protected boolean isLoadInWeeksForOKSO_and_SPO()
    {
        return false;
    }

    @Override
    protected String getPageTitle()
    {
        return "Курсовые работы (проекты) и НИР";
    }

    @Override
    public List<String> getRelatedRegistryElementTypeCodes()
    {
        return ImmutableList.of(EppRegistryStructureCodes.REGISTRY_DISCIPLINE);
    }

    @Override
    public boolean isAllowFCA(EppFControlActionType fca)
    {
        // В блоке с курсовыми можно добавлять только ФИК "курсовой проект" и "курсовая работа"
        return EppFControlActionInfo.SUPPLIER.get().isCourseWork(fca.getId());
    }
}