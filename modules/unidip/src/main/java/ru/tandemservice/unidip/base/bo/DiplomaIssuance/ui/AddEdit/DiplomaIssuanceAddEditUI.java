/* $Id$ */
package ru.tandemservice.unidip.base.bo.DiplomaIssuance.ui.AddEdit;

import org.apache.commons.io.IOUtils;
import org.apache.tapestry.form.validator.Required;
import org.apache.tapestry.form.validator.Validator;
import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unidip.base.bo.DiplomaIssuance.DiplomaIssuanceManager;
import ru.tandemservice.unidip.base.bo.DiplomaIssuance.logic.ContentRowDSHandler;
import ru.tandemservice.unidip.base.bo.DiplomaIssuance.logic.DiplomaContentIssuanceListDSHandler;
import ru.tandemservice.unidip.base.bo.DiplomaIssuance.logic.SeriaNumberValidator;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentIssuance;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaObject;
import ru.tandemservice.unidip.base.entity.order.DipContentIssuanceToDipExtractRelation;
import ru.tandemservice.unidip.base.entity.order.DipStuExcludeExtract;
import ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank;
import ru.tandemservice.unidip.bso.entity.catalog.codes.DipBlankStateCodes;
import ru.tandemservice.unidip.settings.bo.DipSettings.DipSettingsManager;

import java.util.*;

/**
 * @author Andrey Avetisov
 * @since 25.06.2014
 */
@Input({
               @Bind(key = DiplomaIssuanceAddEditUI.DIP_DOCUMENT_ID, binding = "diplomaObject.id"),
               @Bind(key = DiplomaIssuanceAddEditUI.DIP_ISSUANCE_ID, binding = "diplomaIssuance.id")
       })
public class DiplomaIssuanceAddEditUI extends UIPresenter
{
    public static final String DIP_DOCUMENT_ID = "dipDocumentId";
    public static final String DIP_ISSUANCE_ID = "dipIssuanceId";

    private DiplomaObject _diplomaObject = new DiplomaObject();
    private DiplomaIssuance _diplomaIssuance = new DiplomaIssuance();
    private String _issuanceInformation;

    private boolean _editForm;

    private List<DataWrapper> _diplomaContentIssuanceList;
    private DataWrapper _currentEditRow;
    private IUIDataSource _issuanceApplicationDS;
    private IUploadFile _uploadDocument;


    private String beforeEditBlankNumber;
    private String beforeEditBlankSeria;
    private int beforeEditSheetNumber;
    private boolean beforeEditDuplicate;
    private String beforeEditDuplicateRegNum;
    private Date beforeEditDuplicateIssDate;


    private DataWrapper _contentListNumber = new DataWrapper();

    private Long _id = 0L;

    private boolean editRowCreated;

    public Long getNextId()
    {
        return _id++;
    }

    @Override
    public void onComponentRefresh()
    {
        setCurrentEditRow(null);
        setIssuanceApplicationDS(getConfig().getDataSource(DiplomaIssuanceAddEdit.ISSUANCE_APPLICATION_DS));
        _oldBlankMap = new HashMap<>();
        if (_diplomaObject.getId() != null)
        {
            _diplomaObject = DataAccessServices.dao().getNotNull(DiplomaObject.class, _diplomaObject.getId());
            List<DiplomaContentIssuance> contentIssuanceList = DataAccessServices.dao().getList(DiplomaContentIssuance.class, DiplomaContentIssuance.diplomaIssuance(),
                                                                                                _diplomaIssuance, DiplomaContentIssuance.P_BLANK_SERIA, DiplomaContentIssuance.P_BLANK_NUMBER);
            _diplomaContentIssuanceList = new ArrayList<>();
            for (DiplomaContentIssuance contentIssuance : contentIssuanceList)
            {
                DataWrapper wrapper = new DataWrapper(contentIssuance);
                wrapper.getWrapped();
                wrapper.setId(getNextId());
                _diplomaContentIssuanceList.add(wrapper);
                _oldBlankMap.put(contentIssuance.getId(), contentIssuance.getBlank());
            }
        }

        if (_diplomaIssuance.getId() != null)
        {
            setEditForm(true);
            _diplomaIssuance = DataAccessServices.dao().getNotNull(DiplomaIssuance.class, _diplomaIssuance.getId());
            _oldBlankMap.put(_diplomaIssuance.getId(), _diplomaIssuance.getBlank());
            _hasExcludeExtract = IUniBaseDao.instance.get().existsEntity(DipStuExcludeExtract.class, DipStuExcludeExtract.L_ISSUANCE, getDiplomaIssuance());
            _extractRelatedAppendixIds = new HashSet<>();
            for (DipContentIssuanceToDipExtractRelation rel: IUniBaseDao.instance.get().getList(DipContentIssuanceToDipExtractRelation.class, DipContentIssuanceToDipExtractRelation.contentIssuance().diplomaIssuance(), getDiplomaIssuance()))
                _extractRelatedAppendixIds.add(rel.getContentIssuance().getId());
        }
        else
            setEditForm(false);


        if (!isEditForm())
        {
            StringBuilder info = new StringBuilder();
            List<DiplomaIssuance> issuanceList = DataAccessServices.dao().getList(DiplomaIssuance.class, DiplomaIssuance.diplomaObject(), _diplomaObject, DiplomaIssuance.P_ISSUANCE_DATE);
            for (DiplomaIssuance issuance : issuanceList)
            {
                info.append(info.length() > 0 ? "<br/>" : "");
                info.append("рег. номер ").append(issuance.getRegistrationNumber());
                if (null != issuance.getBlankSeria() || null != issuance.getBlankNumber())
                {
                    info.append(", бланк ");
                    if (null != issuance.getBlankSeria())
                        info.append(issuance.getBlankSeria());
                    if (null != issuance.getBlankNumber())
                        info.append(" ").append(issuance.getBlankNumber());
                }
                info.append(" (").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(issuance.getIssuanceDate())).append(")");
            }
            _issuanceInformation = info.toString();

            String regNumber = DipSettingsManager.instance().dao().getDipIssuanceNextRegNumber(_diplomaObject);
            _diplomaIssuance.setRegistrationNumber(regNumber);
        }
    }

    public boolean isInsteadDiplomaDSDisabled()
    {
        if (!ISharedBaseDao.instance.get().existsEntity(DiplomaIssuance.class, DiplomaIssuance.L_DIPLOMA_OBJECT, getDiplomaObject()))
            return true;
        return (isEditForm() && ISharedBaseDao.instance.get().getCount(DiplomaIssuance.class, DiplomaIssuance.L_DIPLOMA_OBJECT, getDiplomaObject()) == 1);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(DiplomaIssuanceAddEdit.DIPLOMA_OBJECT, getDiplomaObject());
        dataSource.put(DiplomaIssuanceAddEdit.ISSUANCE_ID, getDiplomaIssuance().getId());
        dataSource.put(ContentRowDSHandler.DIPLOMA_CONTENT, getDiplomaObject().getContent());
        dataSource.put(DiplomaContentIssuanceListDSHandler.CONTENT_ISSUANCE_LIST, getDiplomaContentIssuanceList());
        dataSource.put(DiplomaIssuanceAddEdit.BIND_APPENDIX, DiplomaIssuanceAddEdit.DS_APPENDIX_BLANK.equals(dataSource.getName()));
        dataSource.put(DiplomaIssuanceAddEdit.BIND_BLANK, DiplomaIssuanceAddEdit.DS_APPENDIX_BLANK.equals(dataSource.getName()) ? getCurrentEditRow().<DiplomaContentIssuance>getWrapped().getBlank() : getDiplomaIssuance().getBlank());
    }

    public String getFormattedContentListNumber()
    {
        return "Приложение&nbsp;№" + ((DiplomaContentIssuance) (getCurrentRow().getWrapped())).getContentListNumber();
    }

    public String getFormattedIssuanceDate()
    {
        if (null != ((DiplomaContentIssuance) (getCurrentRow().getWrapped())).getDuplicateIssuanceDate())
            return DateFormatter.DEFAULT_DATE_FORMATTER.format(((DiplomaContentIssuance) (getCurrentRow().getWrapped())).getDuplicateIssuanceDate());
        else return "";
    }

    public List<DataWrapper> getDiplomaContentIssuanceList()
    {
        return _diplomaContentIssuanceList;
    }

    public void setDiplomaContentIssuanceList(List<DataWrapper> diplomaContentIssuanceList)
    {
        _diplomaContentIssuanceList = diplomaContentIssuanceList;
    }

    public DiplomaContentIssuance getDiplomaContentIssuance()
    {
        return getCurrentRow().getWrapped();
    }

    public DiplomaContentIssuance getCurrentDiplomaContentIssuance()
    {
        return getCurrentRow().getWrapped();
    }

    public DataWrapper getContentListNumber()
    {
        return _contentListNumber;
    }

    public void setContentListNumber(DataWrapper contentListNumber)
    {
        if (contentListNumber != null)
            getCurrentEditRow().<DiplomaContentIssuance>getWrapped().setContentListNumber(contentListNumber.getId().intValue());
        _contentListNumber = contentListNumber;
    }


    public DiplomaObject getDiplomaObject()
    {
        return _diplomaObject;
    }

    public void setDiplomaObject(DiplomaObject diplomaObject)
    {
        _diplomaObject = diplomaObject;
    }

    public DiplomaIssuance getDiplomaIssuance()
    {
        return _diplomaIssuance;
    }

    public void setDiplomaIssuance(DiplomaIssuance diplomaIssuance)
    {
        _diplomaIssuance = diplomaIssuance;
    }

    public boolean isEditForm()
    {
        return _editForm;
    }

    public void setEditForm(boolean editForm)
    {
        _editForm = editForm;
    }

    public IUploadFile getUploadDocument()
    {
        return _uploadDocument;
    }

    public void setUploadDocument(IUploadFile uploadDocument)
    {
        _uploadDocument = uploadDocument;
    }

    public String getIssuanceInformation()
    {
        if (_issuanceInformation.length() > 0)
            return "Для данного документа уже заполнены регистрационные данные: " + "<br/>" + _issuanceInformation;
        else
            return "";
    }

    private final Collection<Validator> _appSeriaNumberValidators = Arrays.<Validator>asList(new Required(), new SeriaNumberValidator());
    public Collection<Validator> getAppSeriaNumberValidators() { return _appSeriaNumberValidators; }

    public IUIDataSource getIssuanceApplicationDS()
    {
        return _issuanceApplicationDS;
    }

    public void setIssuanceApplicationDS(IUIDataSource issuanceApplicationDS)
    {
        _issuanceApplicationDS = issuanceApplicationDS;
    }

    public DataWrapper getCurrentEditRow()
    {
        return _currentEditRow;
    }

    public void setCurrentEditRow(DataWrapper currentEditRow)
    {
        _currentEditRow = currentEditRow;
    }

    public boolean isCurrentRowInEditMode()
    {
        DataWrapper editRow = getCurrentEditRow();
        if (null == editRow)
        {
            return false;
        }

        if (null == getCurrentRow())
        {
            return false;
        }

        return _diplomaContentIssuanceList.indexOf(editRow) == _diplomaContentIssuanceList.indexOf(getCurrentRow());
    }

    public void onClickAddApplication()
    {
        setContentListNumber(null);
        DataWrapper contentIssuanceWrapper = new DataWrapper(new DiplomaContentIssuance());
        contentIssuanceWrapper.setId(getNextId());
        _diplomaContentIssuanceList.add(contentIssuanceWrapper);
        setCurrentEditRow(contentIssuanceWrapper);
        editRowCreated = true;
    }


    public void onClickEditRow()
    {
        setCurrentEditRow(this.<DataWrapper>getListenerParameter());
        final DiplomaContentIssuance wrapped = getCurrentEditRow().getWrapped();

        setContentListNumber(new DataWrapper((long) wrapped.getContentListNumber(), ""));

        beforeEditBlankNumber = wrapped.getBlankNumber();
        beforeEditBlankSeria = wrapped.getBlankSeria();
        beforeEditSheetNumber = wrapped.getContentListNumber();
        beforeEditDuplicate = wrapped.isDuplicate();
        beforeEditDuplicateIssDate = wrapped.getDuplicateIssuanceDate();
        beforeEditDuplicateRegNum = wrapped.getDuplicateRegustrationNumber();
        editRowCreated = false;
    }

    public void onClickDelete()
    {
        DataWrapper contentWrapper = getListenerParameter();
        Long deletingId = contentWrapper.getId();
        for (DataWrapper wrapper : _diplomaContentIssuanceList)
        {
            if (wrapper.getId().equals(deletingId))
            {
                _diplomaContentIssuanceList.remove(wrapper);
                break;
            }
        }
        setCurrentEditRow(null);
    }

    public void onClickCancelEdit()
    {
        if (editRowCreated)
            _diplomaContentIssuanceList.remove(getCurrentEditRow());
        else
        {
            final DiplomaContentIssuance wrapped = getCurrentEditRow().getWrapped();
            wrapped.setBlankNumber(beforeEditBlankNumber);
            wrapped.setBlankSeria(beforeEditBlankSeria);
            wrapped.setContentListNumber(beforeEditSheetNumber);
            wrapped.setDuplicate(beforeEditDuplicate);
            wrapped.setDuplicateIssuanceDate(beforeEditDuplicateIssDate);
            wrapped.setDuplicateRegustrationNumber(beforeEditDuplicateRegNum);
        }
        setCurrentEditRow(null);
    }


    public void onClickSaveRow()
    {
        getCurrentEditRow().<DiplomaContentIssuance>getWrapped().setContentListNumber(_contentListNumber.getId().intValue());
        setCurrentEditRow(null);
    }

    private void setDataBaseFileInDipIssuance()
    {
        if (getUploadDocument() != null)
        {
            try
            {
                DatabaseFile databaseFile = getDiplomaIssuance().getScanCopy() != null ? getDiplomaIssuance().getScanCopy() : new DatabaseFile();
                byte[] content = IOUtils.toByteArray(getUploadDocument().getStream());
                databaseFile.setContent(content);
                databaseFile.setFilename(getUploadDocument().getFileName());
                databaseFile.setContentType(CommonBaseUtil.getContentType(getUploadDocument()));
                if (null == getDiplomaIssuance().getScanCopy())
                {
                    _diplomaIssuance.setScanCopy(databaseFile);
                }
            }
            catch (Throwable e)
            {
                e.printStackTrace();
            }
        }
    }

    public void onClickApply()
    {
        validateData();
        _diplomaIssuance.setDiplomaObject(getDiplomaObject());
        List<DiplomaContentIssuance> issuanceList = new ArrayList<>();
        for (DataWrapper wrapper : _diplomaContentIssuanceList)
        {
            issuanceList.add(wrapper.getWrapped());
        }
        setDataBaseFileInDipIssuance();
        DiplomaIssuanceManager.instance().dao().saveDiplomaIssuance(_diplomaIssuance, issuanceList, getOldBlankMap());
        deactivate();
    }

    private void validateData()
    {
        if (switchedToNotFreeBlank(getDiplomaIssuance().getBlank(), _oldBlankMap.get(getDiplomaIssuance().getId())))
			throw new ApplicationException("Выбранный бланк должен быть в состоянии «Свободен».");
		if (contentIssuanceDuplicatesExist())
			throw new ApplicationException("Невозможно сохранить данные. Выберите разные бланки.");
    }

	private boolean switchedToNotFreeBlank(DipDiplomaBlank blank, DipDiplomaBlank oldBlank)
	{
		return !(blank == null || blank.equals(oldBlank) || blank.getBlankState().getCode().equals(DipBlankStateCodes.FREE));
	}

	private boolean contentIssuanceDuplicatesExist()
	{
		Set<Long> usedAppendixBlankIds = new HashSet<>();
		for (DataWrapper wrapper : _diplomaContentIssuanceList)
		{
			DipDiplomaBlank blank = wrapper.<DiplomaContentIssuance>getWrapped().getBlank();
			if (blank == null)
				continue;
			if (usedAppendixBlankIds.contains(blank.getId()))
				return true;
			usedAppendixBlankIds.add(blank.getId());
		}
		return false;
	}

    private boolean _hasExcludeExtract;
    public boolean isHasExcludeExtract() { return _hasExcludeExtract; }

    public boolean isDiplomaBlankSeriaAndNumberDisabled()
    {
        return isHasExcludeExtract() || getDiplomaIssuance().getBlank() != null;
    }

    public void onChangeDiplomaBlank()
    {
        if (getDiplomaIssuance().getBlank() != null)
        {
            getDiplomaIssuance().setBlankSeria(getDiplomaIssuance().getBlank().getSeria());
            getDiplomaIssuance().setBlankNumber(getDiplomaIssuance().getBlank().getNumber());
        }
    }

    public void onChangeAppendixBlank()
    {
        if (getCurrentDiplomaContentIssuance().getBlank() != null)
        {
            getCurrentDiplomaContentIssuance().setBlankSeria(getCurrentDiplomaContentIssuance().getBlank().getSeria());
            getCurrentDiplomaContentIssuance().setBlankNumber(getCurrentDiplomaContentIssuance().getBlank().getNumber());
        }
    }

    private DataWrapper _currentRow;
    public DataWrapper getCurrentRow() { return _currentRow; }
    public void setCurrentRow(DataWrapper currentRow) { _currentRow = currentRow; }

    private Collection<Long> _extractRelatedAppendixIds = new HashSet<>();
    public boolean isCurrentRowEditDeleteDisabled()
    {
        return getCurrentEditRow() != null || _extractRelatedAppendixIds.contains(getCurrentDiplomaContentIssuance().getId());
    }

    private Map<Long, DipDiplomaBlank> _oldBlankMap;
    public Map<Long, DipDiplomaBlank> getOldBlankMap() { return _oldBlankMap; }
}
