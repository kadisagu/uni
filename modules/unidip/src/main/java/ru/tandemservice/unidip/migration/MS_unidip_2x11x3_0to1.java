package ru.tandemservice.unidip.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLUpdateQuery;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unidip_2x11x3_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.11.3")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность diplomaObject

		// создано обязательное свойство student
		{
			// создать колонку
			tool.createColumn("dip_object_t", new DBColumn("student_id", DBType.LONG));

			// Заполнить значением колонки Студент из УП студента
			SQLUpdateQuery studentInitQuery = new SQLUpdateQuery("dip_object_t", "diploma");
			studentInitQuery.getUpdatedTableFrom().innerJoin(SQLFrom.table("epp_student_eduplanversion_t", "studentEpv"), "diploma.studentepv_id = studentEpv.id");
			studentInitQuery.set("student_id", "studentEpv.student_id");

			tool.executeUpdate(tool.getDialect().getSQLTranslator().toSql(studentInitQuery));

			// сделать колонку NOT NULL
			tool.setColumnNullable("dip_object_t", "student_id", false);

		}


    }
}