/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unidip.print.bo.Default;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

import ru.tandemservice.unidip.base.bo.DipDocument.IDiplomaTypeManager;
import ru.tandemservice.unidip.base.bo.DipDocument.logic.IDiplomaPrintDao;
import ru.tandemservice.unidip.print.bo.Default.logic.DefaultDao;

/**
 * @author iolshvang
 * @since 29.08.11 16:32
 */
@Configuration
public class DefaultManager extends BusinessObjectManager implements IDiplomaTypeManager
{
    public static DefaultManager instance()
    {
        return instance(DefaultManager.class);
    }

    @Bean
    @Override
    public IDiplomaPrintDao printDao()
    {
        return new DefaultDao();
    }
}
