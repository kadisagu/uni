/* $Id$ */
package ru.tandemservice.unidip.base.bo.DipDocument.logic;

import org.tandemframework.core.tool.tree.IHierarchyItem;
import org.tandemframework.core.tool.tree.PlaneTree;
import org.tandemframework.core.view.list.IViewSelectValueStyle;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.tapsupport.component.selection.*;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unidip.base.entity.catalog.DipDocumentType;

import java.util.Collection;
import java.util.List;

/**
 * @author Andrey Avetisov
 * @since 23.06.2014
 */
public class DipDocumentTypeSelectModel extends CommonBaseSelectModel implements ISingleSelectModel
{
    private final boolean _enableOnlyFormingAllowed;
    private final Collection<DipDocumentType> _filterList;

    public DipDocumentTypeSelectModel(boolean enableOnlyFormingAllowed)
    {
        this(null, enableOnlyFormingAllowed);
    }

    public DipDocumentTypeSelectModel(Collection<DipDocumentType> filterList, boolean enableOnlyFormingAllowed)
    {
        _filterList = filterList;
        _enableOnlyFormingAllowed = enableOnlyFormingAllowed;
    }

    @Override
    public int getLevel(Object value)
    {
        int level = 0;
        DipDocumentType item = (DipDocumentType) value;
        while (item.getParent() != null)
        {
            item = item.getParent();
            level++;
        }
        return level;

    }

    protected DQLSelectBuilder createDQL(String filter, Object o)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DipDocumentType.class, "t");

        FilterUtils.applySimpleLikeFilter(builder, "t", DipDocumentType.title(), filter);
        FilterUtils.applySelectFilter(builder, "t", DipDocumentType.id(), o);
        FilterUtils.applySelectFilter(builder, "t", DipDocumentType.id(), _filterList);

        return builder;
    }

    @Override
    protected IListResultBuilder createBuilder(String filter, Object o)
    {
        List<IHierarchyItem> docTypeList = DataAccessServices.dao().getList(createDQL(filter, o));

        return new SimpleListResultBuilder<>(new PlaneTree(docTypeList).getFlatTreeObjectsHierarchicalSorted(DipDocumentType.P_TITLE));
    }

    @Override
    public Object getValue(Object primaryKey)
    {
        ListResult list = findValues(null);
        List objects = list.getObjects();
        if (objects.size() != list.getMaxCount())
            throw new RuntimeException("HBaseSelectModel need all elements!");

        for (Object obj : objects)
            if (getPrimaryKey(obj).equals(primaryKey))
                return obj;

        return null;
    }

    @Override
    public IViewSelectValueStyle getValueStyle(Object value)
    {
        DipDocumentType type = (DipDocumentType) value;
        if (type.isFormingDocAllowed() || !_enableOnlyFormingAllowed)
            return super.getValueStyle(value);

        // Задизаблены рутовые и неактивные типы
        DefaultSelectValueStyle valueStyle = new DefaultSelectValueStyle();
        valueStyle.setDisabled(true);
        valueStyle.setRowStyle("background-color:#D2D2D2");
        return valueStyle;
    }
}
