package ru.tandemservice.unidip.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unidip_2x6x0_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.15"),
                        new ScriptDependency("org.tandemframework.shared", "1.6.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность diplomaContent

        // создано обязательное свойство withSuccess
        {
            // создать колонку
            if (!tool.columnExists("diploma_content", "withsuccess_p"))
                tool.createColumn("diploma_content", new DBColumn("withsuccess_p", DBType.BOOLEAN));

            // задать значение по умолчанию
            java.lang.Boolean defaultWithSuccess = false;        // значение по умолчанию
            tool.executeUpdate("update diploma_content set withsuccess_p=? where withsuccess_p is null", defaultWithSuccess);

            // сделать колонку NOT NULL
            tool.setColumnNullable("diploma_content", "withsuccess_p", false);

        }


    }
}