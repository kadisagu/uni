/* $Id$ */
package ru.tandemservice.unidip.base.bo.DiplomaIssuance;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unidip.base.bo.DipDocument.logic.DipDocumentDao;
import ru.tandemservice.unidip.base.bo.DipDocument.logic.IDipDocumentDao;

/**
 * @author Andrey Avetisov
 * @since 25.06.2014
 */
@Configuration
public class DiplomaIssuanceManager extends BusinessObjectManager
{
    public static DiplomaIssuanceManager instance()
    {
        return instance(DiplomaIssuanceManager.class);
    }

    @Bean
    public IDipDocumentDao dao()
    {
        return new DipDocumentDao();
    }
}
