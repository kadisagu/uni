/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unidip.base.bo.DipDocument.logic;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.dql.*;
import ru.tandemservice.uni.base.bo.UniEduProgram.UniEduProgramManager;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentIssuance;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaObject;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author iolshvang
 * @since 13.07.11 20:41
 */
public class DipDocumentOrgUnitStudentDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String ORG_UNIT_ID = "orgUnitId";
    public static final String DIPLOMA_COLUMN = "diplomaColumn";

    public static final String LAST_NAME = "lastName";
    public static final String STUDENT_STATUS = "studentStatus";
    public static final String COURSE = "course";
    public static final String GROUP = "group";

    public static final String DOCUMENT_TYPE = "documentType";
    public static final String DIPLOMA_CONTENT_ISSUANCE = "diplomaContentIssuance";

    public static final String ISSUANCE_EMPTY = "issuanceEmpty";
    public static final String ISSUE_DATE_FROM = "issueDateFrom";
    public static final String ISSUE_DATE_TO = "issueDateTo";
    public static final String BLANK_SERIA = "seria";
    public static final String BLANK_NUMBER = "number";

    public static final String STATE_COMMISSION_DATE_EMPTY = "stateCommissionDateEmpty";
    public static final String STATE_COMMISSION_DATE_FROM = "stateCommissionDateFrom";
    public static final String STATE_COMMISSION_DATE_TO = "stateCommissionDateTo";
    public static final String STATE_COMMISSION_PROTOCOL_NUMBER = "stateCommissionProtocolNumber";
    public static final String STATE_COMMISSION_CHAIR_FIO = "stateCommissionChairFio";

    public DipDocumentOrgUnitStudentDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        FiltersInfo info = new FiltersInfo(context);

        DSOutput output = DQLSelectOutputBuilder.get(input, getStudentDQL(input, info), context.getSession()).pageable(true).build();

        Map<Long, List<DiplomaObject>> dipsByStudentMap = getDiplomas4Students(info, output.getRecordIds(), context);
        DataWrapper.wrap(output)
                .forEach(wrapper ->
                         {
                             List<DiplomaObject> diplomas = dipsByStudentMap.get(wrapper.getId());
                             if (diplomas == null) return;

                             wrapper.setProperty(DIPLOMA_COLUMN, diplomas.stream()
                                     .sorted((o1, o2) ->
                                             {
                                                 Date date1 = o1.getStateCommissionDate();
                                                 Date date2 = o2.getStateCommissionDate();
                                                 if (date1 != null || date2 != null)
                                                 {
                                                     if (date1 == null)
                                                         return -1;
                                                     if (date2 == null)
                                                         return 1;
                                                     return date1.compareTo(date2);
                                                 }
                                                 return 0;
                                             })
                                     .filter(Objects::nonNull).collect(Collectors.toList()));
                         });

        return output;
    }

    public static DQLSelectBuilder getStudentDQL(DSInput input, FiltersInfo info)
    {
        String alias = "st";
        DQLOrderDescriptionRegistry orderRegistry = new DQLOrderDescriptionRegistry(Student.class, alias);
        orderRegistry.setOrders(Student.studentFullFioWithBirthYearGroupAndStatus(),
                                new OrderDescription(Student.person().identityCard().lastName().s()),
                                new OrderDescription(Student.person().identityCard().firstName().s()),
                                new OrderDescription(Student.person().identityCard().middleName().s()));

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(Student.class, alias)
                .column(property(alias));
        orderRegistry.applyOrder(builder, input.getEntityOrder());
        filterStudents(builder, alias, info);

        IDQLExpression withoutDipExp = notExists(DiplomaObject.class, DiplomaObject.student().s(), property(alias));
        DQLSelectBuilder dipDql = new DQLSelectBuilder()
                .fromEntity(DiplomaObject.class, "dip")
                .where(eq(property("dip", DiplomaObject.student()), property(alias)));
        filterDiplomaObjects(dipDql, "dip", info);

        if (info.withoutDip && info.hasDipObjectOptions)
            builder.where(or(exists(dipDql.buildQuery()), withoutDipExp));
        else if (info.withoutDip)
            builder.where(withoutDipExp);
        else if (info.hasDipObjectOptions)
            builder.where(exists(dipDql.buildQuery()));

        return builder;
    }

    public static Map<Long, List<DiplomaObject>> getDiplomas4Students(FiltersInfo info, List<Long> students, ExecutionContext context)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(DiplomaObject.class, "dip");

        filterDiplomaObjects(builder, "dip", info);

        builder.joinPath(DQLJoinType.inner, DiplomaObject.student().fromAlias("dip"), "st");
        filterStudents(builder, "st", info);
        builder.where(in(property("st"), students));

        builder.column(property("st", Student.id()))
                .column(property("dip"));

        return builder.createStatement(context.getSession()).<Object[]>list()
                .stream()
                .collect(Collectors.groupingBy(raw -> (Long) raw[0], Collectors.mapping(raw -> (DiplomaObject) raw[1], Collectors.toList())));
    }

    public static void filterStudents(DQLSelectBuilder builder, String alias, FiltersInfo info)
    {
        String eouAlias = "eou";
        builder.where(eq(property(alias, Student.archival()), value(Boolean.FALSE)))
                .joinPath(DQLJoinType.inner, Student.educationOrgUnit().fromAlias(alias), eouAlias)
                .where(or(eq(property(eouAlias, EducationOrgUnit.educationLevelHighSchool().orgUnit().id()), value(info.orgUnitId)),
                          eq(property(eouAlias, EducationOrgUnit.formativeOrgUnit().id()), value(info.orgUnitId))));

        if (info.addon != null) info.addon.applyFilters(builder, eouAlias);

        FilterUtils.applySimpleLikeFilter(builder, alias, Student.person().identityCard().lastName().s(), info.lastName);
        FilterUtils.applySelectFilter(builder, Student.status().fromAlias(alias), info.studentStatus);
        FilterUtils.applySelectFilter(builder, Student.course().fromAlias(alias), info.course);
        FilterUtils.applySelectFilter(builder, Student.group().fromAlias(alias), info.group);
    }

    public static void filterDiplomaObjects(DQLSelectBuilder builder, String alias, FiltersInfo info)
    {
        if (!info.hasDipObjectOptions) return;

        FilterUtils.applySelectFilter(builder, alias, DiplomaObject.content().type().id().s(), info.docTypes);

        if (info.stateCommissionDateEmpty)
            builder.where(isNull(property(alias, DiplomaObject.stateCommissionDate())));
        else
            FilterUtils.applyBetweenFilter(builder, alias, DiplomaObject.stateCommissionDate().s(), info.stateCommissionDateFrom, info.stateCommissionDateTo);

        if (info.stateCommissionChairFio.isEnabled())
            builder.where(stringFilter(property(alias, DiplomaObject.stateCommissionChairFio()), info.stateCommissionChairFio.getValue()));

        if (info.stateCommissionProtocolNumber.isEnabled())
            builder.where(stringFilter(property(alias, DiplomaObject.stateCommissionProtocolNumber()), info.stateCommissionProtocolNumber.getValue()));

        if (info.issuanceEmpty)
            builder.where(notExists(DiplomaIssuance.class, DiplomaIssuance.diplomaObject().s(), property(alias)));
        else if (info.hasIssuanceOptions)
        {
            String issAlias = "iss";
            List<IDQLExpression> expressions = getExpression4FilterDiplomaIssuances(issAlias, info);
            expressions.add(eq(property(issAlias, DiplomaIssuance.diplomaObject()), property(alias)));
            builder.where(existsByExpr(DiplomaIssuance.class, issAlias, and(expressions.toArray(new IDQLExpression[expressions.size()]))));
        }
    }

    public static List<IDQLExpression> getExpression4FilterDiplomaIssuances(String alias, FiltersInfo info)
    {
        List<IDQLExpression> expressions = new ArrayList<>();

        if (info.diplomaContentIssuance != null)
        {
            IDQLExpression issContentExp;
            if (info.diplomaContentIssuance)
                issContentExp = exists(DiplomaContentIssuance.class, DiplomaContentIssuance.diplomaIssuance().s(), property(alias));
            else
                issContentExp = notExists(DiplomaContentIssuance.class, DiplomaContentIssuance.diplomaIssuance().s(), property(alias));
            expressions.add(issContentExp);
        }

        if (info.issueDateFrom != null)
            expressions.add(ge(property(alias, DiplomaIssuance.issuanceDate()), value(CoreDateUtils.getDayFirstTimeMoment(info.issueDateFrom), PropertyType.DATE)));

        if (info.issueDateTo != null)
            expressions.add(lt(property(alias, DiplomaIssuance.issuanceDate()), value(CoreDateUtils.getNextDayFirstTimeMoment(info.issueDateTo, 1), PropertyType.DATE)));

        if (info.issueSeria.isEnabled())
            expressions.add(stringFilter(property(alias, DiplomaIssuance.blankSeria()), info.issueSeria.getValue()));

        if (info.issueNumber.isEnabled())
            expressions.add(stringFilter(property(alias, DiplomaIssuance.blankNumber()), info.issueNumber.getValue()));

        return expressions;
    }

    public static class Param<T>
    {
        boolean _enabled;
        T _value;

        public Param(boolean enabled, T value)
        {
            _enabled = enabled;
            _value = value;
        }

        public boolean isEnabled()
        {
            return _enabled;
        }

        public void setEnabled(boolean enabled)
        {
            _enabled = enabled;
        }

        public T getValue()
        {
            return _value;
        }

        public void setValue(T value)
        {
            _value = value;
        }
    }

    public static IDQLExpression stringFilter(IDQLExpression property, String param)
    {
        param = CoreStringUtils.escapeLike(param);
        if (!param.equals("%")) return likeUpper(property, value(param));
        else return isNull(property);
    }

    public static class FiltersInfo
    {
        protected Long orgUnitId;

        protected String lastName;
        protected List<StudentStatus> studentStatus;
        protected UniEduProgramEducationOrgUnitAddon addon;
        protected Course course;
        protected Group group;

        protected List<Long> docTypes;
        protected boolean withoutDip = false;

        protected Boolean stateCommissionDateEmpty;
        protected Date stateCommissionDateFrom;
        protected Date stateCommissionDateTo;
        protected Param<String> stateCommissionChairFio;
        protected Param<String> stateCommissionProtocolNumber;

        protected Boolean issuanceEmpty;
        protected Date issueDateFrom;
        protected Date issueDateTo;
        protected Param<String> issueSeria;
        protected Param<String> issueNumber;

        protected Boolean diplomaContentIssuance;

        protected boolean hasDipObjectOptions = false;
        protected boolean hasIssuanceOptions = false;

        public FiltersInfo(ExecutionContext context)
        {
            orgUnitId = context.get(ORG_UNIT_ID);

            lastName = context.get(LAST_NAME);
            studentStatus = context.get(STUDENT_STATUS);
            addon = context.get(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME);
            course = context.get(COURSE);
            group = context.get(GROUP);

            List<DataWrapper> temp = context.get(DOCUMENT_TYPE);
            docTypes = CollectionUtils.isEmpty(temp) ? null : temp.stream().map(IEntity::getId).collect(Collectors.toList());
            if (CollectionUtils.isNotEmpty(docTypes))
            {
                withoutDip = docTypes.contains(0L);
                if (withoutDip) docTypes.remove(0L);
            }

            if (CollectionUtils.isEmpty(docTypes) && withoutDip) return;

            stateCommissionDateEmpty = context.get(STATE_COMMISSION_DATE_EMPTY);
            if (!stateCommissionDateEmpty)
            {
                stateCommissionDateFrom = context.get(STATE_COMMISSION_DATE_FROM);
                stateCommissionDateTo = context.get(STATE_COMMISSION_DATE_TO);
            }

            stateCommissionChairFio = context.get(STATE_COMMISSION_CHAIR_FIO);
            stateCommissionProtocolNumber = context.get(STATE_COMMISSION_PROTOCOL_NUMBER);

            issuanceEmpty = context.get(ISSUANCE_EMPTY);

            issueDateFrom = context.get(ISSUE_DATE_FROM);
            issueDateTo = context.get(ISSUE_DATE_TO);
            issueSeria = context.get(BLANK_SERIA);
            issueNumber = context.get(BLANK_NUMBER);

            diplomaContentIssuance = context.get(DIPLOMA_CONTENT_ISSUANCE);

            hasIssuanceOptions = issuanceEmpty || issueDateFrom != null || issueDateTo != null || issueSeria.isEnabled() || issueNumber.isEnabled()
                    || diplomaContentIssuance != null;

            hasDipObjectOptions = hasIssuanceOptions || CollectionUtils.isNotEmpty(docTypes)
                    || stateCommissionDateEmpty || stateCommissionDateFrom != null || stateCommissionDateTo != null
                    || stateCommissionChairFio.isEnabled() || stateCommissionProtocolNumber.isEnabled();
        }
    }
}