/* $Id$ */
package ru.tandemservice.unidip.settings.bo.DipSettings;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unidip.settings.bo.DipSettings.logic.DipSettingsDao;
import ru.tandemservice.unidip.settings.bo.DipSettings.logic.IDipSettingsDao;

/**
 * @author Alexey Lopatin
 * @since 26.05.2015
 */
@Configuration
public class DipSettingsManager extends BusinessObjectManager
{
    public static DipSettingsManager instance()
    {
        return instance(DipSettingsManager.class);
    }

    @Bean
    public IDipSettingsDao dao()
    {
        return new DipSettingsDao();
    }
}