/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unidip.base.bo.DipDocument.ui.GlobalList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.NewLineFormatter;
import org.tandemframework.core.view.formatter.StringLimitFormatter;
import org.tandemframework.core.view.list.column.DefaultPublisherLinkResolver;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.DQLSubselectType;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unidip.base.bo.DipDocument.logic.DipDocumentStudentDSHandler;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaObject;
import ru.tandemservice.unidip.base.entity.order.DipStuExcludeExtract;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author iolshvang
 * @since 30.09.11 18:38
 */
@Configuration
public class DipDocumentGlobalList extends BusinessComponentManager
{
    public static final String DIP_DOCUMENT_DS = "dipDocumentDS";
    public static final String ORG_UNIT_DS = "orgUnitDS";
    public static final String PROGRAM_SUBJECT_DS = "programSubjectDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(this.searchListDS(DIP_DOCUMENT_DS, dipDocumentDSColumns(), dipDocumentDSHandler()).create())
                .addDataSource(CommonManager.instance().yesNoDSConfig())
                .addDataSource(selectDS(ORG_UNIT_DS, orgUnitDSHandler()).addColumn(OrgUnit.P_FULL_TITLE))
                .addDataSource(selectDS(PROGRAM_SUBJECT_DS, programSubjectDSHandler()).addColumn(EduProgramSubject.titleWithCode().s()))
                .create();
    }

    @Bean
    public ColumnListExtPoint dipDocumentDSColumns()
    {
        return columnListExtPointBuilder(DIP_DOCUMENT_DS)
                .addColumn(publisherColumn("student", DiplomaObject.student().studentFullFioWithBirthYearGroupAndStatus()).publisherLinkResolver(new DefaultPublisherLinkResolver()
                {
                    @Override
                    public Object getParameters(final IEntity entity)
                    {
	                    DiplomaObject diplomaObject = ((DataWrapper) entity).getWrapped();
                        return new ParametersMap()
                                .add(PublisherActivator.PUBLISHER_ID_KEY, diplomaObject.getStudent().getId())
                                .add("selectedStudentTab", "studentDipTab");

                    }
                }).required(true).order())
                .addColumn(publisherColumn("type", DiplomaObject.content().type().titleWithParent()).required(true).order())
                .addColumn(booleanColumn("withSuccess", DiplomaObject.content().withSuccess()).required(true))
                .addColumn(textColumn(DipDocumentStudentDSHandler.BLANK_SERIA_AND_NUMBER, DipDocumentStudentDSHandler.BLANK_SERIA_AND_NUMBER).formatter(NewLineFormatter.NOBR_IN_LINES).required(true).order())
                .addColumn(textColumn(DipDocumentStudentDSHandler.REGISTRATION_NUMBER, DipDocumentStudentDSHandler.REGISTRATION_NUMBER).formatter(NewLineFormatter.NOBR_IN_LINES).required(true).order())
                .addColumn(textColumn(DipDocumentStudentDSHandler.ISSUANCE_DATE, DipDocumentStudentDSHandler.ISSUANCE_DATE).formatter(NewLineFormatter.NOBR_IN_LINES).required(true).order())
                .addColumn(textColumn(DipDocumentStudentDSHandler.APPLICATION_TO_DIPLOMA, DipDocumentStudentDSHandler.APPLICATION_TO_DIPLOMA).formatter(NewLineFormatter.NOBR_IN_LINES).required(true))
                .addColumn(textColumn("orgUnit", DiplomaObject.student().educationOrgUnit().educationLevelHighSchool().orgUnit().title()))
                .addColumn(textColumn("programSubject", DiplomaObject.content().programSubject().titleWithCode()))
                .addColumn(publisherColumn(DipDocumentStudentDSHandler.EXCLUDE_ORDER_TITLE, DipDocumentStudentDSHandler.EXCLUDE_ORDER_TITLE)
                                   .publisherLinkResolver(new DefaultPublisherLinkResolver()
                                   {
                                       @Override
                                       public Object getParameters(IEntity entity)
                                       {
                                           DipStuExcludeExtract excludeOrderForDiploma = (DipStuExcludeExtract) entity.getProperty(DipDocumentStudentDSHandler.EXCLUDE_ORDER);
                                           return excludeOrderForDiploma.getParagraph().getOrder().getId();
                                       }
                                   }))
                .addColumn(textColumn("comment", DiplomaObject.comment()).formatter(StringLimitFormatter.NON_BROKEN_WORDS_WITH_NEW_LINE_SUPPORT))
                .addColumn(actionColumn("edit", CommonDefines.ICON_EDIT, EDIT_LISTENER).permissionKey("editDipDocumentFromList").disabled(DipDocumentStudentDSHandler.EDIT_DISABLED).required(true))
                .addColumn(actionColumn("addIssuance", CommonDefines.ICON_ADD, "onAddIssunce").permissionKey("addDipDocumentIssuance"))
                .addColumn(actionColumn("print", CommonDefines.ICON_PRINT, "onPrintFromGlobalList").permissionKey("printDipDocument").disabled(DipDocumentStudentDSHandler.PRINT_DISABLED).required(true))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> dipDocumentDSHandler()
    {
        return new DipDocumentStudentDSHandler(getName(), true);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> orgUnitDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), OrgUnit.class)
                .filter(OrgUnit.title())
                .order(OrgUnit.title());
    }

    @Bean IBusinessHandler<DSInput, DSOutput> programSubjectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSubject.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context) {
                super.applyWhereConditions(alias, dql, context);

                DQLSelectBuilder fromDiploma = new DQLSelectBuilder().fromEntity(DiplomaObject.class, "do")
                        .column(property(DiplomaObject.content().programSubject().fromAlias("do")));

                dql.where(eqSubquery(property(alias), DQLSubselectType.any, fromDiploma.buildQuery()));
            }
        }
                .filter(EduProgramSubject.subjectCode())
                .filter(EduProgramSubject.title())
                .order(EduProgramSubject.subjectCode())
                .order(EduProgramSubject.title());
    }
}
