/* $Id$ */
package ru.tandemservice.unidip.base.bo.DipDocument.ui.DiplomaObjectDataAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unidip.base.bo.DipDocument.DipDocumentManager;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaObject;
import ru.tandemservice.unidip.settings.entity.DipFormingRegNumberAlgorithm;

import java.util.Date;
import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 21.05.2015
 */
@Input({
        @Bind(key = DipDocumentDiplomaObjectDataAddUI.PARAM_STUDENT_IDS, binding = "studentIds", required = true),
        @Bind(key = DipDocumentDiplomaObjectDataAddUI.PARAM_ONLY_TITLE, binding = "onlyTitle", required = true)
})
public class DipDocumentDiplomaObjectDataAddUI extends UIPresenter
{
    public static final String PARAM_STUDENT_IDS = "studentIds";
    public static final String PARAM_ONLY_TITLE = "onlyTitle";

    private List<Long> _studentIds;
    private DiplomaObject _diplomaObject = new DiplomaObject();
    private boolean _withSuccess;
    private Date _issuanceDate;
    private boolean _onlyTitle;
	private boolean _useExistingDiplomas = true;

    public void onClickContinue()
    {
	    if (_onlyTitle)
		    DipDocumentManager.instance().dao().createDiplomaTitles(_studentIds, _diplomaObject, _withSuccess, _issuanceDate, _useExistingDiplomas);
	    else
		    DipDocumentManager.instance().dao().createDiplomaObject(_studentIds, _diplomaObject, _withSuccess, _issuanceDate, ContextLocal.getInfoCollector());
        deactivate();
    }

    public boolean isHasAlgorithmGenRegNumber()
    {
        DipFormingRegNumberAlgorithm algorithmRegNumber = DataAccessServices.dao().get(DipFormingRegNumberAlgorithm.class, DipFormingRegNumberAlgorithm.current(), Boolean.TRUE);
        return algorithmRegNumber != null;
    }

    public DiplomaObject getDiplomaObject()
    {
        return _diplomaObject;
    }

    public void setDiplomaObject(DiplomaObject diplomaObject)
    {
        _diplomaObject = diplomaObject;
    }

    public List<Long> getStudentIds()
    {
        return _studentIds;
    }

    public void setStudentIds(List<Long> studentIds)
    {
        _studentIds = studentIds;
    }

    public boolean isWithSuccess()
    {
        return _withSuccess;
    }

    public void setWithSuccess(boolean withSuccess)
    {
        _withSuccess = withSuccess;
    }

    public Date getIssuanceDate()
    {
        return _issuanceDate;
    }

    public void setIssuanceDate(Date issuanceDate)
    {
        _issuanceDate = issuanceDate;
    }

    public boolean isOnlyTitle()
    {
        return _onlyTitle;
    }

    public void setOnlyTitle(boolean onlyTitle)
    {
        _onlyTitle = onlyTitle;
    }

	public boolean isUseExistingDiplomas()
	{
		return _useExistingDiplomas;
	}

	public void setUseExistingDiplomas(boolean useExistingDiplomas)
	{
		_useExistingDiplomas = useExistingDiplomas;
	}
}
