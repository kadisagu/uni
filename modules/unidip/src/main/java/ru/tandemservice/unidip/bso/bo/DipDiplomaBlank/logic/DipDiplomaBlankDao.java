/* $Id:$ */
package ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.logic;

import com.google.common.base.Preconditions;
import com.google.common.collect.Iterables;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.hibsupport.EntityIDGenerator;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unidip.base.entity.catalog.DipDocumentType;
import ru.tandemservice.unidip.base.entity.catalog.codes.DipDocumentTypeCodes;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentIssuance;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaObject;
import ru.tandemservice.unidip.base.entity.order.DipContentIssuanceToDipExtractRelation;
import ru.tandemservice.unidip.base.entity.order.DipStuExcludeExtract;
import ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.ui.Add.DipBlankRowWrapper;
import ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank;
import ru.tandemservice.unidip.bso.entity.catalog.DipBlankDisposalReason;
import ru.tandemservice.unidip.bso.entity.catalog.DipBlankState;
import ru.tandemservice.unidip.bso.entity.catalog.codes.DipBlankStateCodes;
import ru.tandemservice.unidip.bso.entity.catalog.codes.DipBlankTypeCodes;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 30.12.2014
 */
public class DipDiplomaBlankDao extends UniBaseDao implements IDipDiplomaBlankDao
{
    @Override
    public Integer saveDiplomaBlanks(@NotNull Collection<DipBlankRowWrapper> rowWrappers)
    {
        this.lock("dipDiplomaBlank_addBlanks");

        Preconditions.checkNotNull(rowWrappers);
        if (rowWrappers.isEmpty())
            return 0;

        checkBlankSeriaAndNumberFormat(rowWrappers);
        checkBlankNumbersForIntersection(rowWrappers);
        checkCountOfBlanks(rowWrappers);

        ErrorCollector errorCollector = ContextLocal.getErrorCollector();
        final Session session = this.getSession();

        IDQLStatement countStatement = new DQLSelectBuilder()
            .fromEntity(DipDiplomaBlank.class, "b")
            .where(eq(property("b", DipDiplomaBlank.blankType().id()), parameter("type", PropertyType.LONG)))
            .where(eq(property("b", DipDiplomaBlank.seria()), parameter("seria", PropertyType.STRING)))
            .where(ge(property("b", DipDiplomaBlank.number()), parameter("firstNumber", PropertyType.STRING)))
            .where(le(property("b", DipDiplomaBlank.number()), parameter("lastNumber", PropertyType.STRING)))
            .createCountStatement(new DQLExecutionContext(session));

        for (DipBlankRowWrapper rowWrapper : rowWrappers)
        {
            countStatement.setLong("type", rowWrapper.getType().getId());
            countStatement.setString("seria", rowWrapper.getSeria());
            countStatement.setString("firstNumber", rowWrapper.getStringFirstNumber());
            countStatement.setString("lastNumber", rowWrapper.getStringLastNumber());

            if (countStatement.<Number>uniqueResult().intValue() > 0)
                errorCollector.add("Невозможно сохранить бланки «" + rowWrapper.getType().getTitle() + "» серии «" + rowWrapper.getSeria() + "» номера с «"
                    + rowWrapper.getStringFirstNumber() + "» по «" + rowWrapper.getStringLastNumber() + "». В системе уже зарегистрированы бланки с такими номерами.");
        }

        if (errorCollector.hasErrors())
            return null;

        int result = 0;
        short entityCode = EntityRuntime.getMeta(DipDiplomaBlank.class).getEntityCode();
        DipBlankState state = this.getCatalogItem(DipBlankState.class, DipBlankStateCodes.FREE);
        for (final DipBlankRowWrapper rowWrapper : rowWrappers)
        {
            int firstNumber = rowWrapper.getFirstNumber();
            int lastNumber = rowWrapper.getLastNumber();
            int numbersCount = lastNumber - firstNumber + 1;

            DQLInsertValuesBuilder insertBuilder = new DQLInsertValuesBuilder(DipDiplomaBlank.class);
            for (int i = 0; i < numbersCount; i++)
            {
                insertBuilder.value(DipDiplomaBlank.P_ID, EntityIDGenerator.generateNewId(entityCode));
                insertBuilder.value(DipDiplomaBlank.L_BLANK_TYPE, rowWrapper.getType());
                insertBuilder.value(DipDiplomaBlank.P_SERIA, rowWrapper.getSeria());
                insertBuilder.value(DipDiplomaBlank.P_NUMBER, StringUtils.leftPad(String.valueOf(firstNumber + i), 7, "0"));
                insertBuilder.value(DipDiplomaBlank.L_BLANK_STATE, state);
                insertBuilder.value(DipDiplomaBlank.L_STORAGE_LOCATION, rowWrapper.getStorageLocation());
                insertBuilder.value(DipDiplomaBlank.P_PRINTING_OFFICE, rowWrapper.getPrintingOffice());
                insertBuilder.value(DipDiplomaBlank.P_INVOICE, rowWrapper.getInvoice());

                insertBuilder.addBatch();

                if ((i + 1) % DQL.MAX_VALUES_ROW_NUMBER == 0)
                {
                    insertBuilder.createStatement(session).execute();
                    insertBuilder = new DQLInsertValuesBuilder(DipDiplomaBlank.class);
                }
            }

            if (numbersCount % DQL.MAX_VALUES_ROW_NUMBER != 0)
                insertBuilder.createStatement(session).execute();

            result += numbersCount;
        }

        return result;
    }

    private static void checkBlankSeriaAndNumberFormat(@NotNull Collection<DipBlankRowWrapper> wrappers)
    {
        Preconditions.checkNotNull(wrappers);
        final Pattern seriaPattern = Pattern.compile("^\\d{6}$");
        for (DipBlankRowWrapper rowWrapper: wrappers)
            if (!seriaPattern.matcher(rowWrapper.getSeria()).matches() || (rowWrapper.getFirstNumber() < 0) || (rowWrapper.getFirstNumber() < 0)
                    || (rowWrapper.getFirstNumber() > 9999999) || (rowWrapper.getLastNumber() < 0) || (rowWrapper.getLastNumber() > 9999999))
                throw new IllegalStateException();
    }

    private static void checkBlankNumbersForIntersection(@NotNull Collection<DipBlankRowWrapper> wrappers)
    {
        Preconditions.checkNotNull(wrappers);
        if (wrappers.size() < 2)
            return;

        DipBlankRowWrapper[] copy = wrappers.toArray(new DipBlankRowWrapper[wrappers.size()]);
        Arrays.sort(copy, (o1, o2) ->
        {
            int result = o1.getType().getCode().compareTo(o2.getType().getCode());
            if (result == 0)
                result = o1.getSeria().compareTo(o2.getSeria());
            if (result == 0)
                result = o1.getFirstNumber().compareTo(o2.getFirstNumber());
            return result;
        });

        for (int i = 1; i < copy.length; i++)
        {
            DipBlankRowWrapper previous = copy[i - 1];
            DipBlankRowWrapper current = copy[i];

            if (previous.getType().equals(current.getType()) && previous.getSeria().equals(current.getSeria()) && current.getFirstNumber().compareTo(previous.getLastNumber()) <= 0)
                throw new ApplicationException("Невозможно сохранить бланки «" + current.getType().getTitle() + "» серии «" + current.getSeria() + "». Указанные номера с «" +
                    previous.getStringFirstNumber() + "» по «" + previous.getStringLastNumber() + "» пересекаются с номерами с «" + current.getStringFirstNumber() + "» по «" + current.getStringLastNumber() + "».");
        }
    }

    private static void checkCountOfBlanks(@NotNull Collection<DipBlankRowWrapper> wrappers)
    {
        int count = 0;
        for (DipBlankRowWrapper wrapper : wrappers) {
            count += wrapper.getLastNumber() - wrapper.getFirstNumber() + 1;
        }

        if (count > 5000)
            throw new ApplicationException("Нельзя добавить более 5000 бланков одновременно.");
    }

    @Override
    public void doReserveBlank(@NotNull Long blankId, String reservationPurpose)
    {
        Preconditions.checkNotNull(blankId);

        DipDiplomaBlank blank = this.getNotNull(DipDiplomaBlank.class, blankId);
        if (!DipBlankStateCodes.FREE.equals(blank.getBlankState().getCode()))
            throw new ApplicationException("Невозможно зарезервировать бланк. Бланк должен находиться в состоянии «Свободен».");

        blank.setBlankState(this.getCatalogItem(DipBlankState.class, DipBlankStateCodes.RESERVED));
        blank.setReservationPurpose(reservationPurpose);

        this.update(blank);
    }

    @Override
    public void doCancelReserve(@NotNull Long blankId)
    {
        Preconditions.checkNotNull(blankId);

        DipDiplomaBlank blank = this.getNotNull(DipDiplomaBlank.class, blankId);
        if (!DipBlankStateCodes.RESERVED.equals(blank.getBlankState().getCode()))
            throw new ApplicationException("Невозможно отменить резерв. Бланк должен находиться в состоянии «Зарезервирован».");

        blank.setBlankState(this.getCatalogItem(DipBlankState.class, DipBlankStateCodes.FREE));
        blank.setReservationPurpose(null);

        this.update(blank);
    }

    @Override
    public Integer doDisposeBlanks(@NotNull Collection<Long> blankIds, final DipBlankDisposalReason reason, final Date disposalDate)
    {
        Preconditions.checkNotNull(blankIds);

        if (blankIds.isEmpty())
            return 0;

        int counter = 0;
        for (List<Long> idsPart : Iterables.partition(blankIds, DQL.MAX_VALUES_ROW_NUMBER))
        {
            DQLSelectBuilder notFreeDQL = new DQLSelectBuilder()
                    .fromEntity(DipDiplomaBlank.class, "b")
                    .column(value(1))
                    .where(in(property("b"), idsPart))
                    .where(ne(property("b", DipDiplomaBlank.blankState().code()), value(DipBlankStateCodes.FREE)));

            if (existsEntity(notFreeDQL.buildQuery()))
                throw new ApplicationException("Невозможно списать бланки. Выбранные бланки должны находиться в состоянии «Свободен».");

            counter += new DQLUpdateBuilder(DipDiplomaBlank.class)
                    .set(DipDiplomaBlank.L_DISPOSAL_REASON, value(reason))
                    .set(DipDiplomaBlank.P_DISPOSAL_DATE, valueDate(disposalDate))
                    .set(DipDiplomaBlank.L_BLANK_STATE, value(getCatalogItem(DipBlankState.class, DipBlankStateCodes.DISPOSAL)))
                    .where(in(property(DipDiplomaBlank.id()), idsPart))
                    .createStatement(getSession())
                    .execute();
        }

        return counter;
    }

    @Override
    public void doCancelDisposal(@NotNull Long blankId)
    {
        Preconditions.checkNotNull(blankId);

        DipDiplomaBlank blank = this.getNotNull(DipDiplomaBlank.class, blankId);
        if (!DipBlankStateCodes.DISPOSAL.equals(blank.getBlankState().getCode()))
            throw new ApplicationException("Невозможно отменить списание. Бланк должен находиться в состоянии «Списан».");

        blank.setBlankState(this.getCatalogItem(DipBlankState.class, DipBlankStateCodes.FREE));
        blank.setDisposalReason(null);
        blank.setDisposalDate(null);

        this.update(blank);
    }

	@Override
	public void doCancelBlankAppendixesLink(DiplomaIssuance diplomaIssuance)
	{
		DipBlankState freeState = getCatalogItem(DipBlankState.class, DipBlankStateCodes.FREE);
		final String contentIssuanceAlias = "contentIssuance";
		DQLSelectBuilder appendixBlankIds = new DQLSelectBuilder().fromEntity(DiplomaContentIssuance.class, contentIssuanceAlias)
				.where(eq(property(contentIssuanceAlias, DiplomaContentIssuance.diplomaIssuance()), value(diplomaIssuance)))
				.column(property(contentIssuanceAlias, DiplomaContentIssuance.blank().id()));
		executeAndClear(new DQLUpdateBuilder(DipDiplomaBlank.class)
				.set(DipDiplomaBlank.L_BLANK_STATE, value(freeState))
				.where(in(property(DipDiplomaBlank.P_ID), appendixBlankIds.buildQuery())));
	}

	@Override
    public Integer doChangeStorageLocation(@NotNull Collection<Long> blankIds, @NotNull final OrgUnit storageLocation)
    {
        Preconditions.checkNotNull(blankIds);
        Preconditions.checkNotNull(storageLocation);

        if (blankIds.isEmpty())
            return 0;

        final Long storableLocationId = getProperty(DipDiplomaBlank.class, DipDiplomaBlank.storageLocation().id().s(), DipDiplomaBlank.id().s(), blankIds.iterator().next());
        int counter = 0;
        for (List<Long> idsPart : Iterables.partition(blankIds, DQL.MAX_VALUES_ROW_NUMBER))
        {
            DQLSelectBuilder hasOtherLocationDQL = new DQLSelectBuilder()
                    .fromEntity(DipDiplomaBlank.class, "b")
                    .column(value(1))
                    .where(in(property("b"), idsPart))
                    .where(ne(property("b", DipDiplomaBlank.storageLocation().id()), value(storableLocationId)));

            if (existsEntity(hasOtherLocationDQL.buildQuery()))
                throw new ApplicationException("Невозможно сменить место хранения. Выбранные бланки находятся на разных местах хранения.");

            DQLSelectBuilder notFreeDQL = new DQLSelectBuilder()
                    .fromEntity(DipDiplomaBlank.class, "b")
                    .column(value(1))
                    .where(in(property("b"), idsPart))
                    .where(ne(property("b", DipDiplomaBlank.blankState().code()), value(DipBlankStateCodes.FREE)));

            if (existsEntity(notFreeDQL.buildQuery()))
                throw new ApplicationException("Невозможно сменить место хранения. Все выбранные бланки должны находиться в состоянии «Свободен».");

            counter += new DQLUpdateBuilder(DipDiplomaBlank.class)
                    .set(DipDiplomaBlank.L_STORAGE_LOCATION, value(storageLocation))
                    .where(in(property(DipDiplomaBlank.id()), idsPart))
                    .createStatement(getSession())
                    .execute();
        }

        return counter;
    }

    @Override
    public @Nullable String getDiplomaObjectBlankType(@NotNull DipDocumentType documentType, boolean withSuccess, boolean appendix)
    {
        Preconditions.checkNotNull(documentType);
        if (appendix)
        {
            switch (documentType.getCode())
            {
                case DipDocumentTypeCodes.COLLEGE_DIPLOMA : return DipBlankTypeCodes.APPENDIX_FOR_SPO_BLANK;
                case DipDocumentTypeCodes.BACHELOR_DIPLOMA : return DipBlankTypeCodes.APPENDIX_FOR_BACH_SPEC_BLANK;
                case DipDocumentTypeCodes.SPECIALIST_DIPLOMA : return DipBlankTypeCodes.APPENDIX_FOR_BACH_SPEC_BLANK;
                case DipDocumentTypeCodes.MAGISTR_DIPLOMA : return DipBlankTypeCodes.APPENDIX_FOR_MASTER_BLANK;
                case DipDocumentTypeCodes.POSTGRADUATE_DIPLOMA : return DipBlankTypeCodes.APPENDIX_FOR_POST_GRAD_BLANK;
                case DipDocumentTypeCodes.ADUNCTURE_DIPLOMA_ : return DipBlankTypeCodes.APPENDIX_FOR_ADJUNCTURE_BLANK;
            }

            return null;
        }

        switch (documentType.getCode())
        {
            case DipDocumentTypeCodes.COLLEGE_DIPLOMA : return withSuccess ? DipBlankTypeCodes.SPO_WITH_HONOUR_BLANK : DipBlankTypeCodes.SPO_BLANK;
            case DipDocumentTypeCodes.BACHELOR_DIPLOMA : return withSuccess ? DipBlankTypeCodes.BACHELOR_WITH_HONOUR_BLANK : DipBlankTypeCodes.BACHELOR_BLANK;
            case DipDocumentTypeCodes.SPECIALIST_DIPLOMA : return withSuccess ? DipBlankTypeCodes.SPECIALTY_WITH_HONOUR_BLANK : DipBlankTypeCodes.SPECIALTY_BLANK;
            case DipDocumentTypeCodes.MAGISTR_DIPLOMA : return withSuccess ? DipBlankTypeCodes.MASTER_WITH_HONOUR_BLANK : DipBlankTypeCodes.MASTER_BLANK;
            case DipDocumentTypeCodes.POSTGRADUATE_DIPLOMA : return DipBlankTypeCodes.POST_GRADUATE_BLANK;
            case DipDocumentTypeCodes.ADUNCTURE_DIPLOMA_ : return DipBlankTypeCodes.ADJUNCTURE_BLANK;
        }

        return null;
    }

    @Override
    public void massChangeState(Collection<DipDiplomaBlank> input, final DipBlankState state) {

        for (List<DipDiplomaBlank> idsPart : Iterables.partition(input, DQL.MAX_VALUES_ROW_NUMBER))
        {
            new DQLUpdateBuilder(DipDiplomaBlank.class)
                    .set(DipDiplomaBlank.L_BLANK_STATE, value(state))
                    .where(in(property("id"), idsPart))
                    .createStatement(getSession()).execute();
        }
    }

    @Override
    public void massDeleteBlanks(Collection<DipDiplomaBlank> input) {

        final DipBlankState freeState = getCatalogItem(DipBlankState.class, DipBlankStateCodes.FREE);
        for (List<DipDiplomaBlank> idsPart : Iterables.partition(input, DQL.MAX_VALUES_ROW_NUMBER))
        {
            new DQLDeleteBuilder(DipDiplomaBlank.class)
                    .where(in(property("id"), idsPart))
                    .where(eq(property(DipDiplomaBlank.blankState()), value(freeState)))
                    .createStatement(getSession()).execute();
        }
    }

    @Override
    public List<DipDiplomaBlank> getShownBlanks(Collection<Long> blankIds, boolean hideIssued)
    {
        List<DipDiplomaBlank> blanks = this.getList(DipDiplomaBlank.class, blankIds, DipDiplomaBlank.blankType().title().s(), DipDiplomaBlank.seria().s(), DipDiplomaBlank.number().s());
        return hideIssued ?
                blanks.stream().filter(blank -> blank.getBlankState().getCode().equals(DipBlankStateCodes.DISPOSAL)).collect(Collectors.toList()) :
                blanks;
    }

    @Override
    public Map<DipDiplomaBlank, DipStuExcludeExtract> getDiplomaBlanksToExcludeExtracts(List<DipDiplomaBlank> blanks)
    {
        Map<DipDiplomaBlank, DiplomaObject> blanksToDimplomas = getBlanksToDimplomas(blanks);
        Map<DipDiplomaBlank, DipStuExcludeExtract> blanksToExtracts = getBlanksToExtractByDiplomas(blanksToDimplomas);

        blanks.removeAll(blanksToExtracts.keySet());
        blanksToExtracts.putAll(getBlanksToExtractByRelation(blanks));

        return blanksToExtracts;
    }

    /**
     * Соответствие бланка диплома и самого диплома. Не для всех бланков объект диплома есть в системе.
     */
    private Map<DipDiplomaBlank, DiplomaObject> getBlanksToDimplomas(List<DipDiplomaBlank> blanks)
    {
        DQLSelectBuilder issuances = new DQLSelectBuilder()
                .fromEntity(DiplomaIssuance.class, "issuance")
                .column(property("issuance", DiplomaIssuance.blank()))
                .column(property("issuance", DiplomaIssuance.diplomaObject()))
                .where(in(property("issuance", DiplomaIssuance.blank()), blanks));
        DQLSelectBuilder union = new DQLSelectBuilder()
                .fromEntity(DiplomaContentIssuance.class, "content")
                .column(property("content", DiplomaContentIssuance.blank()))
                .column(property("content", DiplomaContentIssuance.diplomaIssuance().diplomaObject()))
                .where(in((property("content", DiplomaContentIssuance.blank())), blanks))
                .union(issuances.buildSelectRule());
        return this.<Object[]>getList(union).stream()
                .collect(Collectors.toMap(objects -> (DipDiplomaBlank)objects[0], objects -> (DiplomaObject)objects[1]));
    }

    /**
     * Соответствие бланка диплома выписке приказа об отчислении (информация ищется по объектам диплома).
     */
    private Map<DipDiplomaBlank, DipStuExcludeExtract> getBlanksToExtractByDiplomas(Map<DipDiplomaBlank, DiplomaObject> blanksToDimplomas)
    {
        DQLSelectBuilder diplomasToExtractDQL = new DQLSelectBuilder()
                .fromEntity(DipStuExcludeExtract.class, "extract")
                .column(property("extract", DipStuExcludeExtract.diploma()))
                .column(property("extract"))
                .where(in(property("extract", DipStuExcludeExtract.diploma()), blanksToDimplomas.values()));
        Map<DiplomaObject, DipStuExcludeExtract> diplomasToExtract = this.<Object[]>getList(diplomasToExtractDQL).stream()
                .collect(Collectors.toMap(objects -> (DiplomaObject) objects[0], objects -> (DipStuExcludeExtract) objects[1]));
        return blanksToDimplomas.entrySet().stream()
                .filter(entry -> diplomasToExtract.containsKey(entry.getValue()))
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> diplomasToExtract.get(entry.getValue())));
    }

    /**
     * Соответствие бланка диплома выписке приказа об отчислении (информация берется из "Связь информации о выданном приложении к документу об обучении с выпиской из приказа об отчислении").
     * Требуется для тех бланков, для которых соответствие не может быть найдено по объекту диплома.
     */
    private Map<DipDiplomaBlank, DipStuExcludeExtract> getBlanksToExtractByRelation(List<DipDiplomaBlank> blanks)
    {
        DQLSelectBuilder rel = new DQLSelectBuilder()
                .fromEntity(DipContentIssuanceToDipExtractRelation.class, "rel")
                .column(property("rel", DipContentIssuanceToDipExtractRelation.contentIssuance().blank()))
                .column(property("rel", DipContentIssuanceToDipExtractRelation.extract()))
                .where(in(property("rel", DipContentIssuanceToDipExtractRelation.contentIssuance().blank()), blanks));
        return this.<Object[]>getList(rel).stream()
                .collect(Collectors.toMap(objects -> (DipDiplomaBlank) objects[0], objects -> (DipStuExcludeExtract) objects[1]));
    }
}
