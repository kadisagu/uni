/* $Id$ */
package ru.tandemservice.unidip.base.bo.DipDocument.ui.AddInformationAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.orgstruct.AcademyRename;
import ru.tandemservice.unidip.base.bo.DipDocument.logic.DipEduInOtherOrganizationDSHandler;
import ru.tandemservice.unidip.base.bo.DipDocument.logic.DiplomaAcademyRenameDSHandler;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaAcademyRenameData;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Andrey Avetisov
 * @since 10.07.2014
 */
@Configuration
public class DipDocumentAddInformationAddEdit extends BusinessComponentManager
{
    public static final String ACADEMY_RENAME_LIST_DS = "academyRenameListDS";
    public static final String ACADEMY_RENAME_COMBO_BOX_DS = "academyRenameComboBoxDS";
    public static final String SPECIALIZATION_SOURCE_DS = "specializationSourceDS";
    public static final String EDU_PROGRAM_FORM_SOURCE_DS = "eduProgramFormSourceDS";
    public static final String EDU_IN_OTHER_ORGANIZATION_LIST_DS = "eduInOtherOrganizationListDS";
    public static final String EDU_ORGANIZATION_DS = "eduOrganizationDS";
    public static final String DIPLOMA_CONTENT = "diplomaContent";

    public static final DataWrapper ORIENTATION = new DataWrapper(1L, "Направленность (профиль) образовательной программы");
    public static final DataWrapper SPECIALIZATION = new DataWrapper(2L, "Специализацию");


    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(ACADEMY_RENAME_LIST_DS, academyRenameDSColumns(), academyRenameDSHandler()))
                .addDataSource(selectDS(ACADEMY_RENAME_COMBO_BOX_DS, academyRenameComboBoxDSHandler()).addColumn("academyRename", "id", source -> {
                    Long academyRenameId = (Long) source;
                    AcademyRename academyRename = DataAccessServices.dao().getNotNull(academyRenameId);
                    return academyRename.getPreviousShortTitle() + ", " + DateFormatter.DATE_FORMATTER_JUST_YEAR.format(academyRename.getDate());
                }))
                .addDataSource(selectDS(EDU_PROGRAM_FORM_SOURCE_DS, eduFormDSHandler()))
                .addDataSource(selectDS(SPECIALIZATION_SOURCE_DS, specializationDSHandler()))
                .addDataSource(searchListDS(EDU_IN_OTHER_ORGANIZATION_LIST_DS, eduInOtherOrganizationDSColumns(), eduInOtherOrganizationDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint academyRenameDSColumns()
    {
        return columnListExtPointBuilder(ACADEMY_RENAME_LIST_DS)
                .addColumn(blockColumn("previousFullTitle", "previousFullTitle").required(true))
                .addColumn(blockColumn("date", "renameDate").required(true))
                .addColumn(blockColumn("edit", "actionBlock").hint(" ").width("1px").hasBlockHeader(true).required(true))
                .addColumn(blockColumn("deleteCancelBlock", "deleteCancelBlock").hint(" ").width("1px").hasBlockHeader(true).required(true))
                .create();
    }

    @Bean
    public ColumnListExtPoint eduInOtherOrganizationDSColumns()
    {
        return columnListExtPointBuilder(EDU_IN_OTHER_ORGANIZATION_LIST_DS)
                .addColumn(blockColumn("creditsBlock", "creditsBlock"))
                .addColumn(blockColumn("weeksBlock", "weeksBlock"))
                .addColumn(blockColumn("eduOrganizationBlock", "eduOrganizationBlock").required(true))
                .addColumn(blockColumn("edit", "editOrganizationBlock").hint(" ").width("1px").hasBlockHeader(true).required(true))
                .addColumn(blockColumn("deleteCancelBlock", "deleteCancelOrganizationBlock").hint(" ").width("1px").hasBlockHeader(true).required(true))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> academyRenameDSHandler()
    {
        return new DiplomaAcademyRenameDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> eduInOtherOrganizationDSHandler()
    {
        return new DipEduInOtherOrganizationDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> academyRenameComboBoxDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), AcademyRename.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                List<DataWrapper> wrappers = context.get("items");
                if (wrappers != null && !wrappers.isEmpty())
                {
                    List<Long> ids = new ArrayList<>();
                    for (DataWrapper wrapper : wrappers)
                    {
                        ids.add(((DiplomaAcademyRenameData) wrapper.getWrapped()).getAcademyRename().getId());
                    }
                    dql.where(DQLExpressions.notIn(DQLExpressions.property(alias, "id"), ids));
                }
            }
        }
                .order(AcademyRename.date());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> specializationDSHandler()
    {
        List<DataWrapper> specializationList = new ArrayList<>(2);
        specializationList.add(ORIENTATION);
        specializationList.add(SPECIALIZATION);

        SimpleTitledComboDataSourceHandler comboDataSourceHandler = new SimpleTitledComboDataSourceHandler(getName());
        comboDataSourceHandler.addAll(specializationList);
        return comboDataSourceHandler;
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> eduFormDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), EduProgramForm.class);
    }

}
