/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unidip.component.catalog.dipDocTemplateCatalog.DipDocTemplateCatalogPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultScriptCatalogPub.DefaultScriptCatalogPubController;
import ru.tandemservice.unidip.base.entity.catalog.DipDocTemplateCatalog;

/**
 * @author vip_delete
 * @since 13.01.2009
 */
public class Controller extends DefaultScriptCatalogPubController<DipDocTemplateCatalog, Model, IDAO>
{
    @Override
    protected DynamicListDataSource createListDataSource(IBusinessComponent context)
    {
        DynamicListDataSource dataSource = super.createListDataSource(context);

        IEntityHandler disabledUserTemplateHandler = entity -> {
            IScriptItem item = (IScriptItem) entity;
            String path = item.getTemplatePath();
            return null == path || null == Thread.currentThread().getContextClassLoader().getResource(path);
        };

        Model model = getModel(context);
        int index = dataSource.getColumn("title").getNumber();

        dataSource.addColumn(new SimpleColumn("Тип документа", DipDocTemplateCatalog.dipDocumentType().titleWithParent()), ++index);
        dataSource.addColumn(new BooleanColumn("Приложение", DipDocTemplateCatalog.P_APPLICATION), ++index);
        dataSource.addColumn(new BooleanColumn("С отличием", DipDocTemplateCatalog.P_WITH_SUCCESS), ++index);

		index = dataSource.getColumn(ActionColumn.EDIT).getNumber() - 1;
		dataSource.addColumn(new ToggleColumn("Используется", DipDocTemplateCatalog.P_ACTIVE).toggleOnListener("onToggleActive").toggleOffListener("onToggleActive"), ++index);

        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteItem", "Удалить элемент «{0}» из справочника?", DipDocTemplateCatalog.P_TITLE).setPermissionKey(model.getCatalogItemDelete()).setDisableHandler(model.getDisabledEntityHandler()));

        ToggleColumn userTemplateColumn = (ToggleColumn) dataSource.getColumn(IScriptItem.HAS_USER_TEMPLATE);
        userTemplateColumn.setDisableHandler(disabledUserTemplateHandler);

        return dataSource;
    }

	public void onToggleActive(IBusinessComponent component)
	{
		getDao().doToggleActive(component.getListenerParameter());
	}

    @Override
    public void onClickEditItem(IBusinessComponent context)
    {
        context.createDefaultChildRegion(new ComponentActivator(CommonManager.catalogComponentProvider().getAddEditComponent(getModel(context).getItemClass()), new ParametersMap()
                .add(DefaultCatalogAddEditModel.CATALOG_ITEM_ID, context.getListenerParameter())
        ));
    }
}