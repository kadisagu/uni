package ru.tandemservice.unidip.base.entity.diploma.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentRow;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaOptDisciplineRow;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Строка блока факультативных дисциплин в документе об обучении
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DiplomaOptDisciplineRowGen extends DiplomaContentRow
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unidip.base.entity.diploma.DiplomaOptDisciplineRow";
    public static final String ENTITY_NAME = "diplomaOptDisciplineRow";
    public static final int VERSION_HASH = 270093972;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof DiplomaOptDisciplineRowGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DiplomaOptDisciplineRowGen> extends DiplomaContentRow.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DiplomaOptDisciplineRow.class;
        }

        public T newInstance()
        {
            return (T) new DiplomaOptDisciplineRow();
        }
    }
    private static final Path<DiplomaOptDisciplineRow> _dslPath = new Path<DiplomaOptDisciplineRow>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DiplomaOptDisciplineRow");
    }
            

    public static class Path<E extends DiplomaOptDisciplineRow> extends DiplomaContentRow.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return DiplomaOptDisciplineRow.class;
        }

        public String getEntityName()
        {
            return "diplomaOptDisciplineRow";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
