package ru.tandemservice.unidip.base.entity.diploma;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.commonbase.base.util.Wiki;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import ru.tandemservice.unidip.base.bo.DipDocument.DipDocumentManager;
import ru.tandemservice.unidip.base.entity.catalog.DipDocumentType;
import ru.tandemservice.unidip.base.entity.diploma.gen.DiplomaObjectGen;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Диплом
 * <p/>
 * Диплом – документ удостоверяющий получение образования (поведение определяется типом)
 */
public class DiplomaObject extends DiplomaObjectGen
{


    /**
     * Вариант 1. Тип документа
     * <p/>
     * Формат: <тип документа>
     * <ul>
     * <li>{@link DipDocumentType Тип документ} — из связанного {@link DiplomaContent содержания документа об обучении},</li>
     * <ul>формат: название из справочника «Типы выдаваемых документов об обучении».</ul>
     * </ul>
     */
    @EntityDSLSupport(parts = {DiplomaObject.L_CONTENT + "." + DiplomaContent.L_TYPE + "." + DipDocumentType.P_TITLE})
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=38372371")
    public String getDocumentType()
    {
        return getContent().getType().getTitle();
    }

    /**
     * Вариант 2. Тип документа и признак "с отличием"
     * <p/>
     * Формат:  <тип документа> <признак с отличием>
     * <ul>
     * <li>{@link DipDocumentType Тип документ} — из связанного {@link DiplomaContent содержания документа об обучении},</li>
     * <ul>формат: название из справочника «Типы выдаваемых документов об обучении».</ul>
     * <li>{@link DiplomaContent#isWithSuccess() Признак с отличием} — из {@link DiplomaContent содержания документа об обучении}.</li>
     * <ul>Если флаг — true, то выводим текст "с отличием", если false — пусто.</ul>
     * </ul>
     */
    @EntityDSLSupport(parts = {DiplomaObject.L_CONTENT + "." + DiplomaContent.L_TYPE + "." + DipDocumentType.P_TITLE, DiplomaObject.L_CONTENT + "." + DiplomaContent.P_WITH_SUCCESS})
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=38372371")
    public String getTitleWithSuccess()
    {
        if (this.getContent().isWithSuccess())
            return getContent().getType().getTitle() + " с отличием";
        else
            return getContent().getType().getTitle();
    }

    /**
     * Вариант 3. Выдаваемый документ
     * <p/>
     * Формат: <вариант 2. тип документа с признаком>[: <серия бланка из факта выдачи> <номер бланка из факта выдачи> ("информация о выданных приложениях")][, <серия бланка из след. факта выдачи> <номер бланка из след. факта выдачи>,...] [ГЭК <Фамилия И.О. председателя ГЭК> <дата защиты> <№ протокола>]
     * <ul>
     * <li>Выводится тип документа об обучении  с признаком отличия  — {@link #getDocumentType Вариант 2}</li>
     * <li>Если у документа есть факт выдачи, то добавляем двоеточие и серию бланка из факта выдачи и номер бланка из факта выдачи. Если у документа несколько фактов выдачи, то выводить через запятые. Если у фактов выдачи нет серии и номера бланка, то факт выдачи не выводим.</li>
     * <li>К выводу серии и номеру бланка добавить в круглых скобках информацию о выданных приложениях (см. формат 4 Информация о выданных приложениях). Если приложения нет — то пропускаем и скобки не выводим.</li>
     * <li>Затем добавляем сведения о ГЭК. Если каких-то сведений нет, то вместо них ничего не выводим.</li>
     * <ul>
     * <li>Фамилия И.О. председателя ГЭК — ФИО председателя ГЭК из Документа об обучении</li>
     * <li>дата защиты — Дата заседания ГЭК из Документа об обучении. Формат ДД.ММ.ГГГГ</li>
     * <li>№ протокола — Номер протокола ГЭК из Документа об обучении. Формат "№ <номер протокола>"</li>
     * </ul>
     * </ul>
     */
    @EntityDSLSupport()
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=38372371")
    public String getIssuantDocument()
    {
        StringBuilder result = new StringBuilder(getTitleWithSuccess());

        String issuanceList = getIssuanceList();

        if (!issuanceList.isEmpty())
            result.append(": ").append(issuanceList);

        String chairFio = getStateCommissionChairFio();
        if (StringUtils.isNotBlank(chairFio))
            result.append(" ГЭК ").append(chairFio);

        Date commissionDate = getStateCommissionDate();
        if (commissionDate != null)
            result.append(" ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(commissionDate));

        String protocolNumber = getStateCommissionProtocolNumber();
        if (StringUtils.isNotBlank(protocolNumber))
            result.append(" № ").append(protocolNumber);

        return result.toString();
    }

    /**
     * Вариант 4. Информация фактах выдачи и выданных приложениях
     * <p/>
     * Формат: <серия бланка из факта выдачи> <номер бланка из факта выдачи> ("информация о выданных приложениях"), <серия бланка из след. факта выдачи> <номер бланка из след. факта выдачи>,...
     * <ul>
     * <li>Если у документа есть факт выдачи, то добавляем двоеточие и серию бланка из факта выдачи и номер бланка из факта выдачи. Если у документа несколько фактов выдачи, то выводить через запятые. Если у фактов выдачи нет серии и номера бланка, то факт выдачи не выводим.</li>
     * <li>К выводу серии и номеру бланка добавить в круглых скобках информацию о выданных приложениях (см. формат 4 Информация о выданных приложениях). Если приложения нет — то пропускаем и скобки не выводим.</li>
     * </ul>
     */
    @EntityDSLSupport()
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=38372371")
    public String getIssuanceList()
    {
        Map<DiplomaIssuance, List<DiplomaContentIssuance>> issuanceMap = DipDocumentManager.instance().dao().getDiplomaIssuanceList(this);
        StringBuilder issuances = new StringBuilder();
        issuanceMap.forEach((issuance, issContentList) ->
                            {
                                String seria = issuance.getBlankSeria();
                                boolean seriaNotBlank = StringUtils.isNotBlank(seria);
                                String number = issuance.getBlankNumber();
                                boolean numberNotBlank = StringUtils.isNotBlank(number);

                                int[] count = {0};
                                StringBuilder issuanceDocuments = new StringBuilder();

                                if (CollectionUtils.isNotEmpty(issContentList))
                                {
                                    issContentList
                                            .stream()
                                            .filter(Objects::nonNull)
                                            .sorted((o1, o2) ->
                                                    {
                                                        int r = CommonCollator.RUSSIAN_COLLATOR.compare(o1.getBlankSeria(), o2.getBlankSeria());
                                                        return r != 0 ? r : CommonCollator.RUSSIAN_COLLATOR.compare(o1.getBlankNumber(), o2.getBlankNumber());
                                                    }
                                            )
                                            .forEach(
                                                    issuantDocument ->
                                                    {
                                                        if (issuantDocument == null) return;

                                                        String blankSeria = issuantDocument.getBlankSeria();
                                                        String blankNumber = issuantDocument.getBlankNumber();
                                                        boolean blankSeriaNotBlank = StringUtils.isNotBlank(blankSeria);
                                                        boolean blankNumberNotBlank = StringUtils.isNotBlank(blankNumber);

                                                        if (blankSeriaNotBlank || blankNumberNotBlank)
                                                        {
                                                            count[0]++;
                                                            if (issuanceDocuments.length() > 0)
                                                                issuanceDocuments.append(", ");
                                                        }

                                                        if (blankSeriaNotBlank)
                                                            issuanceDocuments.append(blankSeria);
                                                        if (blankSeriaNotBlank && blankNumberNotBlank)
                                                            issuanceDocuments.append(" ");
                                                        if (blankNumberNotBlank)
                                                            issuanceDocuments.append(blankNumber);
                                                    });
                                }

                                if (issuances.length() > 0 && (issuanceDocuments.length() > 0 || seriaNotBlank || numberNotBlank))
                                    issuances.append(", ");

                                if (seriaNotBlank)
                                    issuances.append(seria);
                                if (seriaNotBlank && numberNotBlank)
                                    issuances.append(" ");
                                if (numberNotBlank)
                                    issuances.append(number);

                                if (issuanceDocuments.length() > 0)
                                {
                                    if (seriaNotBlank || numberNotBlank)
                                        issuances.append(" ");

                                    if (count[0] == 1) issuanceDocuments.insert(0, "приложение ");
                                    else issuanceDocuments.insert(0, "приложения ");

                                    issuances.append("(").append(issuanceDocuments).append(")");
                                }

                            });

        return issuances.toString();
    }
}