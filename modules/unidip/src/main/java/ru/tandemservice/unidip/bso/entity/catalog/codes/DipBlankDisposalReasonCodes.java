package ru.tandemservice.unidip.bso.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Причина списания бланка"
 * Имя сущности : dipBlankDisposalReason
 * Файл data.xml : unidip-catalogs.data.xml
 */
public interface DipBlankDisposalReasonCodes
{
    /** Константа кода (code) элемента : Допущена ошибка при заполнении (title) */
    String ERROR_AT_FILLING = "errorAtFilling";
    /** Константа кода (code) элемента : Бланки устарели (не актуальны) (title) */
    String BLANKS_ARE_OUT_OF_DATE = "blankAreOutOfDate";
    /** Константа кода (code) элемента : Уничтожение или необратимое повреждение в результате форс-мажорных обстоятельств — войны, стихийного бедствия (title) */
    String FORCE_MAJEURE_DESTRUCTION = "forceMajeureDestruction";
    /** Константа кода (code) элемента : Утрата или необратимое повреждение по вине исполнителя (title) */
    String EXECUTIVE_FAULT_LOSS_OR_DESTRUCTION = "executiveFaultLossOrDestruction";

    Set<String> CODES = ImmutableSet.of(ERROR_AT_FILLING, BLANKS_ARE_OUT_OF_DATE, FORCE_MAJEURE_DESTRUCTION, EXECUTIVE_FAULT_LOSS_OR_DESTRUCTION);
}
