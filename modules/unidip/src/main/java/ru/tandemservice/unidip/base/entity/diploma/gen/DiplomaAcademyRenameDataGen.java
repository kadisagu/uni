package ru.tandemservice.unidip.base.entity.diploma.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.orgstruct.AcademyRename;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaAcademyRenameData;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContent;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Данные о переименовании обр. орг. для вывода в документе об обучении
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DiplomaAcademyRenameDataGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unidip.base.entity.diploma.DiplomaAcademyRenameData";
    public static final String ENTITY_NAME = "diplomaAcademyRenameData";
    public static final int VERSION_HASH = -955982398;
    private static IEntityMeta ENTITY_META;

    public static final String L_ACADEMY_RENAME = "academyRename";
    public static final String L_DIPLOMA_CONTENT = "diplomaContent";

    private AcademyRename _academyRename;     // Переименование образовательной организации
    private DiplomaContent _diplomaContent;     // Приложение к документу об обучении

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Переименование образовательной организации. Свойство не может быть null.
     */
    @NotNull
    public AcademyRename getAcademyRename()
    {
        return _academyRename;
    }

    /**
     * @param academyRename Переименование образовательной организации. Свойство не может быть null.
     */
    public void setAcademyRename(AcademyRename academyRename)
    {
        dirty(_academyRename, academyRename);
        _academyRename = academyRename;
    }

    /**
     * @return Приложение к документу об обучении. Свойство не может быть null.
     */
    @NotNull
    public DiplomaContent getDiplomaContent()
    {
        return _diplomaContent;
    }

    /**
     * @param diplomaContent Приложение к документу об обучении. Свойство не может быть null.
     */
    public void setDiplomaContent(DiplomaContent diplomaContent)
    {
        dirty(_diplomaContent, diplomaContent);
        _diplomaContent = diplomaContent;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof DiplomaAcademyRenameDataGen)
        {
            setAcademyRename(((DiplomaAcademyRenameData)another).getAcademyRename());
            setDiplomaContent(((DiplomaAcademyRenameData)another).getDiplomaContent());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DiplomaAcademyRenameDataGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DiplomaAcademyRenameData.class;
        }

        public T newInstance()
        {
            return (T) new DiplomaAcademyRenameData();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "academyRename":
                    return obj.getAcademyRename();
                case "diplomaContent":
                    return obj.getDiplomaContent();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "academyRename":
                    obj.setAcademyRename((AcademyRename) value);
                    return;
                case "diplomaContent":
                    obj.setDiplomaContent((DiplomaContent) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "academyRename":
                        return true;
                case "diplomaContent":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "academyRename":
                    return true;
                case "diplomaContent":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "academyRename":
                    return AcademyRename.class;
                case "diplomaContent":
                    return DiplomaContent.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DiplomaAcademyRenameData> _dslPath = new Path<DiplomaAcademyRenameData>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DiplomaAcademyRenameData");
    }
            

    /**
     * @return Переименование образовательной организации. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaAcademyRenameData#getAcademyRename()
     */
    public static AcademyRename.Path<AcademyRename> academyRename()
    {
        return _dslPath.academyRename();
    }

    /**
     * @return Приложение к документу об обучении. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaAcademyRenameData#getDiplomaContent()
     */
    public static DiplomaContent.Path<DiplomaContent> diplomaContent()
    {
        return _dslPath.diplomaContent();
    }

    public static class Path<E extends DiplomaAcademyRenameData> extends EntityPath<E>
    {
        private AcademyRename.Path<AcademyRename> _academyRename;
        private DiplomaContent.Path<DiplomaContent> _diplomaContent;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Переименование образовательной организации. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaAcademyRenameData#getAcademyRename()
     */
        public AcademyRename.Path<AcademyRename> academyRename()
        {
            if(_academyRename == null )
                _academyRename = new AcademyRename.Path<AcademyRename>(L_ACADEMY_RENAME, this);
            return _academyRename;
        }

    /**
     * @return Приложение к документу об обучении. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaAcademyRenameData#getDiplomaContent()
     */
        public DiplomaContent.Path<DiplomaContent> diplomaContent()
        {
            if(_diplomaContent == null )
                _diplomaContent = new DiplomaContent.Path<DiplomaContent>(L_DIPLOMA_CONTENT, this);
            return _diplomaContent;
        }

        public Class getEntityClass()
        {
            return DiplomaAcademyRenameData.class;
        }

        public String getEntityName()
        {
            return "diplomaAcademyRenameData";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
