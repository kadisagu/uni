package ru.tandemservice.unidip.base.entity.diploma.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentIssuance;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance;
import ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Информация о выданном приложении к документу об обучении
 *
 * Данные о бланке, использованном для выдачи приложения к диплому.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DiplomaContentIssuanceGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unidip.base.entity.diploma.DiplomaContentIssuance";
    public static final String ENTITY_NAME = "diplomaContentIssuance";
    public static final int VERSION_HASH = -2109427352;
    private static IEntityMeta ENTITY_META;

    public static final String L_DIPLOMA_ISSUANCE = "diplomaIssuance";
    public static final String P_CONTENT_LIST_NUMBER = "contentListNumber";
    public static final String L_BLANK = "blank";
    public static final String P_BLANK_SERIA = "blankSeria";
    public static final String P_BLANK_NUMBER = "blankNumber";
    public static final String P_DUPLICATE = "duplicate";
    public static final String P_DUPLICATE_REGUSTRATION_NUMBER = "duplicateRegustrationNumber";
    public static final String P_DUPLICATE_ISSUANCE_DATE = "duplicateIssuanceDate";

    private DiplomaIssuance _diplomaIssuance;     // Факт выдачи документа
    private int _contentListNumber;     // Номер листа
    private DipDiplomaBlank _blank;     // Бланк
    private String _blankSeria;     // Серия бланка
    private String _blankNumber;     // Номер бланка
    private boolean _duplicate = false;     // Дубликат
    private String _duplicateRegustrationNumber;     // Регистрационный номер дубликата
    private Date _duplicateIssuanceDate;     // Дата выдачи дубликата

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Факт выдачи документа. Свойство не может быть null.
     */
    @NotNull
    public DiplomaIssuance getDiplomaIssuance()
    {
        return _diplomaIssuance;
    }

    /**
     * @param diplomaIssuance Факт выдачи документа. Свойство не может быть null.
     */
    public void setDiplomaIssuance(DiplomaIssuance diplomaIssuance)
    {
        dirty(_diplomaIssuance, diplomaIssuance);
        _diplomaIssuance = diplomaIssuance;
    }

    /**
     * Номер листа приложения.
     *
     * @return Номер листа. Свойство не может быть null.
     */
    @NotNull
    public int getContentListNumber()
    {
        return _contentListNumber;
    }

    /**
     * @param contentListNumber Номер листа. Свойство не может быть null.
     */
    public void setContentListNumber(int contentListNumber)
    {
        dirty(_contentListNumber, contentListNumber);
        _contentListNumber = contentListNumber;
    }

    /**
     * @return Бланк.
     */
    public DipDiplomaBlank getBlank()
    {
        return _blank;
    }

    /**
     * @param blank Бланк.
     */
    public void setBlank(DipDiplomaBlank blank)
    {
        dirty(_blank, blank);
        _blank = blank;
    }

    /**
     * Серия бланка, на котором напечатан лист приложения.
     *
     * @return Серия бланка. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getBlankSeria()
    {
        return _blankSeria;
    }

    /**
     * @param blankSeria Серия бланка. Свойство не может быть null.
     */
    public void setBlankSeria(String blankSeria)
    {
        dirty(_blankSeria, blankSeria);
        _blankSeria = blankSeria;
    }

    /**
     * Номер бланка, на котором напечатан лист приложения.
     *
     * @return Номер бланка. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getBlankNumber()
    {
        return _blankNumber;
    }

    /**
     * @param blankNumber Номер бланка. Свойство не может быть null.
     */
    public void setBlankNumber(String blankNumber)
    {
        dirty(_blankNumber, blankNumber);
        _blankNumber = blankNumber;
    }

    /**
     * Если выдается дубликат приложения без выдачи дубликата диплома, то в системе нужно создать дубликат приложения в рамках исходного факта выдачи диплома (того, чьи рег. данные будут использованы при печати дубликата приложения).
     *
     * @return Дубликат. Свойство не может быть null.
     */
    @NotNull
    public boolean isDuplicate()
    {
        return _duplicate;
    }

    /**
     * @param duplicate Дубликат. Свойство не может быть null.
     */
    public void setDuplicate(boolean duplicate)
    {
        dirty(_duplicate, duplicate);
        _duplicate = duplicate;
    }

    /**
     * Не заполняется для приложений, не являющихся дубликатами, поскольку в таком случае совпадает с регистрационным номером диплома. Можно не заполнять для приложений-дубликатов, тогда тоже будет взят регистрационный номер диплома.
     *
     * @return Регистрационный номер дубликата.
     */
    @Length(max=255)
    public String getDuplicateRegustrationNumber()
    {
        return _duplicateRegustrationNumber;
    }

    /**
     * @param duplicateRegustrationNumber Регистрационный номер дубликата.
     */
    public void setDuplicateRegustrationNumber(String duplicateRegustrationNumber)
    {
        dirty(_duplicateRegustrationNumber, duplicateRegustrationNumber);
        _duplicateRegustrationNumber = duplicateRegustrationNumber;
    }

    /**
     * Не заполняется для приложений, не являющихся дубликатами, поскольку в таком случае совпадает с датой выдачи диплома.
     *
     * @return Дата выдачи дубликата.
     */
    public Date getDuplicateIssuanceDate()
    {
        return _duplicateIssuanceDate;
    }

    /**
     * @param duplicateIssuanceDate Дата выдачи дубликата.
     */
    public void setDuplicateIssuanceDate(Date duplicateIssuanceDate)
    {
        dirty(_duplicateIssuanceDate, duplicateIssuanceDate);
        _duplicateIssuanceDate = duplicateIssuanceDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof DiplomaContentIssuanceGen)
        {
            setDiplomaIssuance(((DiplomaContentIssuance)another).getDiplomaIssuance());
            setContentListNumber(((DiplomaContentIssuance)another).getContentListNumber());
            setBlank(((DiplomaContentIssuance)another).getBlank());
            setBlankSeria(((DiplomaContentIssuance)another).getBlankSeria());
            setBlankNumber(((DiplomaContentIssuance)another).getBlankNumber());
            setDuplicate(((DiplomaContentIssuance)another).isDuplicate());
            setDuplicateRegustrationNumber(((DiplomaContentIssuance)another).getDuplicateRegustrationNumber());
            setDuplicateIssuanceDate(((DiplomaContentIssuance)another).getDuplicateIssuanceDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DiplomaContentIssuanceGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DiplomaContentIssuance.class;
        }

        public T newInstance()
        {
            return (T) new DiplomaContentIssuance();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "diplomaIssuance":
                    return obj.getDiplomaIssuance();
                case "contentListNumber":
                    return obj.getContentListNumber();
                case "blank":
                    return obj.getBlank();
                case "blankSeria":
                    return obj.getBlankSeria();
                case "blankNumber":
                    return obj.getBlankNumber();
                case "duplicate":
                    return obj.isDuplicate();
                case "duplicateRegustrationNumber":
                    return obj.getDuplicateRegustrationNumber();
                case "duplicateIssuanceDate":
                    return obj.getDuplicateIssuanceDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "diplomaIssuance":
                    obj.setDiplomaIssuance((DiplomaIssuance) value);
                    return;
                case "contentListNumber":
                    obj.setContentListNumber((Integer) value);
                    return;
                case "blank":
                    obj.setBlank((DipDiplomaBlank) value);
                    return;
                case "blankSeria":
                    obj.setBlankSeria((String) value);
                    return;
                case "blankNumber":
                    obj.setBlankNumber((String) value);
                    return;
                case "duplicate":
                    obj.setDuplicate((Boolean) value);
                    return;
                case "duplicateRegustrationNumber":
                    obj.setDuplicateRegustrationNumber((String) value);
                    return;
                case "duplicateIssuanceDate":
                    obj.setDuplicateIssuanceDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "diplomaIssuance":
                        return true;
                case "contentListNumber":
                        return true;
                case "blank":
                        return true;
                case "blankSeria":
                        return true;
                case "blankNumber":
                        return true;
                case "duplicate":
                        return true;
                case "duplicateRegustrationNumber":
                        return true;
                case "duplicateIssuanceDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "diplomaIssuance":
                    return true;
                case "contentListNumber":
                    return true;
                case "blank":
                    return true;
                case "blankSeria":
                    return true;
                case "blankNumber":
                    return true;
                case "duplicate":
                    return true;
                case "duplicateRegustrationNumber":
                    return true;
                case "duplicateIssuanceDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "diplomaIssuance":
                    return DiplomaIssuance.class;
                case "contentListNumber":
                    return Integer.class;
                case "blank":
                    return DipDiplomaBlank.class;
                case "blankSeria":
                    return String.class;
                case "blankNumber":
                    return String.class;
                case "duplicate":
                    return Boolean.class;
                case "duplicateRegustrationNumber":
                    return String.class;
                case "duplicateIssuanceDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DiplomaContentIssuance> _dslPath = new Path<DiplomaContentIssuance>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DiplomaContentIssuance");
    }
            

    /**
     * @return Факт выдачи документа. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentIssuance#getDiplomaIssuance()
     */
    public static DiplomaIssuance.Path<DiplomaIssuance> diplomaIssuance()
    {
        return _dslPath.diplomaIssuance();
    }

    /**
     * Номер листа приложения.
     *
     * @return Номер листа. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentIssuance#getContentListNumber()
     */
    public static PropertyPath<Integer> contentListNumber()
    {
        return _dslPath.contentListNumber();
    }

    /**
     * @return Бланк.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentIssuance#getBlank()
     */
    public static DipDiplomaBlank.Path<DipDiplomaBlank> blank()
    {
        return _dslPath.blank();
    }

    /**
     * Серия бланка, на котором напечатан лист приложения.
     *
     * @return Серия бланка. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentIssuance#getBlankSeria()
     */
    public static PropertyPath<String> blankSeria()
    {
        return _dslPath.blankSeria();
    }

    /**
     * Номер бланка, на котором напечатан лист приложения.
     *
     * @return Номер бланка. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentIssuance#getBlankNumber()
     */
    public static PropertyPath<String> blankNumber()
    {
        return _dslPath.blankNumber();
    }

    /**
     * Если выдается дубликат приложения без выдачи дубликата диплома, то в системе нужно создать дубликат приложения в рамках исходного факта выдачи диплома (того, чьи рег. данные будут использованы при печати дубликата приложения).
     *
     * @return Дубликат. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentIssuance#isDuplicate()
     */
    public static PropertyPath<Boolean> duplicate()
    {
        return _dslPath.duplicate();
    }

    /**
     * Не заполняется для приложений, не являющихся дубликатами, поскольку в таком случае совпадает с регистрационным номером диплома. Можно не заполнять для приложений-дубликатов, тогда тоже будет взят регистрационный номер диплома.
     *
     * @return Регистрационный номер дубликата.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentIssuance#getDuplicateRegustrationNumber()
     */
    public static PropertyPath<String> duplicateRegustrationNumber()
    {
        return _dslPath.duplicateRegustrationNumber();
    }

    /**
     * Не заполняется для приложений, не являющихся дубликатами, поскольку в таком случае совпадает с датой выдачи диплома.
     *
     * @return Дата выдачи дубликата.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentIssuance#getDuplicateIssuanceDate()
     */
    public static PropertyPath<Date> duplicateIssuanceDate()
    {
        return _dslPath.duplicateIssuanceDate();
    }

    public static class Path<E extends DiplomaContentIssuance> extends EntityPath<E>
    {
        private DiplomaIssuance.Path<DiplomaIssuance> _diplomaIssuance;
        private PropertyPath<Integer> _contentListNumber;
        private DipDiplomaBlank.Path<DipDiplomaBlank> _blank;
        private PropertyPath<String> _blankSeria;
        private PropertyPath<String> _blankNumber;
        private PropertyPath<Boolean> _duplicate;
        private PropertyPath<String> _duplicateRegustrationNumber;
        private PropertyPath<Date> _duplicateIssuanceDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Факт выдачи документа. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentIssuance#getDiplomaIssuance()
     */
        public DiplomaIssuance.Path<DiplomaIssuance> diplomaIssuance()
        {
            if(_diplomaIssuance == null )
                _diplomaIssuance = new DiplomaIssuance.Path<DiplomaIssuance>(L_DIPLOMA_ISSUANCE, this);
            return _diplomaIssuance;
        }

    /**
     * Номер листа приложения.
     *
     * @return Номер листа. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentIssuance#getContentListNumber()
     */
        public PropertyPath<Integer> contentListNumber()
        {
            if(_contentListNumber == null )
                _contentListNumber = new PropertyPath<Integer>(DiplomaContentIssuanceGen.P_CONTENT_LIST_NUMBER, this);
            return _contentListNumber;
        }

    /**
     * @return Бланк.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentIssuance#getBlank()
     */
        public DipDiplomaBlank.Path<DipDiplomaBlank> blank()
        {
            if(_blank == null )
                _blank = new DipDiplomaBlank.Path<DipDiplomaBlank>(L_BLANK, this);
            return _blank;
        }

    /**
     * Серия бланка, на котором напечатан лист приложения.
     *
     * @return Серия бланка. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentIssuance#getBlankSeria()
     */
        public PropertyPath<String> blankSeria()
        {
            if(_blankSeria == null )
                _blankSeria = new PropertyPath<String>(DiplomaContentIssuanceGen.P_BLANK_SERIA, this);
            return _blankSeria;
        }

    /**
     * Номер бланка, на котором напечатан лист приложения.
     *
     * @return Номер бланка. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentIssuance#getBlankNumber()
     */
        public PropertyPath<String> blankNumber()
        {
            if(_blankNumber == null )
                _blankNumber = new PropertyPath<String>(DiplomaContentIssuanceGen.P_BLANK_NUMBER, this);
            return _blankNumber;
        }

    /**
     * Если выдается дубликат приложения без выдачи дубликата диплома, то в системе нужно создать дубликат приложения в рамках исходного факта выдачи диплома (того, чьи рег. данные будут использованы при печати дубликата приложения).
     *
     * @return Дубликат. Свойство не может быть null.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentIssuance#isDuplicate()
     */
        public PropertyPath<Boolean> duplicate()
        {
            if(_duplicate == null )
                _duplicate = new PropertyPath<Boolean>(DiplomaContentIssuanceGen.P_DUPLICATE, this);
            return _duplicate;
        }

    /**
     * Не заполняется для приложений, не являющихся дубликатами, поскольку в таком случае совпадает с регистрационным номером диплома. Можно не заполнять для приложений-дубликатов, тогда тоже будет взят регистрационный номер диплома.
     *
     * @return Регистрационный номер дубликата.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentIssuance#getDuplicateRegustrationNumber()
     */
        public PropertyPath<String> duplicateRegustrationNumber()
        {
            if(_duplicateRegustrationNumber == null )
                _duplicateRegustrationNumber = new PropertyPath<String>(DiplomaContentIssuanceGen.P_DUPLICATE_REGUSTRATION_NUMBER, this);
            return _duplicateRegustrationNumber;
        }

    /**
     * Не заполняется для приложений, не являющихся дубликатами, поскольку в таком случае совпадает с датой выдачи диплома.
     *
     * @return Дата выдачи дубликата.
     * @see ru.tandemservice.unidip.base.entity.diploma.DiplomaContentIssuance#getDuplicateIssuanceDate()
     */
        public PropertyPath<Date> duplicateIssuanceDate()
        {
            if(_duplicateIssuanceDate == null )
                _duplicateIssuanceDate = new PropertyPath<Date>(DiplomaContentIssuanceGen.P_DUPLICATE_ISSUANCE_DATE, this);
            return _duplicateIssuanceDate;
        }

        public Class getEntityClass()
        {
            return DiplomaContentIssuance.class;
        }

        public String getEntityName()
        {
            return "diplomaContentIssuance";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
