/* $Id$ */
package ru.tandemservice.unidip.base.ext.OrgUnit;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Alexey Lopatin
 * @since 29.05.2015
 */
@Configuration
public class OrgUnitExtManager extends BusinessObjectExtensionManager
{
}
