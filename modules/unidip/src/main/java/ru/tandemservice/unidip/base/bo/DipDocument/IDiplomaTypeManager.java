/* $Id$ */
package ru.tandemservice.unidip.base.bo.DipDocument;

import ru.tandemservice.unidip.base.bo.DipDocument.logic.IDiplomaPrintDao;

/**
 * @author Vasily Zhukov
 * @since 05.05.2011
 */
public interface IDiplomaTypeManager
{
    IDiplomaPrintDao printDao();
}
