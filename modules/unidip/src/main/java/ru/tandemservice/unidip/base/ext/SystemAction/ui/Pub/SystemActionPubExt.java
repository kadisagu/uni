/* $Id:$ */
package ru.tandemservice.unidip.base.ext.SystemAction.ui.Pub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.ui.Pub.SystemActionPub;

/**
 * @author rsizonenko
 * @since 27.02.2015
 */
@Configuration
public class SystemActionPubExt  extends BusinessComponentExtensionManager
{
    public static final String ACTION_PUB_ADDON_NAME = "dipSystemActionPubAddon";

    @Autowired
    private SystemActionPub _systemActionPub;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_systemActionPub.presenterExtPoint())
                .addAddon(uiAddon(ACTION_PUB_ADDON_NAME, DipSystemActionPubAddon.class))
                .create();
    }
}
