/* $Id$ */
package ru.tandemservice.unidip.bso.bo.DiplomaSendFisFrdoReport.logic;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import ru.tandemservice.unidip.bso.entity.blank.DipDiplomaSendFisFrdoReport;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 01.04.2015
 */
public interface IDiplomaSendFisFrdoReportDao
{
    /** Сохраняет отчет вместе с печатной формой. */
    Long createReport(@NotNull DipDiplomaSendFisFrdoReport report, @NotNull DataWrapper eduLevel, List<EduProgramKind> programKindList, List<EduProgramSubject> programSubjectList);
}
