/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unidip.component.listextract.excludeStudents;

import org.tandemframework.core.CoreDateUtils;
import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance;
import ru.tandemservice.unidip.base.entity.order.DipContentIssuanceToDipExtractRelation;
import ru.tandemservice.unidip.base.entity.order.DipStuExcludeExtract;
import ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank;
import ru.tandemservice.unidip.bso.entity.catalog.DipBlankState;
import ru.tandemservice.unidip.bso.entity.catalog.codes.DipBlankStateCodes;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 *
 * @author iolshvang
 * @since 25.07.11 15:50
 */
public class DipStuExcludeExtractDao  extends UniBaseDao implements IExtractComponentDao<DipStuExcludeExtract>
{
    @Override
    public void doCommit(DipStuExcludeExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveListExtractText(extract, MoveStudentDefines.LIST_EXTRACT_TEXT_CODE);

        extract.setStatusOld(extract.getEntity().getStatus());
        extract.getEntity().setStatus(extract.getStatusNew());

        DiplomaIssuance issuance = extract.getIssuance();
        if (issuance != null)
        {
            DipBlankState issued = this.getCatalogItem(DipBlankState.class, DipBlankStateCodes.ISSUED);
            if (issuance.getBlank() != null)
            {
                issuance.getBlank().setBlankState(issued);
                this.update(issuance.getBlank());
            }

            for (DipContentIssuanceToDipExtractRelation rel: this.getList(DipContentIssuanceToDipExtractRelation.class, DipContentIssuanceToDipExtractRelation.extract(), extract))
            {
                DipDiplomaBlank blank = rel.getContentIssuance().getBlank();
                if (blank != null)
                {
                    blank.setBlankState(issued);
                    this.update(blank);
                }
            }
        }

        // заполняем номер и дату приказа
        if (null == extract.getParagraph() || null == extract.getParagraph().getOrder())
            return;
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        } else
        {
            extract.setPrevOrderDate(orderData.getExcludeOrderDate());
            extract.setPrevOrderNumber(orderData.getExcludeOrderNumber());
        }

        extract.setFinishedYear(student.getFinishYear());
        student.setFinishYear(CoreDateUtils.getYear(extract.getParagraph().getOrder().getCommitDate()));

        orderData.setExcludeOrderDate(extract.getParagraph().getOrder().getCommitDate());
        orderData.setExcludeOrderNumber(extract.getParagraph().getOrder().getNumber());
        getSession().saveOrUpdate(orderData);
    }

    @Override
    public void doRollback(DipStuExcludeExtract extract, Map parameters)
    {
        extract.getEntity().setStatus(extract.getStatusOld());

        DiplomaIssuance issuance = extract.getIssuance();
        if (issuance != null)
        {
            DipBlankState linked = this.getCatalogItem(DipBlankState.class, DipBlankStateCodes.LINKED);
            if (issuance.getBlank() != null)
            {
                issuance.getBlank().setBlankState(linked);
                this.update(issuance.getBlank());
            }

            for (DipContentIssuanceToDipExtractRelation rel: this.getList(DipContentIssuanceToDipExtractRelation.class, DipContentIssuanceToDipExtractRelation.extract(), extract))
            {
                DipDiplomaBlank blank = rel.getContentIssuance().getBlank();
                if (blank != null)
                {
                    blank.setBlankState(linked);
                    this.update(blank);
                }
            }
        }

        // возвращаем предыдущие номер и дату приказа
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        }
        student.setFinishYear(extract.getFinishedYear());

        orderData.setExcludeOrderDate(extract.getPrevOrderDate());
        orderData.setExcludeOrderNumber(extract.getPrevOrderNumber());
        getSession().saveOrUpdate(orderData);
    }
}