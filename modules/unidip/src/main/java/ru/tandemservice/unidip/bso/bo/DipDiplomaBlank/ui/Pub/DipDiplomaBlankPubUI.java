/* $Id:$ */
package ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.ui.Pub;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import org.tandemframework.shared.commonbase.utils.DQLSimple;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentIssuance;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaObject;
import ru.tandemservice.unidip.base.entity.order.DipContentIssuanceToDipExtractRelation;
import ru.tandemservice.unidip.base.entity.order.DipStuExcludeExtract;
import ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.DipDiplomaBlankManager;
import ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.ui.DisposalForm.DipDiplomaBlankDisposalForm;
import ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.ui.ReserveForm.DipDiplomaBlankReserveForm;
import ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.ui.StorageLocationChangeForm.DipDiplomaBlankStorageLocationChangeForm;
import ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank;
import ru.tandemservice.unidip.bso.entity.catalog.DipBlankState;
import ru.tandemservice.unidip.bso.entity.catalog.codes.DipBlankStateCodes;
import ru.tandemservice.unimove.entity.catalog.codes.OrderStatesCodes;

import java.util.Collections;

/**
 * @author rsizonenko
 * @since 27.12.2014
 */
@State({@Bind(key = IUIPresenter.PUBLISHER_ID, binding = "blank.id", required = true)})
public class DipDiplomaBlankPubUI extends UIPresenter
{
    private DipDiplomaBlank _blank = new DipDiplomaBlank();
    private DiplomaObject _dipObject;
    private DipStuExcludeExtract _extract = new DipStuExcludeExtract();

    @Override
    public void onComponentRefresh()
    {
        _blank = IUniBaseDao.instance.get().getNotNull(DipDiplomaBlank.class, getBlank().getId());

        DiplomaIssuance dipIssuance = DataAccessServices.dao().get(DiplomaIssuance.class, DiplomaIssuance.blank(), _blank);
        DiplomaContentIssuance dipContentIssuance = DataAccessServices.dao().get(DiplomaContentIssuance.class, DiplomaContentIssuance.blank(), _blank);
        _dipObject = dipIssuance != null ? dipIssuance.getDiplomaObject() : dipContentIssuance != null ? dipContentIssuance.getDiplomaIssuance().getDiplomaObject() : null;

        if (isIssued()) {

            DipStuExcludeExtract extract = new DQLSimple<>(DipStuExcludeExtract.class).top(1)
                    .where(DipStuExcludeExtract.diploma(), _dipObject)
                    .where(DipStuExcludeExtract.committed(), true)
                    .where(DipStuExcludeExtract.paragraph().order().state().code(), OrderStatesCodes.FINISHED)
                    .order(DipStuExcludeExtract.paragraph().order().commitDate(), OrderDirection.desc)
                    .get();

            if (null == extract) {

                final DipContentIssuanceToDipExtractRelation rel = new DQLSimple<>(DipContentIssuanceToDipExtractRelation.class).top(1)
                        .where(DipContentIssuanceToDipExtractRelation.contentIssuance().blank(), _blank)
                        .order(DipContentIssuanceToDipExtractRelation.contentIssuance().diplomaIssuance().issuanceDate(), OrderDirection.desc)
                        .get();

                if (null != rel) {
                    extract = rel.getExtract();
                }
            }

            if (null != extract && extract.isCommitted()) {
                _extract = extract;
            }
        }
    }

    public void onClickReserve()
    {
        this.getActivationBuilder().asRegionDialog(DipDiplomaBlankReserveForm.class).parameter(IUIPresenter.PUBLISHER_ID, getBlank().getId()).activate();
    }

    public void onClickCancelReserve()
    {
        DipDiplomaBlankManager.instance().diplomaBlankDao().doCancelReserve(getBlank().getId());
    }

    public void onClickDispose()
    {
        this.getActivationBuilder()
            .asRegionDialog(DipDiplomaBlankDisposalForm.class)
            .parameter(DipDiplomaBlankManager.KEY_BLANKS, Collections.singletonList(getBlank().getId()))
            .parameter(DipDiplomaBlankManager.KEY_MASS_ACTION, false)
            .activate();
    }

    public void onClickCancelDisposal()
    {
        DipDiplomaBlankManager.instance().diplomaBlankDao().doCancelDisposal(getBlank().getId());
    }

    public void onClickChangeStorageLocation()
    {
        this.getActivationBuilder()
            .asRegionDialog(DipDiplomaBlankStorageLocationChangeForm.class)
            .parameter(DipDiplomaBlankManager.KEY_BLANKS, Collections.singletonList(getBlank().getId()))
            .parameter(DipDiplomaBlankManager.KEY_MASS_ACTION, false)
            .activate();
    }

	public void onClickCancelLink()
	{
		ISharedBaseDao dao = ISharedBaseDao.instance.get();
		DipDiplomaBlank blank = getBlank();
		blank.setBlankState(dao.getCatalogItem(DipBlankState.class, DipBlankStateCodes.FREE));
		dao.update(blank);
	}

    // Util
    public boolean isFree()
    {
        return DipBlankStateCodes.FREE.equals(getBlank().getBlankState().getCode());
    }

    public boolean isReserved()
    {
        return DipBlankStateCodes.RESERVED.equals(getBlank().getBlankState().getCode());
    }

    public boolean isDisposed()
    {
        return DipBlankStateCodes.DISPOSAL.equals(getBlank().getBlankState().getCode());
    }

    public boolean isLinked()
    {
        return DipBlankStateCodes.LINKED.equals(getBlank().getBlankState().getCode());
    }

    public boolean isIssued()
    {
        return DipBlankStateCodes.ISSUED.equals(getBlank().getBlankState().getCode());
    }

    public boolean isLinkedOrIssued()
    {
        String code = getBlank().getBlankState().getCode();
        return DipBlankStateCodes.LINKED.equals(code) || DipBlankStateCodes.ISSUED.equals(code);
    }

	public boolean isPossibleCancelLink()
	{
		if (!isLinked())
			return false;
		ISharedBaseDao dao = ISharedBaseDao.instance.get();
		return !(dao.existsEntity(DiplomaIssuance.class, DiplomaIssuance.L_BLANK, getBlank()) || dao.existsEntity(DiplomaContentIssuance.class, DiplomaContentIssuance.L_BLANK, getBlank()));
	}

    public String getDipObjectTitle()
    {
        if (null == _dipObject) return "Документ отсутствует в системе.";
        return _dipObject.getStudent().getFullFio() + ": " + _dipObject.getContent().getType().getTitle();
    }

    public String getExtractTitle()
    {
        if (null == _extract.getId()) return "Приказ отсутствует в системе.";
        StudentListOrder order = (StudentListOrder) _extract.getParagraph().getOrder();

        StringBuilder title = new StringBuilder();
        if (StringUtils.isNotEmpty(order.getNumber()))
            title.append("№").append(order.getNumber()).append(" ");
        if (null != order.getCommitDate())
            title.append("от ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(order.getCommitDate()));

        return title.toString();
    }

    public DipDiplomaBlank getBlank()
    {
        return _blank;
    }

    public DiplomaObject getDipObject()
    {
        return _dipObject;
    }

    public void setDipObject(DiplomaObject dipObject)
    {
        _dipObject = dipObject;
    }

    public DipStuExcludeExtract getExtract()
    {
        return _extract;
    }

    public void setExtract(DipStuExcludeExtract extract)
    {
        _extract = extract;
    }
}