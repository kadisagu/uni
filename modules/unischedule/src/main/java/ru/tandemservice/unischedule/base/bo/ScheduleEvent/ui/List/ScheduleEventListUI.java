/* $Id:$ */
package ru.tandemservice.unischedule.base.bo.ScheduleEvent.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import ru.tandemservice.uni.dao.IUniBaseDao;

/**
 * @author oleyba
 * @since 6/19/14
 */
public class ScheduleEventListUI extends UIPresenter
{
    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.putAll(this.getSettings().getAsMap(true, ScheduleEventList.BIND_BUILDING, ScheduleEventList.BIND_PLACE, ScheduleEventList.BIND_DURATION_BEGIN, ScheduleEventList.BIND_DURATION_END));
    }

    public void onDeleteEntityFromList()
    {
        IUniBaseDao.instance.get().delete(this.getListenerParameterAsLong());
    }

    public void onClickSearch()
    {
        this.saveSettings();
    }

    public void onClickClear()
    {
        this.clearSettings();
        onClickSearch();
    }
}