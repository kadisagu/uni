/* $Id$ */
package ru.tandemservice.unischedule.base.ext.SystemAction.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import ru.tandemservice.unischedule.base.bo.ScheduleEvent.ui.List.ScheduleEventList;

/**
 * @author azhebko
 * @since 20.06.2014
 */
public class ScheduleSystemActionPubAddon extends UIAddon
{
    public static final String SYSTEM_ACTION_PUB_ADDON_NAME = "unischeduleSystemActionPubAddon";

    public ScheduleSystemActionPubAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public void onClickShowScheduleEventList()
    {
        this.getActivationBuilder().asDesktopRoot(ScheduleEventList.class).activate();
    }
}