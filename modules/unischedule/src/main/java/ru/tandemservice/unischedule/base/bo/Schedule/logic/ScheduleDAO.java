/* $Id$ */
package ru.tandemservice.unischedule.base.bo.Schedule.logic;

import org.apache.commons.collections15.Predicate;
import org.apache.commons.collections15.functors.AllPredicate;
import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unischedule.base.bo.Schedule.ScheduleManager;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBell;

import java.util.Collection;

/**
 * @author Alexey Lopatin
 * @since 29.07.2016
 */
public class ScheduleDAO extends UniBaseDao implements IScheduleDAO
{
    @Override
    public boolean canEditBellsEntriesResult(ScheduleBell bellSchedule)
    {
        ItemListExtPoint<Predicate<ScheduleBell>> extPoint = ScheduleManager.instance().canEditBellScheduleEntriesPredicatesExtPoint();
        if (null != extPoint && null != extPoint.getItems() && null != extPoint.getItems().values() && !extPoint.getItems().values().isEmpty())
        {
            Collection<Predicate<ScheduleBell>> items = extPoint.getItems().values();
            return new AllPredicate(items.toArray(new Predicate[items.size()])).evaluate(bellSchedule);
        } else return true;
    }
}
