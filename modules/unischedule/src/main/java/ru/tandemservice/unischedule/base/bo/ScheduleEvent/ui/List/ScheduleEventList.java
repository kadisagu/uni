/* $Id:$ */
package ru.tandemservice.unischedule.base.bo.ScheduleEvent.ui.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.formatter.NewLineFormatter;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLSelectQuery;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unischedule.base.entity.event.ScheduleEvent;
import ru.tandemservice.unischedule.base.entity.event.ScheduleEventPlace;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 6/19/14
 */
@Configuration
public class ScheduleEventList extends BusinessComponentManager
{
    public static final String DS_SCHEDULE_EVENT = "scheduleEventDS";
    public static final String DS_BUILDING = "buildingDS";
    public static final String DS_PLACE = "placeDS";

    public static final String BIND_BUILDING = "building";
    public static final String BIND_PLACE = "place";
    public static final String BIND_DURATION_BEGIN = "durationBegin";
    public static final String BIND_DURATION_END = "durationEnd";

    public static final String VIEW_PLACES = "places";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(searchListDS(DS_SCHEDULE_EVENT, scheduleEventDSColumns(), scheduleEventDSHandler()))
            .addDataSource(selectDS(DS_BUILDING, buildingDSHandler()))
            .addDataSource(selectDS(DS_PLACE, placeDSHandler()).addColumn(UniplacesPlace.displayableTitle().s()))
            .create();
    }

    @Bean
    public ColumnListExtPoint scheduleEventDSColumns()
    {
        return columnListExtPointBuilder(DS_SCHEDULE_EVENT)
            .addColumn(textColumn("durationBegin", ScheduleEvent.durationBegin()).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).width("15%").order())
            .addColumn(textColumn("durationEnd", ScheduleEvent.durationEnd()).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).width("15%").order())
            .addColumn(textColumn("places", VIEW_PLACES).formatter(NewLineFormatter.NOBR_IN_LINES))
            .addColumn(textColumn("comment", ScheduleEvent.comment()))
            .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER,
                FormattedMessage.with().template(DS_SCHEDULE_EVENT + ".delete.alert")
                    .parameter(ScheduleEvent.durationBegin(), DateFormatter.DATE_FORMATTER_WITH_TIME)
                    .parameter(ScheduleEvent.durationEnd(), DateFormatter.DATE_FORMATTER_WITH_TIME)
                    .create()).permissionKey("deleteScheduleEvent_list"))
            .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler scheduleEventDSHandler()
    {
        return new DefaultSearchDataSourceHandler(this.getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                Long building = context.get(BIND_BUILDING);
                Long place = context.get(BIND_PLACE);
                Date durationBegin = context.get(BIND_DURATION_BEGIN);
                Date durationEnd = context.get(BIND_DURATION_END);

                IDQLSelectQuery scheduleEventIds = new DQLSelectBuilder()
                    .fromEntity(ScheduleEvent.class, "event")
                    .column(property("event", ScheduleEvent.id()))
                    .where(exists(
                        new DQLSelectBuilder()
                            .fromEntity(ScheduleEventPlace.class, "p")
                            .where(eq(property("p", ScheduleEventPlace.scheduleEvent().id()), property("event", ScheduleEvent.id())))
                            .where(building == null ? null : eq(property("p", ScheduleEventPlace.place().floor().unit().building().id()), value(building)))
                            .where(place == null ? null : eq(property("p", ScheduleEventPlace.place().id()), value(place)))
                            .buildQuery()))
                    .where(durationBegin == null ? null : ge(property("event", ScheduleEvent.durationBegin()), valueTimestamp(durationBegin)))
                    .where(durationEnd == null ? null : le(property("event", ScheduleEvent.durationEnd()), valueTime(durationEnd)))
                    .buildQuery();

                Map<Long, Set<String>> scheduleEventPlaceMap = SafeMap.get(TreeSet.class);
                DQLSelectBuilder scheduleEventPlaceBuilder = new DQLSelectBuilder()
                    .fromEntity(ScheduleEventPlace.class, "sep")
                    .column(property("sep"))
                    .where(in(property("sep", ScheduleEventPlace.scheduleEvent().id()), scheduleEventIds));

                for (ScheduleEventPlace scheduleEventPlace: scheduleEventPlaceBuilder.createStatement(context.getSession()).<ScheduleEventPlace>list())
                    scheduleEventPlaceMap.get(scheduleEventPlace.getScheduleEvent().getId()).add(scheduleEventPlace.getPlace().getDisplayableTitle() + " - " + scheduleEventPlace.getPlace().getFullLocationInfo());

                DQLOrderDescriptionRegistry registry = new DQLOrderDescriptionRegistry(ScheduleEvent.class, "e");
                DQLSelectBuilder resultBuilder = registry.buildDQLSelectBuilder()
                    .where(in(property("e", ScheduleEvent.id()), scheduleEventIds));

                registry.setOrders("durationBegin", new OrderDescription("e", ScheduleEvent.durationBegin()));
                registry.setOrders("durationEnd", new OrderDescription("e", ScheduleEvent.durationEnd()));

                registry.applyOrder(resultBuilder, input.getEntityOrder());

                List<ViewWrapper<ScheduleEvent>> result = new ArrayList<>();
                for (ScheduleEvent event: resultBuilder.createStatement(context.getSession()).<ScheduleEvent>list())
                {
                    ViewWrapper<ScheduleEvent> wrapper = new ViewWrapper<>(event);
                    wrapper.setViewProperty(VIEW_PLACES, StringUtils.join(scheduleEventPlaceMap.get(wrapper.getId()), '\n'));
                    result.add(wrapper);
                }

                return ListOutputBuilder.get(input, result).pageable(true).build();
            }
        };
    }

    @Bean
    public IDefaultComboDataSourceHandler buildingDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), UniplacesBuilding.class)
            .filter(UniplacesBuilding.title())
            .order(UniplacesBuilding.title())
            .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler placeDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), UniplacesPlace.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                dql.where(exists(new DQLSelectBuilder()
                    .fromEntity(ScheduleEventPlace.class, "sep")
                    .where(eq(property("sep", ScheduleEventPlace.place().id()), property(alias, UniplacesPlace.id())))
                    .buildQuery()));
            }
        }
            .where(UniplacesPlace.floor().unit().building(), BIND_BUILDING, true)
            .filter(UniplacesPlace.purpose().shortTitle())
            .filter(UniplacesPlace.title())
            .order(UniplacesPlace.purpose().shortTitle())
            .order(UniplacesPlace.title())
            .pageable(true);
    }
}