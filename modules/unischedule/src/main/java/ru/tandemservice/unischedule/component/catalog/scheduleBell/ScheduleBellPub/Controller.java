/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unischedule.component.catalog.scheduleBell.ScheduleBellPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.unischedule.base.bo.Schedule.ui.BellEntriesEdit.ScheduleBellEntriesEdit;
import ru.tandemservice.unischedule.base.bo.Schedule.ui.BellEntriesEdit.ScheduleBellEntriesEditUI;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBell;

/**
 * @author AutoGenerator
 * Created on 31.08.2011 
 */
public class Controller extends DefaultCatalogPubController<ScheduleBell, Model, IDAO>
{
    @Override
    protected DynamicListDataSource<ScheduleBell> createListDataSource(IBusinessComponent context)
    {
        Model model = getModel(context);

        DynamicListDataSource<ScheduleBell> dataSource = new DynamicListDataSource<>(context, this);
        dataSource.addColumn(getCatalogItemLinkColumn(model, "Название", ScheduleBell.P_TITLE));
        dataSource.addColumn(new ActionColumn("Редактировать пары", "card", "onClickEditEntries").setPermissionKey(model.getCatalogItemEdit()));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem").setPermissionKey(model.getCatalogItemEdit()));
        if (model.isUserCatalog())
            dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteItem", "Удалить элемент «{0}» из справочника?", ScheduleBell.P_TITLE).setPermissionKey(model.getCatalogItemDelete()).setDisableHandler(model.getDisabledEntityHandler()));
        return dataSource;
    }

    public void onClickEditEntries(IBusinessComponent component)
    {
        component.createDefaultChildRegion(
                new ComponentActivator(ScheduleBellEntriesEdit.class.getSimpleName(), new ParametersMap().add(ScheduleBellEntriesEditUI.PARAM_BELL_SCHEDULE_ID, component.getListenerParameter()))
        );
    }
}
