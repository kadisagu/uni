/* $Id$ */
package ru.tandemservice.unischedule.rule.condition.schedulebell;

import org.apache.commons.collections15.Predicate;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBell;

/**
 * @author Alexey Lopatin
 * @since 28.07.2016
 */
public class CanEditBellScheduleEntriesPredicate implements Predicate<ScheduleBell>
{
    @Override
    public boolean evaluate(ScheduleBell bellSchedule)
    {
        return true;
    }
}