package ru.tandemservice.unischedule.catalog.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBell;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Звонковое расписание
 *
 * Звонковое расписание. Настройка времени пар - т.е. набор пар, для которых задано время начала и окончания, и номер по порядку.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ScheduleBellGen extends EntityBase
 implements INaturalIdentifiable<ScheduleBellGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unischedule.catalog.entity.ScheduleBell";
    public static final String ENTITY_NAME = "scheduleBell";
    public static final int VERSION_HASH = 2108921330;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_PERIOD_DURATION = "periodDuration";
    public static final String P_ACTIVE = "active";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private int _periodDuration = 2;     // Продолжительность занятия
    private boolean _active;     // Используется
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Продолжительность занятия. Свойство не может быть null.
     */
    @NotNull
    public int getPeriodDuration()
    {
        return _periodDuration;
    }

    /**
     * @param periodDuration Продолжительность занятия. Свойство не может быть null.
     */
    public void setPeriodDuration(int periodDuration)
    {
        dirty(_periodDuration, periodDuration);
        _periodDuration = periodDuration;
    }

    /**
     * @return Используется. Свойство не может быть null.
     */
    @NotNull
    public boolean isActive()
    {
        return _active;
    }

    /**
     * @param active Используется. Свойство не может быть null.
     */
    public void setActive(boolean active)
    {
        dirty(_active, active);
        _active = active;
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ScheduleBellGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((ScheduleBell)another).getCode());
            }
            setPeriodDuration(((ScheduleBell)another).getPeriodDuration());
            setActive(((ScheduleBell)another).isActive());
            setTitle(((ScheduleBell)another).getTitle());
        }
    }

    public INaturalId<ScheduleBellGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<ScheduleBellGen>
    {
        private static final String PROXY_NAME = "ScheduleBellNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof ScheduleBellGen.NaturalId) ) return false;

            ScheduleBellGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ScheduleBellGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ScheduleBell.class;
        }

        public T newInstance()
        {
            return (T) new ScheduleBell();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "periodDuration":
                    return obj.getPeriodDuration();
                case "active":
                    return obj.isActive();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "periodDuration":
                    obj.setPeriodDuration((Integer) value);
                    return;
                case "active":
                    obj.setActive((Boolean) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "periodDuration":
                        return true;
                case "active":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "periodDuration":
                    return true;
                case "active":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "periodDuration":
                    return Integer.class;
                case "active":
                    return Boolean.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ScheduleBell> _dslPath = new Path<ScheduleBell>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ScheduleBell");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unischedule.catalog.entity.ScheduleBell#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Продолжительность занятия. Свойство не может быть null.
     * @see ru.tandemservice.unischedule.catalog.entity.ScheduleBell#getPeriodDuration()
     */
    public static PropertyPath<Integer> periodDuration()
    {
        return _dslPath.periodDuration();
    }

    /**
     * @return Используется. Свойство не может быть null.
     * @see ru.tandemservice.unischedule.catalog.entity.ScheduleBell#isActive()
     */
    public static PropertyPath<Boolean> active()
    {
        return _dslPath.active();
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.unischedule.catalog.entity.ScheduleBell#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends ScheduleBell> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<Integer> _periodDuration;
        private PropertyPath<Boolean> _active;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unischedule.catalog.entity.ScheduleBell#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(ScheduleBellGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Продолжительность занятия. Свойство не может быть null.
     * @see ru.tandemservice.unischedule.catalog.entity.ScheduleBell#getPeriodDuration()
     */
        public PropertyPath<Integer> periodDuration()
        {
            if(_periodDuration == null )
                _periodDuration = new PropertyPath<Integer>(ScheduleBellGen.P_PERIOD_DURATION, this);
            return _periodDuration;
        }

    /**
     * @return Используется. Свойство не может быть null.
     * @see ru.tandemservice.unischedule.catalog.entity.ScheduleBell#isActive()
     */
        public PropertyPath<Boolean> active()
        {
            if(_active == null )
                _active = new PropertyPath<Boolean>(ScheduleBellGen.P_ACTIVE, this);
            return _active;
        }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.unischedule.catalog.entity.ScheduleBell#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(ScheduleBellGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return ScheduleBell.class;
        }

        public String getEntityName()
        {
            return "scheduleBell";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
