package ru.tandemservice.unischedule.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unischedule_2x11x1_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.11.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность scheduleBell

		// создано обязательное свойство periodDuration
		{
			// создать колонку
			tool.createColumn("sc_bell", new DBColumn("periodduration_p", DBType.INTEGER));

			// задать значение по умолчанию
			java.lang.Integer defaultPeriodDuration = 2;
			tool.executeUpdate("update sc_bell set periodduration_p=? where periodduration_p is null", defaultPeriodDuration);

			// сделать колонку NOT NULL
			tool.setColumnNullable("sc_bell", "periodduration_p", false);
		}
    }
}
