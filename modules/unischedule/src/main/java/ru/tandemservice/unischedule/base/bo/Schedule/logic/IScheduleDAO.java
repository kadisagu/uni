package ru.tandemservice.unischedule.base.bo.Schedule.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBell;

/**
 * @author Alexey Lopatin
 * @since 29.07.2016
 */
public interface IScheduleDAO extends INeedPersistenceSupport
{
    boolean canEditBellsEntriesResult(ScheduleBell bellSchedule);
}
