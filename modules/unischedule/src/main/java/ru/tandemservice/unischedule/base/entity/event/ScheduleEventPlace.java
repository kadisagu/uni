package ru.tandemservice.unischedule.base.entity.event;

import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unischedule.base.entity.event.gen.ScheduleEventPlaceGen;

/**
 * Помещение, в котором проводится событие
 *
 * Указывает какие помещения занимает указанное событие
 */
public class ScheduleEventPlace extends ScheduleEventPlaceGen
{
    public ScheduleEventPlace() {}

    public ScheduleEventPlace(UniplacesPlace place, ScheduleEvent event)
    {
        super();
        setPlace(place);
        setScheduleEvent(event);
    }
}