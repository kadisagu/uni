/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unischedule.component.catalog.scheduleBell.ScheduleBellItemPub;

import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogItemPub.DefaultCatalogItemPubDAO;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBell;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry;

/**
 * @author AutoGenerator
 * Created on 31.08.2011
 */
public class DAO extends DefaultCatalogItemPubDAO<ScheduleBell, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        StaticListDataSource<ScheduleBellEntry> dataSource = new StaticListDataSource<>();
        dataSource.addColumn(new SimpleColumn("Номер пары", ScheduleBellEntry.titleWithoutTime().s()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Время", ScheduleBellEntry.time().s()).setOrderable(false));
        dataSource.setupRows(getList(ScheduleBellEntry.class, ScheduleBellEntry.schedule().s(), model.getCatalogItem(), ScheduleBellEntry.number().s()));

        model.setDataSource(dataSource);
    }
}
