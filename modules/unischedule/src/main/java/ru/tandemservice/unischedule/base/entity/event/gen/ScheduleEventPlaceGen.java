package ru.tandemservice.unischedule.base.entity.event.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unischedule.base.entity.event.ScheduleEvent;
import ru.tandemservice.unischedule.base.entity.event.ScheduleEventPlace;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Помещение, в котором проводится событие
 *
 * Указывает какие помещения занимает указанное событие
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ScheduleEventPlaceGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unischedule.base.entity.event.ScheduleEventPlace";
    public static final String ENTITY_NAME = "scheduleEventPlace";
    public static final int VERSION_HASH = 347840013;
    private static IEntityMeta ENTITY_META;

    public static final String L_SCHEDULE_EVENT = "scheduleEvent";
    public static final String L_PLACE = "place";
    public static final String P_COMMENT = "comment";

    private ScheduleEvent _scheduleEvent;     // Событие
    private UniplacesPlace _place;     // Помещение
    private String _comment;     // Комментарий

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Событие. Свойство не может быть null.
     */
    @NotNull
    public ScheduleEvent getScheduleEvent()
    {
        return _scheduleEvent;
    }

    /**
     * @param scheduleEvent Событие. Свойство не может быть null.
     */
    public void setScheduleEvent(ScheduleEvent scheduleEvent)
    {
        dirty(_scheduleEvent, scheduleEvent);
        _scheduleEvent = scheduleEvent;
    }

    /**
     * @return Помещение. Свойство не может быть null.
     */
    @NotNull
    public UniplacesPlace getPlace()
    {
        return _place;
    }

    /**
     * @param place Помещение. Свойство не может быть null.
     */
    public void setPlace(UniplacesPlace place)
    {
        dirty(_place, place);
        _place = place;
    }

    /**
     * @return Комментарий.
     */
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ScheduleEventPlaceGen)
        {
            setScheduleEvent(((ScheduleEventPlace)another).getScheduleEvent());
            setPlace(((ScheduleEventPlace)another).getPlace());
            setComment(((ScheduleEventPlace)another).getComment());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ScheduleEventPlaceGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ScheduleEventPlace.class;
        }

        public T newInstance()
        {
            return (T) new ScheduleEventPlace();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "scheduleEvent":
                    return obj.getScheduleEvent();
                case "place":
                    return obj.getPlace();
                case "comment":
                    return obj.getComment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "scheduleEvent":
                    obj.setScheduleEvent((ScheduleEvent) value);
                    return;
                case "place":
                    obj.setPlace((UniplacesPlace) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "scheduleEvent":
                        return true;
                case "place":
                        return true;
                case "comment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "scheduleEvent":
                    return true;
                case "place":
                    return true;
                case "comment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "scheduleEvent":
                    return ScheduleEvent.class;
                case "place":
                    return UniplacesPlace.class;
                case "comment":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ScheduleEventPlace> _dslPath = new Path<ScheduleEventPlace>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ScheduleEventPlace");
    }
            

    /**
     * @return Событие. Свойство не может быть null.
     * @see ru.tandemservice.unischedule.base.entity.event.ScheduleEventPlace#getScheduleEvent()
     */
    public static ScheduleEvent.Path<ScheduleEvent> scheduleEvent()
    {
        return _dslPath.scheduleEvent();
    }

    /**
     * @return Помещение. Свойство не может быть null.
     * @see ru.tandemservice.unischedule.base.entity.event.ScheduleEventPlace#getPlace()
     */
    public static UniplacesPlace.Path<UniplacesPlace> place()
    {
        return _dslPath.place();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unischedule.base.entity.event.ScheduleEventPlace#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    public static class Path<E extends ScheduleEventPlace> extends EntityPath<E>
    {
        private ScheduleEvent.Path<ScheduleEvent> _scheduleEvent;
        private UniplacesPlace.Path<UniplacesPlace> _place;
        private PropertyPath<String> _comment;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Событие. Свойство не может быть null.
     * @see ru.tandemservice.unischedule.base.entity.event.ScheduleEventPlace#getScheduleEvent()
     */
        public ScheduleEvent.Path<ScheduleEvent> scheduleEvent()
        {
            if(_scheduleEvent == null )
                _scheduleEvent = new ScheduleEvent.Path<ScheduleEvent>(L_SCHEDULE_EVENT, this);
            return _scheduleEvent;
        }

    /**
     * @return Помещение. Свойство не может быть null.
     * @see ru.tandemservice.unischedule.base.entity.event.ScheduleEventPlace#getPlace()
     */
        public UniplacesPlace.Path<UniplacesPlace> place()
        {
            if(_place == null )
                _place = new UniplacesPlace.Path<UniplacesPlace>(L_PLACE, this);
            return _place;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unischedule.base.entity.event.ScheduleEventPlace#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(ScheduleEventPlaceGen.P_COMMENT, this);
            return _comment;
        }

        public Class getEntityClass()
        {
            return ScheduleEventPlace.class;
        }

        public String getEntityName()
        {
            return "scheduleEventPlace";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
