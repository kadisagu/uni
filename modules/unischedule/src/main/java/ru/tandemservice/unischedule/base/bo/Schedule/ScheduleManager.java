/* $Id$ */
package ru.tandemservice.unischedule.base.bo.Schedule;

import org.apache.commons.collections15.Predicate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;
import ru.tandemservice.unischedule.base.bo.Schedule.logic.IScheduleDAO;
import ru.tandemservice.unischedule.base.bo.Schedule.logic.ScheduleDAO;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBell;
import ru.tandemservice.unischedule.rule.condition.schedulebell.CanEditBellScheduleEntriesPredicate;

/**
 * @author Alexey Lopatin
 * @since 28.07.2016
 */
@Configuration
public class ScheduleManager extends BusinessObjectManager
{
    public static final String CAN_EDIT_BELL_SCHEDULE_ENTRIES_COND = CanEditBellScheduleEntriesPredicate.class.getSimpleName();

    public static ScheduleManager instance()
    {
        return instance(ScheduleManager.class);
    }

    @Bean
    public IScheduleDAO dao()
    {
        return new ScheduleDAO();
    }

    @SuppressWarnings("unchecked")
    @Bean
    public ItemListExtPoint<Predicate<ScheduleBell>> canEditBellScheduleEntriesPredicatesExtPoint()
    {
        return (ItemListExtPoint) itemList(Predicate.class)
                .add(CAN_EDIT_BELL_SCHEDULE_ENTRIES_COND, new CanEditBellScheduleEntriesPredicate())
                .create();
    }
}
