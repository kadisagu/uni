/* $Id$ */
package ru.tandemservice.unischedule.util;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectValueStyle;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import org.tandemframework.tapsupport.component.selection.hselect.IHSelectModel;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBell;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry;

import java.util.*;

/**
 * @author oleyba
 * @since 9/2/11
 */
public class SppBellScheduleModel implements IHSelectModel, ISingleSelectModel, IMultiSelectModel
{
    public SppBellScheduleModel()
    {
    }

    @Override
    public int getLevel(Object value)
    {
        return value instanceof ScheduleBell ? 0 : 1;
    }

    @Override
    @Deprecated
    public boolean isSelectable(Object value)
    {
        return (value instanceof ScheduleBellEntry);
    }

    @Override
    public Object getRawValue(Object value)
    {
        return value;
    }

    @Override
    public Object getPackedValue(Object rawValue)
    {
        return rawValue;
    }

    @Override
    public List getValues(Set primaryKeys)
    {
        List<Object> result = new ArrayList<>();
        for (Object key : primaryKeys)
        {
            final Object value = getValue(key);
            if (null != value)
                result.add(value);
        }
        return result;
    }

    @Override
    public Object getValue(Object primaryKey)
    {
        if (!(primaryKey instanceof Long))
            return null;
        final IEntity entity = ISharedBaseDao.instance.get().get((Long) primaryKey);
        if (entity instanceof ScheduleBellEntry || entity instanceof ScheduleBell)
            return entity;
        return null;
    }

    @Override
    public Object getPrimaryKey(Object value)
    {
        if (value instanceof IEntity)
            return ((IEntity) value).getId();
        return null;
    }

    @Override
    public ListResult findValues(String filter)
    {
        Map<ScheduleBell, List<ScheduleBellEntry>> map = new LinkedHashMap<>();
        for (ScheduleBellEntry entry : ISharedBaseDao.instance.get().getList(ScheduleBellEntry.class, ScheduleBellEntry.schedule().active(), Boolean.TRUE, ScheduleBellEntry.schedule().title().s(), ScheduleBellEntry.startTime().s()))
            SafeMap.safeGet(map, entry.getSchedule(), ArrayList.class).add(entry);
        List<Object> options = new ArrayList<>();
        for (ScheduleBell sch : map.keySet())
        {
            options.add(sch);
            options.addAll(map.get(sch));
        }
        return new ListResult<>(options);
    }

    @Override
    public int getColumnCount()
    {
        return 1;
    }

    @Override
    public String[] getColumnTitles()
    {
        return new String[0];
    }

    @Override
    public String getLabelFor(Object value, int columnIndex)
    {
        if (value instanceof ITitled)
            return ((ITitled) value).getTitle();
        return null;
    }

    @Override
    public String getFullLabel(Object value)
    {
        return getLabelFor(value, 0);
    }

    @Override
    public ISelectValueStyle getValueStyle(Object value)
    {
        return null;
    }
}
