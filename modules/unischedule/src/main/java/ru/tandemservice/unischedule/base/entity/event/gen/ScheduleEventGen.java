package ru.tandemservice.unischedule.base.entity.event.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unischedule.base.entity.event.ScheduleEvent;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Событие
 *
 * Указывает, что в определенное время произойдет указанное событие
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ScheduleEventGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unischedule.base.entity.event.ScheduleEvent";
    public static final String ENTITY_NAME = "scheduleEvent";
    public static final int VERSION_HASH = 1372173325;
    private static IEntityMeta ENTITY_META;

    public static final String P_DURATION_BEGIN = "durationBegin";
    public static final String P_DURATION_END = "durationEnd";
    public static final String P_COMMENT = "comment";
    public static final String P_BEGIN_DATE_STR = "beginDateStr";
    public static final String P_DATE_WITH_DAY_OF_WEEK = "dateWithDayOfWeek";
    public static final String P_END_DATE_STR = "endDateStr";
    public static final String P_FULL_TIME_STR = "fullTimeStr";
    public static final String P_TIME_PERIOD_STR = "timePeriodStr";

    private Date _durationBegin;     // Дата начала
    private Date _durationEnd;     // Дата окончания
    private String _comment;     // Комментарий

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата начала. Свойство не может быть null.
     */
    @NotNull
    public Date getDurationBegin()
    {
        return _durationBegin;
    }

    /**
     * @param durationBegin Дата начала. Свойство не может быть null.
     */
    public void setDurationBegin(Date durationBegin)
    {
        dirty(_durationBegin, durationBegin);
        _durationBegin = durationBegin;
    }

    /**
     * @return Дата окончания. Свойство не может быть null.
     */
    @NotNull
    public Date getDurationEnd()
    {
        return _durationEnd;
    }

    /**
     * @param durationEnd Дата окончания. Свойство не может быть null.
     */
    public void setDurationEnd(Date durationEnd)
    {
        dirty(_durationEnd, durationEnd);
        _durationEnd = durationEnd;
    }

    /**
     * @return Комментарий.
     */
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ScheduleEventGen)
        {
            setDurationBegin(((ScheduleEvent)another).getDurationBegin());
            setDurationEnd(((ScheduleEvent)another).getDurationEnd());
            setComment(((ScheduleEvent)another).getComment());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ScheduleEventGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ScheduleEvent.class;
        }

        public T newInstance()
        {
            return (T) new ScheduleEvent();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "durationBegin":
                    return obj.getDurationBegin();
                case "durationEnd":
                    return obj.getDurationEnd();
                case "comment":
                    return obj.getComment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "durationBegin":
                    obj.setDurationBegin((Date) value);
                    return;
                case "durationEnd":
                    obj.setDurationEnd((Date) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "durationBegin":
                        return true;
                case "durationEnd":
                        return true;
                case "comment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "durationBegin":
                    return true;
                case "durationEnd":
                    return true;
                case "comment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "durationBegin":
                    return Date.class;
                case "durationEnd":
                    return Date.class;
                case "comment":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ScheduleEvent> _dslPath = new Path<ScheduleEvent>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ScheduleEvent");
    }
            

    /**
     * @return Дата начала. Свойство не может быть null.
     * @see ru.tandemservice.unischedule.base.entity.event.ScheduleEvent#getDurationBegin()
     */
    public static PropertyPath<Date> durationBegin()
    {
        return _dslPath.durationBegin();
    }

    /**
     * @return Дата окончания. Свойство не может быть null.
     * @see ru.tandemservice.unischedule.base.entity.event.ScheduleEvent#getDurationEnd()
     */
    public static PropertyPath<Date> durationEnd()
    {
        return _dslPath.durationEnd();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unischedule.base.entity.event.ScheduleEvent#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unischedule.base.entity.event.ScheduleEvent#getBeginDateStr()
     */
    public static SupportedPropertyPath<String> beginDateStr()
    {
        return _dslPath.beginDateStr();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unischedule.base.entity.event.ScheduleEvent#getDateWithDayOfWeek()
     */
    public static SupportedPropertyPath<String> dateWithDayOfWeek()
    {
        return _dslPath.dateWithDayOfWeek();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unischedule.base.entity.event.ScheduleEvent#getEndDateStr()
     */
    public static SupportedPropertyPath<String> endDateStr()
    {
        return _dslPath.endDateStr();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unischedule.base.entity.event.ScheduleEvent#getFullTimeStr()
     */
    public static SupportedPropertyPath<String> fullTimeStr()
    {
        return _dslPath.fullTimeStr();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unischedule.base.entity.event.ScheduleEvent#getTimePeriodStr()
     */
    public static SupportedPropertyPath<String> timePeriodStr()
    {
        return _dslPath.timePeriodStr();
    }

    public static class Path<E extends ScheduleEvent> extends EntityPath<E>
    {
        private PropertyPath<Date> _durationBegin;
        private PropertyPath<Date> _durationEnd;
        private PropertyPath<String> _comment;
        private SupportedPropertyPath<String> _beginDateStr;
        private SupportedPropertyPath<String> _dateWithDayOfWeek;
        private SupportedPropertyPath<String> _endDateStr;
        private SupportedPropertyPath<String> _fullTimeStr;
        private SupportedPropertyPath<String> _timePeriodStr;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата начала. Свойство не может быть null.
     * @see ru.tandemservice.unischedule.base.entity.event.ScheduleEvent#getDurationBegin()
     */
        public PropertyPath<Date> durationBegin()
        {
            if(_durationBegin == null )
                _durationBegin = new PropertyPath<Date>(ScheduleEventGen.P_DURATION_BEGIN, this);
            return _durationBegin;
        }

    /**
     * @return Дата окончания. Свойство не может быть null.
     * @see ru.tandemservice.unischedule.base.entity.event.ScheduleEvent#getDurationEnd()
     */
        public PropertyPath<Date> durationEnd()
        {
            if(_durationEnd == null )
                _durationEnd = new PropertyPath<Date>(ScheduleEventGen.P_DURATION_END, this);
            return _durationEnd;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unischedule.base.entity.event.ScheduleEvent#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(ScheduleEventGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unischedule.base.entity.event.ScheduleEvent#getBeginDateStr()
     */
        public SupportedPropertyPath<String> beginDateStr()
        {
            if(_beginDateStr == null )
                _beginDateStr = new SupportedPropertyPath<String>(ScheduleEventGen.P_BEGIN_DATE_STR, this);
            return _beginDateStr;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unischedule.base.entity.event.ScheduleEvent#getDateWithDayOfWeek()
     */
        public SupportedPropertyPath<String> dateWithDayOfWeek()
        {
            if(_dateWithDayOfWeek == null )
                _dateWithDayOfWeek = new SupportedPropertyPath<String>(ScheduleEventGen.P_DATE_WITH_DAY_OF_WEEK, this);
            return _dateWithDayOfWeek;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unischedule.base.entity.event.ScheduleEvent#getEndDateStr()
     */
        public SupportedPropertyPath<String> endDateStr()
        {
            if(_endDateStr == null )
                _endDateStr = new SupportedPropertyPath<String>(ScheduleEventGen.P_END_DATE_STR, this);
            return _endDateStr;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unischedule.base.entity.event.ScheduleEvent#getFullTimeStr()
     */
        public SupportedPropertyPath<String> fullTimeStr()
        {
            if(_fullTimeStr == null )
                _fullTimeStr = new SupportedPropertyPath<String>(ScheduleEventGen.P_FULL_TIME_STR, this);
            return _fullTimeStr;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unischedule.base.entity.event.ScheduleEvent#getTimePeriodStr()
     */
        public SupportedPropertyPath<String> timePeriodStr()
        {
            if(_timePeriodStr == null )
                _timePeriodStr = new SupportedPropertyPath<String>(ScheduleEventGen.P_TIME_PERIOD_STR, this);
            return _timePeriodStr;
        }

        public Class getEntityClass()
        {
            return ScheduleEvent.class;
        }

        public String getEntityName()
        {
            return "scheduleEvent";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getBeginDateStr();

    public abstract String getDateWithDayOfWeek();

    public abstract String getEndDateStr();

    public abstract String getFullTimeStr();

    public abstract String getTimePeriodStr();
}
