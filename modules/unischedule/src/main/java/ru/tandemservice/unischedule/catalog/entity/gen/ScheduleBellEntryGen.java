package ru.tandemservice.unischedule.catalog.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBell;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Пара звонкового расписания
 *
 * Пара звонкового расписания. Хранит настройку времени начала и конца пары с некоторым номером, в минутах от начала дня.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ScheduleBellEntryGen extends EntityBase
 implements INaturalIdentifiable<ScheduleBellEntryGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry";
    public static final String ENTITY_NAME = "scheduleBellEntry";
    public static final int VERSION_HASH = 1490969251;
    private static IEntityMeta ENTITY_META;

    public static final String L_SCHEDULE = "schedule";
    public static final String P_NUMBER = "number";
    public static final String P_START_TIME = "startTime";
    public static final String P_END_TIME = "endTime";
    public static final String P_TIME = "time";
    public static final String P_TITLE = "title";
    public static final String P_TITLE_WITHOUT_TIME = "titleWithoutTime";

    private ScheduleBell _schedule;     // Звонковое расписание
    private int _number;     // Номер
    private int _startTime;     // Время начала (в минутах)
    private int _endTime;     // Время окончания (в минутах)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Звонковое расписание. Свойство не может быть null.
     */
    @NotNull
    public ScheduleBell getSchedule()
    {
        return _schedule;
    }

    /**
     * @param schedule Звонковое расписание. Свойство не может быть null.
     */
    public void setSchedule(ScheduleBell schedule)
    {
        dirty(_schedule, schedule);
        _schedule = schedule;
    }

    /**
     * @return Номер. Свойство не может быть null.
     */
    @NotNull
    public int getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер. Свойство не может быть null.
     */
    public void setNumber(int number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Время начала (в минутах). Свойство не может быть null.
     */
    @NotNull
    public int getStartTime()
    {
        return _startTime;
    }

    /**
     * @param startTime Время начала (в минутах). Свойство не может быть null.
     */
    public void setStartTime(int startTime)
    {
        dirty(_startTime, startTime);
        _startTime = startTime;
    }

    /**
     * @return Время окончания (в минутах). Свойство не может быть null.
     */
    @NotNull
    public int getEndTime()
    {
        return _endTime;
    }

    /**
     * @param endTime Время окончания (в минутах). Свойство не может быть null.
     */
    public void setEndTime(int endTime)
    {
        dirty(_endTime, endTime);
        _endTime = endTime;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ScheduleBellEntryGen)
        {
            if (withNaturalIdProperties)
            {
                setSchedule(((ScheduleBellEntry)another).getSchedule());
                setNumber(((ScheduleBellEntry)another).getNumber());
            }
            setStartTime(((ScheduleBellEntry)another).getStartTime());
            setEndTime(((ScheduleBellEntry)another).getEndTime());
        }
    }

    public INaturalId<ScheduleBellEntryGen> getNaturalId()
    {
        return new NaturalId(getSchedule(), getNumber());
    }

    public static class NaturalId extends NaturalIdBase<ScheduleBellEntryGen>
    {
        private static final String PROXY_NAME = "ScheduleBellEntryNaturalProxy";

        private Long _schedule;
        private int _number;

        public NaturalId()
        {}

        public NaturalId(ScheduleBell schedule, int number)
        {
            _schedule = ((IEntity) schedule).getId();
            _number = number;
        }

        public Long getSchedule()
        {
            return _schedule;
        }

        public void setSchedule(Long schedule)
        {
            _schedule = schedule;
        }

        public int getNumber()
        {
            return _number;
        }

        public void setNumber(int number)
        {
            _number = number;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof ScheduleBellEntryGen.NaturalId) ) return false;

            ScheduleBellEntryGen.NaturalId that = (NaturalId) o;

            if( !equals(getSchedule(), that.getSchedule()) ) return false;
            if( !equals(getNumber(), that.getNumber()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getSchedule());
            result = hashCode(result, getNumber());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getSchedule());
            sb.append("/");
            sb.append(getNumber());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ScheduleBellEntryGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ScheduleBellEntry.class;
        }

        public T newInstance()
        {
            return (T) new ScheduleBellEntry();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "schedule":
                    return obj.getSchedule();
                case "number":
                    return obj.getNumber();
                case "startTime":
                    return obj.getStartTime();
                case "endTime":
                    return obj.getEndTime();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "schedule":
                    obj.setSchedule((ScheduleBell) value);
                    return;
                case "number":
                    obj.setNumber((Integer) value);
                    return;
                case "startTime":
                    obj.setStartTime((Integer) value);
                    return;
                case "endTime":
                    obj.setEndTime((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "schedule":
                        return true;
                case "number":
                        return true;
                case "startTime":
                        return true;
                case "endTime":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "schedule":
                    return true;
                case "number":
                    return true;
                case "startTime":
                    return true;
                case "endTime":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "schedule":
                    return ScheduleBell.class;
                case "number":
                    return Integer.class;
                case "startTime":
                    return Integer.class;
                case "endTime":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ScheduleBellEntry> _dslPath = new Path<ScheduleBellEntry>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ScheduleBellEntry");
    }
            

    /**
     * @return Звонковое расписание. Свойство не может быть null.
     * @see ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry#getSchedule()
     */
    public static ScheduleBell.Path<ScheduleBell> schedule()
    {
        return _dslPath.schedule();
    }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry#getNumber()
     */
    public static PropertyPath<Integer> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Время начала (в минутах). Свойство не может быть null.
     * @see ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry#getStartTime()
     */
    public static PropertyPath<Integer> startTime()
    {
        return _dslPath.startTime();
    }

    /**
     * @return Время окончания (в минутах). Свойство не может быть null.
     * @see ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry#getEndTime()
     */
    public static PropertyPath<Integer> endTime()
    {
        return _dslPath.endTime();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry#getTime()
     */
    public static SupportedPropertyPath<String> time()
    {
        return _dslPath.time();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry#getTitleWithoutTime()
     */
    public static SupportedPropertyPath<String> titleWithoutTime()
    {
        return _dslPath.titleWithoutTime();
    }

    public static class Path<E extends ScheduleBellEntry> extends EntityPath<E>
    {
        private ScheduleBell.Path<ScheduleBell> _schedule;
        private PropertyPath<Integer> _number;
        private PropertyPath<Integer> _startTime;
        private PropertyPath<Integer> _endTime;
        private SupportedPropertyPath<String> _time;
        private SupportedPropertyPath<String> _title;
        private SupportedPropertyPath<String> _titleWithoutTime;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Звонковое расписание. Свойство не может быть null.
     * @see ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry#getSchedule()
     */
        public ScheduleBell.Path<ScheduleBell> schedule()
        {
            if(_schedule == null )
                _schedule = new ScheduleBell.Path<ScheduleBell>(L_SCHEDULE, this);
            return _schedule;
        }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry#getNumber()
     */
        public PropertyPath<Integer> number()
        {
            if(_number == null )
                _number = new PropertyPath<Integer>(ScheduleBellEntryGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Время начала (в минутах). Свойство не может быть null.
     * @see ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry#getStartTime()
     */
        public PropertyPath<Integer> startTime()
        {
            if(_startTime == null )
                _startTime = new PropertyPath<Integer>(ScheduleBellEntryGen.P_START_TIME, this);
            return _startTime;
        }

    /**
     * @return Время окончания (в минутах). Свойство не может быть null.
     * @see ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry#getEndTime()
     */
        public PropertyPath<Integer> endTime()
        {
            if(_endTime == null )
                _endTime = new PropertyPath<Integer>(ScheduleBellEntryGen.P_END_TIME, this);
            return _endTime;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry#getTime()
     */
        public SupportedPropertyPath<String> time()
        {
            if(_time == null )
                _time = new SupportedPropertyPath<String>(ScheduleBellEntryGen.P_TIME, this);
            return _time;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(ScheduleBellEntryGen.P_TITLE, this);
            return _title;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry#getTitleWithoutTime()
     */
        public SupportedPropertyPath<String> titleWithoutTime()
        {
            if(_titleWithoutTime == null )
                _titleWithoutTime = new SupportedPropertyPath<String>(ScheduleBellEntryGen.P_TITLE_WITHOUT_TIME, this);
            return _titleWithoutTime;
        }

        public Class getEntityClass()
        {
            return ScheduleBellEntry.class;
        }

        public String getEntityName()
        {
            return "scheduleBellEntry";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getTime();

    public abstract String getTitle();

    public abstract String getTitleWithoutTime();
}
