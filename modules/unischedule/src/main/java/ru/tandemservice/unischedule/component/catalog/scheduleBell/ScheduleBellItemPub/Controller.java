/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unischedule.component.catalog.scheduleBell.ScheduleBellItemPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogItemPub.DefaultCatalogItemPubController;
import ru.tandemservice.unischedule.base.bo.Schedule.ui.BellEntriesEdit.ScheduleBellEntriesEdit;
import ru.tandemservice.unischedule.base.bo.Schedule.ui.BellEntriesEdit.ScheduleBellEntriesEditUI;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBell;

/**
 * @author AutoGenerator
 * Created on 31.08.2011
 */
public class Controller extends DefaultCatalogItemPubController<ScheduleBell, Model, IDAO>
{
    public void onClickEditEntries(IBusinessComponent component)
    {
        Model model = getModel(component);
        component.createDefaultChildRegion(
                new ComponentActivator(ScheduleBellEntriesEdit.class.getSimpleName(), new ParametersMap().add(ScheduleBellEntriesEditUI.PARAM_BELL_SCHEDULE_ID, model.getCatalogItem().getId()))
        );
    }
}
