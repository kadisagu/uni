package ru.tandemservice.unischedule.base.entity.event;

import org.apache.commons.lang.time.DateUtils;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unischedule.base.entity.event.gen.ScheduleEventGen;

import java.text.SimpleDateFormat;

/**
 * Событие
 * <p/>
 * Указывает, что в определенное время произойдет указанное событие
 */
public class ScheduleEvent extends ScheduleEventGen
{
    @Override
    @EntityDSLSupport(parts = ScheduleEvent.P_DURATION_BEGIN)
    public String getFullTimeStr()
    {
        return isDatesDiffer() ? getBeginDateStr() + " - " + getEndDateStr() : getTimePeriodStr() + " " + getBeginDateStr();
    }

    @Override
    @EntityDSLSupport
    public String getDateWithDayOfWeek()
    {
        return DateFormatter.DEFAULT_DATE_FORMATTER.format(getDurationBegin()) + " (" + new SimpleDateFormat("EEEE", RussianDateFormatUtils.DATE_FORMAT_SYMBOLS_RU).format(getDurationBegin()) + ")";
    }

    @Override
    @EntityDSLSupport(parts = ScheduleEvent.P_DURATION_BEGIN)
    public String getBeginDateStr()
    {
        return DateFormatter.DEFAULT_DATE_FORMATTER.format(getDurationBegin()) + " " + RussianDateFormatUtils.getWeekDayShortName(getDurationBegin());
    }

    @Override
    @EntityDSLSupport(parts = ScheduleEvent.P_DURATION_BEGIN)
    public String getEndDateStr()
    {
        return DateFormatter.DEFAULT_DATE_FORMATTER.format(getDurationEnd()) + " " + RussianDateFormatUtils.getWeekDayShortName(getDurationEnd());
    }

    @Override
    @EntityDSLSupport(parts = ScheduleEvent.P_DURATION_BEGIN)
    public String getTimePeriodStr()
    {
        return DateFormatter.DATE_FORMATTER_TIME.format(getDurationBegin()) + " - " + DateFormatter.DATE_FORMATTER_TIME.format(getDurationEnd());
    }

    public boolean isDatesDiffer()
    {
        return !DateUtils.isSameDay(getDurationBegin(), getDurationEnd());
    }
}