package ru.tandemservice.unischedule.catalog.entity;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.unischedule.catalog.entity.gen.*;

/** @see ru.tandemservice.unischedule.catalog.entity.gen.ScheduleBellEntryGen */
public class ScheduleBellEntry extends ScheduleBellEntryGen implements ITitled
{
    public int getStartHour()
    {
        return getStartTime() / 60;
    }

    public void setStartHour(int startHour)
    {
        setStartTime(getStartTime() % 60 + startHour * 60);
    }

    public int getStartMin()
    {
        return getStartTime() % 60;
    }

    public void setStartMin(int startMin)
    {
        setStartTime(((int) (getStartTime() / 60)) * 60 + startMin);
    }

    public int getEndHour()
    {
        return getEndTime() / 60;
    }

    public void setEndHour(int endHour)
    {
        setEndTime(getEndTime() % 60 + endHour * 60);
    }

    public int getEndMin()
    {
        return getEndTime() % 60;
    }

    public void setEndMin(int endMin)
    {
        setEndTime(((int) (getEndTime() / 60)) * 60 + endMin);
    }

    @Override
    @EntityDSLSupport(parts = ScheduleBellEntry.P_NUMBER)
    public String getTitle()
    {
        return getTitleWithoutTime() + " (" + getTime() + ")";
    }

    @Override
    @EntityDSLSupport(parts = ScheduleBellEntry.P_NUMBER)
    public String getTitleWithoutTime()
    {
        return getNumber() + " пара";
    }

    @Override
    @EntityDSLSupport(parts = ScheduleBellEntry.P_NUMBER)
    public String getTime()
    {
        return format(getStartHour()) + " : " + format(getStartMin()) + " - " + format(getEndHour()) + " : " + format(getEndMin());
    }

    private String format(int value)
    {
        return (value < 10) ? "0" + value : String.valueOf(value);
    }

    public String getPrintTime()
    {
        return format(getStartHour()) + ":" + format(getStartMin()) + "-" + format(getEndHour()) + ":" + format(getEndMin());
    }
}