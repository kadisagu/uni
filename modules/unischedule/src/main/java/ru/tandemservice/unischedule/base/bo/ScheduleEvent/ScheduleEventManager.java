/* $Id:$ */
package ru.tandemservice.unischedule.base.bo.ScheduleEvent;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unischedule.base.bo.ScheduleEvent.logic.IScheduleEventDao;
import ru.tandemservice.unischedule.base.bo.ScheduleEvent.logic.ScheduleEventDao;

/**
 * @author oleyba
 * @since 6/19/14
 */
@Configuration
public class ScheduleEventManager extends BusinessObjectManager
{
    public static ScheduleEventManager instance()
    {
        return instance(ScheduleEventManager.class);
    }

    @Bean
    public IScheduleEventDao dao()
    {
        return new ScheduleEventDao();
    }
}
