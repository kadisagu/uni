/* $Id$ */
package ru.tandemservice.unischedule.base.bo.Schedule.ui.BellEntriesEdit;

import com.google.common.collect.Lists;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.EntityRenumerator;
import ru.tandemservice.unischedule.base.bo.Schedule.ScheduleManager;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBell;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry;

import java.util.List;

/**
 * @author nvankov
 * @since 2/19/14
 */

@Input({@Bind(key = ScheduleBellEntriesEditUI.PARAM_BELL_SCHEDULE_ID, binding = "bellScheduleId")})
public class ScheduleBellEntriesEditUI extends UIPresenter
{
    public static final String PARAM_BELL_SCHEDULE_ID = "bellScheduleId";

    private Long _bellScheduleId;
    private ScheduleBell _bellSchedule;
    private List<ScheduleBellEntry> _entryList = Lists.newArrayList();
    private ScheduleBellEntry _currentEntry;
    private boolean _entryEditDisabled = true;

    public Long getBellScheduleId()
    {
        return _bellScheduleId;
    }

    public void setBellScheduleId(Long bellScheduleId)
    {
        _bellScheduleId = bellScheduleId;
    }

    public ScheduleBell getBellSchedule()
    {
        return _bellSchedule;
    }

    public void setBellSchedule(ScheduleBell bellSchedule)
    {
        _bellSchedule = bellSchedule;
    }

    public List<ScheduleBellEntry> getEntryList()
    {
        return _entryList;
    }

    public void setEntryList(List<ScheduleBellEntry> entryList)
    {
        _entryList = entryList;
    }

    public ScheduleBellEntry getCurrentEntry()
    {
        return _currentEntry;
    }

    public void setCurrentEntry(ScheduleBellEntry currentEntry)
    {
        _currentEntry = currentEntry;
    }

    @Override
    public void onComponentRefresh()
    {
        _entryList.clear();
        _bellSchedule = DataAccessServices.dao().getNotNull(_bellScheduleId);
        _entryEditDisabled = !ScheduleManager.instance().dao().canEditBellsEntriesResult(_bellSchedule);
        for (ScheduleBellEntry entry : DataAccessServices.dao().getList(ScheduleBellEntry.class, ScheduleBellEntry.schedule().s(), _bellSchedule, ScheduleBellEntry.number().s()))
        {
            ScheduleBellEntry e = new ScheduleBellEntry();
            e.update(entry);
            _entryList.add(e);
        }
        if (_entryList.isEmpty())
            _entryList.add(newEntry(_bellSchedule));
    }

    public void onClickApply()
    {
        validate();
        if(getUserContext().getErrorCollector().hasErrors()) return;
        for (ScheduleBellEntry entry : DataAccessServices.dao().getList(ScheduleBellEntry.class, ScheduleBellEntry.schedule().s(), _bellSchedule, ScheduleBellEntry.number().s()))
            DataAccessServices.dao().delete(entry);
        for (ScheduleBellEntry entry : _entryList)
            DataAccessServices.dao().save(entry);
        deactivate();
    }

    public void validate()
    {
        for (int i = 0; i < _entryList.size(); i++)
        {
            ScheduleBellEntry current = _entryList.get(i);
            if (i+1 < _entryList.size() && current.getEndTime() > _entryList.get(i + 1).getStartTime())
            {
                ScheduleBellEntry next = _entryList.get(i + 1);
                _uiSupport.error("Пара не может начинаться раньше, чем заканчивается предыдущая.", "endHour" + current.getNumber(), "endMin" + current.getNumber(), "startHour" + next.getNumber(), "endHour" + next.getNumber());
            }
        }
    }

    public void addEntry(Integer number)
    {
        ScheduleBellEntry previous = null;
        for (ScheduleBellEntry entry : _entryList)
            if (entry.getNumber() == number)
                previous = entry;
        if (null == previous)
            return;
        for (ScheduleBellEntry entry : _entryList)
            if (entry.getNumber() > number)
                entry.setNumber(entry.getNumber() + 1);
        ScheduleBellEntry newEntry = newEntry(previous);
        _entryList.add(newEntry);
        new EntityRenumerator<ScheduleBellEntry>() {
            @Override protected int getNumber(final ScheduleBellEntry e) { return e.getNumber(); }
            @Override protected void setNumber(final ScheduleBellEntry e, final int number) { e.setNumber(number);}
        }.execute(_entryList);
    }

    public void deleteEntry(Integer number)
    {
        ScheduleBellEntry toDeleteEntry = null;
        for (ScheduleBellEntry entry : _entryList)
            if (entry.getNumber() == number)
                toDeleteEntry = entry;
        if (null == toDeleteEntry)
            return;
        _entryList.remove(toDeleteEntry);
        new EntityRenumerator<ScheduleBellEntry>() {
            @Override protected int getNumber(final ScheduleBellEntry e) { return e.getNumber(); }
            @Override protected void setNumber(final ScheduleBellEntry e, final int number) { e.setNumber(number);}
        }.execute(_entryList);
    }

    private ScheduleBellEntry newEntry(ScheduleBellEntry previous)
    {
        ScheduleBellEntry entry = new ScheduleBellEntry();
        entry.setNumber(previous.getNumber() + 1);
        entry.setStartTime(previous.getEndTime() + 10);
        entry.setEndTime(entry.getStartTime() + 80);
        entry.setSchedule(previous.getSchedule());
        return entry;
    }

    private ScheduleBellEntry newEntry(ScheduleBell schedule)
    {
        ScheduleBellEntry entry = new ScheduleBellEntry();
        entry.setNumber(1);
        entry.setStartTime(60*8);
        entry.setEndTime(60*8+80);
        entry.setSchedule(schedule);
        return entry;
    }

    public boolean isEntryEditEnabled()
    {
        return !_entryEditDisabled;
    }

    public boolean isEntryEditDisabled()
    {
        return _entryEditDisabled;
    }

    public boolean isEntryDeleteDisabled()
    {
        return getEntryList().size() <= 1 || isEntryEditDisabled();
    }

    public void onClickAddEntry()
    {
        addEntry((Integer) getListenerParameter());
    }

    public void onClickDeleteEntry()
    {
        deleteEntry((Integer) getListenerParameter());
    }

}

