/* $Id$ */
package ru.tandemservice.unischedule.base.ext.SystemAction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.SystemActionManager;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.util.SystemActionDefinition;
import ru.tandemservice.unischedule.base.ext.SystemAction.ui.Pub.ScheduleSystemActionPubAddon;

/**
 * @author azhebko
 * @since 20.06.2014
 */
@Configuration
public class SystemActionExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private SystemActionManager _systemActionManager;

    @Bean
    public ItemListExtension<SystemActionDefinition> buttonListExtension()
    {
        return this.itemListExtension(_systemActionManager.buttonListExtPoint())
            .add("unischedule_scheduleEventList", new SystemActionDefinition("unischedule", "scheduleEventList", "onClickShowScheduleEventList", ScheduleSystemActionPubAddon.SYSTEM_ACTION_PUB_ADDON_NAME))
            .create();
    }
}