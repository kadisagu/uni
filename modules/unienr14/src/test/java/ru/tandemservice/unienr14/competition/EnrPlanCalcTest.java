/* $Id:$ */
package ru.tandemservice.unienr14.competition;

import com.google.common.collect.Sets;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.utils.test.CommonBaseTestJsonContext;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.EnrCompetitionManager;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.logic.IEnrCompetitionDao;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBS;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

/**
 * @author oleyba
 * @since 6/25/15
 */
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration(loader = TestContextLoader.class)
public class EnrPlanCalcTest
{
    @BeforeTransaction
    public void setupData() {
        // CommonBaseTestJsonInit.init("unienr14/competition/planCalc.equalParts.test.json");
    }

    @Test
    public void testEqualParts() throws Exception
    {
        CommonBaseTestJsonContext context = new CommonBaseTestJsonContext(this.getClass().getResource("data.json"));

        DataAccessServices.dao().getList(EnrEnrollmentCampaign.class);

        EnrEnrollmentCampaign campaign = context.entity(EnrEnrollmentCampaign.class);
        EnrProgramSetBS ps = context.entity(EnrProgramSetBS.class);

        EnrCompetitionManager.instance().dao().doCalculateProgramSetPlans(
            campaign,
            IEnrCompetitionDao.PlanCalculationRule.EQUAL_PARTS,
            Sets.newHashSet(ps.getId()),
            true
        );

        context.check();

//        EnrCompetition c = context.entity(EnrCompetition.class);
//        Assert.assertEquals(c.getPlan(), 5);
    }
}
