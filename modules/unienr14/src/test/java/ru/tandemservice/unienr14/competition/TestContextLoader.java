/* $Id:$ */
package ru.tandemservice.unienr14.competition;

import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextLoader;
import org.tandemframework.core.runtime.ApplicationRuntime;

/**
 * @author oleyba
 * @since 6/26/15
 */
public class TestContextLoader implements ContextLoader
{
    @Override
    public ApplicationContext loadContext(String... locations) throws Exception
    {
        ApplicationRuntime.initForTests();
        return ApplicationRuntime.getApplicationContext();
    }

    @Override
    public String[] processLocations(Class<?> clazz, String... locations)
    {
        return new String[] {};
    }
}

