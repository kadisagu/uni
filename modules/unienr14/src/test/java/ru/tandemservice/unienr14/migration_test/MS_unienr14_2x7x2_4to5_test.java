/* $Id:$ */
package ru.tandemservice.unienr14.migration_test;

import junit.framework.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.connect.DBConnect;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.hibsupport.connection.DataSourceWrapper;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;
import ru.tandemservice.unienr14.migration.MS_unienr14_2x7x2_4to5;

/**
 * @author oleyba
 * @since 3/2/15
 */
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration({"classpath:migration-tests-context.xml"})
public class MS_unienr14_2x7x2_4to5_test
{
    @Autowired
    DataSourceWrapper mainDataSource;

    @Test
    public void run() throws Exception
    {
        try (DBTool dbtool = new DBTool(new DBConnect(mainDataSource))) {

            dbtool.createTable(new DBTable("educationyear_t",
                    new DBColumn("id", DBType.INTEGER).setPrimaryKey(),
                    new DBColumn("intvalue_p", DBType.INTEGER)));

            dbtool.createTable(new DBTable("enr14_campaign_t",
                    new DBColumn("id", DBType.INTEGER).setPrimaryKey(),
                    new DBColumn("educationYear_id", DBType.INTEGER)));

            dbtool.executeUpdate("insert into educationyear_t (id, intvalue_p) values (?, ?)", 1, 2014);
            dbtool.executeUpdate("insert into educationyear_t (id, intvalue_p) values (?, ?)", 2, 2015);
            dbtool.executeUpdate("insert into educationyear_t (id, intvalue_p) values (?, ?)", 3, 2016);

            dbtool.executeUpdate("insert into enr14_campaign_t (id, educationYear_id) values (?, ?)", 1, 1);
            dbtool.executeUpdate("insert into enr14_campaign_t (id, educationYear_id) values (?, ?)", 2, 2);
            dbtool.executeUpdate("insert into enr14_campaign_t (id, educationYear_id) values (?, ?)", 3, 3);

            new MS_unienr14_2x7x2_4to5().run(dbtool);

            Assert.assertTrue(dbtool.columnExists("enr14_campaign_t", "educationYear_id"));

            MigrationUtils.assertExists(dbtool, "select id from enr14_campaign_t where id = ? and open_p = ?", 1, Boolean.FALSE);
            MigrationUtils.assertExists(dbtool, "select id from enr14_campaign_t where id = ? and open_p = ?", 2, Boolean.TRUE);
            MigrationUtils.assertExists(dbtool, "select id from enr14_campaign_t where id = ? and open_p = ?", 3, Boolean.TRUE);

            MigrationUtils.assertNotExists(dbtool, "select id from enr14_campaign_t where id = ? and open_p = ?", 1, Boolean.TRUE);
            MigrationUtils.assertNotExists(dbtool, "select id from enr14_campaign_t where id = ? and open_p = ?", 2, Boolean.FALSE);
            MigrationUtils.assertNotExists(dbtool, "select id from enr14_campaign_t where id = ? and open_p = ?", 3, Boolean.FALSE);
        }
    }
}
