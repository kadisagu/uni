/* $Id$ */
package ru.tandemservice.unienr14.migration_test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.connect.DBConnect;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.hibsupport.connection.DataSourceWrapper;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;
import org.tandemframework.shared.commonbase.utils.MigrationUtils.BatchInsertBuilder;
import org.testng.Assert;
import ru.tandemservice.unienr14.migration.MS_unienr14_2x7x2_0to1;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Nikolay Fedorovskih
 * @since 03.03.2015
 */
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration({"classpath:migration-tests-context.xml"})
public class MS_unienr14_2x7x2_0to1_test
{
    @Autowired
    DataSourceWrapper mainDataSource;

    @Test
    public void run() throws Exception
    {
        try (DBTool tool = new DBTool(new DBConnect(mainDataSource)))
        {
            // Настройки ПК

            tool.createTable(
                    new DBTable("enr14_campaign_settings_t")
                            .addColumn(new DBColumn("id", DBType.LONG).setPrimaryKey())
                            .addColumn(new DBColumn("mxttlachvmntmrkaslngbs_p", DBType.LONG))
                            .addColumn(new DBColumn("mxttlachvmntmrkaslngmstr_p", DBType.LONG))
                            .addColumn(new DBColumn("mxttlachvmntmrkaslnghghr_p", DBType.LONG))
                            .addColumn(new DBColumn("mxttlachvmntmrkaslngspo_p", DBType.LONG))
            );
            BatchInsertBuilder setInsertBuilder = new BatchInsertBuilder((short) 1, "mxttlachvmntmrkaslngbs_p", "mxttlachvmntmrkaslngmstr_p", "mxttlachvmntmrkaslnghghr_p", "mxttlachvmntmrkaslngspo_p");
            Long setsId = setInsertBuilder.addRow(1L, 2L, 3L, 4L);
            setInsertBuilder.executeInsert(tool, "enr14_campaign_settings_t");

            // ПК

            tool.createTable(
                    new DBTable("enr14_campaign_t")
                            .addColumn(new DBColumn("id", DBType.LONG).setPrimaryKey())
                            .addColumn(new DBColumn("settings_id", DBType.LONG).setNullable(false))
            );
            BatchInsertBuilder campInsertBuilder = new BatchInsertBuilder((short) 12, "settings_id");
            campInsertBuilder.addRow(setsId);
            campInsertBuilder.executeInsert(tool, "enr14_campaign_t");

            // Виды приема

            tool.createTable(
                    new DBTable("enr14_c_request_type_t")
                            .addColumn(new DBColumn("id", DBType.LONG).setPrimaryKey())
                            .addColumn(new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false))
            );
            BatchInsertBuilder insertBuilder = new BatchInsertBuilder((short) 123, "code_p");
            Map<String, Long> requestTypeCode2IdMap = new HashMap<>();
            for (int i = 1; i < 5; i++)
            {
                String code = String.valueOf(i);
                Long id = insertBuilder.addRow(code);
                requestTypeCode2IdMap.put(code, id);
            }
            insertBuilder.executeInsert(tool, "enr14_c_request_type_t");

            // Виды инд. достижений в ПК

            tool.createTable(
                    new DBTable("enr14_achievement_type_t")
                            .addColumn(new DBColumn("id", DBType.LONG).setPrimaryKey())
                            .addColumn(new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false))
                            .addColumn(new DBColumn("title_p", DBType.createVarchar(255)).setNullable(false))
                            .addColumn(new DBColumn("description_p", DBType.TEXT))
                            .addColumn(new DBColumn("requesttype_id", DBType.LONG).setNullable(false))
                            .addColumn(new DBColumn("achievementmarkaslong_p", DBType.LONG))
            );

            BatchInsertBuilder achTypeInsertBuilder = new BatchInsertBuilder((short)754, "code_p", "title_p", "description_p", "requesttype_id", "achievementmarkaslong_p");
            achTypeInsertBuilder.addRow("123", "test", "test description", requestTypeCode2IdMap.get("4"), 100L);
            achTypeInsertBuilder.addRow("125", "xxxx", null, requestTypeCode2IdMap.get("1"), 102L);
            achTypeInsertBuilder.executeInsert(tool, "enr14_achievement_type_t");

            // enr14_entrant_achievement_t

            tool.createTable(
                    new DBTable("enr14_entrant_achievement_t")
                            .addColumn(new DBColumn("id", DBType.LONG).setPrimaryKey())
            );

            // run

            new MS_unienr14_2x7x2_0to1().run(tool);

            // checks

            // Настройки ПК создаются на все виды приема
            Number x = (Number) tool.getUniqueResult("select count(*) from enr14_campaign_t");
            Number y = (Number) tool.getUniqueResult("select count(*) from enr14_achievement_sets_t");
            Assert.assertEquals(x.intValue() * y.intValue(), x.intValue() * requestTypeCode2IdMap.size());


            MigrationUtils.assertCount(tool, "enr14_achievement_type_t", 2);
            MigrationUtils.assertCount(tool, "enr14_achievement_kind_t", 3);
            MigrationUtils.assertCount(tool, "enr14_achievement_sets_t", 4);
        }
    }
}