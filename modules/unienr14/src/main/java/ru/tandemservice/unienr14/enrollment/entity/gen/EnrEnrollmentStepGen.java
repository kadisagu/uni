package ru.tandemservice.unienr14.enrollment.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentStepKind;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentStepState;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Шаг зачисления
 *
 * В списке шага зачисления есть все абитуриенты (их выбранные конкурсы), которые подали документы на один из конкурсов, включенных в данный шаг.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrEnrollmentStepGen extends VersionedEntityBase
 implements INaturalIdentifiable<EnrEnrollmentStepGen>, ILazyFieldOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep";
    public static final String ENTITY_NAME = "enrEnrollmentStep";
    public static final int VERSION_HASH = 1741862067;
    private static IEntityMeta ENTITY_META;

    public static final String P_VERSION = "version";
    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_ENROLLMENT_DATE = "enrollmentDate";
    public static final String L_REQUEST_TYPE = "requestType";
    public static final String L_STATE = "state";
    public static final String L_KIND = "kind";
    public static final String P_AUTO_RECOMMENDED = "autoRecommended";
    public static final String P_AUTO_ENROLLED_MARKED = "autoEnrolledMarked";
    public static final String P_PERCENTAGE_AS_LONG = "percentageAsLong";
    public static final String P_ZIP_XML_RECOMMENDATION_STATE = "zipXmlRecommendationState";
    public static final String P_ZIP_XML_ENROLLMENT_STATE = "zipXmlEnrollmentState";
    public static final String P_DATE_STR = "dateStr";
    public static final String P_DELETE_ALLOWED = "deleteAllowed";
    public static final String P_PLAN_EDIT_DISABLED = "planEditDisabled";

    private int _version; 
    private EnrEnrollmentCampaign _enrollmentCampaign;     // ПК
    private Date _enrollmentDate;     // Дата зачисления
    private EnrRequestType _requestType;     // Вид заявления
    private EnrEnrollmentStepState _state;     // Состояние шага зачисления
    private EnrEnrollmentStepKind _kind;     // Вариант работы с шагом зачисления
    private boolean _autoRecommended;     // Абитуриенты рекомендованы автоматически
    private boolean _autoEnrolledMarked;     // Абитуриенты отмечены к зачислению автоматически
    private long _percentageAsLong;     // Процент от числа мест
    private byte[] _zipXmlRecommendationState;     // ZIP-XML: Рекомендация
    private byte[] _zipXmlEnrollmentState;     // ZIP-XML: Зачисление

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public int getVersion()
    {
        return _version;
    }

    /**
     * @param version  Свойство не может быть null.
     */
    public void setVersion(int version)
    {
        dirty(_version, version);
        _version = version;
    }

    /**
     * @return ПК. Свойство не может быть null.
     */
    @NotNull
    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign ПК. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Дата зачисления. Свойство не может быть null.
     */
    @NotNull
    public Date getEnrollmentDate()
    {
        return _enrollmentDate;
    }

    /**
     * @param enrollmentDate Дата зачисления. Свойство не может быть null.
     */
    public void setEnrollmentDate(Date enrollmentDate)
    {
        dirty(_enrollmentDate, enrollmentDate);
        _enrollmentDate = enrollmentDate;
    }

    /**
     * @return Вид заявления. Свойство не может быть null.
     */
    @NotNull
    public EnrRequestType getRequestType()
    {
        return _requestType;
    }

    /**
     * @param requestType Вид заявления. Свойство не может быть null.
     */
    public void setRequestType(EnrRequestType requestType)
    {
        dirty(_requestType, requestType);
        _requestType = requestType;
    }

    /**
     * @return Состояние шага зачисления. Свойство не может быть null.
     */
    @NotNull
    public EnrEnrollmentStepState getState()
    {
        return _state;
    }

    /**
     * @param state Состояние шага зачисления. Свойство не может быть null.
     */
    public void setState(EnrEnrollmentStepState state)
    {
        dirty(_state, state);
        _state = state;
    }

    /**
     * @return Вариант работы с шагом зачисления. Свойство не может быть null.
     */
    @NotNull
    public EnrEnrollmentStepKind getKind()
    {
        return _kind;
    }

    /**
     * @param kind Вариант работы с шагом зачисления. Свойство не может быть null.
     */
    public void setKind(EnrEnrollmentStepKind kind)
    {
        dirty(_kind, kind);
        _kind = kind;
    }

    /**
     * @return Абитуриенты рекомендованы автоматически. Свойство не может быть null.
     */
    @NotNull
    public boolean isAutoRecommended()
    {
        return _autoRecommended;
    }

    /**
     * @param autoRecommended Абитуриенты рекомендованы автоматически. Свойство не может быть null.
     */
    public void setAutoRecommended(boolean autoRecommended)
    {
        dirty(_autoRecommended, autoRecommended);
        _autoRecommended = autoRecommended;
    }

    /**
     * @return Абитуриенты отмечены к зачислению автоматически. Свойство не может быть null.
     */
    @NotNull
    public boolean isAutoEnrolledMarked()
    {
        return _autoEnrolledMarked;
    }

    /**
     * @param autoEnrolledMarked Абитуриенты отмечены к зачислению автоматически. Свойство не может быть null.
     */
    public void setAutoEnrolledMarked(boolean autoEnrolledMarked)
    {
        dirty(_autoEnrolledMarked, autoEnrolledMarked);
        _autoEnrolledMarked = autoEnrolledMarked;
    }

    /**
     * Хранится со смещением в два знака для дробной части.
     *
     * @return Процент от числа мест. Свойство не может быть null.
     */
    @NotNull
    public long getPercentageAsLong()
    {
        return _percentageAsLong;
    }

    /**
     * @param percentageAsLong Процент от числа мест. Свойство не может быть null.
     */
    public void setPercentageAsLong(long percentageAsLong)
    {
        dirty(_percentageAsLong, percentageAsLong);
        _percentageAsLong = percentageAsLong;
    }

    /**
     * XML-данные шага на момент рекомендации
     *
     * @return ZIP-XML: Рекомендация.
     */
    public byte[] getZipXmlRecommendationState()
    {
        initLazyForGet("zipXmlRecommendationState");
        return _zipXmlRecommendationState;
    }

    /**
     * @param zipXmlRecommendationState ZIP-XML: Рекомендация.
     */
    public void setZipXmlRecommendationState(byte[] zipXmlRecommendationState)
    {
        initLazyForSet("zipXmlRecommendationState");
        dirty(_zipXmlRecommendationState, zipXmlRecommendationState);
        _zipXmlRecommendationState = zipXmlRecommendationState;
    }

    /**
     * XML-данные шага на момент зачисления
     *
     * @return ZIP-XML: Зачисление.
     */
    public byte[] getZipXmlEnrollmentState()
    {
        initLazyForGet("zipXmlEnrollmentState");
        return _zipXmlEnrollmentState;
    }

    /**
     * @param zipXmlEnrollmentState ZIP-XML: Зачисление.
     */
    public void setZipXmlEnrollmentState(byte[] zipXmlEnrollmentState)
    {
        initLazyForSet("zipXmlEnrollmentState");
        dirty(_zipXmlEnrollmentState, zipXmlEnrollmentState);
        _zipXmlEnrollmentState = zipXmlEnrollmentState;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrEnrollmentStepGen)
        {
            if (withNaturalIdProperties)
            {
                setEnrollmentCampaign(((EnrEnrollmentStep)another).getEnrollmentCampaign());
                setEnrollmentDate(((EnrEnrollmentStep)another).getEnrollmentDate());
                setRequestType(((EnrEnrollmentStep)another).getRequestType());
            }
            setVersion(((EnrEnrollmentStep)another).getVersion());
            setState(((EnrEnrollmentStep)another).getState());
            setKind(((EnrEnrollmentStep)another).getKind());
            setAutoRecommended(((EnrEnrollmentStep)another).isAutoRecommended());
            setAutoEnrolledMarked(((EnrEnrollmentStep)another).isAutoEnrolledMarked());
            setPercentageAsLong(((EnrEnrollmentStep)another).getPercentageAsLong());
            setZipXmlRecommendationState(((EnrEnrollmentStep)another).getZipXmlRecommendationState());
            setZipXmlEnrollmentState(((EnrEnrollmentStep)another).getZipXmlEnrollmentState());
        }
    }

    public INaturalId<EnrEnrollmentStepGen> getNaturalId()
    {
        return new NaturalId(getEnrollmentCampaign(), getEnrollmentDate(), getRequestType());
    }

    public static class NaturalId extends NaturalIdBase<EnrEnrollmentStepGen>
    {
        private static final String PROXY_NAME = "EnrEnrollmentStepNaturalProxy";

        private Long _enrollmentCampaign;
        private Date _enrollmentDate;
        private Long _requestType;

        public NaturalId()
        {}

        public NaturalId(EnrEnrollmentCampaign enrollmentCampaign, Date enrollmentDate, EnrRequestType requestType)
        {
            _enrollmentCampaign = ((IEntity) enrollmentCampaign).getId();
            _enrollmentDate = org.tandemframework.core.CoreDateUtils.getDayFirstTimeMoment(enrollmentDate);
            _requestType = ((IEntity) requestType).getId();
        }

        public Long getEnrollmentCampaign()
        {
            return _enrollmentCampaign;
        }

        public void setEnrollmentCampaign(Long enrollmentCampaign)
        {
            _enrollmentCampaign = enrollmentCampaign;
        }

        public Date getEnrollmentDate()
        {
            return _enrollmentDate;
        }

        public void setEnrollmentDate(Date enrollmentDate)
        {
            _enrollmentDate = org.tandemframework.core.CoreDateUtils.getDayFirstTimeMoment(enrollmentDate);
        }

        public Long getRequestType()
        {
            return _requestType;
        }

        public void setRequestType(Long requestType)
        {
            _requestType = requestType;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrEnrollmentStepGen.NaturalId) ) return false;

            EnrEnrollmentStepGen.NaturalId that = (NaturalId) o;

            if( !equals(getEnrollmentCampaign(), that.getEnrollmentCampaign()) ) return false;
            if( !equals(getEnrollmentDate(), that.getEnrollmentDate()) ) return false;
            if( !equals(getRequestType(), that.getRequestType()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEnrollmentCampaign());
            result = hashCode(result, getEnrollmentDate());
            result = hashCode(result, getRequestType());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEnrollmentCampaign());
            sb.append("/");
            sb.append(getEnrollmentDate());
            sb.append("/");
            sb.append(getRequestType());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrEnrollmentStepGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrEnrollmentStep.class;
        }

        public T newInstance()
        {
            return (T) new EnrEnrollmentStep();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "version":
                    return obj.getVersion();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "enrollmentDate":
                    return obj.getEnrollmentDate();
                case "requestType":
                    return obj.getRequestType();
                case "state":
                    return obj.getState();
                case "kind":
                    return obj.getKind();
                case "autoRecommended":
                    return obj.isAutoRecommended();
                case "autoEnrolledMarked":
                    return obj.isAutoEnrolledMarked();
                case "percentageAsLong":
                    return obj.getPercentageAsLong();
                case "zipXmlRecommendationState":
                    return obj.getZipXmlRecommendationState();
                case "zipXmlEnrollmentState":
                    return obj.getZipXmlEnrollmentState();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "version":
                    obj.setVersion((Integer) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrEnrollmentCampaign) value);
                    return;
                case "enrollmentDate":
                    obj.setEnrollmentDate((Date) value);
                    return;
                case "requestType":
                    obj.setRequestType((EnrRequestType) value);
                    return;
                case "state":
                    obj.setState((EnrEnrollmentStepState) value);
                    return;
                case "kind":
                    obj.setKind((EnrEnrollmentStepKind) value);
                    return;
                case "autoRecommended":
                    obj.setAutoRecommended((Boolean) value);
                    return;
                case "autoEnrolledMarked":
                    obj.setAutoEnrolledMarked((Boolean) value);
                    return;
                case "percentageAsLong":
                    obj.setPercentageAsLong((Long) value);
                    return;
                case "zipXmlRecommendationState":
                    obj.setZipXmlRecommendationState((byte[]) value);
                    return;
                case "zipXmlEnrollmentState":
                    obj.setZipXmlEnrollmentState((byte[]) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "version":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "enrollmentDate":
                        return true;
                case "requestType":
                        return true;
                case "state":
                        return true;
                case "kind":
                        return true;
                case "autoRecommended":
                        return true;
                case "autoEnrolledMarked":
                        return true;
                case "percentageAsLong":
                        return true;
                case "zipXmlRecommendationState":
                        return true;
                case "zipXmlEnrollmentState":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "version":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "enrollmentDate":
                    return true;
                case "requestType":
                    return true;
                case "state":
                    return true;
                case "kind":
                    return true;
                case "autoRecommended":
                    return true;
                case "autoEnrolledMarked":
                    return true;
                case "percentageAsLong":
                    return true;
                case "zipXmlRecommendationState":
                    return true;
                case "zipXmlEnrollmentState":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "version":
                    return Integer.class;
                case "enrollmentCampaign":
                    return EnrEnrollmentCampaign.class;
                case "enrollmentDate":
                    return Date.class;
                case "requestType":
                    return EnrRequestType.class;
                case "state":
                    return EnrEnrollmentStepState.class;
                case "kind":
                    return EnrEnrollmentStepKind.class;
                case "autoRecommended":
                    return Boolean.class;
                case "autoEnrolledMarked":
                    return Boolean.class;
                case "percentageAsLong":
                    return Long.class;
                case "zipXmlRecommendationState":
                    return byte[].class;
                case "zipXmlEnrollmentState":
                    return byte[].class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrEnrollmentStep> _dslPath = new Path<EnrEnrollmentStep>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrEnrollmentStep");
    }
            

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep#getVersion()
     */
    public static PropertyPath<Integer> version()
    {
        return _dslPath.version();
    }

    /**
     * @return ПК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep#getEnrollmentCampaign()
     */
    public static EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Дата зачисления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep#getEnrollmentDate()
     */
    public static PropertyPath<Date> enrollmentDate()
    {
        return _dslPath.enrollmentDate();
    }

    /**
     * @return Вид заявления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep#getRequestType()
     */
    public static EnrRequestType.Path<EnrRequestType> requestType()
    {
        return _dslPath.requestType();
    }

    /**
     * @return Состояние шага зачисления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep#getState()
     */
    public static EnrEnrollmentStepState.Path<EnrEnrollmentStepState> state()
    {
        return _dslPath.state();
    }

    /**
     * @return Вариант работы с шагом зачисления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep#getKind()
     */
    public static EnrEnrollmentStepKind.Path<EnrEnrollmentStepKind> kind()
    {
        return _dslPath.kind();
    }

    /**
     * @return Абитуриенты рекомендованы автоматически. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep#isAutoRecommended()
     */
    public static PropertyPath<Boolean> autoRecommended()
    {
        return _dslPath.autoRecommended();
    }

    /**
     * @return Абитуриенты отмечены к зачислению автоматически. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep#isAutoEnrolledMarked()
     */
    public static PropertyPath<Boolean> autoEnrolledMarked()
    {
        return _dslPath.autoEnrolledMarked();
    }

    /**
     * Хранится со смещением в два знака для дробной части.
     *
     * @return Процент от числа мест. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep#getPercentageAsLong()
     */
    public static PropertyPath<Long> percentageAsLong()
    {
        return _dslPath.percentageAsLong();
    }

    /**
     * XML-данные шага на момент рекомендации
     *
     * @return ZIP-XML: Рекомендация.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep#getZipXmlRecommendationState()
     */
    public static PropertyPath<byte[]> zipXmlRecommendationState()
    {
        return _dslPath.zipXmlRecommendationState();
    }

    /**
     * XML-данные шага на момент зачисления
     *
     * @return ZIP-XML: Зачисление.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep#getZipXmlEnrollmentState()
     */
    public static PropertyPath<byte[]> zipXmlEnrollmentState()
    {
        return _dslPath.zipXmlEnrollmentState();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep#getDateStr()
     */
    public static SupportedPropertyPath<String> dateStr()
    {
        return _dslPath.dateStr();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep#isDeleteAllowed()
     */
    public static SupportedPropertyPath<Boolean> deleteAllowed()
    {
        return _dslPath.deleteAllowed();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep#isPlanEditDisabled()
     */
    public static SupportedPropertyPath<Boolean> planEditDisabled()
    {
        return _dslPath.planEditDisabled();
    }

    public static class Path<E extends EnrEnrollmentStep> extends EntityPath<E>
    {
        private PropertyPath<Integer> _version;
        private EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<Date> _enrollmentDate;
        private EnrRequestType.Path<EnrRequestType> _requestType;
        private EnrEnrollmentStepState.Path<EnrEnrollmentStepState> _state;
        private EnrEnrollmentStepKind.Path<EnrEnrollmentStepKind> _kind;
        private PropertyPath<Boolean> _autoRecommended;
        private PropertyPath<Boolean> _autoEnrolledMarked;
        private PropertyPath<Long> _percentageAsLong;
        private PropertyPath<byte[]> _zipXmlRecommendationState;
        private PropertyPath<byte[]> _zipXmlEnrollmentState;
        private SupportedPropertyPath<String> _dateStr;
        private SupportedPropertyPath<Boolean> _deleteAllowed;
        private SupportedPropertyPath<Boolean> _planEditDisabled;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep#getVersion()
     */
        public PropertyPath<Integer> version()
        {
            if(_version == null )
                _version = new PropertyPath<Integer>(EnrEnrollmentStepGen.P_VERSION, this);
            return _version;
        }

    /**
     * @return ПК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep#getEnrollmentCampaign()
     */
        public EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Дата зачисления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep#getEnrollmentDate()
     */
        public PropertyPath<Date> enrollmentDate()
        {
            if(_enrollmentDate == null )
                _enrollmentDate = new PropertyPath<Date>(EnrEnrollmentStepGen.P_ENROLLMENT_DATE, this);
            return _enrollmentDate;
        }

    /**
     * @return Вид заявления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep#getRequestType()
     */
        public EnrRequestType.Path<EnrRequestType> requestType()
        {
            if(_requestType == null )
                _requestType = new EnrRequestType.Path<EnrRequestType>(L_REQUEST_TYPE, this);
            return _requestType;
        }

    /**
     * @return Состояние шага зачисления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep#getState()
     */
        public EnrEnrollmentStepState.Path<EnrEnrollmentStepState> state()
        {
            if(_state == null )
                _state = new EnrEnrollmentStepState.Path<EnrEnrollmentStepState>(L_STATE, this);
            return _state;
        }

    /**
     * @return Вариант работы с шагом зачисления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep#getKind()
     */
        public EnrEnrollmentStepKind.Path<EnrEnrollmentStepKind> kind()
        {
            if(_kind == null )
                _kind = new EnrEnrollmentStepKind.Path<EnrEnrollmentStepKind>(L_KIND, this);
            return _kind;
        }

    /**
     * @return Абитуриенты рекомендованы автоматически. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep#isAutoRecommended()
     */
        public PropertyPath<Boolean> autoRecommended()
        {
            if(_autoRecommended == null )
                _autoRecommended = new PropertyPath<Boolean>(EnrEnrollmentStepGen.P_AUTO_RECOMMENDED, this);
            return _autoRecommended;
        }

    /**
     * @return Абитуриенты отмечены к зачислению автоматически. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep#isAutoEnrolledMarked()
     */
        public PropertyPath<Boolean> autoEnrolledMarked()
        {
            if(_autoEnrolledMarked == null )
                _autoEnrolledMarked = new PropertyPath<Boolean>(EnrEnrollmentStepGen.P_AUTO_ENROLLED_MARKED, this);
            return _autoEnrolledMarked;
        }

    /**
     * Хранится со смещением в два знака для дробной части.
     *
     * @return Процент от числа мест. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep#getPercentageAsLong()
     */
        public PropertyPath<Long> percentageAsLong()
        {
            if(_percentageAsLong == null )
                _percentageAsLong = new PropertyPath<Long>(EnrEnrollmentStepGen.P_PERCENTAGE_AS_LONG, this);
            return _percentageAsLong;
        }

    /**
     * XML-данные шага на момент рекомендации
     *
     * @return ZIP-XML: Рекомендация.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep#getZipXmlRecommendationState()
     */
        public PropertyPath<byte[]> zipXmlRecommendationState()
        {
            if(_zipXmlRecommendationState == null )
                _zipXmlRecommendationState = new PropertyPath<byte[]>(EnrEnrollmentStepGen.P_ZIP_XML_RECOMMENDATION_STATE, this);
            return _zipXmlRecommendationState;
        }

    /**
     * XML-данные шага на момент зачисления
     *
     * @return ZIP-XML: Зачисление.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep#getZipXmlEnrollmentState()
     */
        public PropertyPath<byte[]> zipXmlEnrollmentState()
        {
            if(_zipXmlEnrollmentState == null )
                _zipXmlEnrollmentState = new PropertyPath<byte[]>(EnrEnrollmentStepGen.P_ZIP_XML_ENROLLMENT_STATE, this);
            return _zipXmlEnrollmentState;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep#getDateStr()
     */
        public SupportedPropertyPath<String> dateStr()
        {
            if(_dateStr == null )
                _dateStr = new SupportedPropertyPath<String>(EnrEnrollmentStepGen.P_DATE_STR, this);
            return _dateStr;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep#isDeleteAllowed()
     */
        public SupportedPropertyPath<Boolean> deleteAllowed()
        {
            if(_deleteAllowed == null )
                _deleteAllowed = new SupportedPropertyPath<Boolean>(EnrEnrollmentStepGen.P_DELETE_ALLOWED, this);
            return _deleteAllowed;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep#isPlanEditDisabled()
     */
        public SupportedPropertyPath<Boolean> planEditDisabled()
        {
            if(_planEditDisabled == null )
                _planEditDisabled = new SupportedPropertyPath<Boolean>(EnrEnrollmentStepGen.P_PLAN_EDIT_DISABLED, this);
            return _planEditDisabled;
        }

        public Class getEntityClass()
        {
            return EnrEnrollmentStep.class;
        }

        public String getEntityName()
        {
            return "enrEnrollmentStep";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getDateStr();

    public abstract boolean isDeleteAllowed();

    public abstract boolean isPlanEditDisabled();
}
