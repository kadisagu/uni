/**
 *$Id: EnrExamGroupSetScheduleEventTimeUI.java 33636 2014-04-16 04:31:39Z nfedorovskih $
 */
package ru.tandemservice.unienr14.exams.bo.EnrExamGroup.ui.SetScheduleEventTime;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.RadioButtonColumn;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.unienr14.exams.bo.EnrExamGroup.EnrExamGroupManager;
import ru.tandemservice.unienr14.exams.bo.EnrExamGroup.logic.EnrExamGroupSetScheduleEventTimeSearchDSHandler;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroup;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroupScheduleEvent;
import ru.tandemservice.unienr14.exams.entity.EnrExamScheduleEvent;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Alexander Shaburov
 * @since 03.06.13
 */
@State({
    @Bind(key = "examGroupId", binding = "examGroupId", required = true)
})
public class EnrExamGroupSetScheduleEventTimeUI extends UIPresenter
{
    private Long _examGroupId;

    private EnrExamGroup _examGroup;

    @Override
    public void onComponentRefresh()
    {
        _examGroup = DataAccessServices.dao().getNotNull(EnrExamGroup.class, _examGroupId);

        getSettings().set("dateFromFilter", _examGroup.getExamGroupSet().getBeginDate());
        getSettings().set("dateToFilter", _examGroup.getExamGroupSet().getEndDate());
    }

    @SuppressWarnings("unchecked")
    public void onClickApply()
    {
        final Collection selectedList;
        if (isOnceDays())
            selectedList = getConfig().<BaseSearchListDataSource>getDataSource(EnrExamGroupSetScheduleEventTime.EXAM_GROUP_SET_SCHEDULE_EVENT_TIME_SEARCHDS).getOptionColumnSelectedObjects("radio");
        else
            selectedList = getConfig().<BaseSearchListDataSource>getDataSource(EnrExamGroupSetScheduleEventTime.EXAM_GROUP_SET_SCHEDULE_EVENT_TIME_SEARCHDS).getOptionColumnSelectedObjects("check");

        final Collection<EnrExamScheduleEvent> examScheduleEventIdList = new ArrayList<>();
        for (DataWrapper wrapper : (Collection<DataWrapper>) selectedList)
            examScheduleEventIdList.add(wrapper.<EnrExamScheduleEvent>getWrapped());

        EnrExamGroupManager.instance().groupDao().doSetScheduleEvent(_examGroup.getId(), examScheduleEventIdList);

        deactivate();
    }

    public boolean isEmptySelectors()
    {
        return getSettings().get("dateFromFilter") == null && getSettings().get("dateToFilter") == null;
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrExamGroupSetScheduleEventTimeSearchDSHandler.BIND_DATE_FROM, getSettings().get("dateFromFilter"));
        dataSource.put(EnrExamGroupSetScheduleEventTimeSearchDSHandler.BIND_DATE_TO, getSettings().get("dateToFilter"));
        dataSource.put(EnrExamGroupSetScheduleEventTimeSearchDSHandler.BIND_EXAM_GROUP, _examGroup);
    }

    @Override
    public void onAfterDataSourceFetch(IUIDataSource dataSource)
    {
        final CheckboxColumn check = (CheckboxColumn) ((BaseSearchListDataSource) dataSource).getLegacyDataSource().getColumn("check");
        final RadioButtonColumn radio = (RadioButtonColumn) ((BaseSearchListDataSource) dataSource).getLegacyDataSource().getColumn("radio");

        final List<IEntity> eventList = CommonBaseUtil.getPropertiesList(DataAccessServices.dao().getList(EnrExamGroupScheduleEvent.class, EnrExamGroupScheduleEvent.examGroup(), _examGroup), EnrExamGroupScheduleEvent.examScheduleEvent().s());
        if (check != null)
            check.setSelectedObjects(eventList);
        if (radio != null)
            radio.setSelectedEntity(eventList.isEmpty() ? null : eventList.get(0));
    }

    // Getters & Setters

    public boolean isOnceDays()
    {
        return _examGroup.getDays() == 1;
    }

    // Accessors

    public Long getExamGroupId()
    {
        return _examGroupId;
    }

    public void setExamGroupId(Long examGroupId)
    {
        _examGroupId = examGroupId;
    }

    public EnrExamGroup getExamGroup()
    {
        return _examGroup;
    }

    public void setExamGroup(EnrExamGroup examGroup)
    {
        _examGroup = examGroup;
    }
}
