/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.EditArmyData;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;

/**
 * @author oleyba
 * @since 4/22/13
 */
@Input({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "entrant.id")
})
public class EnrEntrantEditArmyDataUI extends UIPresenter
{
    private EnrEntrant entrant = new EnrEntrant();

    @Override
    public void onComponentRefresh()
    {
        setEntrant(IUniBaseDao.instance.get().getNotNull(EnrEntrant.class, getEntrant().getId()));
    }

    public void onClickApply()
    {
        IUniBaseDao.instance.get().save(getEntrant());
        deactivate();
    }

    public EnrEntrant getEntrant()
    {
        return entrant;
    }

    public void setEntrant(EnrEntrant entrant)
    {
        this.entrant = entrant;
    }
}
