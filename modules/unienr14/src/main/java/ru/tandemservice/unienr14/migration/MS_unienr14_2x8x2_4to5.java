package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x8x2_4to5 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrRequestedCompetition

		// создано обязательное свойство refusedToBeEnrolled
		{
			// создать колонку
			tool.createColumn("enr14_requested_comp_t", new DBColumn("refusedtobeenrolled_p", DBType.BOOLEAN));

			// задать значение по умолчанию
            tool.executeUpdate("update enr14_requested_comp_t set refusedtobeenrolled_p=? where refusedtobeenrolled_p is null", (Boolean) false);

			// сделать колонку NOT NULL
			tool.setColumnNullable("enr14_requested_comp_t", "refusedtobeenrolled_p", false);
		}
    }
}