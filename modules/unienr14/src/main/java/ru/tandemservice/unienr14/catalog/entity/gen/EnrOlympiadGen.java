package ru.tandemservice.unienr14.catalog.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unienr14.catalog.entity.EnrOlympiad;
import ru.tandemservice.unienr14.catalog.entity.EnrOlympiadType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Олимпиада
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrOlympiadGen extends EntityBase
 implements INaturalIdentifiable<EnrOlympiadGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.catalog.entity.EnrOlympiad";
    public static final String ENTITY_NAME = "enrOlympiad";
    public static final int VERSION_HASH = -2025050551;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_TITLE = "title";
    public static final String L_EDU_YEAR = "eduYear";
    public static final String L_OLYMPIAD_TYPE = "olympiadType";
    public static final String P_TITLE_WITH_YEAR = "titleWithYear";

    private String _code;     // Код
    private String _title;     // Название
    private EducationYear _eduYear;     // Учебный год
    private EnrOlympiadType _olympiadType;     // Тип олимпиады

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     */
    @NotNull
    public EducationYear getEduYear()
    {
        return _eduYear;
    }

    /**
     * @param eduYear Учебный год. Свойство не может быть null.
     */
    public void setEduYear(EducationYear eduYear)
    {
        dirty(_eduYear, eduYear);
        _eduYear = eduYear;
    }

    /**
     * @return Тип олимпиады. Свойство не может быть null.
     */
    @NotNull
    public EnrOlympiadType getOlympiadType()
    {
        return _olympiadType;
    }

    /**
     * @param olympiadType Тип олимпиады. Свойство не может быть null.
     */
    public void setOlympiadType(EnrOlympiadType olympiadType)
    {
        dirty(_olympiadType, olympiadType);
        _olympiadType = olympiadType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrOlympiadGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((EnrOlympiad)another).getCode());
            }
            setTitle(((EnrOlympiad)another).getTitle());
            setEduYear(((EnrOlympiad)another).getEduYear());
            setOlympiadType(((EnrOlympiad)another).getOlympiadType());
        }
    }

    public INaturalId<EnrOlympiadGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<EnrOlympiadGen>
    {
        private static final String PROXY_NAME = "EnrOlympiadNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrOlympiadGen.NaturalId) ) return false;

            EnrOlympiadGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrOlympiadGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrOlympiad.class;
        }

        public T newInstance()
        {
            return (T) new EnrOlympiad();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "title":
                    return obj.getTitle();
                case "eduYear":
                    return obj.getEduYear();
                case "olympiadType":
                    return obj.getOlympiadType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "eduYear":
                    obj.setEduYear((EducationYear) value);
                    return;
                case "olympiadType":
                    obj.setOlympiadType((EnrOlympiadType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "title":
                        return true;
                case "eduYear":
                        return true;
                case "olympiadType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "title":
                    return true;
                case "eduYear":
                    return true;
                case "olympiadType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "title":
                    return String.class;
                case "eduYear":
                    return EducationYear.class;
                case "olympiadType":
                    return EnrOlympiadType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrOlympiad> _dslPath = new Path<EnrOlympiad>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrOlympiad");
    }
            

    /**
     * @return Код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrOlympiad#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrOlympiad#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrOlympiad#getEduYear()
     */
    public static EducationYear.Path<EducationYear> eduYear()
    {
        return _dslPath.eduYear();
    }

    /**
     * @return Тип олимпиады. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrOlympiad#getOlympiadType()
     */
    public static EnrOlympiadType.Path<EnrOlympiadType> olympiadType()
    {
        return _dslPath.olympiadType();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.catalog.entity.EnrOlympiad#getTitleWithYear()
     */
    public static SupportedPropertyPath<String> titleWithYear()
    {
        return _dslPath.titleWithYear();
    }

    public static class Path<E extends EnrOlympiad> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _title;
        private EducationYear.Path<EducationYear> _eduYear;
        private EnrOlympiadType.Path<EnrOlympiadType> _olympiadType;
        private SupportedPropertyPath<String> _titleWithYear;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrOlympiad#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(EnrOlympiadGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrOlympiad#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EnrOlympiadGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrOlympiad#getEduYear()
     */
        public EducationYear.Path<EducationYear> eduYear()
        {
            if(_eduYear == null )
                _eduYear = new EducationYear.Path<EducationYear>(L_EDU_YEAR, this);
            return _eduYear;
        }

    /**
     * @return Тип олимпиады. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrOlympiad#getOlympiadType()
     */
        public EnrOlympiadType.Path<EnrOlympiadType> olympiadType()
        {
            if(_olympiadType == null )
                _olympiadType = new EnrOlympiadType.Path<EnrOlympiadType>(L_OLYMPIAD_TYPE, this);
            return _olympiadType;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.catalog.entity.EnrOlympiad#getTitleWithYear()
     */
        public SupportedPropertyPath<String> titleWithYear()
        {
            if(_titleWithYear == null )
                _titleWithYear = new SupportedPropertyPath<String>(EnrOlympiadGen.P_TITLE_WITH_YEAR, this);
            return _titleWithYear;
        }

        public Class getEntityClass()
        {
            return EnrOlympiad.class;
        }

        public String getEntityName()
        {
            return "enrOlympiad";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getTitleWithYear();
}
