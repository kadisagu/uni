/* $Id: SecLocalRoleAddEditUI.java 6522 2015-05-18 13:35:33Z oleyba $ */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.ui.LocalRoleAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.entity.Role;
import org.tandemframework.shared.organization.sec.bo.Sec.util.BaseRoleAddEditUI;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.unienr14.sec.entity.RoleConfigLocalEnrCommission;

/**
 * @author Vasily Zhukov
 * @since 07.11.2011
 */
@Input({
        @Bind(key = EnrEnrollmentCommissionLocalRoleAddEditUI.ENROLLMENT_COMMISSION_ID, binding = EnrEnrollmentCommissionLocalRoleAddEditUI.ENROLLMENT_COMMISSION_ID)
})
public class EnrEnrollmentCommissionLocalRoleAddEditUI extends BaseRoleAddEditUI<RoleConfigLocalEnrCommission>
{
    public static final String ENROLLMENT_COMMISSION_ID = "enrollmentCommissionId";

    private Long enrollmentCommissionId;

    @Override
    public void initRoleConfigForAddForm()
    {
        setRole(new Role());
        setRoleConfig(new RoleConfigLocalEnrCommission());
        getRoleConfig().setRole(getRole());
        getRoleConfig().setEnrollmentCommission(DataAccessServices.dao().getNotNull(EnrEnrollmentCommission.class, getEnrollmentCommissionId()));
    }

    // Getters & Setters

    public Long getEnrollmentCommissionId()
    {
        return enrollmentCommissionId;
    }

    public void setEnrollmentCommissionId(Long enrollmentCommissionId)
    {
        this.enrollmentCommissionId = enrollmentCommissionId;
    }
}
