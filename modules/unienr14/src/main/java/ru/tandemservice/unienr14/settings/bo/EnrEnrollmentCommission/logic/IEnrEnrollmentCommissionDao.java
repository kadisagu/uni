/* $Id:$ */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.sec.IPrincipal;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetEnrollmentCommission;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

/**
 * @author oleyba
 * @since 6/11/15
 */
public interface IEnrEnrollmentCommissionDao extends INeedPersistenceSupport
{
    List<EnrEnrollmentCommission> getEnrollmentCommissionList();

    /**
     * Возвращает актуальный набор ОП для ОК для выбранной ПК и вида заявления
     *
     * @param campaign    Приемная кампания
     * @param requestType Вид заявления
     * @return набор ОП для ОК, где ключ - подразделение, ведущее прием по набору ОП
     */
    Map<EnrProgramSetOrgUnit, EnrProgramSetEnrollmentCommission> getProgramSetOrgUnit2EnrollmentCommissionMap(EnrEnrollmentCampaign campaign, EnrRequestType requestType);

    /**
     * @param campaign Приемная кампания
     * @param enrollmentCommissions Отборочные комиссии
     * @return Возвращает подразделения, ведущие прием по набору ОП
     */
    List<EnrProgramSetOrgUnit> getProgramSetOrgUnitList(@NotNull EnrEnrollmentCampaign campaign, List<EnrEnrollmentCommission> enrollmentCommissions);

    /**
     * Сохраняет Набор ОП для ОК
     *
     * @param orgUnit2SetECMap текущий наборов ОП для ОК
     * @param programSetOrgUnit Подразделение, ведущее прием по набору ОП
     * @param enrollmentCommission Отборочная комиссия
     */
    void saveProgramSetEnrollmentCommission(Map<EnrProgramSetOrgUnit, EnrProgramSetEnrollmentCommission> orgUnit2SetECMap, EnrProgramSetOrgUnit programSetOrgUnit, EnrEnrollmentCommission enrollmentCommission);

    /**
     * Фильтрация конкурсов по ОК.
     * Вынесено в ДАО, чтобы переопределять в проекте.
     *
     * @param commissions одна ОК или список ОК.
     */
    boolean filterRequestedCompetitionsByEnrCommission(DQLSelectBuilder dql, String reqCompAlias, Object commissions);

    /**
     * Фильтрация заявлений по ОК.
     * Вынесено в ДАО, чтобы переопределять в проекте.
     *
     * @param commissions одна ОК или список ОК.
     */
    boolean filterEntrantRequestByEnrCommission(DQLSelectBuilder dql, String entrantRequestAlias, Object commissions);

    /**
     * Фильтрация абитуриентов по ОК.
     * Вынесено в ДАО, чтобы переопределять в проекте.
     *
     * @param commissions одна ОК или список ОК.
     */
    boolean filterEntrantByEnrCommission(DQLSelectBuilder dql, String entrantAlias, Object commissions);


    /**
     * Выбранная пользователем ОК в фильтрах и селекторах в модуле хранится в настройках пользователя, как общая для всех страниц модуля.
     * Этот метод позволяет получить такую ОК для текущего пользователя.
     * <p/>
     * Если вы используете такую ОК в списке в фильтре, то при применении фильтров сохраняйте новый выбор пользователя.
     * Если вы используете такую ОК в селекторе, сохраняйте при обновлении селектора.
     * <p/>
     * Для сохранения используйте метод
     *
     * @return выбранная пользователем ОК (может быть null). Вернет null, если метод вызвать вне UserContext.
     * @see IEnrEnrollmentCommissionDao#saveDefaultCommission(ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission)
     * <p/>
     * *
     */
    public EnrEnrollmentCommission getDefaultCommission(boolean required);

    /**
     * Выбранная пользователем ОК в фильтрах и селекторах в модуле хранится в настройках пользователя, как общая для всех страниц модуля.
     * Этот метод позволяет сохранить новый выбор пользователя в настройки.
     * <p/>
     * Если вы используете такую ОК в списке в фильтре, то при применении фильтров сохраняйте новый выбор пользователя.
     * Если вы используете такую ОК в селекторе, сохраняйте при обновлении селектора.
     * <p/>
     * Чтобы получить сохраненное значение, используйте
     *
     * @param commission выбранная пользователем ОК
     * @see IEnrEnrollmentCommissionDao#getDefaultCommission(boolean)
     * <p/>
     */
    public void saveDefaultCommission(EnrEnrollmentCommission commission);

}