/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrantEduDocument.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.fias.base.bo.AddressBase.AddressBaseManager;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.person.base.bo.PersonEduDocument.PersonEduDocumentManager;
import ru.tandemservice.unienr14.request.bo.EnrRequestedCompetition.EnrRequestedCompetitionManager;

/**
 * @author oleyba
 * @since 3/18/14
 */
@Configuration
public class EnrEntrantEduDocumentAddEdit extends BusinessComponentManager
{
    public static final String DS_COUNTRY = "countryDS";
    public static final String DS_SETTLEMENT = "settlementDS";
    public static final String DS_DOCUMENT_KIND = "documentKindDS";
    public static final String DS_EDU_LEVEL = "eduLevelDS";
    public static final String DS_REQ_COMP = "reqCompDS";
    public static final String BIND_EDU_DOC_ID = "eduDocId";
    public static final String BIND_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
				.addDataSource(selectDS(DS_COUNTRY, AddressBaseManager.instance().countryComboDSHandler()))
				.addDataSource(CommonBaseStaticSelectDataSource.selectDS(DS_SETTLEMENT, getName(), AddressItem.settlementComboDSHandler(getName())))
                .addDataSource(selectDS(DS_EDU_LEVEL, PersonEduDocumentManager.instance().eduLevelDSHandler()))
                .addDataSource(selectDS(DS_DOCUMENT_KIND, PersonEduDocumentManager.instance().documentKindDSHandler()))
                .addDataSource(EnrRequestedCompetitionManager.instance().createEnrRequestedCompetitionDS())
                .create();
    }
}