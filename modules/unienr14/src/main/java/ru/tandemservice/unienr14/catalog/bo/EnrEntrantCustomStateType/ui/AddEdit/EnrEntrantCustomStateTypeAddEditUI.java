/* $Id$ */
package ru.tandemservice.unienr14.catalog.bo.EnrEntrantCustomStateType.ui.AddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.crud.BaseCatalogItemAddEditUI;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantCustomStateType;

/**
 * @author nvankov
 * @since 8/15/14
 */
@Input({
        @Bind(key = "catalogItemId", binding = "holder.id"),
        @Bind(key = DefaultCatalogAddEditModel.CATALOG_CODE, binding = "catalogCode")
})
public class EnrEntrantCustomStateTypeAddEditUI extends BaseCatalogItemAddEditUI<EnrEntrantCustomStateType>
{
    @Override
    public String getAdditionalPropertiesPageName()
    {
        return "ru.tandemservice.unienr14.catalog.bo.EnrEntrantCustomStateType.ui.AddEdit.EnrEntrantCustomStateTypeAddEditAdditProps";
    }
}
