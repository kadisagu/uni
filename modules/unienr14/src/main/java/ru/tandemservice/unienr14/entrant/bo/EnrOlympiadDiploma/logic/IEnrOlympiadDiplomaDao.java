/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrOlympiadDiploma.logic;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.person.base.bo.PersonDocument.logic.IPersonDocumentDao;
import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma;

/**
 * @author oleyba
 * @since 5/3/13
 */
public interface IEnrOlympiadDiplomaDao extends INeedPersistenceSupport, IPersonDocumentDao
{
    void saveOrUpdateDiploma(EnrOlympiadDiploma diploma, PersonRole role, IUploadFile scanCopy);
}
