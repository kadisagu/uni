/* $Id:$ */
package ru.tandemservice.unienr14.order.bo.EnrEnrollmentParagraph.logic;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.util.mq.UniDQLExpressions;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderPrintFormType;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderType;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantStateCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrOrderPrintFormTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrOrderTypeCodes;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItem;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.order.bo.EnrCancelParagraph.EnrCancelParagraphManager;
import ru.tandemservice.unienr14.order.bo.EnrOrder.EnrOrderManager;
import ru.tandemservice.unienr14.order.entity.*;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;
import ru.tandemservice.unimove.entity.catalog.codes.ExtractStatesCodes;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 7/8/14
 */
public class EnrEnrollmentParagraphDao extends UniBaseDao implements IEnrEnrollmentParagraphDao
{
    @Override
    public void saveOrUpdateParagraph(EnrOrder order, EnrEnrollmentParagraph paragraph, List<EnrRequestedCompetition> preStudents, EnrRequestedCompetition manager, String groupTitle)
    {
        // В один и тот же приказ нельзя одновременно добавлять параграф
        NamedSyncInTransactionCheckLocker.register(getSession(), "EnrOrder" + order.getId());

        // V A L I D A T E

        if (preStudents == null || preStudents.isEmpty())
            throw new ApplicationException("Параграф не может быть пустым.");

        final Session session = getSession();
        session.refresh(order);

        if (!UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(order.getState().getCode())) {
            throw new ApplicationException("Приказ находится в состоянии «" + order.getState().getTitle() +"», редактирование параграфов запрещено.");
        }

        List<EnrRequestedCompetition> list = new ArrayList<>(preStudents);
        Collections.sort(list, ITitled.TITLED_COMPARATOR);

        if (manager != null && !list.contains(manager))
            throw new ApplicationException("Старостой может быть только один из предварительно зачисленных абитуриентов.");

        // пока мы были на форме state мог очень сильно поменяться, поэтому надо все данные проверить на актуаность
        for (EnrRequestedCompetition reqComp : list)
        {
            final EnrEntrant entrant = reqComp.getRequest().getEntrant();

            // абитуриент не должен быть в другом приказе
            EnrEnrollmentExtract enrollmentExtract = get(EnrEnrollmentExtract.class, IAbstractExtract.L_ENTITY, reqComp);
            if (enrollmentExtract != null && !(paragraph != null && paragraph.equals(enrollmentExtract.getParagraph())))
            {
                String orderNumber = enrollmentExtract.getParagraph().getOrder().getNumber();
                throw new ApplicationException("Абитуриент «" + entrant.getPerson().getFullFio() + "» уже в приказе" + (StringUtils.isEmpty(orderNumber) ? "" : " №" + orderNumber) + ".");
            }

            // данные приказа должны совпадать с данными абитуриента
            if (!order.getEnrollmentCampaign().equals(entrant.getEnrollmentCampaign()))
                throw new ApplicationException("Приемная кампания у абитуриента «" + entrant.getPerson().getFullFio() + "» отличается от указанной в приказе.");
            if (!order.getRequestType().equals(reqComp.getRequest().getType()))
                throw new ApplicationException("Вид заявления у выбранного конкурса «" + reqComp.getTitle() + "» отличается от указанного в приказе.");
            if (!order.getCompensationType().equals(reqComp.getCompetition().getType().getCompensationType()))
                throw new ApplicationException("Вид возмещения затрат у выбранного конкурса «" + reqComp.getTitle() + "» отличается от указанного в приказе.");

            // правильное состояние
            if (!EnrEntrantStateCodes.EXAMS_PASSED.equals(reqComp.getState().getCode()) && !EnrEntrantStateCodes.SELECT_SHOULD_BE_ENROLLED.equals(reqComp.getState().getCode())&& !EnrEntrantStateCodes.IN_ORDER.equals(reqComp.getState().getCode()))
                throw new ApplicationException("Абитуриент «" + entrant.getPerson().getFullFio() + "» не может быть включен в приказ, так как выбранный конкурс в состоянии «" + reqComp.getState().getTitle() + "».");

            // EnrOrgUnit orgUnit, EduProgramForm form, EduProgramSubject subject, EnrCompetitionType competitionType

            // признаки должны соответствовать тому, что выбрано на форме
            if (!paragraph.getEnrOrgUnit().equals(reqComp.getCompetition().getProgramSetOrgUnit().getOrgUnit()))
                throw new ApplicationException("Филиал у выбранного конкурса абитуриента «" + entrant.getPerson().getFullFio() + "» отличается от указанного в приказе.");
            if (!paragraph.getProgramForm().equals(reqComp.getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramForm()))
                throw new ApplicationException("Форма оубчения у выбранного конкурса абитуриента «" + entrant.getPerson().getFullFio() + "» отличается от указанной в приказе.");
            if (!paragraph.getProgramSubject().equals(reqComp.getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramSubject()))
                throw new ApplicationException("Направление (профессия, специальность) у выбранного конкурса абитуриента «" + entrant.getPerson().getFullFio() + "» отличается от указанного в приказе.");
            if (!paragraph.getCompetitionType().equals(reqComp.getCompetition().getType()))
                throw new ApplicationException("Вид приема у выбранного конкурса абитуриента «" + entrant.getPerson().getFullFio() + "» отличается от указанного в приказе.");
            if (!paragraph.isParallel() == reqComp.isParallel())
                throw new ApplicationException(paragraph.isParallel() ? "Выбранный конкурс абитуриента «" + entrant.getPerson().getFullFio() + "» не может быть включен в параграф для поступающих на параллельное обучение, так как для него не установлен флаг «Параллельное обучение»." : "Выбранный конкурс абитуриента «" + entrant.getPerson().getFullFio() + "» не может быть включен в параграф, так как для него установлен флаг «Параллельное обучение».");

            // у абитуриента должно быть итоговое согласие на зачисление
//            if (!reqComp.isEnrollmentAvailable()) {
//                throw new ApplicationException("У абитуриента «" + entrant.getPerson().getFullFio() + "» нет итогового согласия на зачисление.");
//            }
        }

        if (paragraph.getId() != null)
        {
            // E D I T   P A R A G R A P H
            // удаляем выписки у студентов, которые не выбранны на форме. тут специально не перенумеровываем выписки,
            // так как потом будет общая перенумерация по ФИО
            for (IAbstractExtract extract : paragraph.getExtractList()) {
                EnrEnrollmentExtract enrollmentExtract = (EnrEnrollmentExtract) extract;
                if (!list.remove(enrollmentExtract.getEntity())) {
                    if (paragraph.getGroupManager() != null && paragraph.getGroupManager().equals(enrollmentExtract)) {
                        paragraph.setGroupManager(null);
                        session.update(paragraph);
                    }
                    session.delete(enrollmentExtract);
                }
            }
        }
        else
        {
            // C R E A T E   P A R A G R A P H
            paragraph.setOrder(order);
            paragraph.setNumber(order.getParagraphCount() + 1);  // номер параграфа с единицы
            session.save(paragraph);
        }

        int counter = -1;
        for (EnrRequestedCompetition preStudent : list)
        {
            // создаем выписку
            EnrEnrollmentExtract extract = new EnrEnrollmentExtract();

            // в preStudent уже есть все данные для проведения выписки
            extract.setEntity(preStudent);
            extract.setParagraph(paragraph);
            extract.setCreateDate(order.getCreateDate());
            extract.setNumber(counter--); // нет номера. перенумеруем в конце метода update
            extract.setState(getByCode(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_IN_ORDER));
            extract.setGroupTitle(groupTitle);

            // устанавливается состояние «В приказе» для выбранного направления приема
//            extract.getEntity().getRequestedDirection().setState(getByCode(EnrEntrantState.class, EnrEntrantStateCodes.IN_ORDER));
//            session.update(extract.getEntity().getRequestedDirection());

            // сохраняем выписку
            session.save(extract);
        }

        // перенумеруем все выписки, так чтобы сортировка была по ФИО
        session.flush(); // flush перед запросом, чтобы исзлечь ВСЕ выписки
        List<EnrEnrollmentExtract> extractList = getList(EnrEnrollmentExtract.class, IAbstractExtract.L_PARAGRAPH, paragraph);
        Collections.sort(extractList, CommonCollator.comparing(e -> e.getEntity().getRequest().getEntrant().getFullFio()));

        // присваиваем уникальные номера выпискам
        counter = -1 - extractList.size(); /* заведомо неиспользованные номера */
        for (EnrEnrollmentExtract enrollmentExtract : extractList)
        {
            enrollmentExtract.setNumber(counter--);
            session.update(enrollmentExtract);
        }
        session.flush();

        // нумеруем выписки с единицы, расставляем имена групп зачисления, вычисляем выписку старосты
        counter = 1;
        EnrEnrollmentExtract groupManager = null;
        for (EnrEnrollmentExtract enrollmentExtract : extractList)
        {
            enrollmentExtract.setNumber(counter++);
            enrollmentExtract.setGroupTitle(groupTitle);

            if (enrollmentExtract.getEntity().equals(manager)) groupManager = enrollmentExtract;

            session.update(enrollmentExtract);
        }

        paragraph.setGroupManager(groupManager);
        session.update(paragraph);
    }

    @Override
    public void doCreateOrders(EnrEnrollmentStep step)
    {
        List<EnrRequestedCompetition> items = new DQLSelectBuilder()
            .fromEntity(EnrEnrollmentStepItem.class, "i")
            .joinPath(DQLJoinType.inner, EnrEnrollmentStepItem.entity().requestedCompetition().fromAlias("i"), "r")
            .column(property("i", EnrEnrollmentStepItem.entity().requestedCompetition()))
            .where(eq(property("i", EnrEnrollmentStepItem.step()), value(step)))
            .where(eq(property("i", EnrEnrollmentStepItem.shouldBeEnrolled()), value(Boolean.TRUE)))
            .joinEntity("r", DQLJoinType.left, EnrEnrollmentExtract.class, "e", eq(property("e", EnrEnrollmentExtract.entity()), property("r")))
            .where(isNull("e.id"))
            .order(property("i", EnrEnrollmentStepItem.entity().entrant().person().identityCard().lastName()))
            .order(property("i", EnrEnrollmentStepItem.entity().entrant().person().identityCard().firstName()))
            .order(property("i", EnrEnrollmentStepItem.entity().entrant().person().identityCard().middleName()))
            .createStatement(getSession()).list();

        Map<MultiKey, Map<EnrCompetition, List<EnrRequestedCompetition>>> orderMap = new HashMap<>();
        for (EnrRequestedCompetition item : items) {
            if (item.getCompetition().getProgramSetOrgUnit().getFormativeOrgUnit() == null) {
                throw new ApplicationException("Для конкурса «" + item.getCompetition().getTitle() + "» не задано формирующее подразделение - формирование приказов невозможно.");
            }

            // по одному приказу на каждую комбинацию:
            // филиал, вид заявления, форма обучения, вид возмещения затрат, поступает на параллельное обучение
            MultiKey orderKey = new MultiKey(item.getCompetition().getProgramSetOrgUnit().getOrgUnit().getId(),
                item.getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramForm().getCode(),
                item.getCompetition().getType().getCompensationType().getCode(),
                item.isParallel());
            // в рамках каждого приказа отдельный параграф на каждый конкурс
            SafeMap.safeGet(SafeMap.safeGet(orderMap, orderKey, HashMap.class), item.getCompetition(), ArrayList.class).add(item);
        }

        EnrOrderType orderType = getCatalogItem(EnrOrderType.class, EnrOrderTypeCodes.ENROLLMENT);
        EnrOrderPrintFormType printFormType = getCatalogItem(EnrOrderPrintFormType.class, EnrOrderPrintFormTypeCodes.BASE_ENROLLMENT);
        String orderBasicText = EnrOrderManager.instance().dao().getDefaultOrderBasicText(EnrOrderTypeCodes.ENROLLMENT);

        Calendar c = Calendar.getInstance(); c.setTime(new Date());
        Set<String> takenOrderNumbers = new HashSet<>(new DQLSelectBuilder()
            .fromEntity(EnrOrder.class, "e").column(property("e", EnrOrder.number()))
            .where(UniDQLExpressions.eqYear("e", EnrOrder.createDate().s(), c.get(Calendar.YEAR)))
            .createStatement(getSession()).<String>list());

        long orderNumberNext = 1;
        for (Map.Entry<MultiKey, Map<EnrCompetition, List<EnrRequestedCompetition>>> orderEntry : orderMap.entrySet()) {
            EnrRequestedCompetition sample = orderEntry.getValue().values().iterator().next().get(0);

            EnrOrder order = new EnrOrder();
            orderNumberNext = findNumber(orderNumberNext, takenOrderNumbers);
            order.setNumber(String.valueOf(orderNumberNext));
            takenOrderNumbers.add(order.getNumber());
            order.setCommitDate(step.getEnrollmentDate());
            order.setActionDate(step.getEnrollmentDate());
            order.setEnrollmentCampaign(step.getEnrollmentCampaign());
            order.setCompensationType(sample.getCompetition().getType().getCompensationType());
            order.setRequestType(sample.getRequest().getType());
            order.setType(orderType);
            order.setPrintFormType(getPrintFormType(printFormType, sample));
            order.setOrderBasicText(orderBasicText);
            EnrOrderManager.instance().dao().saveOrUpdateOrder(order, new Date(), true, null);
            getSession().flush();

            for (Map.Entry<EnrCompetition, List<EnrRequestedCompetition>> paragraphEntry : orderEntry.getValue().entrySet()) {
                EnrCompetition competition = paragraphEntry.getKey();

                EnrEnrollmentParagraph paragraph = new EnrEnrollmentParagraph();
                paragraph.setOrder(order);
                paragraph.setEnrOrgUnit(competition.getProgramSetOrgUnit().getOrgUnit());
                paragraph.setCompetitionType(competition.getType());
                paragraph.setFormativeOrgUnit(competition.getProgramSetOrgUnit().getFormativeOrgUnit());
                paragraph.setProgramForm(competition.getProgramSetOrgUnit().getProgramSet().getProgramForm());
                paragraph.setProgramSubject(competition.getProgramSetOrgUnit().getProgramSet().getProgramSubject());
                paragraph.setParallel(sample.isParallel());
                saveOrUpdateParagraph(order, paragraph, paragraphEntry.getValue(), null, null);
            }
        }

        int orderTotal = orderMap.keySet().size();

        // а теперь приказы об отмене приказа, запросов будет много, оптимизировать будем потом

        if (step.getEnrollmentCampaign().getSettings().isReenrollByPriorityEnabled()) {

            Map<EnrOrder, Map<EnrCompetition, List<EnrEnrollmentExtract>>> cancelMap = new HashMap<>();
            for (Map.Entry<MultiKey, Map<EnrCompetition, List<EnrRequestedCompetition>>> orderEntry : orderMap.entrySet()) {
                for (Map.Entry<EnrCompetition, List<EnrRequestedCompetition>> paragraphEntry : orderEntry.getValue().entrySet()) {
                    for (EnrRequestedCompetition reqComp : paragraphEntry.getValue()) {
                        List<EnrEnrollmentExtract> extracts = new DQLSelectBuilder()
                            .fromEntity(EnrEnrollmentExtract.class, "e").column("e")
                            .where(eq(property("e", EnrEnrollmentExtract.entity().request().entrant()), value(reqComp.getRequest().getEntrant())))
                            .where(eq(property("e", EnrEnrollmentExtract.entity().request().type()), value(reqComp.getRequest().getType())))
                            .where(eq(property("e", EnrEnrollmentExtract.entity().parallel()), value(reqComp.isParallel())))
                            .where(or(
                                eq(property("e", EnrEnrollmentExtract.state().code()), value(ExtractStatesCodes.ACCEPTED)),
                                eq(property("e", EnrEnrollmentExtract.state().code()), value(ExtractStatesCodes.FINISHED))
                            ))
                            .joinEntity("e", DQLJoinType.left, EnrCancelExtract.class, "c", eq(property("c", EnrCancelExtract.entity()), property("e")))
                            .where(isNull("c.id"))
                            .createStatement(getSession()).list();
                        for (EnrEnrollmentExtract extract : extracts) {
                            SafeMap.safeGet(SafeMap.safeGet(cancelMap, extract.getOrder(), HashMap.class), extract.getCompetition(), ArrayList.class).add(extract);
                        }
                    }
                }
            }

            orderType = getCatalogItem(EnrOrderType.class, EnrOrderTypeCodes.CANCEL);
            printFormType = getCatalogItem(EnrOrderPrintFormType.class, EnrOrderPrintFormTypeCodes.BASE_CANCEL_ENROLLMENT);
            orderBasicText = EnrOrderManager.instance().dao().getDefaultOrderBasicText(EnrOrderTypeCodes.CANCEL);

            for (Map.Entry<EnrOrder, Map<EnrCompetition, List<EnrEnrollmentExtract>>> orderEntry : cancelMap.entrySet()) {
                EnrEnrollmentExtract sample = orderEntry.getValue().values().iterator().next().get(0);

                EnrOrder order = new EnrOrder();
                orderNumberNext = findNumber(orderNumberNext, takenOrderNumbers);
                order.setNumber(String.valueOf(orderNumberNext));
                takenOrderNumbers.add(order.getNumber());
                order.setCommitDate(step.getEnrollmentDate());
                order.setActionDate(step.getEnrollmentDate());
                order.setEnrollmentCampaign(step.getEnrollmentCampaign());
                order.setCompensationType(sample.getCompetition().getType().getCompensationType());
                order.setRequestType(sample.getRequestedCompetition().getRequest().getType());
                order.setType(orderType);
                order.setPrintFormType(getPrintFormType(printFormType, sample));
                order.setOrderBasicText(orderBasicText);
                EnrOrderManager.instance().dao().saveOrUpdateOrder(order, new Date(), true, null);
                getSession().flush();

                for (Map.Entry<EnrCompetition, List<EnrEnrollmentExtract>> paragraphEntry : orderEntry.getValue().entrySet()) {
                    EnrCancelParagraph paragraph = new EnrCancelParagraph();
                    paragraph.setOrder(order);
                    paragraph.setCancelledOrder(orderEntry.getKey());
                    EnrCancelParagraphManager.instance().dao().saveOrUpdateParagraph(order, paragraph, paragraphEntry.getValue());
                }

            }

            orderTotal = orderTotal + cancelMap.entrySet().size();
        }

        if (orderMap.keySet().isEmpty()) {
            ContextLocal.getInfoCollector().add("Не найдено абитуриентов, отмеченных к зачислению и еще не включенных в приказы.");
        } else {

            ContextLocal.getInfoCollector().add("Сформировано приказов: " + orderTotal+ ".");
        }

    }

    private long findNumber(long orderNumberNext, Set<String> takenOrderNumbers)
    {
        while (takenOrderNumbers.contains(String.valueOf(orderNumberNext))) {
            orderNumberNext++;
        }
        return orderNumberNext;
    }

    protected EnrOrderPrintFormType getPrintFormType(EnrOrderPrintFormType defaultType, EnrRequestedCompetition sample) {
        return defaultType;
    }

    protected EnrOrderPrintFormType getPrintFormType(EnrOrderPrintFormType defaultType, EnrEnrollmentExtract sample) {
        return defaultType;
    }

    @Override
    public Collection<EnrAbstractExtract> getOtherExtracts(Collection<EnrEntrant> entrants, EnrAbstractParagraph paragraph)
    {
        return new DQLSelectBuilder()
            .fromEntity(EnrAbstractExtract.class, "e")
            .column(property("e"))
            .where(paragraph == null ? null : ne(property("e", EnrAbstractExtract.paragraph()), value(paragraph)))
            .where(or(
                exists(new DQLSelectBuilder()
                    .fromEntity(EnrEnrollmentExtract.class, "ee")
                    .where(eq(property("ee", EnrEnrollmentExtract.id()), property("e", EnrAbstractExtract.id())))
                    .where(in(property("ee", EnrEnrollmentExtract.entity().request().entrant()), entrants))
                    .buildQuery()),
                exists(new DQLSelectBuilder()
                    .fromEntity(EnrCancelExtract.class, "ce")
                    .where(eq(property("ce", EnrCancelExtract.id()), property("e", EnrAbstractExtract.id())))
                    .where(in(property("ce", EnrCancelExtract.requestedCompetition().request().entrant()), entrants))
                    .buildQuery())))
                .order(property("e", EnrAbstractExtract.paragraph().order().actionDate()), OrderDirection.desc)
                .order(property("e", EnrAbstractExtract.createDate()), OrderDirection.desc)
            .createStatement(this.getSession()).list();
    }
}