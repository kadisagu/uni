package ru.tandemservice.unienr14.competition.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetEnrollmentCommission;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Набор ОП для ОК
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrProgramSetEnrollmentCommissionGen extends EntityBase
 implements INaturalIdentifiable<EnrProgramSetEnrollmentCommissionGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.competition.entity.EnrProgramSetEnrollmentCommission";
    public static final String ENTITY_NAME = "enrProgramSetEnrollmentCommission";
    public static final int VERSION_HASH = -1504925377;
    private static IEntityMeta ENTITY_META;

    public static final String L_PROGRAM_SET_ORG_UNIT = "programSetOrgUnit";
    public static final String L_ENROLLMENT_COMMISSION = "enrollmentCommission";

    private EnrProgramSetOrgUnit _programSetOrgUnit;     // Подразделение, ведущее прием по набору ОП
    private EnrEnrollmentCommission _enrollmentCommission;     // Отборочная комиссия

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Подразделение, ведущее прием по набору ОП. Свойство не может быть null.
     */
    @NotNull
    public EnrProgramSetOrgUnit getProgramSetOrgUnit()
    {
        return _programSetOrgUnit;
    }

    /**
     * @param programSetOrgUnit Подразделение, ведущее прием по набору ОП. Свойство не может быть null.
     */
    public void setProgramSetOrgUnit(EnrProgramSetOrgUnit programSetOrgUnit)
    {
        dirty(_programSetOrgUnit, programSetOrgUnit);
        _programSetOrgUnit = programSetOrgUnit;
    }

    /**
     * @return Отборочная комиссия. Свойство не может быть null.
     */
    @NotNull
    public EnrEnrollmentCommission getEnrollmentCommission()
    {
        return _enrollmentCommission;
    }

    /**
     * @param enrollmentCommission Отборочная комиссия. Свойство не может быть null.
     */
    public void setEnrollmentCommission(EnrEnrollmentCommission enrollmentCommission)
    {
        dirty(_enrollmentCommission, enrollmentCommission);
        _enrollmentCommission = enrollmentCommission;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrProgramSetEnrollmentCommissionGen)
        {
            if (withNaturalIdProperties)
            {
                setProgramSetOrgUnit(((EnrProgramSetEnrollmentCommission)another).getProgramSetOrgUnit());
                setEnrollmentCommission(((EnrProgramSetEnrollmentCommission)another).getEnrollmentCommission());
            }
        }
    }

    public INaturalId<EnrProgramSetEnrollmentCommissionGen> getNaturalId()
    {
        return new NaturalId(getProgramSetOrgUnit(), getEnrollmentCommission());
    }

    public static class NaturalId extends NaturalIdBase<EnrProgramSetEnrollmentCommissionGen>
    {
        private static final String PROXY_NAME = "EnrProgramSetEnrollmentCommissionNaturalProxy";

        private Long _programSetOrgUnit;
        private Long _enrollmentCommission;

        public NaturalId()
        {}

        public NaturalId(EnrProgramSetOrgUnit programSetOrgUnit, EnrEnrollmentCommission enrollmentCommission)
        {
            _programSetOrgUnit = ((IEntity) programSetOrgUnit).getId();
            _enrollmentCommission = ((IEntity) enrollmentCommission).getId();
        }

        public Long getProgramSetOrgUnit()
        {
            return _programSetOrgUnit;
        }

        public void setProgramSetOrgUnit(Long programSetOrgUnit)
        {
            _programSetOrgUnit = programSetOrgUnit;
        }

        public Long getEnrollmentCommission()
        {
            return _enrollmentCommission;
        }

        public void setEnrollmentCommission(Long enrollmentCommission)
        {
            _enrollmentCommission = enrollmentCommission;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrProgramSetEnrollmentCommissionGen.NaturalId) ) return false;

            EnrProgramSetEnrollmentCommissionGen.NaturalId that = (NaturalId) o;

            if( !equals(getProgramSetOrgUnit(), that.getProgramSetOrgUnit()) ) return false;
            if( !equals(getEnrollmentCommission(), that.getEnrollmentCommission()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getProgramSetOrgUnit());
            result = hashCode(result, getEnrollmentCommission());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getProgramSetOrgUnit());
            sb.append("/");
            sb.append(getEnrollmentCommission());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrProgramSetEnrollmentCommissionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrProgramSetEnrollmentCommission.class;
        }

        public T newInstance()
        {
            return (T) new EnrProgramSetEnrollmentCommission();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "programSetOrgUnit":
                    return obj.getProgramSetOrgUnit();
                case "enrollmentCommission":
                    return obj.getEnrollmentCommission();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "programSetOrgUnit":
                    obj.setProgramSetOrgUnit((EnrProgramSetOrgUnit) value);
                    return;
                case "enrollmentCommission":
                    obj.setEnrollmentCommission((EnrEnrollmentCommission) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "programSetOrgUnit":
                        return true;
                case "enrollmentCommission":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "programSetOrgUnit":
                    return true;
                case "enrollmentCommission":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "programSetOrgUnit":
                    return EnrProgramSetOrgUnit.class;
                case "enrollmentCommission":
                    return EnrEnrollmentCommission.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrProgramSetEnrollmentCommission> _dslPath = new Path<EnrProgramSetEnrollmentCommission>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrProgramSetEnrollmentCommission");
    }
            

    /**
     * @return Подразделение, ведущее прием по набору ОП. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetEnrollmentCommission#getProgramSetOrgUnit()
     */
    public static EnrProgramSetOrgUnit.Path<EnrProgramSetOrgUnit> programSetOrgUnit()
    {
        return _dslPath.programSetOrgUnit();
    }

    /**
     * @return Отборочная комиссия. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetEnrollmentCommission#getEnrollmentCommission()
     */
    public static EnrEnrollmentCommission.Path<EnrEnrollmentCommission> enrollmentCommission()
    {
        return _dslPath.enrollmentCommission();
    }

    public static class Path<E extends EnrProgramSetEnrollmentCommission> extends EntityPath<E>
    {
        private EnrProgramSetOrgUnit.Path<EnrProgramSetOrgUnit> _programSetOrgUnit;
        private EnrEnrollmentCommission.Path<EnrEnrollmentCommission> _enrollmentCommission;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Подразделение, ведущее прием по набору ОП. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetEnrollmentCommission#getProgramSetOrgUnit()
     */
        public EnrProgramSetOrgUnit.Path<EnrProgramSetOrgUnit> programSetOrgUnit()
        {
            if(_programSetOrgUnit == null )
                _programSetOrgUnit = new EnrProgramSetOrgUnit.Path<EnrProgramSetOrgUnit>(L_PROGRAM_SET_ORG_UNIT, this);
            return _programSetOrgUnit;
        }

    /**
     * @return Отборочная комиссия. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetEnrollmentCommission#getEnrollmentCommission()
     */
        public EnrEnrollmentCommission.Path<EnrEnrollmentCommission> enrollmentCommission()
        {
            if(_enrollmentCommission == null )
                _enrollmentCommission = new EnrEnrollmentCommission.Path<EnrEnrollmentCommission>(L_ENROLLMENT_COMMISSION, this);
            return _enrollmentCommission;
        }

        public Class getEntityClass()
        {
            return EnrProgramSetEnrollmentCommission.class;
        }

        public String getEntityName()
        {
            return "enrProgramSetEnrollmentCommission";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
