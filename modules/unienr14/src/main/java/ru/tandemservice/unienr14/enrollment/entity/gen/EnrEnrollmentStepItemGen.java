package ru.tandemservice.unienr14.enrollment.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItem;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Абитуриент в конкурсном списке
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrEnrollmentStepItemGen extends EntityBase
 implements INaturalIdentifiable<EnrEnrollmentStepItemGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItem";
    public static final String ENTITY_NAME = "enrEnrollmentStepItem";
    public static final int VERSION_HASH = -622409106;
    private static IEntityMeta ENTITY_META;

    public static final String L_STEP = "step";
    public static final String L_ENTITY = "entity";
    public static final String P_ENTRANT_ARCHIVED = "entrantArchived";
    public static final String P_ENTRANT_REQUEST_TAKE_AWAY_DOCUMENTS = "entrantRequestTakeAwayDocuments";
    public static final String P_ORIGINAL_IN = "originalIn";
    public static final String P_ACCEPTED = "accepted";
    public static final String P_ENROLLMENT_AVAILABLE = "enrollmentAvailable";
    public static final String P_INCLUDED = "included";
    public static final String P_RECOMMENDED = "recommended";
    public static final String P_SHOULD_BE_ENROLLED = "shouldBeEnrolled";

    private EnrEnrollmentStep _step;     // Шаг зачисления
    private EnrRatingItem _entity;     // Абитуриент
    private boolean _entrantArchived;     // Абитуриент архивен
    private boolean _entrantRequestTakeAwayDocuments;     // Абитуриент отозвал заявление
    private boolean _originalIn = false;     // Сдан оригинал док. об образ.
    private boolean _accepted = false;     // Согласие на зачисление
    private boolean _enrollmentAvailable = false;     // Согласие на зачисление (итоговое)
    private boolean _included;     // Включен в конкурсный список
    private boolean _recommended;     // Рекомендован
    private boolean _shouldBeEnrolled;     // К зачислению

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Шаг зачисления. Свойство не может быть null.
     */
    @NotNull
    public EnrEnrollmentStep getStep()
    {
        return _step;
    }

    /**
     * @param step Шаг зачисления. Свойство не может быть null.
     */
    public void setStep(EnrEnrollmentStep step)
    {
        dirty(_step, step);
        _step = step;
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     */
    @NotNull
    public EnrRatingItem getEntity()
    {
        return _entity;
    }

    /**
     * @param entity Абитуриент. Свойство не может быть null.
     */
    public void setEntity(EnrRatingItem entity)
    {
        dirty(_entity, entity);
        _entity = entity;
    }

    /**
     * Абитуриент помещен в архив
     *
     * @return Абитуриент архивен. Свойство не может быть null.
     */
    @NotNull
    public boolean isEntrantArchived()
    {
        return _entrantArchived;
    }

    /**
     * @param entrantArchived Абитуриент архивен. Свойство не может быть null.
     */
    public void setEntrantArchived(boolean entrantArchived)
    {
        dirty(_entrantArchived, entrantArchived);
        _entrantArchived = entrantArchived;
    }

    /**
     * Абитуриент отозвал заявление
     *
     * @return Абитуриент отозвал заявление. Свойство не может быть null.
     */
    @NotNull
    public boolean isEntrantRequestTakeAwayDocuments()
    {
        return _entrantRequestTakeAwayDocuments;
    }

    /**
     * @param entrantRequestTakeAwayDocuments Абитуриент отозвал заявление. Свойство не может быть null.
     */
    public void setEntrantRequestTakeAwayDocuments(boolean entrantRequestTakeAwayDocuments)
    {
        dirty(_entrantRequestTakeAwayDocuments, entrantRequestTakeAwayDocuments);
        _entrantRequestTakeAwayDocuments = entrantRequestTakeAwayDocuments;
    }

    /**
     * Сдан оригинал документа об образовании, использованного при подаче соотв. заявления.
     *
     * @return Сдан оригинал док. об образ.. Свойство не может быть null.
     */
    @NotNull
    public boolean isOriginalIn()
    {
        return _originalIn;
    }

    /**
     * @param originalIn Сдан оригинал док. об образ.. Свойство не может быть null.
     */
    public void setOriginalIn(boolean originalIn)
    {
        dirty(_originalIn, originalIn);
        _originalIn = originalIn;
    }

    /**
     * Получено согласие на зачисление.
     *
     * @return Согласие на зачисление. Свойство не может быть null.
     */
    @NotNull
    public boolean isAccepted()
    {
        return _accepted;
    }

    /**
     * @param accepted Согласие на зачисление. Свойство не может быть null.
     */
    public void setAccepted(boolean accepted)
    {
        dirty(_accepted, accepted);
        _accepted = accepted;
    }

    /**
     * Итоговое согласие на зачисление.
     *
     * @return Согласие на зачисление (итоговое). Свойство не может быть null.
     */
    @NotNull
    public boolean isEnrollmentAvailable()
    {
        return _enrollmentAvailable;
    }

    /**
     * @param enrollmentAvailable Согласие на зачисление (итоговое). Свойство не может быть null.
     */
    public void setEnrollmentAvailable(boolean enrollmentAvailable)
    {
        dirty(_enrollmentAvailable, enrollmentAvailable);
        _enrollmentAvailable = enrollmentAvailable;
    }

    /**
     * У кого included - false, должны интерпретироваться, как отсутствующие в данном списке, несмотря на то, что объекты есть.
     *
     * @return Включен в конкурсный список. Свойство не может быть null.
     */
    @NotNull
    public boolean isIncluded()
    {
        return _included;
    }

    /**
     * @param included Включен в конкурсный список. Свойство не может быть null.
     */
    public void setIncluded(boolean included)
    {
        dirty(_included, included);
        _included = included;
    }

    /**
     * Рекомендован к зачислению.
     *
     * @return Рекомендован. Свойство не может быть null.
     */
    @NotNull
    public boolean isRecommended()
    {
        return _recommended;
    }

    /**
     * @param recommended Рекомендован. Свойство не может быть null.
     */
    public void setRecommended(boolean recommended)
    {
        dirty(_recommended, recommended);
        _recommended = recommended;
    }

    /**
     * Должен быть включен в приказ о зачислении (был рекомендован, сдал все что надо, и был выбран для зачисления автоматом или оператором).
     *
     * @return К зачислению. Свойство не может быть null.
     */
    @NotNull
    public boolean isShouldBeEnrolled()
    {
        return _shouldBeEnrolled;
    }

    /**
     * @param shouldBeEnrolled К зачислению. Свойство не может быть null.
     */
    public void setShouldBeEnrolled(boolean shouldBeEnrolled)
    {
        dirty(_shouldBeEnrolled, shouldBeEnrolled);
        _shouldBeEnrolled = shouldBeEnrolled;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrEnrollmentStepItemGen)
        {
            if (withNaturalIdProperties)
            {
                setStep(((EnrEnrollmentStepItem)another).getStep());
                setEntity(((EnrEnrollmentStepItem)another).getEntity());
            }
            setEntrantArchived(((EnrEnrollmentStepItem)another).isEntrantArchived());
            setEntrantRequestTakeAwayDocuments(((EnrEnrollmentStepItem)another).isEntrantRequestTakeAwayDocuments());
            setOriginalIn(((EnrEnrollmentStepItem)another).isOriginalIn());
            setAccepted(((EnrEnrollmentStepItem)another).isAccepted());
            setEnrollmentAvailable(((EnrEnrollmentStepItem)another).isEnrollmentAvailable());
            setIncluded(((EnrEnrollmentStepItem)another).isIncluded());
            setRecommended(((EnrEnrollmentStepItem)another).isRecommended());
            setShouldBeEnrolled(((EnrEnrollmentStepItem)another).isShouldBeEnrolled());
        }
    }

    public INaturalId<EnrEnrollmentStepItemGen> getNaturalId()
    {
        return new NaturalId(getStep(), getEntity());
    }

    public static class NaturalId extends NaturalIdBase<EnrEnrollmentStepItemGen>
    {
        private static final String PROXY_NAME = "EnrEnrollmentStepItemNaturalProxy";

        private Long _step;
        private Long _entity;

        public NaturalId()
        {}

        public NaturalId(EnrEnrollmentStep step, EnrRatingItem entity)
        {
            _step = ((IEntity) step).getId();
            _entity = ((IEntity) entity).getId();
        }

        public Long getStep()
        {
            return _step;
        }

        public void setStep(Long step)
        {
            _step = step;
        }

        public Long getEntity()
        {
            return _entity;
        }

        public void setEntity(Long entity)
        {
            _entity = entity;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrEnrollmentStepItemGen.NaturalId) ) return false;

            EnrEnrollmentStepItemGen.NaturalId that = (NaturalId) o;

            if( !equals(getStep(), that.getStep()) ) return false;
            if( !equals(getEntity(), that.getEntity()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getStep());
            result = hashCode(result, getEntity());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getStep());
            sb.append("/");
            sb.append(getEntity());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrEnrollmentStepItemGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrEnrollmentStepItem.class;
        }

        public T newInstance()
        {
            return (T) new EnrEnrollmentStepItem();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "step":
                    return obj.getStep();
                case "entity":
                    return obj.getEntity();
                case "entrantArchived":
                    return obj.isEntrantArchived();
                case "entrantRequestTakeAwayDocuments":
                    return obj.isEntrantRequestTakeAwayDocuments();
                case "originalIn":
                    return obj.isOriginalIn();
                case "accepted":
                    return obj.isAccepted();
                case "enrollmentAvailable":
                    return obj.isEnrollmentAvailable();
                case "included":
                    return obj.isIncluded();
                case "recommended":
                    return obj.isRecommended();
                case "shouldBeEnrolled":
                    return obj.isShouldBeEnrolled();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "step":
                    obj.setStep((EnrEnrollmentStep) value);
                    return;
                case "entity":
                    obj.setEntity((EnrRatingItem) value);
                    return;
                case "entrantArchived":
                    obj.setEntrantArchived((Boolean) value);
                    return;
                case "entrantRequestTakeAwayDocuments":
                    obj.setEntrantRequestTakeAwayDocuments((Boolean) value);
                    return;
                case "originalIn":
                    obj.setOriginalIn((Boolean) value);
                    return;
                case "accepted":
                    obj.setAccepted((Boolean) value);
                    return;
                case "enrollmentAvailable":
                    obj.setEnrollmentAvailable((Boolean) value);
                    return;
                case "included":
                    obj.setIncluded((Boolean) value);
                    return;
                case "recommended":
                    obj.setRecommended((Boolean) value);
                    return;
                case "shouldBeEnrolled":
                    obj.setShouldBeEnrolled((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "step":
                        return true;
                case "entity":
                        return true;
                case "entrantArchived":
                        return true;
                case "entrantRequestTakeAwayDocuments":
                        return true;
                case "originalIn":
                        return true;
                case "accepted":
                        return true;
                case "enrollmentAvailable":
                        return true;
                case "included":
                        return true;
                case "recommended":
                        return true;
                case "shouldBeEnrolled":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "step":
                    return true;
                case "entity":
                    return true;
                case "entrantArchived":
                    return true;
                case "entrantRequestTakeAwayDocuments":
                    return true;
                case "originalIn":
                    return true;
                case "accepted":
                    return true;
                case "enrollmentAvailable":
                    return true;
                case "included":
                    return true;
                case "recommended":
                    return true;
                case "shouldBeEnrolled":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "step":
                    return EnrEnrollmentStep.class;
                case "entity":
                    return EnrRatingItem.class;
                case "entrantArchived":
                    return Boolean.class;
                case "entrantRequestTakeAwayDocuments":
                    return Boolean.class;
                case "originalIn":
                    return Boolean.class;
                case "accepted":
                    return Boolean.class;
                case "enrollmentAvailable":
                    return Boolean.class;
                case "included":
                    return Boolean.class;
                case "recommended":
                    return Boolean.class;
                case "shouldBeEnrolled":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrEnrollmentStepItem> _dslPath = new Path<EnrEnrollmentStepItem>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrEnrollmentStepItem");
    }
            

    /**
     * @return Шаг зачисления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItem#getStep()
     */
    public static EnrEnrollmentStep.Path<EnrEnrollmentStep> step()
    {
        return _dslPath.step();
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItem#getEntity()
     */
    public static EnrRatingItem.Path<EnrRatingItem> entity()
    {
        return _dslPath.entity();
    }

    /**
     * Абитуриент помещен в архив
     *
     * @return Абитуриент архивен. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItem#isEntrantArchived()
     */
    public static PropertyPath<Boolean> entrantArchived()
    {
        return _dslPath.entrantArchived();
    }

    /**
     * Абитуриент отозвал заявление
     *
     * @return Абитуриент отозвал заявление. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItem#isEntrantRequestTakeAwayDocuments()
     */
    public static PropertyPath<Boolean> entrantRequestTakeAwayDocuments()
    {
        return _dslPath.entrantRequestTakeAwayDocuments();
    }

    /**
     * Сдан оригинал документа об образовании, использованного при подаче соотв. заявления.
     *
     * @return Сдан оригинал док. об образ.. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItem#isOriginalIn()
     */
    public static PropertyPath<Boolean> originalIn()
    {
        return _dslPath.originalIn();
    }

    /**
     * Получено согласие на зачисление.
     *
     * @return Согласие на зачисление. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItem#isAccepted()
     */
    public static PropertyPath<Boolean> accepted()
    {
        return _dslPath.accepted();
    }

    /**
     * Итоговое согласие на зачисление.
     *
     * @return Согласие на зачисление (итоговое). Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItem#isEnrollmentAvailable()
     */
    public static PropertyPath<Boolean> enrollmentAvailable()
    {
        return _dslPath.enrollmentAvailable();
    }

    /**
     * У кого included - false, должны интерпретироваться, как отсутствующие в данном списке, несмотря на то, что объекты есть.
     *
     * @return Включен в конкурсный список. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItem#isIncluded()
     */
    public static PropertyPath<Boolean> included()
    {
        return _dslPath.included();
    }

    /**
     * Рекомендован к зачислению.
     *
     * @return Рекомендован. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItem#isRecommended()
     */
    public static PropertyPath<Boolean> recommended()
    {
        return _dslPath.recommended();
    }

    /**
     * Должен быть включен в приказ о зачислении (был рекомендован, сдал все что надо, и был выбран для зачисления автоматом или оператором).
     *
     * @return К зачислению. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItem#isShouldBeEnrolled()
     */
    public static PropertyPath<Boolean> shouldBeEnrolled()
    {
        return _dslPath.shouldBeEnrolled();
    }

    public static class Path<E extends EnrEnrollmentStepItem> extends EntityPath<E>
    {
        private EnrEnrollmentStep.Path<EnrEnrollmentStep> _step;
        private EnrRatingItem.Path<EnrRatingItem> _entity;
        private PropertyPath<Boolean> _entrantArchived;
        private PropertyPath<Boolean> _entrantRequestTakeAwayDocuments;
        private PropertyPath<Boolean> _originalIn;
        private PropertyPath<Boolean> _accepted;
        private PropertyPath<Boolean> _enrollmentAvailable;
        private PropertyPath<Boolean> _included;
        private PropertyPath<Boolean> _recommended;
        private PropertyPath<Boolean> _shouldBeEnrolled;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Шаг зачисления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItem#getStep()
     */
        public EnrEnrollmentStep.Path<EnrEnrollmentStep> step()
        {
            if(_step == null )
                _step = new EnrEnrollmentStep.Path<EnrEnrollmentStep>(L_STEP, this);
            return _step;
        }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItem#getEntity()
     */
        public EnrRatingItem.Path<EnrRatingItem> entity()
        {
            if(_entity == null )
                _entity = new EnrRatingItem.Path<EnrRatingItem>(L_ENTITY, this);
            return _entity;
        }

    /**
     * Абитуриент помещен в архив
     *
     * @return Абитуриент архивен. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItem#isEntrantArchived()
     */
        public PropertyPath<Boolean> entrantArchived()
        {
            if(_entrantArchived == null )
                _entrantArchived = new PropertyPath<Boolean>(EnrEnrollmentStepItemGen.P_ENTRANT_ARCHIVED, this);
            return _entrantArchived;
        }

    /**
     * Абитуриент отозвал заявление
     *
     * @return Абитуриент отозвал заявление. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItem#isEntrantRequestTakeAwayDocuments()
     */
        public PropertyPath<Boolean> entrantRequestTakeAwayDocuments()
        {
            if(_entrantRequestTakeAwayDocuments == null )
                _entrantRequestTakeAwayDocuments = new PropertyPath<Boolean>(EnrEnrollmentStepItemGen.P_ENTRANT_REQUEST_TAKE_AWAY_DOCUMENTS, this);
            return _entrantRequestTakeAwayDocuments;
        }

    /**
     * Сдан оригинал документа об образовании, использованного при подаче соотв. заявления.
     *
     * @return Сдан оригинал док. об образ.. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItem#isOriginalIn()
     */
        public PropertyPath<Boolean> originalIn()
        {
            if(_originalIn == null )
                _originalIn = new PropertyPath<Boolean>(EnrEnrollmentStepItemGen.P_ORIGINAL_IN, this);
            return _originalIn;
        }

    /**
     * Получено согласие на зачисление.
     *
     * @return Согласие на зачисление. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItem#isAccepted()
     */
        public PropertyPath<Boolean> accepted()
        {
            if(_accepted == null )
                _accepted = new PropertyPath<Boolean>(EnrEnrollmentStepItemGen.P_ACCEPTED, this);
            return _accepted;
        }

    /**
     * Итоговое согласие на зачисление.
     *
     * @return Согласие на зачисление (итоговое). Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItem#isEnrollmentAvailable()
     */
        public PropertyPath<Boolean> enrollmentAvailable()
        {
            if(_enrollmentAvailable == null )
                _enrollmentAvailable = new PropertyPath<Boolean>(EnrEnrollmentStepItemGen.P_ENROLLMENT_AVAILABLE, this);
            return _enrollmentAvailable;
        }

    /**
     * У кого included - false, должны интерпретироваться, как отсутствующие в данном списке, несмотря на то, что объекты есть.
     *
     * @return Включен в конкурсный список. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItem#isIncluded()
     */
        public PropertyPath<Boolean> included()
        {
            if(_included == null )
                _included = new PropertyPath<Boolean>(EnrEnrollmentStepItemGen.P_INCLUDED, this);
            return _included;
        }

    /**
     * Рекомендован к зачислению.
     *
     * @return Рекомендован. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItem#isRecommended()
     */
        public PropertyPath<Boolean> recommended()
        {
            if(_recommended == null )
                _recommended = new PropertyPath<Boolean>(EnrEnrollmentStepItemGen.P_RECOMMENDED, this);
            return _recommended;
        }

    /**
     * Должен быть включен в приказ о зачислении (был рекомендован, сдал все что надо, и был выбран для зачисления автоматом или оператором).
     *
     * @return К зачислению. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItem#isShouldBeEnrolled()
     */
        public PropertyPath<Boolean> shouldBeEnrolled()
        {
            if(_shouldBeEnrolled == null )
                _shouldBeEnrolled = new PropertyPath<Boolean>(EnrEnrollmentStepItemGen.P_SHOULD_BE_ENROLLED, this);
            return _shouldBeEnrolled;
        }

        public Class getEntityClass()
        {
            return EnrEnrollmentStepItem.class;
        }

        public String getEntityName()
        {
            return "enrEnrollmentStepItem";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
