/* $Id$ */
package ru.tandemservice.unienr14.settings.bo.EnrSettings.ui.AdmissionRequirements;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.catalog.entity.EnrTieBreakRatingRule;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;

/**
 * @author nvankov
 * @since 2/18/16
 */
@Configuration
public class EnrSettingsAdmissionRequirements extends BusinessComponentManager
{
    public static final String ENR_SETTINGS_ACCOUNTING_RULES_ORIG_EDU_DOC_DS = "accountingRulesOrigEduDocDS";
    public static final String ENR_SETTINGS_ENROLL_RULES_BUDGET_DS ="enrollmentRulesBudgetDS";
    public static final String ENR_SETTINGS_ACCOUNTING_RULES_AGREEMENT_BUDGET_DS ="accountingRulesAgreementBudgetDS";
    public static final String ENR_SETTINGS_ENROLL_RULES_CONTRACT_DS ="enrollmentRulesContractDS";
    public static final String ENR_SETTINGS_ACCOUNTING_RULES_AGREEMENT_CONTRACT_DS ="accountingRulesAgreementContractDS";
    public static final String ENR_SETTINGS_ACCEPTED_DEFAULT_CONTRACT_DS ="acceptedContractDefaultDS";
    public static final String ENR_SETTINGS_TIE_BREAK_RATING_RULE_DS = "tieBreakRatingRuleDS";
    public static final String ENR_SETTINGS_REENROLL_ENABLED_DS = "reenrollEnabledDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
                .addDataSource(selectDS(ENR_SETTINGS_ACCOUNTING_RULES_ORIG_EDU_DOC_DS, accountingRulesOrigEduDocDSHandler()))
                .addDataSource(CommonManager.instance().yesNoDSConfig())
                .addDataSource(selectDS(ENR_SETTINGS_ENROLL_RULES_BUDGET_DS, enrollmentRulesBudgetDSHandler()))
                .addDataSource(selectDS(ENR_SETTINGS_ACCOUNTING_RULES_AGREEMENT_BUDGET_DS, accountingRulesAgreementBudgetDSHandler()))
                .addDataSource(selectDS(ENR_SETTINGS_ENROLL_RULES_CONTRACT_DS, enrollmentRulesContractDSHandler()))
                .addDataSource(selectDS(ENR_SETTINGS_ACCOUNTING_RULES_AGREEMENT_CONTRACT_DS, accountingRulesAgreementContractDSHandler()))
                .addDataSource(selectDS(ENR_SETTINGS_ACCEPTED_DEFAULT_CONTRACT_DS, acceptedContractDefaultDSHandler()))
                .addDataSource(selectDS(ENR_SETTINGS_REENROLL_ENABLED_DS, reenrollEnabledDSHandler()))
                .addDataSource(selectDS(ENR_SETTINGS_TIE_BREAK_RATING_RULE_DS, tieBreakRatingRuleDS()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler acceptedContractDefaultDSHandler()
    {
        return new TwinComboDataSourceHandler(getName()).noFirst();
    }

    // «Правила учета оригинала документа об образовании абитуриента»
    @Bean
    public IDefaultComboDataSourceHandler accountingRulesOrigEduDocDSHandler()
    {
        return new TwinComboDataSourceHandler(getName())
                .yesTitle("Оригинал учитывается по всем выбранным конкурсам из заявлений, к которым приложен документ")
                .noTitle("Оригинал учитывается по одному выбранному конкурсу из всех заявлений, к которым приложен документ");
    }

    // «Условия зачисления при поступлении на места в рамках КЦП»
    @Bean
    public IDefaultComboDataSourceHandler enrollmentRulesBudgetDSHandler()
    {
        return new TwinComboDataSourceHandler(getName())
                .yesTitle("Требуется оригинал, согласие на зачисление не требуется")
                .noTitle("Требуется оригинал и согласие на зачисление");
    }

    // «Правила учета согласия на зачисление абитуриента при поступлении на места в рамках КЦП» — отображается, если в предыдущем выбрано «Требуется оригинал и согласие на зачисление»
    @Bean
    public IDefaultComboDataSourceHandler accountingRulesAgreementBudgetDSHandler()
    {
        return new TwinComboDataSourceHandler(getName())
                .yesTitle("Согласие на зачисление может быть подано только по одному выбранному конкурсу в рамках одного вида заявления")
                .noTitle("Согласие на зачисление может быть подано по нескольким выбранным конкурсам");
    }

    // «Условия зачисления при поступлении на места по договору»
    public final static Long OPTION_ENROLL_RULES_CONTRACT_FIRST = 0L; // «Требуется оригинал, согласие на зачисление не требуется»
    public final static Long OPTION_ENROLL_RULES_CONTRACT_SECOND = 1L;// «Требуется согласие на зачисление, оригинал не требуется» — включено по умолчанию
    public final static Long OPTION_ENROLL_RULES_CONTRACT_THIRD = 2L;// «Требуется оригинал или согласие на зачисление»
    public final static Long OPTION_ENROLL_RULES_CONTRACT_FOURTH = 3L;// «Требуется оригинал и согласие на зачисление»

    @Bean
    public IDefaultComboDataSourceHandler enrollmentRulesContractDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName()).addItemList(enrollmentRulesContractExtPoint());
    }

    @Bean
    public ItemListExtPoint<DataWrapper> enrollmentRulesContractExtPoint()
    {
        return itemList(DataWrapper.class)
                .add(OPTION_ENROLL_RULES_CONTRACT_FIRST.toString(), new DataWrapper(OPTION_ENROLL_RULES_CONTRACT_FIRST, "Требуется оригинал, согласие на зачисление не требуется"))
                .add(OPTION_ENROLL_RULES_CONTRACT_SECOND.toString(), new DataWrapper(OPTION_ENROLL_RULES_CONTRACT_SECOND, "Требуется согласие на зачисление, оригинал не требуется"))
                .add(OPTION_ENROLL_RULES_CONTRACT_THIRD.toString(), new DataWrapper(OPTION_ENROLL_RULES_CONTRACT_THIRD, "Требуется оригинал или согласие на зачисление"))
                .add(OPTION_ENROLL_RULES_CONTRACT_FOURTH.toString(), new DataWrapper(OPTION_ENROLL_RULES_CONTRACT_FOURTH, "Требуется оригинал и согласие на зачисление"))
                .create();
    }

    // «Правила учета согласия на зачисление абитуриента при поступлении на места по договору» — отображается, если в предыдущем выбрано отличное от «Требуется оригинал, согласие на зачисление не требуется»
    @Bean
    public IDefaultComboDataSourceHandler accountingRulesAgreementContractDSHandler()
    {
        return new TwinComboDataSourceHandler(getName())
                .yesTitle("Согласие на зачисление может быть подано только по одному выбранному конкурсу в рамках одного вида заявления")
                .noTitle("Согласие на зачисление может быть подано по нескольким выбранным конкурсам");
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> tieBreakRatingRuleDS()
    {
        return new EntityComboDataSourceHandler(getName(), EnrTieBreakRatingRule.class)
                .order(EnrTieBreakRatingRule.code());
    }
    @Bean
    public IDefaultComboDataSourceHandler reenrollEnabledDSHandler()
    {
        return new TwinComboDataSourceHandler(getName()).noFirst();
    }
}



    