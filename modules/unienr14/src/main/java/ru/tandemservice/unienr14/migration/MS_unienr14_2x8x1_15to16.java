/* $Id$ */
package ru.tandemservice.unienr14.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * @author Nikolay Fedorovskih
 * @since 15.06.2015
 */
public class MS_unienr14_2x8x1_15to16 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.17"),
                        new ScriptDependency("org.tandemframework.shared", "1.8.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        tool.executeUpdate("update scriptitem_t set code_p=? where code_p=?", "common.entrantRequest.entrantRequestBenefits", "common.entrantRequest.competitionNoExams");
    }
}