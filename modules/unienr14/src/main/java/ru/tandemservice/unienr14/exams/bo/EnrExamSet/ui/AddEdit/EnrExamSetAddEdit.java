/**
 *$Id: EnrExamSetAddEdit.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.exams.bo.EnrExamSet.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.catalog.entity.EnrExamType;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.logic.ExamSetElementValuesSelectDSHandler;

/**
 * @author Alexander Shaburov
 * @since 17.04.13
 */
@Configuration
public class EnrExamSetAddEdit extends BusinessComponentManager
{
    public static final String ELEMENT_VALUE_SELECT_DS = "elementValueDS";
    public static final String EXAM_TYPE_SELECT_DS = "examTypeDS";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ELEMENT_VALUE_SELECT_DS, elementValueDSHandler()))
                .addDataSource(selectDS(EXAM_TYPE_SELECT_DS, examTypeDSHandler()).addColumn(EnrExamType.shortTitle().s()))
                .create();
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> examTypeDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrExamType.class)
                .order(EnrExamType.title())
                .filter(EnrExamType.title())
                .pageable(true);
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> elementValueDSHandler()
    {
        return new ExamSetElementValuesSelectDSHandler(getName());
    }
}
