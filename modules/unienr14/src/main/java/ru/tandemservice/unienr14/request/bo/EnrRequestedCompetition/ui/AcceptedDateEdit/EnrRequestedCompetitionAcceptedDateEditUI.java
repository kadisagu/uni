/* $Id$ */
package ru.tandemservice.unienr14.request.bo.EnrRequestedCompetition.ui.AcceptedDateEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.sec.runtime.SecurityRuntime;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.EnrEntrantRequestManager;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.DirectionListAddon.EnrEntrantRequestDirectionListAddon;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.bo.EnrSettings.ui.AdmissionRequirements.EnrSettingsAdmissionRequirements;

import java.util.Date;

/**
 * @author nvankov
 * @since 31.05.2016
 */
@Input({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "entityHolder.id", required = true)
})
public class EnrRequestedCompetitionAcceptedDateEditUI extends UIPresenter
{
    private EntityHolder<EnrRequestedCompetition> _entityHolder = new EntityHolder<>();
    private Date _acceptedDate;
    private Date refusedDate;

    private CommonPostfixPermissionModel sec;

    @Override
    public void onComponentRefresh()
    {
        setSec(new CommonPostfixPermissionModel("enrEntrantRequestPub"));
        _entityHolder.refresh();
        setAcceptedDate(getCompetition().getAcceptedDate());
        setRefusedDate(getCompetition().getRefusedDate());
    }

    public void onClickApply()
    {
        EnrEntrantRequestManager.instance().dao().doChangeAcceptedByDate(getCompetition().getId(), getAcceptedDate());
        if (getSecurityKeyStatus("refuseToBeEnrolled")) {
            EnrEntrantRequestManager.instance().dao().doChangeRefusedToBeEnrolledByDate(getCompetition().getId(), getRefusedDate());
        }
        deactivate();
    }

    public boolean isShowAcceptedDate()
    {
        return getSecurityKeyStatus("acceptEnrollment")
                && !(EnrSettingsAdmissionRequirements.OPTION_ENROLL_RULES_CONTRACT_FIRST.equals(getCompetition().getRequest().getEntrant().getEnrollmentCampaign().getSettings().getEnrollmentRulesContract())
                    && !getCompetition().getCompetition().getType().getCompensationType().isBudget())
                && !(TwinComboDataSourceHandler.YES_ID.equals(getCompetition().getRequest().getEntrant().getEnrollmentCampaign().getSettings().getEnrollmentRulesBudget())
                    && getCompetition().getCompetition().getType().getCompensationType().isBudget());
    }

    public boolean isShowRefusedDate()
    {
        return getSecurityKeyStatus("refuseToBeEnrolled");
    }

    // getters and setters

    public EnrRequestedCompetition getCompetition()
    {
        return getEntityHolder().getValue();
    }

    public EntityHolder<EnrRequestedCompetition> getEntityHolder()
    {
        return _entityHolder;
    }

    public void setEntityHolder(EntityHolder<EnrRequestedCompetition> entityHolder)
    {
        _entityHolder = entityHolder;
    }

    public Date getAcceptedDate()
    {
        return _acceptedDate;
    }

    public void setAcceptedDate(Date acceptedDate)
    {
        _acceptedDate = acceptedDate;
    }

    public Date getRefusedDate() {
        return refusedDate;
    }

    public void setRefusedDate(Date refusedDate) {
        this.refusedDate = refusedDate;
    }

    public CommonPostfixPermissionModel getSec()
    {
        return sec;
    }

    public void setSec(CommonPostfixPermissionModel sec)
    {
        this.sec = sec;
    }

    public Boolean getSecurityKeyStatus(String permission) {
        String secKey = getSec().getPermission(permission);
        return CoreServices.securityService().check(getCompetition().getRequest(), ContextLocal.getUserContext().getPrincipalContext(), secKey);
    }
}
