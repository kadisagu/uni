/* $Id:$ */
package ru.tandemservice.unienr14.exams.bo.EnrStateExamResult.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.Pub.EnrEntrantPub;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult;

/**
 * @author oleyba
 * @since 7/16/14
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "holder.id")
})
public class EnrStateExamResultPubUI extends UIPresenter
{
    private EntityHolder<EnrEntrantStateExamResult> _holder = new EntityHolder<>();

    @Override
    public void onComponentRefresh()
    {
        getHolder().refresh();
        _uiActivation.asDesktopRoot(EnrEntrantPub.class)
            .parameter(PublisherActivator.PUBLISHER_ID_KEY, getHolder().getValue().getEntrant().getId())
            .parameter("selectedTab", "stateExamResultTab")
            .activate();
    }

    // getters and setters

    public EntityHolder<EnrEntrantStateExamResult> getHolder()
    {
        return _holder;
    }
}