package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.ui.CheckEnrTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

@Configuration
public class EnrEnrollmentStepCheckEnrTab extends BusinessComponentManager 
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder().create();
    }

}
