package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x8x1_24to25 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrOnlineEntrant

		//  свойство sameAddress стало обязательным
		{
			// задать значение по умолчанию
			java.lang.Boolean defaultSameAddress = false;
			tool.executeUpdate("update enronlineentrant_t set sameaddress_p=? where sameaddress_p is null", defaultSameAddress);

			// сделать колонку NOT NULL
			tool.setColumnNullable("enronlineentrant_t", "sameaddress_p", false);

		}

		//  свойство needDorm стало обязательным
		{
			// задать значение по умолчанию
			java.lang.Boolean defaultNeedDorm = false;
			tool.executeUpdate("update enronlineentrant_t set needdorm_p=? where needdorm_p is null", defaultNeedDorm);

			// сделать колонку NOT NULL
			tool.setColumnNullable("enronlineentrant_t", "needdorm_p", false);

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrOnlineEntrantStatement

		//  свойство exclusiveRights стало обязательным
		{
			// задать значение по умолчанию
			java.lang.Boolean defaultExclusiveRights = false;
			tool.executeUpdate("update enronlineentrantstatement_t set exclusiverights_p=? where exclusiverights_p is null", defaultExclusiveRights);

			// сделать колонку NOT NULL
			tool.setColumnNullable("enronlineentrantstatement_t", "exclusiverights_p", false);

		}

		//  свойство exclusiveRightsWithoutExams стало обязательным
		{
			// задать значение по умолчанию
			java.lang.Boolean defaultExclusiveRightsWithoutExams = false;
			tool.executeUpdate("update enronlineentrantstatement_t set exclusiverightswithoutexams_p=? where exclusiverightswithoutexams_p is null", defaultExclusiveRightsWithoutExams);

			// сделать колонку NOT NULL
			tool.setColumnNullable("enronlineentrantstatement_t", "exclusiverightswithoutexams_p", false);

		}

		//  свойство exclusiveRightsBudget стало обязательным
		{
			// задать значение по умолчанию
			java.lang.Boolean defaultExclusiveRightsBudget = false;
			tool.executeUpdate("update enronlineentrantstatement_t set exclusiverightsbudget_p=? where exclusiverightsbudget_p is null", defaultExclusiveRightsBudget);

			// сделать колонку NOT NULL
			tool.setColumnNullable("enronlineentrantstatement_t", "exclusiverightsbudget_p", false);

		}

		//  свойство exclusiveRightsEmptiveEnroll стало обязательным
		{
			// задать значение по умолчанию
			java.lang.Boolean defaultExclusiveRightsEmptiveEnroll = false;
			tool.executeUpdate("update enronlineentrantstatement_t set exclusiverightsemptiveenroll_p=? where exclusiverightsemptiveenroll_p is null", defaultExclusiveRightsEmptiveEnroll);

			// сделать колонку NOT NULL
			tool.setColumnNullable("enronlineentrantstatement_t", "exclusiverightsemptiveenroll_p", false);

		}

		//  свойство haveAgreementTargetEdu стало обязательным
		{
			// задать значение по умолчанию
			java.lang.Boolean defaultHaveAgreementTargetEdu = false;
			tool.executeUpdate("update enronlineentrantstatement_t set haveagreementtargetedu_p=? where haveagreementtargetedu_p is null", defaultHaveAgreementTargetEdu);

			// сделать колонку NOT NULL
			tool.setColumnNullable("enronlineentrantstatement_t", "haveagreementtargetedu_p", false);

		}


    }
}