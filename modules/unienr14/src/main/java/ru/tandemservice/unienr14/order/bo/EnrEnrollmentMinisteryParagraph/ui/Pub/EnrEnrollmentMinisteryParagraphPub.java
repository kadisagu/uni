/* $Id$ */
package ru.tandemservice.unienr14.order.bo.EnrEnrollmentMinisteryParagraph.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.IColumnListExtPointBuilder;
import org.tandemframework.caf.ui.config.datasource.column.ActionDSColumn;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.formatter.NoWrapFormatter;
import org.tandemframework.core.view.list.column.DefaultPublisherLinkResolver;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.formatter.EnrEntrantCustomStateCollectionFormatter;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.Pub.EnrEntrantPub;
import ru.tandemservice.unienr14.order.bo.EnrEnrollmentMinisteryParagraph.logic.EnrEnrollmentMinisteryExtractDSHandler;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentMinisteryExtract;
import ru.tandemservice.unienr14.request.entity.EnrEntrantForeignRequest;

/**
 * @author nvankov
 * @since 9/1/14
 */
@Configuration
public class EnrEnrollmentMinisteryParagraphPub extends BusinessComponentManager
{
    public static final String ENROLLMENT_EXTRACT_DS = "enrollmentExtractDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(searchListDS(ENROLLMENT_EXTRACT_DS, enrollmentExtractDS(), enrollmentExtractDSHandler()))
                .create();
    }



    /**
     * Создается точка расширения "Список выписок о зачислении по направлению от министерства в карточке параграфа", единая
     * для всех публикаторов параграфов о зачислении  по направлению от министерства
     *
     * @return точка расширения "Список выписок о зачислении  по направлению от министерства в карточке параграфа"
     */
    @Bean
    public ColumnListExtPoint enrollmentExtractDS()
    {
        IColumnListExtPointBuilder columnBuilder = ColumnListExtPoint.with(getName(), ENROLLMENT_EXTRACT_DS)
                .addColumn(publisherColumn("fio", EnrEntrantForeignRequest.entrant().person().fullFio())
                                .entityListProperty(EnrEnrollmentMinisteryExtract.entrantRequest().s())
//                                .primaryKeyPath(EnrAbstractExtract.paragraph().order().id())
                                .publisherLinkResolver(new DefaultPublisherLinkResolver()
                                {
                                    @Override
                                    public String getComponentName(IEntity entity) { return EnrEntrantPub.class.getSimpleName(); }

                                    @Override
                                    public Object getParameters(IEntity entity)
                                    {
                                        return new ParametersMap()
                                                .add(PublisherActivator.PUBLISHER_ID_KEY, ((EnrEntrantForeignRequest) entity).getEntrant().getId())
                                                .add("selectedTab", "foreignRequestTab");
                                    }
                                })
                                .create()
                )
                .addColumn(textColumn(EnrEnrollmentMinisteryExtractDSHandler.CUSTOM_STATES, EnrEnrollmentMinisteryExtractDSHandler.CUSTOM_STATES).formatter(new EnrEntrantCustomStateCollectionFormatter()))
                .addColumn(textColumn("sex", EnrEnrollmentMinisteryExtract.entrantRequest().entrant().person().identityCard().sex().shortTitle()).create())
                .addColumn(textColumn("passport", EnrEnrollmentMinisteryExtract.entrantRequest().entrant().person().identityCard().fullNumber()).formatter(NoWrapFormatter.INSTANCE).create())
                .addColumn(textColumn("citizenship", EnrEnrollmentMinisteryExtract.entrantRequest().entrant().person().identityCard().citizenship().title()).formatter(NoWrapFormatter.INSTANCE).create())
                .addColumn(textColumn("reqNumber", EnrEnrollmentMinisteryExtract.entrantRequest().regNumber()).create())
                .addColumn(textColumn("competition", EnrEnrollmentMinisteryExtract.entrantRequest().competition().title()).create());

        if (Debug.isEnabled()) {
            columnBuilder.addColumn(textColumn("extractState", EnrEnrollmentMinisteryExtract.state().title()));
        }

        return columnBuilder
                .addColumn(publisherColumn("otherExtracts", "title")
                        .entityListProperty(EnrEnrollmentMinisteryExtractDSHandler.OTHER_EXTRACTS)
                        .formatter(CollectionFormatter.COLLECTION_FORMATTER))
                .addColumn(publisherColumn("student", EnrEnrollmentMinisteryExtract.student().personalNumber())
                        .primaryKeyPath(EnrEnrollmentMinisteryExtract.student().id().s()))
                .addColumn(ActionDSColumn.with().name("setStudent").icon(CommonDefines.ICON_CHOOSE)
                        .listener("onClickSetStudent").visible("ui:chooseStudentVisible")
                        .permissionKey("ui:secModel.chooseStudent"))
                .addColumn(ActionDSColumn.with().name("exclude").icon(CommonDefines.ICON_DELETE)
                                .listener("onClickDeleteExtract")
                                .alert(FormattedMessage.with().template("enrollmentMinisteryExtractDS.exclude.alert").parameter(EnrEnrollmentMinisteryExtract.entrantRequest().entrant().person().identityCard().fullFio().s()).create())
                                .permissionKey("ui:secModel.deleteExtract")
                                .disabled(EnrEnrollmentMinisteryExtractDSHandler.NO_EXCLUDE).create()
                )
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> enrollmentExtractDSHandler()
    {
        return new EnrEnrollmentMinisteryExtractDSHandler(getName());
    }
}



    