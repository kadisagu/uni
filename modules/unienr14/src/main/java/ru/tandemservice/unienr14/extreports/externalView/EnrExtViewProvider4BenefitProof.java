/* $Id:$ */
package ru.tandemservice.unienr14.extreports.externalView;

import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.extview.SimpleDQLExternalViewConfig;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.person.catalog.entity.PersonDocumentCategory;
import org.tandemframework.shared.person.catalog.entity.PersonDocumentType;
import ru.tandemservice.unienr14.catalog.entity.*;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantBaseDocument;
import ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma;
import ru.tandemservice.unienr14.request.entity.EnrEntrantBenefitProof;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author oleyba
 * @since 8/27/13
 */
public class EnrExtViewProvider4BenefitProof extends SimpleDQLExternalViewConfig
{

    @Override
    protected DQLSelectBuilder buildDqlQuery() {
        DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(EnrEntrantBenefitProof.class, "proof")

            .joinEntity("proof", DQLJoinType.left, EnrEntrantBaseDocument.class, "doc", eq(property(EnrEntrantBenefitProof.document().fromAlias("proof")), property("doc")))
            .joinPath(DQLJoinType.left, EnrEntrantBaseDocument.documentType().fromAlias("doc"), "docType")
            .joinPath(DQLJoinType.left, EnrEntrantBaseDocument.docRelation().document().documentCategory().fromAlias("doc"), "docCategory")

            .joinEntity("doc", DQLJoinType.left, EnrOlympiadDiploma.class, "dip", eq(property(EnrEntrantBaseDocument.docRelation().document().fromAlias("doc")), property("dip")))
            .joinPath(DQLJoinType.left, EnrOlympiadDiploma.honour().fromAlias("dip"), "dipHonour")
            .joinPath(DQLJoinType.left, EnrOlympiadDiploma.olympiad().fromAlias("dip"), "olymp")
            .joinPath(DQLJoinType.left, EnrOlympiad.olympiadType().fromAlias("olymp"), "olympType")
            .joinPath(DQLJoinType.left, EnrOlympiadDiploma.settlement().fromAlias("dip"), "olympAI")
            .joinPath(DQLJoinType.left, EnrOlympiadDiploma.subject().fromAlias("dip"), "olympSubj");


        column(dql, property(EnrEntrantBenefitProof.benefitStatement().id().fromAlias("proof")), "benefitID").comment("uid факта использования особого права");
        column(dql, property(EnrEntrantBenefitProof.benefitStatement().entrant().id().fromAlias("proof")), "entrantId").comment("uid абитуриента");

        column(dql, property(EnrEntrantBaseDocument.id().fromAlias("doc")), "docID").comment("uid документа, подтверждающего особое право");
        column(dql, property(PersonDocumentType.title().fromAlias("docType")), "documentType").comment("тип документа, подтверждающего особое право");
        column(dql, property(PersonDocumentType.code().fromAlias("docType")), "documentTypeCode").comment("код типа документа, подтверждающего особое право");

        column(dql, property(EnrOlympiadHonour.title().fromAlias("dipHonour")), "honour").comment("степень отличия в дипломе олимпиады");
        column(dql, property(EnrOlympiadHonour.code().fromAlias("dipHonour")), "honourCode").comment("код степени отличия в дипломе олимпиады");
        column(dql, property(EnrOlympiad.title().fromAlias("olymp")), "olympiad").comment("олимпиада");
        column(dql, property(EnrOlympiadType.title().fromAlias("olympType")), "olympiadType").comment("тип олимпиады");
        column(dql, property(EnrOlympiadType.code().fromAlias("olympType")), "olympiadTypeCode").comment("код типа олимпиады");
        column(dql, property(AddressItem.title().fromAlias("olympAI")), "settlement").comment("место проведения олимпиады");
        column(dql, property(EnrOlympiadSubject.title().fromAlias("olympSubj")), "subject").comment("предмет олимпиады");
        column(dql, property(EnrOlympiadSubject.code().fromAlias("olympSubj")), "subjectCode").comment("код предмета олимпиады");

        column(dql, property(PersonDocumentCategory.title().fromAlias("docCategory")), "docCategory").comment("название категории документа");
        column(dql, property(PersonDocumentCategory.code().fromAlias("docCategory")), "docCategoryCode").comment("код категории документа");

        return dql;
    }
}

