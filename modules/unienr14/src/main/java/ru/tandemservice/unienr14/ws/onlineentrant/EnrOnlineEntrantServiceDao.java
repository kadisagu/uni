/* $Id$ */
package ru.tandemservice.unienr14.ws.onlineentrant;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.primitives.Ints;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.tapestry.request.IUploadFile;
import org.hibernate.Session;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.fias.base.bo.AddressBase.AddressBaseManager;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.fias.base.entity.AddressString;
import org.tandemframework.shared.fias.base.entity.ICitizenship;
import org.tandemframework.shared.fias.utils.AddressBaseUtils;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.GraduationHonour2EduDocumentKind;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.IdentityCardToDocumentScanCopy;
import org.tandemframework.shared.person.catalog.entity.*;
import org.tandemframework.shared.person.catalog.entity.codes.IdentityCardTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.*;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.entrant.entity.*;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementType;
import ru.tandemservice.unienr14.ws.ClientSoapFaultException;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 5/8/14
 */
public class EnrOnlineEntrantServiceDao extends CommonDAO implements IEnrOnlineEntrantServiceDao
{
    @Override
    public CatalogsData getCatalogs(String enrollmentCampaignTitle)
    {
//        if(true)
//        {
//            throw new ClientSoapFaultException("01", "test");
//        }

        EnrEnrollmentCampaign enrollmentCampaign = get(EnrEnrollmentCampaign.class, EnrEnrollmentCampaign.title(), enrollmentCampaignTitle);
        if (enrollmentCampaign == null)
            throw new ClientSoapFaultException(ClientSoapFaultException.TANDEM_UNIVERSITY_ERROR_CODE_EXIST_DATA, "Приемная кампания «" + enrollmentCampaignTitle + "» отсутствует в системе Tandem University");

        CatalogsData data = new CatalogsData();

        // модуль person

        // Пол
        for (Sex row : getList(Sex.class, Sex.P_CODE))
            data.sex.row.add(new CatalogsData.CatalogNode(row.getShortTitle(), row.getTitle(), row.getCode()));

        // Пол
        for (AddressCountry row : getList(AddressCountry.class, AddressCountry.P_CODE))
            data.addressCountry.row.add(new CatalogsData.CatalogNode(row.getShortTitle(), row.getTitle(), String.valueOf(row.getCode())));

        // Уровни образования
        for (EduLevel row : getList(EduLevel.class, EduLevel.P_CODE))
            data.eduLevel.row.add(new CatalogsData.CatalogNode(row.getTitle(), row.getCode()));

        // Уровни образования
        for (EduLevel row : getList(EduLevel.class, EduLevel.P_CODE))
            data.eduLevel.row.add(new CatalogsData.CatalogNode(row.getTitle(), row.getCode()));

        // степени родства:
        List<FamilyStatus> familyStatusList = getList(FamilyStatus.class);

        for (FamilyStatus familyStatus : familyStatusList)
        {
            data.familyStatus.catalogNodes.add(new CatalogsData.CatalogNode(familyStatus.getTitle(), familyStatus.getCode()));
        }

        // виды спорта
        List<SportType> sportTypeList = getList(SportType.class);

        for (SportType sportType : sportTypeList)
        {
            data.sportType.catalogNodes.add(new CatalogsData.CatalogNode(sportType.getTitle(), sportType.getCode()));
        }

        // cпортивные разряды и звания
        List<SportRank> sportRankList = getList(SportRank.class);

        for (SportRank sportRank : sportRankList)
        {
            data.sportRank.catalogNodes.add(new CatalogsData.CatalogNode(sportRank.getShortTitle(), sportRank.getTitle(), sportRank.getCode()));
        }

        // иностранные языки;
        List<ForeignLanguage> foreignLanguageList = getList(ForeignLanguage.class);

        for (ForeignLanguage foreignLanguage : foreignLanguageList)
        {
            data.foreignLanguage.catalogNodes.add(new CatalogsData.CatalogNode(foreignLanguage.getShortTitle(), foreignLanguage.getTitle(), foreignLanguage.getCode()));
        }

        // модуль unienr14

        // Источники информации об ОУ
        List<EnrSourceInfoAboutUniversity> enrSourceInfoAboutUniversityList = getList(EnrSourceInfoAboutUniversity.class);

        for (EnrSourceInfoAboutUniversity enrSourceInfoAboutUniversity : enrSourceInfoAboutUniversityList)
        {
            data.enrSourceInfoAboutUniversityNode.catalogNodes.add(new CatalogsData.CatalogNode(enrSourceInfoAboutUniversity.getTitle(), enrSourceInfoAboutUniversity.getCode()));
        }

        // подготовительные курсы
        List<EnrAccessCourse> enrAccessCourseList = getList(EnrAccessCourse.class);

        for (EnrAccessCourse enrAccessCourse : enrAccessCourseList)
        {
            data.enrAccessCourseNode.catalogNodes.add(new CatalogsData.CatalogNode(enrAccessCourse.getShortTitle(), enrAccessCourse.getTitle(), enrAccessCourse.getCode()));
        }

        // подготовительные отделения
        List<EnrAccessDepartment> enrAccessDepartmentList = getList(EnrAccessDepartment.class);

        for (EnrAccessDepartment enrAccessDepartment : enrAccessDepartmentList)
        {
            data.enrAccessDepartmentNode.catalogNodes.add(new CatalogsData.CatalogNode(enrAccessDepartment.getShortTitle(), enrAccessDepartment.getTitle(), enrAccessDepartment.getCode()));
        }

        // Степени отличия по окончании образовательного учреждения
        List<GraduationHonour> graduationHonourList = getList(GraduationHonour.class);
        for (GraduationHonour graduationHonour : graduationHonourList)
        {
            CatalogsData.GraduationHonourNode.GraduationHonourRow row = new CatalogsData.GraduationHonourNode.GraduationHonourRow();
            row.id = graduationHonour.getCode();
            row.title = graduationHonour.getTitle();
            row.userCode = graduationHonour.getUserCode();
            row.disabledDate = graduationHonour.getDisabledDate();
            data.graduationHonourNode.catalogNodes.add(row);
        }

        // Степени отличия для вида док. об образ.
        List<GraduationHonour2EduDocumentKind> graduationHonour2EduDocumentKindList = getList(GraduationHonour2EduDocumentKind.class);
        for (GraduationHonour2EduDocumentKind graduationHonour2EduDocumentKind : graduationHonour2EduDocumentKindList)
        {
            CatalogsData.GraduationHonour2EduDocumentKindNode.GraduationHonour2EduDocumentKindRow row = new CatalogsData.GraduationHonour2EduDocumentKindNode.GraduationHonour2EduDocumentKindRow();
            row.graduationHonour = graduationHonour2EduDocumentKind.getGraduationHonour().getCode();
            row.documentKind = graduationHonour2EduDocumentKind.getDocumentKind().getCode();
            data.graduationHonour2EduDocumentKindNode.catalogNodes.add(row);
        }

        // Типы удостоверения личности
        List<IdentityCardType> identityCardTypeList = getList(IdentityCardType.class);

        for (IdentityCardType identityCardType : identityCardTypeList)
        {
            if(!IdentityCardTypeCodes.BEZ_UDOSTOVERENIYA.equals(identityCardType.getCode()))
            {
                CatalogsData.IdentityCardTypeNode.IdentityCardTypeRow identityCardTypeRow = new CatalogsData.IdentityCardTypeNode.IdentityCardTypeRow();
                identityCardTypeRow.id = identityCardType.getCode();
                identityCardTypeRow.shortTitle = identityCardType.getShortTitle();
                identityCardTypeRow.showSeria = identityCardType.isShowSeria();
                identityCardTypeRow.seriaRequired = identityCardType.isSeriaRequired();
                identityCardTypeRow.numberRequired = identityCardType.isNumberRequired();
                identityCardTypeRow.minSeriaLength = identityCardType.getMinSeriaLength();
                identityCardTypeRow.maxSeriaLength = identityCardType.getMaxSeriaLength();
                identityCardTypeRow.minNumberLength = identityCardType.getMinNumberLength();
                identityCardTypeRow.maxNumberLength = identityCardType.getMaxNumberLength();
                identityCardTypeRow.seriaOnlyDigits = identityCardType.isSeriaOnlyDigits();
                identityCardTypeRow.numberOnlyDigits = identityCardType.isNumberOnlyDigits();
                identityCardTypeRow.onlyCyrillicInFio = identityCardType.isOnlyCyrillicInFio();
                identityCardTypeRow.seriaMask = identityCardType.getSeriaMask();
                identityCardTypeRow.numberMask = identityCardType.getNumberMask();
                identityCardTypeRow.citizenshipDefault = identityCardType.getCitizenshipDefault() != null ? identityCardType.getCitizenshipDefault().getCode() : null;
                identityCardTypeRow.title = identityCardType.getTitle();
                identityCardTypeRow.userCode = identityCardType.getUserCode();
                identityCardTypeRow.disabledDate = identityCardType.getDisabledDate();
                // new
                identityCardTypeRow.numberEnabled = identityCardType.isNumberEnabled();
                identityCardTypeRow.issuancePlaceEnabled = identityCardType.isIssuancePlaceEnabled();
                identityCardTypeRow.issuancePlaceRequired = identityCardType.isIssuancePlaceRequired();
                identityCardTypeRow.issuanceCodeEnabled = identityCardType.isIssuanceCodeEnabled();
                identityCardTypeRow.issuanceCodeRequired = identityCardType.isIssuanceCodeRequired();
                identityCardTypeRow.issuanceDateEnabled = identityCardType.isIssuanceDateEnabled();
                identityCardTypeRow.issuanceDateRequired = identityCardType.isIssuanceDateRequired();
                identityCardTypeRow.birthDateEnabled = identityCardType.isBirthDateEnabled();
                identityCardTypeRow.birthDateRequired = identityCardType.isBirthDateRequired();
                identityCardTypeRow.birthPlaceEnabled = identityCardType.isBirthPlaceEnabled();
                identityCardTypeRow.birthPlaceRequired = identityCardType.isBirthPlaceRequired();


                data.identityCardTypeNode.catalogNodes.add(identityCardTypeRow);
            }
        }


        // Виды документов об образовании
        List<EduDocumentKind> eduDocumentKindList = getList(EduDocumentKind.class);

        for (EduDocumentKind eduDocumentKind : eduDocumentKindList)
        {
            CatalogsData.EduDocumentKindNode.EduDocumentKindRow eduDocumentKindRow = new CatalogsData.EduDocumentKindNode.EduDocumentKindRow();
            eduDocumentKindRow.id = eduDocumentKind.getCode();
            eduDocumentKindRow.shortTitle = eduDocumentKind.getShortTitle();
            eduDocumentKindRow.description = eduDocumentKind.getDescription();
            eduDocumentKindRow.certifyEducationLevel = eduDocumentKind.isCertifyEducationLevel();
            eduDocumentKindRow.certifyQualification = eduDocumentKind.isCertifyQualification();
            if (eduDocumentKind.getFixedEducationLevel() != null)
                eduDocumentKindRow.fixedEducationLevel = eduDocumentKind.getFixedEducationLevel().getCode();
            eduDocumentKindRow.defaultQualification = eduDocumentKind.getDefaultQualification();
            eduDocumentKindRow.seriaAllowed = eduDocumentKind.isSeriaAllowed();
            eduDocumentKindRow.seriaRequired = eduDocumentKind.isSeriaRequired();
            eduDocumentKindRow.numberOnlyDigits = eduDocumentKind.isNumberOnlyDigits();
            eduDocumentKindRow.eduProgramSubjectAllowed = eduDocumentKind.isEduProgramSubjectAllowed();
            eduDocumentKindRow.eduProgramSubjectRequired = eduDocumentKind.isEduProgramSubjectRequired();
            eduDocumentKindRow.avgMarkAllowed = eduDocumentKind.isAvgMarkAllowed();
            eduDocumentKindRow.avgMarkRequired = eduDocumentKind.isAvgMarkRequired();
            eduDocumentKindRow.ruOnly = eduDocumentKind.isRuOnly();
            eduDocumentKindRow.title = eduDocumentKind.getTitle();
            eduDocumentKindRow.userCode = eduDocumentKind.getUserCode();
            eduDocumentKindRow.disabledDate = eduDocumentKind.getDisabledDate();

            data.eduDocumentKindNode.catalogNodes.add(eduDocumentKindRow);
        }

//        вебсервис по персональным данным студента:
//
        // индивидуальные достижения
        List<EnrEntrantAchievementType> enrEntrantAchievementTypeList = new DQLSelectBuilder().fromEntity(EnrEntrantAchievementType.class, "at").where(eq(property("at", EnrEntrantAchievementType.enrollmentCampaign().title()), value(enrollmentCampaignTitle))).createStatement(getSession()).list();
        for (EnrEntrantAchievementType enrEntrantAchievementType : enrEntrantAchievementTypeList)
        {
            CatalogsData.EnrEntrantAchievementTypeNode.EnrEntrantAchievementTypeRow row = new CatalogsData.EnrEntrantAchievementTypeNode.EnrEntrantAchievementTypeRow();
            row.id = enrEntrantAchievementType.getId().toString();
            row.title = enrEntrantAchievementType.getAchievementKind().getTitle();
            row.description = enrEntrantAchievementType.getAchievementKind().getDescription();
            row.achievementMarkAsLong = enrEntrantAchievementType.getAchievementMarkAsLong();
            row.requestType = enrEntrantAchievementType.getAchievementKind().getRequestType().getCode();

            data.enrEntrantAchievementTypeNode.enrEntrantAchievementTypes.add(row);
        }

        return data;
    }

    @Override
    public void createOrUpdateOnlineEntrant(OnlineEntrantData onlineEntrant)
    {
        validateOnlineEntrant(onlineEntrant);

        Session session = getSession();
        NamedSyncInTransactionCheckLocker.register(session.getTransaction(), "unienr14.ws.onlineEntrant" + onlineEntrant.onlineEntrantId, 300);

        EnrEnrollmentCampaign enrollmentCampaign = get(EnrEnrollmentCampaign.class, EnrEnrollmentCampaign.title(), onlineEntrant.enrollmentCampaign);

        // предметы ЕГЭ
        List<EnrStateExamSubject> examSubjectList = getList(EnrStateExamSubject.class);
        Map<String, EnrStateExamSubject> examSubjectMap = Maps.newHashMap();
        for (EnrStateExamSubject stateExamSubject : examSubjectList)
        {
            examSubjectMap.put(stateExamSubject.getCode(), stateExamSubject);
        }

        // семейный статус
        List<FamilyStatus> familyStatusList = getList(FamilyStatus.class);
        Map<String, FamilyStatus> familyStatusMap = Maps.newHashMap();
        for (FamilyStatus familyStatus : familyStatusList)
        {
            familyStatusMap.put(familyStatus.getCode(), familyStatus);
        }

        // иностранный язык
        List<ForeignLanguage> foreignLanguageList = getList(ForeignLanguage.class);
        Map<String, ForeignLanguage> foreignLanguageMap = Maps.newHashMap();
        for (ForeignLanguage foreignLanguage : foreignLanguageList)
        {
            foreignLanguageMap.put(foreignLanguage.getCode(), foreignLanguage);
        }

        // пол
        List<Sex> sexList = getList(Sex.class);
        Map<String, Sex> sexMap = Maps.newHashMap();
        for (Sex sex : sexList)
        {
            sexMap.put(sex.getCode(), sex);
        }

        // страна
        List<AddressCountry> addressCountryList = getList(AddressCountry.class);
        Map<Integer, AddressCountry> addressCountryMap = Maps.newHashMap();
        for (AddressCountry addressCountry : addressCountryList)
        {
            addressCountryMap.put(addressCountry.getCode(), addressCountry);
        }

        // гражданство
        List<ICitizenship> citizenshipList = getList(ICitizenship.class);
        Map<Integer, ICitizenship> citizenshipMap = Maps.newHashMap();
        for (ICitizenship citizenship : citizenshipList)
        {
            citizenshipMap.put(citizenship.getCode(), citizenship);
        }

        // тип удостоверения личности
        List<IdentityCardType> identityCardTypeList = getList(IdentityCardType.class);
        Map<String, IdentityCardType> identityCardTypeMap = Maps.newHashMap();
        for (IdentityCardType identityCardType : identityCardTypeList)
        {
            identityCardTypeMap.put(identityCardType.getCode(), identityCardType);
        }

        // Индивидуальные достижения (коды university)
        List<EnrEntrantAchievementType> achievementTypeList = getList(EnrEntrantAchievementType.class, EnrEntrantAchievementType.enrollmentCampaign().id(), enrollmentCampaign.getId());
        Map<String, EnrEntrantAchievementType> achievementTypeMap = Maps.newHashMap();
        for (EnrEntrantAchievementType achievementType : achievementTypeList)
        {
            achievementTypeMap.put(achievementType.getId().toString(), achievementType);
        }

        // Подготовительные курсы (коды university)
        List<EnrAccessCourse> accessCourseList = getList(EnrAccessCourse.class);
        Map<String, EnrAccessCourse> accessCourseMap = Maps.newHashMap();
        for (EnrAccessCourse accessCourse : accessCourseList)
        {
            accessCourseMap.put(accessCourse.getCode(), accessCourse);
        }

        // Подготовительные отделения (коды university)
        List<EnrAccessDepartment> accessDepartmentList = getList(EnrAccessDepartment.class);
        Map<String, EnrAccessDepartment> accessDepartmentMap = Maps.newHashMap();
        for (EnrAccessDepartment accessDepartment : accessDepartmentList)
        {
            accessDepartmentMap.put(accessDepartment.getCode(), accessDepartment);
        }

        // Источники информации об ОУ (коды university)
        List<EnrSourceInfoAboutUniversity> sourceInfoAboutUniversityList = getList(EnrSourceInfoAboutUniversity.class);
        Map<String, EnrSourceInfoAboutUniversity> sourceInfoAboutUniversityMap = Maps.newHashMap();
        for (EnrSourceInfoAboutUniversity sourceInfoAboutUniversity : sourceInfoAboutUniversityList)
        {
            sourceInfoAboutUniversityMap.put(sourceInfoAboutUniversity.getCode(), sourceInfoAboutUniversity);
        }

        // Вид спорта (код university)
        List<SportType> sportTypeList = getList(SportType.class);
        Map<String, SportType> sportTypeMap = Maps.newHashMap();
        for (SportType sportType : sportTypeList)
        {
            sportTypeMap.put(sportType.getCode(), sportType);
        }

        // Спортивные разряды и звания (код university)
        List<SportRank> sportRankList = getList(SportRank.class);
        Map<String, SportRank> sportRankMap = Maps.newHashMap();
        for (SportRank sportRank : sportRankList)
        {
            sportRankMap.put(sportRank.getCode(), sportRank);
        }

        // Родственник (код university)
        List<RelationDegree> relationDegreeList = getList(RelationDegree.class);
        Map<String, RelationDegree> relationDegreeMap = Maps.newHashMap();
        for (RelationDegree relationDegree : relationDegreeList)
        {
            relationDegreeMap.put(relationDegree.getCode(), relationDegree);
        }

        // Вид документа (код university)
        List<EduDocumentKind> eduDocumentKindList = getList(EduDocumentKind.class);
        Map<String, EduDocumentKind> eduDocumentKindMap = Maps.newHashMap();
        for (EduDocumentKind eduDocumentKind : eduDocumentKindList)
        {
            eduDocumentKindMap.put(eduDocumentKind.getCode(), eduDocumentKind);
        }

        // Актуальный уровень образования (код university)
        List<EduLevel> eduLevelList = getList(EduLevel.class);
        Map<String, EduLevel> eduLevelMap = Maps.newHashMap();
        for (EduLevel eduLevel : eduLevelList)
        {
            eduLevelMap.put(eduLevel.getCode(), eduLevel);
        }

        // Степень отличия по окончании образовательного учреждения (код university)
        List<GraduationHonour> graduationHonourList = getList(GraduationHonour.class);
        Map<String, GraduationHonour> graduationHonourMap = Maps.newHashMap();
        for (GraduationHonour graduationHonour : graduationHonourList)
        {
            graduationHonourMap.put(graduationHonour.getCode(), graduationHonour);
        }

        EnrOnlineEntrant enrOnlineEntrant = get(EnrOnlineEntrant.class, EnrOnlineEntrant.onlineEntrant(), onlineEntrant.onlineEntrantId);
        if (enrOnlineEntrant == null)
        {
            enrOnlineEntrant = new EnrOnlineEntrant();
            enrOnlineEntrant.setRegDate(new Date());
            enrOnlineEntrant.setEnrEnrollmentCampaign(enrollmentCampaign); // приемная кампания
            enrOnlineEntrant.setOnlineEntrant(onlineEntrant.onlineEntrantId); // id абитуриента в Tandem Econline
            enrOnlineEntrant.setPersonalNumber(getMaxEntrantPersonalNumber(enrollmentCampaign) + 1); // Регистрационный номер
        }

        // персональные данные
        enrOnlineEntrant.setRegAddress(onlineEntrant.regAddress); // Адрес регистрации
        enrOnlineEntrant.setSameAddress(onlineEntrant.sameAddress); // Адрес регистрации и фактический адрес совпадают
        if (!onlineEntrant.sameAddress)
            enrOnlineEntrant.setFactAddress(onlineEntrant.factAddress); // Фактический адрес

        enrOnlineEntrant.setNeedDorm(onlineEntrant.needDorm); // Нуждается в общежитии
        enrOnlineEntrant.setContactPhone(onlineEntrant.contactPhone); // Контактный телефон
        enrOnlineEntrant.setMobilePhone(onlineEntrant.mobilePhone); // Мобильный телефон
        enrOnlineEntrant.setWorkPhone(onlineEntrant.workPhone); // Рабочий телефон
        enrOnlineEntrant.setEmail(onlineEntrant.email); // E-mail
        enrOnlineEntrant.setChilds(onlineEntrant.childs); // Количество детей
        enrOnlineEntrant.setCurrentWork(onlineEntrant.currentWork); // Место работы в настоящее время
        enrOnlineEntrant.setCurrentPost(onlineEntrant.currentPost); // Должность
        enrOnlineEntrant.setArmyStartDate(onlineEntrant.armyStartDate); // Дата начала службы
        enrOnlineEntrant.setArmyEndDate(onlineEntrant.armyEndDate); // Дата окончания службы
        enrOnlineEntrant.setArmyEndYear(onlineEntrant.armyEndYear); // Год увольнения в запас

        if (familyStatusMap.get(onlineEntrant.maritalStatus) != null)
            enrOnlineEntrant.setFamilyStatus(familyStatusMap.get(onlineEntrant.maritalStatus)); // Семейное положение

        if (foreignLanguageMap.get(onlineEntrant.foreignLanguage) != null)
            enrOnlineEntrant.setForeignLanguage(foreignLanguageMap.get(onlineEntrant.foreignLanguage)); // Иностранный язык

        // Удостоверение личности
        IdentityCard identityCard = enrOnlineEntrant.getIdentityCard() != null ? enrOnlineEntrant.getIdentityCard() : new IdentityCard();
        identityCard.setFirstName(onlineEntrant.firstName); // Имя
        identityCard.setLastName(onlineEntrant.lastName); // Фамилия
        identityCard.setMiddleName(onlineEntrant.middleName); // Отчество

        identityCard.setSeria(onlineEntrant.seria); // Серия
        identityCard.setNumber(onlineEntrant.number); // Номер
        identityCard.setBirthDate(onlineEntrant.birthDate); // Дата рождения
        identityCard.setBirthPlace(onlineEntrant.birthPlace); // Место рождения
        identityCard.setIssuanceDate(onlineEntrant.issuanceDate); // Дата выдачи удостоверения
        identityCard.setIssuancePlace(onlineEntrant.issuancePlace); // Кем выдано удостоверение
        identityCard.setIssuanceCode(onlineEntrant.issuanceCode); // Код подразделения
        AddressString idCardAddress = identityCard.getAddress() != null ? (AddressString) identityCard.getAddress() : new AddressString();
        idCardAddress.setAddress(onlineEntrant.regAddress);
        AddressBaseManager.instance().dao().saveOrUpdateAddressWithoutEntityUpdate(identityCard, AddressBaseUtils.getSameAddress(idCardAddress), IdentityCard.address().s());
        identityCard.setSex(sexMap.get(onlineEntrant.sex)); // Пол (код university)
        Integer citizenshipCode;
        try
        {
            citizenshipCode = Integer.parseInt(onlineEntrant.citizenship);
        }
        catch (NumberFormatException e)
        {
            throw new RuntimeException("Неверное поле onlineEntrant.citizenship"); // TODO: убрать когда будет сделана валидация
        }

        identityCard.setCitizenship(citizenshipMap.get(citizenshipCode)); // Страна (код university)
        identityCard.setCardType(identityCardTypeMap.get(onlineEntrant.cardType)); // Тип удостоверения личности (код university)

        if (onlineEntrant.idCardScanCopy != null) // Скан-копия документа удостоверения личности
        {
            if (enrOnlineEntrant.getIdCardScanCopy() != null)
            {
                DatabaseFile file = enrOnlineEntrant.getIdCardScanCopy();
                session.delete(file);
            }
            DatabaseFile file = new DatabaseFile();
            file.setFilename(onlineEntrant.idCardScanCopyName);
            file.setContentType(onlineEntrant.idCardScanCopyContentType);
            file.setContent(onlineEntrant.idCardScanCopy);
            session.save(file);
            enrOnlineEntrant.setIdCardScanCopy(file);
        }

        if (onlineEntrant.indAchievScanCopy != null) // Скан-копии документов, подтверждающих ИД
        {
            if (enrOnlineEntrant.getIndAchievScanCopy() != null)
            {
                DatabaseFile file = enrOnlineEntrant.getIndAchievScanCopy();
                session.delete(file);
            }
            DatabaseFile file = new DatabaseFile();
            file.setFilename(onlineEntrant.indAchievScanCopyName);
            file.setContentType(onlineEntrant.indAchievScanCopyContentType);
            file.setContent(onlineEntrant.indAchievScanCopy);
            session.save(file);
            enrOnlineEntrant.setIndAchievScanCopy(file);
        }


        if (onlineEntrant.otherDocScanCopy != null) // Скан-копия иных документов
        {
            if (enrOnlineEntrant.getOtherDocScanCopy() != null)
            {
                DatabaseFile file = enrOnlineEntrant.getOtherDocScanCopy();
                session.delete(file);
            }
            DatabaseFile file = new DatabaseFile();
            file.setFilename(onlineEntrant.otherDocScanCopyName);
            file.setContentType(onlineEntrant.otherDocScanCopyContentType);
            file.setContent(onlineEntrant.otherDocScanCopy);
            session.save(file);
            enrOnlineEntrant.setOtherDocScanCopy(file);
        }

        session.saveOrUpdate(identityCard);

        enrOnlineEntrant.setIdentityCard(identityCard);

        session.saveOrUpdate(enrOnlineEntrant);




        new DQLDeleteBuilder(EnrOnlineEntrantAchievement.class).where(eq(property(EnrOnlineEntrantAchievement.onlineEntrant().id()), value(enrOnlineEntrant.getId()))).createStatement(session).execute();

        for (String individualAchievementCode : onlineEntrant.individualAchievements) // Индивидуальные достижения (коды university)
        {
            if (achievementTypeMap.get(individualAchievementCode) != null)
            {
                EnrOnlineEntrantAchievement entrantAchievement = new EnrOnlineEntrantAchievement();
                entrantAchievement.setOnlineEntrant(enrOnlineEntrant);
                entrantAchievement.setAchievementType(achievementTypeMap.get(individualAchievementCode));
                session.save(entrantAchievement);
            }
        }

        new DQLDeleteBuilder(EnrOnlineEntrantAccessCourse.class).where(eq(property(EnrOnlineEntrantAccessCourse.onlineEntrant().id()), value(enrOnlineEntrant.getId()))).createStatement(session).execute();
        for (String trainingCourseCode : onlineEntrant.trainingCourses) // Подготовительные курсы (коды university)
        {
            if (accessCourseMap.get(trainingCourseCode) != null)
            {
                EnrOnlineEntrantAccessCourse accessCourse = new EnrOnlineEntrantAccessCourse();
                accessCourse.setOnlineEntrant(enrOnlineEntrant);
                accessCourse.setCourse(accessCourseMap.get(trainingCourseCode));
                session.save(accessCourse);
            }
        }

        new DQLDeleteBuilder(EnrOnlineEntrantAccessDepartment.class).where(eq(property(EnrOnlineEntrantAccessDepartment.onlineEntrant().id()), value(enrOnlineEntrant.getId()))).createStatement(session).execute();
        for (String trainingDepartmentCode : onlineEntrant.trainingDepartments) // Подготовительные отделения (коды university)
        {
            if (accessDepartmentMap.get(trainingDepartmentCode) != null)
            {
                EnrOnlineEntrantAccessDepartment accessDepartment = new EnrOnlineEntrantAccessDepartment();
                accessDepartment.setOnlineEntrant(enrOnlineEntrant);
                accessDepartment.setDepartment(accessDepartmentMap.get(trainingDepartmentCode));
                session.save(accessDepartment);
            }
        }

        new DQLDeleteBuilder(EnrOnlineEntrantSourceInfoAboutOU.class).where(eq(property(EnrOnlineEntrantSourceInfoAboutOU.onlineEntrant().id()), value(enrOnlineEntrant.getId()))).createStatement(session).execute();
        for (String sourceInfoAboutOuCode : onlineEntrant.sourceInfoAboutOUList) // Источники информации об ОУ (коды university)
        {
            if (sourceInfoAboutUniversityMap.get(sourceInfoAboutOuCode) != null)
            {
                EnrOnlineEntrantSourceInfoAboutOU infoAboutOU = new EnrOnlineEntrantSourceInfoAboutOU();
                infoAboutOU.setOnlineEntrant(enrOnlineEntrant);
                infoAboutOU.setSourceInfo(sourceInfoAboutUniversityMap.get(sourceInfoAboutOuCode));
                session.save(infoAboutOU);
            }
        }

        new DQLDeleteBuilder(EnrOnlineEntrantSportAchievement.class).where(eq(property(EnrOnlineEntrantSportAchievement.onlineEntrant().id()), value(enrOnlineEntrant.getId()))).createStatement(session).execute();
        for (OnlineEntrantData.PersonSportAchievementData sportAchievementData : onlineEntrant.sportAchievementes) // Спортивные достижения
        {
            if (sportTypeMap.get(sportAchievementData.sportType) != null && sportRankMap.get(sportAchievementData.sportRank) != null)
            {
                EnrOnlineEntrantSportAchievement sportAchievement = new EnrOnlineEntrantSportAchievement();
                sportAchievement.setOnlineEntrant(enrOnlineEntrant);
                sportAchievement.setType(sportTypeMap.get(sportAchievementData.sportType));
                sportAchievement.setRank(sportRankMap.get(sportAchievementData.sportRank));
                session.save(sportAchievement);
            }
        }

        // Ближайший родственник
        EnrOnlineEntrantNextOfKin nextOfKin = get(EnrOnlineEntrantNextOfKin.class, EnrOnlineEntrantNextOfKin.onlineEntrant().id(), enrOnlineEntrant.getId());
        if (nextOfKin == null)
            nextOfKin = new EnrOnlineEntrantNextOfKin();

        if (relationDegreeMap.get(onlineEntrant.relationDegree) != null)
        {
            nextOfKin.setOnlineEntrant(enrOnlineEntrant);
            nextOfKin.setRelationDegree(relationDegreeMap.get(onlineEntrant.relationDegree)); // Родственник (код university)
            nextOfKin.setLastname(onlineEntrant.lastNameNextOfKin);  // Фамилия
            nextOfKin.setFirstname(onlineEntrant.firstNameNextOfKin); // Имя
            nextOfKin.setPatronymic(onlineEntrant.patronymicNextOfKin); // Отчество
            nextOfKin.setEmploymentPlace(onlineEntrant.employmentPlace); // Место работы
            nextOfKin.setPost(onlineEntrant.post); // Должность
            nextOfKin.setPhones(onlineEntrant.phones); // Контактные телефоны
            session.saveOrUpdate(nextOfKin);
        }
        else
        {
            if (nextOfKin.getId() != null)
                session.delete(nextOfKin);
        }

        // Документ об образовании
        EnrOnlineEntrantEduDocument eduDocument = get(EnrOnlineEntrantEduDocument.class, EnrOnlineEntrantEduDocument.onlineEntrant().id(), enrOnlineEntrant.getId());
        if (eduDocument == null)
            eduDocument = new EnrOnlineEntrantEduDocument();

        if (eduDocumentKindMap.get(onlineEntrant.eduDocumentKind) != null)
        {
            eduDocument.setOnlineEntrant(enrOnlineEntrant);
            eduDocument.setDocKind(eduDocumentKindMap.get(onlineEntrant.eduDocumentKind)); // Вид документа (код university)
            eduDocument.setDocumentKindDetailed(onlineEntrant.documentKindDetailed); // Вид документа (по документу, для иных документов)
            eduDocument.setSeria(onlineEntrant.seriaEduDocument); // Серия
            eduDocument.setNumber(onlineEntrant.numberEduDocument); // Номер

            Integer eduOrganizationCountryCode;
            try
            {
                eduOrganizationCountryCode = Integer.parseInt(onlineEntrant.eduOrganizationCountry);
            }
            catch (NumberFormatException e)
            {
                throw new RuntimeException("Неверное поле onlineEntrant.eduOrganizationCountry");  // TODO: убрать когда будет сделана валидация
            }

            eduDocument.setEduOrgCountry(addressCountryMap.get(eduOrganizationCountryCode)); // Страна образовательной организации  (код university)
            eduDocument.setEduOrganizationSettlement(onlineEntrant.eduOrganizationSettlement); // Населенный пункт образовательной организации
            eduDocument.setEduOrganization(onlineEntrant.eduOrganization); // Образовательная организация
            eduDocument.setYearEnd(onlineEntrant.yearEnd); // Год окончания обучения
            eduDocument.setRuRegionCode(onlineEntrant.ruRegionCode); // Код субъекта РФ
            eduDocument.setIssuanceDate(onlineEntrant.issuanceDateEduDocument); // Дата выдачи
            eduDocument.setEduLevel(eduLevelMap.get(onlineEntrant.eduLevel)); // Актуальный уровень образования (код university)
            eduDocument.setDocumentEducationLevel(onlineEntrant.documentEducationLevel); // Уровень образования по документу
            eduDocument.setQualification(onlineEntrant.qualification); // Квалификация
            eduDocument.setEduProgramSubject(onlineEntrant.eduProgramSubject); // Направление (профессия, специальность)
            eduDocument.setHonour(graduationHonourMap.get(onlineEntrant.graduationHonour)); // Степень отличия по окончании образовательного учреждения (код university)
            eduDocument.setAvgMarkAsLong(onlineEntrant.avgMarkAsLong); // Средний балл
            eduDocument.setMark5(onlineEntrant.mark5); // Кол-во оценок «Отлично»
            eduDocument.setMark4(onlineEntrant.mark4); // Кол-во оценок «Хорошо»
            eduDocument.setMark3(onlineEntrant.mark3); // Кол-во оценок «Удовлетворительно»
            eduDocument.setRegistrationNumber(onlineEntrant.registrationNumber); // Регистрационный номер
            if (onlineEntrant.scanCopy != null) // Скан-копия документа об образовании
            {
                if (eduDocument.getScanCopy() != null)
                {
                    DatabaseFile file = eduDocument.getScanCopy();
                    session.delete(file);
                }
                DatabaseFile file = new DatabaseFile();
                file.setFilename(onlineEntrant.scanCopyName);
                file.setContentType(onlineEntrant.scanCopyContentType);
                file.setContent(onlineEntrant.scanCopy);
                session.save(file);
                eduDocument.setScanCopy(file);
            }

            session.saveOrUpdate(eduDocument);
        }

        // данные заявления
        EnrOnlineEntrantStatement statement = get(EnrOnlineEntrantStatement.class, EnrOnlineEntrantStatement.onlineEntrant().id(), enrOnlineEntrant.getId());
        if (statement == null)
            statement = new EnrOnlineEntrantStatement();

        statement.setOnlineEntrant(enrOnlineEntrant);
        statement.setHaveEduLevel(onlineEntrant.haveEduLevel);// Полученное образование
        statement.setWantRequestType(onlineEntrant.wantRequestType); // Программы
        statement.setExclusiveRights(onlineEntrant.exclusiveRights);// У меня есть особые права

        if (onlineEntrant.exclusiveRights)
        {
            statement.setExclusiveRightsWithoutExams(onlineEntrant.exclusiveRightsWithoutExams); // Право на прием без вступительных испытаний
            statement.setExclusiveRightsBudget(onlineEntrant.exclusiveRightsBudget); // Право на прием на обучение за счет бюджетных ассигнований в пределах установленной квоты
            statement.setExclusiveRightsEmptiveEnroll(onlineEntrant.exclusiveRightsEmptiveEnroll); // Преимущественное право зачисления
        }
        else
        {
            statement.setExclusiveRightsWithoutExams(false); // Право на прием без вступительных испытаний
            statement.setExclusiveRightsBudget(false); // Право на прием на обучение за счет бюджетных ассигнований в пределах установленной квоты
            statement.setExclusiveRightsEmptiveEnroll(false); // Преимущественное право зачисления
        }

        statement.setHaveAgreementTargetEdu(onlineEntrant.haveAgreementTargetEdu); // Заключен договор о целевом обучении
        if (onlineEntrant.haveAgreementTargetEdu)
        {
            statement.setAgreementNumber(onlineEntrant.agreementNumber); // Номер договора
            statement.setAgreementDate(onlineEntrant.agreementDate); // Дата заключения
            statement.setAgreementOrg(onlineEntrant.agreementOrg); // Данные об организации
            statement.setAgreementEduProgramm(onlineEntrant.agreementEduProgramm); // Данные об образовательной программе
        }
        else
        {
            statement.setAgreementNumber(null); // Номер договора
            statement.setAgreementDate(null); // Дата заключения
            statement.setAgreementOrg(null); // Данные об организации
            statement.setAgreementEduProgramm(null); // Данные об образовательной программе
        }

        new DQLDeleteBuilder(EnrOnlineEntrantStateExam.class).where(eq(property(EnrOnlineEntrantStateExam.onlineEntrant().id()), value(enrOnlineEntrant.getId()))).createStatement(session).execute();

        if (onlineEntrant.stateExamCodes != null && !onlineEntrant.stateExamCodes.isEmpty()) //сданные предметы ЕГЭ
        {
            for (String stateExamCode : onlineEntrant.stateExamCodes)
            {
                if (examSubjectMap.get(stateExamCode) != null)
                {
                    EnrOnlineEntrantStateExam stateExam = new EnrOnlineEntrantStateExam();
                    stateExam.setOnlineEntrant(enrOnlineEntrant);
                    stateExam.setStateExamSubject(examSubjectMap.get(stateExamCode));
                    session.save(stateExam);
                }
            }
        }
        session.saveOrUpdate(statement);

        new DQLDeleteBuilder(EnrOnlineEntrantCompetition.class).where(eq(property(EnrOnlineEntrantCompetition.statement().id()), value(statement.getId()))).createStatement(session).execute();
        for (OnlineEntrantData.EnrCompetitionData competitionData : onlineEntrant.competitions) // Выбранные конкурсы
        {
            EnrCompetition competition = get(Long.parseLong(competitionData.competition));
            if (competition != null)
            {
                EnrOnlineEntrantCompetition onlineEntrantCompetition = new EnrOnlineEntrantCompetition();
                onlineEntrantCompetition.setStatement(statement);
                onlineEntrantCompetition.setCompetition(competition);
                onlineEntrantCompetition.setPriority(competitionData.priority);
                session.save(onlineEntrantCompetition);
            }
        }
    }

    private void validateOnlineEntrant(OnlineEntrantData onlineEntrant)
    {
        // если онлайн-абитуриент оформлен как абитуриент
        if (isOnlineEntrantStatementAccepted(onlineEntrant.onlineEntrantId))
            throw new ClientSoapFaultException(ClientSoapFaultException.TANDEM_UNIVERSITY_ERROR_CODE_OTHER ,"У абитуриента принято заявление, редактирование данных невозможно.");

        EnrEnrollmentCampaign enrollmentCampaign = get(EnrEnrollmentCampaign.class, EnrEnrollmentCampaign.title(), onlineEntrant.enrollmentCampaign);
        if (enrollmentCampaign == null) throw new ClientSoapFaultException(ClientSoapFaultException.TANDEM_UNIVERSITY_ERROR_CODE_EXIST_DATA, "Приемная кампания «" + onlineEntrant.enrollmentCampaign + "» отсутствует в системе Tandem University");

//        validateRequiredFields(onlineEntrant);
//        validateExistData(onlineEntrant);
//        validateLogic(onlineEntrant);
    }

    private void validateRequiredFields(OnlineEntrantData onlineEntrant)
    {
        // TODO:!!!
    }

    private void validateExistData(OnlineEntrantData onlineEntrant)
    {
        List<String> errors = Lists.newArrayList();

        Sex sex = get(Sex.class, Sex.code(), onlineEntrant.sex);
        if(sex == null) errors.add("Элемент справочника «Пол» с кодом «" + onlineEntrant.sex + "» отсутствует в системе Tandem University (поле OnlineEntrantData.sex)");

        Integer countryCode;
        try
        {
            countryCode = Integer.parseInt(onlineEntrant.citizenship);
        }
        catch (NumberFormatException e)
        {
            countryCode = null;
        }

        AddressCountry country = countryCode != null ? get(AddressCountry.class, AddressCountry.code(), countryCode) : null;

        if (country == null)
            errors.add("Элемент «Страна» с кодом «" + onlineEntrant.citizenship + "» отсутствует в системе Tandem University (поле OnlineEntrantData.citizenship)");


        IdentityCardType cardType = get(IdentityCardType.class, IdentityCardType.code(), onlineEntrant.cardType);
        if(cardType == null) errors.add("Элемент справочника «Тип удостоверения личности» с кодом «" + onlineEntrant.cardType + "» отсутствует в системе Tandem University (поле OnlineEntrantData.cardType)");

        if(!StringUtils.isEmpty(onlineEntrant.haveEduLevel))
        {
            EduLevel eduLevel = get(EduLevel.class, EduLevel.code(), onlineEntrant.haveEduLevel);
            if (eduLevel == null)
                errors.add("Элемент справочника «(Новый) Виды и уровни образования» с кодом «" + onlineEntrant.haveEduLevel + "» отсутствует в системе Tandem University (поле OnlineEntrantData.haveEduLevel)");
        }

        if(!StringUtils.isEmpty(onlineEntrant.wantRequestType))
        {
            EnrRequestType requestType = get(EnrRequestType.class, EnrRequestType.code(), onlineEntrant.wantRequestType);
            if (requestType == null)
                errors.add("Элемент справочника «Виды заявлений» с кодом «" + onlineEntrant.wantRequestType + "» отсутствует в системе Tandem University (поле OnlineEntrantData.wantRequestType)");
        }

        if(onlineEntrant.stateExamCodes != null && !onlineEntrant.stateExamCodes.isEmpty())
        {
            for(String stateExamCode : onlineEntrant.stateExamCodes)
            {
                EnrStateExamSubject stateExamSubject = get(EnrStateExamSubject.class, EnrStateExamSubject.code(), stateExamCode);
                if(stateExamSubject == null) errors.add("Элемент справочника «Предметы ЕГЭ» с кодом «" + stateExamCode + "» отсутствует в системе Tandem University (поле OnlineEntrantData.stateExamCodes)");
            }
        }

        // персональные данные
        if(!StringUtils.isEmpty(onlineEntrant.maritalStatus))
        {
            FamilyStatus familyStatus = get(FamilyStatus.class, FamilyStatus.code(), onlineEntrant.maritalStatus);
            if (familyStatus == null)
                errors.add("Элемент справочника «Семейное положение» с кодом «" + onlineEntrant.maritalStatus + "» отсутствует в системе Tandem University (поле OnlineEntrantData.maritalStatus)");
        }

        if(!StringUtils.isEmpty(onlineEntrant.foreignLanguage))
        {
            ForeignLanguage foreignLanguage = get(ForeignLanguage.class, ForeignLanguage.code(), onlineEntrant.foreignLanguage);
            if (foreignLanguage == null)
                errors.add("Элемент справочника «Иностранные языки» с кодом «" + onlineEntrant.foreignLanguage + "» отсутствует в системе Tandem University (поле OnlineEntrantData.foreignLanguage)");
        }

        if(onlineEntrant.individualAchievements != null && !onlineEntrant.individualAchievements.isEmpty())
        {
            for(String achievement : onlineEntrant.individualAchievements)
            {
                Long achievementId = null;
                boolean numberFormatException = false;
                try
                {
                    achievementId = Long.parseLong(achievement);
                }
                catch (NumberFormatException e)
                {
                    numberFormatException = true;
                }

                EnrEntrantAchievementType entrantAchievementType = achievementId != null ? get(EnrEntrantAchievementType.class, EnrEntrantAchievementType.id(), achievementId) : null;
                if (numberFormatException || entrantAchievementType == null)
                    errors.add("Элемент «Индивидуальные достижения» с id «" + achievement + "» отсутствует в системе Tandem University (поле OnlineEntrantData.individualAchievements)");
            }
        }

        if(onlineEntrant.trainingCourses != null && !onlineEntrant.trainingCourses.isEmpty())
        {
            for(String traingCourse : onlineEntrant.trainingCourses)
            {
                EnrAccessCourse accessCourse = get(EnrAccessCourse.class, EnrAccessCourse.code(), traingCourse);
                if (accessCourse == null)
                    errors.add("Элемент справочника «Подготовительные курсы» с кодом «" + traingCourse + "» отсутствует в системе Tandem University (поле OnlineEntrantData.trainingCourses)");
            }
        }

        if(onlineEntrant.trainingDepartments != null && !onlineEntrant.trainingDepartments.isEmpty())
        {
            for(String trainingDepartment : onlineEntrant.trainingDepartments)
            {
                EnrAccessDepartment accessDepartment = get(EnrAccessDepartment.class, EnrAccessDepartment.code(), trainingDepartment);
                if (accessDepartment == null)
                    errors.add("Элемент справочника «Подготовительные отделения» с кодом «" + trainingDepartment + "» отсутствует в системе Tandem University (поле OnlineEntrantData.trainingDepartments)");
            }
        }

        if(onlineEntrant.sourceInfoAboutOUList != null && !onlineEntrant.sourceInfoAboutOUList.isEmpty())
        {
            for(String sourceInfoAboutOU : onlineEntrant.sourceInfoAboutOUList)
            {
                EnrSourceInfoAboutUniversity sourceInfoAboutUniversity = get(EnrSourceInfoAboutUniversity.class, EnrSourceInfoAboutUniversity.code(), sourceInfoAboutOU);
                if (sourceInfoAboutUniversity == null)
                    errors.add("Элемент справочника «Источники информации об ОУ» с кодом «" + sourceInfoAboutOU + "» отсутствует в системе Tandem University (поле OnlineEntrantData.sourceInfoAboutOUList)");
            }
        }

        if(onlineEntrant.sportAchievementes != null && !onlineEntrant.sportAchievementes.isEmpty())
        {
            for(OnlineEntrantData.PersonSportAchievementData sportAchievementData : onlineEntrant.sportAchievementes)
            {
                SportType sportType = get(SportType.class, SportType.code(), sportAchievementData.sportType);
                if(sportType == null) errors.add("Элемент справочника «Виды спорта» с кодом «" + sportAchievementData.sportType + "» отсутствует в системе Tandem University (поле OnlineEntrantData.sportAchievementes)");
                SportRank sportRank = get(SportRank.class, SportRank.code(), sportAchievementData.sportRank);
                if(sportRank == null) errors.add("Элемент справочника «Спортивные разряды и звания» с кодом «" + sportAchievementData.sportRank + "» отсутствует в системе Tandem University (поле OnlineEntrantData.sportAchievementes)");
            }
        }

        RelationDegree relationDegree = get(RelationDegree.class, RelationDegree.code(), onlineEntrant.relationDegree);
        if(relationDegree == null) errors.add("Элемент справочника «Степень родства» с кодом «" + onlineEntrant.relationDegree + "» отсутствует в системе Tandem University (поле OnlineEntrantData.relationDegree)");

        EduDocumentKind eduDocumentKind = get(EduDocumentKind.class, EduDocumentKind.code(), onlineEntrant.eduDocumentKind);
        if(eduDocumentKind == null) errors.add("Элемент справочника «Виды документов о полученном образовании и (или) квалификации» с кодом «" + onlineEntrant.eduDocumentKind + "» отсутствует в системе Tandem University (поле OnlineEntrantData.eduDocumentKind)");

        Integer eduOrganizationCountryCode;
        try
        {
            eduOrganizationCountryCode = Integer.parseInt(onlineEntrant.eduOrganizationCountry);
        }
        catch (NumberFormatException e)
        {
            eduOrganizationCountryCode = null;
        }

        AddressCountry eduOrganizationCountry = eduOrganizationCountryCode != null ? get(AddressCountry.class, AddressCountry.code(), eduOrganizationCountryCode) : null;

        if (eduOrganizationCountry == null)
            errors.add("Элемент «Страна» с кодом «" + onlineEntrant.eduOrganizationCountry + "» отсутствует в системе Tandem University (поле OnlineEntrantData.eduOrganizationCountry)");

        EduLevel eduLevel = get(EduLevel.class, EduLevel.code(), onlineEntrant.eduLevel);
        if(eduLevel == null) errors.add("Элемент справочника «(Новый) Виды и уровни образования» с кодом «" + onlineEntrant.eduLevel + "» отсутствует в системе Tandem University (поле OnlineEntrantData.eduLevel)");

        EduLevel documentEducationLevel = get(EduLevel.class, EduLevel.code(), onlineEntrant.documentEducationLevel);
        if(documentEducationLevel == null) errors.add("Элемент справочника «(Новый) Виды и уровни образования» с кодом «" + onlineEntrant.documentEducationLevel + "» отсутствует в системе Tandem University (поле OnlineEntrantData.documentEducationLevel)");

        GraduationHonour graduationHonour = get(GraduationHonour.class, GraduationHonour.code(), onlineEntrant.graduationHonour);
        if(graduationHonour == null) errors.add("Элемент справочника «Степени отличия по окончании образовательного учреждения» с кодом «" + onlineEntrant.graduationHonour + "» отсутствует в системе Tandem University (поле OnlineEntrantData.graduationHonour)");


        if(onlineEntrant.competitions != null && !onlineEntrant.competitions.isEmpty())
        {
            for(OnlineEntrantData.EnrCompetitionData competitionData : onlineEntrant.competitions)
            {

                Long competitionId;
                try
                {
                    competitionId = Long.parseLong(competitionData.competition);
                }
                catch (NumberFormatException e)
                {
                    competitionId = null;
                }

                EnrCompetition competition = get(EnrCompetition.class, EnrCompetition.id(), competitionId);
                if (competition == null)
                    errors.add("Элемент «Конкурс» с id «" + competitionData.competition + "» отсутствует в системе Tandem University (поле OnlineEntrantData.competitions)");
            }
        }

        if(!errors.isEmpty())
            throw new ClientSoapFaultException(ClientSoapFaultException.TANDEM_UNIVERSITY_ERROR_CODE_EXIST_DATA, StringUtils.join(errors, ";\n"));
    }

    private void validateLogic(OnlineEntrantData onlineEntrant) {}

    @Override
    public OnlineEntrantData getOnlineEntrant(Long onlineEntrantId, boolean isFullData)
    {
        if(!isOnlineEntrantExist(onlineEntrantId)) return null;

        EnrOnlineEntrant enrOnlineEntrant = get(EnrOnlineEntrant.class, EnrOnlineEntrant.onlineEntrant(), onlineEntrantId);
        EnrOnlineEntrantStatement statement = get(EnrOnlineEntrantStatement.class, EnrOnlineEntrantStatement.onlineEntrant(), enrOnlineEntrant);

        OnlineEntrantData onlineEntrantData = new OnlineEntrantData();

        onlineEntrantData.personalNumber = String.valueOf(enrOnlineEntrant.getPersonalNumber());
        onlineEntrantData.onlineEntrantId = enrOnlineEntrant.getOnlineEntrant();
        if (enrOnlineEntrant.getIdentityCard() != null)
        {
            onlineEntrantData.firstName = enrOnlineEntrant.getIdentityCard().getFirstName();
            onlineEntrantData.lastName = enrOnlineEntrant.getIdentityCard().getLastName();
            onlineEntrantData.middleName = enrOnlineEntrant.getIdentityCard().getMiddleName();
            onlineEntrantData.seria = enrOnlineEntrant.getIdentityCard().getSeria();
            onlineEntrantData.number = enrOnlineEntrant.getIdentityCard().getNumber();
            onlineEntrantData.birthDate = enrOnlineEntrant.getIdentityCard().getBirthDate();
            onlineEntrantData.birthPlace = enrOnlineEntrant.getIdentityCard().getBirthPlace();
            onlineEntrantData.issuanceDate = enrOnlineEntrant.getIdentityCard().getIssuanceDate();
            onlineEntrantData.issuancePlace = enrOnlineEntrant.getIdentityCard().getIssuancePlace();
            onlineEntrantData.issuanceCode = enrOnlineEntrant.getIdentityCard().getIssuanceCode();
            if (enrOnlineEntrant.getIdentityCard().getCitizenship() != null)
                onlineEntrantData.citizenship = String.valueOf(enrOnlineEntrant.getIdentityCard().getCitizenship().getCode());
            if (enrOnlineEntrant.getIdentityCard().getSex() != null)
                onlineEntrantData.sex = enrOnlineEntrant.getIdentityCard().getSex().getCode();

            onlineEntrantData.cardType = enrOnlineEntrant.getIdentityCard().getCardType().getCode();

            if (enrOnlineEntrant.getIdentityCard().getAddress() != null && enrOnlineEntrant.getIdentityCard().getAddress() instanceof AddressString)
                onlineEntrantData.regAddress = ((AddressString) enrOnlineEntrant.getIdentityCard().getAddress()).getAddress();

            if(isFullData)
            {
                if(enrOnlineEntrant.getIdCardScanCopy() != null)
                {
                    onlineEntrantData.idCardScanCopyName = enrOnlineEntrant.getIdCardScanCopy().getFilename();
                }
            }
        }
        onlineEntrantData.sameAddress = enrOnlineEntrant.isSameAddress();
        onlineEntrantData.factAddress = enrOnlineEntrant.getFactAddress();
        onlineEntrantData.needDorm = enrOnlineEntrant.isNeedDorm();

        onlineEntrantData.contactPhone = enrOnlineEntrant.getContactPhone();
        onlineEntrantData.mobilePhone = enrOnlineEntrant.getMobilePhone();
        onlineEntrantData.workPhone = enrOnlineEntrant.getWorkPhone();
        onlineEntrantData.email = enrOnlineEntrant.getEmail();
        onlineEntrantData.childs = enrOnlineEntrant.getChilds();
        onlineEntrantData.currentWork = enrOnlineEntrant.getCurrentWork();
        onlineEntrantData.currentPost = enrOnlineEntrant.getCurrentPost();
        onlineEntrantData.armyStartDate = enrOnlineEntrant.getArmyStartDate();
        onlineEntrantData.armyEndDate = enrOnlineEntrant.getArmyEndDate();
        onlineEntrantData.armyEndYear = enrOnlineEntrant.getArmyEndYear();

        if(enrOnlineEntrant.getFamilyStatus() != null)
            onlineEntrantData.maritalStatus = enrOnlineEntrant.getFamilyStatus().getCode();
        if(enrOnlineEntrant.getForeignLanguage() != null)
            onlineEntrantData.foreignLanguage = enrOnlineEntrant.getForeignLanguage().getCode();

        onlineEntrantData.statementAccepted = enrOnlineEntrant.getEntrant() != null;
        if(isFullData)
        {
            if(enrOnlineEntrant.getIndAchievScanCopy() != null)
            {
                onlineEntrantData.indAchievScanCopyName = enrOnlineEntrant.getIndAchievScanCopy().getFilename();
            }

            if(enrOnlineEntrant.getOtherDocScanCopy() != null)
            {
                onlineEntrantData.otherDocScanCopyName = enrOnlineEntrant.getOtherDocScanCopy().getFilename();
            }

            List<EnrOnlineEntrantCompetition> competitions = getList(EnrOnlineEntrantCompetition.class, EnrOnlineEntrantCompetition.statement(), statement);
            if(!competitions.isEmpty())
            {
                for(EnrOnlineEntrantCompetition competition : competitions)
                {
                    OnlineEntrantData.EnrCompetitionData competitionData = new OnlineEntrantData.EnrCompetitionData();
                    competitionData.competition = competition.getCompetition().getId().toString();
                    competitionData.priority = competition.getPriority();
                    onlineEntrantData.competitions.add(competitionData);
                }
            }

            onlineEntrantData.haveEduLevel = statement.getHaveEduLevel();
            onlineEntrantData.wantRequestType = statement.getWantRequestType();
            onlineEntrantData.exclusiveRights = statement.isExclusiveRights();     // У меня есть особые права
            onlineEntrantData.exclusiveRightsWithoutExams = statement.isExclusiveRightsWithoutExams();     // Право на прием без вступительных испытаний
            onlineEntrantData.exclusiveRightsBudget = statement.isExclusiveRightsBudget();     // Право на прием на обучение за счет бюджетных ассигнований в пределах установленной квоты
            onlineEntrantData.exclusiveRightsEmptiveEnroll = statement.isExclusiveRightsEmptiveEnroll();     // Преимущественное право зачисления

            onlineEntrantData.haveAgreementTargetEdu = statement.isHaveAgreementTargetEdu();     // Заключен договор о целевом обучении
            onlineEntrantData.agreementNumber = statement.getAgreementNumber();     // Номер договора
            onlineEntrantData.agreementDate = statement.getAgreementDate();     // Дата заключения
            onlineEntrantData.agreementOrg = statement.getAgreementOrg();     // Данные об организации
            onlineEntrantData.agreementEduProgramm = statement.getAgreementEduProgramm();     // Данные об образовательной программе

            List<EnrOnlineEntrantStateExam> stateExams = getList(EnrOnlineEntrantStateExam.class, EnrOnlineEntrantStateExam.onlineEntrant().id(), enrOnlineEntrant.getId());
            if(!stateExams.isEmpty())
            {
                for(EnrOnlineEntrantStateExam stateExam : stateExams)
                {
                    onlineEntrantData.stateExamCodes.add(stateExam.getStateExamSubject().getCode());
                }
            }

            EnrOnlineEntrantEduDocument document = get(EnrOnlineEntrantEduDocument.class, EnrOnlineEntrantEduDocument.onlineEntrant().id(), enrOnlineEntrant.getId());
            if(document != null && document.getDocKind() != null)
            {
                onlineEntrantData.eduDocumentKind = document.getDocKind().getCode();
                onlineEntrantData.documentKindDetailed = document.getDocumentKindDetailed();
                onlineEntrantData.seriaEduDocument = document.getSeria();
                onlineEntrantData.numberEduDocument = document.getNumber();
                if(document.getEduOrgCountry() != null)
                    onlineEntrantData.eduOrganizationCountry = String.valueOf(document.getEduOrgCountry().getCode());
                onlineEntrantData.eduOrganizationSettlement = document.getEduOrganizationSettlement();
                onlineEntrantData.eduOrganization = document.getEduOrganization();
                onlineEntrantData.yearEnd = document.getYearEnd();
                onlineEntrantData.ruRegionCode = document.getRuRegionCode();
                onlineEntrantData.issuanceDateEduDocument = document.getIssuanceDate();
                if(document.getEduLevel() != null)
                    onlineEntrantData.eduLevel = document.getEduLevel().getCode();
                onlineEntrantData.documentEducationLevel = document.getDocumentEducationLevel();
                onlineEntrantData.qualification = document.getQualification();
                onlineEntrantData.eduProgramSubject = document.getEduProgramSubject();
                if(document.getHonour() != null)
                    onlineEntrantData.graduationHonour = document.getHonour().getCode();
                onlineEntrantData.avgMarkAsLong = document.getAvgMarkAsLong();
                if(document.getScanCopy() != null && document.getScanCopy().getContent() != null)
                {
                    onlineEntrantData.scanCopyName = document.getScanCopy().getFilename();
                    onlineEntrantData.scanCopyContentType = document.getScanCopy().getContentType();
                    onlineEntrantData.scanCopy = document.getScanCopy().getContent();
                }

                onlineEntrantData.mark5 =  document.getMark5();     // Кол-во оценок «Отлично»
                onlineEntrantData.mark4 =  document.getMark4();     // Кол-во оценок «Хорошо»
                onlineEntrantData.mark3 =  document.getMark3();     // Кол-во оценок «Удовлетворительно»
                onlineEntrantData.registrationNumber =  document.getRegistrationNumber();     // Регистрационный номер

            }
            EnrOnlineEntrantNextOfKin nextOfKin = get(EnrOnlineEntrantNextOfKin.class, EnrOnlineEntrantNextOfKin.onlineEntrant().id(), enrOnlineEntrant.getId());
            if(nextOfKin != null && nextOfKin.getRelationDegree() != null)
            {
                onlineEntrantData.relationDegree = nextOfKin.getRelationDegree().getCode();
                onlineEntrantData.lastNameNextOfKin = nextOfKin.getLastname();
                onlineEntrantData.firstNameNextOfKin = nextOfKin.getFirstname();
                onlineEntrantData.patronymicNextOfKin = nextOfKin.getPatronymic();
                onlineEntrantData.employmentPlace = nextOfKin.getEmploymentPlace();
                onlineEntrantData.post = nextOfKin.getPost();
                onlineEntrantData.phones = nextOfKin.getPhones();
            }

            List<EnrOnlineEntrantAchievement> achievements = getList(EnrOnlineEntrantAchievement.class, EnrOnlineEntrantAchievement.onlineEntrant().id(), enrOnlineEntrant.getId());

            for(EnrOnlineEntrantAchievement individualAchievement : achievements) // Индивидуальные достижения (коды university)
            {
                onlineEntrantData.individualAchievements.add(individualAchievement.getAchievementType().getId().toString());
            }

            List<EnrOnlineEntrantAccessCourse> accessCourses = getList(EnrOnlineEntrantAccessCourse.class, EnrOnlineEntrantAccessCourse.onlineEntrant().id(), enrOnlineEntrant.getId());
            for(EnrOnlineEntrantAccessCourse trainingCourse : accessCourses) // Подготовительные курсы (коды university)
            {
                onlineEntrantData.trainingCourses.add(trainingCourse.getCourse().getCode());
            }

            List<EnrOnlineEntrantAccessDepartment> accessDepartments = getList(EnrOnlineEntrantAccessDepartment.class, EnrOnlineEntrantAccessDepartment.onlineEntrant().id(), enrOnlineEntrant.getId());
            for(EnrOnlineEntrantAccessDepartment trainingDepartment : accessDepartments) // Подготовительные отделения (коды university)
            {
               onlineEntrantData.trainingDepartments.add(trainingDepartment.getDepartment().getCode());
            }

            List<EnrOnlineEntrantSourceInfoAboutOU> sourceInfoAboutOUs = getList(EnrOnlineEntrantSourceInfoAboutOU.class, EnrOnlineEntrantSourceInfoAboutOU.onlineEntrant().id(), enrOnlineEntrant.getId());
            for(EnrOnlineEntrantSourceInfoAboutOU sourceInfoAboutOu : sourceInfoAboutOUs) // Источники информации об ОУ (коды university)
            {
               onlineEntrantData.sourceInfoAboutOUList.add(sourceInfoAboutOu.getSourceInfo().getCode());
            }

            List<EnrOnlineEntrantSportAchievement> sportAchievementes = getList(EnrOnlineEntrantSportAchievement.class, EnrOnlineEntrantSportAchievement.onlineEntrant().id(), enrOnlineEntrant.getId());
            for(EnrOnlineEntrantSportAchievement sportAchievement : sportAchievementes) // Спортивные достижения
            {
                OnlineEntrantData.PersonSportAchievementData sportAchievementData = new OnlineEntrantData.PersonSportAchievementData();
                sportAchievementData.sportType = sportAchievement.getType().getCode();
                sportAchievementData.sportRank = sportAchievement.getRank().getCode();
                onlineEntrantData.sportAchievementes.add(sportAchievementData);
            }


        }

        return onlineEntrantData;
    }

    @Override
    public boolean isOnlineEntrantExist(Long onlineEntrantId)
    {
        return get(EnrOnlineEntrant.class, EnrOnlineEntrant.onlineEntrant(), onlineEntrantId) != null;
    }

    @Override
    public boolean isOnlineEntrantStatementAccepted(Long onlineEntrantId)
    {
        EnrOnlineEntrant enrOnlineEntrant = get(EnrOnlineEntrant.class, EnrOnlineEntrant.onlineEntrant(), onlineEntrantId);
        return enrOnlineEntrant != null && enrOnlineEntrant.getEntrant() != null;
    }

    @Override
    public String getEntrantId(Long onlineEntrantId)
    {
        EnrOnlineEntrant enrOnlineEntrant = get(EnrOnlineEntrant.class, EnrOnlineEntrant.onlineEntrant(), onlineEntrantId);
        return enrOnlineEntrant == null || enrOnlineEntrant.getEntrant() == null ? null : enrOnlineEntrant.getEntrant().getId().toString();
    }

    @Override
    public List<String> getOnlineEntrantCompetitionsUids(Long onlineEntrantId)
    {
        EnrOnlineEntrant enrOnlineEntrant = get(EnrOnlineEntrant.class, EnrOnlineEntrant.onlineEntrant(), onlineEntrantId);
        if(enrOnlineEntrant == null || enrOnlineEntrant.getEntrant() == null) return null;

        List<String> onlineEntrantCompetitionsUids = Lists.newArrayList();
        for(EnrRequestedCompetition competition : getList(EnrRequestedCompetition.class, EnrRequestedCompetition.request().entrant().id(), enrOnlineEntrant.getEntrant().getId()))
        {
            onlineEntrantCompetitionsUids.add(competition.getCompetition().getId().toString());
        }
        if(onlineEntrantCompetitionsUids.isEmpty()) return null;

        return onlineEntrantCompetitionsUids;
    }

    private int getMaxEntrantPersonalNumber(EnrEnrollmentCampaign enrollmentCampaign)
    {
        NamedSyncInTransactionCheckLocker.register(getSession(), "Enr2014OnlineEntrantPersonalNumberSync-" + enrollmentCampaign.getId());

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrOnlineEntrant.class, "e");
        builder.column(DQLFunctions.max(property("e", EnrOnlineEntrant.personalNumber())));
        builder.where(eq(property("e", EnrOnlineEntrant.enrEnrollmentCampaign().id()), value(enrollmentCampaign.getId())));

        Number result = builder.createStatement(getSession()).uniqueResult();

        return (result != null) ? result.intValue() : 0;
    }
}
