/**
 *$Id: EnrExamGroupPubEntrantSearchDSHandler.java 33636 2014-04-16 04:31:39Z nfedorovskih $
 */
package ru.tandemservice.unienr14.exams.bo.EnrExamGroup.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroup;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 03.06.13
 */
public class EnrExamGroupPubEntrantSearchDSHandler extends DefaultSearchDataSourceHandler
{
    public static final String BIND_EXAM_GROUP = "examGroup";

    public static final String V_PROP_INVALID_ENTRANT = "invalidEntrant";

    public EnrExamGroupPubEntrantSearchDSHandler(String ownerId)
    {
        super(ownerId, EnrExamPassDiscipline.class);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final EnrExamGroup examGroup = context.get(BIND_EXAM_GROUP);

        final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EnrExamPassDiscipline.class, "b").column(property("b"))
                .where(eq(property(EnrExamPassDiscipline.examGroup().fromAlias("b")), value(examGroup)))
                .where(isNotNull(property(EnrExamPassDiscipline.examGroup().fromAlias("b"))));

        filter(dql, "b", input, context);
        final DSOutput output = DQLSelectOutputBuilder.get(input, dql, context.getSession()).order().pageable(false).build();

        return wrap(output, input, context);
    }

    /**
     * @param dql builder from EnrExamPassDiscipline
     */
    protected void filter(DQLSelectBuilder dql, String alias, DSInput input, ExecutionContext context)
    {

    }

    protected DSOutput wrap(DSOutput output, DSInput input, ExecutionContext context)
    {
        final Map<Long, List<CoreCollectionUtils.Pair<Long, String>>> entrant2DiscPassFormPairsMap = SafeMap.get(ArrayList.class);
        BatchUtils.execute(output.<EnrExamPassDiscipline>getRecordList(), 128, new BatchUtils.Action<EnrExamPassDiscipline>()
        {
            @Override
            public void execute(Collection<EnrExamPassDiscipline> elements)
            {
                final List<Object[]> list = new DQLSelectBuilder().fromEntity(EnrChosenEntranceExamForm.class, "f")
                        .column(property(EnrChosenEntranceExamForm.chosenEntranceExam().requestedCompetition().request().entrant().id().fromAlias("f")))
                        .column(property(EnrChosenEntranceExamForm.chosenEntranceExam().discipline().id().fromAlias("f")))
                        .column(property(EnrChosenEntranceExamForm.passForm().code().fromAlias("f")))
                        .where(in(property(EnrChosenEntranceExamForm.chosenEntranceExam().requestedCompetition().request().entrant().id().fromAlias("f")), CommonBaseUtil.getPropertiesList(elements, EnrExamPassDiscipline.entrant().id().s())))
                        .where(isNotNull(property(EnrChosenEntranceExamForm.chosenEntranceExam().actualExam().fromAlias("f"))))
                        .createStatement(context.getSession()).list();

                for (Object[] objects : list)
                {
                    final Long entrantId = (Long) objects[0];
                    final Long discId = (Long) objects[1];
                    final String passFormCode = (String) objects[2];

                    entrant2DiscPassFormPairsMap.get(entrantId)
                            .add(new CoreCollectionUtils.Pair<>(discId, passFormCode));
                }
            }
        });

        for (DataWrapper wrapper : DataWrapper.wrap(output))
        {
            final EnrExamPassDiscipline examPassDiscipline = wrapper.getWrapped();

            final boolean validEntrant = entrant2DiscPassFormPairsMap.get(examPassDiscipline.getEntrant().getId()).contains(new CoreCollectionUtils.Pair<>(examPassDiscipline.getDiscipline().getId(), examPassDiscipline.getPassForm().getCode()));

            wrapper.setProperty(V_PROP_INVALID_ENTRANT, !validEntrant);
        }

        return output;
    }
}
