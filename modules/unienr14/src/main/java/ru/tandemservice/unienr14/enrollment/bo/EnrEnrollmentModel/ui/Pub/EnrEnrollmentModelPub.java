/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.config.tab.ITabPanelExtPointBuilder;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.ui.StatsTab.EnrEnrollmentModelStatsTab;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.ui.CheckEnrTab.EnrEnrollmentStepCheckEnrTab;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.ui.CheckRecTab.EnrEnrollmentStepCheckRecTab;
import ru.tandemservice.unienr14.enrollment.entity.EnrModelStep;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 3/27/15
 */
@Configuration
public class EnrEnrollmentModelPub extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(searchListDS("stepDS", stepDSColumns(), stepDSHandler()))
            .create();
    }

    @Bean
    public TabPanelExtPoint enrollmentModelPubTabPanelExtPoint()
    {
        return tabPanelExtPointBuilder("enrollmentModelPubTabPanel")
            .addTab(htmlTab("mainTab", "MainTab"))
            .addTab(componentTab("statsTab", EnrEnrollmentModelStatsTab.class))
            .create();
    }


    @Bean
    public ColumnListExtPoint stepDSColumns()
    {
        return columnListExtPointBuilder("stepDS")
            .addColumn(textColumn("date", EnrModelStep.enrollmentDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order().clickable(true))
            .addColumn(textColumn("kind", EnrModelStep.kind().title()))
            .create();
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> stepDSHandler()
    {
        return new DefaultSearchDataSourceHandler(getName()) {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context) {
                final DQLSelectBuilder dql = new DQLSelectBuilder()
                    .fromEntity(EnrModelStep.class, "s").column(property("s"))
                    .where(eq(property(EnrModelStep.model().id().fromAlias("s")), value(context.<Long>get("model"))));

                return DQLSelectOutputBuilder.get(input, dql, context.getSession()).pageable(true).order().build();
            }
        };
    }
}