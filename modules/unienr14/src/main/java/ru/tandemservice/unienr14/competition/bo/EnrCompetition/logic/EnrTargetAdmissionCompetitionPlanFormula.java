/* $Id:$ */
package ru.tandemservice.unienr14.competition.bo.EnrCompetition.logic;

import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEduLevelRequirementCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrMethodDivCompetitionsCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrRequestTypeCodes;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.competition.entity.EnrTargetAdmissionPlan;

import static ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes.TARGET_ADMISSION;
import static ru.tandemservice.unienr14.catalog.entity.codes.EnrEduLevelRequirementCodes.SOO;
import static ru.tandemservice.unienr14.catalog.entity.codes.EnrEduLevelRequirementCodes.SPO;
import static ru.tandemservice.unienr14.competition.bo.EnrCompetition.logic.EnrCompetitionPlanFormula.*;

/**
 * @author oleyba
 * @since 3/20/15
 */
public class EnrTargetAdmissionCompetitionPlanFormula
{
    public interface ITargetAdmissionPlan {
        EnrTargetAdmissionPlan getTargetAdmissionPlan();
        EnrCompetition getCompetition();
    }

    public static int calculatePlan(ITargetAdmissionPlan taPlan, IPlanCalculationContext context)
    {
        EnrProgramSetBase programSet = taPlan.getCompetition().getProgramSetOrgUnit().getProgramSet();
        String method = programSet.getMethodDivCompetitions().getCode();
        boolean noDiv = !EnrRequestTypeCodes.BS.equals(programSet.getRequestType().getCode()) || method.equals(EnrMethodDivCompetitionsCodes.BS_NO_DIV);

        if (IEnrCompetitionDao.PlanCalculationRule.EQUAL_PARTS.equals(context.correctionRule())) {

            if (noDiv || method.equals(EnrMethodDivCompetitionsCodes.BS_SOO_AND_SPO_APART_VO)) {
                //Установить число мест (объект будет 1), равное плану по виду ЦП.
                return taPlan.getTargetAdmissionPlan().getPlan();
            } else {
                //Установить число мест (объекта будет 2) по базовому уровню их конкурсов
                //|СОО|округл(план/2)|
                //|СПО|план-округл(план/2)|
                //где план — план по виду ЦП.
                if (EnrEduLevelRequirementCodes.SOO.equals(taPlan.getCompetition().getEduLevelRequirement().getCode())) {
                    return half(taPlan.getTargetAdmissionPlan().getPlan());
                } else {
                    return restFromHalf(taPlan.getTargetAdmissionPlan().getPlan());
                }
            }

        } else if (IEnrCompetitionDao.PlanCalculationRule.BY_PASSED.equals(context.correctionRule())) {

            if (noDiv || method.equals(EnrMethodDivCompetitionsCodes.BS_SOO_AND_SPO_APART_VO)) {
                //Установить число мест (объект будет 1), равное плану по виду ЦП.
                return taPlan.getTargetAdmissionPlan().getPlan();
            } else {
                // Установить число мест (объекта будет 2) по базовому уровню их конкурсов
                // СОО| если усп_ЦП(СОО)+усп_ЦП(СПО)>0, то округл(план*усп_ЦП(СОО)/(усп_ЦП(СОО)+усп_ЦП(СПО))), иначе округл(план/2)|
                // СПО| если усп_ЦП(СОО)+усп_ЦП(СПО)>0, то план-округл(план*усп_ЦП(СОО)/(усп_ЦП(СОО)+усп_ЦП(СПО))), иначе план-округл(план/2)|
                if (EnrEduLevelRequirementCodes.SOO.equals(taPlan.getCompetition().getEduLevelRequirement().getCode())) {
                    return mult(taPlan.getTargetAdmissionPlan().getPlan(), k(context, TARGET_ADMISSION, SOO, SOO, SPO));
                } else {
                    return restFromMult(taPlan.getTargetAdmissionPlan().getPlan(), k(context, TARGET_ADMISSION, SOO, SOO, SPO));
                }
            }

        } else {

            // Установить число мест, равное числу зачисленных абитуриентов по связанному конкурсу ЦП с тем же видом ЦП
            return context.e(taPlan.getTargetAdmissionPlan().getEnrCampaignTAKind().getTargetAdmissionKind().getCode(),
                EnrCompetitionTypeCodes.TARGET_ADMISSION,
                taPlan.getCompetition().getEduLevelRequirement().getCode());

        }
    }
}
