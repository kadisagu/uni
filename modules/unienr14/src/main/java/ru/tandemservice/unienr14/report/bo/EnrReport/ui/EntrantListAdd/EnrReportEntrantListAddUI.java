/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantListAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.process.*;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilter;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilterType;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterFormConfig;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.report.bo.EnrReport.EnrReportManager;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.ui.Pub.EnrReportBasePub;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.EnrReportPersonAdd;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author oleyba
 * @since 5/14/14
 */
public class EnrReportEntrantListAddUI extends UIPresenter
{
    private EnrEnrollmentCampaign _enrollmentCampaign;

    private Date dateFrom;
    private Date dateTo;

    private boolean parallelActive;
    private IdentifiableWrapper parallel;
    private List<IdentifiableWrapper> parallelList;

    private IdentifiableWrapper sorting;
    private List<IdentifiableWrapper> sortingList;

    private boolean skipEmptyList;
    private boolean skipEmptyCompetition;
    private boolean replaceProgramSetTitle;
    private boolean skipProgramSetTitle;
    private boolean printPriority = true;

    private long reportId;

    @Override
    public void onComponentPrepareRender()
    {
        if (reportId != 0)
        {
            activatePublisher();
            reportId = 0L;
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void onComponentRefresh()
    {
        setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());

        setDateFrom(getEnrollmentCampaign() == null ? CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())) : getEnrollmentCampaign().getDateFrom());
        setDateTo(getEnrollmentCampaign() == null ? new Date() : getEnrollmentCampaign().getDateTo());

        configUtil(getCompetitionFilterAddon());

        setParallelList(Arrays.asList(new IdentifiableWrapper(0L, "не учитывать выбранные параллельно условия поступления"),
                                      new IdentifiableWrapper(1L, "включать в отчет только выбранные параллельно условия поступления")));
        setSortingList(Arrays.asList(new IdentifiableWrapper(0L, "по номеру заявления"),
                                     new IdentifiableWrapper(1L, "по алфавиту")));

        if (getSorting() == null) setSorting(new IdentifiableWrapper(0L, "по номеру заявления"));
    }
    // validate
    private void validate()
    {
        if (getDateFrom().after(getDateTo()))
            _uiSupport.error("Дата, указанная в параметре \"Заявления с\" не должна быть позже даты в параметре \"Заявления по\".", "dateFrom");

        if(getUserContext().getErrorCollector().hasErrors())
            throw new ApplicationException();
    }

    public void setReportId(long reportId)
    {
        this.reportId = reportId;
    }

    public void onClickApply() {
        validate();
        BusinessComponentUtils.runProcess(new BackgroundProcessThread("Формирование отчета", new BackgroundProcessBase() {
            @Override
            public ProcessResult run(final ProcessState state) {

                setReportId(EnrReportManager.instance().entrantListDao().createReport(EnrReportEntrantListAddUI.this));
                return null;
            }
        }, ProcessDisplayMode.unknown));
    }

    public void activatePublisher()
    {
       _uiActivation.asDesktopRoot(EnrReportBasePub.class)
                .parameter(PUBLISHER_ID, reportId)
                .activate();
       deactivate();
    }


    public void onChangeEnrollmentCampaign()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
        final EnrCompetitionFilterAddon entrantRequestUtil = getCompetitionFilterAddon();
        configUtilWhere(entrantRequestUtil);
        setDateFrom(getEnrollmentCampaign().getDateFrom());
        setDateTo(getEnrollmentCampaign().getDateTo());
    }

    // for report builder

    public boolean isParallelOnly() {
        return isParallelActive() && 1L == getParallel().getId();
    }

    public boolean isSkipParallel() {
        return isParallelActive() && 0L == getParallel().getId();
    }

    public boolean isSortByRequestNumber() {
        return 0L == getSorting().getId();
    }


    // utils

    private void configUtilWhere(EnrCompetitionFilterAddon util)
    {
        util.clearWhereFilter();
        util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EnrCompetition.programSetOrgUnit().orgUnit().enrollmentCampaign(), getEnrollmentCampaign()));
    }

    private void configUtil(EnrCompetitionFilterAddon util)
    {

        util
            .configDoubleWidthFilters(false)
            .configUseEnableCheckbox(true)
            .configSettings(getSettingsKey());

        util.clearFilterItems();

        util
            .addFilterItem(EnrCompetitionFilterAddon.REQUEST_TYPE, new CommonFilterFormConfig(true, false, true, false, true, true))
            .addFilterItem(EnrCompetitionFilterAddon.COMPENSATION_TYPE, CommonFilterFormConfig.DEFAULT_CHECKBOX_SELECT_FILTER_CONFIG)
            .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_FORM, CommonFilterFormConfig.DEFAULT_CHECKBOX_SELECT_FILTER_CONFIG)
            .addFilterItem(EnrCompetitionFilterAddon.COMPETITION_TYPE, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
            .addFilterItem(EnrCompetitionFilterAddon.ENR_ORG_UNIT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
            .addFilterItem(EnrCompetitionFilterAddon.FORMATIVE_ORG_UNIT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
            .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SUBJECT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
            .addFilterItem(EnrCompetitionFilterAddon.EDU_PROGRAM, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
            .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SET, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG);

        configUtilWhere(util);
    }

    public EnrCompetitionFilterAddon getCompetitionFilterAddon()
    {
        return (EnrCompetitionFilterAddon) getConfig().getAddon(EnrReportPersonAdd.COMPETITION_FILTERS_ENTRANT_REQUEST);
    }

    // getters and setters


    public Date getDateFrom()
    {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom)
    {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo()
    {
        return dateTo;
    }

    public void setDateTo(Date dateTo)
    {
        this.dateTo = dateTo;
    }

    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }

    public IdentifiableWrapper getParallel()
    {
        return parallel;
    }

    public void setParallel(IdentifiableWrapper parallel)
    {
        this.parallel = parallel;
    }

    public boolean isParallelActive()
    {
        return parallelActive;
    }

    public void setParallelActive(boolean parallelActive)
    {
        this.parallelActive = parallelActive;
    }

    public List<IdentifiableWrapper> getParallelList()
    {
        return parallelList;
    }

    public void setParallelList(List<IdentifiableWrapper> parallelList)
    {
        this.parallelList = parallelList;
    }

    public boolean isReplaceProgramSetTitle()
    {
        return replaceProgramSetTitle;
    }

    public void setReplaceProgramSetTitle(boolean replaceProgramSetTitle)
    {
        this.replaceProgramSetTitle = replaceProgramSetTitle;
    }

    public boolean isSkipEmptyCompetition()
    {
        return skipEmptyCompetition;
    }

    public void setSkipEmptyCompetition(boolean skipEmptyCompetition)
    {
        this.skipEmptyCompetition = skipEmptyCompetition;
    }

    public boolean isSkipEmptyList()
    {
        return skipEmptyList;
    }

    public void setSkipEmptyList(boolean skipEmptyList)
    {
        this.skipEmptyList = skipEmptyList;
    }

    public boolean isSkipProgramSetTitle()
    {
        return skipProgramSetTitle;
    }

    public void setSkipProgramSetTitle(boolean skipProgramSetTitle)
    {
        this.skipProgramSetTitle = skipProgramSetTitle;
    }

    public IdentifiableWrapper getSorting()
    {
        return sorting;
    }

    public void setSorting(IdentifiableWrapper sorting)
    {
        this.sorting = sorting;
    }

    public List<IdentifiableWrapper> getSortingList()
    {
        return sortingList;
    }

    public void setSortingList(List<IdentifiableWrapper> sortingList)
    {
        this.sortingList = sortingList;
    }

    public boolean isPrintPriority()
    {
        return printPriority;
    }

    public void setPrintPriority(boolean printPriority)
    {
        this.printPriority = printPriority;
    }
}