/**
 *$Id: EnrRequestedDirectionManager.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.request.bo.EnrRequestedCompetition;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.config.itemList.IItemListExtPointBuilder;
import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;

import org.tandemframework.caf.ui.datasource.select.ISelectDSConfigBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.unienr14.request.bo.EnrRequestedCompetition.logic.EnrReqCompBudgetAcceptedRule;
import ru.tandemservice.unienr14.request.bo.EnrRequestedCompetition.logic.EnrReqCompContractAcceptedRule;
import ru.tandemservice.unienr14.request.bo.EnrRequestedCompetition.logic.IPropertyUpdateRule;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.bo.EnrSettings.ui.AdmissionRequirements.EnrSettingsAdmissionRequirements;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

/**
 * @author Alexander Shaburov
 * @since 13.05.13
 */
@Configuration
public class EnrRequestedCompetitionManager extends BusinessObjectManager
{
    public static final String BIND_EDU_DOC_ID = "eduDocId";
    public static final String BIND_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String BIND_ENTRANT = "entrant";
    public static final String DS_REQ_COMP_ORIGINAL = "reqCompDS";

    public static EnrRequestedCompetitionManager instance()
    {
        return instance(EnrRequestedCompetitionManager.class);
    }


    @Bean
    public ItemListExtPoint<IPropertyUpdateRule<Boolean>> enrollmentAvailableRules()
    {
        @SuppressWarnings("unchecked")
        final IItemListExtPointBuilder<IPropertyUpdateRule<Boolean>> extPointBuilder = (IItemListExtPointBuilder)itemList(IPropertyUpdateRule.class);

        return extPointBuilder
        .add("enrRequestedCompetition.acceptedContract", EnrReqCompContractAcceptedRule.INSTANCE)
        .add("enrRequestedCompetition.acceptedBugdet", EnrReqCompBudgetAcceptedRule.INSTANCE)
        .create();
    }


    // датасурс выбранных конкурсов для оригиналов
    public ISelectDSConfigBuilder createEnrRequestedCompetitionDS()
    {
        return CommonBaseStaticSelectDataSource.selectDS(DS_REQ_COMP_ORIGINAL, getName(), EnrRequestedCompetition.defaultSelectDSHandler(getName()).customize((alias, dql, context, filter) -> {
            Long eduDocId = context.get(BIND_EDU_DOC_ID);

            if(eduDocId != null)
            {
                dql.where(eq(property(alias, EnrRequestedCompetition.request().eduDocument().id()), value(eduDocId)));
            }

            return dql;
        })
                .where(EnrRequestedCompetition.request().entrant(), BIND_ENTRANT)
        );
    }
}
