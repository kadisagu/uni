/* $Id: EnrEnrollmentCampaignManager.java 32113 2014-01-27 09:14:01Z hudson $ */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.logic.EnrCampaignDao;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.logic.EnrCampaignPermissionsDao;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.logic.IEnrCampaignDao;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.logic.IEnrCampaignPermissionsDao;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author nvankov
 * @since 4/5/13
 */
@Configuration
public class EnrEnrollmentCampaignManager extends BusinessObjectManager
{
    public static final String DS_ENR_CAMPAIGN = "enrCampaignDS";
    public static final String PARAM_ENR_CAMPAIGN = "enrCampaign";
    public static final String BINDING_ENR_CAMPAIGN_ID = "enrollmentCampaignId";

    public static final Comparator<IEntity> ENR_CAMPAIGN_COMPARATOR = new Comparator<IEntity>() {
        @Override public int compare(IEntity o1, IEntity o2)
        {
            if ((o1 == o2) || !(o1 instanceof EnrEnrollmentCampaign && o2 instanceof EnrEnrollmentCampaign))
                return 0;
            EnrEnrollmentCampaign e1 = (EnrEnrollmentCampaign) o1;
            EnrEnrollmentCampaign e2 = (EnrEnrollmentCampaign) o2;

            int result = -e1.getEducationYear().getIntValue() + e2.getEducationYear().getIntValue();
            if (result == 0)
                result = e1.getTitle().compareTo(e2.getTitle());
            if (result == 0)
                result = -e1.getId().compareTo(e2.getId());

            return result;
        }
    };

    public static EnrEnrollmentCampaignManager instance()
    {
        return instance(EnrEnrollmentCampaignManager.class);
    }

    @Bean
    public IEnrCampaignDao enrCampaignDAO()
    {
        return new EnrCampaignDao();
    }

    @Bean
    public IEnrCampaignPermissionsDao enrCampaignPermissionsDao()
    {
        return new EnrCampaignPermissionsDao();
    }

    /** @return Конфиг стандартного селекта ПК (DEV-2635) */
    @Bean
    public UIDataSourceConfig enrCampaignDSConfig() {
        return SelectDSConfig.with(EnrEnrollmentCampaignManager.DS_ENR_CAMPAIGN, this.getName())
        .dataSourceClass(SelectDataSource.class)
        .handler(this.enrCampaignDSHandler())
        .create();
    }

    public static final String ENR_CAMPAIGN_DS_PARAM_ORG_UNIT = "enrCampaignFilterOrgUnit";

    /** @return Хэндлер для стандартного селекта ПК (DEV-2635) */
    @Bean
    public IDefaultComboDataSourceHandler enrCampaignDSHandler() {
        return EnrEnrollmentCampaign.permissionSelectDSHandler(this.getName());
    }

    public ISelectModel getEnrollmentCampaignModel() {
        return enrCampaignPermissionsDao().getEnrollmentCampaignModel();
    }

    public List<EnrEnrollmentCampaign> getEnrollmentCampaignList() {
        return enrCampaignDAO().getEnrollmentCampaignList();
    }

    public boolean isEnrOrgUnit(OrgUnit entity)
    {
        return ISharedBaseDao.instance.get().existsEntity(EnrOrgUnit.class, EnrOrgUnit.institutionOrgUnit().orgUnit().s(), entity);
    }


}
