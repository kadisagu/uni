/* $Id$ */
package ru.tandemservice.unienr14.entrant.bo.EnrSubmittedEduDocument.logic;

import com.google.common.collect.Lists;
import org.tandemframework.core.common.INaturalId;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import org.tandemframework.tapsupport.component.selection.ListResult;
import org.tandemframework.tapsupport.component.selection.SingleSelectTextModel;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantOriginalDocumentStatus;
import ru.tandemservice.unienr14.entrant.entity.EnrOrganizationOriginalIn;
import ru.tandemservice.unienr14.entrant.entity.gen.EnrEntrantOriginalDocumentStatusGen;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.EnrEntrantRequestManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author azhebko
 * @since 31.07.2014
 */
public class EnrSubmittedEduDocumentDao extends UniBaseDao implements IEnrSubmittedEduDocumentDao
{
    public void updateEntrantOriginalDocumentStatus(Long personEduDocumentId, EnrEnrollmentCampaign campaign)
    {
//        long date = new Date().getTime();
//        System.out.println("EnrSubmittedEduDocumentDao - начало: " + date);

        PersonEduDocument eduDocument = this.getNotNull(personEduDocumentId);
        EnrEntrant entrant = new DQLSelectBuilder()
            .fromEntity(EnrEntrant.class, "e")
            .where(eq(property("e", EnrEntrant.person().id()), value(eduDocument.getPerson().getId())))
            .where(eq(property("e", EnrEntrant.enrollmentCampaign().id()), value(campaign.getId())))
            .createStatement(this.getSession()).uniqueResult();

        if (entrant == null)
            throw new IllegalStateException(); // нет подходящего абитуриента (что странно)

        EnrEntrantOriginalDocumentStatus original = this.getByNaturalId(new EnrEntrantOriginalDocumentStatusGen.NaturalId(entrant, eduDocument));
        if (original == null)
        {
            original = new EnrEntrantOriginalDocumentStatus(entrant, eduDocument);
            original.setRegistrationDate(new Date());
            this.save(original);

            return;
        }

        if (original.isOriginalDocumentHandedIn())
            original.setTakeAwayDate(new Date());
        else
        {
            original.setRegistrationDate(new Date());
            original.setTakeAwayDate(null);
        }

        this.update(original);
//        System.out.println("EnrSubmittedEduDocumentDao - конец: " + (new Date().getTime() - date));
//        System.out.println("EnrSubmittedEduDocumentDao - конец: " + new Date().getTime());
    }

    // к сожалению, CAF этого не умеет
    public SingleSelectTextModel getOrganizationOriginalInDS()
    {
        return new SingleSelectTextModel()
        {
            @Override
            public ListResult findValues(final String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrOrganizationOriginalIn.class, "o")
                        .column(property("o", EnrOrganizationOriginalIn.title())).distinct()
                        .where(likeUpper(property("o", EnrOrganizationOriginalIn.title()), value(CoreStringUtils.escapeLike(filter, true))))
                        .order(property("o", EnrOrganizationOriginalIn.title()));

                List<String> list = builder.createStatement(getSession()).setMaxResults(50).list();
                int number = list.size() == 50 ? ((Number) builder.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult()).intValue() : list.size();
                return new ListResult<>(list, number);
            }
        };
    }

    public void saveOrUpdateOrganizationOriginalIn(@NotNull EnrEntrant entrant, @NotNull PersonEduDocument eduDocument, String title)
    {
        List<EnrOrganizationOriginalIn> oldList = Lists.newArrayList(), newList = Lists.newArrayList();

        EnrOrganizationOriginalIn oldOrgOriginalIn = getByNaturalId(new EnrOrganizationOriginalIn.NaturalId(entrant, eduDocument));
        EnrOrganizationOriginalIn newOrgOriginalIn = new EnrOrganizationOriginalIn(entrant, eduDocument, title);

        if (null != oldOrgOriginalIn) oldList.add(oldOrgOriginalIn);
        if (null != newOrgOriginalIn.getTitle()) newList.add(newOrgOriginalIn);

        new MergeAction.SessionMergeAction<INaturalId, EnrOrganizationOriginalIn>() {
            @Override protected INaturalId key(EnrOrganizationOriginalIn source) { return source.getNaturalId(); }
            @Override protected EnrOrganizationOriginalIn buildRow(EnrOrganizationOriginalIn source) { return source; }
            @Override protected void fill(EnrOrganizationOriginalIn target, EnrOrganizationOriginalIn source) { target.update(source, false); }
        }.merge(oldList, newList);

        flushClearAndRefresh();
    }
}