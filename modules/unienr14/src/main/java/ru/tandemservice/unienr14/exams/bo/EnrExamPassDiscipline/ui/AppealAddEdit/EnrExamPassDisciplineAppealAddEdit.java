/* $Id:$ */
package ru.tandemservice.unienr14.exams.bo.EnrExamPassDiscipline.ui.AppealAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author oleyba
 * @since 6/24/13
 */
@Configuration
public class EnrExamPassDisciplineAppealAddEdit extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .create();
    }
}