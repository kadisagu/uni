/* $Id:$ */
package ru.tandemservice.unienr14.refusal.bo.EnrEntrantRefusal;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unienr14.refusal.bo.EnrEntrantRefusal.logic.IEnrEntrantRefusalDao;
import ru.tandemservice.unienr14.refusal.bo.EnrEntrantRefusal.logic.EnrEntrantRefusalDao;

/**
 * @author oleyba
 * @since 5/20/14
 */
@Configuration
public class EnrEntrantRefusalManager extends BusinessObjectManager
{
    public static EnrEntrantRefusalManager instance()
    {
        return instance(EnrEntrantRefusalManager.class);
    }

    @Bean
    public IEnrEntrantRefusalDao dao()
    {
        return new EnrEntrantRefusalDao();
    }
}
