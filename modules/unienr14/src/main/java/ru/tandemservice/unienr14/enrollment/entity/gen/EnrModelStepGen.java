package ru.tandemservice.unienr14.enrollment.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentStepKind;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentModel;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep;
import ru.tandemservice.unienr14.enrollment.entity.EnrModelStep;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Шаг зачисления в модели
 *
 * В списке шага зачисления есть все абитуриенты (их выбранные конкурсы), которые подали документы на один из конкурсов, включенных в данный шаг.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrModelStepGen extends VersionedEntityBase
 implements INaturalIdentifiable<EnrModelStepGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.enrollment.entity.EnrModelStep";
    public static final String ENTITY_NAME = "enrModelStep";
    public static final int VERSION_HASH = -1456149568;
    private static IEntityMeta ENTITY_META;

    public static final String P_VERSION = "version";
    public static final String L_MODEL = "model";
    public static final String P_ENROLLMENT_DATE = "enrollmentDate";
    public static final String L_KIND = "kind";
    public static final String P_PERCENTAGE_AS_LONG = "percentageAsLong";
    public static final String L_BASE_REAL_STEP = "baseRealStep";
    public static final String P_AUTO_ENROLLED_MARKED = "autoEnrolledMarked";

    private int _version; 
    private EnrEnrollmentModel _model;     // Модель
    private Date _enrollmentDate;     // Дата зачисления
    private EnrEnrollmentStepKind _kind;     // Вариант работы с шагом зачисления
    private long _percentageAsLong;     // Процент от числа мест
    private EnrEnrollmentStep _baseRealStep;     // На базе шага зачисления
    private boolean _autoEnrolledMarked;     // Абитуриенты отмечены к зачислению автоматически

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public int getVersion()
    {
        return _version;
    }

    /**
     * @param version  Свойство не может быть null.
     */
    public void setVersion(int version)
    {
        dirty(_version, version);
        _version = version;
    }

    /**
     * @return Модель. Свойство не может быть null.
     */
    @NotNull
    public EnrEnrollmentModel getModel()
    {
        return _model;
    }

    /**
     * @param model Модель. Свойство не может быть null.
     */
    public void setModel(EnrEnrollmentModel model)
    {
        dirty(_model, model);
        _model = model;
    }

    /**
     * @return Дата зачисления. Свойство не может быть null.
     */
    @NotNull
    public Date getEnrollmentDate()
    {
        return _enrollmentDate;
    }

    /**
     * @param enrollmentDate Дата зачисления. Свойство не может быть null.
     */
    public void setEnrollmentDate(Date enrollmentDate)
    {
        dirty(_enrollmentDate, enrollmentDate);
        _enrollmentDate = enrollmentDate;
    }

    /**
     * @return Вариант работы с шагом зачисления. Свойство не может быть null.
     */
    @NotNull
    public EnrEnrollmentStepKind getKind()
    {
        return _kind;
    }

    /**
     * @param kind Вариант работы с шагом зачисления. Свойство не может быть null.
     */
    public void setKind(EnrEnrollmentStepKind kind)
    {
        dirty(_kind, kind);
        _kind = kind;
    }

    /**
     * Хранится со смещением в два знака для дробной части.
     *
     * @return Процент от числа мест. Свойство не может быть null.
     */
    @NotNull
    public long getPercentageAsLong()
    {
        return _percentageAsLong;
    }

    /**
     * @param percentageAsLong Процент от числа мест. Свойство не может быть null.
     */
    public void setPercentageAsLong(long percentageAsLong)
    {
        dirty(_percentageAsLong, percentageAsLong);
        _percentageAsLong = percentageAsLong;
    }

    /**
     * @return На базе шага зачисления.
     */
    public EnrEnrollmentStep getBaseRealStep()
    {
        return _baseRealStep;
    }

    /**
     * @param baseRealStep На базе шага зачисления.
     */
    public void setBaseRealStep(EnrEnrollmentStep baseRealStep)
    {
        dirty(_baseRealStep, baseRealStep);
        _baseRealStep = baseRealStep;
    }

    /**
     * @return Абитуриенты отмечены к зачислению автоматически. Свойство не может быть null.
     */
    @NotNull
    public boolean isAutoEnrolledMarked()
    {
        return _autoEnrolledMarked;
    }

    /**
     * @param autoEnrolledMarked Абитуриенты отмечены к зачислению автоматически. Свойство не может быть null.
     */
    public void setAutoEnrolledMarked(boolean autoEnrolledMarked)
    {
        dirty(_autoEnrolledMarked, autoEnrolledMarked);
        _autoEnrolledMarked = autoEnrolledMarked;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrModelStepGen)
        {
            if (withNaturalIdProperties)
            {
                setModel(((EnrModelStep)another).getModel());
                setEnrollmentDate(((EnrModelStep)another).getEnrollmentDate());
            }
            setVersion(((EnrModelStep)another).getVersion());
            setKind(((EnrModelStep)another).getKind());
            setPercentageAsLong(((EnrModelStep)another).getPercentageAsLong());
            setBaseRealStep(((EnrModelStep)another).getBaseRealStep());
            setAutoEnrolledMarked(((EnrModelStep)another).isAutoEnrolledMarked());
        }
    }

    public INaturalId<EnrModelStepGen> getNaturalId()
    {
        return new NaturalId(getModel(), getEnrollmentDate());
    }

    public static class NaturalId extends NaturalIdBase<EnrModelStepGen>
    {
        private static final String PROXY_NAME = "EnrModelStepNaturalProxy";

        private Long _model;
        private Date _enrollmentDate;

        public NaturalId()
        {}

        public NaturalId(EnrEnrollmentModel model, Date enrollmentDate)
        {
            _model = ((IEntity) model).getId();
            _enrollmentDate = org.tandemframework.core.CoreDateUtils.getDayFirstTimeMoment(enrollmentDate);
        }

        public Long getModel()
        {
            return _model;
        }

        public void setModel(Long model)
        {
            _model = model;
        }

        public Date getEnrollmentDate()
        {
            return _enrollmentDate;
        }

        public void setEnrollmentDate(Date enrollmentDate)
        {
            _enrollmentDate = org.tandemframework.core.CoreDateUtils.getDayFirstTimeMoment(enrollmentDate);
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrModelStepGen.NaturalId) ) return false;

            EnrModelStepGen.NaturalId that = (NaturalId) o;

            if( !equals(getModel(), that.getModel()) ) return false;
            if( !equals(getEnrollmentDate(), that.getEnrollmentDate()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getModel());
            result = hashCode(result, getEnrollmentDate());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getModel());
            sb.append("/");
            sb.append(getEnrollmentDate());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrModelStepGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrModelStep.class;
        }

        public T newInstance()
        {
            return (T) new EnrModelStep();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "version":
                    return obj.getVersion();
                case "model":
                    return obj.getModel();
                case "enrollmentDate":
                    return obj.getEnrollmentDate();
                case "kind":
                    return obj.getKind();
                case "percentageAsLong":
                    return obj.getPercentageAsLong();
                case "baseRealStep":
                    return obj.getBaseRealStep();
                case "autoEnrolledMarked":
                    return obj.isAutoEnrolledMarked();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "version":
                    obj.setVersion((Integer) value);
                    return;
                case "model":
                    obj.setModel((EnrEnrollmentModel) value);
                    return;
                case "enrollmentDate":
                    obj.setEnrollmentDate((Date) value);
                    return;
                case "kind":
                    obj.setKind((EnrEnrollmentStepKind) value);
                    return;
                case "percentageAsLong":
                    obj.setPercentageAsLong((Long) value);
                    return;
                case "baseRealStep":
                    obj.setBaseRealStep((EnrEnrollmentStep) value);
                    return;
                case "autoEnrolledMarked":
                    obj.setAutoEnrolledMarked((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "version":
                        return true;
                case "model":
                        return true;
                case "enrollmentDate":
                        return true;
                case "kind":
                        return true;
                case "percentageAsLong":
                        return true;
                case "baseRealStep":
                        return true;
                case "autoEnrolledMarked":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "version":
                    return true;
                case "model":
                    return true;
                case "enrollmentDate":
                    return true;
                case "kind":
                    return true;
                case "percentageAsLong":
                    return true;
                case "baseRealStep":
                    return true;
                case "autoEnrolledMarked":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "version":
                    return Integer.class;
                case "model":
                    return EnrEnrollmentModel.class;
                case "enrollmentDate":
                    return Date.class;
                case "kind":
                    return EnrEnrollmentStepKind.class;
                case "percentageAsLong":
                    return Long.class;
                case "baseRealStep":
                    return EnrEnrollmentStep.class;
                case "autoEnrolledMarked":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrModelStep> _dslPath = new Path<EnrModelStep>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrModelStep");
    }
            

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelStep#getVersion()
     */
    public static PropertyPath<Integer> version()
    {
        return _dslPath.version();
    }

    /**
     * @return Модель. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelStep#getModel()
     */
    public static EnrEnrollmentModel.Path<EnrEnrollmentModel> model()
    {
        return _dslPath.model();
    }

    /**
     * @return Дата зачисления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelStep#getEnrollmentDate()
     */
    public static PropertyPath<Date> enrollmentDate()
    {
        return _dslPath.enrollmentDate();
    }

    /**
     * @return Вариант работы с шагом зачисления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelStep#getKind()
     */
    public static EnrEnrollmentStepKind.Path<EnrEnrollmentStepKind> kind()
    {
        return _dslPath.kind();
    }

    /**
     * Хранится со смещением в два знака для дробной части.
     *
     * @return Процент от числа мест. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelStep#getPercentageAsLong()
     */
    public static PropertyPath<Long> percentageAsLong()
    {
        return _dslPath.percentageAsLong();
    }

    /**
     * @return На базе шага зачисления.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelStep#getBaseRealStep()
     */
    public static EnrEnrollmentStep.Path<EnrEnrollmentStep> baseRealStep()
    {
        return _dslPath.baseRealStep();
    }

    /**
     * @return Абитуриенты отмечены к зачислению автоматически. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelStep#isAutoEnrolledMarked()
     */
    public static PropertyPath<Boolean> autoEnrolledMarked()
    {
        return _dslPath.autoEnrolledMarked();
    }

    public static class Path<E extends EnrModelStep> extends EntityPath<E>
    {
        private PropertyPath<Integer> _version;
        private EnrEnrollmentModel.Path<EnrEnrollmentModel> _model;
        private PropertyPath<Date> _enrollmentDate;
        private EnrEnrollmentStepKind.Path<EnrEnrollmentStepKind> _kind;
        private PropertyPath<Long> _percentageAsLong;
        private EnrEnrollmentStep.Path<EnrEnrollmentStep> _baseRealStep;
        private PropertyPath<Boolean> _autoEnrolledMarked;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelStep#getVersion()
     */
        public PropertyPath<Integer> version()
        {
            if(_version == null )
                _version = new PropertyPath<Integer>(EnrModelStepGen.P_VERSION, this);
            return _version;
        }

    /**
     * @return Модель. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelStep#getModel()
     */
        public EnrEnrollmentModel.Path<EnrEnrollmentModel> model()
        {
            if(_model == null )
                _model = new EnrEnrollmentModel.Path<EnrEnrollmentModel>(L_MODEL, this);
            return _model;
        }

    /**
     * @return Дата зачисления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelStep#getEnrollmentDate()
     */
        public PropertyPath<Date> enrollmentDate()
        {
            if(_enrollmentDate == null )
                _enrollmentDate = new PropertyPath<Date>(EnrModelStepGen.P_ENROLLMENT_DATE, this);
            return _enrollmentDate;
        }

    /**
     * @return Вариант работы с шагом зачисления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelStep#getKind()
     */
        public EnrEnrollmentStepKind.Path<EnrEnrollmentStepKind> kind()
        {
            if(_kind == null )
                _kind = new EnrEnrollmentStepKind.Path<EnrEnrollmentStepKind>(L_KIND, this);
            return _kind;
        }

    /**
     * Хранится со смещением в два знака для дробной части.
     *
     * @return Процент от числа мест. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelStep#getPercentageAsLong()
     */
        public PropertyPath<Long> percentageAsLong()
        {
            if(_percentageAsLong == null )
                _percentageAsLong = new PropertyPath<Long>(EnrModelStepGen.P_PERCENTAGE_AS_LONG, this);
            return _percentageAsLong;
        }

    /**
     * @return На базе шага зачисления.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelStep#getBaseRealStep()
     */
        public EnrEnrollmentStep.Path<EnrEnrollmentStep> baseRealStep()
        {
            if(_baseRealStep == null )
                _baseRealStep = new EnrEnrollmentStep.Path<EnrEnrollmentStep>(L_BASE_REAL_STEP, this);
            return _baseRealStep;
        }

    /**
     * @return Абитуриенты отмечены к зачислению автоматически. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelStep#isAutoEnrolledMarked()
     */
        public PropertyPath<Boolean> autoEnrolledMarked()
        {
            if(_autoEnrolledMarked == null )
                _autoEnrolledMarked = new PropertyPath<Boolean>(EnrModelStepGen.P_AUTO_ENROLLED_MARKED, this);
            return _autoEnrolledMarked;
        }

        public Class getEntityClass()
        {
            return EnrModelStep.class;
        }

        public String getEntityName()
        {
            return "enrModelStep";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
