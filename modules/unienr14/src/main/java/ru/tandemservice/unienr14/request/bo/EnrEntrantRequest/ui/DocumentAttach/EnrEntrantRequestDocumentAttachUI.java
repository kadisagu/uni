/* $Id:$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.DocumentAttach;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.commonbase.base.util.wizard.SimpleWizardUIPresenter;
//import org.tandemframework.shared.person.base.entity.PersonDocument;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import org.tandemframework.shared.person.catalog.entity.PersonDocumentType;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.SingleSelectTextModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrantBaseDocument.EnrEntrantBaseDocumentManager;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrantBaseDocument.util.EnrEntrantDocumentViewWrapper;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrantEduDocument.EnrEntrantEduDocumentManager;
import ru.tandemservice.unienr14.entrant.bo.EnrSubmittedEduDocument.EnrSubmittedEduDocumentManager;
import ru.tandemservice.unienr14.entrant.entity.*;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.EnrEntrantRequestManager;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.EnrEntrantRequestAddWizardWizardUI;
import ru.tandemservice.unienr14.request.bo.EnrRequestedCompetition.EnrRequestedCompetitionManager;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequestAttachment;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.IEnrEntrantRequestAttachable;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignEntrantDocument;

import java.util.*;

/**
 * @author oleyba
 * @since 5/4/13
 */
@Input({
    @Bind(key = "requestId", binding = "request.id", required = true)
})
public class EnrEntrantRequestDocumentAttachUI extends UIPresenter
{

    public boolean isInWizard() {
        return SimpleWizardUIPresenter.isInWizard(this) || EnrEntrantRequestAddWizardWizardUI.isInWizard(this);
    }

    private static final IdentifiableWrapper<IEntity> COPY_WRAPPER = new IdentifiableWrapper<>(0L, "копия");
    private static final IdentifiableWrapper<IEntity> ORIGINAL_WRAPPER = new IdentifiableWrapper<>(1L, "оригинал");

    private EnrEntrantRequest request = new EnrEntrantRequest();
    private PersonEduDocument oldEduDocument;

    private StaticListDataSource<ViewWrapper> documentDataSource;

    private ISelectModel attachedOptionListWithOriginalAndCopy;
    private ISelectModel attachedOptionListWithOriginal;
    private ISelectModel attachedOptionListWithCopy;
    private ISelectModel attachedOptionListEmpty;

    private List<Long> documentTypeCopyAllowedIds;
    private List<Long> documentTypeOriginalAllowedIds;

    private String orgOriginalInTitle;
    private EnrOrganizationOriginalIn orgOriginalIn;
    private EnrEntrantOriginalDocumentStatus originalDocStatus;
    private boolean eduDocOriginalHandedIn;
    private SingleSelectTextModel _organizationOriginalInModel = EnrSubmittedEduDocumentManager.instance().dao().getOrganizationOriginalInDS();

    private EnrRequestedCompetition _requestedCompetition;
    private boolean needReqCompForOriginal = false;

    @Override
    public void onComponentActivate()
    {
        setRequest(IUniBaseDao.instance.get().getNotNull(EnrEntrantRequest.class, getRequest().getId()));
        setNeedReqCompForOriginal(EnrEntrantEduDocumentManager.instance().dao().isNeedReqCompForOriginal(getEntrant(), getEduDocument().getId()));
        setOldEduDocument(getRequest().getEduDocument());
        setAttachedOptionListWithOriginalAndCopy(new LazySimpleSelectModel<>(ImmutableList.of(
                COPY_WRAPPER,
                ORIGINAL_WRAPPER)));
        setAttachedOptionListWithOriginal(new LazySimpleSelectModel<>(ImmutableList.of(ORIGINAL_WRAPPER)));
        setAttachedOptionListWithCopy(new LazySimpleSelectModel<>(ImmutableList.of(COPY_WRAPPER)));
        setAttachedOptionListEmpty(new LazySimpleSelectModel<>(Collections.emptyList()));

    }

    @Override
    public void onComponentRefresh()
    {
        prepareDocumentDataSource();

        originalDocStatus = DataAccessServices.dao().getByNaturalId(new EnrEntrantOriginalDocumentStatus.NaturalId(getEntrant(), getEduDocument()));
        if (null != originalDocStatus)
            eduDocOriginalHandedIn = originalDocStatus.isOriginalDocumentHandedIn();

        orgOriginalIn = DataAccessServices.dao().getByNaturalId(new EnrOrganizationOriginalIn.NaturalId(getEntrant(), getEduDocument()));
        orgOriginalInTitle = orgOriginalIn == null ? null : orgOriginalIn.getTitle();

        if(null != originalDocStatus)
            setRequestedCompetition(originalDocStatus.getRequestedCompetition());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        dataSource.put(EnrEntrantBaseDocumentManager.BIND_PERSON, getEntrant().getPerson());
        dataSource.put(EnrEntrantBaseDocumentManager.BIND_REQUEST_TYPE, getRequest().getType());
        dataSource.put(EnrEntrantBaseDocumentManager.BIND_RECEIVE_EDU_LEVEL_FIRST, getRequest().isReceiveEduLevelFirst());
        if(EnrRequestedCompetitionManager.DS_REQ_COMP_ORIGINAL.equals(dataSource.getName()))
        {
            dataSource.put(EnrRequestedCompetitionManager.BIND_EDU_DOC_ID, getEduDocument().getId());
            dataSource.put(EnrRequestedCompetitionManager.BIND_ENTRANT, getEntrant());
        }
    }

    @SuppressWarnings("unchecked")
    public void onClickApply() {
        saveFormData();
        EnrEntrantRequestManager.instance().dao().doUpdateEnrReqCompEnrollAvailableAndEduDocForEntrant(getEntrant());
        deactivate(
            new SimpleWizardUIPresenter.ReturnBuilder(getConfig(), getRequest().getId())
            .buildMap()
        );
    }

    // presenter

    public EnrEntrant getEntrant() {
        return getRequest().getEntrant();
    }

    public PersonEduDocument getEduDocument()
    {
        return getRequest().getEduDocument();
    }

    // utils

    public boolean isShowReqCompSelect()
    {
        return isEduDocOriginalHandedIn() && isNeedReqCompForOriginal();
    }

    public boolean isCopyAttached(Object selectedOption) {
        return COPY_WRAPPER.equals(selectedOption);
    }

    protected boolean isOriginalAttached(Object selectedOption) {
        return ORIGINAL_WRAPPER.equals(selectedOption);
    }

    public boolean isDisabledEduDocOriginalHandedIn()
    {
        return originalDocStatus != null && originalDocStatus.isOriginalDocumentHandedIn();
    }

    public ISelectModel getCurrentDocumentOptionList()
    {

        return getDocumentOptionList(((IEnrEntrantRequestAttachable)getDocumentDataSource().getCurrentEntity().getEntity()).getDocumentType().getId());
    }

    private ISelectModel getDocumentOptionList(Long documentTypeId)
    {
        boolean allowCopy = documentTypeCopyAllowedIds.contains(documentTypeId);
        boolean allowOriginal = documentTypeOriginalAllowedIds.contains(documentTypeId);
        return allowCopy && allowOriginal ? getAttachedOptionListWithOriginalAndCopy() : (allowCopy ? getAttachedOptionListWithCopy() : allowOriginal ? getAttachedOptionListWithOriginal() : getAttachedOptionListEmpty());
    }

    public boolean isCurrentDocumentDisabled()
    {
        Map<Long, Object> attachedMap = ((IValueMapHolder) getDocumentDataSource().getColumn("attached")).getValueMap();
        return !(Boolean) attachedMap.get(getDocumentDataSource().getCurrentEntity().getId());
    }

    @SuppressWarnings("unchecked")
    protected void prepareDocumentDataSource()
    {
        if (getDocumentDataSource() == null) {
            setDocumentDataSource(new StaticListDataSource<ViewWrapper>());

            getDocumentDataSource().addColumn(new SimpleColumn("Тип документа", EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_DOCUMENT_TYPE + ".title").setClickable(false).setOrderable(false));
            getDocumentDataSource().addColumn(new SimpleColumn("Серия и номер", EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_SERIA_AND_NUMBER).setClickable(false).setOrderable(false));
            getDocumentDataSource().addColumn(new SimpleColumn("Дата выдачи", EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_ISSUANCE_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false));
            getDocumentDataSource().addColumn(new SimpleColumn("Информация о документе", EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_INFO).setClickable(false).setOrderable(false));
            getDocumentDataSource().addColumn(new SimpleColumn("Дата добавления", EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_REG_DATE, DateFormatter.DATE_FORMATTER_WITH_TIME).setClickable(false).setOrderable(false));
            getDocumentDataSource().addColumn(new BlockColumn<>("attached", "Приложен").setClickable(false).setOrderable(false));
            getDocumentDataSource().addColumn(new BlockColumn<>("attachment", "К заявлению\nприлагается").setClickable(false).setOrderable(false));
        }

        ArrayList<ViewWrapper> documents = new ArrayList<>();

        for (EnrEntrantBaseDocument document : IUniBaseDao.instance.get().getList(EnrEntrantBaseDocument.class, EnrEntrantBaseDocument.entrant(), getEntrant(), EnrEntrantBaseDocument.registrationDate().s())) {
            documents.add(new EnrEntrantDocumentViewWrapper(document));
        }
        Collections.sort(documents, new EntityComparator<>(new EntityOrder(EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_DOCUMENT_TYPE + ".priority"), new EntityOrder(EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_REG_DATE), new EntityOrder("id")));

        List<Long> documentTypeCopyAllowedIds = Lists.newArrayList();
        List<Long> documentTypeOriginalAllowedIds = Lists.newArrayList();
        for (EnrCampaignEntrantDocument document: IUniBaseDao.instance.get().getList(EnrCampaignEntrantDocument.class, EnrCampaignEntrantDocument.enrollmentCampaign(), getEntrant().getEnrollmentCampaign()))
        {
            if (document.isCopyCanBeAccepted())
                documentTypeCopyAllowedIds.add(document.getDocumentType().getId());
            if (document.isOriginalCanBeAccepted())
                documentTypeOriginalAllowedIds.add(document.getDocumentType().getId());
        }

        this.documentTypeCopyAllowedIds = documentTypeCopyAllowedIds;
        this.documentTypeOriginalAllowedIds = documentTypeOriginalAllowedIds;

        final Map<Long, Object> attachmentMap = ((IValueMapHolder) getDocumentDataSource().getColumn("attachment")).getValueMap();
        attachmentMap.clear();
        for (EnrEntrantRequestAttachment attachment : IUniBaseDao.instance.get().getList(EnrEntrantRequestAttachment.class, EnrEntrantRequestAttachment.entrantRequest(), getRequest())) {
            List<IdentifiableWrapper<IEntity>> wrappers = getDocumentOptionList(attachment.getDocument().getDocumentType().getId()).findValues("").getObjects();
            IdentifiableWrapper<IEntity> attachWrap = attachment.isOriginalHandedIn() ? ORIGINAL_WRAPPER : COPY_WRAPPER;
            if(wrappers.contains(attachWrap))
                attachmentMap.put(attachment.getDocument().getId(), attachWrap);
            else if(!wrappers.isEmpty())
                attachmentMap.put(attachment.getDocument().getId(), wrappers.get(0));
        }

        final Map<Long, Object> attachedMap = ((IValueMapHolder) getDocumentDataSource().getColumn("attached")).getValueMap();
        attachedMap.clear();
        for(IEntity document : documents)
        {
            boolean contains = attachmentMap.containsKey(document.getId());
            attachedMap.put(document.getId(), contains);
            if(!contains)
            {
                EnrEntrantDocumentViewWrapper doc = (EnrEntrantDocumentViewWrapper) document;
                List<IdentifiableWrapper<IEntity>> wrappers = getDocumentOptionList(((PersonDocumentType)doc.getProperty(EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_DOCUMENT_TYPE)).getId()).findValues("").getObjects();
                if(!wrappers.isEmpty())
                {
                    attachmentMap.put(doc.getId(), wrappers.get(0));
                }
            }
        }
        getDocumentDataSource().setupRows(documents);
    }

    @SuppressWarnings("unchecked")
    protected void saveFormData()
    {
        if (TwinComboDataSourceHandler.NO_ID.equals(getRequest().getEntrant().getEnrollmentCampaign().getSettings().getAccountingRulesOrigEduDoc()) && !getOldEduDocument().equals(getRequest().getEduDocument()))
        {
            boolean hasErr = EnrEntrantRequestManager.instance().dao().checkOrigDocuments(getOldEduDocument(), getRequest().getEduDocument(), getRequest());

            if (hasErr)
            {
                throw new ApplicationException("У приложенного или прикладываемого документа об образовании уже принят оригинал.");
            }
        }

        Map<Long, Boolean> resultMap = new HashMap<>();
        final Map<Long, Object> attachmentMap = ((IValueMapHolder) getDocumentDataSource().getColumn("attachment")).getValueMap();

        final Map<Long, Object> attachedMap = ((IValueMapHolder) getDocumentDataSource().getColumn("attached")).getValueMap();

        for(Map.Entry<Long, Object> entry : attachedMap.entrySet())
        {
            if ((Boolean) entry.getValue())
                resultMap.put(entry.getKey(), isOriginalAttached(attachmentMap.get(entry.getKey())));
        }
        if (isEduDocOriginalHandedIn())
            setOrgOriginalInTitle(null);

        EnrEntrantRequestManager.instance().dao().doUpdateAttachments(getRequest(), resultMap, isInWizard() && isEduDocOriginalHandedIn(), getOrgOriginalInTitle(), getRequestedCompetition(), getOldEduDocument());
    }

    // getters and setters

    public EnrEntrantRequest getRequest()
    {
        return request;
    }

    public void setRequest(EnrEntrantRequest request)
    {
        this.request = request;
    }

    public StaticListDataSource<ViewWrapper> getDocumentDataSource()
    {
        return documentDataSource;
    }

    public void setDocumentDataSource(StaticListDataSource<ViewWrapper> documentDataSource)
    {
        this.documentDataSource = documentDataSource;
    }

    public ISelectModel getAttachedOptionListWithOriginalAndCopy()
    {
        return attachedOptionListWithOriginalAndCopy;
    }

    public void setAttachedOptionListWithOriginalAndCopy(ISelectModel attachedOptionListWithOriginalAndCopy)
    {
        this.attachedOptionListWithOriginalAndCopy = attachedOptionListWithOriginalAndCopy;
    }

    public ISelectModel getAttachedOptionListWithOriginal(){ return attachedOptionListWithOriginal; }

    public void setAttachedOptionListWithOriginal(ISelectModel attachedOptionListWithOriginal){ this.attachedOptionListWithOriginal = attachedOptionListWithOriginal; }

    public ISelectModel getAttachedOptionListWithCopy()
    {
        return attachedOptionListWithCopy;
    }

    public void setAttachedOptionListWithCopy(ISelectModel attachedOptionListWithCopy)
    {
        this.attachedOptionListWithCopy = attachedOptionListWithCopy;
    }

    public ISelectModel getAttachedOptionListEmpty()
    {
        return attachedOptionListEmpty;
    }

    public void setAttachedOptionListEmpty(ISelectModel attachedOptionListEmpty)
    {
        this.attachedOptionListEmpty = attachedOptionListEmpty;
    }

    public EnrEntrantOriginalDocumentStatus getOriginalDocStatus()
    {
        return originalDocStatus;
    }

    public void setOriginalDocStatus(EnrEntrantOriginalDocumentStatus originalDocStatus)
    {
        this.originalDocStatus = originalDocStatus;
    }

    public EnrOrganizationOriginalIn getOrgOriginalIn()
    {
        return orgOriginalIn;
    }

    public void setOrgOriginalIn(EnrOrganizationOriginalIn orgOriginalIn)
    {
        this.orgOriginalIn = orgOriginalIn;
    }

    public String getOrgOriginalInTitle()
    {
        return orgOriginalInTitle;
    }

    public void setOrgOriginalInTitle(String orgOriginalInTitle)
    {
        this.orgOriginalInTitle = orgOriginalInTitle;
    }

    public boolean isEduDocOriginalHandedIn()
    {
        return eduDocOriginalHandedIn;
    }

    public void setEduDocOriginalHandedIn(boolean eduDocOriginalHandedIn)
    {
        this.eduDocOriginalHandedIn = eduDocOriginalHandedIn;
    }

    public SingleSelectTextModel getOrganizationOriginalInModel()
    {
        return _organizationOriginalInModel;
    }

    public void setOrganizationOriginalInModel(SingleSelectTextModel organizationOriginalInModel)
    {
        _organizationOriginalInModel = organizationOriginalInModel;
    }

    public EnrRequestedCompetition getRequestedCompetition()
    {
        return _requestedCompetition;
    }

    public void setRequestedCompetition(EnrRequestedCompetition requestedCompetition)
    {
        _requestedCompetition = requestedCompetition;
    }

    public PersonEduDocument getOldEduDocument()
    {
        return oldEduDocument;
    }

    public void setOldEduDocument(PersonEduDocument oldEduDocument)
    {
        this.oldEduDocument = oldEduDocument;
    }

    public boolean isNeedReqCompForOriginal()
    {
        return needReqCompForOriginal;
    }

    public void setNeedReqCompForOriginal(boolean needReqCompForOriginal)
    {
        this.needReqCompForOriginal = needReqCompForOriginal;
    }
}
