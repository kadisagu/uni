/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EnrollmentResultsByOrgUnitsAdd.logic;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.RtfRowIntercepterRawText;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.MergeType;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.IRtfRowIntercepter;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfText;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrExamPassFormCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrExamVariant;
import ru.tandemservice.unienr14.competition.entity.EnrExamVariantPassForm;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EnrollmentResultsByOrgUnitsAdd.EnrReportEnrollmentResultsByOrgUnitsAddUI;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportStageSelector;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportUtil;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.RtfBackslashScreener;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder;
import ru.tandemservice.unienr14.report.entity.EnrReportEnrollmentResultsByOrgUnits;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 21.07.2014
 */
public class EnrReportEnrollmentResultsByOrgUnitsDao extends UniBaseDao implements IEnrReportEnrollmentResultsByOrgUnitsDao {
    @Override
    public long createReport(EnrReportEnrollmentResultsByOrgUnitsAddUI model) {

        EnrReportEnrollmentResultsByOrgUnits report = new EnrReportEnrollmentResultsByOrgUnits();
        report.setFormingDate(new Date());

        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateSelector().getDateFrom());
        report.setDateTo(model.getDateSelector().getDateTo());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, EnrReportEnrollmentResultsByOrgUnits.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, EnrReportEnrollmentResultsByOrgUnits.P_PROGRAM_FORM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPETITION_TYPE, EnrReportEnrollmentResultsByOrgUnits.P_COMPETITION_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, EnrReportEnrollmentResultsByOrgUnits.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, EnrReportEnrollmentResultsByOrgUnits.P_FORMATIVE_ORG_UNIT, "fullTitle");

        if (model.getParallelSelector().isParallelActive())
            report.setParallel(model.getParallelSelector().getParallel().getTitle());

        DatabaseFile content = new DatabaseFile();

        content.setContent(buildReport(model));
        content.setFilename("EnrReportEnrollmentResultsByOrgUnits.rtf");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        save(content);

        report.setContent(content);
        save(report);

        return report.getId();
    }

    private byte[] buildReport(EnrReportEnrollmentResultsByOrgUnitsAddUI model)
    {
        // rtf
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(EnrScriptItem.class, EnrScriptItemCodes.REPORT_ENROLLMENT_RESULTS_BY_ORG_UNITS);
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());

        // Фильтры
        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder()
                .notArchiveOnly()
                .filter(model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo())
                .filter(model.getEnrollmentCampaign());
        filterAddon.applyFilters(requestedCompDQL, requestedCompDQL.competition());

        if (model.getParallelSelector().isParallelActive())
        {
            if (model.getParallelSelector().isParallelOnly())
                requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.TRUE)));
            if (model.getParallelSelector().isSkipParallel())
                requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.FALSE)));
        }
        EnrReportStageSelector.docAcceptanceStageFilter().applyFilter(requestedCompDQL, "reqComp");

        // Выбранные конкурсы
        requestedCompDQL.column(property("reqComp"));
        List<EnrRequestedCompetition> fromQueryReqComps = getList(requestedCompDQL);

        // ВВИ
        requestedCompDQL
                .joinEntity("reqComp", DQLJoinType.inner, EnrChosenEntranceExam.class, "vvi", eq(property(EnrChosenEntranceExam.requestedCompetition().fromAlias("vvi")), property("reqComp")))
                .resetColumns()
                .where(isNotNull(property(EnrChosenEntranceExam.maxMarkForm().fromAlias("vvi"))))
                .column(property("vvi"));
        List<EnrChosenEntranceExam> fromQueryExamList = getList(requestedCompDQL);
        Map<EnrRequestedCompetition, Set<EnrChosenEntranceExam>>  examMap = SafeMap.get(HashSet.class);
        for (EnrChosenEntranceExam exam : fromQueryExamList) {
            examMap.get(exam.getRequestedCompetition()).add(exam);
        }


        Set<OrgUnit> orgUnitSet = new HashSet<>();
        // Карта с данными
        Comparator<OrgUnit> topDependentOrgUnitComparator = new Comparator<OrgUnit>() {
            @Override
            public int compare(OrgUnit o1, OrgUnit o2) {
                int i = Boolean.compare(null == o1.getParent(), null == o2.getParent());
                if (0 != i) { return -i; }

                return o1.getPrintTitle().compareToIgnoreCase(o2.getPrintTitle());
            }
        };
        Map<OrgUnit, Map<OrgUnit, Set<EnrRequestedCompetition>>> dataMap = new TreeMap<>(topDependentOrgUnitComparator);
        for (EnrRequestedCompetition reqComp : fromQueryReqComps) {
            OrgUnit orgUnit = reqComp.getCompetition().getProgramSetOrgUnit().getOrgUnit().getInstitutionOrgUnit().getOrgUnit();
            Map<OrgUnit, Set<EnrRequestedCompetition>> orgUnitSetMap = SafeMap.safeGet(dataMap, orgUnit, TreeMap.class, topDependentOrgUnitComparator);
            Set<EnrRequestedCompetition> requestedCompetitionSet = SafeMap.safeGet(orgUnitSetMap, reqComp.getCompetition().getProgramSetOrgUnit().getEffectiveFormativeOrgUnit(), HashSet.class);
            requestedCompetitionSet.add(reqComp);
            orgUnitSet.add(orgUnit);
        }

        // Направления на формирующем подразделении
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EnrCompetition.class, "comp")
                .joinPath(DQLJoinType.inner, EnrCompetition.programSetOrgUnit().orgUnit().institutionOrgUnit().orgUnit().fromAlias("comp"), "orgUnit")
                .where(in(property("orgUnit"), orgUnitSet))
                .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().enrollmentCampaign().fromAlias("comp")), value(model.getEnrollmentCampaign())))
                .where(eq(property(EnrCompetition.requestType().fromAlias("comp")), value((EnrRequestType)model.getCompetitionFilterAddon().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE).getValue())))
                .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().programForm().fromAlias("comp")), value((EduProgramForm) model.getCompetitionFilterAddon().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM).getValue())))
                .column(property("comp"));


        List<EnrCompetition> fromQueryCompetitions = getList(builder);
        Map<OrgUnit, Set<EnrProgramSetOrgUnit>> programSetOrgUnitsMap = SafeMap.get(HashSet.class);
        for (EnrCompetition competition : fromQueryCompetitions) {
            programSetOrgUnitsMap.get(competition.getProgramSetOrgUnit().getEffectiveFormativeOrgUnit()).add(competition.getProgramSetOrgUnit());
        };

        // Направления на формирующем подразделении у которых есть ЕГЭ во вступ.исп
        builder
                .joinPath(DQLJoinType.inner, EnrCompetition.examSetVariant().fromAlias("comp"), "examSet")
                .joinEntity("examSet", DQLJoinType.inner, EnrExamVariant.class, "examVariant", eq(property(EnrExamVariant.examSetVariant().fromAlias("examVariant")), property("examSet")))
                .joinEntity("examVariant", DQLJoinType.inner, EnrExamVariantPassForm.class, "passForm", eq(property(EnrExamVariantPassForm.examVariant().fromAlias("passForm")), property("examVariant")))
                .where(eq(property(EnrExamVariantPassForm.passForm().code().fromAlias("passForm")), value(EnrExamPassFormCodes.STATE_EXAM)));

        List<EnrCompetition> fromQueryStateExamOnlyCompetitions = getList(builder);
        Map<OrgUnit, Set<EnrProgramSetOrgUnit>> programSetOrgUnitsWithStateExamInCompetitionsMap = SafeMap.get(HashSet.class);
        for (EnrCompetition competition : fromQueryStateExamOnlyCompetitions) {
            programSetOrgUnitsWithStateExamInCompetitionsMap.get(competition.getProgramSetOrgUnit().getEffectiveFormativeOrgUnit()).add(competition.getProgramSetOrgUnit());
        }

        // Формируем таблицу для метки H
        List<String[]> hTable = new ArrayList<>();
        hTable.add(new String[]{"Заявления с", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getDateSelector().getDateFrom())});
        hTable.add(new String[]{"Заявления по", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getDateSelector().getDateTo())});
        List<EnrCompetitionType> competitionTypes = (List) model.getCompetitionFilterAddon().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_COMPETITION_TYPE).getValue();
        if (competitionTypes != null && !competitionTypes.isEmpty())
        {
            hTable.add(new String[]{"Вид приема", new RtfBackslashScreener().screenBackslashes(UniStringUtils.join(competitionTypes, EnrCompetitionType.printTitle().s(), "\\line "))});
        }

        if (model.getParallelSelector().isParallelActive())
        {
            hTable.add(new String[] {"Поступающие параллельно", model.getParallelSelector().getParallel().getTitle()});
        }

        new RtfInjectModifier()
                .put("academyTitle", TopOrgUnit.getInstance().getShortTitle())
                .put("year", DateFormatter.DATE_FORMATTER_JUST_YEAR.format(new Date()))
                .modify(document);

        RtfTable tTableElement = (RtfTable) UniRtfUtil.findElement(document.getElementList(), "T");
        RtfTable hTableElement = (RtfTable) UniRtfUtil.findElement(document.getElementList(), "H");

        document.getElementList().remove(tTableElement);
        document.getElementList().remove(hTableElement);

        boolean firstIteration = true;
        // Заполняем документ
        for (Map.Entry<OrgUnit, Map<OrgUnit, Set<EnrRequestedCompetition>>> filialEntry : dataMap.entrySet()) {


            if (!firstIteration)
            {
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
            }
            firstIteration = false;

            List<String[]> tTable = new ArrayList<>();
            document.addElement(hTableElement.getClone());
            document.addElement(tTableElement.getClone());
            new RtfInjectModifier()
                    .put("filial", filialEntry.getKey().getShortTitleWithTopEmphasized())
                    .put("programForm", ((EduProgramForm) model.getCompetitionFilterAddon().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM).getValue()).getTitle())
                    .put("requestType", ((EnrRequestType) model.getCompetitionFilterAddon().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE).getValue()).getTitle())
                    .modify(document);

            List<Integer> subjectsTotal = new ArrayList<>();
            List<Integer> subjectsWithEgeTotal = new ArrayList<>();
            List<Integer> planKCPSum = new ArrayList<>();
            List<Integer> planContractSum = new ArrayList<>();
            List<EnrEntrantRequest> uniBudgetRequestsTotal = new ArrayList<>();
            List<EnrEntrantRequest> uniBudgetRequestsWithEge = new ArrayList<>();
            List<EnrRequestedCompetition> uniBudgetEnrolledTotal = new ArrayList<>();
            List<EnrRequestedCompetition> uniBudgetEnrolledAnotherSettlement = new ArrayList<>();
            List<EnrRequestedCompetition> uniBudgetEnrolledCountryside = new ArrayList<>();
            List<EnrRequestedCompetition> uniBudgetEnrolledWithEge = new ArrayList<>();
            List<EnrRequestedCompetition> uniBudgetEnrolledWithEgeAnotherSettlement = new ArrayList<>();
            List<EnrRequestedCompetition> uniBudgetEnrolledWithEgeCountryside = new ArrayList<>();
            List<EnrRequestedCompetition> uniBudgetEnrolledJust1Ege = new ArrayList<>();
            List<EnrRequestedCompetition> uniBudgetEnrolledAtLeast2Ege = new ArrayList<>();
            List<EnrRequestedCompetition> uniBudgetEnrolledEgeOnly = new ArrayList<>();
            List<EnrEntrantRequest> uniContractRequestsTotal = new ArrayList<>();
            List<EnrEntrantRequest> uniContractRequestsWithEge = new ArrayList<>();
            List<EnrRequestedCompetition> uniContractEnrolledTotal = new ArrayList<>();
            List<EnrRequestedCompetition> uniContractEnrolledAnotherSettlement = new ArrayList<>();
            List<EnrRequestedCompetition> uniContractEnrolledCountryside = new ArrayList<>();
            List<EnrRequestedCompetition> uniContractEnrolledWithEge = new ArrayList<>();
            List<EnrRequestedCompetition> uniContractEnrolledWithEgeAnotherSettlement = new ArrayList<>();
            List<EnrRequestedCompetition> uniContractEnrolledWithEgeCountryside = new ArrayList<>();
            List<EnrRequestedCompetition> uniContractEnrolledJust1Ege = new ArrayList<>();
            List<EnrRequestedCompetition> uniContractEnrolledAtLeast2Ege = new ArrayList<>();
            List<EnrRequestedCompetition> uniContractEnrolledEgeOnly = new ArrayList<>();


            for (Map.Entry<OrgUnit, Set<EnrRequestedCompetition>> formativeEntry : filialEntry.getValue().entrySet()) {
                OrgUnit formative = formativeEntry.getKey();
                String[] title = new String[]{formative.equals(filialEntry.getKey()) ? formative.getShortTitleWithTopEmphasized() : formative.getPrintTitle(),
                        "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                        "", "", "", "", "", "", "", "", "", "", "", "", ""
                };
                tTable.add(title);
                Set<EduProgramSubject> subjectsWithEgeOnFormative = new HashSet<>();
                Set<EduProgramSubject> subjects = new HashSet<>();
                Integer planKCP = 0;
                Integer planContract = 0;

                for (EnrProgramSetOrgUnit programSetOrgUnit : programSetOrgUnitsMap.get(formative)) {
                    if (programSetOrgUnit.getOrgUnit().getInstitutionOrgUnit().getOrgUnit().equals(filialEntry.getKey())) {
                        subjects.add(programSetOrgUnit.getProgramSet().getProgramSubject());
                        planKCP += programSetOrgUnit.getMinisterialPlan();
                        planContract += programSetOrgUnit.getContractPlan();
                    }
                }

                for (EnrProgramSetOrgUnit programSetOrgUnit : programSetOrgUnitsWithStateExamInCompetitionsMap.get(formative)) {
                    if (programSetOrgUnit.getOrgUnit().getInstitutionOrgUnit().getOrgUnit().equals(filialEntry.getKey()))
                        subjectsWithEgeOnFormative.add(programSetOrgUnit.getProgramSet().getProgramSubject());
                }




                Set<EnrEntrantRequest> budgetRequestsTotal = new HashSet<>();
                Set<EnrEntrantRequest> budgetRequestsWithEge = new HashSet<>();
                Set<EnrRequestedCompetition> budgetEnrolledTotal = new HashSet<>();
                Set<EnrRequestedCompetition> budgetEnrolledAnotherSettlement = new HashSet<>();
                Set<EnrRequestedCompetition> budgetEnrolledCountryside = new HashSet<>();
                Set<EnrRequestedCompetition> budgetEnrolledWithEge = new HashSet<>();
                Set<EnrRequestedCompetition> budgetEnrolledWithEgeAnotherSettlement = new HashSet<>();
                Set<EnrRequestedCompetition> budgetEnrolledWithEgeCountryside = new HashSet<>();
                Set<EnrRequestedCompetition> budgetEnrolledJust1Ege = new HashSet<>();
                Set<EnrRequestedCompetition> budgetEnrolledAtLeast2Ege = new HashSet<>();
                Set<EnrRequestedCompetition> budgetEnrolledEgeOnly = new HashSet<>();

                Set<EnrEntrantRequest> contractRequestsTotal = new HashSet<>();
                Set<EnrEntrantRequest> contractRequestsWithEge = new HashSet<>();
                Set<EnrRequestedCompetition> contractEnrolledTotal = new HashSet<>();
                Set<EnrRequestedCompetition> contractEnrolledAnotherSettlement = new HashSet<>();
                Set<EnrRequestedCompetition> contractEnrolledCountryside = new HashSet<>();
                Set<EnrRequestedCompetition> contractEnrolledWithEge = new HashSet<>();
                Set<EnrRequestedCompetition> contractEnrolledWithEgeAnotherSettlement = new HashSet<>();
                Set<EnrRequestedCompetition> contractEnrolledWithEgeCountryside = new HashSet<>();
                Set<EnrRequestedCompetition> contractEnrolledJust1Ege = new HashSet<>();
                Set<EnrRequestedCompetition> contractEnrolledAtLeast2Ege = new HashSet<>();
                Set<EnrRequestedCompetition> contractEnrolledEgeOnly = new HashSet<>();


                for (EnrRequestedCompetition reqComp : formativeEntry.getValue()) {
                    if (reqComp.getCompetition().getType().getCompensationType().isBudget()) {

                        EnrEntrantRequest request = reqComp.getRequest();
                        budgetRequestsTotal.add(request);

                        if (reqComp.getState().getPriority() == 1)
                            budgetEnrolledTotal.add(reqComp);

                        AddressItem settlement = formative.getAddress() == null ? null : formative.getAddress().getSettlement();


                        if (examMap.containsKey(reqComp)) {

                            boolean hasEge = false;
                            boolean hasNotEge = false;
                            int countOfEge = 0;
                            boolean onlyEge = true;


                            for (EnrChosenEntranceExam exam : examMap.get(reqComp)) {
                                if (exam.getMaxMarkForm().getPassForm().getCode().equals(EnrExamPassFormCodes.STATE_EXAM)) {
                                    hasEge = true;
                                    countOfEge++;
                                } else {
                                    hasNotEge = true;
                                    onlyEge = false;
                                }

                            }
                            if (hasEge)
                                budgetRequestsWithEge.add(request);

                            if (reqComp.getState().getPriority() == 1) {

                                if (hasEge) {
                                    if (request.getIdentityCard().getAddress() instanceof AddressDetailed) {
                                        if (((AddressDetailed) request.getIdentityCard().getAddress()).getSettlement() == null || !((AddressDetailed) request.getIdentityCard().getAddress()).getSettlement().equals(settlement)) {
                                            budgetEnrolledWithEgeAnotherSettlement.add(reqComp);
                                        }
                                        if (((AddressDetailed) request.getIdentityCard().getAddress()).getSettlement() != null && ((AddressDetailed) request.getIdentityCard().getAddress()).getSettlement().getAddressType().isCountryside()) {
                                            budgetEnrolledWithEgeCountryside.add(reqComp);
                                        }
                                    } else {
                                        budgetEnrolledWithEgeAnotherSettlement.add(reqComp);
                                    }
                                }

                                if (hasEge)
                                    budgetEnrolledWithEge.add(reqComp);
                                if (onlyEge)
                                    budgetEnrolledEgeOnly.add(reqComp);
                                if (countOfEge > 1 && hasNotEge)
                                    budgetEnrolledAtLeast2Ege.add(reqComp);
                                if (countOfEge == 1)
                                    budgetEnrolledJust1Ege.add(reqComp);
                            }

                            if (reqComp.getState().getPriority() == 1) {
                                if (request.getIdentityCard().getAddress() instanceof AddressDetailed) {
                                    if (((AddressDetailed) request.getIdentityCard().getAddress()).getSettlement() == null || !((AddressDetailed) request.getIdentityCard().getAddress()).getSettlement().equals(settlement)) {
                                        budgetEnrolledAnotherSettlement.add(reqComp);
                                    }
                                    if (((AddressDetailed) request.getIdentityCard().getAddress()).getSettlement() != null && ((AddressDetailed) request.getIdentityCard().getAddress()).getSettlement().getAddressType().isCountryside()) {
                                        budgetEnrolledCountryside.add(reqComp);
                                    }
                                } else {
                                    budgetEnrolledAnotherSettlement.add(reqComp);
                                }
                            }

                        }

                    } else {
                        EnrEntrantRequest request = reqComp.getRequest();
                        contractRequestsTotal.add(request);
                        AddressItem settlement = formative.getAddress() == null ? null : formative.getAddress().getSettlement();
                        if (reqComp.getState().getPriority() == 1)
                            contractEnrolledTotal.add(reqComp);

                        if (examMap.containsKey(reqComp)) {
                            boolean hasEge = false;
                            boolean hasNotEge = false;
                            int countOfEge = 0;
                            boolean onlyEge = true;

                            for (EnrChosenEntranceExam exam : examMap.get(reqComp)) {
                                if (exam.getMaxMarkForm().getPassForm().getCode().equals(EnrExamPassFormCodes.STATE_EXAM)) {
                                    hasEge = true;
                                    countOfEge++;
                                } else {
                                    hasNotEge = true;
                                    onlyEge = false;
                                }

                            }
                            if (hasEge)
                                contractRequestsWithEge.add(request);

                            if (reqComp.getState().getPriority() == 1) {

                                if (hasEge) {
                                    if (request.getIdentityCard().getAddress() instanceof AddressDetailed) {
                                        if (((AddressDetailed) request.getIdentityCard().getAddress()).getSettlement() == null || !((AddressDetailed) request.getIdentityCard().getAddress()).getSettlement().equals(settlement)) {
                                            contractEnrolledWithEgeAnotherSettlement.add(reqComp);
                                        }
                                        if (((AddressDetailed) request.getIdentityCard().getAddress()).getSettlement() != null && ((AddressDetailed) request.getIdentityCard().getAddress()).getSettlement().getAddressType().isCountryside()) {
                                            contractEnrolledWithEgeCountryside.add(reqComp);
                                        }
                                    } else {
                                        contractEnrolledWithEgeAnotherSettlement.add(reqComp);
                                    }
                                }

                                if (hasEge)
                                    contractEnrolledWithEge.add(reqComp);
                                if (onlyEge)
                                    contractEnrolledEgeOnly.add(reqComp);
                                if (countOfEge > 1 && hasNotEge)
                                    contractEnrolledAtLeast2Ege.add(reqComp);
                                if (countOfEge == 1)
                                    contractEnrolledJust1Ege.add(reqComp);
                            }

                        }
                        if (reqComp.getState().getPriority() == 1) {

                            if (request.getIdentityCard().getAddress() instanceof AddressDetailed) {
                                if (((AddressDetailed) request.getIdentityCard().getAddress()).getSettlement() == null || !((AddressDetailed) request.getIdentityCard().getAddress()).getSettlement().equals(settlement)) {
                                    contractEnrolledAnotherSettlement.add(reqComp);
                                }
                                if (((AddressDetailed) request.getIdentityCard().getAddress()).getSettlement() != null && ((AddressDetailed) request.getIdentityCard().getAddress()).getSettlement().getAddressType().isCountryside()) {
                                    contractEnrolledCountryside.add(reqComp);
                                }
                            } else {
                                contractEnrolledAnotherSettlement.add(reqComp);
                            }
                        }
                    }
                }


                List<String> row = new ArrayList<>();

                // 1
                row.add("");

                // 2
                row.add(String.valueOf(subjects.size()));
                subjectsTotal.add(subjects.size());

                // 3
                row.add(String.valueOf(subjectsWithEgeOnFormative.size()));
                subjectsWithEgeTotal.add(subjectsWithEgeOnFormative.size());

                // 4
                row.add(String.valueOf(planKCP));
                planKCPSum.add(planKCP);

                //5
                row.add(String.valueOf(budgetRequestsTotal.size()));
                uniBudgetRequestsTotal.addAll(budgetRequestsTotal);

                //6
                row.add(String.valueOf(budgetRequestsWithEge.size()));
                uniBudgetRequestsWithEge.addAll(budgetRequestsWithEge);

                //7
                row.add(String.valueOf(budgetEnrolledTotal.size()));
                uniBudgetEnrolledTotal.addAll(budgetEnrolledTotal);

                //8
                row.add(String.valueOf(budgetEnrolledAnotherSettlement.size()));
                uniBudgetEnrolledAnotherSettlement.addAll(budgetEnrolledAnotherSettlement);

                //9
                row.add(String.valueOf(budgetEnrolledCountryside.size()));
                uniBudgetEnrolledCountryside.addAll(budgetEnrolledCountryside);

                //10
                row.add(String.valueOf(budgetEnrolledWithEge.size()));
                uniBudgetEnrolledWithEge.addAll(budgetEnrolledWithEge);

                //11
                row.add(String.valueOf(budgetEnrolledWithEgeAnotherSettlement.size()));
                uniBudgetEnrolledWithEgeAnotherSettlement.addAll(budgetEnrolledWithEgeAnotherSettlement);

                //12
                row.add(String.valueOf(budgetEnrolledWithEgeCountryside.size()));
                uniBudgetEnrolledWithEgeCountryside.addAll(budgetEnrolledWithEgeCountryside);

                //13
                row.add(String.valueOf(budgetEnrolledJust1Ege.size()));
                uniBudgetEnrolledJust1Ege.addAll(budgetEnrolledJust1Ege);

                //14
                row.add(String.valueOf(budgetEnrolledAtLeast2Ege.size()));
                uniBudgetEnrolledAtLeast2Ege.addAll(budgetEnrolledAtLeast2Ege);

                //15
                row.add(String.valueOf(budgetEnrolledEgeOnly.size()));
                uniBudgetEnrolledEgeOnly.addAll(budgetEnrolledEgeOnly);

                // Contract

                //16
                row.add(String.valueOf(planContract));
                planContractSum.add(planContract);

                //17
                row.add(String.valueOf(contractRequestsTotal.size()));
                uniContractRequestsTotal.addAll(contractRequestsTotal);

                //18
                row.add(String.valueOf(contractRequestsWithEge.size()));
                uniContractRequestsWithEge.addAll(contractRequestsWithEge);

                //19
                row.add(String.valueOf(contractEnrolledTotal.size()));
                uniContractEnrolledTotal.addAll(contractEnrolledTotal);

                //20
                row.add(String.valueOf(contractEnrolledAnotherSettlement.size()));
                uniContractEnrolledAnotherSettlement.addAll(contractEnrolledAnotherSettlement);

                //21
                row.add(String.valueOf(contractEnrolledCountryside.size()));
                uniContractEnrolledCountryside.addAll(contractEnrolledCountryside);

                //22
                row.add(String.valueOf(contractEnrolledWithEge.size()));
                uniContractEnrolledWithEge.addAll(contractEnrolledWithEge);

                //23
                row.add(String.valueOf(contractEnrolledWithEgeAnotherSettlement.size()));
                uniContractEnrolledWithEgeAnotherSettlement.addAll(contractEnrolledWithEgeAnotherSettlement);

                //24
                row.add(String.valueOf(contractEnrolledWithEgeCountryside.size()));
                uniContractEnrolledWithEgeCountryside.addAll(contractEnrolledWithEgeCountryside);

                //25
                row.add(String.valueOf(contractEnrolledJust1Ege.size()));
                uniContractEnrolledJust1Ege.addAll(contractEnrolledJust1Ege);

                //26
                row.add(String.valueOf(contractEnrolledAtLeast2Ege.size()));
                uniContractEnrolledAtLeast2Ege.addAll(contractEnrolledAtLeast2Ege);

                //27
                row.add(String.valueOf(contractEnrolledEgeOnly.size()));
                uniContractEnrolledEgeOnly.addAll(contractEnrolledEgeOnly);

                tTable.add(row.toArray(new String[row.size()]));

            }

            List<String> finalRow1 = new ArrayList<>();
            List<String> finalRow2 = new ArrayList<>();

            int subjectsTotalSum = 0;
            int subjectsWithEgeTotalSum = 0;
            int planKCPTotalSum = 0;
            int planContractTotalSum = 0;

            for (Integer value : subjectsTotal) {
                subjectsTotalSum += value;
            }
            for (Integer value : subjectsWithEgeTotal) {
                subjectsWithEgeTotalSum += value;
            }
            for (Integer value : planKCPSum) {
                planKCPTotalSum += value;
            }
            for (Integer value : planContractSum) {
                planContractTotalSum += value;
            }


            // 1 (Номер столбца)
            finalRow1.add("");
            finalRow2.add("");

            // 2
            finalRow1.add("\\b " + String.valueOf(subjectsTotalSum));
            finalRow2.add("\\b " + String.valueOf(subjectsTotalSum));

            // 3
            finalRow1.add("\\b " + String.valueOf(subjectsWithEgeTotalSum));
            finalRow2.add("\\b " + String.valueOf(subjectsWithEgeTotalSum));

            // 4
            finalRow1.add("\\b " + String.valueOf(planKCPTotalSum));
            finalRow2.add("\\b " + String.valueOf(planKCPTotalSum));

            //5
            finalRow1.add("\\b " + String.valueOf(uniBudgetRequestsTotal.size()));
            finalRow2.add("\\b " + countUniqueEntrantsFromRequests(uniBudgetRequestsTotal));

            //6
            finalRow1.add("\\b " + String.valueOf(uniBudgetRequestsWithEge.size()));
            finalRow2.add("\\b " + countUniqueEntrantsFromRequests(uniBudgetRequestsWithEge));

            //7
            finalRow1.add("\\b " + String.valueOf(uniBudgetEnrolledTotal.size()));
            finalRow2.add("\\b " + countUniqueEntrants(uniBudgetEnrolledTotal));

            //8
            finalRow1.add("\\b " + String.valueOf(uniBudgetEnrolledAnotherSettlement.size()));
            finalRow2.add("\\b " + countUniqueEntrants(uniBudgetEnrolledAnotherSettlement));

            //9
            finalRow1.add("\\b " + String.valueOf(uniBudgetEnrolledCountryside.size()));
            finalRow2.add("\\b " + countUniqueEntrants(uniBudgetEnrolledCountryside));

            //10
            finalRow1.add("\\b " + String.valueOf(uniBudgetEnrolledWithEge.size()));
            finalRow2.add("\\b " + countUniqueEntrants(uniBudgetEnrolledWithEge));

            //11
            finalRow1.add("\\b " + String.valueOf(uniBudgetEnrolledWithEgeAnotherSettlement.size()));
            finalRow2.add("\\b " + countUniqueEntrants(uniBudgetEnrolledWithEgeAnotherSettlement));

            //12
            finalRow1.add("\\b " + String.valueOf(uniBudgetEnrolledWithEgeCountryside.size()));
            finalRow2.add("\\b " + countUniqueEntrants(uniBudgetEnrolledWithEgeCountryside));

            //13
            finalRow1.add("\\b " + String.valueOf(uniBudgetEnrolledJust1Ege.size()));
            finalRow2.add("\\b " + countUniqueEntrants(uniBudgetEnrolledJust1Ege));

            //14
            finalRow1.add("\\b " + String.valueOf(uniBudgetEnrolledAtLeast2Ege.size()));
            finalRow2.add("\\b " + countUniqueEntrants(uniBudgetEnrolledAtLeast2Ege));

            //15
            finalRow1.add("\\b " + String.valueOf(uniBudgetEnrolledEgeOnly.size()));
            finalRow2.add("\\b " + countUniqueEntrants(uniBudgetEnrolledEgeOnly));

            // Contract


            //16
            finalRow1.add("\\b " + String.valueOf(planContractTotalSum));
            finalRow2.add("\\b " + String.valueOf(planContractTotalSum));


            //17
            finalRow1.add("\\b " + String.valueOf(uniContractRequestsTotal.size()));
            finalRow2.add("\\b " + countUniqueEntrantsFromRequests(uniContractRequestsTotal));

            //18
            finalRow1.add("\\b " + String.valueOf(uniContractRequestsWithEge.size()));
            finalRow2.add("\\b " + countUniqueEntrantsFromRequests(uniContractRequestsWithEge));

            //19
            finalRow1.add("\\b " + String.valueOf(uniContractEnrolledTotal.size()));
            finalRow2.add("\\b " + countUniqueEntrants(uniContractEnrolledTotal));

            //20
            finalRow1.add("\\b " + String.valueOf(uniContractEnrolledAnotherSettlement.size()));
            finalRow2.add("\\b " + countUniqueEntrants(uniContractEnrolledAnotherSettlement));

            //21
            finalRow1.add("\\b " + String.valueOf(uniContractEnrolledCountryside.size()));
            finalRow2.add("\\b " + countUniqueEntrants(uniContractEnrolledCountryside));

            //22
            finalRow1.add("\\b " + String.valueOf(uniContractEnrolledWithEge.size()));
            finalRow2.add("\\b " + countUniqueEntrants(uniContractEnrolledWithEge));

            //23
            finalRow1.add("\\b " + String.valueOf(uniContractEnrolledWithEgeAnotherSettlement.size()));
            finalRow2.add("\\b " + countUniqueEntrants(uniContractEnrolledWithEgeAnotherSettlement));

            //24
            finalRow1.add("\\b " + String.valueOf(uniContractEnrolledWithEgeCountryside.size()));
            finalRow2.add("\\b " + countUniqueEntrants(uniContractEnrolledWithEgeCountryside));

            //25
            finalRow1.add("\\b " + String.valueOf(uniContractEnrolledJust1Ege.size()));
            finalRow2.add("\\b " + countUniqueEntrants(uniContractEnrolledJust1Ege));

            //26
            finalRow1.add("\\b " + String.valueOf(uniContractEnrolledAtLeast2Ege.size()));
            finalRow2.add("\\b " + countUniqueEntrants(uniContractEnrolledAtLeast2Ege));

            //27
            finalRow1.add("\\b " + String.valueOf(uniContractEnrolledEgeOnly.size()));
            finalRow2.add("\\b " + countUniqueEntrants(uniContractEnrolledEgeOnly));

            tTable.add(new String[]{"\\bИТОГО (По заявлениям)", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                    "", "", "", "", "", "", "", "", "", "", "", "", ""});
            tTable.add(finalRow1.toArray(new String[finalRow1.size()]));

            tTable.add(new String[]{"\\bИТОГО (Человек)", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                    "", "", "", "", "", "", "", "", "", "", "", "", ""});
            tTable.add(finalRow2.toArray(new String[finalRow2.size()]));

            RtfTableModifier modifier = new RtfTableModifier();
            modifier.put("T", tTable.toArray(new String[tTable.size()][]));
            modifier.put("H", hTable.toArray(new String[hTable.size()][]));
            modifier.put("H", new RtfRowIntercepterRawText());


            modifier.put("T", new IRtfRowIntercepter() {
                @Override
                public void beforeModify(RtfTable table, int currentRowIndex) {

                }

                @Override
                public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value) {
                    if (rowIndex % 2 == 0) // каэждая четная строка содержит название формирующего и мерджится
                    {
                        cell.setMergeType(colIndex == 0 ? MergeType.HORIZONTAL_MERGED_FIRST : MergeType.HORIZONTAL_MERGED_NEXT);
                        cell.append(IRtfData.QL);
                    } else cell.append(IRtfData.QR);
                    List<IRtfElement> list = new ArrayList<IRtfElement>();
                    IRtfText text = RtfBean.getElementFactory().createRtfText(value);
                    text.setRaw(true);
                    list.add(text);
                    return list;
                }

                @Override
                public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex) {
                }
            });
            modifier.modify(document);

        }


        return RtfUtil.toByteArray(document);
    }

    private String countUniqueEntrants(Collection<EnrRequestedCompetition> data)
    {
        Set<EnrRequestedCompetition> set = new HashSet<>();
        set.addAll(data);
        Set<EnrEntrant> entrants = new HashSet<>();
        for (EnrRequestedCompetition requestedCompetition : set) {
            entrants.add(requestedCompetition.getRequest().getEntrant());
        }

        return String.valueOf(entrants.size());
    }


    private String countUniqueEntrantsFromRequests(Collection<EnrEntrantRequest> data)
    {
        Set<EnrEntrantRequest> set = new HashSet<>();
        set.addAll(data);
        Set<EnrEntrant> entrants = new HashSet<>();
        for (EnrEntrantRequest request : set) {
            entrants.add(request.getEntrant());
        }

        return String.valueOf(entrants.size());
    }
}
