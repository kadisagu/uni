/* $Id$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.DocumentsStep;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseBinaryOptionGroupModel;
import org.tandemframework.shared.person.base.bo.Person.util.ISecureRoleContext;
import org.tandemframework.shared.person.base.bo.Person.util.SecureRoleContext;
import org.tandemframework.shared.person.base.bo.PersonDocument.PersonDocumentManager;
import org.tandemframework.shared.person.base.bo.PersonDocument.ui.Add.PersonDocumentAdd;
import org.tandemframework.shared.person.base.bo.PersonDocument.ui.RoleTab.PersonDocumentRoleTabUI;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.EnrEntrantRequestAddWizardWizardUI;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.EnrEntrantWizardState;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.IWizardStep;

import java.util.Map;

/**
 * @author nvankov
 * @since 6/5/14
 */
@Input( {
    @Bind(key = EnrEntrantRequestAddWizardWizardUI.PARAM_WIZARD_STATE, binding = "state")
})
public class EnrEntrantRequestAddWizardDocumentsStepUI extends PersonDocumentRoleTabUI implements IWizardStep
{
    public static final String DOCUMENT_FORM_REGION_NAME = "documentFormRegion";

    private EnrEntrantWizardState _state;

    private CommonBaseBinaryOptionGroupModel docTypeModel = new CommonBaseBinaryOptionGroupModel("Диплом участника олимпиады", "Иной документ").selectSecond();

    private EnrOnlineEntrant _onlineEntrant;

    private boolean editMode;

    @Override
    public void onComponentRefresh()
    {
        setPersonRoleModel(SecureRoleContext.instance(getEntrant()).personRoleName(EnrEntrant.class.getSimpleName()).accessible(getEntrant().isAccessible()));
        super.onComponentRefresh();
        getDocTypeModel().selectSecond();
        if(getState().getOnlineEntrantId() != null)
        {
            setOnlineEntrant(DataAccessServices.dao().get(getState().getOnlineEntrantId()));
        }

        _uiActivation.asRegion(PersonDocumentAdd.class, DOCUMENT_FORM_REGION_NAME)
                .parameter(PersonDocumentManager.BIND_PERSON_ROLE_ID, getEntrant().getId())
                .parameter(PersonDocumentManager.BIND_INLINE, true)
                .activate();
    }

    @Override
    public void onComponentBindReturnParameters(final String childRegionName, final Map<String, Object> returnedData) {
        if (DOCUMENT_FORM_REGION_NAME.equals(childRegionName)) {
            final Object doClose = returnedData.get(PersonDocumentManager.BIND_DO_CLOSE);
            if (Boolean.TRUE.equals(doClose)) {
                final Object docId = returnedData.get(PersonDocumentManager.BIND_DOCUMENT_ID);
                if(docId != null && docId instanceof Long)
                {
                    getState().getCreatedObjectIds().add((Long) docId);
                }

                _uiActivation.asRegion(PersonDocumentAdd.class, DOCUMENT_FORM_REGION_NAME)
                        .parameter(PersonDocumentManager.BIND_PERSON_ROLE_ID, getEntrant().getId())
                        .parameter(PersonDocumentManager.BIND_INLINE, true)
                        .activateIfSame().activate();
            }
        }
    }


    // EnrEntrantAllDocumentListAddon.IOwner

    public EnrEntrant getEntrant() { return getState().getEntrant(); }
    public ISecureRoleContext getSecureRoleContext() { return null; }
    // actions

    @Override
    public void saveStepData()
    {
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        super.onBeforeDataSourceFetch(dataSource);

        dataSource.put(EnrEntrantRequestAddWizardDocumentsStep.PARAM_CREATED_OBJECTS_IDS, getState().getCreatedObjectIds());
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asRegion(PersonDocumentAdd.class, DOCUMENT_FORM_REGION_NAME)
                .parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong())
                .parameter(PersonDocumentManager.BIND_PERSON_ROLE_ID, getEntrant().getId())
                .parameter(PersonDocumentManager.BIND_INLINE, true)
                .activateIfSame().activate();
    }

    public void onDeleteEntityFromList()
    {
        PersonDocumentManager.instance().dao().doUnlinkDocument(getListenerParameterAsLong(), getEntrant());
    }


    public void onClickDownloadIndAchFile()
    {
        if (isShowOnlineEntrantIndAchFileInfo()) {

            DatabaseFile file = getOnlineEntrant().getIndAchievScanCopy();
            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType(file.getContentType())
                    .fileName(file.getFilename()).document(file.getContent()), false);
        }
    }

    public void onClickDownloadOtherFile()
    {
        if (isShowOnlineEntrantOtherFileInfo()) {
            DatabaseFile file = getOnlineEntrant().getOtherDocScanCopy();
            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType(file.getContentType())
                    .fileName(file.getFilename()).document(file.getContent()), false);
        }
    }

    // presenter

    public boolean isShowEditModeButtons() {
        return isEditMode();
    }

    public boolean isShowSaveModeButtons() {
        return !isEditMode();
    }

    public boolean isShowOnlineEntrantFilesInfo()
    {
        return isShowOnlineEntrantIndAchFileInfo() || isShowOnlineEntrantOtherFileInfo();
    }

    public boolean isShowOnlineEntrantIndAchFileInfo()
    {
        return getOnlineEntrant() != null && getOnlineEntrant().getIndAchievScanCopy() != null;
    }

    public boolean isShowOnlineEntrantOtherFileInfo()
    {
        return getOnlineEntrant() != null && getOnlineEntrant().getOtherDocScanCopy() != null;
    }


    // getters and setters

    public EnrEntrantWizardState getState()
    {
        return _state;
    }

    public void setState(EnrEntrantWizardState state)
    {
        _state = state;
    }

    public boolean isEditMode()
    {
        return editMode;
    }

    public void setEditMode(boolean editMode)
    {
        this.editMode = editMode;
    }

    public EnrOnlineEntrant getOnlineEntrant()
    {
        return _onlineEntrant;
    }

    public void setOnlineEntrant(EnrOnlineEntrant onlineEntrant)
    {
        _onlineEntrant = onlineEntrant;
    }

    public CommonBaseBinaryOptionGroupModel getDocTypeModel()
    {
        return docTypeModel;
    }

    public void setDocTypeModel(CommonBaseBinaryOptionGroupModel docTypeModel)
    {
        this.docTypeModel = docTypeModel;
    }
}
