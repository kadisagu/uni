/**
 *$Id: EnrEnrollmentCampaignList.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.transformer.BaseOutputTransformer;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.formatter.PropertyFormatter;
import org.tandemframework.core.view.formatter.RowCollectionFormatter;
import org.tandemframework.core.view.util.Icon;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.util.*;

/**
 * @author Alexander Shaburov
 * @since 08.04.13
 */
@Configuration
public class EnrEnrollmentCampaignList extends BusinessComponentManager
{
    public static final String ENROLLMENT_CAMPAIGN_SEARCH_DS = "enrollmentCampaignDS";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(ENROLLMENT_CAMPAIGN_SEARCH_DS, enrollmentCampaignDSColumns(), enrollmentCampaignDSHandler()))
                .addDataSource(EducationCatalogsManager.instance().eduYearDSConfig())
                .create();
    }

    @Bean
    public ColumnListExtPoint enrollmentCampaignDSColumns()
    {
        return columnListExtPointBuilder(ENROLLMENT_CAMPAIGN_SEARCH_DS)
                .addColumn(publisherColumn("title", EnrEnrollmentCampaign.title()).permissionKey("enr14EnrollmentCampaignView"))
                .addColumn(textColumn("eduYear", EnrEnrollmentCampaign.educationYear().title()))
                .addColumn(textColumn("enrPeriod", EnrEnrollmentCampaign.enrollmentPeriod()))
                .addColumn(textColumn("enrOrgUnit", "enrOrgUnit").formatter(new RowCollectionFormatter(new PropertyFormatter(EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s()))))
                .addColumn(toggleColumn("open", EnrEnrollmentCampaign.open())
                            .toggleOnListener("onToggleOpen")
                            .toggleOffListener("onToggleOpen"))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, new Icon(EDIT_COLUMN_NAME), "onClickEditEnrollmentCampaign").permissionKey("enr14EnrollmentCampaign_editEnrollmentCampaign"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, new Icon(DELETE_COLUMN_NAME), "onClickDeleteEnrollmentCampaign", new FormattedMessage("enrollmentCampaignDS.delete.alert", EnrEnrollmentCampaign.title().s())).permissionKey("enr14EnrollmentCampaign_deleteEnrollmentCampaign"))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> enrollmentCampaignDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrEnrollmentCampaign.class) // TODO: выпилить использование EntityComboDataSourceHandler для searchList
            .outputTransformer(new BaseOutputTransformer<DSInput, DSOutput>(ENROLLMENT_CAMPAIGN_SEARCH_DS, this.getName()) {
                @Override public void transformOutput(final DSInput input, final DSOutput output, final ExecutionContext context) {
                    Collections.sort((List) output.getRecordList(), EnrEnrollmentCampaignManager.ENR_CAMPAIGN_COMPARATOR);

                    Map<Long, List<EnrOrgUnit>> map = new HashMap<>();
                    for (EnrOrgUnit orgUnit : IUniBaseDao.instance.get().getList(EnrOrgUnit.class, EnrOrgUnit.enrollmentCampaign().s(), output.getRecordList(), EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s())) {
                        SafeMap.safeGet(map, orgUnit.getEnrollmentCampaign().getId(), ArrayList.class).add(orgUnit);
                    }
                    for (DataWrapper wrapper : DataWrapper.wrap(output)) {
                        wrapper.setProperty("enrOrgUnit", map.get(wrapper.getId()));
                    }
                }
            })
            .where(EnrEnrollmentCampaign.educationYear(), EnrEnrollmentCampaignListUI.PROP_EDU_YEAR)
            .pageable(true);
    }


    @Bean
    public IBusinessHandler<DSInput, DSOutput> educationYearDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EducationYear.class)
                .filter(EducationYear.title())
                .order(EducationYear.intValue(), OrderDirection.desc)
                .pageable(true);
    }
}
