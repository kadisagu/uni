/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.EnrEnrollmentModelManager;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentModel;
import ru.tandemservice.unienr14.request.daemon.EnrRequestExtParamsDaemonBean;

/**
 * @author oleyba
 * @since 3/27/15
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "holder.id", required=true),
    @Bind(key = "selectedTab", binding = "selectedTab")
})
public class EnrEnrollmentModelPubUI extends UIPresenter
{
    private EntityHolder<EnrEnrollmentModel> holder = new EntityHolder<>();
    private String selectedTab;

    @Override
    public void onComponentRefresh()
    {
        getHolder().refresh();
    }

    public void onClickEnroll()
    {
        EnrEnrollmentModelManager.instance().dao().doEnroll(getModel());
    }

    public void onClickRefreshOriginal()
    {
        EnrRequestExtParamsDaemonBean.DAEMON.wakeUpAndWaitDaemon(3600);
        EnrEnrollmentModelManager.instance().dao().doRefreshOriginal(getModel());
    }

    public void onClickAllEnrollmentAvailable()
    {
        EnrEnrollmentModelManager.instance().dao().doAllEnrollmentAvailable(getModel());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put("model", getModel().getId());
    }

    // getters and setters

    public EnrEnrollmentModel getModel()
    {
        return getHolder().getValue();
    }

    public EntityHolder<EnrEnrollmentModel> getHolder()
    {
        return holder;
    }

    public String getSelectedTab()
    {
        return selectedTab;
    }

    public void setSelectedTab(String selectedTab)
    {
        this.selectedTab = selectedTab;
    }
}