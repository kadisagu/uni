package ru.tandemservice.unienr14.enrollment.entity;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.commonbase.base.util.zip.GZipCompressionInterface;

import ru.tandemservice.unienr14.catalog.entity.codes.EnrEnrollmentStepStateCodes;
import ru.tandemservice.unienr14.enrollment.entity.gen.EnrEnrollmentStepGen;

/**
 * Шаг рекомендации к зачислению
 *
 * В списке шага зачисления есть все абитуриенты (их выбранные конкурсы), которые подали документы на один из конкурсов, включенных в данный шаг.
 */
public class EnrEnrollmentStep extends EnrEnrollmentStepGen implements ITitled
{
    public static final GZipCompressionInterface zip = GZipCompressionInterface.INSTANCE;

    @Override
    @EntityDSLSupport(parts = EnrEnrollmentStepGen.P_ENROLLMENT_DATE)
    public String getDateStr() {
        return DateFormatter.DEFAULT_DATE_FORMATTER.format(getEnrollmentDate());
    }

    @Override
    public String getTitle()
    {
        return "Шаг зачисления: " + getDateStr();
    }

    @Override
    @EntityDSLSupport
    public boolean isDeleteAllowed() {
        return getState() != null && EnrEnrollmentStepStateCodes.START.equals(getState().getCode());
    }

    @EntityDSLSupport
    public boolean isPlanEditDisabled() {
        return !isAutoRecommendAllowed() && !isAutoMarkEnrolledAllowed();
    }

    public boolean isNextStateAllowed() {
        return getState() != null && !EnrEnrollmentStepStateCodes.CLOSED.equals(getState().getCode());
    }

    public boolean isPrevStateAllowed() {
        return getState() != null && !EnrEnrollmentStepStateCodes.START.equals(getState().getCode());
    }

    public boolean isAutoRecommendAllowed() {
        return getState() != null && EnrEnrollmentStepStateCodes.START.equals(getState().getCode()) && getKind() != null && getKind().isUseRecommendation();
    }

    public boolean isAllowManualRecommendationAllowed() {
        return isAutoRecommended() && isAutoRecommendAllowed();
    }

    public boolean isClearRecommendationAllowed() {
        return isAutoRecommendAllowed();
    }

    public boolean isAutoMarkEnrolledAllowed() {
        if (getState() == null || getKind() == null) return false;
        if (EnrEnrollmentStepStateCodes.START.equals(getState().getCode()) && !getKind().isUseRecommendation()) return true;
        return EnrEnrollmentStepStateCodes.RECOMMENDED.equals(getState().getCode());
    }

    public boolean isAllowManualMarkEnrolledAllowed() {
        return isAutoEnrolledMarked() && isAutoMarkEnrolledAllowed();
    }

    public boolean isClearMarkEnrolledAllowed() {
        return isAutoMarkEnrolledAllowed();
    }

    public boolean isAutoCreateOrdersAllowed() {
        if (null == getState()) { return false; }
        return EnrEnrollmentStepStateCodes.FORMING_ORDERS.equals(getState().getCode());
    }

    public boolean isRefreshDataAllowed() {
        if (null == getState()) { return false; }
        switch (getState().getCode()) {
            case EnrEnrollmentStepStateCodes.RECOMMENDED: return true;
            case EnrEnrollmentStepStateCodes.START: return true;
        }
        return false;
    }


    public byte[] getXmlEnrollmentState() {
        return zip.decompress(this.getZipXmlEnrollmentState());
    }
    public void setXmlEnrollmentState(byte[] xml) {
        this.setZipXmlEnrollmentState(zip.compress(xml));
    }

    public byte[] getXmlRecommendationState() {
        return zip.decompress(this.getZipXmlRecommendationState());
    }
    public void setXmlRecommendationState(byte[] xml) {
        this.setZipXmlRecommendationState(zip.compress(xml));
    }

}