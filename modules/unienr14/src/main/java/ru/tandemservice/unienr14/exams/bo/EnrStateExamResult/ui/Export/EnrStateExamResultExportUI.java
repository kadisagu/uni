/* $Id$ */
package ru.tandemservice.unienr14.exams.bo.EnrStateExamResult.ui.Export;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.unienr14.exams.bo.EnrStateExamResult.EnrStateExamResultManager;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.Date;

/**
 * @author azhebko
 * @since 11.06.2014
 */
public class EnrStateExamResultExportUI extends UIPresenter
{
    private EnrEnrollmentCampaign _enrCampaign;
    private Date _requestRegDateFrom;
    private Date _requestRegDateTo;
    private boolean _includingForeignCitizens;
    private DataWrapper _icType;

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrOrder.PARAM_ENR_CAMPAIGN, getEnrCampaign());
    }

    @Override
    public void onComponentRefresh()
    {
        setEnrCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
        onChangeEnrCampaign();
    }

    public void onChangeEnrCampaign()
    {
        setRequestRegDateFrom(getEnrCampaign().getDateFrom());
        setRequestRegDateTo(getEnrCampaign().getDateTo());
    }

    public void onClickApply()
    {
        if(_requestRegDateFrom.after(_requestRegDateTo))
            _uiSupport.error("Поле даты подачи заявления «с» не может быть больше, чем поле даты подачи заявления «по».", "requestRegDateFrom", "requestRegDateTo");

        if(getUserContext().getErrorCollector().hasErrors())
            throw new ApplicationException();
        try
        {
            byte[] content = EnrStateExamResultManager.instance().dao().getEntrantStateExamResultExportContent(getEnrCampaign(), getRequestRegDateFrom(), getRequestRegDateTo(), isIncludingForeignCitizens(), _icType, getSettings().get("enrOrders"));
            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer()
                                                            .contentType(DatabaseFile.CONTENT_TYPE_TEXT_CSV)
                                                            .fileName(new DateFormatter("ddMMyyyy").format(new Date()) + "_EGE_EXPORT.csv")
                                                            .document(content), true);

        } catch(Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    public EnrEnrollmentCampaign getEnrCampaign(){ return _enrCampaign; }

    public void setEnrCampaign(EnrEnrollmentCampaign enrCampaign){ _enrCampaign = enrCampaign; }

    public Date getRequestRegDateFrom(){ return _requestRegDateFrom; }

    public void setRequestRegDateFrom(Date requestRegDateFrom){ _requestRegDateFrom = requestRegDateFrom; }

    public Date getRequestRegDateTo(){ return _requestRegDateTo; }

    public void setRequestRegDateTo(Date requestRegDateTo){ _requestRegDateTo = requestRegDateTo; }

    public boolean isIncludingForeignCitizens(){ return _includingForeignCitizens; }

    public void setIncludingForeignCitizens(boolean includingForeignCitizens){ _includingForeignCitizens = includingForeignCitizens; }

    public DataWrapper getIcType()
    {
        return _icType;
    }

    public void setIcType(DataWrapper icType)
    {
        _icType = icType;
    }
}