/* $Id$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.CompetitionsStep;

import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrOriginalSubmissionAndReturnWay;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrOriginalSubmissionAndReturnWayCodes;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantCompetition;
import ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantStatement;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.AddAppend.EnrEntrantRequestAddAppendUI;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.util.EnrRequestedCompetitionWrapper;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.EnrEntrantRequestAddWizardWizardUI;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.EnrEntrantWizardState;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.IWizardStep;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author nvankov
 * @since 6/5/14
 */
@Input( {
    @Bind(key = EnrEntrantRequestAddWizardWizardUI.PARAM_WIZARD_STATE, binding = "state")
})
public class EnrEntrantRequestAddWizardCompetitionsStepUI extends EnrEntrantRequestAddAppendUI implements IWizardStep
{
    private EnrEntrantWizardState _state;
    private EnrOnlineEntrantStatement onlineEntrantStatement;

    public String getAgreementDate()
    {
        return getOnlineEntrantStatement() != null && getOnlineEntrantStatement().getAgreementDate() != null ? DateFormatter.DEFAULT_DATE_FORMATTER.format(getOnlineEntrantStatement().getAgreementDate()) : "";
    }

    @Override
    public void onComponentRefresh()
    {
        setEntrant(IUniBaseDao.instance.get().getNotNull(EnrEntrant.class, getState().getEntrantId()));

        if (getState().getEntrantRequestId() != null) {
            setEntrantRequest(IUniBaseDao.instance.get().getNotNull(EnrEntrantRequest.class, getState().getEntrantRequestId()));
        } else {
            getEntrantRequest().setType(IUniBaseDao.instance.get().getNotNull(EnrRequestType.class, getState().getRequestTypeId()));
            getEntrantRequest().setReceiveEduLevelFirst(getState().isReceiveEduLevelFirst());
            getEntrantRequest().setIdentityCard(IUniBaseDao.instance.get().getNotNull(IdentityCard.class, getState().getIdentityCardId()));
            getEntrantRequest().setEduDocument(IUniBaseDao.instance.get().getNotNull(PersonEduDocument.class, getState().getEduDocumentId()));

            if (!isInitialized()) {
                EnrOriginalSubmissionAndReturnWay submissionAndReturnWay = IUniBaseDao.instance.get().getCatalogItem(EnrOriginalSubmissionAndReturnWay.class, EnrOriginalSubmissionAndReturnWayCodes.LICHNO);
                getEntrantRequest().setOriginalSubmissionWay(submissionAndReturnWay);
                getEntrantRequest().setOriginalReturnWay(submissionAndReturnWay);
                getEntrantRequest().setType(IUniBaseDao.instance.get().getNotNull(EnrRequestType.class, getState().getRequestTypeId()));
            }
        }


        prepareFormData();


        List<EnrOnlineEntrantStatement> onlineEntrantStatements = IUniBaseDao.instance.get().getList(EnrOnlineEntrantStatement.class, EnrOnlineEntrantStatement.onlineEntrant().id(), getState().getOnlineEntrantId());
        setOnlineEntrantStatement(onlineEntrantStatements.isEmpty() ? null : onlineEntrantStatements.get(0));

        if (!isInitialized() && getOnlineEntrantStatement() != null) {

            Set<Long> competitionsFromOtherRequests = new HashSet<>(CollectionUtils.collect(getRequestedCompetitionList(), wrapper -> wrapper.getCompetition().getId()));

            List<EnrOnlineEntrantCompetition> onlineEntrantCompetitions = IUniBaseDao.instance.get().getList(EnrOnlineEntrantCompetition.class, EnrOnlineEntrantCompetition.statement(), getOnlineEntrantStatement(), EnrOnlineEntrantCompetition.priority().s());
            for (EnrOnlineEntrantCompetition onlineComp : onlineEntrantCompetitions) {
                if (competitionsFromOtherRequests.contains(onlineComp.getCompetition().getId())) continue;
                getRequestedCompetitionList().add(new EnrRequestedCompetitionWrapper(onlineComp));
            }
        }

        setInitialized(true);
    }

    @Override
    public void saveStepData()
    {
        this.validate();
        if (getUserContext().getErrorCollector().hasErrors())
            return;

        try {
            if (saveOrUpdateEntrantRequest()) return;
        }
        catch (Exception e) {
            ContextLocal.getSite().getDesktop().setRefreshScheduled(true);
            throw CoreExceptionUtils.getRuntimeException(e);
        }

        getState().setEntrantRequestId(getEntrantRequest().getId());
    }

    // presenter

    public boolean isIdentityCardDisabled() { return true; }
    public boolean isEduDocumentDisabled() { return true; }
    public boolean isRequestTypeDisabled() { return true; }

    public boolean isShowOnlineEntrantTargetAdmissionInfo()
    {
        return getOnlineEntrantStatement() != null && Boolean.TRUE.equals(getOnlineEntrantStatement().isHaveAgreementTargetEdu());
    }

    public boolean isOnlineEntrantHaveExclusiveRightsWithoutExams()
    {
        return getOnlineEntrantStatement() != null && Boolean.TRUE.equals(getOnlineEntrantStatement().isExclusiveRightsWithoutExams());
    }

    public boolean isOnlineEntrantHaveExclusiveRightsBudget()
    {
        return getOnlineEntrantStatement() != null && Boolean.TRUE.equals(getOnlineEntrantStatement().isExclusiveRightsBudget());
    }

    public boolean isOnlineEntrantHaveExclusiveRightsEmptiveEnroll()
    {
        return getOnlineEntrantStatement() != null && Boolean.TRUE.equals(getOnlineEntrantStatement().isExclusiveRightsEmptiveEnroll());
    }

    @Override public boolean isShowRequestDataBlock() { return true;}

    // getter and setter

    public EnrEntrantWizardState getState()
    {
        return _state;
    }

    public void setState(EnrEntrantWizardState state)
    {
        _state = state;
    }

    public EnrOnlineEntrantStatement getOnlineEntrantStatement()
    {
        return onlineEntrantStatement;
    }

    public void setOnlineEntrantStatement(EnrOnlineEntrantStatement onlineEntrantStatement)
    {
        this.onlineEntrantStatement = onlineEntrantStatement;
    }
}
