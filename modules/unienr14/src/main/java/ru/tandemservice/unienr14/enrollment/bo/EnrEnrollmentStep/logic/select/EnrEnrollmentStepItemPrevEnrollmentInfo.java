/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.select;

import org.tandemframework.core.entity.IdentifiableWrapper;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentConflictSolution;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEnrollmentConflictSolutionCodes;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItemOrderInfo;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;

/**
 * @author oleyba
 * @since 4/2/15
 */
public class EnrEnrollmentStepItemPrevEnrollmentInfo extends IdentifiableWrapper implements IEnrSelectionItemPrevEnrollmentInfo
{
    private EnrEnrollmentStepItemOrderInfo orderInfo;

    public EnrEnrollmentStepItemPrevEnrollmentInfo(EnrEnrollmentStepItemOrderInfo orderInfo)
    {
        super(orderInfo.getId(), orderInfo.getTitle());
        this.orderInfo = orderInfo;

    }

    @Override
    public String getInfo()
    {
        return orderInfo.getOrderInfo();
    }

    @Override
    public EnrRequestedCompetition getRequestedCompetition()
    {
        return orderInfo.getExtractEntity();
    }

    @Override
    public boolean isCancelled()
    {
        return orderInfo.isExtractCancelled();
    }

    @Override
    public EnrEnrollmentConflictSolution getSolution()
    {
        return orderInfo.getSolution();
    }

    // действие (при инициализации): добавить информацию о приказе о зачислении в другие группы
    public void apply(final EnrSelectionItem item)
    {
        if (!orderInfo.getItem().equals(item.getStepItem())) { throw new IllegalStateException("step-item-missmatch"); }

        if (!isCancelled()) {
            final EnrSelectionGroup otherGroup = item.getGroup().getContext().getStepItemGroup(this);
            if (null != otherGroup) { otherGroup.registerExtract(new EnrEnrollementStepPrevEnrollmentFact(orderInfo.getExtract())); }
        }

        switch(orderInfo.getSolution().getCode()) {
            case EnrEnrollmentConflictSolutionCodes.ENROLL: {
                item.getAddEnrollList().add(this);
                break;
            }
            case EnrEnrollmentConflictSolutionCodes.REENROLL: {
                item.getReenrollFromList().add(this);
                break;
            }
            case EnrEnrollmentConflictSolutionCodes.SKIP: {
                item.skip(new EnrSelectionSkipReason.OrderInfoSkipConflictSolution(this));
                break;
            }
            default: throw new IllegalStateException("undefined-solution: "+orderInfo.getSolution().getCode());
        }
    }
}
