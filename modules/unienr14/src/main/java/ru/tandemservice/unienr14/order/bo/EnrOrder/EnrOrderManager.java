/* $Id: EcOrderManager.java 20463 2011-10-25 05:43:26Z vdanilov $ */
package ru.tandemservice.unienr14.order.bo.EnrOrder;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unienr14.order.bo.EnrOrder.logic.EnrOrderDao;
import ru.tandemservice.unienr14.order.bo.EnrOrder.logic.IEnrOrderDao;

/**
 * @author Vasily Zhukov
 * @since 24.05.2011
 */
@Configuration
public class EnrOrderManager extends BusinessObjectManager
{
    public static EnrOrderManager instance()
    {
        return instance(EnrOrderManager.class);
    }

    @Bean
    public IEnrOrderDao dao()
    {
        return new EnrOrderDao();
    }
}
