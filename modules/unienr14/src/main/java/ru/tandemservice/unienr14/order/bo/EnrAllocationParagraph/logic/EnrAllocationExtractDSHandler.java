/* $Id:$ */
package ru.tandemservice.unienr14.order.bo.EnrAllocationParagraph.logic;

import com.google.common.collect.Maps;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Transformer;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantCustomState;
import ru.tandemservice.unienr14.order.bo.EnrAllocationParagraph.EnrAllocationParagraphManager;
import ru.tandemservice.unienr14.order.entity.*;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
/**
 * @author oleyba
 * @since 7/8/14
 */
public class EnrAllocationExtractDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String ALLOCATION_PARAGRAPH = "enrollmentParagraphView";
    public static final String NO_EXCLUDE = "noExcludeView";
    public static final String ENROLL_EXTRACT = "enrollExtract";
    public static final String CANCEL_EXTRACT = "cancelExtract";
    public static final String OTHER_EXTRACTS = "otherExtracts";
    public static final String CUSTOM_STATES = "customStates";

    public EnrAllocationExtractDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    @SuppressWarnings("unchecked")
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        EnrAllocationParagraph paragraph = context.get(ALLOCATION_PARAGRAPH);

        List<EnrAllocationExtract> extractList = (List<EnrAllocationExtract>) paragraph.getExtractList();

        if (extractList.isEmpty()) return new DSOutput(input);

        DQLSelectBuilder enrollExtractsBuilder = new DQLSelectBuilder().fromEntity(EnrEnrollmentExtract.class, "ee");
        enrollExtractsBuilder.where(in(property("ee", EnrEnrollmentExtract.entity().id()), CommonBaseEntityUtil.getPropertiesList(extractList, EnrAllocationExtract.entity().id())));

        // reqComp.id -> enrollExtract
        Map<Long, EnrEnrollmentExtract> enrollmentExtractMap = Maps.newHashMap();

        for(EnrEnrollmentExtract enrollExtract : enrollExtractsBuilder.createStatement(context.getSession()).<EnrEnrollmentExtract>list())
        {
            enrollmentExtractMap.put(enrollExtract.getEntity().getId(), enrollExtract);
        }

        boolean noExclude = paragraph.getOrder().isReadonly() || extractList.size() == 1;

        List<DataWrapper> recordList = new ArrayList<>();

        List<EnrCancelExtract> cancelExtracts = IUniBaseDao.instance.get().getList(EnrCancelExtract.class, EnrCancelExtract.entity().paragraph(), paragraph);
        Map<EnrAllocationExtract, EnrCancelExtract> cancelExtractMap = new HashMap<>();
        for (EnrCancelExtract cancelExtract : cancelExtracts) {
            if (cancelExtract.getEntity() instanceof  EnrAllocationExtract) {
                cancelExtractMap.put((EnrAllocationExtract) cancelExtract.getEntity(), cancelExtract);
            }
        }

        Transformer<EnrAbstractExtract, EnrEntrant> extractEntrantTransformer = EnrAbstractExtract::getEntrant;

        Collection<EnrAbstractExtract> otherExtracts = EnrAllocationParagraphManager.instance().dao().getOtherExtracts(CollectionUtils.collect(extractList, extractEntrantTransformer), paragraph);
        Map<Long, List<DataWrapper>> otherExtractMap = SafeMap.get(ArrayList.class);
        for (EnrAbstractExtract extract: otherExtracts)
        {
            if(extract.getRequestedCompetition() != null)
                otherExtractMap.get(extract.getEntrant().getId()).add(new DataWrapper(extract.getParagraph().getOrder().getId(), extract.getShortTitle()));
        }

        Collection<EnrEntrant> entrants = CollectionUtils.collect(extractList, extractEntrantTransformer);
        Map<EnrEntrant, List<EnrEntrantCustomState>> customStatesMap = EnrEntrantManager.instance().dao().getActiveCustomStatesMap(entrants, new Date());

        for (EnrAllocationExtract extract : extractList)
        {
            DataWrapper record = new DataWrapper(extract.getId(), extract.getTitle(), extract);
            record.setProperty(ENROLL_EXTRACT, enrollmentExtractMap.get(extract.getEntity().getId()));
            record.setProperty(NO_EXCLUDE, noExclude);
            record.setProperty(CANCEL_EXTRACT, cancelExtractMap.get(extract));
            record.setProperty(OTHER_EXTRACTS, otherExtractMap.get(extract.getEntrant().getId()));
            record.setProperty(CUSTOM_STATES, customStatesMap.get(extract.getEntrant()));
            recordList.add(record);
        }

        return ListOutputBuilder.get(input, recordList).build();
    }
}

