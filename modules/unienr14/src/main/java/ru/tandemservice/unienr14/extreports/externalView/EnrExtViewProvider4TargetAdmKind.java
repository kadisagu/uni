/* $Id:$ */
package ru.tandemservice.unienr14.extreports.externalView;

import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.extview.SimpleDQLExternalViewConfig;
import ru.tandemservice.unienr14.catalog.entity.EnrTargetAdmissionKind;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignTargetAdmissionKind;

import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author oleyba
 * @since 8/27/13
 */
public class EnrExtViewProvider4TargetAdmKind extends SimpleDQLExternalViewConfig
{

    @Override
    protected DQLSelectBuilder buildDqlQuery() {
        DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(EnrCampaignTargetAdmissionKind.class, "ta")
            .joinPath(DQLJoinType.left, EnrCampaignTargetAdmissionKind.targetAdmissionKind().fromAlias("ta"), "taKind");
        
        column(dql, property(EnrCampaignTargetAdmissionKind.enrollmentCampaign().id().fromAlias("ta")), "enrollmentCampaignId").comment("uid приемной кампании");
        column(dql, property(EnrTargetAdmissionKind.code().fromAlias("taKind")), "targetAdmissionKindCode").comment("код вида целевого приема");
        column(dql, property(EnrTargetAdmissionKind.fullTitle().fromAlias("taKind")), "fullTitle").comment("название вида целевого приема (полное)");
        column(dql, property(EnrTargetAdmissionKind.priority().fromAlias("taKind")), "priority").comment("приоритет");
        column(dql, property(EnrTargetAdmissionKind.title().fromAlias("taKind")), "title").comment("название вида целевого приема");

        return dql;
    }
}
