/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantRefusalAdd;/**
 * @author rsizonenko
 * @since 28.05.2014
 */

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantRefusalReason;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.report.bo.EnrReport.EnrReportManager;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.ui.Pub.EnrReportBasePub;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.util.Date;
import java.util.List;

public class EnrReportEntrantRefusalAddUI extends UIPresenter {


    // fields
    private EnrEnrollmentCampaign _enrollmentCampaign;

    private Date dateFrom;
    private Date dateTo;

    private List<EnrEntrantRefusalReason> _refusalReasonList;

    private List<EnrOrgUnit> _orgUnitList;

    private boolean reasonRequired = false;
    private boolean orgUnitRequired = false;


    // override from UIPresenter
    @Override
    public void onComponentRefresh()
    {
        setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
        setDateFrom(getEnrollmentCampaign() == null ? CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())) : getEnrollmentCampaign().getDateFrom());
        setDateTo(getEnrollmentCampaign() == null ? new Date() : getEnrollmentCampaign().getDateTo());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        dataSource.put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, getEnrollmentCampaign());
    }

    public void onChangeEnrollmentCampaign()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
        setDateFrom(getEnrollmentCampaign().getDateFrom());
        setDateTo(getEnrollmentCampaign().getDateTo());
    }

    // validate
    private void validate()
    {
        if (getDateFrom().after(getDateTo()))
            _uiSupport.error("Дата, указанная в параметре \"Заявления с\" не должна быть позже даты в параметре \"Заявления по\".", "dateFrom");

        if(getUserContext().getErrorCollector().hasErrors())
            throw new ApplicationException();
    }

    // Listeners
    public void onClickApply() {
        validate();
        Long reportId = EnrReportManager.instance().entrantRefusalDao().createReport(this);
        deactivate();
        _uiActivation.asDesktopRoot(EnrReportBasePub.class)
                .parameter(PUBLISHER_ID, reportId)
                .activate();
    }

    // getters-setters
    public EnrEnrollmentCampaign getEnrollmentCampaign() {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign) {
        _enrollmentCampaign = enrollmentCampaign;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public List<EnrEntrantRefusalReason> getRefusalReasonList() {
        return _refusalReasonList;
    }

    public void setRefusalReasonList(List<EnrEntrantRefusalReason> _refusalReasonList) {
        this._refusalReasonList = _refusalReasonList;
    }

    public List<EnrOrgUnit> getOrgUnitList() {
        return _orgUnitList;
    }

    public void setOrgUnitList(List<EnrOrgUnit> _orgUnitList) {
        this._orgUnitList = _orgUnitList;
    }

    public boolean isReasonRequired() {
        return reasonRequired;
    }

    public void setReasonRequired(boolean reasonRequired) {
        this.reasonRequired = reasonRequired;
    }

    public boolean isOrgUnitRequired() {
        return orgUnitRequired;
    }

    public void setOrgUnitRequired(boolean orgUnitRequired) {
        this.orgUnitRequired = orgUnitRequired;
    }
}