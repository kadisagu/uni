/* $Id$ */
package ru.tandemservice.unienr14.settings.bo.EnrIndividualAchievement.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.core.view.util.Icon;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.EnrEntrantRequestManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementType;

/**
 * @author nvankov
 * @since 4/11/14
 */
@Configuration
public class EnrIndividualAchievementList extends BusinessComponentManager
{
    public static final String ENR_CAMP_SELECT_DS = EnrEnrollmentCampaignManager.DS_ENR_CAMPAIGN;
    public static final String INDIVIDUAL_ACHIEVEMENTS_DS = "individualAchievementsDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
                .addDataSource(searchListDS(INDIVIDUAL_ACHIEVEMENTS_DS, enrIndividualAchievementsDSColumns(), enrIndividualAchievementsDSHandler()))
                .addDataSource(EnrEntrantRequestManager.instance().requestTypeDS_1_Config())
                .create();
    }

    @Bean
    public ColumnListExtPoint enrIndividualAchievementsDSColumns()
    {
        return columnListExtPointBuilder(INDIVIDUAL_ACHIEVEMENTS_DS)
                .addColumn(textColumn("title", EnrEntrantAchievementType.achievementKind().title()).order())
                .addColumn(textColumn("description", EnrEntrantAchievementType.achievementKind().description()))
                .addColumn(booleanColumn("forRequest", EnrEntrantAchievementType.forRequest()))
                .addColumn(booleanColumn("marked", EnrEntrantAchievementType.marked()))
                .addColumn(textColumn("mark", EnrEntrantAchievementType.achievementMarkAsLong())
                        .formatter(source -> DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(0.001d * (Long) source)).order())
                .addColumn(actionColumn(EDIT_COLUMN_NAME, new Icon(EDIT_COLUMN_NAME), EDIT_LISTENER))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, new Icon(DELETE_COLUMN_NAME), DELETE_LISTENER, new FormattedMessage("individualAchievementsDS.delete.alert", EnrEntrantAchievementType.achievementKind().title().s())))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler enrIndividualAchievementsDSHandler()
    {
        return new EnrIndividualAchievementDSHandler(getName());
    }
}



    
