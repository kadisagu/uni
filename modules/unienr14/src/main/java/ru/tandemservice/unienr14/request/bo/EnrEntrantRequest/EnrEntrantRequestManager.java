/* $Id:$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequest;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseFilterUtil;
import org.tandemframework.shared.commonbase.base.util.DefaultNumberGenerationRule;
import org.tandemframework.shared.commonbase.base.util.INumberGenerationRule;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unienr14.catalog.bo.EnrCatalogCommons.EnrCatalogCommonsManager;
import ru.tandemservice.unienr14.catalog.bo.EnrRequestType.EnrRequestTypeManager;
import ru.tandemservice.unienr14.catalog.entity.*;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.EnrCompetitionManager;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;
import ru.tandemservice.unienr14.entrant.EnrNumberGenerationRule;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrantBaseDocument.EnrEntrantBaseDocumentManager;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.logic.EnrEntrantRequestDao;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.logic.ExternalOrgUnitDSHandler;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.logic.ICompetitionContextCustomizer;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.logic.IEnrEntrantRequestDao;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.util.EntrantRequestAcceptPrincipalHandler;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitProofDocument;
import ru.tandemservice.unienr14.request.entity.gen.IEnrEntrantBenefitProofDocumentGen;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.logic.EnrOrgUnitBaseDSHandler;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignEntrantDocument;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import ru.tandemservice.unienr14.settings.entity.EnrTargetAdmissionOrgUnit;

import java.util.Collection;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 4/24/13
 */
@Configuration
public class EnrEntrantRequestManager extends BusinessObjectManager
{
    public static final String BIND_PROGRAM_SET = "programSet";
    public static final String BIND_TARGET_ADMISSION_KIND = "targetAdmissionKind";
    public static final String BIND_FILTERED_STATE_EXAM_COMPETITIONS_IDS = "filteredStateExamCompetitionIds";
    public static final String BIND_FORM = "eduForm";
    public static final String BIND_ORG_UNIT = "orgUnit";
    public static final String BIND_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String BIND_EDU_DOCUMENT_LEVEL = "eduDocLevel";
    public static final String BIND_SUBJECT_LIST = "programSubject";
    public static final String BIND_COMPETITION_TYPE_LIST = "competitionType";
    public static final String BIND_EDU_LEVEL_REQ_LIST = "eduLevelReqList";
    public static final String BIND_ENTRANT_ID = EnrEntrantManager.BIND_ENTRANT_ID;
    public static final String BIND_REQUEST_TYPE_ID = "requestTypeId";
    public static final String DS_BENEFIT_CATEGORY_PREFERENCE = "benefitCategoryPreferenceDS";
    public static final String DS_BENEFIT_PROOF_DOC_PREFERENCE = "benefitProofDocPreferenceDS";
    public static final String DS_BENEFIT_CATEGORY_EXCLUSIVE = "benefitCategoryExclusiveDS";
    public static final String DS_BENEFIT_PROOF_DOC_EXCLUSIVE = "benefitProofDocExclusiveDS";
    public static final String DS_BENEFIT_CATEGORY_NO_EXAM = "benefitCategoryNoExamDS";
    public static final String DS_BENEFIT_PROOF_DOC_NO_EXAM = "benefitProofDocNoExamDS";
    public static final String DS_FORM = "formDS";
    public static final String DS_ORG_UNIT = "orgUnitDS";
    public static final String DS_FORMATIVE_ORG_UNIT = "formativeOrgUnitDS";
    public static final String DS_SUBJECT = "subjectDS";
    public static final String DS_COMPETITION_TYPE = "competitionTypeDS";
    public static final String DS_EDU_LEVEL_REQUIREMENT = "eduLevelRequirementDS";
    public static final String DS_ENTRANT_REQUEST = "entrantRequestDS";
    public static final String DS_EXTERNAL_OU = "externalOrgUnitDS";

    public static EnrEntrantRequestManager instance()
    {
        return instance(EnrEntrantRequestManager.class);
    }

    @Bean
    public IEnrEntrantRequestDao dao()
    {
        return new EnrEntrantRequestDao();
    }

    @Bean
    public ItemListExtPoint<ICompetitionContextCustomizer> competitionCustomizer()
    {
        return itemList(ICompetitionContextCustomizer.class)
                .create();
    }

    @Bean
    public IFormatter<String> getEntrantRequestNumberFormatter() {
        return new IFormatter<String>() {
            @Override public String format(String number) {
                return number == null ? "" : number;
            }
        };
    }

    public static final String REQUEST_TYPE_DS = EnrRequestTypeManager.DS_REQUEST_TYPE;

    /**
     * ДС1: выводить названия активных элементов справочника в порядке возрастания их приоритетов
     *
     * @deprecated use EnrRequestTypeManager.instance().requestTypeEnabledDSConfig() instead
     */
    @Bean
    @Deprecated
    public UIDataSourceConfig requestTypeDS_1_Config()
    {
        return EnrRequestTypeManager.instance().requestTypeEnabledDSConfig();
    }

    /**
     * ДС2: выводить названия активных элементов справочника, связанных с видами ОП высшего образования, в порядке возрастания их приоритетов
     * @deprecated use EnrRequestTypeManager.instance().requestTypeProgramKindHighDSConfig() instead
     */
    @Bean
    @Deprecated
    public UIDataSourceConfig requestTypeDS_2_Config()
    {
        return EnrRequestTypeManager.instance().requestTypeProgramKindHighDSConfig();
    }

    /**
     * ДС1: выводить названия активных элементов справочника в порядке возрастания их приоритетов
     * @deprecated use EnrRequestTypeManager.instance().requestTypeEnabledDSHandler() instead
     */
    @Bean
    @Deprecated
    public IDefaultComboDataSourceHandler requestTypeDS_1_Handler()
    {
        return EnrRequestTypeManager.instance().requestTypeEnabledDSHandler();
    }

    /**
     * ДС2: выводить названия активных элементов справочника, связанных с видами ОП высшего образования, в порядке возрастания их приоритетов
     * @deprecated use EnrRequestTypeManager.instance().requestTypeProgramKindHighDSHandler() instead
     */
    @Bean
    @Deprecated
    public IDefaultComboDataSourceHandler requestTypeDS_2_Handler()
    {
        return EnrRequestTypeManager.instance().requestTypeProgramKindHighDSHandler();
    }

    @Bean
    public INumberGenerationRule<EnrEntrantRequest> requestNumberGenerationRule() {
        return new EnrNumberGenerationRule<>(EnrEntrantRequest.class, EnrEntrantRequest.regNumber(), DefaultNumberGenerationRule.toString(EnrEntrantRequest.entrant().enrollmentCampaign().id()));
    }

    public static final String ORIGINAL_SUBMISSION_AND_RETURN_WAY_DS = "originalSubmissionAndReturnWayDS";


    @Bean
    public UIDataSourceConfig originalSubmissionAndReturnWayDSConfig()
    {
        return SelectDSConfig.with(ORIGINAL_SUBMISSION_AND_RETURN_WAY_DS, this.getName())
        .dataSourceClass(SelectDataSource.class)
        .handler(originalSubmissionAndReturnWayDSHandler())
        .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler originalSubmissionAndReturnWayDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), EnrOriginalSubmissionAndReturnWay.class)
        .filter(EnrOriginalSubmissionAndReturnWay.title())
        .order(EnrOriginalSubmissionAndReturnWay.code());
    }

    /**
     * Билдер запроса выбора конкурсов.
     * Используется как в таблице возможных для выбора конкурсов, так и в фильтрах для того, чтобы оставить там только значения, для которых есть конкурс.
     * В фильтрах параметры накидываются по мере зависимости одного от другого.
     */
    public static class CompetitionDQLBuilder
    {
        private EnrEnrollmentCampaign campaign;
        private EnrRequestType requestType;
        private EduLevel eduDocumentLevel;
        private Boolean receiveEduLevelFirst;
        private EduProgramForm programForm;
        private EnrOrgUnit orgUnit;
        private OrgUnit formativeOrgUnit;
        private List<EnrEduLevelRequirement> eduLevelRequirementList;
        private List<EduProgramSubject> subjectList;
        private List<EnrCompetitionType> competitionTypeList;
        private boolean onlyCrimea;

        public CompetitionDQLBuilder campaign(EnrEnrollmentCampaign campaign) { this.campaign = campaign; return this; }
        public CompetitionDQLBuilder requestType(EnrRequestType requestType) { this.requestType = requestType; return this; }
        public CompetitionDQLBuilder receiveEduLevelFirst(Boolean receiveEduLevelFirst) { this.receiveEduLevelFirst = receiveEduLevelFirst; return this; }
        public CompetitionDQLBuilder eduDocumentLevel(EduLevel eduDocumentLevel) { this.eduDocumentLevel = eduDocumentLevel; return this; }
        public CompetitionDQLBuilder programForm(EduProgramForm programForm) { this.programForm = programForm; return this; }
        public CompetitionDQLBuilder orgUnit(EnrOrgUnit orgUnit) { this.orgUnit = orgUnit; return this; }
        public CompetitionDQLBuilder formativeOrgUnit(OrgUnit formativeOrgUnit) { this.formativeOrgUnit = formativeOrgUnit; return this; }
        public CompetitionDQLBuilder eduLevelRequirementList(List<EnrEduLevelRequirement> eduLevelRequirementList) { this.eduLevelRequirementList = eduLevelRequirementList; return this; }
        public CompetitionDQLBuilder subjectList(List<EduProgramSubject> subjectList) { this.subjectList = subjectList; return this; }
        public CompetitionDQLBuilder competitionTypeList(List<EnrCompetitionType> competitionTypeList) { this.competitionTypeList = competitionTypeList; return this; }
        public CompetitionDQLBuilder onlyCrimea(boolean onlyCrimea) {this.onlyCrimea = onlyCrimea; return this; }

        public DQLSelectBuilder build(String alias)
        {
            return build(alias, null);
        }

        public DQLSelectBuilder build(String alias, ExecutionContext context)
        {
            final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EnrCompetition.class, alias);

            if (requestType != null && receiveEduLevelFirst != null) {
                dql.where(eq(property(alias, EnrCompetition.programSetOrgUnit().programSet().enrollmentCampaign()), value(campaign)));
                dql.where(eq(property(alias, EnrCompetition.requestType()), value(requestType)));

                if (eduDocumentLevel != null) {
                    dql.where(exists(
                            EnrEduLevelReqEduLevel.class,
                            EnrEduLevelReqEduLevel.eduLevel().s(), eduDocumentLevel,
                            EnrEduLevelReqEduLevel.enrEduLevelRequirement().s(), property(alias, EnrCompetition.eduLevelRequirement())
                    ));
                }

                if (!receiveEduLevelFirst) {
                    dql.where(eq(property(alias, EnrCompetition.type().compensationType().code()), value(CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT)));
                }

                CommonBaseFilterUtil.applySelectFilter(dql, alias, EnrCompetition.programSetOrgUnit().orgUnit(), orgUnit);
                CommonBaseFilterUtil.applySelectFilter(dql, alias, EnrCompetition.programSetOrgUnit().programSet().programForm(), programForm);
                CommonBaseFilterUtil.applySelectFilter(dql, alias, EnrCompetition.programSetOrgUnit().formativeOrgUnit(), formativeOrgUnit);
                CommonBaseFilterUtil.applySelectFilter(dql, alias, EnrCompetition.eduLevelRequirement(), eduLevelRequirementList);
                CommonBaseFilterUtil.applySelectFilter(dql, alias, EnrCompetition.programSetOrgUnit().programSet().programSubject(), subjectList);
                CommonBaseFilterUtil.applySelectFilter(dql, alias, EnrCompetition.type(), competitionTypeList);

                if (onlyCrimea)
                    dql.where(eq(property(alias, EnrCompetition.programSetOrgUnit().programSet().acceptPeopleResidingInCrimea()), value(true)));

                if (context != null)
                {
                    // Доп. логика проектных слоев
                    for (ICompetitionContextCustomizer customizer : EnrEntrantRequestManager.instance().competitionCustomizer().getItems().values())
                    {
                        customizer.customizeCompetitionDQL(dql, context, alias);
                    }
                }
            }
            else {
                dql.where(nothing());
            }


            return dql;
        }
    }

    /* Выбор конкурсов */

    @Bean
    public IDefaultComboDataSourceHandler eduProgramFormDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramForm.class) {
            @Override protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context) {
                super.applyWhereConditions(alias, dql, context);

                final DQLSelectBuilder compDQL = new CompetitionDQLBuilder()
                        .campaign(context.<EnrEnrollmentCampaign>get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN))
                        .requestType(context.<EnrRequestType>get(EnrEntrantBaseDocumentManager.BIND_REQUEST_TYPE))
                        .receiveEduLevelFirst(context.<Boolean>get(EnrEntrantBaseDocumentManager.BIND_RECEIVE_EDU_LEVEL_FIRST))
                        .eduDocumentLevel(context.<EduLevel>get(EnrEntrantRequestManager.BIND_EDU_DOCUMENT_LEVEL))
                        .build("comp", context);

                compDQL.where(eq(property("comp", EnrCompetition.programSetOrgUnit().programSet().programForm()), property(alias)));

                dql.where(exists(compDQL.column(value(1)).buildQuery()));

                // напрямую в in добавлять это нельзя, там может быть до 4000 значений для БС (в УрГПУ их около 1000)
                final Collection<Long> filteredCompetitionIds = context.get(EnrEntrantRequestManager.BIND_FILTERED_STATE_EXAM_COMPETITIONS_IDS);
                if (null != filteredCompetitionIds) {
                    dql.where(in(property(alias, "id"), EnrCompetitionManager.instance().stateExamFilterDao().getEduProgramFormIds(filteredCompetitionIds)));
                }
            }
        }
        .order(EduProgramForm.code())
        .filter(EduProgramForm.title())
        .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler orgUnitDSHandler()
    {
        return new EnrOrgUnitBaseDSHandler(getName())
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                final DQLSelectBuilder compDQL = new CompetitionDQLBuilder()
                        .campaign(context.<EnrEnrollmentCampaign>get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN))
                        .requestType(context.<EnrRequestType>get(EnrEntrantBaseDocumentManager.BIND_REQUEST_TYPE))
                        .receiveEduLevelFirst(context.<Boolean>get(EnrEntrantBaseDocumentManager.BIND_RECEIVE_EDU_LEVEL_FIRST))
                        .eduDocumentLevel(context.<EduLevel>get(EnrEntrantRequestManager.BIND_EDU_DOCUMENT_LEVEL))
                        .programForm(context.<EduProgramForm>get(EnrEntrantRequestManager.BIND_FORM))
                        .build("comp", context);

                compDQL.where(eq(property("comp", EnrCompetition.programSetOrgUnit().orgUnit()), property(alias)));

                dql.where(exists(compDQL.column(value(1)).buildQuery()));

                // напрямую в in добавлять это нельзя, там может быть до 4000 значений для БС (в УрГПУ их около 1000)
                final Collection<Long> filteredCompetitionIds = context.get(EnrEntrantRequestManager.BIND_FILTERED_STATE_EXAM_COMPETITIONS_IDS);
                if (null != filteredCompetitionIds) {
                    dql.where(in(property(alias, "id"), EnrCompetitionManager.instance().stateExamFilterDao().getProgramSetOrgUnitOuIds(filteredCompetitionIds)));
                }

            }
        }
        .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler formativeOrgUnitDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), OrgUnit.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                final DQLSelectBuilder compDQL = new CompetitionDQLBuilder()
                        .campaign(context.<EnrEnrollmentCampaign>get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN))
                        .requestType(context.<EnrRequestType>get(EnrEntrantBaseDocumentManager.BIND_REQUEST_TYPE))
                        .receiveEduLevelFirst(context.<Boolean>get(EnrEntrantBaseDocumentManager.BIND_RECEIVE_EDU_LEVEL_FIRST))
                        .eduDocumentLevel(context.<EduLevel>get(EnrEntrantRequestManager.BIND_EDU_DOCUMENT_LEVEL))
                        .programForm(context.<EduProgramForm>get(EnrEntrantRequestManager.BIND_FORM))
                        .orgUnit(context.<EnrOrgUnit>get(EnrEntrantRequestManager.BIND_ORG_UNIT))
                        .build("comp", context);

                compDQL.where(eq(property("comp", EnrCompetition.programSetOrgUnit().formativeOrgUnit()), property(alias)));

                dql.where(exists(compDQL.column(value(1)).buildQuery()));

                // напрямую в in добавлять это нельзя, там может быть до 4000 значений для БС (в УрГПУ их около 1000)
                final Collection<Long> filteredCompetitionIds = context.get(EnrEntrantRequestManager.BIND_FILTERED_STATE_EXAM_COMPETITIONS_IDS);
                if (null != filteredCompetitionIds) {
                    dql.where(in(property(alias, "id"), EnrCompetitionManager.instance().stateExamFilterDao().getProgramSetOrgUnitFormativeOuIds(filteredCompetitionIds)));
                }

            }
        }
        .filter(OrgUnit.fullTitle())
        .order(OrgUnit.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler subjectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSubject.class) {
            @Override protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context) {
                super.applyWhereConditions(alias, dql, context);

                final DQLSelectBuilder compDQL = new CompetitionDQLBuilder()
                        .campaign(context.<EnrEnrollmentCampaign>get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN))
                        .requestType(context.<EnrRequestType>get(EnrEntrantBaseDocumentManager.BIND_REQUEST_TYPE))
                        .receiveEduLevelFirst(context.<Boolean>get(EnrEntrantBaseDocumentManager.BIND_RECEIVE_EDU_LEVEL_FIRST))
                        .eduDocumentLevel(context.<EduLevel>get(EnrEntrantRequestManager.BIND_EDU_DOCUMENT_LEVEL))
                        .programForm(context.<EduProgramForm>get(EnrEntrantRequestManager.BIND_FORM))
                        .orgUnit(context.<EnrOrgUnit>get(EnrEntrantRequestManager.BIND_ORG_UNIT))
                        .formativeOrgUnit(context.<OrgUnit>get(EnrEntrantRequestManager.BIND_FORMATIVE_ORG_UNIT))
                        .build("comp", context);

                compDQL.where(eq(property("comp", EnrCompetition.programSetOrgUnit().programSet().programSubject()), property(alias)));

                dql.where(exists(compDQL.column(value(1)).buildQuery()));

                // напрямую в in добавлять это нельзя, там может быть до 4000 значений для БС (в УрГПУ их около 1000)
                final Collection<Long> filteredCompetitionIds = context.get(EnrEntrantRequestManager.BIND_FILTERED_STATE_EXAM_COMPETITIONS_IDS);
                if (null != filteredCompetitionIds) {
                    dql.where(in(property(alias, "id"), EnrCompetitionManager.instance().stateExamFilterDao().getEduProgramSubjectIds(filteredCompetitionIds)));
                }
            }
        }
        .order(EduProgramSubject.code())
        .order(EduProgramSubject.title())
        .filter(EduProgramSubject.code())
        .filter(EduProgramSubject.title())
        .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler eduLevelRequirementDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrEduLevelRequirement.class) {

            @Override protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                final DQLSelectBuilder compDQL = new CompetitionDQLBuilder()
                        .campaign(context.<EnrEnrollmentCampaign>get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN))
                        .requestType(context.<EnrRequestType>get(EnrEntrantBaseDocumentManager.BIND_REQUEST_TYPE))
                        .receiveEduLevelFirst(context.<Boolean>get(EnrEntrantBaseDocumentManager.BIND_RECEIVE_EDU_LEVEL_FIRST))
                        .eduDocumentLevel(context.<EduLevel>get(EnrEntrantRequestManager.BIND_EDU_DOCUMENT_LEVEL))
                        .programForm(context.<EduProgramForm>get(EnrEntrantRequestManager.BIND_FORM))
                        .orgUnit(context.<EnrOrgUnit>get(EnrEntrantRequestManager.BIND_ORG_UNIT))
                        .formativeOrgUnit(context.<OrgUnit>get(EnrEntrantRequestManager.BIND_FORMATIVE_ORG_UNIT))
                        .subjectList(context.<List<EduProgramSubject>>get(EnrEntrantRequestManager.BIND_SUBJECT_LIST))
                        .build("comp", context);

                compDQL.where(eq(property("comp", EnrCompetition.eduLevelRequirement()), property(alias)));

                dql.where(exists(compDQL.column(value(1)).buildQuery()));

                // напрямую в in добавлять это нельзя, там может быть до 4000 значений для БС (в УрГПУ их около 1000)
                final Collection<Long> filteredCompetitionIds = context.get(EnrEntrantRequestManager.BIND_FILTERED_STATE_EXAM_COMPETITIONS_IDS);
                if (null != filteredCompetitionIds) {
                    dql.where(in(property(alias, "id"), EnrCompetitionManager.instance().stateExamFilterDao().getCompetitionEduLevelRequirementIds(filteredCompetitionIds)));
                }
            }
        }
        .order(EnrEduLevelRequirement.code())
        .filter(EnrEduLevelRequirement.title())
        .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler competitionTypeDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrCompetitionType.class) {

            @Override protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                final DQLSelectBuilder compDQL = new CompetitionDQLBuilder()
                        .campaign(context.<EnrEnrollmentCampaign>get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN))
                        .requestType(context.<EnrRequestType>get(EnrEntrantBaseDocumentManager.BIND_REQUEST_TYPE))
                        .receiveEduLevelFirst(context.<Boolean>get(EnrEntrantBaseDocumentManager.BIND_RECEIVE_EDU_LEVEL_FIRST))
                        .eduDocumentLevel(context.<EduLevel>get(EnrEntrantRequestManager.BIND_EDU_DOCUMENT_LEVEL))
                        .programForm(context.<EduProgramForm>get(EnrEntrantRequestManager.BIND_FORM))
                        .orgUnit(context.<EnrOrgUnit>get(EnrEntrantRequestManager.BIND_ORG_UNIT))
                        .formativeOrgUnit(context.<OrgUnit>get(EnrEntrantRequestManager.BIND_FORMATIVE_ORG_UNIT))
                        .subjectList(context.<List<EduProgramSubject>>get(EnrEntrantRequestManager.BIND_SUBJECT_LIST))
                        .eduLevelRequirementList(context.<List<EnrEduLevelRequirement>>get(EnrEntrantRequestManager.BIND_EDU_LEVEL_REQ_LIST))
                        .build("comp", context);

                compDQL.where(eq(property("comp", EnrCompetition.type()), property(alias)));

                dql.where(exists(compDQL.column(value(1)).buildQuery()));

                // напрямую в in добавлять это нельзя, там может быть до 4000 значений для БС (в УрГПУ их около 1000)
                final Collection<Long> filteredCompetitionIds = context.get(EnrEntrantRequestManager.BIND_FILTERED_STATE_EXAM_COMPETITIONS_IDS);
                if (null != filteredCompetitionIds) {
                    dql.where(in(property(alias, "id"), EnrCompetitionManager.instance().stateExamFilterDao().getCompetitionTypeIds(filteredCompetitionIds)));
                }
            }
        }
        .order(EnrCompetitionType.code())
        .filter(EnrCompetitionType.title())
        .pageable(true);
    }

    /* Выбор конкурсов */

    @Bean
    public IDefaultComboDataSourceHandler targetAdmissionOrgUnitSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), ExternalOrgUnit.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(in(property(alias, ExternalOrgUnit.id()), new DQLSelectBuilder()
                .fromEntity(EnrTargetAdmissionOrgUnit.class, "taou")
                .column(property("taou", EnrTargetAdmissionOrgUnit.externalOrgUnit().id()))
                .where(eq(property("taou", EnrTargetAdmissionOrgUnit.enrCampaignTAKind()), commonValue(context.get(EnrEntrantRequestManager.BIND_TARGET_ADMISSION_KIND))))
                .buildQuery()
                ));
            }
        }
        .order(ExternalOrgUnit.title())
        .filter(ExternalOrgUnit.title())
        .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler programSetItemDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrProgramSetItem.class)
        .where(EnrProgramSetItem.programSet(), EnrEntrantRequestManager.BIND_PROGRAM_SET)
        .order(EnrProgramSetItem.program().title())
        .filter(EnrProgramSetItem.program().title())
        .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler benefitProofDocDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), IEnrEntrantBenefitProofDocument.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                String benefitTypeCode = context.get(EnrCatalogCommonsManager.PARAM_BENEFIT_TYPE_CODE);
                EnrEnrollmentCampaign enrollmentCampaign = context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);

                dql
                .where(eq(property(alias, IEnrEntrantBenefitProofDocumentGen.entrant().id()), commonValue(context.get("entrant"))))
                .where(in(
                    property(alias, IEnrEntrantBenefitProofDocumentGen.documentType().id()),
                    new DQLSelectBuilder()
                    .fromEntity(EnrCampaignEntrantDocument.class, "ced")
                    .column(property("ced", EnrCampaignEntrantDocument.documentType().id()))
                    .where(eq(property("ced", EnrCampaignEntrantDocument.enrollmentCampaign()), value(enrollmentCampaign)))
                    .where(eq(property("ced", EnrCampaignEntrantDocument.getBenefitProperty(benefitTypeCode)), value(Boolean.TRUE)))
                    .buildQuery()
                ));
            }
        }
        .filter(IEnrEntrantBenefitProofDocumentGen.documentType().title())
        .filter(IEnrEntrantBenefitProofDocumentGen.seria())
        .filter(IEnrEntrantBenefitProofDocumentGen.number())
        .order(IEnrEntrantBenefitProofDocumentGen.documentType().title())
        .order(IEnrEntrantBenefitProofDocumentGen.seria())
        .order(IEnrEntrantBenefitProofDocumentGen.number());
    }

    @Bean
    public IDefaultComboDataSourceHandler acceptRequestDSHandler()
    {
        return new EntrantRequestAcceptPrincipalHandler(getName());
    }

    @Bean
    public UIDataSourceConfig entrantRequestDSConfig()
    {
        return SelectDSConfig.with(DS_ENTRANT_REQUEST, this.getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(entrantRequestDSHandler())
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler entrantRequestDSHandler()
    {
        return EnrEntrantRequest.defaultSelectDSHandler(getName())
                .customize((alias, dql, context, filter) ->
                {
                    Long entrantId = context.getNotNull(BIND_ENTRANT_ID);
                    Long requestTypeId = context.get(BIND_REQUEST_TYPE_ID);
                    dql.where(eq(property(alias, EnrEntrantRequest.entrant().id()), value(entrantId)));
                    if(requestTypeId != null)
                    {
                        dql.where(eq(property(alias, EnrEntrantRequest.type().id()), value(requestTypeId)));
                    }

                    return dql;
                });
    }

    @Bean
    public UIDataSourceConfig externalOrgUnitDSConfig()
    {
        return SelectDSConfig.with(DS_EXTERNAL_OU, this.getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(externalOrgUnitDS())
                .addColumn(ExternalOrgUnit.titleWithLegalForm().s())
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler externalOrgUnitDS()
    {
        return new ExternalOrgUnitDSHandler(getName());
    }
}
