/* $Id:$ */
package ru.tandemservice.unienr14.order.bo.EnrOrder.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.block.BlockListExtPoint;
import org.tandemframework.caf.ui.config.button.ButtonListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.unienr14.catalog.bo.EnrCatalogOrders.EnrCatalogOrdersManager;
import ru.tandemservice.unienr14.catalog.bo.EnrRequestType.EnrRequestTypeManager;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderBasic;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderPrintFormType;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderReason;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.EnrEntrantRequestManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrOrderReasonToBasicsRel;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 5/17/13
 */
@Configuration
public class EnrOrderAddEdit extends BusinessComponentManager
{
    public static final String PARAM_ORDER_TYPE = "orderType";
    public static final String PARAM_ORDER_REASON = "orderReason";

    public static final String BLOCK_LIST = "blockList";
    public static final String BUTTON_LIST = "buttonList";
    public static final String APPLY_BUTTON = "onClickApply";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
            .addDataSource(selectDS("orderTypeDS", EnrCatalogOrdersManager.instance().orderTypeDSHandler()))
            .addDataSource(selectDS("printFormTypeDS", printFormTypeDSHandler()))
            .addDataSource(EnrRequestTypeManager.instance().requestTypeEnrCampaignDSConfig())
            .addDataSource(selectDS("compensationTypeDS", compensationTypeDSHandler()).addColumn(CompensationType.shortTitle().s()))
            .addDataSource(selectDS("reasonDS", reasonDSHandler()))
            .addDataSource(selectDS("basicDS", basicDSHandler()))
            .create();
    }

    @Bean
    public BlockListExtPoint orderCardBlockList(){
        return blockListExtPointBuilder(BLOCK_LIST)
            .create();
    }

    @Bean
    public ButtonListExtPoint applyButtonList() {
        return buttonListExtPointBuilder(BUTTON_LIST)
            .addButton(submitButton(APPLY_BUTTON, "onClickApply").renderAsLink(false))
            .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler printFormTypeDSHandler() {
        return new EntityComboDataSourceHandler(getName(), EnrOrderPrintFormType.class)
            .where(EnrOrderPrintFormType.orderType(), PARAM_ORDER_TYPE)
            .where(EnrOrderPrintFormType.used(), Boolean.TRUE)
            .order(EnrOrderPrintFormType.priority())
            .filter(EnrOrderPrintFormType.title())
            ;
    }

    @Bean
    public IDefaultComboDataSourceHandler compensationTypeDSHandler() {
        return new EntityComboDataSourceHandler(getName(), CompensationType.class)
            .order(CompensationType.code())
            .filter(CompensationType.shortTitle())
            ;
    }

    @Bean
    public IDefaultComboDataSourceHandler reasonDSHandler() {
        return new EntityComboDataSourceHandler(getName(), EnrOrderReason.class)
            .where(EnrOrderReason.orderType(), PARAM_ORDER_TYPE)
            .order(EnrOrderReason.title())
            .filter(EnrOrderReason.title())
            ;
    }

    @Bean
    public IDefaultComboDataSourceHandler basicDSHandler() {
        return new EntityComboDataSourceHandler(getName(), EnrOrderBasic.class) {
            @Override protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context) {
                super.applyWhereConditions(alias, dql, context);

                EnrOrderReason reason = context.get(PARAM_ORDER_REASON);

                if (null == reason) {
                    dql.where(isNull(property(alias, "id")));
                } else {
                    dql.where(exists(new DQLSelectBuilder()
                        .fromEntity(EnrOrderReasonToBasicsRel.class, "rel").column("rel.id")
                        .where(eq(property("rel", EnrOrderReasonToBasicsRel.basic()), property(alias)))
                        .where(eq(property("rel", EnrOrderReasonToBasicsRel.reason()), value(reason)))
                        .buildQuery()));
                }
            }
        }
            .where(EnrOrderBasic.orderType(), PARAM_ORDER_TYPE)
            .order(EnrOrderBasic.title())
            .filter(EnrOrderBasic.title())
            ;
    }
}