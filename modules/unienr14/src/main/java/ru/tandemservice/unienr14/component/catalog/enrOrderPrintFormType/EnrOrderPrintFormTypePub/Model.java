/* $Id$ */
package ru.tandemservice.unienr14.component.catalog.enrOrderPrintFormType.EnrOrderPrintFormTypePub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultScriptCatalogPub.DefaultScriptCatalogPubModel;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderPrintFormType;

/**
 * @author azhebko
 * @since 15.07.2014
 */
public class Model extends DefaultScriptCatalogPubModel<EnrOrderPrintFormType>
{
}