/* $Id: MS_unienr14_2x6x2_30to31.java 36491 2014-07-18 12:37:05Z azhebko $ */
package ru.tandemservice.unienr14.migration;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.IDBConnect;
import org.tandemframework.dbsupport.ddl.ResultRowProcessor;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Проверка ограничения enrExamPassDiscipline.enrExamPassDisciplinePassForm ( passForm.internal = true )
 * @author zhuj
 * @since 18.07.2014
 */
public class MS_unienr14_2x6x2_31to32 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
        {
            new ScriptDependency("org.tandemframework", "1.6.15"),
            new ScriptDependency("org.tandemframework.shared", "1.6.2")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        tool.executeQuery(new ResultRowProcessor<Object>(null) {
            @Override protected void processRow(IDBConnect ctool, ResultSet rs, Object result) throws SQLException {
                // кто-то умудрился сделать ДДС не на внутреннюю форму сдачи - надо решить, что с этим делать
                throw new IllegalStateException("enr14_exam_pass_discipline_t: id = "+rs.getLong(1)+", passForm.internal != true");
            }
        }, "select id from enr14_exam_pass_discipline_t where passForm_id not in (select id from enr14_c_exam_pass_form_t where internal_p=?)", Boolean.TRUE);
    }
}