package ru.tandemservice.unienr14.competition.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unienr14.competition.entity.EnrExamSetVariant;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetMaster;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Набор ОП для приема (магистратура)
 *
 * Определяет перечень ОП, на который формируются конкурсы, для приема в магистратуру.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrProgramSetMasterGen extends EnrProgramSetBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.competition.entity.EnrProgramSetMaster";
    public static final String ENTITY_NAME = "enrProgramSetMaster";
    public static final int VERSION_HASH = -398944463;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXAM_SET_VARIANT = "examSetVariant";
    public static final String L_EXAM_SET_VARIANT_MIN = "examSetVariantMin";
    public static final String L_EXAM_SET_VARIANT_CON = "examSetVariantCon";

    private EnrExamSetVariant _examSetVariant;     // Набор ВИ
    private EnrExamSetVariant _examSetVariantMin;     // Набор ВИ на бюджет (старый)
    private EnrExamSetVariant _examSetVariantCon;     // Набор ВИ на договор (старый)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Набор ВИ. Свойство не может быть null.
     */
    @NotNull
    public EnrExamSetVariant getExamSetVariant()
    {
        return _examSetVariant;
    }

    /**
     * @param examSetVariant Набор ВИ. Свойство не может быть null.
     */
    public void setExamSetVariant(EnrExamSetVariant examSetVariant)
    {
        dirty(_examSetVariant, examSetVariant);
        _examSetVariant = examSetVariant;
    }

    /**
     * @return Набор ВИ на бюджет (старый). Свойство не может быть null.
     */
    @NotNull
    public EnrExamSetVariant getExamSetVariantMin()
    {
        return _examSetVariantMin;
    }

    /**
     * @param examSetVariantMin Набор ВИ на бюджет (старый). Свойство не может быть null.
     */
    public void setExamSetVariantMin(EnrExamSetVariant examSetVariantMin)
    {
        dirty(_examSetVariantMin, examSetVariantMin);
        _examSetVariantMin = examSetVariantMin;
    }

    /**
     * @return Набор ВИ на договор (старый). Свойство не может быть null.
     */
    @NotNull
    public EnrExamSetVariant getExamSetVariantCon()
    {
        return _examSetVariantCon;
    }

    /**
     * @param examSetVariantCon Набор ВИ на договор (старый). Свойство не может быть null.
     */
    public void setExamSetVariantCon(EnrExamSetVariant examSetVariantCon)
    {
        dirty(_examSetVariantCon, examSetVariantCon);
        _examSetVariantCon = examSetVariantCon;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EnrProgramSetMasterGen)
        {
            setExamSetVariant(((EnrProgramSetMaster)another).getExamSetVariant());
            setExamSetVariantMin(((EnrProgramSetMaster)another).getExamSetVariantMin());
            setExamSetVariantCon(((EnrProgramSetMaster)another).getExamSetVariantCon());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrProgramSetMasterGen> extends EnrProgramSetBase.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrProgramSetMaster.class;
        }

        public T newInstance()
        {
            return (T) new EnrProgramSetMaster();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "examSetVariant":
                    return obj.getExamSetVariant();
                case "examSetVariantMin":
                    return obj.getExamSetVariantMin();
                case "examSetVariantCon":
                    return obj.getExamSetVariantCon();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "examSetVariant":
                    obj.setExamSetVariant((EnrExamSetVariant) value);
                    return;
                case "examSetVariantMin":
                    obj.setExamSetVariantMin((EnrExamSetVariant) value);
                    return;
                case "examSetVariantCon":
                    obj.setExamSetVariantCon((EnrExamSetVariant) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "examSetVariant":
                        return true;
                case "examSetVariantMin":
                        return true;
                case "examSetVariantCon":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "examSetVariant":
                    return true;
                case "examSetVariantMin":
                    return true;
                case "examSetVariantCon":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "examSetVariant":
                    return EnrExamSetVariant.class;
                case "examSetVariantMin":
                    return EnrExamSetVariant.class;
                case "examSetVariantCon":
                    return EnrExamSetVariant.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrProgramSetMaster> _dslPath = new Path<EnrProgramSetMaster>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrProgramSetMaster");
    }
            

    /**
     * @return Набор ВИ. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetMaster#getExamSetVariant()
     */
    public static EnrExamSetVariant.Path<EnrExamSetVariant> examSetVariant()
    {
        return _dslPath.examSetVariant();
    }

    /**
     * @return Набор ВИ на бюджет (старый). Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetMaster#getExamSetVariantMin()
     */
    public static EnrExamSetVariant.Path<EnrExamSetVariant> examSetVariantMin()
    {
        return _dslPath.examSetVariantMin();
    }

    /**
     * @return Набор ВИ на договор (старый). Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetMaster#getExamSetVariantCon()
     */
    public static EnrExamSetVariant.Path<EnrExamSetVariant> examSetVariantCon()
    {
        return _dslPath.examSetVariantCon();
    }

    public static class Path<E extends EnrProgramSetMaster> extends EnrProgramSetBase.Path<E>
    {
        private EnrExamSetVariant.Path<EnrExamSetVariant> _examSetVariant;
        private EnrExamSetVariant.Path<EnrExamSetVariant> _examSetVariantMin;
        private EnrExamSetVariant.Path<EnrExamSetVariant> _examSetVariantCon;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Набор ВИ. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetMaster#getExamSetVariant()
     */
        public EnrExamSetVariant.Path<EnrExamSetVariant> examSetVariant()
        {
            if(_examSetVariant == null )
                _examSetVariant = new EnrExamSetVariant.Path<EnrExamSetVariant>(L_EXAM_SET_VARIANT, this);
            return _examSetVariant;
        }

    /**
     * @return Набор ВИ на бюджет (старый). Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetMaster#getExamSetVariantMin()
     */
        public EnrExamSetVariant.Path<EnrExamSetVariant> examSetVariantMin()
        {
            if(_examSetVariantMin == null )
                _examSetVariantMin = new EnrExamSetVariant.Path<EnrExamSetVariant>(L_EXAM_SET_VARIANT_MIN, this);
            return _examSetVariantMin;
        }

    /**
     * @return Набор ВИ на договор (старый). Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetMaster#getExamSetVariantCon()
     */
        public EnrExamSetVariant.Path<EnrExamSetVariant> examSetVariantCon()
        {
            if(_examSetVariantCon == null )
                _examSetVariantCon = new EnrExamSetVariant.Path<EnrExamSetVariant>(L_EXAM_SET_VARIANT_CON, this);
            return _examSetVariantCon;
        }

        public Class getEntityClass()
        {
            return EnrProgramSetMaster.class;
        }

        public String getEntityName()
        {
            return "enrProgramSetMaster";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
