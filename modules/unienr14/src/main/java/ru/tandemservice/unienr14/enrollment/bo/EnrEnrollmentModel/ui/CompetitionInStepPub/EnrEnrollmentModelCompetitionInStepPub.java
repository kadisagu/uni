/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.ui.CompetitionInStepPub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.core.view.formatter.LongAsDoubleFormatter;
import org.tandemframework.core.view.list.column.DefaultPublisherLinkResolver;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import ru.tandemservice.unienr14.enrollment.entity.EnrModelStepItem;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.Pub.EnrEntrantPub;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;

/**
 * @author oleyba
 * @since 3/27/15
 */
@Configuration
public class EnrEnrollmentModelCompetitionInStepPub extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(searchListDS("itemDS", competitionDSColumns(), competitionDSHandler()))
            .create();
    }

    @Bean
    public ColumnListExtPoint competitionDSColumns()
    {
        return columnListExtPointBuilder("itemDS")
            .addColumn(textColumn("position", EnrModelStepItemDSHandler.VIEW_PROP_RELATIVE_POSITION))
            .addColumn(textColumn("absPos", EnrModelStepItemDSHandler.VIEW_PROP_RELATIVE_RANK))
            .addColumn(publisherColumn("fio", EnrEntrant.person().identityCard().fullFio())
                .entityListProperty(EnrModelStepItem.entity().requestedCompetition().request().entrant().s())
                .publisherLinkResolver(new DefaultPublisherLinkResolver()
                {
                    @Override
                    public String getComponentName(IEntity entity) { return EnrEntrantPub.class.getSimpleName(); }

                    @Override
                    public Object getParameters(IEntity entity)
                    {
                        return new ParametersMap()
                            .add(PublisherActivator.PUBLISHER_ID_KEY, ((EnrEntrant) entity).getId())
                            .add("selectedTab", "ratingTab");
                    }
                }))
            .addColumn(textColumn("totalMark", EnrModelStepItem.entity().totalMarkAsLong()).formatter(LongAsDoubleFormatter.LONG_AS_DOUBLE_FORMATTER_3_SCALE))
            .addColumn(publisherColumn("otherEnrMark", "title")
                .entityListProperty(EnrModelStepItemDSHandler.VIEW_PROP_OTHER_ENR_SELECTION)
                .formatter(CollectionFormatter.COLLECTION_FORMATTER)
                .publisherLinkResolver(new IPublisherLinkResolver()
                {
                    @Override
                    public Object getParameters(IEntity entity)
                    {
                        return ((EnrModelStepItemDSHandler.OtherEnrollmentWrapper) entity).getPublisherParameters();
                    }

                    @Override
                    public String getComponentName(IEntity entity)
                    {
                        return EnrEnrollmentModelCompetitionInStepPub.class.getSimpleName();
                    }
                }))
            .addColumn(textColumn("priority", EnrModelStepItem.entity().requestedCompetition().priority().s()))
            .addColumn(booleanColumn("parallel", EnrModelStepItem.entity().requestedCompetition().parallel().s()))
            .addColumn(toggleColumn("includedEdit", EnrModelStepItem.included())
                .toggleOnListener("onInclude").toggleOnLabel("Включить абитуриента в конкурсный список")
                .toggleOffListener("onExclude").toggleOffLabel("Исключить абитуриента из конкурсного списка")
                .printable(true)
                .visible("ui:editAllowed")
                .create())
            .addColumn(booleanColumn("included", EnrModelStepItem.included())
                .printable(true)
                .visible("ui:editNotAllowed")
                .create())
            .addColumn(textColumn("state", EnrModelStepItem.entity().requestedCompetition().state().stateDaemonSafe()))
            .addColumn(booleanColumn("originalIn", EnrModelStepItem.originalIn())
                .printable(true)
                .create())
            .addColumn(booleanColumn("accepted", EnrModelStepItem.accepted())
                .printable(true)
                .create())
            .addColumn(toggleColumn("enrollmentAvailableEdit", EnrModelStepItem.enrollmentAvailable())
                .toggleOnListener("onSetEnrollmentAvailable").toggleOnLabel("Включить итоговое согласие")
                .toggleOffListener("onClearEnrollmentAvailable").toggleOffLabel("Выключить итоговое согласие")
                .visible("ui:editAllowed")
                .printable(true)
                .create())
            .addColumn(booleanColumn("enrollmentAvailable", EnrModelStepItem.enrollmentAvailable())
                .visible("ui:editNotAllowed")
                .printable(true)
                .create())
            .addColumn(toggleColumn("enrolledEdit", EnrModelStepItem.enrolled())
                .toggleOnListener("onSetEnrolled").toggleOnLabel("Зачислить")
                .toggleOffListener("onClearEnrolled").toggleOffLabel("Не зачислять")
                .printable(true)
                .visible("ui:editAllowed")
                .create())
            .addColumn(booleanColumn("enrolled", EnrModelStepItem.enrolled())
                .printable(true)
                .visible("ui:editNotAllowed")
                .create())
            .addColumn(booleanColumn("reEnrolled", EnrModelStepItem.reEnrolled())
                .printable(true)
                .create())
            .create();
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> competitionDSHandler()
    {
        return new EnrModelStepItemDSHandler(getName());
    }
}