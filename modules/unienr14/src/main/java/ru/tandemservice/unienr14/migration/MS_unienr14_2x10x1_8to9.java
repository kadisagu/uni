/* $Id$ */
package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * @author Alexey Lopatin
 * @since 22.05.2016
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unienr14_2x10x1_8to9 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
		        new ScriptDependency("org.tandemframework", "1.6.18"),
		        new ScriptDependency("org.tandemframework.shared", "1.10.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        Statement stmt = tool.getConnection().createStatement();

        PreparedStatement update = tool.prepareStatement("update enr14_competition_t set fisuidpostfix_p = ? where id = ?");
        ResultSet src = stmt.executeQuery("select comp.id, compType.code_p from enr14_competition_t comp"
                                                 + " inner join enr14_program_set_ou_t ps on ps.id = comp.programsetorgunit_id"
                                                 + " inner join enr14_program_set_base_t psb on psb.id = ps.programset_id"
                                                 + " inner join enr14_campaign_t campaign on campaign.id = psb.enrollmentcampaign_id"
                                                 + " inner join educationyear_t eduYear on eduYear.id = campaign.educationyear_id"
                                                 + " inner join enr14_c_comp_type_t compType on compType.id = comp.type_id"
                                                 + " where eduYear.intvalue_p >= 2016");

        final MigrationUtils.BatchUpdater updater = new MigrationUtils.BatchUpdater("update enr14_competition_t set fisuidpostfix_p = ? where id = ?", DBType.EMPTY_STRING, DBType.LONG);

        while (src.next())
        {
            Long id = src.getLong(1);
            String compTypeCode = src.getString(2);
            String postfix;

            switch (compTypeCode)
            {
                case EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL:
                case EnrCompetitionTypeCodes.MINISTERIAL: postfix = "_cmn"; break;
                case EnrCompetitionTypeCodes.NO_EXAM_CONTRACT:
                case EnrCompetitionTypeCodes.CONTRACT: postfix = "_ctr"; break;
                case EnrCompetitionTypeCodes.EXCLUSIVE: postfix = "_q"; break;
                case EnrCompetitionTypeCodes.TARGET_ADMISSION: postfix = "_t"; break;
                default: throw new IllegalStateException("Unknown competition type");
            }
            updater.addBatch(postfix, id);
        }
        updater.executeUpdate(tool);
    }
}