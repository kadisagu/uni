/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantRegistrationJournalAdd.logic;

import com.beust.jcommander.internal.Lists;
import jxl.JXLException;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantRegistrationJournalAdd.EnrReportEntrantRegistrationJournalAddUI;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportUtil;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.ExcelReportUtils;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.FilterParametersPrinter;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder;
import ru.tandemservice.unienr14.report.entity.EnrReportEntrantRegistrationJournal;
import ru.tandemservice.unienr14.report.entity.EnrReportForEnrollmentCommission;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.EnrEnrollmentCommissionManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


/**
 * @author rsizonenko
 * @since 09.06.2014
 */
public class EnrReportEntrantRegistrationJournalDao extends UniBaseDao implements IEnrReportEntrantRegistrationJournalDao
{
    @Override
    public Long createReport(EnrReportEntrantRegistrationJournalAddUI model)
    {
        EnrReportEntrantRegistrationJournal report = new EnrReportEntrantRegistrationJournal();
        report.setFormingDate(new Date());

        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateSelector().getDateFrom());
        report.setDateTo(model.getDateSelector().getDateTo());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, EnrReportForEnrollmentCommission.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE, EnrReportForEnrollmentCommission.P_COMPENSATION_TYPE, "shortTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, EnrReportForEnrollmentCommission.P_PROGRAM_FORM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPETITION_TYPE, EnrReportForEnrollmentCommission.P_COMPETITION_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, EnrReportForEnrollmentCommission.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, EnrReportForEnrollmentCommission.P_FORMATIVE_ORG_UNIT, "fullTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, EnrReportForEnrollmentCommission.P_PROGRAM_SUBJECT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, EnrReportForEnrollmentCommission.P_EDU_PROGRAM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, EnrReportForEnrollmentCommission.P_PROGRAM_SET, "title");

        if (model.isFilterByEnrCommission())
            report.setEnrollmentCommission(UniStringUtils.join(model.getEnrollmentCommissionList(), EnrEnrollmentCommission.title().s(), "; "));


        report.setGroupByEnrCommission(model.isGroupByEnrCommission() ? "Да" : "Нет");

        DatabaseFile content = new DatabaseFile();
        content.setContent(buildReport(model));
        content.setFilename("EnrReportEntrantRegistrationJournal.xls");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_VND_MS_EXCEL);
        save(content);

        report.setContent(content);
        save(report);
        return report.getId();
    }

    @Override
    public byte[] buildReport(EnrReportEntrantRegistrationJournalAddUI model)
    {
        try
        {
            // документ
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            WritableWorkbook workbook = Workbook.createWorkbook(out);
            WritableSheet sheet = workbook.createSheet("ReportSheet", 0);

            WritableFont headerFont = new WritableFont(WritableFont.createFont("Calibri"), 11, WritableFont.BOLD);
            WritableCellFormat headerFormat = new WritableCellFormat(headerFont);
            int currentIndex = printFilterInfo(model, sheet);

            EnrEnrollmentCampaign campaign = model.getEnrollmentCampaign();

            // Фильтры
            EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
            EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder()
                    .notArchiveOnly()
                    .filter(model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo())
                    .filter(campaign);
            filterAddon.applyFilters(requestedCompDQL, requestedCompDQL.competition());

            if (model.isFilterByEnrCommission())
            {
                EnrEnrollmentCommissionManager.instance().dao().filterEntrantRequestByEnrCommission(requestedCompDQL, requestedCompDQL.request(), model.getEnrollmentCommissionList());
            }

            requestedCompDQL.column(property(requestedCompDQL.request()));

            // компараторы для сортировки
            Comparator<EnrEntrantRequest> requestComparator = (o1, o2) ->
            {
                int result = CommonCollator.RUSSIAN_COLLATOR.compare(o1.getIdentityCard().getFullFio(), o2.getIdentityCard().getFullFio());
                if (result != 0)
                    return result;

                return o1.getStringNumber().compareTo(o2.getStringNumber());
            };



            List<EnrEntrantRequest> requestList = getList(requestedCompDQL);

            List<EnrRequestedCompetition> reqCompList = Lists.newArrayList();

            BatchUtils.execute(requestList, DQL.MAX_VALUES_ROW_NUMBER, elements ->
            {
                reqCompList.addAll(getList(new DQLSelectBuilder()
                        .fromEntity(EnrRequestedCompetition.class, "erc")
                        .where(in(property("erc", EnrRequestedCompetition.request()), elements))));
            });

            Map<EnrEntrantRequest, List<EnrRequestedCompetition>> reqCompMap = new HashMap<>();

            for (EnrRequestedCompetition enrRequestedCompetition : reqCompList) {
                SafeMap.safeGet(reqCompMap, enrRequestedCompetition.getRequest(), ArrayList.class).add(enrRequestedCompetition);
            }

            Map<EnrEnrollmentCommission, Set<EnrEntrantRequest>> commissionMap = new TreeMap<>(ITitled.TITLED_COMPARATOR);
            Set<EnrEntrantRequest> noCommissionSet = new TreeSet<>(requestComparator);

            // Построение документа в зависимости от флага "группировать по комиссиям"
            if (model.isGroupByEnrCommission())
            {
                for (EnrEntrantRequest request : requestList)
                {
                    EnrEnrollmentCommission commission = request.getEnrollmentCommission();
                    if (commission != null)
                    {
                        Set<EnrEntrantRequest> requests = SafeMap.safeGet(commissionMap, commission, HashSet.class);
                        requests.add(request);
                    }
                    else noCommissionSet.add(request);
                }

                for (Map.Entry<EnrEnrollmentCommission, Set<EnrEntrantRequest>> entry : commissionMap.entrySet())
                {
                    currentIndex += 2;
                    sheet.addCell(new Label(1, currentIndex++, entry.getKey().getTitle(), headerFormat));
                    currentIndex = printTableHeader(sheet, currentIndex, model.isPrintEntrantNumberColumn());
                    currentIndex = printTableContent(sheet, entry.getValue(), reqCompMap, currentIndex, model.isPrintEntrantNumberColumn());
                }


                if (noCommissionSet.size() > 0)
                {
                    currentIndex++;
                    sheet.addCell(new Label(1, currentIndex++, "Отборочная комиссия не указана", headerFormat));
                    currentIndex = printTableHeader(sheet, currentIndex, model.isPrintEntrantNumberColumn());
                    currentIndex = printTableContent(sheet, noCommissionSet, reqCompMap, currentIndex, model.isPrintEntrantNumberColumn());
                }
            }
            else
            {
                Set<EnrEntrantRequest> requestSet = new TreeSet<>(requestComparator);
                requestSet.addAll(requestList);

                currentIndex = printTableHeader(sheet, currentIndex, model.isPrintEntrantNumberColumn());
                currentIndex = printTableContent(sheet, requestSet, reqCompMap, currentIndex, model.isPrintEntrantNumberColumn());
            }

            List<Integer> widths = new ArrayList<>();
            widths.add(5);
            if (model.isPrintEntrantNumberColumn())
                widths.add(10);
            widths.add(14);
            widths.add(12);
            widths.add(5);
            widths.add(10);
            widths.add(12);
            widths.add(17);
            widths.add(15);
            widths.add(22);
            widths.add(15);
            widths.add(14);
            widths.add(15);
            widths.add(15);
            widths.add(15);
            widths.add(15);


            setWidths(widths, sheet);

            //Закрываем потоки
            workbook.write();
            workbook.close();
            byte[] bytes = out.toByteArray();
            out.close();

            return bytes;
        }
        catch (JXLException | IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    protected int printFilterInfo(EnrReportEntrantRegistrationJournalAddUI model, WritableSheet sheet) throws JXLException
    {
        int lastUsedRow = 0;
        WritableFont labelFont = new WritableFont(WritableFont.createFont("Calibri"), 14, WritableFont.BOLD);
        WritableFont headerFont = new WritableFont(WritableFont.createFont("Calibri"), 11, WritableFont.BOLD);
        WritableFont contentFont = new WritableFont(WritableFont.createFont("Calibri"), 11);


        WritableCellFormat labelFormat = new WritableCellFormat(labelFont);
        WritableCellFormat headerFormat = new WritableCellFormat(headerFont);
        WritableCellFormat contentFormat = new WritableCellFormat(contentFont);

        List<String[]> hTable = new FilterParametersPrinter().getTableForNonRtf(model.getCompetitionFilterAddon(), model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo());
        if (model.isFilterByEnrCommission())
            hTable.add(new String[]{"Отборочная комиссия", UniStringUtils.join(model.getEnrollmentCommissionList(), EnrEnrollmentCommission.title().s(), "; ")});
        hTable.add(new String[]{"Группировать по отборочным комиссиям", model.isGroupByEnrCommission() ? "Да" : "Нет"});

        sheet.addCell(new Label(1, 0, "Журнал регистрации абитуриентов", labelFormat));
        sheet.addCell(new Label(6, 0, new SimpleDateFormat("dd.MM.yyyy").format(new Date()), labelFormat));

        for (int i = 0; i < hTable.size(); i++)
        {
            sheet.addCell(new Label(1, i + 1, hTable.get(i)[0], headerFormat));
            sheet.addCell(new Label(6, i + 1, hTable.get(i)[1], contentFormat));
            lastUsedRow = i + 1;
        }
        return lastUsedRow;
    }

    protected int printTableHeader(WritableSheet sheet, int rowIndex, boolean addEntrantNumberColumn) throws JXLException
    {
        WritableFont headerFont = new WritableFont(WritableFont.createFont("Calibri"), 10, WritableFont.BOLD);
        WritableCellFormat headerFormat = new WritableCellFormat(headerFont);
        headerFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
        headerFormat.setAlignment(Alignment.CENTRE);
        headerFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        headerFormat.setWrap(true);

        List<String> columns = new ArrayList<>();
        columns.add("№ п/п");
        if (addEntrantNumberColumn)
            columns.add("Номер личного дела");
        columns.add("Ф.И.О");
        columns.add("Дата рождения");
        columns.add("Пол");
        columns.add("Паспорт");
        columns.add("Уровень образования");
        columns.add("Документ об образовании");
        columns.add("Номер заявления");
        columns.add("Адрес регистрации");
        columns.add("Контактный телефон");
        columns.add("Способ возврата оригинала документа об образовании");
        columns.add("Сдан оригинал");
        columns.add("Согласие на зачисление");
        columns.add("Итоговое согласие");
        columns.add("Роспись о возврате заявления и документа об образовании");

        for (int i = 0; i < columns.size(); i++)
        {
            sheet.addCell(new Label(i, rowIndex, columns.get(i), headerFormat));
        }

        return rowIndex + 1;
    }

    protected int printTableContent(WritableSheet sheet, Set<EnrEntrantRequest> requests, Map<EnrEntrantRequest, List<EnrRequestedCompetition>> reqCompMap, int rowIndex, boolean printEntrantNumberColumn) throws JXLException
    {
        WritableFont contentFont = new WritableFont(WritableFont.createFont("Calibri"), 10);
        WritableCellFormat contentFormat = new WritableCellFormat(contentFont);
        contentFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
        contentFormat.setAlignment(Alignment.LEFT);
        contentFormat.setVerticalAlignment(VerticalAlignment.BOTTOM);
        contentFormat.setWrap(true);
        int number = 1;
        for (EnrEntrantRequest request : requests)
        {
            List<String> rowContent = new ArrayList<>();
            rowContent.add(String.valueOf(number));
            if (printEntrantNumberColumn)
                rowContent.add(request.getEntrant().getPersonalNumber());
            rowContent.add(request.getIdentityCard().getFullFio());
            if (request.getIdentityCard().getBirthDate() != null)
                rowContent.add(new SimpleDateFormat("dd.MM.yyyy").format(request.getIdentityCard().getBirthDate()));
            else
                rowContent.add("");
            rowContent.add(request.getIdentityCard().getSex().isMale() ? "М" : "Ж");
            rowContent.add(request.getIdentityCard().getFullNumber());
            if (request.getEduDocument().getEduLevel() != null)
                rowContent.add(request.getEduDocument().getEduLevel().getShortTitle());
            else rowContent.add("");
            rowContent.add(request.getEduDocument().getDisplayableTitle());
            rowContent.add(request.getStringNumber());
            if (request.getIdentityCard().getAddress() != null)
                rowContent.add(request.getIdentityCard().getAddress().getShortTitleWithSettlement());
            else
                rowContent.add("");
            rowContent.add(request.getIdentityCard().getPerson().getContactData().getPhoneDefault());
            rowContent.add(request.getOriginalReturnWay().getTitle());

            List<EnrRequestedCompetition> reqComps = SafeMap.safeGet(reqCompMap, request, ArrayList.class);

            // оригинал/копия
            rowContent.add(YesNoFormatter.INSTANCE.format(request.isEduInstDocOriginalHandedIn()));
            // согл. на зачисление
            rowContent.add(YesNoFormatter.INSTANCE.format(reqComps.stream().filter(EnrRequestedCompetition::isAccepted).count() > 0));
            // итоговое согласие
            rowContent.add(YesNoFormatter.INSTANCE.format(reqComps.stream().filter(EnrRequestedCompetition::isEnrollmentAvailable).count() > 0));

            rowContent.add("");
            ExcelReportUtils.printrow(sheet, rowContent, rowIndex + number - 1, contentFormat);
            number++;
        }

        return rowIndex + number;
    }

    protected static void setWidths(List<Integer> widths, WritableSheet sheet)
    {
        for (int i = 0; i < widths.size(); i++)
        {
            sheet.setColumnView(i, widths.get(i));
        }
    }
}
