package ru.tandemservice.unienr14.catalog.entity;

import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import ru.tandemservice.unienr14.catalog.entity.gen.*;

/**
 * Причина отказа в приеме документов
 */
public class EnrEntrantRefusalReason extends EnrEntrantRefusalReasonGen implements IDynamicCatalogItem
{
}