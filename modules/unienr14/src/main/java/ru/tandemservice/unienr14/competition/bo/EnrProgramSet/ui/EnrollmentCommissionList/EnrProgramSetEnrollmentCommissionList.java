/* $Id$ */
package ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.EnrollmentCommissionList;

import com.google.common.collect.Lists;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.util.Icon;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.logic.EnrProgramSetEnrollmentCommissionDSHandler;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.EnrollmentCommissionList.logic.EnrProgramSetOrgUnitComboDSHandler;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetEnrollmentCommission;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.EnrEntrantRequestManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.EnrEnrollmentCommissionManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.util.Collection;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.EnrollmentCommissionList.EnrProgramSetEnrollmentCommissionListUI.*;

/**
 * @author Alexey Lopatin
 * @since 02.06.2015
 */
@Configuration
public class EnrProgramSetEnrollmentCommissionList extends BusinessComponentManager
{
    public static final String PROGRAM_FORM_DS = "programFormDS";
    public static final String ORG_UNIT_DS = "orgUnitDS";
    public static final String FORMATIVE_ORG_UNIT_DS = "formativeOrgUnitDS";
    public static final String PROGRAM_SUBJECT_DS = "programSubjectDS";
    public static final String ENROLLMENT_COMMISSION_DS = "enrollmentCommissionDS";

    public static final String BLOCK_ENROLLMENT_COMMISSION_DS = "blockEnrollmentCommissionDS";
    public static final String PROGRAM_SET_ENROLLMENT_COMMISSION_DS = "programSetEnrollmentCommissionDS";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
                .addDataSource(EnrEntrantRequestManager.instance().requestTypeDS_1_Config())
                .addDataSource(selectDS(PROGRAM_FORM_DS, programFormDS()))
                .addDataSource(selectDS(ORG_UNIT_DS, orgUnitDS()).addColumn(EnrOrgUnit.institutionOrgUnit().orgUnit().shortTitleWithTopEmphasized().s()))
                .addDataSource(selectDS(FORMATIVE_ORG_UNIT_DS, formativeOrgUnitDS()).addColumn(OrgUnit.title().s()))
                .addDataSource(selectDS(PROGRAM_SUBJECT_DS, programSubjectDS()).addColumn(EduProgramSubject.titleWithCode().s()))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(ENROLLMENT_COMMISSION_DS, getName(), EnrEnrollmentCommission.defaultSelectDSHandler(getName())
                        .customize((alias, dql, context, filter) ->
                        {
                            EnrEnrollmentCampaign campaign = context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
                            EnrRequestType requestType = context.get(EnrProgramSetEnrollmentCommissionListUI.PARAM_REQUEST_TYPE);

                            List<Long> enrollmentCommissionIds = Lists.newArrayList();
                            Collection<Long> ids = CommonBaseEntityUtil.getIdList(EnrEnrollmentCommissionManager.instance().dao().getProgramSetOrgUnit2EnrollmentCommissionMap(campaign, requestType).values());
                            if (!ids.isEmpty())
                            {
                                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrProgramSetEnrollmentCommission.class, "ec")
                                        .column(property("ec", EnrProgramSetEnrollmentCommission.enrollmentCommission().id())).distinct()
                                        .where(in(property("ec", EnrProgramSetEnrollmentCommission.id()), ids));

                                enrollmentCommissionIds = ISharedBaseDao.instance.get().getList(builder);
                            }

                            return dql.where(or(
                                    !enrollmentCommissionIds.isEmpty() ? in(property(alias, EnrEnrollmentCommission.id()), enrollmentCommissionIds) : null,
                                    eq(property(alias, EnrEnrollmentCommission.enabled()), value(Boolean.TRUE))
                            ));
                        })
                ))
                .addDataSource(selectDS(BLOCK_ENROLLMENT_COMMISSION_DS, blockEnrollmentCommissionDS()))
                .addDataSource(searchListDS(PROGRAM_SET_ENROLLMENT_COMMISSION_DS, programSetEnrollmentCommissionDSColumns(), programSetEnrollmentCommissionDS()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler programFormDS()
    {
        return new EnrProgramSetOrgUnitComboDSHandler(getName(), EduProgramForm.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                DQLSelectBuilder builder = createBuilder(context, EnrProgramSetOrgUnit.programSet().programForm(), PARAM_PROGRAM_FORMS).distinct();
                dql.where(in(property(alias), builder.buildQuery()));
            }
        }
                .filter(EduProgramForm.title())
                .order(EduProgramForm.code());
    }

    @Bean
    public IDefaultComboDataSourceHandler orgUnitDS()
    {
        return new EnrProgramSetOrgUnitComboDSHandler(getName(), EnrOrgUnit.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                DQLSelectBuilder builder = createBuilder(context, EnrProgramSetOrgUnit.orgUnit(), PARAM_ORG_UNITS).distinct();
                dql.where(in(property(alias), builder.buildQuery()));
            }
        };
    }

    @Bean
    public IDefaultComboDataSourceHandler formativeOrgUnitDS()
    {
        return new EnrProgramSetOrgUnitComboDSHandler(getName(), OrgUnit.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                DQLSelectBuilder builder = createBuilder(context, EnrProgramSetOrgUnit.formativeOrgUnit(), PARAM_FORMATIVE_ORG_UNITS).distinct();
                dql.where(in(property(alias), builder.buildQuery()));
            }
        }
                .filter(OrgUnit.title())
                .order(OrgUnit.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler programSubjectDS()
    {
        return new EnrProgramSetOrgUnitComboDSHandler(getName(), EduProgramSubject.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                DQLSelectBuilder builder = createBuilder(context, EnrProgramSetOrgUnit.programSet().id(), PARAM_PROGRAM_SUBJECTS);
                dql.where(exists(
                        new DQLSelectBuilder()
                                .fromEntity(EnrProgramSetItem.class, "psi")
                                .where(in(property("psi", EnrProgramSetItem.programSet().id()), builder.buildQuery()))
                                .where(eq(property("psi", EnrProgramSetItem.program().programSubject().id()), property(alias, EduProgramSubject.id())))
                                .buildQuery()
                ));
            }
        }
                .filter(EduProgramSubject.code())
                .filter(EduProgramSubject.title())
                .order(EduProgramSubject.code())
                .order(EduProgramSubject.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler blockEnrollmentCommissionDS()
    {
        return new EntityComboDataSourceHandler(getName(), EnrEnrollmentCommission.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                EnrEnrollmentCommission enrollmentCommission = context.get(EnrProgramSetEnrollmentCommissionListUI.PARAM_CURRENT_ENROLLMENT_COMMISSION);
                if (null == enrollmentCommission)
                    dql.where(eq(property(alias, EnrEnrollmentCommission.enabled()), value(Boolean.TRUE)));
                else
                    dql.where(or(
                            eq(property(alias, EnrEnrollmentCommission.id()), value(enrollmentCommission.getId())),
                            eq(property(alias, EnrEnrollmentCommission.enabled()), value(Boolean.TRUE))
                    ));
            }
        }
                .filter(EnrEnrollmentCommission.title())
                .order(EnrEnrollmentCommission.title());
    }

    @Bean
    public ColumnListExtPoint programSetEnrollmentCommissionDSColumns()
    {
        return columnListExtPointBuilder(PROGRAM_SET_ENROLLMENT_COMMISSION_DS)
                .addColumn(checkboxColumn("check"))
                .addColumn(publisherColumn("programSet", EnrProgramSetOrgUnit.programSet().title().s()).primaryKeyPath(EnrProgramSetOrgUnit.programSet().id().s()))
                .addColumn(textColumn("programSubject", EnrProgramSetOrgUnit.programSet().programSubject().titleWithCode().s()))
                .addColumn(textColumn("developForm", EnrProgramSetOrgUnit.programSet().programForm().title().s()))
                .addColumn(textColumn("orgUnit", EnrProgramSetOrgUnit.orgUnit().institutionOrgUnit().orgUnit().shortTitleWithTopEmphasized().s()))
                .addColumn(textColumn("formativeOrgUnit", EnrProgramSetOrgUnit.formativeOrgUnit().title().s()))
                .addColumn(blockColumn("enrollmentCommission", "enrollmentCommissionBlock"))
                .addColumn(blockColumn("action", "actionBlock").width("1").hint("Редактировать").required(true))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, new Icon("cancel"), DELETE_LISTENER).disabled("ui:disabledDeleteEC")
                                   .alert(new FormattedMessage("programSetEnrollmentCommissionDS.delete.alert", "programSet.title"))
                                   .permissionKey("enr14ProgramSetEnrollmentCommissionDeleteEC"))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> programSetEnrollmentCommissionDS()
    {
        return new EnrProgramSetEnrollmentCommissionDSHandler(getName());
    }
}
