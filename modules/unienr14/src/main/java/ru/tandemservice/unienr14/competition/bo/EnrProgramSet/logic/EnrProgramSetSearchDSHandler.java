/* $Id:$ */
package ru.tandemservice.unienr14.competition.bo.EnrProgramSet.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType2eduProgramKindRel;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.List.EnrProgramSetList;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetSecondary;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 2/4/14
 */
public class EnrProgramSetSearchDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public EnrProgramSetSearchDSHandler(String ownerId)
    {
        super(ownerId, EnrProgramSetBase.class);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final EnrEnrollmentCampaign enrCamp = context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
        final EnrRequestType requestType = context.get(EnrProgramSetList.BIND_REQUEST_TYPE);
        if (enrCamp == null || requestType == null) {
            return new DSOutput(input);
        }

        EduProgramForm programForm = context.get(EnrProgramSetList.BIND_PROGRAM_FORM);
        List<EnrOrgUnit> orgUnits = context.get(EnrProgramSetList.BIND_ORG_UNITS);
        String subjectCode = context.get(EnrProgramSetList.BIND_SUBJECT_CODE);
        List<EduProgramSubject> programSubjects = context.get(EnrProgramSetList.BIND_PROGRAM_SUBJECTS);
        List<EduProgramHigherProf> eduPrograms = context.get(EnrProgramSetList.BIND_EDU_PROGRAMS);
        String programSetTitle = context.get(EnrProgramSetList.BIND_PROGRAM_SET_TITLE);

        final DQLOrderDescriptionRegistry orderDescriptionRegistry = new DQLOrderDescriptionRegistry(EnrProgramSetBase.class, "s");
        final DQLSelectBuilder builder = orderDescriptionRegistry.buildDQLSelectBuilder()
                .column(property("s"))
                .where(notInstanceOf("s", EnrProgramSetSecondary.class));

        EnrProgramSetSearchDSHandler.applyFilters(builder, "s", enrCamp, requestType, programForm, orgUnits, null, programSubjects, eduPrograms);

        FilterUtils.applySimpleLikeFilter(builder, "s", EnrProgramSetBase.programSubject().code(), subjectCode);
        FilterUtils.applySimpleLikeFilter(builder, "s", EnrProgramSetBase.title(), programSetTitle);

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).order(orderDescriptionRegistry).pageable(true).build();
    }

    @SuppressWarnings("ConstantConditions")
    public static DQLSelectBuilder applyFilters(DQLSelectBuilder builder, String alias, EnrEnrollmentCampaign campaign, EnrRequestType requestType, EduProgramForm programForm, List<EnrOrgUnit> orgUnits, List<OrgUnit> formativeOrgUnit, List<EduProgramSubject> programSubjects, List<EduProgramHigherProf> eduPrograms)
    {
        if (requestType == null || campaign == null) {
            builder.where(nothing());
            return builder;
        }
        builder.where(eq(property(alias, EnrProgramSetBase.requestType()), value(requestType)));
        builder.where(eq(property(alias, EnrProgramSetBase.enrollmentCampaign()), value(campaign)));
        builder.joinEntity(alias, DQLJoinType.inner, requestType.getProgramSetClass(), "e", eq(property("e", EnrProgramSetBase.id()), property(alias, EnrProgramSetBase.id())));
        builder.where(exists(
                EnrRequestType2eduProgramKindRel.class,
                EnrRequestType2eduProgramKindRel.requestType().s(), requestType,
                EnrRequestType2eduProgramKindRel.programKind().s(), property(alias, EnrProgramSetBase.programSubject().subjectIndex().programKind())
        ));

        FilterUtils.applySelectFilter(builder, alias, EnrProgramSetBase.programForm().s(), programForm);


        {
            final DQLSelectBuilder programSetOrgUnitBuilder = new DQLSelectBuilder().fromEntity(EnrProgramSetOrgUnit.class, "psou");
            boolean applyFilters = false;
            applyFilters |= FilterUtils.applySelectFilter(programSetOrgUnitBuilder, "psou", EnrProgramSetOrgUnit.orgUnit(), orgUnits);
            applyFilters |= FilterUtils.applySelectFilter(programSetOrgUnitBuilder, "psou", EnrProgramSetOrgUnit.formativeOrgUnit(), formativeOrgUnit);
            if (applyFilters) {
                builder.where(exists(
                        programSetOrgUnitBuilder
                                .column(value(1))
                                .where(eq(property("psou", EnrProgramSetOrgUnit.programSet()), property(alias)))
                                .buildQuery()
                ));
            }
        }


        {
            final DQLSelectBuilder programSetItemBuilder = new DQLSelectBuilder().fromEntity(EnrProgramSetItem.class, "psi");
            boolean applyFilters = false;
            applyFilters |= FilterUtils.applySelectFilter(programSetItemBuilder, "psi", EnrProgramSetItem.program().programSubject(), programSubjects);
            applyFilters |= FilterUtils.applySelectFilter(programSetItemBuilder, "psi", EnrProgramSetItem.program(), eduPrograms);
            if (applyFilters) {
                builder.where(exists(
                        programSetItemBuilder
                                .column(value(1))
                                .where(eq(property("psi", EnrProgramSetItem.programSet()), property(alias)))
                                .buildQuery()
                ));
            }
        }

        return builder;
    }
}
