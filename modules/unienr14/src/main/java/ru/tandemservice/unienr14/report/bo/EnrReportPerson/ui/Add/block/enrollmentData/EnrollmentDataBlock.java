/* $Id$ */
package ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.block.enrollmentData;

import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unimove.entity.catalog.OrderStates;

/**
 * @author azhebko
 * @since 05.08.2014
 */
public class EnrollmentDataBlock
{
    public static final String ENROLLMENT_ORDER_STATE_DS = "enrollmentOrderStateDS";
    public static final String CANCELLED_EXTRACTS_DS = "cancelledExtractsDS";

    public static final Long CANCELLED_EXTRACTS_NOT_INCLUDED = 0L;
    public static final Long CANCELLED_EXTRACTS_INCLUDED = 1L;
    public static final Long CANCELLED_EXTRACTS_ONLY = 2L;

    public static IDefaultComboDataSourceHandler createEnrollmentOrderStateDS(String name)
    {
        return new EntityComboDataSourceHandler(name, OrderStates.class)
            .filter(OrderStates.title())
            .order(OrderStates.code());
    }

    public static IDefaultComboDataSourceHandler createCancelledExtractsDS(String name)
    {
        return new SimpleTitledComboDataSourceHandler(name)
            .addRecord(CANCELLED_EXTRACTS_NOT_INCLUDED, "enrollmentData.cancelledExtractsNotIncluded")
            .addRecord(CANCELLED_EXTRACTS_INCLUDED, "enrollmentData.cancelledExtractsIncluded")
            .addRecord(CANCELLED_EXTRACTS_ONLY, "enrollmentData.cancelledExtractsOnly")
            .filtered(true);
    }
}