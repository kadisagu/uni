/**
 *$Id: EnrRequestedDirectionPubUI.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.request.bo.EnrRequestedCompetition.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.unienr14.request.entity.EnrEntrantBenefitProof;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitProofDocument;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 13.05.13
 */
@State({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "entityHolder.id", required = true)
})
public class EnrRequestedCompetitionPubUI extends UIPresenter
{
    private EntityHolder<EnrRequestedCompetition> _entityHolder = new EntityHolder<>();

    private List<IEnrEntrantBenefitProofDocument> benefitProofList;
    private IEnrEntrantBenefitProofDocument currentBenefitProof;

    @Override
    public void onComponentRefresh()
    {
        _entityHolder.refresh();
        setBenefitProofList(DataAccessServices.dao().<IEnrEntrantBenefitProofDocument>getList(
            new DQLSelectBuilder().fromEntity(EnrEntrantBenefitProof.class, "b")
            .column(property(EnrEntrantBenefitProof.document().fromAlias("b")))
            .where(eq(property(EnrEntrantBenefitProof.benefitStatement().fromAlias("b")), value(getEntityHolder().getValue())))
            .order(property(EnrEntrantBenefitProof.document().id().fromAlias("b")))));
    }

    // Getters & Setters

    public String getSticker()
    {
        return "Выбранный конкурс: " + getEntityHolder().getValue().getTitle() + "; " + "Абитуриент: " + getEntityHolder().getValue().getRequest().getEntrant().getPerson().getFio();
    }

    public EnrRequestedCompetition getRequestedCompetition() {
        return getEntityHolder().getValue();
    }

    /* Generated */

    public EntityHolder<EnrRequestedCompetition> getEntityHolder()
    {
        return _entityHolder;
    }

    public void setEntityHolder(EntityHolder<EnrRequestedCompetition> entityHolder)
    {
        _entityHolder = entityHolder;
    }

    public void setBenefitProofList(List<IEnrEntrantBenefitProofDocument> benefitProofList)
    {
        this.benefitProofList = benefitProofList;
    }

    public IEnrEntrantBenefitProofDocument getCurrentBenefitProof()
    {
        return currentBenefitProof;
    }

    public void setCurrentBenefitProof(IEnrEntrantBenefitProofDocument currentBenefitProof)
    {
        this.currentBenefitProof = currentBenefitProof;
    }

    public List<IEnrEntrantBenefitProofDocument> getBenefitProofList()
    {
        return benefitProofList;
    }
}
