/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.ui.CompetitionPub;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.LongAsDoubleFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.utils.DQLSimple;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.EnrTargetAdmissionKind;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEnrollmentStepStateCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrRequestTypeCodes;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrTargetAdmissionPlan;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.ui.ConflictSolutionChange.EnrEnrollmentStepConflictSolutionChange;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepCompetitionPlan;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItem;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;

/**
 * @author oleyba
 * @since 12/17/13
 */
@State({
    @Bind(key = EnrEnrollmentStepCompetitionPubUI.PARAM_STEP, binding = "step.id"),
    @Bind(key = EnrEnrollmentStepCompetitionPubUI.PARAM_COMPETITION, binding = "competitionHolder.id"),
    @Bind(key = EnrEnrollmentStepCompetitionPubUI.PARAM_TARGET_ADMISSION_KIND, binding = "taKind.id")
})
public class EnrEnrollmentStepCompetitionPubUI extends UIPresenter
{
    public static final String PARAM_STEP = "step";
    public static final String PARAM_COMPETITION = "competition";
    public static final String PARAM_TARGET_ADMISSION_KIND = "taKind";

    private EnrEnrollmentStep step = new EnrEnrollmentStep();
    private EntityHolder<EnrCompetition> competitionHolder = new EntityHolder<>();
    private EnrTargetAdmissionKind taKind = new EnrTargetAdmissionKind();

    private String competitionPlan;

    private IUIDataSource itemDS;

    @Override
    public void onComponentRefresh()
    {
        setStep(IUniBaseDao.instance.get().getNotNull(EnrEnrollmentStep.class, getStep().getId()));
        if (null != getTaKind() && null != getTaKind().getId()) {
            setTaKind(IUniBaseDao.instance.get().get(EnrTargetAdmissionKind.class, getTaKind().getId()));
        } else {
            setTaKind(null);
        }
        getCompetitionHolder().refresh();
        EnrEnrollmentStepCompetitionPlan plan = DataAccessServices.dao().getByNaturalId(new EnrEnrollmentStepCompetitionPlan.NaturalId(getStep(), getCompetition()));
        setCompetitionPlan(String.valueOf(plan == null ? getCompetition().getPlan() : plan.getPlan()));

        boolean taComp = getCompetition().getProgramSetOrgUnit().getProgramSet().getEnrollmentCampaign().getSettings().isTargetAdmissionCompetition();
        if (taComp && getCompetition().isTargetAdmission()) {
            EnrTargetAdmissionPlan taPlan = new DQLSimple<>(EnrTargetAdmissionPlan.class)
                .where(EnrTargetAdmissionPlan.enrCampaignTAKind().targetAdmissionKind(), getTaKind())
                .where(EnrTargetAdmissionPlan.programSetOrgUnit(), getCompetition().getProgramSetOrgUnit())
                .get();
            if (null == taPlan) {
                setCompetitionPlan("не задан план приема по виду ЦП");
                getUserContext().getErrorCollector().add("Не задан план приема по виду ЦП.");
            } else {
                setCompetitionPlan(String.valueOf(taPlan.getPlan()));
            }
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrEnrollmentStepItemDSHandler.BIND_ENR_STEP, getStep());
        dataSource.put(EnrEnrollmentStepItemDSHandler.BIND_COMPETITION, getCompetition());
        dataSource.put(EnrEnrollmentStepItemDSHandler.BIND_TA_KIND, getTaKind());
    }

    // actions

    public void onClickEdit() {
        _uiActivation.asRegionDialog(EnrEnrollmentStepConflictSolutionChange.class)
            .parameter(PublisherActivator.PUBLISHER_ID_KEY, getListenerParameterAsLong())
            .activate();
    }

    public void onClickUp() {
        // todo убрать отсюда все в методы dao
        EnrEnrollmentStepItem item = IUniBaseDao.instance.get().get(getListenerParameterAsLong());
        CommonManager.instance().commonPriorityDao().doChangePriorityUp(item.getEntity().getId(), EnrRatingItem.position().s(), EnrRatingItem.competition(), item.getEntity().getCompetition());
    }

    public void onClickDown() {
        EnrEnrollmentStepItem item = IUniBaseDao.instance.get().get(getListenerParameterAsLong());
        CommonManager.instance().commonPriorityDao().doChangePriorityDown(item.getEntity().getId(), EnrRatingItem.position().s(), EnrRatingItem.competition(), item.getEntity().getCompetition());
    }

    public void onInclude() {
        // todo нужно проверять, что включать вообще можно
        EnrEnrollmentStepItem item = IUniBaseDao.instance.get().get(getListenerParameterAsLong());
        item.setIncluded(true);
        IUniBaseDao.instance.get().update(item);
    }

    public void onExclude() {
        EnrEnrollmentStepItem item = IUniBaseDao.instance.get().get(getListenerParameterAsLong());
        item.setIncluded(false);
        IUniBaseDao.instance.get().update(item);
    }


    public void onRecommend() {
        EnrEnrollmentStepItem item = IUniBaseDao.instance.get().get(getListenerParameterAsLong());
        item.setRecommended(true);
        IUniBaseDao.instance.get().update(item);
    }

    public void onClearRecommend() {
        EnrEnrollmentStepItem item = IUniBaseDao.instance.get().get(getListenerParameterAsLong());
        item.setRecommended(false);
        IUniBaseDao.instance.get().update(item);
    }

    public void onSetShouldBeEnrolled() {
        EnrEnrollmentStepItem item = IUniBaseDao.instance.get().get(getListenerParameterAsLong());
        item.setShouldBeEnrolled(true);
        IUniBaseDao.instance.get().update(item);
    }

    public void onClearShouldBeEnrolled() {
        EnrEnrollmentStepItem item = IUniBaseDao.instance.get().get(getListenerParameterAsLong());
        item.setShouldBeEnrolled(false);
        IUniBaseDao.instance.get().update(item);
    }

    // presenter

    public IEntity getCurrentItem() {
        return getItemDS().getCurrent();
    }

    public IUIDataSource getItemDS()
    {
        if (itemDS == null)
            itemDS = getConfig().getDataSource("itemDS"); // todo сделать константу
        return itemDS;
    }

    public EnrCompetition getCompetition()
    {
        return getCompetitionHolder().getValue();
    }

    public boolean isCurrentItemUpDisabled() {
        // todo убрать адъ после TF-501
        return !EnrEnrollmentStepStateCodes.START.equals(getStep().getState().getCode()) || (Boolean) getCurrentItem().getProperty("upDisabled");
    }

    public boolean isCurrentItemDownDisabled() {
        return !EnrEnrollmentStepStateCodes.START.equals(getStep().getState().getCode()) || (Boolean) getCurrentItem().getProperty("downDisabled");
    }

    public boolean isCurrentItemEditDisabled() {
        return (Boolean) getCurrentItem().getProperty("editDisabled");
    }

    public boolean isIncludedColumnVisible() {
        return !isIncludedEditColumnVisible();
    }

    public boolean isIncludedEditColumnVisible() {
        return EnrEnrollmentStepStateCodes.START.equals(getStep().getState().getCode());
    }

    public boolean isRecommendEditVisible() {
        return getStep().getKind().isUseRecommendation() && !getStep().isAutoRecommended() && EnrEnrollmentStepStateCodes.START.equals(getStep().getState().getCode());
    }

    public boolean isRecommendVisible() {
        return getStep().getKind().isUseRecommendation() && !isRecommendEditVisible();
    }

    public boolean isShouldBeEnrolledEditVisible() {
        return !getStep().isAutoEnrolledMarked()
            && (
                (EnrEnrollmentStepStateCodes.RECOMMENDED.equals(getStep().getState().getCode()) && getStep().getKind().isUseRecommendation())
                || (EnrEnrollmentStepStateCodes.START.equals(getStep().getState().getCode()) && !getStep().getKind().isUseRecommendation())
        );
    }

    public boolean isShouldBeEnrolledVisible() {
        return !isShouldBeEnrolledEditVisible();
    }

    public boolean isCanRefreshData() {
        return getStep().isRefreshDataAllowed();
    }

    public boolean isBakSpecMag()
    {
        String code = getCompetition().getRequestType().getCode();
        return code.equals(EnrRequestTypeCodes.BS) || code.equals(EnrRequestTypeCodes.MASTER);
    }

    public boolean isTrainInter()
    {
        String code = getCompetition().getRequestType().getCode();
        return EnrRequestTypeCodes.INTERNSHIP.equals(code) || EnrRequestTypeCodes.TRAINEESHIP.equals(code);
    }

    public boolean isSpo()
    {
        return EnrRequestTypeCodes.SPO.equals(getCompetition().getRequestType().getCode());
    }

    public String getTotalMark()
    {
        DataWrapper wrapper = _uiConfig.getDataSourceCurrentRecord(EnrEnrollmentStepCompetitionPub.ITEM_DS);
        EnrEnrollmentStepItem stepItem = wrapper.getWrapped();
        EnrRatingItem item = stepItem.getEntity();
        return !item.isStatusEntranceExamsCorrect() || item.getTotalMarkAsLong() == 0 ? "—" : LongAsDoubleFormatter.LONG_AS_DOUBLE_FORMATTER_3_SCALE.format(item.getTotalMarkAsLong());
    }

    public String getEntranceExamSum()
    {
        DataWrapper wrapper = _uiConfig.getDataSourceCurrentRecord(EnrEnrollmentStepCompetitionPub.ITEM_DS);
        EnrEnrollmentStepItem stepItem = wrapper.getWrapped();
        EnrRatingItem item = stepItem.getEntity();
        return !item.isStatusEntranceExamsCorrect() || item.getTotalMarkAsLong() == 0 ? "—" : LongAsDoubleFormatter.LONG_AS_DOUBLE_FORMATTER_3_SCALE.format(item.getTotalMarkAsLong() - item.getAchievementMarkAsLong());
    }

    // getters and setters


    public EntityHolder<EnrCompetition> getCompetitionHolder()
    {
        return competitionHolder;
    }

    public EnrEnrollmentStep getStep()
    {
        return step;
    }

    public void setStep(EnrEnrollmentStep step)
    {
        this.step = step;
    }

    public EnrTargetAdmissionKind getTaKind()
    {
        return taKind;
    }

    public void setTaKind(EnrTargetAdmissionKind taKind)
    {
        this.taKind = taKind;
    }

    public String getCompetitionPlan()
    {
        return competitionPlan;
    }

    public void setCompetitionPlan(String competitionPlan)
    {
        this.competitionPlan = competitionPlan;
    }
}