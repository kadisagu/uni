/* $Id:$ */
package ru.tandemservice.unienr14.request.entity;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.shared.person.catalog.entity.PersonDocumentType;
import ru.tandemservice.unienr14.fis.IFisUidByIdOwner;

import java.util.Comparator;
import java.util.Date;

/**
 * @author oleyba
 * @since 1/31/14
 */
public interface IEnrEntrantRequestAttachable extends IEntity, ITitled, IFisUidByIdOwner
{
    @SuppressWarnings("unchecked")
    public static Comparator<EnrEntrantRequestAttachment> ATTACHMENT_COMPARATOR = new EntityComparator<EnrEntrantRequestAttachment>()
        .addFirst(new EntityOrder(EnrEntrantRequestAttachment.L_DOCUMENT + ".documentType." + PersonDocumentType.P_PRIORITY))
        .addLast(new EntityOrder(EnrEntrantRequestAttachment.L_DOCUMENT + ".registrationDate", OrderDirection.desc));

    PersonDocumentType getDocumentType();

    Date getRegistrationDate();

    public String getDisplayableTitle();

    Date getIssuanceDate();

    String getNumber();

    String getSeria();

    String getIssuancePlace();
}
