/* $Id$ */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.logic.EnrEnrollmentCommissionDao;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.logic.EnrEnrollmentCommissionPermissionDao;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.logic.IEnrEnrollmentCommissionDao;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.logic.IEnrEnrollmentCommissionPermissionDao;

/**
 * @author Alexey Lopatin
 * @since 29.05.2015
 */
@Configuration
public class EnrEnrollmentCommissionManager extends BusinessObjectManager
{
    public static final String DS_ENROLLMENT_COMMISSION = "enrollmentCommissionDS";

    public static EnrEnrollmentCommissionManager instance()
    {
        return instance(EnrEnrollmentCommissionManager.class);
    }

    @Bean
    public IEnrEnrollmentCommissionDao dao() {
        return new EnrEnrollmentCommissionDao();
    }

    public IEnrEnrollmentCommissionPermissionDao permissionDao() {
        return new EnrEnrollmentCommissionPermissionDao();
    }

    @Bean
    public UIDataSourceConfig enrollmentCommissionDSConfig()
    {
        return SelectDSConfig.with(DS_ENROLLMENT_COMMISSION, getName())
            .dataSourceClass(SelectDataSource.class)
            .handler(enrollmentCommissionDSHandler())
            .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler enrollmentCommissionDSHandler()
    {
        return EnrEnrollmentCommission.permissionSelectDSHandler(getName());
    }
}
