/* $Id$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantForeignRequest;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.shared.commonbase.base.util.DefaultNumberGenerationRule;
import org.tandemframework.shared.commonbase.base.util.INumberGenerationRule;
import ru.tandemservice.unienr14.entrant.EnrNumberGenerationRule;
import ru.tandemservice.unienr14.request.bo.EnrEntrantForeignRequest.logic.EnrEntrantForeignRequestDAO;
import ru.tandemservice.unienr14.request.bo.EnrEntrantForeignRequest.logic.IEnrEntrantForeignRequestDAO;
import ru.tandemservice.unienr14.request.entity.EnrEntrantForeignRequest;

/**
 * @author nvankov
 * @since 7/15/14
 */
@Configuration
public class EnrEntrantForeignRequestManager extends BusinessObjectManager
{
    public static EnrEntrantForeignRequestManager instance()
    {
        return instance(EnrEntrantForeignRequestManager.class);
    }

    @Bean
    public IEnrEntrantForeignRequestDAO dao()
    {
        return new EnrEntrantForeignRequestDAO();
    }
}



    