package ru.tandemservice.unienr14.order.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unienr14.order.entity.EnrAbstractOrder;
import ru.tandemservice.unienr14.order.entity.EnrAbstractParagraph;
import ru.tandemservice.unienr14.order.entity.EnrCancelParagraph;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Параграф приказа об отмене приказа по абитуриентам (2014)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrCancelParagraphGen extends EnrAbstractParagraph
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.order.entity.EnrCancelParagraph";
    public static final String ENTITY_NAME = "enrCancelParagraph";
    public static final int VERSION_HASH = 1963079103;
    private static IEntityMeta ENTITY_META;

    public static final String L_CANCELLED_ORDER = "cancelledOrder";
    public static final String P_CANCEL_EXCLUDE_LOWER_PRIORITY_COMPETITIONS = "cancelExcludeLowerPriorityCompetitions";

    private EnrAbstractOrder _cancelledOrder;     // Абстрактный приказ на абитуриентов (2014)
    private boolean _cancelExcludeLowerPriorityCompetitions = false;     // Отменить исключение абитуриентов из конкурсов с более низким приоритетом

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Абстрактный приказ на абитуриентов (2014). Свойство не может быть null.
     */
    @NotNull
    public EnrAbstractOrder getCancelledOrder()
    {
        return _cancelledOrder;
    }

    /**
     * @param cancelledOrder Абстрактный приказ на абитуриентов (2014). Свойство не может быть null.
     */
    public void setCancelledOrder(EnrAbstractOrder cancelledOrder)
    {
        dirty(_cancelledOrder, cancelledOrder);
        _cancelledOrder = cancelledOrder;
    }

    /**
     * См. enrEnrollmentExtract.excludeLowerPriorityCompetitions.
     *
     * @return Отменить исключение абитуриентов из конкурсов с более низким приоритетом. Свойство не может быть null.
     */
    @NotNull
    public boolean isCancelExcludeLowerPriorityCompetitions()
    {
        return _cancelExcludeLowerPriorityCompetitions;
    }

    /**
     * @param cancelExcludeLowerPriorityCompetitions Отменить исключение абитуриентов из конкурсов с более низким приоритетом. Свойство не может быть null.
     */
    public void setCancelExcludeLowerPriorityCompetitions(boolean cancelExcludeLowerPriorityCompetitions)
    {
        dirty(_cancelExcludeLowerPriorityCompetitions, cancelExcludeLowerPriorityCompetitions);
        _cancelExcludeLowerPriorityCompetitions = cancelExcludeLowerPriorityCompetitions;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EnrCancelParagraphGen)
        {
            setCancelledOrder(((EnrCancelParagraph)another).getCancelledOrder());
            setCancelExcludeLowerPriorityCompetitions(((EnrCancelParagraph)another).isCancelExcludeLowerPriorityCompetitions());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrCancelParagraphGen> extends EnrAbstractParagraph.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrCancelParagraph.class;
        }

        public T newInstance()
        {
            return (T) new EnrCancelParagraph();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "cancelledOrder":
                    return obj.getCancelledOrder();
                case "cancelExcludeLowerPriorityCompetitions":
                    return obj.isCancelExcludeLowerPriorityCompetitions();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "cancelledOrder":
                    obj.setCancelledOrder((EnrAbstractOrder) value);
                    return;
                case "cancelExcludeLowerPriorityCompetitions":
                    obj.setCancelExcludeLowerPriorityCompetitions((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "cancelledOrder":
                        return true;
                case "cancelExcludeLowerPriorityCompetitions":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "cancelledOrder":
                    return true;
                case "cancelExcludeLowerPriorityCompetitions":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "cancelledOrder":
                    return EnrAbstractOrder.class;
                case "cancelExcludeLowerPriorityCompetitions":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrCancelParagraph> _dslPath = new Path<EnrCancelParagraph>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrCancelParagraph");
    }
            

    /**
     * @return Абстрактный приказ на абитуриентов (2014). Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrCancelParagraph#getCancelledOrder()
     */
    public static EnrAbstractOrder.Path<EnrAbstractOrder> cancelledOrder()
    {
        return _dslPath.cancelledOrder();
    }

    /**
     * См. enrEnrollmentExtract.excludeLowerPriorityCompetitions.
     *
     * @return Отменить исключение абитуриентов из конкурсов с более низким приоритетом. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrCancelParagraph#isCancelExcludeLowerPriorityCompetitions()
     */
    public static PropertyPath<Boolean> cancelExcludeLowerPriorityCompetitions()
    {
        return _dslPath.cancelExcludeLowerPriorityCompetitions();
    }

    public static class Path<E extends EnrCancelParagraph> extends EnrAbstractParagraph.Path<E>
    {
        private EnrAbstractOrder.Path<EnrAbstractOrder> _cancelledOrder;
        private PropertyPath<Boolean> _cancelExcludeLowerPriorityCompetitions;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Абстрактный приказ на абитуриентов (2014). Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrCancelParagraph#getCancelledOrder()
     */
        public EnrAbstractOrder.Path<EnrAbstractOrder> cancelledOrder()
        {
            if(_cancelledOrder == null )
                _cancelledOrder = new EnrAbstractOrder.Path<EnrAbstractOrder>(L_CANCELLED_ORDER, this);
            return _cancelledOrder;
        }

    /**
     * См. enrEnrollmentExtract.excludeLowerPriorityCompetitions.
     *
     * @return Отменить исключение абитуриентов из конкурсов с более низким приоритетом. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrCancelParagraph#isCancelExcludeLowerPriorityCompetitions()
     */
        public PropertyPath<Boolean> cancelExcludeLowerPriorityCompetitions()
        {
            if(_cancelExcludeLowerPriorityCompetitions == null )
                _cancelExcludeLowerPriorityCompetitions = new PropertyPath<Boolean>(EnrCancelParagraphGen.P_CANCEL_EXCLUDE_LOWER_PRIORITY_COMPETITIONS, this);
            return _cancelExcludeLowerPriorityCompetitions;
        }

        public Class getEntityClass()
        {
            return EnrCancelParagraph.class;
        }

        public String getEntityName()
        {
            return "enrCancelParagraph";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
