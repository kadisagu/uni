package ru.tandemservice.unienr14.competition.entity;

import ru.tandemservice.unienr14.competition.bo.EnrCompetition.logic.EnrPlanZeroFixer;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.logic.EnrTargetAdmissionCompetitionPlanFormula;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.logic.IEnrCompetitionDao;
import ru.tandemservice.unienr14.competition.entity.gen.*;

/** @see ru.tandemservice.unienr14.competition.entity.gen.EnrTargetAdmissionCompetitionPlanGen */
public class EnrTargetAdmissionCompetitionPlan extends EnrTargetAdmissionCompetitionPlanGen implements IEnrCompetitionDao.ICalculatedPlanOwner, EnrPlanZeroFixer.IPlanOwner, EnrTargetAdmissionCompetitionPlanFormula.ITargetAdmissionPlan
{
    public EnrTargetAdmissionCompetitionPlan()
    {
    }

    public EnrTargetAdmissionCompetitionPlan(EnrTargetAdmissionPlan taPlan, EnrCompetition c, int plan, int calculatedPlan)
    {
        setTargetAdmissionPlan(taPlan);
        setCompetition(c);
        setPlan(plan);
        setCalculatedPlan(calculatedPlan);
    }

    @Override
    public String getEduLevelReqCode()
    {
        return getCompetition().getEduLevelRequirement().getCode();
    }

    @Override
    public String getCompTypeCode()
    {
        return getCompetition().getType().getCode();
    }

    @Override
    public int getFixPlan()
    {
        return getCalculatedPlan();
    }

    @Override
    public void setFixPlan(int plan)
    {
        setCalculatedPlan(plan);
    }

    @Override
    public Object planBalanceKey()
    {
        return getTargetAdmissionPlan();
    }
}