package ru.tandemservice.unienr14.exams.daemon;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import org.tandemframework.shared.commonbase.base.util.DoNotUseMe;
import org.tandemframework.shared.commonbase.base.util.DoNotUseMe.Reason;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.competition.entity.EnrExamVariant;
import ru.tandemservice.unienr14.exams.entity.EnrExamSet;
import ru.tandemservice.unienr14.exams.entity.EnrExamSetDiscipline;
import ru.tandemservice.unienr14.exams.entity.EnrExamSetElement;
import ru.tandemservice.unienr14.settings.daemon.EnrSettingsDaemonBean;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDisciplineGroup;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDisciplineGroupElement;

/**
 * @author vdanilov
 */
public class EnrExamSetDaemonDao extends UniBaseDao implements IEnrExamSetDaemonDao {

    @DoNotUseMe(reason=Reason.INTERNAL, comment="Внутренний механизм демона")
    public static final Runnable SETTINGS_DAEMON_ACTIONS = () -> {

        // обновляем перечень дисциплин набора ВИ (по данным элементов набора ВИ)
        try { IEnrExamSetDaemonDao.instance.get().doUpdateExamSetDisciplines(); }
        catch (final Exception t) { Debug.exception(t.getMessage(), t); }

    };

    @Override
    protected void initDao()
    {
        // при изменении набора ВИ вызывать демона
        EnrSettingsDaemonBean.DAEMON.registerWakeUpListener4Class(EnrExamSetElement.class);
    }

    @Override
    public boolean doUpdateExamSetDisciplines()
    {
        int updates = 0;
        Debug.begin("EnrExamSetDaemonDao.doUpdateExamSetDisciplines");
        try {
            final Session session = lock(EnrExamVariant.class.getName());
            final Map<Long, EnrCampaignDiscipline> dscMap = getLoadCacheMap(EnrCampaignDiscipline.class);
            final Map<Long, EnrExamSet> examSetMap = getLoadCacheMap(EnrExamSet.class);

            // сначала дисциплины (они имеют приоритет над группами): переборс ссылок, добавление новых
            Debug.begin("discipline");
            try {

                // перемещение связей (перемещаем ссылку на дисциплину, если, например, она ссылалась на группу)
                Debug.begin("update");
                try {
                    updates += executeAndClear(
                        new DQLUpdateBuilder(EnrExamSetDiscipline.class)

                        .fromEntity(EnrExamSetElement.class, "el")
                        .where(eq(property(EnrExamSetDiscipline.L_EXAM_SET), property(EnrExamSetElement.examSet().fromAlias("el"))))
                        .where(eq(property(EnrExamSetDiscipline.L_DISCIPLINE), property(EnrExamSetElement.value().fromAlias("el"))))

                        .set(EnrExamSetDiscipline.L_SOURCE, property("el"))
                        .where(ne(property(EnrExamSetDiscipline.L_SOURCE), property("el"))),
                        session
                    );
                } finally {
                    Debug.end();
                }

                // добавление новых связей (на основе дисциплин)
                Debug.begin("insert");
                try {
                    final List<Object[]> rows = new DQLSelectBuilder()
                    .fromEntity(EnrExamSetElement.class, "el")
                    .joinEntity("el", DQLJoinType.inner, EnrCampaignDiscipline.class, "dsc", eq(property("dsc"), property(EnrExamSetElement.value().fromAlias("el")))) // условие на то, что это дисциплина
                    .where(notExists(
                        new DQLSelectBuilder()
                        .fromEntity(EnrExamSetDiscipline.class, "rel")
                        .column(property("rel.id"))
                        .where(eq(property(EnrExamSetDiscipline.examSet().fromAlias("rel")), property(EnrExamSetElement.examSet().fromAlias("el"))))
                        .where(eq(property(EnrExamSetDiscipline.discipline().fromAlias("rel")), property(EnrExamSetElement.value().fromAlias("el"))))
                        .buildQuery()
                    ))
                    .column(property(EnrExamSetElement.id().fromAlias("el")))
                    .column(property(EnrExamSetElement.examSet().id().fromAlias("el")))
                    .column(property(EnrExamSetElement.value().id().fromAlias("el")))
                    .createStatement(session).list();

                    for (final Object[] row: rows) {
                        final EnrExamSet set = examSetMap.get(row[1]);
                        final EnrCampaignDiscipline dsc = dscMap.get(row[2]);
                        final EnrExamSetElement setElement = (EnrExamSetElement) session.load(EnrExamSetElement.class, (Long)row[0]); // не надо кэшировать (они гарантированно уникальны)
                        session.save(new EnrExamSetDiscipline(set, dsc, setElement));
                        updates ++;
                    }
                    session.flush();
                    session.clear();

                } finally {
                    Debug.end();
                }

            } finally {
                Debug.end();
            }

            // затем группы: удаление связей с дисциплинами, которых уже нет в группах, создание новых связей
            Debug.begin("group");
            try {

                // удаление связей, в случае, если в группе больше нет дисциплины
                Debug.begin("delete");
                try {
                    updates += executeAndClear(
                        new DQLDeleteBuilder(EnrExamSetDiscipline.class)
                        .where(in(
                            property("id"),
                            new DQLSelectBuilder()
                            .fromEntity(EnrExamSetDiscipline.class, "d").column(property("d.id"))
                            .fromEntity(EnrCampaignDisciplineGroup.class, "g").where(eq(property("g"), property(EnrExamSetDiscipline.source().value().fromAlias("d")))) // условие на то, что это группа
                            .where(notExists(
                                new DQLSelectBuilder()
                                .fromEntity(EnrCampaignDisciplineGroupElement.class, "e").column(property("e.id"))
                                .where(eq(property(EnrCampaignDisciplineGroupElement.group().fromAlias("e")), property("g")))
                                .where(eq(property(EnrCampaignDisciplineGroupElement.discipline().fromAlias("e")), property(EnrExamSetDiscipline.discipline().fromAlias("d"))))
                                .buildQuery()
                            ))
                            .buildQuery()
                        )),

                        session
                    );
                } finally {
                    Debug.end();
                }

                // добавление связей (на основе содержимого групп)
                Debug.begin("insert");
                try {
                    final List<Object[]> rows = new DQLSelectBuilder()
                    .fromEntity(EnrExamSetElement.class, "el")
                    .joinEntity("el", DQLJoinType.inner, EnrCampaignDisciplineGroupElement.class, "gel", eq(property(EnrCampaignDisciplineGroupElement.group().fromAlias("gel")), property(EnrExamSetElement.value().fromAlias("el")))) // условие на то, что это дисциплина
                    .where(notExists(
                        new DQLSelectBuilder()
                        .fromEntity(EnrExamSetDiscipline.class, "rel")
                        .column(property("rel.id"))
                        .where(eq(property(EnrExamSetDiscipline.examSet().fromAlias("rel")), property(EnrExamSetElement.examSet().fromAlias("el"))))
                        .where(eq(property(EnrExamSetDiscipline.discipline().fromAlias("rel")), property(EnrCampaignDisciplineGroupElement.discipline().fromAlias("gel"))))
                        .buildQuery()
                    ))
                    .column(property(EnrExamSetElement.id().fromAlias("el")))
                    .column(property(EnrExamSetElement.examSet().id().fromAlias("el")))
                    .column(property(EnrCampaignDisciplineGroupElement.discipline().id().fromAlias("gel")))
                    .createStatement(session).list();

                    for (final Object[] row: rows) {
                        final EnrExamSet set = examSetMap.get(row[1]);
                        final EnrCampaignDiscipline dsc = dscMap.get(row[2]);
                        final EnrExamSetElement setElement = (EnrExamSetElement) session.load(EnrExamSetElement.class, (Long)row[0]); // не надо кэшировать (они гарантированно уникальны)
                        session.save(new EnrExamSetDiscipline(set, dsc, setElement));
                        updates ++;
                    }
                    session.flush();
                    session.clear();

                } finally {
                    Debug.end();
                }

            } finally {
                Debug.end();
            }

        } finally {
            Debug.end();
        }
        return updates > 0;
    }


}
