package ru.tandemservice.unienr14.enrollment.entity;

import org.tandemframework.core.common.ITitled;

import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.select.IEnrEnrollmentStepItem;
import ru.tandemservice.unienr14.enrollment.entity.gen.EnrEnrollmentStepItemGen;

/**
 * Абитуриент в конкурсном списке
 */
public class EnrEnrollmentStepItem extends EnrEnrollmentStepItemGen implements ITitled, IEnrEnrollmentStepItem
{
    @Override
    public String getTitle()
    {
        if (getStep() == null) {
            return this.getClass().getSimpleName();
        }
        return "Абитуриент в шаге зачисления: " + getStep().getDateStr() + ", " + getEntity().getEntrant().getFio() + ", " + getEntity().getTitle();
    }

}