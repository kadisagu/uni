package ru.tandemservice.unienr14.competition.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.competition.entity.EnrTargetAdmissionPlan;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignTargetAdmissionKind;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * План приема по виду ЦП
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrTargetAdmissionPlanGen extends EntityBase
 implements INaturalIdentifiable<EnrTargetAdmissionPlanGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.competition.entity.EnrTargetAdmissionPlan";
    public static final String ENTITY_NAME = "enrTargetAdmissionPlan";
    public static final int VERSION_HASH = 572800623;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENR_CAMPAIGN_T_A_KIND = "enrCampaignTAKind";
    public static final String L_PROGRAM_SET_ORG_UNIT = "programSetOrgUnit";
    public static final String P_PLAN = "plan";
    public static final String P_PLAN_STRING = "planString";

    private EnrCampaignTargetAdmissionKind _enrCampaignTAKind;     // Вид ЦП, используемый в рамках ПК
    private EnrProgramSetOrgUnit _programSetOrgUnit;     // Подразделение, ведущее прием по набору ОП
    private int _plan;     // План приема

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Вид ЦП, используемый в рамках ПК. Свойство не может быть null.
     */
    @NotNull
    public EnrCampaignTargetAdmissionKind getEnrCampaignTAKind()
    {
        return _enrCampaignTAKind;
    }

    /**
     * @param enrCampaignTAKind Вид ЦП, используемый в рамках ПК. Свойство не может быть null.
     */
    public void setEnrCampaignTAKind(EnrCampaignTargetAdmissionKind enrCampaignTAKind)
    {
        dirty(_enrCampaignTAKind, enrCampaignTAKind);
        _enrCampaignTAKind = enrCampaignTAKind;
    }

    /**
     * @return Подразделение, ведущее прием по набору ОП. Свойство не может быть null.
     */
    @NotNull
    public EnrProgramSetOrgUnit getProgramSetOrgUnit()
    {
        return _programSetOrgUnit;
    }

    /**
     * @param programSetOrgUnit Подразделение, ведущее прием по набору ОП. Свойство не может быть null.
     */
    public void setProgramSetOrgUnit(EnrProgramSetOrgUnit programSetOrgUnit)
    {
        dirty(_programSetOrgUnit, programSetOrgUnit);
        _programSetOrgUnit = programSetOrgUnit;
    }

    /**
     * @return План приема. Свойство не может быть null.
     */
    @NotNull
    public int getPlan()
    {
        return _plan;
    }

    /**
     * @param plan План приема. Свойство не может быть null.
     */
    public void setPlan(int plan)
    {
        dirty(_plan, plan);
        _plan = plan;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrTargetAdmissionPlanGen)
        {
            if (withNaturalIdProperties)
            {
                setEnrCampaignTAKind(((EnrTargetAdmissionPlan)another).getEnrCampaignTAKind());
                setProgramSetOrgUnit(((EnrTargetAdmissionPlan)another).getProgramSetOrgUnit());
            }
            setPlan(((EnrTargetAdmissionPlan)another).getPlan());
        }
    }

    public INaturalId<EnrTargetAdmissionPlanGen> getNaturalId()
    {
        return new NaturalId(getEnrCampaignTAKind(), getProgramSetOrgUnit());
    }

    public static class NaturalId extends NaturalIdBase<EnrTargetAdmissionPlanGen>
    {
        private static final String PROXY_NAME = "EnrTargetAdmissionPlanNaturalProxy";

        private Long _enrCampaignTAKind;
        private Long _programSetOrgUnit;

        public NaturalId()
        {}

        public NaturalId(EnrCampaignTargetAdmissionKind enrCampaignTAKind, EnrProgramSetOrgUnit programSetOrgUnit)
        {
            _enrCampaignTAKind = ((IEntity) enrCampaignTAKind).getId();
            _programSetOrgUnit = ((IEntity) programSetOrgUnit).getId();
        }

        public Long getEnrCampaignTAKind()
        {
            return _enrCampaignTAKind;
        }

        public void setEnrCampaignTAKind(Long enrCampaignTAKind)
        {
            _enrCampaignTAKind = enrCampaignTAKind;
        }

        public Long getProgramSetOrgUnit()
        {
            return _programSetOrgUnit;
        }

        public void setProgramSetOrgUnit(Long programSetOrgUnit)
        {
            _programSetOrgUnit = programSetOrgUnit;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrTargetAdmissionPlanGen.NaturalId) ) return false;

            EnrTargetAdmissionPlanGen.NaturalId that = (NaturalId) o;

            if( !equals(getEnrCampaignTAKind(), that.getEnrCampaignTAKind()) ) return false;
            if( !equals(getProgramSetOrgUnit(), that.getProgramSetOrgUnit()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEnrCampaignTAKind());
            result = hashCode(result, getProgramSetOrgUnit());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEnrCampaignTAKind());
            sb.append("/");
            sb.append(getProgramSetOrgUnit());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrTargetAdmissionPlanGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrTargetAdmissionPlan.class;
        }

        public T newInstance()
        {
            return (T) new EnrTargetAdmissionPlan();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrCampaignTAKind":
                    return obj.getEnrCampaignTAKind();
                case "programSetOrgUnit":
                    return obj.getProgramSetOrgUnit();
                case "plan":
                    return obj.getPlan();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrCampaignTAKind":
                    obj.setEnrCampaignTAKind((EnrCampaignTargetAdmissionKind) value);
                    return;
                case "programSetOrgUnit":
                    obj.setProgramSetOrgUnit((EnrProgramSetOrgUnit) value);
                    return;
                case "plan":
                    obj.setPlan((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrCampaignTAKind":
                        return true;
                case "programSetOrgUnit":
                        return true;
                case "plan":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrCampaignTAKind":
                    return true;
                case "programSetOrgUnit":
                    return true;
                case "plan":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrCampaignTAKind":
                    return EnrCampaignTargetAdmissionKind.class;
                case "programSetOrgUnit":
                    return EnrProgramSetOrgUnit.class;
                case "plan":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrTargetAdmissionPlan> _dslPath = new Path<EnrTargetAdmissionPlan>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrTargetAdmissionPlan");
    }
            

    /**
     * @return Вид ЦП, используемый в рамках ПК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrTargetAdmissionPlan#getEnrCampaignTAKind()
     */
    public static EnrCampaignTargetAdmissionKind.Path<EnrCampaignTargetAdmissionKind> enrCampaignTAKind()
    {
        return _dslPath.enrCampaignTAKind();
    }

    /**
     * @return Подразделение, ведущее прием по набору ОП. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrTargetAdmissionPlan#getProgramSetOrgUnit()
     */
    public static EnrProgramSetOrgUnit.Path<EnrProgramSetOrgUnit> programSetOrgUnit()
    {
        return _dslPath.programSetOrgUnit();
    }

    /**
     * @return План приема. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrTargetAdmissionPlan#getPlan()
     */
    public static PropertyPath<Integer> plan()
    {
        return _dslPath.plan();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.competition.entity.EnrTargetAdmissionPlan#getPlanString()
     */
    public static SupportedPropertyPath<String> planString()
    {
        return _dslPath.planString();
    }

    public static class Path<E extends EnrTargetAdmissionPlan> extends EntityPath<E>
    {
        private EnrCampaignTargetAdmissionKind.Path<EnrCampaignTargetAdmissionKind> _enrCampaignTAKind;
        private EnrProgramSetOrgUnit.Path<EnrProgramSetOrgUnit> _programSetOrgUnit;
        private PropertyPath<Integer> _plan;
        private SupportedPropertyPath<String> _planString;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Вид ЦП, используемый в рамках ПК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrTargetAdmissionPlan#getEnrCampaignTAKind()
     */
        public EnrCampaignTargetAdmissionKind.Path<EnrCampaignTargetAdmissionKind> enrCampaignTAKind()
        {
            if(_enrCampaignTAKind == null )
                _enrCampaignTAKind = new EnrCampaignTargetAdmissionKind.Path<EnrCampaignTargetAdmissionKind>(L_ENR_CAMPAIGN_T_A_KIND, this);
            return _enrCampaignTAKind;
        }

    /**
     * @return Подразделение, ведущее прием по набору ОП. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrTargetAdmissionPlan#getProgramSetOrgUnit()
     */
        public EnrProgramSetOrgUnit.Path<EnrProgramSetOrgUnit> programSetOrgUnit()
        {
            if(_programSetOrgUnit == null )
                _programSetOrgUnit = new EnrProgramSetOrgUnit.Path<EnrProgramSetOrgUnit>(L_PROGRAM_SET_ORG_UNIT, this);
            return _programSetOrgUnit;
        }

    /**
     * @return План приема. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrTargetAdmissionPlan#getPlan()
     */
        public PropertyPath<Integer> plan()
        {
            if(_plan == null )
                _plan = new PropertyPath<Integer>(EnrTargetAdmissionPlanGen.P_PLAN, this);
            return _plan;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.competition.entity.EnrTargetAdmissionPlan#getPlanString()
     */
        public SupportedPropertyPath<String> planString()
        {
            if(_planString == null )
                _planString = new SupportedPropertyPath<String>(EnrTargetAdmissionPlanGen.P_PLAN_STRING, this);
            return _planString;
        }

        public Class getEntityClass()
        {
            return EnrTargetAdmissionPlan.class;
        }

        public String getEntityName()
        {
            return "enrTargetAdmissionPlan";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getPlanString();
}
