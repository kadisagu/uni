/* $Id:$ */
package ru.tandemservice.unienr14.ws.order;

import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.Date;
import java.util.List;

/**
 * @author oleyba
 * @since 7/20/14
 */
public interface IEnrOrderServiceDao
{
    public static final SpringBeanCache<IEnrOrderServiceDao> INSTANCE = new SpringBeanCache<IEnrOrderServiceDao>(IEnrOrderServiceDao.class.getName());

    EnrOrderEnvironmentNode getOrderData(String enrollmentCampaignTitle, Date dateFrom, Date dateTo, String compensationTypeId, List<String> formativeOrgUnitIds, List<String> programFormIds, boolean withPrintData);

    EnrOrderEnvironmentNode getOrderData(EnrEnrollmentCampaign campaign);
}
