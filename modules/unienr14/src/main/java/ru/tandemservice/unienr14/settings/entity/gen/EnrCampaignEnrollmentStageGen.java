package ru.tandemservice.unienr14.settings.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignEnrollmentStage;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Стадия зачисления в рамках ПК
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrCampaignEnrollmentStageGen extends EntityBase
 implements INaturalIdentifiable<EnrCampaignEnrollmentStageGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.settings.entity.EnrCampaignEnrollmentStage";
    public static final String ENTITY_NAME = "enrCampaignEnrollmentStage";
    public static final int VERSION_HASH = -771825996;
    private static IEntityMeta ENTITY_META;

    public static final String L_REQUEST_TYPE = "requestType";
    public static final String L_COMPETITION_TYPE = "competitionType";
    public static final String L_PROGRAM_FORM = "programForm";
    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_DATE = "date";
    public static final String P_ACCEPT_PEOPLE_RESIDING_IN_CRIMEA = "acceptPeopleResidingInCrimea";

    private EnrRequestType _requestType;     // Вид заявления
    private EnrCompetitionType _competitionType;     // Вид приема
    private EduProgramForm _programForm;     // Форма обучения
    private EnrEnrollmentCampaign _enrollmentCampaign;     // ПК
    private Date _date;     // Дата зачисления
    private boolean _acceptPeopleResidingInCrimea = false;     // Прием лиц, постоянно проживающих в Крыму

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Вид заявления. Свойство не может быть null.
     */
    @NotNull
    public EnrRequestType getRequestType()
    {
        return _requestType;
    }

    /**
     * @param requestType Вид заявления. Свойство не может быть null.
     */
    public void setRequestType(EnrRequestType requestType)
    {
        dirty(_requestType, requestType);
        _requestType = requestType;
    }

    /**
     * @return Вид приема. Свойство не может быть null.
     */
    @NotNull
    public EnrCompetitionType getCompetitionType()
    {
        return _competitionType;
    }

    /**
     * @param competitionType Вид приема. Свойство не может быть null.
     */
    public void setCompetitionType(EnrCompetitionType competitionType)
    {
        dirty(_competitionType, competitionType);
        _competitionType = competitionType;
    }

    /**
     * @return Форма обучения. Свойство не может быть null.
     */
    @NotNull
    public EduProgramForm getProgramForm()
    {
        return _programForm;
    }

    /**
     * @param programForm Форма обучения. Свойство не может быть null.
     */
    public void setProgramForm(EduProgramForm programForm)
    {
        dirty(_programForm, programForm);
        _programForm = programForm;
    }

    /**
     * @return ПК. Свойство не может быть null.
     */
    @NotNull
    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign ПК. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Дата зачисления. Свойство не может быть null.
     */
    @NotNull
    public Date getDate()
    {
        return _date;
    }

    /**
     * @param date Дата зачисления. Свойство не может быть null.
     */
    public void setDate(Date date)
    {
        dirty(_date, date);
        _date = date;
    }

    /**
     * @return Прием лиц, постоянно проживающих в Крыму. Свойство не может быть null.
     */
    @NotNull
    public boolean isAcceptPeopleResidingInCrimea()
    {
        return _acceptPeopleResidingInCrimea;
    }

    /**
     * @param acceptPeopleResidingInCrimea Прием лиц, постоянно проживающих в Крыму. Свойство не может быть null.
     */
    public void setAcceptPeopleResidingInCrimea(boolean acceptPeopleResidingInCrimea)
    {
        dirty(_acceptPeopleResidingInCrimea, acceptPeopleResidingInCrimea);
        _acceptPeopleResidingInCrimea = acceptPeopleResidingInCrimea;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrCampaignEnrollmentStageGen)
        {
            if (withNaturalIdProperties)
            {
                setRequestType(((EnrCampaignEnrollmentStage)another).getRequestType());
                setCompetitionType(((EnrCampaignEnrollmentStage)another).getCompetitionType());
                setProgramForm(((EnrCampaignEnrollmentStage)another).getProgramForm());
                setEnrollmentCampaign(((EnrCampaignEnrollmentStage)another).getEnrollmentCampaign());
                setDate(((EnrCampaignEnrollmentStage)another).getDate());
                setAcceptPeopleResidingInCrimea(((EnrCampaignEnrollmentStage)another).isAcceptPeopleResidingInCrimea());
            }
        }
    }

    public INaturalId<EnrCampaignEnrollmentStageGen> getNaturalId()
    {
        return new NaturalId(getRequestType(), getCompetitionType(), getProgramForm(), getEnrollmentCampaign(), getDate(), isAcceptPeopleResidingInCrimea());
    }

    public static class NaturalId extends NaturalIdBase<EnrCampaignEnrollmentStageGen>
    {
        private static final String PROXY_NAME = "EnrCampaignEnrollmentStageNaturalProxy";

        private Long _requestType;
        private Long _competitionType;
        private Long _programForm;
        private Long _enrollmentCampaign;
        private Date _date;
        private boolean _acceptPeopleResidingInCrimea;

        public NaturalId()
        {}

        public NaturalId(EnrRequestType requestType, EnrCompetitionType competitionType, EduProgramForm programForm, EnrEnrollmentCampaign enrollmentCampaign, Date date, boolean acceptPeopleResidingInCrimea)
        {
            _requestType = ((IEntity) requestType).getId();
            _competitionType = ((IEntity) competitionType).getId();
            _programForm = ((IEntity) programForm).getId();
            _enrollmentCampaign = ((IEntity) enrollmentCampaign).getId();
            _date = org.tandemframework.core.CoreDateUtils.getDayFirstTimeMoment(date);
            _acceptPeopleResidingInCrimea = acceptPeopleResidingInCrimea;
        }

        public Long getRequestType()
        {
            return _requestType;
        }

        public void setRequestType(Long requestType)
        {
            _requestType = requestType;
        }

        public Long getCompetitionType()
        {
            return _competitionType;
        }

        public void setCompetitionType(Long competitionType)
        {
            _competitionType = competitionType;
        }

        public Long getProgramForm()
        {
            return _programForm;
        }

        public void setProgramForm(Long programForm)
        {
            _programForm = programForm;
        }

        public Long getEnrollmentCampaign()
        {
            return _enrollmentCampaign;
        }

        public void setEnrollmentCampaign(Long enrollmentCampaign)
        {
            _enrollmentCampaign = enrollmentCampaign;
        }

        public Date getDate()
        {
            return _date;
        }

        public void setDate(Date date)
        {
            _date = org.tandemframework.core.CoreDateUtils.getDayFirstTimeMoment(date);
        }

        public boolean isAcceptPeopleResidingInCrimea()
        {
            return _acceptPeopleResidingInCrimea;
        }

        public void setAcceptPeopleResidingInCrimea(boolean acceptPeopleResidingInCrimea)
        {
            _acceptPeopleResidingInCrimea = acceptPeopleResidingInCrimea;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrCampaignEnrollmentStageGen.NaturalId) ) return false;

            EnrCampaignEnrollmentStageGen.NaturalId that = (NaturalId) o;

            if( !equals(getRequestType(), that.getRequestType()) ) return false;
            if( !equals(getCompetitionType(), that.getCompetitionType()) ) return false;
            if( !equals(getProgramForm(), that.getProgramForm()) ) return false;
            if( !equals(getEnrollmentCampaign(), that.getEnrollmentCampaign()) ) return false;
            if( !equals(getDate(), that.getDate()) ) return false;
            if( !equals(isAcceptPeopleResidingInCrimea(), that.isAcceptPeopleResidingInCrimea()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getRequestType());
            result = hashCode(result, getCompetitionType());
            result = hashCode(result, getProgramForm());
            result = hashCode(result, getEnrollmentCampaign());
            result = hashCode(result, getDate());
            result = hashCode(result, isAcceptPeopleResidingInCrimea());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getRequestType());
            sb.append("/");
            sb.append(getCompetitionType());
            sb.append("/");
            sb.append(getProgramForm());
            sb.append("/");
            sb.append(getEnrollmentCampaign());
            sb.append("/");
            sb.append(getDate());
            sb.append("/");
            sb.append(isAcceptPeopleResidingInCrimea());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrCampaignEnrollmentStageGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrCampaignEnrollmentStage.class;
        }

        public T newInstance()
        {
            return (T) new EnrCampaignEnrollmentStage();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "requestType":
                    return obj.getRequestType();
                case "competitionType":
                    return obj.getCompetitionType();
                case "programForm":
                    return obj.getProgramForm();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "date":
                    return obj.getDate();
                case "acceptPeopleResidingInCrimea":
                    return obj.isAcceptPeopleResidingInCrimea();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "requestType":
                    obj.setRequestType((EnrRequestType) value);
                    return;
                case "competitionType":
                    obj.setCompetitionType((EnrCompetitionType) value);
                    return;
                case "programForm":
                    obj.setProgramForm((EduProgramForm) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrEnrollmentCampaign) value);
                    return;
                case "date":
                    obj.setDate((Date) value);
                    return;
                case "acceptPeopleResidingInCrimea":
                    obj.setAcceptPeopleResidingInCrimea((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "requestType":
                        return true;
                case "competitionType":
                        return true;
                case "programForm":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "date":
                        return true;
                case "acceptPeopleResidingInCrimea":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "requestType":
                    return true;
                case "competitionType":
                    return true;
                case "programForm":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "date":
                    return true;
                case "acceptPeopleResidingInCrimea":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "requestType":
                    return EnrRequestType.class;
                case "competitionType":
                    return EnrCompetitionType.class;
                case "programForm":
                    return EduProgramForm.class;
                case "enrollmentCampaign":
                    return EnrEnrollmentCampaign.class;
                case "date":
                    return Date.class;
                case "acceptPeopleResidingInCrimea":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrCampaignEnrollmentStage> _dslPath = new Path<EnrCampaignEnrollmentStage>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrCampaignEnrollmentStage");
    }
            

    /**
     * @return Вид заявления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignEnrollmentStage#getRequestType()
     */
    public static EnrRequestType.Path<EnrRequestType> requestType()
    {
        return _dslPath.requestType();
    }

    /**
     * @return Вид приема. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignEnrollmentStage#getCompetitionType()
     */
    public static EnrCompetitionType.Path<EnrCompetitionType> competitionType()
    {
        return _dslPath.competitionType();
    }

    /**
     * @return Форма обучения. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignEnrollmentStage#getProgramForm()
     */
    public static EduProgramForm.Path<EduProgramForm> programForm()
    {
        return _dslPath.programForm();
    }

    /**
     * @return ПК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignEnrollmentStage#getEnrollmentCampaign()
     */
    public static EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Дата зачисления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignEnrollmentStage#getDate()
     */
    public static PropertyPath<Date> date()
    {
        return _dslPath.date();
    }

    /**
     * @return Прием лиц, постоянно проживающих в Крыму. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignEnrollmentStage#isAcceptPeopleResidingInCrimea()
     */
    public static PropertyPath<Boolean> acceptPeopleResidingInCrimea()
    {
        return _dslPath.acceptPeopleResidingInCrimea();
    }

    public static class Path<E extends EnrCampaignEnrollmentStage> extends EntityPath<E>
    {
        private EnrRequestType.Path<EnrRequestType> _requestType;
        private EnrCompetitionType.Path<EnrCompetitionType> _competitionType;
        private EduProgramForm.Path<EduProgramForm> _programForm;
        private EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<Date> _date;
        private PropertyPath<Boolean> _acceptPeopleResidingInCrimea;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Вид заявления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignEnrollmentStage#getRequestType()
     */
        public EnrRequestType.Path<EnrRequestType> requestType()
        {
            if(_requestType == null )
                _requestType = new EnrRequestType.Path<EnrRequestType>(L_REQUEST_TYPE, this);
            return _requestType;
        }

    /**
     * @return Вид приема. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignEnrollmentStage#getCompetitionType()
     */
        public EnrCompetitionType.Path<EnrCompetitionType> competitionType()
        {
            if(_competitionType == null )
                _competitionType = new EnrCompetitionType.Path<EnrCompetitionType>(L_COMPETITION_TYPE, this);
            return _competitionType;
        }

    /**
     * @return Форма обучения. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignEnrollmentStage#getProgramForm()
     */
        public EduProgramForm.Path<EduProgramForm> programForm()
        {
            if(_programForm == null )
                _programForm = new EduProgramForm.Path<EduProgramForm>(L_PROGRAM_FORM, this);
            return _programForm;
        }

    /**
     * @return ПК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignEnrollmentStage#getEnrollmentCampaign()
     */
        public EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Дата зачисления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignEnrollmentStage#getDate()
     */
        public PropertyPath<Date> date()
        {
            if(_date == null )
                _date = new PropertyPath<Date>(EnrCampaignEnrollmentStageGen.P_DATE, this);
            return _date;
        }

    /**
     * @return Прием лиц, постоянно проживающих в Крыму. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignEnrollmentStage#isAcceptPeopleResidingInCrimea()
     */
        public PropertyPath<Boolean> acceptPeopleResidingInCrimea()
        {
            if(_acceptPeopleResidingInCrimea == null )
                _acceptPeopleResidingInCrimea = new PropertyPath<Boolean>(EnrCampaignEnrollmentStageGen.P_ACCEPT_PEOPLE_RESIDING_IN_CRIMEA, this);
            return _acceptPeopleResidingInCrimea;
        }

        public Class getEntityClass()
        {
            return EnrCampaignEnrollmentStage.class;
        }

        public String getEntityName()
        {
            return "enrCampaignEnrollmentStage";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
