/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsEduOrgDistributionAdd;/**
 * @author rsizonenko
 * @since 19.06.2014
 */

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.person.catalog.entity.EduDocumentKind;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;

@Configuration
public class EnrReportEntrantsEduOrgDistributionAdd extends BusinessComponentManager {
    public static final String ENR_EDU_DOCUMENT_KIND = "eduDocumentKind";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
                .addAddon(uiAddon(CommonFilterAddon.class.getSimpleName(), EnrCompetitionFilterAddon.class))
                .addDataSource(selectDS(ENR_EDU_DOCUMENT_KIND, eduDocumentKindHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler eduDocumentKindHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduDocumentKind.class)
                .filter(EduDocumentKind.title())
                .order(EduDocumentKind.title())
                .pageable(true);
    }
}
