package ru.tandemservice.unienr14.extreports.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Контекст внешнего отчета"
 * Имя сущности : extReportContext
 * Файл data.xml : unienr.extreport.data.xml
 */
public interface ExtReportContextCodes
{
    /** Константа кода (code) элемента : Глобальные отчеты (title) */
    String GLOBAL_REPORT_CONTEXT = "globalReportContext";
    /** Константа кода (code) элемента : Головное подразделение (title) */
    String TOP_ORG_UNIT_CONTEXT = "uniTopOrgUnitContext";
    /** Константа кода (code) элемента : Подразделение, ведущее прием (title) */
    String ENR_ORG_UNIT_CONTEXT = "uniEnrOrgUnitContext";

    Set<String> CODES = ImmutableSet.of(GLOBAL_REPORT_CONTEXT, TOP_ORG_UNIT_CONTEXT, ENR_ORG_UNIT_CONTEXT);
}
