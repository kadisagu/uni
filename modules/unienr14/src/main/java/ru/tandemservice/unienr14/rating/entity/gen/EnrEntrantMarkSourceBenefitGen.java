package ru.tandemservice.unienr14.rating.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unienr14.catalog.entity.EnrBenefitCategory;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSource;
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceBenefit;
import ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitProofDocument;
import ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitStatement;
import ru.tandemservice.unienr14.request.entity.gen.IEnrEntrantBenefitProofDocumentGen;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Основание балла (особое право)
 *
 * Создается при зачтении документа, предоставляющего особое право за 100 баллов по ВВИ, с учетом формы.
 * Обновляется демоном IEnrRatingDaemonDao#doRefreshSource4Benefit (обновление баллов по особым правам).
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrEntrantMarkSourceBenefitGen extends EnrEntrantMarkSource
 implements IEnrEntrantBenefitStatement{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceBenefit";
    public static final String ENTITY_NAME = "enrEntrantMarkSourceBenefit";
    public static final int VERSION_HASH = 1440969386;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTRANT = "entrant";
    public static final String L_BENEFIT_CATEGORY = "benefitCategory";
    public static final String L_MAIN_PROOF = "mainProof";

    private EnrEntrant _entrant;     // Абитуриент
    private EnrBenefitCategory _benefitCategory;     // Категория особого права
    private IEnrEntrantBenefitProofDocument _mainProof;     // Основной подтверждающий документ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Абитуриент.
     *
     * Это формула "chosenEntranceExamForm.chosenEntranceExam.requestedCompetition.request.entrant".
     */
    public EnrEntrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant Абитуриент.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setEntrant(EnrEntrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    /**
     * @return Категория особого права.
     */
    public EnrBenefitCategory getBenefitCategory()
    {
        return _benefitCategory;
    }

    /**
     * @param benefitCategory Категория особого права.
     */
    public void setBenefitCategory(EnrBenefitCategory benefitCategory)
    {
        dirty(_benefitCategory, benefitCategory);
        _benefitCategory = benefitCategory;
    }

    /**
     * @return Основной подтверждающий документ.
     */
    public IEnrEntrantBenefitProofDocument getMainProof()
    {
        return _mainProof;
    }

    /**
     * @param mainProof Основной подтверждающий документ.
     */
    public void setMainProof(IEnrEntrantBenefitProofDocument mainProof)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && mainProof!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IEnrEntrantBenefitProofDocument.class);
            IEntityMeta actual =  mainProof instanceof IEntity ? EntityRuntime.getMeta((IEntity) mainProof) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_mainProof, mainProof);
        _mainProof = mainProof;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EnrEntrantMarkSourceBenefitGen)
        {
            setEntrant(((EnrEntrantMarkSourceBenefit)another).getEntrant());
            setBenefitCategory(((EnrEntrantMarkSourceBenefit)another).getBenefitCategory());
            setMainProof(((EnrEntrantMarkSourceBenefit)another).getMainProof());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrEntrantMarkSourceBenefitGen> extends EnrEntrantMarkSource.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrEntrantMarkSourceBenefit.class;
        }

        public T newInstance()
        {
            return (T) new EnrEntrantMarkSourceBenefit();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "entrant":
                    return obj.getEntrant();
                case "benefitCategory":
                    return obj.getBenefitCategory();
                case "mainProof":
                    return obj.getMainProof();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "entrant":
                    obj.setEntrant((EnrEntrant) value);
                    return;
                case "benefitCategory":
                    obj.setBenefitCategory((EnrBenefitCategory) value);
                    return;
                case "mainProof":
                    obj.setMainProof((IEnrEntrantBenefitProofDocument) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "entrant":
                        return true;
                case "benefitCategory":
                        return true;
                case "mainProof":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "entrant":
                    return true;
                case "benefitCategory":
                    return true;
                case "mainProof":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "entrant":
                    return EnrEntrant.class;
                case "benefitCategory":
                    return EnrBenefitCategory.class;
                case "mainProof":
                    return IEnrEntrantBenefitProofDocument.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrEntrantMarkSourceBenefit> _dslPath = new Path<EnrEntrantMarkSourceBenefit>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrEntrantMarkSourceBenefit");
    }
            

    /**
     * @return Абитуриент.
     *
     * Это формула "chosenEntranceExamForm.chosenEntranceExam.requestedCompetition.request.entrant".
     * @see ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceBenefit#getEntrant()
     */
    public static EnrEntrant.Path<EnrEntrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return Категория особого права.
     * @see ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceBenefit#getBenefitCategory()
     */
    public static EnrBenefitCategory.Path<EnrBenefitCategory> benefitCategory()
    {
        return _dslPath.benefitCategory();
    }

    /**
     * @return Основной подтверждающий документ.
     * @see ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceBenefit#getMainProof()
     */
    public static IEnrEntrantBenefitProofDocumentGen.Path<IEnrEntrantBenefitProofDocument> mainProof()
    {
        return _dslPath.mainProof();
    }

    public static class Path<E extends EnrEntrantMarkSourceBenefit> extends EnrEntrantMarkSource.Path<E>
    {
        private EnrEntrant.Path<EnrEntrant> _entrant;
        private EnrBenefitCategory.Path<EnrBenefitCategory> _benefitCategory;
        private IEnrEntrantBenefitProofDocumentGen.Path<IEnrEntrantBenefitProofDocument> _mainProof;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Абитуриент.
     *
     * Это формула "chosenEntranceExamForm.chosenEntranceExam.requestedCompetition.request.entrant".
     * @see ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceBenefit#getEntrant()
     */
        public EnrEntrant.Path<EnrEntrant> entrant()
        {
            if(_entrant == null )
                _entrant = new EnrEntrant.Path<EnrEntrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return Категория особого права.
     * @see ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceBenefit#getBenefitCategory()
     */
        public EnrBenefitCategory.Path<EnrBenefitCategory> benefitCategory()
        {
            if(_benefitCategory == null )
                _benefitCategory = new EnrBenefitCategory.Path<EnrBenefitCategory>(L_BENEFIT_CATEGORY, this);
            return _benefitCategory;
        }

    /**
     * @return Основной подтверждающий документ.
     * @see ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceBenefit#getMainProof()
     */
        public IEnrEntrantBenefitProofDocumentGen.Path<IEnrEntrantBenefitProofDocument> mainProof()
        {
            if(_mainProof == null )
                _mainProof = new IEnrEntrantBenefitProofDocumentGen.Path<IEnrEntrantBenefitProofDocument>(L_MAIN_PROOF, this);
            return _mainProof;
        }

        public Class getEntityClass()
        {
            return EnrEntrantMarkSourceBenefit.class;
        }

        public String getEntityName()
        {
            return "enrEntrantMarkSourceBenefit";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
