package ru.tandemservice.unienr14.rating.daemon;

import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.shared.commonbase.base.util.DoNotUseMe;
import org.tandemframework.shared.commonbase.base.util.DoNotUseMe.Reason;

/**
 * @author vdanilov
 */
public interface IEnrRatingDaemonDao {

    final SpringBeanCache<IEnrRatingDaemonDao> instance = new SpringBeanCache<IEnrRatingDaemonDao>(IEnrRatingDaemonDao.class.getName());


    /** @return ICommonDAO.doInTransaction */
    @DoNotUseMe(reason=Reason.INTERNAL, comment="Внутренний механизм демона")
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    <T> T doInTransaction(HibernateCallback<T> actionCallback);

    /** @return ICommonDAO.getCalculatedValue */
    @DoNotUseMe(reason=Reason.INTERNAL, comment="Внутренний механизм демона")
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true, noRollbackFor=Throwable.class)
    <T> T getCalculatedValue(HibernateCallback<T> calculationCallback);

    /**
     * Обновляет enrRequestedCompetition.achievementMark
     * @return true, если что-то было изменено
     */
    @DoNotUseMe(reason=Reason.INTERNAL, comment="Внутренний механизм демона")
    @Transactional(propagation=Propagation.REQUIRED)
    boolean doUpdateAchievementMark();

    /**
     * Обновляет:
     *  1. enrChosenEntranceExam.actualExam
     *  2. enrChosenEntranceExamForm.actualExamPassForm
     *  3. enrChosenEntranceExam.statusPassFormsCorrect
     * @return true, если что-то было изменено
     */
    @DoNotUseMe(reason=Reason.INTERNAL, comment="Внутренний механизм демона")
    @Transactional(propagation=Propagation.REQUIRED)
    boolean doRefreshChosenEntranceExams();

    /**
     * создает необходимые enrEntrantMarkSourceStateExam на основе ЕГЭ абитуриента,
     * обновляет дату актуальности основания enrEntrantMarkSourceStateExam
     * @return true, если что-то было изменено
     */
    @DoNotUseMe(reason=Reason.INTERNAL, comment="Внутренний механизм демона")
    @Transactional(propagation=Propagation.REQUIRED)
    boolean doRefreshSources();

    /**
     * обновляет:
     * 1. enrChosenEntranceExamForm.markSource (для ВВИ-Ф выбирает основание с максимальным баллом)
     * 2. enrChosenEntranceExam.maxMarkForm (для ВВИ выбирает ВВИ-Ф с максимальным баллом)
     * 3. enrChosenEntranceExam.statusMaxMarkComplete (для ВВИ проверяет, что по всем ВВИ-ф есть оценки)
     * @return true, если что-то было изменено
     */
    @DoNotUseMe(reason=Reason.INTERNAL, comment="Внутренний механизм демона")
    @Transactional(propagation=Propagation.REQUIRED)
    boolean doChooseSource();

    /**
     * По выбранным конкурсам, заявлению, их состоянию и состоянию абитуриента актуализирует перечень enrRatingItem и рейтинговый список.
     * @return true, если что-то было изменено
     */
    @DoNotUseMe(reason=Reason.INTERNAL, comment="Внутренний механизм демона")
    @Transactional(propagation=Propagation.REQUIRED)
    boolean doUpdateRatingList();

    /**
     * По шагу зачисления, его элементам и приказам заполняет признаки «excludedBy...»
     * @return true, если что-то было изменено
     */
    @DoNotUseMe(reason=Reason.INTERNAL, comment="Внутренний механизм демона")
    @Transactional(propagation=Propagation.REQUIRED)
    boolean doUpdateExcludedByStatus();


}
