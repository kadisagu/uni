/* $Id$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.AttachedDocsStep;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.context.ContextLocal;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.Pub.EnrEntrantPub;
import ru.tandemservice.unienr14.entrant.daemon.EnrEntrantDaemonBean;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.EnrEntrantRequestManager;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.DocumentAttach.EnrEntrantRequestDocumentAttachUI;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.EnrEntrantRequestAddWizardWizardUI;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.EnrEntrantWizardState;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.IWizardStep;

/**
 * @author nvankov
 * @since 6/5/14
 */
@Input( {
    @Bind(key = EnrEntrantRequestAddWizardWizardUI.PARAM_WIZARD_STATE, binding = "state")
})
public class EnrEntrantRequestAddWizardAttachedDocsStepUI extends EnrEntrantRequestDocumentAttachUI implements IWizardStep
{
    private EnrEntrantWizardState _state;

    public void onClickFinish() {
        saveFormData();

        if (ContextLocal.getErrorCollector().hasErrors()) {
            return;
        }

        EnrEntrantRequestManager.instance().dao().doUpdateEnrReqCompEnrollAvailableAndEduDocForEntrant(getEntrant());
        EnrEntrantDaemonBean.DAEMON.waitForComplete(60);

        EnrEntrantRequestAddWizardWizardUI wizardUI = EnrEntrantRequestAddWizardWizardUI.findWizard(_uiConfig.getBusinessComponent());
        wizardUI.deactivate();

        getActivationBuilder()
            .asDesktopRoot(EnrEntrantPub.class)
            .parameter(PublisherActivator.PUBLISHER_ID_KEY, getState().getEntrantId())
            .parameter("selectedTab", "requestTab")
            .activate();
    }

    @Override
    public void saveStepData()
    {
    }

    public void onClickCancel()
    {
        EnrEntrantRequestAddWizardWizardUI wizardUI = EnrEntrantRequestAddWizardWizardUI.findWizard(_uiConfig.getBusinessComponent());
        wizardUI.deactivate();
    }



    // getters and setters

    public EnrEntrantWizardState getState()
    {
        return _state;
    }

    public void setState(EnrEntrantWizardState state)
    {
        _state = state;
    }

    public void onClickPrevious()
    {
        EnrEntrantRequestAddWizardWizardUI wizardUI = EnrEntrantRequestAddWizardWizardUI.findWizard(_uiConfig.getBusinessComponent());
        wizardUI.onClickPrevious();
    }
}
