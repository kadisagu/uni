package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.ui.CheckTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;

import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.ui.CheckRecTab.EnrEnrollmentStepCheckRecTab;

@Configuration
public class EnrEnrollmentStepCheckTab extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder().create();
    }

    @Bean
    public TabPanelExtPoint enrollmentStepCheckTabPanelExtPoint()
    {
        return tabPanelExtPointBuilder("enrollmentStepCheckTabPanel")
        .addTab(componentTab("checkRecTab", EnrEnrollmentStepCheckRecTab.class))
        // .addTab(componentTab("checkEnrTab", EnrEnrollmentStepCheckEnrTab.class))
        .create();
    }


}
