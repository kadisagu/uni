/* $Id:$ */
package ru.tandemservice.unienr14.request.entity;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.dsl.MetaDSLPath;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.Comparator;

/**
 * Строка с приоритетом. Приоритет уникален в рамках владельца и вида заявления.
 * @author oleyba
 * @since 5/29/13
 */
public interface IEnrEntrantPrioritized extends IEntity
{
    /** Приоритет выбранного конкурса (выбранной ОП в рамках конкурса). */
    int getPriority();

    /** Название строки. */
    String getPriorityRowTitle();

    /** Путь до владельца приоритета. В рамках владельца приоритет уникален. */
    MetaDSLPath getPriorityOwnerPath();

    /** Путь до вида заявления абитуриента. В рамках вида заявления приоритет уникален. */
    MetaDSLPath getRequestTypePath();

    /** Приоритет в рамках выбранного конкурса. Используется {@link IEnrEntrantPrioritized#COMPARATOR при сортировке}. */
    int getCompetitionPriority();

    /** Приоритет в рамках выбранной ОП. Используется {@link IEnrEntrantPrioritized#COMPARATOR при сортировке}. */
    Integer getProgramPriority();

    /** Сортировщик конкурсов по приоритетам. Сперва сравнивает {@link IEnrEntrantPrioritized#getCompetitionPriority() по приоритету выбранного конкурса}, затем {@link IEnrEntrantPrioritized#getProgramPriority() по приоритету выбранной ОП}. */
    public static Comparator<IEnrEntrantPrioritized> COMPARATOR = new Comparator<IEnrEntrantPrioritized>()
    {
        @Override
        public int compare(IEnrEntrantPrioritized o1, IEnrEntrantPrioritized o2)
        {
            int result = UniBaseUtils.compare(o1.getCompetitionPriority(), o2.getCompetitionPriority(), true);
            if (0 == result)
                result = UniBaseUtils.compare(o1.getProgramPriority(), o2.getProgramPriority(), true);
            return result;
        }
    };
}
