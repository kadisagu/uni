package ru.tandemservice.unienr14.ws.entrantrating;

import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

/**
 * @author Vasily Zhukov
 * @since 29.03.2011
 */
public interface IEnrEntrantRatingServiceDao
{
    public static final SpringBeanCache<IEnrEntrantRatingServiceDao> INSTANCE = new SpringBeanCache<IEnrEntrantRatingServiceDao>(IEnrEntrantRatingServiceDao.class.getName());

    EnrEntrantRatingEnvironmentNode getEntrantRatingTAEnvironmentData(EnrEnrollmentCampaign enrollmentCampaign);

    EnrEntrantRatingEnvironmentNode getEntrantRatingTAEnvironmentData(EnrEnrollmentCampaign enrollmentCampaign, EnrCompetition competition);

    EnrEntrantRatingEnvironmentNode getEntrantRatingTAEnvironmentData(String enrollmentCampaignTitle);

    EnrEntrantRatingEnvironmentNode getEntrantRatingTAEnvironmentData(String enrollmentCampaignTitle, String competitionId);
}
