/* $Id:$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.Wizard;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.config.itemList.IItemListExtPointBuilder;
import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.wizard.ISimpleWizardManager;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.InWizardDataEdit.EnrEntrantInWizardDataEdit;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.InWizardDataTabContacts.EnrEntrantInWizardDataTabContacts;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.InWizardDataTabMain.EnrEntrantInWizardDataTabMain;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.InWizardDataTabMilitary.EnrEntrantInWizardDataTabMilitary;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.InWizardDataTabNextOfKin.EnrEntrantInWizardDataTabNextOfKin;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.InWizardDocumentList.EnrEntrantInWizardDocumentList;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.InWizardExamList.EnrEntrantInWizardExamList;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.InWizardRequestList.EnrEntrantInWizardRequestList;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.InWizardSearch.EnrEntrantInWizardSearch;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.AddAppend.EnrEntrantRequestAddAppend;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.DocumentAttach.EnrEntrantRequestDocumentAttach;

@Configuration
public class EnrEntrantRequestWizard extends BusinessComponentManager implements ISimpleWizardManager
{
    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
        .create();
    }

    @Bean
    @Override
    @SuppressWarnings("unchecked")
    public ItemListExtPoint<Class<? extends BusinessComponentManager>> getStepList() {
        return ((IItemListExtPointBuilder)this.itemList(Class.class))
        .add("person-search", EnrEntrantInWizardSearch.class)
        .add("person-request-list", EnrEntrantInWizardRequestList.class)
        .add("entrant-data-edit", EnrEntrantInWizardDataEdit.class)
        .add("entrant-document-list", EnrEntrantInWizardDocumentList.class)
        .add("request-add", EnrEntrantRequestAddAppend.class)
        .add("request-document-attach", EnrEntrantRequestDocumentAttach.class)
        .add("entrant-exam-list", EnrEntrantInWizardExamList.class)
        .add("entrant-datatab-main", EnrEntrantInWizardDataTabMain.class)
        .add("entrant-datatab-contacts", EnrEntrantInWizardDataTabContacts.class)
        .add("entrant-datatab-nextofkin", EnrEntrantInWizardDataTabNextOfKin.class)
        .add("entrant-datatab-military", EnrEntrantInWizardDataTabMilitary.class)
        .create();
    }

}