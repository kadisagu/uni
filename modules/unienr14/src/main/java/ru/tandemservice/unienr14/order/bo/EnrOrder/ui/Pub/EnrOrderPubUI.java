/* $Id:$ */
package ru.tandemservice.unienr14.order.bo.EnrOrder.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.common.base.entity.IPersistentPersonable;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.core.view.formatter.NoWrapFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.MultiValuesColumn;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.sec.entity.Admin;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDefine;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrOrderTypeCodes;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.order.bo.EnrAllocationParagraph.ui.AddEdit.EnrAllocationParagraphAddEdit;
import ru.tandemservice.unienr14.order.bo.EnrAllocationParagraph.ui.AddEdit.EnrAllocationParagraphAddEditUI;
import ru.tandemservice.unienr14.order.bo.EnrCancelParagraph.ui.AddEdit.EnrCancelParagraphAddEdit;
import ru.tandemservice.unienr14.order.bo.EnrCancelParagraph.ui.AddEdit.EnrCancelParagraphAddEditUI;
import ru.tandemservice.unienr14.order.bo.EnrEnrollmentMinisteryParagraph.ui.AddEdit.EnrEnrollmentMinisteryParagraphAddEdit;
import ru.tandemservice.unienr14.order.bo.EnrEnrollmentMinisteryParagraph.ui.AddEdit.EnrEnrollmentMinisteryParagraphAddEditUI;
import ru.tandemservice.unienr14.order.bo.EnrEnrollmentParagraph.ui.AddEdit.EnrEnrollmentParagraphAddEdit;
import ru.tandemservice.unienr14.order.bo.EnrEnrollmentParagraph.ui.AddEdit.EnrEnrollmentParagraphAddEditUI;
import ru.tandemservice.unienr14.order.bo.EnrOrder.EnrOrderManager;
import ru.tandemservice.unienr14.order.bo.EnrOrder.ui.AddEdit.EnrOrderAddEdit;
import ru.tandemservice.unienr14.order.bo.EnrOrder.ui.SetExecutor.EnrOrderSetExecutor;
import ru.tandemservice.unienr14.order.entity.*;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.OrderStates;
import ru.tandemservice.unimv.IAbstractDocument;
import ru.tandemservice.unimv.component.visa.VisaList.IVisaOwnerModel;
import ru.tandemservice.unimv.dao.UnimvDaoFacade;

import java.util.*;

/**
 * @author oleyba
 * @since 7/8/14
 */
@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "order.id"),
        @Bind(key = "selectedTab", binding = "selectedTab")
})
public class EnrOrderPubUI extends UIPresenter implements IVisaOwnerModel
{
    private CommonPostfixPermissionModel _secModel;
    private EnrOrder _order = new EnrOrder();
    private OrderStates _state;
    private String _selectedTab;
    private Long _visingStatus;
    private DynamicListDataSource _dataSource;

    @Override
    public void onComponentRefresh()
    {
        setOrder(IUniBaseDao.instance.get().getNotNull(EnrOrder.class, getOrder().getId()));
        setState(getOrder().getState());
        setSecModel(new CommonPostfixPermissionModel(getSecPostfix()));
        setVisingStatus(UnimvDaoFacade.getVisaDao().getVisingStatus(getOrder()));

        switch (getOrder().getType().getCode()) {
            case (EnrOrderTypeCodes.ENROLLMENT): prepareEnrollmentParagraphDataSource(); break;
            case (EnrOrderTypeCodes.CANCEL): prepareCancelParagraphDataSource(); break;
            case (EnrOrderTypeCodes.ALLOCATION): prepareAllocationParagraphDataSource(); break;
            case (EnrOrderTypeCodes.ENROLLMENT_MIN): prepareEnrollmentMinisteryParagraphDataSource(); break;
            default: throw new IllegalStateException();
        }
    }
    
    // Event Listeners

    public void onClickSendToCoordination()
    {
        _uiSupport.setRefreshScheduled(true);
        IPrincipalContext principalContext = getUserContext().getPrincipalContext();
        if (!(principalContext instanceof IPersistentPersonable)&& !(principalContext instanceof Admin && Debug.isEnabled()))
            throw new ApplicationException(EntityRuntime.getMeta(principalContext.getId()).getTitle() + " не может отправлять документы на согласование.");

        EnrOrderManager.instance().dao().doSendToCoordination(Arrays.asList(getOrder().getId()), principalContext instanceof Admin? null : (IPersistentPersonable) principalContext);
    }

    public void onClickEdit()
    {
        _uiActivation.asRegion(EnrOrderAddEdit.class).parameter("orderId", getOrder().getId()).activate();
    }

    public void onClickSetExecutor()
    {
        _uiActivation.asRegion(EnrOrderSetExecutor.class).parameter("orderId", getOrder().getId()).activate();
    }

    public void onClickAddParagraph()
    {
        switch (getOrder().getType().getCode()) {
            case (EnrOrderTypeCodes.ENROLLMENT):
                _uiActivation.asRegion(EnrEnrollmentParagraphAddEdit.class)
                    .parameter(EnrEnrollmentParagraphAddEditUI.PARAMETER_ORDER_ID, getOrder().getId()).activate();
                return;
            case (EnrOrderTypeCodes.CANCEL):
                _uiActivation.asRegion(EnrCancelParagraphAddEdit.class)
                    .parameter(EnrCancelParagraphAddEditUI.PARAMETER_ORDER_ID, getOrder().getId()).activate();
                return;
            case (EnrOrderTypeCodes.ALLOCATION):
                _uiActivation.asRegion(EnrAllocationParagraphAddEdit.class)
                        .parameter(EnrAllocationParagraphAddEditUI.PARAMETER_ORDER_ID, getOrder().getId()).activate();
                return;
            case (EnrOrderTypeCodes.ENROLLMENT_MIN):
                _uiActivation.asRegion(EnrEnrollmentMinisteryParagraphAddEdit.class)
                        .parameter(EnrEnrollmentMinisteryParagraphAddEditUI.PARAMETER_ORDER_ID, getOrder().getId()).activate();
                return;
            default: throw new IllegalStateException();
        }
    }

    public void onClickEditParagraph()
    {
        switch (getOrder().getType().getCode()) {
            case (EnrOrderTypeCodes.ENROLLMENT):
                _uiActivation.asRegion(EnrEnrollmentParagraphAddEdit.class)
                    .parameter(EnrEnrollmentParagraphAddEditUI.PARAMETER_PARAGRAPH_ID, getListenerParameterAsLong()).activate();
                return;
            case (EnrOrderTypeCodes.CANCEL):
                _uiActivation.asRegion(EnrCancelParagraphAddEdit.class)
                    .parameter(EnrCancelParagraphAddEditUI.PARAMETER_PARAGRAPH_ID, getListenerParameterAsLong()).activate();
                return;
            case (EnrOrderTypeCodes.ALLOCATION):
                _uiActivation.asRegion(EnrAllocationParagraphAddEdit.class)
                        .parameter(EnrCancelParagraphAddEditUI.PARAMETER_PARAGRAPH_ID, getListenerParameterAsLong()).activate();
                return;
            case (EnrOrderTypeCodes.ENROLLMENT_MIN):
                _uiActivation.asRegion(EnrEnrollmentMinisteryParagraphAddEdit.class)
                        .parameter(EnrEnrollmentMinisteryParagraphAddEditUI.PARAMETER_PARAGRAPH_ID, getListenerParameterAsLong()).activate();
                return;
            default: throw new IllegalStateException();
        }
    }

    public void onClickReject()
    {
        _uiSupport.setRefreshScheduled(true);
        EnrOrderManager.instance().dao().doReject(getOrder());
    }

    public void onClickCommit()
    {
        _uiSupport.setRefreshScheduled(true);
        EnrOrderManager.instance().dao().doCommit(Arrays.asList(getOrder().getId()));
    }

    public void onClickSendToFormative()
    {
        _uiSupport.setRefreshScheduled(true);
        EnrOrderManager.instance().dao().doSendToFormative(getOrder().getId());
    }

    public void onClickRollback()
    {
        _uiSupport.setRefreshScheduled(true);
        EnrOrderManager.instance().dao().doRollback(getOrder());
    }

    public void onClickDeleteOrder()
    {
        EnrOrderManager.instance().dao().deleteOrder(getOrder().getId());
        deactivate();
    }

    public void onClickPrintOrder()
    {
        _uiSupport.setRefreshScheduled(true);
        EnrOrderManager.instance().dao().getDownloadPrintForm(getOrder().getId());
    }

    public void onClickDeleteParagraph()
    {
        _uiSupport.setRefreshScheduled(true);
        EnrOrderManager.instance().dao().deleteParagraph(getListenerParameterAsLong());
    }

    public void onClickUp()
    {
        _uiSupport.setRefreshScheduled(true);
        EnrOrderManager.instance().dao().updateParagraphPriority(getListenerParameterAsLong(), -1);
    }

    public void onClickDown()
    {
        _uiSupport.setRefreshScheduled(true);
        EnrOrderManager.instance().dao().updateParagraphPriority(getListenerParameterAsLong(), 1);
    }
    
    // presenter
    

    // IVisaOwnerModel

    public Map<String, Object> getVisaListParameters()
    {
        return new ParametersMap().add("visaOwnerModel", this);
    }

    @Override
    public IAbstractDocument getDocument()
    {
        return _order;
    }

    @Override
    public boolean isVisaListModificable()
    {
        return UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(_order.getState().getCode());
    }

    @Override
    public String getSecPostfix()
    {
        return "enrOrder";
    }

    public boolean isNeedVising()
    {
        return _visingStatus != null;
    }

    // utils

    private void prepareEnrollmentParagraphDataSource()
    {
        DynamicListDataSource dataSource = new DynamicListDataSource(this, component -> {
            List<EnrEnrollmentParagraph> paragraphList = new ArrayList<>();
            Map<EnrEnrollmentParagraph, List<EnrEnrollmentExtract>> map = new HashMap<>();

            for (EnrEnrollmentExtract extract : IUniBaseDao.instance.get().getList(EnrEnrollmentExtract.class, EnrEnrollmentExtract.paragraph().order(), getOrder())) {
                EnrEnrollmentParagraph paragraph = (EnrEnrollmentParagraph) extract.getParagraph();
                List<EnrEnrollmentExtract> extractList = map.get(paragraph);
                if (extractList == null)
                {
                    paragraphList.add(paragraph);
                    map.put(paragraph, extractList = new ArrayList<>());
                }
                extractList.add(extract);
            }

            Collections.sort(paragraphList);

            getDataSource().setCountRow(paragraphList.size());
            UniBaseUtils.createPage(getDataSource(), paragraphList);

            for (ViewWrapper viewWrapper : ViewWrapper.getPatchedList(getDataSource()))
            {
                EnrEnrollmentParagraph paragraph = (EnrEnrollmentParagraph) viewWrapper.getEntity();
                List<EnrEnrollmentExtract> extractList = map.get(paragraph);
                if (null == extractList || extractList.isEmpty()) {
                    viewWrapper.setViewProperty("competition", null);
                    viewWrapper.setViewProperty("extractCount", 0);
                    continue;
                }

                List<EnrCompetition> competitions = new ArrayList<>();
                for (EnrEnrollmentExtract extract : extractList)
                    if (!competitions.contains(extract.getEntity().getCompetition()))
                        competitions.add(extract.getEntity().getCompetition());

                Collections.sort(competitions, ITitled.TITLED_COMPARATOR);
                viewWrapper.setViewProperty("competition", competitions);
                viewWrapper.setViewProperty("extractCount", extractList.size());
            }
        });


        dataSource.addColumn(new PublisherLinkColumn("Название", "title").setFormatter(NoWrapFormatter.INSTANCE).setOrderable(false));
        dataSource.addColumn(new MultiValuesColumn("Конкурс", "title", "competition").setFormatter(CollectionFormatter.COLLECTION_FORMATTER).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Кол-во абитуриентов", "extractCount").setClickable(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Повысить приоритет", CommonBaseDefine.ICO_UP, "onClickUp").setPermissionKey(getSecModel().getPermission("editParagraph")).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Понизить приоритет", CommonBaseDefine.ICO_DOWN, "onClickDown").setPermissionKey(getSecModel().getPermission("editParagraph")).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditParagraph").setPermissionKey(getSecModel().getPermission("editParagraph")).setDisabledProperty(EnrAbstractParagraph.order().readonly().s()));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteParagraph", "Удалить «{0}»?", IAbstractOrder.P_TITLE).setPermissionKey(getSecModel().getPermission("deleteParagraph")).setDisabledProperty(EnrAbstractParagraph.order().readonly().s()));

        setDataSource(dataSource);
    }

    private void prepareCancelParagraphDataSource()
    {
        DynamicListDataSource dataSource = new DynamicListDataSource(this, component -> {
            List<EnrCancelParagraph> paragraphList = new ArrayList<>();
            Map<EnrCancelParagraph, List<EnrCancelExtract>> map = new HashMap<>();

            for (EnrCancelExtract extract : IUniBaseDao.instance.get().getList(EnrCancelExtract.class, EnrCancelExtract.paragraph().order(), getOrder())) {
                EnrCancelParagraph paragraph = (EnrCancelParagraph) extract.getParagraph();
                List<EnrCancelExtract> extractList = map.get(paragraph);
                if (extractList == null)
                {
                    paragraphList.add(paragraph);
                    map.put(paragraph, extractList = new ArrayList<>());
                }
                extractList.add(extract);
            }

            Collections.sort(paragraphList);

            getDataSource().setCountRow(paragraphList.size());
            UniBaseUtils.createPage(getDataSource(), paragraphList);

            for (ViewWrapper viewWrapper : ViewWrapper.getPatchedList(getDataSource()))
            {
                EnrCancelParagraph paragraph = (EnrCancelParagraph) viewWrapper.getEntity();
                List<EnrCancelExtract> extractList = map.get(paragraph);
                if (null == extractList || extractList.isEmpty()) {
                    viewWrapper.setViewProperty("competition", null);
                    viewWrapper.setViewProperty("extractCount", 0);
                    continue;
                }

                List<EnrCompetition> competitions = new ArrayList<>();
                for (EnrCancelExtract extract : extractList)
                    if (!competitions.contains(extract.getCompetition()))
                        competitions.add(extract.getCompetition());

                Collections.sort(competitions, ITitled.TITLED_COMPARATOR);
                viewWrapper.setViewProperty("competition", competitions);
                viewWrapper.setViewProperty("extractCount", extractList.size());
            }
        });


        dataSource.addColumn(new PublisherLinkColumn("Название", "title").setFormatter(NoWrapFormatter.INSTANCE).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Отменяемый приказ", EnrCancelParagraph.cancelledOrder().s() + ".title").setClickable(false).setOrderable(false));
        dataSource.addColumn(new MultiValuesColumn("Конкурс", "title", "competition").setFormatter(CollectionFormatter.COLLECTION_FORMATTER).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Кол-во абитуриентов", "extractCount").setClickable(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Повысить приоритет", CommonBaseDefine.ICO_UP, "onClickUp").setPermissionKey(getSecModel().getPermission("editParagraph")).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Понизить приоритет", CommonBaseDefine.ICO_DOWN, "onClickDown").setPermissionKey(getSecModel().getPermission("editParagraph")).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditParagraph").setPermissionKey(getSecModel().getPermission("editParagraph")).setDisabledProperty(EnrAbstractParagraph.order().readonly().s()));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteParagraph", "Удалить «{0}»?", IAbstractOrder.P_TITLE).setPermissionKey(getSecModel().getPermission("deleteParagraph")).setDisabledProperty(EnrAbstractParagraph.order().readonly().s()));

        setDataSource(dataSource);
    }

    private void prepareAllocationParagraphDataSource()
    {
        DynamicListDataSource dataSource = new DynamicListDataSource(this, component -> {
            List<EnrAllocationParagraph> paragraphList = new ArrayList<>();
            Map<EnrAllocationParagraph, List<EnrAllocationExtract>> map = new HashMap<>();

            for (EnrAllocationExtract extract : IUniBaseDao.instance.get().getList(EnrAllocationExtract.class, EnrAllocationExtract.paragraph().order(), getOrder())) {
                EnrAllocationParagraph paragraph = (EnrAllocationParagraph) extract.getParagraph();
                List<EnrAllocationExtract> extractList = map.get(paragraph);
                if (extractList == null)
                {
                    paragraphList.add(paragraph);
                    map.put(paragraph, extractList = new ArrayList<>());
                }
                extractList.add(extract);
            }

            Collections.sort(paragraphList);

            getDataSource().setCountRow(paragraphList.size());
            UniBaseUtils.createPage(getDataSource(), paragraphList);

            for (ViewWrapper viewWrapper : ViewWrapper.getPatchedList(getDataSource()))
            {
                EnrAllocationParagraph paragraph = (EnrAllocationParagraph) viewWrapper.getEntity();
                List<EnrAllocationExtract> extractList = map.get(paragraph);
                if (null == extractList || extractList.isEmpty()) {
                    viewWrapper.setViewProperty("extractCount", 0);
                    continue;
                }

                List<EnrCompetition> competitions = new ArrayList<>();
                for (EnrAllocationExtract extract : extractList)
                    if (!competitions.contains(extract.getCompetition()))
                        competitions.add(extract.getCompetition());


                Collections.sort(competitions, ITitled.TITLED_COMPARATOR);
                viewWrapper.setViewProperty("extractCount", extractList.size());
                viewWrapper.setViewProperty("filial", paragraph.getEnrProgramSetOrgUnit().getOrgUnit().getInstitutionOrgUnit().getOrgUnit().getShortTitleWithTopEmphasized());
                viewWrapper.setViewProperty("programSetItem", paragraph.getEnrProgramSetItem().getTitle());
            }
        });


        dataSource.addColumn(new PublisherLinkColumn("Название", "title").setFormatter(NoWrapFormatter.INSTANCE).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Филиал", "filial").setFormatter(NoWrapFormatter.INSTANCE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Образовательная программа", "programSetItem").setFormatter(NoWrapFormatter.INSTANCE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Кол-во абитуриентов", "extractCount").setClickable(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Повысить приоритет", CommonBaseDefine.ICO_UP, "onClickUp").setPermissionKey(getSecModel().getPermission("editParagraph")).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Понизить приоритет", CommonBaseDefine.ICO_DOWN, "onClickDown").setPermissionKey(getSecModel().getPermission("editParagraph")).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditParagraph").setPermissionKey(getSecModel().getPermission("editParagraph")).setDisabledProperty(EnrAbstractParagraph.order().readonly().s()));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteParagraph", "Удалить «{0}»?", IAbstractOrder.P_TITLE).setPermissionKey(getSecModel().getPermission("deleteParagraph")).setDisabledProperty(EnrAbstractParagraph.order().readonly().s()));

        setDataSource(dataSource);
    }

    private void prepareEnrollmentMinisteryParagraphDataSource()
    {
        DynamicListDataSource dataSource = new DynamicListDataSource(this, component -> {
            List<EnrEnrollmentMinisteryParagraph> paragraphList = new ArrayList<>();
            Map<EnrEnrollmentMinisteryParagraph, List<EnrEnrollmentMinisteryExtract>> map = new HashMap<>();

            for (EnrEnrollmentMinisteryExtract extract : IUniBaseDao.instance.get().getList(EnrEnrollmentMinisteryExtract.class, EnrEnrollmentMinisteryExtract.paragraph().order(), getOrder())) {
                EnrEnrollmentMinisteryParagraph paragraph = (EnrEnrollmentMinisteryParagraph) extract.getParagraph();
                List<EnrEnrollmentMinisteryExtract> extractList = map.get(paragraph);
                if (extractList == null)
                {
                    paragraphList.add(paragraph);
                    map.put(paragraph, extractList = new ArrayList<>());
                }
                extractList.add(extract);
            }

            Collections.sort(paragraphList);

            getDataSource().setCountRow(paragraphList.size());
            UniBaseUtils.createPage(getDataSource(), paragraphList);

            for (ViewWrapper viewWrapper : ViewWrapper.getPatchedList(getDataSource()))
            {
                EnrEnrollmentMinisteryParagraph paragraph = (EnrEnrollmentMinisteryParagraph) viewWrapper.getEntity();
                List<EnrEnrollmentMinisteryExtract> extractList = map.get(paragraph);
                if (null == extractList || extractList.isEmpty()) {
                    viewWrapper.setViewProperty("competition", null);
                    viewWrapper.setViewProperty("extractCount", 0);
                    continue;
                }

                List<EnrCompetition> competitions = new ArrayList<>();
                for (EnrEnrollmentMinisteryExtract extract : extractList)
                    if (!competitions.contains(extract.getEntity().getCompetition()))
                        competitions.add(extract.getEntity().getCompetition());

                Collections.sort(competitions, ITitled.TITLED_COMPARATOR);
                viewWrapper.setViewProperty("competition", competitions);
                viewWrapper.setViewProperty("extractCount", extractList.size());
            }
        });


        dataSource.addColumn(new PublisherLinkColumn("Название", "title").setFormatter(NoWrapFormatter.INSTANCE).setOrderable(false));
        dataSource.addColumn(new MultiValuesColumn("Конкурс", "title", "competition").setFormatter(CollectionFormatter.COLLECTION_FORMATTER).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Кол-во абитуриентов", "extractCount").setClickable(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Повысить приоритет", CommonBaseDefine.ICO_UP, "onClickUp").setPermissionKey(getSecModel().getPermission("editParagraph")).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Понизить приоритет", CommonBaseDefine.ICO_DOWN, "onClickDown").setPermissionKey(getSecModel().getPermission("editParagraph")).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditParagraph").setPermissionKey(getSecModel().getPermission("editParagraph")).setDisabledProperty(EnrAbstractParagraph.order().readonly().s()));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteParagraph", "Удалить «{0}»?", IAbstractOrder.P_TITLE).setPermissionKey(getSecModel().getPermission("deleteParagraph")).setDisabledProperty(EnrAbstractParagraph.order().readonly().s()));

        setDataSource(dataSource);
    }

    // getters and setters

    public DynamicListDataSource getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource dataSource)
    {
        _dataSource = dataSource;
    }

    public EnrOrder getOrder()
    {
        return _order;
    }

    public void setOrder(EnrOrder order)
    {
        _order = order;
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        _secModel = secModel;
    }

    public String getSelectedTab()
    {
        return _selectedTab;
    }

    public void setSelectedTab(String selectedTab)
    {
        _selectedTab = selectedTab;
    }

    public OrderStates getState()
    {
        return _state;
    }

    public void setState(OrderStates state)
    {
        _state = state;
    }

    public Long getVisingStatus()
    {
        return _visingStatus;
    }

    public void setVisingStatus(Long visingStatus)
    {
        _visingStatus = visingStatus;
    }
}