package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x8x2_5to6 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        if (tool.columnExists("enr14_campaign_settings_t", "examintervalauto_p")) {
            return;
        }

        tool.renameColumn("enr14_campaign_settings_t", "examinterval_p", "examintervalauto_p");

		// создано обязательное свойство examIntervalManual
		{
			// создать колонку
			tool.createColumn("enr14_campaign_settings_t", new DBColumn("examintervalmanual_p", DBType.INTEGER));

			// задать значение по умолчанию
            tool.executeUpdate("update enr14_campaign_settings_t set examintervalmanual_p=? where examintervalmanual_p is null", 1);
            tool.executeUpdate("update enr14_campaign_settings_t set examintervalmanual_p=examintervalauto_p where chckexmintrvlonmnlslctn_p=?", true);

			// сделать колонку NOT NULL
			tool.setColumnNullable("enr14_campaign_settings_t", "examintervalmanual_p", false);

            tool.dropColumn("enr14_campaign_settings_t", "chckexmintrvlonmnlslctn_p");
		}
    }
}