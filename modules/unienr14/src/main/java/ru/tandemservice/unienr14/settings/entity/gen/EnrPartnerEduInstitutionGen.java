package ru.tandemservice.unienr14.settings.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.person.catalog.entity.EduInstitution;
import ru.tandemservice.unienr14.settings.entity.EnrPartnerEduInstitution;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Партнерские образовательные учреждения
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrPartnerEduInstitutionGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.settings.entity.EnrPartnerEduInstitution";
    public static final String ENTITY_NAME = "enrPartnerEduInstitution";
    public static final int VERSION_HASH = 1957091687;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDU_INSTITUTION = "eduInstitution";

    private EduInstitution _eduInstitution;     // Образовательное учреждение

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Образовательное учреждение. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EduInstitution getEduInstitution()
    {
        return _eduInstitution;
    }

    /**
     * @param eduInstitution Образовательное учреждение. Свойство не может быть null и должно быть уникальным.
     */
    public void setEduInstitution(EduInstitution eduInstitution)
    {
        dirty(_eduInstitution, eduInstitution);
        _eduInstitution = eduInstitution;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrPartnerEduInstitutionGen)
        {
            setEduInstitution(((EnrPartnerEduInstitution)another).getEduInstitution());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrPartnerEduInstitutionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrPartnerEduInstitution.class;
        }

        public T newInstance()
        {
            return (T) new EnrPartnerEduInstitution();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "eduInstitution":
                    return obj.getEduInstitution();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "eduInstitution":
                    obj.setEduInstitution((EduInstitution) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "eduInstitution":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "eduInstitution":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "eduInstitution":
                    return EduInstitution.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrPartnerEduInstitution> _dslPath = new Path<EnrPartnerEduInstitution>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrPartnerEduInstitution");
    }
            

    /**
     * @return Образовательное учреждение. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.settings.entity.EnrPartnerEduInstitution#getEduInstitution()
     */
    public static EduInstitution.Path<EduInstitution> eduInstitution()
    {
        return _dslPath.eduInstitution();
    }

    public static class Path<E extends EnrPartnerEduInstitution> extends EntityPath<E>
    {
        private EduInstitution.Path<EduInstitution> _eduInstitution;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Образовательное учреждение. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.settings.entity.EnrPartnerEduInstitution#getEduInstitution()
     */
        public EduInstitution.Path<EduInstitution> eduInstitution()
        {
            if(_eduInstitution == null )
                _eduInstitution = new EduInstitution.Path<EduInstitution>(L_EDU_INSTITUTION, this);
            return _eduInstitution;
        }

        public Class getEntityClass()
        {
            return EnrPartnerEduInstitution.class;
        }

        public String getEntityName()
        {
            return "enrPartnerEduInstitution";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
