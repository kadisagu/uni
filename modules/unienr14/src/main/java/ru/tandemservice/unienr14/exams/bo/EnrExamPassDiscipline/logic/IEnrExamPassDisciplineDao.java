/* $Id$ */
package ru.tandemservice.unienr14.exams.bo.EnrExamPassDiscipline.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.caf.ui.support.IUISettings;
import ru.tandemservice.unienr14.catalog.entity.EnrAbsenceNote;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassAppeal;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @author nvankov
 * @since 4/15/15
 */
public interface IEnrExamPassDisciplineDao extends INeedPersistenceSupport
{
    /**
     * Обновляет перечень ДДС для абитуриента на основании ВВИ:
     * добавляет недостающие и удаляет лишние.
     * Не удаляет ДДС, если в них есть баллы
     *
     * @param entrant абитуриент
     */
    void doRefreshExamList(EnrEntrant entrant);

    public interface IExamPassDisciplineMarkData {
        EnrExamPassDiscipline getDiscipline();
        EnrAbsenceNote getAbsenceNote();
        Long getMarkAsLong();
        Date getMarkDate();
    }

    void updateExamPassDisciplineData(EnrEnrollmentCampaign enrollmentCampaign, List<? extends IExamPassDisciplineMarkData> wrappers);

    void updateExamPassDiscipline(EnrExamPassDiscipline discipline);

    /**
     * Возвращает актуальные дисциплины для сдачи - те, для которых есть ВВИ.
     * Актуальность самих ВВИ имеет смысл проверять отдельно через признак в рейтинге:
     * @param entrant абитуриент
     * @return id актуальных дисциплин для сдачи
     */
    Set<Long> getActualExamPassDisciplineIds(EnrEntrant entrant);

    void doCreateRetake(EnrExamPassDiscipline exam);

    void saveOrUpdateAppeal(EnrExamPassAppeal appeal);

    void deleteExamPassDiscipline(Long disciplineId);
}
