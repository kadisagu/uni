package ru.tandemservice.unienr14.entrant.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.catalog.entity.EnrAccessCourse;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantAccessCourse;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Подготовительный курс абитуриента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrEntrantAccessCourseGen extends EntityBase
 implements INaturalIdentifiable<EnrEntrantAccessCourseGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.entrant.entity.EnrEntrantAccessCourse";
    public static final String ENTITY_NAME = "enrEntrantAccessCourse";
    public static final int VERSION_HASH = -1652536628;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTRANT = "entrant";
    public static final String L_COURSE = "course";

    private EnrEntrant _entrant;     // Абитуриент
    private EnrAccessCourse _course;     // Курс

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     */
    @NotNull
    public EnrEntrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant Абитуриент. Свойство не может быть null.
     */
    public void setEntrant(EnrEntrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public EnrAccessCourse getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(EnrAccessCourse course)
    {
        dirty(_course, course);
        _course = course;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrEntrantAccessCourseGen)
        {
            if (withNaturalIdProperties)
            {
                setEntrant(((EnrEntrantAccessCourse)another).getEntrant());
                setCourse(((EnrEntrantAccessCourse)another).getCourse());
            }
        }
    }

    public INaturalId<EnrEntrantAccessCourseGen> getNaturalId()
    {
        return new NaturalId(getEntrant(), getCourse());
    }

    public static class NaturalId extends NaturalIdBase<EnrEntrantAccessCourseGen>
    {
        private static final String PROXY_NAME = "EnrEntrantAccessCourseNaturalProxy";

        private Long _entrant;
        private Long _course;

        public NaturalId()
        {}

        public NaturalId(EnrEntrant entrant, EnrAccessCourse course)
        {
            _entrant = ((IEntity) entrant).getId();
            _course = ((IEntity) course).getId();
        }

        public Long getEntrant()
        {
            return _entrant;
        }

        public void setEntrant(Long entrant)
        {
            _entrant = entrant;
        }

        public Long getCourse()
        {
            return _course;
        }

        public void setCourse(Long course)
        {
            _course = course;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrEntrantAccessCourseGen.NaturalId) ) return false;

            EnrEntrantAccessCourseGen.NaturalId that = (NaturalId) o;

            if( !equals(getEntrant(), that.getEntrant()) ) return false;
            if( !equals(getCourse(), that.getCourse()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEntrant());
            result = hashCode(result, getCourse());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEntrant());
            sb.append("/");
            sb.append(getCourse());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrEntrantAccessCourseGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrEntrantAccessCourse.class;
        }

        public T newInstance()
        {
            return (T) new EnrEntrantAccessCourse();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entrant":
                    return obj.getEntrant();
                case "course":
                    return obj.getCourse();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entrant":
                    obj.setEntrant((EnrEntrant) value);
                    return;
                case "course":
                    obj.setCourse((EnrAccessCourse) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entrant":
                        return true;
                case "course":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entrant":
                    return true;
                case "course":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entrant":
                    return EnrEntrant.class;
                case "course":
                    return EnrAccessCourse.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrEntrantAccessCourse> _dslPath = new Path<EnrEntrantAccessCourse>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrEntrantAccessCourse");
    }
            

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantAccessCourse#getEntrant()
     */
    public static EnrEntrant.Path<EnrEntrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantAccessCourse#getCourse()
     */
    public static EnrAccessCourse.Path<EnrAccessCourse> course()
    {
        return _dslPath.course();
    }

    public static class Path<E extends EnrEntrantAccessCourse> extends EntityPath<E>
    {
        private EnrEntrant.Path<EnrEntrant> _entrant;
        private EnrAccessCourse.Path<EnrAccessCourse> _course;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantAccessCourse#getEntrant()
     */
        public EnrEntrant.Path<EnrEntrant> entrant()
        {
            if(_entrant == null )
                _entrant = new EnrEntrant.Path<EnrEntrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantAccessCourse#getCourse()
     */
        public EnrAccessCourse.Path<EnrAccessCourse> course()
        {
            if(_course == null )
                _course = new EnrAccessCourse.Path<EnrAccessCourse>(L_COURSE, this);
            return _course;
        }

        public Class getEntityClass()
        {
            return EnrEntrantAccessCourse.class;
        }

        public String getEntityName()
        {
            return "enrEntrantAccessCourse";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
