package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic;

import org.tandemframework.core.runtime.IRuntimeExtension;

import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.EnrEnrollmentStepManager;

/**
 * @author vdanilov
 */
public class EnrEnrollmentStepRuntimeExtension implements IRuntimeExtension {

    @Override
    public void init(Object object) {
        EnrEnrollmentStepManager.instance().dao().doRefreshNecessaryXml();
    }

    @Override
    public void destroy() {}

}
