/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.PublisherActivator;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.EnrEnrollmentModelManager;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.ui.Add.EnrEnrollmentModelAdd;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

/**
 * @author oleyba
 * @since 3/30/15
 */
public class EnrEnrollmentModelListUI extends UIPresenter
{
    private EnrEnrollmentCampaign _enrollmentCampaign;

    @Override
    public void onComponentRefresh() {
        setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, getEnrollmentCampaign());
    }

    public void onClickAdd() {
        getEnrollmentCampaign().checkOpen();
        _uiActivation.asRegionDialog(EnrEnrollmentModelAdd.class)
            .parameter(PublisherActivator.PUBLISHER_ID_KEY, getEnrollmentCampaign().getId())
            .activate();
    }

    public void onClickDelete() {
        EnrEnrollmentModelManager.instance().dao().doDelete(getListenerParameterAsLong());
    }

    public void onEnrCampaignRefresh()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
    }

    public boolean isNothingSelected() {
        return getEnrollmentCampaign() == null;
    }

    // getters and setters


    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }
}