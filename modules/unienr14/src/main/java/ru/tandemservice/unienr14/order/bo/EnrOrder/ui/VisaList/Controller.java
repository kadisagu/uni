/* $Id: Controller.java 22487 2012-04-04 13:16:00Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unienr14.order.bo.EnrOrder.ui.VisaList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.unienr14.order.bo.EnrOrder.ui.VisaGroupAdd.EnrOrderVisaGroupAdd;

/**
 * @author vip_delete
 * @since 28.07.2009
 */
public class Controller extends ru.tandemservice.unimv.component.visa.VisaList.Controller
{
    public void onClickAddVisaGroup(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(EnrOrderVisaGroupAdd.class.getSimpleName(), new ParametersMap()
                .add("documentId", getModel(component).getVisaOwnerModel().getDocument().getId())
        ));
    }
}
