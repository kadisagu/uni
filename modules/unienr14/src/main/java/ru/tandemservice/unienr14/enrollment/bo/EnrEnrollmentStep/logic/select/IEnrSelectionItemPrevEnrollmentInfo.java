/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.select;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentConflictSolution;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;

/**
 * @author oleyba
 * @since 4/2/15
 */
public interface IEnrSelectionItemPrevEnrollmentInfo
{
    EnrRequestedCompetition getRequestedCompetition();

    boolean isCancelled();

    String getInfo();

    void apply(EnrSelectionItem item);

    EnrEnrollmentConflictSolution getSolution();
}
