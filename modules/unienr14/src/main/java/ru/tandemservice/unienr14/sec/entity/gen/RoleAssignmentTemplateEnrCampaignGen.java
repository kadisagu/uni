package ru.tandemservice.unienr14.sec.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.sec.entity.gen.IPrincipalContextGen;
import org.tandemframework.shared.organization.sec.entity.IRoleAssignment;
import org.tandemframework.shared.organization.sec.entity.RoleConfigTemplate;
import ru.tandemservice.unienr14.sec.entity.RoleAssignmentTemplateEnrCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Назначение шаблонной роли для ПК принципалу
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RoleAssignmentTemplateEnrCampaignGen extends EntityBase
 implements IRoleAssignment, INaturalIdentifiable<RoleAssignmentTemplateEnrCampaignGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.sec.entity.RoleAssignmentTemplateEnrCampaign";
    public static final String ENTITY_NAME = "roleAssignmentTemplateEnrCampaign";
    public static final int VERSION_HASH = 1912548990;
    private static IEntityMeta ENTITY_META;

    public static final String L_ROLE_CONFIG = "roleConfig";
    public static final String L_PRINCIPAL_CONTEXT = "principalContext";
    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";

    private RoleConfigTemplate _roleConfig;     // Конфигурация шаблонной роли
    private IPrincipalContext _principalContext;     // Контекст принципала
    private EnrEnrollmentCampaign _enrollmentCampaign;     // Приемная кампания

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Конфигурация шаблонной роли. Свойство не может быть null.
     */
    @NotNull
    public RoleConfigTemplate getRoleConfig()
    {
        return _roleConfig;
    }

    /**
     * @param roleConfig Конфигурация шаблонной роли. Свойство не может быть null.
     */
    public void setRoleConfig(RoleConfigTemplate roleConfig)
    {
        dirty(_roleConfig, roleConfig);
        _roleConfig = roleConfig;
    }

    /**
     * @return Контекст принципала. Свойство не может быть null.
     */
    @NotNull
    public IPrincipalContext getPrincipalContext()
    {
        return _principalContext;
    }

    /**
     * @param principalContext Контекст принципала. Свойство не может быть null.
     */
    public void setPrincipalContext(IPrincipalContext principalContext)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && principalContext!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IPrincipalContext.class);
            IEntityMeta actual =  principalContext instanceof IEntity ? EntityRuntime.getMeta((IEntity) principalContext) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_principalContext, principalContext);
        _principalContext = principalContext;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof RoleAssignmentTemplateEnrCampaignGen)
        {
            if (withNaturalIdProperties)
            {
                setRoleConfig(((RoleAssignmentTemplateEnrCampaign)another).getRoleConfig());
                setPrincipalContext(((RoleAssignmentTemplateEnrCampaign)another).getPrincipalContext());
                setEnrollmentCampaign(((RoleAssignmentTemplateEnrCampaign)another).getEnrollmentCampaign());
            }
        }
    }

    public INaturalId<RoleAssignmentTemplateEnrCampaignGen> getNaturalId()
    {
        return new NaturalId(getRoleConfig(), getPrincipalContext(), getEnrollmentCampaign());
    }

    public static class NaturalId extends NaturalIdBase<RoleAssignmentTemplateEnrCampaignGen>
    {
        private static final String PROXY_NAME = "RoleAssignmentTemplateEnrCampaignNaturalProxy";

        private Long _roleConfig;
        private Long _principalContext;
        private Long _enrollmentCampaign;

        public NaturalId()
        {}

        public NaturalId(RoleConfigTemplate roleConfig, IPrincipalContext principalContext, EnrEnrollmentCampaign enrollmentCampaign)
        {
            _roleConfig = ((IEntity) roleConfig).getId();
            _principalContext = ((IEntity) principalContext).getId();
            _enrollmentCampaign = ((IEntity) enrollmentCampaign).getId();
        }

        public Long getRoleConfig()
        {
            return _roleConfig;
        }

        public void setRoleConfig(Long roleConfig)
        {
            _roleConfig = roleConfig;
        }

        public Long getPrincipalContext()
        {
            return _principalContext;
        }

        public void setPrincipalContext(Long principalContext)
        {
            _principalContext = principalContext;
        }

        public Long getEnrollmentCampaign()
        {
            return _enrollmentCampaign;
        }

        public void setEnrollmentCampaign(Long enrollmentCampaign)
        {
            _enrollmentCampaign = enrollmentCampaign;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof RoleAssignmentTemplateEnrCampaignGen.NaturalId) ) return false;

            RoleAssignmentTemplateEnrCampaignGen.NaturalId that = (NaturalId) o;

            if( !equals(getRoleConfig(), that.getRoleConfig()) ) return false;
            if( !equals(getPrincipalContext(), that.getPrincipalContext()) ) return false;
            if( !equals(getEnrollmentCampaign(), that.getEnrollmentCampaign()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getRoleConfig());
            result = hashCode(result, getPrincipalContext());
            result = hashCode(result, getEnrollmentCampaign());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getRoleConfig());
            sb.append("/");
            sb.append(getPrincipalContext());
            sb.append("/");
            sb.append(getEnrollmentCampaign());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RoleAssignmentTemplateEnrCampaignGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RoleAssignmentTemplateEnrCampaign.class;
        }

        public T newInstance()
        {
            return (T) new RoleAssignmentTemplateEnrCampaign();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "roleConfig":
                    return obj.getRoleConfig();
                case "principalContext":
                    return obj.getPrincipalContext();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "roleConfig":
                    obj.setRoleConfig((RoleConfigTemplate) value);
                    return;
                case "principalContext":
                    obj.setPrincipalContext((IPrincipalContext) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrEnrollmentCampaign) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "roleConfig":
                        return true;
                case "principalContext":
                        return true;
                case "enrollmentCampaign":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "roleConfig":
                    return true;
                case "principalContext":
                    return true;
                case "enrollmentCampaign":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "roleConfig":
                    return RoleConfigTemplate.class;
                case "principalContext":
                    return IPrincipalContext.class;
                case "enrollmentCampaign":
                    return EnrEnrollmentCampaign.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RoleAssignmentTemplateEnrCampaign> _dslPath = new Path<RoleAssignmentTemplateEnrCampaign>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RoleAssignmentTemplateEnrCampaign");
    }
            

    /**
     * @return Конфигурация шаблонной роли. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.sec.entity.RoleAssignmentTemplateEnrCampaign#getRoleConfig()
     */
    public static RoleConfigTemplate.Path<RoleConfigTemplate> roleConfig()
    {
        return _dslPath.roleConfig();
    }

    /**
     * @return Контекст принципала. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.sec.entity.RoleAssignmentTemplateEnrCampaign#getPrincipalContext()
     */
    public static IPrincipalContextGen.Path<IPrincipalContext> principalContext()
    {
        return _dslPath.principalContext();
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.sec.entity.RoleAssignmentTemplateEnrCampaign#getEnrollmentCampaign()
     */
    public static EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    public static class Path<E extends RoleAssignmentTemplateEnrCampaign> extends EntityPath<E>
    {
        private RoleConfigTemplate.Path<RoleConfigTemplate> _roleConfig;
        private IPrincipalContextGen.Path<IPrincipalContext> _principalContext;
        private EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> _enrollmentCampaign;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Конфигурация шаблонной роли. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.sec.entity.RoleAssignmentTemplateEnrCampaign#getRoleConfig()
     */
        public RoleConfigTemplate.Path<RoleConfigTemplate> roleConfig()
        {
            if(_roleConfig == null )
                _roleConfig = new RoleConfigTemplate.Path<RoleConfigTemplate>(L_ROLE_CONFIG, this);
            return _roleConfig;
        }

    /**
     * @return Контекст принципала. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.sec.entity.RoleAssignmentTemplateEnrCampaign#getPrincipalContext()
     */
        public IPrincipalContextGen.Path<IPrincipalContext> principalContext()
        {
            if(_principalContext == null )
                _principalContext = new IPrincipalContextGen.Path<IPrincipalContext>(L_PRINCIPAL_CONTEXT, this);
            return _principalContext;
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.sec.entity.RoleAssignmentTemplateEnrCampaign#getEnrollmentCampaign()
     */
        public EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

        public Class getEntityClass()
        {
            return RoleAssignmentTemplateEnrCampaign.class;
        }

        public String getEntityName()
        {
            return "roleAssignmentTemplateEnrCampaign";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
