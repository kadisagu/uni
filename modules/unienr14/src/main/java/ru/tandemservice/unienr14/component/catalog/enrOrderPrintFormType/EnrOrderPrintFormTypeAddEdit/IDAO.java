/* $Id$ */
package ru.tandemservice.unienr14.component.catalog.enrOrderPrintFormType.EnrOrderPrintFormTypeAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultScriptCatalogAddEdit.IDefaultScriptCatalogAddEditDAO;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderPrintFormType;

/**
 * @author azhebko
 * @since 14.07.2014
 */
public interface IDAO extends IDefaultScriptCatalogAddEditDAO<EnrOrderPrintFormType, Model>
{
}