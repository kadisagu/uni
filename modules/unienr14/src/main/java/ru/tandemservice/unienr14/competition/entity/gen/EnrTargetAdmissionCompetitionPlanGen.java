package ru.tandemservice.unienr14.competition.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrTargetAdmissionCompetitionPlan;
import ru.tandemservice.unienr14.competition.entity.EnrTargetAdmissionPlan;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Число мест по виду ЦП
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrTargetAdmissionCompetitionPlanGen extends EntityBase
 implements INaturalIdentifiable<EnrTargetAdmissionCompetitionPlanGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.competition.entity.EnrTargetAdmissionCompetitionPlan";
    public static final String ENTITY_NAME = "enrTargetAdmissionCompetitionPlan";
    public static final int VERSION_HASH = 727493867;
    private static IEntityMeta ENTITY_META;

    public static final String L_TARGET_ADMISSION_PLAN = "targetAdmissionPlan";
    public static final String L_COMPETITION = "competition";
    public static final String P_PLAN = "plan";
    public static final String P_CALCULATED_PLAN = "calculatedPlan";

    private EnrTargetAdmissionPlan _targetAdmissionPlan;     // План приема по виду ЦП
    private EnrCompetition _competition;     // Конкурс
    private int _plan;     // Число мест
    private int _calculatedPlan;     // Число мест (вычисляемое)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return План приема по виду ЦП. Свойство не может быть null.
     */
    @NotNull
    public EnrTargetAdmissionPlan getTargetAdmissionPlan()
    {
        return _targetAdmissionPlan;
    }

    /**
     * @param targetAdmissionPlan План приема по виду ЦП. Свойство не может быть null.
     */
    public void setTargetAdmissionPlan(EnrTargetAdmissionPlan targetAdmissionPlan)
    {
        dirty(_targetAdmissionPlan, targetAdmissionPlan);
        _targetAdmissionPlan = targetAdmissionPlan;
    }

    /**
     * @return Конкурс. Свойство не может быть null.
     */
    @NotNull
    public EnrCompetition getCompetition()
    {
        return _competition;
    }

    /**
     * @param competition Конкурс. Свойство не может быть null.
     */
    public void setCompetition(EnrCompetition competition)
    {
        dirty(_competition, competition);
        _competition = competition;
    }

    /**
     * @return Число мест. Свойство не может быть null.
     */
    @NotNull
    public int getPlan()
    {
        return _plan;
    }

    /**
     * @param plan Число мест. Свойство не может быть null.
     */
    public void setPlan(int plan)
    {
        dirty(_plan, plan);
        _plan = plan;
    }

    /**
     * @return Число мест (вычисляемое). Свойство не может быть null.
     */
    @NotNull
    public int getCalculatedPlan()
    {
        return _calculatedPlan;
    }

    /**
     * @param calculatedPlan Число мест (вычисляемое). Свойство не может быть null.
     */
    public void setCalculatedPlan(int calculatedPlan)
    {
        dirty(_calculatedPlan, calculatedPlan);
        _calculatedPlan = calculatedPlan;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrTargetAdmissionCompetitionPlanGen)
        {
            if (withNaturalIdProperties)
            {
                setTargetAdmissionPlan(((EnrTargetAdmissionCompetitionPlan)another).getTargetAdmissionPlan());
                setCompetition(((EnrTargetAdmissionCompetitionPlan)another).getCompetition());
            }
            setPlan(((EnrTargetAdmissionCompetitionPlan)another).getPlan());
            setCalculatedPlan(((EnrTargetAdmissionCompetitionPlan)another).getCalculatedPlan());
        }
    }

    public INaturalId<EnrTargetAdmissionCompetitionPlanGen> getNaturalId()
    {
        return new NaturalId(getTargetAdmissionPlan(), getCompetition());
    }

    public static class NaturalId extends NaturalIdBase<EnrTargetAdmissionCompetitionPlanGen>
    {
        private static final String PROXY_NAME = "EnrTargetAdmissionCompetitionPlanNaturalProxy";

        private Long _targetAdmissionPlan;
        private Long _competition;

        public NaturalId()
        {}

        public NaturalId(EnrTargetAdmissionPlan targetAdmissionPlan, EnrCompetition competition)
        {
            _targetAdmissionPlan = ((IEntity) targetAdmissionPlan).getId();
            _competition = ((IEntity) competition).getId();
        }

        public Long getTargetAdmissionPlan()
        {
            return _targetAdmissionPlan;
        }

        public void setTargetAdmissionPlan(Long targetAdmissionPlan)
        {
            _targetAdmissionPlan = targetAdmissionPlan;
        }

        public Long getCompetition()
        {
            return _competition;
        }

        public void setCompetition(Long competition)
        {
            _competition = competition;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrTargetAdmissionCompetitionPlanGen.NaturalId) ) return false;

            EnrTargetAdmissionCompetitionPlanGen.NaturalId that = (NaturalId) o;

            if( !equals(getTargetAdmissionPlan(), that.getTargetAdmissionPlan()) ) return false;
            if( !equals(getCompetition(), that.getCompetition()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getTargetAdmissionPlan());
            result = hashCode(result, getCompetition());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getTargetAdmissionPlan());
            sb.append("/");
            sb.append(getCompetition());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrTargetAdmissionCompetitionPlanGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrTargetAdmissionCompetitionPlan.class;
        }

        public T newInstance()
        {
            return (T) new EnrTargetAdmissionCompetitionPlan();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "targetAdmissionPlan":
                    return obj.getTargetAdmissionPlan();
                case "competition":
                    return obj.getCompetition();
                case "plan":
                    return obj.getPlan();
                case "calculatedPlan":
                    return obj.getCalculatedPlan();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "targetAdmissionPlan":
                    obj.setTargetAdmissionPlan((EnrTargetAdmissionPlan) value);
                    return;
                case "competition":
                    obj.setCompetition((EnrCompetition) value);
                    return;
                case "plan":
                    obj.setPlan((Integer) value);
                    return;
                case "calculatedPlan":
                    obj.setCalculatedPlan((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "targetAdmissionPlan":
                        return true;
                case "competition":
                        return true;
                case "plan":
                        return true;
                case "calculatedPlan":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "targetAdmissionPlan":
                    return true;
                case "competition":
                    return true;
                case "plan":
                    return true;
                case "calculatedPlan":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "targetAdmissionPlan":
                    return EnrTargetAdmissionPlan.class;
                case "competition":
                    return EnrCompetition.class;
                case "plan":
                    return Integer.class;
                case "calculatedPlan":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrTargetAdmissionCompetitionPlan> _dslPath = new Path<EnrTargetAdmissionCompetitionPlan>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrTargetAdmissionCompetitionPlan");
    }
            

    /**
     * @return План приема по виду ЦП. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrTargetAdmissionCompetitionPlan#getTargetAdmissionPlan()
     */
    public static EnrTargetAdmissionPlan.Path<EnrTargetAdmissionPlan> targetAdmissionPlan()
    {
        return _dslPath.targetAdmissionPlan();
    }

    /**
     * @return Конкурс. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrTargetAdmissionCompetitionPlan#getCompetition()
     */
    public static EnrCompetition.Path<EnrCompetition> competition()
    {
        return _dslPath.competition();
    }

    /**
     * @return Число мест. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrTargetAdmissionCompetitionPlan#getPlan()
     */
    public static PropertyPath<Integer> plan()
    {
        return _dslPath.plan();
    }

    /**
     * @return Число мест (вычисляемое). Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrTargetAdmissionCompetitionPlan#getCalculatedPlan()
     */
    public static PropertyPath<Integer> calculatedPlan()
    {
        return _dslPath.calculatedPlan();
    }

    public static class Path<E extends EnrTargetAdmissionCompetitionPlan> extends EntityPath<E>
    {
        private EnrTargetAdmissionPlan.Path<EnrTargetAdmissionPlan> _targetAdmissionPlan;
        private EnrCompetition.Path<EnrCompetition> _competition;
        private PropertyPath<Integer> _plan;
        private PropertyPath<Integer> _calculatedPlan;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return План приема по виду ЦП. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrTargetAdmissionCompetitionPlan#getTargetAdmissionPlan()
     */
        public EnrTargetAdmissionPlan.Path<EnrTargetAdmissionPlan> targetAdmissionPlan()
        {
            if(_targetAdmissionPlan == null )
                _targetAdmissionPlan = new EnrTargetAdmissionPlan.Path<EnrTargetAdmissionPlan>(L_TARGET_ADMISSION_PLAN, this);
            return _targetAdmissionPlan;
        }

    /**
     * @return Конкурс. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrTargetAdmissionCompetitionPlan#getCompetition()
     */
        public EnrCompetition.Path<EnrCompetition> competition()
        {
            if(_competition == null )
                _competition = new EnrCompetition.Path<EnrCompetition>(L_COMPETITION, this);
            return _competition;
        }

    /**
     * @return Число мест. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrTargetAdmissionCompetitionPlan#getPlan()
     */
        public PropertyPath<Integer> plan()
        {
            if(_plan == null )
                _plan = new PropertyPath<Integer>(EnrTargetAdmissionCompetitionPlanGen.P_PLAN, this);
            return _plan;
        }

    /**
     * @return Число мест (вычисляемое). Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrTargetAdmissionCompetitionPlan#getCalculatedPlan()
     */
        public PropertyPath<Integer> calculatedPlan()
        {
            if(_calculatedPlan == null )
                _calculatedPlan = new PropertyPath<Integer>(EnrTargetAdmissionCompetitionPlanGen.P_CALCULATED_PLAN, this);
            return _calculatedPlan;
        }

        public Class getEntityClass()
        {
            return EnrTargetAdmissionCompetitionPlan.class;
        }

        public String getEntityName()
        {
            return "enrTargetAdmissionCompetitionPlan";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
