/**
 *$Id: IEnrExamGroupSetDao.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.exams.bo.EnrExamGroup.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.settings.entity.EnrExamGroupSet;

/**
 * @author Alexander Shaburov
 * @since 17.05.13
 */
public interface IEnrExamGroupSetDao extends INeedPersistenceSupport
{
    /**
     * Сохраняет или обновляет набор экзам. групп.
     * Новый набор добавляется открытым.
     * Проверки:
     * -- Нет ли открытого набора;
     * -- Пересечение по датам с другими наборами в рамках ПК;
     * @param examGroupSet набор экзам. групп
     * @return id сохраненного или обновленного набора
     */
    Long saveOrUpdate(EnrExamGroupSet examGroupSet);

    /**
     * Переключает признак открытасти набора.
     * Открытым может быть только один набор ЭГ, может не быть открытого набора, закрыть набор ЭГ можно в любой момент.
     * @param examGroupSetId id набора экзам. группы
     */
    void doToggleOpen(Long examGroupSetId);

    /**
     * Проверяет есть ли открытый набор в рамках ПК.
     * @return true, если есть открытый набор, иначе false
     */
    boolean hasOpened(Long enrollmentCampaignId);

    /**
     * Возвращает набор ЭГ по умолчанию для выбора ЭГ для ДДС: открытый, или последний добавленный в рамках ПК
     * @param enrollmentCampaignId id ПК
     * @return набор ЭГ
     */
    EnrExamGroupSet getDefaultForExamPassDisc(Long enrollmentCampaignId);
}
