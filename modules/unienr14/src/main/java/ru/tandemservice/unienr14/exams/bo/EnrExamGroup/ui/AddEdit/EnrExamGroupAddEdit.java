/**
 *$Id: EnrExamGroupAddEdit.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.exams.bo.EnrExamGroup.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrExamGroupSet;

/**
 * @author Alexander Shaburov
 * @since 30.05.13
 */
@Configuration
public class EnrExamGroupAddEdit extends BusinessComponentManager
{
    public static final String EXAM_GROUP_SET_SELECT_DS = "examGroupSetSelectDS";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(EXAM_GROUP_SET_SELECT_DS, examGroupSetSelectDSHandler()).addColumn(EnrExamGroupSet.periodTitle().s()))
                .create();
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> examGroupSetSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrExamGroupSet.class)
            .where(EnrExamGroupSet.enrollmentCampaign(), EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN)
            .order(EnrExamGroupSet.beginDate())
            .pageable(true);
    }
}
