/* $Id:$ */
package ru.tandemservice.unienr14.student.bo.EnrStudent;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unienr14.student.bo.EnrStudent.logic.IEnrStudentDao;
import ru.tandemservice.unienr14.student.bo.EnrStudent.logic.EnrStudentDao;

/**
 * @author oleyba
 * @since 8/14/14
 */
@Configuration
public class EnrStudentManager extends BusinessObjectManager
{
    public static EnrStudentManager instance()
    {
        return instance(EnrStudentManager.class);
    }

    @Bean
    public IEnrStudentDao dao()
    {
        return new EnrStudentDao();
    }

    public static boolean isEntrantTabVisible(Long studentId) {
        return instance().dao().isEntrantTabVisible(studentId);
    }
}
