/* $Id$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.AddInfoStep;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.person.catalog.entity.BirthCountry;
import org.tandemframework.shared.person.catalog.entity.SportType;
import ru.tandemservice.unienr14.catalog.entity.EnrAccessCourse;
import ru.tandemservice.unienr14.catalog.entity.EnrAccessDepartment;

import java.util.List;

/**
 * @author nvankov
 * @since 6/5/14
 */
@Configuration
public class EnrEntrantRequestAddWizardAddInfoStep extends BusinessComponentManager
{
    public static final String SPORT_TYPE_DS = "sportTypeDS";
    public static final String ACCESS_COURSE_SELECT_DS = "accessCourseSelectDS";
    public static final String ACCESS_DEPARTMENT_SELECT_DS = "accessDepartmentSelectDS";
    public static final String BIRTH_COUNTRY_DS = "birthCountryDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(SPORT_TYPE_DS, sportTypeComboDSHandler()))
                .addDataSource(selectDS(ACCESS_COURSE_SELECT_DS, accessCourseSelectDSHandler()))
                .addDataSource(selectDS(ACCESS_DEPARTMENT_SELECT_DS, accessDepartmentSelectDSHandler()))
                .addDataSource(selectDS(BIRTH_COUNTRY_DS, birthCountryDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler sportTypeComboDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), SportType.class)
                .filter(SportType.title())
                .order(SportType.title());

    }

    @Bean
    public IDefaultComboDataSourceHandler accessDepartmentSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrAccessDepartment.class)
                .filter(EnrAccessDepartment.title())
                .order(EnrAccessDepartment.title())
                .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler accessCourseSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrAccessCourse.class)
                .filter(EnrAccessCourse.title())
                .order(EnrAccessCourse.title())
                .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler birthCountryDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), BirthCountry.class)
                .filter(BirthCountry.title())
                .order(BirthCountry.title())
                .pageable(true);
    }
}



    