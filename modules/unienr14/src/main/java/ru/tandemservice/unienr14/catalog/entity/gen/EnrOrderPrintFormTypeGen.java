package ru.tandemservice.unienr14.catalog.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.commonbase.catalog.entity.ScriptItem;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderPrintFormType;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Вид печатной формы приказа по абитуриентам
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrOrderPrintFormTypeGen extends ScriptItem
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.catalog.entity.EnrOrderPrintFormType";
    public static final String ENTITY_NAME = "enrOrderPrintFormType";
    public static final int VERSION_HASH = 1799488480;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORDER_TYPE = "orderType";
    public static final String P_PRIORITY = "priority";
    public static final String P_USED = "used";
    public static final String P_BASIC = "basic";
    public static final String P_COMMAND = "command";
    public static final String P_GROUP = "group";
    public static final String P_SELECT_HEADMAN = "selectHeadman";
    public static final String P_REASON_AND_BASIC = "reasonAndBasic";
    public static final String P_PRINT_PDF = "printPdf";

    private EnrOrderType _orderType;     // Тип приказа по абитуриентам
    private int _priority;     // Приоритет
    private boolean _used;     // Используется
    private boolean _basic;     // Отображать основание
    private boolean _command;     // Отображать приказываю
    private boolean _group;     // Отображать группу
    private boolean _selectHeadman;     // Выбор старосты
    private boolean _reasonAndBasic;     // Причина и основание
    private boolean _printPdf = false;     // Печатать в pdf

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Тип приказа по абитуриентам. Свойство не может быть null.
     */
    @NotNull
    public EnrOrderType getOrderType()
    {
        return _orderType;
    }

    /**
     * @param orderType Тип приказа по абитуриентам. Свойство не может быть null.
     */
    public void setOrderType(EnrOrderType orderType)
    {
        dirty(_orderType, orderType);
        _orderType = orderType;
    }

    /**
     * @return Приоритет. Свойство не может быть null.
     */
    @NotNull
    public int getPriority()
    {
        return _priority;
    }

    /**
     * @param priority Приоритет. Свойство не может быть null.
     */
    public void setPriority(int priority)
    {
        dirty(_priority, priority);
        _priority = priority;
    }

    /**
     * @return Используется. Свойство не может быть null.
     */
    @NotNull
    public boolean isUsed()
    {
        return _used;
    }

    /**
     * @param used Используется. Свойство не может быть null.
     */
    public void setUsed(boolean used)
    {
        dirty(_used, used);
        _used = used;
    }

    /**
     * @return Отображать основание. Свойство не может быть null.
     */
    @NotNull
    public boolean isBasic()
    {
        return _basic;
    }

    /**
     * @param basic Отображать основание. Свойство не может быть null.
     */
    public void setBasic(boolean basic)
    {
        dirty(_basic, basic);
        _basic = basic;
    }

    /**
     * @return Отображать приказываю. Свойство не может быть null.
     */
    @NotNull
    public boolean isCommand()
    {
        return _command;
    }

    /**
     * @param command Отображать приказываю. Свойство не может быть null.
     */
    public void setCommand(boolean command)
    {
        dirty(_command, command);
        _command = command;
    }

    /**
     * @return Отображать группу. Свойство не может быть null.
     */
    @NotNull
    public boolean isGroup()
    {
        return _group;
    }

    /**
     * @param group Отображать группу. Свойство не может быть null.
     */
    public void setGroup(boolean group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Выбор старосты. Свойство не может быть null.
     */
    @NotNull
    public boolean isSelectHeadman()
    {
        return _selectHeadman;
    }

    /**
     * @param selectHeadman Выбор старосты. Свойство не может быть null.
     */
    public void setSelectHeadman(boolean selectHeadman)
    {
        dirty(_selectHeadman, selectHeadman);
        _selectHeadman = selectHeadman;
    }

    /**
     * @return Причина и основание. Свойство не может быть null.
     */
    @NotNull
    public boolean isReasonAndBasic()
    {
        return _reasonAndBasic;
    }

    /**
     * @param reasonAndBasic Причина и основание. Свойство не может быть null.
     */
    public void setReasonAndBasic(boolean reasonAndBasic)
    {
        dirty(_reasonAndBasic, reasonAndBasic);
        _reasonAndBasic = reasonAndBasic;
    }

    /**
     * @return Печатать в pdf. Свойство не может быть null.
     */
    @NotNull
    public boolean isPrintPdf()
    {
        return _printPdf;
    }

    /**
     * @param printPdf Печатать в pdf. Свойство не может быть null.
     */
    public void setPrintPdf(boolean printPdf)
    {
        dirty(_printPdf, printPdf);
        _printPdf = printPdf;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EnrOrderPrintFormTypeGen)
        {
            setOrderType(((EnrOrderPrintFormType)another).getOrderType());
            setPriority(((EnrOrderPrintFormType)another).getPriority());
            setUsed(((EnrOrderPrintFormType)another).isUsed());
            setBasic(((EnrOrderPrintFormType)another).isBasic());
            setCommand(((EnrOrderPrintFormType)another).isCommand());
            setGroup(((EnrOrderPrintFormType)another).isGroup());
            setSelectHeadman(((EnrOrderPrintFormType)another).isSelectHeadman());
            setReasonAndBasic(((EnrOrderPrintFormType)another).isReasonAndBasic());
            setPrintPdf(((EnrOrderPrintFormType)another).isPrintPdf());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrOrderPrintFormTypeGen> extends ScriptItem.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrOrderPrintFormType.class;
        }

        public T newInstance()
        {
            return (T) new EnrOrderPrintFormType();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "orderType":
                    return obj.getOrderType();
                case "priority":
                    return obj.getPriority();
                case "used":
                    return obj.isUsed();
                case "basic":
                    return obj.isBasic();
                case "command":
                    return obj.isCommand();
                case "group":
                    return obj.isGroup();
                case "selectHeadman":
                    return obj.isSelectHeadman();
                case "reasonAndBasic":
                    return obj.isReasonAndBasic();
                case "printPdf":
                    return obj.isPrintPdf();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "orderType":
                    obj.setOrderType((EnrOrderType) value);
                    return;
                case "priority":
                    obj.setPriority((Integer) value);
                    return;
                case "used":
                    obj.setUsed((Boolean) value);
                    return;
                case "basic":
                    obj.setBasic((Boolean) value);
                    return;
                case "command":
                    obj.setCommand((Boolean) value);
                    return;
                case "group":
                    obj.setGroup((Boolean) value);
                    return;
                case "selectHeadman":
                    obj.setSelectHeadman((Boolean) value);
                    return;
                case "reasonAndBasic":
                    obj.setReasonAndBasic((Boolean) value);
                    return;
                case "printPdf":
                    obj.setPrintPdf((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "orderType":
                        return true;
                case "priority":
                        return true;
                case "used":
                        return true;
                case "basic":
                        return true;
                case "command":
                        return true;
                case "group":
                        return true;
                case "selectHeadman":
                        return true;
                case "reasonAndBasic":
                        return true;
                case "printPdf":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "orderType":
                    return true;
                case "priority":
                    return true;
                case "used":
                    return true;
                case "basic":
                    return true;
                case "command":
                    return true;
                case "group":
                    return true;
                case "selectHeadman":
                    return true;
                case "reasonAndBasic":
                    return true;
                case "printPdf":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "orderType":
                    return EnrOrderType.class;
                case "priority":
                    return Integer.class;
                case "used":
                    return Boolean.class;
                case "basic":
                    return Boolean.class;
                case "command":
                    return Boolean.class;
                case "group":
                    return Boolean.class;
                case "selectHeadman":
                    return Boolean.class;
                case "reasonAndBasic":
                    return Boolean.class;
                case "printPdf":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrOrderPrintFormType> _dslPath = new Path<EnrOrderPrintFormType>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrOrderPrintFormType");
    }
            

    /**
     * @return Тип приказа по абитуриентам. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrOrderPrintFormType#getOrderType()
     */
    public static EnrOrderType.Path<EnrOrderType> orderType()
    {
        return _dslPath.orderType();
    }

    /**
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrOrderPrintFormType#getPriority()
     */
    public static PropertyPath<Integer> priority()
    {
        return _dslPath.priority();
    }

    /**
     * @return Используется. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrOrderPrintFormType#isUsed()
     */
    public static PropertyPath<Boolean> used()
    {
        return _dslPath.used();
    }

    /**
     * @return Отображать основание. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrOrderPrintFormType#isBasic()
     */
    public static PropertyPath<Boolean> basic()
    {
        return _dslPath.basic();
    }

    /**
     * @return Отображать приказываю. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrOrderPrintFormType#isCommand()
     */
    public static PropertyPath<Boolean> command()
    {
        return _dslPath.command();
    }

    /**
     * @return Отображать группу. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrOrderPrintFormType#isGroup()
     */
    public static PropertyPath<Boolean> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Выбор старосты. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrOrderPrintFormType#isSelectHeadman()
     */
    public static PropertyPath<Boolean> selectHeadman()
    {
        return _dslPath.selectHeadman();
    }

    /**
     * @return Причина и основание. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrOrderPrintFormType#isReasonAndBasic()
     */
    public static PropertyPath<Boolean> reasonAndBasic()
    {
        return _dslPath.reasonAndBasic();
    }

    /**
     * @return Печатать в pdf. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrOrderPrintFormType#isPrintPdf()
     */
    public static PropertyPath<Boolean> printPdf()
    {
        return _dslPath.printPdf();
    }

    public static class Path<E extends EnrOrderPrintFormType> extends ScriptItem.Path<E>
    {
        private EnrOrderType.Path<EnrOrderType> _orderType;
        private PropertyPath<Integer> _priority;
        private PropertyPath<Boolean> _used;
        private PropertyPath<Boolean> _basic;
        private PropertyPath<Boolean> _command;
        private PropertyPath<Boolean> _group;
        private PropertyPath<Boolean> _selectHeadman;
        private PropertyPath<Boolean> _reasonAndBasic;
        private PropertyPath<Boolean> _printPdf;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Тип приказа по абитуриентам. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrOrderPrintFormType#getOrderType()
     */
        public EnrOrderType.Path<EnrOrderType> orderType()
        {
            if(_orderType == null )
                _orderType = new EnrOrderType.Path<EnrOrderType>(L_ORDER_TYPE, this);
            return _orderType;
        }

    /**
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrOrderPrintFormType#getPriority()
     */
        public PropertyPath<Integer> priority()
        {
            if(_priority == null )
                _priority = new PropertyPath<Integer>(EnrOrderPrintFormTypeGen.P_PRIORITY, this);
            return _priority;
        }

    /**
     * @return Используется. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrOrderPrintFormType#isUsed()
     */
        public PropertyPath<Boolean> used()
        {
            if(_used == null )
                _used = new PropertyPath<Boolean>(EnrOrderPrintFormTypeGen.P_USED, this);
            return _used;
        }

    /**
     * @return Отображать основание. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrOrderPrintFormType#isBasic()
     */
        public PropertyPath<Boolean> basic()
        {
            if(_basic == null )
                _basic = new PropertyPath<Boolean>(EnrOrderPrintFormTypeGen.P_BASIC, this);
            return _basic;
        }

    /**
     * @return Отображать приказываю. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrOrderPrintFormType#isCommand()
     */
        public PropertyPath<Boolean> command()
        {
            if(_command == null )
                _command = new PropertyPath<Boolean>(EnrOrderPrintFormTypeGen.P_COMMAND, this);
            return _command;
        }

    /**
     * @return Отображать группу. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrOrderPrintFormType#isGroup()
     */
        public PropertyPath<Boolean> group()
        {
            if(_group == null )
                _group = new PropertyPath<Boolean>(EnrOrderPrintFormTypeGen.P_GROUP, this);
            return _group;
        }

    /**
     * @return Выбор старосты. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrOrderPrintFormType#isSelectHeadman()
     */
        public PropertyPath<Boolean> selectHeadman()
        {
            if(_selectHeadman == null )
                _selectHeadman = new PropertyPath<Boolean>(EnrOrderPrintFormTypeGen.P_SELECT_HEADMAN, this);
            return _selectHeadman;
        }

    /**
     * @return Причина и основание. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrOrderPrintFormType#isReasonAndBasic()
     */
        public PropertyPath<Boolean> reasonAndBasic()
        {
            if(_reasonAndBasic == null )
                _reasonAndBasic = new PropertyPath<Boolean>(EnrOrderPrintFormTypeGen.P_REASON_AND_BASIC, this);
            return _reasonAndBasic;
        }

    /**
     * @return Печатать в pdf. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrOrderPrintFormType#isPrintPdf()
     */
        public PropertyPath<Boolean> printPdf()
        {
            if(_printPdf == null )
                _printPdf = new PropertyPath<Boolean>(EnrOrderPrintFormTypeGen.P_PRINT_PDF, this);
            return _printPdf;
        }

        public Class getEntityClass()
        {
            return EnrOrderPrintFormType.class;
        }

        public String getEntityName()
        {
            return "enrOrderPrintFormType";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
