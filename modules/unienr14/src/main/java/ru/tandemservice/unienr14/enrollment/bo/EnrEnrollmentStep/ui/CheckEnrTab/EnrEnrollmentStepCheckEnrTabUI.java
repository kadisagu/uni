package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.ui.CheckEnrTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.xml.EnrXmlEnrollmentCheckData;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.xml.IEnrXmlEnrollmentCheckDataOwner;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.ui.CompetitionPub.EnrEnrollmentStepCompetitionPub;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.ui.CompetitionPub.EnrEnrollmentStepCompetitionPubUI;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep;

import java.util.Map;

@Input({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "stepHolder.id", required=true)
})
public class EnrEnrollmentStepCheckEnrTabUI extends UIPresenter implements IEnrXmlEnrollmentCheckDataOwner
{
    private final EntityHolder<EnrEnrollmentStep> stepHolder = new EntityHolder<EnrEnrollmentStep>();
    public EntityHolder<EnrEnrollmentStep> getStepHolder() { return this.stepHolder; }
    public EnrEnrollmentStep getStep() { return this.getStepHolder().getValue(); }

    EnrXmlEnrollmentCheckData checkData = new EnrXmlEnrollmentCheckData(false);
    public EnrXmlEnrollmentCheckData getCheckData() { return checkData; }

    @Override
    public String getCompetitionPubBC()
    {
        return EnrEnrollmentStepCompetitionPub.class.getSimpleName();
    }

    @Override
    public Map getCompetitionPubParameters()
    {
        return new ParametersMap()
            .add(EnrEnrollmentStepCompetitionPubUI.PARAM_STEP, getStep().getId())
            .add(EnrEnrollmentStepCompetitionPubUI.PARAM_COMPETITION, getCheckData().getItem().getCompetitionId())
            .add(EnrEnrollmentStepCompetitionPubUI.PARAM_TARGET_ADMISSION_KIND, getCheckData().getItem().getTargetAdmissionKindId())
            ;
    }

    @Override
    public void onComponentRefresh()
    {
        final EnrEnrollmentStep step = this.getStepHolder().refresh(EnrEnrollmentStep.class);
        checkData.refresh(step);
    }

    public void onClickRefreshContext() {
        this.getSupport().setRefreshScheduled(true);
        checkData.forceRefresh();
    }

    public void onClickDownloadXml() {
        checkData.onClickDownloadXml();
    }
}

