package ru.tandemservice.unienr14.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Уровни бюджетного финансирования"
 * Имя сущности : enrLevelBudgetFinancing
 * Файл data.xml : unienr.catalog.data.xml
 */
public interface EnrLevelBudgetFinancingCodes
{
    /** Константа кода (code) элемента : Федеральный бюджет (title) */
    String FEDERAL_BUDGET = "1";
    /** Константа кода (code) элемента : Бюджет субъекта РФ (title) */
    String BUDGET_SUBJECT_R_F = "2";
    /** Константа кода (code) элемента : Местный бюджет (title) */
    String LOCAL_BUDGET = "3";

    Set<String> CODES = ImmutableSet.of(FEDERAL_BUDGET, BUDGET_SUBJECT_R_F, LOCAL_BUDGET);
}
