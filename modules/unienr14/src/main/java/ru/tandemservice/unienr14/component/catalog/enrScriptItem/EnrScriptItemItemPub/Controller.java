/* $Id$ */
// Copyright 2006-2012 Tandem Service Software
package ru.tandemservice.unienr14.component.catalog.enrScriptItem.EnrScriptItemItemPub;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultScriptCatalogItemPub.DefaultScriptCatalogItemPubController;
import ru.tandemservice.unienr14.catalog.entity.IEnrScriptItem;

/**
 * @author Vasily Zhukov
 * @since 22.12.2011
 */
public class Controller<T extends ICatalogItem & IScriptItem & IEnrScriptItem, Model extends ru.tandemservice.unienr14.component.catalog.enrScriptItem.EnrScriptItemItemPub.Model<T>, IDAO extends ru.tandemservice.unienr14.component.catalog.enrScriptItem.EnrScriptItemItemPub.IDAO<T, Model>> extends DefaultScriptCatalogItemPubController
{
}