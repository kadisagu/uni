package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x2_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrReportEnrollmentCampaignPassSummary
		// создано свойство enrollmentCommission
		if (!tool.table("enr14_rep_enr_camp_pass_sum_t").columnExists("enrollmentcommission_p"))
        {
			// создать колонку
			tool.createColumn("enr14_rep_enr_camp_pass_sum_t", new DBColumn("enrollmentcommission_p", DBType.TEXT));

		}

		// создано свойство firstPriorityOnly
        if (!tool.table("enr14_rep_enr_camp_pass_sum_t").columnExists("firstpriorityonly_p"))
		{
			// создать колонку
			tool.createColumn("enr14_rep_enr_camp_pass_sum_t", new DBColumn("firstpriorityonly_p", DBType.createVarchar(255)));

		}


    }
}