/* $Id$ */
package ru.tandemservice.unienr14.order.bo.EnrAllocationParagraph;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unienr14.order.bo.EnrAllocationParagraph.logic.EnrAllocationParagraphDao;
import ru.tandemservice.unienr14.order.bo.EnrAllocationParagraph.logic.IEnrAllocationParagraphDao;

/**
 * @author nvankov
 * @since 8/8/14
 */
@Configuration
public class EnrAllocationParagraphManager extends BusinessObjectManager
{
    public static EnrAllocationParagraphManager instance()
    {
        return instance(EnrAllocationParagraphManager.class);
    }

    @Bean
    public IEnrAllocationParagraphDao dao()
    {
        return new EnrAllocationParagraphDao();
    }
}



    