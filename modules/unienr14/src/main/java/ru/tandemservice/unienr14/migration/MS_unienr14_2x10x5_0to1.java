/* $Id$ */
package ru.tandemservice.unienr14.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.ISQLTranslator;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLUpdateQuery;

/**
 * @author Ekaterina Zvereva
 * @since 28.07.2016
 */
public class MS_unienr14_2x10x5_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.10.5")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ISQLTranslator translator = tool.getDialect().getSQLTranslator();

        SQLUpdateQuery updateQuery = new SQLUpdateQuery("enr14_olymp_diploma_t", "od");
        updateQuery.getUpdatedTableFrom().innerJoin(SQLFrom.table("enr14_c_olymp_t", "o"), "od.olympiad_id=o.id");
        updateQuery.getUpdatedTableFrom().innerJoin(SQLFrom.table("enr14_c_olymp_type_t", "type"),"type.id=o.olympiadtype_id");
        updateQuery.where("od.trainingclass_p is not null and (type.code_p='1' and od.trainingclass_p not in (9, 10, 11))");
        updateQuery.set("trainingclass_p", null);

        tool.executeUpdate(translator.toSql(updateQuery));

    }
}