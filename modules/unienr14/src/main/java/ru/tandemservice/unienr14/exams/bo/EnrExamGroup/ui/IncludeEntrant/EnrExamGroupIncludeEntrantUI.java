/* $Id:$ */
package ru.tandemservice.unienr14.exams.bo.EnrExamGroup.ui.IncludeEntrant;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.exams.bo.EnrExamGroup.EnrExamGroupManager;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroup;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroupScheduleEvent;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrExamGroupSet;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 6/7/14
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "examPassDiscipline.id")
})
public class EnrExamGroupIncludeEntrantUI extends UIPresenter
{
    private EnrExamPassDiscipline examPassDiscipline = new EnrExamPassDiscipline();

    private EnrOrgUnit orgUnit;
    private EnrExamGroupSet examGroupSet;
    private EnrExamGroup examGroup;

    private EnrExamGroup previousExamGroup;

	private EnrExamGroupScheduleEvent groupScheduleEvent;

    @Override
    public void onComponentActivate()
    {
        setExamPassDiscipline(IUniBaseDao.instance.get().getNotNull(EnrExamPassDiscipline.class, getExamPassDiscipline().getId()));

        setPreviousExamGroup(getExamPassDiscipline().getExamGroup());
        examGroup = getExamPassDiscipline().getExamGroup();
		if (examGroup != null)
		{
			final List<EnrExamGroupScheduleEvent> eventsForGroup = DataAccessServices.dao()
					.getList(EnrExamGroupScheduleEvent.class, EnrExamGroupScheduleEvent.examGroup(), examGroup, EnrExamGroupScheduleEvent.examScheduleEvent().scheduleEvent().durationBegin().s());
			groupScheduleEvent = eventsForGroup.get(0);
		}

        EnrExamGroupSet defaultSet = EnrExamGroupManager.instance().setDao().getDefaultForExamPassDisc(getExamPassDiscipline().getEntrant().getEnrollmentCampaign().getId());
        setExamGroupSet(examGroup == null ? defaultSet : examGroup.getExamGroupSet());

        List<EnrOrgUnit> enrOuList = IUniBaseDao.instance.get().getList(new DQLSelectBuilder()
            .fromEntity(EnrOrgUnit.class, "e").column("e")
            .where(eq(property("e", EnrOrgUnit.enrollmentCampaign()), value(getExamPassDiscipline().getEntrant().getEnrollmentCampaign())))
            .where(eq(property("e", EnrOrgUnit.institutionOrgUnit().orgUnit()), value(getExamPassDiscipline().getTerritorialOrgUnit())))
        );
        setOrgUnit(enrOuList.isEmpty() ? null : enrOuList.get(0));
    }

    @Override
    public void onComponentRefresh()
    {
        setExamPassDiscipline(IUniBaseDao.instance.get().getNotNull(EnrExamPassDiscipline.class, getExamPassDiscipline().getId()));
        if (!getExamPassDiscipline().isCanChangeExamGroup()) {
            throw new ApplicationException(EnrExamGroupManager.instance().getProperty("examGroup.addDisc.cannotChangeExamGroup"));
        }
    }

    public void onClickApply()
	{
        EnrExamGroupManager.instance().groupDao().setExamGroupAndUpdateTerritorial(getExamPassDiscipline(), getOrgUnit().getInstitutionOrgUnit().getOrgUnit(), examGroup, true);
        deactivate();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
	    dataSource.put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, getExamPassDiscipline().getEntrant().getEnrollmentCampaign());
        dataSource.put(EnrExamGroupIncludeEntrant.BIND_ENTRANT, getExamPassDiscipline().getEntrant());
        if(EnrExamGroupIncludeEntrant.GROUP_SCHEDULE_EVENT_DS.equals(dataSource.getName()))
        {
            dataSource.put(EnrExamGroupScheduleEvent.DS_PARAM_TERRITORIAL_ORG_UNIT, getOrgUnit().getInstitutionOrgUnit().getOrgUnit());
            dataSource.put(EnrExamGroupScheduleEvent.DS_PARAM_EXAM_GROUP_SET, getExamGroupSet());
            dataSource.put(EnrExamGroupScheduleEvent.DS_PARAM_DISCIPLINE, getExamPassDiscipline().getDiscipline());
            dataSource.put(EnrExamGroupScheduleEvent.DS_PARAM_PASS_FORM, getExamPassDiscipline().getPassForm());
            dataSource.put(EnrExamGroupScheduleEvent.DS_PARAM_CLOSED, false);
            dataSource.put(EnrExamGroupScheduleEvent.DS_PARAM_CURR_EXAM_GROUP, getPreviousExamGroup());
        }
    }

    // getters and setters

    public EnrExamPassDiscipline getExamPassDiscipline()
    {
        return examPassDiscipline;
    }

    public void setExamPassDiscipline(EnrExamPassDiscipline examPassDiscipline)
    {
        this.examPassDiscipline = examPassDiscipline;
    }

    public EnrExamGroupSet getExamGroupSet()
    {
        return examGroupSet;
    }

    public void setExamGroupSet(EnrExamGroupSet examGroupSet)
    {
        this.examGroupSet = examGroupSet;
    }

    public EnrOrgUnit getOrgUnit()
    {
        return orgUnit;
    }

    public void setOrgUnit(EnrOrgUnit orgUnit)
    {
        this.orgUnit = orgUnit;
    }

    public EnrExamGroup getPreviousExamGroup()
    {
        return previousExamGroup;
    }

    public void setPreviousExamGroup(EnrExamGroup previousExamGroup)
    {
        this.previousExamGroup = previousExamGroup;
    }

	public EnrExamGroupScheduleEvent getGroupScheduleEvent()
	{
		return groupScheduleEvent;
	}

	public void setGroupScheduleEvent(EnrExamGroupScheduleEvent groupScheduleEvent)
	{
		this.groupScheduleEvent = groupScheduleEvent;
		this.examGroup = groupScheduleEvent == null ? null : groupScheduleEvent.getExamGroup();
	}
}