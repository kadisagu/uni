package ru.tandemservice.unienr14.enrollment.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentConflictSolution;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItem;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItemOrderInfo;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Информация о предыдущих зачислениях абитуриента для шага зачисления
 *
 * Создается при создании шага зачисления на каждую выписку, по которой абитуриент зачислен, и это зачисление актуально, т.е. приказ не отменен так или иначе, на момент создания шага зачисления.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrEnrollmentStepItemOrderInfoGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItemOrderInfo";
    public static final String ENTITY_NAME = "enrEnrollmentStepItemOrderInfo";
    public static final int VERSION_HASH = 1630215314;
    private static IEntityMeta ENTITY_META;

    public static final String L_ITEM = "item";
    public static final String P_ORDER_INFO = "orderInfo";
    public static final String L_SOLUTION = "solution";
    public static final String L_EXTRACT = "extract";
    public static final String L_EXTRACT_ENTITY = "extractEntity";
    public static final String P_EXTRACT_CANCELLED = "extractCancelled";

    private EnrEnrollmentStepItem _item;     // Абитуриент в списке шага зачисления
    private String _orderInfo;     // Информация о приказе (на момент создания шага)
    private EnrEnrollmentConflictSolution _solution;     // Решение конфликта зачисления
    private EnrEnrollmentExtract _extract;     // Выписка о зачислении (на другой конкурс)
    private EnrRequestedCompetition _extractEntity;     // Выбранный конкурс из выписки
    private boolean _extractCancelled;     // Выписка отменена

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Абитуриент в списке шага зачисления. Свойство не может быть null.
     */
    @NotNull
    public EnrEnrollmentStepItem getItem()
    {
        return _item;
    }

    /**
     * @param item Абитуриент в списке шага зачисления. Свойство не может быть null.
     */
    public void setItem(EnrEnrollmentStepItem item)
    {
        dirty(_item, item);
        _item = item;
    }

    /**
     * @return Информация о приказе (на момент создания шага). Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getOrderInfo()
    {
        return _orderInfo;
    }

    /**
     * @param orderInfo Информация о приказе (на момент создания шага). Свойство не может быть null.
     */
    public void setOrderInfo(String orderInfo)
    {
        dirty(_orderInfo, orderInfo);
        _orderInfo = orderInfo;
    }

    /**
     * @return Решение конфликта зачисления. Свойство не может быть null.
     */
    @NotNull
    public EnrEnrollmentConflictSolution getSolution()
    {
        return _solution;
    }

    /**
     * @param solution Решение конфликта зачисления. Свойство не может быть null.
     */
    public void setSolution(EnrEnrollmentConflictSolution solution)
    {
        dirty(_solution, solution);
        _solution = solution;
    }

    /**
     * Выписка о зачислении (на конкурс, отличный от конкурса из item).
     *
     * @return Выписка о зачислении (на другой конкурс). Свойство не может быть null.
     */
    @NotNull
    public EnrEnrollmentExtract getExtract()
    {
        return _extract;
    }

    /**
     * @param extract Выписка о зачислении (на другой конкурс). Свойство не может быть null.
     */
    public void setExtract(EnrEnrollmentExtract extract)
    {
        dirty(_extract, extract);
        _extract = extract;
    }

    /**
     * Выбранный коркурс (из выписки).
     *
     * @return Выбранный конкурс из выписки. Свойство не может быть null.
     */
    @NotNull
    public EnrRequestedCompetition getExtractEntity()
    {
        return _extractEntity;
    }

    /**
     * @param extractEntity Выбранный конкурс из выписки. Свойство не может быть null.
     */
    public void setExtractEntity(EnrRequestedCompetition extractEntity)
    {
        dirty(_extractEntity, extractEntity);
        _extractEntity = extractEntity;
    }

    /**
     * Фиксирует состояние флага «Отменена» выписки на момент создания шага зачисления.
     *
     * @return Выписка отменена. Свойство не может быть null.
     */
    @NotNull
    public boolean isExtractCancelled()
    {
        return _extractCancelled;
    }

    /**
     * @param extractCancelled Выписка отменена. Свойство не может быть null.
     */
    public void setExtractCancelled(boolean extractCancelled)
    {
        dirty(_extractCancelled, extractCancelled);
        _extractCancelled = extractCancelled;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrEnrollmentStepItemOrderInfoGen)
        {
            setItem(((EnrEnrollmentStepItemOrderInfo)another).getItem());
            setOrderInfo(((EnrEnrollmentStepItemOrderInfo)another).getOrderInfo());
            setSolution(((EnrEnrollmentStepItemOrderInfo)another).getSolution());
            setExtract(((EnrEnrollmentStepItemOrderInfo)another).getExtract());
            setExtractEntity(((EnrEnrollmentStepItemOrderInfo)another).getExtractEntity());
            setExtractCancelled(((EnrEnrollmentStepItemOrderInfo)another).isExtractCancelled());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrEnrollmentStepItemOrderInfoGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrEnrollmentStepItemOrderInfo.class;
        }

        public T newInstance()
        {
            return (T) new EnrEnrollmentStepItemOrderInfo();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "item":
                    return obj.getItem();
                case "orderInfo":
                    return obj.getOrderInfo();
                case "solution":
                    return obj.getSolution();
                case "extract":
                    return obj.getExtract();
                case "extractEntity":
                    return obj.getExtractEntity();
                case "extractCancelled":
                    return obj.isExtractCancelled();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "item":
                    obj.setItem((EnrEnrollmentStepItem) value);
                    return;
                case "orderInfo":
                    obj.setOrderInfo((String) value);
                    return;
                case "solution":
                    obj.setSolution((EnrEnrollmentConflictSolution) value);
                    return;
                case "extract":
                    obj.setExtract((EnrEnrollmentExtract) value);
                    return;
                case "extractEntity":
                    obj.setExtractEntity((EnrRequestedCompetition) value);
                    return;
                case "extractCancelled":
                    obj.setExtractCancelled((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "item":
                        return true;
                case "orderInfo":
                        return true;
                case "solution":
                        return true;
                case "extract":
                        return true;
                case "extractEntity":
                        return true;
                case "extractCancelled":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "item":
                    return true;
                case "orderInfo":
                    return true;
                case "solution":
                    return true;
                case "extract":
                    return true;
                case "extractEntity":
                    return true;
                case "extractCancelled":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "item":
                    return EnrEnrollmentStepItem.class;
                case "orderInfo":
                    return String.class;
                case "solution":
                    return EnrEnrollmentConflictSolution.class;
                case "extract":
                    return EnrEnrollmentExtract.class;
                case "extractEntity":
                    return EnrRequestedCompetition.class;
                case "extractCancelled":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrEnrollmentStepItemOrderInfo> _dslPath = new Path<EnrEnrollmentStepItemOrderInfo>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrEnrollmentStepItemOrderInfo");
    }
            

    /**
     * @return Абитуриент в списке шага зачисления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItemOrderInfo#getItem()
     */
    public static EnrEnrollmentStepItem.Path<EnrEnrollmentStepItem> item()
    {
        return _dslPath.item();
    }

    /**
     * @return Информация о приказе (на момент создания шага). Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItemOrderInfo#getOrderInfo()
     */
    public static PropertyPath<String> orderInfo()
    {
        return _dslPath.orderInfo();
    }

    /**
     * @return Решение конфликта зачисления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItemOrderInfo#getSolution()
     */
    public static EnrEnrollmentConflictSolution.Path<EnrEnrollmentConflictSolution> solution()
    {
        return _dslPath.solution();
    }

    /**
     * Выписка о зачислении (на конкурс, отличный от конкурса из item).
     *
     * @return Выписка о зачислении (на другой конкурс). Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItemOrderInfo#getExtract()
     */
    public static EnrEnrollmentExtract.Path<EnrEnrollmentExtract> extract()
    {
        return _dslPath.extract();
    }

    /**
     * Выбранный коркурс (из выписки).
     *
     * @return Выбранный конкурс из выписки. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItemOrderInfo#getExtractEntity()
     */
    public static EnrRequestedCompetition.Path<EnrRequestedCompetition> extractEntity()
    {
        return _dslPath.extractEntity();
    }

    /**
     * Фиксирует состояние флага «Отменена» выписки на момент создания шага зачисления.
     *
     * @return Выписка отменена. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItemOrderInfo#isExtractCancelled()
     */
    public static PropertyPath<Boolean> extractCancelled()
    {
        return _dslPath.extractCancelled();
    }

    public static class Path<E extends EnrEnrollmentStepItemOrderInfo> extends EntityPath<E>
    {
        private EnrEnrollmentStepItem.Path<EnrEnrollmentStepItem> _item;
        private PropertyPath<String> _orderInfo;
        private EnrEnrollmentConflictSolution.Path<EnrEnrollmentConflictSolution> _solution;
        private EnrEnrollmentExtract.Path<EnrEnrollmentExtract> _extract;
        private EnrRequestedCompetition.Path<EnrRequestedCompetition> _extractEntity;
        private PropertyPath<Boolean> _extractCancelled;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Абитуриент в списке шага зачисления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItemOrderInfo#getItem()
     */
        public EnrEnrollmentStepItem.Path<EnrEnrollmentStepItem> item()
        {
            if(_item == null )
                _item = new EnrEnrollmentStepItem.Path<EnrEnrollmentStepItem>(L_ITEM, this);
            return _item;
        }

    /**
     * @return Информация о приказе (на момент создания шага). Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItemOrderInfo#getOrderInfo()
     */
        public PropertyPath<String> orderInfo()
        {
            if(_orderInfo == null )
                _orderInfo = new PropertyPath<String>(EnrEnrollmentStepItemOrderInfoGen.P_ORDER_INFO, this);
            return _orderInfo;
        }

    /**
     * @return Решение конфликта зачисления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItemOrderInfo#getSolution()
     */
        public EnrEnrollmentConflictSolution.Path<EnrEnrollmentConflictSolution> solution()
        {
            if(_solution == null )
                _solution = new EnrEnrollmentConflictSolution.Path<EnrEnrollmentConflictSolution>(L_SOLUTION, this);
            return _solution;
        }

    /**
     * Выписка о зачислении (на конкурс, отличный от конкурса из item).
     *
     * @return Выписка о зачислении (на другой конкурс). Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItemOrderInfo#getExtract()
     */
        public EnrEnrollmentExtract.Path<EnrEnrollmentExtract> extract()
        {
            if(_extract == null )
                _extract = new EnrEnrollmentExtract.Path<EnrEnrollmentExtract>(L_EXTRACT, this);
            return _extract;
        }

    /**
     * Выбранный коркурс (из выписки).
     *
     * @return Выбранный конкурс из выписки. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItemOrderInfo#getExtractEntity()
     */
        public EnrRequestedCompetition.Path<EnrRequestedCompetition> extractEntity()
        {
            if(_extractEntity == null )
                _extractEntity = new EnrRequestedCompetition.Path<EnrRequestedCompetition>(L_EXTRACT_ENTITY, this);
            return _extractEntity;
        }

    /**
     * Фиксирует состояние флага «Отменена» выписки на момент создания шага зачисления.
     *
     * @return Выписка отменена. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItemOrderInfo#isExtractCancelled()
     */
        public PropertyPath<Boolean> extractCancelled()
        {
            if(_extractCancelled == null )
                _extractCancelled = new PropertyPath<Boolean>(EnrEnrollmentStepItemOrderInfoGen.P_EXTRACT_CANCELLED, this);
            return _extractCancelled;
        }

        public Class getEntityClass()
        {
            return EnrEnrollmentStepItemOrderInfo.class;
        }

        public String getEntityName()
        {
            return "enrEnrollmentStepItemOrderInfo";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
