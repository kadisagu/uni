/* $Id:$ */
package ru.tandemservice.unienr14.settings.bo.EnrSettings.ui.StudentEnrollment;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseSettingsUtil;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.competition.entity.IEnrollmentStudentSettings;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrSettings.EnrSettingsManager;
import ru.tandemservice.unienr14.settings.bo.EnrSettings.ui.SelectEduOu.EnrSettingsSelectEduOu;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

/**
 * @author oleyba
 * @since 8/15/14
 */
public class EnrSettingsStudentEnrollmentUI extends UIPresenter
{
    @Override
    public void onComponentRefresh()
    {
        this.getSettings().set(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
    }

    public void onClickAutoFill() {
        EnrSettingsManager.instance().dao().doAutoFillEduOu(getCampaign());
    }

    public void onClickEdit() {
        _uiActivation.asRegionDialog(EnrSettingsSelectEduOu.class).parameter(PublisherActivator.PUBLISHER_ID_KEY, getListenerParameterAsLong()).activate();
    }

    public void onClickDelete() {
        EnrSettingsManager.instance().dao().doClearEduOu(getListenerParameterAsLong());
    }

    public void onClickPurge() {
        Long id = getListenerParameterAsLong();
        IEnrollmentStudentSettings settings = DataAccessServices.dao().getNotNull(id);
        EducationOrgUnit eduOu = settings.getEducationOrgUnit();

        if (null != eduOu) {
            EducationLevelsHighSchool eduHs = eduOu.getEducationLevelHighSchool();
            EducationLevels eduLvl = eduHs.getEducationLevel();
            try
            {
                EnrSettingsManager.instance().dao().doClearEduOu(id);
                DataAccessServices.dao().delete(eduOu);
            }
            catch (Exception e)
            {
                getUserContext().getInfoCollector().add("Не удалось удалить НПП.");
                EnrSettingsManager.instance().dao().updateSettingsItem(settings, eduOu);
                return;
            }

            if (IUniBaseDao.instance.get().existsEntity(EducationOrgUnit.class, EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, eduHs))
            {
                getUserContext().getInfoCollector().add("Не удалось удалить НПв.");
                return;
            }

            try
            {
                DataAccessServices.dao().delete(eduHs);
            }
            catch (Exception e)
            {
                getUserContext().getInfoCollector().add("Не удалось удалить НПв.");
                return;
            }



            if (IUniBaseDao.instance.get().existsEntity(EducationLevelsHighSchool.class, EducationLevelsHighSchool.L_EDUCATION_LEVEL, eduLvl))
            {
                getUserContext().getInfoCollector().add("Не удалось удалить НПм.");
                return;
            }

            try
            {
                DataAccessServices.dao().delete(eduLvl);
            }
            catch (Exception e)
            {
                getUserContext().getInfoCollector().add("Не удалось удалить НПм.");
                return;
            }

        }

    }

    public void onEnrCampaignRefresh()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getCampaign());
    }

    public void onChangeRequestType()
    {
        onClickClear();
    }

    public void onClickClear()
    {
        CommonBaseSettingsUtil.clearAndSaveSettingsExcept(this.getSettings(), EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, EnrSettingsStudentEnrollment.BIND_REQUEST_TYPE);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, getCampaign());
        dataSource.put(EnrSettingsStudentEnrollment.BIND_REQUEST_TYPE, getRequestType());
        dataSource.put(EnrSettingsStudentEnrollment.BIND_PROGRAM_FORM, this.getSettings().get(EnrSettingsStudentEnrollment.BIND_PROGRAM_FORM));
        dataSource.put(EnrSettingsStudentEnrollment.BIND_ORG_UNITS, this.getSettings().get(EnrSettingsStudentEnrollment.BIND_ORG_UNITS));
        dataSource.put(EnrSettingsStudentEnrollment.BIND_SUBJECT_CODE, this.getSettings().get(EnrSettingsStudentEnrollment.BIND_SUBJECT_CODE));
        dataSource.put(EnrSettingsStudentEnrollment.BIND_PROGRAM_SUBJECTS, this.getSettings().get(EnrSettingsStudentEnrollment.BIND_PROGRAM_SUBJECTS));
        dataSource.put(EnrSettingsStudentEnrollment.BIND_EDU_PROGRAMS, this.getSettings().get(EnrSettingsStudentEnrollment.BIND_EDU_PROGRAMS));
        dataSource.put(EnrSettingsStudentEnrollment.BIND_PROGRAM_SET_TITLE, this.getSettings().get(EnrSettingsStudentEnrollment.BIND_PROGRAM_SET_TITLE));
    }

    public EnrEnrollmentCampaign getCampaign() { return this.getSettings().get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN); }

    public EnrRequestType getRequestType() { return this.getSettings().get(EnrSettingsStudentEnrollment.BIND_REQUEST_TYPE); }

    public boolean isNothingSelected() { return getCampaign() == null || getRequestType() == null; }
}