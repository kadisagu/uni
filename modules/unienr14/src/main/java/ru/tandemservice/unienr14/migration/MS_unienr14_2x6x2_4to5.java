package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x2_4to5 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrEntrantBaseDocument

		// создано свойство scanCopy
        if(tool.tableExists("enr14_enrtant_base_doc_t") && !tool.columnExists("enr14_enrtant_base_doc_t", "scancopy_p"))
		{
			// создать колонку
			tool.createColumn("enr14_enrtant_base_doc_t", new DBColumn("scancopy_p", DBType.LONG));

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrOlympiadDiploma

		// создано свойство scanCopy
        if(tool.tableExists("enr14_olymp_diploma_t") && !tool.columnExists("enr14_olymp_diploma_t", "scancopy_p"))
		{
			// создать колонку
			tool.createColumn("enr14_olymp_diploma_t", new DBColumn("scancopy_p", DBType.LONG));

		}


    }
}