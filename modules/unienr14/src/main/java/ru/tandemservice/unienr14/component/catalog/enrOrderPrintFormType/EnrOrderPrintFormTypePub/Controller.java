/* $Id$ */
package ru.tandemservice.unienr14.component.catalog.enrOrderPrintFormType.EnrOrderPrintFormTypePub;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IEntityHandler;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultScriptCatalogPub.DefaultScriptCatalogPubController;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderPrintFormType;
import ru.tandemservice.unienr14.catalog.entity.IEnrScriptItem;

/**
 * @author azhebko
 * @since 15.07.2014
 */
public class Controller extends DefaultScriptCatalogPubController<EnrOrderPrintFormType, Model, IDAO>
{
    @Override
    protected DynamicListDataSource createListDataSource(IBusinessComponent context)
    {
        final Model model = getModel(context);

        DynamicListDataSource dataSource = super.createListDataSource(context);
        int columnsSize = dataSource.getColumns().size();
        dataSource.addColumn(new ToggleColumn("Печать в pdf", EnrOrderPrintFormType.P_PRINT_PDF)
                        .onLabel("Печатать в pdf")
                        .toggleOnListener("togglePrintPdfScript")
                        .offLabel("Печатать в rtf")
                        .toggleOffListener("togglePrintPdfScript"), columnsSize - 1
        );

        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteItem", "Удалить элемент «{0}» из справочника?", ICatalogItem.CATALOG_ITEM_TITLE)
            .setPermissionKey(model.getCatalogItemDelete())
            .setDisableHandler(entity -> model.getDisabledIds().contains(entity.getId())));

        return dataSource;
    }

    public void togglePrintPdfScript(IBusinessComponent context)
    {

        getDao().doTogglePrintPdfScript(context.<Long>getListenerParameter());
    }
}