package ru.tandemservice.unienr14.catalog.entity;

import ru.tandemservice.unienr14.catalog.entity.codes.EnrEnrollmentStepKindCodes;
import ru.tandemservice.unienr14.catalog.entity.gen.*;

/**
 * Вариант работы с шагом зачисления
 */
public class EnrEnrollmentStepKind extends EnrEnrollmentStepKindGen
{
    public boolean isUseRecommendation() {
        return EnrEnrollmentStepKindCodes.REC_AND_ADHERE.equals(getCode()) || EnrEnrollmentStepKindCodes.REC_AND_IGNORE.equals(getCode());
    }

    public boolean isUsePercentage() {
        return EnrEnrollmentStepKindCodes.NO_REC_PERCENTAGE.equals(getCode());
    }
}