package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unienr14_2x9x3_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies() {
        return new ScriptDependency[] {
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.3")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrInternalExamReason

		// изменен тип свойства description
		{
			// изменить тип колонки
			tool.changeColumnType("enr14_c_int_exam_reason_t", "description_p", DBType.TEXT);

		}


        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrBenefitCategory

        // изменен тип свойства description
        {
            // изменить тип колонки
            tool.changeColumnType("enr14_c_benefit_category_t", "description_p", DBType.TEXT);

        }


    }
}