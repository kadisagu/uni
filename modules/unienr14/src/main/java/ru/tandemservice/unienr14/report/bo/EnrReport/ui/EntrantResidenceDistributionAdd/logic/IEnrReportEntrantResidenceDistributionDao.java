/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantResidenceDistributionAdd.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantResidenceDistributionAdd.EnrReportEntrantResidenceDistributionAddUI;

/**
 * @author rsizonenko
 * @since 20.06.2014
 */
public interface IEnrReportEntrantResidenceDistributionDao extends INeedPersistenceSupport {
    long createReport(EnrReportEntrantResidenceDistributionAddUI model);
}
