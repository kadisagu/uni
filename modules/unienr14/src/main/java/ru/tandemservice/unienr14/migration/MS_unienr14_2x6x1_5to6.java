package ru.tandemservice.unienr14.migration;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.IDBConnect;
import org.tandemframework.dbsupport.ddl.ResultRowProcessor;
import org.tandemframework.dbsupport.ddl.schema.columns.LongDBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x1_5to6 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
        {
            new ScriptDependency("org.tandemframework", "1.6.15"),
            new ScriptDependency("org.tandemframework.shared", "1.6.1")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrEntrantMarkSourceOlympiad -> enrEntrantMarkSourceBenefit

        // интерфейс IEnrEntrantBenefitStatement теперь включает enrEntrantMarkSourceBenefit - view надо грохнуть (она боольше не валидна) - будут падать констрейнты
        tool.dropView("ienrentrantbenefitstatement_v");

        // поехали переименование
        tool.renameTable("enr14_mark_src_olymp_t", "enr14_mark_src_benefit_t");
        tool.entityCodes().rename("enrEntrantMarkSourceOlympiad".toLowerCase(), "enrEntrantMarkSourceBenefit".toLowerCase());
        tool.renameColumn("enr14_mark_src_benefit_t", "olympiaddiploma_id", "mainproof_id");

        // добавляем колонку
        tool.createColumn("enr14_mark_src_benefit_t", new LongDBColumn("benefitcategory_id"));

        // ВОШы
        tool.executeUpdate(
            "update enr14_mark_src_benefit_t set benefitcategory_id=(select x.id from enr14_c_benefit_category_t x where x.code_p=?) " +
            "where id in (" +
            " select b.id from enr14_mark_src_benefit_t b " +
            " inner join enr14_olymp_diploma_t d on d.id=b.mainproof_id " +
            " inner join enr14_c_olymp_t o on o.id=d.olympiad_id" +
            " inner join enr14_c_olymp_type_t t on t.id=o.olympiadtype_id " +
            " where t.code_p=?" +
            ")",
            "023" /*EnrBenefitCategoryCodes.STO_BALLOV_POBEDITELI_I_PRIZERY_VSEROSSIYSKOY_OLIMPIADY_SHKOLNIKOV*/,
            "1" /*EnrOlympiadTypeCodes.ALL_RUSSIA*/
        );

        // всем остальным - обычные олимпиады
        tool.executeUpdate(
            "update enr14_mark_src_benefit_t set benefitcategory_id=(select x.id from enr14_c_benefit_category_t x where x.code_p=?) " +
            "where benefitcategory_id is null",
            "025" /*EnrBenefitCategoryCodes.STO_BALLOV_POBEDITELI_I_PRIZERY_OLIMPIAD_SHKOLNIKOV*/
        );

        // для каждого источника баллов нужно создать теперь свою связь

        // enr14_mark_src_benefit_t.id -> enr14_mark_src_benefit_t.mainproof_id
        Map<Long, Long> map = tool.executeQuery(
            new ResultRowProcessor<Map<Long, Long>>(new HashMap<Long, Long>()) {
                @Override protected void processRow(IDBConnect ctool, ResultSet rs, Map<Long, Long> result) throws SQLException {
                    if (null != result.put(rs.getLong(1), rs.getLong(2))) {
                        throw new IllegalStateException();
                    }
                }
            },
            "select id, mainproof_id from enr14_mark_src_benefit_t"
        );

        Short code = tool.entityCodes().get("enrEntrantBenefitProof".toLowerCase());
        if (null == code) { throw new IllegalStateException(); }

        for (Map.Entry<Long, Long> e: map.entrySet()) {
            Long id = EntityIDGenerator.generateNewId(code);
            tool.executeUpdate("insert into enr14_entrant_benefit_proof_t (id, discriminator, benefitstatement_id, document_id, version) values (?, ?, ?, ?, ?)", id, code, e.getKey(), e.getValue(), 1);
        }


        // обязательность колонок
        tool.setColumnNullable("enr14_mark_src_benefit_t", "benefitcategory_id", false);
        tool.setColumnNullable("enr14_mark_src_benefit_t", "mainproof_id", false);

    }
}