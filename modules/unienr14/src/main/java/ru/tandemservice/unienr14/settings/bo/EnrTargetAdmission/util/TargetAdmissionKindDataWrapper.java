/* $Id:$ */
package ru.tandemservice.unienr14.settings.bo.EnrTargetAdmission.util;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import ru.tandemservice.unienr14.catalog.entity.EnrTargetAdmissionKind;

import java.util.List;

/**
 * @author azhebko
 * @since 25.04.2014
 */
public class TargetAdmissionKindDataWrapper extends DataWrapper
{
    public static final String USE_IN_ENR_CAMPAIGN = "useInEnrCampaign";
    public static final String EXTERNAL_ORG_UNITS = "externalOrgUnits";
    public static final String STATE_INTEREST = "stateInterest";

    private boolean _useInEnrCampaign;
    private boolean _stateInterest;
    private List<ExternalOrgUnit> _externalOrgUnits;

    public TargetAdmissionKindDataWrapper(EnrTargetAdmissionKind entity, boolean useInEnrCampaign, boolean stateInterest, List<ExternalOrgUnit> externalOrgUnits)
    {
        super(entity);
        _useInEnrCampaign = useInEnrCampaign;
        _externalOrgUnits = externalOrgUnits;
        _stateInterest = stateInterest;
    }

    public boolean isUseInEnrCampaign(){ return _useInEnrCampaign; }
    public void setUseInEnrCampaign(boolean useInEnrCampaign){ _useInEnrCampaign = useInEnrCampaign; }

    public boolean isStateInterest(){ return _stateInterest; }
    public void setStateInterest(boolean stateInterest){ _stateInterest = stateInterest; }

    public List<ExternalOrgUnit> getExternalOrgUnits(){ return _externalOrgUnits; }
    public void setExternalOrgUnits(List<ExternalOrgUnit> externalOrgUnits){ _externalOrgUnits = externalOrgUnits; }
}