/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubBenefitTab;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.formatter.NewLineFormatter;
import org.tandemframework.core.view.formatter.PropertyFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.shared.commonbase.base.util.SimplePublisherLinkResolver;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.BenefitEdit.EnrEntrantBenefitEdit;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceBenefit;
import ru.tandemservice.unienr14.request.entity.*;
import ru.tandemservice.unienr14.request.entity.gen.IEnrEntrantBenefitStatementGen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author oleyba
 * @since 2/26/14
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "entrant.id")
})
public class EnrEntrantPubBenefitTabUI extends UIPresenter
{
    private EnrEntrant entrant = new EnrEntrant();
    private StaticListDataSource<EnrBenefitStatementWrapper> dataSource;

    private boolean documentsNotSelected;

    @Override
    public void onComponentRefresh()
    {
        setEntrant(IUniBaseDao.instance.get().getNotNull(EnrEntrant.class, getEntrant().getId()));

        List<EnrBenefitStatementWrapper> benefitList = new ArrayList<>();
        Map<Long, EnrBenefitStatementWrapper> benefitMap = new HashMap<>();

        for (EnrRequestedCompetitionNoExams reqComp : IUniBaseDao.instance.get().getList(EnrRequestedCompetitionNoExams.class,
            EnrRequestedCompetitionNoExams.request().entrant(), getEntrant(),
            EnrRequestedCompetitionNoExams.request().regNumber().s(), EnrRequestedCompetitionNoExams.priority().s())) {
            EnrBenefitStatementWrapper wrapper = new EnrBenefitStatementWrapper(reqComp);
            benefitList.add(wrapper);
            benefitMap.put(wrapper.getId(), wrapper);
        }

        for (EnrRequestedCompetitionExclusive reqComp : IUniBaseDao.instance.get().getList(EnrRequestedCompetitionExclusive.class,
            EnrRequestedCompetitionExclusive.request().entrant(), getEntrant(),
            EnrRequestedCompetitionExclusive.request().regNumber().s(), EnrRequestedCompetitionExclusive.priority().s())) {
            EnrBenefitStatementWrapper wrapper = new EnrBenefitStatementWrapper(reqComp);
            benefitList.add(wrapper);
            benefitMap.put(wrapper.getId(), wrapper);
        }

        for (EnrEntrantRequest request : IUniBaseDao.instance.get().getList(EnrEntrantRequest.class,
            EnrEntrantRequest.entrant(), getEntrant(),
            EnrEntrantRequest.P_REG_NUMBER)) {
            if (request.getBenefitCategory() != null) {
                EnrBenefitStatementWrapper wrapper = new EnrBenefitStatementWrapper(request);
                benefitList.add(wrapper);
                benefitMap.put(wrapper.getId(), wrapper);
            }
        }

        for (EnrEntrantMarkSourceBenefit markSource : IUniBaseDao.instance.get().getList(EnrEntrantMarkSourceBenefit.class,
            EnrEntrantMarkSourceBenefit.entrant(), getEntrant(),
            EnrEntrantMarkSourceBenefit.mainProof().registrationDate().s())) {
            EnrBenefitStatementWrapper wrapper = new EnrBenefitStatementWrapper(markSource);
            benefitList.add(wrapper);
            benefitMap.put(wrapper.getId(), wrapper);
        }

        for (EnrEntrantBenefitProof proof : IUniBaseDao.instance.get().getList(EnrEntrantBenefitProof.class,
            EnrEntrantBenefitProof.benefitStatement().entrant(), getEntrant(),
            EnrEntrantBenefitProof.document().seria().s(), EnrEntrantBenefitProof.document().number().s())) {
            EnrBenefitStatementWrapper wrapper = benefitMap.get(proof.getBenefitStatement().getId());
            if (wrapper == null) continue;
            if (wrapper.getDocumentList() == null) {
                wrapper.setDocumentList(new ArrayList<IEnrEntrantBenefitProofDocument>());
            }
            wrapper.getDocumentList().add(proof.getDocument());
        }

        setDocumentsNotSelected(false);
        for (EnrBenefitStatementWrapper wrapper : benefitList) {
            setDocumentsNotSelected(CollectionUtils.isEmpty(wrapper.getDocumentList()) || isDocumentsNotSelected());
        }

        setDataSource(new StaticListDataSource<EnrBenefitStatementWrapper>());
        getDataSource().addColumn(new SimpleColumn("Вид особого права", "statement." + IEnrEntrantBenefitStatementGen.benefitCategory().benefitType().title().s()).setClickable(false).setOrderable(false));
        getDataSource().addColumn(new SimpleColumn("Использовано в рамках конкурса", "statement." + IEnrEntrantBenefitStatement.P_BENEFIT_STATEMENT_TITLE).setFormatter(NewLineFormatter.NOBR_IN_LINES).setClickable(false).setOrderable(false));
        getDataSource().addColumn(new PublisherLinkColumn("№ заявления", "statement.request.regNumber").setResolver(new SimplePublisherLinkResolver("statement.request.id")).setOrderable(false));
        getDataSource().addColumn(new SimpleColumn("Категория граждан, дающая право", "statement." + IEnrEntrantBenefitStatementGen.benefitCategory().title().s()).setClickable(false).setOrderable(false));
        getDataSource().addColumn(new PublisherLinkColumn("Документы, подтверждающие право", "", "documentList")
        .setResolver(new SimplePublisherLinkResolver(DataWrapper.ID))
        .setFormatter(new PropertyFormatter(IEnrEntrantBenefitProofDocument.DISPLAYABLE_TITLE))
        .setClickable(true).setOrderable(false));
        getDataSource().addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickBenefitEdit").setPermissionKey("enr14EntrantPubBenefitTabEditData"));
        getDataSource().setupRows(benefitList);
    }

    public void onClickBenefitEdit() {
        getActivationBuilder().asRegionDialog(EnrEntrantBenefitEdit.class)
        .parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong())
        .activate();
    }

    public void onClickBenefitDelete() {
        IUniBaseDao.instance.get().delete(getListenerParameterAsLong());
        onComponentRefresh();
    }

    // presenter

    public boolean isNoBenefits() {
        return getDataSource().getEntityList().isEmpty();
    }

    // getters and setters

    public EnrEntrant getEntrant()
    {
        return entrant;
    }

    public void setEntrant(EnrEntrant entrant)
    {
        this.entrant = entrant;
    }

    public StaticListDataSource<EnrBenefitStatementWrapper> getDataSource()
    {
        return dataSource;
    }

    public void setDataSource(StaticListDataSource<EnrBenefitStatementWrapper> dataSource)
    {
        this.dataSource = dataSource;
    }

    public boolean isDocumentsNotSelected()
    {
        return documentsNotSelected;
    }

    public void setDocumentsNotSelected(boolean documentsNotSelected)
    {
        this.documentsNotSelected = documentsNotSelected;
    }

    // inner classes

    @SuppressWarnings("serial")
    public static class EnrBenefitStatementWrapper extends IdentifiableWrapper {
        private IEnrEntrantBenefitStatement statement;
        private List<IEnrEntrantBenefitProofDocument> documentList;

        @SuppressWarnings("unchecked")
        private EnrBenefitStatementWrapper(IEnrEntrantBenefitStatement statement) throws ClassCastException
        {
            super(statement);
            this.statement = statement;
        }

        public List<IEnrEntrantBenefitProofDocument> getDocumentList() { return documentList; }
        public void setDocumentList(List<IEnrEntrantBenefitProofDocument> documentList) { this.documentList = documentList; }
        public IEnrEntrantBenefitStatement getStatement() { return statement; }
        public void setStatement(IEnrEntrantBenefitStatement statement) { this.statement = statement; }
    }
}