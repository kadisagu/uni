/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantFromCoursesCountAdd.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantFromCoursesCountAdd.EnrReportEntrantFromCoursesCountAddUI;

/**
 * @author rsizonenko
 * @since 29.05.2014
 */
public interface IEnrReportEntrantFromCoursesCountDao extends INeedPersistenceSupport
{
    Long createReport(EnrReportEntrantFromCoursesCountAddUI enrReportEntrantFromCoursesCountAddUI);
}
