/**
 *$Id: EnrEnrollmentOrderBasicUI.java 34429 2014-05-26 10:13:50Z oleyba $
 */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentOrder.ui.Basic;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.view.formatter.NewLineFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderBasic;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderReason;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentOrder.EnrEnrollmentOrderManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentOrder.logic.EnrEnrollmentOrderReasonBasicSearchDSHandler;

import java.util.List;

/**
 * @author Alexander Shaburov
 * @since 05.07.13
 */
public class EnrEnrollmentOrderBasicUI extends UIPresenter
{
    private DataWrapper _currentRow;
    private DataWrapper _editedRow;
    private Integer _editedRowIndex;
    private List<EnrOrderBasic> _currentEditRowValue;

    @Override
    public void onComponentRefresh()
    {
        refreshSettingsList();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (getEditedRow() == null) return;

        EnrOrderReason reason = IUniBaseDao.instance.get().getNotNull(EnrOrderReason.class, getEditedRow().getId());
        if (reason == null) return;

        dataSource.put(EnrEnrollmentOrderBasic.PARAM_ORDER_TYPE, reason.getOrderType());
    }

    public void refreshSettingsList()
    {
        getConfig().getDataSource(EnrEnrollmentOrderBasic.REASON_BASIC_SEARCH_DS).doCleanupDataSource();
        getConfig().getDataSource(EnrEnrollmentOrderBasic.REASON_BASIC_SEARCH_DS).doPrepareDataSource();

        _editedRow = null;
        _editedRowIndex = null;
        _currentEditRowValue = null;
    }

    @SuppressWarnings("unchecked")
    public void onClickEditRow()
    {
        if (_editedRow != null)
            return;

        DataWrapper baseRow = findRow();

        _editedRow = new DataWrapper(baseRow.getWrapped());
        _editedRowIndex = getRowList().indexOf(baseRow);

        List<String> currentBasicCodeList = (List<String>) baseRow.getProperty(EnrEnrollmentOrderReasonBasicSearchDSHandler.BASICS_CODE_COLUMN);
        _currentEditRowValue = currentBasicCodeList != null ? DataAccessServices.dao().getList(EnrOrderBasic.class, EnrOrderBasic.code(), currentBasicCodeList, EnrOrderBasic.title().s()) : null;
    }

    public void onClickSaveRow()
    {
        if (_editedRowIndex != null && getRowList().size() > _editedRowIndex && _editedRowIndex >= 0)
            EnrEnrollmentOrderManager.instance().basicDao().updateOrderReasonToBasicsRel(_editedRow.getId(), UniBaseDao.ids(getCurrentBasicList()));

        getSupport().doRefresh();

        refreshSettingsList();
    }

    public void onClickCancelEdit()
    {
        refreshSettingsList();
    }

    // Getters & Setters

    public String getCurrentRowBasicRawTitle()
    {
        return NewLineFormatter.NOBR_IN_LINES.format(_currentRow.<String>get(EnrEnrollmentOrderReasonBasicSearchDSHandler.BASICS_TITLE_COLUMN));
    }

    private DataWrapper findRow()
    {
        final Long id = getListenerParameterAsLong();
        DataWrapper row = null;

        for (DataWrapper r : getRowList())
        {
            if (r.getId().equals(id))
                row = r;
        }

        return row;
    }

    public List<DataWrapper> getRowList()
    {
        return getConfig().getDataSource(EnrEnrollmentOrderBasic.REASON_BASIC_SEARCH_DS).getRecords();
    }

    public List<EnrOrderBasic> getCurrentBasicList()
    {
        if (!isEditMode())
            throw new IllegalStateException();

        return _currentEditRowValue;
    }

    public boolean isEditMode()
    {
        return _editedRow != null;
    }

    public boolean isCurrentRowInEditMode()
    {
        return _editedRow != null && _editedRowIndex.equals(getRowList().indexOf(_currentRow));
    }

    // Accessors

    public DataWrapper getCurrentRow()
    {
        return _currentRow;
    }

    public void setCurrentRow(DataWrapper currentRow)
    {
        _currentRow = currentRow;
    }

    public DataWrapper getEditedRow()
    {
        return _editedRow;
    }

    public void setEditedRow(DataWrapper editedRow)
    {
        _editedRow = editedRow;
    }

    public Integer getEditedRowIndex()
    {
        return _editedRowIndex;
    }

    public void setEditedRowIndex(Integer editedRowIndex)
    {
        _editedRowIndex = editedRowIndex;
    }

    public List<EnrOrderBasic> getCurrentEditRowValue()
    {
        return _currentEditRowValue;
    }

    public void setCurrentEditRowValue(List<EnrOrderBasic> currentEditRowValue)
    {
        _currentEditRowValue = currentEditRowValue;
    }
}
