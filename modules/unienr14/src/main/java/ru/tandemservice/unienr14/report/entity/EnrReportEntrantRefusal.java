package ru.tandemservice.unienr14.report.entity;

import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantRefusalAdd.EnrReportEntrantRefusalAdd;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrReport;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrStorableReportDesc;
import ru.tandemservice.unienr14.report.entity.gen.EnrReportEntrantRefusalGen;

import java.util.Arrays;
import java.util.List;

/**
 * Лица, которым отказали в приеме документов
 */
public class EnrReportEntrantRefusal extends EnrReportEntrantRefusalGen implements IEnrReport
{

    public static final String REPORT_KEY = "enr14ReportEntrantRefusal";


    @SuppressWarnings("unchecked")
    private static List<String> properties = Arrays.asList(P_ENR_ORG_UNIT,
        P_ENR_REFUSAL_REASON);

    @Override
    public IEnrStorableReportDesc getDesc() {
        return getDescripton();
    }

    @Override
    public String getPeriodTitle() {
        return "с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateFrom()) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateTo());
    }

    public static IEnrStorableReportDesc getDescripton() {
        return new IEnrStorableReportDesc() {
            @Override public String getReportKey() { return REPORT_KEY; }
            @Override public Class<? extends IEnrReport> getReportClass() { return EnrReportEntrantRefusal.class; }
            @Override public List<String> getPropertyList() { return properties; }

            @Override public Class<? extends BusinessComponentManager> getAddFormComponent() { return EnrReportEntrantRefusalAdd.class; }
            @Override public String getPubTitle() { return "Отчет «Лица, которым отказали в приеме документов»"; }
            @Override public String getListTitle() { return "Список отчетов «Лица, которым отказали в приеме документов»"; }
        };
    }


}