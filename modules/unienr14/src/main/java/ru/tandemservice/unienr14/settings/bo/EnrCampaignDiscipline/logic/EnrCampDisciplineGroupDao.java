/**
 *$Id: EnrCampDisciplineGroupDao.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.settings.bo.EnrCampaignDiscipline.logic;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import java.util.ArrayList;
import java.util.List;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.settings.daemon.IEnrSettingsDaemonBean;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDisciplineGroup;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDisciplineGroupElement;

/**
 * @author Alexander Shaburov
 * @since 09.04.13
 */
public class EnrCampDisciplineGroupDao extends UniBaseDao implements IEnrCampDisciplineGroupDao
{
    @Override
    public Long saveOrUpdateDiscGroup(EnrCampaignDisciplineGroup group, List<EnrCampaignDiscipline> disciplineList)
    {
        saveOrUpdate(group);

        final List<EnrCampaignDisciplineGroupElement> sourceDiscGroupElementList = new DQLSelectBuilder().fromEntity(EnrCampaignDisciplineGroupElement.class, "e").column(property("e"))
        .where(eq(property(EnrCampaignDisciplineGroupElement.group().fromAlias("e")), value(group)))
        .createStatement(getSession()).list();

        final List<EnrCampaignDisciplineGroupElement> targetDiscGroupElementList = new ArrayList<>(disciplineList.size());
        for (EnrCampaignDiscipline discipline : disciplineList)
        {
            EnrCampaignDisciplineGroupElement element = new EnrCampaignDisciplineGroupElement();
            element.setDiscipline(discipline);
            element.setGroup(group);
            targetDiscGroupElementList.add(element);
        }

        new MergeAction.SessionMergeAction<CoreCollectionUtils.Pair<Long, Long>, EnrCampaignDisciplineGroupElement>() {
            @Override protected CoreCollectionUtils.Pair<Long, Long> key(EnrCampaignDisciplineGroupElement source) {
                return new CoreCollectionUtils.Pair<>(source.getDiscipline().getId(), source.getGroup().getId());
            }
            @Override protected EnrCampaignDisciplineGroupElement buildRow(EnrCampaignDisciplineGroupElement source) {
                return source;
            }
            @Override protected void fill(EnrCampaignDisciplineGroupElement target, EnrCampaignDisciplineGroupElement source)  {
                target.update(source);
            }
        }.merge(sourceDiscGroupElementList, targetDiscGroupElementList);

        // обновляем балл (в той же транзакции)
        IEnrSettingsDaemonBean.instance.get().doUpdateGroupDefaultPassMark();

        return group.getId();
    }
}
