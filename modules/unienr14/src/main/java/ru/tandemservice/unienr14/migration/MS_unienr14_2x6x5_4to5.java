package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x5_4to5 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.5")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrProgramSetItemOrgUnitPlan

		// создано свойство educationOrgUnit
		{
			// создать колонку
			tool.createColumn("enr14_pr_set_item_ou_plan_t", new DBColumn("educationorgunit_id", DBType.LONG));

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrProgramSetOrgUnit

		// создано свойство educationOrgUnit
		{
			// создать колонку
			tool.createColumn("enr14_program_set_ou_t", new DBColumn("educationorgunit_id", DBType.LONG));

		}


    }
}