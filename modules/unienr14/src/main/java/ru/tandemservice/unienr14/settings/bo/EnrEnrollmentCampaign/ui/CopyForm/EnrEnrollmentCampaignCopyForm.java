/* $Id:$ */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.ui.CopyForm;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;

/**
 * @author rsizonenko
 * @since 31.01.2015
 */
@Configuration
public class EnrEnrollmentCampaignCopyForm extends BusinessComponentManager {

    public static final String ENR_CAMPAIGN_DS = "enrollmentCampaignDS";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ENR_CAMPAIGN_DS, enrollmentCampaignDSHandler()))
                .addDataSource(EducationCatalogsManager.instance().eduYearDSConfig())
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> enrollmentCampaignDSHandler()
    {
        return EnrEnrollmentCampaignManager.instance().enrCampaignDSHandler();
    }
}
