/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.HorizontalSummaryEnrollmentResultsAdd.logic;

import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.RtfRowIntercepterRawText;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.RtfElement;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantAccessCourse;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantAccessDepartment;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.HorizontalSummaryEnrollmentResultsAdd.EnrReportHorizontalSummaryEnrollmentResultsAddUI;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportStageSelector;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportUtil;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.FilterParametersPrinter;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.RtfBackslashScreener;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder;
import ru.tandemservice.unienr14.report.entity.EnrReportHorizontalSummaryEnrollmentResults;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionExclusive;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 02.09.2014
 */
public class EnrReportHorizontalSummaryEnrollmentResultsDao extends UniBaseDao implements IEnrReportHorizontalSummaryEnrollmentResultsDao {
    @Override
    public long createReport(EnrReportHorizontalSummaryEnrollmentResultsAddUI model) {
        EnrReportHorizontalSummaryEnrollmentResults report = new EnrReportHorizontalSummaryEnrollmentResults();
        report.setFormingDate(new Date());

        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateSelector().getDateFrom());
        report.setDateTo(model.getDateSelector().getDateTo());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, EnrReportHorizontalSummaryEnrollmentResults.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE, EnrReportHorizontalSummaryEnrollmentResults.P_COMPENSATION_TYPE, "shortTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, EnrReportHorizontalSummaryEnrollmentResults.P_PROGRAM_FORM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPETITION_TYPE, EnrReportHorizontalSummaryEnrollmentResults.P_COMPETITION_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, EnrReportHorizontalSummaryEnrollmentResults.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, EnrReportHorizontalSummaryEnrollmentResults.P_FORMATIVE_ORG_UNIT, "fullTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, EnrReportHorizontalSummaryEnrollmentResults.P_PROGRAM_SUBJECT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, EnrReportHorizontalSummaryEnrollmentResults.P_EDU_PROGRAM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, EnrReportHorizontalSummaryEnrollmentResults.P_PROGRAM_SET, "title");

        if (model.getParallelSelector().isParallelActive())
            report.setParallel(model.getParallelSelector().getParallel().getTitle());

        report.setSplitEduPrograms(model.isSplitEduPrograms() ? "Да" : "Нет");

        DatabaseFile content = new DatabaseFile();

        content.setContent(buildReport(model));
        content.setFilename("EnrReportHorizontalSummaryEnrollmentResults.rtf");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        save(content);

        report.setContent(content);
        save(report);

        return report.getId();
    }

    private byte[] buildReport(EnrReportHorizontalSummaryEnrollmentResultsAddUI model)
    {
        // rtf
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(EnrScriptItem.class, EnrScriptItemCodes.REPORT_HORIZONTAL_SUMMARY_ENROLLMENT_RESULTS);
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());

        // Фильтры
        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder()
                .notArchiveOnly()
                .filter(model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo())
                .filter(model.getEnrollmentCampaign());
        filterAddon.applyFilters(requestedCompDQL, requestedCompDQL.competition());

        if (model.getParallelSelector().isParallelActive())
        {
            if (model.getParallelSelector().isParallelOnly())
                requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.TRUE)));
            if (model.getParallelSelector().isSkipParallel())
                requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.FALSE)));
        }
        EnrReportStageSelector.docAcceptanceStageFilter().applyFilter(requestedCompDQL, "reqComp");

        requestedCompDQL.column("reqComp");

        List<EnrRequestedCompetition> fromQueryRecComps = getList(requestedCompDQL);

        requestedCompDQL.resetColumns().column("entrant");

        List<EnrEntrant> fromQueryEntrants = getList(requestedCompDQL);

        DQLSelectBuilder accessDep = new DQLSelectBuilder().fromEntity(EnrEntrantAccessDepartment.class, "accDep")
                .where(in(property(EnrEntrantAccessDepartment.entrant().fromAlias("accDep")), fromQueryEntrants))
                .column(property(EnrEntrantAccessDepartment.entrant().fromAlias("accDep")));

        List<EnrEntrant> fromQAccessDepEntrants = getList(accessDep);

        requestedCompDQL.joinEntity("entrant", DQLJoinType.inner, EnrEntrantAccessCourse.class, "course", eq(property(EnrEntrantAccessCourse.entrant().fromAlias("course")), property("entrant")));

        List<EnrEntrant> fromQAccessCourseEntrants = getList(requestedCompDQL);

        String reqTypeTitle = ((EnrRequestType) model.getCompetitionFilterAddon().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE).getValue()).getTitle();
        String progFormTitle = ((EduProgramForm) model.getCompetitionFilterAddon().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM).getValue()).getTitle();
        String compensTypeTitle = ((CompensationType) model.getCompetitionFilterAddon().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE).getValue()).isBudget() ? "бюджетные места" : "места с оплатой стоимости обучения";
        String year = DateFormatter.DATE_FORMATTER_JUST_YEAR.format(model.getEnrollmentCampaign().getDateFrom());


        if (model.isSplitEduPrograms())
        {
            Comparator<EnrProgramSetOrgUnit> programSetOrgUnitComparator = new Comparator<EnrProgramSetOrgUnit>() {
                @Override
                public int compare(EnrProgramSetOrgUnit o1, EnrProgramSetOrgUnit o2) {
                    // головное сначала
                    int res = Boolean.compare(o1.getOrgUnit().getInstitutionOrgUnit().getOrgUnit().getParent() == null, o2.getOrgUnit().getInstitutionOrgUnit().getOrgUnit().getParent() == null);
                    if (res != 0) return -res;
                    // затем по алфавиту по филиалу
                    res = o1.getOrgUnit().getInstitutionOrgUnit().getOrgUnit().getShortTitle().compareToIgnoreCase(o2.getOrgUnit().getInstitutionOrgUnit().getOrgUnit().getShortTitle());
                    if (res != 0) return res;
                    // затем по формирующему
                    res = o1.getEffectiveFormativeOrgUnit().getPrintTitle().compareToIgnoreCase(o2.getEffectiveFormativeOrgUnit().getPrintTitle());
                    if (res != 0) return res;
                    // затем по ОП
                    res = o1.getProgramSet().getTitle().compareToIgnoreCase(o2.getProgramSet().getTitle());
                    return res;
                }
            };

            Map<EnrProgramSetOrgUnit, Set<EnrRequestedCompetition>> dataMap = new TreeMap<>(programSetOrgUnitComparator);

            for (EnrRequestedCompetition reqComp : fromQueryRecComps) {
                final Set<EnrRequestedCompetition> requestedCompetitionSet = SafeMap.safeGet(dataMap, reqComp.getCompetition().getProgramSetOrgUnit(), HashSet.class);
                requestedCompetitionSet.add(reqComp);
            }


            RtfElement table = ((RtfElement) UniRtfUtil.findElement(document.getElementList(), "T"));

            document.getElementList().clear();


            boolean firstIter = true;

            for (Map.Entry<EnrProgramSetOrgUnit, Set<EnrRequestedCompetition>> entry : dataMap.entrySet()) {

                if (!firstIter)
                {
                    document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
                    document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
                    document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
                    document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
                    document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
                }

                firstIter = false;

                document.getElementList().add(table.getClone());

                AddressDetailed address = entry.getKey().getOrgUnit().getInstitutionOrgUnit().getOrgUnit().getAddress();
                AddressItem city = address != null ? address.getSettlement() : null;
                AddressItem region = city != null ? city.getParent() : null;

                StringBuilder formativeBuilder = new StringBuilder().append(entry.getKey().getEffectiveFormativeOrgUnit().getPrintTitle());
                if (!entry.getKey().getOrgUnit().getInstitutionOrgUnit().getOrgUnit().equals(TopOrgUnit.getInstance())
                        && !entry.getKey().getOrgUnit().getInstitutionOrgUnit().getOrgUnit().equals(entry.getKey().getEffectiveFormativeOrgUnit()))
                    formativeBuilder.append(" (").append(entry.getKey().getOrgUnit().getInstitutionOrgUnit().getOrgUnit().getShortTitle()).append(")");

                StringBuilder titleBuilder = new StringBuilder().append("РЕЗУЛЬТАТЫ ПРИЕМА В ")
                        .append(TopOrgUnit.getInstance().getShortTitle())
                        .append(" в ")
                        .append(year)
                        .append(" г. \\line ")
                        .append(formativeBuilder.toString()).append(" \\line ")
                        .append(reqTypeTitle).append(", ")
                        .append(progFormTitle).append(" форма обучения, ")
                        .append(compensTypeTitle).append(" \\line ")
                        .append(entry.getKey().getProgramSet().getTitle());

                String[][] title = new String[][]{
                        {
                                new RtfBackslashScreener().screenBackslashes(titleBuilder.toString())
                        }
                };

                new RtfInjectModifier()
                        .put("city", city != null ? city.getTitleWithType() : "Не задан фактический адрес")
                        .put("region", region != null ? region.getTitleWithType() : "Не задан фактический адрес")
                        .modify(document);

                List<String[]> tTable = makeTable(entry.getValue(), fromQAccessCourseEntrants, fromQAccessDepEntrants, city, region);
                RtfTableModifier modifier = new RtfTableModifier();
                modifier.put("T", tTable.toArray(new String[tTable.size()][]));
                modifier.put("title", title);
                modifier.put("title", new RtfRowIntercepterRawText());
                modifier.modify(document);


            }
        }
        else
        {
            AddressItem city = TopOrgUnit.getInstance().getAddress().getSettlement();
            AddressItem region = city != null ? city.getParent() : null;

            StringBuilder titleBuilder = new StringBuilder().append("РЕЗУЛЬТАТЫ ПРИЕМА В ")
                    .append(TopOrgUnit.getInstance().getShortTitle())
                    .append(" в ")
                    .append(year)
                    .append(" г.")
                    .append(FilterParametersPrinter.RTF_LINE)
                    .append(reqTypeTitle).append(", ")
                    .append(progFormTitle).append(" форма обучения, ")
                    .append(compensTypeTitle);

            String[][] title = new String[][]{
                    {
                            new RtfBackslashScreener().screenBackslashes(titleBuilder.toString())
                    }
            };

            new RtfInjectModifier()
                    .put("city", city != null ? city.getTitleWithType() : "Не задан фактический адрес")
                    .put("region", region != null ? region.getTitleWithType() : "Не задан фактический адрес")
                    .modify(document);

            List<String[]> tTable = makeTable(fromQueryRecComps, fromQAccessCourseEntrants, fromQAccessDepEntrants, city, region);
            RtfTableModifier modifier = new RtfTableModifier();
            modifier.put("T", tTable.toArray(new String[tTable.size()][]));
            modifier.put("title", title);
            modifier.put("title", new RtfRowIntercepterRawText());
            modifier.modify(document);
        }

        return RtfUtil.toByteArray(document);
    }

    // метод возвращает таблицу с данными
    private List<String[]> makeTable(Collection<EnrRequestedCompetition> reqComps,
                                     Collection<EnrEntrant> accessCourses,
                                     Collection<EnrEntrant> accessDepartments,
                                     AddressItem city,
                                     AddressItem region)
    {
        List<String[]> tTable = new ArrayList<>();

        Set<EnrEntrantRequest> totalDocAcc = new HashSet<>();
        Set<EnrEntrantRequest> totalExPass = new HashSet<>();
        Set<EnrRequestedCompetition> totalEnrStage = new HashSet<>();

        Set<EnrEntrantRequest> maleDocAcc = new HashSet<>();
        Set<EnrEntrantRequest> maleExPass = new HashSet<>();
        Set<EnrRequestedCompetition> maleEnrStage = new HashSet<>();

        Set<EnrEntrantRequest> femaleDocAcc = new HashSet<>();
        Set<EnrEntrantRequest> femaleExPass = new HashSet<>();
        Set<EnrRequestedCompetition> femaleEnrStage = new HashSet<>();

        Set<EnrEntrantRequest> sooDocAcc = new HashSet<>();
        Set<EnrEntrantRequest> sooExPass = new HashSet<>();
        Set<EnrRequestedCompetition> sooEnrStage = new HashSet<>();

        Set<EnrEntrantRequest> sooCurrYearDocAcc = new HashSet<>();
        Set<EnrEntrantRequest> sooCurrYearExPass = new HashSet<>();
        Set<EnrRequestedCompetition> sooCurrYearEnrStage = new HashSet<>();

        Set<EnrEntrantRequest> npoDocAcc = new HashSet<>();
        Set<EnrEntrantRequest> npoExPass = new HashSet<>();
        Set<EnrRequestedCompetition> npoEnrStage = new HashSet<>();

        Set<EnrEntrantRequest> npoCurrYearDocAcc = new HashSet<>();
        Set<EnrEntrantRequest> npoCurrYearExPass = new HashSet<>();
        Set<EnrRequestedCompetition> npoCurrYearEnrStage = new HashSet<>();

        Set<EnrEntrantRequest> spoDocAcc = new HashSet<>();
        Set<EnrEntrantRequest> spoExPass = new HashSet<>();
        Set<EnrRequestedCompetition> spoEnrStage = new HashSet<>();

        Set<EnrEntrantRequest> spoCurrYearDocAcc = new HashSet<>();
        Set<EnrEntrantRequest> spoCurrYearExPass = new HashSet<>();
        Set<EnrRequestedCompetition> spoCurrYearEnrStage = new HashSet<>();

        Set<EnrEntrantRequest> bachDocAcc = new HashSet<>();
        Set<EnrEntrantRequest> bachExPass = new HashSet<>();
        Set<EnrRequestedCompetition> bachEnrStage = new HashSet<>();

        Set<EnrEntrantRequest> bachCurrYearDocAcc = new HashSet<>();
        Set<EnrEntrantRequest> bachCurrYearExPass = new HashSet<>();
        Set<EnrRequestedCompetition> bachCurrYearEnrStage = new HashSet<>();

        Set<EnrEntrantRequest> specDocAcc = new HashSet<>();
        Set<EnrEntrantRequest> specExPass = new HashSet<>();
        Set<EnrRequestedCompetition> specEnrStage = new HashSet<>();

        Set<EnrEntrantRequest> specCurrYearDocAcc = new HashSet<>();
        Set<EnrEntrantRequest> specCurrYearExPass = new HashSet<>();
        Set<EnrRequestedCompetition> specCurrYearEnrStage = new HashSet<>();

        Set<EnrEntrantRequest> magDocAcc = new HashSet<>();
        Set<EnrEntrantRequest> magExPass = new HashSet<>();
        Set<EnrRequestedCompetition> magEnrStage = new HashSet<>();

        Set<EnrEntrantRequest> magCurrYearDocAcc = new HashSet<>();
        Set<EnrEntrantRequest> magCurrYearExPass = new HashSet<>();
        Set<EnrRequestedCompetition> magCurrYearEnrStage = new HashSet<>();

        Set<EnrEntrantRequest> cityDocAcc = new HashSet<>();
        Set<EnrEntrantRequest> cityExPass = new HashSet<>();
        Set<EnrRequestedCompetition> cityEnrStage = new HashSet<>();

        Set<EnrEntrantRequest> regionDocAcc = new HashSet<>();
        Set<EnrEntrantRequest> regionExPass = new HashSet<>();
        Set<EnrRequestedCompetition> regionEnrStage = new HashSet<>();

        Set<EnrEntrantRequest> nativeDocAcc = new HashSet<>();
        Set<EnrEntrantRequest> nativeExPass = new HashSet<>();
        Set<EnrRequestedCompetition> nativeEnrStage = new HashSet<>();

        Set<EnrEntrantRequest> foreignDocAcc = new HashSet<>();
        Set<EnrEntrantRequest> foreignExPass = new HashSet<>();
        Set<EnrRequestedCompetition> foreignEnrStage = new HashSet<>();

        Set<EnrEntrantRequest> orphanDocAcc = new HashSet<>();
        Set<EnrEntrantRequest> orphanExPass = new HashSet<>();
        Set<EnrRequestedCompetition> orphanEnrStage = new HashSet<>();

        Set<EnrEntrantRequest> invalidDocAcc = new HashSet<>();
        Set<EnrEntrantRequest> invalidExPass = new HashSet<>();
        Set<EnrRequestedCompetition> invalidEnrStage = new HashSet<>();

        Set<EnrEntrantRequest> accessCoursesDocAcc = new HashSet<>();
        Set<EnrEntrantRequest> accessCoursesExPass = new HashSet<>();
        Set<EnrRequestedCompetition> accessCoursesEnrStage = new HashSet<>();

        Set<EnrEntrantRequest> accessDepDocAcc = new HashSet<>();
        Set<EnrEntrantRequest> accessDepExPass = new HashSet<>();
        Set<EnrRequestedCompetition> accessDepEnrStage = new HashSet<>();

        for (EnrRequestedCompetition reqComp : reqComps) {
            boolean examsPassed = EnrReportStageSelector.examPassedCheckByPriority(reqComp.getState().getPriority());
            boolean enrolled = EnrReportStageSelector.enrollmentResultsCheckByPriority(reqComp.getState().getPriority());

            EnrEntrantRequest request = reqComp.getRequest();
            totalDocAcc.add(request);
            if (examsPassed) totalExPass.add(request);
            if (enrolled) totalEnrStage.add(reqComp);

            if (request.getIdentityCard().getSex().isMale()) {
                maleDocAcc.add(request);
                if (examsPassed) maleExPass.add(request);
                if (enrolled) maleEnrStage.add(reqComp);
            }
            else {
                femaleDocAcc.add(request);
                if (examsPassed) femaleExPass.add(request);
                if (enrolled) femaleEnrStage.add(reqComp);
            }

            boolean endedInCurrentYear = (reqComp.getRequest().getEduDocument().getYearEnd()) - CoreDateUtils.getYear(reqComp.getRegDate()) == 0;

            switch (reqComp.getRequest().getEduDocument().getEduLevel().getCode()) {
                case ("2013.1.4") :
                {
                    sooDocAcc.add(request);
                    if (examsPassed) sooExPass.add(request);
                    if (enrolled) sooEnrStage.add(reqComp);

                    if (endedInCurrentYear)
                    {
                        sooCurrYearDocAcc.add(request);
                        if (examsPassed) sooCurrYearExPass.add(request);
                        if (enrolled) sooCurrYearEnrStage.add(reqComp);
                    }

                    break;
                }

                case ("2013.2.1") :
                {
                    spoDocAcc.add(request);
                    if (examsPassed) spoExPass.add(request);
                    if (enrolled) spoEnrStage.add(reqComp);

                    if (endedInCurrentYear)
                    {
                        spoCurrYearDocAcc.add(request);
                        if (examsPassed) spoCurrYearExPass.add(request);
                        if (enrolled) spoCurrYearEnrStage.add(reqComp);
                    }

                    break;
                }

                case ("2013.2.2") :
                {
                    bachDocAcc.add(request);
                    if (examsPassed) bachExPass.add(request);
                    if (enrolled) bachEnrStage.add(reqComp);

                    if (endedInCurrentYear)
                    {
                        bachCurrYearDocAcc.add(request);
                        if (examsPassed) bachCurrYearExPass.add(request);
                        if (enrolled) bachCurrYearEnrStage.add(reqComp);
                    }

                    break;
                }
            }

            switch (reqComp.getRequest().getEduDocument().getEduDocumentKind().getCode()) {
                case ("0030") :
                {
                    npoDocAcc.add(request);
                    if (examsPassed) npoExPass.add(request);
                    if (enrolled) npoEnrStage.add(reqComp);

                    if (endedInCurrentYear)
                    {
                        npoCurrYearDocAcc.add(request);
                        if (examsPassed) npoCurrYearExPass.add(request);
                        if (enrolled) npoCurrYearEnrStage.add(reqComp);
                    }

                    break;
                }

                case ("0060") :
                {
                    specDocAcc.add(request);
                    if (examsPassed) specExPass.add(request);
                    if (enrolled) specEnrStage.add(reqComp);

                    if (endedInCurrentYear)
                    {
                        specCurrYearDocAcc.add(request);
                        if (examsPassed) specCurrYearExPass.add(request);
                        if (enrolled) specCurrYearEnrStage.add(reqComp);
                    }

                    break;
                }

                case ("0070") :
                {
                    magDocAcc.add(request);
                    if (examsPassed) magExPass.add(request);
                    if (enrolled) magEnrStage.add(reqComp);

                    if (endedInCurrentYear)
                    {
                        magCurrYearDocAcc.add(request);
                        if (examsPassed) magCurrYearExPass.add(request);
                        if (enrolled) magCurrYearEnrStage.add(reqComp);
                    }

                    break;
                }
            }

            boolean notCityOrRegion = true;

            if (reqComp.getRequest().getIdentityCard().getAddress() != null && reqComp.getRequest().getIdentityCard().getAddress() instanceof AddressDetailed)
            {
                AddressDetailed address = ((AddressDetailed) reqComp.getRequest().getIdentityCard().getAddress());
                if (address.getSettlement() != null) {
                    if (address.getSettlement().equals(city)) {
                        notCityOrRegion = false;
                        cityDocAcc.add(request);
                        if (examsPassed) cityExPass.add(request);
                        if (enrolled) cityEnrStage.add(reqComp);
                    }
                    if (address.getSettlement().getParent() != null && address.getSettlement().getParent().equals(region))
                    {
                        notCityOrRegion = false;
                        regionDocAcc.add(request);
                        if (examsPassed) regionExPass.add(request);
                        if (enrolled) regionEnrStage.add(reqComp);
                    }
                }
            }


            if (notCityOrRegion)
            {
                if (request.getIdentityCard().getCitizenship().getCode() == 0)
                {
                    nativeDocAcc.add(request);
                    if (examsPassed) nativeExPass.add(request);
                    if (enrolled) nativeEnrStage.add(reqComp);
                }
                else
                {
                    foreignDocAcc.add(request);
                    if (examsPassed) foreignExPass.add(request);
                    if (enrolled) foreignEnrStage.add(reqComp);
                }
            }

            if (reqComp instanceof EnrRequestedCompetitionExclusive)
            {
                switch (((EnrRequestedCompetitionExclusive) reqComp).getBenefitCategory().getCode()){
                    case ("007") :
                    case ("010") :
                    {
                        invalidDocAcc.add(request);
                        if (examsPassed) invalidExPass.add(request);
                        if (enrolled) invalidEnrStage.add(reqComp);
                        break;
                    }
                    case ("008") :
                    case ("009") :
                    {
                        orphanDocAcc.add(request);
                        if (examsPassed) orphanExPass.add(request);
                        if (enrolled) orphanEnrStage.add(reqComp);
                        break;
                    }
                }


            }

            if (accessCourses.contains(request.getEntrant()))
            {
                accessCoursesDocAcc.add(request);
                if (examsPassed) accessCoursesExPass.add(request);
                if (enrolled) accessCoursesEnrStage.add(reqComp);
            }
            if (accessDepartments.contains(request.getEntrant()))
            {
                accessDepDocAcc.add(request);
                if (examsPassed) accessDepExPass.add(request);
                if (enrolled) accessDepEnrStage.add(reqComp);
            }
        }

        tTable.add(new String[]
                {
                        "Подано заявлений",
                        String.valueOf(totalDocAcc.size()),
                        String.valueOf(maleDocAcc.size()),
                        String.valueOf(femaleDocAcc.size()),
                        String.valueOf(sooDocAcc.size()),
                        String.valueOf(sooCurrYearDocAcc.size()),
                        String.valueOf(npoDocAcc.size()),
                        String.valueOf(npoCurrYearDocAcc.size()),
                        String.valueOf(spoDocAcc.size()),
                        String.valueOf(spoCurrYearDocAcc.size()),
                        String.valueOf(bachDocAcc.size()),
                        String.valueOf(bachCurrYearDocAcc.size()),
                        String.valueOf(specDocAcc.size()),
                        String.valueOf(specCurrYearDocAcc.size()),
                        String.valueOf(magDocAcc.size()),
                        String.valueOf(magCurrYearDocAcc.size()),
                        String.valueOf(cityDocAcc.size()),
                        String.valueOf(regionDocAcc.size()),
                        String.valueOf(nativeDocAcc.size()),
                        String.valueOf(foreignDocAcc.size()),
                        String.valueOf(orphanDocAcc.size()),
                        String.valueOf(invalidDocAcc.size()),
                        String.valueOf(accessCoursesDocAcc.size()),
                        String.valueOf(accessDepDocAcc.size())
                });


        tTable.add(new String[]
                {
                        "Сдали все ВИ",
                        String.valueOf(totalExPass.size()),
                        String.valueOf(maleExPass.size()),
                        String.valueOf(femaleExPass.size()),
                        String.valueOf(sooExPass.size()),
                        String.valueOf(sooCurrYearExPass.size()),
                        String.valueOf(npoExPass.size()),
                        String.valueOf(npoCurrYearExPass.size()),
                        String.valueOf(spoExPass.size()),
                        String.valueOf(spoCurrYearExPass.size()),
                        String.valueOf(bachExPass.size()),
                        String.valueOf(bachCurrYearExPass.size()),
                        String.valueOf(specExPass.size()),
                        String.valueOf(specCurrYearExPass.size()),
                        String.valueOf(magExPass.size()),
                        String.valueOf(magCurrYearExPass.size()),
                        String.valueOf(cityExPass.size()),
                        String.valueOf(regionExPass.size()),
                        String.valueOf(nativeExPass.size()),
                        String.valueOf(foreignExPass.size()),
                        String.valueOf(orphanExPass.size()),
                        String.valueOf(invalidExPass.size()),
                        String.valueOf(accessCoursesExPass.size()),
                        String.valueOf(accessDepExPass.size())
                });


        tTable.add(new String[]
                {
                        "Зачислено",
                        String.valueOf(totalEnrStage.size()),
                        String.valueOf(maleEnrStage.size()),
                        String.valueOf(femaleEnrStage.size()),
                        String.valueOf(sooEnrStage.size()),
                        String.valueOf(sooCurrYearEnrStage.size()),
                        String.valueOf(npoEnrStage.size()),
                        String.valueOf(npoCurrYearEnrStage.size()),
                        String.valueOf(spoEnrStage.size()),
                        String.valueOf(spoCurrYearEnrStage.size()),
                        String.valueOf(bachEnrStage.size()),
                        String.valueOf(bachCurrYearEnrStage.size()),
                        String.valueOf(specEnrStage.size()),
                        String.valueOf(specCurrYearEnrStage.size()),
                        String.valueOf(magEnrStage.size()),
                        String.valueOf(magCurrYearEnrStage.size()),
                        String.valueOf(cityEnrStage.size()),
                        String.valueOf(regionEnrStage.size()),
                        String.valueOf(nativeEnrStage.size()),
                        String.valueOf(foreignEnrStage.size()),
                        String.valueOf(orphanEnrStage.size()),
                        String.valueOf(invalidEnrStage.size()),
                        String.valueOf(accessCoursesEnrStage.size()),
                        String.valueOf(accessDepEnrStage.size())
                });
        return tTable;
    }
}
