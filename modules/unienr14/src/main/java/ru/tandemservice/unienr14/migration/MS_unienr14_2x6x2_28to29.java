package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x2_28to29 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrEntrantForeignRequest

		// создана новая сущность
		{
            if(!tool.tableExists("enr14_entrant_foreign_req_t"))
            {
                // создать таблицу
                DBTable dbt = new DBTable("enr14_entrant_foreign_req_t",
                        new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                        new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                        new DBColumn("entrant_id", DBType.LONG).setNullable(false),
                        new DBColumn("competition_id", DBType.LONG).setNullable(false),
                        new DBColumn("regnumber_p", DBType.createVarchar(255)).setNullable(false),
                        new DBColumn("regdate_p", DBType.TIMESTAMP).setNullable(false),
                        new DBColumn("enrollmentdate_p", DBType.DATE),
                        new DBColumn("comment_p", DBType.createVarchar(2048)),
                        new DBColumn("identitycard_id", DBType.LONG).setNullable(false),
                        new DBColumn("edudocument_id", DBType.LONG).setNullable(false)
                );
                tool.createTable(dbt);
            }

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("enrEntrantForeignRequest");

		}


    }
}