/* $Id: EducationPrintBlock.java 7962 2016-05-25 10:31:58Z ezvereva $ */
package ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.print.education;

import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.person.base.bo.PersonEduDocument.PersonEduDocumentManager;
import org.tandemframework.shared.person.base.bo.ReportPerson.ui.Add.ReportPersonAddUI;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.*;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.print.ReportPrintPathColumn;
import org.tandemframework.shared.person.base.entity.*;
import org.tandemframework.shared.person.catalog.entity.BirthCountry;
import org.tandemframework.shared.person.catalog.entity.codes.EduLevelCodes;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.EnrReportPersonAdd;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.EnrReportPersonAddUI;

import java.util.*;

/**
 * @author Vasily Zhukov
 * @since 14.02.2011
 */
@SuppressWarnings("deprecation")
public class EnrEducationPrintBlock extends org.tandemframework.shared.person.base.bo.ReportPerson.ui.Add.print.education.EducationPrintBlock implements IReportPrintBlock
{

    @Override
    public String getCheckJS()
    {
        List<String> ids = new ArrayList<>();
        for (int i = 0; i <= 10; i++)
            ids.add("enrChEducation" + i);
        return ReportJavaScriptUtil.getCheckJS(ids);
    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        modify(dql, printInfo, EnrReportPersonAddUI.ENTRANT_SCHEET);
    }
}
