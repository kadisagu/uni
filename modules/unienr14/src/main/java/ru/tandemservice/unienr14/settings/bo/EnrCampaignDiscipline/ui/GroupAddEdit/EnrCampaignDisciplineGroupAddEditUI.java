/**
 *$Id: EnrCampaignDisciplineGroupAddEditUI.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.settings.bo.EnrCampaignDiscipline.ui.GroupAddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unienr14.catalog.entity.EnrDiscipline;
import ru.tandemservice.unienr14.settings.bo.EnrCampaignDiscipline.EnrCampaignDisciplineManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDisciplineGroup;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDisciplineGroupElement;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 09.04.13
 */
@Input({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "entityHolder.id")
})
public class EnrCampaignDisciplineGroupAddEditUI extends UIPresenter
{
    private EntityHolder<EnrCampaignDisciplineGroup> _entityHolder = new EntityHolder<>(new EnrCampaignDisciplineGroup());

    private List<EnrCampaignDiscipline> _campDisciplineList;

    @Override
    public void onComponentRefresh()
    {
        _entityHolder.refresh();

        if (isAddForm())
            _entityHolder.getValue().setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
        else
            _campDisciplineList = DataAccessServices.dao().getList(
                    new DQLSelectBuilder().fromEntity(EnrCampaignDiscipline.class, "cd").column(property("cd"))
                            .where(eq(property(EnrCampaignDiscipline.enrollmentCampaign().fromAlias("cd")), value(_entityHolder.getValue().getEnrollmentCampaign())))
                            .where(in(property(EnrCampaignDiscipline.id().fromAlias("cd")),
                                    new DQLSelectBuilder().fromEntity(EnrCampaignDisciplineGroupElement.class, "e").column(property(EnrCampaignDisciplineGroupElement.discipline().id().fromAlias("e")))
                                            .where(eq(property(EnrCampaignDisciplineGroupElement.group().fromAlias("e")), value(_entityHolder.getValue())))
                                            .buildQuery())));
    }

    public void onClickApply()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(_entityHolder.getValue().getEnrollmentCampaign());

        EnrCampaignDisciplineManager.instance().groupDao().saveOrUpdateDiscGroup(_entityHolder.getValue(), _campDisciplineList);

        deactivate();
    }

    public boolean isAddForm()
    {
        return _entityHolder.getId() == null;
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, _entityHolder.getValue().getEnrollmentCampaign());
    }

    // Getters & Setters

    public EntityHolder<EnrCampaignDisciplineGroup> getEntityHolder()
    {
        return _entityHolder;
    }

    public void setEntityHolder(EntityHolder<EnrCampaignDisciplineGroup> entityHolder)
    {
        _entityHolder = entityHolder;
    }

    public List<EnrCampaignDiscipline> getCampDisciplineList()
    {
        return _campDisciplineList;
    }

    public void setCampDisciplineList(List<EnrCampaignDiscipline> campDisciplineList)
    {
        _campDisciplineList = campDisciplineList;
    }
}
