/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EnrollmentCampaignPassSummaryAdd;/**
 * @author rsizonenko
 * @since 10.06.2014
 */

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilter;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilterType;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterFormConfig;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.report.bo.EnrReport.EnrReportManager;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportDateSelector;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportStageSelector;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.ui.Pub.EnrReportBasePub;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.EnrReportPersonAdd;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.EnrEnrollmentCommissionManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.Arrays;
import java.util.List;

public class EnrReportEnrollmentCampaignPassSummaryAddUI extends UIPresenter {


    // Fields
    private EnrReportDateSelector dateSelector = new EnrReportDateSelector();
    private EnrEnrollmentCampaign enrollmentCampaign;

    private boolean parallelActive;
    private IdentifiableWrapper parallel;
    private List<IdentifiableWrapper> parallelList;

    private IdentifiableWrapper stage;
    private List<IdentifiableWrapper> stageList;

    private EnrReportStageSelector stageSelector = new EnrReportStageSelector();

    private IdentifiableWrapper formingType;
    private List<IdentifiableWrapper> formingTypeList;


    private boolean filterByEnrollmentCommission = false;
    private boolean disabledEnrollmentCommissionCheckbox = false;
    private List<EnrEnrollmentCommission> enrollmentCommissionList;

    private boolean firstPriorityOnly;

    boolean skipEmpty;

    // From UI
    @Override
    public void onComponentRefresh() {
        setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
        setDisabledEnrollmentCommissionCheckbox(!EnrEnrollmentCommissionManager.instance().permissionDao().hasGlobalPermissionForEnrCommission(UserContext.getInstance().getPrincipalContext()));

        if(isDisabledEnrollmentCommissionCheckbox())
            setFilterByEnrollmentCommission(true);

        setParallelList(Arrays.asList(new IdentifiableWrapper(0L, "Не учитывать выбранные параллельно условия поступления"),
                                      new IdentifiableWrapper(1L, "Включать в отчет только выбранные параллельно условия поступления")));

        setFormingTypeList(Arrays.asList(new IdentifiableWrapper(0L, "По направлениям, профессиям, специальностям"),
                                         new IdentifiableWrapper(1L, "По наборам образовательных программ для приема")));

        configUtil(getCompetitionFilterAddon());
    }

    private void validate()
    {
        if (dateSelector.getDateFrom().after(dateSelector.getDateTo()))
            _uiSupport.error("Дата, указанная в параметре \"Заявления с\" не должна быть позже даты в параметре \"Заявления по\".", "dateFrom");

        if(getUserContext().getErrorCollector().hasErrors())
            throw new ApplicationException();
    }
    // Listeners

    public void onClickApply() {
        validate();
        Long reportId = EnrReportManager.instance().enrollmentCampaignPassSummaryDao().createReport(this);
        deactivate();
        _uiActivation.asDesktopRoot(EnrReportBasePub.class)
                .parameter(PUBLISHER_ID, reportId)
                .activate();
    }

    public void onChangeEnrollmentCampaign()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
        final EnrCompetitionFilterAddon entrantRequestUtil = getCompetitionFilterAddon();
        configUtilWhere(entrantRequestUtil);
        dateSelector.refreshDates(getEnrollmentCampaign());
    }

    // Util

    public boolean isParallelOnly() {
        return isParallelActive() && 1L == getParallel().getId();
    }

    public boolean isSkipParallel() {
        return isParallelActive() && 0L == getParallel().getId();
    }

    public boolean formingByProgramSet()
    {
        return (1L == formingType.getId());
    }

    public boolean formingByProgramSubject()
    {
        return (0L == formingType.getId());
    }

    private void configUtil(EnrCompetitionFilterAddon util)
    {

        util
                .configDoubleWidthFilters(false)
                .configUseEnableCheckbox(true)
                .configSettings(getSettingsKey());

        util.clearFilterItems();

        util
                .addFilterItem(EnrCompetitionFilterAddon.REQUEST_TYPE, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.COMPENSATION_TYPE, new CommonFilterFormConfig(true, false, true, false, true, true))
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_FORM, new CommonFilterFormConfig(true, false, true, false, true, true))
                .addFilterItem(EnrCompetitionFilterAddon.ENR_ORG_UNIT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.FORMATIVE_ORG_UNIT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SUBJECT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.EDU_PROGRAM, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SET, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG);

        configUtilWhere(util);
    }

    private void configUtilWhere(EnrCompetitionFilterAddon util)
    {
        util.clearWhereFilter();
        util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EnrCompetition.programSetOrgUnit().orgUnit().enrollmentCampaign(), getEnrollmentCampaign()));
    }

    // Getters-setters

    public EnrCompetitionFilterAddon getCompetitionFilterAddon()
    {
        return (EnrCompetitionFilterAddon) getConfig().getAddon(EnrReportPersonAdd.COMPETITION_FILTERS_ENTRANT_REQUEST);
    }

    public EnrReportDateSelector getDateSelector() {
        return dateSelector;
    }

    public boolean isParallelActive() {
        return parallelActive;
    }

    public void setParallelActive(boolean parallelActive) {
        this.parallelActive = parallelActive;
    }

    public IdentifiableWrapper getParallel() {
        return parallel;
    }

    public void setParallel(IdentifiableWrapper parallel) {
        this.parallel = parallel;
    }

    public List<IdentifiableWrapper> getParallelList() {
        return parallelList;
    }

    public void setParallelList(List<IdentifiableWrapper> parallelList) {
        this.parallelList = parallelList;
    }

    public IdentifiableWrapper getStage() {
        return stageSelector.getStage();
    }

    public void setStage(IdentifiableWrapper stage) {
        stageSelector.setStage(stage);
    }

    public List<IdentifiableWrapper> getStageList() {
        return stageSelector.getStageList();
    }

    public void setStageList(List<IdentifiableWrapper> stageList) {
        stageSelector.setStageList(stageList);
    }

    public IdentifiableWrapper getFormingType() {
        return formingType;
    }

    public void setFormingType(IdentifiableWrapper formingType) {
        this.formingType = formingType;
    }

    public List<IdentifiableWrapper> getFormingTypeList() {
        return formingTypeList;
    }

    public void setFormingTypeList(List<IdentifiableWrapper> formingTypeList) {
        this.formingTypeList = formingTypeList;
    }

    public boolean isSkipEmpty() {
        return skipEmpty;
    }

    public void setSkipEmpty(boolean skipEmpty) {
        this.skipEmpty = skipEmpty;
    }


    public boolean isFilterByEnrollmentCommission() {
        return filterByEnrollmentCommission;
    }

    public void setFilterByEnrollmentCommission(boolean filterByEnrollmentCommission) {
        this.filterByEnrollmentCommission = filterByEnrollmentCommission;
    }

    public List<EnrEnrollmentCommission> getEnrollmentCommissionList() {
        return enrollmentCommissionList;
    }

    public void setEnrollmentCommissionList(List<EnrEnrollmentCommission> enrollmentCommissionList) {
        this.enrollmentCommissionList = enrollmentCommissionList;
    }

    public boolean isFirstPriorityOnly() {
        return firstPriorityOnly;
    }

    public void setFirstPriorityOnly(boolean firstPriorityOnly) {
        this.firstPriorityOnly = firstPriorityOnly;
    }

    public EnrReportStageSelector getStageSelector() {
        return stageSelector;
    }

    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        this.enrollmentCampaign = enrollmentCampaign;
    }

    public boolean isDisabledEnrollmentCommissionCheckbox()
    {
        return disabledEnrollmentCommissionCheckbox;
    }

    public void setDisabledEnrollmentCommissionCheckbox(boolean disabledEnrollmentCommissionCheckbox)
    {
        this.disabledEnrollmentCommissionCheckbox = disabledEnrollmentCommissionCheckbox;
    }
}