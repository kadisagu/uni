/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReportBase.ui.Pub;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.uni.base.bo.UniReport.util.UniReportUtils;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrReport;
import ru.tandemservice.unienr14.report.entity.EnrReportEntrantsAgeDistribution;

import java.util.Date;

/**
 * @author oleyba
 * @since 5/14/14
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "holder.id")
})
public class EnrReportBasePubUI extends UIPresenter
{
    private EntityHolder<IEnrReport> holder = new EntityHolder<>();
    private IEntityMeta entityMeta;

    private String property;

    @Override
    public void onComponentRefresh()
    {
        getHolder().refresh();
        setEntityMeta(EntityRuntime.getMeta(getReport().getClass()));
    }

    public void onClickPrint()
    {
        BusinessComponentUtils.downloadDocument(UniReportUtils.createRenderer(getReport().getId()), true);
    }

    // presenter

    public String getPropertyValue() {
        Object propertyValue = getReport().getProperty(getProperty());
        if (null == propertyValue)
            return null;
        if (propertyValue instanceof String)
            return (String) propertyValue;
        if (propertyValue instanceof ITitled)
            return ((ITitled) propertyValue).getTitle();
        if (propertyValue instanceof Date)
            return DateFormatter.DEFAULT_DATE_FORMATTER.format((Date) propertyValue);
        throw new IllegalArgumentException();
    }

    public boolean isShowProperty() { return !StringUtils.isEmpty(getPropertyValue()); }
    public String getPropertyName() { return getEntityMeta().getProperty(getProperty()).getTitle(); }

    // getters and setters

    public IEnrReport getReport()
    {
        return getHolder().getValue();
    }

    public EntityHolder<IEnrReport> getHolder()
    {
        return holder;
    }

    public String getProperty()
    {
        return property;
    }

    public void setProperty(String property)
    {
        this.property = property;
    }

    public IEntityMeta getEntityMeta()
    {
        return entityMeta;
    }

    public void setEntityMeta(IEntityMeta entityMeta)
    {
        this.entityMeta = entityMeta;
    }

}
