/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.PublisherActivator;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.EnrEnrollmentStepManager;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.ui.Add.EnrEnrollmentStepAdd;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

/**
 * @author oleyba
 * @since 12/17/13
 */
public class EnrEnrollmentStepListUI extends UIPresenter
{
    private EnrEnrollmentCampaign _enrollmentCampaign;

    @Override
    public void onComponentRefresh() {
        setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, getEnrollmentCampaign());
    }

    public void onClickAdd() {
        getEnrollmentCampaign().checkOpen();
        _uiActivation.asRegionDialog(EnrEnrollmentStepAdd.class).parameter(PublisherActivator.PUBLISHER_ID_KEY, getEnrollmentCampaign().getId()).activate();
    }

    public void onClickDelete() {
        EnrEnrollmentStepManager.instance().dao().doDelete(getListenerParameterAsLong());
    }

    public void onEnrCampaignRefresh()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
    }

    public boolean isNothingSelected() {
        return getEnrollmentCampaign() == null;
    }

    // getters and setters


    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }
}