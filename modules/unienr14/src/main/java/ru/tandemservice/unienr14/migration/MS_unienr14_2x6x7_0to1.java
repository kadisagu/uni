package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x7_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.6")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrEnrollmentMinisteryExtract

        // создана новая сущность
        {
            // создать таблицу
            if(!tool.tableExists("enr14_order_enr_min_extract_t"))
            {
                DBTable dbt = new DBTable("enr14_order_enr_min_extract_t",
                        new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                        new DBColumn("entrantrequest_id", DBType.LONG).setNullable(false),
                        new DBColumn("student_id", DBType.LONG)
                );
                tool.createTable(dbt);
            }

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("enrEnrollmentMinisteryExtract");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrEnrollmentMinisteryParagraph

        // создана новая сущность
        {
            // создать таблицу
            if(!tool.tableExists("enr14_order_enr_min_par_t"))
            {
                DBTable dbt = new DBTable("enr14_order_enr_min_par_t",
                        new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                        new DBColumn("competitiontype_id", DBType.LONG).setNullable(false),
                        new DBColumn("enrorgunit_id", DBType.LONG).setNullable(false),
                        new DBColumn("formativeorgunit_id", DBType.LONG).setNullable(false),
                        new DBColumn("programform_id", DBType.LONG).setNullable(false),
                        new DBColumn("programsubject_id", DBType.LONG).setNullable(false)
                );
                tool.createTable(dbt);
            }

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("enrEnrollmentMinisteryParagraph");

        }


    }
}