/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.select;

import org.tandemframework.core.debug.Debug;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItem;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author oleyba
 * @since 4/2/15
 */
public abstract class EnrEnrollmentStepBaseContext extends EnrSelectionContext
{
    private final EnrEnrollmentStep step;
    public EnrEnrollmentStep getStep() { return this.step; }

    protected EnrEnrollmentStepBaseContext(final EnrEnrollmentStep step, final boolean debug) {
        this.debug = debug;
        this.step = step;
        this.targetAdmissionCompetition = step.getEnrollmentCampaign().getSettings().isTargetAdmissionCompetition();
    }

    // действие: указать перечень абитуриентов
    public Map<EnrEnrollmentStepItem, EnrSelectionItem> setupStepItemList(final List<EnrEnrollmentStepItem> stepItemList)
    {
        Debug.begin("setupStepItemList(" + stepItemList.size() + ")");
        final Map<EnrEnrollmentStepItem, EnrSelectionItem> result = new LinkedHashMap<>();
        for (final EnrEnrollmentStepItem stepItem: stepItemList)
        {
            final EnrSelectionItem selectionItem = this.getGroup(stepItem).add(stepItem);
            if (null != result.put(stepItem, selectionItem)) {
                throw new IllegalStateException("duplicate-step-item: stepItemId=" + stepItem.getId());
            }

            // http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20686888
            // признак «включен в конкурсный список» должен быть «да» (здесь уже проверено, что все ВИ сданы)
            if (!stepItem.isIncluded()) {
                selectionItem.skip(new EnrSelectionSkipReason.NotIncluded());
            }

            // http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20686888
            // признак «Забрал документы» у заявления выбранного конкурса должен быть «нет»
            if (stepItem.isEntrantRequestTakeAwayDocuments()) {
                selectionItem.skip(new EnrSelectionSkipReason.TakeAwayDocument());
            }

            // http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20686888
            // признак «Архивный» у абитуриента должен быть «нет»
            if (stepItem.isEntrantArchived()) {
                selectionItem.skip(new EnrSelectionSkipReason.Archival());
            }
        }
        Debug.end();
        return result;
    }
}
