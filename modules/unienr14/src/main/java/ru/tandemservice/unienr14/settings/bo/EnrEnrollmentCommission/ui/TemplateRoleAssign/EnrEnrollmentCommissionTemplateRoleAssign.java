/* $Id: SecTemplateRoleAssign.java 6520 2015-05-18 12:42:03Z oleyba $ */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.ui.TemplateRoleAssign;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.sec.bo.Sec.ui.RoleAssign.SecRoleAssignUI;
import org.tandemframework.shared.organization.sec.entity.RoleConfig;
import org.tandemframework.shared.organization.sec.entity.RoleConfigTemplate;
import org.tandemframework.shared.organization.sec.entity.RoleConfigTemplateOrgUnit;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.unienr14.catalog.entity.codes.LocalRoleScopeCodes;
import ru.tandemservice.unienr14.sec.entity.RoleAssignmentTemplateEnrCommission;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.EnrEnrollmentCommissionManager;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Vasily Zhukov
 * @since 09.11.2011
 */
@Configuration
public class EnrEnrollmentCommissionTemplateRoleAssign extends BusinessComponentManager
{
    public static final String LOCAL_ROLE_DS = "localRoleDS";
    public static final String DS_ENROLLMENT_COMMISSION = EnrEnrollmentCommissionManager.DS_ENROLLMENT_COMMISSION;

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
            .addDataSource(CommonBaseStaticSelectDataSource.selectDS(DS_ENROLLMENT_COMMISSION, getName(), EnrEnrollmentCommission.enabledSelectDSHandler(getName())))
            .addDataSource(selectDS(LOCAL_ROLE_DS, localRoleComboDSHandler()).addColumn(RoleConfigTemplateOrgUnit.role().title().s()))
            .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler localRoleComboDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), RoleConfigTemplate.class) {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(notIn(property(RoleConfigTemplate.id().fromAlias(alias)), new DQLSelectBuilder()
                    .fromEntity(RoleAssignmentTemplateEnrCommission.class, "r")
                    .column(property(RoleAssignmentTemplateEnrCommission.roleConfig().id().fromAlias("r")))
                    .where(eq(property(RoleAssignmentTemplateEnrCommission.principalContext().id().fromAlias("r")), value(context.<Long>get(SecRoleAssignUI.BIND_PRINCIPAL_CONTEXT_ID))))
                    .where(eq(property(RoleAssignmentTemplateEnrCommission.enrollmentCommission().fromAlias("r")), value(context.<IEntity>get("enrollmentCommission"))))
                    .buildQuery()
                ));
            }
        }
            .where(RoleConfigTemplate.role().active(), Boolean.TRUE)
            .where(RoleConfigTemplate.localRoleScope().code(), (Object) LocalRoleScopeCodes.ENROLLMENT_COMMISSION)
            .filter(RoleConfig.role().title())
            .order(RoleConfigTemplate.role().title());
    }
}
