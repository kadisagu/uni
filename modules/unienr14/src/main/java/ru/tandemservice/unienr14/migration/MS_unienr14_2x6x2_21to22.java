package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.ISQLTranslator;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLSelectQuery;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x2_21to22 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.15"),
                        new ScriptDependency("org.tandemframework.shared", "1.6.2")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrTargetAdmissionPlan

        // создано обязательное свойство planSOO
        if(tool.tableExists("enr14_ta_plan_t") && !tool.columnExists("enr14_ta_plan_t", "plansoo_p"))
        {
            // создать колонку
            tool.createColumn("enr14_ta_plan_t", new DBColumn("plansoo_p", DBType.INTEGER));

            // задать значение по умолчанию
            java.lang.Integer defaultPlanSOO = 0;
            tool.executeUpdate("update enr14_ta_plan_t set plansoo_p=? where plansoo_p is null", defaultPlanSOO);

            // сделать колонку NOT NULL
            tool.setColumnNullable("enr14_ta_plan_t", "plansoo_p", false);

        }

        // создано обязательное свойство planPO
        if(tool.tableExists("enr14_ta_plan_t") && !tool.columnExists("enr14_ta_plan_t", "planpo_p"))
        {
            // создать колонку
            tool.createColumn("enr14_ta_plan_t", new DBColumn("planpo_p", DBType.INTEGER));

            // задать значение по умолчанию
            java.lang.Integer defaultPlanPO = 0;
            tool.executeUpdate("update enr14_ta_plan_t set planpo_p=? where planpo_p is null", defaultPlanPO);

            // сделать колонку NOT NULL
            tool.setColumnNullable("enr14_ta_plan_t", "planpo_p", false);

        }

        // миграция отрицательных значений свойства plan
        if(tool.columnExists("enr14_ta_plan_t", "plan_p"))
        {
            ISQLTranslator translator = tool.getDialect().getSQLTranslator();

            SQLSelectQuery selectQuery = new SQLSelectQuery();
            selectQuery.from(SQLFrom.table("enr14_ta_plan_t", "tap"));
            selectQuery.column("tap.id");
            selectQuery.where("tap.plan_p < 0");

            tool.executeUpdate("update enr14_ta_plan_t set plan_p=? where id in (" + translator.toSql(selectQuery) + ")", 0);
        }
    }
}