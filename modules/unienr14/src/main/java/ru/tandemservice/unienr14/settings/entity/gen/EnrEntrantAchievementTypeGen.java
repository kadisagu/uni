package ru.tandemservice.unienr14.settings.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantAchievementKind;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Вид индивидуального достижения в ПК
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrEntrantAchievementTypeGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementType";
    public static final String ENTITY_NAME = "enrEntrantAchievementType";
    public static final int VERSION_HASH = -129176899;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String L_ACHIEVEMENT_KIND = "achievementKind";
    public static final String P_MARKED = "marked";
    public static final String P_FOR_REQUEST = "forRequest";
    public static final String P_ACHIEVEMENT_MARK_AS_LONG = "achievementMarkAsLong";
    public static final String P_ACHIEVEMENT_MARK = "achievementMark";

    private EnrEnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private EnrEntrantAchievementKind _achievementKind;     // Вид индивидуального достижения
    private boolean _marked;     // Оценивается
    private boolean _forRequest = false;     // Учитывается в рамках заявления
    private long _achievementMarkAsLong;     // Балл (макс. балл) за достижение

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Вид индивидуального достижения. Свойство не может быть null.
     */
    @NotNull
    public EnrEntrantAchievementKind getAchievementKind()
    {
        return _achievementKind;
    }

    /**
     * @param achievementKind Вид индивидуального достижения. Свойство не может быть null.
     */
    public void setAchievementKind(EnrEntrantAchievementKind achievementKind)
    {
        dirty(_achievementKind, achievementKind);
        _achievementKind = achievementKind;
    }

    /**
     * @return Оценивается. Свойство не может быть null.
     */
    @NotNull
    public boolean isMarked()
    {
        return _marked;
    }

    /**
     * @param marked Оценивается. Свойство не может быть null.
     */
    public void setMarked(boolean marked)
    {
        dirty(_marked, marked);
        _marked = marked;
    }

    /**
     * @return Учитывается в рамках заявления. Свойство не может быть null.
     */
    @NotNull
    public boolean isForRequest()
    {
        return _forRequest;
    }

    /**
     * @param forRequest Учитывается в рамках заявления. Свойство не может быть null.
     */
    public void setForRequest(boolean forRequest)
    {
        dirty(_forRequest, forRequest);
        _forRequest = forRequest;
    }

    /**
     * Хранится со смещением в три знака для дробной части.
     * Если достижение оценивается, то поле используется в качестве ограничения максимального балла за достижение, если не оценивается — в качестве балла за достижение.
     *
     * @return Балл (макс. балл) за достижение. Свойство не может быть null.
     */
    @NotNull
    public long getAchievementMarkAsLong()
    {
        return _achievementMarkAsLong;
    }

    /**
     * @param achievementMarkAsLong Балл (макс. балл) за достижение. Свойство не может быть null.
     */
    public void setAchievementMarkAsLong(long achievementMarkAsLong)
    {
        dirty(_achievementMarkAsLong, achievementMarkAsLong);
        _achievementMarkAsLong = achievementMarkAsLong;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrEntrantAchievementTypeGen)
        {
            setEnrollmentCampaign(((EnrEntrantAchievementType)another).getEnrollmentCampaign());
            setAchievementKind(((EnrEntrantAchievementType)another).getAchievementKind());
            setMarked(((EnrEntrantAchievementType)another).isMarked());
            setForRequest(((EnrEntrantAchievementType)another).isForRequest());
            setAchievementMarkAsLong(((EnrEntrantAchievementType)another).getAchievementMarkAsLong());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrEntrantAchievementTypeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrEntrantAchievementType.class;
        }

        public T newInstance()
        {
            return (T) new EnrEntrantAchievementType();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "achievementKind":
                    return obj.getAchievementKind();
                case "marked":
                    return obj.isMarked();
                case "forRequest":
                    return obj.isForRequest();
                case "achievementMarkAsLong":
                    return obj.getAchievementMarkAsLong();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrEnrollmentCampaign) value);
                    return;
                case "achievementKind":
                    obj.setAchievementKind((EnrEntrantAchievementKind) value);
                    return;
                case "marked":
                    obj.setMarked((Boolean) value);
                    return;
                case "forRequest":
                    obj.setForRequest((Boolean) value);
                    return;
                case "achievementMarkAsLong":
                    obj.setAchievementMarkAsLong((Long) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "achievementKind":
                        return true;
                case "marked":
                        return true;
                case "forRequest":
                        return true;
                case "achievementMarkAsLong":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "achievementKind":
                    return true;
                case "marked":
                    return true;
                case "forRequest":
                    return true;
                case "achievementMarkAsLong":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrollmentCampaign":
                    return EnrEnrollmentCampaign.class;
                case "achievementKind":
                    return EnrEntrantAchievementKind.class;
                case "marked":
                    return Boolean.class;
                case "forRequest":
                    return Boolean.class;
                case "achievementMarkAsLong":
                    return Long.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrEntrantAchievementType> _dslPath = new Path<EnrEntrantAchievementType>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrEntrantAchievementType");
    }
            

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementType#getEnrollmentCampaign()
     */
    public static EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Вид индивидуального достижения. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementType#getAchievementKind()
     */
    public static EnrEntrantAchievementKind.Path<EnrEntrantAchievementKind> achievementKind()
    {
        return _dslPath.achievementKind();
    }

    /**
     * @return Оценивается. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementType#isMarked()
     */
    public static PropertyPath<Boolean> marked()
    {
        return _dslPath.marked();
    }

    /**
     * @return Учитывается в рамках заявления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementType#isForRequest()
     */
    public static PropertyPath<Boolean> forRequest()
    {
        return _dslPath.forRequest();
    }

    /**
     * Хранится со смещением в три знака для дробной части.
     * Если достижение оценивается, то поле используется в качестве ограничения максимального балла за достижение, если не оценивается — в качестве балла за достижение.
     *
     * @return Балл (макс. балл) за достижение. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementType#getAchievementMarkAsLong()
     */
    public static PropertyPath<Long> achievementMarkAsLong()
    {
        return _dslPath.achievementMarkAsLong();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementType#getAchievementMark()
     */
    public static SupportedPropertyPath<Double> achievementMark()
    {
        return _dslPath.achievementMark();
    }

    public static class Path<E extends EnrEntrantAchievementType> extends EntityPath<E>
    {
        private EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> _enrollmentCampaign;
        private EnrEntrantAchievementKind.Path<EnrEntrantAchievementKind> _achievementKind;
        private PropertyPath<Boolean> _marked;
        private PropertyPath<Boolean> _forRequest;
        private PropertyPath<Long> _achievementMarkAsLong;
        private SupportedPropertyPath<Double> _achievementMark;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementType#getEnrollmentCampaign()
     */
        public EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Вид индивидуального достижения. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementType#getAchievementKind()
     */
        public EnrEntrantAchievementKind.Path<EnrEntrantAchievementKind> achievementKind()
        {
            if(_achievementKind == null )
                _achievementKind = new EnrEntrantAchievementKind.Path<EnrEntrantAchievementKind>(L_ACHIEVEMENT_KIND, this);
            return _achievementKind;
        }

    /**
     * @return Оценивается. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementType#isMarked()
     */
        public PropertyPath<Boolean> marked()
        {
            if(_marked == null )
                _marked = new PropertyPath<Boolean>(EnrEntrantAchievementTypeGen.P_MARKED, this);
            return _marked;
        }

    /**
     * @return Учитывается в рамках заявления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementType#isForRequest()
     */
        public PropertyPath<Boolean> forRequest()
        {
            if(_forRequest == null )
                _forRequest = new PropertyPath<Boolean>(EnrEntrantAchievementTypeGen.P_FOR_REQUEST, this);
            return _forRequest;
        }

    /**
     * Хранится со смещением в три знака для дробной части.
     * Если достижение оценивается, то поле используется в качестве ограничения максимального балла за достижение, если не оценивается — в качестве балла за достижение.
     *
     * @return Балл (макс. балл) за достижение. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementType#getAchievementMarkAsLong()
     */
        public PropertyPath<Long> achievementMarkAsLong()
        {
            if(_achievementMarkAsLong == null )
                _achievementMarkAsLong = new PropertyPath<Long>(EnrEntrantAchievementTypeGen.P_ACHIEVEMENT_MARK_AS_LONG, this);
            return _achievementMarkAsLong;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementType#getAchievementMark()
     */
        public SupportedPropertyPath<Double> achievementMark()
        {
            if(_achievementMark == null )
                _achievementMark = new SupportedPropertyPath<Double>(EnrEntrantAchievementTypeGen.P_ACHIEVEMENT_MARK, this);
            return _achievementMark;
        }

        public Class getEntityClass()
        {
            return EnrEntrantAchievementType.class;
        }

        public String getEntityName()
        {
            return "enrEntrantAchievementType";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract Double getAchievementMark();
}
