/* $Id:$ */
package ru.tandemservice.unienr14.exams.bo.EnrSchedule.ui.ByRoom;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.common.IIdentifiable;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.list.column.IMergeRowIdResolver;
import org.tandemframework.core.view.list.column.IStyleResolver;
import org.tandemframework.core.view.util.Icon;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.exams.bo.EnrSchedule.logic.EnrScheduleByRoomEventSearchDSHandler;
import ru.tandemservice.unienr14.exams.entity.EnrExamScheduleEvent;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.logic.EnrOrgUnitBaseDSHandler;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignExamRoom;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 5/16/13
 */
@Configuration
public class EnrScheduleByRoom extends BusinessComponentManager
{
    public static final String BIND_RESPONSIBLE_ORG_UNIT = "responsibleOrgUnit";

    public static final String ORG_UNIT_DS = "orgUnitDS";
    public static final String EXAM_ROOM_SELECT_DS = "examRoomSelectDS";
    public static final String SCHEDULE_EVENT_SEARCH_DS = "scheduleEventSearchDS";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
                .addDataSource(selectDS(ORG_UNIT_DS, orgUnitDSHandler()).addColumn(EnrOrgUnit.institutionOrgUnit().orgUnit().shortTitleWithTopEmphasized().s()))
                .addDataSource(selectDS(EXAM_ROOM_SELECT_DS, examRoomSelectDSHandler()).addColumn(EnrCampaignExamRoom.place().titleWithLocation().s()))
                .addDataSource(searchListDS(SCHEDULE_EVENT_SEARCH_DS, scheduleEventSearchDSColumns(), scheduleEventSearchDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint scheduleEventSearchDSColumns()
    {
        final IMergeRowIdResolver dateMerge = entity -> {
            EnrExamScheduleEvent scheduleEvent = ((DataWrapper) entity).getWrapped();
            return String.valueOf(CoreDateUtils.getDayFirstTimeMoment(scheduleEvent.getScheduleEvent().getDurationBegin()).getTime());
        };

        final IStyleResolver roomOccupColorStyleResolver = rowEntity -> {
            DataWrapper wrapper = (DataWrapper) rowEntity;
            final boolean excess = (boolean) wrapper.getProperty(EnrScheduleByRoomEventSearchDSHandler.V_PROP_OCCUPIED_EXCESS);
            if (excess)
                return "background-color:#ffcccc";
            else
                return null;
        };
        return columnListExtPointBuilder(SCHEDULE_EVENT_SEARCH_DS)
                .addColumn(textColumn("eventDate", EnrExamScheduleEvent.scheduleEvent().dateWithDayOfWeek()).merger(dateMerge))
                .addColumn(checkboxColumn("checkbox"))
                .addColumn(textColumn("eventPeriod", EnrExamScheduleEvent.scheduleEvent().timePeriodStr()))
                .addColumn(textColumn("examRoom", EnrExamScheduleEvent.examRoom().placeLocationWithSize()))
                .addColumn(textColumn("discipline", EnrScheduleByRoomEventSearchDSHandler.V_PROP_DISCIPLINE))
                .addColumn(textColumn("commission", EnrExamScheduleEvent.commission()))
                .addColumn(textColumn("examGroups", EnrScheduleByRoomEventSearchDSHandler.V_PROP_EXAM_GROUPS))
                .addColumn(textColumn("roomOccupied", EnrScheduleByRoomEventSearchDSHandler.V_PROP_OCCUPIED_STR).styleResolver(roomOccupColorStyleResolver))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, new Icon(EDIT_COLUMN_NAME), "onClickEditEvent").permissionKey("enr14ScheduleByRoomEditEvent").disabled(EnrScheduleByRoomEventSearchDSHandler.V_PROP_DISABLE_EDIT))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, new Icon(DELETE_COLUMN_NAME), "onClickDeleteEvent").alert(new FormattedMessage("scheduleEventSearchDS.delete.alert", EnrExamScheduleEvent.scheduleEvent().timePeriodStr().s(), EnrExamScheduleEvent.scheduleEvent().dateWithDayOfWeek().s())).permissionKey("enr14ScheduleByRoomDeleteEvent"))
                .addColumn(actionColumn("addEvent", new Icon("add", "ui.addEvent"), "onClickAddEvent").merger(dateMerge).permissionKey("enr14ScheduleByRoomAddEvent"))
                .create();
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> scheduleEventSearchDSHandler()
    {
        return new EnrScheduleByRoomEventSearchDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> examRoomSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrCampaignExamRoom.class)
                .where(EnrCampaignExamRoom.responsibleOrgUnit(), BIND_RESPONSIBLE_ORG_UNIT)
                .where(EnrCampaignExamRoom.enrollmentCampaign(), EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN)
                .order(EnrCampaignExamRoom.place().title())
                .filter(EnrCampaignExamRoom.place().title())
                .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler orgUnitDSHandler()
    {
        return new EnrOrgUnitBaseDSHandler(getName())
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                DQLSelectBuilder responsibleOUBuilder = new DQLSelectBuilder()
                    .fromEntity(EnrCampaignExamRoom.class, "b")
                    .column(property("b", EnrCampaignExamRoom.responsibleOrgUnit().id()))
                    .where(eq(property("b", EnrCampaignExamRoom.enrollmentCampaign()), value(context.<IIdentifiable>get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN))));

                dql.where(in(property(alias, EnrOrgUnit.institutionOrgUnit().orgUnit().id()), responsibleOUBuilder.buildQuery()));
            }
        }
            .where(EnrOrgUnit.enrollmentCampaign(), EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
    }
}
