/* $Id:$ */
package ru.tandemservice.unienr14.extreports.externalView;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.extview.SimpleDQLExternalViewConfig;
import ru.tandemservice.unienr14.enrollment.entity.EnrModelStepItem;

import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author oleyba
 * @since 6/30/15
 */
public class EnrExtViewProvider4Model extends SimpleDQLExternalViewConfig
{
    @Override
    protected DQLSelectBuilder buildDqlQuery()
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(EnrModelStepItem.class, "i");

        column(dql, property("i", EnrModelStepItem.id()), "id").comment("uid элемента");

        column(dql, property("i", EnrModelStepItem.step().id()), "stepId").comment("uid шага");
        column(dql, property("i", EnrModelStepItem.step().enrollmentDate()), "enrollmentDate").comment("дата зачисления (шага)");
        column(dql, property("i", EnrModelStepItem.step().kind().title()), "stepKind").comment("вид шага");
        column(dql, property("i", EnrModelStepItem.step().kind().code()), "stepKindCode").comment("код вида шага");

        column(dql, property("i", EnrModelStepItem.step().model().id()), "modelId").comment("uid модели");
        column(dql, property("i", EnrModelStepItem.step().model().title()), "model").comment("название модели");

        column(dql, property("i", EnrModelStepItem.entity().requestedCompetition().id()), "reqId").comment("uid выбранного конкурса");

        booleanIntColumn(dql, property("i", EnrModelStepItem.originalIn()), "originalIn").comment("сдан оригинал док. об образ. (1 - да, 0 - нет)");
        booleanIntColumn(dql, property("i", EnrModelStepItem.accepted()), "accepted").comment("получено согласие на зачисление (1 - да, 0 - нет)"); // TODO: DEV-9115
        booleanIntColumn(dql, property("i", EnrModelStepItem.included()), "included").comment("включен в конкурсный список (1 - да, 0 - нет)");
        booleanIntColumn(dql, property("i", EnrModelStepItem.enrolled()), "enrolled").comment("зачислен (1 - да, 0 - нет)");
        booleanIntColumn(dql, property("i", EnrModelStepItem.reEnrolled()), "reEnrolled").comment("перезачислен на другой конкурс (1 - да, 0 - нет)");

        return dql;
    }
}
