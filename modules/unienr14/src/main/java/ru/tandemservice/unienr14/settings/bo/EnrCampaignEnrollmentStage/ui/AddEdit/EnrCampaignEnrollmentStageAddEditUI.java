/* $Id:$ */
package ru.tandemservice.unienr14.settings.bo.EnrCampaignEnrollmentStage.ui.AddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.context.ContextLocal;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.settings.bo.EnrCampaignEnrollmentStage.EnrCampaignEnrollmentStageManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignEnrollmentStage;

/**
 * @author oleyba
 * @since 3/24/14
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "stage.id")
})
public class EnrCampaignEnrollmentStageAddEditUI extends UIPresenter
{
    private EnrCampaignEnrollmentStage stage = new EnrCampaignEnrollmentStage();

    @Override
    public void onComponentRefresh()
    {
        if (getStage().getId() != null) {
            setStage(IUniBaseDao.instance.get().getNotNull(EnrCampaignEnrollmentStage.class, getStage().getId()));
        } else {
            getStage().setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
        }
    }

    public void onClickApply() {

        if (isEditForm()) {
            IUniBaseDao.instance.get().saveOrUpdate(getStage());
        } else {
            EnrCampaignEnrollmentStageManager.instance().dao().doSaveEnrollmentStage(
                getStage().getEnrollmentCampaign(),
                getStage().getRequestType(),
                getStage().getCompetitionType(),
                getStage().getProgramForm(),
                getStage().getDate(),
                getStage().isAcceptPeopleResidingInCrimea());
        }
        deactivate();
    }

    public boolean isEditForm() {
        return getStage().getId() != null;
    }

    // getters and setters

    public EnrCampaignEnrollmentStage getStage()
    {
        return stage;
    }

    public void setStage(EnrCampaignEnrollmentStage stage)
    {
        this.stage = stage;
    }
}