package ru.tandemservice.unienr14.request.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unienr14.catalog.entity.EnrBenefitCategory;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionNoExams;
import ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitStatement;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выбранный конкурс без ВИ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrRequestedCompetitionNoExamsGen extends EnrRequestedCompetition
 implements IEnrEntrantBenefitStatement{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionNoExams";
    public static final String ENTITY_NAME = "enrRequestedCompetitionNoExams";
    public static final int VERSION_HASH = 1936809074;
    private static IEntityMeta ENTITY_META;

    public static final String L_BENEFIT_CATEGORY = "benefitCategory";
    public static final String L_PROGRAM_SET_ITEM = "programSetItem";
    public static final String L_ENTRANT = "entrant";

    private EnrBenefitCategory _benefitCategory;     // Категория особого права
    private EnrProgramSetItem _programSetItem;     // Образовательная программа (из набора)
    private EnrEntrant _entrant;     // Абитуриент

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Категория особого права. Свойство не может быть null.
     */
    @NotNull
    public EnrBenefitCategory getBenefitCategory()
    {
        return _benefitCategory;
    }

    /**
     * @param benefitCategory Категория особого права. Свойство не может быть null.
     */
    public void setBenefitCategory(EnrBenefitCategory benefitCategory)
    {
        dirty(_benefitCategory, benefitCategory);
        _benefitCategory = benefitCategory;
    }

    /**
     * @return Образовательная программа (из набора). Свойство не может быть null.
     */
    @NotNull
    public EnrProgramSetItem getProgramSetItem()
    {
        return _programSetItem;
    }

    /**
     * @param programSetItem Образовательная программа (из набора). Свойство не может быть null.
     */
    public void setProgramSetItem(EnrProgramSetItem programSetItem)
    {
        dirty(_programSetItem, programSetItem);
        _programSetItem = programSetItem;
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     *
     * Это формула "request.entrant".
     */
    // @NotNull
    public EnrEntrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant Абитуриент. Свойство не может быть null.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setEntrant(EnrEntrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EnrRequestedCompetitionNoExamsGen)
        {
            setBenefitCategory(((EnrRequestedCompetitionNoExams)another).getBenefitCategory());
            setProgramSetItem(((EnrRequestedCompetitionNoExams)another).getProgramSetItem());
            setEntrant(((EnrRequestedCompetitionNoExams)another).getEntrant());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrRequestedCompetitionNoExamsGen> extends EnrRequestedCompetition.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrRequestedCompetitionNoExams.class;
        }

        public T newInstance()
        {
            return (T) new EnrRequestedCompetitionNoExams();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "benefitCategory":
                    return obj.getBenefitCategory();
                case "programSetItem":
                    return obj.getProgramSetItem();
                case "entrant":
                    return obj.getEntrant();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "benefitCategory":
                    obj.setBenefitCategory((EnrBenefitCategory) value);
                    return;
                case "programSetItem":
                    obj.setProgramSetItem((EnrProgramSetItem) value);
                    return;
                case "entrant":
                    obj.setEntrant((EnrEntrant) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "benefitCategory":
                        return true;
                case "programSetItem":
                        return true;
                case "entrant":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "benefitCategory":
                    return true;
                case "programSetItem":
                    return true;
                case "entrant":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "benefitCategory":
                    return EnrBenefitCategory.class;
                case "programSetItem":
                    return EnrProgramSetItem.class;
                case "entrant":
                    return EnrEntrant.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrRequestedCompetitionNoExams> _dslPath = new Path<EnrRequestedCompetitionNoExams>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrRequestedCompetitionNoExams");
    }
            

    /**
     * @return Категория особого права. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionNoExams#getBenefitCategory()
     */
    public static EnrBenefitCategory.Path<EnrBenefitCategory> benefitCategory()
    {
        return _dslPath.benefitCategory();
    }

    /**
     * @return Образовательная программа (из набора). Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionNoExams#getProgramSetItem()
     */
    public static EnrProgramSetItem.Path<EnrProgramSetItem> programSetItem()
    {
        return _dslPath.programSetItem();
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     *
     * Это формула "request.entrant".
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionNoExams#getEntrant()
     */
    public static EnrEntrant.Path<EnrEntrant> entrant()
    {
        return _dslPath.entrant();
    }

    public static class Path<E extends EnrRequestedCompetitionNoExams> extends EnrRequestedCompetition.Path<E>
    {
        private EnrBenefitCategory.Path<EnrBenefitCategory> _benefitCategory;
        private EnrProgramSetItem.Path<EnrProgramSetItem> _programSetItem;
        private EnrEntrant.Path<EnrEntrant> _entrant;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Категория особого права. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionNoExams#getBenefitCategory()
     */
        public EnrBenefitCategory.Path<EnrBenefitCategory> benefitCategory()
        {
            if(_benefitCategory == null )
                _benefitCategory = new EnrBenefitCategory.Path<EnrBenefitCategory>(L_BENEFIT_CATEGORY, this);
            return _benefitCategory;
        }

    /**
     * @return Образовательная программа (из набора). Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionNoExams#getProgramSetItem()
     */
        public EnrProgramSetItem.Path<EnrProgramSetItem> programSetItem()
        {
            if(_programSetItem == null )
                _programSetItem = new EnrProgramSetItem.Path<EnrProgramSetItem>(L_PROGRAM_SET_ITEM, this);
            return _programSetItem;
        }

    /**
     * @return Абитуриент. Свойство не может быть null.
     *
     * Это формула "request.entrant".
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionNoExams#getEntrant()
     */
        public EnrEntrant.Path<EnrEntrant> entrant()
        {
            if(_entrant == null )
                _entrant = new EnrEntrant.Path<EnrEntrant>(L_ENTRANT, this);
            return _entrant;
        }

        public Class getEntityClass()
        {
            return EnrRequestedCompetitionNoExams.class;
        }

        public String getEntityName()
        {
            return "enrRequestedCompetitionNoExams";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
