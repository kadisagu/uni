package ru.tandemservice.unienr14.migration;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x1_11to12 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
        {
            new ScriptDependency("org.tandemframework", "1.6.15"),
            new ScriptDependency("org.tandemframework.shared", "1.6.1")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        Map<String, Collection<String>> map = new LinkedHashMap<String, Collection<String>>();

        // DEV-4832: элементу «Диплом олимпиады» ставим "да" в поля «Подтверждает особое право - без ВИ» и «Подтверждает особое право - 100 баллов»
        map.put("2"/*EnrEntrantDocumentTypeCodes.OLYMP_DIPLOMA*/, Arrays.<String>asList("benefitNoExams_p", "benefitMaxMark_p"));

        // DEV-4832: элементам «Справка об установлении инвалидности», «Заключение об отсутствии противопоказаний для обучения», «Заключение психолого-медико-педагогической комиссии» - ставим "да" в поле «Подтверждает особое право - квота» и в поле «Подтверждает особое право - преим. право», "нет" во все остальные
        map.put("5"/*EnrEntrantDocumentTypeCodes.DISABLED_CERT*/, Arrays.<String>asList("benefitExclusive_p", "benefitPreference_p"));
        map.put("6"/*EnrEntrantDocumentTypeCodes.CAN_RECEIVE_EDUCATION_CERT*/, Arrays.<String>asList("benefitExclusive_p", "benefitPreference_p"));
        map.put("7"/*EnrEntrantDocumentTypeCodes.PSY_CERT*/, Arrays.<String>asList("benefitExclusive_p", "benefitPreference_p"));

        for (Map.Entry<String, Collection<String>> e: map.entrySet()) {
            for (String field: e.getValue()) {
                tool.executeUpdate("update enr14_camp_entrant_doc_t set "+field+"=? where documentType_id=(select t.id from enr14_c_entrant_doc_type_t t where t.code_p=?)", Boolean.TRUE, e.getKey());
            }
        }

    }
}