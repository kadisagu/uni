/* $Id$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.OrgUnitTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;

/**
 * @author nvankov
 * @since 9/12/14
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "orgUnitId", required = true)
})
public class EnrEntrantOrgUnitTabUI extends UIPresenter
{
    private Long _orgUnitId;
    private OrgUnit _orgUnit;

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }

    public Long getOrgUnitId() { return _orgUnitId; }

    public void setOrgUnitId(Long orgUnitId) { _orgUnitId = orgUnitId; }

    private CommonPostfixPermissionModel _secModel;

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        _secModel = secModel;
    }

    @Override
    public void onComponentRefresh()
    {
        setOrgUnit(DataAccessServices.dao().get(OrgUnit.class, _orgUnitId));
        setSecModel(new OrgUnitSecModel(getOrgUnit()));
    }
}
