/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EnrolledEntrantsDataExportAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportDateSelector;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportStageSelector;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder;
import ru.tandemservice.unienr14.report.entity.EnrReportEntrantDataExport;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 13.08.2014
 */
public class EnrReportEnrolledEntrantsDataExportAddUI extends UIPresenter {

    EnrReportDateSelector dateSelector = new EnrReportDateSelector();
    private EnrEnrollmentCampaign enrollmentCampaign;

    private boolean selectDateFrom = false;
    private boolean selectDateTo = false;

    @Override
    public void onComponentRefresh() {
        setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
        dateSelector.setDateFrom(null);
        dateSelector.setDateTo(null);
    }

    public void onChangeEnrollmentCampaign()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
    }

    public void onClickApply() {

        Long reportId = IUniBaseDao.instance.get().doInTransaction(session -> {
            EnrReportEntrantDataExport report = new EnrReportEntrantDataExport();

            report.setFormingDate(new Date());

            report.setEnrollmentCampaign(getEnrollmentCampaign());
            report.setDateFrom(getDateSelector().getDateFrom());
            report.setDateTo(getDateSelector().getDateTo());

            EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder()
                    .notArchiveOnly()
                    .filter(getEnrollmentCampaign());

            EnrReportStageSelector.enrollmentResultsStageFilter().applyFilter(requestedCompDQL, "reqComp");

            requestedCompDQL
                    .joinEntity("reqComp", DQLJoinType.inner, EnrEnrollmentExtract.class, "extract", eq(property(EnrEnrollmentExtract.entity().fromAlias("extract")), property("reqComp")));

            if (selectDateFrom)
                requestedCompDQL.where(ge(property(EnrEnrollmentExtract.paragraph().order().actionDate().fromAlias("extract")), commonValue(getDateSelector().getDateFrom())));
            if (selectDateTo)
                requestedCompDQL.where(le(property(EnrEnrollmentExtract.paragraph().order().actionDate().fromAlias("extract")), commonValue(getDateSelector().getDateTo())));



            requestedCompDQL.order(property(EnrEntrant.person().identityCard().fullFio().fromAlias(requestedCompDQL.entrant())));

            requestedCompDQL.column(property(EnrEnrollmentExtract.id().fromAlias("extract")));
            List<Long> extracts = requestedCompDQL.createStatement(session).<Long>list();


            requestedCompDQL.resetColumns()
                    .joinEntity("reqComp", DQLJoinType.inner, EnrRatingItem.class, "rating", eq(property(EnrRatingItem.requestedCompetition().fromAlias("rating")), property("reqComp")))
                    .column(property(EnrRatingItem.id().fromAlias("rating")));

            List<Long> ratings = requestedCompDQL.createStatement(session).<Long>list();

            Map<String, Object> scriptResult = CommonManager.instance().scriptDao()
                    .getScriptResult(DataAccessServices.dao().getByCode(EnrScriptItem.class, EnrScriptItemCodes.REPORT_ENROLLED_ENTRANTS_DATA_EXPORT),
                                     "session", session,
                                     "template", new byte[]{}, // Шаблон не используем, делаем заглушку
                                     "extractIds", extracts,
                                     "ratingIds", ratings
                    );

            byte[] content = (byte[]) scriptResult.get(IScriptExecutor.DOCUMENT);
            String filename = (String) scriptResult.get(IScriptExecutor.FILE_NAME);

            if (null == content) {
                throw new ApplicationException("Скрипт печати не вернул содержимое файла. Обратитесь к администратору системы.");
            }
            if (null == filename || !filename.contains(".")) {
                throw new ApplicationException("Скрипт печати вернул некорректное имя файла. Обратитесь к администратору системы.");
            }
            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().fileName(filename).document(content), false);

            return 0L;
        });

        deactivate();
    }


    public EnrReportDateSelector getDateSelector() {
        return dateSelector;
    }

    public boolean isSelectDateFrom() {
        return selectDateFrom;
    }

    public void setSelectDateFrom(boolean selectDateFrom) {
        this.selectDateFrom = selectDateFrom;
    }

    public boolean isSelectDateTo() {
        return selectDateTo;
    }

    public void setSelectDateTo(boolean selectDateTo) {
        this.selectDateTo = selectDateTo;
    }

    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        this.enrollmentCampaign = enrollmentCampaign;
    }
}