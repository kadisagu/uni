/* $Id: PassDisciplineDataParam.java 33636 2014-04-16 04:31:39Z nfedorovskih $ */
package ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.block.passDisciplineData;

import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.*;
import ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroupScheduleEvent;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.EnrReportPersonAddUI;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline;

import java.util.Date;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 02.07.2013
 */
public class PassDisciplineDataParam implements IReportDQLModifier
{
    private IReportParam<EnrCampaignDiscipline> _educationSubject = new ReportParam<>();
    private IReportParam<EnrExamPassForm> _subjectPassForm = new ReportParam<>();
    private IReportParam<Date> _passDate = new ReportParam<>();

    private String entrantAlias(ReportDQL dql)
    {
        // соединяем абитуриента
        String entrantAlias = dql.innerJoinEntity(EnrEntrant.class, EnrEntrant.person());

        // добавляем сортировку
        dql.order(entrantAlias, EnrEntrant.enrollmentCampaign().id());

        return entrantAlias;
    }

    @Override
    public void validateReportParams(ErrorCollector errorCollector)
    {

    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        if (_educationSubject.isActive() || _subjectPassForm.isActive() || _passDate.isActive())
        {
            String nextAlias = dql.nextAlias();

            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrExamPassDiscipline.class, nextAlias)
                    .predicate(DQLPredicateType.distinct)
                    .column(property(EnrExamPassDiscipline.id().fromAlias(nextAlias)))
                    .where(eq(
                            property(EnrExamPassDiscipline.entrant().id().fromAlias(nextAlias)),
                            property(EnrEntrant.id().fromAlias(entrantAlias(dql)))
                    ));

            if (_educationSubject.isActive())
            {
                printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "passDisciplineData.educationSubject", _educationSubject.getData().getTitle());
                builder.where(eq(
                        property(EnrExamPassDiscipline.discipline().id().fromAlias(nextAlias)),
                        value(_educationSubject.getData().getId())
                ));
            }

            if (_subjectPassForm.isActive())
            {
                printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "passDisciplineData.subjectPassForm", _subjectPassForm.getData().getTitle());
                builder.where(eq(
                        property(EnrExamPassDiscipline.passForm().id().fromAlias(nextAlias)),
                        value(_subjectPassForm.getData().getId())
                ));
            }

            if (_passDate.isActive())
            {
                final DQLSelectBuilder eventDQL = new DQLSelectBuilder().fromEntity(EnrExamGroupScheduleEvent.class, "e").column(property(EnrExamGroupScheduleEvent.examGroup().id().fromAlias("e")))
                        .where(inDay(EnrExamGroupScheduleEvent.examScheduleEvent().scheduleEvent().durationBegin().fromAlias("e"), _passDate.getData()));

                printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "passDisciplineData.passDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(_passDate.getData()));
                builder.where(or(
                    in(property(EnrExamPassDiscipline.examGroup().id().fromAlias(nextAlias)), eventDQL.buildQuery()),
                    inDay(EnrExamPassDiscipline.markDate().fromAlias(nextAlias), _passDate.getData())));
            }

            dql.builder.where(exists(builder.buildQuery()));
        }
    }

    public IReportParam<EnrCampaignDiscipline> getEducationSubject()
    {
        return _educationSubject;
    }

    public void setEducationSubject(IReportParam<EnrCampaignDiscipline> educationSubject)
    {
        _educationSubject = educationSubject;
    }

    public IReportParam<EnrExamPassForm> getSubjectPassForm()
    {
        return _subjectPassForm;
    }

    public void setSubjectPassForm(IReportParam<EnrExamPassForm> subjectPassForm)
    {
        _subjectPassForm = subjectPassForm;
    }

    public IReportParam<Date> getPassDate()
    {
        return _passDate;
    }

    public void setPassDate(IReportParam<Date> passDate)
    {
        _passDate = passDate;
    }
}