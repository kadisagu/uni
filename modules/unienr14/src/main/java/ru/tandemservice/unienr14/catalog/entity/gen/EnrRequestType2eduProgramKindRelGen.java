package ru.tandemservice.unienr14.catalog.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType2eduProgramKindRel;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Вид ОП для вида заявления
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrRequestType2eduProgramKindRelGen extends EntityBase
 implements INaturalIdentifiable<EnrRequestType2eduProgramKindRelGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.catalog.entity.EnrRequestType2eduProgramKindRel";
    public static final String ENTITY_NAME = "enrRequestType2eduProgramKindRel";
    public static final int VERSION_HASH = 849830240;
    private static IEntityMeta ENTITY_META;

    public static final String L_REQUEST_TYPE = "requestType";
    public static final String L_PROGRAM_KIND = "programKind";

    private EnrRequestType _requestType;     // Вид заявления
    private EduProgramKind _programKind;     // Вид обазоваительной программы

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Вид заявления. Свойство не может быть null.
     */
    @NotNull
    public EnrRequestType getRequestType()
    {
        return _requestType;
    }

    /**
     * @param requestType Вид заявления. Свойство не может быть null.
     */
    public void setRequestType(EnrRequestType requestType)
    {
        dirty(_requestType, requestType);
        _requestType = requestType;
    }

    /**
     * @return Вид обазоваительной программы. Свойство не может быть null.
     */
    @NotNull
    public EduProgramKind getProgramKind()
    {
        return _programKind;
    }

    /**
     * @param programKind Вид обазоваительной программы. Свойство не может быть null.
     */
    public void setProgramKind(EduProgramKind programKind)
    {
        dirty(_programKind, programKind);
        _programKind = programKind;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrRequestType2eduProgramKindRelGen)
        {
            if (withNaturalIdProperties)
            {
                setRequestType(((EnrRequestType2eduProgramKindRel)another).getRequestType());
                setProgramKind(((EnrRequestType2eduProgramKindRel)another).getProgramKind());
            }
        }
    }

    public INaturalId<EnrRequestType2eduProgramKindRelGen> getNaturalId()
    {
        return new NaturalId(getRequestType(), getProgramKind());
    }

    public static class NaturalId extends NaturalIdBase<EnrRequestType2eduProgramKindRelGen>
    {
        private static final String PROXY_NAME = "EnrRequestType2eduProgramKindRelNaturalProxy";

        private Long _requestType;
        private Long _programKind;

        public NaturalId()
        {}

        public NaturalId(EnrRequestType requestType, EduProgramKind programKind)
        {
            _requestType = ((IEntity) requestType).getId();
            _programKind = ((IEntity) programKind).getId();
        }

        public Long getRequestType()
        {
            return _requestType;
        }

        public void setRequestType(Long requestType)
        {
            _requestType = requestType;
        }

        public Long getProgramKind()
        {
            return _programKind;
        }

        public void setProgramKind(Long programKind)
        {
            _programKind = programKind;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrRequestType2eduProgramKindRelGen.NaturalId) ) return false;

            EnrRequestType2eduProgramKindRelGen.NaturalId that = (NaturalId) o;

            if( !equals(getRequestType(), that.getRequestType()) ) return false;
            if( !equals(getProgramKind(), that.getProgramKind()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getRequestType());
            result = hashCode(result, getProgramKind());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getRequestType());
            sb.append("/");
            sb.append(getProgramKind());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrRequestType2eduProgramKindRelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrRequestType2eduProgramKindRel.class;
        }

        public T newInstance()
        {
            return (T) new EnrRequestType2eduProgramKindRel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "requestType":
                    return obj.getRequestType();
                case "programKind":
                    return obj.getProgramKind();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "requestType":
                    obj.setRequestType((EnrRequestType) value);
                    return;
                case "programKind":
                    obj.setProgramKind((EduProgramKind) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "requestType":
                        return true;
                case "programKind":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "requestType":
                    return true;
                case "programKind":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "requestType":
                    return EnrRequestType.class;
                case "programKind":
                    return EduProgramKind.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrRequestType2eduProgramKindRel> _dslPath = new Path<EnrRequestType2eduProgramKindRel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrRequestType2eduProgramKindRel");
    }
            

    /**
     * @return Вид заявления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrRequestType2eduProgramKindRel#getRequestType()
     */
    public static EnrRequestType.Path<EnrRequestType> requestType()
    {
        return _dslPath.requestType();
    }

    /**
     * @return Вид обазоваительной программы. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrRequestType2eduProgramKindRel#getProgramKind()
     */
    public static EduProgramKind.Path<EduProgramKind> programKind()
    {
        return _dslPath.programKind();
    }

    public static class Path<E extends EnrRequestType2eduProgramKindRel> extends EntityPath<E>
    {
        private EnrRequestType.Path<EnrRequestType> _requestType;
        private EduProgramKind.Path<EduProgramKind> _programKind;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Вид заявления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrRequestType2eduProgramKindRel#getRequestType()
     */
        public EnrRequestType.Path<EnrRequestType> requestType()
        {
            if(_requestType == null )
                _requestType = new EnrRequestType.Path<EnrRequestType>(L_REQUEST_TYPE, this);
            return _requestType;
        }

    /**
     * @return Вид обазоваительной программы. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrRequestType2eduProgramKindRel#getProgramKind()
     */
        public EduProgramKind.Path<EduProgramKind> programKind()
        {
            if(_programKind == null )
                _programKind = new EduProgramKind.Path<EduProgramKind>(L_PROGRAM_KIND, this);
            return _programKind;
        }

        public Class getEntityClass()
        {
            return EnrRequestType2eduProgramKindRel.class;
        }

        public String getEntityName()
        {
            return "enrRequestType2eduProgramKindRel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
