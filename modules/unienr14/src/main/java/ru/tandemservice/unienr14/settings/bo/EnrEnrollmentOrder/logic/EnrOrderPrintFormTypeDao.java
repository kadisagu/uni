/**
 *$Id: EnrOrderPrintFormTypeDao.java 34429 2014-05-26 10:13:50Z oleyba $
 */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentOrder.logic;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderPrintFormType;

/**
 * @author Alexander Shaburov
 * @since 05.07.13
 */
public class EnrOrderPrintFormTypeDao extends UniBaseDao implements IEnrOrderPrintFormTypeDao
{
    @Override
    public void doToggleUsed(long entityId)
    {
        final EnrOrderPrintFormType entity = getNotNull(EnrOrderPrintFormType.class, entityId);
        entity.setUsed(!entity.isUsed());
        update(entity);
    }

    @Override
    public void doToggleBasic(long entityId)
    {
        final EnrOrderPrintFormType entity = getNotNull(EnrOrderPrintFormType.class, entityId);
        entity.setBasic(!entity.isBasic());
        update(entity);
    }

    @Override
    public void doToggleCommand(long entityId)
    {
        final EnrOrderPrintFormType entity = getNotNull(EnrOrderPrintFormType.class, entityId);
        entity.setCommand(!entity.isCommand());
        update(entity);
    }

    @Override
    public void doToggleGroup(long entityId)
    {
        final EnrOrderPrintFormType entity = getNotNull(EnrOrderPrintFormType.class, entityId);
        entity.setGroup(!entity.isGroup());
        update(entity);
    }

    @Override
    public void doToggleHeadman(long entityId)
    {
        final EnrOrderPrintFormType entity = getNotNull(EnrOrderPrintFormType.class, entityId);
        entity.setSelectHeadman(!entity.isSelectHeadman());
        update(entity);
    }

    @Override
    public void doToggleReasonAndBasic(long entityId)
    {
        final EnrOrderPrintFormType entity = getNotNull(EnrOrderPrintFormType.class, entityId);
        entity.setReasonAndBasic(!entity.isReasonAndBasic());
        update(entity);
    }
}
