/* $Id:$ */
package ru.tandemservice.unienr14.sec.bo.EnrSec.logic;

import com.google.common.collect.ImmutableList;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.unienr14.catalog.entity.codes.LocalRoleScopeCodes;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.Collection;
import java.util.Collections;

import static org.tandemframework.hibsupport.dql.DQLExpressions.exists;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author oleyba
 * @since 6/11/15
 */
public class EnrSecDao extends UniBaseDao implements IEnrSecDao
{
    @Override
    public Collection<IEntity> getLocalRoleContexts(EnrEntrant entity, String localRoleScope)
    {
        switch (localRoleScope) {
            case LocalRoleScopeCodes.ORG_UNIT: return entity.getSecLocalEntities();
            case LocalRoleScopeCodes.ENROLLMENT_CAMPAIGN: return Collections.<IEntity>singletonList(entity.getEnrollmentCampaign());
            case LocalRoleScopeCodes.ENROLLMENT_COMMISSION:
                DQLSelectBuilder dql = new DQLSelectBuilder()
                    .fromEntity(EnrEnrollmentCommission.class, "c").column("c").order("c.id")
                    .where(exists(EnrEntrantRequest.class,
                                  EnrEntrantRequest.entrant().s(), entity,
                                  EnrEntrantRequest.enrollmentCommission().s(), property("c")
                    ))
                    ;
                return dql.createStatement(getSession()).list();
        }
        return ImmutableList.of();
    }

    @Override
    public Collection<IEntity> getLocalRoleContexts(EnrEntrantRequest entity, String localRoleScope)
    {
        switch (localRoleScope) {
            case LocalRoleScopeCodes.ENROLLMENT_CAMPAIGN: return Collections.<IEntity>singletonList(entity.getEntrant().getEnrollmentCampaign());
            case LocalRoleScopeCodes.ENROLLMENT_COMMISSION: return Collections.<IEntity>singletonList(entity.getEnrollmentCommission());
        }
        return ImmutableList.of();
    }

    @Override
    public Collection<IEntity> getLocalRoleContexts(EnrRequestedCompetition entity, String localRoleScope)
    {
        return getLocalRoleContexts(entity.getRequest(), localRoleScope);
    }

    @Override
    public Collection<IEntity> getLocalRoleContexts(EnrEnrollmentCampaign entity, String localRoleScope)
    {
        return Collections.<IEntity>singletonList(entity);
    }

    @Override
    public Collection<IEntity> getLocalRoleContexts(EnrEnrollmentCommission entity, String localRoleScope)
    {
        return Collections.<IEntity>singletonList(entity);
    }
}