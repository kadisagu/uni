/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.formatter;

import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.view.formatter.IFormatter;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam;

import java.util.Comparator;
import java.util.List;

/**
 * @author oleyba
 * @since 5/31/13
 */
public class EnrChosenEntranceExamNumberFormatter implements IFormatter<EnrChosenEntranceExam>
{
    private List<EnrChosenEntranceExam> allExams;

    public static Comparator<EnrChosenEntranceExam> COMPARATOR = new EntityComparator<EnrChosenEntranceExam>(
                    new EntityOrder(EnrChosenEntranceExam.actualExam().priority()),
                    new EntityOrder(EnrChosenEntranceExam.discipline().discipline().title())); 
    
    public EnrChosenEntranceExamNumberFormatter(List<EnrChosenEntranceExam> allExams)
    {
        this.allExams = allExams;
    }

    @Override
    public String format(EnrChosenEntranceExam source)
    {
        if (source.getActualExam() == null)
            return "(неакт.) ";
        int index = allExams.indexOf(source);
        EnrChosenEntranceExam previous = index == 0 ? null : allExams.get(index - 1);
        long previousNumber = previous == null || previous.getActualExam() == null ? 0 : previous.getActualExam().getPriority();
        long currentNumber = source.getActualExam().getPriority();
        if (currentNumber == previousNumber)
            return "(неакт.) ";
        return currentNumber + ". ";
    }
}
