package ru.tandemservice.unienr14.request.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.EnrRequestedProgram;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выбранная ОП в рамках конкурса
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrRequestedProgramGen extends VersionedEntityBase
 implements INaturalIdentifiable<EnrRequestedProgramGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.request.entity.EnrRequestedProgram";
    public static final String ENTITY_NAME = "enrRequestedProgram";
    public static final int VERSION_HASH = 1669900752;
    private static IEntityMeta ENTITY_META;

    public static final String P_VERSION = "version";
    public static final String L_REQUESTED_COMPETITION = "requestedCompetition";
    public static final String L_PROGRAM_SET_ITEM = "programSetItem";
    public static final String P_REG_DATE = "regDate";
    public static final String P_PRIORITY = "priority";

    private int _version; 
    private EnrRequestedCompetition _requestedCompetition;     // Выбранный конкурс
    private EnrProgramSetItem _programSetItem;     // ОП
    private Date _regDate;     // Дата добавления
    private int _priority;     // Приоритет в рамках выбранного конкурса

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public int getVersion()
    {
        return _version;
    }

    /**
     * @param version  Свойство не может быть null.
     */
    public void setVersion(int version)
    {
        dirty(_version, version);
        _version = version;
    }

    /**
     * @return Выбранный конкурс. Свойство не может быть null.
     */
    @NotNull
    public EnrRequestedCompetition getRequestedCompetition()
    {
        return _requestedCompetition;
    }

    /**
     * @param requestedCompetition Выбранный конкурс. Свойство не может быть null.
     */
    public void setRequestedCompetition(EnrRequestedCompetition requestedCompetition)
    {
        dirty(_requestedCompetition, requestedCompetition);
        _requestedCompetition = requestedCompetition;
    }

    /**
     * @return ОП. Свойство не может быть null.
     */
    @NotNull
    public EnrProgramSetItem getProgramSetItem()
    {
        return _programSetItem;
    }

    /**
     * @param programSetItem ОП. Свойство не может быть null.
     */
    public void setProgramSetItem(EnrProgramSetItem programSetItem)
    {
        dirty(_programSetItem, programSetItem);
        _programSetItem = programSetItem;
    }

    /**
     * @return Дата добавления. Свойство не может быть null.
     */
    @NotNull
    public Date getRegDate()
    {
        return _regDate;
    }

    /**
     * @param regDate Дата добавления. Свойство не может быть null.
     */
    public void setRegDate(Date regDate)
    {
        dirty(_regDate, regDate);
        _regDate = regDate;
    }

    /**
     * @return Приоритет в рамках выбранного конкурса. Свойство не может быть null.
     */
    @NotNull
    public int getPriority()
    {
        return _priority;
    }

    /**
     * @param priority Приоритет в рамках выбранного конкурса. Свойство не может быть null.
     */
    public void setPriority(int priority)
    {
        dirty(_priority, priority);
        _priority = priority;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrRequestedProgramGen)
        {
            if (withNaturalIdProperties)
            {
                setRequestedCompetition(((EnrRequestedProgram)another).getRequestedCompetition());
                setProgramSetItem(((EnrRequestedProgram)another).getProgramSetItem());
            }
            setVersion(((EnrRequestedProgram)another).getVersion());
            setRegDate(((EnrRequestedProgram)another).getRegDate());
            setPriority(((EnrRequestedProgram)another).getPriority());
        }
    }

    public INaturalId<EnrRequestedProgramGen> getNaturalId()
    {
        return new NaturalId(getRequestedCompetition(), getProgramSetItem());
    }

    public static class NaturalId extends NaturalIdBase<EnrRequestedProgramGen>
    {
        private static final String PROXY_NAME = "EnrRequestedProgramNaturalProxy";

        private Long _requestedCompetition;
        private Long _programSetItem;

        public NaturalId()
        {}

        public NaturalId(EnrRequestedCompetition requestedCompetition, EnrProgramSetItem programSetItem)
        {
            _requestedCompetition = ((IEntity) requestedCompetition).getId();
            _programSetItem = ((IEntity) programSetItem).getId();
        }

        public Long getRequestedCompetition()
        {
            return _requestedCompetition;
        }

        public void setRequestedCompetition(Long requestedCompetition)
        {
            _requestedCompetition = requestedCompetition;
        }

        public Long getProgramSetItem()
        {
            return _programSetItem;
        }

        public void setProgramSetItem(Long programSetItem)
        {
            _programSetItem = programSetItem;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrRequestedProgramGen.NaturalId) ) return false;

            EnrRequestedProgramGen.NaturalId that = (NaturalId) o;

            if( !equals(getRequestedCompetition(), that.getRequestedCompetition()) ) return false;
            if( !equals(getProgramSetItem(), that.getProgramSetItem()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getRequestedCompetition());
            result = hashCode(result, getProgramSetItem());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getRequestedCompetition());
            sb.append("/");
            sb.append(getProgramSetItem());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrRequestedProgramGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrRequestedProgram.class;
        }

        public T newInstance()
        {
            return (T) new EnrRequestedProgram();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "version":
                    return obj.getVersion();
                case "requestedCompetition":
                    return obj.getRequestedCompetition();
                case "programSetItem":
                    return obj.getProgramSetItem();
                case "regDate":
                    return obj.getRegDate();
                case "priority":
                    return obj.getPriority();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "version":
                    obj.setVersion((Integer) value);
                    return;
                case "requestedCompetition":
                    obj.setRequestedCompetition((EnrRequestedCompetition) value);
                    return;
                case "programSetItem":
                    obj.setProgramSetItem((EnrProgramSetItem) value);
                    return;
                case "regDate":
                    obj.setRegDate((Date) value);
                    return;
                case "priority":
                    obj.setPriority((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "version":
                        return true;
                case "requestedCompetition":
                        return true;
                case "programSetItem":
                        return true;
                case "regDate":
                        return true;
                case "priority":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "version":
                    return true;
                case "requestedCompetition":
                    return true;
                case "programSetItem":
                    return true;
                case "regDate":
                    return true;
                case "priority":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "version":
                    return Integer.class;
                case "requestedCompetition":
                    return EnrRequestedCompetition.class;
                case "programSetItem":
                    return EnrProgramSetItem.class;
                case "regDate":
                    return Date.class;
                case "priority":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrRequestedProgram> _dslPath = new Path<EnrRequestedProgram>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrRequestedProgram");
    }
            

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedProgram#getVersion()
     */
    public static PropertyPath<Integer> version()
    {
        return _dslPath.version();
    }

    /**
     * @return Выбранный конкурс. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedProgram#getRequestedCompetition()
     */
    public static EnrRequestedCompetition.Path<EnrRequestedCompetition> requestedCompetition()
    {
        return _dslPath.requestedCompetition();
    }

    /**
     * @return ОП. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedProgram#getProgramSetItem()
     */
    public static EnrProgramSetItem.Path<EnrProgramSetItem> programSetItem()
    {
        return _dslPath.programSetItem();
    }

    /**
     * @return Дата добавления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedProgram#getRegDate()
     */
    public static PropertyPath<Date> regDate()
    {
        return _dslPath.regDate();
    }

    /**
     * @return Приоритет в рамках выбранного конкурса. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedProgram#getPriority()
     */
    public static PropertyPath<Integer> priority()
    {
        return _dslPath.priority();
    }

    public static class Path<E extends EnrRequestedProgram> extends EntityPath<E>
    {
        private PropertyPath<Integer> _version;
        private EnrRequestedCompetition.Path<EnrRequestedCompetition> _requestedCompetition;
        private EnrProgramSetItem.Path<EnrProgramSetItem> _programSetItem;
        private PropertyPath<Date> _regDate;
        private PropertyPath<Integer> _priority;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedProgram#getVersion()
     */
        public PropertyPath<Integer> version()
        {
            if(_version == null )
                _version = new PropertyPath<Integer>(EnrRequestedProgramGen.P_VERSION, this);
            return _version;
        }

    /**
     * @return Выбранный конкурс. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedProgram#getRequestedCompetition()
     */
        public EnrRequestedCompetition.Path<EnrRequestedCompetition> requestedCompetition()
        {
            if(_requestedCompetition == null )
                _requestedCompetition = new EnrRequestedCompetition.Path<EnrRequestedCompetition>(L_REQUESTED_COMPETITION, this);
            return _requestedCompetition;
        }

    /**
     * @return ОП. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedProgram#getProgramSetItem()
     */
        public EnrProgramSetItem.Path<EnrProgramSetItem> programSetItem()
        {
            if(_programSetItem == null )
                _programSetItem = new EnrProgramSetItem.Path<EnrProgramSetItem>(L_PROGRAM_SET_ITEM, this);
            return _programSetItem;
        }

    /**
     * @return Дата добавления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedProgram#getRegDate()
     */
        public PropertyPath<Date> regDate()
        {
            if(_regDate == null )
                _regDate = new PropertyPath<Date>(EnrRequestedProgramGen.P_REG_DATE, this);
            return _regDate;
        }

    /**
     * @return Приоритет в рамках выбранного конкурса. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedProgram#getPriority()
     */
        public PropertyPath<Integer> priority()
        {
            if(_priority == null )
                _priority = new PropertyPath<Integer>(EnrRequestedProgramGen.P_PRIORITY, this);
            return _priority;
        }

        public Class getEntityClass()
        {
            return EnrRequestedProgram.class;
        }

        public String getEntityName()
        {
            return "enrRequestedProgram";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
