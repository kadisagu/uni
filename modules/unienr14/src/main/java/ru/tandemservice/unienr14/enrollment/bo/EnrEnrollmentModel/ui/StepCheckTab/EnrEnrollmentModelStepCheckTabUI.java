/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.ui.StepCheckTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.ui.CompetitionInStepPub.EnrEnrollmentModelCompetitionInStepPub;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.ui.CompetitionInStepPub.EnrEnrollmentModelCompetitionInStepPubUI;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.xml.EnrXmlEnrollmentCheckData;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.xml.IEnrXmlEnrollmentCheckDataOwner;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.ui.CompetitionPub.EnrEnrollmentStepCompetitionPub;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.ui.CompetitionPub.EnrEnrollmentStepCompetitionPubUI;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep;
import ru.tandemservice.unienr14.enrollment.entity.EnrModelStep;

import java.util.Map;

/**
 * @author oleyba
 * @since 4/2/15
 */
@Input({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "stepHolder.id", required=true)
})
public class EnrEnrollmentModelStepCheckTabUI extends UIPresenter implements IEnrXmlEnrollmentCheckDataOwner
{
    private final EntityHolder<EnrModelStep> stepHolder = new EntityHolder<EnrModelStep>();
    public EntityHolder<EnrModelStep> getStepHolder() { return this.stepHolder; }
    public EnrModelStep getStep() { return this.getStepHolder().getValue(); }

    EnrXmlEnrollmentCheckData checkData = new EnrXmlEnrollmentCheckData(false);
    public EnrXmlEnrollmentCheckData getCheckData() { return checkData; }

    @Override
    public String getCompetitionPubBC()
    {
        return EnrEnrollmentModelCompetitionInStepPub.class.getSimpleName();
    }

    @Override
    public Map getCompetitionPubParameters()
    {
        return new ParametersMap()
            .add(EnrEnrollmentModelCompetitionInStepPubUI.PARAM_STEP, getStep().getId())
            .add(EnrEnrollmentModelCompetitionInStepPubUI.PARAM_COMPETITION, getCheckData().getItem().getCompetitionId())
            .add(EnrEnrollmentModelCompetitionInStepPubUI.PARAM_TARGET_ADMISSION_KIND, getCheckData().getItem().getTargetAdmissionKindId())
            ;
    }

    @Override
    public void onComponentRefresh()
    {
        final EnrModelStep step = this.getStepHolder().refresh(EnrModelStep.class);
        checkData.refresh(step);
    }

    public void onClickRefreshContext() {
        this.getSupport().setRefreshScheduled(true);
        checkData.forceRefresh();
    }

    public void onClickDownloadXml() {
        checkData.onClickDownloadXml();
    }
}

