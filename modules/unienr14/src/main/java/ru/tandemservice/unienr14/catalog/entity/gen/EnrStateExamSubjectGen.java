package ru.tandemservice.unienr14.catalog.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.catalog.entity.EnrStateExamSubject;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Предмет ЕГЭ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrStateExamSubjectGen extends EntityBase
 implements INaturalIdentifiable<EnrStateExamSubjectGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.catalog.entity.EnrStateExamSubject";
    public static final String ENTITY_NAME = "enrStateExamSubject";
    public static final int VERSION_HASH = -759146481;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_SUBJECT_F_I_S_CODE = "subjectFISCode";
    public static final String P_SUBJECT_F_I_S_TITLE = "subjectFISTitle";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private String _shortTitle;     // Сокращенное название
    private String _subjectFISCode;     // Код предмета по ФИС
    private String _subjectFISTitle;     // Название предмета по ФИС
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Сокращенное название. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название. Свойство не может быть null и должно быть уникальным.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * Код справочника №1 ФИС ГИА и приема для предмета ЕГЭ
     *
     * @return Код предмета по ФИС. Свойство должно быть уникальным.
     */
    @Length(max=255)
    public String getSubjectFISCode()
    {
        return _subjectFISCode;
    }

    /**
     * @param subjectFISCode Код предмета по ФИС. Свойство должно быть уникальным.
     */
    public void setSubjectFISCode(String subjectFISCode)
    {
        dirty(_subjectFISCode, subjectFISCode);
        _subjectFISCode = subjectFISCode;
    }

    /**
     * Название предмета ЕГЭ в файле с результатами пакетной проверки результата ЕГЭ в ФИС ГИА и приема
     *
     * @return Название предмета по ФИС.
     */
    @Length(max=255)
    public String getSubjectFISTitle()
    {
        return _subjectFISTitle;
    }

    /**
     * @param subjectFISTitle Название предмета по ФИС.
     */
    public void setSubjectFISTitle(String subjectFISTitle)
    {
        dirty(_subjectFISTitle, subjectFISTitle);
        _subjectFISTitle = subjectFISTitle;
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrStateExamSubjectGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((EnrStateExamSubject)another).getCode());
            }
            setShortTitle(((EnrStateExamSubject)another).getShortTitle());
            setSubjectFISCode(((EnrStateExamSubject)another).getSubjectFISCode());
            setSubjectFISTitle(((EnrStateExamSubject)another).getSubjectFISTitle());
            setTitle(((EnrStateExamSubject)another).getTitle());
        }
    }

    public INaturalId<EnrStateExamSubjectGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<EnrStateExamSubjectGen>
    {
        private static final String PROXY_NAME = "EnrStateExamSubjectNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrStateExamSubjectGen.NaturalId) ) return false;

            EnrStateExamSubjectGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrStateExamSubjectGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrStateExamSubject.class;
        }

        public T newInstance()
        {
            return (T) new EnrStateExamSubject();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "shortTitle":
                    return obj.getShortTitle();
                case "subjectFISCode":
                    return obj.getSubjectFISCode();
                case "subjectFISTitle":
                    return obj.getSubjectFISTitle();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "subjectFISCode":
                    obj.setSubjectFISCode((String) value);
                    return;
                case "subjectFISTitle":
                    obj.setSubjectFISTitle((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "shortTitle":
                        return true;
                case "subjectFISCode":
                        return true;
                case "subjectFISTitle":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "shortTitle":
                    return true;
                case "subjectFISCode":
                    return true;
                case "subjectFISTitle":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "shortTitle":
                    return String.class;
                case "subjectFISCode":
                    return String.class;
                case "subjectFISTitle":
                    return String.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrStateExamSubject> _dslPath = new Path<EnrStateExamSubject>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrStateExamSubject");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrStateExamSubject#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Сокращенное название. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrStateExamSubject#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * Код справочника №1 ФИС ГИА и приема для предмета ЕГЭ
     *
     * @return Код предмета по ФИС. Свойство должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrStateExamSubject#getSubjectFISCode()
     */
    public static PropertyPath<String> subjectFISCode()
    {
        return _dslPath.subjectFISCode();
    }

    /**
     * Название предмета ЕГЭ в файле с результатами пакетной проверки результата ЕГЭ в ФИС ГИА и приема
     *
     * @return Название предмета по ФИС.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrStateExamSubject#getSubjectFISTitle()
     */
    public static PropertyPath<String> subjectFISTitle()
    {
        return _dslPath.subjectFISTitle();
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrStateExamSubject#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EnrStateExamSubject> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<String> _subjectFISCode;
        private PropertyPath<String> _subjectFISTitle;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrStateExamSubject#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(EnrStateExamSubjectGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Сокращенное название. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrStateExamSubject#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(EnrStateExamSubjectGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * Код справочника №1 ФИС ГИА и приема для предмета ЕГЭ
     *
     * @return Код предмета по ФИС. Свойство должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrStateExamSubject#getSubjectFISCode()
     */
        public PropertyPath<String> subjectFISCode()
        {
            if(_subjectFISCode == null )
                _subjectFISCode = new PropertyPath<String>(EnrStateExamSubjectGen.P_SUBJECT_F_I_S_CODE, this);
            return _subjectFISCode;
        }

    /**
     * Название предмета ЕГЭ в файле с результатами пакетной проверки результата ЕГЭ в ФИС ГИА и приема
     *
     * @return Название предмета по ФИС.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrStateExamSubject#getSubjectFISTitle()
     */
        public PropertyPath<String> subjectFISTitle()
        {
            if(_subjectFISTitle == null )
                _subjectFISTitle = new PropertyPath<String>(EnrStateExamSubjectGen.P_SUBJECT_F_I_S_TITLE, this);
            return _subjectFISTitle;
        }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrStateExamSubject#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EnrStateExamSubjectGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EnrStateExamSubject.class;
        }

        public String getEntityName()
        {
            return "enrStateExamSubject";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
