/* $Id:$ */
package ru.tandemservice.unienr14.extreports.externalView;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.extview.SimpleDQLExternalViewConfig;
import ru.tandemservice.unienr14.competition.entity.EnrTargetAdmissionPlan;

import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author oleyba
 * @since 8/23/13
 */
public class EnrExtViewProvider4TargetAdmPlan extends SimpleDQLExternalViewConfig
{

    @Override
    protected DQLSelectBuilder buildDqlQuery() {

        DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(EnrTargetAdmissionPlan.class, "ta_plan");

        column(dql, property(EnrTargetAdmissionPlan.id().fromAlias("ta_plan")), "id").comment("uid");
        column(dql, property(EnrTargetAdmissionPlan.plan().fromAlias("ta_plan")), "planValue").comment("план приема");
        column(dql, property(EnrTargetAdmissionPlan.programSetOrgUnit().id().fromAlias("ta_plan")), "programSetOrgUnitId").comment("uid подразделения, ведущего прием по набору ОП");
        column(dql, property(EnrTargetAdmissionPlan.programSetOrgUnit().programSet().id().fromAlias("ta_plan")), "programSetId").comment("uid набора ОП");
        column(dql, property(EnrTargetAdmissionPlan.programSetOrgUnit().orgUnit().id().fromAlias("ta_plan")), "enrOrgUnitId").comment("uid филиала в рамках ПК");
        column(dql, property(EnrTargetAdmissionPlan.enrCampaignTAKind().targetAdmissionKind().code().fromAlias("ta_plan")), "targetAdmissionKindCode").comment("код вида ЦП");
        column(dql, property(EnrTargetAdmissionPlan.enrCampaignTAKind().targetAdmissionKind().fullTitle().fromAlias("ta_plan")), "targetAdmissionKindFullTitle").comment("полное название вида ЦП");
        column(dql, property(EnrTargetAdmissionPlan.enrCampaignTAKind().targetAdmissionKind().title().fromAlias("ta_plan")), "targetAdmissionKindTitle").comment("название вида ЦП");
        return dql;
    }
}

