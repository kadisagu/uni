package ru.tandemservice.unienr14.rating.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm;
import ru.tandemservice.unienr14.competition.entity.EnrExamVariantPassForm;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm;
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSource;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выбранная форма сдачи (ВВИ-ф)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrChosenEntranceExamFormGen extends EntityBase
 implements INaturalIdentifiable<EnrChosenEntranceExamFormGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm";
    public static final String ENTITY_NAME = "enrChosenEntranceExamForm";
    public static final int VERSION_HASH = 1213342471;
    private static IEntityMeta ENTITY_META;

    public static final String L_CHOSEN_ENTRANCE_EXAM = "chosenEntranceExam";
    public static final String L_PASS_FORM = "passForm";
    public static final String L_MARK_SOURCE = "markSource";
    public static final String P_MARK_SOURCE_TIMESTAMP = "markSourceTimestamp";
    public static final String L_ACTUAL_EXAM_PASS_FORM = "actualExamPassForm";
    public static final String P_MARK_AS_DOUBLE_NULL_SAFE = "markAsDoubleNullSafe";
    public static final String P_MARK_AS_LONG = "markAsLong";
    public static final String P_MARK_AS_STRING = "markAsString";

    private EnrChosenEntranceExam _chosenEntranceExam;     // ВВИ
    private EnrExamPassForm _passForm;     // Форма сдачи
    private EnrEntrantMarkSource _markSource;     // Источник балла (основание)
    private Date _markSourceTimestamp;     // Дата изменения источника балла (основания)
    private EnrExamVariantPassForm _actualExamPassForm;     // Настроенная для конкурса форма сдачи ВИ, соотв. данной ВВИ-ф

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return ВВИ. Свойство не может быть null.
     */
    @NotNull
    public EnrChosenEntranceExam getChosenEntranceExam()
    {
        return _chosenEntranceExam;
    }

    /**
     * @param chosenEntranceExam ВВИ. Свойство не может быть null.
     */
    public void setChosenEntranceExam(EnrChosenEntranceExam chosenEntranceExam)
    {
        dirty(_chosenEntranceExam, chosenEntranceExam);
        _chosenEntranceExam = chosenEntranceExam;
    }

    /**
     * @return Форма сдачи. Свойство не может быть null.
     */
    @NotNull
    public EnrExamPassForm getPassForm()
    {
        return _passForm;
    }

    /**
     * @param passForm Форма сдачи. Свойство не может быть null.
     */
    public void setPassForm(EnrExamPassForm passForm)
    {
        dirty(_passForm, passForm);
        _passForm = passForm;
    }

    /**
     * Основание, на котором достигается максимальный балл. Служит источником рейтинга.
     * Обновляется демоном IEnrRatingDaemonDao#doChooseSource (выбор среди связанных оснований).
     *
     * @return Источник балла (основание).
     */
    public EnrEntrantMarkSource getMarkSource()
    {
        return _markSource;
    }

    /**
     * @param markSource Источник балла (основание).
     */
    public void setMarkSource(EnrEntrantMarkSource markSource)
    {
        dirty(_markSource, markSource);
        _markSource = markSource;
    }

    /**
     * Дата изменения значения атрибута markSource.
     * Обновляется демоном IEnrRatingDaemonDao#doChooseSource (выбор среди связанных оснований) при изменении значения в markSource.
     *
     * @return Дата изменения источника балла (основания).
     */
    public Date getMarkSourceTimestamp()
    {
        return _markSourceTimestamp;
    }

    /**
     * @param markSourceTimestamp Дата изменения источника балла (основания).
     */
    public void setMarkSourceTimestamp(Date markSourceTimestamp)
    {
        dirty(_markSourceTimestamp, markSourceTimestamp);
        _markSourceTimestamp = markSourceTimestamp;
    }

    /**
     * Ссылка на подходящую форму настройки ВИ для конкурса.
     * Обновляется демоном IEnrRatingDaemonDao#doRefreshChosenEntranceExams.
     *
     * @return Настроенная для конкурса форма сдачи ВИ, соотв. данной ВВИ-ф.
     */
    public EnrExamVariantPassForm getActualExamPassForm()
    {
        return _actualExamPassForm;
    }

    /**
     * @param actualExamPassForm Настроенная для конкурса форма сдачи ВИ, соотв. данной ВВИ-ф.
     */
    public void setActualExamPassForm(EnrExamVariantPassForm actualExamPassForm)
    {
        dirty(_actualExamPassForm, actualExamPassForm);
        _actualExamPassForm = actualExamPassForm;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrChosenEntranceExamFormGen)
        {
            if (withNaturalIdProperties)
            {
                setChosenEntranceExam(((EnrChosenEntranceExamForm)another).getChosenEntranceExam());
                setPassForm(((EnrChosenEntranceExamForm)another).getPassForm());
            }
            setMarkSource(((EnrChosenEntranceExamForm)another).getMarkSource());
            setMarkSourceTimestamp(((EnrChosenEntranceExamForm)another).getMarkSourceTimestamp());
            setActualExamPassForm(((EnrChosenEntranceExamForm)another).getActualExamPassForm());
        }
    }

    public INaturalId<EnrChosenEntranceExamFormGen> getNaturalId()
    {
        return new NaturalId(getChosenEntranceExam(), getPassForm());
    }

    public static class NaturalId extends NaturalIdBase<EnrChosenEntranceExamFormGen>
    {
        private static final String PROXY_NAME = "EnrChosenEntranceExamFormNaturalProxy";

        private Long _chosenEntranceExam;
        private Long _passForm;

        public NaturalId()
        {}

        public NaturalId(EnrChosenEntranceExam chosenEntranceExam, EnrExamPassForm passForm)
        {
            _chosenEntranceExam = ((IEntity) chosenEntranceExam).getId();
            _passForm = ((IEntity) passForm).getId();
        }

        public Long getChosenEntranceExam()
        {
            return _chosenEntranceExam;
        }

        public void setChosenEntranceExam(Long chosenEntranceExam)
        {
            _chosenEntranceExam = chosenEntranceExam;
        }

        public Long getPassForm()
        {
            return _passForm;
        }

        public void setPassForm(Long passForm)
        {
            _passForm = passForm;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrChosenEntranceExamFormGen.NaturalId) ) return false;

            EnrChosenEntranceExamFormGen.NaturalId that = (NaturalId) o;

            if( !equals(getChosenEntranceExam(), that.getChosenEntranceExam()) ) return false;
            if( !equals(getPassForm(), that.getPassForm()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getChosenEntranceExam());
            result = hashCode(result, getPassForm());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getChosenEntranceExam());
            sb.append("/");
            sb.append(getPassForm());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrChosenEntranceExamFormGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrChosenEntranceExamForm.class;
        }

        public T newInstance()
        {
            return (T) new EnrChosenEntranceExamForm();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "chosenEntranceExam":
                    return obj.getChosenEntranceExam();
                case "passForm":
                    return obj.getPassForm();
                case "markSource":
                    return obj.getMarkSource();
                case "markSourceTimestamp":
                    return obj.getMarkSourceTimestamp();
                case "actualExamPassForm":
                    return obj.getActualExamPassForm();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "chosenEntranceExam":
                    obj.setChosenEntranceExam((EnrChosenEntranceExam) value);
                    return;
                case "passForm":
                    obj.setPassForm((EnrExamPassForm) value);
                    return;
                case "markSource":
                    obj.setMarkSource((EnrEntrantMarkSource) value);
                    return;
                case "markSourceTimestamp":
                    obj.setMarkSourceTimestamp((Date) value);
                    return;
                case "actualExamPassForm":
                    obj.setActualExamPassForm((EnrExamVariantPassForm) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "chosenEntranceExam":
                        return true;
                case "passForm":
                        return true;
                case "markSource":
                        return true;
                case "markSourceTimestamp":
                        return true;
                case "actualExamPassForm":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "chosenEntranceExam":
                    return true;
                case "passForm":
                    return true;
                case "markSource":
                    return true;
                case "markSourceTimestamp":
                    return true;
                case "actualExamPassForm":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "chosenEntranceExam":
                    return EnrChosenEntranceExam.class;
                case "passForm":
                    return EnrExamPassForm.class;
                case "markSource":
                    return EnrEntrantMarkSource.class;
                case "markSourceTimestamp":
                    return Date.class;
                case "actualExamPassForm":
                    return EnrExamVariantPassForm.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrChosenEntranceExamForm> _dslPath = new Path<EnrChosenEntranceExamForm>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrChosenEntranceExamForm");
    }
            

    /**
     * @return ВВИ. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm#getChosenEntranceExam()
     */
    public static EnrChosenEntranceExam.Path<EnrChosenEntranceExam> chosenEntranceExam()
    {
        return _dslPath.chosenEntranceExam();
    }

    /**
     * @return Форма сдачи. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm#getPassForm()
     */
    public static EnrExamPassForm.Path<EnrExamPassForm> passForm()
    {
        return _dslPath.passForm();
    }

    /**
     * Основание, на котором достигается максимальный балл. Служит источником рейтинга.
     * Обновляется демоном IEnrRatingDaemonDao#doChooseSource (выбор среди связанных оснований).
     *
     * @return Источник балла (основание).
     * @see ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm#getMarkSource()
     */
    public static EnrEntrantMarkSource.Path<EnrEntrantMarkSource> markSource()
    {
        return _dslPath.markSource();
    }

    /**
     * Дата изменения значения атрибута markSource.
     * Обновляется демоном IEnrRatingDaemonDao#doChooseSource (выбор среди связанных оснований) при изменении значения в markSource.
     *
     * @return Дата изменения источника балла (основания).
     * @see ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm#getMarkSourceTimestamp()
     */
    public static PropertyPath<Date> markSourceTimestamp()
    {
        return _dslPath.markSourceTimestamp();
    }

    /**
     * Ссылка на подходящую форму настройки ВИ для конкурса.
     * Обновляется демоном IEnrRatingDaemonDao#doRefreshChosenEntranceExams.
     *
     * @return Настроенная для конкурса форма сдачи ВИ, соотв. данной ВВИ-ф.
     * @see ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm#getActualExamPassForm()
     */
    public static EnrExamVariantPassForm.Path<EnrExamVariantPassForm> actualExamPassForm()
    {
        return _dslPath.actualExamPassForm();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm#getMarkAsDoubleNullSafe()
     */
    public static SupportedPropertyPath<Double> markAsDoubleNullSafe()
    {
        return _dslPath.markAsDoubleNullSafe();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm#getMarkAsLong()
     */
    public static SupportedPropertyPath<Long> markAsLong()
    {
        return _dslPath.markAsLong();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm#getMarkAsString()
     */
    public static SupportedPropertyPath<String> markAsString()
    {
        return _dslPath.markAsString();
    }

    public static class Path<E extends EnrChosenEntranceExamForm> extends EntityPath<E>
    {
        private EnrChosenEntranceExam.Path<EnrChosenEntranceExam> _chosenEntranceExam;
        private EnrExamPassForm.Path<EnrExamPassForm> _passForm;
        private EnrEntrantMarkSource.Path<EnrEntrantMarkSource> _markSource;
        private PropertyPath<Date> _markSourceTimestamp;
        private EnrExamVariantPassForm.Path<EnrExamVariantPassForm> _actualExamPassForm;
        private SupportedPropertyPath<Double> _markAsDoubleNullSafe;
        private SupportedPropertyPath<Long> _markAsLong;
        private SupportedPropertyPath<String> _markAsString;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return ВВИ. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm#getChosenEntranceExam()
     */
        public EnrChosenEntranceExam.Path<EnrChosenEntranceExam> chosenEntranceExam()
        {
            if(_chosenEntranceExam == null )
                _chosenEntranceExam = new EnrChosenEntranceExam.Path<EnrChosenEntranceExam>(L_CHOSEN_ENTRANCE_EXAM, this);
            return _chosenEntranceExam;
        }

    /**
     * @return Форма сдачи. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm#getPassForm()
     */
        public EnrExamPassForm.Path<EnrExamPassForm> passForm()
        {
            if(_passForm == null )
                _passForm = new EnrExamPassForm.Path<EnrExamPassForm>(L_PASS_FORM, this);
            return _passForm;
        }

    /**
     * Основание, на котором достигается максимальный балл. Служит источником рейтинга.
     * Обновляется демоном IEnrRatingDaemonDao#doChooseSource (выбор среди связанных оснований).
     *
     * @return Источник балла (основание).
     * @see ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm#getMarkSource()
     */
        public EnrEntrantMarkSource.Path<EnrEntrantMarkSource> markSource()
        {
            if(_markSource == null )
                _markSource = new EnrEntrantMarkSource.Path<EnrEntrantMarkSource>(L_MARK_SOURCE, this);
            return _markSource;
        }

    /**
     * Дата изменения значения атрибута markSource.
     * Обновляется демоном IEnrRatingDaemonDao#doChooseSource (выбор среди связанных оснований) при изменении значения в markSource.
     *
     * @return Дата изменения источника балла (основания).
     * @see ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm#getMarkSourceTimestamp()
     */
        public PropertyPath<Date> markSourceTimestamp()
        {
            if(_markSourceTimestamp == null )
                _markSourceTimestamp = new PropertyPath<Date>(EnrChosenEntranceExamFormGen.P_MARK_SOURCE_TIMESTAMP, this);
            return _markSourceTimestamp;
        }

    /**
     * Ссылка на подходящую форму настройки ВИ для конкурса.
     * Обновляется демоном IEnrRatingDaemonDao#doRefreshChosenEntranceExams.
     *
     * @return Настроенная для конкурса форма сдачи ВИ, соотв. данной ВВИ-ф.
     * @see ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm#getActualExamPassForm()
     */
        public EnrExamVariantPassForm.Path<EnrExamVariantPassForm> actualExamPassForm()
        {
            if(_actualExamPassForm == null )
                _actualExamPassForm = new EnrExamVariantPassForm.Path<EnrExamVariantPassForm>(L_ACTUAL_EXAM_PASS_FORM, this);
            return _actualExamPassForm;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm#getMarkAsDoubleNullSafe()
     */
        public SupportedPropertyPath<Double> markAsDoubleNullSafe()
        {
            if(_markAsDoubleNullSafe == null )
                _markAsDoubleNullSafe = new SupportedPropertyPath<Double>(EnrChosenEntranceExamFormGen.P_MARK_AS_DOUBLE_NULL_SAFE, this);
            return _markAsDoubleNullSafe;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm#getMarkAsLong()
     */
        public SupportedPropertyPath<Long> markAsLong()
        {
            if(_markAsLong == null )
                _markAsLong = new SupportedPropertyPath<Long>(EnrChosenEntranceExamFormGen.P_MARK_AS_LONG, this);
            return _markAsLong;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm#getMarkAsString()
     */
        public SupportedPropertyPath<String> markAsString()
        {
            if(_markAsString == null )
                _markAsString = new SupportedPropertyPath<String>(EnrChosenEntranceExamFormGen.P_MARK_AS_STRING, this);
            return _markAsString;
        }

        public Class getEntityClass()
        {
            return EnrChosenEntranceExamForm.class;
        }

        public String getEntityName()
        {
            return "enrChosenEntranceExamForm";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract double getMarkAsDoubleNullSafe();

    public abstract long getMarkAsLong();

    public abstract String getMarkAsString();
}
