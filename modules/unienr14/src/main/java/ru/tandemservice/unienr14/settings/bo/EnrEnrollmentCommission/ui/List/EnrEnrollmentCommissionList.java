/* $Id$ */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.ui.Pub.EnrEnrollmentCommissionPub;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 29.05.2015
 */
@Configuration
public class EnrEnrollmentCommissionList extends BusinessComponentManager
{
    public static final String ENROLLMENT_COMMISSION_DS = "enrollmentCommissionDS";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(ENROLLMENT_COMMISSION_DS, enrollmentCommissionDSColumns(), enrollmentCommissionDS()))
                .create();
    }

    @Bean
    public ColumnListExtPoint enrollmentCommissionDSColumns()
    {
        return columnListExtPointBuilder(ENROLLMENT_COMMISSION_DS)
                .addColumn(publisherColumn(EnrEnrollmentCommission.P_TITLE, EnrEnrollmentCommission.title()).businessComponent(EnrEnrollmentCommissionPub.class).permissionKey("enr14EnrollmentCommission_view").order())
                .addColumn(textColumn(EnrEnrollmentCommission.P_SHORT_TITLE, EnrEnrollmentCommission.shortTitle()))
                .addColumn(toggleColumn(EnrEnrollmentCommission.P_ENABLED, EnrEnrollmentCommission.enabled())
                                   .toggleOffListener("onClickChangeEnable").toggleOnListener("onClickChangeEnable")
                                   .toggleOnLabel("enrollmentCommissionDS.enabledOnLabel").toggleOffLabel("enrollmentCommissionDS.enabledOffLabel").permissionKey("enr14EnrollmentCommission_enable"))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).permissionKey("enr14EnrollmentCommission_addEdit"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER, alert("enrollmentCommissionDS.delete.alert", EnrEnrollmentCommission.title())).permissionKey("enr14EnrollmentCommission_delete"))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler enrollmentCommissionDS()
    {
        return new DefaultSearchDataSourceHandler(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                String title = context.get(EnrEnrollmentCommissionListUI.PARAM_TITLE);
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrEnrollmentCommission.class, "e").column("e");

                if (null != title)
                    builder.where(likeUpper(property("e", EnrEnrollmentCommission.title()), value(CoreStringUtils.escapeLike(title, true))));

                return DQLSelectOutputBuilder.get(input, builder, context.getSession()).order().build();
            }
        };
    }
}
