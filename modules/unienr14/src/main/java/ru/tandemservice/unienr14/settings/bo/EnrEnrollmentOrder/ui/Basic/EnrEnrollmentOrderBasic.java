/**
 *$Id: EnrEnrollmentOrderBasic.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentOrder.ui.Basic;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderBasic;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentOrder.logic.EnrEnrollmentOrderReasonBasicSearchDSHandler;

/**
 * @author Alexander Shaburov
 * @since 05.07.13
 */
@Configuration
public class EnrEnrollmentOrderBasic extends BusinessComponentManager
{
    public static final String BASIC_SELECT_DS = "basicSelectDS";
    public static final String REASON_BASIC_SEARCH_DS = "reasonBasicSearchDS";
    public static final String PARAM_ORDER_TYPE = "orderType";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(BASIC_SELECT_DS, basicSelectDSHandler()))
                .addDataSource(searchListDS(REASON_BASIC_SEARCH_DS, basicReasonSearchDSColumns(), basicReasonSearchDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint basicReasonSearchDSColumns()
    {
        return columnListExtPointBuilder(REASON_BASIC_SEARCH_DS)
                .create();
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> basicReasonSearchDSHandler()
    {
        return new EnrEnrollmentOrderReasonBasicSearchDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> basicSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrOrderBasic.class)
            .where(EnrOrderBasic.orderType(), PARAM_ORDER_TYPE)
            .order(EnrOrderBasic.title())
            .pageable(true);
    }
}
