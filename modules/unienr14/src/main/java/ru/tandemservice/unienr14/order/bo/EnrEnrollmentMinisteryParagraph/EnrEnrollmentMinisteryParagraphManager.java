/* $Id$ */
package ru.tandemservice.unienr14.order.bo.EnrEnrollmentMinisteryParagraph;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unienr14.order.bo.EnrEnrollmentMinisteryParagraph.logic.EnrEnrollmentMinisteryParagraphDao;
import ru.tandemservice.unienr14.order.bo.EnrEnrollmentMinisteryParagraph.logic.IEnrEnrollmentMinisteryParagraphDao;

/**
 * @author nvankov
 * @since 9/1/14
 */
@Configuration
public class EnrEnrollmentMinisteryParagraphManager extends BusinessObjectManager
{
    public static EnrEnrollmentMinisteryParagraphManager instance()
    {
        return instance(EnrEnrollmentMinisteryParagraphManager.class);
    }

    @Bean
    public IEnrEnrollmentMinisteryParagraphDao dao()
    {
        return new EnrEnrollmentMinisteryParagraphDao();
    }
}

    