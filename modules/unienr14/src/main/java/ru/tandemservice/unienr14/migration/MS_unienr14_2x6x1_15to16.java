package ru.tandemservice.unienr14.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x1_15to16 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
        {
            new ScriptDependency("org.tandemframework", "1.6.15"),
            new ScriptDependency("org.tandemframework.shared", "1.6.1")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        // сбросить timestamp для ВВИ и ВВИ-ф (демон восстановит)
        tool.executeUpdate("update enr14_chosen_entr_exam_form_t set marksourcetimestamp_p=null");
        tool.executeUpdate("update enr14_chosen_entr_exam_t set maxmarkformtimestamp_p=null");
    }
}