/* $Id:$ */
package ru.tandemservice.unienr14.refusal.bo.EnrEntrantRefusal.ui.AddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.refusal.bo.EnrEntrantRefusal.EnrEntrantRefusalManager;
import ru.tandemservice.unienr14.refusal.entity.EnrEntrantRefusal;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;

import java.util.Date;

/**
 * @author rsizonenko
 * @since 20.05.2014.
 */

@Input(@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "enrEntrantRefusal.id"))
public class EnrEntrantRefusalAddEditUI extends UIPresenter {

    private EnrEntrantRefusal enrEntrantRefusal = new EnrEntrantRefusal();

    // override from UIPresenter

    @Override
    public void onComponentRefresh() {
        if (isEditForm()) {
            setEnrEntrantRefusal(IUniBaseDao.instance.get().get(EnrEntrantRefusal.class, getEnrEntrantRefusal().getId()));
        }
        else {
            getEnrEntrantRefusal().setRegistrationDate(new Date());
            getEnrEntrantRefusal().setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrEntrantRefusalAddEdit.ENR_ENROLLMENT_CAMPAIGN, getEnrEntrantRefusal().getEnrollmentCampaign());
        dataSource.put(EnrEntrantRefusalAddEdit.ENR_ORG_UNIT, getEnrEntrantRefusal().getEnrOrgUnit());
    }

    // listeners

    public void onClickApply()
    {
        EnrEntrantRefusalManager.instance().dao().saveOrUpdateEntrantRefusal(getEnrEntrantRefusal());
        deactivate();
    }

    public void onEnrollmentCampaignRefresh()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrEntrantRefusal().getEnrollmentCampaign());
    }

    public void onClickCancel()
    {
        deactivate();
    }

    // getters setters

    public boolean isAddForm(){ return getEnrEntrantRefusal().getId() == null; }

    public boolean isEditForm(){ return !isAddForm(); }

    public EnrEntrantRefusal getEnrEntrantRefusal()
    {
        return enrEntrantRefusal;
    }

    public void setEnrEntrantRefusal(EnrEntrantRefusal enrEntrantRefusal) {
        this.enrEntrantRefusal = enrEntrantRefusal;
    }
}
