/* $Id$ */
package ru.tandemservice.unienr14.ws.onlineentrant;

import com.google.common.collect.Lists;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.List;

/**
 * @author nvankov
 * @since 5/14/14
 */
@XmlRootElement
public class OnlineEntrantData
{
    @XmlAttribute(required = true)
    public String enrollmentCampaign; // название приемной кампании

    @XmlAttribute(required = true)
    public long onlineEntrantId; // id абитуриента в внешней системе

    @XmlAttribute
    public String personalNumber; // персональный номер абитуриента в Tandem University

    @XmlAttribute
    public boolean statementAccepted; // заявление принято

    // данные заявления
    @XmlAttribute
    public String haveEduLevel;     // Полученное образование (eduLevel)
    @XmlAttribute
    public String wantRequestType;     // Программы (enrRequestType)

    @XmlAttribute
    public List<String> stateExamCodes = Lists.newArrayList(); // предметы ЕГЭ

    @XmlAttribute
    public boolean exclusiveRights;     // У меня есть особые права
    @XmlAttribute
    public boolean exclusiveRightsWithoutExams;     // Право на прием без вступительных испытаний
    @XmlAttribute
    public boolean exclusiveRightsBudget;     // Право на прием на обучение за счет бюджетных ассигнований в пределах установленной квоты
    @XmlAttribute
    public boolean exclusiveRightsEmptiveEnroll;     // Преимущественное право зачисления

    @XmlAttribute
    public boolean haveAgreementTargetEdu;     // Заключен договор о целевом обучении
    @XmlAttribute
    public String agreementNumber;     // Номер договора
    @XmlAttribute
    public Date agreementDate;     // Дата заключения
    @XmlAttribute
    public String agreementOrg;     // Данные об организации
    @XmlAttribute
    public String agreementEduProgramm;     // Данные об образовательной программе

    public List<EnrCompetitionData> competitions = Lists.newArrayList(); // Выбранные конкурсы

    public static class EnrCompetitionData
    {
        public EnrCompetitionData()
        {

        }

        @XmlAttribute(required = true)
        public String competition; // Конкурс (код university)

        @XmlAttribute(required = true)
        public int priority; // Приоритет
    }

    // персональные данные

    @XmlAttribute
    public String regAddress; // адрес регистрации
    @XmlAttribute
    public boolean sameAddress; // адреса регистрации и фактический совпадают
    @XmlAttribute
    public String factAddress; // фактический адрес

    @XmlAttribute
    public boolean needDorm; // нуждается в общежитии

    @XmlAttribute
    public String contactPhone; // контактный телефон
    @XmlAttribute
    public String mobilePhone; // мобильный телефон
    @XmlAttribute
    public String workPhone; // рабочий телефон
    @XmlAttribute
    public String email; // e-mail

    @XmlAttribute
    public Integer childs; // Количество детей
    @XmlAttribute
    public String maritalStatus; // Семейное положение (код university)
    @XmlAttribute
    public String currentWork; // Место работы в настоящее время
    @XmlAttribute
    public String currentPost; // Должность
    @XmlAttribute
    public byte[] otherDocScanCopy; // Скан-копия иных документов
    @XmlAttribute
    public String otherDocScanCopyName; // Имя файла скан-копии иных документов
    @XmlAttribute
    public String otherDocScanCopyContentType; // Тип контента скан-копии иных документов


    @XmlAttribute
    public Date armyStartDate; // Дата начала службы
    @XmlAttribute
    public Date armyEndDate; // Дата окончания службы
    @XmlAttribute
    public Integer armyEndYear; // Год увольнения в запас
    @XmlAttribute
    public String foreignLanguage; // Иностранный язык (код university)

    @XmlAttribute
    public List<String> individualAchievements = Lists.newArrayList(); // Индивидуальные достижения (коды university)
    @XmlAttribute
    public byte[] indAchievScanCopy; // Скан-копия документа, подтверждающего индивидуальные достижения
    @XmlAttribute
    public String indAchievScanCopyName; // Имя файла скан-копии документа, подтверждающего индивидуальные достижения
    @XmlAttribute
    public String indAchievScanCopyContentType; // Тип контента скан-копии документа, подтверждающего индивидуальные достижения

    @XmlAttribute
    public List<String> trainingCourses = Lists.newArrayList(); // Подготовительные курсы (коды university)
    @XmlAttribute
    public List<String> trainingDepartments = Lists.newArrayList(); // Подготовительные отделения (коды university)
    @XmlAttribute
    public List<String> sourceInfoAboutOUList = Lists.newArrayList(); // Источники информации об ОУ (коды university)

    public List<PersonSportAchievementData> sportAchievementes = Lists.newArrayList(); // Спортивные достижения

    public static class PersonSportAchievementData
    {
        public PersonSportAchievementData()
        {

        }

        @XmlAttribute(required = true)
        public String sportType; // Вид спорта (код university)

        @XmlAttribute(required = true)
        public String sportRank; // Спортивные разряды и звания (код university)
    }

    // Удостоверение личности
    @XmlAttribute
    public String firstName; // Имя
    @XmlAttribute
    public String lastName; // Фамилия
    @XmlAttribute
    public String middleName; // Отчество

    @XmlAttribute
    public String seria; // Серия
    @XmlAttribute
    public String number; // Номер
    @XmlAttribute
    public Date birthDate; // Дата рождения
    @XmlAttribute
    public String birthPlace; // Место рождения
    @XmlAttribute
    public Date issuanceDate; // Дата выдачи удостоверения
    @XmlAttribute
    public String issuancePlace; // Кем выдано удостоверение
    @XmlAttribute
    public String issuanceCode; // Код подразделения
    @XmlAttribute
    public String sex; // Пол (код university)
    @XmlAttribute
    public String citizenship; // Страна (код university)
    @XmlAttribute
    public String cardType; // Тип удостоверения личности (код university)
    @XmlAttribute
    public byte[] idCardScanCopy; // Скан-копия документа удостверения личности
    @XmlAttribute
    public String idCardScanCopyName; // Имя файла скан-копии документа удостверения личности
    @XmlAttribute
    public String idCardScanCopyContentType; // Тип контента скан-копии документа удостверения личности

    // Ближайший родственник
    public String relationDegree; // Родственник (код university)
    public String lastNameNextOfKin; // Фамилия
    public String firstNameNextOfKin; // Имя
    public String patronymicNextOfKin; // Отчество
    public String employmentPlace; // Место работы
    public String post; // Должность
    public String phones; // Контактные телефоны

    // Документ об образовании

    @XmlAttribute
    public String eduDocumentKind; // Вид документа (код university)
    @XmlAttribute
    public String documentKindDetailed; // Вид документа (по документу, для иных документов)
    @XmlAttribute
    public String seriaEduDocument; // Серия
    @XmlAttribute
    public String numberEduDocument; // Номер
    @XmlAttribute
    public String eduOrganizationCountry; // Страна образовательной организации  (код university)
    @XmlAttribute
    public String eduOrganizationSettlement;// Населенный пункт образовательной организации
    @XmlAttribute
    public String eduOrganization; // Образовательная организация
    @XmlAttribute
    public int yearEnd; // Год окончания обучения
    @XmlAttribute
    public String ruRegionCode; // Код субъекта РФ
    @XmlAttribute
    public Date issuanceDateEduDocument;     // Дата выдачи
    @XmlAttribute
    public String eduLevel; // Актуальный уровень образования (код university)
    @XmlAttribute
    public String documentEducationLevel; // Уровень образования по документу
    @XmlAttribute
    public String qualification; // Квалификация
    @XmlAttribute
    public String eduProgramSubject ;     // Направление (профессия, специальность)
    @XmlAttribute
    public String graduationHonour; // Степень отличия по окончании образовательного учреждения (код university)
    @XmlAttribute
    public Long avgMarkAsLong; // Средний балл
    @XmlAttribute
    public byte[] scanCopy; // Скан-копия документа об образовании
    @XmlAttribute
    public String scanCopyName; // Имя файла скан-копии документа об образовании
    @XmlAttribute
    public String scanCopyContentType; // Тип контента скан-копии документа об образовании
    @XmlAttribute
    public Integer mark5;     // Кол-во оценок «Отлично»
    @XmlAttribute
    public Integer mark4;     // Кол-во оценок «Хорошо»
    @XmlAttribute
    public Integer mark3;     // Кол-во оценок «Удовлетворительно»
    @XmlAttribute
    public String registrationNumber;     // Регистрационный номер


}
