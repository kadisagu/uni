/* $Id: ContactsPrintBlock.java 7969 2016-05-30 12:37:43Z rsizonenko $ */
package ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.print.contacts;

import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.fias.base.entity.ICitizenship;
import org.tandemframework.shared.person.base.bo.ReportPerson.ui.Add.ReportPersonAddUI;
import org.tandemframework.shared.person.base.bo.ReportPerson.ui.Add.print.contacts.ContactsPrintBlock;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.*;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.print.ReportPrintListColumn;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.print.ReportPrintPathColumn;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.EnrReportPersonAddUI;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Vasily Zhukov
 * @since 14.02.2011
 */
public class EnrContactsPrintBlock extends ContactsPrintBlock implements IReportPrintBlock
{
    private IReportPrintCheckbox _contactPhone = new ReportPrintCheckbox();
    private IReportPrintCheckbox _workPhone = new ReportPrintCheckbox();

    @Override
    public String getCheckJS()
    {
        List<String> ids = new ArrayList<>();
        for (int i = 1; i <= 9; i++)
            ids.add("enrChContact" + i);
        return ReportJavaScriptUtil.getCheckJS(ids);
    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        if (_contactPhone.isActive())
        {
            int phoneIdx = dql.addLastAggregateColumn(Person.contactData().phoneDefault());
            printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintPathColumn("contactPhone", phoneIdx, null));
        }

        if (_workPhone.isActive())
        {
            int phoneIdx = dql.addLastAggregateColumn(Person.contactData().phoneWork());
            printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintPathColumn("workPhone", phoneIdx, null));
        }

        modify(dql, printInfo, EnrReportPersonAddUI.ENTRANT_SCHEET);
    }

    public IReportPrintCheckbox getContactPhone() {
        return _contactPhone;
    }

    public IReportPrintCheckbox getWorkPhone() {
        return _workPhone;
    }
}
