/* $Id$ */
package ru.tandemservice.unienr14.entrant.bo.EnrOnlineEntrant.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

/**
 * @author nvankov
 * @since 5/19/14
 */
public class EnrOnlineEntrantListUI extends UIPresenter
{
    private EnrEnrollmentCampaign _enrollmentCampaign;

    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }

    @Override
    public void onComponentRefresh()
    {
        setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(EnrOnlineEntrantList.ONLINE_ENTRANT_DS.equals(dataSource.getName()))
        {
            if (_enrollmentCampaign != null)
            {
                dataSource.put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, _enrollmentCampaign);
                dataSource.putAll(getSettings().getAsMap(
                        true, "lastNameFilter", "firstNameFilter", "middleNameFilter", "personalNumberFilter"));
            }
        }
    }

    public boolean isNothingSelected(){ return getEnrollmentCampaign() == null; }

    public void onEnrCampaignRefresh()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
    }

    public void onDeleteEntityFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public void onClearEntrantLink() {
        IUniBaseDao.instance.get().doInTransaction(session -> {
            EnrOnlineEntrant onlineEntrant = (EnrOnlineEntrant) session.get(EnrOnlineEntrant.class, getListenerParameterAsLong());
            onlineEntrant.setEntrant(null);
            session.update(onlineEntrant);
            return null;
        });
    }
}
