package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x0_25to26 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrEntrantStateExamResult

		// создано обязательное свойство abovePassingMark
		{
			// создать колонку
			tool.createColumn("enr14_state_exam_result_t", new DBColumn("abovepassingmark_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			tool.executeUpdate("update enr14_state_exam_result_t set abovepassingmark_p=? where abovepassingmark_p is null", false);

			// сделать колонку NOT NULL
			tool.setColumnNullable("enr14_state_exam_result_t", "abovepassingmark_p", false);
		}

		// создано обязательное свойство foundInFis
		{
			// создать колонку
			tool.createColumn("enr14_state_exam_result_t", new DBColumn("foundinfis_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			tool.executeUpdate("update enr14_state_exam_result_t set foundinfis_p=? where foundinfis_p is null", false);

			// сделать колонку NOT NULL
			tool.setColumnNullable("enr14_state_exam_result_t", "foundinfis_p", false);
		}
    }
}