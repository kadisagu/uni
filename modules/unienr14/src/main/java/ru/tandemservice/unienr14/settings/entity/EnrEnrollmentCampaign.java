package ru.tandemservice.unienr14.settings.entity;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.gen.EnrEnrollmentCampaignGen;

import java.util.Collections;
import java.util.List;

/**
 * Приемная кампания
 *
 * Проводится в рамках аккредитованного подразделения.
 */
public class EnrEnrollmentCampaign extends EnrEnrollmentCampaignGen implements ITitled
{
    public EnrEnrollmentCampaign()
    {
    }

    @Override
    public String getLocalRoleContextFullTitle()
    {
        return "Приемная кампания " + getTitle();
    }

    /**
     * Период приема.
     */
    @Override
    @EntityDSLSupport
    public String getEnrollmentPeriod()
    {
        return EnrEnrollmentCampaignManager.instance().getProperty("enrollmentCampaign.period", DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateFrom()), DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateTo()));
    }

    public boolean isGreaterThan2014()
    {
        return getEducationYear().getIntValue() > 2014;
    }

    public boolean isLessOrEqualThan2014()
    {
        return !isGreaterThan2014();
    }

    public void checkOpen()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().checkEnrollmentCampaignOpen(this);
    }

    public static EntityComboDataSourceHandler defaultSelectDSHandler(String name) {
        return new EntityComboDataSourceHandler(name, EnrEnrollmentCampaign.class)
        {
            @Override protected boolean isExternalSortRequired() {
                return true;
            }

            @Override protected List<IEntity> sortOptions(List<IEntity> list) {
                Collections.sort(list, EnrEnrollmentCampaignManager.ENR_CAMPAIGN_COMPARATOR);
                return list;
            }
        }
                .filter(EnrEnrollmentCampaign.title());
    }

    public static EntityComboDataSourceHandler permissionSelectDSHandler(String name)
    {
        return defaultSelectDSHandler(name)
                .customize((alias, dql, context, filter) -> dql.where(DQLExpressions.in(DQLExpressions.property(alias, EnrEnrollmentCampaign.id()),
                        EnrEnrollmentCampaignManager.instance().enrCampaignPermissionsDao().getEnrollmentCampaignIds(UserContext.getInstance().getPrincipalContext()))));
    }

    public static EntityComboDataSourceHandler openSelectDSHandler(String name)
    {
        return defaultSelectDSHandler(name)
                .where(EnrEnrollmentCampaign.open(), Boolean.TRUE);
    }
}