package ru.tandemservice.unienr14.rating.daemon;

import org.apache.commons.lang.mutable.MutableInt;
import org.hibernate.Session;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.debug.DebugBlock;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import org.tandemframework.shared.commonbase.base.util.DoNotUseMe;
import org.tandemframework.shared.commonbase.base.util.DoNotUseMe.Reason;
import org.tandemframework.shared.commonbase.base.util.ThreadCountProperty;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentStepState;
import ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEnrollmentStepStateCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantAchievementKindCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrExamPassFormCodes;
import ru.tandemservice.unienr14.competition.entity.EnrExamSetVariant;
import ru.tandemservice.unienr14.competition.entity.EnrExamVariant;
import ru.tandemservice.unienr14.competition.entity.EnrExamVariantPassForm;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItem;
import ru.tandemservice.unienr14.entrant.daemon.EnrEntrantDaemonBean;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantOriginalDocumentStatus;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult;
import ru.tandemservice.unienr14.exams.entity.EnrExamSetDiscipline;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unienr14.order.entity.gen.EnrAbstractExtractGen;
import ru.tandemservice.unienr14.order.entity.gen.EnrAbstractOrderGen;
import ru.tandemservice.unienr14.rating.entity.*;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings;
import ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementType;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;

import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.Callable;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.sum;

/**
 * @author vdanilov
 */
public class EnrRatingDaemonDao extends UniBaseDao implements IEnrRatingDaemonDao {

    private static final ThreadCountProperty RATING_ITEMS_UPDATE_THREAD_COUNT = new ThreadCountProperty(EnrRatingDaemonDao.class.getName()+".threads", 4);

    /** запускается из демона EnrEntrantDaemonBean.DAEMON */
    @DoNotUseMe(reason=Reason.INTERNAL, comment="Внутренний механизм демона")
    public static final Runnable DAEMON_ACTIONS = () -> {
        EnrEntrantDaemonBean.DAEMON.check();
        final IEnrRatingDaemonDao dao = IEnrRatingDaemonDao.instance.get();

        // обновление ссылки ВВИ на настроенную ВИ, ВВИ-Ф на форму настроенной ВИ, признак корректности выбора ВВИ-ф в ВВИ
        dao.doRefreshChosenEntranceExams();

        try (DebugBlock b = Debug.block("sources-update")) {
            dao.doRefreshSources();
        }

        // выбор основания с максимальными баллами для ВВИ-Ф и мексимального ВВИ-Ф для ВВИ, а также признак «сдано все»
        dao.doChooseSource();

        // обновляем баллы за индивидуальные достижения
        dao.doUpdateAchievementMark();

        // удаляем все лишнее из базы, обновляем ссылки (это надо делать в отдельной транзакции)
        IEnrRatingListDao.instance.get().doPrepareRatingItems();

        // создание и обновление рейтингов для абитуриентов
        dao.doUpdateRatingList();

        // обновление данных об исключении из списков
        dao.doUpdateExcludedByStatus();

        // System.out.println(Debug.getRootSection().toFullString());
    };

    ///////////////////////////////////////////////////////
    // листенеры
    ///////////////////////////////////////////////////////

    private static void registerWakeUp(final Class<? extends IEntity> klass, String... affectedProperties) {
        EnrEntrantDaemonBean.DAEMON.registerWakeUpListener4Class(klass, affectedProperties);
    }

    @Override
    protected void initDao()
    {
        // HINT: поскольку демоны не могут пробуждать сами себя, то можно смело регистрировать листенеры на все объекты, которые демоном могут быть изменены

        // настройки ИД
        registerWakeUp(EnrEnrollmentCampaignSettings.class);
        registerWakeUp(EnrEntrantAchievementType.class);

        // при изменении наборов ВИ
        registerWakeUp(EnrExamSetDiscipline.class);

        // при изменении настроенных ВИ и их форм
        registerWakeUp(EnrExamVariant.class);
        registerWakeUp(EnrExamVariantPassForm.class);

        // при изменении ВВИ и ВВИ-Ф (руками с форм)
        registerWakeUp(EnrChosenEntranceExam.class);
        registerWakeUp(EnrChosenEntranceExamForm.class);

        // при изменени любого основания (каскадное удаление, например)
        registerWakeUp(EnrEntrantMarkSource.class);

        // при изменении сертификата ЕГЭ, ДДС (включая пересдачи) и апелляции
        registerWakeUp(EnrEntrantStateExamResult.class);
        registerWakeUp(EnrExamPassDiscipline.class);
        registerWakeUp(EnrExamPassAppeal.class);

        // заявление, признаки оригиналов, выбранные конкурсы, инд.достижения
        registerWakeUp(EnrEntrant.class);
        registerWakeUp(EnrEntrantRequest.class);
        registerWakeUp(EnrRequestedCompetition.class);
//        registerWakeUp(EnrEntrantOriginalDocumentStatus.class);
        registerWakeUp(EnrEntrantAchievement.class);

        // шаги зачисления
        registerWakeUp(EnrEnrollmentStep.class, EnrEnrollmentStep.L_STATE);
        registerWakeUp(EnrEnrollmentStepItem.class, EnrEnrollmentStepItem.P_RECOMMENDED, EnrEnrollmentStepItem.P_SHOULD_BE_ENROLLED);

        // приказы
        registerWakeUp(EnrOrder.class, EnrAbstractOrderGen.L_STATE);
        registerWakeUp(EnrEnrollmentExtract.class, EnrAbstractExtractGen.L_STATE, EnrEnrollmentExtract.P_CANCELLED, EnrEnrollmentExtract.P_EXCLUDE_LOWER_PRIORITY_COMPETITIONS);
    }

    ///////////////////////////////////////////////////////
    // сопоставление ВВИ(-ф) и настроенной ВИ(-ф)
    ///////////////////////////////////////////////////////

    @Override
    public boolean doRefreshChosenEntranceExams()
    {
        EnrEntrantDaemonBean.DAEMON.check();
        Debug.begin("EnrRatingDaemonDao.doRefreshChosenEntranceExams");
        try {
            int updates = 0;
            final Session session = this.lock(EnrChosenEntranceExam.class.getName());

            // обновление ссылки ВВИ на настроенную ВИ (основываемся на работе IEnrExamSetDaemonBean.doUpdateExamSetDisciplines)
            Debug.begin("enrChosenEntranceExam.actualExam");
            try {
                updates += executeAndClear(
                    new DQLUpdateBuilder(EnrChosenEntranceExam.class)
                    .fromEntity(EnrChosenEntranceExam.class, "x").where(eq(property("x.id"), property("id")))
                    .joinPath(DQLJoinType.inner, EnrChosenEntranceExam.requestedCompetition().competition().examSetVariant().fromAlias("x"), "esv")
                    //.where(eq(property(EnrExamSetVariant.examSet().enrollmentCampaign().open().fromAlias("esv")), value(Boolean.TRUE)))
                    .joinEntity("esv", DQLJoinType.left, EnrExamSetDiscipline.class, "esDsc", and(
                        eq(property(EnrExamSetDiscipline.discipline().fromAlias("esDsc")), property(EnrChosenEntranceExam.discipline().fromAlias("x"))),
                        eq(property(EnrExamSetDiscipline.examSet().fromAlias("esDsc")), property(EnrExamSetVariant.examSet().fromAlias("esv")))
                    ))
                    .joinEntity("esDsc", DQLJoinType.left, EnrExamVariant.class, "exVar", and(
                        eq(property(EnrExamVariant.examSetElement().fromAlias("exVar")), property(EnrExamSetDiscipline.source().fromAlias("esDsc"))),
                        eq(property(EnrExamVariant.examSetVariant().fromAlias("exVar")), property("esv"))
                    ))
                    .set(EnrChosenEntranceExam.L_ACTUAL_EXAM, property("exVar"))
                    .where(neNullSafe(property(EnrChosenEntranceExam.L_ACTUAL_EXAM), property("exVar"))),
                    session
                );
            } finally {
                Debug.end();
                // System.out.println(Debug.end().toString());
            }

            // обновление ссылки ВВИ-Ф на форму из настроенной ВИ
            Debug.begin("enrChosenEntranceExamForm.actualExamPassForm");
            try {
                updates += executeAndClear(
                    new DQLUpdateBuilder(EnrChosenEntranceExamForm.class)
                    .fromEntity(EnrChosenEntranceExamForm.class, "x").where(eq(property("x.id"), property("id")))
                    .joinPath(DQLJoinType.inner, EnrChosenEntranceExamForm.chosenEntranceExam().fromAlias("x"), "chEe")
                    .joinEntity("chEe", DQLJoinType.left, EnrExamVariantPassForm.class, "exVarPf", and(
                        eq(property(EnrExamVariantPassForm.examVariant().fromAlias("exVarPf")), property(EnrChosenEntranceExam.actualExam().fromAlias("chEe"))),
                        eq(property(EnrExamVariantPassForm.passForm().fromAlias("exVarPf")), property(EnrChosenEntranceExamForm.passForm().fromAlias("x")))
                    ))
                    .set(EnrChosenEntranceExamForm.L_ACTUAL_EXAM_PASS_FORM, property("exVarPf"))
                    .where(neNullSafe(property(EnrChosenEntranceExamForm.L_ACTUAL_EXAM_PASS_FORM), property("exVarPf"))),
                    session
                );
            } finally {
                Debug.end();
            }

            // на основе только что заполненных данных ВВИ-ф и ВВИ устанавливает признак коректности выбранного набора
            Debug.begin("enrChosenEntranceExam.statusPassFormsCorrect");
            try {

                // сбрасываем признак (если хотябы один enrChosenEntranceExamForm.actualExamPassForm is null)
                // утсанавливаем признак (если все enrChosenEntranceExamForm.actualExamPassForm is not null)
                updates += this.updateBooleanField(
                    EnrChosenEntranceExam.class,
                    EnrChosenEntranceExam.statusPassFormsCorrect(),
                    new DQLSelectBuilder()
                    .fromEntity(EnrChosenEntranceExamForm.class, "tmp").column(property("tmp.id"))
                    .where(eq(property(EnrChosenEntranceExamForm.chosenEntranceExam().id().fromAlias("tmp")), property("id")))
                    .where(isNull(property(EnrChosenEntranceExamForm.actualExamPassForm().fromAlias("tmp")))),
                    Boolean.FALSE
                );

            } finally {
                Debug.end();
            }

            return updates > 0;
        } finally {
            Debug.end();
        }
    }

    ///////////////////////////////////////////////////////
    // выбор максимального основания и ВВИ-ф
    ///////////////////////////////////////////////////////

    private final Comparator<Object[]> doChooseSource_enrEntrantMarkSource_rowComparator = new Comparator<Object[]>()
    {
        @Override
        public int compare(final Object[] o1, final Object[] o2)
        {
            int i;

            // по убыванию балла
            if (0 != (i = Long.compare((Long)o1[2], (Long)o2[2]))) { return -i; }

            // по приоитету основания
            final int s1prio = this.getSourcePriority((Long)o1[1]);
            final int s2prio = this.getSourcePriority((Long)o2[1]);
            if (0 != (i = Integer.compare(s1prio, s2prio))) { return i; }

            // по убыванию даты изменения (если даты нет - то это начало времен)
            final long d1 = (null == o1[3] ? 0L : ((Date)o1[3]).getTime());
            final long d2 = (null == o2[3] ? 0L : ((Date)o2[3]).getTime());
            if (0 != (i = Long.compare(d1, d2))) { return -i; }

            // по id
            return Long.compare((Long)o1[1], (Long)o2[1]);
        }

        private final Map<Class, Integer> sourceClass2priorityMap = new HashMap<>();
        {
            this.sourceClass2priorityMap.put(EnrEntrantMarkSourceBenefit.class, 1);
            this.sourceClass2priorityMap.put(EnrEntrantMarkSourceStateExam.class, 2);
            this.sourceClass2priorityMap.put(EnrEntrantMarkSourceAppeal.class, 4);
            this.sourceClass2priorityMap.put(EnrEntrantMarkSourceExamPass.class, 5);
        }

        private int getSourcePriority(final Long sourceId)
        {
            final Class k =  EntityRuntime.getMeta(sourceId).getEntityClass();
            final Integer i = this.sourceClass2priorityMap.get(k);
            return (null == i ? Integer.MAX_VALUE : i);
        }
    };

    private final Comparator<Object[]> doChooseSource_enrChosenEntranceExamForm_rowComparator = new Comparator<Object[]>()
    {
        @Override
        public int compare(final Object[] o1, final Object[] o2)
        {
            int i;

            // по убыванию балла
            if (0 != (i = Long.compare((Long)o1[2], (Long)o2[2]))) { return -i; }

            // по приоитету формы сдачи
            final int pf1prio = this.getPassFormPriority((String)o1[4]);
            final int pf2prio = this.getPassFormPriority((String)o2[4]);
            if (0 != (i = Integer.compare(pf1prio, pf2prio))) { return i; }

            // по убыванию даты изменения (если даты нет - то это начало времен)
            final long d1 = (null == o1[3] ? 0L : ((Date)o1[3]).getTime());
            final long d2 = (null == o2[3] ? 0L : ((Date)o2[3]).getTime());
            if (0 != (i = Long.compare(d1, d2))) { return -i; }

            // по id
            return Long.compare((Long)o1[1], (Long)o2[1]);
        }

        private final Map<String, Integer> passFormCode2priorityMap = new HashMap<>();
        {
            // DEV-2780: Порядок форм сдачи при наличии одинаковых баллов:
            // Олимп., ЕГЭ, ЕГЭ (вуз), Экзамен, Тест, Собесед.
            this.passFormCode2priorityMap.put(EnrExamPassFormCodes.OLYMPIAD, 1);
            this.passFormCode2priorityMap.put(EnrExamPassFormCodes.STATE_EXAM, 2);
            this.passFormCode2priorityMap.put(EnrExamPassFormCodes.EXAM, 4);
            this.passFormCode2priorityMap.put(EnrExamPassFormCodes.TEST, 5);
            this.passFormCode2priorityMap.put(EnrExamPassFormCodes.INTERVIEW, 6);
        }

        private int getPassFormPriority(final String passFormCode)
        {
            final Integer i = this.passFormCode2priorityMap.get(passFormCode);
            return (null == i ? Integer.MAX_VALUE : i);
        }
    };

    @Override
    public boolean doChooseSource()
    {
        EnrEntrantDaemonBean.DAEMON.check();
        Debug.begin("EnrRatingDaemonDao.doChooseSource");
        try{
            int updates = 0;
            final Session session = this.lock(EnrChosenEntranceExam.class.getName());
            final Date now = new Date();

            // для ВВИ-ф выбираем основание с максимальным баллом (при равенстве баллов пользуемся приоритетом оснований)
            Debug.begin("enrChosenEntranceExamForm.markSource");
            try {

                // формируем перечень данных
                final Map<Long, List<Object[]>> rowMap = SafeMap.get(ArrayList.class);
                {
                    // для каждого ВВИ-ф грузим все его источники баллов (если хотябы в одном что-то изменилось)
                    final Iterable<Object[]> rows = scrollRows(
                        new DQLSelectBuilder()
                        .fromEntity(EnrEntrantMarkSource.class, "src")
                        .column(/*0*/property(EnrEntrantMarkSource.chosenEntranceExamForm().id().fromAlias("src")))
                        .column(/*1*/property(EnrEntrantMarkSource.id().fromAlias("src")))
                        .column(/*2*/property(EnrEntrantMarkSource.markAsLong().fromAlias("src")))
                        .column(/*3*/property(EnrEntrantMarkSource.markTimestamp().fromAlias("src")))
                        .where(in(
                            property(EnrEntrantMarkSource.chosenEntranceExamForm().id().fromAlias("src")),
                            new DQLSelectBuilder()
                            .predicate(DQLPredicateType.distinct)
                            .fromEntity(EnrEntrantMarkSource.class, "tmp")
                            .column(property(EnrEntrantMarkSource.chosenEntranceExamForm().id().fromAlias("tmp")))
                            .where(or(
                                isNull(property(EnrEntrantMarkSource.chosenEntranceExamForm().markSource().fromAlias("tmp"))),
                                isNull(property(EnrEntrantMarkSource.chosenEntranceExamForm().markSourceTimestamp().fromAlias("tmp"))),
                                lt(property(EnrEntrantMarkSource.chosenEntranceExamForm().markSourceTimestamp().fromAlias("tmp")), property(EnrEntrantMarkSource.markTimestamp().fromAlias("tmp")))
                            ))
                            .buildQuery()
                        ))
                        .createStatement(session)
                    );

                    for (final Object[] row: rows) {
                        final Long id = (Long)row[0];
                        rowMap.get(id).add(row);
                    }
                }

                // обновляем источники
                if (rowMap.size() > 0)
                {
                    // XXX: весь этот цирк нужен, чтобы изменить в одной транзакции много объектов
                    // XXX: на каждый update будет своя временная таблица (листенеры) - поэтом массовые оперци надо группировать
                    BatchUtils.execute(rowMap.entrySet(), DQL.MAX_VALUES_ROW_NUMBER, elements -> {
                        final DQLValuesBuilder valuesBuilder = new DQLValuesBuilder(
                            new String[] { "id", "marksrc_id" },
                            new PropertyType[] { PropertyType.LONG, PropertyType.LONG }
                        );

                        for (final Entry<Long, List<Object[]>> e: elements) {
                            final List<Object[]> rows = e.getValue();
                            if (rows.isEmpty()) { throw new IllegalStateException(/*wtf?*/); }

                            // источники есть - обрабатываем (если их больше одного - сортируем)
                            if (rows.size() > 1) {
                                Collections.sort(rows, EnrRatingDaemonDao.this.doChooseSource_enrEntrantMarkSource_rowComparator);
                            }

                            // формируем фрагмент данных
                            final Object[] top = rows.get(0);
                            valuesBuilder.row(value(e.getKey()), commonValue(top[1], PropertyType.LONG));
                        }

                        new DQLUpdateBuilder(EnrChosenEntranceExamForm.class)
                        .fromDataSource(valuesBuilder.build(), "src")
                        .where(eq(property("id"), property("src.id")))
                        .set(EnrChosenEntranceExamForm.L_MARK_SOURCE, property("src.marksrc_id"))
                        .set(EnrChosenEntranceExamForm.P_MARK_SOURCE_TIMESTAMP, value(now, PropertyType.TIMESTAMP)) // дата обновляется всегда, даже если ничего не изменилось
                        .createStatement(session).execute();
                    });

                    updates += rowMap.size();
                }

                // удаляем дату для пустых источников
                updates += new DQLUpdateBuilder(EnrChosenEntranceExamForm.class)
                .set(EnrChosenEntranceExamForm.P_MARK_SOURCE_TIMESTAMP, nul(PropertyType.TIMESTAMP))
                .where(isNotNull(property(EnrChosenEntranceExamForm.P_MARK_SOURCE_TIMESTAMP)))
                .where(isNull(property(EnrChosenEntranceExamForm.L_MARK_SOURCE)))
                .createStatement(session).execute();

            } finally {
                Debug.end();
            }

            // для ВВИ выбираем ВВИ-ф с максимальным баллом (при равенстве баллов пользуемся приоритетом форм)
            Debug.begin("enrChosenEntranceExam.maxMarkForm");
            try {

                // удяляем ссылку на ВВИ-ф (и дату), если по ней нет оценки
                updates += new DQLUpdateBuilder(EnrChosenEntranceExam.class)
                .set(EnrChosenEntranceExam.L_MAX_MARK_FORM, nul(PropertyType.LONG))
                .set(EnrChosenEntranceExam.P_MAX_MARK_FORM_TIMESTAMP, nul(PropertyType.TIMESTAMP))
                .where(or(
                    isNotNull(property(EnrChosenEntranceExam.L_MAX_MARK_FORM)),
                    isNotNull(property(EnrChosenEntranceExam.P_MAX_MARK_FORM_TIMESTAMP))
                ))
                .where(isNull(property(EnrChosenEntranceExam.maxMarkForm().markSource())))
                .createStatement(session).execute();

                // формируем перечень данных
                final Map<Long, List<Object[]>> rowMap = SafeMap.get(ArrayList.class);
                {
                    // для каждого ВВИ грузим все его ВВИ-ф (если хотябы в одном что-то изменилось)
                    final Iterable<Object[]> rows = scrollRows(
                        new DQLSelectBuilder()
                        .fromEntity(EnrChosenEntranceExamForm.class, "src")
                        .where(isNotNull(property(EnrChosenEntranceExamForm.actualExamPassForm().fromAlias("src")))) // ВВИ-ф должен соответствовать разрешенной форме в КГ
                        .where(isNotNull(property(EnrChosenEntranceExamForm.markSource().fromAlias("src")))) // ВВИ-Ф должен содержать балл
                        .column(/*0*/property(EnrChosenEntranceExamForm.chosenEntranceExam().id().fromAlias("src")))
                        .column(/*1*/property(EnrChosenEntranceExamForm.id().fromAlias("src")))
                        .column(/*2*/property(EnrChosenEntranceExamForm.markSource().markAsLong().fromAlias("src")))
                        .column(/*3*/property(EnrChosenEntranceExamForm.markSourceTimestamp().fromAlias("src")))
                        .column(/*4*/property(EnrChosenEntranceExamForm.passForm().code().fromAlias("src")))
                        .where(in(
                            property(EnrChosenEntranceExamForm.chosenEntranceExam().id().fromAlias("src")),
                            new DQLSelectBuilder()
                            .fromEntity(EnrChosenEntranceExamForm.class, "tmp")
                            .predicate(DQLPredicateType.distinct)
                            .column(property(EnrChosenEntranceExamForm.chosenEntranceExam().id().fromAlias("tmp")))
                            .where(or(
                                isNull(property(EnrChosenEntranceExamForm.chosenEntranceExam().maxMarkForm().fromAlias("tmp"))),
                                isNull(property(EnrChosenEntranceExamForm.chosenEntranceExam().maxMarkFormTimestamp().fromAlias("tmp"))),
                                lt(property(EnrChosenEntranceExamForm.chosenEntranceExam().maxMarkFormTimestamp().fromAlias("tmp")), property(EnrChosenEntranceExamForm.markSourceTimestamp().fromAlias("tmp")))
                            ))
                            .buildQuery()
                        ))
                        .createStatement(session)
                    );

                    for (final Object[] row: rows) {
                        final Long id = (Long)row[0];
                        rowMap.get(id).add(row);
                    }
                }

                // обновляем максимальный ВВИ-ф
                if (rowMap.size() > 0)
                {
                    // XXX: весь этот цирк нужен, чтобы изменить в одной транзакции много объектов
                    // XXX: на каждый update будет своя временная таблица (листенеры) - поэтом массовые оперци надо группировать
                    BatchUtils.execute(rowMap.entrySet(), DQL.MAX_VALUES_ROW_NUMBER, elements -> {
                        final DQLValuesBuilder valuesBuilder = new DQLValuesBuilder(
                            new String[] { "id", "markform_id" },
                            new PropertyType[] { PropertyType.LONG, PropertyType.LONG }
                        );

                        for (final Entry<Long, List<Object[]>> e: elements) {
                            final List<Object[]> rows = e.getValue();
                            if (rows.isEmpty()) { throw new IllegalStateException(/*wtf?*/); }

                            // источники есть - обрабатываем
                            if (rows.size() > 1) {
                                Collections.sort(rows, EnrRatingDaemonDao.this.doChooseSource_enrChosenEntranceExamForm_rowComparator);
                            }

                            // формируем фрагмент данных
                            final Object[] top = rows.get(0);
                            valuesBuilder.row(value(e.getKey()), commonValue(top[1], PropertyType.LONG));
                        }

                        new DQLUpdateBuilder(EnrChosenEntranceExam.class)
                        .fromDataSource(valuesBuilder.build(), "src")
                        .where(eq(property("id"), property("src.id")))
                        .set(EnrChosenEntranceExam.L_MAX_MARK_FORM, property("src.markform_id"))
                        .set(EnrChosenEntranceExam.P_MAX_MARK_FORM_TIMESTAMP, value(now, PropertyType.TIMESTAMP)) // дата обновляется всегда, даже если ничего не изменилось
                        .createStatement(session).execute();
                    });

                    updates += rowMap.size();
                }

                // удяляем дату для путсых ВВИ-ф
                updates += new DQLUpdateBuilder(EnrChosenEntranceExam.class)
                .set(EnrChosenEntranceExam.P_MAX_MARK_FORM_TIMESTAMP, nul(PropertyType.TIMESTAMP))
                .where(isNotNull(property(EnrChosenEntranceExam.P_MAX_MARK_FORM_TIMESTAMP)))
                .where(isNull(property(EnrChosenEntranceExam.L_MAX_MARK_FORM)))
                .createStatement(session).execute();

            } finally {
                Debug.end();
            }

            // для ВВИ проеряем, что по всем актуальным ВВИ-ф есть оценки
            Debug.begin("enrChosenEntranceExam.statusMaxMarkComplete");
            try {

                // сбрасываем признак (если хотябы один актуальный enrChosenEntranceExamForm.markSource is null)
                // утсанавливаем признак (если все актуальные enrChosenEntranceExamForm.markSource is not null)
                updates += this.updateBooleanField(
                    EnrChosenEntranceExam.class,
                    EnrChosenEntranceExam.statusMaxMarkComplete(),
                    new DQLSelectBuilder()
                    .fromEntity(EnrChosenEntranceExamForm.class, "tmp").column(property("tmp.id"))
                    .where(isNotNull(property(EnrChosenEntranceExamForm.actualExamPassForm().fromAlias("tmp"))))
                    .where(eq(property(EnrChosenEntranceExamForm.chosenEntranceExam().id().fromAlias("tmp")), property("id")))
                    .where(isNull(property(EnrChosenEntranceExamForm.markSource().fromAlias("tmp")))),
                    Boolean.FALSE
                );

            } finally {
                Debug.end();
            }

            return updates > 0;
        } finally {
            Debug.end();
        }
    }

    ///////////////////////////////////////////////////////
    // обновление оснований баллов
    ///////////////////////////////////////////////////////


    @Override
    public boolean doRefreshSources()
    {
        boolean result = doRefreshSource4Benefit();
        result = doRefreshSource4StateExam() | result;
        result = doRefreshSource4ExamPass() | result;
        result = doRefreshSource4Appeal() | result;

        return result;
    }

    private final boolean crazyCheck = false;
    private final Map<String, Long> enrExamPassFormIdMap = this.getSystemCatalogIdMap(EnrExamPassForm.class);

    /* ДДС - вторые по количеству данных источники */
    private boolean doRefreshSource4ExamPass()
    {
        EnrEntrantDaemonBean.DAEMON.check();
        Debug.begin("EnrRatingDaemonDao.doRefreshSource4ExamPass");
        try {
            return new EnrRatingDaemonMarkSourceUpdate<EnrEntrantMarkSourceExamPass>(EnrEntrantMarkSourceExamPass.class, this.getSession()) {
                @Override
                protected DQLSelectBuilder buildMainDataSource() {
                    return new DQLSelectBuilder()
                            .fromEntity(EnrChosenEntranceExamForm.class, "chef")
                            .column(property(EnrChosenEntranceExamForm.id().fromAlias("chef")), "chef_id")

                            .fromEntity(EnrExamPassDiscipline.class, "expd")
                            .column(property(EnrExamPassDiscipline.id().fromAlias("expd")), "expd_id")
                            .column(property(EnrExamPassDiscipline.markAsLong().fromAlias("expd")), "expd_mark")
                            .where(isNotNull(property(EnrExamPassDiscipline.markAsLong().fromAlias("expd")))) // здесь учитывается то, что оценка и неявка связаны через констрейнт: enrExamPassDisciplineMarkAndAbsence
                            .where(eq(property(EnrExamPassDiscipline.passForm().fromAlias("expd")), property(EnrChosenEntranceExamForm.passForm().fromAlias("chef"))))
                            .where(eq(property(EnrExamPassDiscipline.entrant().fromAlias("expd")), property(EnrChosenEntranceExamForm.chosenEntranceExam().requestedCompetition().request().entrant().fromAlias("chef"))))
                            .where(eq(property(EnrExamPassDiscipline.discipline().fromAlias("expd")), property(EnrChosenEntranceExamForm.chosenEntranceExam().discipline().fromAlias("chef"))))
                            .distinct();
                }

                @Override
                protected IDQLExpression buildJoinExpression(final String srcAlias) {
                    return and(
                            eq(property("chef"), property(srcAlias, EnrEntrantMarkSourceExamPass.L_CHOSEN_ENTRANCE_EXAM_FORM)),
                            eq(property("expd"), property(srcAlias, EnrEntrantMarkSourceExamPass.L_EXAM_PASS_DISCIPLINE))
                    );
                }

                @Override
                protected boolean insertSource(final Object[] row) {
                    final EnrChosenEntranceExamForm chef = (EnrChosenEntranceExamForm)this.session.load(EnrChosenEntranceExamForm.class, (Long)row[0]);
                    final EnrExamPassDiscipline expd = (EnrExamPassDiscipline)this.session.load(EnrExamPassDiscipline.class, (Long)row[1]);
                    final Long value = (Long)row[2];
                    this.session.save(new EnrEntrantMarkSourceExamPass(chef, expd, (null == value ? 0L : value)));
                    return true;
                }

                @Override
                protected int executeUpdate() {
                    final Date now = new Date();

                    return EnrRatingDaemonDao.this.executeAndClear(
                            new DQLUpdateBuilder(this.klass)
                                    .fromDataSource(
                                            this.buildMainDataSource()
                                                    .column(property(EnrExamPassDiscipline.modificationDate().fromAlias("expd")), "expd_date")
                                                    .buildQuery(),
                                            "ds"
                                    )
                                    .where(and(
                                            eq(property(EnrEntrantMarkSourceExamPass.L_EXAM_PASS_DISCIPLINE), property("ds.expd_id")) /* остальные условия не нужны - обновлять нужно все, что ссылается на ДДС */
                                    ))
                                    .where(or(
                                            ne(property(EnrEntrantMarkSource.P_MARK_AS_LONG), property("ds.expd_mark")),
                                            lt(property(EnrEntrantMarkSource.P_MARK_TIMESTAMP), property("ds.expd_date"))
                                    ))
                                    .set(EnrEntrantMarkSource.P_MARK_AS_LONG, property("ds.expd_mark"))
                                    .set(EnrEntrantMarkSource.P_MARK_TIMESTAMP, valueTimestamp(now))
                    );
                }

            }.execute() > 0;
        } finally {
            Debug.end();
        }
    }

    /* ЕГЭ - эти данные будут составлять большинство данных о источниках баллов */
    private boolean doRefreshSource4StateExam()
    {
        EnrEntrantDaemonBean.DAEMON.check();
        Debug.begin("EnrRatingDaemonDao.doRefreshSource4StateExam");
        try {
            final Date now = new Date();
            int result = 0;

            Debug.begin("secondWaveNoMarkRelation");
            try {
                // сбрасываем ссылку (если там появилась оценка либо изменился признак secondWave)
                result += this.executeAndClear(
                    new DQLUpdateBuilder(EnrEntrantStateExamResult.class)
                    .set(EnrEntrantStateExamResult.L_SECOND_WAVE_NO_MARK_RELATION, nul(PropertyType.LONG))
                    .where(or(
                        isNotNull(EnrEntrantStateExamResult.secondWaveNoMarkRelation().markAsLong()),
                        ne(property(EnrEntrantStateExamResult.secondWaveNoMarkRelation().secondWave()), value(Boolean.TRUE))
                    ))
                );

                // сбрасываем ссылку (если в текущем объекте нет балла)
                result += this.executeAndClear(
                    new DQLUpdateBuilder(EnrEntrantStateExamResult.class)
                    .set(EnrEntrantStateExamResult.L_SECOND_WAVE_NO_MARK_RELATION, nul(PropertyType.LONG))
                    .where(isNull(EnrEntrantStateExamResult.P_MARK_AS_LONG))
                    .where(isNotNull(EnrEntrantStateExamResult.L_SECOND_WAVE_NO_MARK_RELATION))
                );

                // формируем ссылку (считаем, что результат НГЭ второй волны без балла единсвенный)
                result += this.executeAndClear(
                    new DQLUpdateBuilder(EnrEntrantStateExamResult.class)

                    .fromEntity(EnrEntrantStateExamResult.class, "x")
                    .where(eq(property(EnrEntrantStateExamResult.L_ENTRANT), property("x", EnrEntrantStateExamResult.L_ENTRANT)))
                    .where(eq(property(EnrEntrantStateExamResult.L_SUBJECT), property("x", EnrEntrantStateExamResult.L_SUBJECT)))
                    .where(eq(property("x", EnrEntrantStateExamResult.P_SECOND_WAVE), value(Boolean.TRUE)))
                    .where(isNull(property("x", EnrEntrantStateExamResult.P_MARK_AS_LONG)))

                    .set(EnrEntrantStateExamResult.L_SECOND_WAVE_NO_MARK_RELATION, property("x"))
                    .where(isNotNull(EnrEntrantStateExamResult.P_MARK_AS_LONG))
                    .where(isNull(EnrEntrantStateExamResult.L_SECOND_WAVE_NO_MARK_RELATION))
                );
            } finally {
                Debug.end();
            }

            final Long passFormId = this.enrExamPassFormIdMap.get(EnrExamPassFormCodes.STATE_EXAM);

            Debug.begin("delete");
            try {

                if (this.crazyCheck) {
                    // убираем те источники баллов, которые не соответсвуют абитуриенту или дисциплине (как же так)
                    // возможно, что этот запрос нужно запускать только в дебаге (т.к. все поля иммутаьельны по смыслу)
                    result += this.executeAndClear(
                        new DQLDeleteBuilder(EnrEntrantMarkSourceStateExam.class)
                        .where(or(
                            ne(property(EnrEntrantMarkSourceStateExam.stateExamResult().entrant()), property(EnrEntrantMarkSourceStateExam.chosenEntranceExamForm().chosenEntranceExam().requestedCompetition().request().entrant())),
                            ne(property(EnrEntrantMarkSourceStateExam.stateExamResult().subject()), property(EnrEntrantMarkSourceStateExam.chosenEntranceExamForm().chosenEntranceExam().discipline().stateExamSubject())),
                            ne(property(EnrEntrantMarkSourceStateExam.chosenEntranceExamForm().passForm().id()), value(passFormId))
                        ))
                    );
                }

                // убираем те источники баллов, которые ссылаются на ЕГЭ без баллов, либо на ЕГЭ без зачтения
                // (возможно, имеет смысл это объединить с предыдущим запросом)
                result += this.executeAndClear(
                    new DQLDeleteBuilder(EnrEntrantMarkSourceStateExam.class)
                    .where(or(
                        isNull(property(EnrEntrantMarkSourceStateExam.stateExamResult().markAsLong())),
                        ne(property(EnrEntrantMarkSourceStateExam.stateExamResult().accepted()), value(Boolean.TRUE))
                    ))
                );

                // убираем те источники баллов, которые ссылаются на ЕГЭ с непустой ссылкой на вторую волну, баллы по которым мельше проходного
                result += this.executeAndClear(
                    new DQLDeleteBuilder(EnrEntrantMarkSourceStateExam.class)
                    .where(isNotNull(property(EnrEntrantMarkSourceStateExam.stateExamResult().secondWaveNoMarkRelation())))
                    .where(lt(property(EnrEntrantMarkSourceStateExam.stateExamResult().markAsLong()), property(EnrEntrantMarkSourceStateExam.chosenEntranceExamForm().actualExamPassForm().examVariant().passMarkAsLong())))
                );

            } finally {
                Debug.end();
            }

            Debug.begin("insert");
            try {
                final Callable<DQLSelectBuilder> dqlBuilder = () -> new DQLSelectBuilder()

                .fromEntity(EnrChosenEntranceExamForm.class, "chef")
                .joinPath(DQLJoinType.inner, EnrChosenEntranceExamForm.chosenEntranceExam().fromAlias("chef"), "che")
                .where(eq(property(EnrChosenEntranceExamForm.passForm().id().fromAlias("chef")), value(passFormId)))

                .fromEntity(EnrEntrantStateExamResult.class, "semk")
                .where(isNotNull(property(EnrEntrantStateExamResult.markAsLong().fromAlias("semk"))))
                .where(eq(property(EnrEntrantStateExamResult.accepted().fromAlias("semk")), value(Boolean.TRUE)))

                .where(eq(property(EnrEntrantStateExamResult.entrant().fromAlias("semk")), property(EnrChosenEntranceExam.requestedCompetition().request().entrant().fromAlias("che"))))
                .where(eq(property(EnrEntrantStateExamResult.subject().fromAlias("semk")), property(EnrChosenEntranceExam.discipline().stateExamSubject().fromAlias("che"))))

                .where(notExists(
                        new DQLSelectBuilder()
                                .fromEntity(EnrEntrantMarkSourceStateExam.class, "src")
                                .column(property("src.id"))
                                .where(eq(property("chef"), property(EnrEntrantMarkSourceStateExam.chosenEntranceExamForm().fromAlias("src"))))
                                .where(eq(property("semk"), property(EnrEntrantMarkSourceStateExam.stateExamResult().fromAlias("src"))))
                                .buildQuery()
                ))

                .column(/*0*/property(EnrChosenEntranceExamForm.id().fromAlias("chef")), "chef_id")
                .column(/*1*/property(EnrEntrantStateExamResult.id().fromAlias("semk")), "semk_id")
                .column(/*2*/property(EnrEntrantStateExamResult.markAsLong().fromAlias("semk")), "semk_mark");

                final Session session = this.getSession();
                final List<Object[]> rows = new ArrayList<>();

                // все ЕГЭ, не связанные с ЕГЭ второй волны без баллов
                rows.addAll(
                    dqlBuilder.call()
                    .where(isNull(property(EnrEntrantStateExamResult.secondWaveNoMarkRelation().fromAlias("semk"))))
                    .createStatement(session).<Object[]>list()
                );

                // все ЕГЭ, связанные с ЕГЭ второй волны без баллов (баллы которых не ниже зачетных)
                rows.addAll(
                    dqlBuilder.call()
                    .where(isNotNull(property(EnrEntrantStateExamResult.secondWaveNoMarkRelation().fromAlias("semk"))))
                    .where(ge(property(EnrEntrantStateExamResult.markAsLong().fromAlias("semk")), property(EnrChosenEntranceExam.actualExam().passMarkAsLong().fromAlias("che"))))
                    .createStatement(session).<Object[]>list()
                );

                // сохраняем все в базу
                for (final Object[] row: rows) {
                    final EnrChosenEntranceExamForm chef = (EnrChosenEntranceExamForm)session.load(EnrChosenEntranceExamForm.class, (Long)row[0]);
                    final EnrEntrantStateExamResult mark = (EnrEntrantStateExamResult)session.load(EnrEntrantStateExamResult.class, (Long)row[1]);
                    final Long value = (Long)row[2];
                    final EnrEntrantMarkSourceStateExam source = new EnrEntrantMarkSourceStateExam(chef, mark, value);
                    source.setMarkTimestamp(now);
                    session.save(source);
                    result ++;
                }
                session.flush();
                session.clear();

            } finally {
                Debug.end();
            }

            Debug.begin("update");
            try {
                result += this.executeAndClear(
                    new DQLUpdateBuilder(EnrEntrantMarkSourceStateExam.class)
                    .fromEntity(EnrEntrantStateExamResult.class, "x")
                    .where(eq(property(EnrEntrantMarkSourceStateExam.L_STATE_EXAM_RESULT), property("x")) /* остальные условия не нужны - обновлять нужно все, что ссылается на запись ЕГЭ */)
                    .where(or(
                        ne(property(EnrEntrantMarkSource.P_MARK_AS_LONG), property(EnrEntrantStateExamResult.markAsLong().fromAlias("x"))),
                        lt(property(EnrEntrantMarkSource.P_MARK_TIMESTAMP), property(EnrEntrantStateExamResult.modificationDate().fromAlias("x")))
                    ))
                    .set(EnrEntrantMarkSource.P_MARK_AS_LONG, property(EnrEntrantStateExamResult.markAsLong().fromAlias("x")))
                    .set(EnrEntrantMarkSource.P_MARK_TIMESTAMP, valueTimestamp(now))
                );
            } finally {
                Debug.end();
            }

            return result > 0;

        } catch (final Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        } finally {
            Debug.end();
        }
    }

    /* этих мало */
    private boolean doRefreshSource4Appeal()
    {
        EnrEntrantDaemonBean.DAEMON.check();
        Debug.begin("EnrRatingDaemonDao.doRefreshSource4Appeal");
        try {
            // здесь пользуемся тем, что источник балла по ДДС должен быть, поэтому линкуем на него (а не на ДДС из аппеляции)

            return new EnrRatingDaemonMarkSourceUpdate<EnrEntrantMarkSourceAppeal>(EnrEntrantMarkSourceAppeal.class, this.getSession()) {

                @Override
                protected DQLSelectBuilder buildMainDataSource() {
                    return new DQLSelectBuilder()
                    .fromEntity(EnrEntrantMarkSourceExamPass.class, "xxx")
                    .column(property(EnrEntrantMarkSourceExamPass.chosenEntranceExamForm().id().fromAlias("xxx")), "chef_id")

                    .fromEntity(EnrExamPassAppeal.class, "appeal")
                    .column(property(EnrExamPassAppeal.id().fromAlias("appeal")), "appeal_id")
                    .column(property(EnrExamPassAppeal.markAsLong().fromAlias("appeal")), "appeal_mark")

                    .where(eq(property(EnrExamPassAppeal.examPassDiscipline().fromAlias("appeal")), property(EnrEntrantMarkSourceExamPass.examPassDiscipline().fromAlias("xxx"))));
                }

                @Override
                protected IDQLExpression buildJoinExpression(final String srcAlias) {
                    return and(
                        eq(property("xxx", EnrEntrantMarkSourceExamPass.L_CHOSEN_ENTRANCE_EXAM_FORM), property(srcAlias, EnrEntrantMarkSourceAppeal.L_CHOSEN_ENTRANCE_EXAM_FORM)),
                        eq(property("appeal"), property(srcAlias, EnrEntrantMarkSourceAppeal.L_EXAM_PASS_APPEAL))
                    );
                }

                @Override
                protected boolean insertSource(final Object[] row) {
                    final EnrChosenEntranceExamForm chef = (EnrChosenEntranceExamForm)this.session.load(EnrChosenEntranceExamForm.class, (Long)row[0]);
                    final EnrExamPassAppeal appeal = (EnrExamPassAppeal)this.session.load(EnrExamPassAppeal.class, (Long)row[1]);
                    final Long value = (Long)row[2];
                    this.session.save(new EnrEntrantMarkSourceAppeal(chef, appeal, (null == value ? 0L : value)));
                    return true;
                }

                @Override
                protected int executeUpdate() {
                    final Date now = new Date();

                    return EnrRatingDaemonDao.this.executeAndClear(
                        new DQLUpdateBuilder(this.klass)
                        .fromDataSource(
                            this.buildMainDataSource()
                            .column(property(EnrExamPassAppeal.modificationDate().fromAlias("appeal")), "appeal_date")
                            .buildQuery(),
                            "ds"
                        )
                        .where(and(
                            eq(property(EnrEntrantMarkSourceAppeal.L_EXAM_PASS_APPEAL), property("ds.appeal_id")) /* остальные условия не нужны - обновлять нужно все, что ссылается на апелляцию */
                        ))
                        .where(or(
                            ne(property(EnrEntrantMarkSourceAppeal.P_MARK_AS_LONG), property("ds.appeal_mark")),
                            lt(property(EnrEntrantMarkSourceAppeal.P_MARK_TIMESTAMP), property("ds.appeal_date"))
                        ))
                        .set(EnrEntrantMarkSourceAppeal.P_MARK_AS_LONG, property("ds.appeal_mark"))
                        .set(EnrEntrantMarkSourceAppeal.P_MARK_TIMESTAMP, valueTimestamp(now))
                    );
                }

            }.execute() > 0;

        } finally {
            Debug.end();
        }
    }

    /* этих мало, они создаются руками */
    private boolean doRefreshSource4Benefit()
    {
        EnrEntrantDaemonBean.DAEMON.check();
        Debug.begin("EnrRatingDaemonDao.doRefreshSource4Olympiad");
        try {
            final Date now = new Date();
            final long mark = 100_000L; // выставляем 100 баллов
            return executeAndClear(
                new DQLUpdateBuilder(EnrEntrantMarkSourceBenefit.class)
                .set(EnrEntrantMarkSource.P_MARK_TIMESTAMP, valueTimestamp(now))
                .set(EnrEntrantMarkSource.P_MARK_AS_LONG, value(mark))
                .where(or(
                    isNull(property(EnrEntrantMarkSource.P_MARK_AS_LONG)),
                    isNull(property(EnrEntrantMarkSource.P_MARK_TIMESTAMP)),
                    ne(property(EnrEntrantMarkSource.P_MARK_AS_LONG), value(mark))
                )),
                this.getSession()
            ) > 0;
        } finally {
            Debug.end();
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////
    // обновление баллов за индивидуальные достижения
    ///////////////////////////////////////////////////////////////////////////////////

    @Override
    public boolean doUpdateAchievementMark()
    {
        EnrEntrantDaemonBean.DAEMON.check();
        Debug.begin("EnrRatingDaemonDao.doUpdateAchievementMark");
        try {
            int count = 0;

            // кэшируем балл - берем из типа ИД
            count += this.executeAndClear(
                new DQLUpdateBuilder(EnrEntrantAchievement.class)
                    .fromDataSource(
                        new DQLSelectBuilder()
                            .fromEntity(EnrEntrantAchievementType.class, "t")
                            .column(property("t.id"), "tid")
                            .column(property("t", EnrEntrantAchievementType.achievementMarkAsLong()), "mark")
                            .where(eq(property("t", EnrEntrantAchievementType.marked()), value(Boolean.FALSE)))
                            .buildQuery(),
                        "ds"
                    )
                    .where(eq(property(EnrEntrantAchievement.L_TYPE), property("ds.tid")))
                    .where(ne(property(EnrEntrantAchievement.P_RATING_MARK_AS_LONG), property("ds.mark")))
                    .set(EnrEntrantAchievement.P_RATING_MARK_AS_LONG, property("ds.mark"))
            );
            // кэшируем балл - обрезаем превышение
            count += this.executeAndClear(
                new DQLUpdateBuilder(EnrEntrantAchievement.class)
                    .fromDataSource(
                        new DQLSelectBuilder()
                            .fromEntity(EnrEntrantAchievementType.class, "t")
                            .column(property("t.id"), "tid")
                            .column(property("t", EnrEntrantAchievementType.achievementMarkAsLong()), "mark")
                            .buildQuery(),
                        "ds"
                    )
                    .where(eq(property(EnrEntrantAchievement.L_TYPE), property("ds.tid")))
                    .where(gt(property(EnrEntrantAchievement.P_RATING_MARK_AS_LONG), property("ds.mark")))
                    .set(EnrEntrantAchievement.P_RATING_MARK_AS_LONG, property("ds.mark"))
            );
            // кэшируем балл - восстанавливаем
            count += this.executeAndClear(
                new DQLUpdateBuilder(EnrEntrantAchievement.class)
                    .fromDataSource(
                        new DQLSelectBuilder()
                            .fromEntity(EnrEntrantAchievement.class, "e")
                            .column(property("e.id"), "eid")
                            .column(property("e", EnrEntrantAchievement.markAsLong()), "mark")
                            .where(eq(property("e", EnrEntrantAchievement.type().marked()), value(Boolean.TRUE)))
                            .where(le(property("e", EnrEntrantAchievement.markAsLong()), property("e", EnrEntrantAchievement.type().achievementMarkAsLong())))
                            .buildQuery(),
                        "ds"
                    )
                    .where(eq(property(EnrEntrantAchievement.P_ID), property("ds.eid")))
                    .where(ne(property(EnrEntrantAchievement.P_RATING_MARK_AS_LONG), property("ds.mark")))
                    .set(EnrEntrantAchievement.P_RATING_MARK_AS_LONG, property("ds.mark"))
            );

            // сбрасываем для всех, у кого нет ни одного достижения
            count += this.executeAndClear(
                new DQLUpdateBuilder(EnrEntrantRequest.class)
                .where(notExists(
                    new DQLSelectBuilder()
                    .fromEntity(EnrEntrantAchievement.class, "a")
                    .column(property("a.id"))
                    .where(eq(property(EnrEntrantAchievement.entrant().enrollmentCampaign().fromAlias("a")), property(EnrEntrantAchievement.type().enrollmentCampaign().fromAlias("a")))) /* убрать в констрейнт */
                    .where(eq(property(EnrEntrantAchievement.entrant().fromAlias("a")), property(EnrEntrantRequest.entrant())))
                    .where(eq(property(EnrEntrantAchievement.type().achievementKind().requestType().fromAlias("a")), property(EnrEntrantRequest.type())))
                    .buildQuery()
                ))
                .where(ne(property(EnrEntrantRequest.P_ENTRANT_ACHIEVEMENT_MARK_SUM), value(0L)))
                .set(EnrEntrantRequest.P_ENTRANT_ACHIEVEMENT_MARK_SUM, value(0L))
            );


            // пересчитываем тем, у кого есть хотя бы одно
            count += this.executeAndClear(
                new DQLUpdateBuilder(EnrEntrantRequest.class)
                    .fromDataSource(
                        new DQLSelectBuilder()
                            .fromEntity(EnrEntrantRequest.class, "rq")
                            .column(property("rq.id"), "request_id")
                            .fromEntity(EnrEntrantAchievement.class, "a")
                            .column(sum(property(EnrEntrantAchievement.ratingMarkAsLong().fromAlias("a"))), "mark_sum")
                            .group(property("rq.id"))
                            .where(eq(property(EnrEntrantAchievement.entrant().enrollmentCampaign().fromAlias("a")), property(EnrEntrantAchievement.type().enrollmentCampaign().fromAlias("a")))) /* убрать в констрейнт */
                            .where(eq(property(EnrEntrantAchievement.entrant().fromAlias("a")), property(EnrEntrantRequest.entrant().fromAlias("rq"))))
                            .where(eq(property(EnrEntrantAchievement.type().achievementKind().requestType().fromAlias("a")), property(EnrEntrantRequest.type().fromAlias("rq"))))
                            .where(or(
                                    eq(property("a", EnrEntrantAchievement.type().forRequest()), value(false)),
                                    and(
                                            eq(property("a", EnrEntrantAchievement.type().forRequest()), value(true)),
                                            eq(property("a", EnrEntrantAchievement.request()), property("rq"))
                                    )
                            ))
                            .buildQuery(),
                        "ds"
                    )
                    .where(eq(property("id"), property("ds.request_id")))
                    .where(ne(property(EnrEntrantRequest.P_ENTRANT_ACHIEVEMENT_MARK_SUM), property("ds.mark_sum")))
                    .set(EnrEntrantRequest.P_ENTRANT_ACHIEVEMENT_MARK_SUM, property("ds.mark_sum"))
            );
            return (count > 0);
        } finally {
            Debug.end();
        }
    }

    ///////////////////////////////////////////////////////
    // обновление данных рейтинга (загрузка и обработка данных в несколько потоков, сохранение)
    ///////////////////////////////////////////////////////

    @Override
    public boolean doUpdateRatingList()
    {
        EnrEntrantDaemonBean.DAEMON.check();
        Debug.begin("EnrRatingDaemonDao.doUpdateRatingList");
        try
        {
            final IEnrRatingListDao dao = IEnrRatingListDao.instance.get();

            // берем перечень всех используемых конкурсов
            final List<EntrantRatingCompetitionData> competitionList = dao.getAllCompetitionIds();

            // считаем, что метод EnrRatingListDao.doPrepareRatingItems уже успешно отработал
            // заполнем источники для рейтинговых списков (чтение из базы в несколько потоков)
            final Map<EntrantRatingCompetitionData, List<EntrantRatingData>> ratingDataListMap = new HashMap<>(competitionList.size());
            if (competitionList.isEmpty()) { return false; }

            // в потоках анализируем все используемые конкурсы, формируем перечень рейтингов
            Debug.begin("competitionIds.size = " + competitionList.size());
            final SyncDaemon.ExecutorService executor = EnrEntrantDaemonBean.DAEMON.getExecutorService(false, RATING_ITEMS_UPDATE_THREAD_COUNT.get());
            try {
                // секция, в которую будем складывать результаты работы подпотоков
                for (final EntrantRatingCompetitionData competitionData: competitionList) {
                    executor.submit(new Runnable() {
                        @Override public String toString() {
                            return "competition=" + competitionData.getCompetitionId();
                        }
                        @Override public void run() {
                            // в потоке независимо вычисляем перечень элементов (затем фильтруем их по совпадению хэша)
                            final List<EntrantRatingData> result;
                            Debug.stopLogging();
                            try { result = EnrRatingListDao.setupValueHash(dao.getRatingItemDataList(competitionData)); }
                            finally { Debug.resumeLogging();}

                            // затем в блокировке объединяем результат (если он есть)
                            if (result.size() > 0) {
                                synchronized (ratingDataListMap) {
                                    if (null != ratingDataListMap.put(competitionData, result)) {
                                        throw new IllegalStateException();
                                    }
                                }
                                Debug.message("update = "+result.size());
                            }
                        }
                    });
                }
            } finally {
                // ждем, пока все закончится
                executor.close();
                Debug.end();
            }

            // сохраняем все в базу (в один поток)
            int updates = 0;
            Debug.begin("save");
            try {
                final List<EntrantRatingData> allRatingDataList = new ArrayList<>();
                for (final Map.Entry<EntrantRatingCompetitionData, List<EntrantRatingData>> e: ratingDataListMap.entrySet()) {
                    allRatingDataList.addAll(e.getValue());
                }
                if (dao.doSaveRatingList(allRatingDataList)) {
                    updates ++;
                }

            } finally {
                Debug.end();
            }

            // возвращаем результат
            return updates > 0;
        } finally {
            Debug.end();
        }
    }

    ///////////////////////////////////////////////////////
    // обновление признаков excludedBy...
    ///////////////////////////////////////////////////////

    @Override
    public boolean doUpdateExcludedByStatus()
    {
        EnrEntrantDaemonBean.DAEMON.check();
        Debug.begin("EnrRatingDaemonDao.doUpdateExcludedByStatus");
        try  {
            final MutableInt updates = new MutableInt(0);

//            Debug.begin("excludedByStepItem");
//            try {
//
//                // перечень состояний шага
//                final List <Long> stepStateIds = new DQLSelectBuilder()
//                .fromEntity(EnrEnrollmentStepState.class, "st").column(property("st.id"))
//                .where(in(property(EnrEnrollmentStepState.code().fromAlias("st")), Arrays.asList(EnrEnrollmentStepStateCodes.CLOSED, EnrEnrollmentStepStateCodes.FORMING_ORDERS)))
//                .createStatement(getSession()).list();
//
//                // только бюджетные
//                final List<Long> budgetCompetitionTypeIds = new DQLSelectBuilder()
//                .fromEntity(EnrCompetitionType.class, "tp").column(property("tp.id"))
//                .where(eq(property(EnrCompetitionType.compensationType().code().fromAlias("tp")), value(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET)))
//                .createStatement(getSession()).list();
//
//                // -----
//                // clear
//                updates.add(this.executeAndClear(
//                    new DQLUpdateBuilder(EnrRatingItem.class)
//                    .where(notExists(
//                        new DQLSelectBuilder()
//                        .fromEntity(EnrEnrollmentStepItem.class, "si")
//                        .where(in(property(EnrEnrollmentStepItem.entity().requestedCompetition().competition().type().id().fromAlias("si")), budgetCompetitionTypeIds))
//                        .where(eq(property(EnrEnrollmentStepItem.entity().id().fromAlias("si")), property("id")))
//                        .where(eq(property(EnrEnrollmentStepItem.included().fromAlias("si")), value(Boolean.TRUE)))
//                        .where(eq(property(EnrEnrollmentStepItem.recommended().fromAlias("si")), value(Boolean.TRUE)))
//                        .where(eq(property(EnrEnrollmentStepItem.originalIn().fromAlias("si")), value(Boolean.FALSE)))
//                        .where(in(property(EnrEnrollmentStepItem.step().state().id().fromAlias("si")), stepStateIds))
//                        .buildQuery()
//                    ))
//                    .where(isNotNull(property(EnrRatingItem.L_EXCLUDED_BY_STEP_ITEM)))
//                    .set(EnrRatingItem.L_EXCLUDED_BY_STEP_ITEM, nul(PropertyType.LONG))
//                ));
//
//                // -----
//                // set
//                Debug.begin("set");
//                try {
//                    // получаем сырые даннче - здесь могут быть дубли
//                    final List<Object[]> rows = new DQLSelectBuilder()
//                    .fromEntity(EnrEnrollmentStepItem.class, "si")
//                    .where(isNull(property(EnrEnrollmentStepItem.entity().excludedByStepItem().fromAlias("si"))))
//                    .where(in(property(EnrEnrollmentStepItem.entity().requestedCompetition().competition().type().id().fromAlias("si")), budgetCompetitionTypeIds))
//                    .where(eq(property(EnrEnrollmentStepItem.included().fromAlias("si")), value(Boolean.TRUE)))
//                    .where(eq(property(EnrEnrollmentStepItem.recommended().fromAlias("si")), value(Boolean.TRUE)))
//                    .where(eq(property(EnrEnrollmentStepItem.originalIn().fromAlias("si")), value(Boolean.FALSE)))
//                    .where(in(property(EnrEnrollmentStepItem.step().state().id().fromAlias("si")), stepStateIds))
//                    .column(property(EnrEnrollmentStepItem.entity().id().fromAlias("si")))
//                    .column(property(EnrEnrollmentStepItem.id().fromAlias("si")))
//                    .order(property(EnrEnrollmentStepItem.id().fromAlias("si"))) // в порядке создания stepItem нужно взять первый
//                    .createStatement(getSession()).list();
//
//                    // группируем и сортируем на уровне кода: enrRatingItem.id -> enrEnrollmentExtract.id
//                    final Map<Long, Long> toBeExcludedMap = new HashMap<>();
//                    for (final Object[] row: rows) {
//                        final Long ratingItemId = (Long)row[0];
//                        final Long stepItemId = (Long)row[1];
//
//                        if (null == toBeExcludedMap.get(ratingItemId)) {
//                            // сохраняем первое значение (остальные игнорируем) - сортировка на уровне кода
//                            toBeExcludedMap.put(ratingItemId, stepItemId);
//                        }
//                    }
//
//                    // сохраняем
//                    BatchUtils.execute(toBeExcludedMap.entrySet(), DQL.MAX_VALUES_ROW_NUMBER, elements -> {
//                        final DQLValuesBuilder valuesBuilder = new DQLValuesBuilder(
//                            new String[] { "item_id", "stepitem_id" },
//                            new PropertyType[] { PropertyType.LONG, PropertyType.LONG }
//                        );
//                        for (final Entry<Long, Long> e: elements) {
//                            valuesBuilder.row(value(e.getKey()), value(e.getValue()));
//                        }
//                        updates.add(EnrRatingDaemonDao.this.executeAndClear(
//                            new DQLUpdateBuilder(EnrRatingItem.class)
//                            .fromDataSource(valuesBuilder.build(), "src")
//                            .where(eq(property("id"), property("src.item_id")))
//                            .where(or(
//                                    isNull(property(EnrRatingItem.L_EXCLUDED_BY_STEP_ITEM)),
//                                    ne(property(EnrRatingItem.L_EXCLUDED_BY_STEP_ITEM), property("src.stepitem_id"))
//                            ))
//                            .set(EnrRatingItem.L_EXCLUDED_BY_STEP_ITEM, property("src.stepitem_id"))
//                        ));
//                    });
//                } finally {
//                    Debug.end();
//                }
//
//            } finally {
//                Debug.end();
//            }

            Debug.begin("excludedByExtract");
            try {

                // перечень состояний выписки
                final List <Long> stepStateIds = new DQLSelectBuilder()
                .fromEntity(ExtractStates.class, "st").column(property("st.id"))
                .where(in(property(ExtractStates.code().fromAlias("st")), EnrEnrollmentExtract.COMMITED_STATE_CODES))
                .createStatement(getSession()).list();

                // -----
                // clear
                updates.add(executeAndClear(
                    new DQLUpdateBuilder(EnrRatingItem.class)
                    .fromEntity(EnrRatingItem.class, "ri").where(eq(property("ri.id"), property("id")))
                    .joinPath(DQLJoinType.inner, EnrRatingItem.excludedByExtract().fromAlias("ri"), "ee")

                    .where(or(
                        // если кто-то вдруг изменил признак у выписки (например, приказом об отмене выписки), либо отменил ее
                        not(or(
                            eq(property(EnrEnrollmentExtract.cancelled().fromAlias("ee")), value(Boolean.FALSE)),
                            eq(property(EnrEnrollmentExtract.excludeLowerPriorityCompetitions().fromAlias("ee")), value(Boolean.TRUE))
                        )),

                        // если выписка поменяла состояние
                        notIn(property(EnrEnrollmentExtract.state().id().fromAlias("ee")), stepStateIds),

                        // если кто-то присобачил ссылку на выписку левого абитуриента (абитуриент, тип заявления)
                        // DEBUG-ONLY: ne(property(EnrRatingItem.requestedCompetition().request().entrant().fromAlias("ri")), property(EnrEnrollmentExtract.entity().request().entrant().fromAlias("ee"))),
                        // DEBUG-ONLY: ne(property(EnrRatingItem.requestedCompetition().request().type().fromAlias("ri")), property(EnrEnrollmentExtract.entity().request().type().fromAlias("ee"))),

                        // если кто-то руками поменял признак параллельного зачисления (поля меняются в интерфейсе)
                        ne(property(EnrRatingItem.requestedCompetition().parallel().fromAlias("ri")), property(EnrEnrollmentExtract.entity().parallel().fromAlias("ee"))),

                        // если вдруг получили конфликт приоритетов в том числе ссылка на самого себя (поля меняются в интерфейсе)
                        lt(property(EnrRatingItem.requestedCompetition().priority().fromAlias("ri")), property(EnrEnrollmentExtract.entity().priority().fromAlias("ee"))),

                        // если настройка ПК «Исключать зачисленных абитуриентов из конкурсов с более низким приоритетом» установлена в «нет»
                        eq(property(EnrRatingItem.entrant().enrollmentCampaign().settings().excludeEnrolledByLowPriority().fromAlias("ri")), value(false))
                    ))

                    // тогда нафиг все сбрасываем
                    .where(isNotNull(property(EnrRatingItem.L_EXCLUDED_BY_EXTRACT)))
                    .set(EnrRatingItem.L_EXCLUDED_BY_EXTRACT, nul(PropertyType.LONG))
                ));

                // -----
                // set
                Debug.begin("set");
                try {
                    // получаем сырые даннче - здесь могут быть дубли
                    final List<Object[]> rows = new DQLSelectBuilder()
                    .fromEntity(EnrRatingItem.class, "ri")
                    .where(isNull(property(EnrRatingItem.excludedByExtract().fromAlias("ri"))))

                    // если настройка ПК «Исключать зачисленных абитуриентов из конкурсов с более низким приоритетом» установлена в «да»
                    .where(eq(property(EnrRatingItem.entrant().enrollmentCampaign().settings().excludeEnrolledByLowPriority().fromAlias("ri")), value(true)))

                    .fromEntity(EnrEnrollmentExtract.class, "ee")
                    .where(or(
                        eq(property(EnrEnrollmentExtract.cancelled().fromAlias("ee")), value(Boolean.FALSE)) /* любая действующая */,
                        eq(property(EnrEnrollmentExtract.excludeLowerPriorityCompetitions().fromAlias("ee")), value(Boolean.TRUE)) /* если не действует, то проверяем признак */
                    ))
                    .where(in(property(EnrEnrollmentExtract.state().id().fromAlias("ee")), stepStateIds))

                    .where(eq(
                        // абитуриент должен быть тем же
                        property(EnrRatingItem.requestedCompetition().request().entrant().fromAlias("ri")),
                        property(EnrEnrollmentExtract.entity().request().entrant().fromAlias("ee"))
                    ))
                    .where(eq(
                        // тип заявления должен быть одинаковый
                        property(EnrRatingItem.requestedCompetition().request().type().fromAlias("ri")),
                        property(EnrEnrollmentExtract.entity().request().type().fromAlias("ee"))
                    ))
                    .where(eq(
                        // признак паралленьго зачисления должен быть одинаковым
                        property(EnrRatingItem.requestedCompetition().parallel().fromAlias("ri")),
                        property(EnrEnrollmentExtract.entity().parallel().fromAlias("ee"))
                    ))

                    .where(gt(
                        // приоритет у RatingItem должен быть ниже (большее число), чем у ВК в выписке
                        property(EnrRatingItem.requestedCompetition().priority().fromAlias("ri")),
                        property(EnrEnrollmentExtract.entity().priority().fromAlias("ee"))
                    ))

                    .column(property("ri.id"))
                    .column(property("ee.id"))
                    .column(property(EnrEnrollmentExtract.entity().priority().fromAlias("ee")))
                    .order(property(EnrEnrollmentExtract.entity().priority().fromAlias("ee")), OrderDirection.desc) // надо брать с самым низким приоритетом (с большим значением)

                    .createStatement(this.getSession()).list();

                    // группируем и сортируем на уровне кода: enrRatingItem.id -> enrEnrollmentExtract.id
                    final Map<Long, Long> toBeExcludedMap = new HashMap<>();
                    for (final Object[] row: rows) {
                        final Long ratingItemId = (Long)row[0];
                        final Long extractId = (Long)row[1];

                        toBeExcludedMap.putIfAbsent(ratingItemId, extractId);
                    }

                    // сохраняем
                    BatchUtils.execute(toBeExcludedMap.entrySet(), DQL.MAX_VALUES_ROW_NUMBER, elements -> {
                        final DQLValuesBuilder valuesBuilder = new DQLValuesBuilder(
                            new String[] { "item_id", "extract_id" },
                            new PropertyType[] { PropertyType.LONG, PropertyType.LONG }
                        );
                        for (final Entry<Long, Long> e: elements) {
                            valuesBuilder.row(value(e.getKey()), value(e.getValue()));
                        }
                        updates.add(EnrRatingDaemonDao.this.executeAndClear(
                            new DQLUpdateBuilder(EnrRatingItem.class)
                            .fromDataSource(valuesBuilder.build(), "src")
                            .where(eq(property("id"), property("src.item_id")))
                            .where(or(
                                    isNull(property(EnrRatingItem.L_EXCLUDED_BY_EXTRACT)),
                                    ne(property(EnrRatingItem.L_EXCLUDED_BY_EXTRACT), property("src.extract_id"))
                            ))
                            .set(EnrRatingItem.L_EXCLUDED_BY_EXTRACT, property("src.extract_id"))
                        ));
                    });
                } finally {
                    Debug.end();
                }

            } finally {
                Debug.end();
            }


            Debug.begin("excludedByExtractMinisterial");
            try {

                // перечень состояний выписки
                final List <Long> stepStateIds = new DQLSelectBuilder()
                .fromEntity(ExtractStates.class, "st").column(property("st.id"))
                .where(in(property(ExtractStates.code().fromAlias("st")), EnrEnrollmentExtract.COMMITED_STATE_CODES))
                .createStatement(getSession()).list();

                // -----
                // clear
                updates.add(executeAndClear(
                    new DQLUpdateBuilder(EnrRatingItem.class)
                        .fromEntity(EnrRatingItem.class, "ri").where(eq(property("ri.id"), property("id")))
                        .joinPath(DQLJoinType.inner, EnrRatingItem.excludedByExtractMinisterial().fromAlias("ri"), "ee")

                        .where(or(
                            // если кто-то отменил выписку
                            eq(property(EnrEnrollmentExtract.cancelled().fromAlias("ee")), value(Boolean.TRUE)),

                            // если выписка поменяла состояние
                            notIn(property(EnrEnrollmentExtract.state().id().fromAlias("ee")), stepStateIds),

                            // если кто-то присобачил ссылку на выписку левого абитуриента (абитуриент, тип заявления)
                            // DEBUG-ONLY: ne(property(EnrRatingItem.requestedCompetition().request().entrant().fromAlias("ri")), property(EnrEnrollmentExtract.entity().request().entrant().fromAlias("ee"))),
                            // DEBUG-ONLY: ne(property(EnrRatingItem.requestedCompetition().request().type().fromAlias("ri")), property(EnrEnrollmentExtract.entity().request().type().fromAlias("ee"))),

                            // если кто-то руками поменял признак параллельного зачисления (поля меняются в интерфейсе)
                            ne(property(EnrRatingItem.requestedCompetition().parallel().fromAlias("ri")), property(EnrEnrollmentExtract.entity().parallel().fromAlias("ee"))),

                            // если не выполняется условие на виды конкурса
                            ne(property(EnrRatingItem.requestedCompetition().competition().type().code().fromAlias("ri")), value(EnrCompetitionTypeCodes.MINISTERIAL)),
                            ne(property(EnrEnrollmentExtract.entity().competition().type().code().fromAlias("ee")), value(EnrCompetitionTypeCodes.EXCLUSIVE)),

                            // если вдруг получили конфликт приоритетов в том числе ссылка на самого себя (поля меняются в интерфейсе)
                            lt(property(EnrRatingItem.requestedCompetition().priority().fromAlias("ri")), property(EnrEnrollmentExtract.entity().priority().fromAlias("ee")))
                        ))

                            // тогда нафиг все сбрасываем
                        .where(isNotNull(property(EnrRatingItem.L_EXCLUDED_BY_EXTRACT_MINISTERIAL)))
                        .set(EnrRatingItem.L_EXCLUDED_BY_EXTRACT_MINISTERIAL, nul(PropertyType.LONG))
                ));

                // -----
                // set
                Debug.begin("set");
                try {
                    // получаем сырые даннче - здесь могут быть дубли
                    final List<Object[]> rows = new DQLSelectBuilder()
                    .fromEntity(EnrRatingItem.class, "ri")
                    .where(isNull(property(EnrRatingItem.excludedByExtractMinisterial().fromAlias("ri"))))

                    .fromEntity(EnrEnrollmentExtract.class, "ee")
                    .where(or(
                        eq(property(EnrEnrollmentExtract.cancelled().fromAlias("ee")), value(Boolean.FALSE)) /* любая действующая */,
                        eq(property(EnrEnrollmentExtract.excludeLowerPriorityCompetitions().fromAlias("ee")), value(Boolean.TRUE)) /* если не действует, то проверяем признак */
                    ))
                    .where(in(property(EnrEnrollmentExtract.state().id().fromAlias("ee")), stepStateIds))

                    .where(eq(
                        // абитуриент должен быть тем же
                        property(EnrRatingItem.requestedCompetition().request().entrant().fromAlias("ri")),
                        property(EnrEnrollmentExtract.entity().request().entrant().fromAlias("ee"))
                    ))
                    .where(eq(
                        // подразделение в наборе ОП должно быть тем же
                        property(EnrRatingItem.requestedCompetition().competition().programSetOrgUnit().fromAlias("ri")),
                        property(EnrEnrollmentExtract.entity().competition().programSetOrgUnit().fromAlias("ee"))
                    ))
                    .where(eq(
                        // признак паралленьго зачисления должен быть одинаковым
                        property(EnrRatingItem.requestedCompetition().parallel().fromAlias("ri")),
                        property(EnrEnrollmentExtract.entity().parallel().fromAlias("ee"))
                    ))
                    // текущий конкурс - по общему конкурсу
                    .where(eq(property(EnrRatingItem.requestedCompetition().competition().type().code().fromAlias("ri")), value(EnrCompetitionTypeCodes.MINISTERIAL)))
                    // зачислен - по квоте или без ВИ
                    .where(eq(property(EnrEnrollmentExtract.entity().competition().type().code().fromAlias("ee")), value(EnrCompetitionTypeCodes.EXCLUSIVE)))

                    .column(property("ri.id"))
                    .column(property("ee.id"))
                    .column(property(EnrEnrollmentExtract.entity().priority().fromAlias("ee")))
                    .order(property(EnrEnrollmentExtract.entity().priority().fromAlias("ee")), OrderDirection.desc) // надо брать с самым низким приоритетом (с большим значением)

                    .createStatement(this.getSession()).list();

                    // группируем и сортируем на уровне кода: enrRatingItem.id -> enrEnrollmentExtract.id
                    final Map<Long, Long> toBeExcludedMap = new HashMap<>();
                    for (final Object[] row: rows) {
                        final Long ratingItemId = (Long)row[0];
                        final Long extractId = (Long)row[1];

                        if (null == toBeExcludedMap.get(ratingItemId)) {
                            // сохраняем первое значение (остальные игнорируем) - сортировка на уровне кода
                            toBeExcludedMap.put(ratingItemId, extractId);
                        }
                    }

                    // сохраняем
                    BatchUtils.execute(toBeExcludedMap.entrySet(), DQL.MAX_VALUES_ROW_NUMBER, elements -> {
                        final DQLValuesBuilder valuesBuilder = new DQLValuesBuilder(
                            new String[] { "item_id", "extract_id" },
                            new PropertyType[] { PropertyType.LONG, PropertyType.LONG }
                        );
                        for (final Entry<Long, Long> e: elements) {
                            valuesBuilder.row(value(e.getKey()), value(e.getValue()));
                        }
                        updates.add(EnrRatingDaemonDao.this.executeAndClear(
                            new DQLUpdateBuilder(EnrRatingItem.class)
                            .fromDataSource(valuesBuilder.build(), "src")
                            .where(eq(property("id"), property("src.item_id")))
                            .where(or(
                                isNull(property(EnrRatingItem.L_EXCLUDED_BY_EXTRACT_MINISTERIAL)),
                                ne(property(EnrRatingItem.L_EXCLUDED_BY_EXTRACT_MINISTERIAL), property("src.extract_id"))
                            ))
                            .set(EnrRatingItem.L_EXCLUDED_BY_EXTRACT_MINISTERIAL, property("src.extract_id"))
                        ));
                    });
                } finally {
                    Debug.end();
                }

            } finally {
                Debug.end();
            }

            return (updates.intValue() > 0);
        } finally {
            Debug.end();
        }
    }

}
