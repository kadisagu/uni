package ru.tandemservice.unienr14.order.entity;

import org.tandemframework.shared.commonbase.utils.CommonCollator;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.order.entity.gen.*;
import ru.tandemservice.unienr14.request.entity.EnrEntrantForeignRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unimove.UnimoveDefines;

/**
 * Выписка приказа о зачислении по направлению Минобрнауки
 */
public class EnrEnrollmentMinisteryExtract extends EnrEnrollmentMinisteryExtractGen
{
    @Override
    public EnrRequestedCompetition getRequestedCompetition()
    {
        return null;
    }

    @Override
    public EnrCompetition getCompetition()
    {
        return getEntrantRequest().getCompetition();
    }

    @Override
    public EnrEntrant getEntrant()
    {
        return getEntrantRequest().getEntrant();
    }

    @Override
    public EnrEntrantForeignRequest getEntity()
    {
        return getEntrantRequest();
    }

    @Override
    public void setEntity(Object entity)
    {
        setEntrantRequest((EnrEntrantForeignRequest) entity);
    }

    @Override
    public boolean isCommitted()
    {
        return UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED.equals(getState().getCode());
    }

    @Override
    public void setCommitted(boolean committed)
    {
    }

    @Override
    public int compareTo(Object o)
    {
        if (o instanceof EnrEnrollmentMinisteryExtract) {
            EnrEnrollmentMinisteryExtract e = (EnrEnrollmentMinisteryExtract) o;
            return CommonCollator.RUSSIAN_COLLATOR.compare(getEntrant().getPerson().getFullFio(), e.getEntrant().getPerson().getFullFio());
        }
        return 0;
    }
}