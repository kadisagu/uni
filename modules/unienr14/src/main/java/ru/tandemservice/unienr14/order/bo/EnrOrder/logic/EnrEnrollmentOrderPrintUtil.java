/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unienr14.order.bo.EnrOrder.logic;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Predicate;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.base.entity.IPersistentIdentityCard;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.node.IRtfText;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unibase.util.SimpleCollectionSelector;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentOrderParagraphPrintFormType;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentParagraph;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising;
import ru.tandemservice.unimv.dao.UnimvDaoFacade;
import ru.tandemservice.unimv.entity.visa.Visa;

import java.util.*;

/**
 * @author vip_delete
 * @since 11.01.2009
 */
public class EnrEnrollmentOrderPrintUtil implements IPrintFormCreator<EnrOrder>
{
    public static final String PARAGRAPHS = "PARAGRAPHS";
    public static final String STUDENT_LIST = "STUDENT_LIST";

    public static void initOrgUnit(RtfInjectModifier modifier, OrgUnit orgUnit, String prefix, String postfix)
    {
        String title = "";

        String nominative = null;
        String genitive = null;
        String dative = null;
        String accusative = null;
        String prepositional = null;
        String label;

        if (orgUnit != null)
        {
            title = orgUnit.getFullTitle();
            nominative = orgUnit.getNominativeCaseTitle();
            genitive = orgUnit.getGenitiveCaseTitle();
            dative = orgUnit.getDativeCaseTitle();
            accusative = orgUnit.getAccusativeCaseTitle();
            prepositional = orgUnit.getPrepositionalCaseTitle();
        }

        label = prefix + postfix;
        modifier.put(label, nominative == null ? title : nominative);
        modifier.put(label + "_N", nominative == null ? title : nominative);
        modifier.put(label + "_G", genitive == null ? title : genitive);
        modifier.put(label + "_D", dative == null ? title : dative);
        modifier.put(label + "_A", accusative == null ? title : accusative);
        modifier.put(label + "_P", prepositional == null ? title : prepositional);
    }

    @Override
    @SuppressWarnings({"unchecked"})
    public RtfDocument createPrintForm(byte[] orderTemplate, EnrOrder order)
    {
        RtfDocument document = new RtfReader().read(orderTemplate);

        TopOrgUnit academy = TopOrgUnit.getInstance();
        AddressDetailed address = academy.getAddress();
        String highSchoolCity = address == null || address.getSettlement() == null ? null : address.getSettlement().getTitleWithType();

        RtfInjectModifier modifier = new RtfInjectModifier();
        modifier
                .put("commitDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(order.getCommitDate()))
                .put("orderNumber", order.getNumber())
                .put("highSchoolTitle", academy.getTitle())
                .put("highSchoolCity", highSchoolCity)
                .put("orderBasicText", order.getOrderBasicText())
                .put("orderText", order.getOrderText())
                .put("textParagraph", order.getTextParagraph())
                .put("executor", order.getExecutor());
        if (order.getReason() != null)
        {
            modifier.put("orderReason", order.getReason().getTitle());
            if (order.getBasic() != null)
            {
                modifier.put("orderBasic", order.getBasic().getTitle());
            }
            else
                modifier.put("orderBasic", "");
        }
        else
        {
            modifier.put("orderReason", "");
            modifier.put("orderBasic", "");
        }

        List<? extends IAbstractParagraph> paragraphList = order.getParagraphList();
        if (paragraphList.size() > 0)
        {
            List<EnrEnrollmentExtract> extractList = IUniBaseDao.instance.get().getList(EnrEnrollmentExtract.class, EnrEnrollmentExtract.paragraph().s(), paragraphList.get(0));
            if (extractList.size() > 0)
            {
                EnrEnrollmentExtract extract = extractList.get(0);
                initOrgUnit(modifier, extract.getEntity().getCompetition().getProgramSetOrgUnit().getOrgUnit().getInstitutionOrgUnit().getOrgUnit(), "enrOrgUnit", "");
                initOrgUnit(modifier, extract.getEntity().getCompetition().getProgramSetOrgUnit().getFormativeOrgUnit(), "formativeOrgUnit", "");
            }
        }

        modifier.modify(document);

        RtfTableModifier tableModifier = createEnrollmentOrderTableModifier(order);
        tableModifier.modify(document);

        injectParagraphs(document, order);

        return document;
    }

    private RtfTableModifier createEnrollmentOrderTableModifier(EnrOrder order)
    {
        RtfTableModifier tableModifier = new RtfTableModifier();
        List<Visa> visaList = UnimvDaoFacade.getVisaDao().getVisaList(order);
        List<String[]> visaData = new ArrayList<>();
        Map<String, List<String[]>> printLabelMap = new HashMap<>();

        for (GroupsMemberVising group : UniDaoFacade.getCoreDao().getCatalogItemListOrderByCode(GroupsMemberVising.class))
            printLabelMap.put(group.getPrintLabel(), new ArrayList<>());

        for (Visa visa : visaList)
        {
            IPersistentIdentityCard identityCard = visa.getPossibleVisa().getEntity().getPerson().getIdentityCard();
            String lastName = identityCard.getLastName();
            String firstName = identityCard.getFirstName();
            String middleName = identityCard.getMiddleName();

            StringBuilder str = new StringBuilder();
            if (StringUtils.isNotEmpty(firstName))
                str.append(firstName.substring(0, 1).toUpperCase()).append(".");
            if (StringUtils.isNotEmpty(middleName))
                str.append(middleName.substring(0, 1).toUpperCase()).append(".");
            str.append(" ").append(lastName);

            printLabelMap.get(visa.getGroupMemberVising().getPrintLabel()).add(new String[]{visa.getPossibleVisa().getTitle(), str.toString()});

            visaData.add(new String[]{visa.getPossibleVisa().getTitle(), str.toString()});
        }
        for (Map.Entry<String, List<String[]>> entry : printLabelMap.entrySet())
            if (!entry.getValue().isEmpty())
                tableModifier.put(entry.getKey(), entry.getValue().toArray(new String[entry.getValue().size()][]));
            else
                tableModifier.put(entry.getKey(), new String[][]{});

        tableModifier.put("VISAS", visaData.toArray(new String[visaData.size()][]));
        return tableModifier;
    }

    @SuppressWarnings("unchecked")
    protected void injectParagraphs(final RtfDocument document, EnrOrder order)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, PARAGRAPHS);

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            List<IRtfElement> parList = new ArrayList<>();

            for (IAbstractParagraph paragraph : order.getParagraphList())
            {
                List<EnrEnrollmentExtract> extractList = IUniBaseDao.instance.get().getList(EnrEnrollmentExtract.class, EnrEnrollmentExtract.paragraph().s(), paragraph);
                if (extractList.size() == 0)
                    throw new ApplicationException("Пустой параграф №" + paragraph.getNumber() + ".");

                byte[] paragraphTemplate = EnrEnrollmentOrderPrintUtil.getEnrollmentOrderParagraphTemplate(paragraph.getId());
                RtfDocument paragraphDocument = new RtfReader().read(paragraphTemplate);

                // первая выписка из параграфа
                EnrEnrollmentExtract firstExtract = extractList.get(0);



                RtfInjectModifier injectModifier = new RtfInjectModifier()
                        .put("parNumber", Integer.toString(paragraph.getNumber()))
                        .put("parNumberConditional", order.getParagraphList().size() > 1 ? String.valueOf(paragraph.getNumber()) + ". " : "")
                        .put("developForm", firstExtract.getEntity().getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramForm().getShortTitle().toLowerCase())
                        .put("educationOrgUnit", firstExtract.getEntity().getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramSubject().getTitleWithCode());

                final String label = "educationType";
                String programKindCode = firstExtract.getEntity().getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramSubject().getEduProgramKind().getCode();
                final String[] types = EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV.equals(programKindCode) || EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV_SREDNEGO_ZVENA.equals(programKindCode) ? UniRtfUtil.SPECIALITY_CASES : (EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_KVALIFITSIROVANNYH_RABOCHIH_SLUJATSHIH_.equals(programKindCode) ? UniRtfUtil.EDU_PROFESSION_CASES  : UniRtfUtil.EDU_DIRECTION_CASES);
                for (int i = 0; i < UniRtfUtil.CASE_POSTFIX.size(); i++) {
                    injectModifier.put(label + UniRtfUtil.CASE_POSTFIX.get(i), types[i]);
                }

                // получаем студентов из параграфа
                StringBuilder buffer = new StringBuilder();
                int counter = 1;
                for (EnrEnrollmentExtract extract : extractList)
                {
                    Person person = extract.getEntity().getRequest().getEntrant().getPerson();

                    buffer.append("\\par ").append(counter++).append(".  ");
                    buffer.append(person.getFullFio());

                    buffer.append(" (").append(extract.getEntity().getRequest().getEduDocument().getEduLevel().getShortTitle()).append(")");

                    EnrRatingItem ratingItem = IUniBaseDao.instance.get().get(EnrRatingItem.class, EnrRatingItem.requestedCompetition(), extract.getRequestedCompetition());
                    Double sumMark = ratingItem == null ? null : ratingItem.getTotalMarkAsDouble();
                    if (sumMark != null)
                        buffer.append(" \\endash  ").append(DoubleFormatter.DOUBLE_FORMATTER_1_DIGITS.format(sumMark));
                }
                buffer.append("\\par");

                // клонируем шаблон параграфа
                RtfDocument paragraphPart = paragraphDocument.getClone();
                injectModifier.modify(paragraphPart);

                RtfSearchResult searchResult = UniRtfUtil.findRtfMark(paragraphPart, STUDENT_LIST);
                if (searchResult.isFound())
                {
                    IRtfText text = RtfBean.getElementFactory().createRtfText(buffer.toString());
                    text.setRaw(true);

                    searchResult.getElementList().set(searchResult.getIndex(), text);
                }

                RtfUtil.modifySourceList(document.getHeader(), paragraphPart.getHeader(), paragraphPart.getElementList());

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraphPart.getElementList());
                parList.add(rtfGroup);
            }

            // полученный список вставляем вместо ключевого слова
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }

    public static byte[] getEnrollmentOrderParagraphTemplate(Long paragraphId)
    {
        EnrEnrollmentParagraph paragraph = IUniBaseDao.instance.get().getNotNull(paragraphId);

        final Long competitionTypeId = paragraph.getCompetitionType().getId();
        final Boolean parallel = paragraph.isParallel();
        final Long requestTypeId = ((EnrOrder) paragraph.getOrder()).getRequestType().getId();

        Collection<EnrEnrollmentOrderParagraphPrintFormType> printForms = IUniBaseDao.instance.get().getList(EnrEnrollmentOrderParagraphPrintFormType.class);
        Predicate<EnrEnrollmentOrderParagraphPrintFormType> initialSortPredicate = type -> {
            // убираем неподходящие печатные формы: у них заданы вид заявления, вид приема, параллельность, и они отличаются указанных в параграфе
            return
                (type.getRequestType() == null || type.getRequestType().getId().equals(requestTypeId)) &&
                (type.getCompetitionType() == null || type.getCompetitionType().getId().equals(competitionTypeId)) &&
                (type.getParallel() == null || type.getParallel().equals(parallel));
        };

        printForms = CollectionUtils.select(printForms, initialSortPredicate);
        Predicate<EnrEnrollmentOrderParagraphPrintFormType> requestTypePredicate = type -> type.getRequestType() != null && type.getRequestType().getId().equals(requestTypeId);
        Predicate<EnrEnrollmentOrderParagraphPrintFormType> competitionTypePredicate = type -> type.getCompetitionType() != null && type.getCompetitionType().getId().equals(competitionTypeId);
        Predicate<EnrEnrollmentOrderParagraphPrintFormType> parallelPredicate = type -> type.getParallel() != null && type.getParallel().equals(parallel);
        List<Predicate<? super EnrEnrollmentOrderParagraphPrintFormType>> predicates = Arrays.<Predicate<? super EnrEnrollmentOrderParagraphPrintFormType>>asList(requestTypePredicate, competitionTypePredicate, parallelPredicate);
        SimpleCollectionSelector.StopCriterion<EnrEnrollmentOrderParagraphPrintFormType> stopCriterion = source -> source.size() == 1;

        SimpleCollectionSelector<EnrEnrollmentOrderParagraphPrintFormType> collectionSelector = new SimpleCollectionSelector<>(printForms, stopCriterion, predicates);

        Collection<EnrEnrollmentOrderParagraphPrintFormType> result = collectionSelector.select();

        if (result.size() == 1)
            return result.iterator().next().getContent();

        // не нашли подходящий, берем базовый
        result = CollectionUtils.select(printForms, type -> type.getRequestType() == null && type.getCompetitionType() == null && type.getParallel() == null);

        if (result.size() == 1)
            return result.iterator().next().getContent();

        throw new IllegalStateException(); // что-то поменялось, ключ Вид заявления + Вид приема + Параллельность больше не является уникальным (или нет базового)
    }
}