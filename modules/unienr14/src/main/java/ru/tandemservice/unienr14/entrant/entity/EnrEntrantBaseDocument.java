package ru.tandemservice.unienr14.entrant.entity;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.unienr14.catalog.entity.codes.PersonDocumentTypeCodes;
import ru.tandemservice.unienr14.entrant.entity.gen.EnrEntrantBaseDocumentGen;

import java.util.Date;

/**
 * Документ абитуриента
 */
public class EnrEntrantBaseDocument extends EnrEntrantBaseDocumentGen
{
    @Override
    public String getTitle()
    {
        if (getDocumentType() == null) {
            return this.getClass().getSimpleName();
        }
        return UniStringUtils.joinWithSeparator(": ", getDocumentType().getShortTitle(), UniStringUtils.joinWithSeparator(" ", getSeria(), getNumber()));
    }

    @Override
    public String getExtendedTitle()
    {
        return getTitle();
    }

    @Override
    public Date getIssuanceDate()
    {
        return getDocRelation().getDocument().getIssuanceDate();
    }

    @Override
    public String getIssuancePlace()
    {
        return getDocRelation().getDocument().getIssuancePlace();
    }

    @Override
    public String getDescription()
    {
        return getDocRelation().getDocument().getDescription();
    }

    /**
     * DEV-2878
     * @return информация о документе
     */
    public String getInfoString() {
        if (PersonDocumentTypeCodes.CAN_RECEIVE_EDUCATION_CERT.equals(getDocumentType().getCode()) || PersonDocumentTypeCodes.PSY_CERT.equals(getDocumentType().getCode())) {
            if (getIssuancePlace() != null)
                return "док. выдан " + getIssuancePlace();
            return null;
        }
        return UniStringUtils.joinWithSeparator(", ", getDescription(), getIssuancePlace() == null ? null : "док. выдан " + getIssuancePlace());
    }

    @Override
    @EntityDSLSupport
    public String getDisplayableTitle()
    {
        switch (this.getDocumentType().getCode())
        {
            case PersonDocumentTypeCodes.CAN_RECEIVE_EDUCATION_CERT : return getDisplayableTitleCanReceiveEducationCert();
            case PersonDocumentTypeCodes.PSY_CERT : return getDisplayableTitlePsyCert();
            default: return getDisplayableTitleOther();
        }
    }

    @Override
    public String getFisUidTitle()
    {
        return getTitle() + ", " + getEntrant().getFullFio();
    }

    private String getDisplayableTitleCanReceiveEducationCert()
    {
        StringBuilder result = new StringBuilder()
        .append(this.getDocumentType().getShortTitle());

        if (this.getSeria() != null || this.getNumber() != null)
            result.append(" -");

        if (this.getSeria() != null)
            result.append(" ").append(this.getSeria());

        if (this.getNumber() != null)
            result.append(" ").append(this.getNumber());

        if (this.getIssuanceDate() != null)
            result.append(" (").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(this.getIssuanceDate())).append(")");

        return result.toString();
    }

    private String getDisplayableTitlePsyCert()
    {
        StringBuilder result = new StringBuilder()
        .append(this.getDocumentType().getShortTitle());

        if (this.getSeria() != null || this.getNumber() != null)
            result.append(" -");

        if (this.getSeria() != null)
            result.append(" ").append(this.getSeria());

        if (this.getNumber() != null)
            result.append(" ").append(this.getNumber());

        if (this.getIssuanceDate() != null)
            result.append(" (").append(DateFormatter.DATE_FORMATTER_JUST_YEAR.format(this.getIssuanceDate())).append(" г.)");

        return result.toString();
    }

    private String getDisplayableTitleOther()
    {
        StringBuilder result = new StringBuilder()
        .append(this.getDocumentType().getShortTitle());

        if (this.getSeria() != null || this.getNumber() != null)
            result.append(" -");

        if (this.getSeria() != null)
            result.append(" ").append(this.getSeria());

        if (this.getNumber() != null)
            result.append(" ").append(this.getNumber());

        if (this.getIssuanceDate() != null)
            result.append(" (").append(DateFormatter.DATE_FORMATTER_JUST_YEAR.format(this.getIssuanceDate())).append(" г.)");

        return result.toString();
    }
}