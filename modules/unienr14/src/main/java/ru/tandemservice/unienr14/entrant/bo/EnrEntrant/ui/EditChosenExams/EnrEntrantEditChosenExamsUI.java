/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.EditChosenExams;

import com.google.common.collect.ImmutableMap;
import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import org.tandemframework.shared.person.catalog.entity.codes.EduLevelCodes;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrBenefitCategory;
import ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm;
import ru.tandemservice.unienr14.catalog.entity.EnrInternalExamReason;
import ru.tandemservice.unienr14.catalog.entity.codes.*;
import ru.tandemservice.unienr14.competition.entity.EnrExamVariant;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.logic.IEnrEntrantDao;
import ru.tandemservice.unienr14.entrant.daemon.EnrEntrantDaemonBean;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.exams.bo.EnrExamPassDiscipline.EnrExamPassDisciplineManager;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.EnrExamSetManager;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.logic.IEnrExamSetDao;
import ru.tandemservice.unienr14.exams.entity.EnrExamSetElement;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm;
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceBenefit;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitProofDocument;
import ru.tandemservice.unienr14.request.entity.gen.IEnrEntrantBenefitProofDocumentGen;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDisciplineGroup;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDisciplineGroupElement;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignEntrantDocument;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 5/6/13
 */
@Input({
    @Bind(key = "entrantId", binding = "entrant.id", required=true),
    @Bind(key = EnrEntrantEditChosenExamsUI.PARAM_FORCE_REFRESH_EXAM_LIST, binding="forceRefreshExamList")
})
public class EnrEntrantEditChosenExamsUI extends UIPresenter
{
    public static final String PARAM_FORCE_REFRESH_EXAM_LIST = "forceRefreshExamList";

    private EnrEntrant entrant = new EnrEntrant();

    private boolean forceRefreshExamList = true;
    public boolean isForceRefreshExamList() { return this.forceRefreshExamList; }
    public void setForceRefreshExamList(boolean forceRefreshExamList) { this.forceRefreshExamList = forceRefreshExamList; }

    private List<EnrExamPassForm> passFormList;
    private EnrExamPassForm currentPassForm;

    private List<CompetitionRowWrapper> rowList;
    private CompetitionRowWrapper currentRow;
    private DiscWrapper currentDiscRow;

    private Map<Long, DiscWrapper> discWrapperByIdMap = new HashMap<>();

    private List<EnrBenefitCategory> diplomaCategoryList;
    private List<IEnrEntrantBenefitProofDocument> diplomaAllList;

    private EnrExamPassForm passFormOlympiad;

    private boolean noExamsToChoose;

    private boolean allowSelectInternalExamReason;
    private EnrInternalExamReason internalExamReason;

    @Override
    public void onComponentRefresh()
    {
        setEntrant(IUniBaseDao.instance.get().getNotNull(EnrEntrant.class, getEntrant().getId()));

        setDiplomaCategoryList(IUniBaseDao.instance.get().<EnrBenefitCategory>getList(
            new DQLSelectBuilder()
            .fromEntity(EnrBenefitCategory.class, "c")
            .column(property("c"))
            .order(property(EnrBenefitCategory.code().fromAlias("c")))
            .where(eq(property(EnrBenefitCategory.benefitType().code().fromAlias("c")), value(EnrBenefitTypeCodes.MAX_EXAM_RESULT)))
        ));

        setDiplomaAllList(IUniBaseDao.instance.get().<IEnrEntrantBenefitProofDocument>getList(
            new DQLSelectBuilder()
            .fromEntity(IEnrEntrantBenefitProofDocument.class, "x")
            .column(property("x"))
            .order(property(IEnrEntrantBenefitProofDocumentGen.registrationDate().fromAlias("x")))
            .where(eq(property(IEnrEntrantBenefitProofDocumentGen.entrant().fromAlias("x")), value(getEntrant())))
            .where(exists(
                new DQLSelectBuilder()
                .fromEntity(EnrCampaignEntrantDocument.class, "r")
                .column(property("r.id"))
                .where(eq(property(EnrCampaignEntrantDocument.documentType().fromAlias("r")), property(IEnrEntrantBenefitProofDocumentGen.documentType().fromAlias("x"))))
                .where(eq(property(EnrCampaignEntrantDocument.enrollmentCampaign().fromAlias("r")), value(getEntrant().getEnrollmentCampaign())))
                .where(eq(property(EnrCampaignEntrantDocument.benefitMaxMark().fromAlias("r")), value(Boolean.TRUE)))
                .buildQuery()
            ))
        ));


        // убираем олимпиаду - ее нельзя выбрать, можно сделать зачтение олимпиады и она будет создана там
        setPassFormList(IUniBaseDao.instance.get().getCatalogItemListOrderByCode(EnrExamPassForm.class));
        setPassFormOlympiad(IUniBaseDao.instance.get().getCatalogItem(EnrExamPassForm.class, EnrExamPassFormCodes.OLYMPIAD));
        getPassFormList().remove(getPassFormOlympiad());

        setRowList(new ArrayList<>());
        getDiscWrapperByIdMap().clear();

        DQLSelectBuilder dql = new DQLSelectBuilder()
        .fromEntity(EnrRequestedCompetition.class, "r").column("r").order(property(EnrRequestedCompetition.priority().fromAlias("r")))
        .where(eq(property(EnrRequestedCompetition.request().entrant().fromAlias("r")), value(getEntrant())))
        .where(isNull(property(EnrRequestedCompetition.request().takeAwayDocumentDate().fromAlias("r"))))
        .where(not(or(
            eq(property(EnrRequestedCompetition.competition().type().code().fromAlias("r")), value(EnrCompetitionTypeCodes.NO_EXAM_CONTRACT)),
            eq(property(EnrRequestedCompetition.competition().type().code().fromAlias("r")), value(EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL))
        )));
        List<EnrRequestedCompetition> requestedCompetitionList = dql.createStatement(_uiSupport.getSession()).<EnrRequestedCompetition>list();

        Map<Long, IEnrExamSetDao.IExamSetSettings> examSetContent = EnrExamSetManager.instance().dao().getExamSetContent(CollectionUtils.collect(requestedCompetitionList, requestedCompetition -> {
            return requestedCompetition.getCompetition().getExamSetVariant().getId();
        }));

        setAllowSelectInternalExamReason(false);
        for (EnrRequestedCompetition requestedCompetition : requestedCompetitionList) {

            String requestTypeCode = requestedCompetition.getRequest().getType().getCode();
            String eduLevelReqCode = requestedCompetition.getCompetition().getEduLevelRequirement().getCode();
            if (EnrRequestTypeCodes.BS.equals(requestTypeCode) && !EnrEduLevelRequirementCodes.PO.equals(eduLevelReqCode))
                setAllowSelectInternalExamReason(true);

            if (getInternalExamReason() == null && requestedCompetition.getRequest().getInternalExamReason() != null)
                setInternalExamReason(requestedCompetition.getRequest().getInternalExamReason());

            IEnrExamSetDao.IExamSetSettings examSetSettings = examSetContent.get(requestedCompetition.getCompetition().getExamSetVariant().getId());

            if (examSetSettings == null || examSetSettings.isNotFullySetup())
                throw new ApplicationException("Для выбранного конкурса «" + requestedCompetition.getTitle() + "» не настроен набор ВИ. Настройте набор ВИ перед выбором ВИ для абитуриента.");

            List<IEnrExamSetDao.IExamSetElementSettings> elementList = examSetSettings.getElementList();

            if (elementList.isEmpty())
                continue;

            CompetitionRowWrapper competitionRowWrapper = new CompetitionRowWrapper(requestedCompetition);
            getRowList().add(competitionRowWrapper);

            Collections.sort(elementList, (o1, o2) -> o1.getPriority() - o2.getPriority());
            for (IEnrExamSetDao.IExamSetElementSettings elementSettings : elementList) {
                DiscWrapper discWrapper = new DiscWrapper(requestedCompetition, elementSettings.getCgExam());

                competitionRowWrapper.getDiscRowList().add(discWrapper);
                getDiscWrapperByIdMap().put(discWrapper.getId(), discWrapper);

                for (Map.Entry<EnrExamPassForm, Boolean> entry : elementSettings.getPassFormMap().entrySet())
                    if (Boolean.TRUE.equals(entry.getValue()))
                        discWrapper.getAllowedPassForms().add(entry.getKey());
                if (elementSettings.getElement().getValue() instanceof EnrCampaignDiscipline) {
                    discWrapper.setDiscList(Collections.singletonList((EnrCampaignDiscipline) elementSettings.getElement().getValue()));
                }
                else if (elementSettings.getElement().getValue() instanceof EnrCampaignDisciplineGroup) {
                    discWrapper.setDiscList(new ArrayList<>());
                    for (EnrCampaignDisciplineGroupElement el : IUniBaseDao.instance.get().getList(EnrCampaignDisciplineGroupElement.class, EnrCampaignDisciplineGroupElement.group().s(), elementSettings.getElement().getValue(), EnrCampaignDisciplineGroupElement.discipline().discipline().title().s())) {
                        discWrapper.getDiscList().add(el.getDiscipline());
                    }
                }
            }
        }

        setNoExamsToChoose(getRowList().isEmpty());

        for (EnrChosenEntranceExam exam : IUniBaseDao.instance.get().getList(EnrChosenEntranceExam.class, EnrChosenEntranceExam.requestedCompetition().request().entrant(), getEntrant())) {

            EnrExamVariant actualExam = exam.getActualExam();
            if (actualExam == null)
                continue;

            DiscWrapper wrapper = getDiscWrapperByIdMap().get(DiscWrapper.countWrapperId(exam));
            if (null != wrapper)
                wrapper.setDisc(exam.getDiscipline());
        }

        for (EnrChosenEntranceExamForm examForm : IUniBaseDao.instance.get().getList(EnrChosenEntranceExamForm.class, EnrChosenEntranceExamForm.chosenEntranceExam().requestedCompetition().request().entrant(), getEntrant())) {
            if (examForm.getChosenEntranceExam().getActualExam() == null)
                continue;

            DiscWrapper wrapper = getDiscWrapperByIdMap().get(DiscWrapper.countWrapperId(examForm.getChosenEntranceExam()));
            if (null != wrapper && wrapper.getAllowedPassForms().contains(examForm.getPassForm()))
                wrapper.getSelectedPassForms().add(examForm.getPassForm());
        }

        for (EnrEntrantMarkSourceBenefit olympiad : IUniBaseDao.instance.get().getList(EnrEntrantMarkSourceBenefit.class, EnrEntrantMarkSourceBenefit.entrant(), getEntrant())) {
            if (olympiad.getChosenEntranceExamForm().getChosenEntranceExam().getActualExam() == null) continue;
            DiscWrapper wrapper = getDiscWrapperByIdMap().get(DiscWrapper.countWrapperId(olympiad.getChosenEntranceExamForm().getChosenEntranceExam()));
            if (null != wrapper && wrapper.getAllowedPassForms().contains(getPassFormOlympiad())) {
                wrapper.getSelectedPassForms().add(olympiad.getPassForm());
                wrapper.setDiplomaCategory(olympiad.getBenefitCategory());
                wrapper.setDiploma(olympiad.getMainProof());
            }
        }
    }

    @SuppressWarnings("unchecked")
    public void onClickApply()
    {
        if (saveFormData()) return;

        deactivate();
    }


    public void onSelectOlympiad() {
        DiscWrapper discWrapper = getDiscWrapperByIdMap().get(getListenerParameterAsLong());
        discWrapper.getSelectedPassForms().clear();

        if (null != discWrapper.getDiploma()) {
            if (null == discWrapper.getDiplomaCategory()) {
                discWrapper.setDiploma(null);
            }
        }
    }

    public void onChangeInternalExamReason()
    {
        if (getInternalExamReason() != null) return;
        for (CompetitionRowWrapper row: rowList) {
            if (!isNeedExternalExamReason(row.getCompetition())) continue;
            for (DiscWrapper disc : row.getDiscRowList()) {
                boolean hasStateExam = false;
                for (EnrExamPassForm passForm : disc.getAllowedPassForms())
                    hasStateExam |= passForm.getCode().equals(EnrExamPassFormCodes.STATE_EXAM);
                if (!hasStateExam) continue;

                for (EnrExamPassForm passForm: new ArrayList<>(disc.getSelectedPassForms())) {
                    if (passForm.isInternal()) {
                        disc.getSelectedPassForms().remove(passForm);
                    }
                }
            }
        }
    }

    // presenter

    public boolean isOlympiadCategoryDisabled() {
        return getCurrentRow() != null && getCurrentDiscRow() != null && !getCurrentDiscRow().getAllowedPassForms().contains(getPassFormOlympiad());
    }

    public boolean isOlympiadDisabled() {
        if (isOlympiadCategoryDisabled()) { return true; }
        return getCurrentRow() != null && getCurrentDiscRow() != null && null == getCurrentDiscRow().getDiplomaCategory();
    }


    public boolean isPassFormSelectionDisabled()
    {
        return isPassFormSelectionDisabledByDiploma() || isPassFormSelectionDisabledByInternalExamReason();
    }

    public int getHeaderColspan() {
        return isAllowSelectDiploma() ? getPassFormList().size() + 5 : getPassFormList().size() + 3;
    }

    public int getHeaderRowspan() {
        return isAllowSelectDiploma() ? 3 : 2;
    }

    public int getPassFormColspan() {
        return isAllowSelectDiploma() ? getPassFormList().size() + 2 : getPassFormList().size();
    }

    public int getPassFormRowspan() {
        return isAllowSelectDiploma() ? 2 : 1;
    }

    public boolean isAllowSelectDiploma() {
        return getDiplomaAllList() != null && !getDiplomaAllList().isEmpty();
    }

    public boolean isFirstDiscRow() {
        return (getCurrentRow() != null && !getCurrentRow().getDiscRowList().isEmpty() && getCurrentRow().getDiscRowList().get(0).equals(getCurrentDiscRow()));
    }

    public boolean isCurrentPassFormSelected() {
        return getCurrentDiscRow() != null && getCurrentDiscRow().getSelectedPassForms().contains(getCurrentPassForm());
    }

    public void setCurrentPassFormSelected(boolean selected) {
        if (getCurrentDiscRow() == null || getCurrentPassForm() == null)
            return;
        if (selected)
            getCurrentDiscRow().getSelectedPassForms().add(getCurrentPassForm());
        else
            getCurrentDiscRow().getSelectedPassForms().remove(getCurrentPassForm());
    }

    public boolean isCurrentPassFormAllowed() {
        return getCurrentDiscRow() != null && getCurrentDiscRow().getAllowedPassForms().contains(getCurrentPassForm());
    }

    public String getCurrentPassFormInputId() {
        if (getCurrentRow() == null || getCurrentDiscRow() == null || getCurrentPassForm() == null)
            return null;
        return "passFormCheckBox." + getCurrentRow().getId().hashCode() + "." + getCurrentDiscRow().getId().hashCode() + "." + getCurrentPassForm().getId().hashCode();
    }

    public String getCurrentPassFormInputRowId() {
        if (getCurrentRow() == null || getCurrentDiscRow() == null)
            return null;
        return "passFormCheckBoxRow." + getCurrentRow().getId().hashCode() + "." + getCurrentDiscRow().getId().hashCode();
    }

    public Collection<IEnrEntrantBenefitProofDocument> getCurrentRowDiplomaList()
    {
        // TODO: DEV-4832 фильтровать по категории ?
        return getDiplomaAllList();
    }

    // utils

    @SuppressWarnings("unchecked")
    protected boolean saveFormData()
    {
        for (CompetitionRowWrapper wrapper : getRowList()) {
            for (DiscWrapper discWrapper : wrapper.getDiscRowList()) {
                if (discWrapper.getDiploma() == null && discWrapper.getSelectedPassForms().isEmpty()) {
                    ContextLocal.getErrorCollector().add("Для ВИ «" + discWrapper.getExamSetElement().getTitle() + "» (" + wrapper.getCompetitionTitle() + ") не выбрана ни одна форма сдачи.");
                    return true;
                }
            }
        }

        final Map<EnrRequestedCompetition, List<IEnrEntrantDao.IChosenExamWrapper>> chosenExamMap = new HashMap<>();
        for (CompetitionRowWrapper wrapper : getRowList()) {
            chosenExamMap.put(wrapper.getCompetition(), (List) wrapper.getDiscRowList());
        }

        // действия нужно делать в одной транзакции, иначе демон придет дважды
        DataAccessServices.dao().doInTransaction(session -> {
            EnrEntrantManager.instance().dao().saveChosenExams(getEntrant(), chosenExamMap, getInternalExamReason());
            if (isForceRefreshExamList()) {
                EnrExamPassDisciplineManager.instance().dao().doRefreshExamList(getEntrant());
            }
            return null;
        });

        EnrEntrantDaemonBean.DAEMON.waitForComplete(60);
        return false;
    }

    private boolean isPassFormSelectionDisabledByInternalExamReason()
    {
        if (getCurrentRow() == null) return false;
        if (getCurrentPassForm() == null || !getCurrentPassForm().isInternal()) return false;
        if (getInternalExamReason() != null || !isNeedExternalExamReason(getCurrentRow().getCompetition())) return false;

        boolean hasStateExam = false;
        for (EnrExamPassForm passForm : new ArrayList<>(getCurrentDiscRow().getAllowedPassForms()))
            hasStateExam |= passForm.getCode().equals(EnrExamPassFormCodes.STATE_EXAM);
        return hasStateExam;
    }

    private boolean isPassFormSelectionDisabledByDiploma() {
        return null != getCurrentDiscRow() && (null != getCurrentDiscRow().getDiplomaCategory() || null != getCurrentDiscRow().getDiploma());
    }

    private boolean isNeedExternalExamReason(EnrRequestedCompetition requestedCompetition)
    {
        if (!requestedCompetition.getRequest().getType().getCode().equals(EnrRequestTypeCodes.BS)) return false;
        if (EnrEduLevelRequirementCodes.PO.equals(requestedCompetition.getCompetition().getEduLevelRequirement().getCode())) return false;
        EduLevel eduLevel = requestedCompetition.getRequest().getEduDocument().getEduLevel();
        return eduLevel != null && eduLevel.getCode().equals(EduLevelCodes.SREDNEE_OBTSHEE_OBRAZOVANIE);
    }

    // inner classes

    @SuppressWarnings("serial")
    public static class CompetitionRowWrapper extends IdentifiableWrapper<EnrRequestedCompetition>
    {
        private EnrRequestedCompetition competitionGroup;
        private List<DiscWrapper> discRowList = new ArrayList<>();

        private CompetitionRowWrapper(EnrRequestedCompetition i) throws ClassCastException {
            super(i);
            competitionGroup = i;
        }

        public String getCompetitionTitle() { return getCompetition().getTitle(); }
        public EnrRequestedCompetition getCompetition() { return competitionGroup; }
        public List<DiscWrapper> getDiscRowList() { return discRowList; }
        public int getRowspan() { return getDiscRowList().size(); }
    }

    @SuppressWarnings("serial")
    public static class DiscWrapper extends IdentifiableWrapper<EnrExamVariant> implements IEnrEntrantDao.IChosenExamWrapper
    {
        private EnrExamVariant exam;
        private List<EnrCampaignDiscipline> discList;
        private EnrCampaignDiscipline disc;
        private Set<EnrExamPassForm> allowedPassForms = new HashSet<>();
        private Set<EnrExamPassForm> selectedPassForms = new HashSet<>();
        private EnrBenefitCategory diplomaCategory;
        private IEnrEntrantBenefitProofDocument diploma;

        private DiscWrapper(EnrRequestedCompetition competition, EnrExamVariant exam) throws ClassCastException {
            super(countWrapperId(competition, exam), exam.getTitle());
            this.exam = exam;
        }

        public EnrExamVariant getExam() { return exam; }
        public EnrExamSetElement getExamSetElement() { return exam.getExamSetElement(); }
        public List<EnrCampaignDiscipline> getDiscList() { return discList; }
        public void setDiscList(List<EnrCampaignDiscipline> discList) { this.discList = discList; }
        public EnrCampaignDiscipline getDisc() { return disc; }
        public void setDisc(EnrCampaignDiscipline disc) { this.disc = disc; }
        public Set<EnrExamPassForm> getAllowedPassForms() { return allowedPassForms; }
        @Override public Set<EnrExamPassForm> getSelectedPassForms() { return selectedPassForms; }
        @Override public EnrExamVariant getActualExam() { return getExam(); }
        @Override public EnrCampaignDiscipline getDiscipline() { return getDisc(); }

        @Override public EnrBenefitCategory getDiplomaCategory() { return diplomaCategory; }
        public void setDiplomaCategory(EnrBenefitCategory diplomaCategory) { this.diplomaCategory = diplomaCategory; }

        @Override public IEnrEntrantBenefitProofDocument getDiploma() { return diploma; }
        public void setDiploma(IEnrEntrantBenefitProofDocument diploma) { this.diploma = diploma; }

        private static final Map<String, Integer> requestTypeCodeHash = ImmutableMap.<String, Integer>builder()
                .put(EnrRequestTypeCodes.BS, 1)
                .put(EnrRequestTypeCodes.MASTER, 2)
                .put(EnrRequestTypeCodes.HIGHER, 3)
                .put(EnrRequestTypeCodes.POSTGRADUATE, 4)
                .put(EnrRequestTypeCodes.TRAINEESHIP, 5)
                .put(EnrRequestTypeCodes.INTERNSHIP, 6)
                .put(EnrRequestTypeCodes.SPO, 7)
                .build();

        private static long countWrapperId(EnrRequestedCompetition competition, EnrExamVariant exam) { return (long) requestTypeCodeHash.get(competition.getRequest().getType().getCode()) * 10000 + competition.getPriority() * 100 + exam.getExamSetElement().getNumber(); }
        public static Long countWrapperId(EnrChosenEntranceExam chosenEntranceExam) { return countWrapperId(chosenEntranceExam.getRequestedCompetition(), chosenEntranceExam.getActualExam()); }
    }

    // getters and setters

    public EnrEntrant getEntrant()
    {
        return entrant;
    }

    public void setEntrant(EnrEntrant entrant)
    {
        this.entrant = entrant;
    }

    public List<EnrExamPassForm> getPassFormList()
    {
        return passFormList;
    }

    public void setPassFormList(List<EnrExamPassForm> passFormList)
    {
        this.passFormList = passFormList;
    }

    public EnrExamPassForm getCurrentPassForm()
    {
        return currentPassForm;
    }

    public void setCurrentPassForm(EnrExamPassForm currentPassForm)
    {
        this.currentPassForm = currentPassForm;
    }

    public List<CompetitionRowWrapper> getRowList()
    {
        return rowList;
    }

    public void setRowList(List<CompetitionRowWrapper> rowList)
    {
        this.rowList = rowList;
    }

    public CompetitionRowWrapper getCurrentRow()
    {
        return currentRow;
    }

    public void setCurrentRow(CompetitionRowWrapper currentRow)
    {
        this.currentRow = currentRow;
    }

    public DiscWrapper getCurrentDiscRow()
    {
        return currentDiscRow;
    }

    public void setCurrentDiscRow(DiscWrapper currentDiscRow)
    {
        this.currentDiscRow = currentDiscRow;
    }

    public List<IEnrEntrantBenefitProofDocument> getDiplomaAllList()
    {
        return diplomaAllList;
    }

    public void setDiplomaAllList(List<IEnrEntrantBenefitProofDocument> diplomaAllList)
    {
        this.diplomaAllList = diplomaAllList;
    }

    public List<EnrBenefitCategory> getDiplomaCategoryList()
    {
        return this.diplomaCategoryList;
    }

    public void setDiplomaCategoryList(List<EnrBenefitCategory> diplomaCategoryList)
    {
        this.diplomaCategoryList = diplomaCategoryList;
    }

    public Map<Long, DiscWrapper> getDiscWrapperByIdMap()
    {
        return discWrapperByIdMap;
    }

    public EnrExamPassForm getPassFormOlympiad()
    {
        return passFormOlympiad;
    }

    public void setPassFormOlympiad(EnrExamPassForm passFormOlympiad)
    {
        this.passFormOlympiad = passFormOlympiad;
    }

    public boolean isNoExamsToChoose()
    {
        return noExamsToChoose;
    }

    public void setNoExamsToChoose(boolean noExamsToChoose)
    {
        this.noExamsToChoose = noExamsToChoose;
    }

    public boolean isAllowSelectInternalExamReason(){ return allowSelectInternalExamReason; }

    public void setAllowSelectInternalExamReason(boolean allowSelectInternalExamReason){ this.allowSelectInternalExamReason = allowSelectInternalExamReason; }

    public EnrInternalExamReason getInternalExamReason(){ return internalExamReason; }

    public void setInternalExamReason(EnrInternalExamReason internalExamReason){ this.internalExamReason = internalExamReason; }
}