package ru.tandemservice.unienr14.entrant.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.person.catalog.entity.EduDocumentKind;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import org.tandemframework.shared.person.catalog.entity.GraduationHonour;
import ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Документ об образовании
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrOnlineEntrantEduDocumentGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument";
    public static final String ENTITY_NAME = "enrOnlineEntrantEduDocument";
    public static final int VERSION_HASH = -1249851859;
    private static IEntityMeta ENTITY_META;

    public static final String P_DOCUMENT_KIND_DETAILED = "documentKindDetailed";
    public static final String P_SERIA = "seria";
    public static final String P_NUMBER = "number";
    public static final String P_EDU_ORGANIZATION_SETTLEMENT = "eduOrganizationSettlement";
    public static final String P_EDU_ORGANIZATION = "eduOrganization";
    public static final String P_YEAR_END = "yearEnd";
    public static final String P_RU_REGION_CODE = "ruRegionCode";
    public static final String P_ISSUANCE_DATE = "issuanceDate";
    public static final String P_DOCUMENT_EDUCATION_LEVEL = "documentEducationLevel";
    public static final String P_QUALIFICATION = "qualification";
    public static final String P_EDU_PROGRAM_SUBJECT = "eduProgramSubject";
    public static final String P_AVG_MARK_AS_LONG = "avgMarkAsLong";
    public static final String P_MARK5 = "mark5";
    public static final String P_MARK4 = "mark4";
    public static final String P_MARK3 = "mark3";
    public static final String P_REGISTRATION_NUMBER = "registrationNumber";
    public static final String L_EDU_ORG_COUNTRY = "eduOrgCountry";
    public static final String L_EDU_LEVEL = "eduLevel";
    public static final String L_HONOUR = "honour";
    public static final String L_DOC_KIND = "docKind";
    public static final String L_ONLINE_ENTRANT = "onlineEntrant";
    public static final String L_SCAN_COPY = "scanCopy";

    private String _documentKindDetailed;     // Вид документа (по документу, для иных документов)
    private String _seria;     // Серия
    private String _number;     // Номер
    private String _eduOrganizationSettlement;     // Населенный пункт образовательной организации
    private String _eduOrganization;     // Образовательная организация
    private Integer _yearEnd;     // Год окончания обучения
    private String _ruRegionCode;     // Код субъекта РФ
    private Date _issuanceDate;     // Дата выдачи
    private String _documentEducationLevel;     // Уровень образования по документу
    private String _qualification;     // Квалификация
    private String _eduProgramSubject;     // Направление (профессия, специальность)
    private Long _avgMarkAsLong;     // Средний балл
    private Integer _mark5;     // Кол-во оценок «Отлично»
    private Integer _mark4;     // Кол-во оценок «Хорошо»
    private Integer _mark3;     // Кол-во оценок «Удовлетворительно»
    private String _registrationNumber;     // Регистрационный номер
    private AddressCountry _eduOrgCountry;     // Страна образовательной организации
    private EduLevel _eduLevel;     // Актуальный уровень образования
    private GraduationHonour _honour;     // Степень отличия по окончании образовательного учреждения
    private EduDocumentKind _docKind;     // Вид документа
    private EnrOnlineEntrant _onlineEntrant;     // Абитуриент
    private DatabaseFile _scanCopy;     // Скан-копия документа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Вид документа (по документу, для иных документов).
     */
    @Length(max=255)
    public String getDocumentKindDetailed()
    {
        return _documentKindDetailed;
    }

    /**
     * @param documentKindDetailed Вид документа (по документу, для иных документов).
     */
    public void setDocumentKindDetailed(String documentKindDetailed)
    {
        dirty(_documentKindDetailed, documentKindDetailed);
        _documentKindDetailed = documentKindDetailed;
    }

    /**
     * @return Серия.
     */
    @Length(max=255)
    public String getSeria()
    {
        return _seria;
    }

    /**
     * @param seria Серия.
     */
    public void setSeria(String seria)
    {
        dirty(_seria, seria);
        _seria = seria;
    }

    /**
     * @return Номер.
     */
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Населенный пункт образовательной организации.
     */
    @Length(max=255)
    public String getEduOrganizationSettlement()
    {
        return _eduOrganizationSettlement;
    }

    /**
     * @param eduOrganizationSettlement Населенный пункт образовательной организации.
     */
    public void setEduOrganizationSettlement(String eduOrganizationSettlement)
    {
        dirty(_eduOrganizationSettlement, eduOrganizationSettlement);
        _eduOrganizationSettlement = eduOrganizationSettlement;
    }

    /**
     * @return Образовательная организация.
     */
    @Length(max=255)
    public String getEduOrganization()
    {
        return _eduOrganization;
    }

    /**
     * @param eduOrganization Образовательная организация.
     */
    public void setEduOrganization(String eduOrganization)
    {
        dirty(_eduOrganization, eduOrganization);
        _eduOrganization = eduOrganization;
    }

    /**
     * @return Год окончания обучения.
     */
    public Integer getYearEnd()
    {
        return _yearEnd;
    }

    /**
     * @param yearEnd Год окончания обучения.
     */
    public void setYearEnd(Integer yearEnd)
    {
        dirty(_yearEnd, yearEnd);
        _yearEnd = yearEnd;
    }

    /**
     * @return Код субъекта РФ.
     */
    @Length(max=255)
    public String getRuRegionCode()
    {
        return _ruRegionCode;
    }

    /**
     * @param ruRegionCode Код субъекта РФ.
     */
    public void setRuRegionCode(String ruRegionCode)
    {
        dirty(_ruRegionCode, ruRegionCode);
        _ruRegionCode = ruRegionCode;
    }

    /**
     * @return Дата выдачи.
     */
    public Date getIssuanceDate()
    {
        return _issuanceDate;
    }

    /**
     * @param issuanceDate Дата выдачи.
     */
    public void setIssuanceDate(Date issuanceDate)
    {
        dirty(_issuanceDate, issuanceDate);
        _issuanceDate = issuanceDate;
    }

    /**
     * @return Уровень образования по документу.
     */
    @Length(max=255)
    public String getDocumentEducationLevel()
    {
        return _documentEducationLevel;
    }

    /**
     * @param documentEducationLevel Уровень образования по документу.
     */
    public void setDocumentEducationLevel(String documentEducationLevel)
    {
        dirty(_documentEducationLevel, documentEducationLevel);
        _documentEducationLevel = documentEducationLevel;
    }

    /**
     * @return Квалификация.
     */
    @Length(max=255)
    public String getQualification()
    {
        return _qualification;
    }

    /**
     * @param qualification Квалификация.
     */
    public void setQualification(String qualification)
    {
        dirty(_qualification, qualification);
        _qualification = qualification;
    }

    /**
     * @return Направление (профессия, специальность).
     */
    @Length(max=255)
    public String getEduProgramSubject()
    {
        return _eduProgramSubject;
    }

    /**
     * @param eduProgramSubject Направление (профессия, специальность).
     */
    public void setEduProgramSubject(String eduProgramSubject)
    {
        dirty(_eduProgramSubject, eduProgramSubject);
        _eduProgramSubject = eduProgramSubject;
    }

    /**
     * @return Средний балл.
     */
    public Long getAvgMarkAsLong()
    {
        return _avgMarkAsLong;
    }

    /**
     * @param avgMarkAsLong Средний балл.
     */
    public void setAvgMarkAsLong(Long avgMarkAsLong)
    {
        dirty(_avgMarkAsLong, avgMarkAsLong);
        _avgMarkAsLong = avgMarkAsLong;
    }

    /**
     * @return Кол-во оценок «Отлично».
     */
    public Integer getMark5()
    {
        return _mark5;
    }

    /**
     * @param mark5 Кол-во оценок «Отлично».
     */
    public void setMark5(Integer mark5)
    {
        dirty(_mark5, mark5);
        _mark5 = mark5;
    }

    /**
     * @return Кол-во оценок «Хорошо».
     */
    public Integer getMark4()
    {
        return _mark4;
    }

    /**
     * @param mark4 Кол-во оценок «Хорошо».
     */
    public void setMark4(Integer mark4)
    {
        dirty(_mark4, mark4);
        _mark4 = mark4;
    }

    /**
     * @return Кол-во оценок «Удовлетворительно».
     */
    public Integer getMark3()
    {
        return _mark3;
    }

    /**
     * @param mark3 Кол-во оценок «Удовлетворительно».
     */
    public void setMark3(Integer mark3)
    {
        dirty(_mark3, mark3);
        _mark3 = mark3;
    }

    /**
     * @return Регистрационный номер.
     */
    @Length(max=255)
    public String getRegistrationNumber()
    {
        return _registrationNumber;
    }

    /**
     * @param registrationNumber Регистрационный номер.
     */
    public void setRegistrationNumber(String registrationNumber)
    {
        dirty(_registrationNumber, registrationNumber);
        _registrationNumber = registrationNumber;
    }

    /**
     * @return Страна образовательной организации. Свойство не может быть null.
     */
    @NotNull
    public AddressCountry getEduOrgCountry()
    {
        return _eduOrgCountry;
    }

    /**
     * @param eduOrgCountry Страна образовательной организации. Свойство не может быть null.
     */
    public void setEduOrgCountry(AddressCountry eduOrgCountry)
    {
        dirty(_eduOrgCountry, eduOrgCountry);
        _eduOrgCountry = eduOrgCountry;
    }

    /**
     * @return Актуальный уровень образования.
     */
    public EduLevel getEduLevel()
    {
        return _eduLevel;
    }

    /**
     * @param eduLevel Актуальный уровень образования.
     */
    public void setEduLevel(EduLevel eduLevel)
    {
        dirty(_eduLevel, eduLevel);
        _eduLevel = eduLevel;
    }

    /**
     * @return Степень отличия по окончании образовательного учреждения.
     */
    public GraduationHonour getHonour()
    {
        return _honour;
    }

    /**
     * @param honour Степень отличия по окончании образовательного учреждения.
     */
    public void setHonour(GraduationHonour honour)
    {
        dirty(_honour, honour);
        _honour = honour;
    }

    /**
     * @return Вид документа. Свойство не может быть null.
     */
    @NotNull
    public EduDocumentKind getDocKind()
    {
        return _docKind;
    }

    /**
     * @param docKind Вид документа. Свойство не может быть null.
     */
    public void setDocKind(EduDocumentKind docKind)
    {
        dirty(_docKind, docKind);
        _docKind = docKind;
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     */
    @NotNull
    public EnrOnlineEntrant getOnlineEntrant()
    {
        return _onlineEntrant;
    }

    /**
     * @param onlineEntrant Абитуриент. Свойство не может быть null.
     */
    public void setOnlineEntrant(EnrOnlineEntrant onlineEntrant)
    {
        dirty(_onlineEntrant, onlineEntrant);
        _onlineEntrant = onlineEntrant;
    }

    /**
     * @return Скан-копия документа.
     */
    public DatabaseFile getScanCopy()
    {
        return _scanCopy;
    }

    /**
     * @param scanCopy Скан-копия документа.
     */
    public void setScanCopy(DatabaseFile scanCopy)
    {
        dirty(_scanCopy, scanCopy);
        _scanCopy = scanCopy;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrOnlineEntrantEduDocumentGen)
        {
            setDocumentKindDetailed(((EnrOnlineEntrantEduDocument)another).getDocumentKindDetailed());
            setSeria(((EnrOnlineEntrantEduDocument)another).getSeria());
            setNumber(((EnrOnlineEntrantEduDocument)another).getNumber());
            setEduOrganizationSettlement(((EnrOnlineEntrantEduDocument)another).getEduOrganizationSettlement());
            setEduOrganization(((EnrOnlineEntrantEduDocument)another).getEduOrganization());
            setYearEnd(((EnrOnlineEntrantEduDocument)another).getYearEnd());
            setRuRegionCode(((EnrOnlineEntrantEduDocument)another).getRuRegionCode());
            setIssuanceDate(((EnrOnlineEntrantEduDocument)another).getIssuanceDate());
            setDocumentEducationLevel(((EnrOnlineEntrantEduDocument)another).getDocumentEducationLevel());
            setQualification(((EnrOnlineEntrantEduDocument)another).getQualification());
            setEduProgramSubject(((EnrOnlineEntrantEduDocument)another).getEduProgramSubject());
            setAvgMarkAsLong(((EnrOnlineEntrantEduDocument)another).getAvgMarkAsLong());
            setMark5(((EnrOnlineEntrantEduDocument)another).getMark5());
            setMark4(((EnrOnlineEntrantEduDocument)another).getMark4());
            setMark3(((EnrOnlineEntrantEduDocument)another).getMark3());
            setRegistrationNumber(((EnrOnlineEntrantEduDocument)another).getRegistrationNumber());
            setEduOrgCountry(((EnrOnlineEntrantEduDocument)another).getEduOrgCountry());
            setEduLevel(((EnrOnlineEntrantEduDocument)another).getEduLevel());
            setHonour(((EnrOnlineEntrantEduDocument)another).getHonour());
            setDocKind(((EnrOnlineEntrantEduDocument)another).getDocKind());
            setOnlineEntrant(((EnrOnlineEntrantEduDocument)another).getOnlineEntrant());
            setScanCopy(((EnrOnlineEntrantEduDocument)another).getScanCopy());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrOnlineEntrantEduDocumentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrOnlineEntrantEduDocument.class;
        }

        public T newInstance()
        {
            return (T) new EnrOnlineEntrantEduDocument();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "documentKindDetailed":
                    return obj.getDocumentKindDetailed();
                case "seria":
                    return obj.getSeria();
                case "number":
                    return obj.getNumber();
                case "eduOrganizationSettlement":
                    return obj.getEduOrganizationSettlement();
                case "eduOrganization":
                    return obj.getEduOrganization();
                case "yearEnd":
                    return obj.getYearEnd();
                case "ruRegionCode":
                    return obj.getRuRegionCode();
                case "issuanceDate":
                    return obj.getIssuanceDate();
                case "documentEducationLevel":
                    return obj.getDocumentEducationLevel();
                case "qualification":
                    return obj.getQualification();
                case "eduProgramSubject":
                    return obj.getEduProgramSubject();
                case "avgMarkAsLong":
                    return obj.getAvgMarkAsLong();
                case "mark5":
                    return obj.getMark5();
                case "mark4":
                    return obj.getMark4();
                case "mark3":
                    return obj.getMark3();
                case "registrationNumber":
                    return obj.getRegistrationNumber();
                case "eduOrgCountry":
                    return obj.getEduOrgCountry();
                case "eduLevel":
                    return obj.getEduLevel();
                case "honour":
                    return obj.getHonour();
                case "docKind":
                    return obj.getDocKind();
                case "onlineEntrant":
                    return obj.getOnlineEntrant();
                case "scanCopy":
                    return obj.getScanCopy();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "documentKindDetailed":
                    obj.setDocumentKindDetailed((String) value);
                    return;
                case "seria":
                    obj.setSeria((String) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "eduOrganizationSettlement":
                    obj.setEduOrganizationSettlement((String) value);
                    return;
                case "eduOrganization":
                    obj.setEduOrganization((String) value);
                    return;
                case "yearEnd":
                    obj.setYearEnd((Integer) value);
                    return;
                case "ruRegionCode":
                    obj.setRuRegionCode((String) value);
                    return;
                case "issuanceDate":
                    obj.setIssuanceDate((Date) value);
                    return;
                case "documentEducationLevel":
                    obj.setDocumentEducationLevel((String) value);
                    return;
                case "qualification":
                    obj.setQualification((String) value);
                    return;
                case "eduProgramSubject":
                    obj.setEduProgramSubject((String) value);
                    return;
                case "avgMarkAsLong":
                    obj.setAvgMarkAsLong((Long) value);
                    return;
                case "mark5":
                    obj.setMark5((Integer) value);
                    return;
                case "mark4":
                    obj.setMark4((Integer) value);
                    return;
                case "mark3":
                    obj.setMark3((Integer) value);
                    return;
                case "registrationNumber":
                    obj.setRegistrationNumber((String) value);
                    return;
                case "eduOrgCountry":
                    obj.setEduOrgCountry((AddressCountry) value);
                    return;
                case "eduLevel":
                    obj.setEduLevel((EduLevel) value);
                    return;
                case "honour":
                    obj.setHonour((GraduationHonour) value);
                    return;
                case "docKind":
                    obj.setDocKind((EduDocumentKind) value);
                    return;
                case "onlineEntrant":
                    obj.setOnlineEntrant((EnrOnlineEntrant) value);
                    return;
                case "scanCopy":
                    obj.setScanCopy((DatabaseFile) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "documentKindDetailed":
                        return true;
                case "seria":
                        return true;
                case "number":
                        return true;
                case "eduOrganizationSettlement":
                        return true;
                case "eduOrganization":
                        return true;
                case "yearEnd":
                        return true;
                case "ruRegionCode":
                        return true;
                case "issuanceDate":
                        return true;
                case "documentEducationLevel":
                        return true;
                case "qualification":
                        return true;
                case "eduProgramSubject":
                        return true;
                case "avgMarkAsLong":
                        return true;
                case "mark5":
                        return true;
                case "mark4":
                        return true;
                case "mark3":
                        return true;
                case "registrationNumber":
                        return true;
                case "eduOrgCountry":
                        return true;
                case "eduLevel":
                        return true;
                case "honour":
                        return true;
                case "docKind":
                        return true;
                case "onlineEntrant":
                        return true;
                case "scanCopy":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "documentKindDetailed":
                    return true;
                case "seria":
                    return true;
                case "number":
                    return true;
                case "eduOrganizationSettlement":
                    return true;
                case "eduOrganization":
                    return true;
                case "yearEnd":
                    return true;
                case "ruRegionCode":
                    return true;
                case "issuanceDate":
                    return true;
                case "documentEducationLevel":
                    return true;
                case "qualification":
                    return true;
                case "eduProgramSubject":
                    return true;
                case "avgMarkAsLong":
                    return true;
                case "mark5":
                    return true;
                case "mark4":
                    return true;
                case "mark3":
                    return true;
                case "registrationNumber":
                    return true;
                case "eduOrgCountry":
                    return true;
                case "eduLevel":
                    return true;
                case "honour":
                    return true;
                case "docKind":
                    return true;
                case "onlineEntrant":
                    return true;
                case "scanCopy":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "documentKindDetailed":
                    return String.class;
                case "seria":
                    return String.class;
                case "number":
                    return String.class;
                case "eduOrganizationSettlement":
                    return String.class;
                case "eduOrganization":
                    return String.class;
                case "yearEnd":
                    return Integer.class;
                case "ruRegionCode":
                    return String.class;
                case "issuanceDate":
                    return Date.class;
                case "documentEducationLevel":
                    return String.class;
                case "qualification":
                    return String.class;
                case "eduProgramSubject":
                    return String.class;
                case "avgMarkAsLong":
                    return Long.class;
                case "mark5":
                    return Integer.class;
                case "mark4":
                    return Integer.class;
                case "mark3":
                    return Integer.class;
                case "registrationNumber":
                    return String.class;
                case "eduOrgCountry":
                    return AddressCountry.class;
                case "eduLevel":
                    return EduLevel.class;
                case "honour":
                    return GraduationHonour.class;
                case "docKind":
                    return EduDocumentKind.class;
                case "onlineEntrant":
                    return EnrOnlineEntrant.class;
                case "scanCopy":
                    return DatabaseFile.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrOnlineEntrantEduDocument> _dslPath = new Path<EnrOnlineEntrantEduDocument>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrOnlineEntrantEduDocument");
    }
            

    /**
     * @return Вид документа (по документу, для иных документов).
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument#getDocumentKindDetailed()
     */
    public static PropertyPath<String> documentKindDetailed()
    {
        return _dslPath.documentKindDetailed();
    }

    /**
     * @return Серия.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument#getSeria()
     */
    public static PropertyPath<String> seria()
    {
        return _dslPath.seria();
    }

    /**
     * @return Номер.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Населенный пункт образовательной организации.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument#getEduOrganizationSettlement()
     */
    public static PropertyPath<String> eduOrganizationSettlement()
    {
        return _dslPath.eduOrganizationSettlement();
    }

    /**
     * @return Образовательная организация.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument#getEduOrganization()
     */
    public static PropertyPath<String> eduOrganization()
    {
        return _dslPath.eduOrganization();
    }

    /**
     * @return Год окончания обучения.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument#getYearEnd()
     */
    public static PropertyPath<Integer> yearEnd()
    {
        return _dslPath.yearEnd();
    }

    /**
     * @return Код субъекта РФ.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument#getRuRegionCode()
     */
    public static PropertyPath<String> ruRegionCode()
    {
        return _dslPath.ruRegionCode();
    }

    /**
     * @return Дата выдачи.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument#getIssuanceDate()
     */
    public static PropertyPath<Date> issuanceDate()
    {
        return _dslPath.issuanceDate();
    }

    /**
     * @return Уровень образования по документу.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument#getDocumentEducationLevel()
     */
    public static PropertyPath<String> documentEducationLevel()
    {
        return _dslPath.documentEducationLevel();
    }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument#getQualification()
     */
    public static PropertyPath<String> qualification()
    {
        return _dslPath.qualification();
    }

    /**
     * @return Направление (профессия, специальность).
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument#getEduProgramSubject()
     */
    public static PropertyPath<String> eduProgramSubject()
    {
        return _dslPath.eduProgramSubject();
    }

    /**
     * @return Средний балл.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument#getAvgMarkAsLong()
     */
    public static PropertyPath<Long> avgMarkAsLong()
    {
        return _dslPath.avgMarkAsLong();
    }

    /**
     * @return Кол-во оценок «Отлично».
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument#getMark5()
     */
    public static PropertyPath<Integer> mark5()
    {
        return _dslPath.mark5();
    }

    /**
     * @return Кол-во оценок «Хорошо».
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument#getMark4()
     */
    public static PropertyPath<Integer> mark4()
    {
        return _dslPath.mark4();
    }

    /**
     * @return Кол-во оценок «Удовлетворительно».
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument#getMark3()
     */
    public static PropertyPath<Integer> mark3()
    {
        return _dslPath.mark3();
    }

    /**
     * @return Регистрационный номер.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument#getRegistrationNumber()
     */
    public static PropertyPath<String> registrationNumber()
    {
        return _dslPath.registrationNumber();
    }

    /**
     * @return Страна образовательной организации. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument#getEduOrgCountry()
     */
    public static AddressCountry.Path<AddressCountry> eduOrgCountry()
    {
        return _dslPath.eduOrgCountry();
    }

    /**
     * @return Актуальный уровень образования.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument#getEduLevel()
     */
    public static EduLevel.Path<EduLevel> eduLevel()
    {
        return _dslPath.eduLevel();
    }

    /**
     * @return Степень отличия по окончании образовательного учреждения.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument#getHonour()
     */
    public static GraduationHonour.Path<GraduationHonour> honour()
    {
        return _dslPath.honour();
    }

    /**
     * @return Вид документа. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument#getDocKind()
     */
    public static EduDocumentKind.Path<EduDocumentKind> docKind()
    {
        return _dslPath.docKind();
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument#getOnlineEntrant()
     */
    public static EnrOnlineEntrant.Path<EnrOnlineEntrant> onlineEntrant()
    {
        return _dslPath.onlineEntrant();
    }

    /**
     * @return Скан-копия документа.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument#getScanCopy()
     */
    public static DatabaseFile.Path<DatabaseFile> scanCopy()
    {
        return _dslPath.scanCopy();
    }

    public static class Path<E extends EnrOnlineEntrantEduDocument> extends EntityPath<E>
    {
        private PropertyPath<String> _documentKindDetailed;
        private PropertyPath<String> _seria;
        private PropertyPath<String> _number;
        private PropertyPath<String> _eduOrganizationSettlement;
        private PropertyPath<String> _eduOrganization;
        private PropertyPath<Integer> _yearEnd;
        private PropertyPath<String> _ruRegionCode;
        private PropertyPath<Date> _issuanceDate;
        private PropertyPath<String> _documentEducationLevel;
        private PropertyPath<String> _qualification;
        private PropertyPath<String> _eduProgramSubject;
        private PropertyPath<Long> _avgMarkAsLong;
        private PropertyPath<Integer> _mark5;
        private PropertyPath<Integer> _mark4;
        private PropertyPath<Integer> _mark3;
        private PropertyPath<String> _registrationNumber;
        private AddressCountry.Path<AddressCountry> _eduOrgCountry;
        private EduLevel.Path<EduLevel> _eduLevel;
        private GraduationHonour.Path<GraduationHonour> _honour;
        private EduDocumentKind.Path<EduDocumentKind> _docKind;
        private EnrOnlineEntrant.Path<EnrOnlineEntrant> _onlineEntrant;
        private DatabaseFile.Path<DatabaseFile> _scanCopy;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Вид документа (по документу, для иных документов).
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument#getDocumentKindDetailed()
     */
        public PropertyPath<String> documentKindDetailed()
        {
            if(_documentKindDetailed == null )
                _documentKindDetailed = new PropertyPath<String>(EnrOnlineEntrantEduDocumentGen.P_DOCUMENT_KIND_DETAILED, this);
            return _documentKindDetailed;
        }

    /**
     * @return Серия.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument#getSeria()
     */
        public PropertyPath<String> seria()
        {
            if(_seria == null )
                _seria = new PropertyPath<String>(EnrOnlineEntrantEduDocumentGen.P_SERIA, this);
            return _seria;
        }

    /**
     * @return Номер.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(EnrOnlineEntrantEduDocumentGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Населенный пункт образовательной организации.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument#getEduOrganizationSettlement()
     */
        public PropertyPath<String> eduOrganizationSettlement()
        {
            if(_eduOrganizationSettlement == null )
                _eduOrganizationSettlement = new PropertyPath<String>(EnrOnlineEntrantEduDocumentGen.P_EDU_ORGANIZATION_SETTLEMENT, this);
            return _eduOrganizationSettlement;
        }

    /**
     * @return Образовательная организация.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument#getEduOrganization()
     */
        public PropertyPath<String> eduOrganization()
        {
            if(_eduOrganization == null )
                _eduOrganization = new PropertyPath<String>(EnrOnlineEntrantEduDocumentGen.P_EDU_ORGANIZATION, this);
            return _eduOrganization;
        }

    /**
     * @return Год окончания обучения.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument#getYearEnd()
     */
        public PropertyPath<Integer> yearEnd()
        {
            if(_yearEnd == null )
                _yearEnd = new PropertyPath<Integer>(EnrOnlineEntrantEduDocumentGen.P_YEAR_END, this);
            return _yearEnd;
        }

    /**
     * @return Код субъекта РФ.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument#getRuRegionCode()
     */
        public PropertyPath<String> ruRegionCode()
        {
            if(_ruRegionCode == null )
                _ruRegionCode = new PropertyPath<String>(EnrOnlineEntrantEduDocumentGen.P_RU_REGION_CODE, this);
            return _ruRegionCode;
        }

    /**
     * @return Дата выдачи.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument#getIssuanceDate()
     */
        public PropertyPath<Date> issuanceDate()
        {
            if(_issuanceDate == null )
                _issuanceDate = new PropertyPath<Date>(EnrOnlineEntrantEduDocumentGen.P_ISSUANCE_DATE, this);
            return _issuanceDate;
        }

    /**
     * @return Уровень образования по документу.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument#getDocumentEducationLevel()
     */
        public PropertyPath<String> documentEducationLevel()
        {
            if(_documentEducationLevel == null )
                _documentEducationLevel = new PropertyPath<String>(EnrOnlineEntrantEduDocumentGen.P_DOCUMENT_EDUCATION_LEVEL, this);
            return _documentEducationLevel;
        }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument#getQualification()
     */
        public PropertyPath<String> qualification()
        {
            if(_qualification == null )
                _qualification = new PropertyPath<String>(EnrOnlineEntrantEduDocumentGen.P_QUALIFICATION, this);
            return _qualification;
        }

    /**
     * @return Направление (профессия, специальность).
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument#getEduProgramSubject()
     */
        public PropertyPath<String> eduProgramSubject()
        {
            if(_eduProgramSubject == null )
                _eduProgramSubject = new PropertyPath<String>(EnrOnlineEntrantEduDocumentGen.P_EDU_PROGRAM_SUBJECT, this);
            return _eduProgramSubject;
        }

    /**
     * @return Средний балл.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument#getAvgMarkAsLong()
     */
        public PropertyPath<Long> avgMarkAsLong()
        {
            if(_avgMarkAsLong == null )
                _avgMarkAsLong = new PropertyPath<Long>(EnrOnlineEntrantEduDocumentGen.P_AVG_MARK_AS_LONG, this);
            return _avgMarkAsLong;
        }

    /**
     * @return Кол-во оценок «Отлично».
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument#getMark5()
     */
        public PropertyPath<Integer> mark5()
        {
            if(_mark5 == null )
                _mark5 = new PropertyPath<Integer>(EnrOnlineEntrantEduDocumentGen.P_MARK5, this);
            return _mark5;
        }

    /**
     * @return Кол-во оценок «Хорошо».
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument#getMark4()
     */
        public PropertyPath<Integer> mark4()
        {
            if(_mark4 == null )
                _mark4 = new PropertyPath<Integer>(EnrOnlineEntrantEduDocumentGen.P_MARK4, this);
            return _mark4;
        }

    /**
     * @return Кол-во оценок «Удовлетворительно».
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument#getMark3()
     */
        public PropertyPath<Integer> mark3()
        {
            if(_mark3 == null )
                _mark3 = new PropertyPath<Integer>(EnrOnlineEntrantEduDocumentGen.P_MARK3, this);
            return _mark3;
        }

    /**
     * @return Регистрационный номер.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument#getRegistrationNumber()
     */
        public PropertyPath<String> registrationNumber()
        {
            if(_registrationNumber == null )
                _registrationNumber = new PropertyPath<String>(EnrOnlineEntrantEduDocumentGen.P_REGISTRATION_NUMBER, this);
            return _registrationNumber;
        }

    /**
     * @return Страна образовательной организации. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument#getEduOrgCountry()
     */
        public AddressCountry.Path<AddressCountry> eduOrgCountry()
        {
            if(_eduOrgCountry == null )
                _eduOrgCountry = new AddressCountry.Path<AddressCountry>(L_EDU_ORG_COUNTRY, this);
            return _eduOrgCountry;
        }

    /**
     * @return Актуальный уровень образования.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument#getEduLevel()
     */
        public EduLevel.Path<EduLevel> eduLevel()
        {
            if(_eduLevel == null )
                _eduLevel = new EduLevel.Path<EduLevel>(L_EDU_LEVEL, this);
            return _eduLevel;
        }

    /**
     * @return Степень отличия по окончании образовательного учреждения.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument#getHonour()
     */
        public GraduationHonour.Path<GraduationHonour> honour()
        {
            if(_honour == null )
                _honour = new GraduationHonour.Path<GraduationHonour>(L_HONOUR, this);
            return _honour;
        }

    /**
     * @return Вид документа. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument#getDocKind()
     */
        public EduDocumentKind.Path<EduDocumentKind> docKind()
        {
            if(_docKind == null )
                _docKind = new EduDocumentKind.Path<EduDocumentKind>(L_DOC_KIND, this);
            return _docKind;
        }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument#getOnlineEntrant()
     */
        public EnrOnlineEntrant.Path<EnrOnlineEntrant> onlineEntrant()
        {
            if(_onlineEntrant == null )
                _onlineEntrant = new EnrOnlineEntrant.Path<EnrOnlineEntrant>(L_ONLINE_ENTRANT, this);
            return _onlineEntrant;
        }

    /**
     * @return Скан-копия документа.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument#getScanCopy()
     */
        public DatabaseFile.Path<DatabaseFile> scanCopy()
        {
            if(_scanCopy == null )
                _scanCopy = new DatabaseFile.Path<DatabaseFile>(L_SCAN_COPY, this);
            return _scanCopy;
        }

        public Class getEntityClass()
        {
            return EnrOnlineEntrantEduDocument.class;
        }

        public String getEntityName()
        {
            return "enrOnlineEntrantEduDocument";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
