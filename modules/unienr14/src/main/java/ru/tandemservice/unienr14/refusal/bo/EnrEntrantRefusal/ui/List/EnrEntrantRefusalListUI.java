/* $Id:$ */
package ru.tandemservice.unienr14.refusal.bo.EnrEntrantRefusal.ui.List;


import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.PublisherActivator;
import ru.tandemservice.unienr14.refusal.bo.EnrEntrantRefusal.EnrEntrantRefusalManager;
import ru.tandemservice.unienr14.refusal.bo.EnrEntrantRefusal.ui.AddEdit.EnrEntrantRefusalAddEdit;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

/**
 * @author rsizonenko
 * @since 20.05.2014.
 */

public class EnrEntrantRefusalListUI extends UIPresenter {

    public static final String ORG_UNIT = "orgUnit";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String MIDDLE_NAME = "middleName";
    public static final String REGISTERED_TO_FILTER = "registeredToFilter";
    public static final String REGISTERED_FROM_FILTER = "registeredFromFilter";

    private EnrEnrollmentCampaign _enrEnrollmentCampaign;

    // override methods from UIPresenter

    @Override
    public void onComponentRefresh() {
        setEnrEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(ORG_UNIT, getSettings().get(ORG_UNIT));
        dataSource.put(EnrEntrantRefusalList.ENR_ENROLLMENT_CAMPAIGN, getEnrEnrollmentCampaign());
        dataSource.put(FIRST_NAME, getSettings().get(FIRST_NAME));
        dataSource.put(LAST_NAME, getSettings().get(LAST_NAME));
        dataSource.put(MIDDLE_NAME, getSettings().get(MIDDLE_NAME));
        dataSource.put(REGISTERED_FROM_FILTER, getSettings().get(REGISTERED_FROM_FILTER));
        dataSource.put(REGISTERED_TO_FILTER, getSettings().get(REGISTERED_TO_FILTER));
    }

    // listeners

    public void onClickEditEnrEntrantRefusal()
    {
        _uiActivation.asRegionDialog(EnrEntrantRefusalAddEdit.class).parameter(PublisherActivator.PUBLISHER_ID_KEY, getListenerParameter()).activate();
    }

    public void onClickAddEnrEntrantRefusal()
    {
        _uiActivation.asRegionDialog(EnrEntrantRefusalAddEdit.class).activate();
    }

    public void onClickDeleteEnrEntrantRefusal()
    {
        EnrEntrantRefusalManager.instance().dao().deleteEntrantRefusal(getListenerParameterAsLong());
    }

    public void onClickSearch()
    {
        saveSettings();
    }

    public void onClickClear()
    {
        clearSettings();
        onClickSearch();
    }

    public void onEnrollmentCampaignRefresh()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrEnrollmentCampaign());
    }

    public boolean isNothingSelected() {
        return getEnrEnrollmentCampaign() == null;
    }

    // getters-setters

    public EnrEnrollmentCampaign getEnrEnrollmentCampaign()
    {
        return _enrEnrollmentCampaign;
    }

    public void setEnrEnrollmentCampaign(EnrEnrollmentCampaign enrEnrollmentCampaign) {
        _enrEnrollmentCampaign = enrEnrollmentCampaign;
    }
}
