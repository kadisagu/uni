package ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.event;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.dsl.EntityPath;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import org.tandemframework.shared.commonbase.events.EventListenerLocker;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.EnrEntrantRequestManager;
import ru.tandemservice.unienr14.request.entity.*;

import java.util.HashSet;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class EnrEntrantRequestBlockListenerBean {


    public static class Listener implements IDSetEventListener {

        private final String path;

        private Listener(EntityPath<EnrEntrantRequest> requestPath) {
            this.path = (null == requestPath ? null : requestPath.toString());
        }

        @Override public void onEvent(DSetEvent event)
        {
            final IEntityMeta entityMeta = event.getMultitude().getEntityMeta();
            final Number count = new DQLSelectBuilder().fromEntity(entityMeta.getEntityClass(), "x")
            .where(in(property("x.id"), event.getMultitude().getInExpression()))
            .where(isNotNull(property(EnrEntrantRequest.takeAwayDocumentDate().fromAlias(null == this.path ? "x" : ("x."+this.path)))))
            .column(DQLFunctions.count(property("x.id")))
            .createStatement(event.getContext())
            .uniqueResult();

            if ((null != count) && (count.intValue() > 0)) {
                final EventListenerLocker.Handler<Listener> handler = LOCKER.handler();
                final Set<String> affectedProperties = new HashSet<String>(event.getMultitude().getAffectedProperties());
                affectedProperties.remove(EnrEntrantRequest.P_VERSION);

                if (EnrEntrantRequest.class.isAssignableFrom(entityMeta.getEntityClass())) {
                    affectedProperties.remove(EnrEntrantRequest.P_TAKE_AWAY_DOCUMENT_DATE);
                }

                if (EnrRequestedCompetition.class.isAssignableFrom(entityMeta.getEntityClass())) {
                    affectedProperties.remove(EnrRequestedCompetition.P_PRIORITY);
                    affectedProperties.remove(EnrRequestedCompetition.L_STATE);
                }

                if (EnrRequestedProgram.class.isAssignableFrom(entityMeta.getEntityClass())) {
                    affectedProperties.remove(EnrRequestedProgram.P_PRIORITY);
                }

                if (affectedProperties.size() > 0) {
                    if (!handler.handle(this, entityMeta, affectedProperties, count, event)) {
                        throw new ApplicationException(EnrEntrantRequestManager.instance().getProperty("fatal.take-away-request-change"), true);
                    }
                }
            }
        }
    }

    public static final EventListenerLocker<Listener> LOCKER = new EventListenerLocker<Listener>();

    @SuppressWarnings("unchecked")
    public void init() {

        Object rows[][] = {
            {EnrEntrantRequest.class, null },
            {EnrRequestedCompetition.class, EnrRequestedCompetition.request() },
            {EnrRequestedProgram.class, EnrRequestedProgram.requestedCompetition().request() },
            {EnrEntrantBenefitProof.class, EnrEntrantBenefitProof.benefitStatement().entrant() },
            {EnrEntrantRequestAttachment.class, EnrEntrantRequestAttachment.entrantRequest() },
        };

        for (Object[] row: rows) {
            Class<? extends IEntity> entityClass = (Class<? extends IEntity>) row[0];
            EntityPath<EnrEntrantRequest> requestPath = (EntityPath<EnrEntrantRequest>) row[1];

            Listener listener = new Listener(requestPath);
            DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, entityClass, listener);
            DSetEventManager.getInstance().registerListener(DSetEventType.beforeUpdate, entityClass, listener);
            DSetEventManager.getInstance().registerListener(DSetEventType.beforeDelete, entityClass, listener);
        }
    }


}
