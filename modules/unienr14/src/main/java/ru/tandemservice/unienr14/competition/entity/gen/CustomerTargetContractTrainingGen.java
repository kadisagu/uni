package ru.tandemservice.unienr14.competition.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import ru.tandemservice.unienr14.competition.entity.CustomerTargetContractTraining;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Организация-заказчик целевой контрактной подготовки
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class CustomerTargetContractTrainingGen extends EntityBase
 implements INaturalIdentifiable<CustomerTargetContractTrainingGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.competition.entity.CustomerTargetContractTraining";
    public static final String ENTITY_NAME = "customerTargetContractTraining";
    public static final int VERSION_HASH = 1469447353;
    private static IEntityMeta ENTITY_META;

    public static final String L_PROGRAM_SET = "programSet";
    public static final String L_EXTERNAL_ORG_UNIT = "externalOrgUnit";

    private EnrProgramSetBase _programSet;     // Набор ОП
    private ExternalOrgUnit _externalOrgUnit;     // Внешняя организация

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Набор ОП. Свойство не может быть null.
     */
    @NotNull
    public EnrProgramSetBase getProgramSet()
    {
        return _programSet;
    }

    /**
     * @param programSet Набор ОП. Свойство не может быть null.
     */
    public void setProgramSet(EnrProgramSetBase programSet)
    {
        dirty(_programSet, programSet);
        _programSet = programSet;
    }

    /**
     * @return Внешняя организация. Свойство не может быть null.
     */
    @NotNull
    public ExternalOrgUnit getExternalOrgUnit()
    {
        return _externalOrgUnit;
    }

    /**
     * @param externalOrgUnit Внешняя организация. Свойство не может быть null.
     */
    public void setExternalOrgUnit(ExternalOrgUnit externalOrgUnit)
    {
        dirty(_externalOrgUnit, externalOrgUnit);
        _externalOrgUnit = externalOrgUnit;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof CustomerTargetContractTrainingGen)
        {
            if (withNaturalIdProperties)
            {
                setProgramSet(((CustomerTargetContractTraining)another).getProgramSet());
                setExternalOrgUnit(((CustomerTargetContractTraining)another).getExternalOrgUnit());
            }
        }
    }

    public INaturalId<CustomerTargetContractTrainingGen> getNaturalId()
    {
        return new NaturalId(getProgramSet(), getExternalOrgUnit());
    }

    public static class NaturalId extends NaturalIdBase<CustomerTargetContractTrainingGen>
    {
        private static final String PROXY_NAME = "CustomerTargetContractTrainingNaturalProxy";

        private Long _programSet;
        private Long _externalOrgUnit;

        public NaturalId()
        {}

        public NaturalId(EnrProgramSetBase programSet, ExternalOrgUnit externalOrgUnit)
        {
            _programSet = ((IEntity) programSet).getId();
            _externalOrgUnit = ((IEntity) externalOrgUnit).getId();
        }

        public Long getProgramSet()
        {
            return _programSet;
        }

        public void setProgramSet(Long programSet)
        {
            _programSet = programSet;
        }

        public Long getExternalOrgUnit()
        {
            return _externalOrgUnit;
        }

        public void setExternalOrgUnit(Long externalOrgUnit)
        {
            _externalOrgUnit = externalOrgUnit;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof CustomerTargetContractTrainingGen.NaturalId) ) return false;

            CustomerTargetContractTrainingGen.NaturalId that = (NaturalId) o;

            if( !equals(getProgramSet(), that.getProgramSet()) ) return false;
            if( !equals(getExternalOrgUnit(), that.getExternalOrgUnit()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getProgramSet());
            result = hashCode(result, getExternalOrgUnit());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getProgramSet());
            sb.append("/");
            sb.append(getExternalOrgUnit());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends CustomerTargetContractTrainingGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) CustomerTargetContractTraining.class;
        }

        public T newInstance()
        {
            return (T) new CustomerTargetContractTraining();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "programSet":
                    return obj.getProgramSet();
                case "externalOrgUnit":
                    return obj.getExternalOrgUnit();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "programSet":
                    obj.setProgramSet((EnrProgramSetBase) value);
                    return;
                case "externalOrgUnit":
                    obj.setExternalOrgUnit((ExternalOrgUnit) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "programSet":
                        return true;
                case "externalOrgUnit":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "programSet":
                    return true;
                case "externalOrgUnit":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "programSet":
                    return EnrProgramSetBase.class;
                case "externalOrgUnit":
                    return ExternalOrgUnit.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<CustomerTargetContractTraining> _dslPath = new Path<CustomerTargetContractTraining>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "CustomerTargetContractTraining");
    }
            

    /**
     * @return Набор ОП. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.CustomerTargetContractTraining#getProgramSet()
     */
    public static EnrProgramSetBase.Path<EnrProgramSetBase> programSet()
    {
        return _dslPath.programSet();
    }

    /**
     * @return Внешняя организация. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.CustomerTargetContractTraining#getExternalOrgUnit()
     */
    public static ExternalOrgUnit.Path<ExternalOrgUnit> externalOrgUnit()
    {
        return _dslPath.externalOrgUnit();
    }

    public static class Path<E extends CustomerTargetContractTraining> extends EntityPath<E>
    {
        private EnrProgramSetBase.Path<EnrProgramSetBase> _programSet;
        private ExternalOrgUnit.Path<ExternalOrgUnit> _externalOrgUnit;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Набор ОП. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.CustomerTargetContractTraining#getProgramSet()
     */
        public EnrProgramSetBase.Path<EnrProgramSetBase> programSet()
        {
            if(_programSet == null )
                _programSet = new EnrProgramSetBase.Path<EnrProgramSetBase>(L_PROGRAM_SET, this);
            return _programSet;
        }

    /**
     * @return Внешняя организация. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.CustomerTargetContractTraining#getExternalOrgUnit()
     */
        public ExternalOrgUnit.Path<ExternalOrgUnit> externalOrgUnit()
        {
            if(_externalOrgUnit == null )
                _externalOrgUnit = new ExternalOrgUnit.Path<ExternalOrgUnit>(L_EXTERNAL_ORG_UNIT, this);
            return _externalOrgUnit;
        }

        public Class getEntityClass()
        {
            return CustomerTargetContractTraining.class;
        }

        public String getEntityName()
        {
            return "customerTargetContractTraining";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
