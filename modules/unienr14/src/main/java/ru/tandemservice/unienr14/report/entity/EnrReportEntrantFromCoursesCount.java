package ru.tandemservice.unienr14.report.entity;

import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantFromCoursesCountAdd.EnrReportEntrantFromCoursesCountAdd;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrReport;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrStorableReportDesc;
import ru.tandemservice.unienr14.report.entity.gen.EnrReportEntrantFromCoursesCountGen;

import java.util.Arrays;
import java.util.List;

/**
 * Информация о количестве абитуриентов, обучавшихся на подготовительных курсах
 */
public class EnrReportEntrantFromCoursesCount extends EnrReportEntrantFromCoursesCountGen implements IEnrReport
{
    public static final String REPORT_KEY = "enr14ReportEntrantFromCoursesCount";

    @SuppressWarnings("unchecked")
    private static List<String> properties = Arrays.asList(P_REQUEST_TYPE,
        P_PROGRAM_FORM,
        P_ENR_ORG_UNIT,
        P_FORMATIVE_ORG_UNIT,
        P_PROGRAM_SUBJECT,
        P_EDU_PROGRAM,
        P_PROGRAM_SET,
        P_PARALLEL);

    public static IEnrStorableReportDesc getDescription()
    {
        return new IEnrStorableReportDesc() {
            @Override public String getReportKey() { return REPORT_KEY; }
            @Override public Class<? extends IEnrReport> getReportClass() { return EnrReportEntrantFromCoursesCount.class; }
            @Override public List<String> getPropertyList() { return properties; }

            @Override public Class<? extends BusinessComponentManager> getAddFormComponent() { return EnrReportEntrantFromCoursesCountAdd.class; }
            @Override public String getPubTitle() { return "Отчет «Информация о количестве абитуриентов, обучавшихся на подготовительных курсах»"; }
            @Override public String getListTitle() { return "Список отчетов «Информация о количестве абитуриентов, обучавшихся на подготовительных курсах»"; }
        };
    }

    @Override public IEnrStorableReportDesc getDesc() {
        return getDescription();
    }

    @Override public String getPeriodTitle() {
        return "с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateFrom()) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateTo());
    }
}