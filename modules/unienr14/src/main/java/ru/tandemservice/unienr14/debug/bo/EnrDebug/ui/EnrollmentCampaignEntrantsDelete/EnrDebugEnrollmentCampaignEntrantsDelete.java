/**
 *$Id$
 */
package ru.tandemservice.unienr14.debug.bo.EnrDebug.ui.EnrollmentCampaignEntrantsDelete;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.unienr14.debug.bo.EnrDebug.EnrDebugManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;

/**
 * @author Alexander Zhebko
 * @since 17.03.2014
 */
@Configuration
public class EnrDebugEnrollmentCampaignEntrantsDelete extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
                .create();
    }
}