/* $Id:$ */
package ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.util;

import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;

import java.util.List;

/**
 * @author oleyba
 * @since 8/1/14
 */
public interface IEnrPAChoiceResult
{
    EnrProgramSetItem getChosen();
    List<EnrProgramSetItem> getAvailableChoices();
}
