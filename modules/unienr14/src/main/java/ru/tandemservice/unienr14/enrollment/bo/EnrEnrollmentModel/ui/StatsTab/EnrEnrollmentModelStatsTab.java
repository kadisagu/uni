/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.ui.StatsTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.logic.EnrModelCompetitionWrapper;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.ui.CompetitionPub.EnrEnrollmentModelCompetitionPub;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.logic.EnrModelCompetitionDSHandler;

/**
 * @author oleyba
 * @since 3/27/15
 */
@Configuration
public class EnrEnrollmentModelStatsTab extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addAddon(uiAddon(CommonFilterAddon.class.getSimpleName(), EnrCompetitionFilterAddon.class))
            .addDataSource(searchListDS("competitionDS", competitionDSColumns(), competitionDSHandler()))
            .create();
    }

    @Bean
    public ColumnListExtPoint competitionDSColumns()
    {
        return columnListExtPointBuilder("competitionDS")
            .addColumn(publisherColumn("title", EnrCompetition.title())
                .publisherLinkResolver(new IPublisherLinkResolver()
                {
                    @Override
                    public Object getParameters(IEntity entity)
                    {
                        return ((EnrModelCompetitionWrapper) entity).getPublisherParameters();
                    }

                    @Override
                    public String getComponentName(IEntity entity)
                    {
                        return EnrEnrollmentModelCompetitionPub.class.getSimpleName();
                    }
                }))
            .addColumn(textColumn("plan", EnrModelCompetitionDSHandler.VIEW_PROP_PLAN))
            .addColumn(textColumn("enrolled", EnrModelCompetitionDSHandler.VIEW_PROP_ENROLLED))
            .addColumn(textColumn("avgStateExamMark", EnrModelCompetitionDSHandler.VIEW_PROP_AVG_STATE_EXAM_MARK))
            .addColumn(textColumn("passRating", EnrModelCompetitionDSHandler.VIEW_PROP_PASS_RATING))
//            .addColumn(actionColumn(EDIT_COLUMN_NAME, new Icon(EDIT_COLUMN_NAME), "onClickPlanEdit")
//                .permissionKey("enr14EnrollmentStepPlanEdit")
//            )
            .create();
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> competitionDSHandler()
    {
        return new EnrModelCompetitionDSHandler(getName());
    }
}