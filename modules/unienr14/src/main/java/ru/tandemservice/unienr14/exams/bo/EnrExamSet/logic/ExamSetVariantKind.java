package ru.tandemservice.unienr14.exams.bo.EnrExamSet.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrMethodDivCompetitionsCodes;
import ru.tandemservice.unienr14.competition.entity.*;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.EnrExamSetManager;
import ru.tandemservice.unienr14.exams.entity.EnrExamSet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author oleyba
 * @since 1/27/15
 */
public enum ExamSetVariantKind
{
    BS_BASE, // Набор ВИ (СОО)
    BS_BASE_SEC, // Набор ВИ (СОО и СПО)
    BS_BASE_HIGH, // Набор ВИ (СОО и ВО)
    BS_SEC, // Набор ВИ (СПО)
    BS_HIGH, // Набор ВИ (ВО)
    BS_SEC_HIGH, // Набор ВИ (ПО)
    MAS_B, // Набор ВИ на бюджет
    MAS_C, // Набор ВИ на договор
    SINGLE // Набор ВИ
    ;

    public String getTitle() {
        switch (this) {
            case BS_BASE: return "Набор ВИ (СОО)";
            case BS_BASE_SEC: return "Набор ВИ (СОО и СПО)";
            case BS_BASE_HIGH: return "Набор ВИ (СОО и ВО)";
            case BS_SEC: return "Набор ВИ (СПО)";
            case BS_HIGH: return "Набор ВИ (ВО)";
            case BS_SEC_HIGH: return "Набор ВИ (ПО)";
            case MAS_B: return "Набор ВИ на бюджет";
            case MAS_C: return "Набор ВИ на договор";
            case SINGLE: return "Набор ВИ";
        }
        throw new IllegalStateException();
    }

    public String getEditButtonTitle() {
        return "Настроить " + StringUtils.uncapitalize(getTitle());
    }

    private static ExamSetVariantWrapper wrap(EnrExamSetVariant variant, ExamSetVariantKind kind, EnrProgramSetBase ps) {

        if (variant != null) {
            IEnrExamSetDao.IExamSetSettings examSetSettings = EnrExamSetManager.instance().dao().getExamSetContent(Collections.singleton(variant.getId())).get(variant.getId());
            return new ExamSetVariantWrapper(variant.getId(), examSetSettings.getTitle(), kind, ps);
        }
        return new ExamSetVariantWrapper((long) kind.ordinal(), "", kind, ps);
    }

    public static List<ExamSetVariantWrapper> getExamSetVariants(EnrProgramSetBase ps) {
        List<ExamSetVariantWrapper> wrappers = new ArrayList<>();

        if (ps instanceof EnrProgramSetBS) {
            if (ps.isLessOrEqualThan2014()) {
                throw new IllegalArgumentException();
            }

            EnrProgramSetBS bs = (EnrProgramSetBS) ps;

            switch (ps.getMethodDivCompetitions().getCode()) {
                case EnrMethodDivCompetitionsCodes.BS_NO_DIV:
                    wrappers.add(wrap(bs.getExamSetVariantBase(), SINGLE, ps)); break;
                case EnrMethodDivCompetitionsCodes.BS_SOO_SPO_VO:
                    wrappers.add(wrap(bs.getExamSetVariantBase(), BS_BASE, ps));
                    wrappers.add(wrap(bs.getExamSetVariantSec(), BS_SEC, ps));
                    wrappers.add(wrap(bs.getExamSetVariantHigh(), BS_HIGH, ps));
                    break;
                case EnrMethodDivCompetitionsCodes.BS_SOO_PO:
                    wrappers.add(wrap(bs.getExamSetVariantBase(), BS_BASE, ps));
                    wrappers.add(wrap(bs.getExamSetVariantSec(), BS_SEC_HIGH, ps));
                    break;
                case EnrMethodDivCompetitionsCodes.BS_SOO_AND_SPO_APART_VO:
                    wrappers.add(wrap(bs.getExamSetVariantBase(), BS_BASE_SEC, ps));
                    wrappers.add(wrap(bs.getExamSetVariantHigh(), BS_HIGH, ps));
                    break;
                case EnrMethodDivCompetitionsCodes.BS_SOO_AND_VO_APART_SPO:
                    wrappers.add(wrap(bs.getExamSetVariantBase(), BS_BASE_HIGH, ps));
                    wrappers.add(wrap(bs.getExamSetVariantSec(), BS_SEC, ps));
                    break;
            }
        } else if (ps instanceof EnrProgramSetSecondary) {
            wrappers.add(wrap(((EnrProgramSetSecondary)ps).getExamSetVariant(), SINGLE, ps));
        }  else if (ps instanceof EnrProgramSetMaster) {
            if (ps.isGreaterThan2014()) {
                wrappers.add(wrap(((EnrProgramSetMaster)ps).getExamSetVariant(), SINGLE, ps));
            } else {
                wrappers.add(wrap(((EnrProgramSetMaster)ps).getExamSetVariantMin(), MAS_B, ps));
                wrappers.add(wrap(((EnrProgramSetMaster)ps).getExamSetVariantCon(), MAS_C, ps));
            }
        }  else if (ps instanceof EnrProgramSetHigher) {
            wrappers.add(wrap(((EnrProgramSetHigher)ps).getExamSetVariant(), SINGLE, ps));
        }

        return wrappers;
    }

    public EnrExamSetVariant getVariant(EnrProgramSetBase ps)
    {
        if (ps instanceof EnrProgramSetBS) {
            if (ps.isLessOrEqualThan2014()) {
                throw new IllegalArgumentException();
            }
            if (!ps.isValidExamSetVariants()) {
                return null; // если настройки установлены неправильно, всегда будем создавать новую
            }
            switch (this) {
                case SINGLE: case BS_BASE: case BS_BASE_SEC: case BS_BASE_HIGH: return ((EnrProgramSetBS) ps).getExamSetVariantBase();
                case BS_SEC:case BS_SEC_HIGH: return ((EnrProgramSetBS) ps).getExamSetVariantSec();
                case BS_HIGH: return ((EnrProgramSetBS) ps).getExamSetVariantHigh();
            }
        } else if (ps instanceof EnrProgramSetSecondary) {
            return ((EnrProgramSetSecondary)ps).getExamSetVariant();
        }  else if (ps instanceof EnrProgramSetMaster) {
            switch (this) {
                case MAS_B: return ((EnrProgramSetMaster) ps).getExamSetVariantMin();
                case MAS_C: return ((EnrProgramSetMaster) ps).getExamSetVariantCon();
                case SINGLE: return ((EnrProgramSetMaster) ps).getExamSetVariant();
            }
        }  else if (ps instanceof EnrProgramSetHigher) {
            return ((EnrProgramSetHigher)ps).getExamSetVariant();
        }

        throw new IllegalArgumentException();
    }

    public void setVariant(EnrProgramSetBase ps, EnrExamSetVariant examSetVariant)
    {
        if (ps instanceof EnrProgramSetBS) {
            if (ps.isLessOrEqualThan2014()) {
                throw new IllegalArgumentException();
            }

            switch (this) {
                case SINGLE:
                    ((EnrProgramSetBS) ps).setExamSetVariantBase(examSetVariant);
                    ((EnrProgramSetBS) ps).setExamSetVariantSec(examSetVariant);
                    ((EnrProgramSetBS) ps).setExamSetVariantHigh(examSetVariant);
                    return;
                case BS_BASE:
                    ((EnrProgramSetBS) ps).setExamSetVariantBase(examSetVariant);
                    return;
                case BS_BASE_SEC:
                    ((EnrProgramSetBS) ps).setExamSetVariantBase(examSetVariant);
                    ((EnrProgramSetBS) ps).setExamSetVariantSec(examSetVariant);
                    return;
                case BS_BASE_HIGH:
                    ((EnrProgramSetBS) ps).setExamSetVariantBase(examSetVariant);
                    ((EnrProgramSetBS) ps).setExamSetVariantHigh(examSetVariant);
                    return;
                case BS_SEC:
                    ((EnrProgramSetBS) ps).setExamSetVariantSec(examSetVariant);
                    return;
                case BS_SEC_HIGH:
                    ((EnrProgramSetBS) ps).setExamSetVariantSec(examSetVariant);
                    ((EnrProgramSetBS) ps).setExamSetVariantHigh(examSetVariant);
                    return;
                case BS_HIGH:
                    ((EnrProgramSetBS) ps).setExamSetVariantHigh(examSetVariant);
                    return;
            }
        } else if (ps instanceof EnrProgramSetSecondary) {
            ((EnrProgramSetSecondary)ps).setExamSetVariant(examSetVariant);
            return;
        }  else if (ps instanceof EnrProgramSetMaster) {
            switch (this) {
                case MAS_B:
                    ((EnrProgramSetMaster) ps).setExamSetVariantMin(examSetVariant);
                    return;
                case MAS_C:
                    ((EnrProgramSetMaster) ps).setExamSetVariantCon(examSetVariant);
                    return;
                case SINGLE:
                    ((EnrProgramSetMaster) ps).setExamSetVariant(examSetVariant);
                    return;
            }
        }  else if (ps instanceof EnrProgramSetHigher) {
            ((EnrProgramSetHigher)ps).setExamSetVariant(examSetVariant);
            return;
        }

        throw new IllegalArgumentException();
    }

    public void validateSelection(EnrProgramSetBase ps, EnrExamSet examSet)
    {
        if (!(ps instanceof EnrProgramSetBS) || !ps.isGreaterThan2014()) {
            return;
        }

        EnrProgramSetBS bs = (EnrProgramSetBS) ps;

        boolean error = false;

        EnrExamSet secES = bs.getExamSetVariantSec() == null ? null : bs.getExamSetVariantSec().getExamSet();
        EnrExamSet highES = bs.getExamSetVariantHigh() == null ? null : bs.getExamSetVariantHigh().getExamSet();
        EnrExamSet baseES = bs.getExamSetVariantBase() == null ? null : bs.getExamSetVariantBase().getExamSet();


        switch (this) {
            case SINGLE:
                return;
            case BS_BASE:
                error = examSet.equals(secES) || examSet.equals(highES);
                break;
            case BS_BASE_SEC:
                error = examSet.equals(highES);
                break;
            case BS_BASE_HIGH:
                error = examSet.equals(secES);
                break;
            case BS_SEC:
                error = examSet.equals(baseES) || examSet.equals(highES);
                break;
            case BS_SEC_HIGH:
                error = examSet.equals(baseES);
                break;
            case BS_HIGH:
                error = examSet.equals(baseES) || examSet.equals(secES);
                break;
        }

        if (error) {
            throw new ApplicationException("Выбранные настройки наборов ВИ не соответствуют способу деления конкурсов.");
        }
    }

    public static class ExamSetVariantWrapper extends IdentifiableWrapper {
        private ExamSetVariantKind kind;
        private EnrProgramSetBase owner;

        public ExamSetVariantWrapper(Long id, String title, ExamSetVariantKind kind, EnrProgramSetBase owner) throws ClassCastException
        {
            super(id, title);
            this.kind = kind;
            this.owner = owner;
        }

        public ExamSetVariantKind getKind() { return kind; }
        public EnrProgramSetBase getOwner() { return owner; }
    }
}
