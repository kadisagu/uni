/**
 *$Id: ReportPersonAddExt.java 33259 2014-03-27 11:59:44Z azhebko $
 */
package ru.tandemservice.unienr14.base.ext.ReportPerson.ui.Add;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import org.tandemframework.shared.person.base.bo.ReportPerson.ui.Add.ReportPersonAdd;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.EnrReportPersonAdd;

/**
 * @author Alexander Shaburov
 * @since 19.06.13
 */
@Configuration
public class ReportPersonAddExt extends BusinessComponentExtensionManager
{
    // first level tabs
    public static final String ENTRANT_TAB = "enr14EntrantTab";

    @Autowired
    private ReportPersonAdd _reportPersonAdd;

    @Bean
    public TabPanelExtension tabPanelExtension()
    {
        return tabPanelExtensionBuilder(_reportPersonAdd.tabPanelExtPoint())
                .addTab(componentTab(ENTRANT_TAB, EnrReportPersonAdd.class).parameters("mvel:['summaryReport' : presenter.summaryReport]").visible("mvel:presenter.isTabVisible('" + ENTRANT_TAB + "')"))
                .create();
    }
}