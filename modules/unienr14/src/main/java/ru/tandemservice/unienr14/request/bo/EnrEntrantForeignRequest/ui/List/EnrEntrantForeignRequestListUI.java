/* $Id$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantForeignRequest.ui.List;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilter;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilterType;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterFormConfig;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.tapsupport.component.selection.*;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.order.bo.EnrEnrollmentMinisteryParagraph.EnrEnrollmentMinisteryParagraphManager;
import ru.tandemservice.unienr14.request.bo.EnrEntrantForeignRequest.ui.AddEdit.EnrEntrantForeignRequestAddEdit;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 7/15/14
 */
public class EnrEntrantForeignRequestListUI extends UIPresenter
{
    private EnrEnrollmentCampaign _enrollmentCampaign;
    public static final Long NO_CITIZENSHIP = 0L;

    private ISelectModel _citizenshipModel;
    private DataWrapper _citizenshipFilter;


    @Override
    public void onComponentRefresh()
    {
        setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
        setCitizenshipModel(initCitizenshipModel());

        CommonFilterAddon util = (CommonFilterAddon) getConfig().getAddon(CommonFilterAddon.class.getSimpleName());
        util
                .configDoubleWidthFilters(false)
                .configUseEnableCheckbox(false)
                .configSettings(getSettingsKey());

        util.clearFilterItems();
        util
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_FORM, CommonFilterFormConfig.DEFAULT_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.ENR_ORG_UNIT, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.FORMATIVE_ORG_UNIT, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG);


        configWhereFilters();
    }

    private void configWhereFilters()
    {
        CommonFilterAddon util = (CommonFilterAddon) getConfig().getAddon(CommonFilterAddon.class.getSimpleName());
        if (util != null)
        {
            util.clearWhereFilter();
            util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EnrCompetition.programSetOrgUnit().orgUnit().enrollmentCampaign(), getEnrollmentCampaign()));
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        getSettings().set("citizenshipFilter", getCitizenshipFilter() == null ? null : getCitizenshipFilter().getId());
        dataSource.put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, getEnrollmentCampaign());
        dataSource.put("settings", getSettings());
        dataSource.put(EnrEntrantForeignRequestList.PARAM_ENR_COMPETITION_UTIL, getConfig().getAddon(CommonFilterAddon.class.getSimpleName()));
    }

    private ISelectModel initCitizenshipModel()
    {
        return new CommonSingleSelectModel()
        {
            @Override
            public Object getPrimaryKey(Object value)
            {
                return value instanceof Long ? value : super.getPrimaryKey(value);
            }

            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(AddressCountry.class, "b").column(property("b"));

                if (o != null)
                {
                    if (NO_CITIZENSHIP.equals(o))
                    {
                        final List<DataWrapper> list = new ArrayList<>();
                        final DataWrapper element = new DataWrapper();
                        element.setId(NO_CITIZENSHIP);
                        element.setTitle("Не гражданин РФ");
                        list.add(0, element);
                        return new SimpleListResultBuilder<>(list);
                    }
                    else
                    {
                        builder.where(eq(property(AddressCountry.id().fromAlias("b")), commonValue(o, PropertyType.LONG)));
                        return new SimpleListResultBuilder<>(Collections.singleton(new DataWrapper(IUniBaseDao.instance.get().getList(builder).iterator().next())));
                    }
                }

                if (StringUtils.isNotEmpty(filter))
                    builder.where(likeUpper(property(AddressCountry.title().fromAlias("b")), value(CoreStringUtils.escapeLike(filter))));

                builder.order(property(AddressCountry.title().fromAlias("b")));

                final List<DataWrapper> patchedList = new ArrayList<>();
                final boolean addNoCitizenship = StringUtils.containsIgnoreCase("Не гражданин РФ", filter);
                if (addNoCitizenship)
                {
                    final DataWrapper element = new DataWrapper();
                    element.setId(NO_CITIZENSHIP);
                    element.setTitle("Не гражданин РФ");
                    patchedList.add(0, element);
                }
                final DQLListResultBuilder<IEntity> resultBuilder = new DQLListResultBuilder<>(builder, addNoCitizenship ? 49 : 50);
                for (IEntity entity : resultBuilder.findOptions().getObjects())
                    patchedList.add(new DataWrapper(entity));

                return new AbstractListResultBuilder()
                {
                    final ListResult _result = new ListResult<>(patchedList, IUniBaseDao.instance.get().getCount(builder) + (addNoCitizenship ? 1 : 0));

                    @Override
                    public ListResult findOptions()
                    {
                        return _result;
                    }

                    @Override
                    public List getValues()
                    {
                        if( _result.getObjects().size()!=_result.getMaxCount() )
                        {
                            // похоже, запрос был с ограничением количества записей и в результате часть значений была потеряна
                            // данный запрос не должен содержать ограничение записей, иначе в Мультиселект нельзя будет добавить больше значений
                            throw new RuntimeException("Some values are lost. Objects:" + _result.getObjects().size() + " maxCount:" + _result.getMaxCount());
                        }
                        return _result.getObjects();
                    }
                };
            }
        };
    }

    public void onEnrCampaignRefresh()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
    }

    public void onClickAdd()
    {
        _uiActivation.asRegion(EnrEntrantForeignRequestAddEdit.class)
                .parameter(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, getEnrollmentCampaign().getId())
                .activate();
    }

    public void onClickCreateOrders()
    {
        EnrEnrollmentMinisteryParagraphManager.instance().dao().doCreateOrders(getEnrollmentCampaign());
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asRegion(EnrEntrantForeignRequestAddEdit.class)
                .parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong())
                .activate();
    }

    public void onDeleteEntityFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public boolean isNothingSelected(){ return null == getEnrollmentCampaign(); }

    public void onClickSearch()
    {
        CommonFilterAddon util = (CommonFilterAddon) getConfig().getAddon(CommonFilterAddon.class.getSimpleName());
        if (util != null)
            util.saveSettings();

        saveSettings();
    }

    public void onClickClear()
    {
        CommonFilterAddon util = (CommonFilterAddon) getConfig().getAddon(CommonFilterAddon.class.getSimpleName());
        if (util != null)
            util.clearSettings();

        setCitizenshipFilter(null);
        clearSettings();
        configWhereFilters();
        onClickSearch();
    }


    public EnrEnrollmentCampaign getEnrollmentCampaign(){ return _enrollmentCampaign; }
    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign){ _enrollmentCampaign = enrollmentCampaign; }

    public ISelectModel getCitizenshipModel()
    {
        return _citizenshipModel;
    }

    public void setCitizenshipModel(ISelectModel citizenshipModel)
    {
        _citizenshipModel = citizenshipModel;
    }

    public DataWrapper getCitizenshipFilter()
    {
        return _citizenshipFilter;
    }

    public void setCitizenshipFilter(DataWrapper citizenshipFilter)
    {
        _citizenshipFilter = citizenshipFilter;
    }
}
