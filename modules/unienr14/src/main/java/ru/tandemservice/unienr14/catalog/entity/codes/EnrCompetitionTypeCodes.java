package ru.tandemservice.unienr14.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Вид приема"
 * Имя сущности : enrCompetitionType
 * Файл data.xml : unienr.catalog.data.xml
 */
public interface EnrCompetitionTypeCodes
{
    /** Константа кода (code) элемента : Без ВИ в рамках КЦП (title) */
    String NO_EXAM_MINISTERIAL = "01";
    /** Константа кода (code) элемента : В рамках квоты лиц, имеющих особые права (title) */
    String EXCLUSIVE = "02";
    /** Константа кода (code) элемента : Целевой прием (title) */
    String TARGET_ADMISSION = "03";
    /** Константа кода (code) элемента : Общий конкурс (title) */
    String MINISTERIAL = "04";
    /** Константа кода (code) элемента : Без ВИ по договору (title) */
    String NO_EXAM_CONTRACT = "05";
    /** Константа кода (code) элемента : По договору (title) */
    String CONTRACT = "06";

    Set<String> CODES = ImmutableSet.of(NO_EXAM_MINISTERIAL, EXCLUSIVE, TARGET_ADMISSION, MINISTERIAL, NO_EXAM_CONTRACT, CONTRACT);
}
