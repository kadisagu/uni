/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubDocumentTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.Pub.EnrEntrantPub;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubAllDocumentTab.EnrEntrantPubAllDocumentTab;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubDocumentRoleTab.EnrEntrantPubDocumentRoleTab;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubEduDocumentTab.EnrEntrantPubEduDocumentTab;
import ru.tandemservice.unienr14.entrant.bo.EnrOlympiadDiploma.ui.List.EnrOlympiadDiplomaList;

/**
 * @author oleyba
 * @since 4/22/13
 */
@Configuration
public class EnrEntrantPubDocumentTab extends BusinessComponentManager
{
    public static final String TAB_PANEL = "entrantDocumentTabPanel";
    public static final String TAB_PANEL_REGION_NAME = EnrEntrantPub.TAB_PANEL_SUB_REGION_NAME;

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
        .create();
    }

    @Bean
    public TabPanelExtPoint entrantDocumentTabPanelExtPoint()
    {
        return tabPanelExtPointBuilder(TAB_PANEL)
//            .addTab(componentTab("allDocumentTab", EnrEntrantPubAllDocumentTab.class).permissionKey("enr14EntrantPubAllDocumentTabView"))
            .addTab(componentTab("documentRoleTab", EnrEntrantPubDocumentRoleTab.class).permissionKey("viewTabPersonRoleDoc_enrEntrant"))
            .addTab(componentTab("eduDocumentTab", EnrEntrantPubEduDocumentTab.class).permissionKey("enr14EntrantPubEduDocumentTabView"))
//            .addTab(componentTab("olympiadDiplomaTab", EnrOlympiadDiplomaList.class).permissionKey("enr14EntrantPubOlympiadDiplomaTabView"))
            .create();
    }
}
