/* $Id:$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.dsl.MetaDSLPath;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.INumberQueueDAO;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantState;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItem;
import ru.tandemservice.unienr14.enrollment.entity.EnrModelStepItem;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.logic.EnrEntrantDao;
import ru.tandemservice.unienr14.entrant.bo.EnrSubmittedEduDocument.EnrSubmittedEduDocumentManager;
import ru.tandemservice.unienr14.entrant.daemon.EnrEntrantDaemonBean;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantOriginalDocumentStatus;
import ru.tandemservice.unienr14.entrant.entity.gen.EnrEntrantOriginalDocumentStatusGen;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.EnrEntrantRequestManager;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.util.EnrRequestedCompetitionWrapper;
import ru.tandemservice.unienr14.request.bo.EnrRequestedCompetition.EnrRequestedCompetitionManager;
import ru.tandemservice.unienr14.request.bo.EnrRequestedCompetition.logic.IPropertyUpdateRule;
import ru.tandemservice.unienr14.request.entity.*;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings;
import ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementType;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantStateCodes.*;

/**
 * @author oleyba
 * @since 4/24/13
 */
public class EnrEntrantRequestDao extends UniBaseDao implements IEnrEntrantRequestDao
{
    @Override
    public void updateDocumentsReturn(Long entrantRequestId)
    {
        EnrEntrantRequest entrantRequest = IUniBaseDao.instance.get().getNotNull(EnrEntrantRequest.class, entrantRequestId);

        if(!entrantRequest.isTakeAwayDocument())
        {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrRequestedCompetition.class, "rc");
            builder.column(property("rc", EnrRequestedCompetition.state()));
            builder.distinct();
            builder.where(eq(property("rc", EnrRequestedCompetition.request().id()), value(entrantRequest.getId())));
            builder.where(in(property("rc", EnrRequestedCompetition.state().code()), Lists.newArrayList(ENROLLED, IN_ORDER, SELECT_SHOULD_BE_ENROLLED)));
            builder.top(1);
            builder.order(property("rc", EnrRequestedCompetition.state().priority()));

            EnrEntrantState state = builder.createStatement(getSession()).uniqueResult();

            if (state != null)
            {
                throw new ApplicationException("Нельзя отозвать заявление абитуриента в состоянии «" + state.getTitle() + "».");
            }
        }

        EnrEntrantDao.lockEntrant(getSession(), entrantRequest.getEntrant());

        entrantRequest.setTakeAwayDocument(!entrantRequest.isTakeAwayDocument());
        update(entrantRequest);
        EnrEntrantManager.instance().dao().doNormalizePriorityPerEntrantAndRequestType(entrantRequest.getEntrant(), entrantRequest.getType());
        getSession().flush();

        Integer maxPriority = new DQLSelectBuilder()
        .fromEntity(EnrRequestedCompetition.class, "r").column(DQLFunctions.max(property(EnrRequestedCompetition.priority().fromAlias("r"))))
        .where(eq(property("r", EnrRequestedCompetition.request().entrant()), value(entrantRequest.getEntrant())))
        .where(eq(property("r", EnrRequestedCompetition.request().type()), value(entrantRequest.getType())))
        .where(ne(property("r", EnrRequestedCompetition.request().id()), value(entrantRequestId)))
        .createStatement(getSession()).uniqueResult();
        if (null == maxPriority) maxPriority = 0;

        for (EnrRequestedCompetition reqComp : getList(EnrRequestedCompetition.class, EnrRequestedCompetition.request(), entrantRequest, EnrRequestedCompetition.priority().s())) {
            reqComp.setPriority(++maxPriority);
            saveRequestedCompetition(reqComp);
        }

        EnrEntrantManager.instance().dao().doNormalizePriorityPerEntrantAndRequestType(entrantRequest.getEntrant(), entrantRequest.getType());
    }

    protected void saveRequestedCompetition(EnrRequestedCompetition reqComp) {
        reqComp.refreshUniqueKey();
        save(reqComp);
    }

    @Override
    public EnrEntrantRequest doCreateEntrantRequest(EnrEntrant entrant, EnrEntrantRequest request, List<IEnrEntrantBenefitProofDocument> requestBenefitProofDocs, List<EnrRequestedCompetitionWrapper> selectedDirections)
    {
        EnrEntrantDao.lockEntrant(getSession(), entrant);
        EnrEntrantDao.lockEnrollmentCampaign(getSession(), entrant);

        EnrEntrantManager.instance().dao().doNormalizePriorityPerEntrant(entrant);

        EnrEntrantRequest newRequest = new EnrEntrantRequest();
        if (request != null) {
            newRequest.update(request);
        }

        newRequest.setEntrant(entrant);

        if (StringUtils.isEmpty(newRequest.getRegNumber())&& !entrant.getEnrollmentCampaign().getSettings().isNumberOnRequestManual()) {
            newRequest.setRegNumber(getNextEntrantRequestNumber(newRequest));
        }

        checkRequestNumberUnique(newRequest);

        save(newRequest);

        // выставляем отрицательные приоритеты всем существующим выбранным конкурсам в соответствии с их порядком в списке
        for (EnrRequestedCompetitionWrapper wrapper : selectedDirections) {
            if (null != wrapper.getExistingCompetitionId()) {
                EnrRequestedCompetition existingReqComp = get(EnrRequestedCompetition.class, wrapper.getExistingCompetitionId());
                existingReqComp.setPriority(-1 - selectedDirections.indexOf(wrapper));
                update(existingReqComp);
            }
        }
        getSession().flush();

        // сохраняем новые выбранные конкурсы
        for (EnrRequestedCompetitionWrapper wrapper : selectedDirections) {
            if (null != wrapper.getExistingCompetitionId())
                continue;

            EnrRequestedCompetition reqComp;
            if (wrapper.getCompetition().isExclusive()) {
                reqComp = new EnrRequestedCompetitionExclusive(newRequest, wrapper.getCompetition());
            } else if (wrapper.getCompetition().isTargetAdmission()) {
                reqComp = new EnrRequestedCompetitionTA(newRequest, wrapper.getCompetition());
            } else if (wrapper.getCompetition().isNoExams()) {
                reqComp = new EnrRequestedCompetitionNoExams(newRequest, wrapper.getCompetition());
            } else {
                reqComp = new EnrRequestedCompetition(newRequest, wrapper.getCompetition());
            }
            wrapper.updateFields(reqComp);

            if(!reqComp.getCompetition().getType().getCompensationType().isBudget() && entrant.getEnrollmentCampaign().getSettings().isAcceptedContractDefault())
            {
                reqComp.setAccepted(true);
                reqComp.setAcceptedDate(new Date());
            }
            else
            {
                reqComp.setAccepted(false);
                reqComp.setAcceptedDate(null);
            }

            reqComp.setPriority(selectedDirections.indexOf(wrapper) + 1);

            saveRequestedCompetition(reqComp);

            if (reqComp instanceof IEnrEntrantBenefitStatement)
            {
                EnrEntrantManager.instance().dao().doSaveBenefitData((IEnrEntrantBenefitStatement) reqComp, ((IEnrEntrantBenefitStatement) reqComp).getBenefitCategory(), wrapper.getBenefitProofDocs());
            }
        }
        getSession().flush();

        // выставляем приоритеты существующим конкурсам в соответствии с их порядком в списке
        for (EnrRequestedCompetitionWrapper wrapper : selectedDirections) {
            if (null != wrapper.getExistingCompetitionId()) {
                EnrRequestedCompetition existingReqComp = get(EnrRequestedCompetition.class, wrapper.getExistingCompetitionId());
                existingReqComp.setPriority(- existingReqComp.getPriority());
                update(existingReqComp);
            }
        }

        EnrEntrantManager.instance().dao().doSaveBenefitData(newRequest, newRequest.getBenefitCategory(), requestBenefitProofDocs);

        return newRequest;
    }

    @Override
    public String getNextEntrantRequestNumber(EnrEntrantRequest entrantRequest)
    {
        return INumberQueueDAO.instance.get().getNextNumber(EnrEntrantRequestManager.instance().requestNumberGenerationRule(), entrantRequest);
    }

    @Override
    public void doUpdateEntrantRequest(EnrEntrantRequest entrantRequest, PersonEduDocument oldEduDocument, List<IEnrEntrantBenefitProofDocument> requestBenefitProofDocs)
    {
        EnrEntrant entrant = entrantRequest.getEntrant();
        EnrEntrantDao.lockEntrant(getSession(), entrant);

        checkRequestNumberUnique(entrantRequest);

        if(TwinComboDataSourceHandler.NO_ID.equals(entrant.getEnrollmentCampaign().getSettings().getAccountingRulesOrigEduDoc()) && !Objects.equals(entrantRequest.getEduDocument(), oldEduDocument))
        {
            EnrEntrantOriginalDocumentStatus oldDoc = getByNaturalId(new EnrEntrantOriginalDocumentStatus.NaturalId(entrant, oldEduDocument));
            EnrEntrantOriginalDocumentStatus newDoc = getByNaturalId(new EnrEntrantOriginalDocumentStatus.NaturalId(entrant, entrantRequest.getEduDocument()));
            if(oldDoc != null && !oldDoc.isOriginalDocumentHandedIn() && oldDoc.getRequestedCompetition() != null && entrantRequest.equals(oldDoc.getRequestedCompetition().getRequest()))
            {
                oldDoc.setRequestedCompetition(null);
                update(oldDoc);
            }

            if(newDoc != null && !newDoc.isOriginalDocumentHandedIn() && newDoc.getRequestedCompetition() != null && entrantRequest.equals(newDoc.getRequestedCompetition().getRequest()))
            {
                newDoc.setRequestedCompetition(null);
                update(newDoc);
            }
        }
        saveOrUpdate(entrantRequest);

        EnrEntrantManager.instance().dao().doSaveBenefitData(entrantRequest, entrantRequest.getBenefitCategory(), requestBenefitProofDocs);
    }

    @Override
    public void doAppendDirections(EnrEntrantRequest entrantRequest, List<EnrRequestedCompetitionWrapper> selectedDirections)
    {
        EnrEntrant entrant = entrantRequest.getEntrant();
        EnrEntrantDao.lockEntrant(getSession(), entrant);

        update(entrantRequest);

        EnrEntrantManager.instance().dao().doNormalizePriorityPerEntrantAndRequestType(entrantRequest.getEntrant(), entrantRequest.getType());

        Map<EnrCompetition, EnrRequestedCompetition> existingCurrentRequestCompetitions = new HashMap<>();
        for (EnrRequestedCompetition reqComp : getList(EnrRequestedCompetition.class, EnrRequestedCompetition.request(), entrantRequest)) {
            existingCurrentRequestCompetitions.put(reqComp.getCompetition(), reqComp);
        }

        // если на форме были "пересозданы" уже существующие выбранные конкурсы этого заявления, то возвращаем им их id
        for (EnrRequestedCompetitionWrapper wrapper : selectedDirections) {
            if (null == wrapper.getExistingCompetitionId() && wrapper.isFromCurrentRequest()) {
                EnrRequestedCompetition existingReqComp = existingCurrentRequestCompetitions.get(wrapper.getCompetition());
                if (null != existingReqComp)
                    wrapper.setExistingCompetitionId(existingReqComp.getId());
            }
        }

        // выставляем отрицательные приоритеты всем существующим выбранным конкурсам в соответствии с их порядком в списке
        // заодно удаляем из мапы все вновь выбранные конкурсы данного заявления
        for (EnrRequestedCompetitionWrapper wrapper : selectedDirections) {
            if (null != wrapper.getExistingCompetitionId()) {
                EnrRequestedCompetition existingReqComp = get(EnrRequestedCompetition.class, wrapper.getExistingCompetitionId());
                existingReqComp.setPriority(-1 - selectedDirections.indexOf(wrapper));
                if (wrapper.isAllowEdit()) {
                    wrapper.updateFields(existingReqComp);
                    if (existingReqComp instanceof IEnrEntrantBenefitStatement) {
                        EnrEntrantManager.instance().dao().doSaveBenefitData((IEnrEntrantBenefitStatement) existingReqComp, ((IEnrEntrantBenefitStatement) existingReqComp).getBenefitCategory(), wrapper.getBenefitProofDocs());
                    }
                }
                update(existingReqComp);
            }

            if (wrapper.isFromCurrentRequest()) {
                existingCurrentRequestCompetitions.remove(wrapper.getCompetition());
            }
        }
        getSession().flush();

        // удаляем выбранные конкурсы данного заявления, которые были удалены с формы
        for (EnrRequestedCompetition requestedCompetition : existingCurrentRequestCompetitions.values()) {
            delete(requestedCompetition);
        }
        getSession().flush();

        // сохраняем новые выбранные конкурсы
        for (EnrRequestedCompetitionWrapper wrapper : selectedDirections) {

            if (null != wrapper.getExistingCompetitionId())
                continue;

            EnrRequestedCompetition reqComp;
            if (wrapper.getCompetition().isExclusive()) {
                reqComp = new EnrRequestedCompetitionExclusive(entrantRequest, wrapper.getCompetition());
            } else if (wrapper.getCompetition().isTargetAdmission()) {
                reqComp = new EnrRequestedCompetitionTA(entrantRequest, wrapper.getCompetition());
            } else if (wrapper.getCompetition().isNoExams()) {
                reqComp = new EnrRequestedCompetitionNoExams(entrantRequest, wrapper.getCompetition());
            } else {
                reqComp = new EnrRequestedCompetition(entrantRequest, wrapper.getCompetition());
            }
            wrapper.updateFields(reqComp);
            if(!reqComp.getCompetition().getType().getCompensationType().isBudget() && entrantRequest.getEntrant().getEnrollmentCampaign().getSettings().isAcceptedContractDefault())
            {
                reqComp.setAccepted(true);
                reqComp.setAcceptedDate(new Date());
            }
            else
            {
                reqComp.setAccepted(false);
                reqComp.setAcceptedDate(null);
            }
            reqComp.setPriority(selectedDirections.indexOf(wrapper) + 1);

            saveRequestedCompetition(reqComp);

            if (reqComp instanceof IEnrEntrantBenefitStatement) {
                EnrEntrantManager.instance().dao().doSaveBenefitData((IEnrEntrantBenefitStatement) reqComp, ((IEnrEntrantBenefitStatement) reqComp).getBenefitCategory(), wrapper.getBenefitProofDocs());
            }
        }
        getSession().flush();

        // выставляем приоритеты существующим конкурсам в соответствии с их порядком в списке
        for (EnrRequestedCompetitionWrapper wrapper : selectedDirections) {
            if (null != wrapper.getExistingCompetitionId()) {
                EnrRequestedCompetition existingReqComp = get(EnrRequestedCompetition.class, wrapper.getExistingCompetitionId());
                existingReqComp.setPriority(- existingReqComp.getPriority());
            }
        }
    }

    @Override
    public void doUpdateAttachments(EnrEntrantRequest request, Map<Long, Boolean> attachedMap, boolean eduDocumentOriginalHandedInNow, String currentEditOrgOriginalInTitle, EnrRequestedCompetition requestedCompetition, PersonEduDocument oldEduDocument)
    {
        if(TwinComboDataSourceHandler.NO_ID.equals(request.getEntrant().getEnrollmentCampaign().getSettings().getAccountingRulesOrigEduDoc()) && !Objects.equals(request.getEduDocument(), oldEduDocument))
        {
            EnrEntrantOriginalDocumentStatus oldDoc = getByNaturalId(new EnrEntrantOriginalDocumentStatus.NaturalId(request.getEntrant(), oldEduDocument));
            EnrEntrantOriginalDocumentStatus newDoc = getByNaturalId(new EnrEntrantOriginalDocumentStatus.NaturalId(request.getEntrant(), request.getEduDocument()));
            if(oldDoc != null && !oldDoc.isOriginalDocumentHandedIn() && oldDoc.getRequestedCompetition() != null && request.equals(oldDoc.getRequestedCompetition().getRequest()))
            {
                oldDoc.setRequestedCompetition(null);
                update(oldDoc);
            }

            if(newDoc != null && !newDoc.isOriginalDocumentHandedIn() && newDoc.getRequestedCompetition() != null && request.equals(newDoc.getRequestedCompetition().getRequest()))
            {
                newDoc.setRequestedCompetition(null);
                update(newDoc);
            }
        }

        getSession().merge(request);

        Map<Long, EnrEntrantRequestAttachment> existingMap = new HashMap<>();
        for (EnrEntrantRequestAttachment attachment : IUniBaseDao.instance.get().getList(EnrEntrantRequestAttachment.class, EnrEntrantRequestAttachment.entrantRequest(), request)) {
            existingMap.put(attachment.getDocument().getId(), attachment);
        }

        for (Map.Entry<Long, Boolean> entry : attachedMap.entrySet()) {
            if (entry.getValue() == null) continue;

            EnrEntrantRequestAttachment attachment = existingMap.get(entry.getKey());
            if (null == attachment) {
                attachment = new EnrEntrantRequestAttachment();
                attachment.setDocument((IEnrEntrantRequestAttachable) get(entry.getKey()));
                attachment.setEntrantRequest(request);
            }
            attachment.setOriginalHandedIn(entry.getValue());

            saveOrUpdate(attachment);

            existingMap.remove(entry.getKey());
        }

        for (EnrEntrantRequestAttachment attachment : existingMap.values()) {
            delete(attachment);
        }

        if (eduDocumentOriginalHandedInNow) {
            EnrEntrantOriginalDocumentStatus originalStatus = getByNaturalId(new EnrEntrantOriginalDocumentStatusGen.NaturalId(request.getEntrant(), request.getEduDocument()));
            if (null != originalStatus)
            {
                if(TwinComboDataSourceHandler.NO_ID.equals(request.getEntrant().getEnrollmentCampaign().getSettings().getAccountingRulesOrigEduDoc()) && requestedCompetition != null && !ObjectUtils.equals(originalStatus.getRequestedCompetition(), requestedCompetition))
                    originalStatus.setRequestedCompetition(requestedCompetition);
            }
            else
            {
                originalStatus = new EnrEntrantOriginalDocumentStatus(request.getEntrant(), request.getEduDocument());
                originalStatus.setRegistrationDate(new Date());
                originalStatus.setTakeAwayDate(null);
            }
            saveOrUpdate(originalStatus);
        }
        else
        {
            EnrSubmittedEduDocumentManager.instance().dao().saveOrUpdateOrganizationOriginalIn(request.getEntrant(), request.getEduDocument(), currentEditOrgOriginalInTitle);
        }
    }

    private void checkRequestNumberUnique(EnrEntrantRequest entrantRequest)
    {
        EnrEntrantDao.lockEntrant(getSession(), entrantRequest.getEntrant());
        EnrEntrantDao.lockEnrollmentCampaign(getSession(), entrantRequest.getEntrant());

        DQLSelectBuilder dql = new DQLSelectBuilder()
        .fromEntity(EnrEntrantRequest.class, "e")
        .column("e.id")
        .where(eq(property(EnrEntrantRequest.entrant().enrollmentCampaign().fromAlias("e")), value(entrantRequest.getEntrant().getEnrollmentCampaign())))
        .where(eq(property(EnrEntrantRequest.regNumber().fromAlias("e")), value(entrantRequest.getRegNumber())))
        .where(ne(property(EnrEntrantRequest.id().fromAlias("e")), value(entrantRequest.getId())))
        ;

        if (existsEntity(dql.buildQuery())) {
            throw new ApplicationException("Номер заявления должен быть уникальным в рамках приемной кампании.");
        }
    }

    @Override
    public void deleteEntrantRequest(Long entrantRequestId)
    {
        if (existsEntity(EnrEnrollmentStepItem.class, EnrEnrollmentStepItem.entity().requestedCompetition().request().id().s(), entrantRequestId)) {
            throw new ApplicationException("Невозможно удалить заявление, так как выбранный в рамках данного заявления конкурс включен в шаг зачисления.");
        }
        if (existsEntity(EnrModelStepItem.class, EnrModelStepItem.entity().requestedCompetition().request().id().s(), entrantRequestId)) {
            throw new ApplicationException("Невозможно удалить заявление, так как выбранный в рамках данного заявления конкурс включен в модель зачисления.");
        }

        Session session = getSession();
        EnrEntrantRequest entrantRequest = getNotNull(EnrEntrantRequest.class, entrantRequestId);
        EnrEntrantDao.lockEntrant(session, entrantRequest.getEntrant());
        EnrEntrantDao.lockEnrollmentCampaign(getSession(), entrantRequest.getEntrant());

        new DQLDeleteBuilder(EnrEntrantBenefitProof.class)
            .where(in("id", new DQLSelectBuilder()
                .fromEntity(EnrEntrantBenefitProof.class, "p").column("p.id")
                .joinEntity("p", DQLJoinType.inner, EnrRequestedCompetition.class, "req", eq(property(EnrEntrantBenefitProof.benefitStatement().id().fromAlias("p")), property("req.id")))
                .where(eq(property("req", EnrRequestedCompetition.request().id()), value(entrantRequestId)))
                .buildQuery()
            )).createStatement(getSession()).execute();
        getSession().flush();

        session.delete(entrantRequest);

        EnrEntrantManager.instance().dao().doNormalizePriorityPerEntrantAndRequestType(entrantRequest.getEntrant(), entrantRequest.getType());
    }

    @Override
    public void deleteRequestedCompetition(Long competitionId)
    {
        if (existsEntity(EnrEnrollmentStepItem.class, EnrEnrollmentStepItem.entity().requestedCompetition().id().s(), competitionId)) {
            throw new ApplicationException("Невозможно удалить выбранный конкурс, так как он включен в шаг зачисления.");
        }
        if (existsEntity(EnrModelStepItem.class, EnrModelStepItem.entity().requestedCompetition().id().s(), competitionId)) {
            throw new ApplicationException("Невозможно удалить выбранный конкурс, так как он включен в модель зачисления.");
        }

        Session session = getSession();

        EnrRequestedCompetition reqComp = getNotNull(EnrRequestedCompetition.class, competitionId);
        EnrEntrant entrant = reqComp.getRequest().getEntrant();

        EnrEntrantDao.lockEntrant(session, entrant);

        if (getCount(EnrRequestedCompetition.class, EnrRequestedCompetition.request().s(), reqComp.getRequest()) < 2)
            throw new ApplicationException("В заявлении должен остаться хотя бы один выбранный конкурс.");

        delete(reqComp);

        EnrEntrantManager.instance().dao().doNormalizePriorityPerEntrantAndRequestType(entrant, reqComp.getRequest().getType());
    }


    @Override
    public void doChangePriorityDown(Long requestedCompetitionId)
    {
        Session session = getSession();

        IEnrEntrantPrioritized reqComp = getNotNull(requestedCompetitionId);
        EnrEntrantRequest request = reqComp instanceof EnrRequestedCompetition ? ((EnrRequestedCompetition) reqComp).getRequest() : ((EnrRequestedProgram) reqComp).getRequestedCompetition().getRequest();
        EnrEntrant entrant = request.getEntrant();

        EnrEntrantDao.lockEntrant(session, entrant);

        List<MetaDSLPath> metaList = Lists.newArrayList(reqComp.getPriorityOwnerPath(), reqComp.getRequestTypePath());
        if (reqComp instanceof EnrRequestedCompetition)
            metaList.add(EnrRequestedCompetition.request().takeAwayDocument());
        MetaDSLPath[] paths = metaList.toArray(new MetaDSLPath[metaList.size()]);

        CommonManager.instance().commonPriorityDao().doChangePriorityDown(reqComp, reqComp instanceof EnrRequestedCompetition ? EnrRequestedCompetition.class : EnrRequestedProgram.class, "priority", paths);

        EnrEntrantManager.instance().dao().doNormalizePriorityPerEntrantAndRequestType(entrant, request.getType());
        ContextLocal.getSite().getDesktop().setRefreshScheduled(true);
    }

    @Override
    public void doChangePriorityUp(Long requestedCompetitionId)
    {
        Session session = getSession();

        IEnrEntrantPrioritized reqComp = getNotNull(requestedCompetitionId);
        EnrEntrantRequest request = reqComp instanceof EnrRequestedCompetition ? ((EnrRequestedCompetition) reqComp).getRequest() : ((EnrRequestedProgram) reqComp).getRequestedCompetition().getRequest();
        EnrEntrant entrant = request.getEntrant();

        EnrEntrantDao.lockEntrant(session, entrant);

        List<MetaDSLPath> metaList = Lists.newArrayList(reqComp.getPriorityOwnerPath(), reqComp.getRequestTypePath());
        if (reqComp instanceof EnrRequestedCompetition)
            metaList.add(EnrRequestedCompetition.request().takeAwayDocument());
        MetaDSLPath[] paths = metaList.toArray(new MetaDSLPath[metaList.size()]);

        CommonManager.instance().commonPriorityDao().doChangePriorityUp(reqComp, reqComp instanceof EnrRequestedCompetition ? EnrRequestedCompetition.class : EnrRequestedProgram.class, "priority", paths);

        EnrEntrantManager.instance().dao().doNormalizePriorityPerEntrantAndRequestType(entrant, request.getType());
        ContextLocal.getSite().getDesktop().setRefreshScheduled(true);
    }

    @Override
    public void doChangeParallel(Long requestedCompetitionId)
    {
        EnrRequestedCompetition competition = getNotNull(EnrRequestedCompetition.class, requestedCompetitionId);
        competition.setParallel(!competition.isParallel());
        update(competition);
    }

    @Override
    public void doChangeProfileEducation(Long requestedCompetitionId)
    {
        EnrRequestedCompetition competition = getNotNull(EnrRequestedCompetition.class, requestedCompetitionId);
        competition.setProfileEducation(!competition.isProfileEducation());
        update(competition);
    }

    @Override
    public void doChangeAccepted(Long requestedCompetitionId)
    {
        EnrRequestedCompetition competition = getNotNull(EnrRequestedCompetition.class, requestedCompetitionId);
        EnrEnrollmentCampaignSettings settings = competition.getRequest().getEntrant().getEnrollmentCampaign().getSettings();
        if (!competition.isAccepted())
        {
            if (competition.getCompetition().getType().getCompensationType().isBudget())
            {

                if (TwinComboDataSourceHandler.YES_ID.equals(settings.getAccountingRulesAgreementBudget()))
                {
                    DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrRequestedCompetition.class, "rc")
                            .where(eq(property("rc", EnrRequestedCompetition.request().entrant()), value(competition.getRequest().getEntrant())))
                            .where(eq(property("rc", EnrRequestedCompetition.competition().requestType()), value(competition.getCompetition().getRequestType())))
                            .where(eq(property("rc", EnrRequestedCompetition.competition().type().compensationType().code()), value(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET)))
                            .where(eq(property("rc", EnrRequestedCompetition.accepted()), value(Boolean.TRUE)))
                            .where(ne(property("rc", EnrRequestedCompetition.id()), value(competition.getId())));

                    for(EnrRequestedCompetition comp : this.<EnrRequestedCompetition>getList(builder))
                    {
                        comp.setAccepted(false);
                        comp.setAcceptedDate(null);
                        update(comp);
                    }

//                    this.executeAndClear(new DQLUpdateBuilder(EnrRequestedCompetition.class)
//                            .fromEntity(EnrRequestedCompetition.class, "rc").where(eq(property("rc.id"), property("id")))
//                            .where(eq(property("rc", EnrRequestedCompetition.request().entrant()), value(competition.getRequest().getEntrant())))
//                            .where(eq(property("rc", EnrRequestedCompetition.competition().requestType()), value(competition.getCompetition().getRequestType())))
//                            .where(eq(property("rc", EnrRequestedCompetition.competition().type().compensationType().code()), value(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET)))
//                            .where(eq(property("rc", EnrRequestedCompetition.accepted()), value(Boolean.TRUE)))
//                            .where(ne(property("rc", EnrRequestedCompetition.id()), value(competition.getId())))
//                            .set(EnrRequestedCompetition.P_ACCEPTED, value(Boolean.FALSE))
//                            .set(EnrRequestedCompetition.P_ACCEPTED_DATE, nul()));
                }
            } else
            {
                if (TwinComboDataSourceHandler.YES_ID.equals(settings.getAccountingRulesAgreementContract()))
                {

                    DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrRequestedCompetition.class, "rc")
                            .where(eq(property("rc", EnrRequestedCompetition.request().entrant()), value(competition.getRequest().getEntrant())))
                            .where(eq(property("rc", EnrRequestedCompetition.competition().requestType()), value(competition.getCompetition().getRequestType())))
                            .where(eq(property("rc", EnrRequestedCompetition.competition().type().compensationType().code()), value(CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT)))
                            .where(eq(property("rc", EnrRequestedCompetition.accepted()), value(Boolean.TRUE)))
                            .where(ne(property("rc", EnrRequestedCompetition.id()), value(competition.getId())));

                    for(EnrRequestedCompetition comp : this.<EnrRequestedCompetition>getList(builder))
                    {
                        comp.setAccepted(false);
                        comp.setAcceptedDate(null);
                        update(comp);
                    }

//                    this.executeAndClear(new DQLUpdateBuilder(EnrRequestedCompetition.class)
//                            .fromEntity(EnrRequestedCompetition.class, "rc").where(eq(property("rc.id"), property("id")))
//                            .where(eq(property("rc", EnrRequestedCompetition.request().entrant()), value(competition.getRequest().getEntrant())))
//                            .where(eq(property("rc", EnrRequestedCompetition.competition().requestType()), value(competition.getCompetition().getRequestType())))
//                            .where(eq(property("rc", EnrRequestedCompetition.competition().type().compensationType().code()), value(CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT)))
//                            .where(eq(property("rc", EnrRequestedCompetition.accepted()), value(Boolean.TRUE)))
//                            .where(ne(property("rc", EnrRequestedCompetition.id()), value(competition.getId())))
//                            .set(EnrRequestedCompetition.P_ACCEPTED, value(Boolean.FALSE))
//                            .set(EnrRequestedCompetition.P_ACCEPTED_DATE, nul()));
                }
            }

            competition.setAcceptedDate(new Date());
        }
        else
        {
            competition.setAcceptedDate(null);
        }

        competition.setAccepted(!competition.isAccepted());
        update(competition);
    }

    @Override
    public void doChangeAcceptedByDate(Long requestedCompetitionId, Date acceptedDate)
    {
        EnrRequestedCompetition competition = getNotNull(EnrRequestedCompetition.class, requestedCompetitionId);
        EnrEnrollmentCampaignSettings settings = competition.getRequest().getEntrant().getEnrollmentCampaign().getSettings();
        if (acceptedDate != null)
        {
            if (competition.getCompetition().getType().getCompensationType().isBudget())
            {

                if (TwinComboDataSourceHandler.YES_ID.equals(settings.getAccountingRulesAgreementBudget()))
                {
                    DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrRequestedCompetition.class, "rc")
                            .where(eq(property("rc", EnrRequestedCompetition.request().entrant()), value(competition.getRequest().getEntrant())))
                            .where(eq(property("rc", EnrRequestedCompetition.competition().requestType()), value(competition.getCompetition().getRequestType())))
                            .where(eq(property("rc", EnrRequestedCompetition.competition().type().compensationType().code()), value(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET)))
                            .where(eq(property("rc", EnrRequestedCompetition.accepted()), value(Boolean.TRUE)))
                            .where(ne(property("rc", EnrRequestedCompetition.id()), value(competition.getId())));

                    for(EnrRequestedCompetition comp : this.<EnrRequestedCompetition>getList(builder))
                    {
                        comp.setAccepted(false);
                        comp.setAcceptedDate(null);
                        update(comp);
                    }

//                    this.executeAndClear(new DQLUpdateBuilder(EnrRequestedCompetition.class)
//                            .fromEntity(EnrRequestedCompetition.class, "rc").where(eq(property("rc.id"), property("id")))
//                            .where(eq(property("rc", EnrRequestedCompetition.request().entrant()), value(competition.getRequest().getEntrant())))
//                            .where(eq(property("rc", EnrRequestedCompetition.competition().requestType()), value(competition.getCompetition().getRequestType())))
//                            .where(eq(property("rc", EnrRequestedCompetition.competition().type().compensationType().code()), value(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET)))
//                            .where(eq(property("rc", EnrRequestedCompetition.accepted()), value(Boolean.TRUE)))
//                            .where(ne(property("rc", EnrRequestedCompetition.id()), value(competition.getId())))
//                            .set(EnrRequestedCompetition.P_ACCEPTED, value(Boolean.FALSE))
//                            .set(EnrRequestedCompetition.P_ACCEPTED_DATE, nul()));
                }
            } else
            {
                if (TwinComboDataSourceHandler.YES_ID.equals(settings.getAccountingRulesAgreementContract()))
                {
                    DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrRequestedCompetition.class, "rc")
                            .where(eq(property("rc", EnrRequestedCompetition.request().entrant()), value(competition.getRequest().getEntrant())))
                            .where(eq(property("rc", EnrRequestedCompetition.competition().requestType()), value(competition.getCompetition().getRequestType())))
                            .where(eq(property("rc", EnrRequestedCompetition.competition().type().compensationType().code()), value(CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT)))
                            .where(eq(property("rc", EnrRequestedCompetition.accepted()), value(Boolean.TRUE)))
                            .where(ne(property("rc", EnrRequestedCompetition.id()), value(competition.getId())));

                    for(EnrRequestedCompetition comp : this.<EnrRequestedCompetition>getList(builder))
                    {
                        comp.setAccepted(false);
                        comp.setAcceptedDate(null);
                        update(comp);
                    }

//                    this.executeAndClear(new DQLUpdateBuilder(EnrRequestedCompetition.class)
//                            .fromEntity(EnrRequestedCompetition.class, "rc").where(eq(property("rc.id"), property("id")))
//                            .where(eq(property("rc", EnrRequestedCompetition.request().entrant()), value(competition.getRequest().getEntrant())))
//                            .where(eq(property("rc", EnrRequestedCompetition.competition().requestType()), value(competition.getCompetition().getRequestType())))
//                            .where(eq(property("rc", EnrRequestedCompetition.competition().type().compensationType().code()), value(CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT)))
//                            .where(eq(property("rc", EnrRequestedCompetition.accepted()), value(Boolean.TRUE)))
//                            .where(ne(property("rc", EnrRequestedCompetition.id()), value(competition.getId())))
//                            .set(EnrRequestedCompetition.P_ACCEPTED, value(Boolean.FALSE))
//                            .set(EnrRequestedCompetition.P_ACCEPTED_DATE, nul()));
                }
            }

            competition.setAcceptedDate(acceptedDate);
            competition.setAccepted(true);
        }
        else
        {
            competition.setAcceptedDate(null);
            competition.setAccepted(false);
        }

        update(competition);
    }

    @Override
    public void doChangeAcceleratedLearning(Long requestedCompetitionId) {
        EnrRequestedCompetition competition = getNotNull(EnrRequestedCompetition.class, requestedCompetitionId);
        competition.setAcceleratedLearning(!competition.isAcceleratedLearning());
        update(competition);
    }

    @Override
    public void doChangeRefusedToBeEnrolled(Long requestedCompetitionId)
    {

        EnrRequestedCompetition competition = getNotNull(EnrRequestedCompetition.class, requestedCompetitionId);
        if (competition.getRefusedToBeEnrolled()) {
            competition.setRefusedDate(null);
            EnrEntrantDaemonBean.DAEMON.registerAfterCompleteWakeUpAndWait();
        } else {
            // «выбран к зачислению», «в приказе», «зачислен», «забрал документы», «в архиве»
            if (competition.getState() != null && Sets.newHashSet(SELECT_SHOULD_BE_ENROLLED, IN_ORDER, ENROLLED, TAKE_DOCUMENTS_AWAY, ENTRANT_ARCHIVED).contains(competition.getState().getCode())) {
                throw new ApplicationException("Нельзя принять отказ от зачисления по выбранному конкурсу в состоянии «" + competition.getState().getTitle() + "».");
            }

            competition.setState(getCatalogItem(EnrEntrantState.class, EXCLUDED_BY_REJECT));
            competition.setRefusedDate(new Date());
        }
        update(competition);
    }

    @Override
    public void doChangeRefusedToBeEnrolledByDate(Long requestedCompetitionId, Date refusedDate)
    {
        EnrRequestedCompetition competition = getNotNull(EnrRequestedCompetition.class, requestedCompetitionId);
        if (refusedDate == null) {
            competition.setRefusedDate(null);
            EnrEntrantDaemonBean.DAEMON.registerAfterCompleteWakeUpAndWait();
        } else {
            if(competition.getRefusedDate() == null)
            {
                // «выбран к зачислению», «в приказе», «зачислен», «забрал документы», «в архиве»
                if (competition.getState() != null && Sets.newHashSet(SELECT_SHOULD_BE_ENROLLED, IN_ORDER, ENROLLED, TAKE_DOCUMENTS_AWAY, ENTRANT_ARCHIVED).contains(competition.getState().getCode()))
                {
                    throw new ApplicationException("Нельзя принять отказ от зачисления по выбранному конкурсу в состоянии «" + competition.getState().getTitle() + "».");
                }
                competition.setRefusedDate(refusedDate);
                competition.setState(getCatalogItem(EnrEntrantState.class, EXCLUDED_BY_REJECT));
            }
        }
        update(competition);
    }

    @Override
    public void doUpdateEnrReqCompEnrollAvailableAndEduDocForEntrant(EnrEntrant entrant)
    {
        doUpdateEnrReqCompEnrollAvailableAndEduDoc(entrant);
    }

    @Override
    public void doChangeOriginalForReqComp(Long requestedCompetitionId)
    {
        EnrRequestedCompetition requestedCompetition = get(requestedCompetitionId);
        EnrEntrantOriginalDocumentStatus originalDocumentStatus = getByNaturalId(new EnrEntrantOriginalDocumentStatus.NaturalId(requestedCompetition.getRequest().getEntrant(), requestedCompetition.getRequest().getEduDocument()));
        if(requestedCompetition.isOriginalDocumentHandedIn())
        {
            originalDocumentStatus.setOriginalDocumentHandedIn(false);
            originalDocumentStatus.setRequestedCompetition(null);
        }
        else
        {
            DQLUpdateBuilder updateBuilder = new DQLUpdateBuilder(EnrEntrantOriginalDocumentStatus.class)
                    .where(eq(property(EnrEntrantOriginalDocumentStatus.requestedCompetition()), value(requestedCompetition)))
                    .set(EnrEntrantOriginalDocumentStatus.L_REQUESTED_COMPETITION, nul());

            if (originalDocumentStatus == null)
            {
                originalDocumentStatus = new EnrEntrantOriginalDocumentStatus(requestedCompetition.getRequest().getEntrant(), requestedCompetition.getRequest().getEduDocument());
                originalDocumentStatus.setRegistrationDate(new Date());
                originalDocumentStatus.setRequestedCompetition(requestedCompetition);
            }
            else
            {
                updateBuilder.where(ne(property(EnrEntrantOriginalDocumentStatus.id()), originalDocumentStatus.getId()));
                originalDocumentStatus.setOriginalDocumentHandedIn(true);
                originalDocumentStatus.setRequestedCompetition(requestedCompetition);
            }

            this.executeAndClear(updateBuilder);
        }
        saveOrUpdate(originalDocumentStatus);
    }

    @Override
    public boolean checkOrigDocuments(PersonEduDocument oldEduDocument, PersonEduDocument newEduDocument, EnrEntrantRequest request)
    {
        EnrEntrantOriginalDocumentStatus oldDoc = getByNaturalId(new EnrEntrantOriginalDocumentStatus.NaturalId(request.getEntrant(), oldEduDocument));
        EnrEntrantOriginalDocumentStatus newDoc = getByNaturalId(new EnrEntrantOriginalDocumentStatus.NaturalId(request.getEntrant(), newEduDocument));

        return (oldDoc != null && oldDoc.isOriginalDocumentHandedIn()) || (newDoc != null && newDoc.isOriginalDocumentHandedIn());
    }

    @Override
    public boolean isShowRequestColumn(Long entrantId)
    {
        return existsEntity(new DQLSelectBuilder()
                .fromEntity(EnrEntrantRequest.class, "r")
                .where(eq(property("r", EnrEntrantRequest.entrant().id()), value(entrantId)))
                .where(exists(new DQLSelectBuilder()
                        .fromEntity(EnrEntrantAchievementType.class, "at")
                        .where(eq(property("at", EnrEntrantAchievementType.forRequest()), value(true)))
                        .where(eq(property("at", EnrEntrantAchievementType.achievementKind().requestType()), property("r", EnrEntrantRequest.type())))
                        .where(eq(property("at", EnrEntrantAchievementType.enrollmentCampaign()), property("r", EnrEntrantRequest.entrant().enrollmentCampaign())))
                        .buildQuery()))
                .buildQuery());
    }

    @Override
    public int doUpdateEnrReqCompEnrollAvailableAndEduDoc()
    {
        return doUpdateEnrReqCompEnrollAvailableAndEduDoc(null);
    }

    private int doUpdateEnrReqCompEnrollAvailableAndEduDoc(EnrEntrant entrant)
    {
        String baseLockName = "doUpdateEnrReqCompEnrollAvailableAndEduDoc";
        String entrantLockName = "doUpdateEnrReqCompEnrollAvailableAndEduDocEntrant";
        Session session = getSession();

        if(entrant != null)
        {
            if(NamedSyncInTransactionCheckLocker.hasLock(baseLockName))
            {
                NamedSyncInTransactionCheckLocker.register(session, baseLockName);
            }
            else
            {
                if(!NamedSyncInTransactionCheckLocker.hasLock(entrantLockName))
                {
                    NamedSyncInTransactionCheckLocker.register(session, entrantLockName);
                }
                NamedSyncInTransactionCheckLocker.register(session, baseLockName + entrant.getId());
            }
        }
        else
        {
            if(NamedSyncInTransactionCheckLocker.hasLock(entrantLockName))
            {
                NamedSyncInTransactionCheckLocker.register(session, entrantLockName);
            }
            NamedSyncInTransactionCheckLocker.register(session, baseLockName);
        }

        int updates = 0;
        // установить конкурс для оригинала
        if(entrant != null)
        {
            updates += doUpdateReqCompForEduDoc(entrant.getEnrollmentCampaign().getId(), entrant.getEnrollmentCampaign().getSettings().getEnrollmentRulesContract(), entrant, session);
        }
        else
        {
            DQLSelectBuilder ecBuilder = new DQLSelectBuilder()
                    .fromEntity(EnrRequestedCompetition.class, "x")
                    .distinct()
                    .column(property(EnrRequestedCompetition.request().entrant().enrollmentCampaign().fromAlias("x")))
                    .where(eq(property(EnrRequestedCompetition.request().entrant().enrollmentCampaign().open().fromAlias("x")), value(Boolean.TRUE)));

            for (EnrEnrollmentCampaign ec : ecBuilder.createStatement(session).<EnrEnrollmentCampaign>list())
            {
                updates += doUpdateReqCompForEduDoc(ec.getId(), ec.getSettings().getEnrollmentRulesContract(), null, session);
            }
        }
        // установить конкурс для оригинала

        if(entrant != null)
        {
            EnrEnrollmentCampaignSettings settings = entrant.getEnrollmentCampaign().getSettings();
            updates += doUpdateEnrReqCompEnrollAvailableAndEduDoc(entrant.getEnrollmentCampaign().getId(), settings.getAccountingRulesOrigEduDoc(), settings.getEnrollmentRulesContract(), entrant, session);
        }
        else
        {
            DQLSelectBuilder ecBuilder = new DQLSelectBuilder()
                    .fromEntity(EnrRequestedCompetition.class, "x")
                    .distinct()
                    .column(property(EnrRequestedCompetition.request().entrant().enrollmentCampaign().id().fromAlias("x")))
                    .column(property(EnrRequestedCompetition.request().entrant().enrollmentCampaign().settings().accountingRulesOrigEduDoc().fromAlias("x")))
                    .column(property(EnrRequestedCompetition.request().entrant().enrollmentCampaign().settings().enrollmentRulesContract().fromAlias("x")))
                    .where(eq(property(EnrRequestedCompetition.request().entrant().enrollmentCampaign().open().fromAlias("x")), value(Boolean.TRUE)));

            for (Object[] ec : ecBuilder.createStatement(session).<Object[]>list())
            {
                updates += doUpdateEnrReqCompEnrollAvailableAndEduDoc((Long)ec[0], (Long)ec[1], (Long)ec[2], null, session);
            }
        }

        return updates;
    }

    private int doUpdateReqCompForEduDoc(Long enrCampaignId, Long enrollmentRulesContract, EnrEntrant entrant, Session session)
    {
        int updates = 0;

        DQLSelectBuilder origDocReqCompUpdateDataSource = new DQLSelectBuilder().fromEntity(EnrRequestedCompetition.class, "rc")
                .joinPath(DQLJoinType.inner, EnrRequestedCompetition.request().fromAlias("rc"), "rq")
                .joinEntity("rc", DQLJoinType.inner, EnrEntrantOriginalDocumentStatus.class, "od", and(eq(property("od", EnrEntrantOriginalDocumentStatus.entrant()), property("rq", EnrEntrantRequest.entrant())),
                        eq(property("od", EnrEntrantOriginalDocumentStatus.eduDocument()), property("rq", EnrEntrantRequest.eduDocument()))))
                .column(property("rc", EnrRequestedCompetition.id()), "rcid")
                .column(property("od", EnrEntrantOriginalDocumentStatus.id()), "docid")
                .column(DQLFunctions.rowNumberBuilder().partition(property("od")).order(property("rc", EnrRequestedCompetition.priority())).build(), "pr")
                .where(eq(property("rc", EnrRequestedCompetition.refusedToBeEnrolled()), value(Boolean.FALSE)))
                .where(eq(property("rc", EnrRequestedCompetition.request().takeAwayDocument()), value(Boolean.FALSE)))
                .where(isNull(property("od", EnrEntrantOriginalDocumentStatus.requestedCompetition())))
                .where(isNull(property("od", EnrEntrantOriginalDocumentStatus.takeAwayDate())))
                .where(eq(property("od", EnrEntrantOriginalDocumentStatus.entrant().enrollmentCampaign().settings().accountingRulesOrigEduDoc()), value(TwinComboDataSourceHandler.NO_ID)));

        if(entrant != null)
            origDocReqCompUpdateDataSource.where(eq(property("rc", EnrRequestedCompetition.request().entrant()), value(entrant)));
        else
            origDocReqCompUpdateDataSource.where(eq(property("od", EnrEntrantOriginalDocumentStatus.entrant().enrollmentCampaign().id()), value(enrCampaignId)));

        DQLUpdateBuilder origDocReqCompUpdateBuilder = new DQLUpdateBuilder(EnrEntrantOriginalDocumentStatus.class)
                .fromDataSource(
                        origDocReqCompUpdateDataSource.buildQuery(),
                        "src"
                )
                .where(eq(property("id"), property("src.docid")))
                .where(eq(property("src.pr"), value(1)))
                .set(EnrEntrantOriginalDocumentStatus.L_REQUESTED_COMPETITION, property("src.rcid"));

        updates += CommonDAO.executeAndClear(origDocReqCompUpdateBuilder, session);

        return updates;
    }

    private int doUpdateEnrReqCompEnrollAvailableAndEduDoc(Long enrollmentCampaingId, Long accountingRulesOrigEduDoc, Long enrollmentRulesContract, EnrEntrant entrant, Session session)
    {
        final Collection<IPropertyUpdateRule<Boolean>> rules = EnrRequestedCompetitionManager.instance().enrollmentAvailableRules().getItems().values();

        int updates = 0;
        // кеш оригиналов
        DQLSelectBuilder updateOrigDataSource = new DQLSelectBuilder().fromEntity(EnrEntrantRequest.class, "er")
                .joinEntity("er", DQLJoinType.left, EnrEntrantOriginalDocumentStatus.class, "origDoc", and(
                        eq(property("origDoc", EnrEntrantOriginalDocumentStatus.entrant()), property("er", EnrEntrantRequest.entrant())),
                        eq(property("origDoc", EnrEntrantOriginalDocumentStatus.eduDocument()), property("er", EnrEntrantRequest.eduDocument()))
                ))
                .column(property("er", EnrEntrantRequest.id()), "erid");

        if(TwinComboDataSourceHandler.NO_ID.equals(accountingRulesOrigEduDoc))
        {
            DQLSelectBuilder existsBuilder = new DQLSelectBuilder().fromEntity(EnrEntrantOriginalDocumentStatus.class, "od")
                    .where(eqNullSafe(property("od"), property("origDoc")))
                    .where(eq(property("od", EnrEntrantOriginalDocumentStatus.requestedCompetition().request()), property("er")));

            updateOrigDataSource.column(caseExpr(exists(existsBuilder.buildQuery()), property("origDoc.id"), nul()), "original");
        }
        else
        {
            updateOrigDataSource.column(property("origDoc.id"), "original");
        }


        if(entrant != null)
            updateOrigDataSource.where(eq(property("er", EnrEntrantRequest.entrant()), value(entrant)));
        else
            updateOrigDataSource.where(eq(property("er", EnrEntrantRequest.entrant().enrollmentCampaign().id()), value(enrollmentCampaingId)));

        DQLUpdateBuilder updateOrigBuilder = new DQLUpdateBuilder(EnrEntrantRequest.class)
                .fromDataSource(updateOrigDataSource.buildQuery(), "src")
                .where(eq(property(EnrEntrantRequest.id()), property("src.erid")))
                .where(neNullSafe(property(EnrEntrantRequest.eduInstDocOriginalRef()), property("src.original")))
                .set(EnrEntrantRequest.L_EDU_INST_DOC_ORIGINAL_REF, property("src.original"));

        updates +=  CommonDAO.executeAndClear(updateOrigBuilder, session);
        // кеш оригиналов

        // EnrRequestedCompetition.originalDocumentHandedIn()
        DQLSelectBuilder origInHandExistBuilder = new DQLSelectBuilder().fromEntity(EnrEntrantOriginalDocumentStatus.class, "origDoc")
                .where(eq(property("origDoc"), property("rc", EnrRequestedCompetition.request().eduInstDocOriginalRef())))
                .where(eq(property("origDoc", EnrEntrantOriginalDocumentStatus.originalDocumentHandedIn()), value(true)));

        if(TwinComboDataSourceHandler.NO_ID.equals(accountingRulesOrigEduDoc))
        {
            origInHandExistBuilder.where(eq(property("origDoc", EnrEntrantOriginalDocumentStatus.requestedCompetition()), property("rc")));
        }

        DQLSelectBuilder updateReqCompOrigDataSource = new DQLSelectBuilder().fromEntity(EnrRequestedCompetition.class, "rc")
                .joinPath(DQLJoinType.inner, EnrRequestedCompetition.request().fromAlias("rc"), "request")
                .column(property("rc", EnrRequestedCompetition.id()), "rcid")
                .column(caseExpr(exists(origInHandExistBuilder.buildQuery()), value(true), value(false)), "orig");

        if(entrant != null)
            updateReqCompOrigDataSource.where(eq(property("rc", EnrRequestedCompetition.request().entrant()), value(entrant)));
        else
            updateReqCompOrigDataSource.where(eq(property("rc", EnrRequestedCompetition.request().entrant().enrollmentCampaign().id()), value(enrollmentCampaingId)));

        DQLUpdateBuilder updateReqCompOrigBuilder = new DQLUpdateBuilder(EnrRequestedCompetition.class)
                .fromDataSource(updateReqCompOrigDataSource.buildQuery(), "src")
                .where(eq(property(EnrRequestedCompetition.id()), property("src.rcid")))
                .where(ne(property(EnrRequestedCompetition.originalDocumentHandedIn()), property("src.orig")))
                .set(EnrRequestedCompetition.P_ORIGINAL_DOCUMENT_HANDED_IN, property("src.orig"));

        updates += CommonDAO.executeAndClear(updateReqCompOrigBuilder, session);
        // EnrRequestedCompetition.originalDocumentHandedIn()

        // update enrollmentAvailable
        final Map<String, Object> params = new ParametersMap().add("enrollmentCampaignId", enrollmentCampaingId);
        final DQLCaseExpressionBuilder valueCaseBuilder = this.valueCaseBuilder(params, rules, Boolean.FALSE);

        DQLSelectBuilder updateEnrollmentAvailableDataSource = new DQLSelectBuilder()
                .fromEntity(EnrRequestedCompetition.class, "x")
                .column(property("x"), "id")
                .column(valueCaseBuilder.build(), "value");

        if (entrant != null)
            updateEnrollmentAvailableDataSource.where(eq(property("x", EnrRequestedCompetition.request().entrant()), value(entrant)));
        else
            updateEnrollmentAvailableDataSource.where(eq(property(EnrRequestedCompetition.request().entrant().enrollmentCampaign().id().fromAlias("x")), value(enrollmentCampaingId)));

        DQLUpdateBuilder updateEnrollmentAvailableBuilder = new DQLUpdateBuilder(EnrRequestedCompetition.class)
                .fromDataSource(
                        updateEnrollmentAvailableDataSource.buildQuery(),
                        "src"
                )
                .where(eq(property("id"), property("src.id")))
                .where(ne(property(EnrRequestedCompetition.P_ENROLLMENT_AVAILABLE), property("src.value")))
                .set(EnrRequestedCompetition.P_ENROLLMENT_AVAILABLE, property("src.value"));

        updates += CommonDAO.executeAndClear(
                updateEnrollmentAvailableBuilder, session
        );
        // update enrollmentAvailable
        return updates;
    }

    private <T> Map<T, IDQLExpression> getResultMap(final Map<String, Object> params, final Collection<IPropertyUpdateRule<T>> rules, final T defaultValue)
    {
        final Map<T, IDQLExpression> resultMap = new HashMap<T, IDQLExpression>();
        for (final IPropertyUpdateRule<T> rule: rules) {
            final Map<T, IDQLExpression> ruleMap = rule.buildValueExpressionMap("x", params);
            if (null != ruleMap) {
                for (final Map.Entry<T, IDQLExpression> e: ruleMap.entrySet()) {
                    if (ObjectUtils.equals(e.getKey(), defaultValue)) { throw new IllegalStateException(); }
                    resultMap.put(e.getKey(), and(e.getValue(), resultMap.get(e.getKey())));
                }
            }
        }
        return resultMap;
    }

    private <T> DQLCaseExpressionBuilder valueCaseBuilder(final Map<String, Object> params, final Collection<IPropertyUpdateRule<T>> rules, final T defaultValue)
    {
        final Map<T, IDQLExpression> resultMap = getResultMap(params, rules, defaultValue);
        final DQLCaseExpressionBuilder valueCaseBuilder = new DQLCaseExpressionBuilder();
        for (final Map.Entry<T, IDQLExpression> e: resultMap.entrySet()) {
            valueCaseBuilder.when(e.getValue(), commonValue(e.getKey()));
        }
        valueCaseBuilder.otherwise(value(Boolean.FALSE));
        return valueCaseBuilder;
    }

}
