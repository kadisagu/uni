package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import static org.tandemframework.shared.commonbase.utils.MigrationUtils.*;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x8x2_6to7 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrDisablementType

		// создано обязательное свойство priority
		{
			// создать колонку
			tool.createColumn("enr14_c_disablement_type_t", new DBColumn("priority_p", DBType.INTEGER));

			// задать значение по умолчанию
            final BatchUpdater updater = new BatchUpdater("update enr14_c_disablement_type_t set priority_p=? where code_p=?", DBType.INTEGER, DBType.EMPTY_STRING);
            updater.addBatch(1, "1");   // Ребенок-инвалид
            updater.addBatch(2, "2");   // I группа
            updater.addBatch(3, "3");   // II группа
            updater.addBatch(6, "4");   // III группа
            updater.executeUpdate(tool);

			// сделать колонку NOT NULL
			tool.setColumnNullable("enr14_c_disablement_type_t", "priority_p", false);
		}
    }
}