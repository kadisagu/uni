package ru.tandemservice.unienr14.entrant.daemon;

import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import org.tandemframework.shared.commonbase.events.EventListenerLocker;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.event.EnrEntrantBlockListenerBean;
import ru.tandemservice.unienr14.rating.daemon.EnrRatingDaemonDao;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.event.EnrEntrantRequestBlockListenerBean;
import ru.tandemservice.unienr14.settings.daemon.EnrSettingsDaemonBean;

/**
 * @author vdanilov
 */
public class EnrEntrantDaemonBean extends UniBaseDao implements IEnrEntrantDaemonBean {

    public static final SyncDaemon DAEMON = new SyncDaemon(EnrEntrantDaemonBean.class.getName(), 120, EnrSettingsDaemonBean.DAEMON.getName()) {
        @Override protected void main()
        {
            try(
            EventListenerLocker.Lock lock1 = EnrEntrantBlockListenerBean.LOCKER.lock(EventListenerLocker.<EnrEntrantBlockListenerBean.Listener>noopHandler());
            EventListenerLocker.Lock lock2 = EnrEntrantRequestBlockListenerBean.LOCKER.lock(EventListenerLocker.<EnrEntrantRequestBlockListenerBean.Listener>noopHandler());
            ) {

                // обновляем рейтинг (на основе его данных будет формироваться состояние)
                EnrRatingDaemonDao.DAEMON_ACTIONS.run();

                // затем считаем состояние
                EnrEntrantStateDaemonDao.DAEMON_ACTIONS.run();
            }
        }
    };


}
