/* $Id:$ */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.logic;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLSelectableQuery;
import org.tandemframework.shared.organization.sec.bo.Sec.SecManager;
import org.tandemframework.shared.organization.sec.bo.Sec.util.RoleAssignmentCacheSource;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType2eduProgramKindRel;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetEnrollmentCommission;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.sec.entity.RoleAssignmentLocalEnrCommission;
import ru.tandemservice.unienr14.sec.entity.RoleAssignmentTemplateEnrCommission;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.EnrEnrollmentCommissionManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 6/11/15
 */
public class EnrEnrollmentCommissionDao extends UniBaseDao implements IEnrEnrollmentCommissionDao
{
    public static final String SETTINGS_KEY_PREFIX_DEFAULT_ENR_COMMISSION = "enrDefaultEnrCommission.";
    public static final String SETTINGS_KEY_DEFAULT_ENR_COMMISSION = "commission";

    public static final RoleAssignmentCacheSource RA_SRC_TEMPLATE_ENR_COMMISSION = new RoleAssignmentCacheSource("t_enr_comm")
    {
        @Override
        public IDQLSelectableQuery query()
        {
            return new DQLSelectBuilder()
                .fromEntity(RoleAssignmentTemplateEnrCommission.class, "t_enr_comm_a")
                .column(property("t_enr_comm_a", RoleAssignmentTemplateEnrCommission.principalContext().id()), "u_pc")
                .column(property("t_enr_comm_a", RoleAssignmentTemplateEnrCommission.roleConfig().role().id()), "u_role")
                .column(property("t_enr_comm_a", RoleAssignmentTemplateEnrCommission.enrollmentCommission().id()), "u_entity")
                .buildQuery();
        }
    };

    public static final RoleAssignmentCacheSource RA_SRC_LOCAL_ENR_COMMISSION = new RoleAssignmentCacheSource("l_enr_comm")
    {
        @Override
        public IDQLSelectableQuery query()
        {
            return new DQLSelectBuilder()
                .fromEntity(RoleAssignmentLocalEnrCommission.class, "t_enr_comm_a")
                .column(property("t_enr_comm_a", RoleAssignmentLocalEnrCommission.principalContext().id()), "u_pc")
                .column(property("t_enr_comm_a", RoleAssignmentLocalEnrCommission.roleConfig().role().id()), "u_role")
                .column(property("t_enr_comm_a", RoleAssignmentLocalEnrCommission.roleConfig().enrollmentCommission().id()), "u_entity")
                .buildQuery();
        }
    };

    @Override
    public List<EnrEnrollmentCommission> getEnrollmentCommissionList()
    {
        return getList(EnrEnrollmentCommission.class, EnrEnrollmentCommission.title().s());
    }

    @Override
    public EnrEnrollmentCommission getDefaultCommission(boolean required)
    {
        String settingsOwner = DataSettingsFacade.getSettingsOwner();
        if (null == settingsOwner)
            return null;

        IDataSettings settings = DataSettingsFacade.getSettings(settingsOwner, SETTINGS_KEY_PREFIX_DEFAULT_ENR_COMMISSION);
        Object campaign = settings.get(SETTINGS_KEY_DEFAULT_ENR_COMMISSION);

        List<EnrEnrollmentCommission> enrollmentCommissionList = EnrEnrollmentCommissionManager.instance().permissionDao().getEnrollmentCommissionList(UserContext.getInstance().getPrincipalContext());

        if(required && (campaign == null || (campaign instanceof EnrEnrollmentCommission && !enrollmentCommissionList.contains(campaign))))
        {
            if(!enrollmentCommissionList.isEmpty())
            {
                EnrEnrollmentCommission enrollmentCommission = enrollmentCommissionList.get(0);
                saveDefaultCommission(enrollmentCommission);
                return enrollmentCommission;
            }
        }

        if (campaign instanceof EnrEnrollmentCommission)
            return (EnrEnrollmentCommission) campaign;

        return null;
    }

    @Override
    public void saveDefaultCommission(EnrEnrollmentCommission commission)
    {
        String settingsOwner = DataSettingsFacade.getSettingsOwner();
        if (null == settingsOwner)
            return;

        IDataSettings settings = DataSettingsFacade.getSettings(settingsOwner, SETTINGS_KEY_PREFIX_DEFAULT_ENR_COMMISSION);
        settings.set(SETTINGS_KEY_DEFAULT_ENR_COMMISSION, commission);
        DataSettingsFacade.saveSettings(settings);
    }


    @Override
    public Map<EnrProgramSetOrgUnit, EnrProgramSetEnrollmentCommission> getProgramSetOrgUnit2EnrollmentCommissionMap(EnrEnrollmentCampaign campaign, EnrRequestType requestType)
    {
        Map<EnrProgramSetOrgUnit, EnrProgramSetEnrollmentCommission> resultMap = Maps.newHashMap();
        if (null == campaign || null == requestType) return resultMap;

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrProgramSetOrgUnit.class, "epr").column("epr")
                .joinPath(DQLJoinType.inner, EnrProgramSetOrgUnit.programSet().fromAlias("epr"), "s")
                .where(eq(property("ec", EnrProgramSetEnrollmentCommission.programSetOrgUnit().id()), property("epr.id")))
                .where(eq(property("s", EnrProgramSetBase.enrollmentCampaign()), value(campaign)))
                .joinEntity("s", DQLJoinType.inner, requestType.getProgramSetClass(), "e", eq(property("e", EnrProgramSetBase.id()), property("s", EnrProgramSetBase.id())))
                .where(exists(
                        EnrRequestType2eduProgramKindRel.class,
                        EnrRequestType2eduProgramKindRel.requestType().s(), requestType,
                        EnrRequestType2eduProgramKindRel.programKind().s(), property("s", EnrProgramSetBase.programSubject().subjectIndex().programKind())
                ));

        List<EnrProgramSetEnrollmentCommission> enrollmentCommissionList = new DQLSelectBuilder().fromEntity(EnrProgramSetEnrollmentCommission.class, "ec").where(exists(builder.buildQuery())).createStatement(getSession()).list();

        for (EnrProgramSetEnrollmentCommission commission : enrollmentCommissionList)
            resultMap.put(commission.getProgramSetOrgUnit(), commission);

        return resultMap;
    }

    @Override
    public List<EnrProgramSetOrgUnit> getProgramSetOrgUnitList(@NotNull EnrEnrollmentCampaign campaign, List<EnrEnrollmentCommission> enrollmentCommission)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrProgramSetEnrollmentCommission.class, "ou")
                .column(property("psOu")).distinct()
                .joinPath(DQLJoinType.inner, EnrProgramSetEnrollmentCommission.programSetOrgUnit().fromAlias("ou"), "psOu")
                .where(eq(property("psOu", EnrProgramSetOrgUnit.programSet().enrollmentCampaign()), value(campaign)));

        if (null != enrollmentCommission && !enrollmentCommission.isEmpty())
        {
            builder.where(in(property("ou", EnrProgramSetEnrollmentCommission.enrollmentCommission()), enrollmentCommission));
        }
        return builder.createStatement(getSession()).list();
    }

    @Override
    public void saveProgramSetEnrollmentCommission(Map<EnrProgramSetOrgUnit, EnrProgramSetEnrollmentCommission> orgUnit2SetECMap, EnrProgramSetOrgUnit programSetOrgUnit, EnrEnrollmentCommission enrollmentCommission)
    {
        EnrProgramSetEnrollmentCommission commission = orgUnit2SetECMap.get(programSetOrgUnit);
        if (null != enrollmentCommission)
        {
            if (null != commission)
            {
                if (enrollmentCommission.equals(commission.getEnrollmentCommission())) return;
                DataAccessServices.dao().delete(commission);
            }

            commission = new EnrProgramSetEnrollmentCommission();
            commission.setProgramSetOrgUnit(programSetOrgUnit);
            commission.setEnrollmentCommission(enrollmentCommission);
            DataAccessServices.dao().saveOrUpdate(commission);
        }
        else if (null != commission)
            DataAccessServices.dao().delete(commission);
    }


    @Override
    public boolean filterRequestedCompetitionsByEnrCommission(DQLSelectBuilder dql, String reqCompAlias, Object commissions)
    {
        if (commissions == null) {
            return false;
        }

        Preconditions.checkArgument(commissions instanceof EnrEnrollmentCommission || commissions instanceof Collection);

        return FilterUtils.applySelectFilter(dql, reqCompAlias, EnrRequestedCompetition.request().enrollmentCommission(), commissions);
    }

    @Override
    public boolean filterEntrantRequestByEnrCommission(DQLSelectBuilder dql, String entrantRequestAlias, Object commissions)
    {
        if (commissions == null) {
            return false;
        }

        Preconditions.checkArgument(commissions instanceof EnrEnrollmentCommission || commissions instanceof Collection);

        return FilterUtils.applySelectFilter(dql, entrantRequestAlias, EnrEntrantRequest.enrollmentCommission(), commissions);
    }

    @Override
    public boolean filterEntrantByEnrCommission(DQLSelectBuilder dql, String entrantAlias, Object commissions)
    {
        if (commissions == null || (commissions instanceof Collection && ((Collection) commissions).isEmpty())) {
            return false;
        }

        Preconditions.checkArgument(commissions instanceof EnrEnrollmentCommission || commissions instanceof Collection);

        dql.where(exists(EnrEntrantRequest.class,
                         EnrEntrantRequest.entrant().s(), property(entrantAlias),
                         EnrEntrantRequest.enrollmentCommission().s(), commissions
        ));

        return true;
    }


}