package ru.tandemservice.unienr14.exams.entity;

import ru.tandemservice.unienr14.exams.entity.gen.*;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline;

/**
 * Дисциплина набора ВИ
 *
 * Сводные данные по дисциплинам, входящим в набор ВИ.
 * Обновляется демоном IEnrExamSetDaemonBean и listener-ами на основе Элементов набора ВИ (enrExamSetElement).
 * Данная сущность обеспечивает уникальность набора дисциплин внутри ВИ (включая содержимое групп).
 */
public class EnrExamSetDiscipline extends EnrExamSetDisciplineGen
{
    public EnrExamSetDiscipline() {
    }

    public EnrExamSetDiscipline(EnrExamSet examSet, EnrCampaignDiscipline discipline) {
        this.setExamSet(examSet);
        this.setDiscipline(discipline);
    }

    public EnrExamSetDiscipline(EnrExamSet examSet, EnrCampaignDiscipline discipline, EnrExamSetElement source) {
        this(examSet, discipline);
        this.setSource(source);
    }
}