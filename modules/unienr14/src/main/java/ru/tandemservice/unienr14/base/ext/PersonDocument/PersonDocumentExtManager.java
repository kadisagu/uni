/* $Id:$ */
package ru.tandemservice.unienr14.base.ext.PersonDocument;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.shared.person.base.bo.PersonDocument.PersonDocumentManager;
import org.tandemframework.shared.person.base.bo.PersonDocument.logic.PersonDocumentContext;
import org.tandemframework.shared.person.base.bo.PersonDocument.logic.PersonDocumentPropertySetCreator;
import org.tandemframework.shared.person.base.bo.PersonDocument.logic.PersonDocumentTypeConfig;
import org.tandemframework.shared.person.base.bo.PersonDocument.logic.PersonDocumentTypeSettingsConfig;
import org.tandemframework.shared.person.base.entity.PersonDocument;
import org.tandemframework.shared.person.base.util.PersonDocumentUtil;
import ru.tandemservice.unienr14.catalog.entity.codes.PersonDocumentTypeCodes;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrantBaseDocument.EnrEntrantBaseDocumentManager;
import ru.tandemservice.unienr14.entrant.bo.EnrOlympiadDiploma.ui.AddEdit.EnrOlympiadDiplomaAddEdit;
import ru.tandemservice.unienr14.entrant.bo.EnrOlympiadDiploma.ui.Pub.EnrOlympiadDiplomaPub;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma;

/**
 * @author nvankov
 * @since 30.11.2016
 */
@Configuration
public class PersonDocumentExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private PersonDocumentManager _personDocumentManager;

    @Bean
    public ItemListExtension<PersonDocumentContext> documentContext()
    {
        return this.itemListExtension(this._personDocumentManager.documentContext())
                .add(PersonDocumentUtil.key(EnrEntrant.class), new PersonDocumentContext(PersonDocumentUtil.key(EnrEntrant.class), "Абитуриент", true).manager(EnrEntrantBaseDocumentManager.instance())).create();
    }

    @Bean
    public ItemListExtension<PersonDocumentTypeConfig<? extends PersonDocument>> documentTypeConfigs()
    {
        return this.itemListExtension(this._personDocumentManager.documentTypeConfigs())
                .add(PersonDocumentTypeCodes.OLYMP_DIPLOMA, new PersonDocumentTypeConfig<EnrOlympiadDiploma>(EnrOlympiadDiploma.class, EnrOlympiadDiplomaPub.class, EnrOlympiadDiplomaAddEdit.class)
                        .unmodifiableType()
                        .formatter(new IFormatter<EnrOlympiadDiploma>()
                        {
                            @Override
                            public String format(EnrOlympiadDiploma source)
                            {
                                return source.getInfoString();
                            }
                        })
                )
                .create();
    }

    @Bean
    public ItemListExtension<PersonDocumentTypeSettingsConfig> documentTypeSettingsConfig()
    {
        return this.itemListExtension(this._personDocumentManager.documentTypeSettingsConfigs())
                .add(PersonDocumentUtil.key(EnrEntrant.class, PersonDocumentTypeCodes.ORPHAN),
                        new PersonDocumentTypeSettingsConfig(PersonDocumentUtil.key(EnrEntrant.class, PersonDocumentTypeCodes.ORPHAN),
                                PersonDocumentPropertySetCreator.get(PersonDocument.class, PersonDocumentUtil.DEFAULT_PERSON_DOCUMENT_PROPERTIES)
                                        .unmodifiableUsedRequiredProp(PersonDocument.L_DOCUMENT_CATEGORY)
                                        .create())
                                .viewAndLink(true)
                                .unmodifiableViewAndLink(true)
                                .createAndEdit(true)
                                .unmodifiableCreateAndEdit(true))
                .add(PersonDocumentUtil.key(EnrEntrant.class, PersonDocumentTypeCodes.SPORTS_DIPLOMA),
                        new PersonDocumentTypeSettingsConfig(PersonDocumentUtil.key(EnrEntrant.class, PersonDocumentTypeCodes.SPORTS_DIPLOMA),
                                PersonDocumentPropertySetCreator.get(PersonDocument.class, PersonDocumentUtil.DEFAULT_PERSON_DOCUMENT_PROPERTIES)
                                        .unmodifiableUsedRequiredProp(PersonDocument.L_DOCUMENT_CATEGORY)
                                        .create())
                                .viewAndLink(true)
                                .unmodifiableViewAndLink(true)
                                .createAndEdit(true)
                                .unmodifiableCreateAndEdit(true))
                .add(PersonDocumentUtil.key(EnrEntrant.class, PersonDocumentTypeCodes.CAN_RECEIVE_EDUCATION_CERT),
                        new PersonDocumentTypeSettingsConfig(PersonDocumentUtil.key(EnrEntrant.class, PersonDocumentTypeCodes.CAN_RECEIVE_EDUCATION_CERT),
                                PersonDocumentPropertySetCreator.get(PersonDocument.class, PersonDocumentUtil.DEFAULT_PERSON_DOCUMENT_PROPERTIES)
                                        .usedUnmodifiableRequiredProp(PersonDocument.P_NUMBER)
                                        .usedUnmodifiableRequiredProp(PersonDocument.P_ISSUANCE_DATE)
                                        .create())
                                .viewAndLink(true)
                                .unmodifiableViewAndLink(true)
                                .createAndEdit(true)
                                .unmodifiableCreateAndEdit(true))
                .add(PersonDocumentUtil.key(EnrEntrant.class, PersonDocumentTypeCodes.OLYMP_DIPLOMA),
                        new PersonDocumentTypeSettingsConfig(PersonDocumentUtil.key(EnrEntrant.class, PersonDocumentTypeCodes.OLYMP_DIPLOMA), PersonDocumentUtil.DEFAULT_PERSON_DOCUMENT_PROPERTIES)
                                .viewAndLink(true)
                                .unmodifiableViewAndLink(true)
                                .createAndEdit(true)
                                .unmodifiableCreateAndEdit(true))
                .add(PersonDocumentUtil.key(EnrEntrant.class, PersonDocumentTypeCodes.DISABLED_CERT),
                        new PersonDocumentTypeSettingsConfig(PersonDocumentUtil.key(EnrEntrant.class, PersonDocumentTypeCodes.DISABLED_CERT),
                                PersonDocumentPropertySetCreator.get(PersonDocument.class, PersonDocumentUtil.DEFAULT_PERSON_DOCUMENT_PROPERTIES)
                                        .unmodifiableUsedRequiredProp(PersonDocument.L_DOCUMENT_CATEGORY)
                                        .usedUnmodifiableRequiredProp(PersonDocument.P_SERIA)
                                        .usedUnmodifiableRequiredProp(PersonDocument.P_NUMBER)
                                        .create())
                                .viewAndLink(true)
                                .unmodifiableViewAndLink(true)
                                .createAndEdit(true)
                                .unmodifiableCreateAndEdit(true))
                .add(PersonDocumentUtil.key(EnrEntrant.class, PersonDocumentTypeCodes.PSY_CERT),
                        new PersonDocumentTypeSettingsConfig(PersonDocumentUtil.key(EnrEntrant.class, PersonDocumentTypeCodes.PSY_CERT),
                                PersonDocumentPropertySetCreator.get(PersonDocument.class, PersonDocumentUtil.DEFAULT_PERSON_DOCUMENT_PROPERTIES)
                                        .usedUnmodifiableRequiredProp(PersonDocument.P_NUMBER)
                                        .create())
                                .viewAndLink(true)
                                .unmodifiableViewAndLink(true)
                                .createAndEdit(true)
                                .unmodifiableCreateAndEdit(true))
                .add(PersonDocumentUtil.key(EnrEntrant.class, PersonDocumentTypeCodes.COMBAT_VETERANS),
                        new PersonDocumentTypeSettingsConfig(PersonDocumentUtil.key(EnrEntrant.class, PersonDocumentTypeCodes.COMBAT_VETERANS),
                                PersonDocumentPropertySetCreator.get(PersonDocument.class, PersonDocumentUtil.DEFAULT_PERSON_DOCUMENT_PROPERTIES)
                                        .unmodifiableUsedRequiredProp(PersonDocument.L_DOCUMENT_CATEGORY)
                                        .create())
                                .viewAndLink(true)
                                .unmodifiableViewAndLink(true)
                                .createAndEdit(true)
                                .unmodifiableCreateAndEdit(true))
                .add(PersonDocumentUtil.key(EnrEntrant.class, PersonDocumentTypeCodes.BELONGING_NATION),
                        new PersonDocumentTypeSettingsConfig(PersonDocumentUtil.key(EnrEntrant.class, PersonDocumentTypeCodes.BELONGING_NATION),
                                PersonDocumentPropertySetCreator.get(PersonDocument.class, PersonDocumentUtil.DEFAULT_PERSON_DOCUMENT_PROPERTIES)
                                        .unmodifiableUsedRequiredProp(PersonDocument.L_DOCUMENT_CATEGORY)
                                        .create())
                                .viewAndLink(true)
                                .unmodifiableViewAndLink(true)
                                .createAndEdit(true)
                                .unmodifiableCreateAndEdit(true))
                .create();
    }
}
