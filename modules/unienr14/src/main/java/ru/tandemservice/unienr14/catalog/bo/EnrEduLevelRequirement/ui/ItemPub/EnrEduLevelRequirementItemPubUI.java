/* $Id$ */
package ru.tandemservice.unienr14.catalog.bo.EnrEduLevelRequirement.ui.ItemPub;

import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.ui.DynamicItemPub.CatalogDynamicItemPubUI;

/**
 * @author Nikolay Fedorovskih
 * @since 11.03.2015
 */
public class EnrEduLevelRequirementItemPubUI extends CatalogDynamicItemPubUI
{
    public static final String EDU_LEVEL_REQUIREMENT_ID_FILTER_PROPERTY = "itemId";

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EDU_LEVEL_REQUIREMENT_ID_FILTER_PROPERTY, getCatalogItem().getId());
    }
}