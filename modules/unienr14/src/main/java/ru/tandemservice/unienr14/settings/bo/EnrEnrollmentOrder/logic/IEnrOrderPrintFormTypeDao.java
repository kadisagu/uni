/**
 *$Id: IEnrOrderPrintFormTypeDao.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentOrder.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;

/**
 * @author Alexander Shaburov
 * @since 05.07.13
 */
public interface IEnrOrderPrintFormTypeDao extends INeedPersistenceSupport
{
    /**
     * Используется.
     * @param entityId id Типа приказа в приемной кампании
     */
    void doToggleUsed(long entityId);

    /**
     * Отображать основание.
     * @param entityId id Типа приказа в приемной кампании
     */
    void doToggleBasic(long entityId);

    /**
     * Отображать приказываю.
     * @param entityId id Типа приказа в приемной кампании
     */
    void doToggleCommand(long entityId);

    /**
     * Отображать группу.
     * @param entityId id Типа приказа в приемной кампании
     */
    void doToggleGroup(long entityId);

    /**
     * Выбор старосты.
     * @param entityId id Типа приказа в приемной кампании
     */
    void doToggleHeadman(long entityId);

    /**
     * Причина и основание.
     * @param entityId id Типа приказа в приемной кампании
     */
    void doToggleReasonAndBasic(long entityId);
}
