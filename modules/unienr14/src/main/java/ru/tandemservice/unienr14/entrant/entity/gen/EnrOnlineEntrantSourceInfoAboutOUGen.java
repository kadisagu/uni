package ru.tandemservice.unienr14.entrant.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unienr14.catalog.entity.EnrSourceInfoAboutUniversity;
import ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantSourceInfoAboutOU;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Информация об ОУ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrOnlineEntrantSourceInfoAboutOUGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantSourceInfoAboutOU";
    public static final String ENTITY_NAME = "enrOnlineEntrantSourceInfoAboutOU";
    public static final int VERSION_HASH = 401828593;
    private static IEntityMeta ENTITY_META;

    public static final String L_SOURCE_INFO = "sourceInfo";
    public static final String L_ONLINE_ENTRANT = "onlineEntrant";

    private EnrSourceInfoAboutUniversity _sourceInfo;     // ЕГЭ
    private EnrOnlineEntrant _onlineEntrant;     // Абитуриент

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return ЕГЭ. Свойство не может быть null.
     */
    @NotNull
    public EnrSourceInfoAboutUniversity getSourceInfo()
    {
        return _sourceInfo;
    }

    /**
     * @param sourceInfo ЕГЭ. Свойство не может быть null.
     */
    public void setSourceInfo(EnrSourceInfoAboutUniversity sourceInfo)
    {
        dirty(_sourceInfo, sourceInfo);
        _sourceInfo = sourceInfo;
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     */
    @NotNull
    public EnrOnlineEntrant getOnlineEntrant()
    {
        return _onlineEntrant;
    }

    /**
     * @param onlineEntrant Абитуриент. Свойство не может быть null.
     */
    public void setOnlineEntrant(EnrOnlineEntrant onlineEntrant)
    {
        dirty(_onlineEntrant, onlineEntrant);
        _onlineEntrant = onlineEntrant;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrOnlineEntrantSourceInfoAboutOUGen)
        {
            setSourceInfo(((EnrOnlineEntrantSourceInfoAboutOU)another).getSourceInfo());
            setOnlineEntrant(((EnrOnlineEntrantSourceInfoAboutOU)another).getOnlineEntrant());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrOnlineEntrantSourceInfoAboutOUGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrOnlineEntrantSourceInfoAboutOU.class;
        }

        public T newInstance()
        {
            return (T) new EnrOnlineEntrantSourceInfoAboutOU();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "sourceInfo":
                    return obj.getSourceInfo();
                case "onlineEntrant":
                    return obj.getOnlineEntrant();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "sourceInfo":
                    obj.setSourceInfo((EnrSourceInfoAboutUniversity) value);
                    return;
                case "onlineEntrant":
                    obj.setOnlineEntrant((EnrOnlineEntrant) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "sourceInfo":
                        return true;
                case "onlineEntrant":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "sourceInfo":
                    return true;
                case "onlineEntrant":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "sourceInfo":
                    return EnrSourceInfoAboutUniversity.class;
                case "onlineEntrant":
                    return EnrOnlineEntrant.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrOnlineEntrantSourceInfoAboutOU> _dslPath = new Path<EnrOnlineEntrantSourceInfoAboutOU>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrOnlineEntrantSourceInfoAboutOU");
    }
            

    /**
     * @return ЕГЭ. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantSourceInfoAboutOU#getSourceInfo()
     */
    public static EnrSourceInfoAboutUniversity.Path<EnrSourceInfoAboutUniversity> sourceInfo()
    {
        return _dslPath.sourceInfo();
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantSourceInfoAboutOU#getOnlineEntrant()
     */
    public static EnrOnlineEntrant.Path<EnrOnlineEntrant> onlineEntrant()
    {
        return _dslPath.onlineEntrant();
    }

    public static class Path<E extends EnrOnlineEntrantSourceInfoAboutOU> extends EntityPath<E>
    {
        private EnrSourceInfoAboutUniversity.Path<EnrSourceInfoAboutUniversity> _sourceInfo;
        private EnrOnlineEntrant.Path<EnrOnlineEntrant> _onlineEntrant;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return ЕГЭ. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantSourceInfoAboutOU#getSourceInfo()
     */
        public EnrSourceInfoAboutUniversity.Path<EnrSourceInfoAboutUniversity> sourceInfo()
        {
            if(_sourceInfo == null )
                _sourceInfo = new EnrSourceInfoAboutUniversity.Path<EnrSourceInfoAboutUniversity>(L_SOURCE_INFO, this);
            return _sourceInfo;
        }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantSourceInfoAboutOU#getOnlineEntrant()
     */
        public EnrOnlineEntrant.Path<EnrOnlineEntrant> onlineEntrant()
        {
            if(_onlineEntrant == null )
                _onlineEntrant = new EnrOnlineEntrant.Path<EnrOnlineEntrant>(L_ONLINE_ENTRANT, this);
            return _onlineEntrant;
        }

        public Class getEntityClass()
        {
            return EnrOnlineEntrantSourceInfoAboutOU.class;
        }

        public String getEntityName()
        {
            return "enrOnlineEntrantSourceInfoAboutOU";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
