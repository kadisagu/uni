package ru.tandemservice.unienr14.rating.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.competition.entity.EnrExamVariant;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выбранное вступительное испытание (ВВИ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrChosenEntranceExamGen extends EntityBase
 implements INaturalIdentifiable<EnrChosenEntranceExamGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam";
    public static final String ENTITY_NAME = "enrChosenEntranceExam";
    public static final int VERSION_HASH = 92514391;
    private static IEntityMeta ENTITY_META;

    public static final String L_REQUESTED_COMPETITION = "requestedCompetition";
    public static final String L_DISCIPLINE = "discipline";
    public static final String L_MAX_MARK_FORM = "maxMarkForm";
    public static final String P_MAX_MARK_FORM_TIMESTAMP = "maxMarkFormTimestamp";
    public static final String L_ACTUAL_EXAM = "actualExam";
    public static final String P_STATUS_PASS_FORMS_CORRECT = "statusPassFormsCorrect";
    public static final String P_STATUS_MAX_MARK_COMPLETE = "statusMaxMarkComplete";

    private EnrRequestedCompetition _requestedCompetition;     // Выбранный конкурс
    private EnrCampaignDiscipline _discipline;     // Дисциплина ПК
    private EnrChosenEntranceExamForm _maxMarkForm;     // Источник балла (ВВИ-ф)
    private Date _maxMarkFormTimestamp;     // Дата изменения источника балла (ВВИ-ф)
    private EnrExamVariant _actualExam;     // Настройка ВИ для конкурса, соотв. данному ВВИ
    private boolean _statusPassFormsCorrect = false;     // Набор выбранных форм сдачи (ВВИ-ф) соответствует настройке ВИ для конкурса
    private boolean _statusMaxMarkComplete = false;     // Сданы все выбранные актуальные формы сдачи (ВВИ-ф)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выбранный конкурс. Свойство не может быть null.
     */
    @NotNull
    public EnrRequestedCompetition getRequestedCompetition()
    {
        return _requestedCompetition;
    }

    /**
     * @param requestedCompetition Выбранный конкурс. Свойство не может быть null.
     */
    public void setRequestedCompetition(EnrRequestedCompetition requestedCompetition)
    {
        dirty(_requestedCompetition, requestedCompetition);
        _requestedCompetition = requestedCompetition;
    }

    /**
     * @return Дисциплина ПК. Свойство не может быть null.
     */
    @NotNull
    public EnrCampaignDiscipline getDiscipline()
    {
        return _discipline;
    }

    /**
     * @param discipline Дисциплина ПК. Свойство не может быть null.
     */
    public void setDiscipline(EnrCampaignDiscipline discipline)
    {
        dirty(_discipline, discipline);
        _discipline = discipline;
    }

    /**
     * ВВИ-ф (среди актуальных), на которой достигнут максимальный балл. Служит источником для рейтинга.
     * Обновляется демоном IEnrRatingDaemonDao#doChooseSource (выбор среди связанных ВВИ-ф).
     *
     * @return Источник балла (ВВИ-ф).
     */
    public EnrChosenEntranceExamForm getMaxMarkForm()
    {
        return _maxMarkForm;
    }

    /**
     * @param maxMarkForm Источник балла (ВВИ-ф).
     */
    public void setMaxMarkForm(EnrChosenEntranceExamForm maxMarkForm)
    {
        dirty(_maxMarkForm, maxMarkForm);
        _maxMarkForm = maxMarkForm;
    }

    /**
     * Дата изменения значения атрибута maxMarkForm.
     * Обновляется демоном IEnrRatingDaemonDao#doChooseSource (выбор среди связанных ВВИ-ф) при изменении значения в maxMarkForm.
     *
     * @return Дата изменения источника балла (ВВИ-ф).
     */
    public Date getMaxMarkFormTimestamp()
    {
        return _maxMarkFormTimestamp;
    }

    /**
     * @param maxMarkFormTimestamp Дата изменения источника балла (ВВИ-ф).
     */
    public void setMaxMarkFormTimestamp(Date maxMarkFormTimestamp)
    {
        dirty(_maxMarkFormTimestamp, maxMarkFormTimestamp);
        _maxMarkFormTimestamp = maxMarkFormTimestamp;
    }

    /**
     * Ссылка на подходящую настройку ВИ для конкурса (через дисциплину).
     * Обновляется демоном IEnrRatingDaemonDao#doRefreshChosenEntranceExams.
     *
     * @return Настройка ВИ для конкурса, соотв. данному ВВИ.
     */
    public EnrExamVariant getActualExam()
    {
        return _actualExam;
    }

    /**
     * @param actualExam Настройка ВИ для конкурса, соотв. данному ВВИ.
     */
    public void setActualExam(EnrExamVariant actualExam)
    {
        dirty(_actualExam, actualExam);
        _actualExam = actualExam;
    }

    /**
     * true, если все ВВИ-ф соответствуют формам из настроенной ВИ: все actualExamPassForm заполнены (покрытие проверять не требуется, дублей здесь быть не может).
     * Обновляется демоном IEnrRatingDaemonDao#doRefreshChosenEntranceExams.
     *
     * @return Набор выбранных форм сдачи (ВВИ-ф) соответствует настройке ВИ для конкурса. Свойство не может быть null.
     */
    @NotNull
    public boolean isStatusPassFormsCorrect()
    {
        return _statusPassFormsCorrect;
    }

    /**
     * @param statusPassFormsCorrect Набор выбранных форм сдачи (ВВИ-ф) соответствует настройке ВИ для конкурса. Свойство не может быть null.
     */
    public void setStatusPassFormsCorrect(boolean statusPassFormsCorrect)
    {
        dirty(_statusPassFormsCorrect, statusPassFormsCorrect);
        _statusPassFormsCorrect = statusPassFormsCorrect;
    }

    /**
     * true, если для всех актуальных ВВИ-ф (соответствует формам настроенных ВИ) указан источник балла.
     * Обновляется демоном IEnrRatingDaemonDao#doChooseSource.
     *
     * @return Сданы все выбранные актуальные формы сдачи (ВВИ-ф). Свойство не может быть null.
     */
    @NotNull
    public boolean isStatusMaxMarkComplete()
    {
        return _statusMaxMarkComplete;
    }

    /**
     * @param statusMaxMarkComplete Сданы все выбранные актуальные формы сдачи (ВВИ-ф). Свойство не может быть null.
     */
    public void setStatusMaxMarkComplete(boolean statusMaxMarkComplete)
    {
        dirty(_statusMaxMarkComplete, statusMaxMarkComplete);
        _statusMaxMarkComplete = statusMaxMarkComplete;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrChosenEntranceExamGen)
        {
            if (withNaturalIdProperties)
            {
                setRequestedCompetition(((EnrChosenEntranceExam)another).getRequestedCompetition());
                setDiscipline(((EnrChosenEntranceExam)another).getDiscipline());
            }
            setMaxMarkForm(((EnrChosenEntranceExam)another).getMaxMarkForm());
            setMaxMarkFormTimestamp(((EnrChosenEntranceExam)another).getMaxMarkFormTimestamp());
            setActualExam(((EnrChosenEntranceExam)another).getActualExam());
            setStatusPassFormsCorrect(((EnrChosenEntranceExam)another).isStatusPassFormsCorrect());
            setStatusMaxMarkComplete(((EnrChosenEntranceExam)another).isStatusMaxMarkComplete());
        }
    }

    public INaturalId<EnrChosenEntranceExamGen> getNaturalId()
    {
        return new NaturalId(getRequestedCompetition(), getDiscipline());
    }

    public static class NaturalId extends NaturalIdBase<EnrChosenEntranceExamGen>
    {
        private static final String PROXY_NAME = "EnrChosenEntranceExamNaturalProxy";

        private Long _requestedCompetition;
        private Long _discipline;

        public NaturalId()
        {}

        public NaturalId(EnrRequestedCompetition requestedCompetition, EnrCampaignDiscipline discipline)
        {
            _requestedCompetition = ((IEntity) requestedCompetition).getId();
            _discipline = ((IEntity) discipline).getId();
        }

        public Long getRequestedCompetition()
        {
            return _requestedCompetition;
        }

        public void setRequestedCompetition(Long requestedCompetition)
        {
            _requestedCompetition = requestedCompetition;
        }

        public Long getDiscipline()
        {
            return _discipline;
        }

        public void setDiscipline(Long discipline)
        {
            _discipline = discipline;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrChosenEntranceExamGen.NaturalId) ) return false;

            EnrChosenEntranceExamGen.NaturalId that = (NaturalId) o;

            if( !equals(getRequestedCompetition(), that.getRequestedCompetition()) ) return false;
            if( !equals(getDiscipline(), that.getDiscipline()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getRequestedCompetition());
            result = hashCode(result, getDiscipline());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getRequestedCompetition());
            sb.append("/");
            sb.append(getDiscipline());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrChosenEntranceExamGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrChosenEntranceExam.class;
        }

        public T newInstance()
        {
            return (T) new EnrChosenEntranceExam();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "requestedCompetition":
                    return obj.getRequestedCompetition();
                case "discipline":
                    return obj.getDiscipline();
                case "maxMarkForm":
                    return obj.getMaxMarkForm();
                case "maxMarkFormTimestamp":
                    return obj.getMaxMarkFormTimestamp();
                case "actualExam":
                    return obj.getActualExam();
                case "statusPassFormsCorrect":
                    return obj.isStatusPassFormsCorrect();
                case "statusMaxMarkComplete":
                    return obj.isStatusMaxMarkComplete();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "requestedCompetition":
                    obj.setRequestedCompetition((EnrRequestedCompetition) value);
                    return;
                case "discipline":
                    obj.setDiscipline((EnrCampaignDiscipline) value);
                    return;
                case "maxMarkForm":
                    obj.setMaxMarkForm((EnrChosenEntranceExamForm) value);
                    return;
                case "maxMarkFormTimestamp":
                    obj.setMaxMarkFormTimestamp((Date) value);
                    return;
                case "actualExam":
                    obj.setActualExam((EnrExamVariant) value);
                    return;
                case "statusPassFormsCorrect":
                    obj.setStatusPassFormsCorrect((Boolean) value);
                    return;
                case "statusMaxMarkComplete":
                    obj.setStatusMaxMarkComplete((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "requestedCompetition":
                        return true;
                case "discipline":
                        return true;
                case "maxMarkForm":
                        return true;
                case "maxMarkFormTimestamp":
                        return true;
                case "actualExam":
                        return true;
                case "statusPassFormsCorrect":
                        return true;
                case "statusMaxMarkComplete":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "requestedCompetition":
                    return true;
                case "discipline":
                    return true;
                case "maxMarkForm":
                    return true;
                case "maxMarkFormTimestamp":
                    return true;
                case "actualExam":
                    return true;
                case "statusPassFormsCorrect":
                    return true;
                case "statusMaxMarkComplete":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "requestedCompetition":
                    return EnrRequestedCompetition.class;
                case "discipline":
                    return EnrCampaignDiscipline.class;
                case "maxMarkForm":
                    return EnrChosenEntranceExamForm.class;
                case "maxMarkFormTimestamp":
                    return Date.class;
                case "actualExam":
                    return EnrExamVariant.class;
                case "statusPassFormsCorrect":
                    return Boolean.class;
                case "statusMaxMarkComplete":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrChosenEntranceExam> _dslPath = new Path<EnrChosenEntranceExam>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrChosenEntranceExam");
    }
            

    /**
     * @return Выбранный конкурс. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam#getRequestedCompetition()
     */
    public static EnrRequestedCompetition.Path<EnrRequestedCompetition> requestedCompetition()
    {
        return _dslPath.requestedCompetition();
    }

    /**
     * @return Дисциплина ПК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam#getDiscipline()
     */
    public static EnrCampaignDiscipline.Path<EnrCampaignDiscipline> discipline()
    {
        return _dslPath.discipline();
    }

    /**
     * ВВИ-ф (среди актуальных), на которой достигнут максимальный балл. Служит источником для рейтинга.
     * Обновляется демоном IEnrRatingDaemonDao#doChooseSource (выбор среди связанных ВВИ-ф).
     *
     * @return Источник балла (ВВИ-ф).
     * @see ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam#getMaxMarkForm()
     */
    public static EnrChosenEntranceExamForm.Path<EnrChosenEntranceExamForm> maxMarkForm()
    {
        return _dslPath.maxMarkForm();
    }

    /**
     * Дата изменения значения атрибута maxMarkForm.
     * Обновляется демоном IEnrRatingDaemonDao#doChooseSource (выбор среди связанных ВВИ-ф) при изменении значения в maxMarkForm.
     *
     * @return Дата изменения источника балла (ВВИ-ф).
     * @see ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam#getMaxMarkFormTimestamp()
     */
    public static PropertyPath<Date> maxMarkFormTimestamp()
    {
        return _dslPath.maxMarkFormTimestamp();
    }

    /**
     * Ссылка на подходящую настройку ВИ для конкурса (через дисциплину).
     * Обновляется демоном IEnrRatingDaemonDao#doRefreshChosenEntranceExams.
     *
     * @return Настройка ВИ для конкурса, соотв. данному ВВИ.
     * @see ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam#getActualExam()
     */
    public static EnrExamVariant.Path<EnrExamVariant> actualExam()
    {
        return _dslPath.actualExam();
    }

    /**
     * true, если все ВВИ-ф соответствуют формам из настроенной ВИ: все actualExamPassForm заполнены (покрытие проверять не требуется, дублей здесь быть не может).
     * Обновляется демоном IEnrRatingDaemonDao#doRefreshChosenEntranceExams.
     *
     * @return Набор выбранных форм сдачи (ВВИ-ф) соответствует настройке ВИ для конкурса. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam#isStatusPassFormsCorrect()
     */
    public static PropertyPath<Boolean> statusPassFormsCorrect()
    {
        return _dslPath.statusPassFormsCorrect();
    }

    /**
     * true, если для всех актуальных ВВИ-ф (соответствует формам настроенных ВИ) указан источник балла.
     * Обновляется демоном IEnrRatingDaemonDao#doChooseSource.
     *
     * @return Сданы все выбранные актуальные формы сдачи (ВВИ-ф). Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam#isStatusMaxMarkComplete()
     */
    public static PropertyPath<Boolean> statusMaxMarkComplete()
    {
        return _dslPath.statusMaxMarkComplete();
    }

    public static class Path<E extends EnrChosenEntranceExam> extends EntityPath<E>
    {
        private EnrRequestedCompetition.Path<EnrRequestedCompetition> _requestedCompetition;
        private EnrCampaignDiscipline.Path<EnrCampaignDiscipline> _discipline;
        private EnrChosenEntranceExamForm.Path<EnrChosenEntranceExamForm> _maxMarkForm;
        private PropertyPath<Date> _maxMarkFormTimestamp;
        private EnrExamVariant.Path<EnrExamVariant> _actualExam;
        private PropertyPath<Boolean> _statusPassFormsCorrect;
        private PropertyPath<Boolean> _statusMaxMarkComplete;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выбранный конкурс. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam#getRequestedCompetition()
     */
        public EnrRequestedCompetition.Path<EnrRequestedCompetition> requestedCompetition()
        {
            if(_requestedCompetition == null )
                _requestedCompetition = new EnrRequestedCompetition.Path<EnrRequestedCompetition>(L_REQUESTED_COMPETITION, this);
            return _requestedCompetition;
        }

    /**
     * @return Дисциплина ПК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam#getDiscipline()
     */
        public EnrCampaignDiscipline.Path<EnrCampaignDiscipline> discipline()
        {
            if(_discipline == null )
                _discipline = new EnrCampaignDiscipline.Path<EnrCampaignDiscipline>(L_DISCIPLINE, this);
            return _discipline;
        }

    /**
     * ВВИ-ф (среди актуальных), на которой достигнут максимальный балл. Служит источником для рейтинга.
     * Обновляется демоном IEnrRatingDaemonDao#doChooseSource (выбор среди связанных ВВИ-ф).
     *
     * @return Источник балла (ВВИ-ф).
     * @see ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam#getMaxMarkForm()
     */
        public EnrChosenEntranceExamForm.Path<EnrChosenEntranceExamForm> maxMarkForm()
        {
            if(_maxMarkForm == null )
                _maxMarkForm = new EnrChosenEntranceExamForm.Path<EnrChosenEntranceExamForm>(L_MAX_MARK_FORM, this);
            return _maxMarkForm;
        }

    /**
     * Дата изменения значения атрибута maxMarkForm.
     * Обновляется демоном IEnrRatingDaemonDao#doChooseSource (выбор среди связанных ВВИ-ф) при изменении значения в maxMarkForm.
     *
     * @return Дата изменения источника балла (ВВИ-ф).
     * @see ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam#getMaxMarkFormTimestamp()
     */
        public PropertyPath<Date> maxMarkFormTimestamp()
        {
            if(_maxMarkFormTimestamp == null )
                _maxMarkFormTimestamp = new PropertyPath<Date>(EnrChosenEntranceExamGen.P_MAX_MARK_FORM_TIMESTAMP, this);
            return _maxMarkFormTimestamp;
        }

    /**
     * Ссылка на подходящую настройку ВИ для конкурса (через дисциплину).
     * Обновляется демоном IEnrRatingDaemonDao#doRefreshChosenEntranceExams.
     *
     * @return Настройка ВИ для конкурса, соотв. данному ВВИ.
     * @see ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam#getActualExam()
     */
        public EnrExamVariant.Path<EnrExamVariant> actualExam()
        {
            if(_actualExam == null )
                _actualExam = new EnrExamVariant.Path<EnrExamVariant>(L_ACTUAL_EXAM, this);
            return _actualExam;
        }

    /**
     * true, если все ВВИ-ф соответствуют формам из настроенной ВИ: все actualExamPassForm заполнены (покрытие проверять не требуется, дублей здесь быть не может).
     * Обновляется демоном IEnrRatingDaemonDao#doRefreshChosenEntranceExams.
     *
     * @return Набор выбранных форм сдачи (ВВИ-ф) соответствует настройке ВИ для конкурса. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam#isStatusPassFormsCorrect()
     */
        public PropertyPath<Boolean> statusPassFormsCorrect()
        {
            if(_statusPassFormsCorrect == null )
                _statusPassFormsCorrect = new PropertyPath<Boolean>(EnrChosenEntranceExamGen.P_STATUS_PASS_FORMS_CORRECT, this);
            return _statusPassFormsCorrect;
        }

    /**
     * true, если для всех актуальных ВВИ-ф (соответствует формам настроенных ВИ) указан источник балла.
     * Обновляется демоном IEnrRatingDaemonDao#doChooseSource.
     *
     * @return Сданы все выбранные актуальные формы сдачи (ВВИ-ф). Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam#isStatusMaxMarkComplete()
     */
        public PropertyPath<Boolean> statusMaxMarkComplete()
        {
            if(_statusMaxMarkComplete == null )
                _statusMaxMarkComplete = new PropertyPath<Boolean>(EnrChosenEntranceExamGen.P_STATUS_MAX_MARK_COMPLETE, this);
            return _statusMaxMarkComplete;
        }

        public Class getEntityClass()
        {
            return EnrChosenEntranceExam.class;
        }

        public String getEntityName()
        {
            return "enrChosenEntranceExam";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
