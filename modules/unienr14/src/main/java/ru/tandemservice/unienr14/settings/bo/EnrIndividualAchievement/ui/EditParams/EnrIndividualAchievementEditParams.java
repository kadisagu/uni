/* $Id$ */
package ru.tandemservice.unienr14.settings.bo.EnrIndividualAchievement.ui.EditParams;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author nvankov
 * @since 4/11/14
 */
@Configuration
public class EnrIndividualAchievementEditParams extends BusinessComponentManager
{
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .create();
    }
}



    