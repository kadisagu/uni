package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x2_27to28 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
        {
            new ScriptDependency("org.tandemframework", "1.6.15"),
            new ScriptDependency("org.tandemframework.shared", "1.6.2")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrEnrollmentCampaignSettings

        // создано обязательное свойство reenrollByPriorityEnabled
        {
            // создать колонку
            tool.createColumn("enr14_campaign_settings_t", new DBColumn("reenrollbypriorityenabled_p", DBType.BOOLEAN));

            // задать значение по умолчанию
            java.lang.Boolean defaultReenrollByPriorityEnabled = Boolean.TRUE;
            tool.executeUpdate("update enr14_campaign_settings_t set reenrollbypriorityenabled_p=? where reenrollbypriorityenabled_p is null", defaultReenrollByPriorityEnabled);

            // сделать колонку NOT NULL
            tool.setColumnNullable("enr14_campaign_settings_t", "reenrollbypriorityenabled_p", false);

        }


    }
}