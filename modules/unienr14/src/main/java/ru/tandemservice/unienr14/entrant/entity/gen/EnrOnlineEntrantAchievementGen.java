package ru.tandemservice.unienr14.entrant.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantAchievement;
import ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Индивидуальное достижение онлайн-абитуриента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrOnlineEntrantAchievementGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantAchievement";
    public static final String ENTITY_NAME = "enrOnlineEntrantAchievement";
    public static final int VERSION_HASH = 705752097;
    private static IEntityMeta ENTITY_META;

    public static final String L_ACHIEVEMENT_TYPE = "achievementType";
    public static final String L_ONLINE_ENTRANT = "onlineEntrant";

    private EnrEntrantAchievementType _achievementType;     // Индивидуальное достижение
    private EnrOnlineEntrant _onlineEntrant;     // Абитуриент

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Индивидуальное достижение. Свойство не может быть null.
     */
    @NotNull
    public EnrEntrantAchievementType getAchievementType()
    {
        return _achievementType;
    }

    /**
     * @param achievementType Индивидуальное достижение. Свойство не может быть null.
     */
    public void setAchievementType(EnrEntrantAchievementType achievementType)
    {
        dirty(_achievementType, achievementType);
        _achievementType = achievementType;
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     */
    @NotNull
    public EnrOnlineEntrant getOnlineEntrant()
    {
        return _onlineEntrant;
    }

    /**
     * @param onlineEntrant Абитуриент. Свойство не может быть null.
     */
    public void setOnlineEntrant(EnrOnlineEntrant onlineEntrant)
    {
        dirty(_onlineEntrant, onlineEntrant);
        _onlineEntrant = onlineEntrant;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrOnlineEntrantAchievementGen)
        {
            setAchievementType(((EnrOnlineEntrantAchievement)another).getAchievementType());
            setOnlineEntrant(((EnrOnlineEntrantAchievement)another).getOnlineEntrant());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrOnlineEntrantAchievementGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrOnlineEntrantAchievement.class;
        }

        public T newInstance()
        {
            return (T) new EnrOnlineEntrantAchievement();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "achievementType":
                    return obj.getAchievementType();
                case "onlineEntrant":
                    return obj.getOnlineEntrant();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "achievementType":
                    obj.setAchievementType((EnrEntrantAchievementType) value);
                    return;
                case "onlineEntrant":
                    obj.setOnlineEntrant((EnrOnlineEntrant) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "achievementType":
                        return true;
                case "onlineEntrant":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "achievementType":
                    return true;
                case "onlineEntrant":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "achievementType":
                    return EnrEntrantAchievementType.class;
                case "onlineEntrant":
                    return EnrOnlineEntrant.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrOnlineEntrantAchievement> _dslPath = new Path<EnrOnlineEntrantAchievement>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrOnlineEntrantAchievement");
    }
            

    /**
     * @return Индивидуальное достижение. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantAchievement#getAchievementType()
     */
    public static EnrEntrantAchievementType.Path<EnrEntrantAchievementType> achievementType()
    {
        return _dslPath.achievementType();
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantAchievement#getOnlineEntrant()
     */
    public static EnrOnlineEntrant.Path<EnrOnlineEntrant> onlineEntrant()
    {
        return _dslPath.onlineEntrant();
    }

    public static class Path<E extends EnrOnlineEntrantAchievement> extends EntityPath<E>
    {
        private EnrEntrantAchievementType.Path<EnrEntrantAchievementType> _achievementType;
        private EnrOnlineEntrant.Path<EnrOnlineEntrant> _onlineEntrant;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Индивидуальное достижение. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantAchievement#getAchievementType()
     */
        public EnrEntrantAchievementType.Path<EnrEntrantAchievementType> achievementType()
        {
            if(_achievementType == null )
                _achievementType = new EnrEntrantAchievementType.Path<EnrEntrantAchievementType>(L_ACHIEVEMENT_TYPE, this);
            return _achievementType;
        }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantAchievement#getOnlineEntrant()
     */
        public EnrOnlineEntrant.Path<EnrOnlineEntrant> onlineEntrant()
        {
            if(_onlineEntrant == null )
                _onlineEntrant = new EnrOnlineEntrant.Path<EnrOnlineEntrant>(L_ONLINE_ENTRANT, this);
            return _onlineEntrant;
        }

        public Class getEntityClass()
        {
            return EnrOnlineEntrantAchievement.class;
        }

        public String getEntityName()
        {
            return "enrOnlineEntrantAchievement";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
