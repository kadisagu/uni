/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic;

import java.util.Date;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;

import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentStepKind;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

/**
 * @author oleyba
 * @since 12/17/13
 */
public interface IEnrEnrollmentStepDao extends INeedPersistenceSupport
{
    /**
     * Создает шаг зачисления, заполняет конкурсные списки
     * @param campaign приемная кампания
     * @param enrollmentDate дата зачисления
     * @param requestType вид заявления
     * @param stepKind
     * @param percentageAsLong
     */
    void createNewStep(EnrEnrollmentCampaign campaign, Date enrollmentDate, EnrRequestType requestType, EnrEnrollmentStepKind stepKind, Long percentageAsLong);

    // Обновить данные об оригиналах и согласии на зачисление
    void doRefreshData(EnrEnrollmentStep step);

    void doMoveToNextState(EnrEnrollmentStep step);

    void doMoveToPrevState(EnrEnrollmentStep step);

    void doDelete(Long stepId);

    void doAutoRecommend(EnrEnrollmentStep step);

    void doAllowManualRecommendation(EnrEnrollmentStep step);

    void doClearRecommendation(EnrEnrollmentStep step);

    void doAutoMarkEnrolled(EnrEnrollmentStep step);

    void doAllowManualMarkEnrolled(EnrEnrollmentStep step);

    void doClearMarkEnrolled(EnrEnrollmentStep step);

    void doRefreshNecessaryXml();

    /**
     * Хак DEV-5696 для ПГУПС (сдается мне, что будут и другие)
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doSelectEnrollmentHack5696(EnrEnrollmentStep step);



}