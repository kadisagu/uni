package ru.tandemservice.unienr14.ws.entrantrating;

import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.DeclinableManager;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.catalog.entity.InflectorVariant;
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import org.tandemframework.shared.sandbox.utils.FastBeanFunction;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramOrientation;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.uniedu.program.entity.EduProgramSecondaryProf;
import ru.tandemservice.unienr14.catalog.entity.*;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEnrollmentStepStateCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantStateCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrExamPassFormCodes;
import ru.tandemservice.unienr14.competition.entity.*;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItem;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.formatter.EnrChosenEntranceExamNumberFormatter;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.EnrExamSetManager;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.logic.IEnrExamSetDao;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm;
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSource;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;
import ru.tandemservice.unienr14.request.entity.*;
import ru.tandemservice.unienr14.settings.bo.EnrTargetAdmission.EnrTargetAdmissionManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementType;
import ru.tandemservice.unienr14.settings.entity.IEnrExamSetElementValue;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Vasily Zhukov
 * @since 29.03.2011
 */
public class EnrEntrantRatingServiceDao extends CommonDAO implements IEnrEntrantRatingServiceDao
{
    public static final String ORIGINAL = "1";
    public static final String COPY = "2";
    public static final String ENROLLED = "3";


    @Override
    public EnrEntrantRatingEnvironmentNode getEntrantRatingTAEnvironmentData(String enrollmentCampaignTitle)
    {
//        try
//        {
//            Thread.sleep(90000);
//        }
//        catch (InterruptedException e)
//        {
//            e.printStackTrace();
//        }
        // загружаем приемную кампанию
        EnrEnrollmentCampaign enrollmentCampaign = get(EnrEnrollmentCampaign.class, EnrEnrollmentCampaign.P_TITLE, enrollmentCampaignTitle);
        if (enrollmentCampaign == null)
            throw new RuntimeException("EnrEnrollmentCampaign with title '" + enrollmentCampaignTitle + "' not found!");

        return getEntrantRatingTAEnvironmentData(enrollmentCampaign);
    }

    @Override
    public EnrEntrantRatingEnvironmentNode getEntrantRatingTAEnvironmentData(String enrollmentCampaignTitle, String enrollmentDirectionId)
    {
//        try
//        {
//            Thread.sleep(90000);
//        }
//        catch (InterruptedException e)
//        {
//            e.printStackTrace();
//        }

        // загружаем приемную кампания
        EnrEnrollmentCampaign enrollmentCampaign = get(EnrEnrollmentCampaign.class, EnrEnrollmentCampaign.P_TITLE, enrollmentCampaignTitle);
        if (enrollmentCampaign == null)
            throw new RuntimeException("EnrEnrollmentCampaign with title '" + enrollmentCampaignTitle + "' not found!");

        // загружаем направление приема
        EnrCompetition enrollmentDirection = getNotNull(Long.parseLong(enrollmentDirectionId));
        if (enrollmentDirection == null)
            throw new RuntimeException("EnrDirection with id '" + enrollmentDirectionId + "' not found!");

        return getEntrantRatingTAEnvironmentData(enrollmentCampaign, enrollmentDirection);
    }

    @Override
    public EnrEntrantRatingEnvironmentNode getEntrantRatingTAEnvironmentData(final EnrEnrollmentCampaign enrollmentCampaign)
    {
        EnrEntrantRatingEnvironmentNode env = new EnrEntrantRatingEnvironmentNode();

        env.enrollmentCampaignTitle = enrollmentCampaign.getTitle();
        env.educationYearTitle = enrollmentCampaign.getEducationYear().getTitle();
        Debug.begin("debug-forming-xml");
        try {
            fillEntrantRatingTAEnvironmentNode(enrollmentCampaign, null, env);
        } catch (Exception e) {
            throw CoreExceptionUtils.getRuntimeException(e);
        } finally {
            Debug.end();
        }

        return env;
    }

    @Override
    public EnrEntrantRatingEnvironmentNode getEntrantRatingTAEnvironmentData(final EnrEnrollmentCampaign enrollmentCampaign, final EnrCompetition competition)
    {
        EnrEntrantRatingEnvironmentNode env = new EnrEntrantRatingEnvironmentNode();

        env.enrollmentCampaignTitle = enrollmentCampaign.getTitle();

        env.educationYearTitle = enrollmentCampaign.getEducationYear().getTitle();

        env.competitionId = competition.getId().toString();
        fillEntrantRatingTAEnvironmentNode(enrollmentCampaign, competition, env);

        return env;
    }

    private void fillEntrantRatingTAEnvironmentNode(EnrEnrollmentCampaign enrollmentCampaign,
                                                    EnrCompetition singleCompetition,
                                                    EnrEntrantRatingEnvironmentNode env)
    {
        // сохраняем все виды целевого приема
        Set<EnrTargetAdmissionKind> usedTaKinds = EnrTargetAdmissionManager.instance().dao().getUsedTaKinds(enrollmentCampaign);
        List<EnrTargetAdmissionKind> allTaKinds = IUniBaseDao.instance.get().getList(EnrTargetAdmissionKind.class);
        Collections.sort(allTaKinds, CommonBaseUtil.PRIORITY_SIMPLE_SAFE_COMPARATOR);
        for (EnrTargetAdmissionKind row : allTaKinds)
            env.targetAdmissionKind.row.add(new EnrEntrantRatingEnvironmentNode.TargetAdmissionKindNode.TargetAdmissionKindRow(row.getTitle(), row.getFullTitle(), row.getCode(), row.getPriority(), usedTaKinds.contains(row)));

        // сохраняем все льготы
        for (EnrBenefitCategory row : DataAccessServices.dao().getList(EnrBenefitCategory.class, EnrBenefitCategory.P_CODE))
            env.benefitCategory.row.add(new EnrEntrantRatingEnvironmentNode.Row(row.getShortTitle(), row.getTitle(), row.getCode()));

        // сохраняем все способы подачи и возврата оригиналов документов
        for (EnrOriginalSubmissionAndReturnWay row : DataAccessServices.dao().getList(EnrOriginalSubmissionAndReturnWay.class, EnrOriginalSubmissionAndReturnWay.P_CODE))
            env.originalSubmissionAndReturnWay.row.add(new EnrEntrantRatingEnvironmentNode.Row(row.getTitle(), row.getCode()));

        List<EnrCompetition> competitionList = singleCompetition == null ? IUniBaseDao.instance.get().getList(EnrCompetition.class, EnrCompetition.programSetOrgUnit().programSet().enrollmentCampaign(), enrollmentCampaign, EnrCompetition.id().s()) : Arrays.asList(singleCompetition);



        Collection<EnrProgramSetBase> programSets = new HashSet<>(Collections2.transform(competitionList, FastBeanFunction.get(EnrCompetition.programSetOrgUnit().programSet())));

        // Индивидуальные достижения
        Map<EnrEntrant, Map<EnrRequestType, List<EnrEntrantAchievement>>> entrantAchievementMap = SafeMap.get(HashMap.class);

        DQLSelectBuilder achievementDQL = new DQLSelectBuilder().fromEntity(EnrEntrantAchievement.class, "ach");
        if (singleCompetition != null) {
            achievementDQL.where(exists(new DQLSelectBuilder()
                    .fromEntity(EnrRequestedCompetition.class, "rc")
                    .where(eq(property("rc", EnrRequestedCompetition.request().entrant()), property("ach", EnrEntrantAchievement.entrant())))
                    .where(eq(property("rc", EnrRequestedCompetition.competition()), value(singleCompetition)))
                    .buildQuery()))
            .where(eq(property("ach", EnrEntrantAchievement.type().achievementKind().requestType()), value(singleCompetition.getRequestType())));
        } else {
            achievementDQL.where(eq(property("ach", EnrEntrantAchievement.entrant().enrollmentCampaign()), value(enrollmentCampaign)));
        }

        for(EnrEntrantAchievement achievement : this.<EnrEntrantAchievement>getList(achievementDQL))
        {
            SafeMap.safeGet(entrantAchievementMap.get(achievement.getEntrant()), achievement.getType().getAchievementKind().getRequestType(), ArrayList.class).add(achievement);
        }

        // данные по наборам ВИ
        Collection<Long> variantIds = new HashSet<>(Collections2.transform(competitionList, FastBeanFunction.get(EnrCompetition.examSetVariant().id())));
        Map<Long,IEnrExamSetDao.IExamSetSettings> examSetSettingsMap = EnrExamSetManager.instance().dao().getExamSetContent(variantIds);

        Map<EnrCompetition, List<EnrRatingItem>> ratingMap = SafeMap.get(ArrayList.class);
        Map<EnrRequestedCompetition, Map<EnrExamVariant, EnrChosenEntranceExam>> markMap = SafeMap.get(HashMap.class);
        Map<EnrRequestedCompetition, EnrEnrollmentExtract> extractMap = new HashMap<>();
        Map<EnrRequestedCompetition, EnrEnrollmentStepItem> recommendationMap = new HashMap<>();

        // Подгружаем в сессию параметры

        // ВВИ-ф
        DQLSelectBuilder fetchExams = new DQLSelectBuilder()
        .fromEntity(EnrChosenEntranceExamForm.class, "vvif")
        .where(eq(property(EnrChosenEntranceExamForm.chosenEntranceExam().requestedCompetition().competition().programSetOrgUnit().programSet().enrollmentCampaign().fromAlias("vvif")), value(enrollmentCampaign)))
        .fetchPath(DQLJoinType.inner, EnrChosenEntranceExamForm.chosenEntranceExam().fromAlias("vvif"), "vvi")
        .joinEntity("vvif", DQLJoinType.inner, EnrEntrantMarkSource.class, "ms", eq(property(EnrEntrantMarkSource.chosenEntranceExamForm().fromAlias("ms")), property("vvif")))
        .fetchPath(DQLJoinType.inner, EnrChosenEntranceExamForm.chosenEntranceExam().discipline().fromAlias("vvif"), "disc");

        fetchExams.createStatement(getSession()).list();

        // Аккредитованные подразделения
        DQLSelectBuilder fetchProgramSets = new DQLSelectBuilder()
        .fromEntity(EnrProgramSetOrgUnit.class, "psou")
        .where(eq(property(EnrProgramSetOrgUnit.programSet().enrollmentCampaign().fromAlias("psou")), value(enrollmentCampaign)))
        .fetchPath(DQLJoinType.inner, EnrProgramSetOrgUnit.programSet().fromAlias("psou"), "progSet")
        .fetchPath(DQLJoinType.inner, EnrProgramSetOrgUnit.programSet().programSubject().fromAlias("psou"), "subj")
        .fetchPath(DQLJoinType.inner, EnrProgramSetOrgUnit.programSet().programForm().fromAlias("psou"), "progForm")
        .fetchPath(DQLJoinType.inner, EnrProgramSetOrgUnit.programSet().programSubject().subjectIndex().programKind().eduLevel().fromAlias("psou"), "eduLevel")
        .fetchPath(DQLJoinType.inner, EnrProgramSetOrgUnit.orgUnit().institutionOrgUnit().orgUnit().fromAlias("psou"), "orgUnit")
        .joinEntity("psou", DQLJoinType.left, EduProgramSpecialization.class, "eps", eq(property(EduProgramSpecialization.programSubject().fromAlias("eps")), property(EnrProgramSetOrgUnit.programSet().programSubject().fromAlias("psou"))));

        fetchProgramSets.createStatement(getSession()).list();

        // Выбранные конкурсы
        DQLSelectBuilder fetchRatings = new DQLSelectBuilder()
        .fromEntity(EnrRequestedCompetition.class, "reqComp")
        .where(eq(property(EnrRequestedCompetition.competition().programSetOrgUnit().programSet().enrollmentCampaign().fromAlias("reqComp")), value(enrollmentCampaign)))
        .fetchPath(DQLJoinType.inner, EnrRequestedCompetition.request().entrant().fromAlias("reqComp"), true);

        DataAccessServices.dao().getList(fetchRatings);

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrRatingItem.class, "ri")
                .fetchPath(DQLJoinType.inner, EnrRatingItem.requestedCompetition().fromAlias("ri"), "rc", true)
                .fetchPath(DQLJoinType.inner, EnrRatingItem.entrant().fromAlias("ri"), "e", true)
                .fetchPath(DQLJoinType.inner, EnrRatingItem.entrant().person().fromAlias("ri"), "p", true)
                .fetchPath(DQLJoinType.inner, EnrRatingItem.entrant().person().identityCard().fromAlias("ri"), "ic", true)
                .fetchPath(DQLJoinType.inner, EnrRatingItem.requestedCompetition().request().eduDocument().fromAlias("ri"), "eduDoc", true)
                .fetchPath(DQLJoinType.inner, EnrRatingItem.requestedCompetition().request().fromAlias("ri"), "rq", true)
                .fetchPath(DQLJoinType.inner, EnrRatingItem.requestedCompetition().request().originalSubmissionWay().fromAlias("ri"), "sw", true);

        if(singleCompetition == null)
        {
            builder.where(eq(property("ri", EnrRatingItem.requestedCompetition().request().entrant().enrollmentCampaign()), value(enrollmentCampaign)));
        }
        else
        {
            builder.where(eq(property("ri", EnrRatingItem.competition()), value(singleCompetition)));
        }

        List<EnrRatingItem> ratingItems = builder.createStatement(getSession()).list();

//        List<EnrRatingItem> ratingItems = singleCompetition == null ?
//            IUniBaseDao.instance.get().getList(EnrRatingItem.class, EnrRatingItem.requestedCompetition().request().entrant().enrollmentCampaign(), enrollmentCampaign)
//            : IUniBaseDao.instance.get().getList(EnrRatingItem.class, EnrRatingItem.competition(), singleCompetition);

            for (EnrRatingItem item : ratingItems) {
                ratingMap.get(item.getCompetition()).add(item);
            }

            DQLSelectBuilder markDQL = new DQLSelectBuilder().fromEntity(EnrChosenEntranceExam.class, "c").column("c")
            .where(isNotNull(property(EnrChosenEntranceExam.actualExam().fromAlias("c"))));
            if (singleCompetition != null) {
                markDQL.where(eq(property(EnrChosenEntranceExam.requestedCompetition().competition().fromAlias("c")), value(singleCompetition)));
            } else {
                markDQL.where(eq(property(EnrChosenEntranceExam.actualExam().examSetElement().examSet().enrollmentCampaign().fromAlias("c")), value(enrollmentCampaign)));
            }
            for (EnrChosenEntranceExam exam : IUniBaseDao.instance.get().<EnrChosenEntranceExam>getList(markDQL)) {
                markMap.get(exam.getRequestedCompetition()).put(exam.getActualExam(), exam);
            }

            Map<EnrProgramSetBase, List<EduProgramProf>> programMap = SafeMap.get(ArrayList.class);
            for (EnrProgramSetItem item : IUniBaseDao.instance.get().getList(EnrProgramSetItem.class, EnrProgramSetItem.programSet(), programSets)) {
                programMap.get(item.getProgramSet()).add(item.getProgram());
            }
            for (EnrProgramSetBase programSetBase : programSets) {
                if (programSetBase instanceof EnrProgramSetSecondary) {
                    programMap.get(programSetBase).add(((EnrProgramSetSecondary)programSetBase).getProgram());
                }
            }

            List<EnrEnrollmentExtract> extracts = singleCompetition == null ?
                IUniBaseDao.instance.get().getList(EnrEnrollmentExtract.class, EnrEnrollmentExtract.entity().request().entrant().enrollmentCampaign(), enrollmentCampaign)
                : IUniBaseDao.instance.get().getList(EnrEnrollmentExtract.class, EnrEnrollmentExtract.entity().competition(), singleCompetition);
                for (EnrEnrollmentExtract item : extracts) {
                    extractMap.put(item.getEntity(), item);
                }

                DQLSelectBuilder stepDQL = new DQLSelectBuilder().fromEntity(EnrEnrollmentStepItem.class, "i").column("i")
                        .where(notIn(property(EnrEnrollmentStepItem.step().state().code().fromAlias("i")), Lists.newArrayList(EnrEnrollmentStepStateCodes.CLOSED, EnrEnrollmentStepStateCodes.START)))
                        .where(eq(property(EnrEnrollmentStepItem.recommended().fromAlias("i")), value(true)))
                        .order(property(EnrEnrollmentStepItem.step().enrollmentDate().fromAlias("i")), OrderDirection.desc);
                if (singleCompetition != null) {
                    markDQL.where(eq(property(EnrEnrollmentStepItem.entity().competition().fromAlias("c")), value(singleCompetition)));
                } else {
                    markDQL.where(eq(property(EnrEnrollmentStepItem.step().enrollmentCampaign().fromAlias("c")), value(enrollmentCampaign)));
                }
                for (EnrEnrollmentStepItem stepId : IUniBaseDao.instance.get().<EnrEnrollmentStepItem>getList(stepDQL)) {
                    recommendationMap.put(stepId.getEntity().getRequestedCompetition(), stepId);
                }

                // сортируем списки

                for (List<EnrRatingItem> list : ratingMap.values())
                    Collections.sort(list, new EntityComparator<>(new EntityOrder(EnrRatingItem.position().s(), OrderDirection.asc)));

                // заполняем списки по рейтингу в выгружаемых данных
                for (EnrCompetition competition : competitionList)
                {
                    //            IEnrExamSetDao.IExamSetSettings setSettings = examSetSettingsMap.get(competition.getExamSetVariant().getId());
                    OrgUnit enrOrgUnit = competition.getProgramSetOrgUnit().getOrgUnit().getInstitutionOrgUnit().getOrgUnit();
                    OrgUnit formativeOrgUnit = competition.getProgramSetOrgUnit().getFormativeOrgUnit();

                    EnrEntrantRatingEnvironmentNode.CompetitionRow row = new EnrEntrantRatingEnvironmentNode.CompetitionRow();

                    row.id = competition.getId().toString();
                    row.plan = competition.getPlan();
                    row.eduProgramSubject = competition.getProgramSetOrgUnit().getProgramSet().getProgramSubject().getTitleWithCode();
                    row.eduProgramSubjectType = competition.getProgramSetOrgUnit().getProgramSet().getProgramSubject().getSubjectIndex().getEduProgramSubjectType();
                    row.eduProgramForm = competition.getProgramSetOrgUnit().getProgramSet().getProgramForm().getTitle();
                    row.eduProgramKind = competition.getProgramSetOrgUnit().getProgramSet().getProgramSubject().getSubjectIndex().getProgramKind().getTitle();
                    row.eduLevel = competition.getProgramSetOrgUnit().getProgramSet().getProgramSubject().getSubjectIndex().getProgramKind().getEduLevel().getTitle();
                    row.eduLevelCode = competition.getProgramSetOrgUnit().getProgramSet().getProgramSubject().getSubjectIndex().getProgramKind().getEduLevel().getCode();
                    row.programSet = competition.getProgramSetOrgUnit().getProgramSet().getTitle();
                    row.enrOrgUnit = enrOrgUnit.getFullTitle();
                    row.enrOrgUnitTerritorialFullTitle = enrOrgUnit.getTerritorialFullTitle();
                    row.enrOrgUnitTypeCode = enrOrgUnit.getOrgUnitType().getCode();
                    row.enrOrgUnitSettlement = (enrOrgUnit.getAddress() != null && enrOrgUnit.getAddress().getSettlement() != null) ? enrOrgUnit.getAddress().getSettlement().getTitleWithType() : "";
                    row.formativeOrgUnit = formativeOrgUnit.getFullTitle();
                    row.formativeOrgUnitNominativeCaseTitle = (!StringUtils.isEmpty(formativeOrgUnit.getNominativeCaseTitle()) ? formativeOrgUnit.getNominativeCaseTitle() : formativeOrgUnit.getTitle());
                    row.formativeOrgUnitTypeCode = formativeOrgUnit.getOrgUnitType().getCode();
                    row.formativeOrgUnitSettlement = ((formativeOrgUnit.getAddress() != null && formativeOrgUnit.getAddress().getSettlement() != null) ? formativeOrgUnit.getAddress().getSettlement().getTitleWithType() : "");
                    row.requestType = competition.getRequestType().getCode();
                    row.competitionType = competition.getType().getTitle();
                    row.competitionTypePublicTitle = competition.getType().getPublicTitle();
                    row.compensationTypeShortTitle = competition.getType().getCompensationType().getShortTitle();
                    row.eduLevelRequirementCode = competition.getEduLevelRequirement().getCode();
                    row.eduLevelRequirement = competition.getEduLevelRequirement().getShortTitle();
                    InflectorVariant variant = IUniBaseDao.instance.get().getCatalogItem(InflectorVariant.class, InflectorVariantCodes.RU_GENITIVE);
                    String eduLevelRequirementGenetiveTitle = DeclinableManager.instance().dao().getPropertyValue(competition.getEduLevelRequirement(), EnrEduLevelRequirement.P_TITLE, variant);
                    row.eduLevelRequirementGenetiveTitle = StringUtils.isEmpty(eduLevelRequirementGenetiveTitle) ? "" : eduLevelRequirementGenetiveTitle;

                    List<EduProgramProf> programs = programMap.get(competition.getProgramSetOrgUnit().getProgramSet());
                    List<String> eduProgramTitles = new ArrayList<>();

                    for (EduProgramProf program : programs) {
                        boolean inDepthStudy = (program instanceof EduProgramSecondaryProf) && ((EduProgramSecondaryProf) program).isInDepthStudy();
                        EduProgramSpecialization programSpecialization = program instanceof EduProgramHigherProf ? ((EduProgramHigherProf) program).getProgramSpecialization() : null;
                        boolean rootSpec = programSpecialization != null && programSpecialization.isRootSpecialization();
                        eduProgramTitles.add(UniStringUtils.joinWithSeparator(", ",
                            programSpecialization == null ? null : (rootSpec ? "без профиля" : programSpecialization.getTitle()),
                                program.getEduProgramTrait() == null ? null : program.getEduProgramTrait().getTitle(),
                                    inDepthStudy ? "Углубленная подготовка" : null));

                        EnrEntrantRatingEnvironmentNode.CompetitionRow.EduProgramRow eduProgramRow = new EnrEntrantRatingEnvironmentNode.CompetitionRow.EduProgramRow();
                        eduProgramRow.id = program.getId().toString();
                        eduProgramRow.programSpec = programSpecialization != null ? programSpecialization.getTitle() : null;
                        eduProgramRow.rootSpec = rootSpec;
                        eduProgramRow.eduProgramForm = program.getForm().getTitle();
                        eduProgramRow.secondaryInDepth = inDepthStudy ? "углуб. изуч." : "";
                        final EduProgramOrientation orientation = program instanceof EduProgramHigherProf ? ((EduProgramHigherProf) program).getProgramOrientation() : null;
                        eduProgramRow.bachelorQualificationShortTitle = orientation != null ? orientation.getShortTitle() : null;
                        eduProgramRow.duration = program.getDuration().getTitle();
                        eduProgramRow.eduProgramTrait = program.getEduProgramTrait() != null ? program.getEduProgramTrait().getTitle() : "";
                        row.eduProgram.row.add(eduProgramRow);
                    }

                    Collections.sort(row.eduProgram.row, (o1, o2) -> {
                        if(o1.rootSpec && o2.rootSpec)
                        {
                            return o1.programSpec.compareTo(o2.programSpec);
                        }
                        else if (o1.rootSpec) // && !o2.rootSpec)
                        {
                            return -1;
                        }
                        else if (o2.rootSpec) // && !o1.rootSpec
                        {
                            return 1;
                        }
                        else
                        {
                            return o1.programSpec.compareTo(o2.programSpec);
                        }
                    });

                    row.profileTitle = StringUtils.join(eduProgramTitles, "; ");

                    IEnrExamSetDao.IExamSetSettings examSetSettings = examSetSettingsMap.get(competition.getExamSetVariant().getId());
                    if (examSetSettings != null) {

                        for (IEnrExamSetDao.IExamSetElementSettings discipline : examSetSettings.getElementList()) {
                            row.entranceDiscipline.row.add(fillDisciplineRow(discipline));
                        }
                    }

                    List<EnrRatingItem> competitionRatingItems = ratingMap.get(competition);
                    if (null != competitionRatingItems) {
                        for (EnrRatingItem ratingItem : competitionRatingItems) {
                            EnrRequestedCompetition reqComp = ratingItem.getRequestedCompetition();
                            Map<EnrRequestType, List<EnrEntrantAchievement>> achMap = entrantAchievementMap.get(ratingItem.getEntrant());
                            row.entrant.row.add(fillEntrantRow(ratingItem, markMap.get(reqComp), recommendationMap.get(reqComp), extractMap.get(reqComp), examSetSettings, achMap != null ? achMap.get(competition.getRequestType()) : null));
                        }
                    }
                    env.competition.row.add(row);
                }
    }

    private EnrEntrantRatingEnvironmentNode.CompetitionRow.EntranceDisciplineRow fillDisciplineRow(IEnrExamSetDao.IExamSetElementSettings discipline)
    {
        EnrEntrantRatingEnvironmentNode.CompetitionRow.EntranceDisciplineRow disciplineRow = new EnrEntrantRatingEnvironmentNode.CompetitionRow.EntranceDisciplineRow();
        IEnrExamSetElementValue value = discipline.getElement().getValue();
        disciplineRow.id = value.getId().toString();
        disciplineRow.shortTitle = value.getShortTitle();
        disciplineRow.title = value.getTitle();
        disciplineRow.examType = discipline.getElement().getType().getCode();
        disciplineRow.passMarkMultipliedBy1000 = discipline.getPassMarkAsLong();
        disciplineRow.priority = discipline.getPriority();
        if(discipline.getPassFormMap() != null && !discipline.getPassFormMap().isEmpty())
        {
            for(Map.Entry<EnrExamPassForm, Boolean> entry : discipline.getPassFormMap().entrySet())
            {
                if(entry.getValue())
                    disciplineRow.passForms.add(new EnrEntrantRatingEnvironmentNode.Row(entry.getKey().getTitle(), entry.getKey().getCode()));
            }
            Collections.sort(disciplineRow.passForms);

        }
        return disciplineRow;
    }

    private EnrEntrantRatingEnvironmentNode.CompetitionRow.RequestedCompetitionRow fillEntrantRow(EnrRatingItem ratingItem, Map<EnrExamVariant, EnrChosenEntranceExam> markMap, EnrEnrollmentStepItem enrollmentStepItem, EnrEnrollmentExtract extract, IEnrExamSetDao.IExamSetSettings examSetSettings, List<EnrEntrantAchievement> enrEntrantAchievements)
    {
        final EnrEntrantRatingEnvironmentNode.CompetitionRow.RequestedCompetitionRow entrantRow = new EnrEntrantRatingEnvironmentNode.CompetitionRow.RequestedCompetitionRow();

        final EnrEntrant entrant = ratingItem.getRequestedCompetition().getRequest().getEntrant();
        final EnrRequestedCompetition reqComp = ratingItem.getRequestedCompetition();
        final EnrEntrantRequest request = reqComp.getRequest();
        final PersonEduDocument eduDocument = request.getEduDocument();

        entrantRow.entrantId = String.valueOf(entrant.getId());
        entrantRow.regNumber = request.getStringNumber();
        entrantRow.fio = entrant.getFio();
        entrantRow.fullFio = entrant.getFullFio();
        entrantRow.priority = reqComp.getPriority();
        if (reqComp instanceof EnrRequestedCompetitionTA) {
            EnrRequestedCompetitionTA compTA = (EnrRequestedCompetitionTA) reqComp;
            entrantRow.targetAdmissionKind = compTA.getTargetAdmissionKind().getTargetAdmissionKind().getCode();
            entrantRow.targetAdmissionContractNumber = compTA.getContractNumber();
            entrantRow.targetAdmissionContractDate = compTA.getContractDate();
            entrantRow.targetAdmissionExtOrgUnit = compTA.getTargetAdmissionOrgUnit().getTitle();
            entrantRow.targetAdmissionExtOrgUnitShortTitle = compTA.getTargetAdmissionOrgUnit().getShortTitle();
            entrantRow.targetAdmissionKindInterestsState = compTA.getTargetAdmissionKind().isStateInterest();
        }
        else if (reqComp instanceof EnrRequestedCompetitionExclusive) {
            EnrRequestedCompetitionExclusive compExcl = (EnrRequestedCompetitionExclusive) reqComp;
            entrantRow.benefitCategory = compExcl.getBenefitCategory().getCode();
            entrantRow.benefitProgram = compExcl.getProgramSetItem().getProgram().getId().toString();
        }
        else if (reqComp instanceof EnrRequestedCompetitionNoExams) {
            EnrRequestedCompetitionNoExams compNoExam = (EnrRequestedCompetitionNoExams) reqComp;
            entrantRow.benefitCategory = compNoExam.getBenefitCategory().getCode();
            entrantRow.benefitProgram = compNoExam.getProgramSetItem().getProgram().getId().toString();
        }
        if (extract != null) {
            EnrOrder order = extract.getOrder();
            entrantRow.orderId = order.getId().toString();
            entrantRow.orderNumber = order.getNumber();
            entrantRow.orderDate = order.getCommitDate();
        }
        if (request.getBenefitCategory() != null) {
            entrantRow.preferenceCategory = request.getBenefitCategory().getCode();
        }

        entrantRow.originalSubmissionWay = ratingItem.getRequestedCompetition().getRequest().getOriginalSubmissionWay().getCode();
        entrantRow.finalMark = ratingItem.getTotalMarkAsStringReplacingZero();
        entrantRow.entranceMark = ratingItem.getEntranceSumAsStringReplacingZero();
        entrantRow.achievementMark = ratingItem.getAchievementMarkAsString();
        entrantRow.passedExam = ratingItem.isStatusRatingPositiveSafe();
        entrantRow.priorityPerEntrant = reqComp.getPriority();
        entrantRow.position = ratingItem.getPosition();
        entrantRow.absolutePosition = ratingItem.getPositionAbsolute();
        entrantRow.averageEduInstitutionMark = eduDocument.getAvgMarkAsLong() == null ? "-" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(eduDocument.getAvgMarkAsDouble());
        entrantRow.originalIn = reqComp.isOriginalDocumentHandedIn();

        entrantRow.accepted = reqComp.isEnrollmentAvailable();
        entrantRow.acceptedEntrant = reqComp.isAccepted();
        entrantRow.refusedToBeEnrolled = reqComp.getRefusedToBeEnrolled();

        if(EnrEntrantStateCodes.RECOMMENDED.equals(reqComp.getState().getCode()))
        {
            entrantRow.recommended = true;
            if (enrollmentStepItem != null)
                entrantRow.recommendedEnrollmentDate = DateFormatter.DEFAULT_DATE_FORMATTER.format(enrollmentStepItem.getStep().getEnrollmentDate());
        }
        else
            entrantRow.recommended = false;

        EnrEntrantState entrantState = reqComp.getState();
        entrantRow.status = entrantState == null ? "-" : entrantState.getTitle();
        entrantRow.statusCode = entrantState == null ? "-" : entrantState.getCode();

        entrantRow.needDormitory = entrant.getPerson().isNeedDormitory();

        for (IEnrExamSetDao.IExamSetElementSettings exam : examSetSettings.getElementList())
        {
            /**
             * Оценки по дисциплинам вступительных испытаний. Каждая оценка может быть:
             * 1. x, если нет актуальной ВВИ по данному конкурсу
             * 2. -, если для актуальной ВВИ нет основания балла
             * 3. иначе балл из основания балла             *
             * @see EntranceDisciplineNode
             */

            EnrChosenEntranceExam entranceExam = markMap.get(exam.getCgExam());
            if (entranceExam == null)
                entrantRow.marks.add("x");
            else if (entranceExam.getMarkSource() == null)
                entrantRow.marks.add("-");
            else
                entrantRow.marks.add(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(entranceExam.getMarkAsDouble()));
        }

        if(markMap != null && !markMap.isEmpty())
        {
            List<EnrChosenEntranceExam> examList = Lists.newArrayList(markMap.values());
            Collections.sort(examList, EnrChosenEntranceExamNumberFormatter.COMPARATOR);
            EnrChosenEntranceExamNumberFormatter numberFormatter = new EnrChosenEntranceExamNumberFormatter(examList);
            for (EnrChosenEntranceExam exam : examList)
            {
                entrantRow.markEntranceExams.add(numberFormatter.format(exam) + exam.getDiscipline().getTitle() + ": " + (exam.getMarkSource() == null ? "—" : exam.getMarkSource().getMarkAsString()) + ( exam.getMarkSource() != null && exam.getMarkSource().getChosenEntranceExamForm() != null && EnrExamPassFormCodes.STATE_EXAM.equals(exam.getMarkSource().getChosenEntranceExamForm().getPassForm().getCode()) ? " (" + exam.getMarkSource().getChosenEntranceExamForm().getPassForm().getTitle() + ")" : ""));
            }
        }

        if(enrEntrantAchievements != null )
        {
            List<EnrEntrantAchievement> achievements = Lists.newArrayList(enrEntrantAchievements);
            Collections.sort(achievements, new EntityComparator<>(new EntityOrder(EnrEntrantAchievement.mark(), OrderDirection.desc)));

            int i = 0;
            for (EnrEntrantAchievement achievement : achievements)
            {
                EnrEntrantAchievementType type = achievement.getType();
                long mark = type.isMarked() ? achievement.getRatingMarkAsLong() : type.getAchievementMarkAsLong();

                entrantRow.markEntrantAchievements.add(++i + ". " + type.getAchievementKind().getShortTitle() + " — " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(mark * 0.001d));
            }
        }
        return entrantRow;
    }
}
