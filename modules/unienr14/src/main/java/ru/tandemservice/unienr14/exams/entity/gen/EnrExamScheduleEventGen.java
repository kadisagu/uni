package ru.tandemservice.unienr14.exams.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm;
import ru.tandemservice.unienr14.exams.entity.EnrExamScheduleEvent;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignExamRoom;
import ru.tandemservice.unischedule.base.entity.event.ScheduleEvent;
import ru.tandemservice.unischedule.base.entity.event.ScheduleEventPlace;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Событие в расписании ВИ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrExamScheduleEventGen extends EntityBase
 implements INaturalIdentifiable<EnrExamScheduleEventGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.exams.entity.EnrExamScheduleEvent";
    public static final String ENTITY_NAME = "enrExamScheduleEvent";
    public static final int VERSION_HASH = 584492659;
    private static IEntityMeta ENTITY_META;

    public static final String L_SCHEDULE_EVENT = "scheduleEvent";
    public static final String L_SCHEDULE_EVENT_PLACE = "scheduleEventPlace";
    public static final String L_EXAM_ROOM = "examRoom";
    public static final String L_DISCIPLINE = "discipline";
    public static final String L_PASS_FORM = "passForm";
    public static final String P_COMMISSION = "commission";
    public static final String P_SIZE = "size";

    private ScheduleEvent _scheduleEvent;     // Событие базового расписания
    private ScheduleEventPlace _scheduleEventPlace;     // Место проведения события базового расписания
    private EnrCampaignExamRoom _examRoom;     // Место проведения
    private EnrCampaignDiscipline _discipline;     // Дисциплина ПК
    private EnrExamPassForm _passForm;     // Форма сдачи
    private String _commission;     // Комиссия
    private int _size;     // Число мест

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Событие базового расписания. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public ScheduleEvent getScheduleEvent()
    {
        return _scheduleEvent;
    }

    /**
     * @param scheduleEvent Событие базового расписания. Свойство не может быть null и должно быть уникальным.
     */
    public void setScheduleEvent(ScheduleEvent scheduleEvent)
    {
        dirty(_scheduleEvent, scheduleEvent);
        _scheduleEvent = scheduleEvent;
    }

    /**
     * @return Место проведения события базового расписания. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public ScheduleEventPlace getScheduleEventPlace()
    {
        return _scheduleEventPlace;
    }

    /**
     * @param scheduleEventPlace Место проведения события базового расписания. Свойство не может быть null и должно быть уникальным.
     */
    public void setScheduleEventPlace(ScheduleEventPlace scheduleEventPlace)
    {
        dirty(_scheduleEventPlace, scheduleEventPlace);
        _scheduleEventPlace = scheduleEventPlace;
    }

    /**
     * @return Место проведения. Свойство не может быть null.
     */
    @NotNull
    public EnrCampaignExamRoom getExamRoom()
    {
        return _examRoom;
    }

    /**
     * @param examRoom Место проведения. Свойство не может быть null.
     */
    public void setExamRoom(EnrCampaignExamRoom examRoom)
    {
        dirty(_examRoom, examRoom);
        _examRoom = examRoom;
    }

    /**
     * @return Дисциплина ПК. Свойство не может быть null.
     */
    @NotNull
    public EnrCampaignDiscipline getDiscipline()
    {
        return _discipline;
    }

    /**
     * @param discipline Дисциплина ПК. Свойство не может быть null.
     */
    public void setDiscipline(EnrCampaignDiscipline discipline)
    {
        dirty(_discipline, discipline);
        _discipline = discipline;
    }

    /**
     * @return Форма сдачи. Свойство не может быть null.
     */
    @NotNull
    public EnrExamPassForm getPassForm()
    {
        return _passForm;
    }

    /**
     * @param passForm Форма сдачи. Свойство не может быть null.
     */
    public void setPassForm(EnrExamPassForm passForm)
    {
        dirty(_passForm, passForm);
        _passForm = passForm;
    }

    /**
     * @return Комиссия.
     */
    @Length(max=2000)
    public String getCommission()
    {
        return _commission;
    }

    /**
     * @param commission Комиссия.
     */
    public void setCommission(String commission)
    {
        dirty(_commission, commission);
        _commission = commission;
    }

    /**
     * @return Число мест. Свойство не может быть null.
     */
    @NotNull
    public int getSize()
    {
        return _size;
    }

    /**
     * @param size Число мест. Свойство не может быть null.
     */
    public void setSize(int size)
    {
        dirty(_size, size);
        _size = size;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrExamScheduleEventGen)
        {
            if (withNaturalIdProperties)
            {
                setScheduleEvent(((EnrExamScheduleEvent)another).getScheduleEvent());
            }
            setScheduleEventPlace(((EnrExamScheduleEvent)another).getScheduleEventPlace());
            setExamRoom(((EnrExamScheduleEvent)another).getExamRoom());
            setDiscipline(((EnrExamScheduleEvent)another).getDiscipline());
            setPassForm(((EnrExamScheduleEvent)another).getPassForm());
            setCommission(((EnrExamScheduleEvent)another).getCommission());
            setSize(((EnrExamScheduleEvent)another).getSize());
        }
    }

    public INaturalId<EnrExamScheduleEventGen> getNaturalId()
    {
        return new NaturalId(getScheduleEvent());
    }

    public static class NaturalId extends NaturalIdBase<EnrExamScheduleEventGen>
    {
        private static final String PROXY_NAME = "EnrExamScheduleEventNaturalProxy";

        private Long _scheduleEvent;

        public NaturalId()
        {}

        public NaturalId(ScheduleEvent scheduleEvent)
        {
            _scheduleEvent = ((IEntity) scheduleEvent).getId();
        }

        public Long getScheduleEvent()
        {
            return _scheduleEvent;
        }

        public void setScheduleEvent(Long scheduleEvent)
        {
            _scheduleEvent = scheduleEvent;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrExamScheduleEventGen.NaturalId) ) return false;

            EnrExamScheduleEventGen.NaturalId that = (NaturalId) o;

            if( !equals(getScheduleEvent(), that.getScheduleEvent()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getScheduleEvent());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getScheduleEvent());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrExamScheduleEventGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrExamScheduleEvent.class;
        }

        public T newInstance()
        {
            return (T) new EnrExamScheduleEvent();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "scheduleEvent":
                    return obj.getScheduleEvent();
                case "scheduleEventPlace":
                    return obj.getScheduleEventPlace();
                case "examRoom":
                    return obj.getExamRoom();
                case "discipline":
                    return obj.getDiscipline();
                case "passForm":
                    return obj.getPassForm();
                case "commission":
                    return obj.getCommission();
                case "size":
                    return obj.getSize();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "scheduleEvent":
                    obj.setScheduleEvent((ScheduleEvent) value);
                    return;
                case "scheduleEventPlace":
                    obj.setScheduleEventPlace((ScheduleEventPlace) value);
                    return;
                case "examRoom":
                    obj.setExamRoom((EnrCampaignExamRoom) value);
                    return;
                case "discipline":
                    obj.setDiscipline((EnrCampaignDiscipline) value);
                    return;
                case "passForm":
                    obj.setPassForm((EnrExamPassForm) value);
                    return;
                case "commission":
                    obj.setCommission((String) value);
                    return;
                case "size":
                    obj.setSize((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "scheduleEvent":
                        return true;
                case "scheduleEventPlace":
                        return true;
                case "examRoom":
                        return true;
                case "discipline":
                        return true;
                case "passForm":
                        return true;
                case "commission":
                        return true;
                case "size":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "scheduleEvent":
                    return true;
                case "scheduleEventPlace":
                    return true;
                case "examRoom":
                    return true;
                case "discipline":
                    return true;
                case "passForm":
                    return true;
                case "commission":
                    return true;
                case "size":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "scheduleEvent":
                    return ScheduleEvent.class;
                case "scheduleEventPlace":
                    return ScheduleEventPlace.class;
                case "examRoom":
                    return EnrCampaignExamRoom.class;
                case "discipline":
                    return EnrCampaignDiscipline.class;
                case "passForm":
                    return EnrExamPassForm.class;
                case "commission":
                    return String.class;
                case "size":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrExamScheduleEvent> _dslPath = new Path<EnrExamScheduleEvent>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrExamScheduleEvent");
    }
            

    /**
     * @return Событие базового расписания. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamScheduleEvent#getScheduleEvent()
     */
    public static ScheduleEvent.Path<ScheduleEvent> scheduleEvent()
    {
        return _dslPath.scheduleEvent();
    }

    /**
     * @return Место проведения события базового расписания. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamScheduleEvent#getScheduleEventPlace()
     */
    public static ScheduleEventPlace.Path<ScheduleEventPlace> scheduleEventPlace()
    {
        return _dslPath.scheduleEventPlace();
    }

    /**
     * @return Место проведения. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamScheduleEvent#getExamRoom()
     */
    public static EnrCampaignExamRoom.Path<EnrCampaignExamRoom> examRoom()
    {
        return _dslPath.examRoom();
    }

    /**
     * @return Дисциплина ПК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamScheduleEvent#getDiscipline()
     */
    public static EnrCampaignDiscipline.Path<EnrCampaignDiscipline> discipline()
    {
        return _dslPath.discipline();
    }

    /**
     * @return Форма сдачи. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamScheduleEvent#getPassForm()
     */
    public static EnrExamPassForm.Path<EnrExamPassForm> passForm()
    {
        return _dslPath.passForm();
    }

    /**
     * @return Комиссия.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamScheduleEvent#getCommission()
     */
    public static PropertyPath<String> commission()
    {
        return _dslPath.commission();
    }

    /**
     * @return Число мест. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamScheduleEvent#getSize()
     */
    public static PropertyPath<Integer> size()
    {
        return _dslPath.size();
    }

    public static class Path<E extends EnrExamScheduleEvent> extends EntityPath<E>
    {
        private ScheduleEvent.Path<ScheduleEvent> _scheduleEvent;
        private ScheduleEventPlace.Path<ScheduleEventPlace> _scheduleEventPlace;
        private EnrCampaignExamRoom.Path<EnrCampaignExamRoom> _examRoom;
        private EnrCampaignDiscipline.Path<EnrCampaignDiscipline> _discipline;
        private EnrExamPassForm.Path<EnrExamPassForm> _passForm;
        private PropertyPath<String> _commission;
        private PropertyPath<Integer> _size;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Событие базового расписания. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamScheduleEvent#getScheduleEvent()
     */
        public ScheduleEvent.Path<ScheduleEvent> scheduleEvent()
        {
            if(_scheduleEvent == null )
                _scheduleEvent = new ScheduleEvent.Path<ScheduleEvent>(L_SCHEDULE_EVENT, this);
            return _scheduleEvent;
        }

    /**
     * @return Место проведения события базового расписания. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamScheduleEvent#getScheduleEventPlace()
     */
        public ScheduleEventPlace.Path<ScheduleEventPlace> scheduleEventPlace()
        {
            if(_scheduleEventPlace == null )
                _scheduleEventPlace = new ScheduleEventPlace.Path<ScheduleEventPlace>(L_SCHEDULE_EVENT_PLACE, this);
            return _scheduleEventPlace;
        }

    /**
     * @return Место проведения. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamScheduleEvent#getExamRoom()
     */
        public EnrCampaignExamRoom.Path<EnrCampaignExamRoom> examRoom()
        {
            if(_examRoom == null )
                _examRoom = new EnrCampaignExamRoom.Path<EnrCampaignExamRoom>(L_EXAM_ROOM, this);
            return _examRoom;
        }

    /**
     * @return Дисциплина ПК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamScheduleEvent#getDiscipline()
     */
        public EnrCampaignDiscipline.Path<EnrCampaignDiscipline> discipline()
        {
            if(_discipline == null )
                _discipline = new EnrCampaignDiscipline.Path<EnrCampaignDiscipline>(L_DISCIPLINE, this);
            return _discipline;
        }

    /**
     * @return Форма сдачи. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamScheduleEvent#getPassForm()
     */
        public EnrExamPassForm.Path<EnrExamPassForm> passForm()
        {
            if(_passForm == null )
                _passForm = new EnrExamPassForm.Path<EnrExamPassForm>(L_PASS_FORM, this);
            return _passForm;
        }

    /**
     * @return Комиссия.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamScheduleEvent#getCommission()
     */
        public PropertyPath<String> commission()
        {
            if(_commission == null )
                _commission = new PropertyPath<String>(EnrExamScheduleEventGen.P_COMMISSION, this);
            return _commission;
        }

    /**
     * @return Число мест. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamScheduleEvent#getSize()
     */
        public PropertyPath<Integer> size()
        {
            if(_size == null )
                _size = new PropertyPath<Integer>(EnrExamScheduleEventGen.P_SIZE, this);
            return _size;
        }

        public Class getEntityClass()
        {
            return EnrExamScheduleEvent.class;
        }

        public String getEntityName()
        {
            return "enrExamScheduleEvent";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
