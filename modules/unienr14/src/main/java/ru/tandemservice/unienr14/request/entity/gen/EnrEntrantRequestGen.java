package ru.tandemservice.unienr14.request.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.sec.entity.gen.IPrincipalContextGen;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import ru.tandemservice.unienr14.catalog.entity.EnrBenefitCategory;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.unienr14.catalog.entity.EnrInternalExamReason;
import ru.tandemservice.unienr14.catalog.entity.EnrOriginalSubmissionAndReturnWay;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantOriginalDocumentStatus;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitStatement;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Заявление абитуриента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrEntrantRequestGen extends VersionedEntityBase
 implements IEnrEntrantBenefitStatement, ILazyFieldOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.request.entity.EnrEntrantRequest";
    public static final String ENTITY_NAME = "enrEntrantRequest";
    public static final int VERSION_HASH = -13435484;
    private static IEntityMeta ENTITY_META;

    public static final String P_VERSION = "version";
    public static final String L_ENTRANT = "entrant";
    public static final String L_TYPE = "type";
    public static final String P_REG_NUMBER = "regNumber";
    public static final String P_REG_DATE = "regDate";
    public static final String P_COMMENT = "comment";
    public static final String P_TAKE_AWAY_DOCUMENT_DATE = "takeAwayDocumentDate";
    public static final String P_TAKE_AWAY_DOCUMENT = "takeAwayDocument";
    public static final String L_IDENTITY_CARD = "identityCard";
    public static final String L_EDU_DOCUMENT = "eduDocument";
    public static final String L_EDU_INST_DOC_ORIGINAL_REF = "eduInstDocOriginalRef";
    public static final String P_EDU_INST_DOC_ORIGINAL_HANDED_IN = "eduInstDocOriginalHandedIn";
    public static final String L_BENEFIT_CATEGORY = "benefitCategory";
    public static final String L_ENROLLMENT_COMMISSION = "enrollmentCommission";
    public static final String P_SPECIAL_SCHOOL = "specialSchool";
    public static final String P_EVENING_SCHOOL = "eveningSchool";
    public static final String P_ENTRANT_ACHIEVEMENT_MARK_SUM = "entrantAchievementMarkSum";
    public static final String P_FILED_BY_TRUSTEE = "filedByTrustee";
    public static final String P_TRUSTEE_DETAILS = "trusteeDetails";
    public static final String L_ORIGINAL_SUBMISSION_WAY = "originalSubmissionWay";
    public static final String L_ORIGINAL_RETURN_WAY = "originalReturnWay";
    public static final String L_INTERNAL_EXAM_REASON = "internalExamReason";
    public static final String P_INTERNATIONAL_TREATY_CONTRACTOR = "internationalTreatyContractor";
    public static final String P_GENERAL_EDU_SCHOOL = "generalEduSchool";
    public static final String P_RECEIVE_EDU_LEVEL_FIRST = "receiveEduLevelFirst";
    public static final String L_ACCEPT_PRINCIPAL_CONTEXT = "acceptPrincipalContext";
    public static final String P_ACCEPT_REQUEST = "acceptRequest";
    public static final String P_STRING_NUMBER = "stringNumber";
    public static final String P_STRING_NUMBER_WITH_EC_TITLE = "stringNumberWithEcTitle";

    private int _version; 
    private EnrEntrant _entrant;     // Абитуриент
    private EnrRequestType _type;     // Тип
    private String _regNumber;     // Номер заявления
    private Date _regDate;     // Дата добавления заявления
    private String _comment;     // Комментарий
    private Date _takeAwayDocumentDate;     // Дата отзыва заявления
    
    private IdentityCard _identityCard;     // Удостоверение личности
    private PersonEduDocument _eduDocument;     // Документ о полученном образовании
    private EnrEntrantOriginalDocumentStatus _eduInstDocOriginalRef;     // Оригинал док. об образ.
    
    private EnrBenefitCategory _benefitCategory;     // Категория особого права (при наличии преим. права на поступление)
    private EnrEnrollmentCommission _enrollmentCommission;     // Отборочная комиссия
    private boolean _specialSchool;     // Выпускник специальной (коррекционной) образовательной организации
    private boolean _eveningSchool;     // Выпускник вечерней (сменной) образовательной организации
    private long _entrantAchievementMarkSum = 0l;     // Сумма баллов за индивидуальные достижения (без учета баллов аттестата и настройки)
    private boolean _filedByTrustee;     // Подано доверенным лицом
    private String _trusteeDetails;     // Реквизиты доверенного лица
    private EnrOriginalSubmissionAndReturnWay _originalSubmissionWay;     // Способ подачи оригиналов документов
    private EnrOriginalSubmissionAndReturnWay _originalReturnWay;     // Способ возврата оригиналов документов
    private EnrInternalExamReason _internalExamReason;     // Категории лиц, обладающих правом сдавать общеобразовательные ВИ по материалам ОУ
    private boolean _internationalTreatyContractor;     // Абитуриент, поступающий по международному договору
    private boolean _generalEduSchool;     // Выпускник общеобразовательной организации
    private boolean _receiveEduLevelFirst = true;     // Получает образование данного уровня впервые
    private IPrincipalContext _acceptPrincipalContext;     // Принял заявление (контекст принципала)
    private String _acceptRequest;     // Принял заявление

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public int getVersion()
    {
        return _version;
    }

    /**
     * @param version  Свойство не может быть null.
     */
    public void setVersion(int version)
    {
        dirty(_version, version);
        _version = version;
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     */
    @NotNull
    public EnrEntrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant Абитуриент. Свойство не может быть null.
     */
    public void setEntrant(EnrEntrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    /**
     * @return Тип. Свойство не может быть null.
     */
    @NotNull
    public EnrRequestType getType()
    {
        return _type;
    }

    /**
     * @param type Тип. Свойство не может быть null.
     */
    public void setType(EnrRequestType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Номер заявления. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getRegNumber()
    {
        return _regNumber;
    }

    /**
     * @param regNumber Номер заявления. Свойство не может быть null.
     */
    public void setRegNumber(String regNumber)
    {
        dirty(_regNumber, regNumber);
        _regNumber = regNumber;
    }

    /**
     * На форме добавления заявления и в мастере миллисекунды обнуляются.
     *
     * @return Дата добавления заявления. Свойство не может быть null.
     */
    @NotNull
    public Date getRegDate()
    {
        return _regDate;
    }

    /**
     * @param regDate Дата добавления заявления. Свойство не может быть null.
     */
    public void setRegDate(Date regDate)
    {
        dirty(_regDate, regDate);
        _regDate = regDate;
    }

    /**
     * @return Комментарий.
     */
    public String getComment()
    {
        initLazyForGet("comment");
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        initLazyForSet("comment");
        dirty(_comment, comment);
        _comment = comment;
    }

    /**
     * Дата, когда абитуриент забирает документы (отзывает заявление), null - заявление считается действующим.
     *
     * @return Дата отзыва заявления.
     */
    public Date getTakeAwayDocumentDate()
    {
        return _takeAwayDocumentDate;
    }

    /**
     * @param takeAwayDocumentDate Дата отзыва заявления.
     */
    public void setTakeAwayDocumentDate(Date takeAwayDocumentDate)
    {
        dirty(_takeAwayDocumentDate, takeAwayDocumentDate);
        _takeAwayDocumentDate = takeAwayDocumentDate;
    }

    public boolean isTakeAwayDocument() { 
				 		return null != getTakeAwayDocumentDate(); 
				 	}

    public void setTakeAwayDocument(boolean takeAwayDocument) {
				   	    if (isInitLazyInProgress()) { return; }
				   	    if (takeAwayDocument == isTakeAwayDocument()) { return; } 
						setTakeAwayDocumentDate(takeAwayDocument ? new Date() : null);
				   	}

    /**
     * @return Удостоверение личности. Свойство не может быть null.
     */
    @NotNull
    public IdentityCard getIdentityCard()
    {
        return _identityCard;
    }

    /**
     * @param identityCard Удостоверение личности. Свойство не может быть null.
     */
    public void setIdentityCard(IdentityCard identityCard)
    {
        dirty(_identityCard, identityCard);
        _identityCard = identityCard;
    }

    /**
     * @return Документ о полученном образовании. Свойство не может быть null.
     */
    @NotNull
    public PersonEduDocument getEduDocument()
    {
        return _eduDocument;
    }

    /**
     * @param eduDocument Документ о полученном образовании. Свойство не может быть null.
     */
    public void setEduDocument(PersonEduDocument eduDocument)
    {
        dirty(_eduDocument, eduDocument);
        _eduDocument = eduDocument;
    }

    /**
     * Ссылка на оригинал документа об образовании, если таковые принесены.
     * Обновляется демоном IEnrRequestExtParamsDaemonBean#doUpdateRequestInstDocOriginalRef.
     *
     * @return Оригинал док. об образ..
     */
    public EnrEntrantOriginalDocumentStatus getEduInstDocOriginalRef()
    {
        return _eduInstDocOriginalRef;
    }

    /**
     * @param eduInstDocOriginalRef Оригинал док. об образ..
     */
    public void setEduInstDocOriginalRef(EnrEntrantOriginalDocumentStatus eduInstDocOriginalRef)
    {
        dirty(_eduInstDocOriginalRef, eduInstDocOriginalRef);
        _eduInstDocOriginalRef = eduInstDocOriginalRef;
    }

    public boolean isEduInstDocOriginalHandedIn() { 
				 		return null != getEduInstDocOriginalRef() && getEduInstDocOriginalRef().isOriginalDocumentHandedIn();
				 	}

    public void setEduInstDocOriginalHandedIn(boolean eduInstDocOriginalHandedIn) {
				   	    if (isInitLazyInProgress()) { return; }
				   	    if (eduInstDocOriginalHandedIn == isEduInstDocOriginalHandedIn()) { return; } 
				   	}

    /**
     * @return Категория особого права (при наличии преим. права на поступление).
     */
    public EnrBenefitCategory getBenefitCategory()
    {
        return _benefitCategory;
    }

    /**
     * @param benefitCategory Категория особого права (при наличии преим. права на поступление).
     */
    public void setBenefitCategory(EnrBenefitCategory benefitCategory)
    {
        dirty(_benefitCategory, benefitCategory);
        _benefitCategory = benefitCategory;
    }

    /**
     * @return Отборочная комиссия.
     */
    public EnrEnrollmentCommission getEnrollmentCommission()
    {
        return _enrollmentCommission;
    }

    /**
     * @param enrollmentCommission Отборочная комиссия.
     */
    public void setEnrollmentCommission(EnrEnrollmentCommission enrollmentCommission)
    {
        dirty(_enrollmentCommission, enrollmentCommission);
        _enrollmentCommission = enrollmentCommission;
    }

    /**
     * @return Выпускник специальной (коррекционной) образовательной организации. Свойство не может быть null.
     */
    @NotNull
    public boolean isSpecialSchool()
    {
        return _specialSchool;
    }

    /**
     * @param specialSchool Выпускник специальной (коррекционной) образовательной организации. Свойство не может быть null.
     */
    public void setSpecialSchool(boolean specialSchool)
    {
        dirty(_specialSchool, specialSchool);
        _specialSchool = specialSchool;
    }

    /**
     * @return Выпускник вечерней (сменной) образовательной организации. Свойство не может быть null.
     */
    @NotNull
    public boolean isEveningSchool()
    {
        return _eveningSchool;
    }

    /**
     * @param eveningSchool Выпускник вечерней (сменной) образовательной организации. Свойство не может быть null.
     */
    public void setEveningSchool(boolean eveningSchool)
    {
        dirty(_eveningSchool, eveningSchool);
        _eveningSchool = eveningSchool;
    }

    /**
     * Хранится со смещением в три знака для дробной части.
     * Промежуточное кэширующее поле: заполняется суммой баллов по всем enrEntrantAchievement для заявления. Значение в данном поле не лимитировано настройкой.
     * Полный балл с учетом ограничений находится в рейтинге enrRatingItem.achievementMarkAsLong.
     * Обновляется демоном IEnrRatingDaemonDao#doUpdateAchievementMark.
     *
     * @return Сумма баллов за индивидуальные достижения (без учета баллов аттестата и настройки). Свойство не может быть null.
     */
    @NotNull
    public long getEntrantAchievementMarkSum()
    {
        return _entrantAchievementMarkSum;
    }

    /**
     * @param entrantAchievementMarkSum Сумма баллов за индивидуальные достижения (без учета баллов аттестата и настройки). Свойство не может быть null.
     */
    public void setEntrantAchievementMarkSum(long entrantAchievementMarkSum)
    {
        dirty(_entrantAchievementMarkSum, entrantAchievementMarkSum);
        _entrantAchievementMarkSum = entrantAchievementMarkSum;
    }

    /**
     * @return Подано доверенным лицом. Свойство не может быть null.
     */
    @NotNull
    public boolean isFiledByTrustee()
    {
        return _filedByTrustee;
    }

    /**
     * @param filedByTrustee Подано доверенным лицом. Свойство не может быть null.
     */
    public void setFiledByTrustee(boolean filedByTrustee)
    {
        dirty(_filedByTrustee, filedByTrustee);
        _filedByTrustee = filedByTrustee;
    }

    /**
     * @return Реквизиты доверенного лица.
     */
    public String getTrusteeDetails()
    {
        return _trusteeDetails;
    }

    /**
     * @param trusteeDetails Реквизиты доверенного лица.
     */
    public void setTrusteeDetails(String trusteeDetails)
    {
        dirty(_trusteeDetails, trusteeDetails);
        _trusteeDetails = trusteeDetails;
    }

    /**
     * @return Способ подачи оригиналов документов. Свойство не может быть null.
     */
    @NotNull
    public EnrOriginalSubmissionAndReturnWay getOriginalSubmissionWay()
    {
        return _originalSubmissionWay;
    }

    /**
     * @param originalSubmissionWay Способ подачи оригиналов документов. Свойство не может быть null.
     */
    public void setOriginalSubmissionWay(EnrOriginalSubmissionAndReturnWay originalSubmissionWay)
    {
        dirty(_originalSubmissionWay, originalSubmissionWay);
        _originalSubmissionWay = originalSubmissionWay;
    }

    /**
     * @return Способ возврата оригиналов документов. Свойство не может быть null.
     */
    @NotNull
    public EnrOriginalSubmissionAndReturnWay getOriginalReturnWay()
    {
        return _originalReturnWay;
    }

    /**
     * @param originalReturnWay Способ возврата оригиналов документов. Свойство не может быть null.
     */
    public void setOriginalReturnWay(EnrOriginalSubmissionAndReturnWay originalReturnWay)
    {
        dirty(_originalReturnWay, originalReturnWay);
        _originalReturnWay = originalReturnWay;
    }

    /**
     * @return Категории лиц, обладающих правом сдавать общеобразовательные ВИ по материалам ОУ.
     */
    public EnrInternalExamReason getInternalExamReason()
    {
        return _internalExamReason;
    }

    /**
     * @param internalExamReason Категории лиц, обладающих правом сдавать общеобразовательные ВИ по материалам ОУ.
     */
    public void setInternalExamReason(EnrInternalExamReason internalExamReason)
    {
        dirty(_internalExamReason, internalExamReason);
        _internalExamReason = internalExamReason;
    }

    /**
     * @return Абитуриент, поступающий по международному договору. Свойство не может быть null.
     */
    @NotNull
    public boolean isInternationalTreatyContractor()
    {
        return _internationalTreatyContractor;
    }

    /**
     * @param internationalTreatyContractor Абитуриент, поступающий по международному договору. Свойство не может быть null.
     */
    public void setInternationalTreatyContractor(boolean internationalTreatyContractor)
    {
        dirty(_internationalTreatyContractor, internationalTreatyContractor);
        _internationalTreatyContractor = internationalTreatyContractor;
    }

    /**
     * @return Выпускник общеобразовательной организации. Свойство не может быть null.
     */
    @NotNull
    public boolean isGeneralEduSchool()
    {
        return _generalEduSchool;
    }

    /**
     * @param generalEduSchool Выпускник общеобразовательной организации. Свойство не может быть null.
     */
    public void setGeneralEduSchool(boolean generalEduSchool)
    {
        dirty(_generalEduSchool, generalEduSchool);
        _generalEduSchool = generalEduSchool;
    }

    /**
     * @return Получает образование данного уровня впервые. Свойство не может быть null.
     */
    @NotNull
    public boolean isReceiveEduLevelFirst()
    {
        return _receiveEduLevelFirst;
    }

    /**
     * @param receiveEduLevelFirst Получает образование данного уровня впервые. Свойство не может быть null.
     */
    public void setReceiveEduLevelFirst(boolean receiveEduLevelFirst)
    {
        dirty(_receiveEduLevelFirst, receiveEduLevelFirst);
        _receiveEduLevelFirst = receiveEduLevelFirst;
    }

    /**
     * @return Принял заявление (контекст принципала).
     */
    public IPrincipalContext getAcceptPrincipalContext()
    {
        return _acceptPrincipalContext;
    }

    /**
     * @param acceptPrincipalContext Принял заявление (контекст принципала).
     */
    public void setAcceptPrincipalContext(IPrincipalContext acceptPrincipalContext)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && acceptPrincipalContext!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IPrincipalContext.class);
            IEntityMeta actual =  acceptPrincipalContext instanceof IEntity ? EntityRuntime.getMeta((IEntity) acceptPrincipalContext) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_acceptPrincipalContext, acceptPrincipalContext);
        _acceptPrincipalContext = acceptPrincipalContext;
    }

    /**
     * кэширует loginedTitle принципала
     *
     * @return Принял заявление.
     */
    @Length(max=255)
    public String getAcceptRequest()
    {
        return _acceptRequest;
    }

    /**
     * @param acceptRequest Принял заявление.
     */
    public void setAcceptRequest(String acceptRequest)
    {
        dirty(_acceptRequest, acceptRequest);
        _acceptRequest = acceptRequest;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrEntrantRequestGen)
        {
            setVersion(((EnrEntrantRequest)another).getVersion());
            setEntrant(((EnrEntrantRequest)another).getEntrant());
            setType(((EnrEntrantRequest)another).getType());
            setRegNumber(((EnrEntrantRequest)another).getRegNumber());
            setRegDate(((EnrEntrantRequest)another).getRegDate());
            setComment(((EnrEntrantRequest)another).getComment());
            setTakeAwayDocumentDate(((EnrEntrantRequest)another).getTakeAwayDocumentDate());
            setTakeAwayDocument(((EnrEntrantRequest)another).isTakeAwayDocument());
            setIdentityCard(((EnrEntrantRequest)another).getIdentityCard());
            setEduDocument(((EnrEntrantRequest)another).getEduDocument());
            setEduInstDocOriginalRef(((EnrEntrantRequest)another).getEduInstDocOriginalRef());
            setEduInstDocOriginalHandedIn(((EnrEntrantRequest)another).isEduInstDocOriginalHandedIn());
            setBenefitCategory(((EnrEntrantRequest)another).getBenefitCategory());
            setEnrollmentCommission(((EnrEntrantRequest)another).getEnrollmentCommission());
            setSpecialSchool(((EnrEntrantRequest)another).isSpecialSchool());
            setEveningSchool(((EnrEntrantRequest)another).isEveningSchool());
            setEntrantAchievementMarkSum(((EnrEntrantRequest)another).getEntrantAchievementMarkSum());
            setFiledByTrustee(((EnrEntrantRequest)another).isFiledByTrustee());
            setTrusteeDetails(((EnrEntrantRequest)another).getTrusteeDetails());
            setOriginalSubmissionWay(((EnrEntrantRequest)another).getOriginalSubmissionWay());
            setOriginalReturnWay(((EnrEntrantRequest)another).getOriginalReturnWay());
            setInternalExamReason(((EnrEntrantRequest)another).getInternalExamReason());
            setInternationalTreatyContractor(((EnrEntrantRequest)another).isInternationalTreatyContractor());
            setGeneralEduSchool(((EnrEntrantRequest)another).isGeneralEduSchool());
            setReceiveEduLevelFirst(((EnrEntrantRequest)another).isReceiveEduLevelFirst());
            setAcceptPrincipalContext(((EnrEntrantRequest)another).getAcceptPrincipalContext());
            setAcceptRequest(((EnrEntrantRequest)another).getAcceptRequest());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrEntrantRequestGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrEntrantRequest.class;
        }

        public T newInstance()
        {
            return (T) new EnrEntrantRequest();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "version":
                    return obj.getVersion();
                case "entrant":
                    return obj.getEntrant();
                case "type":
                    return obj.getType();
                case "regNumber":
                    return obj.getRegNumber();
                case "regDate":
                    return obj.getRegDate();
                case "comment":
                    return obj.getComment();
                case "takeAwayDocumentDate":
                    return obj.getTakeAwayDocumentDate();
                case "takeAwayDocument":
                    return obj.isTakeAwayDocument();
                case "identityCard":
                    return obj.getIdentityCard();
                case "eduDocument":
                    return obj.getEduDocument();
                case "eduInstDocOriginalRef":
                    return obj.getEduInstDocOriginalRef();
                case "eduInstDocOriginalHandedIn":
                    return obj.isEduInstDocOriginalHandedIn();
                case "benefitCategory":
                    return obj.getBenefitCategory();
                case "enrollmentCommission":
                    return obj.getEnrollmentCommission();
                case "specialSchool":
                    return obj.isSpecialSchool();
                case "eveningSchool":
                    return obj.isEveningSchool();
                case "entrantAchievementMarkSum":
                    return obj.getEntrantAchievementMarkSum();
                case "filedByTrustee":
                    return obj.isFiledByTrustee();
                case "trusteeDetails":
                    return obj.getTrusteeDetails();
                case "originalSubmissionWay":
                    return obj.getOriginalSubmissionWay();
                case "originalReturnWay":
                    return obj.getOriginalReturnWay();
                case "internalExamReason":
                    return obj.getInternalExamReason();
                case "internationalTreatyContractor":
                    return obj.isInternationalTreatyContractor();
                case "generalEduSchool":
                    return obj.isGeneralEduSchool();
                case "receiveEduLevelFirst":
                    return obj.isReceiveEduLevelFirst();
                case "acceptPrincipalContext":
                    return obj.getAcceptPrincipalContext();
                case "acceptRequest":
                    return obj.getAcceptRequest();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "version":
                    obj.setVersion((Integer) value);
                    return;
                case "entrant":
                    obj.setEntrant((EnrEntrant) value);
                    return;
                case "type":
                    obj.setType((EnrRequestType) value);
                    return;
                case "regNumber":
                    obj.setRegNumber((String) value);
                    return;
                case "regDate":
                    obj.setRegDate((Date) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
                case "takeAwayDocumentDate":
                    obj.setTakeAwayDocumentDate((Date) value);
                    return;
                case "takeAwayDocument":
                    obj.setTakeAwayDocument((Boolean) value);
                    return;
                case "identityCard":
                    obj.setIdentityCard((IdentityCard) value);
                    return;
                case "eduDocument":
                    obj.setEduDocument((PersonEduDocument) value);
                    return;
                case "eduInstDocOriginalRef":
                    obj.setEduInstDocOriginalRef((EnrEntrantOriginalDocumentStatus) value);
                    return;
                case "eduInstDocOriginalHandedIn":
                    obj.setEduInstDocOriginalHandedIn((Boolean) value);
                    return;
                case "benefitCategory":
                    obj.setBenefitCategory((EnrBenefitCategory) value);
                    return;
                case "enrollmentCommission":
                    obj.setEnrollmentCommission((EnrEnrollmentCommission) value);
                    return;
                case "specialSchool":
                    obj.setSpecialSchool((Boolean) value);
                    return;
                case "eveningSchool":
                    obj.setEveningSchool((Boolean) value);
                    return;
                case "entrantAchievementMarkSum":
                    obj.setEntrantAchievementMarkSum((Long) value);
                    return;
                case "filedByTrustee":
                    obj.setFiledByTrustee((Boolean) value);
                    return;
                case "trusteeDetails":
                    obj.setTrusteeDetails((String) value);
                    return;
                case "originalSubmissionWay":
                    obj.setOriginalSubmissionWay((EnrOriginalSubmissionAndReturnWay) value);
                    return;
                case "originalReturnWay":
                    obj.setOriginalReturnWay((EnrOriginalSubmissionAndReturnWay) value);
                    return;
                case "internalExamReason":
                    obj.setInternalExamReason((EnrInternalExamReason) value);
                    return;
                case "internationalTreatyContractor":
                    obj.setInternationalTreatyContractor((Boolean) value);
                    return;
                case "generalEduSchool":
                    obj.setGeneralEduSchool((Boolean) value);
                    return;
                case "receiveEduLevelFirst":
                    obj.setReceiveEduLevelFirst((Boolean) value);
                    return;
                case "acceptPrincipalContext":
                    obj.setAcceptPrincipalContext((IPrincipalContext) value);
                    return;
                case "acceptRequest":
                    obj.setAcceptRequest((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "version":
                        return true;
                case "entrant":
                        return true;
                case "type":
                        return true;
                case "regNumber":
                        return true;
                case "regDate":
                        return true;
                case "comment":
                        return true;
                case "takeAwayDocumentDate":
                        return true;
                case "takeAwayDocument":
                        return true;
                case "identityCard":
                        return true;
                case "eduDocument":
                        return true;
                case "eduInstDocOriginalRef":
                        return true;
                case "eduInstDocOriginalHandedIn":
                        return true;
                case "benefitCategory":
                        return true;
                case "enrollmentCommission":
                        return true;
                case "specialSchool":
                        return true;
                case "eveningSchool":
                        return true;
                case "entrantAchievementMarkSum":
                        return true;
                case "filedByTrustee":
                        return true;
                case "trusteeDetails":
                        return true;
                case "originalSubmissionWay":
                        return true;
                case "originalReturnWay":
                        return true;
                case "internalExamReason":
                        return true;
                case "internationalTreatyContractor":
                        return true;
                case "generalEduSchool":
                        return true;
                case "receiveEduLevelFirst":
                        return true;
                case "acceptPrincipalContext":
                        return true;
                case "acceptRequest":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "version":
                    return true;
                case "entrant":
                    return true;
                case "type":
                    return true;
                case "regNumber":
                    return true;
                case "regDate":
                    return true;
                case "comment":
                    return true;
                case "takeAwayDocumentDate":
                    return true;
                case "takeAwayDocument":
                    return true;
                case "identityCard":
                    return true;
                case "eduDocument":
                    return true;
                case "eduInstDocOriginalRef":
                    return true;
                case "eduInstDocOriginalHandedIn":
                    return true;
                case "benefitCategory":
                    return true;
                case "enrollmentCommission":
                    return true;
                case "specialSchool":
                    return true;
                case "eveningSchool":
                    return true;
                case "entrantAchievementMarkSum":
                    return true;
                case "filedByTrustee":
                    return true;
                case "trusteeDetails":
                    return true;
                case "originalSubmissionWay":
                    return true;
                case "originalReturnWay":
                    return true;
                case "internalExamReason":
                    return true;
                case "internationalTreatyContractor":
                    return true;
                case "generalEduSchool":
                    return true;
                case "receiveEduLevelFirst":
                    return true;
                case "acceptPrincipalContext":
                    return true;
                case "acceptRequest":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "version":
                    return Integer.class;
                case "entrant":
                    return EnrEntrant.class;
                case "type":
                    return EnrRequestType.class;
                case "regNumber":
                    return String.class;
                case "regDate":
                    return Date.class;
                case "comment":
                    return String.class;
                case "takeAwayDocumentDate":
                    return Date.class;
                case "takeAwayDocument":
                    return Boolean.class;
                case "identityCard":
                    return IdentityCard.class;
                case "eduDocument":
                    return PersonEduDocument.class;
                case "eduInstDocOriginalRef":
                    return EnrEntrantOriginalDocumentStatus.class;
                case "eduInstDocOriginalHandedIn":
                    return Boolean.class;
                case "benefitCategory":
                    return EnrBenefitCategory.class;
                case "enrollmentCommission":
                    return EnrEnrollmentCommission.class;
                case "specialSchool":
                    return Boolean.class;
                case "eveningSchool":
                    return Boolean.class;
                case "entrantAchievementMarkSum":
                    return Long.class;
                case "filedByTrustee":
                    return Boolean.class;
                case "trusteeDetails":
                    return String.class;
                case "originalSubmissionWay":
                    return EnrOriginalSubmissionAndReturnWay.class;
                case "originalReturnWay":
                    return EnrOriginalSubmissionAndReturnWay.class;
                case "internalExamReason":
                    return EnrInternalExamReason.class;
                case "internationalTreatyContractor":
                    return Boolean.class;
                case "generalEduSchool":
                    return Boolean.class;
                case "receiveEduLevelFirst":
                    return Boolean.class;
                case "acceptPrincipalContext":
                    return IPrincipalContext.class;
                case "acceptRequest":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrEntrantRequest> _dslPath = new Path<EnrEntrantRequest>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrEntrantRequest");
    }
            

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#getVersion()
     */
    public static PropertyPath<Integer> version()
    {
        return _dslPath.version();
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#getEntrant()
     */
    public static EnrEntrant.Path<EnrEntrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return Тип. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#getType()
     */
    public static EnrRequestType.Path<EnrRequestType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Номер заявления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#getRegNumber()
     */
    public static PropertyPath<String> regNumber()
    {
        return _dslPath.regNumber();
    }

    /**
     * На форме добавления заявления и в мастере миллисекунды обнуляются.
     *
     * @return Дата добавления заявления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#getRegDate()
     */
    public static PropertyPath<Date> regDate()
    {
        return _dslPath.regDate();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * Дата, когда абитуриент забирает документы (отзывает заявление), null - заявление считается действующим.
     *
     * @return Дата отзыва заявления.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#getTakeAwayDocumentDate()
     */
    public static PropertyPath<Date> takeAwayDocumentDate()
    {
        return _dslPath.takeAwayDocumentDate();
    }

    /**
     * Формула, вычисляется на основе поля takeAwayDocumentDate.
     *
     * @return Забрал документы. Свойство не может быть null.
     *
     * Это формула "case when takeAwayDocumentDate is null then false else true end".
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#isTakeAwayDocument()
     */
    public static PropertyPath<Boolean> takeAwayDocument()
    {
        return _dslPath.takeAwayDocument();
    }

    /**
     * @return Удостоверение личности. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#getIdentityCard()
     */
    public static IdentityCard.Path<IdentityCard> identityCard()
    {
        return _dslPath.identityCard();
    }

    /**
     * @return Документ о полученном образовании. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#getEduDocument()
     */
    public static PersonEduDocument.Path<PersonEduDocument> eduDocument()
    {
        return _dslPath.eduDocument();
    }

    /**
     * Ссылка на оригинал документа об образовании, если таковые принесены.
     * Обновляется демоном IEnrRequestExtParamsDaemonBean#doUpdateRequestInstDocOriginalRef.
     *
     * @return Оригинал док. об образ..
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#getEduInstDocOriginalRef()
     */
    public static EnrEntrantOriginalDocumentStatus.Path<EnrEntrantOriginalDocumentStatus> eduInstDocOriginalRef()
    {
        return _dslPath.eduInstDocOriginalRef();
    }

    /**
     * Формула, вычисляется на основе поля eduInstDocOriginalRef и его поля оriginalDocumentHandedIn.
     *
     * @return Приложен оригинал док. об образовании. Свойство не может быть null.
     *
     * Это формула "case when exists(select x.id from enrEntrantOriginalDocumentStatus x where x=eduInstDocOriginalRef and ((x.takeAwayDate is null) or (x.registrationDate > x.takeAwayDate))) then true else false end".
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#isEduInstDocOriginalHandedIn()
     */
    public static PropertyPath<Boolean> eduInstDocOriginalHandedIn()
    {
        return _dslPath.eduInstDocOriginalHandedIn();
    }

    /**
     * @return Категория особого права (при наличии преим. права на поступление).
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#getBenefitCategory()
     */
    public static EnrBenefitCategory.Path<EnrBenefitCategory> benefitCategory()
    {
        return _dslPath.benefitCategory();
    }

    /**
     * @return Отборочная комиссия.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#getEnrollmentCommission()
     */
    public static EnrEnrollmentCommission.Path<EnrEnrollmentCommission> enrollmentCommission()
    {
        return _dslPath.enrollmentCommission();
    }

    /**
     * @return Выпускник специальной (коррекционной) образовательной организации. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#isSpecialSchool()
     */
    public static PropertyPath<Boolean> specialSchool()
    {
        return _dslPath.specialSchool();
    }

    /**
     * @return Выпускник вечерней (сменной) образовательной организации. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#isEveningSchool()
     */
    public static PropertyPath<Boolean> eveningSchool()
    {
        return _dslPath.eveningSchool();
    }

    /**
     * Хранится со смещением в три знака для дробной части.
     * Промежуточное кэширующее поле: заполняется суммой баллов по всем enrEntrantAchievement для заявления. Значение в данном поле не лимитировано настройкой.
     * Полный балл с учетом ограничений находится в рейтинге enrRatingItem.achievementMarkAsLong.
     * Обновляется демоном IEnrRatingDaemonDao#doUpdateAchievementMark.
     *
     * @return Сумма баллов за индивидуальные достижения (без учета баллов аттестата и настройки). Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#getEntrantAchievementMarkSum()
     */
    public static PropertyPath<Long> entrantAchievementMarkSum()
    {
        return _dslPath.entrantAchievementMarkSum();
    }

    /**
     * @return Подано доверенным лицом. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#isFiledByTrustee()
     */
    public static PropertyPath<Boolean> filedByTrustee()
    {
        return _dslPath.filedByTrustee();
    }

    /**
     * @return Реквизиты доверенного лица.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#getTrusteeDetails()
     */
    public static PropertyPath<String> trusteeDetails()
    {
        return _dslPath.trusteeDetails();
    }

    /**
     * @return Способ подачи оригиналов документов. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#getOriginalSubmissionWay()
     */
    public static EnrOriginalSubmissionAndReturnWay.Path<EnrOriginalSubmissionAndReturnWay> originalSubmissionWay()
    {
        return _dslPath.originalSubmissionWay();
    }

    /**
     * @return Способ возврата оригиналов документов. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#getOriginalReturnWay()
     */
    public static EnrOriginalSubmissionAndReturnWay.Path<EnrOriginalSubmissionAndReturnWay> originalReturnWay()
    {
        return _dslPath.originalReturnWay();
    }

    /**
     * @return Категории лиц, обладающих правом сдавать общеобразовательные ВИ по материалам ОУ.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#getInternalExamReason()
     */
    public static EnrInternalExamReason.Path<EnrInternalExamReason> internalExamReason()
    {
        return _dslPath.internalExamReason();
    }

    /**
     * @return Абитуриент, поступающий по международному договору. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#isInternationalTreatyContractor()
     */
    public static PropertyPath<Boolean> internationalTreatyContractor()
    {
        return _dslPath.internationalTreatyContractor();
    }

    /**
     * @return Выпускник общеобразовательной организации. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#isGeneralEduSchool()
     */
    public static PropertyPath<Boolean> generalEduSchool()
    {
        return _dslPath.generalEduSchool();
    }

    /**
     * @return Получает образование данного уровня впервые. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#isReceiveEduLevelFirst()
     */
    public static PropertyPath<Boolean> receiveEduLevelFirst()
    {
        return _dslPath.receiveEduLevelFirst();
    }

    /**
     * @return Принял заявление (контекст принципала).
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#getAcceptPrincipalContext()
     */
    public static IPrincipalContextGen.Path<IPrincipalContext> acceptPrincipalContext()
    {
        return _dslPath.acceptPrincipalContext();
    }

    /**
     * кэширует loginedTitle принципала
     *
     * @return Принял заявление.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#getAcceptRequest()
     */
    public static PropertyPath<String> acceptRequest()
    {
        return _dslPath.acceptRequest();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#getStringNumber()
     */
    public static SupportedPropertyPath<String> stringNumber()
    {
        return _dslPath.stringNumber();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#getStringNumberWithEcTitle()
     */
    public static SupportedPropertyPath<String> stringNumberWithEcTitle()
    {
        return _dslPath.stringNumberWithEcTitle();
    }

    public static class Path<E extends EnrEntrantRequest> extends EntityPath<E>
    {
        private PropertyPath<Integer> _version;
        private EnrEntrant.Path<EnrEntrant> _entrant;
        private EnrRequestType.Path<EnrRequestType> _type;
        private PropertyPath<String> _regNumber;
        private PropertyPath<Date> _regDate;
        private PropertyPath<String> _comment;
        private PropertyPath<Date> _takeAwayDocumentDate;
        private PropertyPath<Boolean> _takeAwayDocument;
        private IdentityCard.Path<IdentityCard> _identityCard;
        private PersonEduDocument.Path<PersonEduDocument> _eduDocument;
        private EnrEntrantOriginalDocumentStatus.Path<EnrEntrantOriginalDocumentStatus> _eduInstDocOriginalRef;
        private PropertyPath<Boolean> _eduInstDocOriginalHandedIn;
        private EnrBenefitCategory.Path<EnrBenefitCategory> _benefitCategory;
        private EnrEnrollmentCommission.Path<EnrEnrollmentCommission> _enrollmentCommission;
        private PropertyPath<Boolean> _specialSchool;
        private PropertyPath<Boolean> _eveningSchool;
        private PropertyPath<Long> _entrantAchievementMarkSum;
        private PropertyPath<Boolean> _filedByTrustee;
        private PropertyPath<String> _trusteeDetails;
        private EnrOriginalSubmissionAndReturnWay.Path<EnrOriginalSubmissionAndReturnWay> _originalSubmissionWay;
        private EnrOriginalSubmissionAndReturnWay.Path<EnrOriginalSubmissionAndReturnWay> _originalReturnWay;
        private EnrInternalExamReason.Path<EnrInternalExamReason> _internalExamReason;
        private PropertyPath<Boolean> _internationalTreatyContractor;
        private PropertyPath<Boolean> _generalEduSchool;
        private PropertyPath<Boolean> _receiveEduLevelFirst;
        private IPrincipalContextGen.Path<IPrincipalContext> _acceptPrincipalContext;
        private PropertyPath<String> _acceptRequest;
        private SupportedPropertyPath<String> _stringNumber;
        private SupportedPropertyPath<String> _stringNumberWithEcTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#getVersion()
     */
        public PropertyPath<Integer> version()
        {
            if(_version == null )
                _version = new PropertyPath<Integer>(EnrEntrantRequestGen.P_VERSION, this);
            return _version;
        }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#getEntrant()
     */
        public EnrEntrant.Path<EnrEntrant> entrant()
        {
            if(_entrant == null )
                _entrant = new EnrEntrant.Path<EnrEntrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return Тип. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#getType()
     */
        public EnrRequestType.Path<EnrRequestType> type()
        {
            if(_type == null )
                _type = new EnrRequestType.Path<EnrRequestType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Номер заявления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#getRegNumber()
     */
        public PropertyPath<String> regNumber()
        {
            if(_regNumber == null )
                _regNumber = new PropertyPath<String>(EnrEntrantRequestGen.P_REG_NUMBER, this);
            return _regNumber;
        }

    /**
     * На форме добавления заявления и в мастере миллисекунды обнуляются.
     *
     * @return Дата добавления заявления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#getRegDate()
     */
        public PropertyPath<Date> regDate()
        {
            if(_regDate == null )
                _regDate = new PropertyPath<Date>(EnrEntrantRequestGen.P_REG_DATE, this);
            return _regDate;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(EnrEntrantRequestGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * Дата, когда абитуриент забирает документы (отзывает заявление), null - заявление считается действующим.
     *
     * @return Дата отзыва заявления.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#getTakeAwayDocumentDate()
     */
        public PropertyPath<Date> takeAwayDocumentDate()
        {
            if(_takeAwayDocumentDate == null )
                _takeAwayDocumentDate = new PropertyPath<Date>(EnrEntrantRequestGen.P_TAKE_AWAY_DOCUMENT_DATE, this);
            return _takeAwayDocumentDate;
        }

    /**
     * Формула, вычисляется на основе поля takeAwayDocumentDate.
     *
     * @return Забрал документы. Свойство не может быть null.
     *
     * Это формула "case when takeAwayDocumentDate is null then false else true end".
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#isTakeAwayDocument()
     */
        public PropertyPath<Boolean> takeAwayDocument()
        {
            if(_takeAwayDocument == null )
                _takeAwayDocument = new PropertyPath<Boolean>(EnrEntrantRequestGen.P_TAKE_AWAY_DOCUMENT, this);
            return _takeAwayDocument;
        }

    /**
     * @return Удостоверение личности. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#getIdentityCard()
     */
        public IdentityCard.Path<IdentityCard> identityCard()
        {
            if(_identityCard == null )
                _identityCard = new IdentityCard.Path<IdentityCard>(L_IDENTITY_CARD, this);
            return _identityCard;
        }

    /**
     * @return Документ о полученном образовании. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#getEduDocument()
     */
        public PersonEduDocument.Path<PersonEduDocument> eduDocument()
        {
            if(_eduDocument == null )
                _eduDocument = new PersonEduDocument.Path<PersonEduDocument>(L_EDU_DOCUMENT, this);
            return _eduDocument;
        }

    /**
     * Ссылка на оригинал документа об образовании, если таковые принесены.
     * Обновляется демоном IEnrRequestExtParamsDaemonBean#doUpdateRequestInstDocOriginalRef.
     *
     * @return Оригинал док. об образ..
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#getEduInstDocOriginalRef()
     */
        public EnrEntrantOriginalDocumentStatus.Path<EnrEntrantOriginalDocumentStatus> eduInstDocOriginalRef()
        {
            if(_eduInstDocOriginalRef == null )
                _eduInstDocOriginalRef = new EnrEntrantOriginalDocumentStatus.Path<EnrEntrantOriginalDocumentStatus>(L_EDU_INST_DOC_ORIGINAL_REF, this);
            return _eduInstDocOriginalRef;
        }

    /**
     * Формула, вычисляется на основе поля eduInstDocOriginalRef и его поля оriginalDocumentHandedIn.
     *
     * @return Приложен оригинал док. об образовании. Свойство не может быть null.
     *
     * Это формула "case when exists(select x.id from enrEntrantOriginalDocumentStatus x where x=eduInstDocOriginalRef and ((x.takeAwayDate is null) or (x.registrationDate > x.takeAwayDate))) then true else false end".
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#isEduInstDocOriginalHandedIn()
     */
        public PropertyPath<Boolean> eduInstDocOriginalHandedIn()
        {
            if(_eduInstDocOriginalHandedIn == null )
                _eduInstDocOriginalHandedIn = new PropertyPath<Boolean>(EnrEntrantRequestGen.P_EDU_INST_DOC_ORIGINAL_HANDED_IN, this);
            return _eduInstDocOriginalHandedIn;
        }

    /**
     * @return Категория особого права (при наличии преим. права на поступление).
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#getBenefitCategory()
     */
        public EnrBenefitCategory.Path<EnrBenefitCategory> benefitCategory()
        {
            if(_benefitCategory == null )
                _benefitCategory = new EnrBenefitCategory.Path<EnrBenefitCategory>(L_BENEFIT_CATEGORY, this);
            return _benefitCategory;
        }

    /**
     * @return Отборочная комиссия.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#getEnrollmentCommission()
     */
        public EnrEnrollmentCommission.Path<EnrEnrollmentCommission> enrollmentCommission()
        {
            if(_enrollmentCommission == null )
                _enrollmentCommission = new EnrEnrollmentCommission.Path<EnrEnrollmentCommission>(L_ENROLLMENT_COMMISSION, this);
            return _enrollmentCommission;
        }

    /**
     * @return Выпускник специальной (коррекционной) образовательной организации. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#isSpecialSchool()
     */
        public PropertyPath<Boolean> specialSchool()
        {
            if(_specialSchool == null )
                _specialSchool = new PropertyPath<Boolean>(EnrEntrantRequestGen.P_SPECIAL_SCHOOL, this);
            return _specialSchool;
        }

    /**
     * @return Выпускник вечерней (сменной) образовательной организации. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#isEveningSchool()
     */
        public PropertyPath<Boolean> eveningSchool()
        {
            if(_eveningSchool == null )
                _eveningSchool = new PropertyPath<Boolean>(EnrEntrantRequestGen.P_EVENING_SCHOOL, this);
            return _eveningSchool;
        }

    /**
     * Хранится со смещением в три знака для дробной части.
     * Промежуточное кэширующее поле: заполняется суммой баллов по всем enrEntrantAchievement для заявления. Значение в данном поле не лимитировано настройкой.
     * Полный балл с учетом ограничений находится в рейтинге enrRatingItem.achievementMarkAsLong.
     * Обновляется демоном IEnrRatingDaemonDao#doUpdateAchievementMark.
     *
     * @return Сумма баллов за индивидуальные достижения (без учета баллов аттестата и настройки). Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#getEntrantAchievementMarkSum()
     */
        public PropertyPath<Long> entrantAchievementMarkSum()
        {
            if(_entrantAchievementMarkSum == null )
                _entrantAchievementMarkSum = new PropertyPath<Long>(EnrEntrantRequestGen.P_ENTRANT_ACHIEVEMENT_MARK_SUM, this);
            return _entrantAchievementMarkSum;
        }

    /**
     * @return Подано доверенным лицом. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#isFiledByTrustee()
     */
        public PropertyPath<Boolean> filedByTrustee()
        {
            if(_filedByTrustee == null )
                _filedByTrustee = new PropertyPath<Boolean>(EnrEntrantRequestGen.P_FILED_BY_TRUSTEE, this);
            return _filedByTrustee;
        }

    /**
     * @return Реквизиты доверенного лица.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#getTrusteeDetails()
     */
        public PropertyPath<String> trusteeDetails()
        {
            if(_trusteeDetails == null )
                _trusteeDetails = new PropertyPath<String>(EnrEntrantRequestGen.P_TRUSTEE_DETAILS, this);
            return _trusteeDetails;
        }

    /**
     * @return Способ подачи оригиналов документов. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#getOriginalSubmissionWay()
     */
        public EnrOriginalSubmissionAndReturnWay.Path<EnrOriginalSubmissionAndReturnWay> originalSubmissionWay()
        {
            if(_originalSubmissionWay == null )
                _originalSubmissionWay = new EnrOriginalSubmissionAndReturnWay.Path<EnrOriginalSubmissionAndReturnWay>(L_ORIGINAL_SUBMISSION_WAY, this);
            return _originalSubmissionWay;
        }

    /**
     * @return Способ возврата оригиналов документов. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#getOriginalReturnWay()
     */
        public EnrOriginalSubmissionAndReturnWay.Path<EnrOriginalSubmissionAndReturnWay> originalReturnWay()
        {
            if(_originalReturnWay == null )
                _originalReturnWay = new EnrOriginalSubmissionAndReturnWay.Path<EnrOriginalSubmissionAndReturnWay>(L_ORIGINAL_RETURN_WAY, this);
            return _originalReturnWay;
        }

    /**
     * @return Категории лиц, обладающих правом сдавать общеобразовательные ВИ по материалам ОУ.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#getInternalExamReason()
     */
        public EnrInternalExamReason.Path<EnrInternalExamReason> internalExamReason()
        {
            if(_internalExamReason == null )
                _internalExamReason = new EnrInternalExamReason.Path<EnrInternalExamReason>(L_INTERNAL_EXAM_REASON, this);
            return _internalExamReason;
        }

    /**
     * @return Абитуриент, поступающий по международному договору. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#isInternationalTreatyContractor()
     */
        public PropertyPath<Boolean> internationalTreatyContractor()
        {
            if(_internationalTreatyContractor == null )
                _internationalTreatyContractor = new PropertyPath<Boolean>(EnrEntrantRequestGen.P_INTERNATIONAL_TREATY_CONTRACTOR, this);
            return _internationalTreatyContractor;
        }

    /**
     * @return Выпускник общеобразовательной организации. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#isGeneralEduSchool()
     */
        public PropertyPath<Boolean> generalEduSchool()
        {
            if(_generalEduSchool == null )
                _generalEduSchool = new PropertyPath<Boolean>(EnrEntrantRequestGen.P_GENERAL_EDU_SCHOOL, this);
            return _generalEduSchool;
        }

    /**
     * @return Получает образование данного уровня впервые. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#isReceiveEduLevelFirst()
     */
        public PropertyPath<Boolean> receiveEduLevelFirst()
        {
            if(_receiveEduLevelFirst == null )
                _receiveEduLevelFirst = new PropertyPath<Boolean>(EnrEntrantRequestGen.P_RECEIVE_EDU_LEVEL_FIRST, this);
            return _receiveEduLevelFirst;
        }

    /**
     * @return Принял заявление (контекст принципала).
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#getAcceptPrincipalContext()
     */
        public IPrincipalContextGen.Path<IPrincipalContext> acceptPrincipalContext()
        {
            if(_acceptPrincipalContext == null )
                _acceptPrincipalContext = new IPrincipalContextGen.Path<IPrincipalContext>(L_ACCEPT_PRINCIPAL_CONTEXT, this);
            return _acceptPrincipalContext;
        }

    /**
     * кэширует loginedTitle принципала
     *
     * @return Принял заявление.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#getAcceptRequest()
     */
        public PropertyPath<String> acceptRequest()
        {
            if(_acceptRequest == null )
                _acceptRequest = new PropertyPath<String>(EnrEntrantRequestGen.P_ACCEPT_REQUEST, this);
            return _acceptRequest;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#getStringNumber()
     */
        public SupportedPropertyPath<String> stringNumber()
        {
            if(_stringNumber == null )
                _stringNumber = new SupportedPropertyPath<String>(EnrEntrantRequestGen.P_STRING_NUMBER, this);
            return _stringNumber;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequest#getStringNumberWithEcTitle()
     */
        public SupportedPropertyPath<String> stringNumberWithEcTitle()
        {
            if(_stringNumberWithEcTitle == null )
                _stringNumberWithEcTitle = new SupportedPropertyPath<String>(EnrEntrantRequestGen.P_STRING_NUMBER_WITH_EC_TITLE, this);
            return _stringNumberWithEcTitle;
        }

        public Class getEntityClass()
        {
            return EnrEntrantRequest.class;
        }

        public String getEntityName()
        {
            return "enrEntrantRequest";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getStringNumber();

    public abstract String getStringNumberWithEcTitle();
}
