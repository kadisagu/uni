/**
 *$Id: EnrExamRoomList.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.exams.bo.EnrExamRoom.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.common.IIdentifiable;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.core.view.util.Icon;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.exams.bo.EnrExamRoom.EnrExamRoomManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.logic.EnrOrgUnitBaseDSHandler;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignExamRoom;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 20.05.13
 */
@Configuration
public class EnrExamRoomList extends BusinessComponentManager
{
    public static final String ENR_CAMP_SELECT_DS = EnrEnrollmentCampaignManager.DS_ENR_CAMPAIGN;
    public static final String ORG_UNIT_DS = "orgUnitDS";
    public static final String EXAM_ROOM_SEARCH_DS = "examRoomSearchDS";

    public static final String BIND_TERR_ORG_UNIT = "orgUnit";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
                .addDataSource(selectDS(ORG_UNIT_DS, orgUnitDSHandler()).addColumn(EnrOrgUnit.institutionOrgUnit().orgUnit().shortTitleWithTopEmphasized().s()))
                .addDataSource(searchListDS(EXAM_ROOM_SEARCH_DS, examRoomSearchDSColumns(), examRoomSearchDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint examRoomSearchDSColumns()
    {
        return columnListExtPointBuilder(EXAM_ROOM_SEARCH_DS)
                .addColumn(textColumn("title", EnrCampaignExamRoom.place().displayableTitle()).required(true))
                .addColumn(textColumn("size", EnrCampaignExamRoom.size()))
                .addColumn(textColumn("roomType", EnrCampaignExamRoom.examRoomType().title()))
                .addColumn(indicatorColumn("forDisabledPeople", EnrCampaignExamRoom.preparedForDisabledPeople())
                        .addIndicator(true, new IndicatorColumn.Item(IndicatorColumn.YES))
                        .addIndicator(false, new IndicatorColumn.Item(IndicatorColumn.NO)))
                .addColumn(textColumn("comment", EnrCampaignExamRoom.comment()))
                .addColumn(textColumn("location", EnrCampaignExamRoom.place().fullLocationInfo()))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, new Icon(EDIT_COLUMN_NAME), "onClickEditExamRoom")
                    .permissionKey("enr14ExamRoomListEditRoom"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, new Icon(DELETE_COLUMN_NAME), "onClickDeleteExamRoom")
                    .alert(new FormattedMessage("examRoomSearchDS.delete.alert"))
                    .permissionKey("enr14ExamRoomListDeleteRoom"))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> examRoomSearchDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrCampaignExamRoom.class)
                .where(EnrCampaignExamRoom.enrollmentCampaign(), EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN)
                .where(EnrCampaignExamRoom.responsibleOrgUnit(), BIND_TERR_ORG_UNIT)
                .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler orgUnitDSHandler()
    {
        return new EnrOrgUnitBaseDSHandler(getName())
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                DQLSelectBuilder psOuBuilder = new DQLSelectBuilder().fromEntity(EnrProgramSetOrgUnit.class, "d")
                    .column(property(EnrProgramSetOrgUnit.orgUnit().id().fromAlias("d")))
                    .where(eq(property(EnrProgramSetOrgUnit.programSet().enrollmentCampaign().fromAlias("d")), value(context.<IIdentifiable>get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN))));

                dql.where(in(property(EnrOrgUnit.id().fromAlias(alias)), psOuBuilder.buildQuery()));
            }
        }
            .where(EnrOrgUnit.enrollmentCampaign(), EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
    }
}
