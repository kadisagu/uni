/* $Id:$ */
package ru.tandemservice.unienr14.extreports.externalView;

import org.tandemframework.core.entity.dsl.PropertyPath;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLCaseExpressionBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.IDQLExpression;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.concat;
import static org.tandemframework.hibsupport.dql.DQLFunctions.substring;

/**
 * @author oleyba
 * @since 8/27/13
 */
public class EnrExtViewUtil
{
    public static IDQLExpression yesNoColumn(PropertyPath<Boolean> dsl)
    {
        return new DQLCaseExpressionBuilder().when(eq(property(dsl), value(Boolean.TRUE)), value("да")).otherwise(value("нет")).build();
    }

    public static IDQLExpression asLongColumn(PropertyPath<Long> dsl)
    {
        return DQLExpressions.mul(property(dsl), value(0.001));
    }

    public static IDQLExpression certNumberColumn(PropertyPath<String> dsl)
    {
        return concat(
            substring(property(dsl), 1, 2), value("-"),
            substring(property(dsl), 3, 9), value("-"),
            substring(property(dsl), 12, 2));
    }

}
