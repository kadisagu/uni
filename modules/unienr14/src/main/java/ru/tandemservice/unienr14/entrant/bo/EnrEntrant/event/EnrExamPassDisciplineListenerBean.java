/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.event;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 6/26/13
 */
public class EnrExamPassDisciplineListenerBean
{
    public void init() {
        DSetEventManager.getInstance().registerListener(DSetEventType.beforeDelete, EnrExamPassDiscipline.class, NoRetakeValidate.INSTANCE);
    }

    /**
     * Ненулевые номера должны быть уникальны в рамках ПК.
     */
    public static class NoRetakeValidate implements IDSetEventListener
    {
        public static final NoRetakeValidate INSTANCE = new NoRetakeValidate();

        @Override
        public void onEvent(DSetEvent event)
        {
            final Long countDisciplinesWithRetake = new DQLSelectBuilder()
                .fromDataSource(event.getMultitude().getSource(), "c").column(property("c"))
                .where(eq(property(EnrExamPassDiscipline.retake().fromAlias("c")), value(Boolean.FALSE)))
                .where(exists(new DQLSelectBuilder()
                    .fromEntity(EnrExamPassDiscipline.class, "r")
                    .where(eq(property(EnrExamPassDiscipline.entrant().fromAlias("c")), property(EnrExamPassDiscipline.entrant().fromAlias("r"))))
                    .where(eq(property(EnrExamPassDiscipline.discipline().fromAlias("c")), property(EnrExamPassDiscipline.discipline().fromAlias("r"))))
                    .where(eq(property(EnrExamPassDiscipline.passForm().fromAlias("c")), property(EnrExamPassDiscipline.passForm().fromAlias("r"))))
                    .where(eq(property(EnrExamPassDiscipline.retake().fromAlias("r")), value(Boolean.TRUE)))
                    .buildQuery()))
                .createCountStatement(event.getContext()).uniqueResult();
            
            if (countDisciplinesWithRetake > 0)
                throw new ApplicationException(EnrEntrantManager.instance().getProperty("enrExamPassDiscipline.cannotDeleteDisciplineWithRetake"));
        }
    }
    
}
