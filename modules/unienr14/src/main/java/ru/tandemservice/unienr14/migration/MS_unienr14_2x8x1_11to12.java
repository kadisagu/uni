package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;

import java.sql.PreparedStatement;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x8x1_11to12 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrLevelBudgetFinancing

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("enr14_c_lev_budget_financing_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_enrlevelbudgetfinancing"),
				new DBColumn("discriminator", DBType.SHORT).setNullable(false),
				new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
				new DBColumn("shorttitle_p", DBType.createVarchar(255)).setNullable(false),
				new DBColumn("priority_p", DBType.INTEGER).setNullable(false),
				new DBColumn("title_p", DBType.createVarchar(1200))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("enrLevelBudgetFinancing");


            PreparedStatement insert = tool.prepareStatement("insert into enr14_c_lev_budget_financing_t (id, discriminator, code_p, title_p ,shorttitle_p, priority_p) values (?, ?, ?, ?, ?, ?)");
            String[] titles = {"Федеральный бюджет", "Бюджет субъекта РФ", "Местный бюджет"};
            String[] shortTitles = {"федеральный", "региональный", "местный"};

            for (int i = 0; i < titles.length; i++)
            {
                Long id = EntityIDGenerator.generateNewId(entityCode);
                int priority = i + 1;
                String code = String.valueOf(priority);

                insert.setLong(1, id);
                insert.setShort(2, entityCode);
                insert.setString(3, code);
                insert.setString(4, titles[i]);
                insert.setString(5, shortTitles[i]);
                insert.setInt(6, priority);
                insert.executeUpdate();
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrEnrollmentCampaignSettings

        // создано обязательное свойство levelBudgetFinancing
        {
            // создать колонку
            tool.createColumn("enr14_campaign_settings_t", new DBColumn("levelbudgetfinancing_id", DBType.LONG));

            // задать значение по умолчанию всем ПК - "Федеральный бюджет"
            Long defaultLevelBudgetFinancing = (Long) tool.getUniqueResult("select id from enr14_c_lev_budget_financing_t where code_p='1'");
            tool.executeUpdate("update enr14_campaign_settings_t set levelbudgetfinancing_id=? where levelbudgetfinancing_id is null", defaultLevelBudgetFinancing);

            // сделать колонку NOT NULL
            tool.setColumnNullable("enr14_campaign_settings_t", "levelbudgetfinancing_id", false);
        }
    }
}