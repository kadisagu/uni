/* $Id$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.util;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.shared.person.base.entity.IdentityCard;

/**
 * @author nvankov
 * @since 6/5/14
 */
public class IdentityCardOption extends DataWrapper
{
    private IdentityCard _identityCard;
    private boolean _onlineEntrant;

    public IdentityCardOption(Long id, String title, IdentityCard identityCard, boolean isOnlineEntrant)
    {
        super(id, title);
        _identityCard = identityCard;
        _onlineEntrant = isOnlineEntrant;
    }

    public IdentityCard getIdentityCard()
    {
        return _identityCard;
    }

    public boolean isOnlineEntrant()
    {
        return _onlineEntrant;
    }
}
