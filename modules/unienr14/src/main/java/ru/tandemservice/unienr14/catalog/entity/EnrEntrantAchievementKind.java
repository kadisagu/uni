package ru.tandemservice.unienr14.catalog.entity;

import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.BaseDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import ru.tandemservice.unienr14.catalog.bo.EnrCatalogCommons.ui.AchievementKindAddEdit.EnrCatalogCommonsAchievementKindAddEdit;
import ru.tandemservice.unienr14.catalog.entity.gen.EnrEntrantAchievementKindGen;

/** @see ru.tandemservice.unienr14.catalog.entity.gen.EnrEntrantAchievementKindGen */
public class EnrEntrantAchievementKind extends EnrEntrantAchievementKindGen implements IDynamicCatalogItem
{
    public static final int MARK_LIMIT = 10;

    public static IDynamicCatalogDesc getUiDesc() {
        return new BaseDynamicCatalogDesc() {
            @Override public String getAddEditComponentName() {
                return EnrCatalogCommonsAchievementKindAddEdit.class.getSimpleName();
            }
        };
    }
}
