package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x3_11to12 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
        {
            new ScriptDependency("org.tandemframework", "1.6.15"),
            new ScriptDependency("org.tandemframework.shared", "1.6.3")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrEnrollmentStep

        // создано обязательное свойство version
        {
            // создать колонку
            if (!tool.columnExists("enr14_enr_step_t", "version")) {
                tool.createColumn("enr14_enr_step_t", new DBColumn("version", DBType.INTEGER));

                // задать значение по умолчанию
                java.lang.Integer defaultVersion = 1;		// TODO: задайте NOT NULL значение!
                tool.executeUpdate("update enr14_enr_step_t set version=? where version is null", defaultVersion);

                // сделать колонку NOT NULL
                tool.setColumnNullable("enr14_enr_step_t", "version", false);
            }

        }

        // создано свойство zipXmlRecommendationState
        {
            // создать колонку
            if (!tool.columnExists("enr14_enr_step_t", "zipxmlrecommendationstate_p")) {
                tool.createColumn("enr14_enr_step_t", new DBColumn("zipxmlrecommendationstate_p", DBType.BLOB));
            }
        }

        // создано свойство zipXmlEnrollmentState
        {
            // создать колонку
            if (!tool.columnExists("enr14_enr_step_t", "zipxmlenrollmentstate_p")) {
                tool.createColumn("enr14_enr_step_t", new DBColumn("zipxmlenrollmentstate_p", DBType.BLOB));
            }
        }


    }
}