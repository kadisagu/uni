/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.utils;

import org.tandemframework.core.context.UserContext;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.List;

/**
 * @author rsizonenko
 * @since 29.05.2014
 * @deprecated для mvc использовать EnrEnrollmentCampaignManager.instance().getEnrollmentCampaignModel(), для caf EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig()
 */
@Deprecated
public class EnrReportEnrollmentCampaignSelector {

    public EnrReportEnrollmentCampaignSelector() {
    }

    private EnrEnrollmentCampaign _enrollmentCampaign;
    private List<EnrEnrollmentCampaign> _enrollmentCampaignList = EnrEnrollmentCampaignManager.instance().enrCampaignPermissionsDao().getEnrollmentCampaignList(UserContext.getInstance().getPrincipalContext());

    public void setEnrollmentCampaignFromDefault() {
        setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
    }

    public EnrEnrollmentCampaign getEnrollmentCampaign() {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrEnrollmentCampaign _enrollmentCampaign) {
        this._enrollmentCampaign = _enrollmentCampaign;
    }

    public List<EnrEnrollmentCampaign> getEnrollmentCampaignList() {
        return _enrollmentCampaignList;
    }

    public void setEnrollmentCampaignList(List<EnrEnrollmentCampaign> _enrollmentCampaignList) {
        this._enrollmentCampaignList = _enrollmentCampaignList;
    }
}
