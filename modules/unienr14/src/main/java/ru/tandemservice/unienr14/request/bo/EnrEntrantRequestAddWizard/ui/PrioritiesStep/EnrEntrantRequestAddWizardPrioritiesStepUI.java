/* $Id$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.PrioritiesStep;

import com.google.common.collect.Maps;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.ContextLocal;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PriorityEdit.EnrEntrantPriorityEditUI;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.EnrEntrantRequestAddWizardWizardUI;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.EnrEntrantWizardState;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.IWizardStep;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;

import java.util.Map;

/**
 * @author nvankov
 * @since 6/5/14
 */
@Input( {
    @Bind(key = EnrEntrantRequestAddWizardWizardUI.PARAM_WIZARD_STATE, binding = "state")
})
public class EnrEntrantRequestAddWizardPrioritiesStepUI extends EnrEntrantPriorityEditUI implements IWizardStep
{
    private EnrEntrantWizardState _state;

    @Override
    public void onComponentRefresh()
    {
        setRequestType(IUniBaseDao.instance.get().get(EnrRequestType.class, getState().getRequestTypeId()));
        prepareReqComRowList();
    }

    @Override
    public void saveStepData()
    {
        try
        {
            Map<EnrRequestedCompetition, Map<EnrProgramSetItem, Integer>> priorityMap = Maps.newHashMap();
            if (validate(priorityMap).hasErrors()) return;
            saveFormData(priorityMap);
        }
        catch (Exception e)
        {
            ContextLocal.getSite().getDesktop().setRefreshScheduled(true);
            throw CoreExceptionUtils.getRuntimeException(e);
        }
    }

    // getters and setters

    public EnrEntrantWizardState getState()
    {
        return _state;
    }

    public void setState(EnrEntrantWizardState state)
    {
        _state = state;
    }


}
