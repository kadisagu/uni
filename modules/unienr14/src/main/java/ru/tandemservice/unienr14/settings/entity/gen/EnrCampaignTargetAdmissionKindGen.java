package ru.tandemservice.unienr14.settings.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.catalog.entity.EnrTargetAdmissionKind;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignTargetAdmissionKind;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Вид ЦП, используемый в рамках ПК
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrCampaignTargetAdmissionKindGen extends EntityBase
 implements INaturalIdentifiable<EnrCampaignTargetAdmissionKindGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.settings.entity.EnrCampaignTargetAdmissionKind";
    public static final String ENTITY_NAME = "enrCampaignTargetAdmissionKind";
    public static final int VERSION_HASH = -1583147019;
    private static IEntityMeta ENTITY_META;

    public static final String L_TARGET_ADMISSION_KIND = "targetAdmissionKind";
    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_STATE_INTEREST = "stateInterest";

    private EnrTargetAdmissionKind _targetAdmissionKind;     // Вид ЦП
    private EnrEnrollmentCampaign _enrollmentCampaign;     // ПК
    private boolean _stateInterest = false;     // Прием в интересах государства

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Вид ЦП. Свойство не может быть null.
     */
    @NotNull
    public EnrTargetAdmissionKind getTargetAdmissionKind()
    {
        return _targetAdmissionKind;
    }

    /**
     * @param targetAdmissionKind Вид ЦП. Свойство не может быть null.
     */
    public void setTargetAdmissionKind(EnrTargetAdmissionKind targetAdmissionKind)
    {
        dirty(_targetAdmissionKind, targetAdmissionKind);
        _targetAdmissionKind = targetAdmissionKind;
    }

    /**
     * @return ПК. Свойство не может быть null.
     */
    @NotNull
    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign ПК. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Прием в интересах государства. Свойство не может быть null.
     */
    @NotNull
    public boolean isStateInterest()
    {
        return _stateInterest;
    }

    /**
     * @param stateInterest Прием в интересах государства. Свойство не может быть null.
     */
    public void setStateInterest(boolean stateInterest)
    {
        dirty(_stateInterest, stateInterest);
        _stateInterest = stateInterest;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrCampaignTargetAdmissionKindGen)
        {
            if (withNaturalIdProperties)
            {
                setTargetAdmissionKind(((EnrCampaignTargetAdmissionKind)another).getTargetAdmissionKind());
                setEnrollmentCampaign(((EnrCampaignTargetAdmissionKind)another).getEnrollmentCampaign());
            }
            setStateInterest(((EnrCampaignTargetAdmissionKind)another).isStateInterest());
        }
    }

    public INaturalId<EnrCampaignTargetAdmissionKindGen> getNaturalId()
    {
        return new NaturalId(getTargetAdmissionKind(), getEnrollmentCampaign());
    }

    public static class NaturalId extends NaturalIdBase<EnrCampaignTargetAdmissionKindGen>
    {
        private static final String PROXY_NAME = "EnrCampaignTargetAdmissionKindNaturalProxy";

        private Long _targetAdmissionKind;
        private Long _enrollmentCampaign;

        public NaturalId()
        {}

        public NaturalId(EnrTargetAdmissionKind targetAdmissionKind, EnrEnrollmentCampaign enrollmentCampaign)
        {
            _targetAdmissionKind = ((IEntity) targetAdmissionKind).getId();
            _enrollmentCampaign = ((IEntity) enrollmentCampaign).getId();
        }

        public Long getTargetAdmissionKind()
        {
            return _targetAdmissionKind;
        }

        public void setTargetAdmissionKind(Long targetAdmissionKind)
        {
            _targetAdmissionKind = targetAdmissionKind;
        }

        public Long getEnrollmentCampaign()
        {
            return _enrollmentCampaign;
        }

        public void setEnrollmentCampaign(Long enrollmentCampaign)
        {
            _enrollmentCampaign = enrollmentCampaign;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrCampaignTargetAdmissionKindGen.NaturalId) ) return false;

            EnrCampaignTargetAdmissionKindGen.NaturalId that = (NaturalId) o;

            if( !equals(getTargetAdmissionKind(), that.getTargetAdmissionKind()) ) return false;
            if( !equals(getEnrollmentCampaign(), that.getEnrollmentCampaign()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getTargetAdmissionKind());
            result = hashCode(result, getEnrollmentCampaign());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getTargetAdmissionKind());
            sb.append("/");
            sb.append(getEnrollmentCampaign());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrCampaignTargetAdmissionKindGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrCampaignTargetAdmissionKind.class;
        }

        public T newInstance()
        {
            return (T) new EnrCampaignTargetAdmissionKind();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "targetAdmissionKind":
                    return obj.getTargetAdmissionKind();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "stateInterest":
                    return obj.isStateInterest();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "targetAdmissionKind":
                    obj.setTargetAdmissionKind((EnrTargetAdmissionKind) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrEnrollmentCampaign) value);
                    return;
                case "stateInterest":
                    obj.setStateInterest((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "targetAdmissionKind":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "stateInterest":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "targetAdmissionKind":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "stateInterest":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "targetAdmissionKind":
                    return EnrTargetAdmissionKind.class;
                case "enrollmentCampaign":
                    return EnrEnrollmentCampaign.class;
                case "stateInterest":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrCampaignTargetAdmissionKind> _dslPath = new Path<EnrCampaignTargetAdmissionKind>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrCampaignTargetAdmissionKind");
    }
            

    /**
     * @return Вид ЦП. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignTargetAdmissionKind#getTargetAdmissionKind()
     */
    public static EnrTargetAdmissionKind.Path<EnrTargetAdmissionKind> targetAdmissionKind()
    {
        return _dslPath.targetAdmissionKind();
    }

    /**
     * @return ПК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignTargetAdmissionKind#getEnrollmentCampaign()
     */
    public static EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Прием в интересах государства. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignTargetAdmissionKind#isStateInterest()
     */
    public static PropertyPath<Boolean> stateInterest()
    {
        return _dslPath.stateInterest();
    }

    public static class Path<E extends EnrCampaignTargetAdmissionKind> extends EntityPath<E>
    {
        private EnrTargetAdmissionKind.Path<EnrTargetAdmissionKind> _targetAdmissionKind;
        private EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<Boolean> _stateInterest;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Вид ЦП. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignTargetAdmissionKind#getTargetAdmissionKind()
     */
        public EnrTargetAdmissionKind.Path<EnrTargetAdmissionKind> targetAdmissionKind()
        {
            if(_targetAdmissionKind == null )
                _targetAdmissionKind = new EnrTargetAdmissionKind.Path<EnrTargetAdmissionKind>(L_TARGET_ADMISSION_KIND, this);
            return _targetAdmissionKind;
        }

    /**
     * @return ПК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignTargetAdmissionKind#getEnrollmentCampaign()
     */
        public EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Прием в интересах государства. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignTargetAdmissionKind#isStateInterest()
     */
        public PropertyPath<Boolean> stateInterest()
        {
            if(_stateInterest == null )
                _stateInterest = new PropertyPath<Boolean>(EnrCampaignTargetAdmissionKindGen.P_STATE_INTEREST, this);
            return _stateInterest;
        }

        public Class getEntityClass()
        {
            return EnrCampaignTargetAdmissionKind.class;
        }

        public String getEntityName()
        {
            return "enrCampaignTargetAdmissionKind";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
