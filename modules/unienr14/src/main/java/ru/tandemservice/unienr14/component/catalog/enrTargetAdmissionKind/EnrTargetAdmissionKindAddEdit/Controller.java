/**
 *$Id: Controller.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.component.catalog.enrTargetAdmissionKind.EnrTargetAdmissionKindAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditController;
import ru.tandemservice.unienr14.catalog.entity.EnrTargetAdmissionKind;

/**
 * @author Alexander Shaburov
 * @since 12.04.13
 */
public class Controller extends DefaultCatalogAddEditController<EnrTargetAdmissionKind, Model, IDAO>
{
}
