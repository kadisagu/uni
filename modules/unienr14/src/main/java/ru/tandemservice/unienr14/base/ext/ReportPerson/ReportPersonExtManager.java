/**
 *$Id: ReportPersonExtManager.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.base.ext.ReportPerson;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Alexander Shaburov
 * @since 19.06.13
 */
@Configuration
public class ReportPersonExtManager extends BusinessObjectExtensionManager
{
}
