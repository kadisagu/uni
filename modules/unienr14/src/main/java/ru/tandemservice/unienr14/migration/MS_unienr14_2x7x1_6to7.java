/* $Id:$ */
package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author oleyba
 * @since 2/17/15
 */
public class MS_unienr14_2x7x1_6to7 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        if (!tool.columnExists("enr14_ta_plan_t", "planpo_p"))
            return;

        // гарантировать наличие кода сущности
        short entityCode = tool.entityCodes().ensure("enrTargetAdmissionCompetitionPlan");

        Map<Long, List<Wrapper>> wrapperMap = new HashMap<>();

        Statement stmt = tool.getConnection().createStatement();
        ResultSet rs = stmt.executeQuery("select id, programsetorgunit_id, planpo_p, plansoo_p, calculatedplanpo_p, calculatedplansoo_p, plan_p from enr14_ta_plan_t");
        while(rs.next())
        {
            Wrapper w = new Wrapper();
            w.id = rs.getLong(1);
            w.psId = rs.getLong(2);
            w.planPO = rs.getInt(3);
            w.planSOO = rs.getInt(4);
            w.calculatedPlanPO = rs.getInt(5);
            w.calculatedPlanSOO = rs.getInt(6);
            w.plan = rs.getInt(7);

            SafeMap.safeGet(wrapperMap, w.psId, ArrayList.class).add(w);
        }

        rs = stmt.executeQuery("select c.id, c.programsetorgunit_id, r.code_p " +
            " from enr14_competition_t c " +
            " inner join enr14_c_comp_type_t t on c.type_id = t.id " +
            " inner join enr14_c_lev_req_t r on c.edulevelrequirement_id = r.id" +
            " where " +
            " t.code_p = '03'");
        while(rs.next())
        {
            Long id = rs.getLong(1);
            Long psId = rs.getLong(2);
            String r = rs.getString(3);

            List<Wrapper> wrappers = wrapperMap.get(psId);
            if (null == wrappers) continue;

            for (Wrapper w : wrappers) {
                if ("3".equals(r)) {
                    w.compPO = id;
                } else if ("2".equals(r)) {
                    w.compSOO = id;
                } else {
                    w.comp = id;
                }
            }
        }

        PreparedStatement insert = tool.prepareStatement("insert into enr14_ta_plan_comp_t (id, discriminator,targetadmissionplan_id,competition_id,plan_p,calculatedplan_p) values (?, ?, ?, ?, ?,?)");
        for (List<Wrapper> wrappers : wrapperMap.values()) {
            for (Wrapper w : wrappers) {

                if (w.compPO != null) {
                    Long insertId = EntityIDGenerator.generateNewId(entityCode);
                    insert.setLong(1, insertId);
                    insert.setShort(2, entityCode);
                    insert.setLong(3, w.id);
                    insert.setLong(4, w.compPO);
                    insert.setInt(5, w.planPO);
                    insert.setInt(6, w.calculatedPlanPO);
                    insert.executeUpdate();
                }

                if (w.compSOO != null) {
                    Long insertId = EntityIDGenerator.generateNewId(entityCode);
                    insert.setLong(1, insertId);
                    insert.setShort(2, entityCode);
                    insert.setLong(3, w.id);
                    insert.setLong(4, w.compSOO);
                    insert.setInt(5, w.planSOO);
                    insert.setInt(6, w.calculatedPlanSOO);
                    insert.executeUpdate();
                }

                if (w.comp != null) {
                    Long insertId = EntityIDGenerator.generateNewId(entityCode);
                    insert.setLong(1, insertId);
                    insert.setShort(2, entityCode);
                    insert.setLong(3, w.id);
                    insert.setLong(4, w.comp);
                    insert.setInt(5, w.plan);
                    insert.setInt(6, w.plan);
                    insert.executeUpdate();
                }
            }
        }


        tool.dropColumn("enr14_ta_plan_t", "planpo_p");
        tool.dropColumn("enr14_ta_plan_t", "plansoo_p");
        tool.dropColumn("enr14_ta_plan_t", "calculatedplanpo_p");
        tool.dropColumn("enr14_ta_plan_t", "calculatedplansoo_p");
    }

    private class Wrapper {
        private Long id;
        private Long psId;

        private int plan;
        private int planPO;
        private int planSOO;
        private int calculatedPlanPO;
        private int calculatedPlanSOO;

        private Long comp;
        private Long compPO;
        private Long compSOO;
    }
}
