package ru.tandemservice.unienr14.programAlloc.entity;

import ru.tandemservice.unienr14.catalog.entity.EnrTargetAdmissionKind;
import ru.tandemservice.unienr14.programAlloc.entity.gen.*;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA;

/**
 * Абитуриент в распределении по ОП
 */
public class EnrProgramAllocationItem extends EnrProgramAllocationItemGen
{
    public EnrTargetAdmissionKind getTargetAdmissionKind() {
        if (getEntrant() instanceof EnrRequestedCompetitionTA) {
           return ((EnrRequestedCompetitionTA)getEntrant()).getTargetAdmissionKind().getTargetAdmissionKind();
        }
        return null;
    }
}