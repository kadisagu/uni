package ru.tandemservice.unienr14.settings.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementSettings;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Настройки индивидуальных достижений в ПК
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrEntrantAchievementSettingsGen extends EntityBase
 implements INaturalIdentifiable<EnrEntrantAchievementSettingsGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementSettings";
    public static final String ENTITY_NAME = "enrEntrantAchievementSettings";
    public static final int VERSION_HASH = -1806144474;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String L_REQUEST_TYPE = "requestType";
    public static final String P_MAX_TOTAL_ACHIEVEMENT_MARK_AS_LONG = "maxTotalAchievementMarkAsLong";
    public static final String P_MAX_TOTAL_ACHIEVEMENT_MARK = "maxTotalAchievementMark";

    private EnrEnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private EnrRequestType _requestType;     // Вид заявления
    private long _maxTotalAchievementMarkAsLong;     // Ограничение на сумму баллов индивидуальных достижений

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Вид заявления. Свойство не может быть null.
     */
    @NotNull
    public EnrRequestType getRequestType()
    {
        return _requestType;
    }

    /**
     * @param requestType Вид заявления. Свойство не может быть null.
     */
    public void setRequestType(EnrRequestType requestType)
    {
        dirty(_requestType, requestType);
        _requestType = requestType;
    }

    /**
     * Хранится со смещением в три знака для дробной части.
     * Задает максимальное значение суммы баллов всех достижений абитуриента, за исключением итогового сочинения.
     *
     * @return Ограничение на сумму баллов индивидуальных достижений. Свойство не может быть null.
     */
    @NotNull
    public long getMaxTotalAchievementMarkAsLong()
    {
        return _maxTotalAchievementMarkAsLong;
    }

    /**
     * @param maxTotalAchievementMarkAsLong Ограничение на сумму баллов индивидуальных достижений. Свойство не может быть null.
     */
    public void setMaxTotalAchievementMarkAsLong(long maxTotalAchievementMarkAsLong)
    {
        dirty(_maxTotalAchievementMarkAsLong, maxTotalAchievementMarkAsLong);
        _maxTotalAchievementMarkAsLong = maxTotalAchievementMarkAsLong;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrEntrantAchievementSettingsGen)
        {
            if (withNaturalIdProperties)
            {
                setEnrollmentCampaign(((EnrEntrantAchievementSettings)another).getEnrollmentCampaign());
                setRequestType(((EnrEntrantAchievementSettings)another).getRequestType());
            }
            setMaxTotalAchievementMarkAsLong(((EnrEntrantAchievementSettings)another).getMaxTotalAchievementMarkAsLong());
        }
    }

    public INaturalId<EnrEntrantAchievementSettingsGen> getNaturalId()
    {
        return new NaturalId(getEnrollmentCampaign(), getRequestType());
    }

    public static class NaturalId extends NaturalIdBase<EnrEntrantAchievementSettingsGen>
    {
        private static final String PROXY_NAME = "EnrEntrantAchievementSettingsNaturalProxy";

        private Long _enrollmentCampaign;
        private Long _requestType;

        public NaturalId()
        {}

        public NaturalId(EnrEnrollmentCampaign enrollmentCampaign, EnrRequestType requestType)
        {
            _enrollmentCampaign = ((IEntity) enrollmentCampaign).getId();
            _requestType = ((IEntity) requestType).getId();
        }

        public Long getEnrollmentCampaign()
        {
            return _enrollmentCampaign;
        }

        public void setEnrollmentCampaign(Long enrollmentCampaign)
        {
            _enrollmentCampaign = enrollmentCampaign;
        }

        public Long getRequestType()
        {
            return _requestType;
        }

        public void setRequestType(Long requestType)
        {
            _requestType = requestType;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrEntrantAchievementSettingsGen.NaturalId) ) return false;

            EnrEntrantAchievementSettingsGen.NaturalId that = (NaturalId) o;

            if( !equals(getEnrollmentCampaign(), that.getEnrollmentCampaign()) ) return false;
            if( !equals(getRequestType(), that.getRequestType()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEnrollmentCampaign());
            result = hashCode(result, getRequestType());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEnrollmentCampaign());
            sb.append("/");
            sb.append(getRequestType());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrEntrantAchievementSettingsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrEntrantAchievementSettings.class;
        }

        public T newInstance()
        {
            return (T) new EnrEntrantAchievementSettings();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "requestType":
                    return obj.getRequestType();
                case "maxTotalAchievementMarkAsLong":
                    return obj.getMaxTotalAchievementMarkAsLong();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrEnrollmentCampaign) value);
                    return;
                case "requestType":
                    obj.setRequestType((EnrRequestType) value);
                    return;
                case "maxTotalAchievementMarkAsLong":
                    obj.setMaxTotalAchievementMarkAsLong((Long) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "requestType":
                        return true;
                case "maxTotalAchievementMarkAsLong":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "requestType":
                    return true;
                case "maxTotalAchievementMarkAsLong":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrollmentCampaign":
                    return EnrEnrollmentCampaign.class;
                case "requestType":
                    return EnrRequestType.class;
                case "maxTotalAchievementMarkAsLong":
                    return Long.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrEntrantAchievementSettings> _dslPath = new Path<EnrEntrantAchievementSettings>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrEntrantAchievementSettings");
    }
            

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementSettings#getEnrollmentCampaign()
     */
    public static EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Вид заявления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementSettings#getRequestType()
     */
    public static EnrRequestType.Path<EnrRequestType> requestType()
    {
        return _dslPath.requestType();
    }

    /**
     * Хранится со смещением в три знака для дробной части.
     * Задает максимальное значение суммы баллов всех достижений абитуриента, за исключением итогового сочинения.
     *
     * @return Ограничение на сумму баллов индивидуальных достижений. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementSettings#getMaxTotalAchievementMarkAsLong()
     */
    public static PropertyPath<Long> maxTotalAchievementMarkAsLong()
    {
        return _dslPath.maxTotalAchievementMarkAsLong();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementSettings#getMaxTotalAchievementMark()
     */
    public static SupportedPropertyPath<Double> maxTotalAchievementMark()
    {
        return _dslPath.maxTotalAchievementMark();
    }

    public static class Path<E extends EnrEntrantAchievementSettings> extends EntityPath<E>
    {
        private EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> _enrollmentCampaign;
        private EnrRequestType.Path<EnrRequestType> _requestType;
        private PropertyPath<Long> _maxTotalAchievementMarkAsLong;
        private SupportedPropertyPath<Double> _maxTotalAchievementMark;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementSettings#getEnrollmentCampaign()
     */
        public EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Вид заявления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementSettings#getRequestType()
     */
        public EnrRequestType.Path<EnrRequestType> requestType()
        {
            if(_requestType == null )
                _requestType = new EnrRequestType.Path<EnrRequestType>(L_REQUEST_TYPE, this);
            return _requestType;
        }

    /**
     * Хранится со смещением в три знака для дробной части.
     * Задает максимальное значение суммы баллов всех достижений абитуриента, за исключением итогового сочинения.
     *
     * @return Ограничение на сумму баллов индивидуальных достижений. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementSettings#getMaxTotalAchievementMarkAsLong()
     */
        public PropertyPath<Long> maxTotalAchievementMarkAsLong()
        {
            if(_maxTotalAchievementMarkAsLong == null )
                _maxTotalAchievementMarkAsLong = new PropertyPath<Long>(EnrEntrantAchievementSettingsGen.P_MAX_TOTAL_ACHIEVEMENT_MARK_AS_LONG, this);
            return _maxTotalAchievementMarkAsLong;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementSettings#getMaxTotalAchievementMark()
     */
        public SupportedPropertyPath<Double> maxTotalAchievementMark()
        {
            if(_maxTotalAchievementMark == null )
                _maxTotalAchievementMark = new SupportedPropertyPath<Double>(EnrEntrantAchievementSettingsGen.P_MAX_TOTAL_ACHIEVEMENT_MARK, this);
            return _maxTotalAchievementMark;
        }

        public Class getEntityClass()
        {
            return EnrEntrantAchievementSettings.class;
        }

        public String getEntityName()
        {
            return "enrEntrantAchievementSettings";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract double getMaxTotalAchievementMark();
}
