/* $Id$ */
package ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.ItemOrgUnitPlanTab.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLDataSource;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.ItemOrgUnitPlanTab.EnrProgramSetItemOrgUnitPlanTabUI;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItemOrgUnitPlan;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author azhebko
 * @since 01.08.2014
 */
public class ProgramSetItemDSHandler extends DefaultSearchDataSourceHandler
{
    public ProgramSetItemDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        IUniBaseDao dao = IUniBaseDao.instance.get();
        EnrProgramSetOrgUnit orgUnit = dao.getNotNull(context.<Long>get("orgUnit"));

        IDQLDataSource planSubQuery = new DQLSelectBuilder()
            .fromEntity(EnrProgramSetItemOrgUnitPlan.class, "p")
            .column(property("p", EnrProgramSetItemOrgUnitPlan.programSetItem().id()), "id")
            .column(property("p", EnrProgramSetItemOrgUnitPlan.plan()), "plan")
            .where(eq(property("p", EnrProgramSetItemOrgUnitPlan.programSetOrgUnit().id()), value(orgUnit.getId())))
            .buildQuery();

        // нужен для сортировки по колонкам
        IDQLDataSource dataSource = new DQLSelectBuilder()
            .fromEntity(EnrProgramSetItem.class, "e")
            .joinDataSource("e", DQLJoinType.left, planSubQuery, "s", eq(property("e", EnrProgramSetItem.id()), property("s.id")))
            .column(property("e", EnrProgramSetItem.id()), "id")
            .column(property("s", "plan"), "plan")
            .column(property("e", EnrProgramSetItem.program().title()), "title")
            .where(eq(property("e", EnrProgramSetItem.programSet().id()), value(orgUnit.getProgramSet().getId())))
            .buildQuery();

        DQLOrderDescriptionRegistry orderDescription = new DQLOrderDescriptionRegistry(EnrProgramSetItem.class, "e");
        DQLSelectBuilder builder = orderDescription.buildDQLSelectBuilder()
            .joinDataSource("e", DQLJoinType.inner, dataSource, "s", eq(property("e", EnrProgramSetItem.id()), property("s", "id")))
            .column(property("s", "id"))
            .column(property("s", "title"))
            .column(property("s", "plan"));

        orderDescription
            .setOrders("title", new OrderDescription("s", "title"))
            .setOrders("plan", new OrderDescription("s", "plan"));

        orderDescription.applyOrder(builder, input.getEntityOrder());

        builder.order(property("s", "title"), OrderDirection.asc);

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).order().pageable(false).build()
                .transform((Object[] row) -> new EnrProgramSetItemOrgUnitPlanTabUI.ProgramSetItemWrapper((Long) row[0], (String) row[1], (Integer) row[2]));
    }
}