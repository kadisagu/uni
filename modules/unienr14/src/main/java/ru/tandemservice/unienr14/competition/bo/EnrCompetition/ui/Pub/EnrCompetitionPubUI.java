/* $Id:$ */
package ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.Pub;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.LongAsDoubleFormatter;
import org.tandemframework.core.view.formatter.NewLineFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrRequestTypeCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.logic.EnrRatingItemDSHandler;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetSecondary;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.ui.CompetitionPub.EnrEnrollmentStepCompetitionPubUI;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItem;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.EnrExamSetManager;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.logic.IEnrExamSetDao;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


/**
 * @author oleyba
 * @since 3/17/14
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "competition.id")
})
public class EnrCompetitionPubUI extends UIPresenter
{
    private EnrCompetition competition = new EnrCompetition();

    private String programTitles;
    private String examSetTitle;

    private List<EnrEnrollmentStep> stepList;
    private EnrEnrollmentStep step;

    @Override
    public void onComponentRefresh()
    {
        setCompetition(IUniBaseDao.instance.get().getNotNull(EnrCompetition.class, getCompetition().getId()));

        List<EduProgramHigherProf> programList = new ArrayList<EduProgramHigherProf>();
        EnrProgramSetBase programSet = getCompetition().getProgramSetOrgUnit().getProgramSet();
        if (programSet instanceof EnrProgramSetSecondary)
        {
            setProgramTitles(((EnrProgramSetSecondary) programSet).getProgram().getTitle());

        } else
        {
            for (EnrProgramSetItem item : IUniBaseDao.instance.get().getList(EnrProgramSetItem.class, EnrProgramSetItem.programSet(), programSet))
                programList.add(item.getProgram());
            setProgramTitles(NewLineFormatter.NOBR_IN_LINES.format(StringUtils.join(CollectionUtils.collect(programList, EduProgramProf::getTitleAndConditions), "\n")));
        }

        Long examSetVariant = getCompetition().getExamSetVariant().getId();
        IEnrExamSetDao.IExamSetSettings examSetSettings = EnrExamSetManager.instance().dao().getExamSetContent(Collections.singleton(examSetVariant)).get(examSetVariant);
        if (examSetSettings != null) {
            setExamSetTitle(examSetSettings.getTitle());
        }

        DQLSelectBuilder stepDQL = new DQLSelectBuilder()
            .fromEntity(EnrEnrollmentStep.class, "s").column("s").order(property(EnrEnrollmentStep.enrollmentDate().fromAlias("s")))
            .where(exists(new DQLSelectBuilder()
                .fromEntity(EnrEnrollmentStepItem.class, "i").column("i.id")
                .where(eq(property(EnrEnrollmentStepItem.step().fromAlias("i")), property("s")))
                .where(eq(property(EnrEnrollmentStepItem.entity().requestedCompetition().competition().fromAlias("i")), value(getCompetition())))
                .buildQuery()))
            ;
        setStepList(stepDQL.createStatement(_uiSupport.getSession()).<EnrEnrollmentStep>list());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrRatingItemDSHandler.BIND_COMPETITION, getCompetition());
    }

    public Map getEnrollmentPubParams() {
        if (getStep() == null) return null;
        return new ParametersMap()
            .add(EnrEnrollmentStepCompetitionPubUI.PARAM_STEP, getStep().getId())
            .add(EnrEnrollmentStepCompetitionPubUI.PARAM_COMPETITION, getCompetition().getId());
    }

    public boolean isBakSpecMag()
    {
        String code = getCompetition().getRequestType().getCode();
        return code.equals(EnrRequestTypeCodes.BS) || code.equals(EnrRequestTypeCodes.MASTER);
    }

    public boolean isTrainInter()
    {
        String code = getCompetition().getRequestType().getCode();
        return EnrRequestTypeCodes.INTERNSHIP.equals(code) || EnrRequestTypeCodes.TRAINEESHIP.equals(code);
    }

    public boolean isSpo()
    {
        return EnrRequestTypeCodes.SPO.equals(getCompetition().getRequestType().getCode());
    }

    public String getTotalMark()
    {
        DataWrapper wrapper = _uiConfig.getDataSourceCurrentRecord(EnrCompetitionPub.ITEM_DS);
        EnrRatingItem item = wrapper.getWrapped();
        return !item.isStatusEntranceExamsCorrect() || item.getTotalMarkAsLong() == 0 ? "—" : LongAsDoubleFormatter.LONG_AS_DOUBLE_FORMATTER_3_SCALE.format(item.getTotalMarkAsLong());
    }

    public String getEntranceExamSum()
    {
        DataWrapper wrapper = _uiConfig.getDataSourceCurrentRecord(EnrCompetitionPub.ITEM_DS);
        EnrRatingItem item = wrapper.getWrapped();
        return !item.isStatusEntranceExamsCorrect() || item.getTotalMarkAsLong() == 0 ? "—" : LongAsDoubleFormatter.LONG_AS_DOUBLE_FORMATTER_3_SCALE.format(item.getTotalMarkAsLong() - item.getAchievementMarkAsLong());
    }

    // getters and setters

    public EnrCompetition getCompetition()
    {
        return competition;
    }

    public void setCompetition(EnrCompetition competition)
    {
        this.competition = competition;
    }

    public String getExamSetTitle()
    {
        return examSetTitle;
    }

    public void setExamSetTitle(String examSetTitle)
    {
        this.examSetTitle = examSetTitle;
    }

    public String getProgramTitles()
    {
        return programTitles;
    }

    public void setProgramTitles(String programTitles)
    {
        this.programTitles = programTitles;
    }

    public EnrEnrollmentStep getStep()
    {
        return step;
    }

    public void setStep(EnrEnrollmentStep step)
    {
        this.step = step;
    }

    public List<EnrEnrollmentStep> getStepList()
    {
        return stepList;
    }

    public void setStepList(List<EnrEnrollmentStep> stepList)
    {
        this.stepList = stepList;
    }

    public boolean isProgramSetSecondary(){ return getCompetition().getProgramSetOrgUnit().getProgramSet() instanceof EnrProgramSetSecondary; }
}