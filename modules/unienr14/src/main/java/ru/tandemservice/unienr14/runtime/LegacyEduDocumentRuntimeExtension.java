/* $Id:$ */
package ru.tandemservice.unienr14.runtime;

import org.tandemframework.common.catalog.runtime.CatalogGroupRuntime;
import org.tandemframework.core.runtime.IRuntimeExtension;
import org.tandemframework.shared.person.base.bo.PersonEduDocument.PersonEduDocumentManager;

/**
 * @author oleyba
 * @since 5/1/14
 */
public class LegacyEduDocumentRuntimeExtension implements IRuntimeExtension
{

    @Override
    public void init(Object object)
    {
        if (PersonEduDocumentManager.isShowLegacyEduDocuments()) {
            throw new IllegalStateException("Module unienr14 will not work with legacy education documents enabled. Set property person.showLegacyEduDocumentTab to 'false' or remove it.");
        }
    }

    @Override
    public void destroy()
    {
    }
}
