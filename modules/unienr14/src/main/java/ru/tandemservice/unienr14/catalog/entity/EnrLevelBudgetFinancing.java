package ru.tandemservice.unienr14.catalog.entity;

import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IPrioritizedCatalogItem;
import ru.tandemservice.unienr14.catalog.entity.gen.*;

/** @see ru.tandemservice.unienr14.catalog.entity.gen.EnrLevelBudgetFinancingGen */
public class EnrLevelBudgetFinancing extends EnrLevelBudgetFinancingGen implements IDynamicCatalogItem, IPrioritizedCatalogItem
{
}