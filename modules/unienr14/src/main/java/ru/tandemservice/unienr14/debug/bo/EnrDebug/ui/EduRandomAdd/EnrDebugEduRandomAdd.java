/**
 *$Id$
 */
package ru.tandemservice.unienr14.debug.bo.EnrDebug.ui.EduRandomAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uniedu.base.bo.EducationYear.EducationYearManager;

/**
 * @author Alexander Zhebko
 * @since 17.03.2014
 */
@Configuration
public class EnrDebugEduRandomAdd extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(EducationYearManager.instance().eduYearDSConfig())
                .create();
    }
}