package ru.tandemservice.unienr14.report.bo.EnrReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.AdmissionPlansAdd.logic.EnrReportAdmissionPlansDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.AdmissionPlansAdd.logic.IEnrReportAdmissionPlansDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.AdmissionResultsByEnrollmentStagesAdd.logic.EnrReportAdmissionResultsByEnrollmentStagesDAO;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.AdmissionResultsByEnrollmentStagesAdd.logic.IEnrReportAdmissionResultsByEnrollmentStagesDAO;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.DailyRequestsCumulativeAdd.logic.EnrReportDailyRequestsCumulativeDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.DailyRequestsCumulativeAdd.logic.IEnrReportDailyRequestsCumulativeDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EnrollmentCampaignPassSummaryAdd.logic.EnrReportEnrollmentCampaignPassSummaryDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EnrollmentCampaignPassSummaryAdd.logic.IEnrReportEnrollmentCampaignPassSummaryDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EnrollmentCommissionMeetingProtocolAdd.logic.EnrReportEnrollmentCommissionMeetingProtocolDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EnrollmentCommissionMeetingProtocolAdd.logic.IEnrReportEnrollmentCommissionMeetingProtocolDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EnrollmentResultsByOrgUnitsAdd.logic.EnrReportEnrollmentResultsByOrgUnitsDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EnrollmentResultsByOrgUnitsAdd.logic.IEnrReportEnrollmentResultsByOrgUnitsDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EnrollmentResultsBySubjectGroupsAdd.logic.EnrReportEnrollmentResultsBySubjectGroupsDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EnrollmentResultsBySubjectGroupsAdd.logic.IEnrReportEnrollmentResultsBySubjectGroupsDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantFromCoursesCountAdd.logic.EnrReportEntrantFromCoursesCountDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantFromCoursesCountAdd.logic.IEnrReportEntrantFromCoursesCountDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantListAdd.logic.EnrReportEntrantListDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantListAdd.logic.IEnrReportEntrantListDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantOnExamAdd.logic.EnrReportEntrantOnExamDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantOnExamAdd.logic.IEnrReportEntrantOnExamDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantRefusalAdd.logic.EnrReportEntrantRefusalDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantRefusalAdd.logic.IEnrReportEntrantRefusalDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantRegistrationJournalAdd.logic.EnrReportEntrantRegistrationJournalDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantRegistrationJournalAdd.logic.IEnrReportEntrantRegistrationJournalDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantResidenceDistributionAdd.logic.EnrReportEntrantResidenceDistributionDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantResidenceDistributionAdd.logic.IEnrReportEntrantResidenceDistributionDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsAgeDistributionAdd.logic.EnrReportEntrantsAgeDistributionDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsAgeDistributionAdd.logic.IEnrReportEntrantsAgeDistributionDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsBenefitDistributionAdd.logic.EnrReportEntrantsBenefitDistributionDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsBenefitDistributionAdd.logic.IEnrReportEntrantsBenefitDistributionDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsEduOrgDistributionAdd.logic.EnrReportEntrantsEduOrgDistributionDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsEduOrgDistributionAdd.logic.IEnrReportEntrantsEduOrgDistributionDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsEduProfileAdd.logic.EnrReportEntrantsEduProfileDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsEduProfileAdd.logic.IEnrReportEntrantsEduProfileDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsForeignLangsAdd.logic.EnrReportEntrantsForeignLangsDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsForeignLangsAdd.logic.IEnrReportEntrantsForeignLangsDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsSecondFormRegJournalAdd.logic.EnrReportEntrantsSecondFormRegJournalDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsSecondFormRegJournalAdd.logic.IEnrReportEntrantsSecondFormRegJournalDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsTargetAdmInfoAdd.logic.EnrReportEntrantsTargetAdmInfoDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsTargetAdmInfoAdd.logic.IEnrReportEntrantsTargetAdmInfoDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.ExamAdmissionProtocolAdd.logic.EnrReportExamAdmissionProtocolDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.ExamAdmissionProtocolAdd.logic.IEnrReportExamAdmissionProtocolDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.ExamResultsByDifferentSourcesAdd.logic.EnrReportExamResultsByDifferentSourcesDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.ExamResultsByDifferentSourcesAdd.logic.IEnrReportExamResultsByDifferentSourcesDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.ForEnrollmentCommissionAdd.logic.EnrReportForEnrollmentCommissionDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.ForEnrollmentCommissionAdd.logic.IEnrReportForEnrollmentCommissionDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.HorizontalSummaryEnrollmentResultsAdd.logic.EnrReportHorizontalSummaryEnrollmentResultsDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.HorizontalSummaryEnrollmentResultsAdd.logic.IEnrReportHorizontalSummaryEnrollmentResultsDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.InformationSourcesAdd.logic.EnrReportInformationSourcesDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.InformationSourcesAdd.logic.IEnrReportInformationSourcesDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.OrgUnitDocAcceptanceAdd.logic.EnrReportOrgUnitDocAcceptanceDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.OrgUnitDocAcceptanceAdd.logic.IEnrReportOrgUnitDocAcceptanceDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.OrgUnitEnrollmentResultsAdd.logic.EnrReportOrgUnitEnrollmentResultsDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.OrgUnitEnrollmentResultsAdd.logic.IEnrReportOrgUnitEnrollmentResultsDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.RatingListAdd.logic.EnrReportRatingListDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.RatingListAdd.logic.IEnrReportRatingListDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.RequestCountAdd.logic.EnrReportRequestCountDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.RequestCountAdd.logic.IEnrReportRequestCountDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.SelectedEduProgramsSummaryAdd.logic.EnrReportSelectedEduProgramsSummaryDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.SelectedEduProgramsSummaryAdd.logic.IEnrReportSelectedEduProgramsSummaryDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.StateExamAverageScoreAdd.logic.EnrReportStateExamAverageScoreDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.StateExamAverageScoreAdd.logic.IEnrReportStateExamAverageScoreDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.StateExamEnrollmentResultsAdd.logic.EnrReportStateExamEnrollmentResultsDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.StateExamEnrollmentResultsAdd.logic.IEnrReportStateExamEnrollmentResultsDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.StateExamEnrollmentResultsPlanCompetitionScoresAdd.logic.EnrReportStateExamEnrollmentResultsPlanCompetitionScoresDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.StateExamEnrollmentResultsPlanCompetitionScoresAdd.logic.IEnrReportStateExamEnrollmentResultsPlanCompetitionScoresDao;

/**
 * @author Alexander Shaburov
 * @since 15.11.13
 */
@Configuration
public class EnrReportManager extends BusinessObjectManager
{
    public static EnrReportManager instance()
    {
        return instance(EnrReportManager.class);
    }

    @Bean
    public IEnrReportEntrantListDao entrantListDao()
    {
        return new EnrReportEntrantListDao();
    }

    @Bean
    public IEnrReportRatingListDao ratingListDao()
    {
        return new EnrReportRatingListDao();
    }

    @Bean
    public IEnrReportRequestCountDao requestCountDao()
    {
        return new EnrReportRequestCountDao();
    }

    @Bean
    public IEnrReportEntrantOnExamDao entrantOnExamDao() {
        return new EnrReportEntrantOnExamDao();
    }

    @Bean
    public IEnrReportEntrantRefusalDao entrantRefusalDao() {
        return new EnrReportEntrantRefusalDao();
    }

    @Bean
    public IEnrReportEntrantFromCoursesCountDao entrantFromCoursesCountDao()
    {
        return new EnrReportEntrantFromCoursesCountDao();
    }

    @Bean
    public IEnrReportEntrantsForeignLangsDao entrantsForeignLangsDao()
    {
        return new EnrReportEntrantsForeignLangsDao();
    }

    @Bean
    public IEnrReportOrgUnitDocAcceptanceDao orgUnitDocAcceptanceDao()
    {
        return new EnrReportOrgUnitDocAcceptanceDao();
    }

    @Bean
    public IEnrReportDailyRequestsCumulativeDao dailyRequestsCumulativeDao()
    {
        return new EnrReportDailyRequestsCumulativeDao();
    }

    @Bean
    public IEnrReportForEnrollmentCommissionDao forEnrollmentCommissionDao()
    {
        return new EnrReportForEnrollmentCommissionDao();
    }

    @Bean
    public IEnrReportEntrantRegistrationJournalDao entrantRegistrationJournalDao()
    {
        return new EnrReportEntrantRegistrationJournalDao();
    }

    @Bean
    public IEnrReportExamAdmissionProtocolDao examAdmissionProtocolDao()
    {
        return new EnrReportExamAdmissionProtocolDao();
    }

    @Bean
    public IEnrReportEnrollmentCampaignPassSummaryDao enrollmentCampaignPassSummaryDao()
    {
        return new EnrReportEnrollmentCampaignPassSummaryDao();
    }

    @Bean
    public IEnrReportInformationSourcesDao informationSourcesDao()
    {
        return new EnrReportInformationSourcesDao();
    }

    @Bean
    public IEnrReportEntrantsAgeDistributionDao entrantsAgeDistributionDao()
    {
        return new EnrReportEntrantsAgeDistributionDao();
    }

    @Bean
    public IEnrReportEntrantsEduOrgDistributionDao entrantsEduOrgDistributionDao()
    {
        return new EnrReportEntrantsEduOrgDistributionDao();
    }

    @Bean
    public IEnrReportEntrantResidenceDistributionDao entrantResidenceDistributionDao()
    {
        return new EnrReportEntrantResidenceDistributionDao();
    }

    @Bean
    public IEnrReportEntrantsEduProfileDao entrantsEduProfileDao()
    {
        return new EnrReportEntrantsEduProfileDao();
    }

    @Bean
    public IEnrReportEntrantsTargetAdmInfoDao entrantsTargetAdmInfoDao()
    {
        return new EnrReportEntrantsTargetAdmInfoDao();
    }

    @Bean
    public IEnrReportEntrantsSecondFormRegJournalDao entrantsSecondFormRegJournalDao()
    {
        return new EnrReportEntrantsSecondFormRegJournalDao();
    }

    @Bean
    public IEnrReportStateExamAverageScoreDao stateExamAverageScoreDao()
    {
        return new EnrReportStateExamAverageScoreDao();
    }

    @Bean
    public IEnrReportExamResultsByDifferentSourcesDao examResultsByDifferentSourcesDao()
    {
        return new EnrReportExamResultsByDifferentSourcesDao();
    }

    @Bean
    public IEnrReportSelectedEduProgramsSummaryDao selectedEduProgramsSummaryDao()
    {
        return new EnrReportSelectedEduProgramsSummaryDao();
    }

    @Bean
    public IEnrReportEntrantsBenefitDistributionDao entrantsBenefitDistributionDao()
    {
        return new EnrReportEntrantsBenefitDistributionDao();
    }

    @Bean
    public IEnrReportOrgUnitEnrollmentResultsDao orgUnitEnrollmentResultsDao()
    {
        return new EnrReportOrgUnitEnrollmentResultsDao();
    }

    @Bean
    public IEnrReportEnrollmentResultsByOrgUnitsDao enrollmentResultsByOrgUnitsDao()
    {
        return new EnrReportEnrollmentResultsByOrgUnitsDao();
    }

    @Bean
    public IEnrReportStateExamEnrollmentResultsDao stateExamEnrollmentResultsDao()
    {
        return new EnrReportStateExamEnrollmentResultsDao();
    }

    @Bean
    public IEnrReportStateExamEnrollmentResultsPlanCompetitionScoresDao stateExamEnrollmentResultsPlanCompetitionScoresDao()
    {
        return new EnrReportStateExamEnrollmentResultsPlanCompetitionScoresDao();
    }

    @Bean
    public IEnrReportEnrollmentCommissionMeetingProtocolDao enrollmentCommissionMeetingProtocolDao()
    {
        return new EnrReportEnrollmentCommissionMeetingProtocolDao();
    }

    @Bean
    public IEnrReportAdmissionResultsByEnrollmentStagesDAO admissionResultsByEnrollmentStagesDao()
    {
        return new EnrReportAdmissionResultsByEnrollmentStagesDAO();
    }

    @Bean
    public IEnrReportEnrollmentResultsBySubjectGroupsDao enrollmentResultsBySubjectGroupsDao()
    {
        return new EnrReportEnrollmentResultsBySubjectGroupsDao();
    }

    @Bean
    public IEnrReportHorizontalSummaryEnrollmentResultsDao horizontalSummaryEnrollmentResultsDao()
    {
        return new EnrReportHorizontalSummaryEnrollmentResultsDao();
    }

    @Bean
    public IEnrReportAdmissionPlansDao admissionPlansDao()
    {
        return new EnrReportAdmissionPlansDao();
    }
}
