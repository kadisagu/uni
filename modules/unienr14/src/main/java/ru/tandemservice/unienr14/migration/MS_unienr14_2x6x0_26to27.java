/* $Id$ */
package ru.tandemservice.unienr14.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.PreparedStatement;
import java.util.HashMap;
import java.util.Map;

/**
 * @author azhebko
 * @since 16.06.2014
 */
public class MS_unienr14_2x6x0_26to27 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        Map<String, String> old2newCodeMap = new HashMap<>(6);
        {
            old2newCodeMap.put("1", "02"); // EnrCompetitionTypeCodes.EXCLUSIVE);
            old2newCodeMap.put("2", "03"); // EnrCompetitionTypeCodes.TARGET_ADMISSION);
            old2newCodeMap.put("3", "04"); // EnrCompetitionTypeCodes.MINISTERIAL);
            old2newCodeMap.put("4", "06"); // EnrCompetitionTypeCodes.CONTRACT);
            old2newCodeMap.put("5", "01"); // EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL);
            old2newCodeMap.put("6", "05"); // EnrCompetitionTypeCodes.NO_EXAM_CONTRACT);
        }

        PreparedStatement preparedStatement = tool.getConnection().prepareStatement("update enr14_c_comp_type_t set code_p = ? where code_p = ?");
        for (Map.Entry<String, String> entry: old2newCodeMap.entrySet())
        {
            preparedStatement.setString(1, entry.getValue());
            preparedStatement.setString(2, entry.getKey());
            preparedStatement.execute();
        }
    }
}