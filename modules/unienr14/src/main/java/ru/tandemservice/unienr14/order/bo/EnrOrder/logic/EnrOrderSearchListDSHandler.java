package ru.tandemservice.unienr14.order.bo.EnrOrder.logic;

import com.google.common.collect.Iterables;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderType;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.order.entity.*;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.OrderStates;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Stanislav Shibarshin
 * @since 13.04.17
 */
public class EnrOrderSearchListDSHandler extends DefaultSearchDataSourceHandler
{

    public static final String ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String FILTER_UTIL = "filterUtil";

    public static final String CREATE_DATE_FILTER_NAME = "createDate";
    public static final String COMMIT_DATE_FILTER_NAME = "commitDate";
    public static final String NUMBER_FILTER_NAME = "number";
    public static final String ORDER_TYPE_FILTER_NAME = "orderType";
    public static final String ORDER_STATE_FILTER_NAME = "orderState";

    public EnrOrderSearchListDSHandler(String ownerId)
    {
        super(ownerId, EnrOrder.class);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {

        DQLSelectBuilder orderDQL = getOrderDQL(context);
        new DQLOrderDescriptionRegistry(EnrOrder.class, "o").applyOrder(orderDQL, input.getEntityOrder());

        DSOutput output = DQLSelectOutputBuilder.get(input, orderDQL, context.getSession()).pageable(true).build();

        List<EnrOrder> entityList = output.getRecordList();
        List<Long> entityIds = entityList.stream().map(EntityBase::getId).collect(Collectors.toList());

        DQLSelectBuilder parDQL = new DQLSelectBuilder()
                .fromEntity(EnrAbstractExtract.class, "p")
                .column(property("p"))
                .column(property(EnrAbstractExtract.paragraph().order().id().fromAlias("p")))
                .column(property(EnrAbstractExtract.paragraph().id().fromAlias("p")))
                .where(in(property("p", EnrAbstractExtract.paragraph().order()), entityIds));

        Map<Long, Set<Long>> parMap = SafeMap.get(HashSet.class);
        Map<Long, Set<Long>> competitionMap = SafeMap.get(HashSet.class);

        List<Object[]> extractList = parDQL.createStatement(getSession()).list();
        for (Object[] e : extractList)
        {
            EnrCompetition ent = ((EnrAbstractExtract) e[0]).getCompetition();
            Long id = (Long) e[1];
            parMap.get(id).add((Long) e[2]);
            competitionMap.get(id).add(ent.getId());
        }
        Set<Long> competitionIds = competitionMap.values().stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());

        Map<Long, Object[]> result = new HashMap<>();
        for (List<Long> ids : Iterables.partition(competitionIds, DQL.MAX_VALUES_ROW_NUMBER))
        {
            List<Object[]> portionResultList = new DQLSelectBuilder().fromEntity(EnrCompetition.class, "e")
                    .joinPath(DQLJoinType.inner, EnrCompetition.programSetOrgUnit().programSet().programForm().fromAlias("e"), "pf")
                    .joinPath(DQLJoinType.inner, EnrCompetition.programSetOrgUnit().orgUnit().fromAlias("e"), "ou")
                    .column(property(EnrCompetition.id().fromAlias("e")))
                    .column(property("ou"))
                    .column(property(EduProgramForm.title().fromAlias("pf")))
                    .where(in(property(EnrCompetition.id().fromAlias("e")), ids)).createStatement(getSession()).list();

            Map<Long, Object[]> portionResultMap = portionResultList.stream().collect(Collectors.toMap(
                    o -> (Long) o[0],
                    o -> Arrays.copyOfRange(o, 1, 3)
            ));

            for (Long key : portionResultMap.keySet()) {
                if (!result.containsKey(key))
                    result.put(key, portionResultMap.get(key));
            }
        }

        for (Object[] e : extractList)
        {
            EnrCompetition ent = ((EnrAbstractExtract) e[0]).getCompetition();
            getSession().evict(e[0]);
            getSession().evict(ent);
        }

        return output.transform(orderObject ->
        {
            EnrOrder order = (EnrOrder) orderObject;
            final ViewWrapper<EnrOrder> wrapper = new ViewWrapper<>(order);
            boolean finished = UnimoveDefines.CATALOG_ORDER_STATE_FINISHED.equals(order.getState().getCode());
            int parCount = parMap.get(wrapper.getId()).size();
            wrapper.setViewProperty("disabledPrint", !finished && parCount == 0);
            wrapper.setViewProperty("disabledStateChange", !order.getState().getCode().equals(UnimoveDefines.CATALOG_EXTRACT_STATE_ACCEPTED) && !order.getState().getCode().equals(UnimoveDefines.CATALOG_EXTRACT_STATE_FORMATIVE));
            String programForm = competitionMap.get(wrapper.getId()).stream()
                    .map(competitionId -> result.get(competitionId)[1]).map(String.class::cast)
                    .collect(Collectors.toSet()).stream()
                    .sorted().collect(Collectors.joining(", "));

            String enrOrgUnit = competitionMap.get(wrapper.getId()).stream()
                    .map(competitionId -> result.get(competitionId)[0])
                    .map(EnrOrgUnit.class::cast).map(EnrOrgUnit::getDepartmentTitle)
                    .collect(Collectors.toSet()).stream()
                    .sorted().collect(Collectors.joining(", "));

            wrapper.setViewProperty("programForm", programForm);
            wrapper.setViewProperty("enrOrgUnit", enrOrgUnit);
            return wrapper;
        });
    }

    protected DQLSelectBuilder getOrderDQL(ExecutionContext context)
    {
        EnrEnrollmentCampaign enrollmentCampaign = context.get(ENROLLMENT_CAMPAIGN);
        CommonFilterAddon util = context.get(FILTER_UTIL);
        final Date createDate = context.get(CREATE_DATE_FILTER_NAME);
        final Date commitDate = context.get(COMMIT_DATE_FILTER_NAME);
        final String number = context.get(NUMBER_FILTER_NAME);
        final EnrOrderType orderType = context.get(ORDER_TYPE_FILTER_NAME);
        final OrderStates orderState = context.get(ORDER_STATE_FILTER_NAME);

        DQLSelectBuilder orderDQL = new DQLSelectBuilder()
                .fromEntity(EnrOrder.class, "o").column("o")
                .where(eq(property("o", EnrOrder.enrollmentCampaign()), value(enrollmentCampaign)));

        FilterUtils.applyBetweenFilter(orderDQL, "o", EnrOrder.createDate().s(), createDate, createDate);
        FilterUtils.applyBetweenFilter(orderDQL, "o", EnrOrder.commitDate().s(), commitDate, commitDate);
        FilterUtils.applyLikeFilter(orderDQL, number, EnrOrder.number().fromAlias("o"));

        if (orderType != null)
            orderDQL.where(eq(property("o", EnrOrder.type()), value(orderType)));
        if (orderState != null)
            orderDQL.where(eq(property("o", EnrOrder.state()), value(orderState)));

        if (util != null && !util.isEmptyFilters()) {
            IDQLSelectableQuery inCompetitionQuery = util.getEntityIdsFilteredBuilder("c").buildQuery();
            orderDQL
                    .where(or(
                            exists(
                                    new DQLSelectBuilder()
                                            .fromEntity(EnrEnrollmentExtract.class, "ee")
                                            .where(eq(property("ee", EnrEnrollmentExtract.paragraph().order().id()), property("o", EnrOrder.id())))
                                            .where(in(property("ee", EnrEnrollmentExtract.entity().competition().id()), inCompetitionQuery))
                                            .buildQuery()
                            ),
                            exists(
                                    new DQLSelectBuilder()
                                            .fromEntity(EnrEnrollmentMinisteryExtract.class, "me")
                                            .where(eq(property("me", EnrEnrollmentMinisteryExtract.paragraph().order().id()), property("o", EnrOrder.id())))
                                            .where(in(property("me", EnrEnrollmentMinisteryExtract.entrantRequest().competition().id()), inCompetitionQuery))
                                            .buildQuery()
                            ),
                            exists(
                                    new DQLSelectBuilder()
                                            .fromEntity(EnrAllocationExtract.class, "ae")
                                            .where(eq(property("ae", EnrAllocationExtract.paragraph().order().id()), property("o", EnrOrder.id())))
                                            .where(in(property("ae", EnrAllocationExtract.entity().competition().id()), inCompetitionQuery))
                                            .buildQuery()
                            ),
                            exists(
                                    new DQLSelectBuilder()
                                            .fromEntity(EnrCancelExtract.class, "ce")
                                            .where(eq(property("ce", EnrCancelExtract.paragraph().order().id()), property("o", EnrOrder.id())))
                                            .where(in(property("ce", EnrCancelExtract.requestedCompetition().competition().id()), inCompetitionQuery))
                                            .buildQuery())));
        }
        return orderDQL;
    }
}
