package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLUpdateQuery;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unienr14_2x10x1_12to13 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.10.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrEntrantAchievement


        // создано свойство request
        if(!tool.columnExists("enr14_entrant_achievement_t", "request_id"))
        {
            // создать колонку
            tool.createColumn("enr14_entrant_achievement_t", new DBColumn("request_id", DBType.LONG));

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrEntrantAchievementType

        // создано обязательное свойство forRequest
        if(!tool.columnExists("enr14_achievement_type_t", "forrequest_p"))
        {
            // создать колонку
            tool.createColumn("enr14_achievement_type_t", new DBColumn("forrequest_p", DBType.BOOLEAN));

            // задать значение по умолчанию
            tool.executeUpdate("update enr14_achievement_type_t set forrequest_p=? where forrequest_p is null", false);

            // сделать колонку NOT NULL
            tool.setColumnNullable("enr14_achievement_type_t", "forrequest_p", false);

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность EnrCampaignEntrantDocument
        // enr14_camp_entrant_doc_t achievement_p enrollmentcampaign_id / documenttype_id enr14_c_entrant_doc_type_t / enr14_campaign_t educationyear_id -> educationyear_t intvalue_p

        // Установить для типов документа «Диплом участника олимпиады» признак «Подтверждает индивидуальное достижение» = «да» для ПК с годом 2016/2017 и больше.
        {
            SQLUpdateQuery updateQuery = new SQLUpdateQuery("enr14_camp_entrant_doc_t", "ced");
            updateQuery.getUpdatedTableFrom()
                    .innerJoin(SQLFrom.table("enr14_c_entrant_doc_type_t", "dt"), "dt.id=ced.documenttype_id")
                    .innerJoin(SQLFrom.table("enr14_campaign_t", "ec"), "ec.id=ced.enrollmentcampaign_id")
                    .innerJoin(SQLFrom.table("educationyear_t", "ey"), "ey.id=ec.educationyear_id");
            updateQuery
                    .set("achievement_p", "?")
                    .where("dt.code_p=?")
                    .where("ey.intvalue_p >= ?");

            // EnrEntrantDocumentTypeCodes
            /** Константа кода (code) элемента : Диплом участника олимпиады (title) */
            String OLYMP_DIPLOMA = "2";
            int eduYear = 2016;

            tool.executeUpdate(tool.getDialect().getSQLTranslator().toSql(updateQuery), true, OLYMP_DIPLOMA, eduYear);
        }
    }
}
