/* $Id$ */
package ru.tandemservice.unienr14.entrant.bo.EnrOnlineEntrant.ui.Pub;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCardToDocumentScanCopy;
import ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument;

import java.util.List;

/**
 * @author Denis Katkov
 * @since 24.06.2016
 */
@Input(
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "holder.id", required = true)
)
public class EnrOnlineEntrantPubUI extends UIPresenter
{
    public static final String IDENTITY_CARD_TO_DOCUMENT_SCAN_COPY = "identityCardToDocumentScanCopy";
    public static final String ENR_ONLINE_ENTRANT_EDU_DOCUMENT = "enrOnlineEntrantEduDocument";

    private EntityHolder<EnrOnlineEntrant> holder = new EntityHolder<>();
    private DatabaseFile documentScanCopy;
    private EnrOnlineEntrantEduDocument entrantEduDocument;

    @Override
    public void onComponentRefresh()
    {
        documentScanCopy = getOnlineEntrant().getIdCardScanCopy();

        List<EnrOnlineEntrantEduDocument> educationCopies = DataAccessServices.dao().getList(EnrOnlineEntrantEduDocument.class, EnrOnlineEntrantEduDocument.onlineEntrant().id().s(), getOnlineEntrant().getId());
        if (CollectionUtils.isNotEmpty(educationCopies)) {
            entrantEduDocument = educationCopies.get(0);
        }
    }

    public void onClickDownloadFile()
    {
        String page = getListenerParameter();
        DatabaseFile file = null;
        if (IDENTITY_CARD_TO_DOCUMENT_SCAN_COPY.equals(page)) {
            file = documentScanCopy;
        } else if (ENR_ONLINE_ENTRANT_EDU_DOCUMENT.equals(page)) {
            file = entrantEduDocument.getScanCopy();
        } else if ("indAchiveScanCopy".equals(page)) {
            file = getOnlineEntrant().getIndAchievScanCopy();
        } else if ("otherDocScanCopy".equals(page)) {
            file = getOnlineEntrant().getOtherDocScanCopy();
        }

        if (file != null) {
            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType(file.getContentType()).fileName(file.getFilename()).document(file.getContent()), false);
        }
    }

    public void deleteOnlineEntrant()
    {
        DataAccessServices.dao().delete(getHolder().getValue().getId());
        deactivate();
    }

    public String getDeleteOnlineEntrantAlert()
    {
        return String.format(getConfig().getProperty("ui.delete.alert"), getOnlineEntrant().getIdentityCard().getFullFio());
    }

    public String getStickerTitle()
    {
        return String.format(getConfig().getProperty("ui.sticker"), getOnlineEntrant().getIdentityCard().getFullFio(), getOnlineEntrant().getPersonalNumber());
    }

    public EntityHolder<EnrOnlineEntrant> getHolder()
    {
        return holder;
    }

    public void setHolder(EntityHolder<EnrOnlineEntrant> holder)
    {
        this.holder = holder;
    }

    public EnrOnlineEntrantEduDocument getEntrantEduDocument()
    {
        return entrantEduDocument;
    }

    public EnrOnlineEntrant getOnlineEntrant()
    {
        return getHolder().getValue();
    }

    public DatabaseFile getDocumentScanCopy()
    {
        return documentScanCopy;
    }
}