/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsEduOrgDistributionAdd.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsEduOrgDistributionAdd.EnrReportEntrantsEduOrgDistributionAddUI;

/**
 * @author rsizonenko
 * @since 19.06.2014
 */
public interface IEnrReportEntrantsEduOrgDistributionDao extends INeedPersistenceSupport {
    long createReport(EnrReportEntrantsEduOrgDistributionAddUI model);
}
