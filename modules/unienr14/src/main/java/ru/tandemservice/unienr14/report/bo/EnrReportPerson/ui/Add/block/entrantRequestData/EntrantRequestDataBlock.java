package ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.block.entrantRequestData;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.unienr14.catalog.entity.*;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.block.entrantData.EntrantDataParam;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.util.EntrantRequestAcceptPrincipalHandler;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrTargetAdmission.EnrTargetAdmissionManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Vasily Zhukov
 * @since 08.02.2011
 */
public class EntrantRequestDataBlock
{
    // names
    public static final String QUALIFICATION_DS = "qualificationDS";
    public static final String FORMATIVE_ORGUNIT_DS = "formativeOrgUnitDS";
    public static final String TERRITORIAL_ORGUNIT_DS = "territorialOrgUnitDS";
    public static final String EDUCATION_ORGUNIT_DS = "educationOrgUnitDS";
    public static final String DEVELOP_FORM_DS = "developFormDS";
    public static final String DEVELOP_CONDITION_DS = "developConditionDS";
    public static final String DEVELOP_TECH_DS = "developTechDS";
    public static final String DEVELOP_PERIOD_DS = "developPeriodDS";
    public static final String COMPENSATION_TYPE_DS = "compensationTypeDS";
    public static final String COMPETITION_TYPE_DS = "competitionTypeDS";
    public static final String BENEFIT_TYPE_DS = "benefitTypeDS";
    public static final String BENEFIT_CATEGORY_DS = "benefitCategoryDS";
    public static final String TARGET_ADMISSION_KIND_DS = "targetAdmissionKindDS";
    public static final String ENTRANT_STATE_DS = "entrantStateDS";
    public static final String ACCEPT_REQUEST_DS = "acceptRequestDS";
    public static final String TARGET_CONTRACT_ORGUNIT_DS = "targetContractOrgUnitDS";

    // datasource parameter names
    public static final String PARAM_FORMATIVE_ORG_UNIT = "formativeOrgUnitList";
    public static final String PARAM_TERRITORIAL_ORG_UNIT = "territorialOrgUnitList";
    public static final String PARAM_BENEFIT_TYPE = "enrBenefitType";


    public static void onBeforeDataSourceFetch(IUIDataSource dataSource, EntrantRequestDataParam param)
    {
        String name = dataSource.getName();

//        if (EDUCATION_ORGUNIT_DS.equals(name))
//        {
//            param.getFormativeOrgUnit().putParamIfActive(dataSource, PARAM_FORMATIVE_ORG_UNIT);
//            param.getTerritorialOrgUnit().putParamIfActive(dataSource, PARAM_TERRITORIAL_ORG_UNIT);
//        }

        if (BENEFIT_CATEGORY_DS.equals(name))
        {
            param.getBenefitType().putParamIfActive(dataSource, PARAM_BENEFIT_TYPE);
        }
    }

    public static void onBeforeDataSourceFetch(IUIDataSource dataSource, EntrantDataParam param)
    {
        String name = dataSource.getName();

        if (BENEFIT_TYPE_DS.equalsIgnoreCase(name) || BENEFIT_CATEGORY_DS.equalsIgnoreCase(name) || TARGET_ADMISSION_KIND_DS.equalsIgnoreCase(name))
        {
            param.getEnrollmentCampaign().putParamIfActive(dataSource, EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
        }
        else if (ACCEPT_REQUEST_DS.equals(name))
        {
            param.getEnrollmentCampaign().putParamIfActive(dataSource, EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
            param.getEnrollmentCommission().putParamIfActive(dataSource, EntrantRequestAcceptPrincipalHandler.PARAM_ENROLLMENT_COMMISSION);
        }
    }

    public static IDefaultComboDataSourceHandler createCompetitionTypeDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, EnrCompetitionType.class);
    }

    public static IDefaultComboDataSourceHandler createCompensationTypeDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, CompensationType.class, CompensationType.shortTitle());
    }

    public static IDefaultComboDataSourceHandler createBenefitTypeDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, EnrBenefitType.class);
    }

    public static IDefaultComboDataSourceHandler createBenefitCategoryDS(String name)
    {
        return new EntityComboDataSourceHandler(name, EnrBenefitCategory.class)
            .where(EnrBenefitCategory.benefitType(), PARAM_BENEFIT_TYPE, false)
            .order(EnrBenefitCategory.shortTitle())
            .order(EnrBenefitCategory.benefitType().shortTitle())
            .filter(EnrBenefitCategory.shortTitle())
            ;
    }

    public static IDefaultComboDataSourceHandler createTargetAdmissionKindDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, EnrTargetAdmissionKind.class)
        {
            @Override
            protected void prepareConditions(final ExecutionParameters<DSInput, DSOutput> ep)
            {
                super.prepareConditions(ep);

                final EnrEnrollmentCampaign enrCamp = ep.context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);

                BatchUtils.execute(UniBaseDao.ids(EnrTargetAdmissionManager.instance().dao().getUsedTaKinds(enrCamp)), 128, new BatchUtils.Action<Long>()
                {
                    @Override
                    @SuppressWarnings("unchecked")
                    public void execute(Collection<Long> elements)
                    {
                        ep.dqlBuilder.where(in(property("e.id"), new ArrayList(elements)));
                    }
                });
            }
        };
    }

    public static IDefaultComboDataSourceHandler createEntrantStateDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, EnrEntrantState.class);
    }
}
