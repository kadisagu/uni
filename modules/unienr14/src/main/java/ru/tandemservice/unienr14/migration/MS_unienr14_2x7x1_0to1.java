package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x7x1_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrEduLevelReqEduLevel

		// создана новая сущность
		{
			// создать таблицу
			if(!tool.tableExists("enr14_edulev_req_to_edulev_t"))
			{
				DBTable dbt = new DBTable("enr14_edulev_req_to_edulev_t",
						new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
						new DBColumn("discriminator", DBType.SHORT).setNullable(false),
						new DBColumn("enredulevelrequirement_id", DBType.LONG).setNullable(false),
						new DBColumn("edulevel_id", DBType.LONG).setNullable(false)
				);
				tool.createTable(dbt);
			}
			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("enrEduLevelReqEduLevel");

		}


    }
}