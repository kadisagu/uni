package ru.tandemservice.unienr14.exams.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroup;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline;
import ru.tandemservice.unienr14.settings.entity.EnrExamGroupSet;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Экзаменационная группа (ЭГ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrExamGroupGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.exams.entity.EnrExamGroup";
    public static final String ENTITY_NAME = "enrExamGroup";
    public static final int VERSION_HASH = -984496161;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXAM_GROUP_SET = "examGroupSet";
    public static final String P_TITLE = "title";
    public static final String L_TERRITORIAL_ORG_UNIT = "territorialOrgUnit";
    public static final String L_DISCIPLINE = "discipline";
    public static final String L_PASS_FORM = "passForm";
    public static final String P_SIZE = "size";
    public static final String P_DAYS = "days";
    public static final String P_CLOSED = "closed";
    public static final String P_DISCIPLINE_TITLE = "disciplineTitle";
    public static final String P_EXAM_TITLE_WITH_SCHEDULE = "examTitleWithSchedule";
    public static final String P_OPENED = "opened";

    private EnrExamGroupSet _examGroupSet;     // Набор ЭГ
    private String _title;     // Название
    private OrgUnit _territorialOrgUnit;     // Территориальное подр.
    private EnrCampaignDiscipline _discipline;     // Дисциплина ПК
    private EnrExamPassForm _passForm;     // Форма сдачи
    private int _size;     // Число мест
    private int _days;     // Дней сдачи
    private boolean _closed;     // Закрыта

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Набор ЭГ. Свойство не может быть null.
     */
    @NotNull
    public EnrExamGroupSet getExamGroupSet()
    {
        return _examGroupSet;
    }

    /**
     * @param examGroupSet Набор ЭГ. Свойство не может быть null.
     */
    public void setExamGroupSet(EnrExamGroupSet examGroupSet)
    {
        dirty(_examGroupSet, examGroupSet);
        _examGroupSet = examGroupSet;
    }

    /**
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Территориальное подр.. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    /**
     * @param territorialOrgUnit Территориальное подр.. Свойство не может быть null.
     */
    public void setTerritorialOrgUnit(OrgUnit territorialOrgUnit)
    {
        dirty(_territorialOrgUnit, territorialOrgUnit);
        _territorialOrgUnit = territorialOrgUnit;
    }

    /**
     * @return Дисциплина ПК. Свойство не может быть null.
     */
    @NotNull
    public EnrCampaignDiscipline getDiscipline()
    {
        return _discipline;
    }

    /**
     * @param discipline Дисциплина ПК. Свойство не может быть null.
     */
    public void setDiscipline(EnrCampaignDiscipline discipline)
    {
        dirty(_discipline, discipline);
        _discipline = discipline;
    }

    /**
     * @return Форма сдачи. Свойство не может быть null.
     */
    @NotNull
    public EnrExamPassForm getPassForm()
    {
        return _passForm;
    }

    /**
     * @param passForm Форма сдачи. Свойство не может быть null.
     */
    public void setPassForm(EnrExamPassForm passForm)
    {
        dirty(_passForm, passForm);
        _passForm = passForm;
    }

    /**
     * @return Число мест. Свойство не может быть null.
     */
    @NotNull
    public int getSize()
    {
        return _size;
    }

    /**
     * @param size Число мест. Свойство не может быть null.
     */
    public void setSize(int size)
    {
        dirty(_size, size);
        _size = size;
    }

    /**
     * @return Дней сдачи. Свойство не может быть null.
     */
    @NotNull
    public int getDays()
    {
        return _days;
    }

    /**
     * @param days Дней сдачи. Свойство не может быть null.
     */
    public void setDays(int days)
    {
        dirty(_days, days);
        _days = days;
    }

    /**
     * @return Закрыта. Свойство не может быть null.
     */
    @NotNull
    public boolean isClosed()
    {
        return _closed;
    }

    /**
     * @param closed Закрыта. Свойство не может быть null.
     */
    public void setClosed(boolean closed)
    {
        dirty(_closed, closed);
        _closed = closed;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrExamGroupGen)
        {
            setExamGroupSet(((EnrExamGroup)another).getExamGroupSet());
            setTitle(((EnrExamGroup)another).getTitle());
            setTerritorialOrgUnit(((EnrExamGroup)another).getTerritorialOrgUnit());
            setDiscipline(((EnrExamGroup)another).getDiscipline());
            setPassForm(((EnrExamGroup)another).getPassForm());
            setSize(((EnrExamGroup)another).getSize());
            setDays(((EnrExamGroup)another).getDays());
            setClosed(((EnrExamGroup)another).isClosed());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrExamGroupGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrExamGroup.class;
        }

        public T newInstance()
        {
            return (T) new EnrExamGroup();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "examGroupSet":
                    return obj.getExamGroupSet();
                case "title":
                    return obj.getTitle();
                case "territorialOrgUnit":
                    return obj.getTerritorialOrgUnit();
                case "discipline":
                    return obj.getDiscipline();
                case "passForm":
                    return obj.getPassForm();
                case "size":
                    return obj.getSize();
                case "days":
                    return obj.getDays();
                case "closed":
                    return obj.isClosed();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "examGroupSet":
                    obj.setExamGroupSet((EnrExamGroupSet) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "territorialOrgUnit":
                    obj.setTerritorialOrgUnit((OrgUnit) value);
                    return;
                case "discipline":
                    obj.setDiscipline((EnrCampaignDiscipline) value);
                    return;
                case "passForm":
                    obj.setPassForm((EnrExamPassForm) value);
                    return;
                case "size":
                    obj.setSize((Integer) value);
                    return;
                case "days":
                    obj.setDays((Integer) value);
                    return;
                case "closed":
                    obj.setClosed((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "examGroupSet":
                        return true;
                case "title":
                        return true;
                case "territorialOrgUnit":
                        return true;
                case "discipline":
                        return true;
                case "passForm":
                        return true;
                case "size":
                        return true;
                case "days":
                        return true;
                case "closed":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "examGroupSet":
                    return true;
                case "title":
                    return true;
                case "territorialOrgUnit":
                    return true;
                case "discipline":
                    return true;
                case "passForm":
                    return true;
                case "size":
                    return true;
                case "days":
                    return true;
                case "closed":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "examGroupSet":
                    return EnrExamGroupSet.class;
                case "title":
                    return String.class;
                case "territorialOrgUnit":
                    return OrgUnit.class;
                case "discipline":
                    return EnrCampaignDiscipline.class;
                case "passForm":
                    return EnrExamPassForm.class;
                case "size":
                    return Integer.class;
                case "days":
                    return Integer.class;
                case "closed":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrExamGroup> _dslPath = new Path<EnrExamGroup>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrExamGroup");
    }
            

    /**
     * @return Набор ЭГ. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamGroup#getExamGroupSet()
     */
    public static EnrExamGroupSet.Path<EnrExamGroupSet> examGroupSet()
    {
        return _dslPath.examGroupSet();
    }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamGroup#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Территориальное подр.. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamGroup#getTerritorialOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> territorialOrgUnit()
    {
        return _dslPath.territorialOrgUnit();
    }

    /**
     * @return Дисциплина ПК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamGroup#getDiscipline()
     */
    public static EnrCampaignDiscipline.Path<EnrCampaignDiscipline> discipline()
    {
        return _dslPath.discipline();
    }

    /**
     * @return Форма сдачи. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamGroup#getPassForm()
     */
    public static EnrExamPassForm.Path<EnrExamPassForm> passForm()
    {
        return _dslPath.passForm();
    }

    /**
     * @return Число мест. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamGroup#getSize()
     */
    public static PropertyPath<Integer> size()
    {
        return _dslPath.size();
    }

    /**
     * @return Дней сдачи. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamGroup#getDays()
     */
    public static PropertyPath<Integer> days()
    {
        return _dslPath.days();
    }

    /**
     * @return Закрыта. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamGroup#isClosed()
     */
    public static PropertyPath<Boolean> closed()
    {
        return _dslPath.closed();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamGroup#getDisciplineTitle()
     */
    public static SupportedPropertyPath<String> disciplineTitle()
    {
        return _dslPath.disciplineTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamGroup#getExamTitleWithSchedule()
     */
    public static SupportedPropertyPath<String> examTitleWithSchedule()
    {
        return _dslPath.examTitleWithSchedule();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamGroup#isOpened()
     */
    public static SupportedPropertyPath<Boolean> opened()
    {
        return _dslPath.opened();
    }

    public static class Path<E extends EnrExamGroup> extends EntityPath<E>
    {
        private EnrExamGroupSet.Path<EnrExamGroupSet> _examGroupSet;
        private PropertyPath<String> _title;
        private OrgUnit.Path<OrgUnit> _territorialOrgUnit;
        private EnrCampaignDiscipline.Path<EnrCampaignDiscipline> _discipline;
        private EnrExamPassForm.Path<EnrExamPassForm> _passForm;
        private PropertyPath<Integer> _size;
        private PropertyPath<Integer> _days;
        private PropertyPath<Boolean> _closed;
        private SupportedPropertyPath<String> _disciplineTitle;
        private SupportedPropertyPath<String> _examTitleWithSchedule;
        private SupportedPropertyPath<Boolean> _opened;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Набор ЭГ. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamGroup#getExamGroupSet()
     */
        public EnrExamGroupSet.Path<EnrExamGroupSet> examGroupSet()
        {
            if(_examGroupSet == null )
                _examGroupSet = new EnrExamGroupSet.Path<EnrExamGroupSet>(L_EXAM_GROUP_SET, this);
            return _examGroupSet;
        }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamGroup#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EnrExamGroupGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Территориальное подр.. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamGroup#getTerritorialOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> territorialOrgUnit()
        {
            if(_territorialOrgUnit == null )
                _territorialOrgUnit = new OrgUnit.Path<OrgUnit>(L_TERRITORIAL_ORG_UNIT, this);
            return _territorialOrgUnit;
        }

    /**
     * @return Дисциплина ПК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamGroup#getDiscipline()
     */
        public EnrCampaignDiscipline.Path<EnrCampaignDiscipline> discipline()
        {
            if(_discipline == null )
                _discipline = new EnrCampaignDiscipline.Path<EnrCampaignDiscipline>(L_DISCIPLINE, this);
            return _discipline;
        }

    /**
     * @return Форма сдачи. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamGroup#getPassForm()
     */
        public EnrExamPassForm.Path<EnrExamPassForm> passForm()
        {
            if(_passForm == null )
                _passForm = new EnrExamPassForm.Path<EnrExamPassForm>(L_PASS_FORM, this);
            return _passForm;
        }

    /**
     * @return Число мест. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamGroup#getSize()
     */
        public PropertyPath<Integer> size()
        {
            if(_size == null )
                _size = new PropertyPath<Integer>(EnrExamGroupGen.P_SIZE, this);
            return _size;
        }

    /**
     * @return Дней сдачи. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamGroup#getDays()
     */
        public PropertyPath<Integer> days()
        {
            if(_days == null )
                _days = new PropertyPath<Integer>(EnrExamGroupGen.P_DAYS, this);
            return _days;
        }

    /**
     * @return Закрыта. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamGroup#isClosed()
     */
        public PropertyPath<Boolean> closed()
        {
            if(_closed == null )
                _closed = new PropertyPath<Boolean>(EnrExamGroupGen.P_CLOSED, this);
            return _closed;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamGroup#getDisciplineTitle()
     */
        public SupportedPropertyPath<String> disciplineTitle()
        {
            if(_disciplineTitle == null )
                _disciplineTitle = new SupportedPropertyPath<String>(EnrExamGroupGen.P_DISCIPLINE_TITLE, this);
            return _disciplineTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamGroup#getExamTitleWithSchedule()
     */
        public SupportedPropertyPath<String> examTitleWithSchedule()
        {
            if(_examTitleWithSchedule == null )
                _examTitleWithSchedule = new SupportedPropertyPath<String>(EnrExamGroupGen.P_EXAM_TITLE_WITH_SCHEDULE, this);
            return _examTitleWithSchedule;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.exams.entity.EnrExamGroup#isOpened()
     */
        public SupportedPropertyPath<Boolean> opened()
        {
            if(_opened == null )
                _opened = new SupportedPropertyPath<Boolean>(EnrExamGroupGen.P_OPENED, this);
            return _opened;
        }

        public Class getEntityClass()
        {
            return EnrExamGroup.class;
        }

        public String getEntityName()
        {
            return "enrExamGroup";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getDisciplineTitle();

    public abstract String getExamTitleWithSchedule();

    public abstract boolean isOpened();
}
