/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReportBase;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.config.itemList.IItemListExtPointBuilder;
import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.support.IUIActivation;
import org.tandemframework.core.common.ITitled;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EnrolledEntrantsDataExportAdd.EnrReportEnrolledEntrantsDataExportAdd;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrStorableReportDesc;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.ui.List.EnrReportBaseList;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.ui.List.EnrReportBaseListUI;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.View.EnrReportPersonView;
import ru.tandemservice.unienr14.report.entity.*;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author oleyba
 * @since 5/12/14
 */
@Configuration
public class EnrReportBaseManager extends BusinessObjectManager
{
    public static EnrReportBaseManager instance()
    {
        return instance(EnrReportBaseManager.class);
    }

    @Bean
    public ItemListExtPoint<IEnrReportDefinition> reportListExtPoint()
    {
        IItemListExtPointBuilder<IEnrReportDefinition> extPointBuilder = itemList(IEnrReportDefinition.class);
        extPointBuilder.add("enr14ReportPerson", getReportDefinition("enr14ReportPerson", EnrReportPersonView.class));
        extPointBuilder.add("enr14ReportEnrolledEntrantsDataExport", getReportDefinition("enr14ReportEnrolledEntrantsDataExport", EnrReportEnrolledEntrantsDataExportAdd.class));
        for (String storableReportKey : getStorableReportDescList().keySet()) {
            extPointBuilder.add(storableReportKey, getStorableReportDefinition(storableReportKey));
        }

        return extPointBuilder.create();
    }

//    public static void main(String[] args) {
//        for (Map.Entry<String, IEnrStorableReportDesc> e : getStorableReportDescList().entrySet()) {
//            System.out.println("<permission name=\""+e.getKey() + "\" title=\"Работа с отчетом «" + e.getValue().getPubTitle().substring(e.getValue().getPubTitle().indexOf("«") + 1, e.getValue().getPubTitle().lastIndexOf("»")) + "»\"/>");
//        }
//    }

    private static Map<String, IEnrStorableReportDesc> getStorableReportDescList() {
        Map<String, IEnrStorableReportDesc> map = new LinkedHashMap<>();
        map.put(EnrReportEntrantList.REPORT_KEY, EnrReportEntrantList.getDescription());
        map.put(EnrReportRatingList.REPORT_KEY, EnrReportRatingList.getDescription());
        map.put(EnrReportCompetitionList.REPORT_KEY, EnrReportCompetitionList.getDescription());
        map.put(EnrReportRequestCount.REPORT_KEY, EnrReportRequestCount.getDescription());
        map.put(EnrReportEntrantOnExam.REPORT_KEY, EnrReportEntrantOnExam.getDescription());
        map.put(EnrReportEntrantRefusal.REPORT_KEY, EnrReportEntrantRefusal.getDescripton());
        map.put(EnrReportEntrantFromCoursesCount.REPORT_KEY, EnrReportEntrantFromCoursesCount.getDescription());
        map.put(EnrReportEntrantsForeignLangs.REPORT_KEY, EnrReportEntrantsForeignLangs.getDescription());
        map.put(EnrReportOrgUnitDocAcceptance.REPORT_KEY, EnrReportOrgUnitDocAcceptance.getDescription());
        map.put(EnrReportDailyRequestsCumulative.REPORT_KEY, EnrReportDailyRequestsCumulative.getDescription());
        map.put(EnrReportEntrantDataExport.REPORT_KEY, EnrReportEntrantDataExport.getDescription());
        map.put(EnrReportForEnrollmentCommission.REPORT_KEY, EnrReportForEnrollmentCommission.getDescripton());
        map.put(EnrReportEntrantRegistrationJournal.REPORT_KEY, EnrReportEntrantRegistrationJournal.getDescripton());
        map.put(EnrReportExamAdmissionProtocol.REPORT_KEY, EnrReportExamAdmissionProtocol.getDescripton());
        map.put(EnrReportEnrollmentCampaignPassSummary.REPORT_KEY, EnrReportEnrollmentCampaignPassSummary.getDescription());
        map.put(EnrReportInformationSources.REPORT_KEY, EnrReportInformationSources.getDescripton());
        map.put(EnrReportEntrantsAgeDistribution.REPORT_KEY, EnrReportEntrantsAgeDistribution.getDescripton());
        map.put(EnrReportEntrantsEduOrgDistribution.REPORT_KEY, EnrReportEntrantsEduOrgDistribution.getDescripton());
        map.put(EnrReportEntrantResidenceDistribution.REPORT_KEY, EnrReportEntrantResidenceDistribution.getDescription());
        map.put(EnrReportEntrantsEduProfile.REPORT_KEY, EnrReportEntrantsEduProfile.getDescripton());
        map.put(EnrReportEntrantsTargetAdmInfo.REPORT_KEY, EnrReportEntrantsTargetAdmInfo.getDescripton());
        map.put(EnrReportEntrantsSecondFormRegJournal.REPORT_KEY, EnrReportEntrantsSecondFormRegJournal.getDescription());
        map.put(EnrReportStateExamAverageScore.REPORT_KEY, EnrReportStateExamAverageScore.getDescription());
        map.put(EnrReportExamResultsByDifferentSources.REPORT_KEY, EnrReportExamResultsByDifferentSources.getDescription());
        map.put(EnrReportSelectedEduProgramsSummary.REPORT_KEY, EnrReportSelectedEduProgramsSummary.getDescription());
        map.put(EnrReportEntrantsBenefitDistribution.REPORT_KEY, EnrReportEntrantsBenefitDistribution.getDescription());
        map.put(EnrReportOrgUnitEnrollmentResults.REPORT_KEY, EnrReportOrgUnitEnrollmentResults.getDescription());
        map.put(EnrReportEnrollmentResultsByOrgUnits.REPORT_KEY, EnrReportEnrollmentResultsByOrgUnits.getDescription());
        map.put(EnrReportStateExamEnrollmentResults.REPORT_KEY, EnrReportStateExamEnrollmentResults.getDescription());
        map.put(EnrReportEnrollmentCommissionMeetingProtocol.REPORT_KEY, EnrReportEnrollmentCommissionMeetingProtocol.getDescription());
        map.put(EnrReportAdmissionResultsByEnrollmentStages.REPORT_KEY, EnrReportAdmissionResultsByEnrollmentStages.getDescription());
        map.put(EnrReportEnrollmentResultsBySubjectGroups.REPORT_KEY, EnrReportEnrollmentResultsBySubjectGroups.getDescription());
        map.put(EnrReportHorizontalSummaryEnrollmentResults.REPORT_KEY, EnrReportHorizontalSummaryEnrollmentResults.getDescription());
        map.put(EnrReportStateExamEnrollmentResultsPlanCompetitionScores.REPORT_KEY, EnrReportStateExamEnrollmentResultsPlanCompetitionScores.getDescription());
        map.put(EnrReportAdmissionPlans.REPORT_KEY, EnrReportAdmissionPlans.getDescription());
        return map;
    }

    @Bean
    public ItemListExtPoint<IEnrStorableReportDesc> storableReportDescExtPoint() {
        IItemListExtPointBuilder<IEnrStorableReportDesc> items = itemList(IEnrStorableReportDesc.class);
        for (Map.Entry<String, IEnrStorableReportDesc> entry : getStorableReportDescList().entrySet())
        {
            items.add(entry.getKey(), entry.getValue());
        }
        return items.create();
    }

    public static interface IEnrReportDefinition extends ITitled {
        String getName();
        String getVisibleExpression();
        void onClickViewReport(String reportName, IUIActivation uiActivation);
    }

    public static IEnrReportDefinition getReportDefinition(final String name, final Class<? extends BusinessComponentManager> businessComponent) {
        return new IEnrReportDefinition() {
            @Override public String getName() { return name; }
            @Override public String getTitle() { return EnrReportBaseManager.instance().getProperty(name + ".title"); }

            @Override public void onClickViewReport(String reportName, IUIActivation uiActivation) {
                uiActivation.asDesktopRoot(businessComponent).activate();
            }

            @Override public String getVisibleExpression() {
                final String property = name + ".visible";
                if (EnrReportBaseManager.instance().hasProperty(property)) {
                    return EnrReportBaseManager.instance().getProperty(property);
                } else {
                    return null;
                }
            }
        };
    }

    public static IEnrReportDefinition getStorableReportDefinition(final String name) {
        return new IEnrReportDefinition() {
            @Override public String getName() { return name; }
            @Override public String getTitle() { return EnrReportBaseManager.instance().getProperty(name + ".title"); }
            @Override public void onClickViewReport(String reportName, IUIActivation uiActivation) {
                uiActivation
                    .asDesktopRoot(EnrReportBaseList.class)
                    .parameter(EnrReportBaseListUI.BIND_REPORT_DESC, name)
                    .activate();
            }
            @Override public String getVisibleExpression() {
                final String property = name + ".visible";
                if (EnrReportBaseManager.instance().hasProperty(property)) {
                    return EnrReportBaseManager.instance().getProperty(property);
                } else {
                    return null;
                }
            }
        };
    }}
