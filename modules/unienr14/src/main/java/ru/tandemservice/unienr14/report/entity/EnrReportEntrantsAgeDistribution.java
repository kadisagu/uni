package ru.tandemservice.unienr14.report.entity;

import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsAgeDistributionAdd.EnrReportEntrantsAgeDistributionAdd;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrReport;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrStorableReportDesc;
import ru.tandemservice.unienr14.report.entity.gen.EnrReportEntrantsAgeDistributionGen;

import java.util.Arrays;
import java.util.List;

/**
 * Распределение абитуриентов по возрасту и полу
 */
public class EnrReportEntrantsAgeDistribution extends EnrReportEntrantsAgeDistributionGen implements IEnrReport
{
    public static final String REPORT_KEY = "enr14ReportEntrantsAgeDistribution";


    @SuppressWarnings("unchecked")
    private static List<String> properties = Arrays.asList(
            P_REQUEST_TYPE,
            P_PROGRAM_FORM,
            P_ENR_ORG_UNIT,
            P_FORMATIVE_ORG_UNIT,
            P_PROGRAM_SUBJECT,
            P_EDU_PROGRAM,
            P_PROGRAM_SET,
            P_PARALLEL,
            P_CALCULATION_AGE_DATE
    );

    @Override
    public IEnrStorableReportDesc getDesc() {
        return getDescripton();
    }

    @Override
    public String getPeriodTitle() {
        return "с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateFrom()) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateTo());
    }

    public static IEnrStorableReportDesc getDescripton() {
        return new IEnrStorableReportDesc() {
            @Override public String getReportKey() { return REPORT_KEY; }
            @Override public Class<? extends IEnrReport> getReportClass() { return EnrReportEntrantsAgeDistribution.class; }
            @Override public List<String> getPropertyList() { return properties; }

            @Override public Class<? extends BusinessComponentManager> getAddFormComponent() { return EnrReportEntrantsAgeDistributionAdd.class; }
            @Override public String getPubTitle() { return "Отчет «Распределение абитуриентов по возрасту и полу»"; }
            @Override public String getListTitle() { return "Список отчетов «Распределение абитуриентов по возрасту и полу»"; }
        };
    }


}