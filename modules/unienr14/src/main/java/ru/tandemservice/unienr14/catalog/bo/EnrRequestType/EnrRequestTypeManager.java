/* $Id:$ */
package ru.tandemservice.unienr14.catalog.bo.EnrRequestType;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import ru.tandemservice.unienr14.catalog.bo.EnrRequestType.logic.EnrRequestTypeDao;
import ru.tandemservice.unienr14.catalog.bo.EnrRequestType.logic.IEnrRequestTypeDao;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;

/**
 * @author nvankov
 * @since 15.06.2016
 */
@Configuration
public class EnrRequestTypeManager extends BusinessObjectManager
{
    public static final String DS_REQUEST_TYPE = "requestTypeDS";

    public static EnrRequestTypeManager instance()
    {
        return instance(EnrRequestTypeManager.class);
    }

    @Bean
    public IEnrRequestTypeDao dao()
    {
        return new EnrRequestTypeDao();
    }

    /**
     * ДС1: выводить названия активных элементов справочника в порядке возрастания их приоритетов
     */
    @Bean
    public UIDataSourceConfig requestTypeEnabledDSConfig()
    {
        return SelectDSConfig.with(DS_REQUEST_TYPE, this.getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(requestTypeEnabledDSHandler())
                .create();
    }

    /**
     * ДС1: выводить названия активных элементов справочника в порядке возрастания их приоритетов
     */
    @Bean
    public IDefaultComboDataSourceHandler requestTypeEnabledDSHandler()
    {
        return EnrRequestType.defaultSelectDSHandler(getName(), true);
    }


    /**
     * виды заявлений, полученные по наборам ОП указанной приемной кампании
     */
    @Bean
    public UIDataSourceConfig requestTypeEnrCampaignDSConfig()
    {
        return SelectDSConfig.with(DS_REQUEST_TYPE, this.getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(requestTypeEnrCampaignDSHandler())
                .create();
    }

    /**
     * виды заявлений, полученные по наборам ОП указанной приемной кампании
     */
    @Bean
    public IDefaultComboDataSourceHandler requestTypeEnrCampaignDSHandler()
    {
        return EnrRequestType.requestTypeEnrCampaignDSHandler(getName(), false);
    }


    /**
     * ДС2: выводить названия активных элементов справочника, связанных с видами ОП высшего образования, в порядке возрастания их приоритетов
     */
    @Bean
    public UIDataSourceConfig requestTypeProgramKindHighDSConfig()
    {
        return SelectDSConfig.with(DS_REQUEST_TYPE, this.getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(requestTypeProgramKindHighDSHandler())
                .create();
    }

    /**
     * ДС2: выводить названия активных элементов справочника, связанных с видами ОП высшего образования, в порядке возрастания их приоритетов
     */
    @Bean
    public IDefaultComboDataSourceHandler requestTypeProgramKindHighDSHandler()
    {
        return EnrRequestType.requestTypeProgramKindDSHandler(getName(), true);
    }
}
