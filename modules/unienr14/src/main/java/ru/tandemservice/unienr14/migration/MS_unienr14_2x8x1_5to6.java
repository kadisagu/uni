package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x8x1_5to6 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrInternalExamReason

		// создано свойство userCode
		{
			// создать колонку
			tool.createColumn("enr14_c_int_exam_reason_t", new DBColumn("usercode_p", DBType.createVarchar(255)));
		}

		// создано свойство disabledDate
		{
			// создать колонку
			tool.createColumn("enr14_c_int_exam_reason_t", new DBColumn("disableddate_p", DBType.TIMESTAMP));
		}

		//  свойство title стало обязательным
		{
			// сделать колонку NOT NULL
			tool.setColumnNullable("enr14_c_int_exam_reason_t", "title_p", false);
		}
    }
}