/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EnrollmentCampaignPassSummaryAdd;/**
 * @author rsizonenko
 * @since 10.06.2014
 */

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.EnrEnrollmentCommissionManager;

@Configuration
public class EnrReportEnrollmentCampaignPassSummaryAdd extends BusinessComponentManager
{
    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addAddon(uiAddon(CommonFilterAddon.class.getSimpleName(), EnrCompetitionFilterAddon.class))
                .addDataSource(EnrEnrollmentCommissionManager.instance().enrollmentCommissionDSConfig())
                .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
                .create();
    }
}
