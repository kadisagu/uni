package ru.tandemservice.unienr14.entrant.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantOriginalDocumentStatus;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Оригинал документов
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrEntrantOriginalDocumentStatusGen extends EntityBase
 implements INaturalIdentifiable<EnrEntrantOriginalDocumentStatusGen>, ILazyFieldOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.entrant.entity.EnrEntrantOriginalDocumentStatus";
    public static final String ENTITY_NAME = "enrEntrantOriginalDocumentStatus";
    public static final int VERSION_HASH = 22951141;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTRANT = "entrant";
    public static final String L_EDU_DOCUMENT = "eduDocument";
    public static final String L_REQUESTED_COMPETITION = "requestedCompetition";
    public static final String P_REGISTRATION_DATE = "registrationDate";
    public static final String P_TAKE_AWAY_DATE = "takeAwayDate";
    public static final String P_ORIGINAL_DOCUMENT_HANDED_IN = "originalDocumentHandedIn";

    private EnrEntrant _entrant;     // Абитуриент
    private PersonEduDocument _eduDocument;     // Документ о полученном образовании
    private EnrRequestedCompetition _requestedCompetition;     // Выбранный конкурс
    private Date _registrationDate;     // Дата подачи оригинала
    private Date _takeAwayDate = null;     // Дата отзыва оригинала
    

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     */
    @NotNull
    public EnrEntrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant Абитуриент. Свойство не может быть null.
     */
    public void setEntrant(EnrEntrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    /**
     * @return Документ о полученном образовании. Свойство не может быть null.
     */
    @NotNull
    public PersonEduDocument getEduDocument()
    {
        return _eduDocument;
    }

    /**
     * @param eduDocument Документ о полученном образовании. Свойство не может быть null.
     */
    public void setEduDocument(PersonEduDocument eduDocument)
    {
        dirty(_eduDocument, eduDocument);
        _eduDocument = eduDocument;
    }

    /**
     * @return Выбранный конкурс. Свойство должно быть уникальным.
     */
    public EnrRequestedCompetition getRequestedCompetition()
    {
        return _requestedCompetition;
    }

    /**
     * @param requestedCompetition Выбранный конкурс. Свойство должно быть уникальным.
     */
    public void setRequestedCompetition(EnrRequestedCompetition requestedCompetition)
    {
        dirty(_requestedCompetition, requestedCompetition);
        _requestedCompetition = requestedCompetition;
    }

    /**
     * @return Дата подачи оригинала. Свойство не может быть null.
     */
    @NotNull
    public Date getRegistrationDate()
    {
        return _registrationDate;
    }

    /**
     * @param registrationDate Дата подачи оригинала. Свойство не может быть null.
     */
    public void setRegistrationDate(Date registrationDate)
    {
        dirty(_registrationDate, registrationDate);
        _registrationDate = registrationDate;
    }

    /**
     * @return Дата отзыва оригинала.
     */
    public Date getTakeAwayDate()
    {
        return _takeAwayDate;
    }

    /**
     * @param takeAwayDate Дата отзыва оригинала.
     */
    public void setTakeAwayDate(Date takeAwayDate)
    {
        dirty(_takeAwayDate, takeAwayDate);
        _takeAwayDate = takeAwayDate;
    }

    public static boolean isOriginalDocumentHandedIn(Date regDate, Date awayDate) {
				        boolean originalDocumentHandedIn = ((null == awayDate) || (regDate.after(awayDate)));
        				return originalDocumentHandedIn;
    				}
				
				 	public boolean isOriginalDocumentHandedIn() {
				 		Date regDate = getRegistrationDate();
				 		Date awayDate = getTakeAwayDate();
				 		return isOriginalDocumentHandedIn(regDate, awayDate);
				 	}

    public void setOriginalDocumentHandedIn(boolean value) {
				   	    if (isInitLazyInProgress()) { return; }
				   	    if (value == isOriginalDocumentHandedIn()) { return; } 
				   	    if (value) {
				   	    	setRegistrationDate(new Date());
				   	    } else {
				   	    	setTakeAwayDate(new Date());
				   	    }
				   	}

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrEntrantOriginalDocumentStatusGen)
        {
            if (withNaturalIdProperties)
            {
                setEntrant(((EnrEntrantOriginalDocumentStatus)another).getEntrant());
                setEduDocument(((EnrEntrantOriginalDocumentStatus)another).getEduDocument());
            }
            setRequestedCompetition(((EnrEntrantOriginalDocumentStatus)another).getRequestedCompetition());
            setRegistrationDate(((EnrEntrantOriginalDocumentStatus)another).getRegistrationDate());
            setTakeAwayDate(((EnrEntrantOriginalDocumentStatus)another).getTakeAwayDate());
            setOriginalDocumentHandedIn(((EnrEntrantOriginalDocumentStatus)another).isOriginalDocumentHandedIn());
        }
    }

    public INaturalId<EnrEntrantOriginalDocumentStatusGen> getNaturalId()
    {
        return new NaturalId(getEntrant(), getEduDocument());
    }

    public static class NaturalId extends NaturalIdBase<EnrEntrantOriginalDocumentStatusGen>
    {
        private static final String PROXY_NAME = "EnrEntrantOriginalDocumentStatusNaturalProxy";

        private Long _entrant;
        private Long _eduDocument;

        public NaturalId()
        {}

        public NaturalId(EnrEntrant entrant, PersonEduDocument eduDocument)
        {
            _entrant = ((IEntity) entrant).getId();
            _eduDocument = ((IEntity) eduDocument).getId();
        }

        public Long getEntrant()
        {
            return _entrant;
        }

        public void setEntrant(Long entrant)
        {
            _entrant = entrant;
        }

        public Long getEduDocument()
        {
            return _eduDocument;
        }

        public void setEduDocument(Long eduDocument)
        {
            _eduDocument = eduDocument;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrEntrantOriginalDocumentStatusGen.NaturalId) ) return false;

            EnrEntrantOriginalDocumentStatusGen.NaturalId that = (NaturalId) o;

            if( !equals(getEntrant(), that.getEntrant()) ) return false;
            if( !equals(getEduDocument(), that.getEduDocument()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEntrant());
            result = hashCode(result, getEduDocument());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEntrant());
            sb.append("/");
            sb.append(getEduDocument());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrEntrantOriginalDocumentStatusGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrEntrantOriginalDocumentStatus.class;
        }

        public T newInstance()
        {
            return (T) new EnrEntrantOriginalDocumentStatus();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entrant":
                    return obj.getEntrant();
                case "eduDocument":
                    return obj.getEduDocument();
                case "requestedCompetition":
                    return obj.getRequestedCompetition();
                case "registrationDate":
                    return obj.getRegistrationDate();
                case "takeAwayDate":
                    return obj.getTakeAwayDate();
                case "originalDocumentHandedIn":
                    return obj.isOriginalDocumentHandedIn();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entrant":
                    obj.setEntrant((EnrEntrant) value);
                    return;
                case "eduDocument":
                    obj.setEduDocument((PersonEduDocument) value);
                    return;
                case "requestedCompetition":
                    obj.setRequestedCompetition((EnrRequestedCompetition) value);
                    return;
                case "registrationDate":
                    obj.setRegistrationDate((Date) value);
                    return;
                case "takeAwayDate":
                    obj.setTakeAwayDate((Date) value);
                    return;
                case "originalDocumentHandedIn":
                    obj.setOriginalDocumentHandedIn((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entrant":
                        return true;
                case "eduDocument":
                        return true;
                case "requestedCompetition":
                        return true;
                case "registrationDate":
                        return true;
                case "takeAwayDate":
                        return true;
                case "originalDocumentHandedIn":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entrant":
                    return true;
                case "eduDocument":
                    return true;
                case "requestedCompetition":
                    return true;
                case "registrationDate":
                    return true;
                case "takeAwayDate":
                    return true;
                case "originalDocumentHandedIn":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entrant":
                    return EnrEntrant.class;
                case "eduDocument":
                    return PersonEduDocument.class;
                case "requestedCompetition":
                    return EnrRequestedCompetition.class;
                case "registrationDate":
                    return Date.class;
                case "takeAwayDate":
                    return Date.class;
                case "originalDocumentHandedIn":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrEntrantOriginalDocumentStatus> _dslPath = new Path<EnrEntrantOriginalDocumentStatus>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrEntrantOriginalDocumentStatus");
    }
            

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantOriginalDocumentStatus#getEntrant()
     */
    public static EnrEntrant.Path<EnrEntrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return Документ о полученном образовании. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantOriginalDocumentStatus#getEduDocument()
     */
    public static PersonEduDocument.Path<PersonEduDocument> eduDocument()
    {
        return _dslPath.eduDocument();
    }

    /**
     * @return Выбранный конкурс. Свойство должно быть уникальным.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantOriginalDocumentStatus#getRequestedCompetition()
     */
    public static EnrRequestedCompetition.Path<EnrRequestedCompetition> requestedCompetition()
    {
        return _dslPath.requestedCompetition();
    }

    /**
     * @return Дата подачи оригинала. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantOriginalDocumentStatus#getRegistrationDate()
     */
    public static PropertyPath<Date> registrationDate()
    {
        return _dslPath.registrationDate();
    }

    /**
     * @return Дата отзыва оригинала.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantOriginalDocumentStatus#getTakeAwayDate()
     */
    public static PropertyPath<Date> takeAwayDate()
    {
        return _dslPath.takeAwayDate();
    }

    /**
     * Формула, вычисляется на основе комбинации полей registrationDate и takeAwayDate:
     * true, если дата возврата пустая (ни разу не отзывали оригинал), или раньше даты приема документов (повторно подали оригинал)
     *
     * @return Принят оригинал док. об образ.. Свойство не может быть null.
     *
     * Это формула "case when ((takeAwayDate is null) or (registrationDate > takeAwayDate)) then true else false end".
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantOriginalDocumentStatus#isOriginalDocumentHandedIn()
     */
    public static PropertyPath<Boolean> originalDocumentHandedIn()
    {
        return _dslPath.originalDocumentHandedIn();
    }

    public static class Path<E extends EnrEntrantOriginalDocumentStatus> extends EntityPath<E>
    {
        private EnrEntrant.Path<EnrEntrant> _entrant;
        private PersonEduDocument.Path<PersonEduDocument> _eduDocument;
        private EnrRequestedCompetition.Path<EnrRequestedCompetition> _requestedCompetition;
        private PropertyPath<Date> _registrationDate;
        private PropertyPath<Date> _takeAwayDate;
        private PropertyPath<Boolean> _originalDocumentHandedIn;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantOriginalDocumentStatus#getEntrant()
     */
        public EnrEntrant.Path<EnrEntrant> entrant()
        {
            if(_entrant == null )
                _entrant = new EnrEntrant.Path<EnrEntrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return Документ о полученном образовании. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantOriginalDocumentStatus#getEduDocument()
     */
        public PersonEduDocument.Path<PersonEduDocument> eduDocument()
        {
            if(_eduDocument == null )
                _eduDocument = new PersonEduDocument.Path<PersonEduDocument>(L_EDU_DOCUMENT, this);
            return _eduDocument;
        }

    /**
     * @return Выбранный конкурс. Свойство должно быть уникальным.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantOriginalDocumentStatus#getRequestedCompetition()
     */
        public EnrRequestedCompetition.Path<EnrRequestedCompetition> requestedCompetition()
        {
            if(_requestedCompetition == null )
                _requestedCompetition = new EnrRequestedCompetition.Path<EnrRequestedCompetition>(L_REQUESTED_COMPETITION, this);
            return _requestedCompetition;
        }

    /**
     * @return Дата подачи оригинала. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantOriginalDocumentStatus#getRegistrationDate()
     */
        public PropertyPath<Date> registrationDate()
        {
            if(_registrationDate == null )
                _registrationDate = new PropertyPath<Date>(EnrEntrantOriginalDocumentStatusGen.P_REGISTRATION_DATE, this);
            return _registrationDate;
        }

    /**
     * @return Дата отзыва оригинала.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantOriginalDocumentStatus#getTakeAwayDate()
     */
        public PropertyPath<Date> takeAwayDate()
        {
            if(_takeAwayDate == null )
                _takeAwayDate = new PropertyPath<Date>(EnrEntrantOriginalDocumentStatusGen.P_TAKE_AWAY_DATE, this);
            return _takeAwayDate;
        }

    /**
     * Формула, вычисляется на основе комбинации полей registrationDate и takeAwayDate:
     * true, если дата возврата пустая (ни разу не отзывали оригинал), или раньше даты приема документов (повторно подали оригинал)
     *
     * @return Принят оригинал док. об образ.. Свойство не может быть null.
     *
     * Это формула "case when ((takeAwayDate is null) or (registrationDate > takeAwayDate)) then true else false end".
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantOriginalDocumentStatus#isOriginalDocumentHandedIn()
     */
        public PropertyPath<Boolean> originalDocumentHandedIn()
        {
            if(_originalDocumentHandedIn == null )
                _originalDocumentHandedIn = new PropertyPath<Boolean>(EnrEntrantOriginalDocumentStatusGen.P_ORIGINAL_DOCUMENT_HANDED_IN, this);
            return _originalDocumentHandedIn;
        }

        public Class getEntityClass()
        {
            return EnrEntrantOriginalDocumentStatus.class;
        }

        public String getEntityName()
        {
            return "enrEntrantOriginalDocumentStatus";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
