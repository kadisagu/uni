package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x0_13to14 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrRequestedCompetition

		// создано обязательное свойство profileEducation
		{
			// создать колонку
			tool.createColumn("enr14_requested_comp_t", new DBColumn("profileeducation_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			boolean defaultProfileEducation = false;
			tool.executeUpdate("update enr14_requested_comp_t set profileeducation_p=? where profileeducation_p is null", defaultProfileEducation);

			// сделать колонку NOT NULL
			tool.setColumnNullable("enr14_requested_comp_t", "profileeducation_p", false);
		}
    }
}