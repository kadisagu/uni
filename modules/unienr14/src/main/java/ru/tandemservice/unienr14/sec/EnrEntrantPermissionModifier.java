// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unienr14.sec;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.ISecurityConfigMetaMapModifier;
import org.tandemframework.sec.meta.*;
import org.tandemframework.shared.commonbase.sec.PermissionMetaUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;

import java.util.Map;

/**
 * @author oleyba
 * @since 01.12.2010
 */
public class EnrEntrantPermissionModifier implements ISecurityConfigMetaMapModifier
{
    @Override
    public void modify(final Map<String, SecurityConfigMeta> securityConfigMetaMap)
    {
        final SecurityConfigMeta config = new SecurityConfigMeta();
        config.setModuleName("unienr14");
        config.setName("unienr14-sec-config");
        config.setTitle("");

        ModuleGlobalGroupMeta mggOrgstruct = PermissionMetaUtil.createModuleGlobalGroup(config, "orgstructModuleGlobalGroup", "Модуль «Орг. структура»");
        ModuleLocalGroupMeta mlgOrgstruct = PermissionMetaUtil.createModuleLocalGroup(config, "orgstructLocalGroup", "Модуль «Орг. структура»");

        for (OrgUnitType description : DataAccessServices.dao().getList(OrgUnitType.class, OrgUnitType.P_TITLE))
        {
            String code = description.getCode();
            ClassGroupMeta gcg = PermissionMetaUtil.createClassGroup(mggOrgstruct, code + "PermissionClassGroup", "Объект «" + description.getTitle() + "»", OrgUnit.class.getName());
            ClassGroupMeta localcg = PermissionMetaUtil.createClassGroup(mlgOrgstruct, code + "LocalClassGroup", "Объект «" + description.getTitle() + "»", OrgUnit.class.getName());

            PermissionGroupMeta pgEcfTab = PermissionMetaUtil.createPermissionGroup(config, code + "Enr14TabPermissionGroup", "Вкладка «Абитуриенты»");
            PermissionMetaUtil.createGroupRelation(config, pgEcfTab.getName(), gcg.getName());
            PermissionMetaUtil.createGroupRelation(config, pgEcfTab.getName(), localcg.getName());
            PermissionMetaUtil.createPermission(pgEcfTab, "orgUnit_viewEnr14Tab_" + code, "Просмотр");

            PermissionGroupMeta pgEcfPkgTab = PermissionMetaUtil.createPermissionGroup(pgEcfTab, code + "Enr14EntrantsTabPermissionGroup", "Вкладка «Абитуриенты»");
            PermissionMetaUtil.createPermission(pgEcfPkgTab, "orgUnit_viewEnr14EntrantsTab_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(pgEcfPkgTab, "orgUnit_archiveEnr14EntrantsTab_" + code, "Списание в архив");
            PermissionMetaUtil.createPermission(pgEcfPkgTab, "orgUnit_deleteEnr14EntrantsTab_" + code, "Удаление");
        }

        securityConfigMetaMap.put(config.getName(), config);


    }
}
