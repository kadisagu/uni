/* $Id$ */
package ru.tandemservice.unienr14.catalog.bo.EnrEduLevelRequirement.ui.ItemPub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.commonbase.component.catalog.CommonBaseCatalogDefine;
import ru.tandemservice.unienr14.catalog.entity.EnrEduLevelReqEduLevel;

/**
 * @author Nikolay Fedorovskih
 * @since 11.03.2015
 */
@Configuration
public class EnrEduLevelRequirementItemPub extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(searchListDS("reqEduLevelDS", reqEduLevelDSColumns(), programSetSearchDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint reqEduLevelDSColumns()
    {
        return columnListExtPointBuilder("reqEduLevelDS")
                .addColumn(publisherColumn("eduLevel", EnrEduLevelReqEduLevel.eduLevel().title())
                                   .primaryKeyPath(EnrEduLevelReqEduLevel.eduLevel().id())
                                   .businessComponent(CommonBaseCatalogDefine.DEFAULT_CATALOG_ITEM_PUB))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> programSetSearchDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrEduLevelReqEduLevel.class)
                .where(EnrEduLevelReqEduLevel.enrEduLevelRequirement(), EnrEduLevelRequirementItemPubUI.EDU_LEVEL_REQUIREMENT_ID_FILTER_PROPERTY)
                .order(EnrEduLevelReqEduLevel.eduLevel().code());
    }

}