package ru.tandemservice.unienr14.enrollment.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.enrollment.entity.EnrModelStep;
import ru.tandemservice.unienr14.enrollment.entity.EnrModelStepItem;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Абитуриент в модели
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrModelStepItemGen extends EntityBase
 implements INaturalIdentifiable<EnrModelStepItemGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.enrollment.entity.EnrModelStepItem";
    public static final String ENTITY_NAME = "enrModelStepItem";
    public static final int VERSION_HASH = 99574788;
    private static IEntityMeta ENTITY_META;

    public static final String L_STEP = "step";
    public static final String L_ENTITY = "entity";
    public static final String P_ORIGINAL_IN = "originalIn";
    public static final String P_ACCEPTED = "accepted";
    public static final String P_ENROLLMENT_AVAILABLE = "enrollmentAvailable";
    public static final String P_INCLUDED = "included";
    public static final String P_ENROLLED = "enrolled";
    public static final String P_RE_ENROLLED = "reEnrolled";

    private EnrModelStep _step;     // Шаг
    private EnrRatingItem _entity;     // Абитуриент
    private boolean _originalIn = false;     // Сдан оригинал док. об образ.
    private boolean _accepted = false;     // Согласие на зачисление
    private boolean _enrollmentAvailable = false;     // Согласие на зачисление (итоговое)
    private boolean _included;     // Включен в конкурсный список
    private boolean _enrolled;     // Зачислен
    private boolean _reEnrolled;     // Перезачислен на другой конкурс

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Шаг. Свойство не может быть null.
     */
    @NotNull
    public EnrModelStep getStep()
    {
        return _step;
    }

    /**
     * @param step Шаг. Свойство не может быть null.
     */
    public void setStep(EnrModelStep step)
    {
        dirty(_step, step);
        _step = step;
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     */
    @NotNull
    public EnrRatingItem getEntity()
    {
        return _entity;
    }

    /**
     * @param entity Абитуриент. Свойство не может быть null.
     */
    public void setEntity(EnrRatingItem entity)
    {
        dirty(_entity, entity);
        _entity = entity;
    }

    /**
     * Сдан оригинал документа об образовании, использованного при подаче соотв. заявления.
     *
     * @return Сдан оригинал док. об образ.. Свойство не может быть null.
     */
    @NotNull
    public boolean isOriginalIn()
    {
        return _originalIn;
    }

    /**
     * @param originalIn Сдан оригинал док. об образ.. Свойство не может быть null.
     */
    public void setOriginalIn(boolean originalIn)
    {
        dirty(_originalIn, originalIn);
        _originalIn = originalIn;
    }

    /**
     * Получено согласие на зачисление.
     *
     * @return Согласие на зачисление. Свойство не может быть null.
     */
    @NotNull
    public boolean isAccepted()
    {
        return _accepted;
    }

    /**
     * @param accepted Согласие на зачисление. Свойство не может быть null.
     */
    public void setAccepted(boolean accepted)
    {
        dirty(_accepted, accepted);
        _accepted = accepted;
    }

    /**
     * Итоговое согласие на зачисление.
     *
     * @return Согласие на зачисление (итоговое). Свойство не может быть null.
     */
    @NotNull
    public boolean isEnrollmentAvailable()
    {
        return _enrollmentAvailable;
    }

    /**
     * @param enrollmentAvailable Согласие на зачисление (итоговое). Свойство не может быть null.
     */
    public void setEnrollmentAvailable(boolean enrollmentAvailable)
    {
        dirty(_enrollmentAvailable, enrollmentAvailable);
        _enrollmentAvailable = enrollmentAvailable;
    }

    /**
     * У кого included - false, должны интерпретироваться, как отсутствующие в данном списке, несмотря на то, что объекты есть.
     *
     * @return Включен в конкурсный список. Свойство не может быть null.
     */
    @NotNull
    public boolean isIncluded()
    {
        return _included;
    }

    /**
     * @param included Включен в конкурсный список. Свойство не может быть null.
     */
    public void setIncluded(boolean included)
    {
        dirty(_included, included);
        _included = included;
    }

    /**
     * Зачислен.
     *
     * @return Зачислен. Свойство не может быть null.
     */
    @NotNull
    public boolean isEnrolled()
    {
        return _enrolled;
    }

    /**
     * @param enrolled Зачислен. Свойство не может быть null.
     */
    public void setEnrolled(boolean enrolled)
    {
        dirty(_enrolled, enrolled);
        _enrolled = enrolled;
    }

    /**
     * Зачислен на другой конкурс с отменой отчисления по этому конкурсу.
     *
     * @return Перезачислен на другой конкурс. Свойство не может быть null.
     */
    @NotNull
    public boolean isReEnrolled()
    {
        return _reEnrolled;
    }

    /**
     * @param reEnrolled Перезачислен на другой конкурс. Свойство не может быть null.
     */
    public void setReEnrolled(boolean reEnrolled)
    {
        dirty(_reEnrolled, reEnrolled);
        _reEnrolled = reEnrolled;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrModelStepItemGen)
        {
            if (withNaturalIdProperties)
            {
                setStep(((EnrModelStepItem)another).getStep());
                setEntity(((EnrModelStepItem)another).getEntity());
            }
            setOriginalIn(((EnrModelStepItem)another).isOriginalIn());
            setAccepted(((EnrModelStepItem)another).isAccepted());
            setEnrollmentAvailable(((EnrModelStepItem)another).isEnrollmentAvailable());
            setIncluded(((EnrModelStepItem)another).isIncluded());
            setEnrolled(((EnrModelStepItem)another).isEnrolled());
            setReEnrolled(((EnrModelStepItem)another).isReEnrolled());
        }
    }

    public INaturalId<EnrModelStepItemGen> getNaturalId()
    {
        return new NaturalId(getStep(), getEntity());
    }

    public static class NaturalId extends NaturalIdBase<EnrModelStepItemGen>
    {
        private static final String PROXY_NAME = "EnrModelStepItemNaturalProxy";

        private Long _step;
        private Long _entity;

        public NaturalId()
        {}

        public NaturalId(EnrModelStep step, EnrRatingItem entity)
        {
            _step = ((IEntity) step).getId();
            _entity = ((IEntity) entity).getId();
        }

        public Long getStep()
        {
            return _step;
        }

        public void setStep(Long step)
        {
            _step = step;
        }

        public Long getEntity()
        {
            return _entity;
        }

        public void setEntity(Long entity)
        {
            _entity = entity;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrModelStepItemGen.NaturalId) ) return false;

            EnrModelStepItemGen.NaturalId that = (NaturalId) o;

            if( !equals(getStep(), that.getStep()) ) return false;
            if( !equals(getEntity(), that.getEntity()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getStep());
            result = hashCode(result, getEntity());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getStep());
            sb.append("/");
            sb.append(getEntity());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrModelStepItemGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrModelStepItem.class;
        }

        public T newInstance()
        {
            return (T) new EnrModelStepItem();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "step":
                    return obj.getStep();
                case "entity":
                    return obj.getEntity();
                case "originalIn":
                    return obj.isOriginalIn();
                case "accepted":
                    return obj.isAccepted();
                case "enrollmentAvailable":
                    return obj.isEnrollmentAvailable();
                case "included":
                    return obj.isIncluded();
                case "enrolled":
                    return obj.isEnrolled();
                case "reEnrolled":
                    return obj.isReEnrolled();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "step":
                    obj.setStep((EnrModelStep) value);
                    return;
                case "entity":
                    obj.setEntity((EnrRatingItem) value);
                    return;
                case "originalIn":
                    obj.setOriginalIn((Boolean) value);
                    return;
                case "accepted":
                    obj.setAccepted((Boolean) value);
                    return;
                case "enrollmentAvailable":
                    obj.setEnrollmentAvailable((Boolean) value);
                    return;
                case "included":
                    obj.setIncluded((Boolean) value);
                    return;
                case "enrolled":
                    obj.setEnrolled((Boolean) value);
                    return;
                case "reEnrolled":
                    obj.setReEnrolled((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "step":
                        return true;
                case "entity":
                        return true;
                case "originalIn":
                        return true;
                case "accepted":
                        return true;
                case "enrollmentAvailable":
                        return true;
                case "included":
                        return true;
                case "enrolled":
                        return true;
                case "reEnrolled":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "step":
                    return true;
                case "entity":
                    return true;
                case "originalIn":
                    return true;
                case "accepted":
                    return true;
                case "enrollmentAvailable":
                    return true;
                case "included":
                    return true;
                case "enrolled":
                    return true;
                case "reEnrolled":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "step":
                    return EnrModelStep.class;
                case "entity":
                    return EnrRatingItem.class;
                case "originalIn":
                    return Boolean.class;
                case "accepted":
                    return Boolean.class;
                case "enrollmentAvailable":
                    return Boolean.class;
                case "included":
                    return Boolean.class;
                case "enrolled":
                    return Boolean.class;
                case "reEnrolled":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrModelStepItem> _dslPath = new Path<EnrModelStepItem>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrModelStepItem");
    }
            

    /**
     * @return Шаг. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelStepItem#getStep()
     */
    public static EnrModelStep.Path<EnrModelStep> step()
    {
        return _dslPath.step();
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelStepItem#getEntity()
     */
    public static EnrRatingItem.Path<EnrRatingItem> entity()
    {
        return _dslPath.entity();
    }

    /**
     * Сдан оригинал документа об образовании, использованного при подаче соотв. заявления.
     *
     * @return Сдан оригинал док. об образ.. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelStepItem#isOriginalIn()
     */
    public static PropertyPath<Boolean> originalIn()
    {
        return _dslPath.originalIn();
    }

    /**
     * Получено согласие на зачисление.
     *
     * @return Согласие на зачисление. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelStepItem#isAccepted()
     */
    public static PropertyPath<Boolean> accepted()
    {
        return _dslPath.accepted();
    }

    /**
     * Итоговое согласие на зачисление.
     *
     * @return Согласие на зачисление (итоговое). Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelStepItem#isEnrollmentAvailable()
     */
    public static PropertyPath<Boolean> enrollmentAvailable()
    {
        return _dslPath.enrollmentAvailable();
    }

    /**
     * У кого included - false, должны интерпретироваться, как отсутствующие в данном списке, несмотря на то, что объекты есть.
     *
     * @return Включен в конкурсный список. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelStepItem#isIncluded()
     */
    public static PropertyPath<Boolean> included()
    {
        return _dslPath.included();
    }

    /**
     * Зачислен.
     *
     * @return Зачислен. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelStepItem#isEnrolled()
     */
    public static PropertyPath<Boolean> enrolled()
    {
        return _dslPath.enrolled();
    }

    /**
     * Зачислен на другой конкурс с отменой отчисления по этому конкурсу.
     *
     * @return Перезачислен на другой конкурс. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelStepItem#isReEnrolled()
     */
    public static PropertyPath<Boolean> reEnrolled()
    {
        return _dslPath.reEnrolled();
    }

    public static class Path<E extends EnrModelStepItem> extends EntityPath<E>
    {
        private EnrModelStep.Path<EnrModelStep> _step;
        private EnrRatingItem.Path<EnrRatingItem> _entity;
        private PropertyPath<Boolean> _originalIn;
        private PropertyPath<Boolean> _accepted;
        private PropertyPath<Boolean> _enrollmentAvailable;
        private PropertyPath<Boolean> _included;
        private PropertyPath<Boolean> _enrolled;
        private PropertyPath<Boolean> _reEnrolled;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Шаг. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelStepItem#getStep()
     */
        public EnrModelStep.Path<EnrModelStep> step()
        {
            if(_step == null )
                _step = new EnrModelStep.Path<EnrModelStep>(L_STEP, this);
            return _step;
        }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelStepItem#getEntity()
     */
        public EnrRatingItem.Path<EnrRatingItem> entity()
        {
            if(_entity == null )
                _entity = new EnrRatingItem.Path<EnrRatingItem>(L_ENTITY, this);
            return _entity;
        }

    /**
     * Сдан оригинал документа об образовании, использованного при подаче соотв. заявления.
     *
     * @return Сдан оригинал док. об образ.. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelStepItem#isOriginalIn()
     */
        public PropertyPath<Boolean> originalIn()
        {
            if(_originalIn == null )
                _originalIn = new PropertyPath<Boolean>(EnrModelStepItemGen.P_ORIGINAL_IN, this);
            return _originalIn;
        }

    /**
     * Получено согласие на зачисление.
     *
     * @return Согласие на зачисление. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelStepItem#isAccepted()
     */
        public PropertyPath<Boolean> accepted()
        {
            if(_accepted == null )
                _accepted = new PropertyPath<Boolean>(EnrModelStepItemGen.P_ACCEPTED, this);
            return _accepted;
        }

    /**
     * Итоговое согласие на зачисление.
     *
     * @return Согласие на зачисление (итоговое). Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelStepItem#isEnrollmentAvailable()
     */
        public PropertyPath<Boolean> enrollmentAvailable()
        {
            if(_enrollmentAvailable == null )
                _enrollmentAvailable = new PropertyPath<Boolean>(EnrModelStepItemGen.P_ENROLLMENT_AVAILABLE, this);
            return _enrollmentAvailable;
        }

    /**
     * У кого included - false, должны интерпретироваться, как отсутствующие в данном списке, несмотря на то, что объекты есть.
     *
     * @return Включен в конкурсный список. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelStepItem#isIncluded()
     */
        public PropertyPath<Boolean> included()
        {
            if(_included == null )
                _included = new PropertyPath<Boolean>(EnrModelStepItemGen.P_INCLUDED, this);
            return _included;
        }

    /**
     * Зачислен.
     *
     * @return Зачислен. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelStepItem#isEnrolled()
     */
        public PropertyPath<Boolean> enrolled()
        {
            if(_enrolled == null )
                _enrolled = new PropertyPath<Boolean>(EnrModelStepItemGen.P_ENROLLED, this);
            return _enrolled;
        }

    /**
     * Зачислен на другой конкурс с отменой отчисления по этому конкурсу.
     *
     * @return Перезачислен на другой конкурс. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelStepItem#isReEnrolled()
     */
        public PropertyPath<Boolean> reEnrolled()
        {
            if(_reEnrolled == null )
                _reEnrolled = new PropertyPath<Boolean>(EnrModelStepItemGen.P_RE_ENROLLED, this);
            return _reEnrolled;
        }

        public Class getEntityClass()
        {
            return EnrModelStepItem.class;
        }

        public String getEntityName()
        {
            return "enrModelStepItem";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
