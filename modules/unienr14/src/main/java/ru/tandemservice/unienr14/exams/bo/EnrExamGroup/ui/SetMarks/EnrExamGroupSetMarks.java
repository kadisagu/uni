/**
 *$Id: enr14ExamGroupSetMarks.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.exams.bo.EnrExamGroup.ui.SetMarks;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.catalog.entity.EnrAbsenceNote;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;

/**
 * @author Alexander Shaburov
 * @since 09.07.13
 */
@Configuration
public class EnrExamGroupSetMarks extends BusinessComponentManager
{
    public static final String EXAM_PASS_DISCIPLINE_SEARCH_DS = "examPassDisciplineSearchDS";
    public static final String ABSENCE_NOTE_SELECT_DS = "absenceNoteSelectDS";

    public static final String BIND_EXAM_GROUP_ID = "bindExamGroupId";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
        .addDataSource(searchListDS(EXAM_PASS_DISCIPLINE_SEARCH_DS, examPassDisciplineSearchDSColumns(), examPassDisciplineSearchDSHandler()))
        .addDataSource(selectDS(ABSENCE_NOTE_SELECT_DS, absenceNoteSelectDSHandler()))
        .create();
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> absenceNoteSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrAbsenceNote.class)
        .order(EnrAbsenceNote.title())
        .filter(EnrAbsenceNote.title())
        .pageable(true);
    }

    @Bean
    public ColumnListExtPoint examPassDisciplineSearchDSColumns()
    {
        return columnListExtPointBuilder(EXAM_PASS_DISCIPLINE_SEARCH_DS)
        .addColumn(textColumn("personalNumber", EnrExamPassDiscipline.entrant().personalNumber()).order())
        .addColumn(publisherColumn("fio", EnrExamPassDiscipline.entrant().person().fullFio()).primaryKeyPath(EnrExamPassDiscipline.entrant().id()).parameters("mvel:['selectedTab':'requestTab']").order())
        .addColumn(textColumn("identityCard", EnrExamPassDiscipline.entrant().person().identityCard().fullNumber()))
        .addColumn(textColumn("code", EnrExamPassDiscipline.code()).order())
        .addColumn(headerColumn("marks")
            .addSubColumn(blockColumn("absenceNote").width("218").create())
            .addSubColumn(blockColumn("mark").width("109").create()))
            .create();
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> examPassDisciplineSearchDSHandler()
    {

        return new EntityComboDataSourceHandler(getName(), EnrExamPassDiscipline.class)
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                final DSOutput output = super.execute(input, context);
                return ListOutputBuilder.get(input, output.getRecordList()).pageable(isPageable()).build().ordering(new EntityComparator(input.getEntityOrder()));
            }
        }
        .where(EnrExamPassDiscipline.examGroup().id(), BIND_EXAM_GROUP_ID)
        .pageable(false);
    }
}
