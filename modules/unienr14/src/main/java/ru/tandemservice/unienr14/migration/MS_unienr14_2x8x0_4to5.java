package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;
import org.tandemframework.shared.commonbase.utils.MigrationUtils.BatchInsertBuilder;
import org.tandemframework.shared.commonbase.utils.MigrationUtils.BatchUpdater;

import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.List;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused"})
public class MS_unienr14_2x8x0_4to5 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrRequestType2eduProgramKindRel

		// создана новая сущность
		{
			// создать таблицу
			tool.createTable(new DBTable("enr14_req_type_2_prog_kind_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_ce653782"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("requesttype_id", DBType.LONG).setNullable(false), 
				new DBColumn("programkind_id", DBType.LONG).setNullable(false)
			));

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("enrRequestType2eduProgramKindRel");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrRequestType

		// создано обязательное свойство priority
		tool.createColumn("enr14_c_request_type_t", new DBColumn("priority_p", DBType.INTEGER));

		final BatchUpdater updater = new BatchUpdater("update enr14_c_request_type_t set priority_p=? where code_p=?", DBType.INTEGER, DBType.EMPTY_STRING);
		updater.addBatch(1, "1"); // бакалавриат/специалитет
		updater.addBatch(2, "2"); // магистратура
		updater.addBatch(3, "3"); // кадры высшей квалификации
		updater.addBatch(7, "4"); // спо
		updater.executeUpdate(tool);


		final BatchInsertBuilder insertBuilder = new BatchInsertBuilder("code_p", "shorttitle_p", "priority_p", "title_p");
		insertBuilder.setEntityCode(tool, "enrRequestType");
		final Long postgraduateRequestTypeId = insertBuilder.addRow("5", "аспирантура (адъюнктура)", 4, "Программы аспирантуры (адъюнктуры)");
		final Long traineeshipRequestTypeId = insertBuilder.addRow("6", "ординатура", 5, "Программы ординатуры");
		final Long internshipRequestTypeId = insertBuilder.addRow("7", "интернатура", 6, "Программы интернатуры");
		insertBuilder.executeInsert(tool, "enr14_c_request_type_t");

		// сделать колонку NOT NULL
		tool.setColumnNullable("enr14_c_request_type_t", "priority_p", false);

		// создано свойство userCode
		tool.createColumn("enr14_c_request_type_t", new DBColumn("usercode_p", DBType.createVarchar(255)));

		// создано свойство disabledDate
		tool.createColumn("enr14_c_request_type_t", new DBColumn("disableddate_p", DBType.TIMESTAMP));
		PreparedStatement statement = tool.prepareStatement("update enr14_c_request_type_t set disableddate_p=? where code_p=?");
		statement.setTimestamp(1, new Timestamp(System.currentTimeMillis()));
		statement.setString(2, "3");
		statement.execute();



		// Скопировать настройки индивидуальных достижений ПК для новых видов заявлений из настроек для кадров высшей квалификации
		final List<Object[]> higherAchievementSettingsRows = tool.executeQuery(
				MigrationUtils.processor(Long.class, Long.class),
				"select s.enrollmentcampaign_id, s.mxttlachvmntmrkaslng_p from enr14_achievement_sets_t s join enr14_c_request_type_t rt on s.requesttype_id=rt.id where rt.code_p=?", "3"
		);
		final BatchInsertBuilder achievInsert = new BatchInsertBuilder("enrollmentcampaign_id", "mxttlachvmntmrkaslng_p", "requesttype_id");
		achievInsert.setEntityCode(tool, "enrEntrantAchievementSettings");
		for (Object[] row : higherAchievementSettingsRows)
		{
			final Long enrCampaignId = (Long) row[0];
			final Long maxTotalAchievementMarkAsLong = (Long) row[1];
			achievInsert.addRow(enrCampaignId, maxTotalAchievementMarkAsLong, postgraduateRequestTypeId);
			achievInsert.addRow(enrCampaignId, maxTotalAchievementMarkAsLong, traineeshipRequestTypeId);
			achievInsert.addRow(enrCampaignId, maxTotalAchievementMarkAsLong, internshipRequestTypeId);
		}
		achievInsert.executeInsert(tool, "enr14_achievement_sets_t");
    }
}