/* $Id$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubAchievementTab;

import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrantAchievement.ui.AddEdit.EnrEntrantAchievementAddEdit;
import ru.tandemservice.unienr14.entrant.daemon.EnrEntrantDaemonBean;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement;
import ru.tandemservice.unienr14.entrant.entity.EnrPersonEduDocumentRel;
import ru.tandemservice.unienr14.entrant.entity.IEnrEntrantAchievementProofDocument;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.EnrEntrantRequestManager;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings;
import ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementType;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
/**
 * @author nvankov
 * @since 4/11/14
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "entrantId")
})
public class EnrEntrantPubAchievementTabUI extends UIPresenter
{
    private Long _entrantId;
    private List<EnrRatingItem> _summaryAchievementList = new ArrayList<>();
    private EnrRatingItem _currentItem;

    private EnrEnrollmentCampaignSettings campaignSettings;

    private boolean _showRequestColumn;


    @Override
    public void onComponentRefresh()
    {
        EnrEntrant entrant = IUniBaseDao.instance.get().getNotNull(EnrEntrant.class, getEntrantId());
        setCampaignSettings(entrant.getEnrollmentCampaign().getSettings());

        _showRequestColumn = EnrEntrantRequestManager.instance().dao().isShowRequestColumn(entrant.getId());

        updateSummaryAchievementList();
    }

    private void updateSummaryAchievementList()
    {
        Set<Long> requestIds = new HashSet<>();
        getSummaryAchievementList().clear();
        for (EnrRatingItem ratingItem : IUniBaseDao.instance.get().getList(EnrRatingItem.class, EnrRatingItem.requestedCompetition().request().entrant().id(), getEntrantId(), EnrRatingItem.requestedCompetition().priority().s())) {
            if (requestIds.contains(ratingItem.getRequestedCompetition().getRequest().getId())) continue;
            getSummaryAchievementList().add(ratingItem);
            requestIds.add(ratingItem.getRequestedCompetition().getRequest().getId());
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(EnrEntrantPubAchievementTab.INDIVIDUAL_ACHIEVEMENTS_DS.equals(dataSource.getName()))
            dataSource.put("entrantId", _entrantId);
    }

    // Listeners
    public void onClickAddAchievement(){ _uiActivation.asRegionDialog(EnrEntrantAchievementAddEdit.class).parameter("entrantId", _entrantId).activate(); }

    public void onEditEntityFromList() { _uiActivation.asRegionDialog(EnrEntrantAchievementAddEdit.class).parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong()).activate(); }

    public void onDeleteEntityFromList()
    {
        DataAccessServices.dao().doInTransaction(session ->
        {
            EnrEntrantAchievement achievement = getEntityByListenerParameterAsLong();
            session.delete(achievement);
            IEnrEntrantAchievementProofDocument document = achievement.getDocument();
            if (document instanceof EnrPersonEduDocumentRel)
            {
                List<EnrEntrantAchievement> achievements = DataAccessServices.dao().getList(EnrEntrantAchievement.class, EnrEntrantAchievement.document(), document);
                if (achievements.isEmpty())
                    session.delete(document);
            }
            return null;
        });
        EnrEntrantDaemonBean.DAEMON.wakeUpDaemon();
        getSupport().doRefresh();
    }


    // presenter

    public String getCurrentSummaryAchievementLabel() {
        EnrEntrantRequest request = getCurrentItem().getRequestedCompetition().getRequest();
        return request.getTitle() + " (" + request.getType().getShortTitle() + ")";
    }

    public String getCurrentSummaryAchievementMark() {
        return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(getCurrentItem().getAchievementMarkAsDouble()) + (isLimitExceededForRequest(getCurrentItem()) ? " (ограничено максимальным баллом)": "");
    }

    // Getters && Setters

    public boolean isNotEqRatingMark()
    {
        List<EnrEntrantAchievement> achievementList = DataAccessServices.dao().getList(EnrEntrantAchievement.class, EnrEntrantAchievement.entrant().id(), _entrantId);
        for (EnrEntrantAchievement achievement : achievementList)
        {
            Long mark = achievement.getType().isMarked() ? achievement.getMarkAsLong() : achievement.getType().getAchievementMarkAsLong();
            if (achievement.getRatingMarkAsLong() != mark) return true;
        }
        return false;
    }

    public boolean isShowRequestColumn()
    {
        return _showRequestColumn;
    }

    public boolean isDisplayAnyInfo()
    {
        return isLimitExceeded();
    }

    public boolean isLimitExceeded()
    {
        return CollectionUtils.exists(getSummaryAchievementList(), this::isLimitExceededForRequest) ;
    }

    private boolean isLimitExceededForRequest(EnrRatingItem ratingItem)
    {
        return ratingItem.getRequestedCompetition().getRequest().getEntrantAchievementMarkSum() > ratingItem.getAchievementMarkAsLong();
    }

    public Long getEntrantId(){ return _entrantId; }
    public void setEntrantId(Long entrantId){ _entrantId = entrantId; }

    public List<EnrRatingItem> getSummaryAchievementList()
    {
        return _summaryAchievementList;
    }

    public EnrRatingItem getCurrentItem()
    {
        return _currentItem;
    }

    public void setCurrentItem(EnrRatingItem currentItem)
    {
        _currentItem = currentItem;
    }

    public EnrEnrollmentCampaignSettings getCampaignSettings()
    {
        return campaignSettings;
    }

    public void setCampaignSettings(EnrEnrollmentCampaignSettings campaignSettings)
    {
        this.campaignSettings = campaignSettings;
    }
}
