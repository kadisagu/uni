/* $Id:$ */
package ru.tandemservice.unienr14.extreports.externalView;

import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.extview.SimpleDQLExternalViewConfig;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import org.tandemframework.shared.person.catalog.entity.EduDocumentKind;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import org.tandemframework.shared.person.catalog.entity.GraduationHonour;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.program.entity.EduProgramSecondaryProf;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetSecondary;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentMinisteryExtract;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentMinisteryParagraph;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unienr14.request.entity.EnrEntrantForeignRequest;
import ru.tandemservice.unimove.entity.catalog.OrderStates;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.*;

/**
 * @author oleyba
 * @since 9/12/14
 */
public class EnrExtViewProvider4ForeignRequest extends SimpleDQLExternalViewConfig
{
    @Override
    protected DQLSelectBuilder buildDqlQuery()
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(EnrEntrantForeignRequest.class, "f_request")

            .joinPath(DQLJoinType.left, EnrEntrantForeignRequest.identityCard().fromAlias("f_request"), "idc")
            .joinEntity("idc", DQLJoinType.left, AddressCountry.class, "citizenship_country", eq(property("idc", IdentityCard.citizenship().id()), property("citizenship_country", AddressCountry.id())))

            .joinPath(DQLJoinType.left, EnrEntrantForeignRequest.eduDocument().fromAlias("f_request"), "edu_doc")
            .joinPath(DQLJoinType.left, PersonEduDocument.eduDocumentKind().fromAlias("edu_doc"), "edu_doc_kind")
            .joinPath(DQLJoinType.left, PersonEduDocument.eduLevel().fromAlias("edu_doc"), "edu_level")
            .joinPath(DQLJoinType.left, PersonEduDocument.graduationHonour().fromAlias("edu_doc"), "grad_honour")
            .joinPath(DQLJoinType.left, PersonEduDocument.eduOrganizationAddressItem().fromAlias("edu_doc"), "edu_doc_address_item")
            .joinPath(DQLJoinType.left, AddressItem.country().fromAlias("edu_doc_address_item"), "edu_doc_address_country")
            .joinPath(DQLJoinType.left, EnrEntrantForeignRequest.competition().programSetOrgUnit().programSet().fromAlias("f_request"), "ps")
            .joinPath(DQLJoinType.left, EnrProgramSetBase.programSubject().subjectIndex().programKind().fromAlias("ps"), "program_kind")


            .joinEntity("ps", DQLJoinType.left, EnrProgramSetSecondary.class, "secondaryPS", eq(property(EnrProgramSetBase.id().fromAlias("ps")), property(EnrProgramSetSecondary.id().fromAlias("secondaryPS"))))

            .joinEntity("f_request", DQLJoinType.left, EnrEnrollmentMinisteryExtract.class, "enr_extract", eq(property(EnrEnrollmentMinisteryExtract.entrantRequest().fromAlias("enr_extract")), property("f_request")))
            .joinEntity("enr_extract", DQLJoinType.left, EnrEnrollmentMinisteryParagraph.class, "enr_par", eq(property(EnrEnrollmentMinisteryExtract.paragraph().fromAlias("enr_extract")), property("enr_par")))
            .joinEntity("enr_par", DQLJoinType.left, EnrOrder.class, "enr_order", eq(property(EnrEnrollmentMinisteryParagraph.order().fromAlias("enr_par")), property("enr_order")))
            .joinPath(DQLJoinType.left, EnrOrder.state().fromAlias("enr_order"), "enr_order_state")
                .joinPath(DQLJoinType.left, EnrProgramSetSecondary.program().fromAlias("secondaryPS"), "secondaryProgram")
                .joinPath(DQLJoinType.left, EduProgramSecondaryProf.baseLevel().fromAlias("secondaryProgram"), "baseLevel")
            ;

        column(dql, property("f_request", EnrEntrantForeignRequest.id()), "id").comment("uid направления");
        column(dql, property("f_request", EnrEntrantForeignRequest.enrollmentDate()), "enrollmentDate").comment("дата зачисления из направления");
        column(dql, property("f_request", EnrEntrantForeignRequest.regDate()), "regDate").comment("дата добавления направления");
        column(dql, property("f_request", EnrEntrantForeignRequest.regNumber()), "regNumber").comment("дата добавления направления");
        column(dql, property("f_request", EnrEntrantForeignRequest.competition().id()), "competitionId").comment("uid конкурса");
        column(dql, property("f_request", EnrEntrantForeignRequest.competition().programSetOrgUnit().programSet().programSubject().title()), "subject").comment("направление (профессия, специальность)");
        column(dql, property("f_request", EnrEntrantForeignRequest.competition().programSetOrgUnit().programSet().programSubject().subjectCode()), "subjectCode").comment("код направления (профессии, специальности)");
        column(dql, property("f_request", EnrEntrantForeignRequest.competition().programSetOrgUnit().programSet().programSubject().subjectIndex().programKind().code()), "programKindCode").comment("код вида обр. программы");

        column(dql, property("f_request", EnrEntrantForeignRequest.competition().programSetOrgUnit().orgUnit().id()), "enrOrgUnitId").comment("uid подразделения, ведущего прием по набору ОП");
        column(dql, property("f_request", EnrEntrantForeignRequest.competition().programSetOrgUnit().orgUnit().institutionOrgUnit().orgUnit().shortTitle()), "enrOrgUnitShortTitle").comment("сокращенное название филиала в рамках ПК");

        column(dql, property("f_request", EnrEntrantForeignRequest.competition().programSetOrgUnit().orgUnit().enrollmentCampaign().title()), "enrollmentCampaign").comment("приемная кампания");
        column(dql, property("f_request", EnrEntrantForeignRequest.competition().programSetOrgUnit().orgUnit().enrollmentCampaign().educationYear().intValue()), "enrollmentCampaignYear").comment("год приема (календарный)");

        column(dql, property("f_request", EnrEntrantForeignRequest.competition().programSetOrgUnit().programSet().programForm().title()), "programForm").comment("форма обучения");

        booleanIntColumn(dql, EduProgramKind.programBachelorDegree().fromAlias("program_kind"), "bachelor").comment("ОП по программе бакалавриата (1 - да, 0 - нет)");
        booleanIntColumn(dql, EduProgramKind.programHigherProf().fromAlias("program_kind"), "high").comment("ОП по программе высшего образования (1 - да, 0 - нет)");
        booleanIntColumn(dql, EduProgramKind.programMasterDegree().fromAlias("program_kind"), "master").comment("ОП по программе магистратуры (1 - да, 0 - нет)");
        booleanIntColumn(dql, EduProgramKind.programSecondaryProf().fromAlias("program_kind"), "middle").comment("ОП по программе СПО (1 - да, 0 - нет)");
        booleanIntColumn(dql, EduProgramKind.programSpecialistDegree().fromAlias("program_kind"), "specialty").comment("ОП по программе специалитета (1 - да, 0 - нет)");


        booleanIntColumn(dql, EduProgramSecondaryProf.inDepthStudy().fromAlias("secondaryProgram"), "inDepthStudy").comment("Углубленное изучение для ОП по программам СПО (1 - да, 0 - нет)");
        column(dql, property("baseLevel", EduLevel.code()), "baseLevelCode").comment("Код базы образования");

        column(dql, diffyears(createDate(year(property(EnrEntrantForeignRequest.regDate().fromAlias("f_request"))), value(1), value(1)), property(IdentityCard.birthDate().fromAlias("idc"))), "age").comment("полных лет (на момент подачи заявления)");
        column(dql, property(IdentityCard.birthDate().fromAlias("idc")), "birthDate").comment("дата рождения");
        column(dql, year(property(IdentityCard.birthDate().fromAlias("idc"))), "birthDateYear").comment("год рождения");
        column(dql, property(AddressCountry.title().fromAlias("citizenship_country")), "citizenship").comment("гражданство");
        column(dql, property(AddressCountry.digitalCode().fromAlias("citizenship_country")), "citizenshipCode").comment("код страны гражданства");
        column(dql, property(IdentityCard.firstName().fromAlias("idc")), "firstName").comment("имя");
        column(dql, property(IdentityCard.lastName().fromAlias("idc")), "lastName").comment("фамилия");
        column(dql, property(IdentityCard.middleName().fromAlias("idc")), "middleName").comment("отчество");
        column(dql, property(IdentityCard.fullFio().fromAlias("idc")), "fullFio").comment("ФИО");
        column(dql, property(IdentityCard.sex().title().fromAlias("idc")), "sex").comment("пол");
        column(dql, property(IdentityCard.sex().code().fromAlias("idc")), "sexCode").comment("код пола");

        column(dql, property(AddressCountry.title().fromAlias("edu_doc_address_country")), "eduInstCountry").comment("страна, в которой получен док. об образ.");
        column(dql, property(AddressCountry.digitalCode().fromAlias("edu_doc_address_country")), "eduInstCountryCode").comment("код страны, в которой получен док. об образ.");
        column(dql, property(EduLevel.title().fromAlias("edu_level")), "eduLevel").comment("уровень образования (по документу об образовании, актуальный)");
        column(dql, property(EduLevel.code().fromAlias("edu_level")), "eduLevelCode").comment("код уровня образования (по документу об образовании, актуальный)");
        column(dql, property(PersonEduDocument.documentEducationLevel().fromAlias("edu_doc")), "eduDocumentEduLevel").comment("уровень образования (по документу об образовании, как указан в документе)");

        column(dql, property(EduDocumentKind.title().fromAlias("edu_doc_kind")), "eduDocumentKind").comment("вид документа об образовании");
        column(dql, property(EduDocumentKind.code().fromAlias("edu_doc_kind")), "eduDocumentKindCode").comment("код вида документа об образовании");
        column(dql, property(GraduationHonour.title().fromAlias("grad_honour")), "graduationHonour").comment("степень отличия в документе об образовании");

        column( dql, DQL.parseExpression("(edu_doc.avgMarkAsLong/1000)"), "eduDocumentAverageMark" ).comment("средний балл по документу об образовании");
        column(dql, property(PersonEduDocument.issuanceDate().fromAlias("edu_doc")), "eduDocumentIssuanceDate").comment("дата выдачи документа об образовании");
        column(dql, property(PersonEduDocument.yearEnd().fromAlias("edu_doc")), "eduDocumentYearEnd").comment("год окончания по документу об образовании");


        column(dql, property(EnrOrder.id().fromAlias("enr_order")), "enrOrderId").comment("uid приказа о зачислении");
        column(dql, property(EnrOrder.number().fromAlias("enr_order")), "enrOrderNumber").comment("номер приказа о зачислении");
        column(dql, property(EnrOrder.commitDate().fromAlias("enr_order")), "enrOrderDate").comment("дата приказа о зачислении");
        column(dql, property(OrderStates.title().fromAlias("enr_order_state")), "enrOrderState").comment("состояние приказа о зачислении");
        column(dql, property(OrderStates.code().fromAlias("enr_order_state")), "enrOrderStateCode").comment("код состояния приказа о зачислении");

        return dql;
    }
}
