package ru.tandemservice.unienr14.report.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uni.entity.report.StorableReport;
import ru.tandemservice.unienr14.report.entity.EnrReportAdmissionPlans;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сводка о планах приема, средних баллах, результатах зачисления
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrReportAdmissionPlansGen extends StorableReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.report.entity.EnrReportAdmissionPlans";
    public static final String ENTITY_NAME = "enrReportAdmissionPlans";
    public static final int VERSION_HASH = 762304321;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_DATE_FROM = "dateFrom";
    public static final String P_DATE_TO = "dateTo";
    public static final String P_STAGE = "stage";
    public static final String P_REQUEST_TYPE = "requestType";
    public static final String P_PROGRAM_FORM = "programForm";
    public static final String P_COMPETITION_TYPE = "competitionType";
    public static final String P_ENR_ORG_UNIT = "enrOrgUnit";
    public static final String P_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String P_PROGRAM_SUBJECT = "programSubject";
    public static final String P_EDU_PROGRAM = "eduProgram";
    public static final String P_PROGRAM_SET = "programSet";
    public static final String P_FIRST_WAVE_DATE_FROM = "firstWaveDateFrom";
    public static final String P_FIRST_WAVE_DATE_TO = "firstWaveDateTo";
    public static final String P_SECOND_WAVE_DATE_FROM = "secondWaveDateFrom";
    public static final String P_SECOND_WAVE_DATE_TO = "secondWaveDateTo";
    public static final String P_THIRD_WAVE_DATE_FROM = "thirdWaveDateFrom";
    public static final String P_THIRD_WAVE_DATE_TO = "thirdWaveDateTo";
    public static final String P_PARALLEL = "parallel";

    private EnrEnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private Date _dateFrom;     // Заявления с
    private Date _dateTo;     // Заявления по
    private String _stage;     // Стадия приемной кампании
    private String _requestType;     // Вид заявления
    private String _programForm;     // Форма обучения
    private String _competitionType;     // Вид приема
    private String _enrOrgUnit;     // Филиал
    private String _formativeOrgUnit;     // Формирующее подр.
    private String _programSubject;     // Направление, спец., профессия
    private String _eduProgram;     // Образовательная программа
    private String _programSet;     // Набор образовательных программ
    private Date _firstWaveDateFrom;     // Первая волна зачисления с
    private Date _firstWaveDateTo;     // Первая волна зачисления по
    private Date _secondWaveDateFrom;     // Вторая волна зачисления с
    private Date _secondWaveDateTo;     // Вторая волна зачисления по
    private Date _thirdWaveDateFrom;     // Третья волна зачисления с
    private Date _thirdWaveDateTo;     // Третья волна зачисления по
    private String _parallel;     // Поступающие параллельно

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Заявления с. Свойство не может быть null.
     */
    @NotNull
    public Date getDateFrom()
    {
        return _dateFrom;
    }

    /**
     * @param dateFrom Заявления с. Свойство не может быть null.
     */
    public void setDateFrom(Date dateFrom)
    {
        dirty(_dateFrom, dateFrom);
        _dateFrom = dateFrom;
    }

    /**
     * @return Заявления по. Свойство не может быть null.
     */
    @NotNull
    public Date getDateTo()
    {
        return _dateTo;
    }

    /**
     * @param dateTo Заявления по. Свойство не может быть null.
     */
    public void setDateTo(Date dateTo)
    {
        dirty(_dateTo, dateTo);
        _dateTo = dateTo;
    }

    /**
     * @return Стадия приемной кампании. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getStage()
    {
        return _stage;
    }

    /**
     * @param stage Стадия приемной кампании. Свойство не может быть null.
     */
    public void setStage(String stage)
    {
        dirty(_stage, stage);
        _stage = stage;
    }

    /**
     * @return Вид заявления.
     */
    @Length(max=255)
    public String getRequestType()
    {
        return _requestType;
    }

    /**
     * @param requestType Вид заявления.
     */
    public void setRequestType(String requestType)
    {
        dirty(_requestType, requestType);
        _requestType = requestType;
    }

    /**
     * @return Форма обучения.
     */
    @Length(max=255)
    public String getProgramForm()
    {
        return _programForm;
    }

    /**
     * @param programForm Форма обучения.
     */
    public void setProgramForm(String programForm)
    {
        dirty(_programForm, programForm);
        _programForm = programForm;
    }

    /**
     * @return Вид приема.
     */
    @Length(max=255)
    public String getCompetitionType()
    {
        return _competitionType;
    }

    /**
     * @param competitionType Вид приема.
     */
    public void setCompetitionType(String competitionType)
    {
        dirty(_competitionType, competitionType);
        _competitionType = competitionType;
    }

    /**
     * @return Филиал.
     */
    public String getEnrOrgUnit()
    {
        return _enrOrgUnit;
    }

    /**
     * @param enrOrgUnit Филиал.
     */
    public void setEnrOrgUnit(String enrOrgUnit)
    {
        dirty(_enrOrgUnit, enrOrgUnit);
        _enrOrgUnit = enrOrgUnit;
    }

    /**
     * @return Формирующее подр..
     */
    public String getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    /**
     * @param formativeOrgUnit Формирующее подр..
     */
    public void setFormativeOrgUnit(String formativeOrgUnit)
    {
        dirty(_formativeOrgUnit, formativeOrgUnit);
        _formativeOrgUnit = formativeOrgUnit;
    }

    /**
     * @return Направление, спец., профессия.
     */
    public String getProgramSubject()
    {
        return _programSubject;
    }

    /**
     * @param programSubject Направление, спец., профессия.
     */
    public void setProgramSubject(String programSubject)
    {
        dirty(_programSubject, programSubject);
        _programSubject = programSubject;
    }

    /**
     * @return Образовательная программа.
     */
    public String getEduProgram()
    {
        return _eduProgram;
    }

    /**
     * @param eduProgram Образовательная программа.
     */
    public void setEduProgram(String eduProgram)
    {
        dirty(_eduProgram, eduProgram);
        _eduProgram = eduProgram;
    }

    /**
     * @return Набор образовательных программ.
     */
    public String getProgramSet()
    {
        return _programSet;
    }

    /**
     * @param programSet Набор образовательных программ.
     */
    public void setProgramSet(String programSet)
    {
        dirty(_programSet, programSet);
        _programSet = programSet;
    }

    /**
     * @return Первая волна зачисления с. Свойство не может быть null.
     */
    @NotNull
    public Date getFirstWaveDateFrom()
    {
        return _firstWaveDateFrom;
    }

    /**
     * @param firstWaveDateFrom Первая волна зачисления с. Свойство не может быть null.
     */
    public void setFirstWaveDateFrom(Date firstWaveDateFrom)
    {
        dirty(_firstWaveDateFrom, firstWaveDateFrom);
        _firstWaveDateFrom = firstWaveDateFrom;
    }

    /**
     * @return Первая волна зачисления по. Свойство не может быть null.
     */
    @NotNull
    public Date getFirstWaveDateTo()
    {
        return _firstWaveDateTo;
    }

    /**
     * @param firstWaveDateTo Первая волна зачисления по. Свойство не может быть null.
     */
    public void setFirstWaveDateTo(Date firstWaveDateTo)
    {
        dirty(_firstWaveDateTo, firstWaveDateTo);
        _firstWaveDateTo = firstWaveDateTo;
    }

    /**
     * @return Вторая волна зачисления с. Свойство не может быть null.
     */
    @NotNull
    public Date getSecondWaveDateFrom()
    {
        return _secondWaveDateFrom;
    }

    /**
     * @param secondWaveDateFrom Вторая волна зачисления с. Свойство не может быть null.
     */
    public void setSecondWaveDateFrom(Date secondWaveDateFrom)
    {
        dirty(_secondWaveDateFrom, secondWaveDateFrom);
        _secondWaveDateFrom = secondWaveDateFrom;
    }

    /**
     * @return Вторая волна зачисления по. Свойство не может быть null.
     */
    @NotNull
    public Date getSecondWaveDateTo()
    {
        return _secondWaveDateTo;
    }

    /**
     * @param secondWaveDateTo Вторая волна зачисления по. Свойство не может быть null.
     */
    public void setSecondWaveDateTo(Date secondWaveDateTo)
    {
        dirty(_secondWaveDateTo, secondWaveDateTo);
        _secondWaveDateTo = secondWaveDateTo;
    }

    /**
     * @return Третья волна зачисления с. Свойство не может быть null.
     */
    @NotNull
    public Date getThirdWaveDateFrom()
    {
        return _thirdWaveDateFrom;
    }

    /**
     * @param thirdWaveDateFrom Третья волна зачисления с. Свойство не может быть null.
     */
    public void setThirdWaveDateFrom(Date thirdWaveDateFrom)
    {
        dirty(_thirdWaveDateFrom, thirdWaveDateFrom);
        _thirdWaveDateFrom = thirdWaveDateFrom;
    }

    /**
     * @return Третья волна зачисления по. Свойство не может быть null.
     */
    @NotNull
    public Date getThirdWaveDateTo()
    {
        return _thirdWaveDateTo;
    }

    /**
     * @param thirdWaveDateTo Третья волна зачисления по. Свойство не может быть null.
     */
    public void setThirdWaveDateTo(Date thirdWaveDateTo)
    {
        dirty(_thirdWaveDateTo, thirdWaveDateTo);
        _thirdWaveDateTo = thirdWaveDateTo;
    }

    /**
     * @return Поступающие параллельно.
     */
    public String getParallel()
    {
        return _parallel;
    }

    /**
     * @param parallel Поступающие параллельно.
     */
    public void setParallel(String parallel)
    {
        dirty(_parallel, parallel);
        _parallel = parallel;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EnrReportAdmissionPlansGen)
        {
            setEnrollmentCampaign(((EnrReportAdmissionPlans)another).getEnrollmentCampaign());
            setDateFrom(((EnrReportAdmissionPlans)another).getDateFrom());
            setDateTo(((EnrReportAdmissionPlans)another).getDateTo());
            setStage(((EnrReportAdmissionPlans)another).getStage());
            setRequestType(((EnrReportAdmissionPlans)another).getRequestType());
            setProgramForm(((EnrReportAdmissionPlans)another).getProgramForm());
            setCompetitionType(((EnrReportAdmissionPlans)another).getCompetitionType());
            setEnrOrgUnit(((EnrReportAdmissionPlans)another).getEnrOrgUnit());
            setFormativeOrgUnit(((EnrReportAdmissionPlans)another).getFormativeOrgUnit());
            setProgramSubject(((EnrReportAdmissionPlans)another).getProgramSubject());
            setEduProgram(((EnrReportAdmissionPlans)another).getEduProgram());
            setProgramSet(((EnrReportAdmissionPlans)another).getProgramSet());
            setFirstWaveDateFrom(((EnrReportAdmissionPlans)another).getFirstWaveDateFrom());
            setFirstWaveDateTo(((EnrReportAdmissionPlans)another).getFirstWaveDateTo());
            setSecondWaveDateFrom(((EnrReportAdmissionPlans)another).getSecondWaveDateFrom());
            setSecondWaveDateTo(((EnrReportAdmissionPlans)another).getSecondWaveDateTo());
            setThirdWaveDateFrom(((EnrReportAdmissionPlans)another).getThirdWaveDateFrom());
            setThirdWaveDateTo(((EnrReportAdmissionPlans)another).getThirdWaveDateTo());
            setParallel(((EnrReportAdmissionPlans)another).getParallel());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrReportAdmissionPlansGen> extends StorableReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrReportAdmissionPlans.class;
        }

        public T newInstance()
        {
            return (T) new EnrReportAdmissionPlans();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "dateFrom":
                    return obj.getDateFrom();
                case "dateTo":
                    return obj.getDateTo();
                case "stage":
                    return obj.getStage();
                case "requestType":
                    return obj.getRequestType();
                case "programForm":
                    return obj.getProgramForm();
                case "competitionType":
                    return obj.getCompetitionType();
                case "enrOrgUnit":
                    return obj.getEnrOrgUnit();
                case "formativeOrgUnit":
                    return obj.getFormativeOrgUnit();
                case "programSubject":
                    return obj.getProgramSubject();
                case "eduProgram":
                    return obj.getEduProgram();
                case "programSet":
                    return obj.getProgramSet();
                case "firstWaveDateFrom":
                    return obj.getFirstWaveDateFrom();
                case "firstWaveDateTo":
                    return obj.getFirstWaveDateTo();
                case "secondWaveDateFrom":
                    return obj.getSecondWaveDateFrom();
                case "secondWaveDateTo":
                    return obj.getSecondWaveDateTo();
                case "thirdWaveDateFrom":
                    return obj.getThirdWaveDateFrom();
                case "thirdWaveDateTo":
                    return obj.getThirdWaveDateTo();
                case "parallel":
                    return obj.getParallel();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrEnrollmentCampaign) value);
                    return;
                case "dateFrom":
                    obj.setDateFrom((Date) value);
                    return;
                case "dateTo":
                    obj.setDateTo((Date) value);
                    return;
                case "stage":
                    obj.setStage((String) value);
                    return;
                case "requestType":
                    obj.setRequestType((String) value);
                    return;
                case "programForm":
                    obj.setProgramForm((String) value);
                    return;
                case "competitionType":
                    obj.setCompetitionType((String) value);
                    return;
                case "enrOrgUnit":
                    obj.setEnrOrgUnit((String) value);
                    return;
                case "formativeOrgUnit":
                    obj.setFormativeOrgUnit((String) value);
                    return;
                case "programSubject":
                    obj.setProgramSubject((String) value);
                    return;
                case "eduProgram":
                    obj.setEduProgram((String) value);
                    return;
                case "programSet":
                    obj.setProgramSet((String) value);
                    return;
                case "firstWaveDateFrom":
                    obj.setFirstWaveDateFrom((Date) value);
                    return;
                case "firstWaveDateTo":
                    obj.setFirstWaveDateTo((Date) value);
                    return;
                case "secondWaveDateFrom":
                    obj.setSecondWaveDateFrom((Date) value);
                    return;
                case "secondWaveDateTo":
                    obj.setSecondWaveDateTo((Date) value);
                    return;
                case "thirdWaveDateFrom":
                    obj.setThirdWaveDateFrom((Date) value);
                    return;
                case "thirdWaveDateTo":
                    obj.setThirdWaveDateTo((Date) value);
                    return;
                case "parallel":
                    obj.setParallel((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                        return true;
                case "dateFrom":
                        return true;
                case "dateTo":
                        return true;
                case "stage":
                        return true;
                case "requestType":
                        return true;
                case "programForm":
                        return true;
                case "competitionType":
                        return true;
                case "enrOrgUnit":
                        return true;
                case "formativeOrgUnit":
                        return true;
                case "programSubject":
                        return true;
                case "eduProgram":
                        return true;
                case "programSet":
                        return true;
                case "firstWaveDateFrom":
                        return true;
                case "firstWaveDateTo":
                        return true;
                case "secondWaveDateFrom":
                        return true;
                case "secondWaveDateTo":
                        return true;
                case "thirdWaveDateFrom":
                        return true;
                case "thirdWaveDateTo":
                        return true;
                case "parallel":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return true;
                case "dateFrom":
                    return true;
                case "dateTo":
                    return true;
                case "stage":
                    return true;
                case "requestType":
                    return true;
                case "programForm":
                    return true;
                case "competitionType":
                    return true;
                case "enrOrgUnit":
                    return true;
                case "formativeOrgUnit":
                    return true;
                case "programSubject":
                    return true;
                case "eduProgram":
                    return true;
                case "programSet":
                    return true;
                case "firstWaveDateFrom":
                    return true;
                case "firstWaveDateTo":
                    return true;
                case "secondWaveDateFrom":
                    return true;
                case "secondWaveDateTo":
                    return true;
                case "thirdWaveDateFrom":
                    return true;
                case "thirdWaveDateTo":
                    return true;
                case "parallel":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return EnrEnrollmentCampaign.class;
                case "dateFrom":
                    return Date.class;
                case "dateTo":
                    return Date.class;
                case "stage":
                    return String.class;
                case "requestType":
                    return String.class;
                case "programForm":
                    return String.class;
                case "competitionType":
                    return String.class;
                case "enrOrgUnit":
                    return String.class;
                case "formativeOrgUnit":
                    return String.class;
                case "programSubject":
                    return String.class;
                case "eduProgram":
                    return String.class;
                case "programSet":
                    return String.class;
                case "firstWaveDateFrom":
                    return Date.class;
                case "firstWaveDateTo":
                    return Date.class;
                case "secondWaveDateFrom":
                    return Date.class;
                case "secondWaveDateTo":
                    return Date.class;
                case "thirdWaveDateFrom":
                    return Date.class;
                case "thirdWaveDateTo":
                    return Date.class;
                case "parallel":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrReportAdmissionPlans> _dslPath = new Path<EnrReportAdmissionPlans>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrReportAdmissionPlans");
    }
            

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionPlans#getEnrollmentCampaign()
     */
    public static EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Заявления с. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionPlans#getDateFrom()
     */
    public static PropertyPath<Date> dateFrom()
    {
        return _dslPath.dateFrom();
    }

    /**
     * @return Заявления по. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionPlans#getDateTo()
     */
    public static PropertyPath<Date> dateTo()
    {
        return _dslPath.dateTo();
    }

    /**
     * @return Стадия приемной кампании. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionPlans#getStage()
     */
    public static PropertyPath<String> stage()
    {
        return _dslPath.stage();
    }

    /**
     * @return Вид заявления.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionPlans#getRequestType()
     */
    public static PropertyPath<String> requestType()
    {
        return _dslPath.requestType();
    }

    /**
     * @return Форма обучения.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionPlans#getProgramForm()
     */
    public static PropertyPath<String> programForm()
    {
        return _dslPath.programForm();
    }

    /**
     * @return Вид приема.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionPlans#getCompetitionType()
     */
    public static PropertyPath<String> competitionType()
    {
        return _dslPath.competitionType();
    }

    /**
     * @return Филиал.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionPlans#getEnrOrgUnit()
     */
    public static PropertyPath<String> enrOrgUnit()
    {
        return _dslPath.enrOrgUnit();
    }

    /**
     * @return Формирующее подр..
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionPlans#getFormativeOrgUnit()
     */
    public static PropertyPath<String> formativeOrgUnit()
    {
        return _dslPath.formativeOrgUnit();
    }

    /**
     * @return Направление, спец., профессия.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionPlans#getProgramSubject()
     */
    public static PropertyPath<String> programSubject()
    {
        return _dslPath.programSubject();
    }

    /**
     * @return Образовательная программа.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionPlans#getEduProgram()
     */
    public static PropertyPath<String> eduProgram()
    {
        return _dslPath.eduProgram();
    }

    /**
     * @return Набор образовательных программ.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionPlans#getProgramSet()
     */
    public static PropertyPath<String> programSet()
    {
        return _dslPath.programSet();
    }

    /**
     * @return Первая волна зачисления с. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionPlans#getFirstWaveDateFrom()
     */
    public static PropertyPath<Date> firstWaveDateFrom()
    {
        return _dslPath.firstWaveDateFrom();
    }

    /**
     * @return Первая волна зачисления по. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionPlans#getFirstWaveDateTo()
     */
    public static PropertyPath<Date> firstWaveDateTo()
    {
        return _dslPath.firstWaveDateTo();
    }

    /**
     * @return Вторая волна зачисления с. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionPlans#getSecondWaveDateFrom()
     */
    public static PropertyPath<Date> secondWaveDateFrom()
    {
        return _dslPath.secondWaveDateFrom();
    }

    /**
     * @return Вторая волна зачисления по. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionPlans#getSecondWaveDateTo()
     */
    public static PropertyPath<Date> secondWaveDateTo()
    {
        return _dslPath.secondWaveDateTo();
    }

    /**
     * @return Третья волна зачисления с. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionPlans#getThirdWaveDateFrom()
     */
    public static PropertyPath<Date> thirdWaveDateFrom()
    {
        return _dslPath.thirdWaveDateFrom();
    }

    /**
     * @return Третья волна зачисления по. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionPlans#getThirdWaveDateTo()
     */
    public static PropertyPath<Date> thirdWaveDateTo()
    {
        return _dslPath.thirdWaveDateTo();
    }

    /**
     * @return Поступающие параллельно.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionPlans#getParallel()
     */
    public static PropertyPath<String> parallel()
    {
        return _dslPath.parallel();
    }

    public static class Path<E extends EnrReportAdmissionPlans> extends StorableReport.Path<E>
    {
        private EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<Date> _dateFrom;
        private PropertyPath<Date> _dateTo;
        private PropertyPath<String> _stage;
        private PropertyPath<String> _requestType;
        private PropertyPath<String> _programForm;
        private PropertyPath<String> _competitionType;
        private PropertyPath<String> _enrOrgUnit;
        private PropertyPath<String> _formativeOrgUnit;
        private PropertyPath<String> _programSubject;
        private PropertyPath<String> _eduProgram;
        private PropertyPath<String> _programSet;
        private PropertyPath<Date> _firstWaveDateFrom;
        private PropertyPath<Date> _firstWaveDateTo;
        private PropertyPath<Date> _secondWaveDateFrom;
        private PropertyPath<Date> _secondWaveDateTo;
        private PropertyPath<Date> _thirdWaveDateFrom;
        private PropertyPath<Date> _thirdWaveDateTo;
        private PropertyPath<String> _parallel;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionPlans#getEnrollmentCampaign()
     */
        public EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Заявления с. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionPlans#getDateFrom()
     */
        public PropertyPath<Date> dateFrom()
        {
            if(_dateFrom == null )
                _dateFrom = new PropertyPath<Date>(EnrReportAdmissionPlansGen.P_DATE_FROM, this);
            return _dateFrom;
        }

    /**
     * @return Заявления по. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionPlans#getDateTo()
     */
        public PropertyPath<Date> dateTo()
        {
            if(_dateTo == null )
                _dateTo = new PropertyPath<Date>(EnrReportAdmissionPlansGen.P_DATE_TO, this);
            return _dateTo;
        }

    /**
     * @return Стадия приемной кампании. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionPlans#getStage()
     */
        public PropertyPath<String> stage()
        {
            if(_stage == null )
                _stage = new PropertyPath<String>(EnrReportAdmissionPlansGen.P_STAGE, this);
            return _stage;
        }

    /**
     * @return Вид заявления.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionPlans#getRequestType()
     */
        public PropertyPath<String> requestType()
        {
            if(_requestType == null )
                _requestType = new PropertyPath<String>(EnrReportAdmissionPlansGen.P_REQUEST_TYPE, this);
            return _requestType;
        }

    /**
     * @return Форма обучения.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionPlans#getProgramForm()
     */
        public PropertyPath<String> programForm()
        {
            if(_programForm == null )
                _programForm = new PropertyPath<String>(EnrReportAdmissionPlansGen.P_PROGRAM_FORM, this);
            return _programForm;
        }

    /**
     * @return Вид приема.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionPlans#getCompetitionType()
     */
        public PropertyPath<String> competitionType()
        {
            if(_competitionType == null )
                _competitionType = new PropertyPath<String>(EnrReportAdmissionPlansGen.P_COMPETITION_TYPE, this);
            return _competitionType;
        }

    /**
     * @return Филиал.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionPlans#getEnrOrgUnit()
     */
        public PropertyPath<String> enrOrgUnit()
        {
            if(_enrOrgUnit == null )
                _enrOrgUnit = new PropertyPath<String>(EnrReportAdmissionPlansGen.P_ENR_ORG_UNIT, this);
            return _enrOrgUnit;
        }

    /**
     * @return Формирующее подр..
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionPlans#getFormativeOrgUnit()
     */
        public PropertyPath<String> formativeOrgUnit()
        {
            if(_formativeOrgUnit == null )
                _formativeOrgUnit = new PropertyPath<String>(EnrReportAdmissionPlansGen.P_FORMATIVE_ORG_UNIT, this);
            return _formativeOrgUnit;
        }

    /**
     * @return Направление, спец., профессия.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionPlans#getProgramSubject()
     */
        public PropertyPath<String> programSubject()
        {
            if(_programSubject == null )
                _programSubject = new PropertyPath<String>(EnrReportAdmissionPlansGen.P_PROGRAM_SUBJECT, this);
            return _programSubject;
        }

    /**
     * @return Образовательная программа.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionPlans#getEduProgram()
     */
        public PropertyPath<String> eduProgram()
        {
            if(_eduProgram == null )
                _eduProgram = new PropertyPath<String>(EnrReportAdmissionPlansGen.P_EDU_PROGRAM, this);
            return _eduProgram;
        }

    /**
     * @return Набор образовательных программ.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionPlans#getProgramSet()
     */
        public PropertyPath<String> programSet()
        {
            if(_programSet == null )
                _programSet = new PropertyPath<String>(EnrReportAdmissionPlansGen.P_PROGRAM_SET, this);
            return _programSet;
        }

    /**
     * @return Первая волна зачисления с. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionPlans#getFirstWaveDateFrom()
     */
        public PropertyPath<Date> firstWaveDateFrom()
        {
            if(_firstWaveDateFrom == null )
                _firstWaveDateFrom = new PropertyPath<Date>(EnrReportAdmissionPlansGen.P_FIRST_WAVE_DATE_FROM, this);
            return _firstWaveDateFrom;
        }

    /**
     * @return Первая волна зачисления по. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionPlans#getFirstWaveDateTo()
     */
        public PropertyPath<Date> firstWaveDateTo()
        {
            if(_firstWaveDateTo == null )
                _firstWaveDateTo = new PropertyPath<Date>(EnrReportAdmissionPlansGen.P_FIRST_WAVE_DATE_TO, this);
            return _firstWaveDateTo;
        }

    /**
     * @return Вторая волна зачисления с. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionPlans#getSecondWaveDateFrom()
     */
        public PropertyPath<Date> secondWaveDateFrom()
        {
            if(_secondWaveDateFrom == null )
                _secondWaveDateFrom = new PropertyPath<Date>(EnrReportAdmissionPlansGen.P_SECOND_WAVE_DATE_FROM, this);
            return _secondWaveDateFrom;
        }

    /**
     * @return Вторая волна зачисления по. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionPlans#getSecondWaveDateTo()
     */
        public PropertyPath<Date> secondWaveDateTo()
        {
            if(_secondWaveDateTo == null )
                _secondWaveDateTo = new PropertyPath<Date>(EnrReportAdmissionPlansGen.P_SECOND_WAVE_DATE_TO, this);
            return _secondWaveDateTo;
        }

    /**
     * @return Третья волна зачисления с. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionPlans#getThirdWaveDateFrom()
     */
        public PropertyPath<Date> thirdWaveDateFrom()
        {
            if(_thirdWaveDateFrom == null )
                _thirdWaveDateFrom = new PropertyPath<Date>(EnrReportAdmissionPlansGen.P_THIRD_WAVE_DATE_FROM, this);
            return _thirdWaveDateFrom;
        }

    /**
     * @return Третья волна зачисления по. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionPlans#getThirdWaveDateTo()
     */
        public PropertyPath<Date> thirdWaveDateTo()
        {
            if(_thirdWaveDateTo == null )
                _thirdWaveDateTo = new PropertyPath<Date>(EnrReportAdmissionPlansGen.P_THIRD_WAVE_DATE_TO, this);
            return _thirdWaveDateTo;
        }

    /**
     * @return Поступающие параллельно.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportAdmissionPlans#getParallel()
     */
        public PropertyPath<String> parallel()
        {
            if(_parallel == null )
                _parallel = new PropertyPath<String>(EnrReportAdmissionPlansGen.P_PARALLEL, this);
            return _parallel;
        }

        public Class getEntityClass()
        {
            return EnrReportAdmissionPlans.class;
        }

        public String getEntityName()
        {
            return "enrReportAdmissionPlans";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
