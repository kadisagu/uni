/* $Id:$ */
package ru.tandemservice.unienr14.order.bo.EnrEnrollmentMinisteryParagraph.logic;

import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantCustomState;
import ru.tandemservice.unienr14.order.bo.EnrEnrollmentMinisteryParagraph.ui.AddEdit.EnrEnrollmentMinisteryParagraphAddEdit;
import ru.tandemservice.unienr14.request.entity.EnrEntrantForeignRequest;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 7/7/14
 */
public class EnrParagraphEntrantSelectionDSHandler extends DefaultSearchDataSourceHandler
{
    public static final String ENROLL_EXTRACT = "enrollExtract";
    public static final String CUSTOM_STATES = "customStates";

    public EnrParagraphEntrantSelectionDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final EnrCompetitionType competitionType = context.get(EnrEnrollmentMinisteryParagraphAddEdit.BIND_COMPETITION_TYPE);

        final DQLOrderDescriptionRegistry orderDescriptionRegistry = new DQLOrderDescriptionRegistry(EnrEntrantForeignRequest.class, "r");
        final DQLSelectBuilder dql = orderDescriptionRegistry.buildDQLSelectBuilder().column(property("r"))
                .joinPath(DQLJoinType.inner, EnrEntrantForeignRequest.competition().fromAlias("r"), "c")                
                .where(eq(property("c", EnrCompetition.programSetOrgUnit().programSet().enrollmentCampaign()), commonValue(context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN))))
                .where(eq(property("c", EnrCompetition.requestType()), commonValue(context.get(EnrEnrollmentMinisteryParagraphAddEdit.BIND_REQUEST_TYPE))))
                .where(eq(property("c", EnrCompetition.type().compensationType()), commonValue(context.get(EnrEnrollmentMinisteryParagraphAddEdit.BIND_COMPENSATION_TYPE))))
                .where(eq(property("c", EnrCompetition.programSetOrgUnit().programSet().programForm()), commonValue(context.get(EnrEnrollmentMinisteryParagraphAddEdit.BIND_FORM))))
                .where(eq(property("c", EnrCompetition.programSetOrgUnit().orgUnit()), commonValue(context.get(EnrEnrollmentMinisteryParagraphAddEdit.BIND_ORG_UNIT))))
                .where(eq(property("c", EnrCompetition.programSetOrgUnit().programSet().programSubject()), commonValue(context.get(EnrEnrollmentMinisteryParagraphAddEdit.BIND_SUBJECT))))
                .where(eq(property("c", EnrCompetition.type()), value(competitionType)))
                ;

        OrgUnit formativeOrgUnit = context.get(EnrEnrollmentMinisteryParagraphAddEdit.BIND_FORMATIVE_ORG_UNIT);
        EnrProgramSetBase programSet = context.get(EnrEnrollmentMinisteryParagraphAddEdit.BIND_PROGRAM_SET);
        EnrCompetition competition = context.get(EnrEnrollmentMinisteryParagraphAddEdit.BIND_COMPETITION);

        if (formativeOrgUnit != null) {
            dql.where(eq(property("c", EnrCompetition.programSetOrgUnit().formativeOrgUnit()), value(formativeOrgUnit)));
        }
        if (programSet != null) {
            dql.where(eq(property("c", EnrCompetition.programSetOrgUnit().programSet()), value(programSet)));
        }
        if (competition != null) {
            dql.where(eq(property("r", EnrEntrantForeignRequest.competition()), value(competition)));
        }

        orderDescriptionRegistry.applyOrder(dql, input.getEntityOrder());

        List<EnrEntrantForeignRequest> requests = dql.createStatement(context.getSession()).list();
        Collection<EnrEntrant> entrants = CollectionUtils.collect(requests, EnrEntrantForeignRequest::getEntrant);

        Map<EnrEntrant, List<EnrEntrantCustomState>> customStatesMap = EnrEntrantManager.instance().dao().getActiveCustomStatesMap(entrants, new Date());

        List<DataWrapper> result = new ArrayList<>();
        for (EnrEntrantForeignRequest request : requests)
        {
            DataWrapper record = new DataWrapper(request);
            record.setProperty(CUSTOM_STATES, customStatesMap.get(request.getEntrant()));
            result.add(record);
        }

        DSOutput output = ListOutputBuilder.get(input, result).pageable(false).build().ordering(new EntityComparator(
                new EntityOrder(EnrEntrantForeignRequest.entrant().person().fullFio(), OrderDirection.asc),
                new EntityOrder(EnrEntrantForeignRequest.id(), OrderDirection.asc)));

        output.setCountRecord(Math.max(1, output.getRecordList().size()));

        return output;
    }


}
