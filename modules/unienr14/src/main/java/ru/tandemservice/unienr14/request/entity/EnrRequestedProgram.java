package ru.tandemservice.unienr14.request.entity;

import org.tandemframework.core.entity.dsl.MetaDSLPath;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;
import ru.tandemservice.unienr14.request.entity.gen.*;

/**
 * Выбранная ОП в рамках конкурса
 */
public class EnrRequestedProgram extends EnrRequestedProgramGen implements IEnrEntrantPrioritized
{
    public EnrRequestedProgram()
    {
    }

    public EnrRequestedProgram(EnrRequestedCompetition requestedCompetition, EnrProgramSetItem programSetItem) {
        setRequestedCompetition(requestedCompetition);
        setProgramSetItem(programSetItem);
    }

    // ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubProfileTab.IEnrEntrantPrioritized

    @Override public String getPriorityRowTitle() { return "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + getProgramSetItem().getProgram().getTitleWithCodeAndConditionsShortWithForm(); }
    @Override public MetaDSLPath getPriorityOwnerPath() { return requestedCompetition(); }
    @Override public MetaDSLPath getRequestTypePath() { return requestedCompetition().request().type(); }
    @Override public int getCompetitionPriority() { return getRequestedCompetition().getPriority(); }
    @Override public Integer getProgramPriority() { return getPriority(); }
}