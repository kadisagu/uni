package ru.tandemservice.unienr14.order.entity;

import java.util.List;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;

import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unienr14.order.entity.gen.EnrAbstractOrderGen;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.OrderStates;

/**
 * Абстрактный приказ на абитуриентов (2014)
 */
public abstract class EnrAbstractOrder extends EnrAbstractOrderGen implements IAbstractOrder
{
    @Override
    @EntityDSLSupport
    public boolean isReadonly() {
        return !UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(getState().getCode());
    }

    @Override
    public void setState(ICatalogItem state) {
        setState((OrderStates) state);
    }

    @Override
    public int getParagraphCount()
    {
        return UniDaoFacade.getCoreDao().getCount(EnrAbstractParagraph.class, IAbstractParagraph.L_ORDER, this);
    }

    @Override
    @SuppressWarnings({"unchecked"})
    public List<? extends IAbstractParagraph> getParagraphList()
    {
        return UniDaoFacade.getCoreDao().getList(EnrAbstractParagraph.class, IAbstractParagraph.L_ORDER, this, IAbstractParagraph.P_NUMBER);
    }
}