/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.core.view.formatter.LongAsDoubleFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.INumberGenerationRule;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.catalog.bo.EnrCatalogCommons.EnrCatalogCommonsManager;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;
import ru.tandemservice.unienr14.entrant.EnrNumberGenerationRule;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.logic.EnrEntrantDao;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.logic.IEnrEntrantDao;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitProofDocument;
import ru.tandemservice.unienr14.request.entity.gen.IEnrEntrantBenefitProofDocumentGen;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignEntrantDocument;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 4/9/13
 */
@Configuration
public class EnrEntrantManager extends BusinessObjectManager
{
    public static final IFormatter<Long> DEFAULT_MARK_FORMATTER = LongAsDoubleFormatter.LONG_AS_DOUBLE_FORMATTER_3_SCALE;

    public static final String BIND_REQUESTED_COMPETITION = "requestedCompetition";
    public static final String BIND_ENTRANT = "entrant";
    public static final String BIND_ENTRANT_ID = "entrantId";

    public static EnrEntrantManager instance()
    {
        return instance(EnrEntrantManager.class);
    }

    @Bean
    public IEnrEntrantDao dao()
    {
        return new EnrEntrantDao();
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> benefitProofDocumentDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), IEnrEntrantBenefitProofDocument.class) {
            @Override protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context) {
                super.applyWhereConditions(alias, dql, context);

                EnrEntrant entrant = context.get(BIND_ENTRANT);
                if (null == entrant) { return; }

                String benefitTypeCode = context.get(EnrCatalogCommonsManager.PARAM_BENEFIT_TYPE_CODE);
                EnrEnrollmentCampaign enrollmentCampaign = entrant.getEnrollmentCampaign();

                dql.where(exists(
                    new DQLSelectBuilder()
                    .fromEntity(EnrCampaignEntrantDocument.class, "rel")
                    .where(eq(property(EnrCampaignEntrantDocument.documentType().fromAlias("rel")), property(IEnrEntrantBenefitProofDocumentGen.documentType().fromAlias(alias))))
                    .where(eq(property(EnrCampaignEntrantDocument.enrollmentCampaign().fromAlias("rel")), value(enrollmentCampaign)))
                    .where(eq(property("rel", EnrCampaignEntrantDocument.getBenefitProperty(benefitTypeCode)), value(Boolean.TRUE)))
                    .buildQuery()
                ));
            }
        }
        .where(IEnrEntrantBenefitProofDocumentGen.entrant(), BIND_ENTRANT)
        .order(IEnrEntrantBenefitProofDocumentGen.seria())
        .order(IEnrEntrantBenefitProofDocumentGen.number())
        .filter(IEnrEntrantBenefitProofDocumentGen.seria())
        .filter(IEnrEntrantBenefitProofDocumentGen.number())
        .pageable(true);
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> noExamsProgramSetItemDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrProgramSetItem.class) {
            @Override protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context) {
                super.applyWhereConditions(alias, dql, context);

                EnrRequestedCompetition requestedCompetition = context.get(BIND_REQUESTED_COMPETITION);
                if (null == requestedCompetition)
                    dql.where(isNull(alias + ".id"));
                else
                    dql.where(eq(property(EnrProgramSetItem.programSet().fromAlias(alias)), value(requestedCompetition.getCompetition().getProgramSetOrgUnit().getProgramSet())));
            }
        }
        .order(EnrProgramSetItem.program().title())
        .filter(EnrProgramSetItem.program().title())
        .pageable(true);
    }

    @Bean
    public INumberGenerationRule<EnrEntrant> entrantNumberGenerationRule() {
        return new EnrNumberGenerationRule<EnrEntrant>(EnrEntrant.class, EnrEntrant.personalNumber(), EnrNumberGenerationRule.toString(EnrEntrant.enrollmentCampaign().educationYear().intValue())) {
            @Override public String buildCandidate(EnrEntrant entrant, int currentQueueValue) {
                int year = entrant.getEnrollmentCampaign().getEducationYear().getIntValue();
                return String.format("%02d%05d", (year % 100), currentQueueValue);
            }
        };
    }

    public static final Long OPTION_ENTRANT = 1L;
    public static final Long OPTION_ONLINE_ENTRANT = 2L;

    @Bean
    public ItemListExtPoint<DataWrapper> addEntrantWizardFirstStepOptionsExtPoint()
    {
        return itemList(DataWrapper.class).
        add(OPTION_ENTRANT.toString(), new DataWrapper(OPTION_ENTRANT, "Ввод данных с «нуля»")).
        add(OPTION_ONLINE_ENTRANT.toString(), new DataWrapper(OPTION_ONLINE_ENTRANT, "Ввод с использованием данных из онлайн-регистрации")).
        create();
    }
}
