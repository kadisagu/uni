/**
 *$Id: EnrTargetAdmissionList.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.settings.bo.EnrTargetAdmission.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import ru.tandemservice.unienr14.catalog.entity.EnrTargetAdmissionKind;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrTargetAdmission.logic.EnrTargetAdmissionKindSearchDSHandler;
import ru.tandemservice.unienr14.settings.bo.EnrTargetAdmission.util.TargetAdmissionKindDataWrapper;

/**
 * @author Alexander Shaburov
 * @since 15.04.13
 */
@Configuration
public class EnrTargetAdmissionList extends BusinessComponentManager
{
    public static final String ENR_CAMP_SELECT_DS = EnrEnrollmentCampaignManager.DS_ENR_CAMPAIGN;
    public static final String TARGET_ADMISSION_SEARCH_DS = "targetAdmissionKindSearchDS";

    public static final Long EXTERNAL_ORG_USE_ID = 1L;
    public static final Long EXTERNAL_ORG_NOT_USE_ID = 2L;

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
                .addDataSource(searchListDS(TARGET_ADMISSION_SEARCH_DS, targetAdmissionKindSearchDSColumns(), targetAdmissionKindSearchDSHandler()))
                .addDataSource(selectDS("extOrgUnitDS", extOrgUnitDSHandler()).addColumn(ExternalOrgUnit.titleWithLegalForm().s()))
                .create();
    }

    @Bean
    public ColumnListExtPoint targetAdmissionKindSearchDSColumns()
    {
        return columnListExtPointBuilder(TARGET_ADMISSION_SEARCH_DS)
                .addColumn(textColumn("title", EnrTargetAdmissionKind.title()).width("25%"))
                .addColumn(toggleColumn("use", TargetAdmissionKindDataWrapper.USE_IN_ENR_CAMPAIGN)
                        .toggleOnListener("onToogleTargetAdmissionUse")
                        .toggleOffListener("onToogleTargetAdmissionNotUse").disabled("ui:editMode"))
                .addColumn(toggleColumn("stateInterest", TargetAdmissionKindDataWrapper.STATE_INTEREST)
                        .toggleOnListener("onToogleTargetAdmissionStateInterestTrue")
                        .toggleOffListener("onToogleTargetAdmissionStateInterestFalse").disabled("ui:stateInterestDisabled"))
                .addColumn(blockColumn("extOrgUnit"))
                .addColumn(blockColumn("actions").width("1px"))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> targetAdmissionKindSearchDSHandler()
    {
        return new EnrTargetAdmissionKindSearchDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler extOrgUnitDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), ExternalOrgUnit.class)
                .filter(ExternalOrgUnit.title())
                .filter(ExternalOrgUnit.legalForm().title())
                .order(ExternalOrgUnit.title())
                .order(ExternalOrgUnit.legalForm().title());
    }
}
