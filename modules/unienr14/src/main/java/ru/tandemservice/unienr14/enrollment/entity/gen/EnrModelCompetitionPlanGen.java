package ru.tandemservice.unienr14.enrollment.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.enrollment.entity.EnrModelCompetitionPlan;
import ru.tandemservice.unienr14.enrollment.entity.EnrModelStep;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Число мест для зачисления по конкурсу в модели
 *
 * Число мест для зачисления по конкурсу в рамках шага.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrModelCompetitionPlanGen extends EntityBase
 implements INaturalIdentifiable<EnrModelCompetitionPlanGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.enrollment.entity.EnrModelCompetitionPlan";
    public static final String ENTITY_NAME = "enrModelCompetitionPlan";
    public static final int VERSION_HASH = 2097238668;
    private static IEntityMeta ENTITY_META;

    public static final String L_STEP = "step";
    public static final String L_COMPETITION = "competition";
    public static final String P_BASE_PLAN = "basePlan";
    public static final String P_PLAN = "plan";

    private EnrModelStep _step;     // Шаг
    private EnrCompetition _competition;     // Конкурс (комбинация условий поступления) для приема
    private int _basePlan;     // Число мест до применения процента
    private int _plan;     // Число мест

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Шаг. Свойство не может быть null.
     */
    @NotNull
    public EnrModelStep getStep()
    {
        return _step;
    }

    /**
     * @param step Шаг. Свойство не может быть null.
     */
    public void setStep(EnrModelStep step)
    {
        dirty(_step, step);
        _step = step;
    }

    /**
     * @return Конкурс (комбинация условий поступления) для приема. Свойство не может быть null.
     */
    @NotNull
    public EnrCompetition getCompetition()
    {
        return _competition;
    }

    /**
     * @param competition Конкурс (комбинация условий поступления) для приема. Свойство не может быть null.
     */
    public void setCompetition(EnrCompetition competition)
    {
        dirty(_competition, competition);
        _competition = competition;
    }

    /**
     * @return Число мест до применения процента. Свойство не может быть null.
     */
    @NotNull
    public int getBasePlan()
    {
        return _basePlan;
    }

    /**
     * @param basePlan Число мест до применения процента. Свойство не может быть null.
     */
    public void setBasePlan(int basePlan)
    {
        dirty(_basePlan, basePlan);
        _basePlan = basePlan;
    }

    /**
     * @return Число мест. Свойство не может быть null.
     */
    @NotNull
    public int getPlan()
    {
        return _plan;
    }

    /**
     * @param plan Число мест. Свойство не может быть null.
     */
    public void setPlan(int plan)
    {
        dirty(_plan, plan);
        _plan = plan;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrModelCompetitionPlanGen)
        {
            if (withNaturalIdProperties)
            {
                setStep(((EnrModelCompetitionPlan)another).getStep());
                setCompetition(((EnrModelCompetitionPlan)another).getCompetition());
            }
            setBasePlan(((EnrModelCompetitionPlan)another).getBasePlan());
            setPlan(((EnrModelCompetitionPlan)another).getPlan());
        }
    }

    public INaturalId<EnrModelCompetitionPlanGen> getNaturalId()
    {
        return new NaturalId(getStep(), getCompetition());
    }

    public static class NaturalId extends NaturalIdBase<EnrModelCompetitionPlanGen>
    {
        private static final String PROXY_NAME = "EnrModelCompetitionPlanNaturalProxy";

        private Long _step;
        private Long _competition;

        public NaturalId()
        {}

        public NaturalId(EnrModelStep step, EnrCompetition competition)
        {
            _step = ((IEntity) step).getId();
            _competition = ((IEntity) competition).getId();
        }

        public Long getStep()
        {
            return _step;
        }

        public void setStep(Long step)
        {
            _step = step;
        }

        public Long getCompetition()
        {
            return _competition;
        }

        public void setCompetition(Long competition)
        {
            _competition = competition;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrModelCompetitionPlanGen.NaturalId) ) return false;

            EnrModelCompetitionPlanGen.NaturalId that = (NaturalId) o;

            if( !equals(getStep(), that.getStep()) ) return false;
            if( !equals(getCompetition(), that.getCompetition()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getStep());
            result = hashCode(result, getCompetition());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getStep());
            sb.append("/");
            sb.append(getCompetition());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrModelCompetitionPlanGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrModelCompetitionPlan.class;
        }

        public T newInstance()
        {
            return (T) new EnrModelCompetitionPlan();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "step":
                    return obj.getStep();
                case "competition":
                    return obj.getCompetition();
                case "basePlan":
                    return obj.getBasePlan();
                case "plan":
                    return obj.getPlan();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "step":
                    obj.setStep((EnrModelStep) value);
                    return;
                case "competition":
                    obj.setCompetition((EnrCompetition) value);
                    return;
                case "basePlan":
                    obj.setBasePlan((Integer) value);
                    return;
                case "plan":
                    obj.setPlan((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "step":
                        return true;
                case "competition":
                        return true;
                case "basePlan":
                        return true;
                case "plan":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "step":
                    return true;
                case "competition":
                    return true;
                case "basePlan":
                    return true;
                case "plan":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "step":
                    return EnrModelStep.class;
                case "competition":
                    return EnrCompetition.class;
                case "basePlan":
                    return Integer.class;
                case "plan":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrModelCompetitionPlan> _dslPath = new Path<EnrModelCompetitionPlan>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrModelCompetitionPlan");
    }
            

    /**
     * @return Шаг. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelCompetitionPlan#getStep()
     */
    public static EnrModelStep.Path<EnrModelStep> step()
    {
        return _dslPath.step();
    }

    /**
     * @return Конкурс (комбинация условий поступления) для приема. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelCompetitionPlan#getCompetition()
     */
    public static EnrCompetition.Path<EnrCompetition> competition()
    {
        return _dslPath.competition();
    }

    /**
     * @return Число мест до применения процента. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelCompetitionPlan#getBasePlan()
     */
    public static PropertyPath<Integer> basePlan()
    {
        return _dslPath.basePlan();
    }

    /**
     * @return Число мест. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelCompetitionPlan#getPlan()
     */
    public static PropertyPath<Integer> plan()
    {
        return _dslPath.plan();
    }

    public static class Path<E extends EnrModelCompetitionPlan> extends EntityPath<E>
    {
        private EnrModelStep.Path<EnrModelStep> _step;
        private EnrCompetition.Path<EnrCompetition> _competition;
        private PropertyPath<Integer> _basePlan;
        private PropertyPath<Integer> _plan;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Шаг. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelCompetitionPlan#getStep()
     */
        public EnrModelStep.Path<EnrModelStep> step()
        {
            if(_step == null )
                _step = new EnrModelStep.Path<EnrModelStep>(L_STEP, this);
            return _step;
        }

    /**
     * @return Конкурс (комбинация условий поступления) для приема. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelCompetitionPlan#getCompetition()
     */
        public EnrCompetition.Path<EnrCompetition> competition()
        {
            if(_competition == null )
                _competition = new EnrCompetition.Path<EnrCompetition>(L_COMPETITION, this);
            return _competition;
        }

    /**
     * @return Число мест до применения процента. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelCompetitionPlan#getBasePlan()
     */
        public PropertyPath<Integer> basePlan()
        {
            if(_basePlan == null )
                _basePlan = new PropertyPath<Integer>(EnrModelCompetitionPlanGen.P_BASE_PLAN, this);
            return _basePlan;
        }

    /**
     * @return Число мест. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelCompetitionPlan#getPlan()
     */
        public PropertyPath<Integer> plan()
        {
            if(_plan == null )
                _plan = new PropertyPath<Integer>(EnrModelCompetitionPlanGen.P_PLAN, this);
            return _plan;
        }

        public Class getEntityClass()
        {
            return EnrModelCompetitionPlan.class;
        }

        public String getEntityName()
        {
            return "enrModelCompetitionPlan";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
