/* $Id$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantForeignRequest.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantCustomStateType;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantCustomState;
import ru.tandemservice.unienr14.request.bo.EnrEntrantForeignRequest.ui.List.EnrEntrantForeignRequestList;
import ru.tandemservice.unienr14.request.bo.EnrEntrantForeignRequest.ui.List.EnrEntrantForeignRequestListUI;
import ru.tandemservice.unienr14.request.entity.EnrEntrantForeignRequest;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 7/15/14
 */
public class EnrEntrantForeignRequestDSHandler extends DefaultSearchDataSourceHandler
{
    public static final String ENTRANT_CUSTOM_STATE_PROP = "entrantExtStatus";

    public EnrEntrantForeignRequestDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {

        IDataSettings settings = context.get("settings");
        EnrEnrollmentCampaign enrollmentCampaign = context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
        String lastName = settings.get("lastNameFilter");
        String firstName = settings.get("firstNameFilter");
        String requestNumber = settings.get("requestNumberFilter");
        Date dateFrom = settings.get("registeredFromFilter");
        Date dateTo = settings.get("registeredToFilter");
        Long citizenshipFilter = settings.get("citizenshipFilter");
        List<EnrEntrantCustomStateType> entrantCustomStates = settings.get("entrantCustomStateFilter");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrEntrantForeignRequest.class, "fd")
                .joinPath(DQLJoinType.inner, EnrEntrantForeignRequest.entrant().person().fromAlias("fd"), "person")
                .joinPath(DQLJoinType.inner, Person.identityCard().fromAlias("person"), "idc");
        builder.column("fd");

        FilterUtils.applyLikeFilter(builder, lastName, IdentityCard.lastName().fromAlias("idc"));
        FilterUtils.applyLikeFilter(builder, firstName, IdentityCard.firstName().fromAlias("idc"));

        if (citizenshipFilter != null) {
            if (citizenshipFilter.equals(EnrEntrantForeignRequestListUI.NO_CITIZENSHIP))
                builder.where(ne(property(IdentityCard.citizenship().code().fromAlias("idc")), value(IKladrDefines.RUSSIA_COUNTRY_CODE)));
            else
                builder.where(eq(property(IdentityCard.citizenship().id().fromAlias("idc")), value(citizenshipFilter)));
        }

        if (!StringUtils.isEmpty(requestNumber)) {
            FilterUtils.applyLikeFilter(builder, requestNumber, EnrEntrantForeignRequest.regNumber().fromAlias("fd"));
        }

        if (dateFrom != null || dateTo != null) {
            FilterUtils.applyBetweenFilter(builder, "fd", EnrEntrantForeignRequest.regDate().s(), dateFrom, dateTo);
        }

        builder.where(eq(property("fd", EnrEntrantForeignRequest.entrant().enrollmentCampaign().id()), value(enrollmentCampaign.getId())));

        CommonFilterAddon util = context.get(EnrEntrantForeignRequestList.PARAM_ENR_COMPETITION_UTIL);
        if (util != null && !util.isEmptyFilters())
        {
            builder.where(in(property("fd", EnrEntrantForeignRequest.competition().id()), util.getEntityIdsFilteredBuilder("fd").buildQuery()));
        }

        filterByCustomState(entrantCustomStates, builder);

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).order().build().transform((EnrEntrantForeignRequest request) -> {
            ViewWrapper<EnrEntrantForeignRequest> wrapper = new ViewWrapper<>(request);
            List<EnrEntrantCustomState> customStates = DataAccessServices.dao().getList(EnrEntrantCustomState.class, EnrEntrantCustomState.entrant(), request.getEntrant());
            if(!customStates.isEmpty())
                wrapper.setViewProperty(ENTRANT_CUSTOM_STATE_PROP, customStates);
            else
                wrapper.setViewProperty(ENTRANT_CUSTOM_STATE_PROP, null);
            return wrapper;
        });
    }

    protected void filterByCustomState(List<EnrEntrantCustomStateType> entrantCustomStates, DQLSelectBuilder builder)
    {
        if (entrantCustomStates != null && !entrantCustomStates.isEmpty())
        {
            DQLSelectBuilder csDQL = new DQLSelectBuilder()
                    .fromEntity(EnrEntrantCustomState.class, "ecs")
                    .where(eq(property("ecs", EnrEntrantCustomState.entrant()), property("fd", EnrEntrantForeignRequest.entrant())))
                    .where(in(property("ecs", EnrEntrantCustomState.customState()), entrantCustomStates));

            FilterUtils.applyInPeriodFilterNullSafe(csDQL, "ecs", EnrEntrantCustomState.P_BEGIN_DATE, EnrEntrantCustomState.P_END_DATE, new Date());

            builder.where(exists(csDQL.buildQuery()));
        }
    }
}
