package ru.tandemservice.unienr14.entrant.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unienr14.catalog.entity.EnrStateExamSubject;
import ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantStateExam;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * ЕГЭ онлайн-абитуриента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrOnlineEntrantStateExamGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantStateExam";
    public static final String ENTITY_NAME = "enrOnlineEntrantStateExam";
    public static final int VERSION_HASH = -693357464;
    private static IEntityMeta ENTITY_META;

    public static final String L_STATE_EXAM_SUBJECT = "stateExamSubject";
    public static final String L_ONLINE_ENTRANT = "onlineEntrant";

    private EnrStateExamSubject _stateExamSubject;     // ЕГЭ
    private EnrOnlineEntrant _onlineEntrant;     // Абитуриент

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return ЕГЭ. Свойство не может быть null.
     */
    @NotNull
    public EnrStateExamSubject getStateExamSubject()
    {
        return _stateExamSubject;
    }

    /**
     * @param stateExamSubject ЕГЭ. Свойство не может быть null.
     */
    public void setStateExamSubject(EnrStateExamSubject stateExamSubject)
    {
        dirty(_stateExamSubject, stateExamSubject);
        _stateExamSubject = stateExamSubject;
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     */
    @NotNull
    public EnrOnlineEntrant getOnlineEntrant()
    {
        return _onlineEntrant;
    }

    /**
     * @param onlineEntrant Абитуриент. Свойство не может быть null.
     */
    public void setOnlineEntrant(EnrOnlineEntrant onlineEntrant)
    {
        dirty(_onlineEntrant, onlineEntrant);
        _onlineEntrant = onlineEntrant;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrOnlineEntrantStateExamGen)
        {
            setStateExamSubject(((EnrOnlineEntrantStateExam)another).getStateExamSubject());
            setOnlineEntrant(((EnrOnlineEntrantStateExam)another).getOnlineEntrant());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrOnlineEntrantStateExamGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrOnlineEntrantStateExam.class;
        }

        public T newInstance()
        {
            return (T) new EnrOnlineEntrantStateExam();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "stateExamSubject":
                    return obj.getStateExamSubject();
                case "onlineEntrant":
                    return obj.getOnlineEntrant();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "stateExamSubject":
                    obj.setStateExamSubject((EnrStateExamSubject) value);
                    return;
                case "onlineEntrant":
                    obj.setOnlineEntrant((EnrOnlineEntrant) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "stateExamSubject":
                        return true;
                case "onlineEntrant":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "stateExamSubject":
                    return true;
                case "onlineEntrant":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "stateExamSubject":
                    return EnrStateExamSubject.class;
                case "onlineEntrant":
                    return EnrOnlineEntrant.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrOnlineEntrantStateExam> _dslPath = new Path<EnrOnlineEntrantStateExam>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrOnlineEntrantStateExam");
    }
            

    /**
     * @return ЕГЭ. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantStateExam#getStateExamSubject()
     */
    public static EnrStateExamSubject.Path<EnrStateExamSubject> stateExamSubject()
    {
        return _dslPath.stateExamSubject();
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantStateExam#getOnlineEntrant()
     */
    public static EnrOnlineEntrant.Path<EnrOnlineEntrant> onlineEntrant()
    {
        return _dslPath.onlineEntrant();
    }

    public static class Path<E extends EnrOnlineEntrantStateExam> extends EntityPath<E>
    {
        private EnrStateExamSubject.Path<EnrStateExamSubject> _stateExamSubject;
        private EnrOnlineEntrant.Path<EnrOnlineEntrant> _onlineEntrant;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return ЕГЭ. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantStateExam#getStateExamSubject()
     */
        public EnrStateExamSubject.Path<EnrStateExamSubject> stateExamSubject()
        {
            if(_stateExamSubject == null )
                _stateExamSubject = new EnrStateExamSubject.Path<EnrStateExamSubject>(L_STATE_EXAM_SUBJECT, this);
            return _stateExamSubject;
        }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantStateExam#getOnlineEntrant()
     */
        public EnrOnlineEntrant.Path<EnrOnlineEntrant> onlineEntrant()
        {
            if(_onlineEntrant == null )
                _onlineEntrant = new EnrOnlineEntrant.Path<EnrOnlineEntrant>(L_ONLINE_ENTRANT, this);
            return _onlineEntrant;
        }

        public Class getEntityClass()
        {
            return EnrOnlineEntrantStateExam.class;
        }

        public String getEntityName()
        {
            return "enrOnlineEntrantStateExam";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
