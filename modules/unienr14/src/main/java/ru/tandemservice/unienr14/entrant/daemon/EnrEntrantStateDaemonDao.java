package ru.tandemservice.unienr14.entrant.daemon;

import org.hibernate.Session;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import org.tandemframework.shared.commonbase.base.util.DoNotUseMe;
import org.tandemframework.shared.commonbase.base.util.DoNotUseMe.Reason;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantState;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEnrollmentStepStateCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantStateCodes;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItem;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.min;

/**
 * Демон обновляет состояния выбранных конкурсов и абитуриента
 * @author vdanilov
 */
public class EnrEntrantStateDaemonDao extends UniBaseDao implements IEnrEntrantStateDaemonDao
{
    /** запускается из демона EnrEntrantDaemonBean.DAEMON */
    @DoNotUseMe(reason=Reason.INTERNAL, comment="Внутренний механизм демона")
    public static final Runnable DAEMON_ACTIONS = () -> {
        EnrEntrantDaemonBean.DAEMON.check();
        IEnrEntrantStateDaemonDao dao = IEnrEntrantStateDaemonDao.instance.get();

        // обновляет статус абитуриента
        try { dao.doUpdateRequestedCompetitionStatus(); }
        catch (final Exception t) { Debug.exception(t.getMessage(), t); }

        // обновляет статус абитуриента
        try { dao.doUpdateEntrantStatus(); }
        catch (final Exception t) { Debug.exception(t.getMessage(), t); }
    };

    @Override
    protected void initDao()
    {
    }


    @Override
    public boolean doUpdateEntrantStatus()
    {
        EnrEntrantDaemonBean.DAEMON.check();
        Debug.begin("EnrEntrantStateDaemonBean.doUpdateEntrantStatus");
        try {
            int updates = 0;
            Session session = lock(EnrEntrant.class.getName());

            // сбрасываем всем абитуриентам состояние, если в них нет ВКГ с непустыми состояниями
            updates += executeAndClear(
                new DQLUpdateBuilder(EnrEntrant.class)
                .set(EnrEntrant.L_STATE, nul(PropertyType.LONG))
                .where(isNotNull(property(EnrEntrant.state())))
                .where(notExists(
                    new DQLSelectBuilder()
                    .fromEntity(EnrRequestedCompetition.class, "tmp")
                    .column(property("tmp.id"))
                    .where(isNotNull(property(EnrRequestedCompetition.state().fromAlias("tmp"))))
                    .where(eq(property(EnrRequestedCompetition.request().entrant().id().fromAlias("tmp")), property("id")))
                    .buildQuery()
                )),
                session
            );

            // выставляем состояние из ВКГ
            // XXX: важно, что priority - уникальное поле
            updates += executeAndClear(
                new DQLUpdateBuilder(EnrEntrant.class)
                .fromDataSource(
                    new DQLSelectBuilder()
                    .fromEntity(EnrRequestedCompetition.class, "cg")
                    .column(property(EnrRequestedCompetition.request().entrant().id().fromAlias("cg")), "id")
                    .group(property(EnrRequestedCompetition.request().entrant().id().fromAlias("cg")))
                    .column(min(property(EnrRequestedCompetition.state().priority().fromAlias("cg"))), "priority")
                    .buildQuery(),
                    "ds"
                )
                .joinEntity("ds", DQLJoinType.inner, EnrEntrantState.class, "st", eq(property("ds.priority"), property("st.priority")))
                .where(eq(property("id"), property("ds.id")))
                .set(EnrEntrant.L_STATE, property("st"))
                .where(or(
                    isNull(property(EnrEntrant.L_STATE)),
                    ne(property(EnrEntrant.L_STATE), property("st"))
                )),
                session
            );

            return updates > 0;
        } finally {
            Debug.end();
        }
    }

    private final Map<String, Long> enrEntrantStateIdMap = getSystemCatalogIdMap(EnrEntrantState.class);

    @Override
    public boolean doUpdateRequestedCompetitionStatus()
    {
        EnrEntrantDaemonBean.DAEMON.check();
        Debug.begin("EnrEntrantStateDaemonBean.doUpdateRequestedCompetitionStatus");
        try {
            int updates = 0;
            final Session session = lock(EnrRequestedCompetition.class.getName());

            final List<Long> processedStateIds = new ArrayList<>();

            final Long stateTakeDocAwayId = enrEntrantStateIdMap.get(EnrEntrantStateCodes.TAKE_DOCUMENTS_AWAY);
            final Long stateEnrolledId = enrEntrantStateIdMap.get(EnrEntrantStateCodes.ENROLLED);
            final Long stateInOrderId = enrEntrantStateIdMap.get(EnrEntrantStateCodes.IN_ORDER);
            final Long stateSelectEnrolledId = enrEntrantStateIdMap.get(EnrEntrantStateCodes.SELECT_SHOULD_BE_ENROLLED);
            final Long stateRecommendedId = enrEntrantStateIdMap.get(EnrEntrantStateCodes.RECOMMENDED);
            final Long stateExamsPassedId = enrEntrantStateIdMap.get(EnrEntrantStateCodes.EXAMS_PASSED);
            final Long stateOutOfCompetition = enrEntrantStateIdMap.get(EnrEntrantStateCodes.OUT_OF_COMPETITION);
            final Long stateActiveId = enrEntrantStateIdMap.get(EnrEntrantStateCodes.ACTIVE);
            final Long stateEntrantArchivedId = enrEntrantStateIdMap.get(EnrEntrantStateCodes.ENTRANT_ARCHIVED);
            final Long stateExcludedByExtract = enrEntrantStateIdMap.get(EnrEntrantStateCodes.EXCLUDED_BY_EXTRACT);
            final Long stateExcludedByReject = enrEntrantStateIdMap.get(EnrEntrantStateCodes.EXCLUDED_BY_REJECT);

            // null-to-active
            Debug.begin("null-to-active");
            try {
                updates += executeAndClear(
                    new DQLUpdateBuilder(EnrRequestedCompetition.class)
                    .set(EnrRequestedCompetition.L_STATE, value(stateActiveId))
                    .where(isNull(property(EnrRequestedCompetition.L_STATE))),
                    session
                );
            } finally {
                Debug.end();
            }

            // архив
            Debug.begin("entrant-archive");
            try {

                // добавляем состояние
                processedStateIds.add(stateEntrantArchivedId);

                // выставляем врхив
                updates += executeAndClear(
                    new DQLUpdateBuilder(EnrRequestedCompetition.class)
                    .where(notIn(property(EnrRequestedCompetition.state().id()), processedStateIds))
                    .set(EnrRequestedCompetition.L_STATE, value(stateEntrantArchivedId))
                    .where(eq(property(EnrRequestedCompetition.request().entrant().archival()), value(Boolean.TRUE))),
                    session
                );

                // сбрасываем
                updates += executeAndClear(
                    new DQLUpdateBuilder(EnrRequestedCompetition.class)
                    .where(eq(property(EnrRequestedCompetition.state().id()), value(stateEntrantArchivedId)))
                    .set(EnrRequestedCompetition.L_STATE, value(stateActiveId))
                    .where(eq(property(EnrRequestedCompetition.request().entrant().archival()), value(Boolean.FALSE))),
                    session
                );

            } finally {
                Debug.end();
            }

            // забрал документы
            Debug.begin("take-away");
            try {

                // добавляем состояние
                processedStateIds.add(stateTakeDocAwayId);

                // если состояние отлично от «забрал документы», а документы забраны - то переводим в него
                updates += executeAndClear(
                    new DQLUpdateBuilder(EnrRequestedCompetition.class)
                    .where(notIn(property(EnrRequestedCompetition.state().id()), processedStateIds))
                    .set(EnrRequestedCompetition.L_STATE, value(stateTakeDocAwayId))

                    .where(isNotNull(property(EnrRequestedCompetition.request().takeAwayDocumentDate()))),
                    session
                );

                // сбрасываем признак в противном случае
                updates += executeAndClear(
                    new DQLUpdateBuilder(EnrRequestedCompetition.class)
                    .where(eq(property(EnrRequestedCompetition.state().id()), value(stateTakeDocAwayId)))
                    .set(EnrRequestedCompetition.L_STATE, value(stateActiveId))

                    .where(isNull(property(EnrRequestedCompetition.request().takeAwayDocumentDate()))),
                    session
                );

            } finally {
                Debug.end();
            }

            // зачислен
            Debug.begin("enrolled");
            try {

                // добавляем состояние
                processedStateIds.add(stateEnrolledId);

                // неотмененные выписки в проведенных состояниях
                final DQLSelectBuilder extractDql = new DQLSelectBuilder()
                .fromEntity(EnrEnrollmentExtract.class, "e").column(property("e.id"))
                .where(eq(property(EnrEnrollmentExtract.cancelled().fromAlias("e")), value(Boolean.FALSE)))
                .where(in(property(EnrEnrollmentExtract.state().code().fromAlias("e")), EnrEnrollmentExtract.COMMITED_STATE_CODES));

                // если состояние отлично от «зачислен» (и предыдущих), а выписка о зачислении проведена - выставляем «зачислен»
                updates += executeAndClear(
                    new DQLUpdateBuilder(EnrRequestedCompetition.class)
                    .where(notIn(property(EnrRequestedCompetition.state().id()), processedStateIds))
                    .set(EnrRequestedCompetition.L_STATE, value(stateEnrolledId))
                    .where(exists(extractDql.where(eq(property(EnrEnrollmentExtract.entity().id().fromAlias("e")), property("id"))).buildQuery())),
                    session
                );

                // сбрасываем признак в противном случае
                updates += executeAndClear(
                    new DQLUpdateBuilder(EnrRequestedCompetition.class)
                    .where(eq(property(EnrRequestedCompetition.state().id()), value(stateEnrolledId)))
                    .set(EnrRequestedCompetition.L_STATE, value(stateActiveId))
                    .where(notExists(extractDql.where(eq(property(EnrEnrollmentExtract.entity().id().fromAlias("e")), property("id"))).buildQuery())),
                    session
                );

            } finally {
                Debug.end();
            }

            // в приказе
            Debug.begin("in-order");
            try {

                // добавляем состояние
                processedStateIds.add(stateInOrderId);

                // неотмененные выписки (в любых состояниях)
                final DQLSelectBuilder extractDql = new DQLSelectBuilder()
                .fromEntity(EnrEnrollmentExtract.class, "e").column(property("e.id"))
                .where(eq(property(EnrEnrollmentExtract.cancelled().fromAlias("e")), value(Boolean.FALSE)));

                // если состояние отлично от «в приказе» (и предыдущих), а выписка о зачислении есть - выставляем «в приказе»
                updates += executeAndClear(
                    new DQLUpdateBuilder(EnrRequestedCompetition.class)
                    .where(notIn(property(EnrRequestedCompetition.state().id()), processedStateIds))
                    .set(EnrRequestedCompetition.L_STATE, value(stateInOrderId))
                    .where(exists(extractDql.where(eq(property(EnrEnrollmentExtract.entity().id().fromAlias("e")), property("id"))).buildQuery())),
                    session
                );

                // сбрасываем признак в противном случае
                updates += executeAndClear(
                    new DQLUpdateBuilder(EnrRequestedCompetition.class)
                    .where(eq(property(EnrRequestedCompetition.state().id()), value(stateInOrderId)))
                    .set(EnrRequestedCompetition.L_STATE, value(stateActiveId))
                    .where(notExists(extractDql.where(eq(property(EnrEnrollmentExtract.entity().id().fromAlias("e")), property("id"))).buildQuery())),
                    session
                );

            } finally {
                Debug.end();
            }


            // исключен из конкурса (не принес оригиналы, либо зачислен на более высокий приоритет)
            Debug.begin("excluded-by-extract");
            try {

                // добавляем состояние
                processedStateIds.add(stateExcludedByExtract);

                // выславляем значение
                updates += executeAndClear(
                    new DQLUpdateBuilder(EnrRequestedCompetition.class)
                    .where(notIn(property(EnrRequestedCompetition.state().id()), processedStateIds))
                    .set(EnrRequestedCompetition.L_STATE, value(stateExcludedByExtract))

                    .where(exists(
                        new DQLSelectBuilder()
                        .fromEntity(EnrRatingItem.class, "e")
                        .where(eq(property(EnrRatingItem.requestedCompetition().id().fromAlias("e")), property("id")))
                        .where(or(
                            isNotNull(property(EnrRatingItem.excludedByExtract().fromAlias("e"))),
                            isNotNull(property(EnrRatingItem.excludedByExtractMinisterial().fromAlias("e")))
                        ))
                        .buildQuery()
                    )),
                    session
                );

                // сбрасываем значение
                updates += executeAndClear(
                    new DQLUpdateBuilder(EnrRequestedCompetition.class)
                    .where(eq(property(EnrRequestedCompetition.state().id()), value(stateExcludedByExtract)))
                    .set(EnrRequestedCompetition.L_STATE, value(stateActiveId))

                    .where(notExists(
                        new DQLSelectBuilder()
                        .fromEntity(EnrRatingItem.class, "e")
                        .where(eq(property(EnrRatingItem.requestedCompetition().id().fromAlias("e")), property("id")))
                        .where(or(
                            isNotNull(property(EnrRatingItem.excludedByExtract().fromAlias("e"))),
                            isNotNull(property(EnrRatingItem.excludedByExtractMinisterial().fromAlias("e")))
                        ))
                        .buildQuery()
                    )),
                    session
                );

            } finally {
                Debug.end();
            }

            Debug.begin("excluded-by-reject");
            try {

                // добавляем состояние
                processedStateIds.add(stateExcludedByReject);

                // выславляем значение
                updates += executeAndClear(
                    new DQLUpdateBuilder(EnrRequestedCompetition.class)
                    .where(notIn(property(EnrRequestedCompetition.state().id()), processedStateIds))
                    .set(EnrRequestedCompetition.L_STATE, value(stateExcludedByReject))

                    .where(or(
                        exists(
                            new DQLSelectBuilder()
                            .fromEntity(EnrRatingItem.class, "e")
                            .where(eq(property(EnrRatingItem.requestedCompetition().id().fromAlias("e")), property("id")))
                            .where(isNotNull(property(EnrRatingItem.excludedByStepItem().fromAlias("e"))))
                            .buildQuery()
                        ),
                        exists(
                            new DQLSelectBuilder()
                            .fromEntity(EnrEnrollmentExtract.class, "ee")
                            .where(eq(property(EnrEnrollmentExtract.entity().id().fromAlias("ee")), property("id")))
                            .where(eq(property(EnrEnrollmentExtract.cancelled().fromAlias("ee")), value(Boolean.TRUE)))
                            .buildQuery()
                        ),
                        eq(property(EnrRequestedCompetition.refusedToBeEnrolled()), value(Boolean.TRUE))
                    )),
                    session
                );

                // сбрасываем значение
                updates += executeAndClear(
                    new DQLUpdateBuilder(EnrRequestedCompetition.class)
                    .where(eq(property(EnrRequestedCompetition.state().id()), value(stateExcludedByReject)))
                    .set(EnrRequestedCompetition.L_STATE, value(stateActiveId))

                    .where(
                        and(
                            notExists(
                                new DQLSelectBuilder()
                                .fromEntity(EnrRatingItem.class, "e")
                                .where(eq(property(EnrRatingItem.requestedCompetition().id().fromAlias("e")), property("id")))
                                .where(isNotNull(property(EnrRatingItem.excludedByStepItem().fromAlias("e"))))
                                .buildQuery()
                            ),
                            notExists(
                                new DQLSelectBuilder()
                                .fromEntity(EnrEnrollmentExtract.class, "ee")
                                .where(eq(property(EnrEnrollmentExtract.entity().id().fromAlias("ee")), property("id")))
                                .where(eq(property(EnrEnrollmentExtract.cancelled().fromAlias("ee")), value(Boolean.TRUE)))
                                .buildQuery()
                            ),
                            ne(property(EnrRequestedCompetition.refusedToBeEnrolled()), value(Boolean.TRUE))
                        )),
                        session
                );

            } finally {
                Debug.end();
            }

            // Выбран к зачислению
            Debug.begin("select-enrolled");
            try {

                // добавляем состояние
                processedStateIds.add(stateSelectEnrolledId);

                // запрос
                DQLSelectBuilder shouldBeEnrolledDql = new DQLSelectBuilder()
                .fromEntity(EnrEnrollmentStepItem.class, "e")
                .where(eq(property(EnrEnrollmentStepItem.entity().requestedCompetition().id().fromAlias("e")), property("id")))
                .where(eq(property(EnrEnrollmentStepItem.shouldBeEnrolled().fromAlias("e")), value(Boolean.TRUE)))
                .where(notIn(property(EnrEnrollmentStepItem.step().state().code().fromAlias("e")), EnrEnrollmentStepStateCodes.CLOSED, EnrEnrollmentStepStateCodes.START));

                // выставляем признак
                updates += executeAndClear(
                    new DQLUpdateBuilder(EnrRequestedCompetition.class)
                    .where(notIn(property(EnrRequestedCompetition.state().id()), processedStateIds))
                    .set(EnrRequestedCompetition.L_STATE, value(stateSelectEnrolledId))

                    .where(exists(
                        shouldBeEnrolledDql
                        .buildQuery()
                    )),
                    session
                );

                // сбрасываем признак в противном случае
                updates += executeAndClear(
                    new DQLUpdateBuilder(EnrRequestedCompetition.class)
                    .where(eq(property(EnrRequestedCompetition.state().id()), value(stateSelectEnrolledId)))
                    .set(EnrRequestedCompetition.L_STATE, value(stateActiveId))

                    .where(notExists(
                        shouldBeEnrolledDql
                        .buildQuery()
                    )),
                    session
                );

            } finally {
                Debug.end();
            }

            // рекомендованы
            Debug.begin("recommended");
            try {

                // добавляем состояние
                processedStateIds.add(stateRecommendedId);

                // запрос
                DQLSelectBuilder recommendedDql = new DQLSelectBuilder()
                .fromEntity(EnrEnrollmentStepItem.class, "e")
                .where(eq(property(EnrEnrollmentStepItem.entity().requestedCompetition().id().fromAlias("e")), property("id")))
                .where(eq(property(EnrEnrollmentStepItem.recommended().fromAlias("e")), value(Boolean.TRUE)))
                .where(notIn(property(EnrEnrollmentStepItem.step().state().code().fromAlias("e")), EnrEnrollmentStepStateCodes.CLOSED, EnrEnrollmentStepStateCodes.START));

                // выставляем признак
                updates += executeAndClear(
                    new DQLUpdateBuilder(EnrRequestedCompetition.class)
                    .where(notIn(property(EnrRequestedCompetition.state().id()), processedStateIds))
                    .set(EnrRequestedCompetition.L_STATE, value(stateRecommendedId))

                    .where(exists(
                        recommendedDql
                        .buildQuery()
                    )),
                    session
                );

                // сбрасываем признак в противном случае
                updates += executeAndClear(
                    new DQLUpdateBuilder(EnrRequestedCompetition.class)
                    .where(eq(property(EnrRequestedCompetition.state().id()), value(stateRecommendedId)))
                    .set(EnrRequestedCompetition.L_STATE, value(stateActiveId))

                    .where(notExists(
                        recommendedDql
                        .buildQuery()
                    )),
                    session
                );

            } finally {
                Debug.end();
            }

            // к зачислению, выбыл из конкурса
            Debug.begin("exam-passed, out-of-competition");
            try {

                // если состояние отлично от «к зачислению» (и предыдущих), выставляем «к зачислению»
                // если это БЕЗ-ВИ либо в конкурсе есть ВИ, рейтинг есть, он корректен, по всем ВИ есть оценки, все оценки не ниже пороговых
                updates += executeAndClear(
                    new DQLUpdateBuilder(EnrRequestedCompetition.class)
                    .where(notIn(property(EnrRequestedCompetition.state().id()), processedStateIds))
                    .where(ne(property(EnrRequestedCompetition.state().id()), value(stateExamsPassedId)))
                    .set(EnrRequestedCompetition.L_STATE, value(stateExamsPassedId))

                    .fromEntity(EnrRatingItem.class, "ri")
                    .where(eq(property("id"), property(EnrRatingItem.requestedCompetition().fromAlias("ri"))))
                    .where(eq(property(EnrRatingItem.statusRatingPositive().fromAlias("ri")), value(Boolean.TRUE))),
                    session
                );

                // если состояние отлично от «выбыл из конкурса» (и предыдущих), выставляем «выбыл из конкурса»
                // если в конкурсе есть ВИ, рейтинг есть, он корректен, по всем ВИ есть оценки, есть оценка ниже пороговой
                updates += executeAndClear(
                    new DQLUpdateBuilder(EnrRequestedCompetition.class)
                    .where(notIn(property(EnrRequestedCompetition.state().id()), processedStateIds))
                    .where(ne(property(EnrRequestedCompetition.state().id()), value(stateOutOfCompetition)))
                    .set(EnrRequestedCompetition.L_STATE, value(stateOutOfCompetition))

                    .fromEntity(EnrRatingItem.class, "ri")
                    .where(eq(property("id"), property(EnrRatingItem.requestedCompetition().fromAlias("ri"))))
                    .where(eq(property(EnrRatingItem.statusRatingPositive().fromAlias("ri")), value(Boolean.FALSE))),
                    session
                );

                // сбрасываем признак в противном случае
                updates += executeAndClear(
                    new DQLUpdateBuilder(EnrRequestedCompetition.class)
                    .where(or(
                        eq(property(EnrRequestedCompetition.state().id()), value(stateExamsPassedId)),
                        eq(property(EnrRequestedCompetition.state().id()), value(stateOutOfCompetition))
                    ))
                    .set(EnrRequestedCompetition.L_STATE, value(stateActiveId))

                    .fromEntity(EnrRatingItem.class, "ri")
                    .where(eq(property("id"), property(EnrRatingItem.requestedCompetition().fromAlias("ri"))))
                    .where(isNull(property(EnrRatingItem.statusRatingPositive().fromAlias("ri")))),
                    session
                );

                // добавляем состояния
                processedStateIds.add(stateExamsPassedId);
                processedStateIds.add(stateOutOfCompetition);

            } finally {
                Debug.end();
            }

            return updates > 0;
        } finally {
            Debug.end();
        }
    }


}
