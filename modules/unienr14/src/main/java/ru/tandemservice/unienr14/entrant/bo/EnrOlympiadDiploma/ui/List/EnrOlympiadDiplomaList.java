/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrOlympiadDiploma.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.util.Icon;
import ru.tandemservice.unienr14.entrant.bo.EnrOlympiadDiploma.logic.EnrOlympiadDiplomaDSHandler;
import ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma;

/**
 * @author oleyba
 * @since 5/3/13
 */
@Configuration
public class EnrOlympiadDiplomaList extends BusinessComponentManager
{
    public static final String DS_DIPLOMA = "diplomaDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
            .addDataSource(searchListDS(DS_DIPLOMA, diplomaDSColumns(), diplomaDSHandler()))
            .create();
    }

    @Bean
    @SuppressWarnings("unchecked")
    public ColumnListExtPoint diplomaDSColumns()
    {
        return columnListExtPointBuilder(DS_DIPLOMA)
            .addColumn(textColumn("olympiadType", EnrOlympiadDiploma.olympiad().olympiadType().title()).order())
            .addColumn(textColumn("honour", EnrOlympiadDiploma.honour().title()).order())
            .addColumn(textColumn("olympiad", EnrOlympiadDiploma.olympiad().title()).order())
            .addColumn(textColumn("subject", EnrOlympiadDiploma.subject().title()).order())
            .addColumn(textColumn("seriaAndNumber", EnrOlympiadDiploma.seriaAndNumber()).order())
            .addColumn(textColumn("issuanceDate", EnrOlympiadDiploma.issuanceDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order())
            .addColumn(textColumn("settlement", EnrOlympiadDiploma.settlement().title()).order())
            .addColumn(textColumn("registrationDate", EnrOlympiadDiploma.registrationDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order())
            .addColumn(actionColumn("scanCopy", CommonDefines.ICON_PRINT, "onClickDownloadScanCopy").disabled("ui:currentScanCopyDisabled").permissionKey("enr14EntrantPubOlympiadDiplomaTabPrintDip"))
            .addColumn(actionColumn(EDIT_COLUMN_NAME, new Icon(EDIT_COLUMN_NAME), "onClickEdit")
                    .permissionKey("enr14EntrantPubOlympiadDiplomaTabEditDip"))
            .addColumn(actionColumn(DELETE_COLUMN_NAME, new Icon(DELETE_COLUMN_NAME), "onClickDelete", new FormattedMessage("diplomaDS.delete.alert", EnrOlympiadDiploma.olympiad().title().s()))
                .permissionKey("enr14EntrantPubOlympiadDiplomaTabDeleteDip"))
            .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> diplomaDSHandler()
    {
        return new EnrOlympiadDiplomaDSHandler(getName());
    }
}
