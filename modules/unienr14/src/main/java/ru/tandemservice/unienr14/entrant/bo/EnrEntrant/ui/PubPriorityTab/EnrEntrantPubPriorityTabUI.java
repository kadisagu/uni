/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubPriorityTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.view.formatter.RawFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IEntityHandler;
import org.tandemframework.core.view.list.column.IIconResolver;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDefine;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PriorityEdit.EnrEntrantPriorityEdit;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.EnrEntrantRequestManager;
import ru.tandemservice.unienr14.request.entity.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 5/29/13
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "entrant.id")
})
public class EnrEntrantPubPriorityTabUI extends UIPresenter
{
    private EnrEntrant entrant = new EnrEntrant();
    //private StaticListDataSource<IEnrEntrantPrioritized> dataSource;

    private boolean profileNotSelected;

    private List<IRequestTypePriorityBlock> _requestTypePriorityBlocks;
    private IRequestTypePriorityBlock _currentBlock;

    @Override
    public void onComponentRefresh()
    {
        setEntrant(IUniBaseDao.instance.get().getNotNull(EnrEntrant.class, getEntrant().getId()));
        setProfileNotSelected(EnrEntrantManager.instance().dao().isProfileNotSelected(getEntrant()));
        List<EnrRequestType> requestTypes = IUniBaseDao.instance.get().getList(new DQLSelectBuilder()
                .fromEntity(EnrRequestType.class, "rt")
                .where(or(
                        in(property("rt.id"),
                                new DQLSelectBuilder()
                                    .fromEntity(EnrRequestedCompetition.class, "c")
                                    .column(property("c", EnrRequestedCompetition.request().type().id()))
                                    .where(eq(property("c", EnrRequestedCompetition.request().entrant()), value(getEntrant())))
                                    .where(eq(property("c", EnrRequestedCompetition.request().takeAwayDocument()), value(Boolean.FALSE)))
                                    .buildQuery()),
                        in(property("rt.id"),
                                new DQLSelectBuilder()
                                    .fromEntity(EnrRequestedProgram.class, "p")
                                    .where(eq(property("p", EnrRequestedProgram.requestedCompetition().request().entrant()), value(getEntrant())))
                                    .where(eq(property("p", EnrRequestedProgram.requestedCompetition().request().takeAwayDocument()), value(Boolean.FALSE)))
                                    .buildQuery())))
                .order(property("rt", EnrRequestType.code())));

        setRequestTypePriorityBlocks(new ArrayList<>(requestTypes.size()));
        for (final EnrRequestType requestType: requestTypes)
        {
            boolean allowProgramPriorities = false;
            List<IEnrEntrantPrioritized> wrappers = new ArrayList<>();
            {
                List<EnrRequestedCompetition> requestedCompetitions = IUniBaseDao.instance.get().getList(
                        new DQLSelectBuilder()
                                .fromEntity(EnrRequestedCompetition.class, "c")
                                .where(eq(property("c", EnrRequestedCompetition.request().entrant()), value(getEntrant())))
                                .where(eq(property("c", EnrRequestedCompetition.request().takeAwayDocument()), value(Boolean.FALSE)))
                                .where(eq(property("c", EnrRequestedCompetition.request().type()), value(requestType)))
                );

                List<EnrRequestedProgram> requestedPrograms = IUniBaseDao.instance.get().getList(
                    new DQLSelectBuilder()
                        .fromEntity(EnrRequestedProgram.class, "p")
                        .where(eq(property("p", EnrRequestedProgram.requestedCompetition().request().entrant()), value(getEntrant())))
                        .where(eq(property("p", EnrRequestedProgram.requestedCompetition().request().takeAwayDocument()), value(Boolean.FALSE)))
                        .where(eq(property("p", EnrRequestedProgram.requestedCompetition().request().type()), value(requestType)))
                );

                for (EnrRequestedCompetition rcg : requestedCompetitions)
                {
                    if (rcg.getCompetition().isAllowProgramPriorities())
                    {
                        allowProgramPriorities = true;
                    }
                    wrappers.add(rcg);
                }
                for (EnrRequestedProgram rp : requestedPrograms)
                {
                    EnrRequestedCompetition requestedCompetition = rp.getRequestedCompetition();
                    if (requestedCompetition.getCompetition().isAllowProgramPriorities())
                    {
                        EnrProgramSetItem program = rp.getProgramSetItem();
                        if (requestedCompetition instanceof EnrRequestedCompetitionNoExams)
                        {
                            EnrRequestedCompetitionNoExams comp = (EnrRequestedCompetitionNoExams) requestedCompetition;
                            if (program.equals(comp.getProgramSetItem()))
                            {
                                wrappers.add(rp);
                            }
                        }
                        else if (requestedCompetition instanceof EnrRequestedCompetitionExclusive)
                        {
                            EnrRequestedCompetitionExclusive comp = (EnrRequestedCompetitionExclusive) requestedCompetition;
                            if (program.equals(comp.getProgramSetItem()))
                            {
                                wrappers.add(rp);
                            }
                        }
                        else
                            wrappers.add(rp);
                    }
                }
            }

            Collections.sort(wrappers, IEnrEntrantPrioritized.COMPARATOR);

            final StaticListDataSource<IEnrEntrantPrioritized> dataSource = new StaticListDataSource<>();
            {
                dataSource.addColumn(new SimpleColumn("Название", "priorityRowTitle").setFormatter(RawFormatter.INSTANCE).setClickable(false).setOrderable(false));
                if (getEntrant().isAccessible())
                {
                    IEntityHandler disabledCompetitionHandler = entity -> !(entity instanceof EnrRequestedCompetition);
                    IEntityHandler disabledProgramHandler = entity -> !(entity instanceof EnrRequestedProgram);
                    IIconResolver iconCompUpResolver = entity -> entity instanceof EnrRequestedCompetition ? CommonBaseDefine.ICO_UP : null;
                    IIconResolver iconCompDownResolver = entity -> entity instanceof EnrRequestedCompetition ? CommonBaseDefine.ICO_DOWN : null;
                    IIconResolver iconProgramUpResolver = entity -> entity instanceof EnrRequestedProgram ? CommonBaseDefine.ICO_UP : null;
                    IIconResolver iconProgramDownResolver = entity -> entity instanceof EnrRequestedProgram ? CommonBaseDefine.ICO_DOWN : null;

                    ActionColumn compUp = new ActionColumn("Повысить приоритет конкурса", CommonBaseDefine.ICO_UP, "onClickRowUp");
                    compUp.setIconResolver(iconCompUpResolver);

                    ActionColumn compDown = new ActionColumn("Понизить приоритет конкурса", CommonBaseDefine.ICO_DOWN, "onClickRowDown");
                    compDown.setIconResolver(iconCompDownResolver);

                    ActionColumn programUp = new ActionColumn("Повысить приоритет ОП", CommonBaseDefine.ICO_UP, "onClickRowUp");
                    programUp.setIconResolver(iconProgramUpResolver);

                    ActionColumn programDown = new ActionColumn("Понизить приоритет ОП", CommonBaseDefine.ICO_DOWN, "onClickRowDown");
                    programDown.setIconResolver(iconProgramDownResolver);

                    dataSource.addColumn(compUp.setPermissionKey("enr14EntrantPubPriorityTabEditProfileAndPriority").setDisableHandler(disabledCompetitionHandler).setOrderable(false));
                    dataSource.addColumn(compDown.setPermissionKey("enr14EntrantPubPriorityTabEditProfileAndPriority").setDisableHandler(disabledCompetitionHandler).setOrderable(false));
                    dataSource.addColumn(programUp.setPermissionKey("enr14EntrantPubPriorityTabEditProfileAndPriority").setDisableHandler(disabledProgramHandler).setVisible(allowProgramPriorities).setOrderable(false));
                    dataSource.addColumn(programDown.setPermissionKey("enr14EntrantPubPriorityTabEditProfileAndPriority").setDisableHandler(disabledProgramHandler).setVisible(allowProgramPriorities).setOrderable(false));
                }

                if (Debug.isEnabled()){ dataSource.addColumn(new SimpleColumn("Пр", "priority").setClickable(false).setOrderable(false)); }
                dataSource.setupRows(wrappers);
            }

            getRequestTypePriorityBlocks().add(new IRequestTypePriorityBlock()
            {
                @Override public String getCaption(){ return "Приоритеты выбранных конкурсов и образовательных программ для абитуриента <p style=\"padding:0; margin:5px 0 0 0\"/>" + requestType.getTitle(); }
                @Override public StaticListDataSource<IEnrEntrantPrioritized> getDataSource(){ return dataSource; }
            });
        }
    }

    public void onClickProfileAndPriorityEdit() {
        _uiActivation.asRegion(EnrEntrantPriorityEdit.class).top()
            .parameter("entrantId", getEntrant().getId())
            .activate();
    }

    public void onClickRowUp()
    {
        EnrEntrantRequestManager.instance().dao().doChangePriorityUp(getListenerParameterAsLong());
        ContextLocal.getSite().getDesktop().setRefreshScheduled(true);
    }

    public void onClickRowDown()
    {
        EnrEntrantRequestManager.instance().dao().doChangePriorityDown(getListenerParameterAsLong());
        ContextLocal.getSite().getDesktop().setRefreshScheduled(true);
    }

    // getters and setters

    public EnrEntrant getEntrant()
    {
        return entrant;
    }

    public void setEntrant(EnrEntrant entrant)
    {
        this.entrant = entrant;
    }

    public boolean isProfileNotSelected()
    {
        return profileNotSelected;
    }

    public void setProfileNotSelected(boolean profileNotSelected)
    {
        this.profileNotSelected = profileNotSelected;
    }

    public List<IRequestTypePriorityBlock> getRequestTypePriorityBlocks(){ return _requestTypePriorityBlocks; }
    public void setRequestTypePriorityBlocks(List<IRequestTypePriorityBlock> requestTypePriorityBlocks){ _requestTypePriorityBlocks = requestTypePriorityBlocks; }

    public IRequestTypePriorityBlock getCurrentBlock(){ return _currentBlock; }
    public void setCurrentBlock(IRequestTypePriorityBlock currentBlock){ _currentBlock = currentBlock; }


    /** Блок выбранных конкурсов (образовательных программ) с одинаковым видом заявления. */
    public interface IRequestTypePriorityBlock
    {
        String getCaption();
        StaticListDataSource<IEnrEntrantPrioritized> getDataSource();
    }
}