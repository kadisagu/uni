/* $Id:$ */
package ru.tandemservice.unienr14.order.bo.EnrEnrollmentParagraph;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unienr14.order.bo.EnrEnrollmentParagraph.logic.EnrEnrollmentParagraphDao;
import ru.tandemservice.unienr14.order.bo.EnrEnrollmentParagraph.logic.IEnrEnrollmentParagraphDao;

/**
 * @author oleyba
 * @since 7/8/14
 */
@Configuration
public class EnrEnrollmentParagraphManager extends BusinessObjectManager
{
    public static EnrEnrollmentParagraphManager instance()
    {
        return instance(EnrEnrollmentParagraphManager.class);
    }

    @Bean
    public IEnrEnrollmentParagraphDao dao()
    {
        return new EnrEnrollmentParagraphDao();
    }
}
