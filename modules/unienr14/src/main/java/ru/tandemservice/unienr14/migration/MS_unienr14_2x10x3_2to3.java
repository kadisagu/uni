package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.*;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

import java.util.List;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unienr14_2x10x3_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.3")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrOnlineEntrant  identitycard_id -> dnttycrdtdcmntscncpy_t identitycard_id  scancopydocument_id

        if(tool.columnExists("enronlineentrant_t", "idcardscancopy_id"))
        {
            SQLSelectQuery selectQuery = new SQLSelectQuery().from(SQLFrom.table("enronlineentrant_t", "oe")
                    .innerJoin(SQLFrom.table("dnttycrdtdcmntscncpy_t", "sc"), "sc.identitycard_id=oe.identitycard_id"))
                    .column("oe.id")
                    .column("sc.id")
                    .column("sc.scancopydocument_id")
                    .where("sc.isactivescancopy_p=?");

            ISQLTranslator translator = tool.getDialect().getSQLTranslator();

            List<Object[]> resultList = tool.executeQuery(
                    MigrationUtils.processor(Long.class, Long.class, Long.class),
                    translator.toSql(selectQuery), true);

            SQLUpdateQuery onlineEntrantUpdateQuery = new SQLUpdateQuery("enronlineentrant_t", "oe")
                    .set("idcardscancopy_id", "?")
                    .where("oe.id=?");

            SQLDeleteQuery iCardScanCopyDeleteQuery = new SQLDeleteQuery("dnttycrdtdcmntscncpy_t")
                    .where("id=?");

            for(Object[] objects : resultList)
            {
                Long onlineEntrantId = (Long) objects[0];
                Long iCardScanCopyId = (Long) objects[1];
                Long scanCopyId = (Long) objects[2];

                tool.executeUpdate(translator.toSql(onlineEntrantUpdateQuery), scanCopyId, onlineEntrantId);
                tool.executeUpdate(translator.toSql(iCardScanCopyDeleteQuery), iCardScanCopyId);
            }
        }
    }
}