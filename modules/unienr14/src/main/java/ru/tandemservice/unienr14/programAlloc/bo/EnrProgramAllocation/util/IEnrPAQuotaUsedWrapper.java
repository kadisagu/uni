/* $Id: IEcgpQuotaUsedDTO.java 25114 2012-12-03 11:25:11Z vdanilov $ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.util;

import java.util.Map;

/**
 * Занятые места по профилям
 * 
 * @author Vasily Zhukov
 * @since 10.05.2012
 */
public interface IEnrPAQuotaUsedWrapper
{
    /**
     * @return суммарное количество использованных мест
     */
    int getTotalUsed();

    /**
     * @return профиль -> занято мест (всегда не null)
     */
    Map<Long, Integer> getUsedMap();
}
