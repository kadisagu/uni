/* $Id$ */
package ru.tandemservice.unienr14.exams.bo.EnrStateExamResult.ui.Import;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.process.*;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.unienr14.exams.bo.EnrStateExamResult.EnrStateExamResultManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.Date;

/**
 * @author azhebko
 * @since 11.06.2014
 */
public class EnrStateExamResultImportUI extends UIPresenter
{
    private EnrEnrollmentCampaign _enrCampaign;
    private IUploadFile _importFile;
    private byte[] importResultsContent = null;

    @Override
    public void onComponentPrepareRender()
    {
        if (importResultsContent != null) {
            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().xls().fileName(new DateFormatter("ddMMyyyy").format(new Date()) + "_EGE_IMPORT_RESULTS.xls").document(importResultsContent), true);
            importResultsContent = null;
        }
    }

    @Override
    public void onComponentRefresh()
    {
        setEnrCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
    }

    public void onClickApply()
    {
        IBackgroundProcess process = new BackgroundProcessBase()
        {
            @Override
            public ProcessResult run(ProcessState state)
            {
                state.setDisplayMode(ProcessDisplayMode.unknown);
                try
                {
                    importResultsContent = EnrStateExamResultManager.instance().dao().updateStateExamResultsAndGetImportResultsContent(getEnrCampaign(), getImportFile().getStream());

                } catch(Throwable t)
                {
                    throw CoreExceptionUtils.getRuntimeException(t);
                }
                return null;
            }
        };
        BusinessComponentUtils.runProcess(new BackgroundProcessThread("Импорт результатов ЕГЭ", process));
    }

    public EnrEnrollmentCampaign getEnrCampaign(){ return _enrCampaign; }
    public void setEnrCampaign(EnrEnrollmentCampaign enrCampaign){ _enrCampaign = enrCampaign; }

    public IUploadFile getImportFile(){ return _importFile; }
    public void setImportFile(IUploadFile importFile){ _importFile = importFile; }
}