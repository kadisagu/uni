/* $Id: DAO.java 32113 2014-01-27 09:14:01Z hudson $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubLogTab;

import org.tandemframework.shared.fias.base.entity.Address;
import org.tandemframework.shared.person.base.entity.*;
import ru.tandemservice.uni.component.log.EntityLogViewBase.Model;
import ru.tandemservice.unienr14.entrant.entity.*;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;

import java.util.*;

/**
 * @author ekachanova
 */
public class DAO extends ru.tandemservice.uni.component.log.EntityLogViewBase.DAO
{
    private static final List<String> _entityClassNames = Arrays.asList(
        EnrEntrant.ENTITY_CLASS,
        Person.ENTITY_CLASS,
        IdentityCard.ENTITY_CLASS,
        PersonEduDocument.ENTITY_CLASS,
        PersonBenefit.ENTITY_CLASS,
        PersonNextOfKin.ENTITY_CLASS,
        PersonSportAchievement.ENTITY_CLASS,
        PersonForeignLanguage.ENTITY_CLASS,
        Address.ENTITY_CLASS,
        EnrEntrantAccessCourse.ENTITY_CLASS,
        EnrEntrantAccessDepartment.ENTITY_CLASS,
        EnrEntrantInfoAboutUniversity.ENTITY_CLASS,
        EnrEntrantStateExamResult.ENTITY_CLASS,
        EnrEntrantBaseDocument.ENTITY_CLASS,
        EnrOlympiadDiploma.ENTITY_CLASS,
//        EnrAbstractExtract.ENTITY_CLASS,
//        EnrPreliminaryEnrollmentStudent.ENTITY_CLASS,
//        EnrEntrantRequest.ENTITY_CLASS,
//        EnrRequestedCompGroup.ENTITY_CLASS,
//        EnrRequestedDirection.ENTITY_CLASS,
//        EnrRequestedProfile.ENTITY_CLASS,
//        EnrEntrantBenefit.ENTITY_CLASS,
//        EnrEntrantBenefitProof.ENTITY_CLASS,
//        EnrEntrantRequestAttachment.ENTITY_CLASS,
        EnrChosenEntranceExam.ENTITY_CLASS,
        EnrChosenEntranceExamForm.ENTITY_CLASS,
        EnrExamPassDiscipline.ENTITY_CLASS
    );

    @Override
    protected List<String> getEntityClassNames()
    {
        return _entityClassNames;
    }

    @Override
    protected Collection<Long> getEntityIds(Model model)
    {
        EnrEntrant entrant = get(EnrEntrant.class, model.getEntityId());
        Set<Long> entityIds = new HashSet<Long>();
        entityIds.add(entrant.getId());
        entityIds.add(entrant.getPerson().getId());
        entityIds.add(entrant.getPerson().getIdentityCard().getId());
        return entityIds;
    }
}
