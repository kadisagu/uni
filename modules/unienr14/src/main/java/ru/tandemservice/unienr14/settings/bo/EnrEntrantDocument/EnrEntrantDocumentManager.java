/**
 *$Id: EnrEntrantDocumentManager.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.settings.bo.EnrEntrantDocument;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unienr14.settings.bo.EnrEntrantDocument.logic.EnrEntrantDocumentTypeDao;
import ru.tandemservice.unienr14.settings.bo.EnrEntrantDocument.logic.IEnrEntrantDocumentTypeDao;

/**
 * @author Alexander Shaburov
 * @since 30.04.13
 */
@Configuration
public class EnrEntrantDocumentManager extends BusinessObjectManager
{
    public static EnrEntrantDocumentManager instance()
    {
        return instance(EnrEntrantDocumentManager.class);
    }

    @Bean
    public IEnrEntrantDocumentTypeDao typeDao()
    {
        return new EnrEntrantDocumentTypeDao();
    }
}
