/* $Id$ */
package ru.tandemservice.unienr14.settings.bo.EnrEntrantOnlineTreated.ui.AddEdit;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import ru.tandemservice.unienr14.settings.bo.EnrSettings.logic.IEnrSettingsDao;

/**
 * @author Denis Katkov
 * @since 17.06.2016
 */
public class EnrEntrantOnlineTreatedAddEditUI extends UIPresenter
{
    private DataWrapper entrantOnlineTreated;

    @Override
    public void onComponentRefresh()
    {
        IDataSettings settings = DataSettingsFacade.getSettings(IEnrSettingsDao.ENR_SETTINGS);
        Boolean entrantOnlineTreated = settings.get(IEnrSettingsDao.SETTINGS_ENTRANT_ONLINE_TREATED);
        if (entrantOnlineTreated == null) {
            settings.set(IEnrSettingsDao.SETTINGS_ENTRANT_ONLINE_TREATED, Boolean.TRUE);
            DataSettingsFacade.saveSettings(settings);
            setEntrantOnlineTreated(new DataWrapper(0L, "Допускается, такие онлайн-абитуриенты подсвечиваются серым цветом"));
        } else if (entrantOnlineTreated) {
            setEntrantOnlineTreated(new DataWrapper(0L, "Допускается, такие онлайн-абитуриенты подсвечиваются серым цветом"));
        } else {
            setEntrantOnlineTreated(new DataWrapper(1L, "Не допускается, такие онлайн-абитуриенты в списке для выбора не отображаются"));
        }
    }

    public void onClickApply()
    {
        IDataSettings settings = DataSettingsFacade.getSettings(IEnrSettingsDao.ENR_SETTINGS);
        settings.set(IEnrSettingsDao.SETTINGS_ENTRANT_ONLINE_TREATED, getEntrantOnlineTreated().getId() == 0L);
        DataSettingsFacade.saveSettings(settings);
    }

    public DataWrapper getEntrantOnlineTreated()
    {
        return entrantOnlineTreated;
    }

    public void setEntrantOnlineTreated(DataWrapper entrantOnlineTreated)
    {
        this.entrantOnlineTreated = entrantOnlineTreated;
    }
}