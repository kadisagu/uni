/* $Id$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.List;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseSettingsUtil;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilter;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilterType;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterFormConfig;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.logic.CitizenshipModel;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.logic.EnrEntrantDSHandler;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.Add.EnrEntrantAdd;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.util.EntrantRequestAcceptPrincipalHandler;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.EnrEntrantRequestAddWizardWizard;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.EnrEnrollmentCommissionManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.Collections;
import java.util.List;

/**
 * @author azhebko
 * @since 01.05.2014
 */
public class EnrEntrantListUI extends UIPresenter
{


    public static final Long SHOW_ARCHIVAL_CODE = 0L;
    public static final Long SHOW_NON_ARCHIVAL_CODE = 1L;

    private ISelectModel _citizenshipModel;
    private DataWrapper _citizenshipFilter;

    private EnrEnrollmentCampaign _enrollmentCampaign;
    private EnrEnrollmentCommission _enrollmentCommission;
    private boolean _hasGlobalPermissionForEnrollmentCommissions;
    private List<DataWrapper> _acceptRequestFilter;

    @Override
    public void onComponentRefresh()
    {
        setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
        setHasGlobalPermissionForEnrollmentCommissions(EnrEnrollmentCommissionManager.instance().permissionDao().hasGlobalPermissionForEnrCommission(UserContext.getInstance().getPrincipalContext()));
        setEnrollmentCommission(EnrEnrollmentCommissionManager.instance().dao().getDefaultCommission(!isHasGlobalPermissionForEnrollmentCommissions()));
        setCitizenshipModel(new CitizenshipModel());

        CommonFilterAddon util = (CommonFilterAddon) getConfig().getAddon(CommonFilterAddon.class.getSimpleName());
        util
                .configDoubleWidthFilters(false)
                .configUseEnableCheckbox(false)
                .configSettings(getSettingsKey());

        util.clearFilterItems();
        util
                .addFilterItem(EnrCompetitionFilterAddon.REQUEST_TYPE, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.COMPENSATION_TYPE, CommonFilterFormConfig.DEFAULT_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_FORM, CommonFilterFormConfig.DEFAULT_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.COMPETITION_TYPE, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.ENR_ORG_UNIT, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.FORMATIVE_ORG_UNIT, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SUBJECT, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.EDU_PROGRAM, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SET, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG);

        configWhereFilters();
    }
    private void configWhereFilters()
    {
        CommonFilterAddon util = (CommonFilterAddon) getConfig().getAddon(CommonFilterAddon.class.getSimpleName());
        if (util != null)
        {
            util.clearWhereFilter();
            util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EnrCompetition.programSetOrgUnit().orgUnit().enrollmentCampaign(), getEnrollmentCampaign()));
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        getSettings().set("citizenshipFilter", getCitizenshipFilter() == null ? null : getCitizenshipFilter().getId());
        dataSource.put("settings", getSettings());
        dataSource.put(EnrEntrantDSHandler.PARAM_ENR_COMPETITION_UTIL, getConfig().getAddon(CommonFilterAddon.class.getSimpleName()));
        dataSource.put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, getEnrollmentCampaign());
        dataSource.put(EnrEntrantList.PARAM_ENROLLMENT_COMMISSION, getEnrollmentCommission() != null? Collections.singletonList(getEnrollmentCommission()):null);
        dataSource.put("acceptRequestFilter", CommonBaseEntityUtil.getPropertiesList(getAcceptRequestFilter(), "title"));
    }

    public void onClickSearch()
    {
        CommonFilterAddon util = (CommonFilterAddon) getConfig().getAddon(CommonFilterAddon.class.getSimpleName());
        if (util != null)
            util.saveSettings();

        saveSettings();
    }

    public void onClickClear()
    {
        CommonFilterAddon util = (CommonFilterAddon) getConfig().getAddon(CommonFilterAddon.class.getSimpleName());
        if (util != null)
            util.clearSettings();

        setCitizenshipFilter(null);
        setAcceptRequestFilter(null);
        CommonBaseSettingsUtil.clearSettingsExcept(getSettings(), EnrEntrantList.PARAM_ENROLLMENT_COMMISSION);
        configWhereFilters();
        onClickSearch();
    }

    public void onEnrCampaignRefresh()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
        configWhereFilters();
    }

    public void onEnrEnrollmentCommissionRefresh()
    {
        EnrEnrollmentCommissionManager.instance().dao().saveDefaultCommission(getEnrollmentCommission());
        configWhereFilters();
        saveSettings();
    }

    public void onClickRequestWizardNew()
    {
        EnrEnrollmentCampaign enrollmentCampaign = getEnrollmentCampaign();
        if (null != enrollmentCampaign) {
            getEnrollmentCampaign().checkOpen();
        }
        _uiActivation.asDesktopRoot(EnrEntrantRequestAddWizardWizard.class).activate();
    }

    public void onClickAddEntrant()
    {
        if (null != getEnrollmentCampaign()) getEnrollmentCampaign().checkOpen();
        getActivationBuilder().asRegion(EnrEntrantAdd.class).parameter(EnrEnrollmentCampaignManager.BINDING_ENR_CAMPAIGN_ID, getEnrollmentCampaign().getId()).activate();
    }


    public void onClickDeleteEntrant()
    {
        EnrEntrant entrant = IUniBaseDao.instance.get().getNotNull(EnrEntrant.class, getListenerParameterAsLong());
        EnrEntrantManager.instance().dao().deleteEntrant(entrant);
    }

    public void onClickSwitchEntrantArchival()
    {
        EnrEntrant entrant = IUniBaseDao.instance.get().getNotNull(EnrEntrant.class, getListenerParameterAsLong());
        EnrEntrantManager.instance().dao().doChangeEntrantArchival(entrant);
    }

    public ISelectModel getCitizenshipModel(){ return _citizenshipModel; }
    public void setCitizenshipModel(ISelectModel citizenshipModel){ _citizenshipModel = citizenshipModel; }

    public DataWrapper getCitizenshipFilter(){ return _citizenshipFilter; }
    public void setCitizenshipFilter(DataWrapper citizenshipFilter){ _citizenshipFilter = citizenshipFilter; }

    public EnrEnrollmentCampaign getEnrollmentCampaign(){ return _enrollmentCampaign; }
    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign){ _enrollmentCampaign = enrollmentCampaign; }

    public EnrEnrollmentCommission getEnrollmentCommission()
    {
        return _enrollmentCommission;
    }

    public void setEnrollmentCommission(EnrEnrollmentCommission enrollmentCommission)
    {
        _enrollmentCommission = enrollmentCommission;
    }

    public boolean isHasGlobalPermissionForEnrollmentCommissions()
    {
        return _hasGlobalPermissionForEnrollmentCommissions;
    }

    public void setHasGlobalPermissionForEnrollmentCommissions(boolean hasGlobalPermissionForEnrollmentCommissions)
    {
        _hasGlobalPermissionForEnrollmentCommissions = hasGlobalPermissionForEnrollmentCommissions;
    }

    public boolean isNothingSelected(){ return null == getEnrollmentCampaign() || (!isHasGlobalPermissionForEnrollmentCommissions() && getEnrollmentCommission() == null); }


    public List<DataWrapper> getAcceptRequestFilter()
    {
        return _acceptRequestFilter;
    }

    public void setAcceptRequestFilter(List<DataWrapper> acceptRequestFilter)
    {
        _acceptRequestFilter = acceptRequestFilter;
    }
}