package ru.tandemservice.unienr14.settings.daemon;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;

/**
 * @author vdanilov
 */
public interface IEnrSettingsDaemonBean {

    final SpringBeanCache<IEnrSettingsDaemonBean> instance = new SpringBeanCache<IEnrSettingsDaemonBean>(IEnrSettingsDaemonBean.class.getName());

    // блокировка всех демонов в модуле
    final String GLOBAL_DAEMON_LOCK = "enr14globalDaemonLock";

    /**
     * Обновляет зачетный бал по умполчанию для группы (на основе дисциплин, входящих в нее)
     * @return true, если что-то было изменено
     */
    @Transactional(propagation=Propagation.REQUIRED)
    boolean doUpdateGroupDefaultPassMark();


}
