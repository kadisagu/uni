package ru.tandemservice.unienr14.settings.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignOrderVisaItem;
import ru.tandemservice.unienr14.settings.entity.EnrVisasTemplate;
import ru.tandemservice.unimv.IPossibleVisa;
import ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising;
import ru.tandemservice.unimv.entity.visa.gen.IPossibleVisaGen;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Виза для приказа о зачислении
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrCampaignOrderVisaItemGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.settings.entity.EnrCampaignOrderVisaItem";
    public static final String ENTITY_NAME = "enrCampaignOrderVisaItem";
    public static final int VERSION_HASH = 1306512416;
    private static IEntityMeta ENTITY_META;

    public static final String L_POSSIBLE_VISA = "possibleVisa";
    public static final String L_VISA_TEMPLATE = "visaTemplate";
    public static final String P_PRIORITY = "priority";
    public static final String L_GROUPS_MEMBER_VISING = "groupsMemberVising";

    private IPossibleVisa _possibleVisa;     // Возможная виза
    private EnrVisasTemplate _visaTemplate;     // Шаблон визирования
    private int _priority;     // Приоритет
    private GroupsMemberVising _groupsMemberVising;     // Группа участников визирования

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Возможная виза. Свойство не может быть null.
     */
    @NotNull
    public IPossibleVisa getPossibleVisa()
    {
        return _possibleVisa;
    }

    /**
     * @param possibleVisa Возможная виза. Свойство не может быть null.
     */
    public void setPossibleVisa(IPossibleVisa possibleVisa)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && possibleVisa!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IPossibleVisa.class);
            IEntityMeta actual =  possibleVisa instanceof IEntity ? EntityRuntime.getMeta((IEntity) possibleVisa) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_possibleVisa, possibleVisa);
        _possibleVisa = possibleVisa;
    }

    /**
     * @return Шаблон визирования. Свойство не может быть null.
     */
    @NotNull
    public EnrVisasTemplate getVisaTemplate()
    {
        return _visaTemplate;
    }

    /**
     * @param visaTemplate Шаблон визирования. Свойство не может быть null.
     */
    public void setVisaTemplate(EnrVisasTemplate visaTemplate)
    {
        dirty(_visaTemplate, visaTemplate);
        _visaTemplate = visaTemplate;
    }

    /**
     * @return Приоритет. Свойство не может быть null.
     */
    @NotNull
    public int getPriority()
    {
        return _priority;
    }

    /**
     * @param priority Приоритет. Свойство не может быть null.
     */
    public void setPriority(int priority)
    {
        dirty(_priority, priority);
        _priority = priority;
    }

    /**
     * @return Группа участников визирования. Свойство не может быть null.
     */
    @NotNull
    public GroupsMemberVising getGroupsMemberVising()
    {
        return _groupsMemberVising;
    }

    /**
     * @param groupsMemberVising Группа участников визирования. Свойство не может быть null.
     */
    public void setGroupsMemberVising(GroupsMemberVising groupsMemberVising)
    {
        dirty(_groupsMemberVising, groupsMemberVising);
        _groupsMemberVising = groupsMemberVising;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrCampaignOrderVisaItemGen)
        {
            setPossibleVisa(((EnrCampaignOrderVisaItem)another).getPossibleVisa());
            setVisaTemplate(((EnrCampaignOrderVisaItem)another).getVisaTemplate());
            setPriority(((EnrCampaignOrderVisaItem)another).getPriority());
            setGroupsMemberVising(((EnrCampaignOrderVisaItem)another).getGroupsMemberVising());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrCampaignOrderVisaItemGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrCampaignOrderVisaItem.class;
        }

        public T newInstance()
        {
            return (T) new EnrCampaignOrderVisaItem();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "possibleVisa":
                    return obj.getPossibleVisa();
                case "visaTemplate":
                    return obj.getVisaTemplate();
                case "priority":
                    return obj.getPriority();
                case "groupsMemberVising":
                    return obj.getGroupsMemberVising();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "possibleVisa":
                    obj.setPossibleVisa((IPossibleVisa) value);
                    return;
                case "visaTemplate":
                    obj.setVisaTemplate((EnrVisasTemplate) value);
                    return;
                case "priority":
                    obj.setPriority((Integer) value);
                    return;
                case "groupsMemberVising":
                    obj.setGroupsMemberVising((GroupsMemberVising) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "possibleVisa":
                        return true;
                case "visaTemplate":
                        return true;
                case "priority":
                        return true;
                case "groupsMemberVising":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "possibleVisa":
                    return true;
                case "visaTemplate":
                    return true;
                case "priority":
                    return true;
                case "groupsMemberVising":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "possibleVisa":
                    return IPossibleVisa.class;
                case "visaTemplate":
                    return EnrVisasTemplate.class;
                case "priority":
                    return Integer.class;
                case "groupsMemberVising":
                    return GroupsMemberVising.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrCampaignOrderVisaItem> _dslPath = new Path<EnrCampaignOrderVisaItem>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrCampaignOrderVisaItem");
    }
            

    /**
     * @return Возможная виза. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignOrderVisaItem#getPossibleVisa()
     */
    public static IPossibleVisaGen.Path<IPossibleVisa> possibleVisa()
    {
        return _dslPath.possibleVisa();
    }

    /**
     * @return Шаблон визирования. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignOrderVisaItem#getVisaTemplate()
     */
    public static EnrVisasTemplate.Path<EnrVisasTemplate> visaTemplate()
    {
        return _dslPath.visaTemplate();
    }

    /**
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignOrderVisaItem#getPriority()
     */
    public static PropertyPath<Integer> priority()
    {
        return _dslPath.priority();
    }

    /**
     * @return Группа участников визирования. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignOrderVisaItem#getGroupsMemberVising()
     */
    public static GroupsMemberVising.Path<GroupsMemberVising> groupsMemberVising()
    {
        return _dslPath.groupsMemberVising();
    }

    public static class Path<E extends EnrCampaignOrderVisaItem> extends EntityPath<E>
    {
        private IPossibleVisaGen.Path<IPossibleVisa> _possibleVisa;
        private EnrVisasTemplate.Path<EnrVisasTemplate> _visaTemplate;
        private PropertyPath<Integer> _priority;
        private GroupsMemberVising.Path<GroupsMemberVising> _groupsMemberVising;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Возможная виза. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignOrderVisaItem#getPossibleVisa()
     */
        public IPossibleVisaGen.Path<IPossibleVisa> possibleVisa()
        {
            if(_possibleVisa == null )
                _possibleVisa = new IPossibleVisaGen.Path<IPossibleVisa>(L_POSSIBLE_VISA, this);
            return _possibleVisa;
        }

    /**
     * @return Шаблон визирования. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignOrderVisaItem#getVisaTemplate()
     */
        public EnrVisasTemplate.Path<EnrVisasTemplate> visaTemplate()
        {
            if(_visaTemplate == null )
                _visaTemplate = new EnrVisasTemplate.Path<EnrVisasTemplate>(L_VISA_TEMPLATE, this);
            return _visaTemplate;
        }

    /**
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignOrderVisaItem#getPriority()
     */
        public PropertyPath<Integer> priority()
        {
            if(_priority == null )
                _priority = new PropertyPath<Integer>(EnrCampaignOrderVisaItemGen.P_PRIORITY, this);
            return _priority;
        }

    /**
     * @return Группа участников визирования. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignOrderVisaItem#getGroupsMemberVising()
     */
        public GroupsMemberVising.Path<GroupsMemberVising> groupsMemberVising()
        {
            if(_groupsMemberVising == null )
                _groupsMemberVising = new GroupsMemberVising.Path<GroupsMemberVising>(L_GROUPS_MEMBER_VISING, this);
            return _groupsMemberVising;
        }

        public Class getEntityClass()
        {
            return EnrCampaignOrderVisaItem.class;
        }

        public String getEntityName()
        {
            return "enrCampaignOrderVisaItem";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
