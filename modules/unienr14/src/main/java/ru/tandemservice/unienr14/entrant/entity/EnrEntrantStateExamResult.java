package ru.tandemservice.unienr14.entrant.entity;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.unienr14.entrant.entity.gen.EnrEntrantStateExamResultGen;
import ru.tandemservice.unienr14.fis.IFisUidByIdOwner;

/**
 * Результат ЕГЭ
 */
public class EnrEntrantStateExamResult extends EnrEntrantStateExamResultGen implements ITitled, IEnrStateExamResultMark, IFisUidByIdOwner
{
    @EntityDSLSupport
    @Override
    public String getTitle() {
        if (getSubject() == null) {
            return this.getClass().getSimpleName();
        }
        return getSubject().getTitle() + " (" + getYear() + ")";
    }

    @Override
    public String getFisUidTitle()
    {
        return "Результат ЕГЭ: " + getTitle() + ", " + getEntrant().getFullFio();
    }

    @EntityDSLSupport
    @Override
    public Integer getMark() {
        return getMarkAsLong() == null ? null : (int) (getMarkAsLong()/ 1000L);
    }

    public void setMark(Integer mark) {
        if (null == mark)
            setMarkAsLong(null);
        else
            setMarkAsLong(mark* 1000L);
    }

    @EntityDSLSupport
    @Override
    public boolean isEditDisabled() {
        return isVerifiedByUser();
    }
}