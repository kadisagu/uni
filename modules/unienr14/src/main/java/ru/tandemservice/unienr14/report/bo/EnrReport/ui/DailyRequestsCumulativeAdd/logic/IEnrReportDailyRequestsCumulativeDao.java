/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.DailyRequestsCumulativeAdd.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.DailyRequestsCumulativeAdd.EnrReportDailyRequestsCumulativeAddUI;

/**
 * @author rsizonenko
 * @since 04.06.2014
 */
public interface IEnrReportDailyRequestsCumulativeDao extends INeedPersistenceSupport {
    Long createReport(EnrReportDailyRequestsCumulativeAddUI enrReportDailyRequestsCumulativeAddUI);
}
