/* $Id:$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.ActionsAddon.EnrEntrantRequestActionsAddon;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.DirectionListAddon.EnrEntrantRequestDirectionListAddon;

/**
 * @author oleyba
 * @since 5/3/13
 */
@Configuration
public class EnrEntrantRequestPub extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
            .addAddon(uiAddon(EnrEntrantRequestActionsAddon.NAME, EnrEntrantRequestActionsAddon.class))
            .addAddon(uiAddon(EnrEntrantRequestDirectionListAddon.NAME, EnrEntrantRequestDirectionListAddon.class))
            .create();
    }
}
