/* $Id$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.OrgUnitList;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilter;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilterType;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterFormConfig;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.logic.CitizenshipModel;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.logic.EnrEntrantDSHandler;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.Add.EnrEntrantAdd;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.Wizard.EnrEntrantRequestWizard;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.Wizard.EnrEntrantRequestWizardUI;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.EnrEntrantRequestAddWizardWizard;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

/**
 * @author azhebko
 * @since 10.05.2014
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "orgUnitId", required = true)
})
public class EnrEntrantOrgUnitListUI extends UIPresenter
{
    private Long _orgUnitId;
    private OrgUnit _orgUnit;

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }

    public Long getOrgUnitId() { return _orgUnitId; }

    public void setOrgUnitId(Long orgUnitId) { _orgUnitId = orgUnitId; }

    private CommonPostfixPermissionModel _secModel;

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        _secModel = secModel;
    }

    public static final Long SHOW_ARCHIVAL_CODE = 0L;
    public static final Long SHOW_NON_ARCHIVAL_CODE = 1L;

    private ISelectModel _citizenshipModel;
    private DataWrapper _citizenshipFilter;
    private EnrEnrollmentCampaign _enrollmentCampaign;


    @Override
    public void onComponentRefresh()
    {
        setOrgUnit(DataAccessServices.dao().get(OrgUnit.class, _orgUnitId));
        setSecModel(new OrgUnitSecModel(getOrgUnit()));

        setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
        setCitizenshipModel(new CitizenshipModel());

        CommonFilterAddon util = (CommonFilterAddon) getConfig().getAddon(CommonFilterAddon.class.getSimpleName());
        util
                .configDoubleWidthFilters(false)
                .configUseEnableCheckbox(false)
                .configSettings(getSettingsKey());

        util.clearFilterItems();
        util
                .addFilterItem(EnrCompetitionFilterAddon.REQUEST_TYPE, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.COMPENSATION_TYPE, CommonFilterFormConfig.DEFAULT_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_FORM, CommonFilterFormConfig.DEFAULT_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.COMPETITION_TYPE, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.ENR_ORG_UNIT, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.FORMATIVE_ORG_UNIT, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SUBJECT, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.EDU_PROGRAM, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SET, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG);

        configWhereFilters();
    }
    private void configWhereFilters()
    {
        CommonFilterAddon util = (CommonFilterAddon) getConfig().getAddon(CommonFilterAddon.class.getSimpleName());
        if (util != null)
            util
                    .clearWhereFilter()
                    .configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EnrCompetition.programSetOrgUnit().orgUnit().enrollmentCampaign(), this.getEnrollmentCampaign()))
                    .configWhereOrFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EnrCompetition.programSetOrgUnit().orgUnit().institutionOrgUnit().orgUnit().id(), this.getOrgUnitId()), "formativeOrEnrOrgUnit")
                    .configWhereOrFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EnrCompetition.programSetOrgUnit().formativeOrgUnit().id(), this.getOrgUnitId()), "formativeOrEnrOrgUnit");
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        getSettings().set("citizenshipFilter", getCitizenshipFilter() == null ? null : getCitizenshipFilter().getId());
        dataSource.put("settings", getSettings());
        dataSource.put(EnrEntrantDSHandler.PARAM_ENR_COMPETITION_UTIL, getConfig().getAddon(CommonFilterAddon.class.getSimpleName()));
        dataSource.put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, getEnrollmentCampaign());
        dataSource.put(EnrEntrantDSHandler.PARAM_ORG_UNIT, _orgUnitId);
    }

    public void onClickSearch()
    {
        CommonFilterAddon util = (CommonFilterAddon) getConfig().getAddon(CommonFilterAddon.class.getSimpleName());
        if (util != null)
            util.saveSettings();

        saveSettings();
    }

    public void onClickClear()
    {
        CommonFilterAddon util = (CommonFilterAddon) getConfig().getAddon(CommonFilterAddon.class.getSimpleName());
        if (util != null)
            util.clearSettings();

        setCitizenshipFilter(null);
        clearSettings();
        configWhereFilters();
        onClickSearch();
    }

    public void onEnrCampaignRefresh()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
        configWhereFilters();
    }

    public void onClickRequestWizard()
    {
        EnrEnrollmentCampaign enrollmentCampaign = getEnrollmentCampaign();
        if (null == enrollmentCampaign) {
            throw new ApplicationException("Не выбрана приемная кампания.");
        }

        getActivationBuilder().asRegion(EnrEntrantRequestWizard.class).parameter(EnrEntrantRequestWizardUI.PUBLISHER_ID, getEnrollmentCampaign().getId()).activate();
    }

    public void onClickAddEntrant()
    {
        getActivationBuilder().asRegion(EnrEntrantAdd.class).parameter(EnrEnrollmentCampaignManager.BINDING_ENR_CAMPAIGN_ID, getEnrollmentCampaign().getId()).activate();
    }


    public void onClickDeleteEntrant()
    {
        EnrEntrant entrant = IUniBaseDao.instance.get().getNotNull(EnrEntrant.class, getListenerParameterAsLong());
        EnrEntrantManager.instance().dao().deleteEntrant(entrant);
    }

    public void onClickSwitchEntrantArchival()
    {
        EnrEntrant entrant = IUniBaseDao.instance.get().getNotNull(EnrEntrant.class, getListenerParameterAsLong());
        EnrEntrantManager.instance().dao().doChangeEntrantArchival(entrant);
    }

    public ISelectModel getCitizenshipModel(){ return _citizenshipModel; }
    public void setCitizenshipModel(ISelectModel citizenshipModel){ _citizenshipModel = citizenshipModel; }

    public DataWrapper getCitizenshipFilter(){ return _citizenshipFilter; }
    public void setCitizenshipFilter(DataWrapper citizenshipFilter){ _citizenshipFilter = citizenshipFilter; }

    public EnrEnrollmentCampaign getEnrollmentCampaign(){ return _enrollmentCampaign; }
    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign){ _enrollmentCampaign = enrollmentCampaign; }

    public boolean isNothingSelected(){ return null == getEnrollmentCampaign(); }

    public void onClickRequestWizardNew()
    {
        _uiActivation.asDesktopRoot(EnrEntrantRequestAddWizardWizard.class).activate();
    }
}