/* $Id$ */
package ru.tandemservice.unienr14.settings.bo.EnrIndividualAchievement.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantAchievementKind;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;

/**
 * @author nvankov
 * @since 4/11/14
 */
@Configuration
public class EnrIndividualAchievementAddEdit extends BusinessComponentManager
{
    public static final String ACHIEVEMENT_KIND_DS = "achievementKindDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
                .addDataSource(selectDS(ACHIEVEMENT_KIND_DS, achievementKindDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler achievementKindDSHandler() {
        return new EntityComboDataSourceHandler(getName(), EnrEntrantAchievementKind.class)
                .where(EnrEntrantAchievementKind.requestType(), EnrIndividualAchievementAddEditUI.REQUEST_TYPE_PROPERTY)
                .order(EnrEntrantAchievementKind.title())
                .filter(EnrEntrantAchievementKind.title())
                .pageable(false)
                ;
    }
}



    