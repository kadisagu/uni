package ru.tandemservice.unienr14.request.bo.EnrRequestedCompetition.logic;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantOriginalDocumentStatus;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.Collections;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class EnrReqCompBudgetAcceptedRule implements IPropertyUpdateRule<Boolean> {

    public static final IPropertyUpdateRule<Boolean> INSTANCE = new EnrReqCompBudgetAcceptedRule();

    @Override
    public Map<Boolean, IDQLExpression> buildValueExpressionMap(String alias, Map<String, Object> params) {
        Long enrollmentCampaignId = (Long)params.get("enrollmentCampaignId");

        Long enrollmentRulesBudget = DataAccessServices.dao().<Long>getList(
                new DQLSelectBuilder()
                        .fromEntity(EnrEnrollmentCampaign.class, "tmp")
                        .column(property(EnrEnrollmentCampaign.settings().enrollmentRulesBudget().fromAlias("tmp")))
                        .where(eq(property("tmp.id"), value(enrollmentCampaignId)))
        ).iterator().next();

        if(TwinComboDataSourceHandler.YES_ID.equals(enrollmentRulesBudget))
        {
            return Collections.singletonMap(
                    Boolean.TRUE,
                    or(
                            and(
                                    eq(property(EnrRequestedCompetition.competition().type().compensationType().code().fromAlias(alias)), value(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET)),
                                    eq(property(EnrRequestedCompetition.originalDocumentHandedIn().fromAlias(alias)), value(Boolean.TRUE))
                            ),
                            eq(property(EnrRequestedCompetition.competition().type().compensationType().code().fromAlias(alias)), value(CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT))
                    )
            );
        }

        if(TwinComboDataSourceHandler.NO_ID.equals(enrollmentRulesBudget))
        {
            return Collections.singletonMap(
                    Boolean.TRUE,
                    or(
                            and(
                                    eq(property(EnrRequestedCompetition.competition().type().compensationType().code().fromAlias(alias)), value(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET)),
                                    eq(property(EnrRequestedCompetition.originalDocumentHandedIn().fromAlias(alias)), value(Boolean.TRUE)),
                                    eq(property(EnrRequestedCompetition.accepted().fromAlias(alias)), value(Boolean.TRUE))
                            ),
                            eq(property(EnrRequestedCompetition.competition().type().compensationType().code().fromAlias(alias)), value(CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT))
                    )
            );
        }

        return null;
    }

    @Override
    public void registerWakeUp(final IDSetEventListener wakeUpListener)
    {
    }
}
