package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.InWizardDataEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.wizard.SimpleWizardUIPresenter;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInline;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInlineConfig;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInlineUI;
import org.tandemframework.shared.fias.base.bo.util.CitizenshipAutocompleteModel;
import org.tandemframework.shared.fias.base.entity.*;
import org.tandemframework.shared.fias.utils.AddressBaseUtils;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.bo.Person.util.IssuancePlaceSelectModel;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonContactData;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.shared.person.catalog.entity.Sex;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.Wizard.EnrEntrantRequestWizardUI;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @author vdanilov
 */
@Input({
    @Bind(key=EnrEntrantRequestWizardUI.BC_PARAM_ENROLLMENT_CAMPAIGN_ID, binding="enrollmentCampaignHolder.id", required=true),
    @Bind(key=EnrEntrantRequestWizardUI.BC_PARAM_PERSON_ID, binding="person.id"),
    @Bind(key=EnrEntrantRequestWizardUI.BC_PARAM_ENTRANT_ID, binding="entrant.id"),
    @Bind(key="identityCard.cardType", binding="identityCard.cardType"),
    @Bind(key="identityCard.seria", binding="identityCard.seria"),
    @Bind(key="identityCard.number", binding="identityCard.number"),
    @Bind(key="identityCard.firstName", binding="identityCard.firstName"),
    @Bind(key="identityCard.lastName", binding="identityCard.lastName")
})
public class EnrEntrantInWizardDataEditUI extends UIPresenter {

    public static final String REGION_REG_ADDRESS = "regAddressRegion";

    private final EntityHolder<EnrEnrollmentCampaign> enrollmentCampaignHolder = new EntityHolder<>();
    public EntityHolder<EnrEnrollmentCampaign> getEnrollmentCampaignHolder() { return this.enrollmentCampaignHolder; }
    public EnrEnrollmentCampaign getEnrollmentCampaign() { return this.getEnrollmentCampaignHolder().getValue(); }

    private EnrEntrant entrant;
    public EnrEntrant getEntrant() { return this.entrant; }
    public void setEntrant(EnrEntrant entrant) { this.entrant = entrant; }

    {
        // должны быть заполнены: абитуриент, персона, карточка
        IdentityCard identityCard = new IdentityCard();
        identityCard.setCitizenship(IUniBaseDao.instance.get().get(AddressCountry.class, AddressCountry.P_CODE, IKladrDefines.RUSSIA_COUNTRY_CODE));

        Person person = new Person();
        person.setIdentityCard(identityCard);
        person.setContactData(new PersonContactData());

        EnrEntrant entrant = new EnrEntrant();
        entrant.setRegistrationDate(new Date());
        entrant.setPerson(person);

        this.setEntrant(entrant);
    }

    public Person getPerson() { return getEntrant().getPerson(); }
    public IdentityCard getIdentityCard() { return getPerson().getIdentityCard(); }

    private List<IdentityCardType> identityCardTypeList = Collections.emptyList();
    public List<IdentityCardType> getIdentityCardTypeList() { return this.identityCardTypeList; }
    public void setIdentityCardTypeList(final List<IdentityCardType> identityCardTypeList) { this.identityCardTypeList = identityCardTypeList; }

    public IdentityCardType getIdentityCardType() { return getIdentityCard().getCardType(); }

    private org.tandemframework.shared.person.base.bo.Person.ui.AddressEdit.Model addressModel = new org.tandemframework.shared.person.base.bo.Person.ui.AddressEdit.Model();
    public org.tandemframework.shared.person.base.bo.Person.ui.AddressEdit.Model getAddressModel() { return this.addressModel; }

    private List<Sex> sexList;
    public List<Sex> getSexList() { return this.sexList; }
    public void setSexList(List<Sex> sexList) { this.sexList = sexList; }

    private ISelectModel countryModel;
    public ISelectModel getCountryModel() { return this.countryModel; }
    public void setCountryModel(ISelectModel countryModel) { this.countryModel = countryModel; }

    private ISelectModel issuancePlaceModel;
    public ISelectModel getIssuancePlaceModel() { return this.issuancePlaceModel; }
    public void setIssuancePlaceModel(ISelectModel issuancePlaceModel) { this.issuancePlaceModel = issuancePlaceModel; }

    @Override
    public void onComponentRefresh()
    {
        final IUniBaseDao dao = IUniBaseDao.instance.get();
        this.setIdentityCardTypeList(dao.getCatalogItemList(IdentityCardType.class));
        this.setSexList(dao.getCatalogItemList(Sex.class));
        this.setCountryModel(new CitizenshipAutocompleteModel());
        this.setIssuancePlaceModel(new IssuancePlaceSelectModel());

        final EnrEnrollmentCampaign ec = this.getEnrollmentCampaignHolder().refresh(EnrEnrollmentCampaign.class);

        if (null != getEntrant().getId())
        {
            // если абитуриент есть, то просто грузим его (вместе с персоной)
            setEntrant(dao.getNotNull(EnrEntrant.class, getEntrant().getId()));
        }
        else if (null != getEntrant().getPerson().getId())
        {
            // если есть только персона, то грузим персону и пробуем найти абитуриента
            Person person = dao.getNotNull(Person.class, getEntrant().getPerson().getId());
            getEntrant().setPerson(person);

            EnrEntrant entrant = EnrEntrantManager.instance().dao().findSuitableEntrant(ec, person);
            if (null != entrant) {
                this.setEntrant(entrant);
            }
        }

        if (null == getEntrant().getEnrollmentCampaign()) {
            this.getEntrant().setEnrollmentCampaign(ec);
        }

        {
//            org.tandemframework.shared.person.base.bo.Person.ui.AddressEdit.Model addressModel = getAddressModel();
            AddressBase address = getIdentityCard().getAddress();
            if (null == address)
            {
                AddressDetailed orgUnitAddress = null;
                // если адрес не задан, то берем его из подразделений (вплоть до ВУЗа)
                OrgUnit ou = TopOrgUnit.getInstance();
                while (null != ou) {
                    AddressDetailed ouAddress = ou.getAddress();
                    if (null != ouAddress) {
                        break;
                    }
                    ou = ou.getParent();
                }
                if(null != orgUnitAddress)
                {
                    if(IKladrDefines.RUSSIA_COUNTRY_CODE == orgUnitAddress.getCountry().getCode())
                    {
                        address = new AddressRu();
                    }
                    else
                    {
                        address = new AddressInter();
                    }

                    ((AddressDetailed)address).setCountry(orgUnitAddress.getCountry());
                    ((AddressDetailed)address).setSettlement(orgUnitAddress.getSettlement());
                }
            }
            else
            {
                if(null == address.getId() || (address instanceof AddressDetailed && null == ((AddressDetailed)address).getCountry()))
                {
                    // если адрес не задан, то берем его из подразделений (вплоть до ВУЗа)
                    OrgUnit ou = TopOrgUnit.getInstance();
                    while (null != ou) {
                        AddressDetailed ouAddress = ou.getAddress();
                        if (null != ouAddress) {
                            ((AddressDetailed)address).setCountry(ouAddress.getCountry());
                            ((AddressDetailed)address).setSettlement(ouAddress.getSettlement());
                            break;
                        }
                        ou = ou.getParent();
                    }
                }
            }

            AddressBaseEditInlineConfig addressConfig = new AddressBaseEditInlineConfig();
            addressConfig.setDetailedOnly(true);
            addressConfig.setDetailLevel(4);
            addressConfig.setInline(true);
            addressConfig.setAreaVisible(true);
            addressConfig.setWithoutFieldSet(true);
            addressConfig.setAddressBase(address);

            _uiActivation.asRegion(AddressBaseEditInline.class, REGION_REG_ADDRESS).
                    parameter(AddressBaseEditInlineUI.BIND_CONFIG, addressConfig).
                    activate();


        }
    }

    public void onChangeIdCardType()
    {
        getIdentityCard().setCitizenship(getIdentityCard().getCardType().getCitizenshipDefault());
    }

    public void onClickNext()
    {
        AddressBaseEditInlineUI regAddressEditUI = _uiSupport.getChildUI(REGION_REG_ADDRESS);
        if(regAddressEditUI.getResult() == null) throw new RuntimeException("Адрес обязателен");
        final AddressBase regAddress = regAddressEditUI.getResult();

        Long id = DataAccessServices.dao().doInTransaction(session -> {
            ErrorCollector errors = ContextLocal.getErrorCollector();

            EnrEntrant entrant1 = getEntrant();
            Person person = entrant1.getPerson();

            person.getIdentityCard().setRegistrationPeriodFrom(addressModel.getRegistrationPeriodFrom());
            person.getIdentityCard().setRegistrationPeriodTo(addressModel.getRegistrationPeriodTo());

            IdentityCard identityCard = person.getIdentityCard();
            int amount = -13;
            if (identityCard.getBirthDate() != null && identityCard.getBirthDate().after(CoreDateUtils.add(CoreDateUtils.getDayFirstTimeMoment(new Date()), Calendar.YEAR, amount))) {
                errors.add("Абитуриент должен быть старше 13 лет.", "birthDate");
            }

            PersonManager.instance().dao().checkIdentityCard(identityCard, errors);

            //        if (entrant.getRegistrationDate().after(new Date()))  {
            //            errors.add("Дата добавления должна быть не позже сегодняшней.", "registrationDate");
            //        }
            //
            // TODO?: PersonManager.instance().dao().checkPersonUnique(person);

            return EnrEntrantManager.instance().dao().doSaveEntrantWizardData(entrant1, regAddress,
                    addressModel.isAddressEquals() ? AddressBaseUtils.getSameAddress(regAddress) : null);
        });

        deactivate(
            new SimpleWizardUIPresenter.ReturnBuilder(getConfig(), id)
            .add(EnrEntrantRequestWizardUI.BC_PARAM_ENTRANT_ID, id)
            .buildMap()
        );
    }


}
