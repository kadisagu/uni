/* $Id:$ */
package ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.Pub;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.formatter.NewLineFormatter;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.EnrCompetitionManager;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.logic.EnrCompetitionDSHandler;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.CorrectBSPlans.EnrCompetitionCorrectBSPlans;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.CorrectBSPlans.EnrCompetitionCorrectBSPlansUI;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.PlanEdit.EnrCompetitionPlanEdit;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.EnrProgramSetManager;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.AddEdit.EnrProgramSetAddEdit;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.AddEdit.EnrProgramSetAddEditUI;
import ru.tandemservice.unienr14.competition.entity.*;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.EnrExamSetManager;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.logic.ExamSetVariantKind;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.logic.IEnrExamSetDao;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.ui.VariantAddEdit.EnrExamSetVariantAddEdit;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.ui.VariantAddEdit.EnrExamSetVariantAddEditUI;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author oleyba
 * @since 2/4/14
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "programSetHolder.id", required = true),
    @Bind(key = "selectedPage", binding = "selectedPage")
})
public class EnrProgramSetPubUI extends UIPresenter
{
    private EntityHolder<EnrProgramSetBase> _programSetHolder = new EntityHolder<>();

    private String _examSetTitle;
    private String _examSetMinTitle;
    private String _examSetConTitle;

    private String _programTitles;

    private List<EnrProgramSetOrgUnit> _rowList;
    private EnrProgramSetOrgUnit _currentRow;

    private StaticListDataSource<ExamSetVariantWrapper> _examSetVariantDS;

    private boolean _competitionOpen;

    private boolean examSetSettingsDiffer;
    private boolean competitionIncorrect;
    private boolean competitionTaPlanIncorrect;

    private Collection<Long> _taPlanDiffProgramSetOrgUnits;

    private String _selectedPage = EnrProgramSetPub.PROGRAM_SET_INFO_TAB;

    private List<ExamSetVariantKind.ExamSetVariantWrapper> examSetVariantList = new ArrayList<>();
    private ExamSetVariantKind.ExamSetVariantWrapper examSetVariant;

    private String _externalOrgUnitsTitle;

    public void onComponentRefresh()
    {
        getProgramSetHolder().setValue(IUniBaseDao.instance.get().getNotNull(EnrProgramSetSecondary.class, getProgramSetHolder().getId()));

        setRowList(IUniBaseDao.instance.get().getList(EnrProgramSetOrgUnit.class, EnrProgramSetOrgUnit.programSet(), getProgramSet(), EnrProgramSetOrgUnit.orgUnit().institutionOrgUnit().orgUnit().fullTitle().s()));
        setCompetitionOpen(ISharedBaseDao.instance.get().existsEntity(EnrCompetition.class, EnrCompetition.programSetOrgUnit().programSet().s(), getProgramSet()));

        if (isShowExamSetVariantBlock())
        {
            setExamSetVariantList(Collections.<ExamSetVariantKind.ExamSetVariantWrapper>emptyList());

            EnrProgramSetBS programSetBS = (EnrProgramSetBS) getProgramSet();

            List<EnrCompetition> competitions = IUniBaseDao.instance.get().getList(EnrCompetition.class, EnrCompetition.programSetOrgUnit().programSet(), programSetBS);
            List<EnrExamSetVariant> variants = IUniBaseDao.instance.get().getList(EnrExamSetVariant.class, EnrExamSetVariant.owner(), programSetBS);
            Map<Long, IEnrExamSetDao.IExamSetSettings> examSetContent = EnrExamSetManager.instance().dao().getExamSetContent(UniBaseDao.ids(variants));

            List<ExamSetVariantWrapper> wrappers = new ArrayList<>();
            for (final EnrExamSetVariant variant : variants) {
                wrappers.add(
                    new ExamSetVariantWrapper(
                        variant,
                        examSetContent.get(variant.getId()).getTitle(),
                        CollectionUtils.select(competitions, object -> variant.equals(object.getExamSetVariant()) && !object.isNoExams())
                    )
                );
            }

            Collections.sort(wrappers, ITitled.TITLED_COMPARATOR);

            StaticListDataSource<ExamSetVariantWrapper> examSetVariantDS = new StaticListDataSource<>();
            setExamSetVariantDS(examSetVariantDS);

            examSetVariantDS.setupRows(wrappers);
            examSetVariantDS.setMinCountRow(1);
            examSetVariantDS.addColumn(new SimpleColumn("Набор ВИ", "title").setFormatter(NewLineFormatter.NOBR_IN_LINES).setClickable(false).setOrderable(false));
            examSetVariantDS.addColumn(new SimpleColumn("Настройка набора ВИ", "setupTitle").setFormatter(NewLineFormatter.NOBR_IN_LINES).setClickable(false).setOrderable(false));
            examSetVariantDS.addColumn(new SimpleColumn("Установлен для конкурсов", "competitionsTitle").setFormatter(NewLineFormatter.NOBR_IN_LINES).setClickable(false).setOrderable(false));
        }  else {
            setExamSetVariantList(ExamSetVariantKind.getExamSetVariants(getProgramSet()));
        }

        if (!isProgramSetSecondary())
        {
            List<EduProgramHigherProf> programList = new ArrayList<>();
            for (EnrProgramSetItem item : IUniBaseDao.instance.get().getList(EnrProgramSetItem.class, EnrProgramSetItem.programSet(), getProgramSet()))
                programList.add(item.getProgram());

            setProgramTitles(NewLineFormatter.NOBR_IN_LINES.format(StringUtils.join(CollectionUtils.collect(programList, EduProgramHigherProf::getTitleAndConditions), "\n")));
            if (getProgramSet().isTargetContractTraining())
            {
                setExternalOrgUnitsTitle(CommonBaseStringUtil.joinNotEmpty(EnrProgramSetManager.instance().dao().getExternalOrgUnitsForProgramSet(getProgramSet())
                                                                                   .stream()
                                                                                   .map(ExternalOrgUnit::getTitleWithLegalForm)
                                                                                   .sorted()
                                                                                   .collect(Collectors.toList()), "\n"));
            }
        }

        if (isProgramSetTargetAdmissionPlanTabVisible()) {
            _taPlanDiffProgramSetOrgUnits = EnrProgramSetManager.instance().dao().getTAPlanDifferentProgramSetOrgUnits(getProgramSet().getId());
            setCompetitionTaPlanIncorrect(EnrProgramSetManager.instance().dao().isCompetitionTaPlanIncorrect(getProgramSet()));
        } else {
            _taPlanDiffProgramSetOrgUnits = new ArrayList<>();
            setCompetitionTaPlanIncorrect(false);
        }

        setExamSetSettingsDiffer(EnrProgramSetManager.instance().dao().isExamSetSettingsDiffer(getProgramSet()));
        setCompetitionIncorrect(!EnrProgramSetManager.instance().dao().isCompetitionValid(getProgramSet()));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrCompetitionDSHandler.PARAM_PROGRAM_SET, getProgramSet());
    }


    // Listeners
    public void onClickEdit()
    {
        getProgramSet().getEnrollmentCampaign().checkOpen();
        _uiActivation.asRegion(EnrProgramSetAddEdit.class).top()
            .parameter(UIPresenter.PUBLISHER_ID, getProgramSet().getId())
            .parameter(EnrProgramSetAddEditUI.SECONDARY, isProgramSetSecondary())
            .activate();
    }

    public void onClickDelete()
    {
        EnrProgramSetManager.instance().dao().deleteProgramSet(getProgramSet().getId());
        deactivate();
    }

    public void onClickEditExamSetVariant() {
        getActivationBuilder().asRegionDialog(EnrExamSetVariantAddEdit.class)
            .parameter(EnrExamSetVariantAddEditUI.BIND_OWNER_ID, getProgramSet().getId())
            .parameter(EnrExamSetVariantAddEditUI.BIND_VARIANT_KIND, ExamSetVariantKind.valueOf((String) getListenerParameter()))
            .activate();
    }

    public void onClickOpenCompetition()
    {
        getProgramSet().getEnrollmentCampaign().checkOpen();
        getSupport().setRefreshScheduled(true);
        EnrProgramSetManager.instance().dao().doOpenCompetition(getProgramSet());
    }

    public void onClickRefreshCompetition()
    {
        getProgramSet().getEnrollmentCampaign().checkOpen();
        getSupport().setRefreshScheduled(true);
        EnrProgramSetManager.instance().dao().doRefreshCompetition(getProgramSet());
    }

    public void onClickCloseCompetition()
    {
        getSupport().setRefreshScheduled(true);
        EnrProgramSetManager.instance().dao().doCloseCompetition(getProgramSet());
    }

    public void onToggleAllowProgramPriorities()
    {
        EnrCompetitionManager.instance().dao().doToggleAllowProgramPriorities(getListenerParameterAsLong());
    }

    public void onClickCorrectPlans()
    {
        getActivationBuilder().asRegionDialog(EnrCompetitionCorrectBSPlans.class)
            .parameter(PUBLISHER_ID, _programSetHolder.getValue().getEnrollmentCampaign().getId())
            .parameter(EnrCompetitionCorrectBSPlansUI.BIND_PS_IDS, Collections.singletonList(_programSetHolder.getValue().getId()))
            .activate();
    }

    public void onEditCompetitionPlan()
    {
        _uiActivation.asRegionDialog(EnrCompetitionPlanEdit.class).parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong()).activate();
    }


    // Permission keys
    public String getViewPubPermissionKey(){ return isProgramSetSecondary() ? "enr14CompetitionSecondaryPubView" : "enr14ProgramSetPubView"; }

    public String getCloseCompetitionPermissionKey(){ return isProgramSetSecondary() ? "enr14CompetitionSecondaryPubDeleteCompetition" : "enr14ProgramSetPubDeleteCompetition"; }

    public String getOpenCompetitionPermissionKey(){ return isProgramSetSecondary() ? "enr14CompetitionSecondaryPubCreateCompetition" : "enr14ProgramSetPubCreateCompetition"; }

    public String getRefreshCompetitionPermissionKey(){ return isProgramSetSecondary() ? "enr14CompetitionSecondaryPubRefreshCompetition" : "enr14ProgramSetPubRefreshCompetition"; }

    public String getCorrectPlansPermissionKey(){ return isProgramSetSecondary() ? "enr14CompetitionSecondaryPubCorrectPlans" : "enr14ProgramSetPubCorrectPlans"; }

    // util
    public String getSticker(){ return getConfig().getProperty(isProgramSetSecondary() ? "ui.sticker.programSetSecondary" : "ui.sticker.programSet"); }

    public boolean isTargetAdmissionPlanIncorrect(){ return  !_taPlanDiffProgramSetOrgUnits.isEmpty(); }

    public boolean isTabsMode()
    {
        return isProgramSetTargetAdmissionPlanTabVisible() || isProgramSetItemOrgUnitPlanTabVisible();
    }

    public boolean isProgramSetMasterOrHigher(){ return isProgramSetMaster() || isProgramSetHigher(); }

    public String getCurrentProgramSetOUStyle(){ return _taPlanDiffProgramSetOrgUnits.contains(getCurrentRow().getId()) ? "color: red;" : ""; }

    public boolean isPlansSetManually()
    {
        return ISharedBaseDao.instance.get().existsEntity(
                new DQLSelectBuilder()
                        .fromEntity(EnrCompetition.class, "c")
                        .where(DQLExpressions.eq(DQLExpressions.property("c", EnrCompetition.programSetOrgUnit().programSet()), DQLExpressions.value(getProgramSet())))
                        .where(DQLExpressions.ne(DQLExpressions.property("c", EnrCompetition.plan()), DQLExpressions.property("c", EnrCompetition.calculatedPlan())))
                        .buildQuery());
    }

    public String getPlansSetManuallyStyle()
    {
        return isPlansSetManually() ? "color: red;" : "" ;
    }

    public boolean isProgramSetTargetAdmissionPlanTabVisible()
    {
        return !isProgramSetSecondary() && getProgramSet().getEnrollmentCampaign().getSettings().isTargetAdmissionCompetition();
    }

    public boolean isProgramSetItemOrgUnitPlanTabVisible()
    {
        return !isProgramSetSecondary() && ISharedBaseDao.instance.get().existsEntity(EnrCompetition.class,
            EnrCompetition.programSetOrgUnit().programSet().s(), getProgramSet(),
            EnrCompetition.allowProgramPriorities().s(), Boolean.TRUE);
    }

    public boolean isExamSetSettingsIncorrect()
    {
        return getProgramSet().isGreaterThan2014() && !getProgramSet().isValidExamSetVariants();
    }

    public boolean isShowExamSetVariantBlock()
    {
        return isProgramSetBS() && getProgramSet().isLessOrEqualThan2014();
    }

    public boolean isAllowEdit() {
        return getProgramSet().isGreaterThan2014();
    }

    public boolean isProgramSetSecondary() { return getProgramSet() instanceof EnrProgramSetSecondary; }
    public boolean isProgramSetMaster() { return getProgramSet() instanceof EnrProgramSetMaster; }
    public boolean isProgramSetHigher()  { return getProgramSet() instanceof EnrProgramSetHigher; }
    public boolean isProgramSetBS() { return getProgramSet() instanceof EnrProgramSetBS; }

    /** Строка набора ВИ в списке. */
    @SuppressWarnings("serial")
    public class ExamSetVariantWrapper extends IdentifiableWrapper<EnrExamSetVariant>
    {
        private String setupTitle;
        private String competitionsTitle;

        private ExamSetVariantWrapper(EnrExamSetVariant i, String title, Collection<EnrCompetition> collectionList) throws ClassCastException
        {
            super(i.getId(), title);

            {
                List<String> setupTitleList = new ArrayList<>();
                for (String property : EnrProgramSetBS.VARIANT_USAGE_PROPERTIES) {
                    if (i.equals(getProgramSet().getProperty(property)))
                        setupTitleList.add(EnrProgramSetBS.getVariantUsageTitle(property));
                }
                this.setupTitle = StringUtils.join(setupTitleList, "\n");
            }

            {
                List<String> competitionTitleList = new ArrayList<>();
                for (EnrCompetition competition: collectionList) {
                    competitionTitleList.add(competition.getTitle());
                }
                Collections.sort(competitionTitleList);
                this.competitionsTitle = StringUtils.join(competitionTitleList, "\n");
            }

        }

        public String getSetupTitle(){ return setupTitle; }
        public String getCompetitionsTitle() { return this.competitionsTitle; }
        public String getTitleOneLine(){ return getTitle().replace("\n", "; "); }
    }


    // Getters && Setters

    public EntityHolder<EnrProgramSetBase> getProgramSetHolder(){ return _programSetHolder; }

    public EnrProgramSetBase getProgramSet(){ return getProgramSetHolder().getValue(); }

    public boolean isCompetitionOpen(){ return _competitionOpen; }
    public void setCompetitionOpen(boolean competitionOpen){ _competitionOpen = competitionOpen; }

    public StaticListDataSource<ExamSetVariantWrapper> getExamSetVariantDS(){ return _examSetVariantDS; }
    public void setExamSetVariantDS(StaticListDataSource<ExamSetVariantWrapper> examSetVariantDS){ _examSetVariantDS = examSetVariantDS; }

    public EnrProgramSetOrgUnit getCurrentRow(){ return _currentRow; }
    public void setCurrentRow(EnrProgramSetOrgUnit currentRow){ _currentRow = currentRow; }

    public List<EnrProgramSetOrgUnit> getRowList(){ return _rowList; }
    public void setRowList(List<EnrProgramSetOrgUnit> rowList){ _rowList = rowList; }

    public String getProgramTitles(){ return _programTitles; }
    public void setProgramTitles(String programTitles){ _programTitles = programTitles; }

    public String getExamSetTitle(){ return _examSetTitle; }
    public void setExamSetTitle(String examSetTitle){ _examSetTitle = examSetTitle; }

    public String getExamSetMinTitle(){ return _examSetMinTitle; }
    public void setExamSetMinTitle(String examSetMinTitle){ _examSetMinTitle = examSetMinTitle; }

    public String getExamSetConTitle(){ return _examSetConTitle; }
    public void setExamSetConTitle(String examSetConTitle){ _examSetConTitle = examSetConTitle; }

    public String getSelectedPage(){ return _selectedPage; }
    public void setSelectedPage(String selectedPage){ _selectedPage = selectedPage; }

    public boolean isExamSetSettingsDiffer() { return examSetSettingsDiffer; }
    public void setExamSetSettingsDiffer(boolean examSetSettingsDiffer) { this.examSetSettingsDiffer = examSetSettingsDiffer; }

    public ExamSetVariantKind.ExamSetVariantWrapper getExamSetVariant()
    {
        return examSetVariant;
    }

    public void setExamSetVariant(ExamSetVariantKind.ExamSetVariantWrapper examSetVariant)
    {
        this.examSetVariant = examSetVariant;
    }

    public List<ExamSetVariantKind.ExamSetVariantWrapper> getExamSetVariantList()
    {
        return examSetVariantList;
    }

    public void setExamSetVariantList(List<ExamSetVariantKind.ExamSetVariantWrapper> examSetVariantList)
    {
        this.examSetVariantList = examSetVariantList;
    }

    public boolean isCompetitionIncorrect()
    {
        return competitionIncorrect;
    }

    public void setCompetitionIncorrect(boolean competitionIncorrect)
    {
        this.competitionIncorrect = competitionIncorrect;
    }

    public boolean isCompetitionTaPlanIncorrect()
    {
        return competitionTaPlanIncorrect;
    }

    public void setCompetitionTaPlanIncorrect(boolean competitionTaPlanIncorrect)
    {
        this.competitionTaPlanIncorrect = competitionTaPlanIncorrect;
    }

    public String getExternalOrgUnitsTitle()
    {
        return _externalOrgUnitsTitle;
    }

    public void setExternalOrgUnitsTitle(String externalOrgUnitsTitle)
    {
        _externalOrgUnitsTitle = externalOrgUnitsTitle;
    }
}