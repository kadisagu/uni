/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantRegistrationJournalAdd.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantRegistrationJournalAdd.EnrReportEntrantRegistrationJournalAddUI;

/**
 * @author rsizonenko
 * @since 09.06.2014
 */
public interface IEnrReportEntrantRegistrationJournalDao extends INeedPersistenceSupport
{
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    Long createReport(EnrReportEntrantRegistrationJournalAddUI enrReportEntrantRegistrationJournalAddUI);

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    byte[] buildReport(EnrReportEntrantRegistrationJournalAddUI model);
}
