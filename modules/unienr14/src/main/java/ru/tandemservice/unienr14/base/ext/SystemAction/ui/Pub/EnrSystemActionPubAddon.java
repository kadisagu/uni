/**
 *$Id: EppSystemActionPubAddon.java 26910 2013-04-10 05:45:04Z vdanilov $
 */
package ru.tandemservice.unienr14.base.ext.SystemAction.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.shared.commonbase.base.entity.NumberQueue;
import ru.tandemservice.unienr14.debug.bo.EnrDebug.ui.EduRandomAdd.EnrDebugEduRandomAdd;
import ru.tandemservice.unienr14.debug.bo.EnrDebug.ui.EnrollmentCampaignClean.EnrDebugEnrollmentCampaignClean;
import ru.tandemservice.unienr14.debug.bo.EnrDebug.ui.EnrollmentCampaignEntrantsDelete.EnrDebugEnrollmentCampaignEntrantsDelete;
import ru.tandemservice.unienr14.debug.bo.EnrDebug.ui.EnrollmentCampaignRandomAdd.EnrDebugEnrollmentCampaignRandomAdd;
import ru.tandemservice.unienr14.debug.bo.EnrDebug.ui.EnrollmentCampaignRatingRandomAdd.EnrDebugEnrollmentCampaignRatingRandomAdd;
import ru.tandemservice.unienr14.debug.bo.EnrDebug.ui.EntrantRandomAdd.EnrDebugEntrantRandomAdd;
import ru.tandemservice.unienr14.entrant.EnrNumberGenerationRule;
import ru.tandemservice.unienr14.report.bo.EnrWSExport.ui.CampaignInfo.EnrWSExportCampaignInfo;
import ru.tandemservice.unienr14.report.bo.EnrWSExport.ui.EntrantRating.EnrWSExportEntrantRating;
import ru.tandemservice.unienr14.report.bo.EnrWSExport.ui.Orders.EnrWSExportOrders;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class EnrSystemActionPubAddon extends UIAddon
{
    public EnrSystemActionPubAddon(IUIPresenter presenter, String name, String componentId) {
        super(presenter, name, componentId);
    }

    /** Заполняет базу рандомными направленностями и образовательными программами на указанный учебный год */
    public void onClickFillEduRandom() {
        getActivationBuilder().asRegionDialog(EnrDebugEduRandomAdd.class).activate();
    }

    /** Заполняет базу рандомными настройками выбранной ПК*/
    public void onClickFillEcRandom() {
        getActivationBuilder().asRegionDialog(EnrDebugEnrollmentCampaignRandomAdd.class).activate();
    }

    /** Заполняет базу произвольными абитуриентами для выбранной ПК, количеством не больше заданного*/
    public void onClickFillEcRandomEntrant() {
        getActivationBuilder().asRegionDialog(EnrDebugEntrantRandomAdd.class).activate();
    }

    /** Обновляет рейтинговые списки для выбранной ПК */
    public void onClickFillEcRating() {
        getActivationBuilder().asRegionDialog(EnrDebugEnrollmentCampaignRatingRandomAdd.class).activate();
    }

    /**Удаляет данные абитуриентов и заявлений в выбранной ПК */
    public void onClickDeleteEcEntrants()
    {
        getActivationBuilder().asRegionDialog(EnrDebugEnrollmentCampaignEntrantsDelete.class).activate();
    }

    /**Удаляет выбранную ПК */
    public void onClickCleanEnrollmentCampaign()
    {
        getActivationBuilder().asRegionDialog(EnrDebugEnrollmentCampaignClean.class).activate();
    }



    public void onClickExportCampaignInfo() throws Exception
    {
        getPresenter().getActivationBuilder().asRegionDialog(EnrWSExportCampaignInfo.class).activate();
    }


    public void onClickExportRatings() throws Exception
    {
        getPresenter().getActivationBuilder().asRegionDialog(EnrWSExportEntrantRating.class).activate();
    }

    public void onClickExportOrders() throws Exception
    {
        getPresenter().getActivationBuilder().asRegionDialog(EnrWSExportOrders.class).activate();
    }

    public void onClickResetNumberQueue() {
        DataAccessServices.dao().doInTransaction(
                session -> new DQLDeleteBuilder(NumberQueue.class)
                        .where(like(property(NumberQueue.key()), value(EnrNumberGenerationRule.PREFIX_ENR14 + "%")))
                        .createStatement(session).execute()
        );
    }
}
