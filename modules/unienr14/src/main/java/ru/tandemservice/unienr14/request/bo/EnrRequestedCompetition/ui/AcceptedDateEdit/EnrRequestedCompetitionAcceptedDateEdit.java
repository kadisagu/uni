/* $Id$ */
package ru.tandemservice.unienr14.request.bo.EnrRequestedCompetition.ui.AcceptedDateEdit;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;

/**
 * @author nvankov
 * @since 31.05.2016
 */
@Configuration
public class EnrRequestedCompetitionAcceptedDateEdit extends BusinessComponentManager
{
}
