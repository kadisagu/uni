/**
 *$Id: EnrEnrollmentOrderVisaTemplateListUI.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentOrder.ui.VisaTemplateList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentOrder.ui.VisaTemplateAddEdit.EnrEnrollmentOrderVisaTemplateAddEdit;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignOrderVisaItem;

/**
 * @author Alexander Shaburov
 * @since 31.07.13
 */
public class EnrEnrollmentOrderVisaTemplateListUI extends UIPresenter
{
    public void onClickAddVisaGroup()
    {
        getActivationBuilder().asRegion(EnrEnrollmentOrderVisaTemplateAddEdit.class)
                .activate();
    }

    public void onClickUp()
    {
        final EnrCampaignOrderVisaItem entity = getEntityByListenerParameterAsLong();

        CommonManager.instance().commonPriorityDao().doChangePriorityUp(getListenerParameterAsLong(),
                EnrCampaignOrderVisaItem.visaTemplate(), entity.getVisaTemplate(),
                EnrCampaignOrderVisaItem.groupsMemberVising(), entity.getGroupsMemberVising());
    }

    public void onClickDown()
    {
        final EnrCampaignOrderVisaItem entity = getEntityByListenerParameterAsLong();

        CommonManager.instance().commonPriorityDao().doChangePriorityDown(getListenerParameterAsLong(),
                EnrCampaignOrderVisaItem.visaTemplate(), entity.getVisaTemplate(),
                EnrCampaignOrderVisaItem.groupsMemberVising(), entity.getGroupsMemberVising());
    }

    public void onClickEdit()
    {
        final EnrCampaignOrderVisaItem entity = getEntityByListenerParameterAsLong();

        getActivationBuilder().asRegion(EnrEnrollmentOrderVisaTemplateAddEdit.class)
                .parameter("visaTemplateId", entity.getVisaTemplate().getId())
                .activate();
    }

    public void onClickDelete()
    {
        final EnrCampaignOrderVisaItem entity = getEntityByListenerParameterAsLong();

        DataAccessServices.dao().delete(entity.getVisaTemplate());
    }
}
