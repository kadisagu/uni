/* $Id$ */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.ui.AddEdit.EnrEnrollmentCommissionAddEdit;

/**
 * @author Alexey Lopatin
 * @since 29.05.2015
 */
@Input({
        @Bind(key = IUIPresenter.PUBLISHER_ID, binding = "enrollmentCommission.id", required = true),
        @Bind(key = "selectedPage", binding = "selectedPage")
})
public class EnrEnrollmentCommissionPubUI extends UIPresenter
{
    private EnrEnrollmentCommission _enrollmentCommission = new EnrEnrollmentCommission();
    private String _selectedPage;

    @Override
    public void onComponentRefresh()
    {
        _enrollmentCommission = DataAccessServices.dao().getNotNull(_enrollmentCommission.getId());
    }

    public void onClickEditEnrollmentCommission()
    {
        _uiActivation.asRegionDialog(EnrEnrollmentCommissionAddEdit.class)
                .parameter(PUBLISHER_ID, _enrollmentCommission.getId())
                .activate();
    }

    public EnrEnrollmentCommission getEnrollmentCommission()
    {
        return _enrollmentCommission;
    }

    public void setEnrollmentCommission(EnrEnrollmentCommission enrollmentCommission)
    {
        _enrollmentCommission = enrollmentCommission;
    }

    public String getSelectedPage()
    {
        return _selectedPage;
    }

    public void setSelectedPage(String selectedPage)
    {
        _selectedPage = selectedPage;
    }
}
