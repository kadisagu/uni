/* $Id$ */
package ru.tandemservice.unienr14.order.logic;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import ru.tandemservice.uni.component.student.StudentPersonCard.IPrintStudentEnrData;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;

/**
 * @author Nikolay Fedorovskih
 * @since 10.09.2014
 */
public class PrintStudentEnrData implements IPrintStudentEnrData
{
    @Override
    public RtfInjectModifier getEnrPrintDataModifier(Long studentId)
    {
        final EnrEnrollmentExtract extract = DataAccessServices.dao().get(EnrEnrollmentExtract.class, EnrEnrollmentExtract.L_STUDENT, studentId);
        if (extract == null || extract.getOrder() == null)
            return null;

        final EnrOrder order = extract.getOrder();
        final EnrRatingItem ratingItem = DataAccessServices.dao().get(EnrRatingItem.class, EnrRatingItem.L_REQUESTED_COMPETITION, extract.getRequestedCompetition());
        final Double sumMark = ratingItem != null ? ratingItem.getTotalMarkAsDouble() : null;

        return new RtfInjectModifier()
                .put("orderNumber", order.getNumber())
                .put("orderDate", order.getCommitDate() != null ? DateFormatter.DEFAULT_DATE_FORMATTER.format(order.getCommitDate()) : "")
                .put("course", "1") // всегда зачисляются на первый курс
                .put("ball", DoubleFormatter.DOUBLE_FORMATTER_1_DIGITS.format(sumMark));
    }
}