package ru.tandemservice.unienr14.report.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uni.entity.report.StorableReport;
import ru.tandemservice.unienr14.report.entity.EnrReportDailyRequestsCumulative;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Ежедневная сводка по заявлениям с нарастающим итогом
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrReportDailyRequestsCumulativeGen extends StorableReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.report.entity.EnrReportDailyRequestsCumulative";
    public static final String ENTITY_NAME = "enrReportDailyRequestsCumulative";
    public static final int VERSION_HASH = 2051076104;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_DATE_FROM = "dateFrom";
    public static final String P_DATE_TO = "dateTo";
    public static final String P_SPLITTED = "splitted";
    public static final String P_REQUEST_TYPE = "requestType";
    public static final String P_PROGRAM_FORM = "programForm";
    public static final String P_ENR_ORG_UNIT = "enrOrgUnit";
    public static final String P_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String P_PROGRAM_SUBJECT = "programSubject";
    public static final String P_EDU_PROGRAM = "eduProgram";
    public static final String P_PROGRAM_SET = "programSet";

    private EnrEnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private Date _dateFrom;     // Заявления добавлены с
    private Date _dateTo;     // Заявления добавлены по
    private String _splitted;     // Получить сводку по филиалам
    private String _requestType;     // Вид заявления
    private String _programForm;     // Форма обучения
    private String _enrOrgUnit;     // Филиал
    private String _formativeOrgUnit;     // Формирующее подр.
    private String _programSubject;     // Направление, спец., профессия
    private String _eduProgram;     // Образовательная программа
    private String _programSet;     // Набор образовательных программ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Заявления добавлены с. Свойство не может быть null.
     */
    @NotNull
    public Date getDateFrom()
    {
        return _dateFrom;
    }

    /**
     * @param dateFrom Заявления добавлены с. Свойство не может быть null.
     */
    public void setDateFrom(Date dateFrom)
    {
        dirty(_dateFrom, dateFrom);
        _dateFrom = dateFrom;
    }

    /**
     * @return Заявления добавлены по. Свойство не может быть null.
     */
    @NotNull
    public Date getDateTo()
    {
        return _dateTo;
    }

    /**
     * @param dateTo Заявления добавлены по. Свойство не может быть null.
     */
    public void setDateTo(Date dateTo)
    {
        dirty(_dateTo, dateTo);
        _dateTo = dateTo;
    }

    /**
     * @return Получить сводку по филиалам.
     */
    @Length(max=255)
    public String getSplitted()
    {
        return _splitted;
    }

    /**
     * @param splitted Получить сводку по филиалам.
     */
    public void setSplitted(String splitted)
    {
        dirty(_splitted, splitted);
        _splitted = splitted;
    }

    /**
     * @return Вид заявления.
     */
    @Length(max=255)
    public String getRequestType()
    {
        return _requestType;
    }

    /**
     * @param requestType Вид заявления.
     */
    public void setRequestType(String requestType)
    {
        dirty(_requestType, requestType);
        _requestType = requestType;
    }

    /**
     * @return Форма обучения.
     */
    @Length(max=255)
    public String getProgramForm()
    {
        return _programForm;
    }

    /**
     * @param programForm Форма обучения.
     */
    public void setProgramForm(String programForm)
    {
        dirty(_programForm, programForm);
        _programForm = programForm;
    }

    /**
     * @return Филиал.
     */
    public String getEnrOrgUnit()
    {
        return _enrOrgUnit;
    }

    /**
     * @param enrOrgUnit Филиал.
     */
    public void setEnrOrgUnit(String enrOrgUnit)
    {
        dirty(_enrOrgUnit, enrOrgUnit);
        _enrOrgUnit = enrOrgUnit;
    }

    /**
     * @return Формирующее подр..
     */
    public String getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    /**
     * @param formativeOrgUnit Формирующее подр..
     */
    public void setFormativeOrgUnit(String formativeOrgUnit)
    {
        dirty(_formativeOrgUnit, formativeOrgUnit);
        _formativeOrgUnit = formativeOrgUnit;
    }

    /**
     * @return Направление, спец., профессия.
     */
    public String getProgramSubject()
    {
        return _programSubject;
    }

    /**
     * @param programSubject Направление, спец., профессия.
     */
    public void setProgramSubject(String programSubject)
    {
        dirty(_programSubject, programSubject);
        _programSubject = programSubject;
    }

    /**
     * @return Образовательная программа.
     */
    public String getEduProgram()
    {
        return _eduProgram;
    }

    /**
     * @param eduProgram Образовательная программа.
     */
    public void setEduProgram(String eduProgram)
    {
        dirty(_eduProgram, eduProgram);
        _eduProgram = eduProgram;
    }

    /**
     * @return Набор образовательных программ.
     */
    public String getProgramSet()
    {
        return _programSet;
    }

    /**
     * @param programSet Набор образовательных программ.
     */
    public void setProgramSet(String programSet)
    {
        dirty(_programSet, programSet);
        _programSet = programSet;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EnrReportDailyRequestsCumulativeGen)
        {
            setEnrollmentCampaign(((EnrReportDailyRequestsCumulative)another).getEnrollmentCampaign());
            setDateFrom(((EnrReportDailyRequestsCumulative)another).getDateFrom());
            setDateTo(((EnrReportDailyRequestsCumulative)another).getDateTo());
            setSplitted(((EnrReportDailyRequestsCumulative)another).getSplitted());
            setRequestType(((EnrReportDailyRequestsCumulative)another).getRequestType());
            setProgramForm(((EnrReportDailyRequestsCumulative)another).getProgramForm());
            setEnrOrgUnit(((EnrReportDailyRequestsCumulative)another).getEnrOrgUnit());
            setFormativeOrgUnit(((EnrReportDailyRequestsCumulative)another).getFormativeOrgUnit());
            setProgramSubject(((EnrReportDailyRequestsCumulative)another).getProgramSubject());
            setEduProgram(((EnrReportDailyRequestsCumulative)another).getEduProgram());
            setProgramSet(((EnrReportDailyRequestsCumulative)another).getProgramSet());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrReportDailyRequestsCumulativeGen> extends StorableReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrReportDailyRequestsCumulative.class;
        }

        public T newInstance()
        {
            return (T) new EnrReportDailyRequestsCumulative();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "dateFrom":
                    return obj.getDateFrom();
                case "dateTo":
                    return obj.getDateTo();
                case "splitted":
                    return obj.getSplitted();
                case "requestType":
                    return obj.getRequestType();
                case "programForm":
                    return obj.getProgramForm();
                case "enrOrgUnit":
                    return obj.getEnrOrgUnit();
                case "formativeOrgUnit":
                    return obj.getFormativeOrgUnit();
                case "programSubject":
                    return obj.getProgramSubject();
                case "eduProgram":
                    return obj.getEduProgram();
                case "programSet":
                    return obj.getProgramSet();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrEnrollmentCampaign) value);
                    return;
                case "dateFrom":
                    obj.setDateFrom((Date) value);
                    return;
                case "dateTo":
                    obj.setDateTo((Date) value);
                    return;
                case "splitted":
                    obj.setSplitted((String) value);
                    return;
                case "requestType":
                    obj.setRequestType((String) value);
                    return;
                case "programForm":
                    obj.setProgramForm((String) value);
                    return;
                case "enrOrgUnit":
                    obj.setEnrOrgUnit((String) value);
                    return;
                case "formativeOrgUnit":
                    obj.setFormativeOrgUnit((String) value);
                    return;
                case "programSubject":
                    obj.setProgramSubject((String) value);
                    return;
                case "eduProgram":
                    obj.setEduProgram((String) value);
                    return;
                case "programSet":
                    obj.setProgramSet((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                        return true;
                case "dateFrom":
                        return true;
                case "dateTo":
                        return true;
                case "splitted":
                        return true;
                case "requestType":
                        return true;
                case "programForm":
                        return true;
                case "enrOrgUnit":
                        return true;
                case "formativeOrgUnit":
                        return true;
                case "programSubject":
                        return true;
                case "eduProgram":
                        return true;
                case "programSet":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return true;
                case "dateFrom":
                    return true;
                case "dateTo":
                    return true;
                case "splitted":
                    return true;
                case "requestType":
                    return true;
                case "programForm":
                    return true;
                case "enrOrgUnit":
                    return true;
                case "formativeOrgUnit":
                    return true;
                case "programSubject":
                    return true;
                case "eduProgram":
                    return true;
                case "programSet":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return EnrEnrollmentCampaign.class;
                case "dateFrom":
                    return Date.class;
                case "dateTo":
                    return Date.class;
                case "splitted":
                    return String.class;
                case "requestType":
                    return String.class;
                case "programForm":
                    return String.class;
                case "enrOrgUnit":
                    return String.class;
                case "formativeOrgUnit":
                    return String.class;
                case "programSubject":
                    return String.class;
                case "eduProgram":
                    return String.class;
                case "programSet":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrReportDailyRequestsCumulative> _dslPath = new Path<EnrReportDailyRequestsCumulative>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrReportDailyRequestsCumulative");
    }
            

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportDailyRequestsCumulative#getEnrollmentCampaign()
     */
    public static EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Заявления добавлены с. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportDailyRequestsCumulative#getDateFrom()
     */
    public static PropertyPath<Date> dateFrom()
    {
        return _dslPath.dateFrom();
    }

    /**
     * @return Заявления добавлены по. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportDailyRequestsCumulative#getDateTo()
     */
    public static PropertyPath<Date> dateTo()
    {
        return _dslPath.dateTo();
    }

    /**
     * @return Получить сводку по филиалам.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportDailyRequestsCumulative#getSplitted()
     */
    public static PropertyPath<String> splitted()
    {
        return _dslPath.splitted();
    }

    /**
     * @return Вид заявления.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportDailyRequestsCumulative#getRequestType()
     */
    public static PropertyPath<String> requestType()
    {
        return _dslPath.requestType();
    }

    /**
     * @return Форма обучения.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportDailyRequestsCumulative#getProgramForm()
     */
    public static PropertyPath<String> programForm()
    {
        return _dslPath.programForm();
    }

    /**
     * @return Филиал.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportDailyRequestsCumulative#getEnrOrgUnit()
     */
    public static PropertyPath<String> enrOrgUnit()
    {
        return _dslPath.enrOrgUnit();
    }

    /**
     * @return Формирующее подр..
     * @see ru.tandemservice.unienr14.report.entity.EnrReportDailyRequestsCumulative#getFormativeOrgUnit()
     */
    public static PropertyPath<String> formativeOrgUnit()
    {
        return _dslPath.formativeOrgUnit();
    }

    /**
     * @return Направление, спец., профессия.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportDailyRequestsCumulative#getProgramSubject()
     */
    public static PropertyPath<String> programSubject()
    {
        return _dslPath.programSubject();
    }

    /**
     * @return Образовательная программа.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportDailyRequestsCumulative#getEduProgram()
     */
    public static PropertyPath<String> eduProgram()
    {
        return _dslPath.eduProgram();
    }

    /**
     * @return Набор образовательных программ.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportDailyRequestsCumulative#getProgramSet()
     */
    public static PropertyPath<String> programSet()
    {
        return _dslPath.programSet();
    }

    public static class Path<E extends EnrReportDailyRequestsCumulative> extends StorableReport.Path<E>
    {
        private EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<Date> _dateFrom;
        private PropertyPath<Date> _dateTo;
        private PropertyPath<String> _splitted;
        private PropertyPath<String> _requestType;
        private PropertyPath<String> _programForm;
        private PropertyPath<String> _enrOrgUnit;
        private PropertyPath<String> _formativeOrgUnit;
        private PropertyPath<String> _programSubject;
        private PropertyPath<String> _eduProgram;
        private PropertyPath<String> _programSet;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportDailyRequestsCumulative#getEnrollmentCampaign()
     */
        public EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Заявления добавлены с. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportDailyRequestsCumulative#getDateFrom()
     */
        public PropertyPath<Date> dateFrom()
        {
            if(_dateFrom == null )
                _dateFrom = new PropertyPath<Date>(EnrReportDailyRequestsCumulativeGen.P_DATE_FROM, this);
            return _dateFrom;
        }

    /**
     * @return Заявления добавлены по. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportDailyRequestsCumulative#getDateTo()
     */
        public PropertyPath<Date> dateTo()
        {
            if(_dateTo == null )
                _dateTo = new PropertyPath<Date>(EnrReportDailyRequestsCumulativeGen.P_DATE_TO, this);
            return _dateTo;
        }

    /**
     * @return Получить сводку по филиалам.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportDailyRequestsCumulative#getSplitted()
     */
        public PropertyPath<String> splitted()
        {
            if(_splitted == null )
                _splitted = new PropertyPath<String>(EnrReportDailyRequestsCumulativeGen.P_SPLITTED, this);
            return _splitted;
        }

    /**
     * @return Вид заявления.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportDailyRequestsCumulative#getRequestType()
     */
        public PropertyPath<String> requestType()
        {
            if(_requestType == null )
                _requestType = new PropertyPath<String>(EnrReportDailyRequestsCumulativeGen.P_REQUEST_TYPE, this);
            return _requestType;
        }

    /**
     * @return Форма обучения.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportDailyRequestsCumulative#getProgramForm()
     */
        public PropertyPath<String> programForm()
        {
            if(_programForm == null )
                _programForm = new PropertyPath<String>(EnrReportDailyRequestsCumulativeGen.P_PROGRAM_FORM, this);
            return _programForm;
        }

    /**
     * @return Филиал.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportDailyRequestsCumulative#getEnrOrgUnit()
     */
        public PropertyPath<String> enrOrgUnit()
        {
            if(_enrOrgUnit == null )
                _enrOrgUnit = new PropertyPath<String>(EnrReportDailyRequestsCumulativeGen.P_ENR_ORG_UNIT, this);
            return _enrOrgUnit;
        }

    /**
     * @return Формирующее подр..
     * @see ru.tandemservice.unienr14.report.entity.EnrReportDailyRequestsCumulative#getFormativeOrgUnit()
     */
        public PropertyPath<String> formativeOrgUnit()
        {
            if(_formativeOrgUnit == null )
                _formativeOrgUnit = new PropertyPath<String>(EnrReportDailyRequestsCumulativeGen.P_FORMATIVE_ORG_UNIT, this);
            return _formativeOrgUnit;
        }

    /**
     * @return Направление, спец., профессия.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportDailyRequestsCumulative#getProgramSubject()
     */
        public PropertyPath<String> programSubject()
        {
            if(_programSubject == null )
                _programSubject = new PropertyPath<String>(EnrReportDailyRequestsCumulativeGen.P_PROGRAM_SUBJECT, this);
            return _programSubject;
        }

    /**
     * @return Образовательная программа.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportDailyRequestsCumulative#getEduProgram()
     */
        public PropertyPath<String> eduProgram()
        {
            if(_eduProgram == null )
                _eduProgram = new PropertyPath<String>(EnrReportDailyRequestsCumulativeGen.P_EDU_PROGRAM, this);
            return _eduProgram;
        }

    /**
     * @return Набор образовательных программ.
     * @see ru.tandemservice.unienr14.report.entity.EnrReportDailyRequestsCumulative#getProgramSet()
     */
        public PropertyPath<String> programSet()
        {
            if(_programSet == null )
                _programSet = new PropertyPath<String>(EnrReportDailyRequestsCumulativeGen.P_PROGRAM_SET, this);
            return _programSet;
        }

        public Class getEntityClass()
        {
            return EnrReportDailyRequestsCumulative.class;
        }

        public String getEntityName()
        {
            return "enrReportDailyRequestsCumulative";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
