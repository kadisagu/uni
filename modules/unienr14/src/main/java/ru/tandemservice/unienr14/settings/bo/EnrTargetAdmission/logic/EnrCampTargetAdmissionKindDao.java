/**
 *$Id: EnrCampTargetAdmissionKindDao.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.settings.bo.EnrTargetAdmission.logic;

import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.DaoCache;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrTargetAdmissionKind;
import ru.tandemservice.unienr14.competition.entity.EnrTargetAdmissionPlan;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA;
import ru.tandemservice.unienr14.settings.bo.EnrTargetAdmission.EnrTargetAdmissionManager;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignTargetAdmissionKind;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrTargetAdmissionOrgUnit;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 15.04.13
 */
public class EnrCampTargetAdmissionKindDao extends UniBaseDao implements IEnrCampTargetAdmissionKindDao
{
    @Override
    public Long doToogleTargetAdmissionKindUse(EnrTargetAdmissionKind kind, EnrEnrollmentCampaign enrollmentCampaign)
    {
        final Long relationId = new DQLSelectBuilder().fromEntity(EnrCampaignTargetAdmissionKind.class, "c").column(property(EnrCampaignTargetAdmissionKind.id().fromAlias("c")))
        .where(eq(property(EnrCampaignTargetAdmissionKind.targetAdmissionKind().fromAlias("c")), value(kind)))
        .where(eq(property(EnrCampaignTargetAdmissionKind.enrollmentCampaign().fromAlias("c")), value(enrollmentCampaign)))
        .createStatement(getSession()).uniqueResult();

        if (relationId != null)
            return relationId;

        EnrCampaignTargetAdmissionKind newRelation = new EnrCampaignTargetAdmissionKind();
        newRelation.setTargetAdmissionKind(kind);
        newRelation.setEnrollmentCampaign(enrollmentCampaign);

        save(newRelation);
        return newRelation.getId();
    }

    @Override
    public boolean doToogleTargetAdmissionKindNotUse(EnrTargetAdmissionKind kind, EnrEnrollmentCampaign enrollmentCampaign)
    {
        final Long relationId = new DQLSelectBuilder().fromEntity(EnrCampaignTargetAdmissionKind.class, "c").column(property(EnrCampaignTargetAdmissionKind.id().fromAlias("c")))
        .where(eq(property(EnrCampaignTargetAdmissionKind.targetAdmissionKind().fromAlias("c")), value(kind)))
        .where(eq(property(EnrCampaignTargetAdmissionKind.enrollmentCampaign().fromAlias("c")), value(enrollmentCampaign)))
        .createStatement(getSession()).uniqueResult();

        if (relationId == null)
            return false;

        if (existsEntity(EnrRequestedCompetitionTA.class, EnrRequestedCompetitionTA.L_TARGET_ADMISSION_KIND, relationId))
            throw new ApplicationException("Нельзя выключить вид ЦП, который уже используется в заявлении абитуриента.");

        if (existsEntity(EnrTargetAdmissionOrgUnit.class, EnrTargetAdmissionOrgUnit.L_ENR_CAMPAIGN_T_A_KIND, relationId))
            throw new ApplicationException("Нельзя выключить вид ЦП, для которого уже указана организация ЦП.");

        if (existsEntity(EnrTargetAdmissionPlan.class, EnrTargetAdmissionPlan.L_ENR_CAMPAIGN_T_A_KIND, relationId))
            throw new ApplicationException("Нельзя выключить вид ЦП, для которого уже задан план приема.");

        final List<Long> taIdList = new ArrayList<>();
        taIdList.add(relationId);

        final Long reqDirCount = new DQLSelectBuilder().fromEntity(EnrRequestedCompetitionTA.class, "d").column(property(EnrRequestedCompetitionTA.id().fromAlias("d")))
            .where(in(property(EnrRequestedCompetitionTA.targetAdmissionKind().id().fromAlias("d")), taIdList))
            .where(eq(property(EnrRequestedCompetitionTA.request().entrant().enrollmentCampaign().fromAlias("d")), value(enrollmentCampaign)))
            .createCountStatement(new DQLExecutionContext(getSession())).uniqueResult();

        if (reqDirCount > 0)
            throw new ApplicationException(EnrTargetAdmissionManager.instance().getProperty("targetAdmissionKind.useInEnrCampException"));

        delete(relationId);
        return true;
    }

    @Override
    public Set<EnrTargetAdmissionKind> getUsedTaKinds(EnrEnrollmentCampaign campaign)
    {
        String key = "getUsedTaKinds."+campaign.getId();
        Set<EnrTargetAdmissionKind> kinds = DaoCache.get(key);
        if (null != kinds) { return kinds; }

        kinds = new HashSet<>();
        for (EnrCampaignTargetAdmissionKind ta : getList(EnrCampaignTargetAdmissionKind.class, EnrCampaignTargetAdmissionKind.enrollmentCampaign(), campaign, EnrCampaignTargetAdmissionKind.targetAdmissionKind().priority().s())) {
            kinds.add(ta.getTargetAdmissionKind());
        }

        DaoCache.put(key, kinds);
        return kinds;
    }

    @Override
    public List<EnrTargetAdmissionKind> getSortedTaKindList(EnrEnrollmentCampaign campaign)
    {
        // todo cache it too

        List<EnrTargetAdmissionKind> result = new ArrayList<>();

        for (EnrCampaignTargetAdmissionKind ta : getList(EnrCampaignTargetAdmissionKind.class, EnrCampaignTargetAdmissionKind.enrollmentCampaign(), campaign, EnrCampaignTargetAdmissionKind.targetAdmissionKind().priority().s())) {
            result.add(ta.getTargetAdmissionKind());
        }

        Collections.sort(result, CommonBaseUtil.PRIORITY_SIMPLE_SAFE_COMPARATOR);

        return result;
    }

    @Override
    public boolean isUsedOnceTA(EnrEnrollmentCampaign campaign)
    {
        return getUsedTaKinds(campaign).size() == 1;
    }

    @Override
    public void updateTAKindExtOrgUnits(Long taKindId, EnrEnrollmentCampaign campaign, List<ExternalOrgUnit> externalOrgUnits)
    {
        EnrCampaignTargetAdmissionKind enrCampaignTargetAdmissionKind = new DQLSelectBuilder()
                .fromEntity(EnrCampaignTargetAdmissionKind.class, "e")
                .where(eq(property("e", EnrCampaignTargetAdmissionKind.targetAdmissionKind().id()), value(taKindId)))
                .where(eq(property("e", EnrCampaignTargetAdmissionKind.enrollmentCampaign()), value(campaign)))
                .createStatement(getSession()).uniqueResult();

        if (enrCampaignTargetAdmissionKind == null)
        {
            save(enrCampaignTargetAdmissionKind = new EnrCampaignTargetAdmissionKind(campaign, getNotNull(EnrTargetAdmissionKind.class, taKindId)));
        }

        List<ExternalOrgUnit> oldOrgUnits = new ArrayList<>();
        for (EnrTargetAdmissionOrgUnit ou: getList(EnrTargetAdmissionOrgUnit.class, EnrTargetAdmissionOrgUnit.enrCampaignTAKind(), enrCampaignTargetAdmissionKind))
        {
            oldOrgUnits.add(ou.getExternalOrgUnit());
        }

        Collection<ExternalOrgUnit> toSaveList = CollectionUtils.subtract(externalOrgUnits, oldOrgUnits);
        Collection<ExternalOrgUnit> toDelList = CollectionUtils.subtract(oldOrgUnits, externalOrgUnits);

        for (ExternalOrgUnit externalOrgUnit: toSaveList)
        {
            save(new EnrTargetAdmissionOrgUnit(enrCampaignTargetAdmissionKind, externalOrgUnit));
        }

        new DQLDeleteBuilder(EnrTargetAdmissionOrgUnit.class)
                .where(eq(property(EnrTargetAdmissionOrgUnit.enrCampaignTAKind()), value(enrCampaignTargetAdmissionKind)))
                .where(in(property(EnrTargetAdmissionOrgUnit.externalOrgUnit()), toDelList)).createStatement(getSession()).execute();
    }

    @Override
    public void doToogleTargetAdmissionStateInterest(EnrTargetAdmissionKind kind, EnrEnrollmentCampaign enrollmentCampaign, boolean stateInterest)
    {
        EnrCampaignTargetAdmissionKind relation = new DQLSelectBuilder().fromEntity(EnrCampaignTargetAdmissionKind.class, "c")
                .where(eq(property(EnrCampaignTargetAdmissionKind.targetAdmissionKind().fromAlias("c")), value(kind)))
                .where(eq(property(EnrCampaignTargetAdmissionKind.enrollmentCampaign().fromAlias("c")), value(enrollmentCampaign)))
                .createStatement(getSession()).uniqueResult();

        if(relation == null)
            throw new ApplicationException("Вид ЦП «" + kind.getTitle() + "» не используется в приемной кампании «" + enrollmentCampaign.getTitle() + "»");

        relation.setStateInterest(stateInterest);
        update(relation);
    }
}
