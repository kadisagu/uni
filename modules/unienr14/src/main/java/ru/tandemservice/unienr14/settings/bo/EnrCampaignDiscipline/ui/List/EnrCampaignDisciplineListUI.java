package ru.tandemservice.unienr14.settings.bo.EnrCampaignDiscipline.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.PageableSearchListDataSource;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unienr14.catalog.entity.EnrDiscipline;
import ru.tandemservice.unienr14.catalog.entity.EnrStateExamSubject;
import ru.tandemservice.unienr14.settings.bo.EnrCampaignDiscipline.EnrCampaignDisciplineManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Created with IntelliJ IDEA.
 * User: amakarova
 * Date: 17.04.13
 */
public class EnrCampaignDisciplineListUI extends UIPresenter {

    public static final String PARAM_ENR_CAMPAIGN_DS = "enrCampaignDS";
    private Map<Long, EnrStateExamSubject> _valueMap;
    private List<IEntity> _selected = new ArrayList<>();
    private boolean _isHasChanged = false;

    @Override
    public void onComponentRefresh()
    {
        getSettings().set(PARAM_ENR_CAMPAIGN_DS, EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
    }

    public EnrEnrollmentCampaign getCampaign() {
        return EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign();
    }

    public void onEnrCampaignRefresh()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getSettings().<EnrEnrollmentCampaign>get(PARAM_ENR_CAMPAIGN_DS));
        getSelectedEntities().clear();
        _selected.clear();
        _isHasChanged = false;
    }

    private EnrDiscipline getCurrentDiscipline() {
        return ((EnrDiscipline) getDataSource().getCurrent());
    }

    private IUIDataSource getDataSource()
    {
        return _uiConfig.getDataSource(EnrCampaignDisciplineList.CAMP_DISCIPLINE_LIST_DS);
    }

    public boolean isNothingSelected()
    {
        return getSettings().get(PARAM_ENR_CAMPAIGN_DS) == null;
    }

    private Collection<IEntity> getSelectedEntities()
    {
        return ((CheckboxColumn) ((PageableSearchListDataSource) getDataSource()).getLegacyDataSource().getColumn("used")).getSelectedObjects();
    }

    public void onChangeSubject()
    {
        _isHasChanged = true;
    }

    public boolean isDisciplineNotUsed()
    {
        return !getSelectedEntities().contains(getCurrentDiscipline());
    }

    public void onClickApply()
    {
        Set<Long> enrCampaignDisciplines = new HashSet<>();
        for (IEntity entity : _selected)
        {
            EnrDiscipline discipline = (EnrDiscipline) entity;
            EnrStateExamSubject examSubject = _valueMap.get(discipline.getId());
            Long id = EnrCampaignDisciplineManager.instance().dao().saveOrUpdateCampDiscipline(discipline, getCampaign(), examSubject);
            enrCampaignDisciplines.add(id);
        }
        for (EnrCampaignDiscipline campDiscipline: getCampaignDisciplines(getCampaign())) {
            if ((enrCampaignDisciplines.size() > 0 && !enrCampaignDisciplines.contains(campDiscipline.getId()))
                    || enrCampaignDisciplines.size() == 0)
                DataAccessServices.dao().delete(campDiscipline);
        }
        refreshDSMap();
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onAfterDataSourceFetch(IUIDataSource dataSource)
    {
        if (EnrCampaignDisciplineList.CAMP_DISCIPLINE_LIST_DS.equals(dataSource.getName()) && dataSource.hasRecords())
        {
            if (!_isHasChanged) {
                refreshDSMap();
            } else {
                _selected.clear();
                _selected.addAll(getSelectedEntities());
            }
            DynamicListDataSource ds = ((PageableSearchListDataSource) dataSource).getLegacyDataSource();
            CheckboxColumn checkboxColumn = ((CheckboxColumn) ds.getColumn("used"));
            if (checkboxColumn != null) checkboxColumn.setSelectedObjects(_selected);
            BlockColumn<EnrStateExamSubject> blockColumn = (BlockColumn<EnrStateExamSubject>) ds.getColumn("stateExamSubject");
            blockColumn.setValueMap(_valueMap);
        }
    }

    private void refreshDSMap()
    {
        List<EnrCampaignDiscipline> disciplineList = getCampaignDisciplines(getCampaign());
        _valueMap = new HashMap<>();
        for (EnrCampaignDiscipline item : disciplineList) {
            EnrDiscipline discipline = item.getDiscipline();
            _valueMap.put(discipline.getId(), item.getStateExamSubject());
            if (!_selected.contains(discipline))
                _selected.add(discipline);
        }
     }

    /**
     * Возвращает список дисциплин для данной приемной кампании
     *
     * @return список дисциплин
     */
    public List<EnrCampaignDiscipline> getCampaignDisciplines(EnrEnrollmentCampaign enrollmentCampaign)
    {
        DQLSelectBuilder selectBuilder = new DQLSelectBuilder().fromEntity(EnrCampaignDiscipline.class, "disc")
                .where(eq(property(EnrCampaignDiscipline.enrollmentCampaign().fromAlias("disc")), value(enrollmentCampaign)));
        return _uiSupport.createStatement(selectBuilder).list();
    }

    public Map<Long, EnrStateExamSubject> getValueMap() {
        return _valueMap;
    }

    public void setValueMap(Map<Long, EnrStateExamSubject> _valueMap) {
        this._valueMap = _valueMap;
    }
}