package ru.tandemservice.unienr14.programAlloc.entity;

import org.tandemframework.core.common.ITitled;
import ru.tandemservice.unienr14.programAlloc.entity.gen.*;

/**
 * Распределение по ОП
 */
public class EnrProgramAllocation extends EnrProgramAllocationGen implements ITitled
{
    @Override
    public String getTitle()
    {
        if (getProgramSetOrgUnit() == null) {
            return this.getClass().getSimpleName();
        }
        return getProgramSetOrgUnit().getTitle();
    }
}