/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsEduProfileAdd.logic;

import org.tandemframework.rtf.RtfRowIntercepterRawText;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsEduProfileAdd.EnrReportEntrantsEduProfileAddUI;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportStageSelector;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportUtil;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.FilterParametersPrinter;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder;
import ru.tandemservice.unienr14.report.entity.EnrReportEntrantsEduProfile;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.util.Date;
import java.util.HashSet;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 20.06.2014
 */
public class EnrReportEntrantsEduProfileDao extends UniBaseDao implements IEnrReportEntrantsEduProfileDao
{
    @Override
    public long createReport(EnrReportEntrantsEduProfileAddUI model) {

        EnrReportEntrantsEduProfile report = new EnrReportEntrantsEduProfile();
        report.setFormingDate(new Date());

        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateSelector().getDateFrom());
        report.setDateTo(model.getDateSelector().getDateTo());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, EnrReportEntrantsEduProfile.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, EnrReportEntrantsEduProfile.P_PROGRAM_FORM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPETITION_TYPE, EnrReportEntrantsEduProfile.P_COMPETITION_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, EnrReportEntrantsEduProfile.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, EnrReportEntrantsEduProfile.P_FORMATIVE_ORG_UNIT, "fullTitle");
        if (model.getParallelSelector().isParallelActive())
            report.setParallel(model.getParallelSelector().getParallel().getTitle());

        DatabaseFile content = new DatabaseFile();

        content.setContent(buildReport(model));
        content.setFilename("EnrReportEntrantsEduProfile.rtf");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        save(content);

        report.setContent(content);
        save(report);

        return report.getId();
    }

    private byte[] buildReport(EnrReportEntrantsEduProfileAddUI model)
    {
        //rtf
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(EnrScriptItem.class, EnrScriptItemCodes.REPORT_ENTRANTS_EDU_PROFILE);
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();



        EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder()
                .notArchiveOnly()
                .filter(model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo())
                .filter(model.getEnrollmentCampaign());
        filterAddon.applyFilters(requestedCompDQL, requestedCompDQL.competition());

        EnrReportStageSelector.docAcceptanceStageFilter().applyFilter(requestedCompDQL, "reqComp");

        // Убираем отозванные заявления
        requestedCompDQL
                .where(eq(property("request", EnrEntrantRequest.takeAwayDocument()), value(Boolean.FALSE)))
                .column(property("reqComp"));
        if (model.getParallelSelector().isParallelActive()) {
            if (model.getParallelSelector().isParallelOnly())
                requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.TRUE)));
            if (model.getParallelSelector().isSkipParallel())
                requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.FALSE)));
        }
        List<EnrRequestedCompetition> requestedCompetitionList = getList(requestedCompDQL);

        requestedCompDQL
                .where(eq(property("reqComp", EnrRequestedCompetition.profileEducation()), value(Boolean.TRUE)));
        List<EnrRequestedCompetition> requestedCompetitionsWithProfile = getList(requestedCompDQL);


        // Подсчет результатов для таблицы
        HashSet<EnrEntrantRequest> requestsBudget = new HashSet<>();
        HashSet<EnrEntrantRequest> requestsBudgetWP = new HashSet<>();
        HashSet<EnrEntrantRequest> requestsCommercial = new HashSet<>();
        HashSet<EnrEntrantRequest> requestsCommercialWP = new HashSet<>();
        HashSet<EnrEntrant> entrantsBudgetCompletedComp = new HashSet<>();
        HashSet<EnrEntrant> entrantsBudgetCompletedCompWP = new HashSet<>();
        HashSet<EnrEntrant> entrantsBudgetAccepted = new HashSet<>();
        HashSet<EnrEntrant> entrantsBudgetAcceptedWP = new HashSet<>();
        HashSet<EnrEntrant> entrantsCommercialCompletedComp = new HashSet<>();
        HashSet<EnrEntrant> entrantsCommercialCompletedCompWP = new HashSet<>();
        HashSet<EnrEntrant> entrantsCommercialAccepted = new HashSet<>();
        HashSet<EnrEntrant> entrantsCommercialAcceptedWP = new HashSet<>();

        // Общие результаты
        for (EnrRequestedCompetition competition : requestedCompetitionList)
        {
            if (competition.getCompetition().getType().getCompensationType().isBudget())
            {
                requestsBudget.add(competition.getRequest());
                if (EnrReportStageSelector.examPassedCheckByPriority(competition.getState().getPriority()))
                    entrantsBudgetCompletedComp.add(competition.getRequest().getEntrant());
                if (EnrReportStageSelector.enrollmentResultsCheckByPriority(competition.getState().getPriority()))
                    entrantsBudgetAccepted.add(competition.getRequest().getEntrant());
            }
            else
            {
                requestsCommercial.add(competition.getRequest());
                if (EnrReportStageSelector.examPassedCheckByPriority(competition.getState().getPriority()))
                    entrantsCommercialCompletedComp.add(competition.getRequest().getEntrant());
                if (EnrReportStageSelector.enrollmentResultsCheckByPriority(competition.getState().getPriority()))
                    entrantsCommercialAccepted.add(competition.getRequest().getEntrant());
            }
        }

        // Для тех, у кого было профилированное образование
        for (EnrRequestedCompetition competition : requestedCompetitionsWithProfile)
        {
            if (competition.getCompetition().getType().getCompensationType().isBudget())
            {
                requestsBudgetWP.add(competition.getRequest());
                if (EnrReportStageSelector.examPassedCheckByPriority(competition.getState().getPriority()))
                    entrantsBudgetCompletedCompWP.add(competition.getRequest().getEntrant());
                if (EnrReportStageSelector.enrollmentResultsCheckByPriority(competition.getState().getPriority()))
                    entrantsBudgetAcceptedWP.add(competition.getRequest().getEntrant());
            }
            else
            {
                requestsCommercialWP.add(competition.getRequest());
                if (EnrReportStageSelector.examPassedCheckByPriority(competition.getState().getPriority()))
                    entrantsCommercialCompletedCompWP.add(competition.getRequest().getEntrant());
                if (EnrReportStageSelector.enrollmentResultsCheckByPriority(competition.getState().getPriority()))
                    entrantsCommercialAcceptedWP.add(competition.getRequest().getEntrant());
            }
        }

        // Таблица, первая и вторая строки.
        String[][] t1Table = {
                {
                        String.valueOf(requestsBudget.size()),
                        String.valueOf(requestsCommercial.size()),
                        String.valueOf(requestsBudgetWP.size()),
                        String.valueOf(requestsCommercialWP.size()),
                        String.valueOf(entrantsBudgetCompletedComp.size()),
                        String.valueOf(entrantsCommercialCompletedComp.size()),
                        String.valueOf(entrantsBudgetCompletedCompWP.size()),
                        String.valueOf(entrantsCommercialCompletedCompWP.size()),
                        String.valueOf(entrantsBudgetAccepted.size()),
                        String.valueOf(entrantsCommercialAccepted.size()),
                        String.valueOf(entrantsBudgetAcceptedWP.size()),
                        String.valueOf(entrantsCommercialAcceptedWP.size())
                }
        };
        String[][] t12Table ={
                {
                        "в %",
                        String.valueOf(percentage(requestsBudgetWP.size(), requestsBudget.size())),
                        String.valueOf(percentage(requestsCommercialWP.size(), requestsCommercial.size())),
                        "",
                        String.valueOf(percentage(entrantsBudgetCompletedCompWP.size(), entrantsBudgetCompletedComp.size())),
                        String.valueOf(percentage(entrantsCommercialCompletedCompWP.size(), entrantsCommercialCompletedComp.size())),
                        "",
                        String.valueOf(percentage(entrantsBudgetAcceptedWP.size(), entrantsBudgetAccepted.size())),
                        String.valueOf(percentage(entrantsCommercialAcceptedWP.size(), entrantsCommercialAccepted.size()))
                }
        };


        // H - таблица
        List<String[]> hTable = new FilterParametersPrinter().getTable(filterAddon, model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo());
        if (model.getParallelSelector().isParallelActive())
            hTable.add(new String [] {"Поступающие параллельно",model.getParallelSelector().getParallel().getTitle()});

        // Запись в документ
        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("T1", t1Table);
        tableModifier.put("T1.2", t12Table);
        tableModifier.put("H", hTable.toArray(new String[hTable.size()][]));
        tableModifier.put("H", new RtfRowIntercepterRawText());
        tableModifier.put("T2", new RtfRowIntercepterRawText());
        tableModifier.modify(document);

        return RtfUtil.toByteArray(document);
    }


    // возвращает какой процент составляет a от b.
    private int percentage(int a, int b)
    {
        return Math.round(100*(float)a/(float)b);
    }

}
