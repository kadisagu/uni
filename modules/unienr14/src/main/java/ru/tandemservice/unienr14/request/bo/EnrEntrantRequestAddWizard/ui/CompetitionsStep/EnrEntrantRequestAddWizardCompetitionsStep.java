/* $Id$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.CompetitionsStep;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unienr14.catalog.bo.EnrCatalogCommons.EnrCatalogCommonsManager;
import ru.tandemservice.unienr14.catalog.bo.EnrRequestType.EnrRequestTypeManager;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrantBaseDocument.EnrEntrantBaseDocumentManager;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.EnrEntrantRequestManager;
import ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitProofDocument;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.EnrEnrollmentCommissionManager;
import ru.tandemservice.unienr14.settings.bo.EnrTargetAdmission.EnrTargetAdmissionManager;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

/**
 * @author nvankov
 * @since 6/5/14
 */
@Configuration
public class EnrEntrantRequestAddWizardCompetitionsStep extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
        .addDataSource(EnrRequestTypeManager.instance().requestTypeEnrCampaignDSConfig())
        .addDataSource(EnrEnrollmentCommissionManager.instance().enrollmentCommissionDSConfig())

        .addDataSource(EnrEntrantBaseDocumentManager.instance().identityCardDSConfig())
        .addDataSource(EnrEntrantBaseDocumentManager.instance().eduDocumentDSConfig())
        .addDataSource(EnrEntrantRequestManager.instance().originalSubmissionAndReturnWayDSConfig())

        /* Выбор конкурсов */
        .addDataSource(selectDS(EnrEntrantRequestManager.DS_FORM, EnrEntrantRequestManager.instance().eduProgramFormDSHandler()))
        .addDataSource(selectDS(EnrEntrantRequestManager.DS_ORG_UNIT, EnrEntrantRequestManager.instance().orgUnitDSHandler()).addColumn(EnrOrgUnit.institutionOrgUnit().orgUnit().shortTitleWithTopEmphasized().s()))
        .addDataSource(selectDS(EnrEntrantRequestManager.DS_FORMATIVE_ORG_UNIT, EnrEntrantRequestManager.instance().formativeOrgUnitDSHandler()).addColumn(OrgUnit.fullTitle().s()))
        .addDataSource(selectDS(EnrEntrantRequestManager.DS_SUBJECT, EnrEntrantRequestManager.instance().subjectDSHandler()).addColumn(EduProgramSubject.titleWithCode().s()))
        .addDataSource(selectDS(EnrEntrantRequestManager.DS_COMPETITION_TYPE, EnrEntrantRequestManager.instance().competitionTypeDSHandler()))
        .addDataSource(selectDS(EnrEntrantRequestManager.DS_EDU_LEVEL_REQUIREMENT, EnrEntrantRequestManager.instance().eduLevelRequirementDSHandler()))
        /* Выбор конкурсов */

        .addDataSource(EnrTargetAdmissionManager.instance().enrCampaignTaKindsSelectDSConfig())
        .addDataSource(selectDS("targetAdmissionOrganizationDS", EnrEntrantRequestManager.instance().targetAdmissionOrgUnitSelectDSHandler()))
        .addDataSource(selectDS(EnrEntrantRequestManager.DS_BENEFIT_CATEGORY_PREFERENCE, EnrCatalogCommonsManager.instance().benefitCategoryDSHandler()))
        .addDataSource(selectDS(EnrEntrantRequestManager.DS_BENEFIT_CATEGORY_EXCLUSIVE, EnrCatalogCommonsManager.instance().benefitCategoryDSHandler()))
        .addDataSource(selectDS(EnrEntrantRequestManager.DS_BENEFIT_CATEGORY_NO_EXAM, EnrCatalogCommonsManager.instance().benefitCategoryDSHandler()))
        .addDataSource(selectDS(EnrEntrantRequestManager.DS_BENEFIT_PROOF_DOC_PREFERENCE, EnrEntrantRequestManager.instance().benefitProofDocDSHandler()).addColumn(IEnrEntrantBenefitProofDocument.DISPLAYABLE_TITLE))
        .addDataSource(selectDS(EnrEntrantRequestManager.DS_BENEFIT_PROOF_DOC_EXCLUSIVE, EnrEntrantRequestManager.instance().benefitProofDocDSHandler()).addColumn(IEnrEntrantBenefitProofDocument.DISPLAYABLE_TITLE))
        .addDataSource(selectDS(EnrEntrantRequestManager.DS_BENEFIT_PROOF_DOC_NO_EXAM, EnrEntrantRequestManager.instance().benefitProofDocDSHandler()).addColumn(IEnrEntrantBenefitProofDocument.DISPLAYABLE_TITLE))
        .addDataSource(selectDS("programSetItemDS", EnrEntrantRequestManager.instance().programSetItemDSHandler()).addColumn(EnrProgramSetItem.program().title().s()))
        .addDataSource(EnrEntrantRequestManager.instance().externalOrgUnitDSConfig())
        .create();
    }
}



