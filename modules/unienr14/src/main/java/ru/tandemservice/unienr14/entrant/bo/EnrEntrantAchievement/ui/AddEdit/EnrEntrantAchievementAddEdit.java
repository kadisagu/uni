/* $Id$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrantAchievement.ui.AddEdit;

import com.google.common.collect.Lists;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import org.tandemframework.shared.person.catalog.entity.codes.EduDocumentKindCodes;
import org.tandemframework.shared.person.catalog.entity.codes.GraduationHonourCodes;
import ru.tandemservice.unienr14.catalog.bo.EnrRequestType.EnrRequestTypeManager;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrantAchievementProofDocument.EnrEntrantAchievementProofDocumentManager;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrantBaseDocument.util.EnrEntrantDocumentViewWrapper;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantBaseDocument;
import ru.tandemservice.unienr14.entrant.entity.IEnrEntrantAchievementProofDocument;
import ru.tandemservice.unienr14.entrant.entity.gen.IEnrEntrantAchievementProofDocumentGen;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.EnrEntrantRequestManager;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitProofDocument;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignEntrantDocument;
import ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementType;

import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 4/11/14
 */
@Configuration
public class EnrEntrantAchievementAddEdit extends BusinessComponentManager
{
    public static final String ACHIEVEMENT_DS = "achievementDS";
    public static final String DOCUMENT_DS = "documentDS";
    public static final String DS_ENTRANT_REQUEST = EnrEntrantRequestManager.DS_ENTRANT_REQUEST;
    public static final String BIND_ENTRANT_ID = EnrEntrantManager.BIND_ENTRANT_ID;
    public static final String BIND_REQUEST_TYPE_ID = EnrEntrantRequestManager.BIND_REQUEST_TYPE_ID;

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(EnrRequestTypeManager.instance().requestTypeEnrCampaignDSConfig())
                .addDataSource(selectDS(ACHIEVEMENT_DS, achievementDS()).addColumn(EnrEntrantAchievementType.achievementKind().title().s()))
                .addDataSource(selectDS(DOCUMENT_DS, EnrEntrantAchievementProofDocumentManager.instance().achievementProofDocumentDSHandler()).addColumn(IEnrEntrantBenefitProofDocument.DISPLAYABLE_TITLE))
                .addDataSource(EnrEntrantRequestManager.instance().entrantRequestDSConfig())
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler requestTypeDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrRequestType.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(exists(EnrEntrantAchievementType.class,
                                 EnrEntrantAchievementType.achievementKind().requestType().s(), property(alias),
                                 EnrEntrantAchievementType.enrollmentCampaign().s(), context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN)
                ));
            }
        }
                .filter(EnrRequestType.title())
                .where(EnrRequestType.enabled(), true)
                .order(EnrRequestType.priority());
    }

    @Bean
    public IDefaultComboDataSourceHandler requestDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrEntrantRequest.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(exists(EnrEntrantAchievementType.class,
                        EnrEntrantAchievementType.achievementKind().requestType().s(), property(alias),
                        EnrEntrantAchievementType.enrollmentCampaign().s(), context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN)
                ));
            }
        }
                .filter(EnrRequestType.title())
                .where(EnrRequestType.enabled(), true)
                .order(EnrRequestType.priority());
    }

    @Bean
    public IDefaultComboDataSourceHandler achievementDS()
    {
        return new EntityComboDataSourceHandler(getName(), EnrEntrantAchievementType.class)
                .filter(EnrEntrantAchievementType.achievementKind().title())
                .where(EnrEntrantAchievementType.enrollmentCampaign(), EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN)
                .where(EnrEntrantAchievementType.achievementKind().requestType().id(), BIND_REQUEST_TYPE_ID);
    }
}



    