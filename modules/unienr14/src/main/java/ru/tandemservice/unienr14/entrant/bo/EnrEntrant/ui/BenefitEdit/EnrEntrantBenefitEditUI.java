/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.BenefitEdit;

import java.util.Collections;
import java.util.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.entity.EntityHolder;

import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.catalog.bo.EnrCatalogCommons.EnrCatalogCommonsManager;
import ru.tandemservice.unienr14.catalog.entity.EnrBenefitCategory;
import ru.tandemservice.unienr14.catalog.entity.EnrBenefitType;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceBenefit;
import ru.tandemservice.unienr14.request.entity.EnrEntrantBenefitProof;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionNoExams;
import ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitProofDocument;
import ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitStatement;

/**
 * @author oleyba
 * @since 2/26/14
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "entityHolder.id")
})
public class EnrEntrantBenefitEditUI extends UIPresenter
{
    private EntityHolder<IEnrEntrantBenefitStatement> entityHolder = new EntityHolder<>();

    private EnrBenefitType benefitType;
    private EnrBenefitCategory benefitCategory;
    private List<IEnrEntrantBenefitProofDocument> documentList;

    private EnrProgramSetItem programSetItem;

    @Override
    public void onComponentRefresh()
    {
        getEntityHolder().refresh();
        if (getBenefitType() == null) {
            setBenefitType(getStatement().getBenefitCategory().getBenefitType());
            setBenefitCategory(getStatement().getBenefitCategory());
            setDocumentList(IUniBaseDao.instance.get().<EnrEntrantBenefitProof, IEnrEntrantBenefitStatement, IEnrEntrantBenefitProofDocument>getPropertiesList(
                EnrEntrantBenefitProof.class,
                EnrEntrantBenefitProof.benefitStatement(),
                getStatement(),
                false,
                EnrEntrantBenefitProof.document()
            ));

            if (isCompetitionNoExam()) {
                setProgramSetItem(((EnrRequestedCompetitionNoExams)getStatement()).getProgramSetItem());
            }
        }
    }

    public void onClickApply() {
        if (isCompetitionNoExam()) {
            EnrEntrantManager.instance().dao().doSaveBenefitNoExamsData(((EnrRequestedCompetitionNoExams)getStatement()), getProgramSetItem(), getBenefitCategory(), getDocumentList());
        } else if (isMarkSourceBenefit()) {
            EnrEntrantManager.instance().dao().doSaveBenefitMarkSourceData(((EnrEntrantMarkSourceBenefit)getStatement()), getBenefitCategory(), getFirstDocument());
        } else {
            EnrEntrantManager.instance().dao().doSaveBenefitData(getStatement(), getBenefitCategory(), getDocumentList());
        }
        deactivate();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrCatalogCommonsManager.PARAM_BENEFIT_TYPE_CODE, getBenefitType().getCode());
        dataSource.put(EnrEntrantManager.BIND_REQUESTED_COMPETITION, isSelectProgram() ? ((EnrRequestedCompetitionNoExams)getStatement()) : null);
        dataSource.put(EnrEntrantManager.BIND_ENTRANT, getStatement().getEntrant());
    }

    // presenter

    public boolean isCompetitionNoExam() {
        return getStatement() instanceof EnrRequestedCompetitionNoExams;
    }

    public boolean isMarkSourceBenefit() {
        return getStatement() instanceof EnrEntrantMarkSourceBenefit;
    }

    public boolean isSelectProgram() {
        return isCompetitionNoExam();
    }

    public boolean isSingleProof() {
        return isMarkSourceBenefit();
    }

    public IEnrEntrantBenefitProofDocument getFirstDocument() {
        final List<IEnrEntrantBenefitProofDocument> list = getDocumentList();
        return ((null == list || list.isEmpty()) ? null : list.get(0));
    }

    public void setFirstDocument(IEnrEntrantBenefitProofDocument document) {
        setDocumentList(Collections.singletonList(document));
    }


    // getters and setters

    public IEnrEntrantBenefitStatement getStatement() {
        return getEntityHolder().getValue();
    }

    public EntityHolder<IEnrEntrantBenefitStatement> getEntityHolder()
    {
        return entityHolder;
    }

    public EnrBenefitCategory getBenefitCategory()
    {
        return benefitCategory;
    }

    public void setBenefitCategory(EnrBenefitCategory benefitCategory)
    {
        this.benefitCategory = benefitCategory;
    }

    public EnrBenefitType getBenefitType()
    {
        return benefitType;
    }

    public void setBenefitType(EnrBenefitType benefitType)
    {
        this.benefitType = benefitType;
    }

    public List<IEnrEntrantBenefitProofDocument> getDocumentList()
    {
        return documentList;
    }

    public void setDocumentList(List<IEnrEntrantBenefitProofDocument> documentList)
    {
        this.documentList = documentList;
    }

    public void setEntityHolder(EntityHolder<IEnrEntrantBenefitStatement> entityHolder)
    {
        this.entityHolder = entityHolder;
    }

    public EnrProgramSetItem getProgramSetItem()
    {
        return programSetItem;
    }

    public void setProgramSetItem(EnrProgramSetItem programSetItem)
    {
        this.programSetItem = programSetItem;
    }
}