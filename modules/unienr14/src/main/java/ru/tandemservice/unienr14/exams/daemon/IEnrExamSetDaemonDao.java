package ru.tandemservice.unienr14.exams.daemon;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;

/**
 * @author vdanilov
 */
public interface IEnrExamSetDaemonDao {

    final SpringBeanCache<IEnrExamSetDaemonDao> instance = new SpringBeanCache<IEnrExamSetDaemonDao>(IEnrExamSetDaemonDao.class.getName());

    /**
     * Обновляет перечень дисциплин набора ВИ (по данным элементов набора ВИ)
     * @return true, если что-то было изменено
     */
    @Transactional(propagation=Propagation.REQUIRED)
    boolean doUpdateExamSetDisciplines();

}
