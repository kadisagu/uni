/* $Id:$ */
package ru.tandemservice.unienr14.competition.bo.EnrCompetition;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

import ru.tandemservice.unienr14.competition.bo.EnrCompetition.logic.EnrCompetitionDao;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.logic.EnrCompetitionStateExamFilterDao;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.logic.IEnrCompetitionDao;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.logic.IEnrCompetitionStateExamFilterDao;

/**
 * @author oleyba
 * @since 2/3/14
 */
@Configuration
public class EnrCompetitionManager extends BusinessObjectManager
{
    public static EnrCompetitionManager instance()
    {
        return instance(EnrCompetitionManager.class);
    }

    @Bean
    public IEnrCompetitionDao dao()
    {
        return new EnrCompetitionDao();
    }

    @Bean
    public IEnrCompetitionStateExamFilterDao stateExamFilterDao()
    {
        return new EnrCompetitionStateExamFilterDao();
    }

}
