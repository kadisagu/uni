package ru.tandemservice.unienr14.request.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignTargetAdmissionKind;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выбранный конкурс по ЦП
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrRequestedCompetitionTAGen extends EnrRequestedCompetition
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA";
    public static final String ENTITY_NAME = "enrRequestedCompetitionTA";
    public static final int VERSION_HASH = -1973675558;
    private static IEntityMeta ENTITY_META;

    public static final String L_TARGET_ADMISSION_KIND = "targetAdmissionKind";
    public static final String L_TARGET_ADMISSION_ORG_UNIT = "targetAdmissionOrgUnit";
    public static final String P_CONTRACT_NUMBER = "contractNumber";
    public static final String P_CONTRACT_DATE = "contractDate";

    private EnrCampaignTargetAdmissionKind _targetAdmissionKind;     // Вид ЦП
    private ExternalOrgUnit _targetAdmissionOrgUnit;     // Организация (ЦП)
    private String _contractNumber;     // Номер договора
    private Date _contractDate;     // Дата заключения договора

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Вид ЦП. Свойство не может быть null.
     */
    @NotNull
    public EnrCampaignTargetAdmissionKind getTargetAdmissionKind()
    {
        return _targetAdmissionKind;
    }

    /**
     * @param targetAdmissionKind Вид ЦП. Свойство не может быть null.
     */
    public void setTargetAdmissionKind(EnrCampaignTargetAdmissionKind targetAdmissionKind)
    {
        dirty(_targetAdmissionKind, targetAdmissionKind);
        _targetAdmissionKind = targetAdmissionKind;
    }

    /**
     * @return Организация (ЦП). Свойство не может быть null.
     */
    @NotNull
    public ExternalOrgUnit getTargetAdmissionOrgUnit()
    {
        return _targetAdmissionOrgUnit;
    }

    /**
     * @param targetAdmissionOrgUnit Организация (ЦП). Свойство не может быть null.
     */
    public void setTargetAdmissionOrgUnit(ExternalOrgUnit targetAdmissionOrgUnit)
    {
        dirty(_targetAdmissionOrgUnit, targetAdmissionOrgUnit);
        _targetAdmissionOrgUnit = targetAdmissionOrgUnit;
    }

    /**
     * @return Номер договора.
     */
    @Length(max=255)
    public String getContractNumber()
    {
        return _contractNumber;
    }

    /**
     * @param contractNumber Номер договора.
     */
    public void setContractNumber(String contractNumber)
    {
        dirty(_contractNumber, contractNumber);
        _contractNumber = contractNumber;
    }

    /**
     * @return Дата заключения договора.
     */
    public Date getContractDate()
    {
        return _contractDate;
    }

    /**
     * @param contractDate Дата заключения договора.
     */
    public void setContractDate(Date contractDate)
    {
        dirty(_contractDate, contractDate);
        _contractDate = contractDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EnrRequestedCompetitionTAGen)
        {
            setTargetAdmissionKind(((EnrRequestedCompetitionTA)another).getTargetAdmissionKind());
            setTargetAdmissionOrgUnit(((EnrRequestedCompetitionTA)another).getTargetAdmissionOrgUnit());
            setContractNumber(((EnrRequestedCompetitionTA)another).getContractNumber());
            setContractDate(((EnrRequestedCompetitionTA)another).getContractDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrRequestedCompetitionTAGen> extends EnrRequestedCompetition.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrRequestedCompetitionTA.class;
        }

        public T newInstance()
        {
            return (T) new EnrRequestedCompetitionTA();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "targetAdmissionKind":
                    return obj.getTargetAdmissionKind();
                case "targetAdmissionOrgUnit":
                    return obj.getTargetAdmissionOrgUnit();
                case "contractNumber":
                    return obj.getContractNumber();
                case "contractDate":
                    return obj.getContractDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "targetAdmissionKind":
                    obj.setTargetAdmissionKind((EnrCampaignTargetAdmissionKind) value);
                    return;
                case "targetAdmissionOrgUnit":
                    obj.setTargetAdmissionOrgUnit((ExternalOrgUnit) value);
                    return;
                case "contractNumber":
                    obj.setContractNumber((String) value);
                    return;
                case "contractDate":
                    obj.setContractDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "targetAdmissionKind":
                        return true;
                case "targetAdmissionOrgUnit":
                        return true;
                case "contractNumber":
                        return true;
                case "contractDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "targetAdmissionKind":
                    return true;
                case "targetAdmissionOrgUnit":
                    return true;
                case "contractNumber":
                    return true;
                case "contractDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "targetAdmissionKind":
                    return EnrCampaignTargetAdmissionKind.class;
                case "targetAdmissionOrgUnit":
                    return ExternalOrgUnit.class;
                case "contractNumber":
                    return String.class;
                case "contractDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrRequestedCompetitionTA> _dslPath = new Path<EnrRequestedCompetitionTA>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrRequestedCompetitionTA");
    }
            

    /**
     * @return Вид ЦП. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA#getTargetAdmissionKind()
     */
    public static EnrCampaignTargetAdmissionKind.Path<EnrCampaignTargetAdmissionKind> targetAdmissionKind()
    {
        return _dslPath.targetAdmissionKind();
    }

    /**
     * @return Организация (ЦП). Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA#getTargetAdmissionOrgUnit()
     */
    public static ExternalOrgUnit.Path<ExternalOrgUnit> targetAdmissionOrgUnit()
    {
        return _dslPath.targetAdmissionOrgUnit();
    }

    /**
     * @return Номер договора.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA#getContractNumber()
     */
    public static PropertyPath<String> contractNumber()
    {
        return _dslPath.contractNumber();
    }

    /**
     * @return Дата заключения договора.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA#getContractDate()
     */
    public static PropertyPath<Date> contractDate()
    {
        return _dslPath.contractDate();
    }

    public static class Path<E extends EnrRequestedCompetitionTA> extends EnrRequestedCompetition.Path<E>
    {
        private EnrCampaignTargetAdmissionKind.Path<EnrCampaignTargetAdmissionKind> _targetAdmissionKind;
        private ExternalOrgUnit.Path<ExternalOrgUnit> _targetAdmissionOrgUnit;
        private PropertyPath<String> _contractNumber;
        private PropertyPath<Date> _contractDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Вид ЦП. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA#getTargetAdmissionKind()
     */
        public EnrCampaignTargetAdmissionKind.Path<EnrCampaignTargetAdmissionKind> targetAdmissionKind()
        {
            if(_targetAdmissionKind == null )
                _targetAdmissionKind = new EnrCampaignTargetAdmissionKind.Path<EnrCampaignTargetAdmissionKind>(L_TARGET_ADMISSION_KIND, this);
            return _targetAdmissionKind;
        }

    /**
     * @return Организация (ЦП). Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA#getTargetAdmissionOrgUnit()
     */
        public ExternalOrgUnit.Path<ExternalOrgUnit> targetAdmissionOrgUnit()
        {
            if(_targetAdmissionOrgUnit == null )
                _targetAdmissionOrgUnit = new ExternalOrgUnit.Path<ExternalOrgUnit>(L_TARGET_ADMISSION_ORG_UNIT, this);
            return _targetAdmissionOrgUnit;
        }

    /**
     * @return Номер договора.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA#getContractNumber()
     */
        public PropertyPath<String> contractNumber()
        {
            if(_contractNumber == null )
                _contractNumber = new PropertyPath<String>(EnrRequestedCompetitionTAGen.P_CONTRACT_NUMBER, this);
            return _contractNumber;
        }

    /**
     * @return Дата заключения договора.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA#getContractDate()
     */
        public PropertyPath<Date> contractDate()
        {
            if(_contractDate == null )
                _contractDate = new PropertyPath<Date>(EnrRequestedCompetitionTAGen.P_CONTRACT_DATE, this);
            return _contractDate;
        }

        public Class getEntityClass()
        {
            return EnrRequestedCompetitionTA.class;
        }

        public String getEntityName()
        {
            return "enrRequestedCompetitionTA";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
