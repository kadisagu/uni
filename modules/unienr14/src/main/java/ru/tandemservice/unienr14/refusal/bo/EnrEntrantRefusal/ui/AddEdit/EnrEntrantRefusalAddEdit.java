/* $Id:$ */
package ru.tandemservice.unienr14.refusal.bo.EnrEntrantRefusal.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantRefusalReason;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.logic.EnrOrgUnitBaseDSHandler;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.util.Collections;
import java.util.List;

/**
 * @author rsizonenko
 * @since 20.05.2014.
 */

@Configuration
public class EnrEntrantRefusalAddEdit extends BusinessComponentManager
{
    public static final String ENR_ENROLLMENT_CAMPAIGN_DS = "enrEnrollmentCampaignDS";
    public static final String ENR_ORG_UNIT_DS = "enrOrgUnitDS";
    public static final String ENR_ENTRANT_REFUSAL_REASON = "enrEntrantRefusalReason";
    public static final String ENR_ENROLLMENT_CAMPAIGN = "enrEnrollmentCampaign";
    public static final String ENR_ORG_UNIT = "enrOrgUnit";


    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ENR_ENROLLMENT_CAMPAIGN_DS, EnrEnrollmentCampaignManager.instance().enrCampaignDSHandler()))
                .addDataSource(selectDS(ENR_ORG_UNIT_DS, enrOrgUnitDSHandler())
                        .addColumn(EnrOrgUnit.institutionOrgUnit().orgUnit().shortTitleWithTopEmphasized().s()))
                .addDataSource(selectDS(ENR_ENTRANT_REFUSAL_REASON, enrEntrantRefusalReasonHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler enrOrgUnitDSHandler()
    {
        return new EnrOrgUnitBaseDSHandler(getName())
            .where(EnrOrgUnit.enrollmentCampaign(), ENR_ENROLLMENT_CAMPAIGN)
            .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler enrEntrantRefusalReasonHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrEntrantRefusalReason.class)
                .filter(EnrEntrantRefusalReason.title())
                .order(EnrEntrantRefusalReason.title())
                .pageable(true);
    }
}
