package ru.tandemservice.unienr14.entrant.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.person.catalog.entity.SportRank;
import org.tandemframework.shared.person.catalog.entity.SportType;
import ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantSportAchievement;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Спортивное достижение онлайн-абитуриента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrOnlineEntrantSportAchievementGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantSportAchievement";
    public static final String ENTITY_NAME = "enrOnlineEntrantSportAchievement";
    public static final int VERSION_HASH = -1868854286;
    private static IEntityMeta ENTITY_META;

    public static final String L_TYPE = "type";
    public static final String L_RANK = "rank";
    public static final String L_ONLINE_ENTRANT = "onlineEntrant";

    private SportType _type;     // Вид спорта
    private SportRank _rank;     // Спортивные разряды и звания
    private EnrOnlineEntrant _onlineEntrant;     // Абитуриент

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Вид спорта. Свойство не может быть null.
     */
    @NotNull
    public SportType getType()
    {
        return _type;
    }

    /**
     * @param type Вид спорта. Свойство не может быть null.
     */
    public void setType(SportType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Спортивные разряды и звания. Свойство не может быть null.
     */
    @NotNull
    public SportRank getRank()
    {
        return _rank;
    }

    /**
     * @param rank Спортивные разряды и звания. Свойство не может быть null.
     */
    public void setRank(SportRank rank)
    {
        dirty(_rank, rank);
        _rank = rank;
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     */
    @NotNull
    public EnrOnlineEntrant getOnlineEntrant()
    {
        return _onlineEntrant;
    }

    /**
     * @param onlineEntrant Абитуриент. Свойство не может быть null.
     */
    public void setOnlineEntrant(EnrOnlineEntrant onlineEntrant)
    {
        dirty(_onlineEntrant, onlineEntrant);
        _onlineEntrant = onlineEntrant;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrOnlineEntrantSportAchievementGen)
        {
            setType(((EnrOnlineEntrantSportAchievement)another).getType());
            setRank(((EnrOnlineEntrantSportAchievement)another).getRank());
            setOnlineEntrant(((EnrOnlineEntrantSportAchievement)another).getOnlineEntrant());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrOnlineEntrantSportAchievementGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrOnlineEntrantSportAchievement.class;
        }

        public T newInstance()
        {
            return (T) new EnrOnlineEntrantSportAchievement();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "type":
                    return obj.getType();
                case "rank":
                    return obj.getRank();
                case "onlineEntrant":
                    return obj.getOnlineEntrant();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "type":
                    obj.setType((SportType) value);
                    return;
                case "rank":
                    obj.setRank((SportRank) value);
                    return;
                case "onlineEntrant":
                    obj.setOnlineEntrant((EnrOnlineEntrant) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "type":
                        return true;
                case "rank":
                        return true;
                case "onlineEntrant":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "type":
                    return true;
                case "rank":
                    return true;
                case "onlineEntrant":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "type":
                    return SportType.class;
                case "rank":
                    return SportRank.class;
                case "onlineEntrant":
                    return EnrOnlineEntrant.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrOnlineEntrantSportAchievement> _dslPath = new Path<EnrOnlineEntrantSportAchievement>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrOnlineEntrantSportAchievement");
    }
            

    /**
     * @return Вид спорта. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantSportAchievement#getType()
     */
    public static SportType.Path<SportType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Спортивные разряды и звания. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantSportAchievement#getRank()
     */
    public static SportRank.Path<SportRank> rank()
    {
        return _dslPath.rank();
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantSportAchievement#getOnlineEntrant()
     */
    public static EnrOnlineEntrant.Path<EnrOnlineEntrant> onlineEntrant()
    {
        return _dslPath.onlineEntrant();
    }

    public static class Path<E extends EnrOnlineEntrantSportAchievement> extends EntityPath<E>
    {
        private SportType.Path<SportType> _type;
        private SportRank.Path<SportRank> _rank;
        private EnrOnlineEntrant.Path<EnrOnlineEntrant> _onlineEntrant;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Вид спорта. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantSportAchievement#getType()
     */
        public SportType.Path<SportType> type()
        {
            if(_type == null )
                _type = new SportType.Path<SportType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Спортивные разряды и звания. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantSportAchievement#getRank()
     */
        public SportRank.Path<SportRank> rank()
        {
            if(_rank == null )
                _rank = new SportRank.Path<SportRank>(L_RANK, this);
            return _rank;
        }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantSportAchievement#getOnlineEntrant()
     */
        public EnrOnlineEntrant.Path<EnrOnlineEntrant> onlineEntrant()
        {
            if(_onlineEntrant == null )
                _onlineEntrant = new EnrOnlineEntrant.Path<EnrOnlineEntrant>(L_ONLINE_ENTRANT, this);
            return _onlineEntrant;
        }

        public Class getEntityClass()
        {
            return EnrOnlineEntrantSportAchievement.class;
        }

        public String getEntityName()
        {
            return "enrOnlineEntrantSportAchievement";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
