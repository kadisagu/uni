/* $Id:$ */
package ru.tandemservice.unienr14.catalog.bo.EnrRequestType.logic;

import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 16.06.2016
 */
public class EnrRequestTypeDao extends UniBaseDao implements IEnrRequestTypeDao
{
    public static final String SETTINGS_KEY_PREFIX_DEFAULT_REQUEST_TYPE = "enrDefaultRequestType.";
    public static final String SETTINGS_KEY_DEFAULT_REQUEST_TYPE = "requestType";

    @Override
    public EnrRequestType getDefaultRequestType(EnrEnrollmentCampaign enrollmentCampaign)
    {
        if(enrollmentCampaign == null) return null;

        String settingsOwner = DataSettingsFacade.getSettingsOwner();
        if (null == settingsOwner)
            return null;

        IDataSettings settings = DataSettingsFacade.getSettings(settingsOwner, SETTINGS_KEY_PREFIX_DEFAULT_REQUEST_TYPE);
        Object defaultRequestType = settings.get(SETTINGS_KEY_DEFAULT_REQUEST_TYPE + "_" + enrollmentCampaign.getId());

        List<EnrRequestType> requestTypes = getRequestTypeList(enrollmentCampaign);

        if(requestTypes.isEmpty()) return null;

        if(defaultRequestType == null)
        {
            EnrRequestType requestType = requestTypes.get(0);
            saveDefaultRequestType(requestType, enrollmentCampaign);
            return requestType;
        }

        if(defaultRequestType instanceof EnrRequestType)
        {
            EnrRequestType requestType = (EnrRequestType) defaultRequestType;
            if(!requestTypes.contains(requestType))
            {
                requestType = requestTypes.get(0);
                saveDefaultRequestType(requestType, enrollmentCampaign);
            }

            return requestType;

        }

        return null;
    }

    @Override
    public List<EnrRequestType> getRequestTypeList(EnrEnrollmentCampaign enrollmentCampaign)
    {
        if(enrollmentCampaign == null)
            return null;

        DQLSelectBuilder dql =  new DQLSelectBuilder().fromEntity(EnrRequestType.class, "rt")
                .where(exists(new DQLSelectBuilder().fromEntity(EnrProgramSetBase.class, "psb")
                        .where(eq(property("psb", EnrProgramSetBase.requestType()), property("rt")))
                        .where(eq(property("psb", EnrProgramSetBase.enrollmentCampaign()), value(enrollmentCampaign)))
                        .buildQuery()));

        return dql.createStatement(getSession()).list();
    }

    @Override
    public void saveDefaultRequestType(EnrRequestType requestType, EnrEnrollmentCampaign enrollmentCampaign)
    {
        if(enrollmentCampaign == null) return;

        String settingsOwner = DataSettingsFacade.getSettingsOwner();
        if (null == settingsOwner)
            return;

        IDataSettings settings = DataSettingsFacade.getSettings(settingsOwner, SETTINGS_KEY_PREFIX_DEFAULT_REQUEST_TYPE);
        settings.set(SETTINGS_KEY_DEFAULT_REQUEST_TYPE + "_" + enrollmentCampaign.getId(), requestType);
        DataSettingsFacade.saveSettings(settings);
    }
}
