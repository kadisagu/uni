/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.InformationSourcesAdd.logic;

import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.rtf.RtfRowIntercepterRawText;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.EnrSourceInfoAboutUniversity;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantInfoAboutUniversity;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.InformationSourcesAdd.EnrReportInformationSourcesAddUI;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.FilterParametersPrinter;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder;
import ru.tandemservice.unienr14.report.entity.EnrReportInformationSources;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportUtil;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.text.SimpleDateFormat;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 18.06.2014
 */
public class EnrReportInformationSourcesDao extends UniBaseDao implements IEnrReportInformationSourcesDao
{
    @Override
    public long createReport(EnrReportInformationSourcesAddUI model) {

        EnrReportInformationSources report = new EnrReportInformationSources();

        report.setFormingDate(new Date());

        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateSelector().getDateFrom());
        report.setDateTo(model.getDateSelector().getDateTo());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, EnrReportInformationSources.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE, EnrReportInformationSources.P_COMPENSATION_TYPE, "shortTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, EnrReportInformationSources.P_PROGRAM_FORM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPETITION_TYPE, EnrReportInformationSources.P_COMPETITION_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, EnrReportInformationSources.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, EnrReportInformationSources.P_FORMATIVE_ORG_UNIT, "fullTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, EnrReportInformationSources.P_PROGRAM_SUBJECT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, EnrReportInformationSources.P_EDU_PROGRAM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, EnrReportInformationSources.P_PROGRAM_SET, "title");

        if (model.getParallelSelector().isParallelActive())
            report.setParallel(model.getParallelSelector().getParallel().getTitle());
        report.setStage(model.getStageSelector().getStage().getTitle());

        DatabaseFile content = new DatabaseFile();

        content.setContent(buildReport(model));
        content.setFilename("EnrReportInformationSources.rtf");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        save(content);

        report.setContent(content);
        save(report);

        return report.getId();
    }

    private byte[] buildReport(EnrReportInformationSourcesAddUI model)
    {
        // rtf
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(EnrScriptItem.class, EnrScriptItemCodes.REPORT_INFORMATION_SOURCES);
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());

        // Дата

        new RtfInjectModifier().put("dateForm", new SimpleDateFormat("dd.MM.yyyy").format(new Date())).modify(document);

        // Фильтры
        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder()
                .notArchiveOnly()
                .filter(model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo())
                .filter(model.getEnrollmentCampaign());
        filterAddon.applyFilters(requestedCompDQL, requestedCompDQL.competition());

        if (model.getParallelSelector().isParallelActive())
        {
            if (model.getParallelSelector().isParallelOnly())
                requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.TRUE)));
            if (model.getParallelSelector().isSkipParallel())
                requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.FALSE)));
        }

        model.getStageSelector().applyFilter(requestedCompDQL, "reqComp");
        requestedCompDQL
               // .where(le(property("reqComp", EnrRequestedCompetition.state().priority()), value(model.getStageSelector().getStage().getId())))
                .joinEntity("entrant", DQLJoinType.inner, EnrEntrantInfoAboutUniversity.class, "info", eq(property("entrant", EnrEntrant.id()), property("info", EnrEntrantInfoAboutUniversity.entrant().id())))
                .column(property("info"));

        List<EnrEntrantInfoAboutUniversity> dataList = getList(requestedCompDQL);

        Map<EnrSourceInfoAboutUniversity, Set<EnrEntrant>> infoMap = new HashMap<>();

        for (EnrEntrantInfoAboutUniversity infoSource : dataList)
        {
            if (!infoMap.containsKey(infoSource.getSourceInfo()))
                infoMap.put(infoSource.getSourceInfo(), new HashSet<EnrEntrant>());
            infoMap.get(infoSource.getSourceInfo()).add(infoSource.getEntrant());
        }

        int entrantsTotal = 0;

        List<PairKey<EnrSourceInfoAboutUniversity, Set<EnrEntrant>>> sortedContent = new ArrayList<>();

        for (Map.Entry<EnrSourceInfoAboutUniversity, Set<EnrEntrant>> entry : infoMap.entrySet())
        {
            sortedContent.add(new PairKey<EnrSourceInfoAboutUniversity, Set<EnrEntrant>>(entry.getKey(), entry.getValue()));
            entrantsTotal += entry.getValue().size();
        }

        Collections.sort(sortedContent, new Comparator<PairKey<EnrSourceInfoAboutUniversity, Set<EnrEntrant>>>() {
            @Override
            public int compare(PairKey<EnrSourceInfoAboutUniversity, Set<EnrEntrant>> o1, PairKey<EnrSourceInfoAboutUniversity, Set<EnrEntrant>> o2) {
                int res = o2.getSecond().size() - o1.getSecond().size();
                if (res!=0) return res;
                return o1.getFirst().getTitle().compareToIgnoreCase(o2.getFirst().getTitle());
            }
        });

        List<String[]> hTable = new FilterParametersPrinter().getTable(model.getCompetitionFilterAddon(), model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo());
        hTable.add(2, new String[]{"Стадия приемной кампании", model.getStageSelector().getStage().getTitle()});
        if (model.getParallelSelector().isParallelActive())
            hTable.add(2, new String[]{"Поступающие параллельно", model.getParallelSelector().getParallel().getTitle()});

        List<String[]> tTable = new ArrayList<>();

        for (PairKey<EnrSourceInfoAboutUniversity, Set<EnrEntrant>> pair : sortedContent)
        {
            StringBuilder builder = new StringBuilder().append(pair.getSecond().size()).append(" (").append(pair.getSecond().size()*100/entrantsTotal).append("%)");
            tTable.add(new String[]{
                    pair.getFirst().getTitle(),
                    builder.toString()
            });
        }
        tTable.add(new String[]{"Всего", String.valueOf(entrantsTotal)});

        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("H", hTable.toArray(new String[hTable.size()][]));
        tableModifier.put("H", new RtfRowIntercepterRawText());
        tableModifier.put("T", tTable.toArray(new String[tTable.size()][]));
        tableModifier.modify(document);


        return RtfUtil.toByteArray(document);
    }
}
