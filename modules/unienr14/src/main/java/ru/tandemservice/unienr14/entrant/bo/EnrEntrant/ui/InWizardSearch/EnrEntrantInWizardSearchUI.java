package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.InWizardSearch;

import org.apache.tapestry.form.validator.BaseValidator;
import org.hibernate.Session;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.commonbase.base.util.wizard.SimpleWizardUIPresenter;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonRole;
import org.tandemframework.shared.person.base.entity.gen.PersonRoleGen;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.shared.person.catalog.entity.codes.IdentityCardTypeCodes;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.Wizard.EnrEntrantRequestWizardUI;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.*;
import java.util.Map.Entry;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
@Input({
    @Bind(key=EnrEntrantRequestWizardUI.BC_PARAM_ENROLLMENT_CAMPAIGN_ID, binding="enrollmentCampaignHolder.id", required=true)
})
public class EnrEntrantInWizardSearchUI extends UIPresenter {

    private final EntityHolder<EnrEnrollmentCampaign> enrollmentCampaignHolder = new EntityHolder<>();
    public EntityHolder<EnrEnrollmentCampaign> getEnrollmentCampaignHolder() { return this.enrollmentCampaignHolder; }
    public EnrEnrollmentCampaign getEnrollmentCampaign() { return this.getEnrollmentCampaignHolder().getValue(); }

    // шаг 1: форма поиска

    private List<IdentityCardType> identityCardTypeList = Collections.emptyList();
    public List<IdentityCardType> getIdentityCardTypeList() { return this.identityCardTypeList; }
    public void setIdentityCardTypeList(final List<IdentityCardType> identityCardTypeList) { this.identityCardTypeList = identityCardTypeList; }

    private IdentityCardType identityCardType;
    public IdentityCardType getIdentityCardType() { return this.identityCardType; }
    public void setIdentityCardType(final IdentityCardType identityCardType) { this.identityCardType = identityCardType; }

    private String cardFirstName;
    public String getCardFirstName() { return this.cardFirstName; }
    public void setCardFirstName(final String cardFirstName) { this.cardFirstName = cardFirstName; }

    private String cardLastName;
    public String getCardLastName() { return this.cardLastName; }
    public void setCardLastName(final String cardLastName) { this.cardLastName = cardLastName; }

    private String cardSeria;
    public String getCardSeria() {
        final IdentityCardType identityCardType = this.getIdentityCardType();
        if (null == identityCardType) { return null; }
        if (!identityCardType.isShowSeria()) { return null; }
        return this.cardSeria;
    }
    public void setCardSeria(final String cardSeria) { this.cardSeria = cardSeria; }

    private String cardNumber;
    public String getCardNumber() {
        final IdentityCardType identityCardType = this.getIdentityCardType();
        if (null == identityCardType) { return null; }
        return this.cardNumber;
    }
    public void setCardNumber(final String cardNumber) { this.cardNumber = cardNumber; }

    // шаг 2: результаты поиска

    private StaticListDataSource<DataWrapper> similarDataSource;
    public StaticListDataSource<DataWrapper> getSimilarDataSource() { return this.similarDataSource; }
    public void setSimilarDataSource(final StaticListDataSource<DataWrapper> similarDataSource) { this.similarDataSource = similarDataSource; }

    private DataWrapper theSamePersonWrapper;
    public DataWrapper getTheSamePersonWrapper() { return this.theSamePersonWrapper; }
    public void setTheSamePersonWrapper(final DataWrapper theSamePersonWrapper) { this.theSamePersonWrapper = theSamePersonWrapper; }


    ////////////////////////////////


    @Override
    public void onComponentRefresh() {
        final IUniBaseDao dao = IUniBaseDao.instance.get();
        this.setIdentityCardTypeList(dao.getCatalogItemList(IdentityCardType.class));
        if (null == this.getIdentityCardType()) {
            this.setIdentityCardType(dao.getCatalogItem(IdentityCardType.class, IdentityCardTypeCodes.PASPORT_GRAJDANINA_ROSSIYSKOY_FEDERATSII));
        }

        this.getEnrollmentCampaignHolder().refresh(EnrEnrollmentCampaign.class);
    }


    public void onClickSearchSimilar()
    {
        //////////////////

        final NavigableMap<Double, List<DataWrapper>> similarMap = PersonManager.instance().dao().getSimilarPerson(
            this.getIdentityCardType(),
            this.getCardLastName(),
            this.getCardFirstName(),
            this.getCardSeria(),
            this.getCardNumber()
        );

        if (similarMap.isEmpty()) {
            // перейти на форму создания персоны
            // onClickPickNewPerson();
            // return;
        }

        //////////////////

        final Set<Long> ids = new HashSet<Long>();
        for (final List<DataWrapper> records : similarMap.values()) {
            ids.addAll(CommonDAO.ids(records));
        }

        final Session session = this.getSupport().getSession();
        final Map<Person, List<PersonRole>> person2RoleMap = new HashMap<>();
        BatchUtils.execute(ids, 512, new BatchUtils.Action<Long>() {
            @Override public void execute(final Collection<Long> ids) {
                final List<PersonRole> personRoleList = new DQLSelectBuilder()
                .fromEntity(PersonRole.class, "r").column("r")
                .where(in(property(PersonRoleGen.person().id().fromAlias("r")), ids))
                .order(property(EntityBase.id().fromAlias("r")))
                .createStatement(session)
                .list();

                for (final PersonRole personRole : personRoleList) {
                    final List<PersonRole> list = SafeMap.safeGet(person2RoleMap, personRole.getPerson(), ArrayList.class);
                    list.add(personRole);
                }
            }
        });

        final Map<Person, List<EnrEntrantRequest>> person2requestMap = new HashMap<>();
        BatchUtils.execute(ids, 512, new BatchUtils.Action<Long>() {
            @Override public void execute(final Collection<Long> ids) {
                final List<EnrEntrantRequest> requestList = new DQLSelectBuilder()
                .fromEntity(EnrEntrantRequest.class, "r").column("r")
                .where(in(property(EnrEntrantRequest.entrant().person().id().fromAlias("r")), ids))
                .where(eq(property(EnrEntrantRequest.entrant().enrollmentCampaign().educationYear().fromAlias("r")), value(getEnrollmentCampaign().getEducationYear())))
                .order(property(EnrEntrantRequest.entrant().enrollmentCampaign().title().fromAlias("r")))
                .order(property(EnrEntrantRequest.regNumber().fromAlias("r")))
                .order(property(EntityBase.id().fromAlias("r")))
                .createStatement(session)
                .list();
                for (final EnrEntrantRequest request : requestList) {
                    final List<EnrEntrantRequest> list = SafeMap.safeGet(person2requestMap, request.getEntrant().getPerson(), ArrayList.class);
                    list.add(request);
                }
            }
        });

        //////////////////

        final List<DataWrapper> result = new ArrayList<>();
        for (final Entry<Double, List<DataWrapper>> e : similarMap.entrySet()) {
            for (final DataWrapper wrapper: e.getValue()) {
                final Person person = DataAccessServices.dao().get(Person.class, wrapper.getId());
                final List<PersonRole> roles = person2RoleMap.get(person);
                final List<EnrEntrantRequest> requests = person2requestMap.get(person);

                wrapper.setProperty("identityCardFullNumber", person.getIdentityCard().getFullNumber());
                wrapper.setProperty("roles", (null == roles ? Collections.emptyList() : roles));
                wrapper.setProperty("requests", (null == requests ? Collections.emptyList() : requests));
                wrapper.setProperty("similarity", e.getKey());

                result.add(wrapper);
            }
        }

        //////////////////

        final StaticListDataSource<DataWrapper> similarDataSource = new StaticListDataSource<>(result);
        this.setTheSamePersonWrapper(null);
        this.setSimilarDataSource(similarDataSource);

        // ищем 100% совпадение, если список похожих не пустой, совпадение близко к 1 и таких персон не более одной
        // * в системе могут быть персоны с одинаковыми ФИО и данными, как дубли, например)
        // * кроме того не учитывается отчество
        if (!similarMap.isEmpty()) {
            final Entry<Double, List<DataWrapper>> firstEntry = similarMap.entrySet().iterator().next();
            if (firstEntry.getKey().doubleValue() > 0.9999999f) {
                if (1 == firstEntry.getValue().size()) {
                    setTheSamePersonWrapper(firstEntry.getValue().get(0));
                }
            }
        }

        similarDataSource.addColumn(new SimpleColumn("ФИО", "title").setClickable(false).setOrderable(false));
        similarDataSource.addColumn(new SimpleColumn("Удостоверение личности", "identityCardFullNumber").setClickable(false).setOrderable(false));
        similarDataSource.addColumn(new BlockColumn("requestLink", "Заявления " +getEnrollmentCampaign().getEducationYear().getTitle()).setClickable(false).setOrderable(false));
        similarDataSource.addColumn(new BlockColumn("pickPerson", "Выбор").setClickable(false).setOrderable(false));
    }

    public void onClickPickPerson()
    {
        final DataWrapper theSamePersonWrapper = getTheSamePersonWrapper();
        final Person person = DataAccessServices.dao().get(Person.class, this.getSupport().getListenerParameterAsLong());
        if (null != theSamePersonWrapper) {
            if (!theSamePersonWrapper.getId().equals(person.getId())) {
                throw new ApplicationException("Найдено 100% совпадение. Выберите существующего абитуриента либо уточните параметры поиска.");
            }
        }

        // переходим дальше, указываем данные персоны
        deactivate(
            buildReturnMap()
            .add(EnrEntrantRequestWizardUI.BC_PARAM_PERSON_ID, person.getId())
            .buildMap()
        );
    }

    public void onClickPickNewPerson()
    {
        if (isTheSamePersonExists()) {
            throw new ApplicationException("Найдено 100% совпадение. Выберите существующего абитуриента либо уточните параметры поиска.");
        }

        // переходим дальше, указыаем данные формы
        deactivate(
            buildReturnMap()
            .buildMap()
        );
    }

    public boolean isCanPickPerson()
    {
        final DataWrapper theSamePersonWrapper = getTheSamePersonWrapper();
        if (null == theSamePersonWrapper) { return true; }

        final DataWrapper entity = getSimilarDataSource().getCurrentEntity();
        return theSamePersonWrapper.getId().equals(entity.getId());
    }

    public boolean isTheSamePersonExists()
    {
        return (null != getTheSamePersonWrapper());
    }


    protected SimpleWizardUIPresenter.ReturnBuilder buildReturnMap() {
        return new SimpleWizardUIPresenter.ReturnBuilder(getConfig(), getEnrollmentCampaignHolder().getId())
        .add(EnrEntrantRequestWizardUI.BC_PARAM_PERSON_ID, null)
        .add("identityCard.cardType", getIdentityCardType())
        .add("identityCard.seria", getCardSeria())
        .add("identityCard.number", getCardNumber())
        .add("identityCard.lastName", getCardLastName())
        .add("identityCard.firstName", getCardFirstName());
    }


}
