package ru.tandemservice.unienr14.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Тип олимпиады"
 * Имя сущности : enrOlympiadType
 * Файл data.xml : unienr.olymp.catalog.data.xml
 */
public interface EnrOlympiadTypeCodes
{
    /** Константа кода (code) элемента : Всероссийская олимпиада школьников (title) */
    String ALL_RUSSIA = "1";
    /** Константа кода (code) элемента : Олимпиада школьников из перечня Минобрнауки (title) */
    String STATE_LIST = "2";
    /** Константа кода (code) элемента : Другая олимпиада (title) */
    String OTHER = "3";
    /** Константа кода (code) элемента : Всеукраинская ученическая олимпиада (4 этап) (title) */
    String UKRAINIAN_OLYMPIAD = "4";
    /** Константа кода (code) элемента : Международная олимпиада (title) */
    String INTERNATIONAL_OLYMPIAD = "5";

    Set<String> CODES = ImmutableSet.of(ALL_RUSSIA, STATE_LIST, OTHER, UKRAINIAN_OLYMPIAD, INTERNATIONAL_OLYMPIAD);
}
