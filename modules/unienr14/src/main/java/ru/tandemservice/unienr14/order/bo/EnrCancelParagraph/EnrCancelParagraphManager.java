/* $Id:$ */
package ru.tandemservice.unienr14.order.bo.EnrCancelParagraph;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unienr14.order.bo.EnrCancelParagraph.logic.IEnrCancelParagraphDao;
import ru.tandemservice.unienr14.order.bo.EnrCancelParagraph.logic.EnrCancelParagraphDao;

/**
 * @author oleyba
 * @since 7/10/14
 */
@Configuration
public class EnrCancelParagraphManager extends BusinessObjectManager
{
    public static EnrCancelParagraphManager instance()
    {
        return instance(EnrCancelParagraphManager.class);
    }

    @Bean
    public IEnrCancelParagraphDao dao()
    {
        return new EnrCancelParagraphDao();
    }
}
