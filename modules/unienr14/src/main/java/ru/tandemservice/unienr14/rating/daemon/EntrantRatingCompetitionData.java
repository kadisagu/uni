package ru.tandemservice.unienr14.rating.daemon;

import org.tandemframework.core.tool.IdGen;
import org.tandemframework.shared.sandbox.utils.RSHashBuilder;

import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes;

/**
 * Сводные данные конкурса (для рейтингового списка)
 * @author vdanilov
 */
public class EntrantRatingCompetitionData {

    private final Long competitionId;
    public Long getCompetitionId() { return this.competitionId; }

    private final String requestTypeCode;
    public String getRequestTypeCode() { return this.requestTypeCode; }

    public final String competitionTypeCode;
    public String getCompetitionTypeCode() { return this.competitionTypeCode; }

    private final long valueHash;
    public long getValueHash() { return this.valueHash; }

    private final boolean noExam;
    public boolean isNoExam() { return this.noExam; }

    private final String tieBreakRuleCode;
    public String getTieBreakRuleCode() { return tieBreakRuleCode; }

    private final String eduProgramKindCode;
    public String getEduProgramKindCode(){return eduProgramKindCode;}

    public EntrantRatingCompetitionData(final Long competitionId, final String requestTypeCode, final String competitionTypeCode, final String tieBreakRuleCode, final String eduProgramKind) {
        this.competitionId = competitionId;
        this.requestTypeCode = requestTypeCode;
        this.competitionTypeCode = competitionTypeCode;
        this.noExam = (EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL.equals(competitionTypeCode) || EnrCompetitionTypeCodes.NO_EXAM_CONTRACT.equals(competitionTypeCode));
        this.tieBreakRuleCode = tieBreakRuleCode;
        this.eduProgramKindCode = eduProgramKind;

        // конструируем хэш
        final RSHashBuilder b = new RSHashBuilder();
        b.add(this.getCompetitionId() >>> IdGen.CODE_BITS);
        b.add(this.getRequestTypeCode().hashCode());
        b.add(this.getCompetitionTypeCode().hashCode());
        b.add(this.noExam ? 1L : 0L);
        b.add(this.getEduProgramKindCode().hashCode());
        this.valueHash = b.result();
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) { return true; }
        if (!(obj instanceof EntrantRatingCompetitionData)) { return false; }
        final EntrantRatingCompetitionData o = (EntrantRatingCompetitionData)obj;
        return this.getCompetitionId().equals(o.getCompetitionId());
    }

    @Override
    public int hashCode() {
        return this.getCompetitionId().hashCode();
    }

    @Override
    public String toString() {
        return "EntrantRatingCompetitionData("+this.getCompetitionId()+", rqTp="+this.getRequestTypeCode()+", cTp="+this.getCompetitionTypeCode()+")";
    }

    private long totalAchievementMarkLimit;
    public long getTotalAchievementMarkLimit() { return this.totalAchievementMarkLimit; }
    public void setTotalAchievementMarkLimit(long totalAchievementMarkLimit) { this.totalAchievementMarkLimit = Math.max(0L, totalAchievementMarkLimit); }


}
