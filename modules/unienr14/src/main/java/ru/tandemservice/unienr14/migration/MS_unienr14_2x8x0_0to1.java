package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x8x0_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrEnrollmentModel

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("enr14_enr_model_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_enrenrollmentmodel"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("version", DBType.INTEGER).setNullable(false), 
				new DBColumn("enrollmentcampaign_id", DBType.LONG).setNullable(false), 
				new DBColumn("requesttype_id", DBType.LONG).setNullable(false), 
				new DBColumn("title_p", DBType.createVarchar(255)).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("enrEnrollmentModel");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrModelCompetitionPlan

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("enr14_enr_model_comp_plan_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_enrmodelcompetitionplan"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("modelstep_id", DBType.LONG).setNullable(false), 
				new DBColumn("competition_id", DBType.LONG).setNullable(false), 
				new DBColumn("baseplan_p", DBType.INTEGER).setNullable(false), 
				new DBColumn("plan_p", DBType.INTEGER).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("enrModelCompetitionPlan");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrModelStep

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("enr14_enr_model_step_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_enrmodelstep"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("version", DBType.INTEGER).setNullable(false), 
				new DBColumn("enrollmentmodel_id", DBType.LONG).setNullable(false), 
				new DBColumn("enrollmentdate_p", DBType.DATE).setNullable(false), 
				new DBColumn("state_id", DBType.LONG).setNullable(false), 
				new DBColumn("kind_id", DBType.LONG).setNullable(false), 
				new DBColumn("autoenrolledmarked_p", DBType.BOOLEAN).setNullable(false), 
				new DBColumn("percentageaslong_p", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("enrModelStep");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrModelStepItem

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("enr14_enr_model_step_item_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_enrmodelstepitem"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("modelstep_id", DBType.LONG).setNullable(false), 
				new DBColumn("entity_id", DBType.LONG).setNullable(false), 
				new DBColumn("originalin_p", DBType.BOOLEAN).setNullable(false), 
				new DBColumn("accepted_p", DBType.BOOLEAN).setNullable(false), 
				new DBColumn("included_p", DBType.BOOLEAN).setNullable(false), 
				new DBColumn("enrolled_p", DBType.BOOLEAN).setNullable(false), 
				new DBColumn("reenrolled_p", DBType.BOOLEAN).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("enrModelStepItem");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrModelStepStage

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("enr14_enr_model_step_stage_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_enrmodelstepstage"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("modelstep_id", DBType.LONG).setNullable(false), 
				new DBColumn("enrollmentstage_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("enrModelStepStage");

		}


    }
}