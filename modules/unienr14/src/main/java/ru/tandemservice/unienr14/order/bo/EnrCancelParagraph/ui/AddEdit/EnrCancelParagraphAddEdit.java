/* $Id:$ */
package ru.tandemservice.unienr14.order.bo.EnrCancelParagraph.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.column.CheckboxDSColumn;
import org.tandemframework.caf.ui.config.datasource.column.TextDSColumn;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.core.view.formatter.NoWrapFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderType;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrOrderTypeCodes;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.formatter.EnrEntrantCustomStateCollectionFormatter;
import ru.tandemservice.unienr14.order.bo.EnrCancelParagraph.logic.EnrCancelParagraphExtractSelectionDSHandler;
import ru.tandemservice.unienr14.order.entity.EnrAbstractExtract;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unimove.entity.catalog.codes.OrderStatesCodes;

import java.util.Arrays;

/**
 * @author oleyba
 * @since 7/10/14
 */
@Configuration
public class EnrCancelParagraphAddEdit extends BusinessComponentManager
{
    public static final String BIND_REQUEST_TYPE = "requestType";
    public static final String BIND_COMPENSATION_TYPE = "compensationType";
    public static final String BIND_ORG_UNIT = "orgUnit";
    public static final String BIND_CANCELLED_ORDER_TYPE = "cancelledOrderType";
    public static final String BIND_CANCELLED_ORDER = "cancelledOrder";
    public static final String BIND_PARAGRAPH = "paragraph";

    public static final String DS_CANCELLED_ORDER_TYPE = "orderTypeDS";
    public static final String DS_CANCELLED_ORDER = "cancelledOrderDS";
    public static final String DS_EXTRACT = "extractDS";

    public static final String CHECKBOX_COLUMN = "checkbox";



    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(searchListDS(DS_EXTRACT, extractDS(), extractDSHandler()))
            .addDataSource(selectDS(DS_CANCELLED_ORDER_TYPE, cancelledOrderTypeDSHandler()))
            .addDataSource(selectDS(DS_CANCELLED_ORDER, cancelledOrderDSHandler()))
            .create();
    }

    /**
     * Создается точка расширения "Список выбранных конкурсов на форме добавления/редактирования параграфа", единая
     * для всех форм добавления/редактирования параграфов о зачислении
     *
     * @return точка расширения "Список выбранных конкурсов на форме добавления/редактирования параграфа"
     */
    @Bean
    public ColumnListExtPoint extractDS()
    {
        return ColumnListExtPoint.with(getName(), "extractDS")
            .addColumn(CheckboxDSColumn.with().name(CHECKBOX_COLUMN).create())
            .addColumn(textColumn("fio", EnrAbstractExtract.SL_REQUESTED_COMPETITION + "." + EnrRequestedCompetition.request().entrant().person().identityCard().fullFio()).formatter(NoWrapFormatter.INSTANCE).create())
            .addColumn(TextDSColumn.with().name(EnrCancelParagraphExtractSelectionDSHandler.CUSTOM_STATES).path(EnrCancelParagraphExtractSelectionDSHandler.CUSTOM_STATES).formatter(new EnrEntrantCustomStateCollectionFormatter()).create())
            .addColumn(textColumn("sex", EnrAbstractExtract.SL_REQUESTED_COMPETITION + "." + EnrRequestedCompetition.request().entrant().person().identityCard().sex().shortTitle()).create())
            .addColumn(textColumn("passport", EnrAbstractExtract.SL_REQUESTED_COMPETITION + "." + EnrRequestedCompetition.request().entrant().person().identityCard().fullNumber()).formatter(NoWrapFormatter.INSTANCE).create())
            .addColumn(textColumn("eduLevel", EnrAbstractExtract.SL_REQUESTED_COMPETITION + "." + EnrRequestedCompetition.request().eduDocument().eduLevel().shortTitle()).create())
            .addColumn(textColumn("competition", EnrAbstractExtract.SL_REQUESTED_COMPETITION + "." + EnrRequestedCompetition.competition().title()).create())
            .addColumn(textColumn("conditions", EnrAbstractExtract.SL_REQUESTED_COMPETITION + "." + EnrRequestedCompetition.parametersTitle()).create())
            .addColumn(textColumn("state", EnrAbstractExtract.SL_REQUESTED_COMPETITION + "." + EnrRequestedCompetition.state().stateDaemonSafe()).create())
            .addColumn(textColumn("extractState", EnrAbstractExtract.state().title()).create())
            .addColumn(textColumn("otherExtracts", EnrCancelParagraphExtractSelectionDSHandler.OTHER_EXTRACTS).formatter(CollectionFormatter.COLLECTION_FORMATTER))
            .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> extractDSHandler()
    {
        return new EnrCancelParagraphExtractSelectionDSHandler(getName());
    }

    /* Выбор конкурсов */

    @Bean
    public IDefaultComboDataSourceHandler cancelledOrderTypeDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrOrderType.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(DQLExpressions.ne(DQLExpressions.property(alias, EnrOrderType.code()), DQLExpressions.value(EnrOrderTypeCodes.ENROLLMENT_MIN)));
            }
        }
            .order(EnrOrderType.code())
            .filter(EnrOrderType.title())
            .pageable(false);
    }

    @Bean
    public IDefaultComboDataSourceHandler cancelledOrderDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrOrder.class)
            .where(EnrOrder.state().code(), Arrays.asList(OrderStatesCodes.ACCEPTED, OrderStatesCodes.FINISHED))
            .where(EnrOrder.enrollmentCampaign(), EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN)
            .where(EnrOrder.compensationType(), BIND_COMPENSATION_TYPE)
            .where(EnrOrder.requestType(), BIND_REQUEST_TYPE)
            .where(EnrOrder.type(), BIND_CANCELLED_ORDER_TYPE)
            .order(EnrOrder.number())
            .filter(EnrOrder.number())
            .pageable(true);
    }

}