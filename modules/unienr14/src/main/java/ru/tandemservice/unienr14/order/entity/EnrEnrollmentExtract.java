package ru.tandemservice.unienr14.order.entity;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.order.entity.gen.EnrAbstractExtractGen;
import ru.tandemservice.unienr14.order.entity.gen.EnrAbstractOrderGen;
import ru.tandemservice.unienr14.order.entity.gen.EnrAbstractParagraphGen;
import ru.tandemservice.unienr14.order.entity.gen.EnrEnrollmentExtractGen;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.codes.ExtractStatesCodes;

import java.util.Arrays;
import java.util.Collection;

/**
 * Выписка приказа о зачислении абитуриентов (2014)
 */
public class EnrEnrollmentExtract extends EnrEnrollmentExtractGen
{
    // состояния выписок в которых мы считаем, что выписка проведена
    // todo enrollment: что это ваще?
    public static final Collection<String> COMMITED_STATE_CODES = Arrays.asList(ExtractStatesCodes.FINISHED, ExtractStatesCodes.ACCEPTED);

    @Override
    public int compareTo(Object o)
    {
        if (o instanceof EnrEnrollmentExtract) {
            EnrEnrollmentExtract e = (EnrEnrollmentExtract) o;
            return CommonCollator.RUSSIAN_COLLATOR.compare(getEntity().getRequest().getEntrant().getPerson().getFullFio(), e.getEntity().getRequest().getEntrant().getPerson().getFullFio());
        }
        return 0;
    }

    @Override
    public boolean isCommitted()
    {
        return UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED.equals(getState().getCode());
    }

    @Override
    public void setEntity(Object entity)
    {
        setEntity((EnrRequestedCompetition) entity);
    }

    @Override
    public void setCommitted(boolean committed)
    {
        // не нужно
    }

    @Override
    public EnrRequestedCompetition getRequestedCompetition()
    {
        return getEntity();
    }

    @Override
    public EnrCompetition getCompetition()
    {
        return getEntity().getCompetition();
    }

    @Override
    public EnrEntrant getEntrant()
    {
        return getEntity().getRequest().getEntrant();
    }

    @Override
    @EntityDSLSupport(parts = {
        EnrAbstractExtractGen.L_PARAGRAPH+"."+EnrAbstractParagraphGen.L_ORDER+"."+EnrAbstractOrderGen.P_NUMBER,
        EnrAbstractExtractGen.L_PARAGRAPH+"."+EnrAbstractParagraphGen.L_ORDER+"."+EnrAbstractOrderGen.P_COMMIT_DATE,
        EnrAbstractExtractGen.L_PARAGRAPH+"."+EnrAbstractParagraphGen.L_ORDER+"."+EnrAbstractOrderGen.P_ID
    })
    public String getOrderInfo() {
        return getShortTitle();
    }

    public EnrOrder getOrder() {
        return (EnrOrder) getParagraph().getOrder();
    }
}