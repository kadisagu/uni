package ru.tandemservice.unienr14.catalog.entity;

import com.google.common.collect.ImmutableList;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.BaseDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import ru.tandemservice.unienr14.catalog.entity.gen.*;

import java.util.*;

/**
 * Тип приказа по абитуриентам
 */
public class EnrOrderType extends EnrOrderTypeGen implements IDynamicCatalogItem
{
    private static final List<String> HIDDEN_FIELDS = ImmutableList.of(EnrOrderType.P_COMPONENT_PREFIX);

    public static IDynamicCatalogDesc getUiDesc() {
        return new BaseDynamicCatalogDesc() {
            @Override public Collection<String> getHiddenFields() {
                return HIDDEN_FIELDS;
            }
        };
    }
}