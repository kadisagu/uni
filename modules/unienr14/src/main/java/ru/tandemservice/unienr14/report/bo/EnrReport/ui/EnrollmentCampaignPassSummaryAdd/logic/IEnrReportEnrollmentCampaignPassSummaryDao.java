/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EnrollmentCampaignPassSummaryAdd.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EnrollmentCampaignPassSummaryAdd.EnrReportEnrollmentCampaignPassSummaryAddUI;

/**
 * @author rsizonenko
 * @since 10.06.2014
 */
public interface IEnrReportEnrollmentCampaignPassSummaryDao extends INeedPersistenceSupport {
    Long createReport(EnrReportEnrollmentCampaignPassSummaryAddUI enrReportEnrollmentCampaignPassSummaryAddUI);
}
