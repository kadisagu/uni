/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.utils;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.ICommonFilterItem;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrReport;
import ru.tandemservice.unienr14.report.entity.EnrReportEntrantList;

import java.util.Collection;

/**
 * @author oleyba
 * @since 5/14/14
 */
public class EnrReportUtil
{
    public static void setReportFilterValue(EnrCompetitionFilterAddon filterAddon, IEnrReport report, String settingName, String reportPropertyName, String titleProperty)
    {
        ICommonFilterItem filterItem = filterAddon.getFilterItem(settingName);
        Object filterItemValue = filterItem.getValue();
        if (!filterItem.isEnableCheckboxChecked() || filterItemValue == null) {
            report.setProperty(reportPropertyName, null);
            return;
        }
        if (filterItem.getFormConfig().isMultiSelect()) {
            report.setProperty(reportPropertyName, UniStringUtils.join((Collection) filterItemValue, titleProperty, "; "));
        } else {
            report.setProperty(reportPropertyName, ((IEntity) filterItemValue).getProperty(titleProperty));
        }
    }
}
