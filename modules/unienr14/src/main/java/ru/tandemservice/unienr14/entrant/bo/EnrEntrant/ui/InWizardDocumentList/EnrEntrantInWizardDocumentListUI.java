package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.InWizardDocumentList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.activator.TopRegionActivation;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.wizard.SimpleWizardUIPresenter;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubAllDocumentTab.EnrEntrantPubAllDocumentTab;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubAllDocumentTab.EnrEntrantPubAllDocumentTabUI;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.Wizard.EnrEntrantRequestWizardUI;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.Map;

/**
 * @author vdanilov
 */
@Input({
    @Bind(key=EnrEntrantRequestWizardUI.BC_PARAM_ENTRANT_ID, binding="entrantHolder.id", required=true)
})
@TopRegionActivation
public class EnrEntrantInWizardDocumentListUI extends UIPresenter {

    private final EntityHolder<EnrEntrant> entrantHolder = new EntityHolder<>();
    public EntityHolder<EnrEntrant> getEntrantHolder() { return this.entrantHolder; }
    public EnrEntrant getEntrant() { return this.getEntrantHolder().getValue(); }
    public EnrEnrollmentCampaign getEnrollmentCampaign() { return this.getEntrant().getEnrollmentCampaign(); }

    public String getEntrantDocumentTabComponentName() {
        return EnrEntrantPubAllDocumentTab.class.getSimpleName();
    }

    public Map<String, Object> getEntrantDocumentTabComponentParameters() {
        return new ParametersMap()
        .add(UIPresenter.PUBLISHER_ID, getEntrantHolder().getId())
        .add(EnrEntrantPubAllDocumentTabUI.PARAM_INLINE, Boolean.TRUE);
    }

    @Override
    public void onComponentRefresh() {
    }

    public void onClickNext() {

        // здесь нужно реализовать проверки, например, на то, что у персоны есть документ об образовании
        {
            if (!ISharedBaseDao.instance.get().existsEntity(PersonEduDocument.class, PersonEduDocument.L_PERSON, getEntrant().getPerson())) {
                throw new ApplicationException("Необходимо добавить документ о полученном образовании.");
            }
        }

        // просто переходим дальше
        deactivate(
            new SimpleWizardUIPresenter.ReturnBuilder(getConfig(), getEntrantHolder().getId())
                .add("entrantId", getEntrantHolder().getId())
                .buildMap()
        );
    }

}
