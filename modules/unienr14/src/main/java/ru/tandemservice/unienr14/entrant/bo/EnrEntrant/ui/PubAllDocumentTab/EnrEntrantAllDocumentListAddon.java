/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubAllDocumentTab;

import com.google.common.collect.Maps;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.TitledFormatter;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.SimplePublisherLinkResolver;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.commonbase.remoteDocument.dto.RemoteDocumentDTO;
import org.tandemframework.shared.commonbase.remoteDocument.service.ICommonRemoteDocumentService;
import org.tandemframework.shared.commonbase.remoteDocument.service.IRemoteDocumentProvider;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.bo.Person.ui.IdentityCardAddEdit.PersonIdentityCardAddEdit;
import org.tandemframework.shared.person.base.bo.Person.ui.IdentityCardEditInline.PersonIdentityCardEditInline;
import org.tandemframework.shared.person.base.bo.Person.util.ISecureRoleContext;
import org.tandemframework.shared.person.base.bo.PersonDocument.PersonDocumentManager;
import org.tandemframework.shared.person.base.bo.PersonDocument.ui.Add.PersonDocumentAdd;
import org.tandemframework.shared.person.base.bo.PersonEduDocument.PersonEduDocumentManager;
import org.tandemframework.shared.person.base.bo.PersonEduDocument.ui.AddEdit.PersonEduDocumentAddEditUI;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.PersonDocument;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import org.tandemframework.shared.person.remoteDocument.ext.RemoteDocument.RemoteDocumentExtManager;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrantBaseDocument.util.EnrEntrantDocumentViewWrapper;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrantEduDocument.ui.AddEdit.EnrEntrantEduDocumentAddEdit;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrantEduDocument.ui.AddEdit.EnrEntrantEduDocumentAddEditUI;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantBaseDocument;
import ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma;
import ru.tandemservice.unienr14.legacy.EnrPersonLegacyUtils;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequestAttachment;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 6/12/14
 */
public class EnrEntrantAllDocumentListAddon extends UIAddon
{
    public static final String NAME = "EnrEntrantAllDocumentListAddon";

    public interface IOwner extends IUIPresenter {
        EnrEntrant getEntrant();
        ISecureRoleContext getSecureRoleContext();
        String getPrintPermissionKey();
        String getEditPermissionKey();
        String getDeletePermissionKey();
    }
    
    private StaticListDataSource<IEntity> dataSource;
    
    public EnrEntrantAllDocumentListAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }
    
    public void prepareDataSource(boolean allowLinks, AbstractColumn editColumn, AbstractColumn deleteColumn)
    {
        if (getDataSource() == null) {
            setDataSource(new StaticListDataSource<IEntity>());
            getDataSource().setMinCountRow(5);

            ISecureRoleContext secureRoleContext = getOwner().getSecureRoleContext();

            if (allowLinks) {
                PublisherLinkColumn linkColumn = new PublisherLinkColumn("Тип документа", EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_DOCUMENT_TYPE_TITLE);
                linkColumn.setResolver(EnrPersonLegacyUtils.personRelatedStuffLinkResolver(secureRoleContext));
                linkColumn.setOrderable(false);
                getDataSource().addColumn(linkColumn);
            } else {
                getDataSource().addColumn(new SimpleColumn("Тип документа", EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_DOCUMENT_TYPE_TITLE).setClickable(false).setOrderable(false));
            }

            getDataSource().addColumn(new SimpleColumn("Серия и номер", EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_SERIA_AND_NUMBER).setClickable(false).setOrderable(false));
            getDataSource().addColumn(new SimpleColumn("Дата выдачи", EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_ISSUANCE_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false));
            getDataSource().addColumn(new PublisherLinkColumn("Приложен к заявлению №", "", EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_REQUEST_ATTACHMENTS).setResolver(new SimplePublisherLinkResolver(DataWrapper.ID)).setFormatter(TitledFormatter.INSTANCE).setClickable(true).setOrderable(false));
            getDataSource().addColumn(new SimpleColumn("Информация о документе", EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_INFO).setClickable(false).setOrderable(false));
            getDataSource().addColumn(new SimpleColumn("Дата добавления", EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_REG_DATE, DateFormatter.DATE_FORMATTER_WITH_TIME).setClickable(false).setOrderable(false));

            AbstractColumn printColumn = new ActionColumn("Скан-копия", CommonDefines.ICON_PRINT.getName(), "EnrEntrantAllDocumentListAddon:onClickDownloadScanCopy").setDisableHandler(entity -> {
                Boolean withoutScanCopy = (Boolean) ((EnrEntrantDocumentViewWrapper) entity).getViewProperty(EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_WITHOUT_SCAN_COPY);
                return withoutScanCopy != null ? withoutScanCopy : true;
            });
            if (null != getOwner().getPrintPermissionKey()) printColumn.setPermissionKey(getOwner().getPrintPermissionKey());
            getDataSource().addColumn(printColumn);

            if (editColumn != null) {
                getDataSource().addColumn(editColumn);
            } else if (secureRoleContext != null && secureRoleContext.isAccessible()) {
                editColumn = new ActionColumn("Редактировать", ActionColumn.EDIT, "EnrEntrantAllDocumentListAddon:onClickEdit");
                if (null != getOwner().getEditPermissionKey()) editColumn.setPermissionKey(getOwner().getEditPermissionKey());
                getDataSource().addColumn(editColumn);
            }
            if (deleteColumn != null) {
                getDataSource().addColumn(deleteColumn);
            } else  if (secureRoleContext == null || secureRoleContext.isAccessible()) {
                deleteColumn = new ActionColumn("Удалить", ActionColumn.DELETE, "EnrEntrantAllDocumentListAddon:onClickDelete", "Удалить документ {0}?", "title");
                if (null != getOwner().getDeletePermissionKey()) deleteColumn.setPermissionKey(getOwner().getDeletePermissionKey());
                getDataSource().addColumn(deleteColumn);
            }
            
        }

        // documetn id -> scanCopy id
        Map<Long, Long> scanCopyMap = Maps.newHashMap();

        List<ViewWrapper<IEntity>> documents = new ArrayList<>();

        for (IdentityCard idc : IUniBaseDao.instance.get().getList(IdentityCard.class, IdentityCard.person(), getEntrant().getPerson(), IdentityCard.id().s())) {
            documents.add(new EnrEntrantDocumentViewWrapper(idc));
        }

        for (PersonEduDocument eduDocument : IUniBaseDao.instance.get().getList(PersonEduDocument.class, PersonEduDocument.person(), getEntrant().getPerson(), PersonEduDocument.id().s())) {
            documents.add(new EnrEntrantDocumentViewWrapper(eduDocument));
            if(eduDocument.getScanCopy() != null)
                scanCopyMap.put(eduDocument.getId(), eduDocument.getScanCopy().getId());
        }

//        for (EnrOlympiadDiploma document : IUniBaseDao.instance.get().getList(EnrOlympiadDiploma.class, EnrOlympiadDiploma.entrant(), getEntrant(), EnrEntrantBaseDocument.registrationDate().s())) {
//            documents.add(new EnrEntrantDocumentViewWrapper(document));
//            if(document.getScanCopy() != null)
//                scanCopyMap.put(document.getId(), document.getScanCopy());
//        } // TODO: SH-1904

        for (EnrEntrantBaseDocument document : IUniBaseDao.instance.get().getList(EnrEntrantBaseDocument.class, EnrEntrantBaseDocument.entrant(), getEntrant(), EnrEntrantBaseDocument.registrationDate().s())) {
            documents.add(new EnrEntrantDocumentViewWrapper(document));
            if(document.getDocRelation().getDocument().getScanCopy() != null)
                scanCopyMap.put(document.getId(), document.getDocRelation().getDocument().getScanCopy());
        }

        // filters

        final EnrEntrantRequest request = getOwner().getSettings().get("entrantRequest");

        if (request != null)
        {
            final List<Long> idList = DataAccessServices.dao().getList(
                new DQLSelectBuilder().fromEntity(EnrEntrantRequestAttachment.class, "b").column(property(EnrEntrantRequestAttachment.document().id().fromAlias("b")))
                .where(in(property(EnrEntrantRequestAttachment.document().id().fromAlias("b")), UniBaseDao.ids(documents)))
                .where(eq(property(EnrEntrantRequestAttachment.entrantRequest().fromAlias("b")), value(request))));
            idList.add(request.getIdentityCard().getId());
            idList.add(request.getEduDocument().getId());

            for (Iterator<ViewWrapper<IEntity>> it=documents.iterator(); it.hasNext(); )
            {
                if (!idList.contains(it.next().getId()))
                    it.remove();
            }
        }

        // wrap

        final Map<Long, List<DataWrapper>> doc2AttachmentsMap = SafeMap.get(ArrayList.class);
        for (EnrEntrantRequestAttachment attachment : DataAccessServices.dao().getList(EnrEntrantRequestAttachment.class, EnrEntrantRequestAttachment.document().id(), UniBaseDao.ids(documents)))
        {
            doc2AttachmentsMap.get(attachment.getDocument().getId())
            .add(new DataWrapper(attachment.getEntrantRequest().getId(), attachment.getEntrantRequest().getRegNumber() + " (" + (attachment.isOriginalHandedIn() ? "оригинал" : "копия") + ")"));
        }

        for (Object[] objects : IUniBaseDao.instance.get().<Object[]>getList(new DQLSelectBuilder().fromEntity(EnrEntrantRequest.class, "r")
            .column(property(EnrEntrantRequest.identityCard().id().fromAlias("r")))
            .column(property(EnrEntrantRequest.id().fromAlias("r")))
            .column(property(EnrEntrantRequest.regNumber().fromAlias("r")))
            .where(eq(property(EnrEntrantRequest.identityCard().person().fromAlias("r")), value(getEntrant().getPerson())))))
        {
            final Long icId = (Long) objects[0];
            final Long reqId = (Long) objects[1];
            final String regNumber = (String) objects[2];

            doc2AttachmentsMap.get(icId)
            .add(new DataWrapper(reqId, regNumber + " (копия)"));
        }

        for (EnrEntrantRequest entrantRequest : IUniBaseDao.instance.get().<EnrEntrantRequest>getList(new DQLSelectBuilder()
            .fromEntity(EnrEntrantRequest.class, "r").column("r")
            .where(eq(property(EnrEntrantRequest.entrant().fromAlias("r")), value(getEntrant())))))
        {
            doc2AttachmentsMap.get(entrantRequest.getEduDocument().getId())
            .add(new DataWrapper(entrantRequest.getId(), entrantRequest.getRegNumber() + " (" + (entrantRequest.isEduInstDocOriginalHandedIn() ? "оригинал" : "копия") + ")"));
        }

        for (ViewWrapper<IEntity> wrapper : documents) {
            wrapper.setViewProperty(EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_REQUEST_ATTACHMENTS, doc2AttachmentsMap.get(wrapper.getId()));
            wrapper.setViewProperty(EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_WITHOUT_SCAN_COPY, scanCopyMap.get(wrapper.getId()) == null);
        }

        Collections.sort(documents, new EntityComparator<IEntity>(new EntityOrder(EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_DOCUMENT_TYPE + ".priority"), new EntityOrder(EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_REG_DATE), new EntityOrder("id")));

        getDataSource().setupRows(documents);
    }
    
    // actions

    public void onClickDownloadScanCopy()
    {
        IEntity document = IUniBaseDao.instance.get().get(getListenerParameterAsLong());
        if (document instanceof PersonEduDocument && ((PersonEduDocument) document).getScanCopy() != null) {
            DatabaseFile scanCopy = ((PersonEduDocument) document).getScanCopy();
            byte[] content = scanCopy.getContent();
            if (content == null)
                throw new ApplicationException("Файл скан-копии пуст.");
            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType(scanCopy.getContentType()).fileName(scanCopy.getFilename()).document(content), true);
        }
        else if (document instanceof EnrEntrantBaseDocument && ((EnrEntrantBaseDocument) document).getDocRelation().getDocument().getScanCopy() != null) {
            try (ICommonRemoteDocumentService service = IRemoteDocumentProvider.instance.get().taskService(RemoteDocumentExtManager.PERSON_DOCUMENTS_SERVICE_NAME))
            {
                RemoteDocumentDTO scanCopy = service.get(((EnrEntrantBaseDocument) document).getDocRelation().getDocument().getScanCopy());
                if(scanCopy != null)
                {
                    BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType(scanCopy.getFileType()).fileName(scanCopy.getFileName()).document(scanCopy.getContent()), true);
                }
            }
        }
    }

    public void onClickEdit()
    {
        IEntity document = IUniBaseDao.instance.get().get(getListenerParameterAsLong());
        if (document instanceof PersonEduDocument) {
            getOwner().getActivationBuilder().asRegionDialog(EnrEntrantEduDocumentAddEdit.class)
                .parameter(EnrEntrantEduDocumentAddEditUI.BIND_ENTRANT, getEntrant().getId())
                .parameter(PersonEduDocumentAddEditUI.BIND_PERSON, getEntrant().getPerson().getId())
                .parameter(PublisherActivator.PUBLISHER_ID_KEY, document.getId())
                .activate();
        }
        else if (document instanceof PersonDocument) {
            getOwner().getActivationBuilder().asRegionDialog(PersonDocumentAdd.class)
                    .parameter(PersonDocumentManager.BIND_PERSON_ROLE_ID, getOwner().getSecureRoleContext().getSecuredObject().getId())
//                    .parameter(EnrEntrantBaseDocumentAdd.BIND_EDIT_FORM, true)
                    .parameter(PersonDocumentManager.BIND_DOCUMENT_ID, document.getId())
                    .activate();
        }
        else if (document instanceof IdentityCard) {
            getOwner().getActivationBuilder().asRegionDialog(PersonIdentityCardAddEdit.class)
            .parameter(UIPresenter.PUBLISHER_ID, getEntrant().getId())
            .parameter(PersonIdentityCardEditInline.IDENTITY_CARD_ID, document.getId())
            .parameter("birthDateCheck", getOwner().getSecureRoleContext().isHasBirthDateCheck())
            .activate();
        }
        ContextLocal.getSite().getDesktop().setRefreshScheduled(true);
    }

    public void onClickDelete()
    {
        IEntity document = IUniBaseDao.instance.get().get(getListenerParameterAsLong());
        if (document instanceof PersonEduDocument) {
            PersonEduDocumentManager.instance().dao().deleteDocument(getListenerParameterAsLong());
        }
        else if (document instanceof PersonDocument) {
            PersonDocumentManager.instance().dao().doUnlinkDocument(getListenerParameterAsLong(), getEntrant());
        }
        else if (document instanceof IdentityCard) {
            PersonManager.instance().dao().deleteIdentityCard(getListenerParameterAsLong());
        }
        ContextLocal.getSite().getDesktop().setRefreshScheduled(true);
    }
    
    // utils
    
    private IOwner getOwner() {
        return ((IOwner) getPresenter());
    }
    
    private EnrEntrant getEntrant() {
        return getOwner().getEntrant();
    }

    // getters and setters

    public StaticListDataSource<IEntity> getDataSource()
    {
        return dataSource;
    }

    public void setDataSource(StaticListDataSource<IEntity> dataSource)
    {
        this.dataSource = dataSource;
    }
}
