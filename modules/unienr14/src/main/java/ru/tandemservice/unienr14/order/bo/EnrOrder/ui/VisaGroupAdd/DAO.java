/* $Id: DAO.java 25247 2012-12-12 09:25:55Z vdanilov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unienr14.order.bo.EnrOrder.ui.VisaGroupAdd;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignOrderVisaItem;
import ru.tandemservice.unienr14.settings.entity.EnrVisasTemplate;
import ru.tandemservice.unimv.IAbstractDocument;
import ru.tandemservice.unimv.entity.visa.Visa;

import java.util.List;

/**
 * @author vip_delete
 * @since 28.07.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        EnrOrder order = getNotNull(EnrOrder.class, model.getDocumentId());

        MQBuilder builder = new MQBuilder(EnrCampaignOrderVisaItem.ENTITY_CLASS, "e", new String[]{EnrCampaignOrderVisaItem.visaTemplate().s()});
        builder.addOrder("e", EnrCampaignOrderVisaItem.visaTemplate().title());

        model.setVisaGroupListModel(new LazySimpleSelectModel<>(builder.<EnrVisasTemplate>getResultList(getSession())));
    }

    @Override
    public void update(Model model)
    {
        EnrOrder order = getNotNull(EnrOrder.class, model.getDocumentId());

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrCampaignOrderVisaItem.class, "i");
        builder.column("i");
        builder.where(DQLExpressions.eq(DQLExpressions.property("i", EnrCampaignOrderVisaItem.visaTemplate()), DQLExpressions.value(model.getVisaGroup())));
        builder.order(DQLExpressions.property("i", EnrCampaignOrderVisaItem.P_PRIORITY));
        List<EnrCampaignOrderVisaItem> itemList = builder.createStatement(new DQLExecutionContext(getSession())).list();

        IAbstractDocument document = get(model.getDocumentId());
        for (EnrCampaignOrderVisaItem visaItem : itemList)
        {
//            if (UnimvDaoFacade.getVisaDao().getVisa(document, visaItem.getPossibleVisa()) == null)
//            {
                Visa visa = new Visa();
                visa.setMandatory(false);
                visa.setPossibleVisa(visaItem.getPossibleVisa());
                visa.setDocument(document);
                visa.setGroupMemberVising(visaItem.getGroupsMemberVising());
                visa.setIndex(getCount(Visa.class, Visa.L_DOCUMENT, document));
                save(visa);
//            }
        }
    }
}
