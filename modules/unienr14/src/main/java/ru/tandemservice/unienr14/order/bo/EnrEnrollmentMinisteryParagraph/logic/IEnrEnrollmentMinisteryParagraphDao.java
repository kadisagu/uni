/* $Id$ */
package ru.tandemservice.unienr14.order.bo.EnrEnrollmentMinisteryParagraph.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.order.entity.EnrAbstractExtract;
import ru.tandemservice.unienr14.order.entity.EnrAbstractParagraph;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentMinisteryParagraph;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unienr14.request.entity.EnrEntrantForeignRequest;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.Collection;
import java.util.List;

/**
 * @author nvankov
 * @since 9/1/14
 */
public interface IEnrEnrollmentMinisteryParagraphDao extends INeedPersistenceSupport
{
    void saveOrUpdateParagraph(EnrOrder order, EnrEnrollmentMinisteryParagraph paragraph, List<EnrEntrantForeignRequest> preStudents);

    void doCreateOrders(EnrEnrollmentCampaign enrEnrollmentCampaign);

    Collection<EnrAbstractExtract> getOtherExtracts(Collection<EnrEntrant> collect, EnrAbstractParagraph paragraph);
}
