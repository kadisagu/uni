/* $Id:$ */
package ru.tandemservice.unienr14.settings.bo.EnrCampaignEnrollmentStage;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unienr14.settings.bo.EnrCampaignEnrollmentStage.logic.IEnrCampaignEnrollmentStagesDao;
import ru.tandemservice.unienr14.settings.bo.EnrCampaignEnrollmentStage.logic.EnrCampaignEnrollmentStagesDao;

/**
 * @author oleyba
 * @since 3/17/14
 */
@Configuration
public class EnrCampaignEnrollmentStageManager extends BusinessObjectManager
{
    public static EnrCampaignEnrollmentStageManager instance()
    {
        return instance(EnrCampaignEnrollmentStageManager.class);
    }

    @Bean
    public IEnrCampaignEnrollmentStagesDao dao()
    {
        return new EnrCampaignEnrollmentStagesDao();
    }
}
