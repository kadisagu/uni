package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unienr14_2x9x3_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.3")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrEnrollmentCampaignSettings

		// создано обязательное свойство accountingRulesOrigEduDoc
		{
			// создать колонку
			tool.createColumn("enr14_campaign_settings_t", new DBColumn("accountingrulesorigedudoc_p", DBType.LONG));

			// задать значение по умолчанию
			Long defaultAccountingRulesOrigEduDoc = 0L;
			tool.executeUpdate("update enr14_campaign_settings_t set accountingrulesorigedudoc_p=? where accountingrulesorigedudoc_p is null", defaultAccountingRulesOrigEduDoc);

			// сделать колонку NOT NULL
			tool.setColumnNullable("enr14_campaign_settings_t", "accountingrulesorigedudoc_p", false);

		}

		// создано обязательное свойство enrollmentRulesBudget
		{
			// создать колонку
			tool.createColumn("enr14_campaign_settings_t", new DBColumn("enrollmentrulesbudget_p", DBType.LONG));

			// задать значение по умолчанию
			Long defaultEnrollmentRulesBudget = 1L;
			tool.executeUpdate("update enr14_campaign_settings_t set enrollmentrulesbudget_p=? where enrollmentrulesbudget_p is null", defaultEnrollmentRulesBudget);

			// сделать колонку NOT NULL
			tool.setColumnNullable("enr14_campaign_settings_t", "enrollmentrulesbudget_p", false);

		}

		// создано обязательное свойство accountingRulesAgreementBudget
		{
			// создать колонку
			tool.createColumn("enr14_campaign_settings_t", new DBColumn("ccntngrlsagrmntbdgt_p", DBType.LONG));

			// задать значение по умолчанию
			Long defaultAccountingRulesAgreementBudget = 0L;
			tool.executeUpdate("update enr14_campaign_settings_t set ccntngrlsagrmntbdgt_p=? where ccntngrlsagrmntbdgt_p is null", defaultAccountingRulesAgreementBudget);

			// сделать колонку NOT NULL
			tool.setColumnNullable("enr14_campaign_settings_t", "ccntngrlsagrmntbdgt_p", false);

		}

		// создано обязательное свойство enrollmentRulesContract
		{
			// создать колонку
			tool.createColumn("enr14_campaign_settings_t", new DBColumn("enrollmentrulescontract_p", DBType.LONG));

			// задать значение по умолчанию
			Long defaultEnrollmentRulesContract = 1L;
			tool.executeUpdate("update enr14_campaign_settings_t set enrollmentrulescontract_p=? where enrollmentrulescontract_p is null", defaultEnrollmentRulesContract);

			// сделать колонку NOT NULL
			tool.setColumnNullable("enr14_campaign_settings_t", "enrollmentrulescontract_p", false);

		}

		// создано обязательное свойство accountingRulesAgreementContract
		{
			// создать колонку
			tool.createColumn("enr14_campaign_settings_t", new DBColumn("ccntngrlsagrmntcntrct_p", DBType.LONG));

			// задать значение по умолчанию
			Long defaultAccountingRulesAgreementContract = 1L;
			tool.executeUpdate("update enr14_campaign_settings_t set ccntngrlsagrmntcntrct_p=? where ccntngrlsagrmntcntrct_p is null", defaultAccountingRulesAgreementContract);

			// сделать колонку NOT NULL
			tool.setColumnNullable("enr14_campaign_settings_t", "ccntngrlsagrmntcntrct_p", false);

		}

		// изменен тип свойства enrEntrantContractExistCheckMode
		{
			// изменить тип колонки
			tool.changeColumnType("enr14_campaign_settings_t", "nrentrntcntrctexstchckmd_p", DBType.LONG);

		}


    }
}