package ru.tandemservice.unienr14.enrollment.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepCompetitionPlan;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Число мест для зачисления
 *
 * Число мест для зачисления по конкурсу в рамках шага.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrEnrollmentStepCompetitionPlanGen extends EntityBase
 implements INaturalIdentifiable<EnrEnrollmentStepCompetitionPlanGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepCompetitionPlan";
    public static final String ENTITY_NAME = "enrEnrollmentStepCompetitionPlan";
    public static final int VERSION_HASH = 1520326800;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_STEP = "enrollmentStep";
    public static final String L_COMPETITION = "competition";
    public static final String P_PLAN = "plan";

    private EnrEnrollmentStep _enrollmentStep;     // Шаг
    private EnrCompetition _competition;     // Конкурс (комбинация условий поступления) для приема
    private int _plan;     // Число мест

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Шаг. Свойство не может быть null.
     */
    @NotNull
    public EnrEnrollmentStep getEnrollmentStep()
    {
        return _enrollmentStep;
    }

    /**
     * @param enrollmentStep Шаг. Свойство не может быть null.
     */
    public void setEnrollmentStep(EnrEnrollmentStep enrollmentStep)
    {
        dirty(_enrollmentStep, enrollmentStep);
        _enrollmentStep = enrollmentStep;
    }

    /**
     * @return Конкурс (комбинация условий поступления) для приема. Свойство не может быть null.
     */
    @NotNull
    public EnrCompetition getCompetition()
    {
        return _competition;
    }

    /**
     * @param competition Конкурс (комбинация условий поступления) для приема. Свойство не может быть null.
     */
    public void setCompetition(EnrCompetition competition)
    {
        dirty(_competition, competition);
        _competition = competition;
    }

    /**
     * @return Число мест. Свойство не может быть null.
     */
    @NotNull
    public int getPlan()
    {
        return _plan;
    }

    /**
     * @param plan Число мест. Свойство не может быть null.
     */
    public void setPlan(int plan)
    {
        dirty(_plan, plan);
        _plan = plan;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrEnrollmentStepCompetitionPlanGen)
        {
            if (withNaturalIdProperties)
            {
                setEnrollmentStep(((EnrEnrollmentStepCompetitionPlan)another).getEnrollmentStep());
                setCompetition(((EnrEnrollmentStepCompetitionPlan)another).getCompetition());
            }
            setPlan(((EnrEnrollmentStepCompetitionPlan)another).getPlan());
        }
    }

    public INaturalId<EnrEnrollmentStepCompetitionPlanGen> getNaturalId()
    {
        return new NaturalId(getEnrollmentStep(), getCompetition());
    }

    public static class NaturalId extends NaturalIdBase<EnrEnrollmentStepCompetitionPlanGen>
    {
        private static final String PROXY_NAME = "EnrEnrollmentStepCompetitionPlanNaturalProxy";

        private Long _enrollmentStep;
        private Long _competition;

        public NaturalId()
        {}

        public NaturalId(EnrEnrollmentStep enrollmentStep, EnrCompetition competition)
        {
            _enrollmentStep = ((IEntity) enrollmentStep).getId();
            _competition = ((IEntity) competition).getId();
        }

        public Long getEnrollmentStep()
        {
            return _enrollmentStep;
        }

        public void setEnrollmentStep(Long enrollmentStep)
        {
            _enrollmentStep = enrollmentStep;
        }

        public Long getCompetition()
        {
            return _competition;
        }

        public void setCompetition(Long competition)
        {
            _competition = competition;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrEnrollmentStepCompetitionPlanGen.NaturalId) ) return false;

            EnrEnrollmentStepCompetitionPlanGen.NaturalId that = (NaturalId) o;

            if( !equals(getEnrollmentStep(), that.getEnrollmentStep()) ) return false;
            if( !equals(getCompetition(), that.getCompetition()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEnrollmentStep());
            result = hashCode(result, getCompetition());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEnrollmentStep());
            sb.append("/");
            sb.append(getCompetition());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrEnrollmentStepCompetitionPlanGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrEnrollmentStepCompetitionPlan.class;
        }

        public T newInstance()
        {
            return (T) new EnrEnrollmentStepCompetitionPlan();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrollmentStep":
                    return obj.getEnrollmentStep();
                case "competition":
                    return obj.getCompetition();
                case "plan":
                    return obj.getPlan();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrollmentStep":
                    obj.setEnrollmentStep((EnrEnrollmentStep) value);
                    return;
                case "competition":
                    obj.setCompetition((EnrCompetition) value);
                    return;
                case "plan":
                    obj.setPlan((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrollmentStep":
                        return true;
                case "competition":
                        return true;
                case "plan":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrollmentStep":
                    return true;
                case "competition":
                    return true;
                case "plan":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrollmentStep":
                    return EnrEnrollmentStep.class;
                case "competition":
                    return EnrCompetition.class;
                case "plan":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrEnrollmentStepCompetitionPlan> _dslPath = new Path<EnrEnrollmentStepCompetitionPlan>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrEnrollmentStepCompetitionPlan");
    }
            

    /**
     * @return Шаг. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepCompetitionPlan#getEnrollmentStep()
     */
    public static EnrEnrollmentStep.Path<EnrEnrollmentStep> enrollmentStep()
    {
        return _dslPath.enrollmentStep();
    }

    /**
     * @return Конкурс (комбинация условий поступления) для приема. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepCompetitionPlan#getCompetition()
     */
    public static EnrCompetition.Path<EnrCompetition> competition()
    {
        return _dslPath.competition();
    }

    /**
     * @return Число мест. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepCompetitionPlan#getPlan()
     */
    public static PropertyPath<Integer> plan()
    {
        return _dslPath.plan();
    }

    public static class Path<E extends EnrEnrollmentStepCompetitionPlan> extends EntityPath<E>
    {
        private EnrEnrollmentStep.Path<EnrEnrollmentStep> _enrollmentStep;
        private EnrCompetition.Path<EnrCompetition> _competition;
        private PropertyPath<Integer> _plan;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Шаг. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepCompetitionPlan#getEnrollmentStep()
     */
        public EnrEnrollmentStep.Path<EnrEnrollmentStep> enrollmentStep()
        {
            if(_enrollmentStep == null )
                _enrollmentStep = new EnrEnrollmentStep.Path<EnrEnrollmentStep>(L_ENROLLMENT_STEP, this);
            return _enrollmentStep;
        }

    /**
     * @return Конкурс (комбинация условий поступления) для приема. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepCompetitionPlan#getCompetition()
     */
        public EnrCompetition.Path<EnrCompetition> competition()
        {
            if(_competition == null )
                _competition = new EnrCompetition.Path<EnrCompetition>(L_COMPETITION, this);
            return _competition;
        }

    /**
     * @return Число мест. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepCompetitionPlan#getPlan()
     */
        public PropertyPath<Integer> plan()
        {
            if(_plan == null )
                _plan = new PropertyPath<Integer>(EnrEnrollmentStepCompetitionPlanGen.P_PLAN, this);
            return _plan;
        }

        public Class getEntityClass()
        {
            return EnrEnrollmentStepCompetitionPlan.class;
        }

        public String getEntityName()
        {
            return "enrEnrollmentStepCompetitionPlan";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
