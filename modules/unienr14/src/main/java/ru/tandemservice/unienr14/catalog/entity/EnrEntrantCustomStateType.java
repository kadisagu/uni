package ru.tandemservice.unienr14.catalog.entity;

import com.google.common.collect.ImmutableList;
import org.apache.cxf.common.util.StringUtils;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.BaseDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import ru.tandemservice.uni.entity.catalog.IColoredEntity;
import ru.tandemservice.unienr14.catalog.bo.EnrEntrantCustomStateType.ui.AddEdit.EnrEntrantCustomStateTypeAddEdit;
import ru.tandemservice.unienr14.catalog.entity.gen.EnrEntrantCustomStateTypeGen;

import java.util.Collection;
import java.util.List;

/**
 * Вид дополнительного статуса абитуриента
 */
public class EnrEntrantCustomStateType extends EnrEntrantCustomStateTypeGen implements IDynamicCatalogItem, IColoredEntity
{
    private static List<String> HIDDEN_PROPERTIES = ImmutableList.of(EnrEntrantCustomStateType.P_HTML_COLOR);

    public static IDynamicCatalogDesc getUiDesc() {
        return new BaseDynamicCatalogDesc() {

            @Override public Collection<String> getHiddenFields() { return HIDDEN_PROPERTIES; }

            @Override
            public Collection<String> getHiddenFieldsAddEditForm() {
                return ImmutableList.of(EnrEntrantCustomStateType.P_HTML_COLOR, EnrEntrantCustomStateType.P_DESCRIPTION);
            }

            @Override public void addAdditionalColumns(DynamicListDataSource<ICatalogItem> dataSource) {
                dataSource.addColumn(new SimpleColumn("Цвет", EnrEntrantCustomStateType.P_HTML_COLOR)
                        .setStyleResolver(IColoredEntity.COLORED_STYLE_RESOLVER).setOrderable(false).setClickable(false).setWidth(10));
            }

            @Override public String getAddEditComponentName() { return EnrEntrantCustomStateTypeAddEdit.class.getSimpleName(); }
        };
    }

    public String getColorExampleSpan()
    {
        String example;
        if (null == getTitle())
            example = "Пример";
        else
            example = getTitle();
        if(!StringUtils.isEmpty(getHtmlColor()))
        {
            if (getHtmlColor().matches("^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$"))
                return "<span style=\"color:" + getHtmlColor() + ";\" >" + example + "</span>";
            else
                return example;
        }
        else
            return example;
    }


    public static EntityComboDataSourceHandler defaultSelectDSHandler(String ownerId)
    {
        return new EntityComboDataSourceHandler(ownerId, EnrEntrantCustomStateType.class)
                .titleProperty(EnrEntrantCustomStateType.title().s())
                .filter(EnrEntrantCustomStateType.title())
                .order(EnrEntrantCustomStateType.title());
    }
}