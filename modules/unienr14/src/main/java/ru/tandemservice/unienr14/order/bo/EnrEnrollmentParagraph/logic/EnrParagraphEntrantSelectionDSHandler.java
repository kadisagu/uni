/* $Id:$ */
package ru.tandemservice.unienr14.order.bo.EnrEnrollmentParagraph.logic;

import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantCustomStateType;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantStateCodes;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.logic.CitizenshipModel;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantCustomState;
import ru.tandemservice.unienr14.order.bo.EnrEnrollmentParagraph.EnrEnrollmentParagraphManager;
import ru.tandemservice.unienr14.order.bo.EnrEnrollmentParagraph.ui.AddEdit.EnrEnrollmentParagraphAddEdit;
import ru.tandemservice.unienr14.order.entity.EnrAbstractExtract;
import ru.tandemservice.unienr14.order.entity.EnrAbstractParagraph;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 7/7/14
 */
public class EnrParagraphEntrantSelectionDSHandler extends DefaultSearchDataSourceHandler
{
    public static final String OTHER_EXTRACTS = "otherExtracts";
    public static final String CUSTOM_STATES = "customStates";

    public EnrParagraphEntrantSelectionDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final EnrCompetitionType competitionType = context.get(EnrEnrollmentParagraphAddEdit.BIND_COMPETITION_TYPE);

        final DQLOrderDescriptionRegistry orderDescriptionRegistry = new DQLOrderDescriptionRegistry(EnrRequestedCompetition.class, "r");
        final DQLSelectBuilder dql = orderDescriptionRegistry.buildDQLSelectBuilder().column(property("r"))
            .joinPath(DQLJoinType.inner, EnrRequestedCompetition.competition().fromAlias("r"), "c")
            .where(eq(property("r", EnrRequestedCompetition.parallel()), commonValue(context.get(EnrEnrollmentParagraphAddEdit.BIND_PARALLEL))))
            .where(eq(property("c", EnrCompetition.programSetOrgUnit().programSet().enrollmentCampaign()), commonValue(context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN))))
            .where(eq(property("c", EnrCompetition.requestType()), commonValue(context.get(EnrEnrollmentParagraphAddEdit.BIND_REQUEST_TYPE))))
            .where(eq(property("c", EnrCompetition.type().compensationType()), commonValue(context.get(EnrEnrollmentParagraphAddEdit.BIND_COMPENSATION_TYPE))))
            .where(eq(property("c", EnrCompetition.programSetOrgUnit().programSet().programForm()), commonValue(context.get(EnrEnrollmentParagraphAddEdit.BIND_FORM))))
            .where(eq(property("c", EnrCompetition.programSetOrgUnit().orgUnit()), commonValue(context.get(EnrEnrollmentParagraphAddEdit.BIND_ORG_UNIT))))
            .where(eq(property("c", EnrCompetition.programSetOrgUnit().programSet().programSubject()), commonValue(context.get(EnrEnrollmentParagraphAddEdit.BIND_SUBJECT))))
            .where(eq(property("c", EnrCompetition.type()), value(competitionType)))
        ;

        OrgUnit formativeOrgUnit = context.get(EnrEnrollmentParagraphAddEdit.BIND_FORMATIVE_ORG_UNIT);
        List programSetList = context.get(EnrEnrollmentParagraphAddEdit.BIND_PROGRAM_SET_LIST);
        List competitionList = context.get(EnrEnrollmentParagraphAddEdit.BIND_COMPETITION_LIST);
        List targetAdmissionKindList = context.get(EnrEnrollmentParagraphAddEdit.BIND_TARGET_ADMISIION_KIND_LIST);
        Long citizenshipId = context.get(EnrEnrollmentParagraphAddEdit.BIND_CITIZENSHIP_ID);
        Boolean isOriginalHandIn = context.getBoolean(EnrEnrollmentParagraphAddEdit.BIND_ORIGINAL_HAND_IN, false);
        Boolean isEnrollmentAccepted = context.getBoolean(EnrEnrollmentParagraphAddEdit.BIND_ENROLLMENT_ACCEPTED, false);
        Boolean isEnrollmentAvailable = context.getBoolean(EnrEnrollmentParagraphAddEdit.BIND_ENROLLMENT_AVAILABLE, false);
        List<EnrEntrantCustomStateType> statusList = context.get(EnrEnrollmentParagraphAddEdit.BIND_CUSTOM_STATE);

        if (formativeOrgUnit != null) {
            dql.where(eq(property("c", EnrCompetition.programSetOrgUnit().formativeOrgUnit()), value(formativeOrgUnit)));
        }
        if (programSetList != null && !programSetList.isEmpty()) {
            dql.where(in(property("c", EnrCompetition.programSetOrgUnit().programSet()), programSetList));
        }
        if (competitionList != null && !competitionList.isEmpty()) {
            dql.where(in(property("r", EnrRequestedCompetition.competition()), competitionList));
        }
        if (targetAdmissionKindList != null && !targetAdmissionKindList.isEmpty() && competitionType != null && EnrCompetitionTypeCodes.TARGET_ADMISSION.equals(competitionType.getCode())) {
            dql.joinEntity("r", DQLJoinType.inner, EnrRequestedCompetitionTA.class, "rTA", eq("r", "rTA"));
            dql.where(in(property("rTA", EnrRequestedCompetitionTA.targetAdmissionKind().targetAdmissionKind()), targetAdmissionKindList));
        }
        if (citizenshipId != null) {
            if (citizenshipId.equals(CitizenshipModel.NO_RUSSIAN_CITIZENSHIP))
                dql.where(ne(property("r", EnrRequestedCompetition.request().identityCard().citizenship().code()), value(IKladrDefines.RUSSIA_COUNTRY_CODE)));
            else
                dql.where(eqValue(property("r", EnrRequestedCompetition.request().identityCard().citizenship()), citizenshipId));
        }

        if (statusList != null && !statusList.isEmpty())
        {
            dql.joinPath(DQLJoinType.inner, EnrRequestedCompetition.request().entrant().fromAlias("r"), "entrant")
                    .where(exists(new DQLSelectBuilder()
                                          .fromEntity(EnrEntrantCustomState.class, "st")
                                          .where(in(property("st", EnrEntrantCustomState.customState()), statusList))
                                          .where(eq(property("st", EnrEntrantCustomState.entrant()), property("entrant")))
                                          .buildQuery()));
        }

        if (Boolean.TRUE.equals(context.get(EnrEnrollmentParagraphAddEdit.BIND_ONLY_ENROLLED_MARKED))) {
            dql.where(or(
                eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.SELECT_SHOULD_BE_ENROLLED)),
                eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.IN_ORDER))
            ));
        } else {
            dql.where(or(
                eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.SELECT_SHOULD_BE_ENROLLED)),
                eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.IN_ORDER)),
                eq(property("r", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.EXAMS_PASSED))
            ));
        }

        if(isOriginalHandIn)
            dql.where(eq(property("r", EnrRequestedCompetition.originalDocumentHandedIn()), value(true)));

        if(isEnrollmentAccepted)
            dql.where(eq(property("r", EnrRequestedCompetition.accepted()), value(true)));

        if(isEnrollmentAvailable)
            dql.where(eq(property("r", EnrRequestedCompetition.enrollmentAvailable()), value(true)));

        orderDescriptionRegistry.applyOrder(dql, input.getEntityOrder());

        List<EnrRequestedCompetition> requestedCompetitions = dql.createStatement(context.getSession()).list();
        Collection<EnrEntrant> entrants = CollectionUtils.collect(requestedCompetitions, enrRequestedCompetition -> enrRequestedCompetition.getRequest().getEntrant());

        Collection<EnrAbstractExtract> otherExtracts = EnrEnrollmentParagraphManager.instance().dao().getOtherExtracts(entrants, context.<EnrAbstractParagraph>get(EnrEnrollmentParagraphAddEdit.BIND_PARAGRAPH));
        Map<Long, List<String>> otherExtractMap = SafeMap.get(ArrayList.class);
        for (EnrAbstractExtract extract: otherExtracts)
        {
            if(extract.getRequestedCompetition() != null)
                otherExtractMap.get(extract.getRequestedCompetition().getId()).add(extract.getShortTitle());
        }

        Map<EnrEntrant, List<EnrEntrantCustomState>> customStatesMap = EnrEntrantManager.instance().dao().getActiveCustomStatesMap(entrants, new Date());

        List<DataWrapper> result = new ArrayList<>();
        for (EnrRequestedCompetition requestedCompetition: requestedCompetitions)
        {
            DataWrapper record = new DataWrapper(requestedCompetition);
            record.setProperty(OTHER_EXTRACTS, otherExtractMap.get(requestedCompetition.getId()));
            record.setProperty(CUSTOM_STATES, customStatesMap.get(requestedCompetition.getRequest().getEntrant()));
            result.add(record);
        }

        DSOutput output = ListOutputBuilder.get(input, result).pageable(false).build().ordering(new EntityComparator(
            new EntityOrder(EnrRequestedCompetition.request().entrant().person().fullFio(), OrderDirection.asc),
            new EntityOrder(EnrRequestedCompetition.id(), OrderDirection.asc)));

        output.setCountRecord(Math.max(1, output.getRecordList().size()));

        return output;
    }

}
