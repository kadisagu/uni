/* $Id:$ */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.ui.SecTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.sec.entity.Role;
import org.tandemframework.shared.organization.sec.bo.Sec.SecManager;

/**
 * @author oleyba
 * @since 5/19/15
 */
@Configuration
public class EnrEnrollmentCommissionSecTab extends BusinessComponentManager
{
    public static final String ROLE_STATE_DS = "roleStateDS";
    public static final String LOCAL_ROLE_DS = "localRoleDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ROLE_STATE_DS, SecManager.instance().roleStateComboDSHandler()))
                .addDataSource(searchListDS(LOCAL_ROLE_DS, localRoleDS(), localRoleDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint localRoleDS()
    {
        return columnListExtPointBuilder(LOCAL_ROLE_DS)
                .addColumn(publisherColumn("name", Role.title()))
                .addColumn(toggleColumn("active", Role.active()).toggleOffLabel("Роль выключена").toggleOffListener("onClickTurnOffRole").toggleOnLabel("Роль включена").toggleOnListener("onClickTurnOnRole"))
                .addColumn(actionColumn("export", CommonDefines.ICON_IMPORT, "onClickExportRole"))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).permissionKey("ui:editPermissionKey"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER, alert("localRoleDS.delete.alert", Role.title())).permissionKey("ui:deletePermissionKey").create())
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> localRoleDSHandler()
    {
        return new LocalRoleDSHandler(getName());
    }
}