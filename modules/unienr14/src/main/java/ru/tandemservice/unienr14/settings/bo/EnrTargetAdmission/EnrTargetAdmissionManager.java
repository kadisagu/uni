/**
 *$Id: EnrTargetAdmissionManager.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.settings.bo.EnrTargetAdmission;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrTargetAdmissionKind;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.competition.entity.EnrTargetAdmissionPlan;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrTargetAdmission.logic.EnrCampTargetAdmissionKindDao;
import ru.tandemservice.unienr14.settings.bo.EnrTargetAdmission.logic.IEnrCampTargetAdmissionKindDao;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignTargetAdmissionKind;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 15.04.13
 */
@Configuration
public class EnrTargetAdmissionManager extends BusinessObjectManager
{
    public static final String USED_T_A_KINDS_SELECT_DS = "usedTaKindsSelectDS";
    public static final String CAMPAIGN_T_A_KINDS_SELECT_DS = "campaignTaKindsSelectDS";

    public static final String BIND_PROGRAM_SET_ORG_UNIT = "programSetOrgUnit";

    public static EnrTargetAdmissionManager instance()
    {
        return instance(EnrTargetAdmissionManager.class);
    }

    @Bean
    public IEnrCampTargetAdmissionKindDao dao()
    {
        return new EnrCampTargetAdmissionKindDao();
    }

    /**
     * Селект используемых в рамках ПК Видов целевого приема.
     */
    @Bean
    public UIDataSourceConfig usedTaKindsSelectDSConfig()
    {
        return SelectDSConfig.with(USED_T_A_KINDS_SELECT_DS, getName())
        .dataSourceClass(SelectDataSource.class)
        .handler(usedTaKindsSelectDSHandler())
        .create();
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> usedTaKindsSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrTargetAdmissionKind.class)
        {
            @Override
            protected void applyWhereConditions(final String alias, final DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                final EnrEnrollmentCampaign enrCamp = context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);

                BatchUtils.execute(UniBaseDao.ids(EnrTargetAdmissionManager.instance().dao().getUsedTaKinds(enrCamp)), 128, new BatchUtils.Action<Long>()
                {
                    @Override
                    @SuppressWarnings("unchecked")
                    public void execute(Collection<Long> elements)
                    {
                        dql.where(in(property(EnrTargetAdmissionKind.id().fromAlias(alias)), new ArrayList(elements)));
                    }
                });
            }
        }
                .order(EnrTargetAdmissionKind.priority())
                .filter(EnrTargetAdmissionKind.title())
                .pageable(true);
    }

    @Bean
    public UIDataSourceConfig enrCampaignTaKindsSelectDSConfig()
    {
        return SelectDSConfig.with(CAMPAIGN_T_A_KINDS_SELECT_DS, getName())
            .addColumn(EnrCampaignTargetAdmissionKind.targetAdmissionKind().title().s())
            .dataSourceClass(SelectDataSource.class)
            .handler(enrCampaignTaKindsSelectDSHandler())
            .create();
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> enrCampaignTaKindsSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrCampaignTargetAdmissionKind.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                EnrEnrollmentCampaign enrCampaign = context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
                EnrProgramSetOrgUnit programSetOrgUnit = context.get(BIND_PROGRAM_SET_ORG_UNIT);

                if (enrCampaign.getSettings().isTargetAdmissionCompetition())
                {
                    dql.where(exists(
                            new DQLSelectBuilder().fromEntity(EnrTargetAdmissionPlan.class, "ap")
                                    .where(eq(property("ap", EnrTargetAdmissionPlan.enrCampaignTAKind()), property(alias)))
                                    .where(eq(property("ap", EnrTargetAdmissionPlan.programSetOrgUnit()), value(programSetOrgUnit)))
                                    .buildQuery()
                    ));
                }
            }
        }
            .where(EnrCampaignTargetAdmissionKind.enrollmentCampaign(), EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN)
            .order(EnrCampaignTargetAdmissionKind.targetAdmissionKind().priority())
            .filter(EnrCampaignTargetAdmissionKind.targetAdmissionKind().title())
            .pageable(true);
    }
}
