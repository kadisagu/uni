/**
 *$Id: EnrExamGroupSetScheduleEventTime.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.exams.bo.EnrExamGroup.ui.SetScheduleEventTime;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.core.view.list.column.IMergeRowIdResolver;
import ru.tandemservice.unienr14.exams.bo.EnrExamGroup.logic.EnrExamGroupSetScheduleEventTimeSearchDSHandler;
import ru.tandemservice.unienr14.exams.entity.EnrExamScheduleEvent;

/**
 * @author Alexander Shaburov
 * @since 03.06.13
 */
@Configuration
public class EnrExamGroupSetScheduleEventTime extends BusinessComponentManager
{
    public static final String EXAM_GROUP_SET_SCHEDULE_EVENT_TIME_SEARCHDS = "examGroupSetScheduleEventTimeSearchDS";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(EXAM_GROUP_SET_SCHEDULE_EVENT_TIME_SEARCHDS, examGroupSetScheduleEventTimeSearchDSColumns(), examGroupSetScheduleEventTimeSearchDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint examGroupSetScheduleEventTimeSearchDSColumns()
    {
        final IMergeRowIdResolver dateMerge = entity -> {
            EnrExamScheduleEvent scheduleEvent = ((DataWrapper) entity).getWrapped();
            return String.valueOf(CoreDateUtils.getDayFirstTimeMoment(scheduleEvent.getScheduleEvent().getDurationBegin()).getTime());
        };

        return columnListExtPointBuilder(EXAM_GROUP_SET_SCHEDULE_EVENT_TIME_SEARCHDS)
                .addColumn(radioColumn("radio").visible("mvel:presenter.onceDays").controlInHeader(false))
                .addColumn(checkboxColumn("check").visible("mvel:!presenter.onceDays").controlInHeader(false))
                .addColumn(textColumn("eventDate", EnrExamScheduleEvent.scheduleEvent().dateWithDayOfWeek()).merger(dateMerge))
                .addColumn(textColumn("eventPeriod", EnrExamScheduleEvent.scheduleEvent().timePeriodStr()))
                .addColumn(textColumn("place", EnrExamScheduleEvent.examRoom().placeLocationWithSize()))
                .addColumn(textColumn("commission", EnrExamScheduleEvent.commission()))
                .addColumn(textColumn("examGroups", EnrExamGroupSetScheduleEventTimeSearchDSHandler.V_PROP_EXAM_GROUPS).formatter(CollectionFormatter.ROW_FORMATTER))
                .addColumn(textColumn("roomOccupied", EnrExamGroupSetScheduleEventTimeSearchDSHandler.V_PROP_ROOM_OCCUPIED_STR).styleResolver(rowEntity -> {
                    final DataWrapper wrapper = (DataWrapper) rowEntity;
                    final Boolean exceed = (Boolean) wrapper.getProperty(EnrExamGroupSetScheduleEventTimeSearchDSHandler.V_PROP_SIZE_EXCEED);
                    return exceed ? "background-color:#ffcccc" : null;
                }))
                .create();
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> examGroupSetScheduleEventTimeSearchDSHandler()
    {
        return new EnrExamGroupSetScheduleEventTimeSearchDSHandler(getName());
    }
}
