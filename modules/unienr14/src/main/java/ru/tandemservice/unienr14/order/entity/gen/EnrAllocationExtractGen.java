package ru.tandemservice.unienr14.order.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unienr14.order.entity.EnrAbstractExtract;
import ru.tandemservice.unienr14.order.entity.EnrAllocationExtract;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка приказа о распределении по образовательным программам
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrAllocationExtractGen extends EnrAbstractExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.order.entity.EnrAllocationExtract";
    public static final String ENTITY_NAME = "enrAllocationExtract";
    public static final int VERSION_HASH = 1254276271;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTITY = "entity";
    public static final String P_GROUP_TITLE = "groupTitle";
    public static final String L_STUDENT = "student";

    private EnrRequestedCompetition _entity;     // Выбранный конкурс
    private String _groupTitle;     // Зачислить в группу
    private Student _student;     // Студент

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выбранный конкурс. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EnrRequestedCompetition getEntity()
    {
        return _entity;
    }

    /**
     * @param entity Выбранный конкурс. Свойство не может быть null и должно быть уникальным.
     */
    public void setEntity(EnrRequestedCompetition entity)
    {
        dirty(_entity, entity);
        _entity = entity;
    }

    /**
     * @return Зачислить в группу.
     */
    @Length(max=255)
    public String getGroupTitle()
    {
        return _groupTitle;
    }

    /**
     * @param groupTitle Зачислить в группу.
     */
    public void setGroupTitle(String groupTitle)
    {
        dirty(_groupTitle, groupTitle);
        _groupTitle = groupTitle;
    }

    /**
     * @return Студент. Свойство должно быть уникальным.
     */
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство должно быть уникальным.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EnrAllocationExtractGen)
        {
            setEntity(((EnrAllocationExtract)another).getEntity());
            setGroupTitle(((EnrAllocationExtract)another).getGroupTitle());
            setStudent(((EnrAllocationExtract)another).getStudent());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrAllocationExtractGen> extends EnrAbstractExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrAllocationExtract.class;
        }

        public T newInstance()
        {
            return (T) new EnrAllocationExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "entity":
                    return obj.getEntity();
                case "groupTitle":
                    return obj.getGroupTitle();
                case "student":
                    return obj.getStudent();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "entity":
                    obj.setEntity((EnrRequestedCompetition) value);
                    return;
                case "groupTitle":
                    obj.setGroupTitle((String) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "entity":
                        return true;
                case "groupTitle":
                        return true;
                case "student":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "entity":
                    return true;
                case "groupTitle":
                    return true;
                case "student":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "entity":
                    return EnrRequestedCompetition.class;
                case "groupTitle":
                    return String.class;
                case "student":
                    return Student.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrAllocationExtract> _dslPath = new Path<EnrAllocationExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrAllocationExtract");
    }
            

    /**
     * @return Выбранный конкурс. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.order.entity.EnrAllocationExtract#getEntity()
     */
    public static EnrRequestedCompetition.Path<EnrRequestedCompetition> entity()
    {
        return _dslPath.entity();
    }

    /**
     * @return Зачислить в группу.
     * @see ru.tandemservice.unienr14.order.entity.EnrAllocationExtract#getGroupTitle()
     */
    public static PropertyPath<String> groupTitle()
    {
        return _dslPath.groupTitle();
    }

    /**
     * @return Студент. Свойство должно быть уникальным.
     * @see ru.tandemservice.unienr14.order.entity.EnrAllocationExtract#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    public static class Path<E extends EnrAllocationExtract> extends EnrAbstractExtract.Path<E>
    {
        private EnrRequestedCompetition.Path<EnrRequestedCompetition> _entity;
        private PropertyPath<String> _groupTitle;
        private Student.Path<Student> _student;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выбранный конкурс. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.order.entity.EnrAllocationExtract#getEntity()
     */
        public EnrRequestedCompetition.Path<EnrRequestedCompetition> entity()
        {
            if(_entity == null )
                _entity = new EnrRequestedCompetition.Path<EnrRequestedCompetition>(L_ENTITY, this);
            return _entity;
        }

    /**
     * @return Зачислить в группу.
     * @see ru.tandemservice.unienr14.order.entity.EnrAllocationExtract#getGroupTitle()
     */
        public PropertyPath<String> groupTitle()
        {
            if(_groupTitle == null )
                _groupTitle = new PropertyPath<String>(EnrAllocationExtractGen.P_GROUP_TITLE, this);
            return _groupTitle;
        }

    /**
     * @return Студент. Свойство должно быть уникальным.
     * @see ru.tandemservice.unienr14.order.entity.EnrAllocationExtract#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

        public Class getEntityClass()
        {
            return EnrAllocationExtract.class;
        }

        public String getEntityName()
        {
            return "enrAllocationExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
