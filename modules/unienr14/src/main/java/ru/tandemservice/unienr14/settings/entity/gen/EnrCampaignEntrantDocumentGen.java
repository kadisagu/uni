package ru.tandemservice.unienr14.settings.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.person.catalog.entity.PersonDocumentType;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignEntrantDocument;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Тип документа абитуриента, используемый в рамках ПК
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrCampaignEntrantDocumentGen extends EntityBase
 implements INaturalIdentifiable<EnrCampaignEntrantDocumentGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.settings.entity.EnrCampaignEntrantDocument";
    public static final String ENTITY_NAME = "enrCampaignEntrantDocument";
    public static final int VERSION_HASH = 1004125033;
    private static IEntityMeta ENTITY_META;

    public static final String L_DOCUMENT_TYPE = "documentType";
    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_BENEFIT_NO_EXAMS = "benefitNoExams";
    public static final String P_BENEFIT_EXCLUSIVE = "benefitExclusive";
    public static final String P_BENEFIT_PREFERENCE = "benefitPreference";
    public static final String P_BENEFIT_MAX_MARK = "benefitMaxMark";
    public static final String P_ACHIEVEMENT = "achievement";
    public static final String P_COPY_CAN_BE_ACCEPTED = "copyCanBeAccepted";
    public static final String P_ORIGINAL_CAN_BE_ACCEPTED = "originalCanBeAccepted";
    public static final String P_SEND_F_I_S = "sendFIS";

    private PersonDocumentType _documentType;     // Тип документа
    private EnrEnrollmentCampaign _enrollmentCampaign;     // ПК
    private boolean _benefitNoExams;     // Подтверждает особое право - без ВИ
    private boolean _benefitExclusive;     // Подтверждает особое право - квота
    private boolean _benefitPreference;     // Подтверждает особое право - преимущество
    private boolean _benefitMaxMark;     // Подтверждает особое право - 100 баллов
    private boolean _achievement;     // Подтверждает индивидуальное достижение
    private boolean _copyCanBeAccepted;     // Разрешено принимать копии
    private boolean _originalCanBeAccepted;     // Разрешено принимать оригинал
    private boolean _sendFIS = true;     // Передавать в ФИС ГИА и приема

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Тип документа. Свойство не может быть null.
     */
    @NotNull
    public PersonDocumentType getDocumentType()
    {
        return _documentType;
    }

    /**
     * @param documentType Тип документа. Свойство не может быть null.
     */
    public void setDocumentType(PersonDocumentType documentType)
    {
        dirty(_documentType, documentType);
        _documentType = documentType;
    }

    /**
     * @return ПК. Свойство не может быть null.
     */
    @NotNull
    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign ПК. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Подтверждает особое право - без ВИ. Свойство не может быть null.
     */
    @NotNull
    public boolean isBenefitNoExams()
    {
        return _benefitNoExams;
    }

    /**
     * @param benefitNoExams Подтверждает особое право - без ВИ. Свойство не может быть null.
     */
    public void setBenefitNoExams(boolean benefitNoExams)
    {
        dirty(_benefitNoExams, benefitNoExams);
        _benefitNoExams = benefitNoExams;
    }

    /**
     * @return Подтверждает особое право - квота. Свойство не может быть null.
     */
    @NotNull
    public boolean isBenefitExclusive()
    {
        return _benefitExclusive;
    }

    /**
     * @param benefitExclusive Подтверждает особое право - квота. Свойство не может быть null.
     */
    public void setBenefitExclusive(boolean benefitExclusive)
    {
        dirty(_benefitExclusive, benefitExclusive);
        _benefitExclusive = benefitExclusive;
    }

    /**
     * @return Подтверждает особое право - преимущество. Свойство не может быть null.
     */
    @NotNull
    public boolean isBenefitPreference()
    {
        return _benefitPreference;
    }

    /**
     * @param benefitPreference Подтверждает особое право - преимущество. Свойство не может быть null.
     */
    public void setBenefitPreference(boolean benefitPreference)
    {
        dirty(_benefitPreference, benefitPreference);
        _benefitPreference = benefitPreference;
    }

    /**
     * @return Подтверждает особое право - 100 баллов. Свойство не может быть null.
     */
    @NotNull
    public boolean isBenefitMaxMark()
    {
        return _benefitMaxMark;
    }

    /**
     * @param benefitMaxMark Подтверждает особое право - 100 баллов. Свойство не может быть null.
     */
    public void setBenefitMaxMark(boolean benefitMaxMark)
    {
        dirty(_benefitMaxMark, benefitMaxMark);
        _benefitMaxMark = benefitMaxMark;
    }

    /**
     * @return Подтверждает индивидуальное достижение. Свойство не может быть null.
     */
    @NotNull
    public boolean isAchievement()
    {
        return _achievement;
    }

    /**
     * @param achievement Подтверждает индивидуальное достижение. Свойство не может быть null.
     */
    public void setAchievement(boolean achievement)
    {
        dirty(_achievement, achievement);
        _achievement = achievement;
    }

    /**
     * @return Разрешено принимать копии. Свойство не может быть null.
     */
    @NotNull
    public boolean isCopyCanBeAccepted()
    {
        return _copyCanBeAccepted;
    }

    /**
     * @param copyCanBeAccepted Разрешено принимать копии. Свойство не может быть null.
     */
    public void setCopyCanBeAccepted(boolean copyCanBeAccepted)
    {
        dirty(_copyCanBeAccepted, copyCanBeAccepted);
        _copyCanBeAccepted = copyCanBeAccepted;
    }

    /**
     * @return Разрешено принимать оригинал. Свойство не может быть null.
     */
    @NotNull
    public boolean isOriginalCanBeAccepted()
    {
        return _originalCanBeAccepted;
    }

    /**
     * @param originalCanBeAccepted Разрешено принимать оригинал. Свойство не может быть null.
     */
    public void setOriginalCanBeAccepted(boolean originalCanBeAccepted)
    {
        dirty(_originalCanBeAccepted, originalCanBeAccepted);
        _originalCanBeAccepted = originalCanBeAccepted;
    }

    /**
     * @return Передавать в ФИС ГИА и приема. Свойство не может быть null.
     */
    @NotNull
    public boolean isSendFIS()
    {
        return _sendFIS;
    }

    /**
     * @param sendFIS Передавать в ФИС ГИА и приема. Свойство не может быть null.
     */
    public void setSendFIS(boolean sendFIS)
    {
        dirty(_sendFIS, sendFIS);
        _sendFIS = sendFIS;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrCampaignEntrantDocumentGen)
        {
            if (withNaturalIdProperties)
            {
                setDocumentType(((EnrCampaignEntrantDocument)another).getDocumentType());
                setEnrollmentCampaign(((EnrCampaignEntrantDocument)another).getEnrollmentCampaign());
            }
            setBenefitNoExams(((EnrCampaignEntrantDocument)another).isBenefitNoExams());
            setBenefitExclusive(((EnrCampaignEntrantDocument)another).isBenefitExclusive());
            setBenefitPreference(((EnrCampaignEntrantDocument)another).isBenefitPreference());
            setBenefitMaxMark(((EnrCampaignEntrantDocument)another).isBenefitMaxMark());
            setAchievement(((EnrCampaignEntrantDocument)another).isAchievement());
            setCopyCanBeAccepted(((EnrCampaignEntrantDocument)another).isCopyCanBeAccepted());
            setOriginalCanBeAccepted(((EnrCampaignEntrantDocument)another).isOriginalCanBeAccepted());
            setSendFIS(((EnrCampaignEntrantDocument)another).isSendFIS());
        }
    }

    public INaturalId<EnrCampaignEntrantDocumentGen> getNaturalId()
    {
        return new NaturalId(getDocumentType(), getEnrollmentCampaign());
    }

    public static class NaturalId extends NaturalIdBase<EnrCampaignEntrantDocumentGen>
    {
        private static final String PROXY_NAME = "EnrCampaignEntrantDocumentNaturalProxy";

        private Long _documentType;
        private Long _enrollmentCampaign;

        public NaturalId()
        {}

        public NaturalId(PersonDocumentType documentType, EnrEnrollmentCampaign enrollmentCampaign)
        {
            _documentType = ((IEntity) documentType).getId();
            _enrollmentCampaign = ((IEntity) enrollmentCampaign).getId();
        }

        public Long getDocumentType()
        {
            return _documentType;
        }

        public void setDocumentType(Long documentType)
        {
            _documentType = documentType;
        }

        public Long getEnrollmentCampaign()
        {
            return _enrollmentCampaign;
        }

        public void setEnrollmentCampaign(Long enrollmentCampaign)
        {
            _enrollmentCampaign = enrollmentCampaign;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrCampaignEntrantDocumentGen.NaturalId) ) return false;

            EnrCampaignEntrantDocumentGen.NaturalId that = (NaturalId) o;

            if( !equals(getDocumentType(), that.getDocumentType()) ) return false;
            if( !equals(getEnrollmentCampaign(), that.getEnrollmentCampaign()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getDocumentType());
            result = hashCode(result, getEnrollmentCampaign());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getDocumentType());
            sb.append("/");
            sb.append(getEnrollmentCampaign());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrCampaignEntrantDocumentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrCampaignEntrantDocument.class;
        }

        public T newInstance()
        {
            return (T) new EnrCampaignEntrantDocument();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "documentType":
                    return obj.getDocumentType();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "benefitNoExams":
                    return obj.isBenefitNoExams();
                case "benefitExclusive":
                    return obj.isBenefitExclusive();
                case "benefitPreference":
                    return obj.isBenefitPreference();
                case "benefitMaxMark":
                    return obj.isBenefitMaxMark();
                case "achievement":
                    return obj.isAchievement();
                case "copyCanBeAccepted":
                    return obj.isCopyCanBeAccepted();
                case "originalCanBeAccepted":
                    return obj.isOriginalCanBeAccepted();
                case "sendFIS":
                    return obj.isSendFIS();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "documentType":
                    obj.setDocumentType((PersonDocumentType) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrEnrollmentCampaign) value);
                    return;
                case "benefitNoExams":
                    obj.setBenefitNoExams((Boolean) value);
                    return;
                case "benefitExclusive":
                    obj.setBenefitExclusive((Boolean) value);
                    return;
                case "benefitPreference":
                    obj.setBenefitPreference((Boolean) value);
                    return;
                case "benefitMaxMark":
                    obj.setBenefitMaxMark((Boolean) value);
                    return;
                case "achievement":
                    obj.setAchievement((Boolean) value);
                    return;
                case "copyCanBeAccepted":
                    obj.setCopyCanBeAccepted((Boolean) value);
                    return;
                case "originalCanBeAccepted":
                    obj.setOriginalCanBeAccepted((Boolean) value);
                    return;
                case "sendFIS":
                    obj.setSendFIS((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "documentType":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "benefitNoExams":
                        return true;
                case "benefitExclusive":
                        return true;
                case "benefitPreference":
                        return true;
                case "benefitMaxMark":
                        return true;
                case "achievement":
                        return true;
                case "copyCanBeAccepted":
                        return true;
                case "originalCanBeAccepted":
                        return true;
                case "sendFIS":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "documentType":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "benefitNoExams":
                    return true;
                case "benefitExclusive":
                    return true;
                case "benefitPreference":
                    return true;
                case "benefitMaxMark":
                    return true;
                case "achievement":
                    return true;
                case "copyCanBeAccepted":
                    return true;
                case "originalCanBeAccepted":
                    return true;
                case "sendFIS":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "documentType":
                    return PersonDocumentType.class;
                case "enrollmentCampaign":
                    return EnrEnrollmentCampaign.class;
                case "benefitNoExams":
                    return Boolean.class;
                case "benefitExclusive":
                    return Boolean.class;
                case "benefitPreference":
                    return Boolean.class;
                case "benefitMaxMark":
                    return Boolean.class;
                case "achievement":
                    return Boolean.class;
                case "copyCanBeAccepted":
                    return Boolean.class;
                case "originalCanBeAccepted":
                    return Boolean.class;
                case "sendFIS":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrCampaignEntrantDocument> _dslPath = new Path<EnrCampaignEntrantDocument>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrCampaignEntrantDocument");
    }
            

    /**
     * @return Тип документа. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignEntrantDocument#getDocumentType()
     */
    public static PersonDocumentType.Path<PersonDocumentType> documentType()
    {
        return _dslPath.documentType();
    }

    /**
     * @return ПК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignEntrantDocument#getEnrollmentCampaign()
     */
    public static EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Подтверждает особое право - без ВИ. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignEntrantDocument#isBenefitNoExams()
     */
    public static PropertyPath<Boolean> benefitNoExams()
    {
        return _dslPath.benefitNoExams();
    }

    /**
     * @return Подтверждает особое право - квота. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignEntrantDocument#isBenefitExclusive()
     */
    public static PropertyPath<Boolean> benefitExclusive()
    {
        return _dslPath.benefitExclusive();
    }

    /**
     * @return Подтверждает особое право - преимущество. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignEntrantDocument#isBenefitPreference()
     */
    public static PropertyPath<Boolean> benefitPreference()
    {
        return _dslPath.benefitPreference();
    }

    /**
     * @return Подтверждает особое право - 100 баллов. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignEntrantDocument#isBenefitMaxMark()
     */
    public static PropertyPath<Boolean> benefitMaxMark()
    {
        return _dslPath.benefitMaxMark();
    }

    /**
     * @return Подтверждает индивидуальное достижение. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignEntrantDocument#isAchievement()
     */
    public static PropertyPath<Boolean> achievement()
    {
        return _dslPath.achievement();
    }

    /**
     * @return Разрешено принимать копии. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignEntrantDocument#isCopyCanBeAccepted()
     */
    public static PropertyPath<Boolean> copyCanBeAccepted()
    {
        return _dslPath.copyCanBeAccepted();
    }

    /**
     * @return Разрешено принимать оригинал. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignEntrantDocument#isOriginalCanBeAccepted()
     */
    public static PropertyPath<Boolean> originalCanBeAccepted()
    {
        return _dslPath.originalCanBeAccepted();
    }

    /**
     * @return Передавать в ФИС ГИА и приема. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignEntrantDocument#isSendFIS()
     */
    public static PropertyPath<Boolean> sendFIS()
    {
        return _dslPath.sendFIS();
    }

    public static class Path<E extends EnrCampaignEntrantDocument> extends EntityPath<E>
    {
        private PersonDocumentType.Path<PersonDocumentType> _documentType;
        private EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<Boolean> _benefitNoExams;
        private PropertyPath<Boolean> _benefitExclusive;
        private PropertyPath<Boolean> _benefitPreference;
        private PropertyPath<Boolean> _benefitMaxMark;
        private PropertyPath<Boolean> _achievement;
        private PropertyPath<Boolean> _copyCanBeAccepted;
        private PropertyPath<Boolean> _originalCanBeAccepted;
        private PropertyPath<Boolean> _sendFIS;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Тип документа. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignEntrantDocument#getDocumentType()
     */
        public PersonDocumentType.Path<PersonDocumentType> documentType()
        {
            if(_documentType == null )
                _documentType = new PersonDocumentType.Path<PersonDocumentType>(L_DOCUMENT_TYPE, this);
            return _documentType;
        }

    /**
     * @return ПК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignEntrantDocument#getEnrollmentCampaign()
     */
        public EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Подтверждает особое право - без ВИ. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignEntrantDocument#isBenefitNoExams()
     */
        public PropertyPath<Boolean> benefitNoExams()
        {
            if(_benefitNoExams == null )
                _benefitNoExams = new PropertyPath<Boolean>(EnrCampaignEntrantDocumentGen.P_BENEFIT_NO_EXAMS, this);
            return _benefitNoExams;
        }

    /**
     * @return Подтверждает особое право - квота. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignEntrantDocument#isBenefitExclusive()
     */
        public PropertyPath<Boolean> benefitExclusive()
        {
            if(_benefitExclusive == null )
                _benefitExclusive = new PropertyPath<Boolean>(EnrCampaignEntrantDocumentGen.P_BENEFIT_EXCLUSIVE, this);
            return _benefitExclusive;
        }

    /**
     * @return Подтверждает особое право - преимущество. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignEntrantDocument#isBenefitPreference()
     */
        public PropertyPath<Boolean> benefitPreference()
        {
            if(_benefitPreference == null )
                _benefitPreference = new PropertyPath<Boolean>(EnrCampaignEntrantDocumentGen.P_BENEFIT_PREFERENCE, this);
            return _benefitPreference;
        }

    /**
     * @return Подтверждает особое право - 100 баллов. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignEntrantDocument#isBenefitMaxMark()
     */
        public PropertyPath<Boolean> benefitMaxMark()
        {
            if(_benefitMaxMark == null )
                _benefitMaxMark = new PropertyPath<Boolean>(EnrCampaignEntrantDocumentGen.P_BENEFIT_MAX_MARK, this);
            return _benefitMaxMark;
        }

    /**
     * @return Подтверждает индивидуальное достижение. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignEntrantDocument#isAchievement()
     */
        public PropertyPath<Boolean> achievement()
        {
            if(_achievement == null )
                _achievement = new PropertyPath<Boolean>(EnrCampaignEntrantDocumentGen.P_ACHIEVEMENT, this);
            return _achievement;
        }

    /**
     * @return Разрешено принимать копии. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignEntrantDocument#isCopyCanBeAccepted()
     */
        public PropertyPath<Boolean> copyCanBeAccepted()
        {
            if(_copyCanBeAccepted == null )
                _copyCanBeAccepted = new PropertyPath<Boolean>(EnrCampaignEntrantDocumentGen.P_COPY_CAN_BE_ACCEPTED, this);
            return _copyCanBeAccepted;
        }

    /**
     * @return Разрешено принимать оригинал. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignEntrantDocument#isOriginalCanBeAccepted()
     */
        public PropertyPath<Boolean> originalCanBeAccepted()
        {
            if(_originalCanBeAccepted == null )
                _originalCanBeAccepted = new PropertyPath<Boolean>(EnrCampaignEntrantDocumentGen.P_ORIGINAL_CAN_BE_ACCEPTED, this);
            return _originalCanBeAccepted;
        }

    /**
     * @return Передавать в ФИС ГИА и приема. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignEntrantDocument#isSendFIS()
     */
        public PropertyPath<Boolean> sendFIS()
        {
            if(_sendFIS == null )
                _sendFIS = new PropertyPath<Boolean>(EnrCampaignEntrantDocumentGen.P_SEND_F_I_S, this);
            return _sendFIS;
        }

        public Class getEntityClass()
        {
            return EnrCampaignEntrantDocument.class;
        }

        public String getEntityName()
        {
            return "enrCampaignEntrantDocument";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
