package ru.tandemservice.unienr14.settings.entity;

import ru.tandemservice.unienr14.settings.entity.gen.*;

/**
 * Дисциплина в группе в рамках ПК
 */
public class EnrCampaignDisciplineGroupElement extends EnrCampaignDisciplineGroupElementGen
{
    public EnrCampaignDisciplineGroupElement()
    {
    }

    public EnrCampaignDisciplineGroupElement(EnrCampaignDiscipline discipline, EnrCampaignDisciplineGroup group)
    {
        setDiscipline(discipline);
        setGroup(group);
    }
}