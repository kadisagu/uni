/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EnrollmentResultsBySubjectGroupsAdd.logic;

import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.rtf.RtfRowIntercepterRawText;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrExamPassFormCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EnrollmentResultsBySubjectGroupsAdd.EnrReportEnrollmentResultsBySubjectGroupsAddUI;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportStageSelector;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportUtil;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder;
import ru.tandemservice.unienr14.report.entity.EnrReportEnrollmentResultsBySubjectGroups;
import ru.tandemservice.unienr14.request.entity.*;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 28.08.2014
 */
public class EnrReportEnrollmentResultsBySubjectGroupsDao extends UniBaseDao implements IEnrReportEnrollmentResultsBySubjectGroupsDao {
    @Override
    public long createReport(EnrReportEnrollmentResultsBySubjectGroupsAddUI model) {
        EnrReportEnrollmentResultsBySubjectGroups report = new EnrReportEnrollmentResultsBySubjectGroups();
        report.setFormingDate(new Date());

        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateSelector().getDateFrom());
        report.setDateTo(model.getDateSelector().getDateTo());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, EnrReportEnrollmentResultsBySubjectGroups.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE, EnrReportEnrollmentResultsBySubjectGroups.P_COMPENSATION_TYPE, "shortTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, EnrReportEnrollmentResultsBySubjectGroups.P_PROGRAM_FORM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPETITION_TYPE, EnrReportEnrollmentResultsBySubjectGroups.P_COMPETITION_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, EnrReportEnrollmentResultsBySubjectGroups.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, EnrReportEnrollmentResultsBySubjectGroups.P_FORMATIVE_ORG_UNIT, "fullTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, EnrReportEnrollmentResultsBySubjectGroups.P_PROGRAM_SUBJECT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, EnrReportEnrollmentResultsBySubjectGroups.P_EDU_PROGRAM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, EnrReportEnrollmentResultsBySubjectGroups.P_PROGRAM_SET, "title");
        if (model.getParallelSelector().isParallelActive()) {
            report.setParallel(model.getParallelSelector().getParallel().getTitle());
        }


        DatabaseFile content = new DatabaseFile();
        content.setContent(buildReport(model));
        content.setFilename("EnrReportEnrollmentResultsBySubjectGroups.rtf");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        save(content);

        report.setContent(content);
        save(report);

        return report.getId();
    }

    private byte[] buildReport(EnrReportEnrollmentResultsBySubjectGroupsAddUI model)
    {
        // rtf
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(EnrScriptItem.class, EnrScriptItemCodes.REPORT_ENROLLMENT_RESULTS_BY_SUBJECT_GROUPS);
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());

        // Фильтры
        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder()
                .notArchiveOnly()
                .filter(model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo())
                .filter(model.getEnrollmentCampaign());
        filterAddon.applyFilters(requestedCompDQL, requestedCompDQL.competition());

        if (model.getParallelSelector().isParallelActive())
        {
            if (model.getParallelSelector().isParallelOnly())
                requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.TRUE)));
            if (model.getParallelSelector().isSkipParallel())
                requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.FALSE)));
        }
        EnrReportStageSelector.docAcceptanceStageFilter().applyFilter(requestedCompDQL, "reqComp");

        requestedCompDQL.column("reqComp");


        Comparator<EduProgramSubject> subjectComparator = new Comparator<EduProgramSubject>() {
            @Override
            public int compare(EduProgramSubject o1, EduProgramSubject o2) {
                int res = o1.getSubjectIndex().getCode().substring(0,4).compareTo(o2.getSubjectIndex().getCode().substring(0, 4));
                if (res != 0) return -res;
                res = o1.getGroupTitle().compareTo(o2.getGroupTitle());
                return res;
            }
        };

        List<EnrRequestedCompetition> fromQueryReqComps = getList(requestedCompDQL);

        Map<EduProgramSubject, Set<EnrRequestedCompetition>> dataMap = new TreeMap<>(subjectComparator);

        for (EnrRequestedCompetition reqComp : fromQueryReqComps) {
            final Set<EnrRequestedCompetition> requestedCompetitionSet = SafeMap.safeGet(dataMap, reqComp.getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramSubject(), HashSet.class);
            requestedCompetitionSet.add(reqComp);
        }


        EnrReportStageSelector.enrollmentResultsStageFilter().applyFilter(requestedCompDQL, "reqComp");
        requestedCompDQL
                .resetColumns()
                .joinEntity("reqComp", DQLJoinType.inner, EnrChosenEntranceExam.class, "vvi", eq(property(EnrChosenEntranceExam.requestedCompetition().fromAlias("vvi")), property("reqComp")))
                .where(eq(property(EnrChosenEntranceExam.maxMarkForm().passForm().code().fromAlias("vvi")), value(EnrExamPassFormCodes.STATE_EXAM)))
                .column("vvi");

        List<EnrChosenEntranceExam> fromQueryVvi = getList(requestedCompDQL);

        Map<EnrRequestedCompetition, Set<EnrChosenEntranceExam>> vviMap = SafeMap.get(HashSet.class);

        for (EnrChosenEntranceExam exam : fromQueryVvi) {
            vviMap.get(exam.getRequestedCompetition()).add(exam);
        }

        NumberFormat formatter = new DecimalFormat("#0.00");

        List<String[]> tTable = new ArrayList<>();

        int[] totalRow = new int[6];

        for (Map.Entry<EduProgramSubject, Set<EnrRequestedCompetition>> entry : dataMap.entrySet()) {

            List<String> row = new ArrayList<>();

            EduProgramSubject subject = entry.getKey();

            row.add(subject.getGroupTitle());



            Set<EnrEntrantRequest> requests = new HashSet<>();
            Set<EnrEntrant> noViEnrolled = new HashSet<>();
            Set<EnrEntrant> quotaEnrolled = new HashSet<>();
            Set<EnrEntrant> taEnrolled = new HashSet<>();
            Set<EnrEntrant> commonEnrolled = new HashSet<>();
            Set<EnrEntrant> totalEnrolled = new HashSet<>();


            List<Double> marksList = new ArrayList<>();

            for (EnrRequestedCompetition reqComp : entry.getValue()) {
                EnrEntrantRequest request = reqComp.getRequest();
                EnrEntrant entrant = request.getEntrant();
                requests.add(request);
                if (reqComp.getState().getPriority() == 1) {

                    totalEnrolled.add(entrant);


                    if (reqComp instanceof EnrRequestedCompetitionNoExams)
                        noViEnrolled.add(entrant);
                    else if (reqComp instanceof EnrRequestedCompetitionExclusive)
                        quotaEnrolled.add(entrant);
                    else if (reqComp instanceof EnrRequestedCompetitionTA)
                        taEnrolled.add(entrant);
                    else commonEnrolled.add(entrant);

                    for (EnrChosenEntranceExam exam : vviMap.get(reqComp)) {
                        marksList.add(exam.getMarkAsDouble());
                    }
                }
            }

            row.add(String.valueOf(requests.size()));
            row.add(String.valueOf(noViEnrolled.size()));
            row.add(String.valueOf(quotaEnrolled.size()));
            row.add(String.valueOf(taEnrolled.size()));
            row.add(String.valueOf(commonEnrolled.size()));
            row.add(String.valueOf(totalEnrolled.size()));

            totalRow[0] += requests.size();
            totalRow[1] += noViEnrolled.size();
            totalRow[2] += quotaEnrolled.size();
            totalRow[3] += taEnrolled.size();
            totalRow[4] += commonEnrolled.size();
            totalRow[5] += totalEnrolled.size();


            Double average = 0.0;
            for (Double aDouble : marksList) {
                average += aDouble;
            }

            if (marksList.size() > 0) {
                average = average / marksList.size();
                row.add(formatter.format(average));
            } else row.add("0");


            tTable.add(row.toArray(new String[row.size()]));
        }



        RtfTableModifier modifier = new RtfTableModifier();
        modifier.put("T", tTable.toArray(new String[tTable.size()][]));
        modifier.put("T", new RtfRowIntercepterRawText());
        modifier.put("total", new RtfRowIntercepterRawText());
        modifier.put("total", new String[][]
                {
                    {
                        "ВСЕГО",
                        "\\qc " + String.valueOf(totalRow[0]),
                        "\\qc " + String.valueOf(totalRow[1]),
                        "\\qc " + String.valueOf(totalRow[2]),
                        "\\qc " + String.valueOf(totalRow[3]),
                        "\\qc " + String.valueOf(totalRow[4]),
                        "\\qc " + String.valueOf(totalRow[5])
                    }
                }
        );
        modifier.modify(document);

        new RtfInjectModifier()
                .put("compensationType", ((CompensationType)model.getCompetitionFilterAddon().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE).getValue()).isBudget() ? "бюджетные места" : "места с оплатой стоимости обучения")
                .put("programForm", ((EduProgramForm)model.getCompetitionFilterAddon().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM).getValue()).getTitle())
                .put("requestType", ((EnrRequestType)model.getCompetitionFilterAddon().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE).getValue()).getTitle())
                .put("year", DateFormatter.DATE_FORMATTER_JUST_YEAR.format(new Date()))
                .put("academyTitle", TopOrgUnit.getInstance().getPrintTitle())
                .modify(document);


        return RtfUtil.toByteArray(document);
    }
}
