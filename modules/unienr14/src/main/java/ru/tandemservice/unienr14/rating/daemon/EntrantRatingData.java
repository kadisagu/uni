package ru.tandemservice.unienr14.rating.daemon;

import org.tandemframework.core.tool.IdGen;
import org.tandemframework.shared.sandbox.utils.RSHashBuilder;

/**
 * Данные элемента рейтингового списка
 * @author vdanilov
 */
public class EntrantRatingData {

    public static final long NO_HASH = 0L; // отсутствие значения хэша


    // оболочка для БЕЗ-ВИ (содержит данные для сортировки людей БЕЗ-ВИ между собой)
    protected static class NoExamWrapper {

        // категория права
        private final String benefitCategoryCode;
        public String getBenefitCategoryCode() { return this.benefitCategoryCode; }

        public NoExamWrapper(final String benefitCategoryCode) {
            this.benefitCategoryCode = benefitCategoryCode;
        }

        public long calculateValueHashCode() {
            return this.getBenefitCategoryCode().hashCode();
        }

    }

    protected EntrantRatingData(final EntrantRatingCompetitionData competitionData, final Long ratingItemId, final Long requestedCompetitionId, final int examSetSize) {
        this.competitionData = competitionData;
        this.ratingItemId = ratingItemId;
        this.requestedCompetitionId = requestedCompetitionId;
        this.markArray = new long[examSetSize];
    }

    // enrCompetiton: entrantRatingCompetitionData
    private final EntrantRatingCompetitionData competitionData;
    public EntrantRatingCompetitionData getCompetitionData() { return this.competitionData; }

    // enrRatingItem.id: ссылка на соответствующий ratingItem если он есть
    private final Long ratingItemId;
    public Long getRatingItemId() { return this.ratingItemId; }

    // enrRatingItem.valueHash: значение valueHash объекта в базе, если таковое есть
    private long databaseValueHash = NO_HASH;
    public long getDatabaseValueHash() { return this.databaseValueHash; }
    public void setDatabaseValueHash(final long databaseValueHash) { this.databaseValueHash = databaseValueHash; }
    public void setDatabaseValueHash(final Long databaseValueHash) { this.databaseValueHash = (null == databaseValueHash ? 0L : databaseValueHash.longValue()); }

    // EnrRequestedCompetition: выбранный конкурс
    private final Long requestedCompetitionId;
    public Long getRequestedCompetitionId() { return this.requestedCompetitionId; }

    // выбранный конкурс активный ()
    private boolean requestedCompetitionActive;
    public boolean isRequestedCompetitionActive() { return this.requestedCompetitionActive; }
    public void setRequestedCompetitionActive(boolean requestedCompetitionActive) { this.requestedCompetitionActive = requestedCompetitionActive; }

    // преимущественное право из заявления
    private boolean preemtive;
    public boolean isPreemtive() { return this.preemtive; }
    protected void setPreemtive(final boolean preemtive) { this.preemtive = preemtive; }

    // ОП без ВИ (по данному конкурсу, если есть)
    private NoExamWrapper noExamWrapper;
    public NoExamWrapper getNoExamWrapper() { return this.noExamWrapper; }
    protected void setNoExamWrapper(final NoExamWrapper noExamWrapper) { this.noExamWrapper = noExamWrapper; }

    // ошибки в оценках (ВВИ не соответсвует набору, например)
    private boolean markError;
    public boolean isMarkError() { return this.markError; }
    public void registerMarkError() { this.markError = true; }

    // оценки по вступительным испытаниям (в порядке следования ВИ в наборе)
    private final long[] markArray;
    public long[] getMarkArray() { return this.markArray; }
    public long getMark(final int i) { return this.getMarkArray()[i]; }

    // сумма баллов за ВИ
    private long markSum = 0L;
    public long getMarkSum() { return this.markSum; }

    // ВИ набора, по которым есть оценка (биотвые поля; я сомневаюсь, что у абитуриента будет 63 ВИ)
    private long markMask = 0L;
    public long getMarkMask() { return this.markMask; }

    // ВИ набора, по которым сданы не все ВВИ-ф (биотвые поля; я сомневаюсь, что у абитуриента будет 63 ВИ)
    private long nonCompleteMarkMask = 0L;
    public long getNonCompleteMarkMask() { return this.nonCompleteMarkMask; }

    // ВИ набора, сданные ниже пороговых (биотвые поля; я сомневаюсь, что у абитуриента будет 63 ВИ)
    private long nonPassMarkMask = 0L;
    public long getNonPassMarkMask() { return this.nonPassMarkMask; }

    // true, если оценка присутсвует
    public boolean isMarkPresent(final int markIndex) { return 0 != (this.getMarkMask() & (1L << markIndex)) ; }

    // true, если по всем ВИ есть оценки
    public boolean isAllMarkPresent() { return (this.markArray.length == Long.bitCount(this.getMarkMask())); }

    // true, если по всем ВИ либо сданы все ВВИ-ф, либо балл не ниже порогового
    // эквивалентное утверждение: не существует ВИ для которой балл ниже порогового и есть несданный ВВИ-ф
    public boolean isAllMarkCompleteOrPassed() { return (0 == Long.bitCount(this.getNonCompleteMarkMask() & this.getNonPassMarkMask())); }

    // true, если есть ВИ (по которой сданы все ВВИ-ф) с баллом ниже порогового
    public boolean isNonPassedCompleteMarkExists() { return Long.bitCount(this.getNonPassMarkMask() & (this.getNonCompleteMarkMask() ^ Long.MAX_VALUE)) > 0; }

    // true, если есть ВИ с баллом ниже порогового (не важно, сданы все ВВИ-ф или нет)
    public boolean isNonPassedMarkExists() { return Long.bitCount(this.getNonPassMarkMask()) > 0; }

    // добавляет оценку за ВИ
    public void registerMark(final int markIndex, final long mark, final boolean pass, final boolean complete)
    {
        final long mask = 1L << markIndex;

        // помечаем оценку
        this.markMask |= mask;
        if (!pass) { nonPassMarkMask |= mask; }
        if (!complete) { nonCompleteMarkMask |= mask; }

        // сравниваем с предыдущим значением (первый раз там 0)
        final long prev = this.markArray[markIndex];
        if (prev < mark) {
            this.markSum += mark;
            this.markSum -= prev;
            this.markArray[markIndex] = mark;
        }
    }

    // добавляет баллы ИД
    public void addAchievementSumToMarkSum()
    {
        this.markSum += this.achievementMark;
    }

    // ВИ набора, для которых есть ВВИ (биотвые поля; я сомневаюсь, что у абитуриента будет 63 ВИ)
    private long examMask = 0L;
    public long getExamMask() { return this.examMask; }

    // ВИ набора, для которых есть более одного ВВИ - несоответствие ВВИ и набора (биотвые поля; я сомневаюсь, что у абитуриента будет 63 ВИ)
    private long duplicateExamMask = 0L;
    public long getDuplicateExamMask() { return this.duplicateExamMask; }

    // true, если по всем ВИ есть ВВИ
    public boolean isAllExamPresent() { return (this.markArray.length == Long.bitCount(this.getExamMask())); }

    // true, если есть ВИ, по которым есть несколько ВВИ
    public boolean isDuplicateExamExists() { return (Long.bitCount(this.getDuplicateExamMask()) > 0); }

    // добавляет ВВИ
    public void registerExam(final int markIndex) {
        final long mask = 1L << markIndex;

        if (0 != (this.examMask & mask)) {
            // если это дубль, то помечаем его как дубль
            this.duplicateExamMask |= mask;
        }

        // помечаем ВВИ
        this.examMask |= mask;
    }

    // // баллы по индивидуальным достижениям (срений балл аттестата)
    private long eduInstitutionAvgMark = 0;
    public long getEduInstitutionAvgMark() { return this.eduInstitutionAvgMark; }
    protected void setEduInstitutionAvgMark(final long eduInstitutionAvgMark) { this.eduInstitutionAvgMark = eduInstitutionAvgMark; }

    // баллы по индивидуальным достижениям (на усмотрение ВУЗа)
    private long achievementMark = 0;
    public long getAchievementMark() { return this.achievementMark; }
    protected void setAchievementMark(final long achievementMark) { this.achievementMark = achievementMark; }

    // положение в базе (соответсвует позции enrRatingItem, отсортированной по enrRatingItem.position)
    private int databaseIndex = Integer.MAX_VALUE; // по умолчанию все элементы в конце списка
    public int getDatabaseIndex() { return this.databaseIndex; }
    public void setDatabaseIndex(final int databaseIndex) { this.databaseIndex = databaseIndex; }

    // позиция только на основе данных сравнения (неуникальная)
    private int positionNonUnique;
    public int getPositionNonUnique() { return this.positionNonUnique; }
    public void setPositionNonUnique(final int positionNonUnique) { this.positionNonUnique = positionNonUnique; }

    // позиция (уникальная)
    private int positionUnique;
    public int getPositionUnique() { return this.positionUnique; }
    public void setPositionUnique(final int positionUnique) { this.positionUnique = positionUnique; }

    // вычисляет хэш
    public long calculateValueHash(final long base)
    {
        // здесь нужна hash-функция, которая будет чувствительна к изменению любого аргумента,
        // а также будет равномерно заполнять интервал значений Long
        final RSHashBuilder b = new RSHashBuilder(base);
        b.add(this.getCompetitionData().getValueHash());
        b.add(this.getRequestedCompetitionId().longValue() >>> IdGen.CODE_BITS);
        b.add(this.isRequestedCompetitionActive() ? 1L : 0L);
        b.add(null == this.getNoExamWrapper() ? 0L : this.getNoExamWrapper().calculateValueHashCode());
        b.add(this.isPreemtive() ? 1L : 0L);
        b.add(this.getPositionNonUnique());
        b.add(this.getPositionUnique());
        b.add(this.isMarkError() ? 1L : 0L);
        b.add(this.getNonPassMarkMask());
        b.add(this.getNonCompleteMarkMask());
        b.add(this.getDuplicateExamMask());
        b.add(this.getExamMask());
        b.add(this.getMarkMask());
        b.add(this.getMarkSum());
        b.add(this.getAchievementMark());
        b.add(this.getEduInstitutionAvgMark());
        for (final long mark: this.markArray) {
            b.add(mark);
        }

        long result = b.result();
        if (EntrantRatingData.NO_HASH == result) {
            // значение 0 возвращать нельзя, оно соответсвует отсутсвию хэша
            result = 1; // возвращаем 1
        }

        return result;
    }




}