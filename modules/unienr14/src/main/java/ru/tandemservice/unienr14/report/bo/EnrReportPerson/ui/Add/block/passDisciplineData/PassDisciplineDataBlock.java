/* $Id: PassDisciplineDataBlock.java 33636 2014-04-16 04:31:39Z nfedorovskih $ */
package ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.block.passDisciplineData;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportParam;
import ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 02.07.2013
 */
public class PassDisciplineDataBlock
{
    public static final String EDUCATION_SUBJECT_DS = "educationSubjectDS";
    public static final String SUBJECT_PASS_FORM_DS = "subjectPassFormDS";

    public static void onBeforeDataSourceFetch(IUIDataSource dataSource, IReportParam<EnrEnrollmentCampaign> enrollmentCampaignParam)
    {
        enrollmentCampaignParam.putParamIfActive(dataSource, EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
    }

    public static IDefaultComboDataSourceHandler createEducationSubjectDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, EnrCampaignDiscipline.class, EnrCampaignDiscipline.discipline().title())
        {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
                super.prepareConditions(ep);
                EnrEnrollmentCampaign enrollmentCampaign = ep.context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);

//                final DQLSelectBuilder examSetDQL = new DQLSelectBuilder().fromEntity(EnrCompetitionGroup.class, "cg").column(property(EnrCompetitionGroup.examSet().fromAlias("cg")))
//                        .where(isNotNull(property(EnrCompetitionGroup.examSet().fromAlias("cg"))))
//                        .where(eq(property(EnrCompetitionGroup.enrollmentCampaign().fromAlias("cg")), value(enrollmentCampaign, PropertyType.ANY)));
//
//                final DQLSelectBuilder elementValueDQL = new DQLSelectBuilder().fromEntity(EnrExamSetElement.class, "ese").column(property(EnrExamSetElement.value().id().fromAlias("ese")))
//                        .where(in(property(EnrExamSetElement.examSet().fromAlias("ese")), examSetDQL.buildQuery()))
//                        .predicate(DQLPredicateType.distinct);

                ep.dqlBuilder
                        .where(eq(property(EnrCampaignDiscipline.enrollmentCampaign().fromAlias("e")), value(enrollmentCampaign)))
//                        .where(in(property(EnrCampaignDiscipline.id().fromAlias("e")), elementValueDQL.buildQuery()))
                ;
            }
        };
    }

    public static IDefaultComboDataSourceHandler createSubjectPassFormDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, EnrExamPassForm.class, EnrExamPassForm.title())
        {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
                super.prepareConditions(ep);

                EnrEnrollmentCampaign enrollmentCampaign = ep.context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);

                final DQLSelectBuilder passFormDQL = new DQLSelectBuilder().fromEntity(EnrExamPassDiscipline.class, "d").column(property(EnrExamPassDiscipline.passForm().id().fromAlias("d")))
                        .where(eq(property(EnrExamPassDiscipline.entrant().enrollmentCampaign().fromAlias("d")), value(enrollmentCampaign)));

                ep.dqlBuilder.where(in(property("e.id"), passFormDQL.buildQuery()));
            }
        };
    }
}