/* $Id$ */
package ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.EnrollmentCommissionList.logic;

import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.dsl.MetaDSLPath;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType2eduProgramKindRel;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.logic.EnrProgramSetSearchDSHandler;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.EnrollmentCommissionList.EnrProgramSetEnrollmentCommissionListUI.*;

/**
 * @author Alexey Lopatin
 * @since 04.06.2015
 */
public class EnrProgramSetOrgUnitComboDSHandler extends EntityComboDataSourceHandler
{
    public EnrProgramSetOrgUnitComboDSHandler(String ownerId, Class<? extends IEntity> entityClass)
    {
        super(ownerId, entityClass);
        pageable(true);
    }

    public DQLSelectBuilder createBuilder(ExecutionContext context, MetaDSLPath propertyPath, String keyClear)
    {
        context.put(keyClear, null);
        EnrEnrollmentCampaign campaign = context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
        EnrRequestType requestType = context.get(PARAM_REQUEST_TYPE);
        List<EduProgramForm> programForms = context.get(PARAM_PROGRAM_FORMS);
        List<EnrOrgUnit> orgUnits = context.get(PARAM_ORG_UNITS);
        List<OrgUnit> formativeOrgUnits = context.get(PARAM_FORMATIVE_ORG_UNITS);
        List<EduProgramSubject> programSubjects = context.get(PARAM_PROGRAM_SUBJECTS);

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EnrProgramSetOrgUnit.class, "psOu")
                .column(property("psOu", propertyPath))
                .joinPath(DQLJoinType.inner, EnrProgramSetOrgUnit.programSet().fromAlias("psOu"), "s")
                .joinEntity("s", DQLJoinType.inner, requestType.getProgramSetClass(), "ps", eq(property("ps", EnrProgramSetBase.id()), property("s", EnrProgramSetBase.id())))
                .where(exists(
                        EnrRequestType2eduProgramKindRel.class,
                        EnrRequestType2eduProgramKindRel.requestType().s(), requestType,
                        EnrRequestType2eduProgramKindRel.programKind().s(), property("s", EnrProgramSetBase.programSubject().subjectIndex().programKind())
                ));

        if (null != programForms && !programForms.isEmpty())
        {
            dql.where(in(property("s", EnrProgramSetBase.programForm()), programForms));
        }

        EnrProgramSetSearchDSHandler.applyFilters(dql, "s", campaign, requestType, null, orgUnits, formativeOrgUnits, programSubjects, null);
        return dql;
    }
}
