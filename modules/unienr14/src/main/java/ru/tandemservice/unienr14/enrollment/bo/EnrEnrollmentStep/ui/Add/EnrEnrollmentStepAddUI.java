/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.ui.Add;

import java.util.Date;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;

import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.catalog.bo.EnrRequestType.EnrRequestTypeManager;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentStepKind;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEnrollmentStepKindCodes;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.EnrEnrollmentStepManager;
import ru.tandemservice.unienr14.entrant.daemon.EnrEntrantDaemonBean;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

/**
 * @author oleyba
 * @since 12/17/13
 */
@Input({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "campaign.id")
})
public class EnrEnrollmentStepAddUI extends UIPresenter
{
    private EnrEnrollmentCampaign campaign = new EnrEnrollmentCampaign();

    private EnrRequestType requestType;
    private DataWrapper enrollmentDate;
    private EnrEnrollmentStepKind stepKind;
    private Long percentageAsLong;

    @Override
    public void onComponentRefresh() {
        setCampaign(IUniBaseDao.instance.get().getNotNull(EnrEnrollmentCampaign.class, getCampaign().getId()));
        setRequestType(EnrRequestTypeManager.instance().dao().getDefaultRequestType(getCampaign()));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, getCampaign());
        if (EnrEnrollmentStepAdd.ENROLLMENT_DATE_DS.equals(dataSource.getName())) {
            dataSource.put(EnrEnrollmentStepAdd.REQUEST_TYPE, getRequestType());
        }
    }

    public void onClickApply() {
        EnrRequestTypeManager.instance().dao().saveDefaultRequestType(getRequestType(), getCampaign());
        EnrEntrantDaemonBean.DAEMON.waitForComplete();
        Date date = (Date) this.enrollmentDate.getProperty("date");
        EnrEnrollmentStepManager.instance().dao().createNewStep(getCampaign(), date, getRequestType(), getStepKind(), getPercentageAsLong());
        deactivate();
    }

    public void onChangeKind() {
        if (getStepKind() != null && EnrEnrollmentStepKindCodes.NO_REC_PERCENTAGE.equals(getStepKind().getCode()) && null == getPercentageAsLong()) {
            setPercentageAsLong(8000L);
        }
    }

    // getters and setters

    public boolean isShowPercentage() {
        return getStepKind() != null && getStepKind().isUsePercentage();
    }


    public EnrEnrollmentCampaign getCampaign()
    {
        return campaign;
    }

    public void setCampaign(EnrEnrollmentCampaign campaign)
    {
        this.campaign = campaign;
    }

    public EnrRequestType getRequestType(){ return requestType; }

    public void setRequestType(EnrRequestType requestType){ this.requestType = requestType; }

    public DataWrapper getEnrollmentDate(){ return enrollmentDate; }

    public void setEnrollmentDate(DataWrapper enrollmentDate){ this.enrollmentDate = enrollmentDate; }

    public EnrEnrollmentStepKind getStepKind()
    {
        return stepKind;
    }

    public void setStepKind(EnrEnrollmentStepKind stepKind)
    {
        this.stepKind = stepKind;
    }

    public Long getPercentageAsLong()
    {
        return percentageAsLong;
    }

    public void setPercentageAsLong(Long percentageAsLong)
    {
        this.percentageAsLong = percentageAsLong;
    }
}