/* $Id:$ */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniedu.orgunit.entity.EduInstitutionOrgUnit;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementSettings;

import java.util.List;

/**
 * dao для работы с приемной кампанией и ее настройками
 *
 * @author oleyba
 * @since 4/9/13
 */
public interface IEnrCampaignDao extends INeedPersistenceSupport
{
    List<EnrEnrollmentCampaign> getEnrollmentCampaignList();

    /**
     * Обертка для передачи настроек ПК.
     */
    public static interface IEnrCampSettings
    {
        /**
         * Выбирать организацию, направившую абитуриента, при указании ЦП
         */
        Boolean isUseExternalOrgUnitForTA();
    }

    /**
     * Выбранная пользователем ПК в фильтрах и селекторах в модуле хранится в настройках пользователя, как общая для всех страниц модуля.
     * Этот метод позволяет получить такую ПК для текущего пользователя.
     * <p/>
     * Если вы используете такую ПК в списке в фильтре, то при применении фильтров сохраняйте новый выбор пользователя.
     * Если вы используете такую ПК в селекторе, сохраняйте при обновлении селектора.
     * <p/>
     * Для сохранения используйте метод
     *
     * @return выбранная пользователем ПК (может быть null). Вернет null, если метод вызвать вне UserContext.
     * @see IEnrCampaignDao#saveDefaultCampaign(ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign)
     * <p/>
     * (DEV-2635)
     * *
     */
    public EnrEnrollmentCampaign getDefaultCampaign();

    /**
     * Выбранная пользователем ПК в фильтрах и селекторах в модуле хранится в настройках пользователя, как общая для всех страниц модуля.
     * Этот метод позволяет сохранить новый выбор пользователя в настройки.
     * <p/>
     * Если вы используете такую ПК в списке в фильтре, то при применении фильтров сохраняйте новый выбор пользователя.
     * Если вы используете такую ПК в селекторе, сохраняйте при обновлении селектора.
     * <p/>
     * Чтобы получить сохраненное значение, используйте
     *
     * @param campaign выбранная пользователем ПК
     * @see ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.logic.IEnrCampaignDao#getDefaultCampaign()
     * <p/>
     * (DEV-2635)
     */
    public void saveDefaultCampaign(EnrEnrollmentCampaign campaign);

    /**
     * Сохраняет\обновляет Приемную кампанию и её настройки.
     * Выполняются валидации.
     *
     * @param entity         Приемная кампания
     * @param enrOrgUnitList
     * @return id приемной кампании, null если при валидации ошибка
     */
    Long saveOrUpdateCampaign(EnrEnrollmentCampaign entity, List<EduInstitutionOrgUnit> enrOrgUnitList);

    /**
     * Дата начала должна быть раньше даты окончания.
     *
     * @param entity Приемная кампания
     * @return коллектор пользовательских ошибок
     */
    ErrorCollector validateCampaign(EnrEnrollmentCampaign entity);

    /**
     * Удаляет приемную компанию.
     *
     * @param entity Приемная кампания
     */
    void deleteCampaign(EnrEnrollmentCampaign entity);

    /**
     * Обновляет настройки ПК на указанные.
     *
     * @param entity   приемная компания
     * @param settings настройки
     * @return id объекта, который хранит настройки
     */
    Long updateEnrCampSettings(EnrEnrollmentCampaign entity, IEnrCampSettings settings);


    /**
     * Копирование ПК на следующий год (DEV-6673)
     *
     * @param srcCampaign          копируемая ПК
     * @param newCampaignContainer новая ПК (объект-контейнер данных: название, учебный год и даты начала/окончания)
     * @return новая ПК
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    EnrEnrollmentCampaign copyEnrollmentCampaign(EnrEnrollmentCampaign srcCampaign, EnrEnrollmentCampaign newCampaignContainer);

    /**
     * Получение настройки индивидуальных достижений в ПК для вида заявления
     */
    EnrEntrantAchievementSettings getAchievementSettings(EnrEnrollmentCampaign campaign, EnrRequestType requestType);

    /**
     * Закрывает/открывает ПК.
     * @param ecId id ПК
     */
    void doChangeOpen(Long ecId);

    /**
     * Проверяет, что ПК открыта - если закрыта, выбрасывает ApplicationException с соответствующим текстом.
     * @param campaign ПК
     */
    void checkEnrollmentCampaignOpen(EnrEnrollmentCampaign campaign);
}
