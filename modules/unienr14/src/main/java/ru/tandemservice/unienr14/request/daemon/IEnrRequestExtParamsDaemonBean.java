package ru.tandemservice.unienr14.request.daemon;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.shared.commonbase.base.util.DoNotUseMe;

/**
 * @author vdanilov
 */
public interface IEnrRequestExtParamsDaemonBean extends INeedPersistenceSupport {

    public static final SpringBeanCache<IEnrRequestExtParamsDaemonBean> instance = new SpringBeanCache<>(IEnrRequestExtParamsDaemonBean.class.getName());

    /**
     * Обновляет поле EnrRequestedCompetition.сontractEnrollmentAvailable
     * @return true, если что-то изменилось
     */
    boolean doUpdateEnrReqCompEnrollAvailableAndEduDoc();
}
