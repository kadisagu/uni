package ru.tandemservice.unienr14.debug.bo.EnrDebug.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

/**
 * @author vdanilov
 */
public interface IEnrDebugDao extends INeedPersistenceSupport {

    /** Заполняет базу произвольными ОП на указанный учебный год */
    void doFullEduRandom(EducationYear year);

    /** Заполняет базу произвольными настройками выбранной ПК*/
    void doFullEcRandom(EnrEnrollmentCampaign ec);

    /** Заполняет базу произвольными абитуриентами для выбранной ПК, количеством не больше заданного*/
    void doFullEcRandomEntrant(EnrEnrollmentCampaign ec, int entrantsNumber);

    /** Обновляет рейтинговые списки для выбранной ПК */
    void doFullEcRating(EnrEnrollmentCampaign ec);

    /**Удаляет данные абитуриентов и заявлений в выбранной ПК */
    void doDeleteEcEntrants(EnrEnrollmentCampaign ec);

    /**Удаляет выбранную ПК */
    void doCleanEnrollmentCampaign(EnrEnrollmentCampaign ec);
}
