package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.select;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import java.util.*;

import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.util.DebugUtils;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrTargetAdmissionCompetitionPlan;
import ru.tandemservice.unienr14.competition.entity.EnrTargetAdmissionPlan;
import ru.tandemservice.unienr14.competition.entity.gen.EnrTargetAdmissionPlanGen;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.EnrEnrollmentStepDao;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepCompetitionPlan;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItem;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItemOrderInfo;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignTargetAdmissionKind;

/**
 * @author vdanilov
 */
public class EnrSelectionDao extends UniBaseDao implements IEnrSelectionDao {

    @Override
    public EnrEnrollmentStepRecommendationContext prepareRecommendationContext(final EnrEnrollmentStep step, final boolean debug) {
        final EnrEnrollmentStepRecommendationContext context = new EnrEnrollmentStepRecommendationContext(step, debug);
        Debug.begin("prepareRecommendationContext");
        try {
            DebugUtils.debug(
                new DebugUtils.Section("init") {
                    @Override public void execute() {
                        EnrSelectionDao.this.doInitContext(context);
                    }
                }
            );
        } catch (final Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        } finally {
            Debug.end();
        }
        return context;
    }


    @Override
    public EnrEnrollmentStepEnrollmentContext prepareEnrollmentContext(final EnrEnrollmentStep step, final boolean debug) {
        final EnrEnrollmentStepEnrollmentContext context = new EnrEnrollmentStepEnrollmentContext(step, debug);
        Debug.begin("prepareEnrollmentContext");
        try {
            DebugUtils.debug(
                new DebugUtils.Section("init") {
                    @Override public void execute() {
                        EnrSelectionDao.this.doInitContext(context);
                    }
                }
            );
        } catch (final Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        } finally {
            Debug.end();
        }
        return context;
    }


    @Override
    public void doExecuteSelection(final EnrEnrollmentStepBaseContext context)
    {
        this.lock("entrantSelectionContext."+context.getStep().getEnrollmentCampaign().getEducationYear());
        Debug.begin("doExecuteSelection");
        try {
            DebugUtils.debug(
                new DebugUtils.Section("check") {
                    @Override public void execute() {
                        EnrSelectionDao.this.doCheckContext(context);
                    }
                },
                new DebugUtils.Section("execute") {
                    @Override public void execute() {
                        EnrSelectionDao.this.doExecuteContext(context);
                    }
                },
                new DebugUtils.Section("apply") {
                    @Override public void execute() {
                        EnrSelectionDao.this.doApplyContext(context);
                    }
                }
            );
        } catch (final Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        } finally {
            Debug.end();
        }
    }

    /** инициализирует данные о списке абитуриентов, существующих ранее приказах, планы по конкурсам */
    protected void doInitContext(final EnrEnrollmentStepBaseContext context)
    {
        // сначала обрабатываем строки шага
        Debug.begin("stepItems");
        try {
            // preload
            Debug.begin("preload");
            try {
                // конкурсы
                this.getList(
                    new DQLSelectBuilder()
                    .fromEntity(EnrCompetition.class, "c")
                    .where(exists(
                        new DQLSelectBuilder().fromEntity(EnrEnrollmentStepItem.class, "si").column(property("si.id"))
                        .where(eq(property(EnrEnrollmentStepItem.step().fromAlias("si")), value(context.getStep())))
                        .where(eq(property(EnrEnrollmentStepItem.entity().requestedCompetition().competition().fromAlias("si")), property("c")))
                        .buildQuery()
                    ))
                );

                // выбранные конкурсы
                this.getList(
                    new DQLSelectBuilder()
                    .fromEntity(EnrRequestedCompetition.class, "rc")
                    .where(exists(
                        new DQLSelectBuilder().fromEntity(EnrEnrollmentStepItem.class, "si").column(property("si.id"))
                        .where(eq(property(EnrEnrollmentStepItem.step().fromAlias("si")), value(context.getStep())))
                        .where(eq(property(EnrEnrollmentStepItem.entity().requestedCompetition().fromAlias("si")), property("rc")))
                        .buildQuery()
                    ))
                );
            } finally {
                Debug.end();
            }

            final Map<EnrEnrollmentStepItem, EnrSelectionItem> wrapperMap = context.setupStepItemList(
                new DQLSelectBuilder()
                .fromEntity(EnrEnrollmentStepItem.class, "si").column(property("si"))
                .fetchPath(DQLJoinType.inner, EnrEnrollmentStepItem.entity().fromAlias("si"), "ri")
                .where(eq(property(EnrEnrollmentStepItem.step().fromAlias("si")), value(context.getStep())))
                .order(property(EnrRatingItem.position().fromAlias("ri")))
                .createStatement(this.getSession()).<EnrEnrollmentStepItem>list()
            );

            // orders
            Debug.begin("orders");
            try {
                final List<EnrEnrollmentStepItemOrderInfo> orderInfoList = new DQLSelectBuilder()
                .fromEntity(EnrEnrollmentStepItemOrderInfo.class, "oi").column(property("oi"))
                .where(eq(property(EnrEnrollmentStepItemOrderInfo.item().step().fromAlias("oi")), value(context.getStep())))
                .createStatement(this.getSession()).list();

                for (final EnrEnrollmentStepItemOrderInfo orderInfo: orderInfoList) {
                    new EnrEnrollmentStepItemPrevEnrollmentInfo(orderInfo).apply(wrapperMap.get(orderInfo.getItem()));
                }
            } finally {
                Debug.end();
            }


        } finally {
            Debug.end();
        }

        Map<Long, Integer> planMap = new HashMap<>();
        if (context.getStep().getKind().isUsePercentage()) {
            for (EnrEnrollmentStepCompetitionPlan plan : getList(EnrEnrollmentStepCompetitionPlan.class, EnrEnrollmentStepCompetitionPlan.enrollmentStep(), context.getStep())) {
                planMap.put(plan.getCompetition().getId(), plan.getPlan());
            }
        }

        // планы приема (по всем группам)
        Debug.begin("setupPlan");
        try {
            final Set<String> generalCompetitionCodes = new HashSet<>(Arrays.asList(EnrCompetitionTypeCodes.MINISTERIAL, EnrCompetitionTypeCodes.CONTRACT));
            final Collection<EnrSelectionGroup> selectionGroups = context.getStepItemGroupMap().values();
            for (final EnrSelectionGroup group: selectionGroups) {
                final EnrCompetition comp = group.getKey().getCompetition();
                if (generalCompetitionCodes.contains(comp.getType().getCode()))
                {
                    final Integer plan = context.getStep().getKind().isUsePercentage() ? planMap.get(comp.getId()) : comp.getPlan();
                    if (null == plan) {
                        throw new ApplicationException("Не задано число мест по конкурсу: " + comp.getTitle() + ".");
                    }
                    group.setupPlan(plan);
                }
                else
                {
                    final EnrCampaignTargetAdmissionKind taKind = group.getKey().getTargetAdmissionKind();
                    if (null == taKind)
                    {
                        group.setupPlan(comp.getPlan());
                    }
                    else
                    {
                        final EnrTargetAdmissionPlan taPlan = this.getByNaturalId(new EnrTargetAdmissionPlanGen.NaturalId(taKind, comp.getProgramSetOrgUnit()));
                        if (null == taPlan) {
                            throw new ApplicationException("Для лицензированного подразделения «"+comp.getProgramSetOrgUnit().getOrgUnit().getDepartmentTitle()+"» набора ОП «"+comp.getProgramSetOrgUnit().getProgramSet().getTitle()+"» не настроен план приема по виду ЦП «"+taKind.getTitle()+"».");
                        }

                        final EnrTargetAdmissionCompetitionPlan taCompPlan = this.getByNaturalId(new EnrTargetAdmissionCompetitionPlan.NaturalId(taPlan, comp));
                        if (null == taCompPlan) {
                            throw new ApplicationException("Для конкурса «"+comp.getTitle()+"» не настроен план приема по виду ЦП «"+taKind.getTitle()+"».");
                        }

                        group.setupPlan(taCompPlan.getPlan());
                    }
                }
            }
        } finally {
            Debug.end();
        }

        // вызываем логику внутри контекста
        context.afterInitCallback();
    }


    /** проверки непосредственно перед исполнением */
    protected void doCheckContext(final EnrEnrollmentStepBaseContext context) {

        /*
         * если есть объект «Информация о предыдущих зачислениях абитуриента для шага зачисления» в рамках данного шага, ссылающийся на выписку не в состоянии «Согласована/Проведена»,
         * то выводить сообщение «Не все выписки по абитуриентам согласованы, автоматический выбор абитуриентов невозможен.».
         */
        {
            final List<EnrEnrollmentStepItemOrderInfo> nonCompleteOrderInfoList = new DQLSelectBuilder()
            .fromEntity(EnrEnrollmentStepItemOrderInfo.class, "oi")
            .where(eq(property(EnrEnrollmentStepItemOrderInfo.item().step().fromAlias("oi")), value(context.getStep())))
            .where(notIn(property(EnrEnrollmentStepItemOrderInfo.extract().state().code().fromAlias("oi")), EnrEnrollmentStepDao.EXTRACT_STATUS_OK))
            .createStatement(this.getSession()).list();

            if (nonCompleteOrderInfoList.size() > 0) {
                throw new ApplicationException("Не все выписки по абитуриентам согласованы, автоматический выбор абитуриентов невозможен.");
            }
        }


        /*
         * если есть выписка, для которой должен быть объект «Информация о предыдущих зачислениях абитуриента для шага зачисления» в рамках данного шага, но его нет,
         * выводить сообщение «Данные приказов о зачислении изменились с момента фиксации списков, автоматический выбор абитуриентов невозможен.».
         */
        {
            final List<EnrEnrollmentExtract> newExtractList = new DQLSelectBuilder()
            .fromEntity(EnrEnrollmentExtract.class, "e").column("e")
            .where(in(
                property(EnrEnrollmentExtract.entity().request().entrant().id().fromAlias("e")),
                new DQLSelectBuilder()
                .fromEntity(EnrEnrollmentStepItem.class, "i")
                .column(property(EnrEnrollmentStepItem.entity().requestedCompetition().request().entrant().id().fromAlias("i")))
                .where(eq(property(EnrEnrollmentStepItem.step().fromAlias("i")), value(context.getStep())))
                .buildQuery()
            ))
            .where(notExists(
                new DQLSelectBuilder()
                .fromEntity(EnrEnrollmentStepItemOrderInfo.class, "oi")
                .where(eq(
                    property("e"),
                    property(EnrEnrollmentStepItemOrderInfo.extract().fromAlias("oi"))
                ))
                .where(eq(property(EnrEnrollmentStepItemOrderInfo.item().step().fromAlias("oi")), value(context.getStep())))
                .buildQuery()
            ))
            .createStatement(this.getSession()).list();

            if (newExtractList.size() > 0) {
                final EnrEnrollmentExtract first = newExtractList.iterator().next();
                throw new ApplicationException("Данные приказов о зачислении изменились с момента фиксации списков, автоматический выбор абитуриентов невозможен (создана новая выписка о зачислении: "+first.getTitle()+").");
            }
        }
    }


    /** выбирает абитуриентов */
    protected void doExecuteContext(final EnrSelectionContext context) {
        context.executeSelection();
    }

    /** сохраняет результаты в базу */
    protected void doApplyContext(final EnrSelectionContext context) {
        context.applyResults();
    }
}
