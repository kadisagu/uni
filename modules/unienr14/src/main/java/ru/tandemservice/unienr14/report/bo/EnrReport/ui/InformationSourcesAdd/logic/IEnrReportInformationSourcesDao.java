/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.InformationSourcesAdd.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.InformationSourcesAdd.EnrReportInformationSourcesAddUI;

/**
 * @author rsizonenko
 * @since 18.06.2014
 */
public interface IEnrReportInformationSourcesDao extends INeedPersistenceSupport {
    public long createReport(EnrReportInformationSourcesAddUI model);
}
