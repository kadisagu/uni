/* $Id$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.logic;

import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;

/**
 * @author Nikolay Fedorovskih
 * @since 30.06.2015
 */
public interface ICompetitionContextCustomizer
{
    void customizeCompetitionDQL(DQLSelectBuilder dql, ExecutionContext context, String competitionAlias);
}