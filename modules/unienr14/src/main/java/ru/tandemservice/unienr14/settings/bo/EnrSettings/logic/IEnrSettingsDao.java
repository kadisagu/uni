/* $Id:$ */
package ru.tandemservice.unienr14.settings.bo.EnrSettings.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.unienr14.competition.entity.IEnrollmentStudentSettings;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.List;

/**
 * @author oleyba
 * @since 8/15/14
 */
public interface IEnrSettingsDao extends INeedPersistenceSupport
{
    String ENR_SETTINGS = "enrSettings";
    String SETTINGS_ENTRANT_ONLINE_TREATED = "enrEntrantOnlineTreated";

    void doAutoFillEduOu(EnrEnrollmentCampaign campaign);

    void doClearEduOu(Long listenerParameterAsLong);

    List<EducationOrgUnit> getEduOuList(IEnrollmentStudentSettings settings);

    boolean checkAccountingRulesAgreementBudget(EnrEnrollmentCampaign campaign);

    boolean checkAccountingRulesAgreementContract(EnrEnrollmentCampaign campaign);

    //сохраняем НПП в настройку, обновляя признаки используемости и набора студентов
    void updateSettingsItem(IEnrollmentStudentSettings settings, EducationOrgUnit eduOU);
}