package ru.tandemservice.unienr14.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x0_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        tool.executeUpdate("delete from enr14_camp_entrant_doc_t where documenttype_id in (select id from enr14_c_entrant_doc_type_t where code_p = ?)", "3" /* EnrEntrantDocumentTypeCodes.STATE_EXAM_CERT */);
        tool.executeUpdate("delete from enr14_c_entrant_doc_type_t where code_p = ?", "3" /* EnrEntrantDocumentTypeCodes.STATE_EXAM_CERT */);

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrStateExamSubjectMark

		// сущность была удалена
		{
			// удалить таблицу
			tool.dropTable("enr14_state_exam_mark_t", false /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("enrStateExamSubjectMark");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrStateExamCertificate

		// сущность была удалена
		{
            tool.executeUpdate("delete from enr14_entr_req_attachment_t where document_id in (select id from enr14_state_exam_cert_t)");

            tool.dropView("ienrentrantrequestattachable_v");

			// удалить таблицу
			tool.dropTable("enr14_state_exam_cert_t", false /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("enrStateExamCertificate");
		}
    }
}
