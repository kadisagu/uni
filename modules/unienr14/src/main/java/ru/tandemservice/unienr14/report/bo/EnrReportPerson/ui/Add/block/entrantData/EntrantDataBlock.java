package ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.block.entrantData;

import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.catalog.entity.EnrAccessCourse;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantCustomStateType;
import ru.tandemservice.unienr14.catalog.entity.EnrSourceInfoAboutUniversity;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.EnrEnrollmentCommissionManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.Arrays;

/**
 * @author Vasily Zhukov
 * @since 08.02.2011
 */
public class EntrantDataBlock
{
    // names
    public static final String ACCESS_COURSES_DS = "accessCoursesDS";
    public static final String INFO_SOURCE_DS = "infoSourceDS";
    public static final String ENTRANT_CUSTOM_STATE_DS = "entrantCustomStateDS";


    public static void onBeforeDataSourceFetch(IUIDataSource dataSource, EntrantDataParam param)
    {
    }

    public static IDefaultComboDataSourceHandler createAccessCoursesDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, EnrAccessCourse.class);
    }

    public static IDefaultComboDataSourceHandler createInfoSourceDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, EnrSourceInfoAboutUniversity.class);
    }

    public static IDefaultComboDataSourceHandler createEntrantCustomStateDS(String name)
    {
        return new EntityComboDataSourceHandler(name, EnrEntrantCustomStateType.class)
                .order(EnrEntrantCustomStateType.title())
                .filter(EnrEntrantCustomStateType.title());
    }
}
