package ru.tandemservice.unienr14.order.logic;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import java.util.List;
import java.util.Set;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;

import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;

/**
 * @author vdanilov
 */
public class EnrRequestedCompetitionLockListenerBean {

    private static final IDSetEventListener ENR_COMPETITION_LISTENER = new IDSetEventListener() {
        @Override public void onEvent(final DSetEvent event) {
            final Set<String> properties = event.getMultitude().getAffectedProperties();
            if (properties.contains(EnrRequestedCompetition.P_PARALLEL)) {
                final List<Long> eeIds = new DQLSelectBuilder()
                .fromEntity(EnrEnrollmentExtract.class, "ee")
                .column(property("ee.id"))
                .where(in(property(EnrEnrollmentExtract.entity().id().fromAlias("ee")), event.getMultitude().getInExpression()))
                .createStatement(event.getContext()).list();
                if (eeIds.size() > 0) {
                    // TODO: if (eeIds.size > 1) { формировать сообщение о приказах }
                    throw new ApplicationException("Абитуриент уже включен в приказ о зачислении, измененение признака «Поступление на параллельное обучение» запрещено.", true);
                }
            }
        }
    };

    public void init() {
        DSetEventManager.getInstance().registerListener(DSetEventType.beforeUpdate, EnrRequestedCompetition.class, ENR_COMPETITION_LISTENER);
    }

}
