/* $Id$ */
package ru.tandemservice.unienr14.runtime;

import org.tandemframework.core.runtime.IRuntimeExtension;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import org.tandemframework.shared.person.base.bo.PersonEduDocument.PersonEduDocumentManager;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;

/**
 * @author azhebko
 * @since 25.06.2014
 */
@SuppressWarnings("deprecation")
public class EduDocumentMigrationRuntimeExtension implements IRuntimeExtension
{
    @Override
    @SuppressWarnings("deprecation")
    public void init(Object object)
    {
        if (!ISharedBaseDao.instance.get().existsEntity(PersonEduDocument.class)
            && ISharedBaseDao.instance.get().existsEntity(org.tandemframework.shared.person.base.entity.PersonEduInstitution.class))
            PersonEduDocumentManager.instance().dao().doMigrate();
    }

    @Override
    public void destroy()
    {
    }
}