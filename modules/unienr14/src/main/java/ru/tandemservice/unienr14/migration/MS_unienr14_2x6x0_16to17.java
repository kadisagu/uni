package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x0_16to17 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrEntrantStateExamResult

		// создано свойство verifiedByUserComment
		{
			// создать колонку
			tool.createColumn("enr14_state_exam_result_t", new DBColumn("verifiedbyusercomment_p", DBType.TEXT));
		}
    }
}