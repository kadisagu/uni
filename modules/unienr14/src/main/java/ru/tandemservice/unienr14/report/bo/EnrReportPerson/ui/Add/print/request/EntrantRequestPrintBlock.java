/* $Id: EntrantRequestPrintBlock.java 33259 2014-03-27 11:59:44Z azhebko $ */
package ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.print.request;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonCatalogUtil;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.*;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.print.ReportPrintListColumn;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.print.ReportPrintMapColumn;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.print.ReportPrintPathColumn;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unienr14.catalog.entity.EnrBenefitCategory;
import ru.tandemservice.unienr14.catalog.entity.EnrStateExamSubject;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult;
import ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroupScheduleEvent;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam;
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSource;
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceAppeal;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.EnrReportPersonAdd;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.EnrReportPersonAddUI;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.util.EnrReportPersonUtil;
import ru.tandemservice.unienr14.request.entity.*;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

/**
 * @author Vasily Zhukov
 * @since 31.08.2011
 */
public class EntrantRequestPrintBlock implements IReportPrintBlock
{
    private IReportPrintCheckbox _block = new ReportPrintCheckbox();
    private IReportPrintCheckbox _requestNumber = new ReportPrintCheckbox();
    private IReportPrintCheckbox _requestDate = new ReportPrintCheckbox();
    private IReportPrintCheckbox _technicCommission = new ReportPrintCheckbox().disabled();
    private IReportPrintCheckbox _examGroup = new ReportPrintCheckbox();

    private IReportPrintCheckbox _requestType = new ReportPrintCheckbox();
    private IReportPrintCheckbox _compensationType = new ReportPrintCheckbox();
    private IReportPrintCheckbox _developForm = new ReportPrintCheckbox();
    private IReportPrintCheckbox _competitionKind = new ReportPrintCheckbox();
    private IReportPrintCheckbox _enrOrgUnit = new ReportPrintCheckbox();
    private IReportPrintCheckbox _formativeOrgUnit = new ReportPrintCheckbox();
    //private IReportPrintCheckbox _programSubject = new ReportPrintCheckbox();
    private IReportPrintCheckbox _programSet = new ReportPrintCheckbox();

    private IReportPrintCheckbox _finalMark = new ReportPrintCheckbox();
    private IReportPrintCheckbox _priority = new ReportPrintCheckbox();
    private IReportPrintCheckbox _olympiad = new ReportPrintCheckbox();
    private IReportPrintCheckbox _disciplineResult = new ReportPrintCheckbox();
    private IReportPrintCheckbox _disciplineResultDetail = new ReportPrintCheckbox();
    private IReportPrintCheckbox _parallel = new ReportPrintCheckbox();
    private IReportPrintCheckbox _targetAdmission = new ReportPrintCheckbox();
    private IReportPrintCheckbox _targetAdmissionKind = new ReportPrintCheckbox();
    // private IReportPrintCheckbox _serviceLength = new ReportPrintCheckbox();
    private IReportPrintCheckbox _documentState = new ReportPrintCheckbox();
    private IReportPrintCheckbox _accepted = new ReportPrintCheckbox();
    private IReportPrintCheckbox _enrollmentAvailable = new ReportPrintCheckbox();
    private IReportPrintCheckbox _passPartnerEduInstitution = new ReportPrintCheckbox();
    private IReportPrintCheckbox _directionState = new ReportPrintCheckbox();
    private IReportPrintCheckbox _stateExamInternal = new ReportPrintCheckbox();
    private IReportPrintCheckbox _directionComment = new ReportPrintCheckbox();
    private IReportPrintCheckbox _requestComment = new ReportPrintCheckbox();
    private IReportPrintCheckbox _stateExam = new ReportPrintCheckbox();
    private IReportPrintCheckbox _stateExamDetail = new ReportPrintCheckbox();
    private IReportPrintCheckbox _benefit = new ReportPrintCheckbox();
    private IReportPrintCheckbox _filedByTrustee = new ReportPrintCheckbox();
    private IReportPrintCheckbox _originalSubmissionWay = new ReportPrintCheckbox();
    private IReportPrintCheckbox _originalReturnWay = new ReportPrintCheckbox();
    private IReportPrintCheckbox _stateExamResultStatus = new ReportPrintCheckbox();
    private IReportPrintCheckbox _achievementsMarks = new ReportPrintCheckbox();
    private IReportPrintCheckbox _achievementSumMark = new ReportPrintCheckbox();
    private IReportPrintCheckbox _acceptRequest = new ReportPrintCheckbox();
    private IReportPrintCheckbox _targetContractOrgUnit = new ReportPrintCheckbox();
    private IReportPrintCheckbox _targetContract = new ReportPrintCheckbox();

    @Override
    public String getCheckJS()
    {
        List<String> ids = new ArrayList<>();
        for (int i = 1; i <= 35; i++)
            ids.add("chRequest" + StringUtils.leftPad(""+i, 3, '0'));
        return ReportJavaScriptUtil.getCheckJS(ids);
    }

    @SuppressWarnings("unused")
    @Override
    public void modify(ReportDQL dql, final IReportPrintInfo printInfo)
    {
        // соединяем абитуриента через left join, потому что мог быть выбран фильтр "не является абитуриентом"
        // если абитуриент был ранее соединен по inner join, то останется inner join
        String entrantAlias = dql.leftJoinEntity(EnrEntrant.class, EnrEntrant.person());

        // соединяем заявление абитуриента через left join (так как заявления может не быть)
        String requestAlias = dql.leftJoinEntity(entrantAlias, EnrEntrantRequest.class, EnrEntrantRequest.entrant());

        // соединяем выбранные конкрусы с заявлением
        String reqCompAlias = dql.leftJoinEntity(requestAlias, EnrRequestedCompetition.class, EnrRequestedCompetition.request());

        // соединяем результаты ЕГЭ
        String stateExamAlias = dql.leftJoinEntity(entrantAlias, EnrEntrantStateExamResult.class, EnrEntrantStateExamResult.entrant());

        // добавляем колонку entrant, агрегируем в список
        final int entrantIndex = dql.addListAggregateColumn(entrantAlias);

        // добавляем колонку request, агрегируем в мап
        final int requestIndex = dql.addMapAggregateColumn(requestAlias, entrantIndex);

        // добавляем колонку direction, агрегируем в мап
        final int reqCompIndex = dql.addMapAggregateColumn(reqCompAlias, entrantIndex, requestIndex);

        // добавляем колонку stateExamResult, агрегируем в список
        final int stateExamResultIndex = dql.addListAggregateColumn(stateExamAlias);

        if (_requestNumber.isActive())
            printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("requestNumber", requestIndex, null, new IFormatter<EnrEntrantRequest>() {
                @Override
                public String format(EnrEntrantRequest entrantRequest)
                {
                    return entrantRequest == null ? null : entrantRequest.getStringNumber();
                }
            }));

        if (_requestDate.isActive())
            printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("requestDate", requestIndex, null, new IFormatter<EnrEntrantRequest>() {
                @Override
                public String format(EnrEntrantRequest entrantRequest)
                {
                    return entrantRequest == null ? null : DateFormatter.DEFAULT_DATE_FORMATTER.format(entrantRequest.getRegDate());
                }
            }));

        if (_requestType.isActive())
            printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("requestType", reqCompIndex, EnrRequestedCompetition.competition().requestType().title()));

        if (_compensationType.isActive())
            printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("compensationType", reqCompIndex, EnrRequestedCompetition.competition().type().compensationType().shortTitle()));

        if (_developForm.isActive())
            printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("developForm", reqCompIndex, EnrRequestedCompetition.competition().programSetOrgUnit().programSet().programForm().title()));

        if (_competitionKind.isActive())
            printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("competitionKind", reqCompIndex, EnrRequestedCompetition.competition().type().title()));

        if (_enrOrgUnit.isActive())
            printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("enrOrgUnit", reqCompIndex, EnrRequestedCompetition.competition().programSetOrgUnit().orgUnit().institutionOrgUnit().orgUnit().title()));

        if (_formativeOrgUnit.isActive())
            printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("formativeOrgUnit", reqCompIndex, EnrRequestedCompetition.competition().programSetOrgUnit().formativeOrgUnit().title()));

        printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("programSubject", reqCompIndex, EnrRequestedCompetition.competition().programSetOrgUnit().programSet().programSubject().titleWithCode()));

        if (_programSet.isActive())
            printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("programSet", reqCompIndex, EnrRequestedCompetition.competition().programSetOrgUnit().programSet().title()));

        if (_targetAdmission.isActive())
            printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("targetAdmission", reqCompIndex, null, new IFormatter<EnrRequestedCompetition>()
            {
                @Override public String format(EnrRequestedCompetition source) {
                    return source == null ? null : ((source instanceof EnrRequestedCompetitionTA) ? "да" : "нет");
                }
            }));

        if (_targetAdmissionKind.isActive())
            printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("targetAdmissionKind", reqCompIndex, null, new IFormatter<EnrRequestedCompetition>()
                {
                    @Override public String format(EnrRequestedCompetition source) {
                        return source == null ? null : ((source instanceof EnrRequestedCompetitionTA) ? ((EnrRequestedCompetitionTA)source).getTargetAdmissionKind().getTitle() : null);
                    }
                }));

        if (_parallel.isActive())
            printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("parallel", reqCompIndex, EnrRequestedCompetition.parallel(), new IFormatter<Boolean>()
            {
                @Override
                public String format(Boolean source)
                {
                    return source != null ? (source ? "да" : null) : null;
                }
            }));

        if (_benefit.isActive())
            printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("benefit", reqCompIndex, null, new IFormatter<EnrRequestedCompetition>() {
                @Override
                public String format(EnrRequestedCompetition competition)
                {
                    if (competition == null) return null;

                    List<EnrBenefitCategory> benefits = new ArrayList<>();

                    if (competition instanceof EnrRequestedCompetitionNoExams) {
                        benefits.add(((EnrRequestedCompetitionNoExams)competition).getBenefitCategory());
                    }
                    if (competition instanceof EnrRequestedCompetitionExclusive) {
                        benefits.add(((EnrRequestedCompetitionExclusive)competition).getBenefitCategory());
                    }
                    if (competition.getRequest().getBenefitCategory() != null) {
                        benefits.add(competition.getRequest().getBenefitCategory());
                    }

                    StringBuilder result = new StringBuilder();
                    for (EnrBenefitCategory benefit : benefits) {
                        if (result.length() != 0) result.append("\n");
                        result.append(benefit.getBenefitType().getShortTitle()).append("\n").append("категория: ").append(benefit.getShortTitle());
                    }
                    return result.toString();
                }
            }));

        if (_directionState.isActive())
            printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("directionState", reqCompIndex, EnrRequestedCompetition.state().title()));

        if (_documentState.isActive())
            printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("documentState", reqCompIndex, EnrRequestedCompetition.originalDocumentHandedIn(), new YesNoFormatter()));
        if (_accepted.isActive())
            printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("accepted", reqCompIndex, EnrRequestedCompetition.accepted(), new YesNoFormatter()));
        if (_enrollmentAvailable.isActive())
            printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("enrollmentAvailable", reqCompIndex, EnrRequestedCompetition.enrollmentAvailable(), new YesNoFormatter()));

//        if (_passPartnerEduInstitution.isActive())
//            printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("passPartnerEduInstitution", requestIndex, null, new IFormatter<EnrEntrantRequest>()
//            {
//                @Override
//                public String format(EnrEntrantRequest source)
//                {
//                    if (source == null || source.getEduInstitution().getEduInstitution() == null)
//                        return null;
//
//                    final List<Long> partnerEduInstitutionList = EnrReportPersonUtil.getPartnerEduInstitutionList(printInfo);
//                    if (CollectionUtils.isEmpty(partnerEduInstitutionList))
//                        return null;
//
//                    return partnerEduInstitutionList.contains(source.getEduInstitution().getEduInstitution().getId()) ? "да" : "нет";
//                }
//            })
//            {
//                @Override
//                public void prefetch(List<Object[]> rows)
//                {
//                    EnrReportPersonUtil.prefetchPassPartnerEduInstitution(printInfo);
//                }
//            });

        if (_examGroup.isActive()) {
            printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("examGroup", reqCompIndex, null, new IFormatter<EnrRequestedCompetition>() {
                @Override
                public String format(EnrRequestedCompetition competition)
                {
                    if (competition == null) return null;

                    final List<EnrExamGroupScheduleEvent> events = EnrReportPersonUtil.getEntrantExamGroups(printInfo, competition);
                    if (null == events || events.isEmpty()) return null;

                    final List<String> titleList = new ArrayList<>(events.size());
                    for (EnrExamGroupScheduleEvent event: events) {
                        titleList.add(event.getExamGroup().getTitle() + " (" + event.getExamScheduleEvent().getScheduleEvent().getBeginDateStr() + " " + event.getExamScheduleEvent().getScheduleEvent().getTimePeriodStr() + ", ауд. " + event.getExamScheduleEvent().getScheduleEventPlace().getPlace().getNumber() + ")");
                    }

                    Collections.sort(titleList);
                    return StringUtils.join(titleList, '\n');
                }
            }) {
                @Override
                public void prefetch(List<Object[]> rows)
                {
                    EnrReportPersonUtil.prefetchEntrantExamGroups(printInfo, rows, reqCompIndex);
                }
            });
        }

        if (_finalMark.isActive())
            printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("finalMark", reqCompIndex, null, new IFormatter<EnrRequestedCompetition>() {
                @Override public String format(EnrRequestedCompetition direction) {
                    if (direction == null) return null;
                    EnrRatingItem ratingItem = EnrReportPersonUtil.getRatingItem(printInfo, direction.getId());
                    if (ratingItem == null) return null;

                    return ratingItem.getTotalMarkAsString();
                }
            })
            {
                @Override public void prefetch(List<Object[]> rows) {
                    EnrReportPersonUtil.prefetchRatingData(printInfo, rows, reqCompIndex);
                }
            }
                .number());

        if (_priority.isActive())
            printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("priority", reqCompIndex, EnrRequestedCompetition.priority()).number());

        if (_disciplineResult.isActive())
            printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("disciplineResult", reqCompIndex, null, new IFormatter<EnrRequestedCompetition>() {
                @Override
                public String format(EnrRequestedCompetition direction)
                {
                    if (direction == null) return null;

                    Map<EnrCampaignDiscipline, EnrChosenEntranceExam> chosenExamMap = EnrReportPersonUtil.getChosenExamMap(printInfo, direction.getId());

                    List<EnrChosenEntranceExam> disciplineList = new ArrayList<>(chosenExamMap.values());

                    Collections.sort(disciplineList, new EntityComparator<EnrChosenEntranceExam>(new EntityOrder(EnrChosenEntranceExam.discipline().stateExamSubject().code().s())));

                    List<String> disciplineTitleList = new ArrayList<>();
                    for (EnrChosenEntranceExam chosen : disciplineList)
                    {
                        String passForm = null;
                        if (chosen.getMaxMarkForm() != null && chosen.getMaxMarkForm().getMarkSource() != null)
                        {
                            final EnrEntrantMarkSource markSource = chosen.getMaxMarkForm().getMarkSource();
                            if (markSource instanceof EnrEntrantMarkSourceAppeal)
                                passForm = chosen.getMaxMarkForm().getPassForm().getTitle() + ", апелл.";
                            else
                                passForm = chosen.getMaxMarkForm().getPassForm().getTitle();
                        }

                        disciplineTitleList.add(chosen.getDiscipline().getTitle() + " - " + (chosen.getMarkAsDouble() == null ? "" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(chosen.getMarkAsDouble())) + (passForm == null ? "" : " (" + passForm + ")"));
                    }

                    return StringUtils.join(disciplineTitleList, '\n');
                }
            })
            {
                @Override public void prefetch(List<Object[]> rows)
                {
                    EnrReportPersonUtil.prefetchMarkData(printInfo, rows, reqCompIndex);
                }
            });

        // детализация по дисциплинам строится только при выбранной приемной кампании
        if (_disciplineResultDetail.isActive() && printInfo.getSharedObject(EnrReportPersonAdd.PREFETCH_ENROLLMENT_CAMPAIGN) != null)
        {
            EnrEnrollmentCampaign enrollmentCampaign = printInfo.getSharedObject(EnrReportPersonAdd.PREFETCH_ENROLLMENT_CAMPAIGN);

            List<EnrCampaignDiscipline> disciplineList = DataAccessServices.dao().getList(EnrCampaignDiscipline.class, EnrCampaignDiscipline.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign, EnrCampaignDiscipline.discipline().code().s());

            for (final EnrCampaignDiscipline discipline : disciplineList)
            {
                printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("disciplineResultDetail", reqCompIndex, null, new IFormatter<EnrRequestedCompetition>()
                {
                    @Override
                    public String format(EnrRequestedCompetition direction)
                    {
                        if (direction == null) return null;

                        EnrChosenEntranceExam chosen = EnrReportPersonUtil.getChosenExamMap(printInfo, direction.getId()).get(discipline);
                        if (chosen == null) return null;
                        return chosen.getMarkAsDouble() == null ? null : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(chosen.getMarkAsDouble());
                    }
                })
                {
                    @Override public void prefetch(List<Object[]> rows)
                    {
                        EnrReportPersonUtil.prefetchMarkData(printInfo, rows, reqCompIndex);
                    }
                }
                        .title(discipline.getShortTitle()).number());
            }
        }


//        if (_serviceLength.isActive())
//            printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("serviceLength", directionIndex, null, new IFormatter<EnrRequestedCompetition>()
//            {
//                @Override
//                public String format(EnrRequestedCompetition direction)
//                {
//                    if (direction == null) return null;
//
//                    List<String> itemList = new ArrayList<>();
//                    if (direction.getProfileWorkExperienceYears() != null)
//                        itemList.add("лет: " + direction.getProfileWorkExperienceYears());
//                    if (direction.getProfileWorkExperienceMonths() != null)
//                        itemList.add("месяцев: " + direction.getProfileWorkExperienceMonths());
//                    if (direction.getProfileWorkExperienceDays() != null)
//                        itemList.add("дней: " + direction.getProfileWorkExperienceDays());
//                    return StringUtils.join(itemList.iterator(), "; ");
//                }
//            }));

//        if (_stateExamInternal.isActive())
//            printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("stateExamInternal", directionIndex, null, new IFormatter<EnrRequestedCompetition>()
//            {
//                @Override
//                public String format(EnrRequestedCompetition direction)
//                {
//                    if (direction == null) return null;
//
//                    EnrRatingDataUtil util = EnrReportPersonUtil.getEnrRatingDataUtil(printInfo, direction);
//
//                    List<EnrChosenEntranceExam> disciplineList = new ArrayList<>(util.getChosenExamSet(direction));
//                    Collections.sort(disciplineList, new EntityComparator<EnrChosenEntranceExam>(new EntityOrder(EnrChosenEntranceExam.discipline().discipline().code().s())));
//
//                    List<String> disciplineStateInternalTitleList = new ArrayList<>();
//                    for (EnrChosenEntranceExam chosen : disciplineList)
//                    {
//
//                        if (chosen.getMaxMarkForm() != null && chosen.getMaxMarkForm().getPassForm().getCode().equals(EnrExamPassFormCodes.STATE_EXAM_INTERNAL))
//                        {
//                            double finalMark = chosen.getMarkAsDouble();
//                            disciplineStateInternalTitleList.add(chosen.getDiscipline().getTitle() + " - " + (finalMark == -1.0 ? "" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(finalMark)));
//                        }
//                    }
//                    return StringUtils.join(disciplineStateInternalTitleList, '\n');
//                }
//            })
//            {
//                @Override public void prefetch(List<Object[]> rows)
//                {
//                    EnrReportPersonUtil.prefetchEnrRatingDataUtil(printInfo, rows, directionIndex);
//                }
//            });

        if (_directionComment.isActive())
            printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("directionComment", reqCompIndex, EnrRequestedCompetition.comment()));

        //Принял заявление
        if (_acceptRequest.isActive())
            printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("acceptRequest", requestIndex, EnrEntrantRequest.acceptRequest()));

        if (_requestComment.isActive())
            printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("requestComment", requestIndex, EnrEntrantRequest.comment()));


        if (_filedByTrustee.isActive())
            printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("filedByTrustee", requestIndex, null, new IFormatter<EnrEntrantRequest>()
            {
                @Override
                public String format(EnrEntrantRequest source)
                {
                    if (source == null) return "";
                    String trusteeDetails = StringUtils.trimToNull(source.getTrusteeDetails());
                    return source.isFiledByTrustee() ? "да" + (trusteeDetails == null ? "" : " (" + source.getTrusteeDetails() + ")") : "нет";
                }
            }));

        if (_originalSubmissionWay.isActive())
            printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("originalSubmissionWay", requestIndex, EnrEntrantRequest.originalSubmissionWay().title()));

        if (_originalReturnWay.isActive())
            printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("originalReturnWay", requestIndex, EnrEntrantRequest.originalReturnWay().title()));

        if (_olympiad.isActive()) {
            printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("olympiad", reqCompIndex, null, new IFormatter<EnrRequestedCompetition>() {
                @Override
                public String format(EnrRequestedCompetition direction)
                {
                    if (direction == null) return null;

                    final List<EnrOlympiadDiploma> entrantOlympiad = EnrReportPersonUtil.getEntrantOlympiad(printInfo, direction.getRequest().getEntrant());
                    if (null == entrantOlympiad || entrantOlympiad.isEmpty()) return null;

                    // превращаем в строки
                    final List<String> diplomaTitleList = new ArrayList<>(entrantOlympiad.size());
                    for (EnrOlympiadDiploma od: entrantOlympiad)
                    {
                        StringBuilder builder = new StringBuilder()
                                .append(od.getTitle()).append(", ")
                                .append(StringUtils.uncapitalize(od.getHonour().getTitle()))
                                .append(", «").append(od.getOlympiad().getTitle()).append("»")
                                .append(", предмет олимпиады: ")
                                .append(od.getSubject().getTitle());

                        diplomaTitleList.add(builder.toString());
                    }

                    // сортируем все еще раз
                    Collections.sort(diplomaTitleList);
                    return StringUtils.join(diplomaTitleList, '\n');
                }
            }) {
                @Override public void prefetch(List<Object[]> rows) {
                    EnrReportPersonUtil.prefetchEntrantOlympiad(printInfo, rows, entrantIndex);
                }
            });
        }

        final List<EnrStateExamSubject> stateExamSubjectList = DataAccessServices.dao().getList(EnrStateExamSubject.class);
        final int ENTRANT_MARK_IDX = 0;
        final int CERTIFICATE_NUMBER_IDX = 1;
        Collections.sort(stateExamSubjectList, CommonCatalogUtil.CATALOG_CODE_COMPARATOR);

        if (_stateExam.isActive())
        {
            printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintListColumn("stateExam", entrantIndex, null, new IFormatter<EnrEntrant>()
            {
                @Override
                public String format(EnrEntrant entrant)
                {
                    if (entrant == null) return null;

                    List<String> yearList = new ArrayList<>();
                    Map<Integer, Map<Long, Object[]>> yearMap = EnrReportPersonUtil.getStateExamSubjectMarkMap(printInfo, entrant);
                    if (yearMap != null)
                    {
                        for (Map.Entry<Integer, Map<Long, Object[]>> yearEntry: yearMap.entrySet())
                        {
                            Integer year = yearEntry.getKey();
                            List<String> marks = new ArrayList<>();
                            for (EnrStateExamSubject subject : stateExamSubjectList)
                            {
                                Object[] data = yearEntry.getValue().get(subject.getId());
                                if (data != null)
                                    marks.add(subject.getShortTitle() + " - " + String.valueOf(data[ENTRANT_MARK_IDX]));
                            }

                            yearList.add(String.valueOf(year) + ": " + StringUtils.join(marks, ", ") + ";");
                        }
                    }

                    return StringUtils.join(yearList, '\n');
                }
            })
            {
                @Override
                public void prefetch(List<Object[]> rows)
                {
                    EnrReportPersonUtil.prefetchStateExamSubjectMarkMap(printInfo, rows, entrantIndex, stateExamResultIndex);
                }
            });
        }

        if (_stateExamDetail.isActive())
        {
            for (final EnrStateExamSubject stateExamSubject : stateExamSubjectList)
            {
                printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintListColumn("stateExamDetail", entrantIndex, null, new IFormatter<EnrEntrant>() {
                    @Override public String format(EnrEntrant entrant) {
                        if (entrant == null) return null;
                        Map<Integer, Map<Long, Object[]>> yearMap = EnrReportPersonUtil.getStateExamSubjectMarkMap(printInfo, entrant);
                        Integer mark = null;
                        if (yearMap != null)
                        {
                            for (Map<Long, Object[]> subjectMap: yearMap.values())
                            {
                                Object[] data = subjectMap.get(stateExamSubject.getId());
                                if (data != null && (mark == null || (Integer) data[ENTRANT_MARK_IDX] > mark))
                                    mark = (Integer) data[ENTRANT_MARK_IDX];
                            }
                        }
                        return mark == null ? null : Integer.toString(mark);
                    }
                }) {
                    @Override public void prefetch(List<Object[]> rows) {
                        EnrReportPersonUtil.prefetchStateExamSubjectMarkMap(printInfo, rows, entrantIndex, stateExamResultIndex);
                    }
                }.title(stateExamSubject.getShortTitle()).number());
            }
        }

        if (_stateExamResultStatus.isActive())
        {
            printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintListColumn("stateExamResultStatus", entrantIndex, null, new IFormatter<EnrEntrant>()
            {
                @Override
                public String format(EnrEntrant entrant)
                {
                    if (entrant == null) return null;
                    List<EnrEntrantStateExamResult> stateExamResults = EnrReportPersonUtil.getStateExamResults(printInfo, entrant);
                    Comparator<EnrEntrantStateExamResult> comparator = new EntityComparator<>(
                            new EntityOrder(EnrEntrantStateExamResult.subject().code().s()),
                            new EntityOrder(EnrEntrantStateExamResult.markAsLong().s())
                    );
                    Collections.sort(stateExamResults, comparator);

                    List<String> result = new ArrayList<>();
                    for (EnrEntrantStateExamResult stateExamResult: stateExamResults)
                    {
                        List<String> properties = new ArrayList<>();

                        properties.add(stateExamResult.getSubject().getShortTitle() + (stateExamResult.getMarkAsLong() == null ? "" : " " + stateExamResult.getMark()));

                        if (stateExamResult.isAccepted())
                            properties.add("зачтен");

                        if (stateExamResult.getStateCheckStatus() != null)
                            properties.add("ФИС - " + stateExamResult.getStateCheckStatus());

                        if (stateExamResult.isVerifiedByUser())
                            properties.add("пров. вручную" + (stateExamResult.getVerifiedByUserComment() == null ? "" : " - " + stateExamResult.getVerifiedByUserComment()));

                        properties.add(String.valueOf(stateExamResult.getYear()));

                        if (stateExamResult.getDocumentNumber() != null)
                            properties.add("свид. " + stateExamResult.getDocumentNumber());

                        result.add(StringUtils.join(properties, ", "));
                    }

                    return StringUtils.join(result, '\n');
                }
            })
            {
                @Override public void prefetch(List<Object[]> rows){ EnrReportPersonUtil.prefetchStateExamResults(printInfo, rows, stateExamResultIndex); }
            });
        }

        final NumberFormat formatter = new DecimalFormat("#0.##");

        // Баллы за индивидуальные достижения
        if (_achievementsMarks.isActive())
        {
            printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("achievementsMarks", requestIndex, null, new IFormatter<EnrEntrantRequest>() {
                @Override
                public String format(EnrEntrantRequest request) {
                    if (request == null) return null;

                    List<EnrEntrantAchievement> achievements = EnrReportPersonUtil.getEntrantAchievements(printInfo, request.getId());

                    if (achievements == null) return null;

                    StringBuilder builder = new StringBuilder();

                    int num = 0;

                    for (EnrEntrantAchievement achievement : achievements) {
                        if (builder.length() != 0) builder.append("\n");
                        builder.append(++num).append(". ").append(achievement.getType().getAchievementKind().getShortTitle())
                                .append(" — ").append(formatter.format(achievement.getRatingMarkAsLong()/1000.0));
                    }

                    return builder.toString();
                }
            })
            {
                @Override
                public void prefetch(List<Object[]> rows) {
                    EnrReportPersonUtil.prefetchAchievementData(printInfo, rows, requestIndex);
                }
            });
        }
        // Сумма баллов за индивидуальные достижения
        if (_achievementSumMark.isActive())
        {
            printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("achievementSumMark", reqCompIndex, null, new IFormatter<EnrRequestedCompetition>() {
                @Override public String format(EnrRequestedCompetition direction) {
                    if (direction == null) return null;
                    EnrRatingItem ratingItem = EnrReportPersonUtil.getRatingItem(printInfo, direction.getId());
                    if (ratingItem == null) return null;

                    return formatter.format(ratingItem.getAchievementMarkAsLong()/1000.0);
                }
            })
            {
                @Override public void prefetch(List<Object[]> rows) {
                    EnrReportPersonUtil.prefetchRatingData(printInfo, rows, reqCompIndex);
                }
            });
        }

        //Организация - заказчик
        if (_targetContractOrgUnit.isActive())
            printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("targetContractOrgUnit", reqCompIndex, EnrRequestedCompetition.externalOrgUnit().titleWithLegalForm()));

        //Целевая контрактная подготовка
        if (_targetContract.isActive())
            printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("targetContract", reqCompIndex, EnrRequestedCompetition.competition().programSetOrgUnit().programSet().targetContractTraining(), new IFormatter<Boolean>()
            {
                @Override
                public String format(Boolean source)
                {
                    return source != null? (source ? "да" : "нет") : null;
                }
            }));


        // Итоговые данные

        printInfo.addPrintTotalData(EnrReportPersonAddUI.ENTRANT_SCHEET, new IReportPrintTotalData()
        {
            @Override
            public void prefetch(List<Object[]> rows)
            {
                EnrReportPersonUtil.prefetchCountEntrantsTotalData(printInfo, rows, entrantIndex);
            }

            @Override
            public String getName()
            {
                return EnrReportPersonAdd.PREFETCH_TOTAL_DATA_ENTRANTS;
            }

            @Override
            public String getValue(ReportPrintInfo printInfo)
            {
                return printInfo.getSharedObject(EnrReportPersonAdd.PREFETCH_TOTAL_DATA_ENTRANTS).toString();
            }
        });

        printInfo.addPrintTotalData(EnrReportPersonAddUI.ENTRANT_SCHEET, new IReportPrintTotalData()
        {
            @Override
            public void prefetch(List<Object[]> rows)
            {
                EnrReportPersonUtil.prefetchCountRequestsTotalData(printInfo, rows, requestIndex);
            }

            @Override
            public String getName()
            {
                return EnrReportPersonAdd.PREFETCH_TOTAL_DATA_REQUESTS;
            }

            @Override
            public String getValue(ReportPrintInfo printInfo)
            {
                return printInfo.getSharedObject(EnrReportPersonAdd.PREFETCH_TOTAL_DATA_REQUESTS).toString();
            }
        });

        printInfo.addPrintTotalData(EnrReportPersonAddUI.ENTRANT_SCHEET, new IReportPrintTotalData()
        {
            @Override
            public void prefetch(List<Object[]> rows)
            {
                EnrReportPersonUtil.prefetchCountCompetitionsTotalData(printInfo, rows, reqCompIndex);
            }

            @Override
            public String getName()
            {
                return EnrReportPersonAdd.PREFETCH_TOTAL_DATA_COMPETITIONS;
            }

            @Override
            public String getValue(ReportPrintInfo printInfo)
            {
                return printInfo.getSharedObject(EnrReportPersonAdd.PREFETCH_TOTAL_DATA_COMPETITIONS).toString();
            }
        });
    }

    // Getters

    public IReportPrintCheckbox getBlock()
    {
        return _block;
    }

    public IReportPrintCheckbox getRequestNumber()
    {
        return _requestNumber;
    }

    public IReportPrintCheckbox getRequestDate()
    {
        return _requestDate;
    }

    public IReportPrintCheckbox getTechnicCommission()
    {
        return _technicCommission;
    }

    public IReportPrintCheckbox getExamGroup()
    {
        return _examGroup;
    }

    public IReportPrintCheckbox getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    public IReportPrintCheckbox getDevelopForm()
    {
        return _developForm;
    }

    public IReportPrintCheckbox getCompensationType()
    {
        return _compensationType;
    }

    public IReportPrintCheckbox getFinalMark()
    {
        return _finalMark;
    }

    public IReportPrintCheckbox getPriority()
    {
        return _priority;
    }

    public IReportPrintCheckbox getOlympiad()
    {
        return _olympiad;
    }

    public IReportPrintCheckbox getDisciplineResult()
    {
        return _disciplineResult;
    }

    public IReportPrintCheckbox getDisciplineResultDetail()
    {
        return _disciplineResultDetail;
    }

    public IReportPrintCheckbox getParallel()
    {
        return _parallel;
    }

    public IReportPrintCheckbox getTargetAdmission()
    {
        return _targetAdmission;
    }

    public IReportPrintCheckbox getTargetAdmissionKind()
    {
        return _targetAdmissionKind;
    }

    public IReportPrintCheckbox getPassPartnerEduInstitution()
    {
        return _passPartnerEduInstitution;
    }

    public IReportPrintCheckbox getCompetitionKind()
    {
        return _competitionKind;
    }

//    public IReportPrintCheckbox getServiceLength()
//    {
//        return _serviceLength;
//    }

    public IReportPrintCheckbox getDocumentState()
    {
        return _documentState;
    }

    public IReportPrintCheckbox getDirectionState()
    {
        return _directionState;
    }

    public IReportPrintCheckbox getStateExamInternal()
    {
        return _stateExamInternal;
    }

    public IReportPrintCheckbox getDirectionComment()
    {
        return _directionComment;
    }

    public IReportPrintCheckbox getRequestComment()
    {
        return _requestComment;
    }

    public IReportPrintCheckbox getStateExam()
    {
        return _stateExam;
    }

    public IReportPrintCheckbox getStateExamDetail()
    {
        return _stateExamDetail;
    }

    public IReportPrintCheckbox getBenefit()
    {
        return _benefit;
    }

    public IReportPrintCheckbox getRequestType()
    {
        return _requestType;
    }

    public IReportPrintCheckbox getEnrOrgUnit()
    {
        return _enrOrgUnit;
    }

    public IReportPrintCheckbox getProgramSet()
    {
        return _programSet;
    }

    public IReportPrintCheckbox getFiledByTrustee(){ return _filedByTrustee; }

    public IReportPrintCheckbox getOriginalSubmissionWay(){ return _originalSubmissionWay; }

    public IReportPrintCheckbox getOriginalReturnWay(){ return _originalReturnWay; }

    public IReportPrintCheckbox getStateExamResultStatus(){ return _stateExamResultStatus; }

    public IReportPrintCheckbox getAchievmentSumMark() {
        return _achievementSumMark;
    }

    public IReportPrintCheckbox getAchievmentsMarks() {
        return _achievementsMarks;
    }

    public IReportPrintCheckbox getAcceptRequest()
    {
        return _acceptRequest;
    }

    public IReportPrintCheckbox getTargetContract()
    {
        return _targetContract;
    }

    public IReportPrintCheckbox getTargetContractOrgUnit()
    {
        return _targetContractOrgUnit;
    }

    public IReportPrintCheckbox getAccepted() {
        return _accepted;
    }

    public IReportPrintCheckbox getEnrollmentAvailable() {
        return _enrollmentAvailable;
    }
}
