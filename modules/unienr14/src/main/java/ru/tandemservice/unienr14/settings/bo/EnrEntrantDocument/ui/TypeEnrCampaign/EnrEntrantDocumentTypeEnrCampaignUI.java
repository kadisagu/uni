/**
 *$Id: EnrEntrantDocumentTypeEnrCampaignUI.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.settings.bo.EnrEntrantDocument.ui.TypeEnrCampaign;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.meta.entity.data.ItemPolicy;
import org.tandemframework.core.runtime.EntityDataRuntime;
import org.tandemframework.core.tool.synchronization.SynchronizeMeta;
import org.tandemframework.shared.commonbase.base.util.Zlo;

import org.tandemframework.shared.person.catalog.entity.PersonDocumentType;
import ru.tandemservice.unienr14.catalog.entity.codes.PersonDocumentTypeCodes;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrEntrantDocument.EnrEntrantDocumentManager;
import ru.tandemservice.unienr14.settings.bo.EnrEntrantDocument.logic.EnrEntrantDocumentTypeEnrCampaignSearchDSHandler;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignEntrantDocument;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

/**
 * @author Alexander Shaburov
 * @since 30.04.13
 */
public class EnrEntrantDocumentTypeEnrCampaignUI extends UIPresenter
{
    @Override
    public void onComponentRefresh()
    {
        final EnrEnrollmentCampaign defaultCampaign = EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign();
        getSettings().set(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, defaultCampaign);

        if (defaultCampaign != null) {
            EnrEntrantDocumentManager.instance().typeDao().doSetEnrCampDefaultSettings(defaultCampaign.getId());
        }
    }

    public boolean isSystemItem()
    {
        final PersonDocumentType entity = getConfig().getDataSource(EnrEntrantDocumentTypeEnrCampaign.ENTRANT_DOCUMENT_TYPE_SEARCH_DS).<DataWrapper>getCurrent().getWrapped();
        final ItemPolicy itemPolicy = EntityDataRuntime.getEntityPolicy(PersonDocumentType.ENTITY_NAME).getItemPolicy(entity.getCode());
        return itemPolicy != null && !SynchronizeMeta.temp.equals(itemPolicy.getSynchronize());
    }

    public boolean isSystemItemExcludeOlympiad()
    {
        final PersonDocumentType entity = getConfig().getDataSource(EnrEntrantDocumentTypeEnrCampaign.ENTRANT_DOCUMENT_TYPE_SEARCH_DS).<DataWrapper>getCurrent().getWrapped();
        return !PersonDocumentTypeCodes.OLYMP_DIPLOMA.equals(entity.getCode()) && isSystemItem();
    }

    public boolean isEduDocOrIdentityCard()
    {
        final PersonDocumentType entity = getConfig().getDataSource(EnrEntrantDocumentTypeEnrCampaign.ENTRANT_DOCUMENT_TYPE_SEARCH_DS).<DataWrapper>getCurrent().getWrapped();
        return PersonDocumentTypeCodes.INDENTITY_CARD.equals(entity.getCode()) || PersonDocumentTypeCodes.EDU_INSTITUTION.equals(entity.getCode());
    }

    public void onToggleEntrantDocumentTypeUse() {
        EnrEntrantDocumentManager.instance().typeDao().doToggleUsed(getListenerParameterAsLong(), getEnrEnrollmentCampaign().getId(), true);
        getSupport().setRefreshScheduled(true);
    }

    public void onToggleEntrantDocumentTypeNotUse() {
        EnrEntrantDocumentManager.instance().typeDao().doToggleUsed(getListenerParameterAsLong(), getEnrEnrollmentCampaign().getId(), false);
        getSupport().setRefreshScheduled(true);
    }

    @Zlo("метод должен быть один, с параметром")
    public void onToggleEntrantDocumentTypeBenefitExclusive() {
        EnrEntrantDocumentManager.instance().typeDao().doToggleProperty(getListenerParameterAsLong(), getEnrEnrollmentCampaign().getId(), EnrCampaignEntrantDocument.P_BENEFIT_EXCLUSIVE, true);
    }

    @Zlo("метод должен быть один, с параметром")
    public void onToggleEntrantDocumentTypeNotBenefitExclusive() {
        EnrEntrantDocumentManager.instance().typeDao().doToggleProperty(getListenerParameterAsLong(), getEnrEnrollmentCampaign().getId(), EnrCampaignEntrantDocument.P_BENEFIT_EXCLUSIVE, false);
    }

    @Zlo("метод должен быть один, с параметром")
    public void onToggleEntrantDocumentTypeBenefitMaxMark() {
        EnrEntrantDocumentManager.instance().typeDao().doToggleProperty(getListenerParameterAsLong(), getEnrEnrollmentCampaign().getId(), EnrCampaignEntrantDocument.P_BENEFIT_MAX_MARK, true);
    }

    @Zlo("метод должен быть один, с параметром")
    public void onToggleEntrantDocumentTypeNotBenefitMaxMark() {
        EnrEntrantDocumentManager.instance().typeDao().doToggleProperty(getListenerParameterAsLong(), getEnrEnrollmentCampaign().getId(), EnrCampaignEntrantDocument.P_BENEFIT_MAX_MARK, false);
    }

    @Zlo("метод должен быть один, с параметром")
    public void onToggleEntrantDocumentTypeBenefitNoExams() {
        EnrEntrantDocumentManager.instance().typeDao().doToggleProperty(getListenerParameterAsLong(), getEnrEnrollmentCampaign().getId(), EnrCampaignEntrantDocument.P_BENEFIT_NO_EXAMS, true);
    }

    @Zlo("метод должен быть один, с параметром")
    public void onToggleEntrantDocumentTypeNotBenefitNoExams() {
        EnrEntrantDocumentManager.instance().typeDao().doToggleProperty(getListenerParameterAsLong(), getEnrEnrollmentCampaign().getId(), EnrCampaignEntrantDocument.P_BENEFIT_NO_EXAMS, false);
    }

    @Zlo("метод должен быть один, с параметром")
    public void onToggleEntrantDocumentTypeBenefitPreference() {
        EnrEntrantDocumentManager.instance().typeDao().doToggleProperty(getListenerParameterAsLong(), getEnrEnrollmentCampaign().getId(), EnrCampaignEntrantDocument.P_BENEFIT_PREFERENCE, true);
    }

    @Zlo("метод должен быть один, с параметром")
    public void onToggleEntrantDocumentTypeNotBenefitPreference() {
        EnrEntrantDocumentManager.instance().typeDao().doToggleProperty(getListenerParameterAsLong(), getEnrEnrollmentCampaign().getId(), EnrCampaignEntrantDocument.P_BENEFIT_PREFERENCE, false);
    }

    @Zlo("метод должен быть один, с параметром")
    public void onToggleEntrantDocumentTypeCopyAccepted() {
        EnrEntrantDocumentManager.instance().typeDao().doToggleProperty(getListenerParameterAsLong(), getEnrEnrollmentCampaign().getId(), EnrCampaignEntrantDocument.P_COPY_CAN_BE_ACCEPTED, true);
    }

    @Zlo("метод должен быть один, с параметром")
    public void onToggleEntrantDocumentTypeCopyNotAccepted() {
        EnrEntrantDocumentManager.instance().typeDao().doToggleProperty(getListenerParameterAsLong(), getEnrEnrollmentCampaign().getId(), EnrCampaignEntrantDocument.P_COPY_CAN_BE_ACCEPTED, false);
    }

    @Zlo("метод должен быть один, с параметром")
    public void onToggleEntrantDocumentTypeOriginalAccepted() {
        EnrEntrantDocumentManager.instance().typeDao().doToggleProperty(getListenerParameterAsLong(), getEnrEnrollmentCampaign().getId(), EnrCampaignEntrantDocument.P_ORIGINAL_CAN_BE_ACCEPTED, true);
    }

    @Zlo("метод должен быть один, с параметром")
    public void onToggleEntrantDocumentTypeOriginalNotAccepted() {
        EnrEntrantDocumentManager.instance().typeDao().doToggleProperty(getListenerParameterAsLong(), getEnrEnrollmentCampaign().getId(), EnrCampaignEntrantDocument.P_ORIGINAL_CAN_BE_ACCEPTED, false);
    }

    @Zlo("метод должен быть один, с параметром")
    public void onToggleEntrantDocumentTypeConfirmAchievement() {
        EnrEntrantDocumentManager.instance().typeDao().doToggleProperty(getListenerParameterAsLong(), getEnrEnrollmentCampaign().getId(), EnrCampaignEntrantDocument.P_ACHIEVEMENT, true);
    }

    @Zlo("метод должен быть один, с параметром")
    public void onToggleEntrantDocumentTypeNotConfirmAchievement() {
        EnrEntrantDocumentManager.instance().typeDao().doToggleProperty(getListenerParameterAsLong(), getEnrEnrollmentCampaign().getId(), EnrCampaignEntrantDocument.P_ACHIEVEMENT, false);
    }

    @Zlo("метод должен быть один, с параметром")
    public void onToggleEntrantDocumentTypeSendFIS()
    {
        EnrEntrantDocumentManager.instance().typeDao().doToggleProperty(getListenerParameterAsLong(), getEnrEnrollmentCampaign().getId(), EnrCampaignEntrantDocument.P_SEND_F_I_S, true);
    }

    @Zlo("метод должен быть один, с параметром")
    public void onToggleEntrantDocumentTypeNotSendFIS()
    {
        EnrEntrantDocumentManager.instance().typeDao().doToggleProperty(getListenerParameterAsLong(), getEnrEnrollmentCampaign().getId(), EnrCampaignEntrantDocument.P_SEND_F_I_S, false);
    }

    public boolean isNothingSelected()
    {
        return getEnrEnrollmentCampaign() == null;
    }

    public void onEnrCampaignRefresh()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrEnrollmentCampaign());

        if (getEnrEnrollmentCampaign() != null)
            EnrEntrantDocumentManager.instance().typeDao().doSetEnrCampDefaultSettings(getEnrEnrollmentCampaign().getId());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, getEnrEnrollmentCampaign());
    }

    // Getters & Setters

    public EnrEnrollmentCampaign getEnrEnrollmentCampaign()
    {
        return getSettings().get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
    }
}
