/* $Id:$ */
package ru.tandemservice.unienr14.extreports.externalView;

import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLCaseExpressionBuilder;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.extview.SimpleDQLExternalViewConfig;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramOrientation;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramTrait;
import ru.tandemservice.uniedu.program.entity.EduProgram;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.uniedu.program.entity.EduProgramSecondaryProf;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetSecondary;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.cast;

/**
 * @author oleyba
 * @since 6/9/14
 */
public class EnrExtViewProvider4ProgramSetItem extends SimpleDQLExternalViewConfig
{
    @Override
    protected DQLSelectBuilder buildDqlQuery() {

        DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(EnrProgramSetItem.class, "ps_item")
            .joinPath(DQLJoinType.left, EnrProgramSetItem.program().fromAlias("ps_item"), "program")
            .joinPath(DQLJoinType.left, EduProgramHigherProf.programOrientation().fromAlias("program"), "programOrient")
            .joinPath(DQLJoinType.left, EduProgram.kind().fromAlias("program"), "program_kind")
            .joinPath(DQLJoinType.left, EduProgram.institutionOrgUnit().orgUnit().fromAlias("program"), "instOu")
            .joinPath(DQLJoinType.left, EduProgram.ownerOrgUnit().orgUnit().fromAlias("program"), "ownerOu")
            .joinPath(DQLJoinType.left, EduProgram.eduProgramTrait().fromAlias("program"), "program_trait")
            .union(
                new DQLSelectBuilder()
                    .fromEntity(EnrProgramSetSecondary.class, "ps_sec")
                    .joinPath(DQLJoinType.left, EnrProgramSetSecondary.program().fromAlias("ps_sec"), "program_sec")
                    .joinPath(DQLJoinType.left, EduProgram.kind().fromAlias("program_sec"), "program_kind_sec")
                    .joinPath(DQLJoinType.left, EduProgram.institutionOrgUnit().orgUnit().fromAlias("program_sec"), "instOu_sec")
                    .joinPath(DQLJoinType.left, EduProgram.ownerOrgUnit().orgUnit().fromAlias("program_sec"), "ownerOu_sec")
                    .joinPath(DQLJoinType.left, EduProgram.eduProgramTrait().fromAlias("program_sec"), "program_trait_sec")
                    
                    .column(property("ps_sec", EnrProgramSetSecondary.id()), "itemId")
                    .column(property("program_sec", EduProgram.id()), "programId")
                    .column(property("program_sec", EduProgram.title()), "programTitle")
                    .column(property("program_sec", EduProgram.shortTitle()), "shortTitle")
                    .column(property("program_kind_sec", EduProgramKind.title()), "programKind")
                    .column(property("program_kind_sec", EduProgramKind.code()), "programKindCode")
                    .column(property("program_kind_sec", EduProgramKind.shortTitle()), "programKindShortTitle")
                    .column(property("program_sec", EduProgram.year().title()), "year")
            
                    .column(property("instOu_sec", OrgUnit.id()), "instOuId")
                    .column(property("instOu_sec", OrgUnit.fullTitle()), "instOuFullTitle")
                    .column(property("instOu_sec", OrgUnit.shortTitle()), "instOuShortTitle")
                    .column(property("ownerOu_sec", OrgUnit.id()), "ownerOuId")
                    .column(property("ownerOu_sec", OrgUnit.fullTitle()), "ownerOuFullTitle")
                    .column(property("ownerOu_sec", OrgUnit.shortTitle()), "ownerOuShortTitle")
            
                    .column(property("program_sec", EduProgram.form().title()), "programForm")
                    .column(property("program_sec", EduProgram.form().code()), "programFormCode")
            
                    .column(property("program_trait_sec", EduProgramTrait.title()), "programTrait")
                    .column(property("program_trait_sec", EduProgramTrait.code()), "programTraitCode")
            
                    .column(property("program_sec", EduProgramProf.baseLevel().title()), "baseLevel")
                    .column(property("program_sec", EduProgramProf.baseLevel().code()), "baseLevelCode")

                    .column(property("program_sec", EduProgramProf.programQualification().title()), "programQualification")
                    .column(property("program_sec", EduProgramProf.programQualification().code()), "programQualificationCode")

                    .column(cast(value("-"), PropertyType.STRING), "programOrientation")
                    .column(cast(value("-"), PropertyType.STRING), "programOrientationCode")

                    .column(cast(value("-"), PropertyType.STRING), "programSpecialization")

                    .column(new DQLCaseExpressionBuilder(property("program_sec", EduProgramSecondaryProf.inDepthStudy()))
                        .when(value(Boolean.TRUE), value(1))
                        .when(value(Boolean.FALSE), value(0))
                        .otherwise(value(0))
                        .build(), "inDepthStudy")

                    .buildSelectRule()
            );

        column(dql, property("ps_item", EnrProgramSetItem.id()), "itemId").comment("uid записи");
        column(dql, property("program", EduProgram.id()), "programId").comment("uid образовательной программы");
        column(dql, property("program", EduProgram.title()), "programTitle").comment("название образовательной программы");
        column(dql, property("program", EduProgram.shortTitle()), "shortTitle").comment("сокращенное название образовательной программы");
        column(dql, property("program_kind", EduProgramKind.title()), "programKind").comment("вид обр. программы");
        column(dql, property("program_kind", EduProgramKind.code()), "programKindCode").comment("код вида обр. программы");
        column(dql, property("program_kind", EduProgramKind.shortTitle()), "programKindShortTitle").comment("сокр. название вида обр. программы");
        column(dql, property("program", EduProgram.year().title()), "year").comment("год начала обучение");

        column(dql, property("instOu", OrgUnit.id()), "instOuId").comment("uid лицензированного подразделения");
        column(dql, property("instOu", OrgUnit.fullTitle()), "instOuFullTitle").comment("полное название лицензированного подразделения");
        column(dql, property("instOu", OrgUnit.shortTitle()), "instOuShortTitle").comment("сокращенное название лицензированного подразделения");
        column(dql, property("ownerOu", OrgUnit.id()), "ownerOuId").comment("uid выпускающего подразделения");
        column(dql, property("ownerOu", OrgUnit.fullTitle()), "ownerOuFullTitle").comment("полное название выпускающего подразделения");
        column(dql, property("ownerOu", OrgUnit.shortTitle()), "ownerOuShortTitle").comment("сокращенное название выпускающего подразделения");

        column(dql, property("program", EduProgram.form().title()), "programForm").comment("форма обучения");
        column(dql, property("program", EduProgram.form().code()), "programFormCode").comment("код формы обучения");

        column(dql, property("program_trait", EduProgramTrait.title()), "programTrait").comment("особенность реализации");
        column(dql, property("program_trait", EduProgramTrait.code()), "programTraitCode").comment("код особенности реализации");

        column(dql, property("program", EduProgramProf.baseLevel().title()), "baseLevel").comment("необходимый для обучения уровень образования");
        column(dql, property("program", EduProgramProf.baseLevel().code()), "baseLevelCode").comment("код необходимого уровня образования");

        column(dql, property("program", EduProgramProf.programQualification().title()), "programQualification").comment("квалификация");
        column(dql, property("program", EduProgramProf.programQualification().code()), "programQualificationCode").comment("код квалификации");

        column(dql, property("programOrient", EduProgramOrientation.title()), "programOrientation").comment("ориентация");
        column(dql, property("programOrient", EduProgramOrientation.code()), "programOrientationCode").comment("код ориентации");

        column(dql, property("program", EduProgramHigherProf.programSpecialization().title()), "programSpecialization").comment("направленность (профиль, специализация)");

        column(dql, 0, PropertyType.INTEGER, "inDepthStudy").comment("углубленное изучение (только для программ СПО)");

        return dql;
    }
}

