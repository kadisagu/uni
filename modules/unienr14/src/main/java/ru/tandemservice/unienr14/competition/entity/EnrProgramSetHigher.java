package ru.tandemservice.unienr14.competition.entity;

import com.google.common.collect.ImmutableSet;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrMethodDivCompetitionsCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrRequestTypeCodes;
import ru.tandemservice.unienr14.competition.entity.gen.EnrProgramSetHigherGen;

import java.util.Set;

/**
 * Набор ОП для приема (кадры высшей квалификации)
 *
 * Определяет перечень ОП, на который формируются конкурсы, для приема в аспирантуру.
 */
public class EnrProgramSetHigher extends EnrProgramSetHigherGen implements EnrProgramSetBase.ISingleExamSetOwner
{
    public static final Set<String> AVAILABLE_DIV_METHOD_CODE = ImmutableSet.of(
            EnrMethodDivCompetitionsCodes.HIGHER_NO_DIV,
            EnrMethodDivCompetitionsCodes.POSTGRADUATE_NO_DIV,
            EnrMethodDivCompetitionsCodes.TRAINEESHIP_NO_DIV,
            EnrMethodDivCompetitionsCodes.INTERNSHIP_NO_DIV
    );

    public EnrProgramSetHigher()
    {
    }

    public static String getRequiredDivMethodCode(String requestTypeCode)
    {
        switch (requestTypeCode)
        {
            case EnrRequestTypeCodes.HIGHER : return EnrMethodDivCompetitionsCodes.HIGHER_NO_DIV;
            case EnrRequestTypeCodes.POSTGRADUATE : return EnrMethodDivCompetitionsCodes.POSTGRADUATE_NO_DIV;
            case EnrRequestTypeCodes.TRAINEESHIP : return EnrMethodDivCompetitionsCodes.TRAINEESHIP_NO_DIV;
            case EnrRequestTypeCodes.INTERNSHIP : return EnrMethodDivCompetitionsCodes.INTERNSHIP_NO_DIV;
            default: throw new IllegalArgumentException();
        }
    }

    @Override
    public boolean isContractDividedByEduLevel()
    {
        return false;
    }

    @Override
    public boolean isValidExamSetVariants()
    {
        return true;
    }

    @Override
    public boolean isMinisterialDividedByEduLevel()
    {
        return false;
    }
}