package ru.tandemservice.unienr14.settings.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniedu.orgunit.entity.EduInstitutionOrgUnit;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Филиал в рамках ПК
 *
 * Подразделение, аккредитованное осуществлять прием.
 * Существует в рамках приемной кампании учебного года, т.е. создается, если есть аккредитация на прием в этом учебном году.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrOrgUnitGen extends EntityBase
 implements INaturalIdentifiable<EnrOrgUnitGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.settings.entity.EnrOrgUnit";
    public static final String ENTITY_NAME = "enrOrgUnit";
    public static final int VERSION_HASH = 665803336;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String L_INSTITUTION_ORG_UNIT = "institutionOrgUnit";
    public static final String P_FIS_DATA_SENDING_INDEPENDENT = "fisDataSendingIndependent";
    public static final String P_LOGIN = "login";
    public static final String P_PASSWORD = "password";
    public static final String P_FIS_UID = "fisUid";
    public static final String P_DEPARTMENT_TITLE = "departmentTitle";
    public static final String P_TITLE = "title";

    private EnrEnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private EduInstitutionOrgUnit _institutionOrgUnit;     // Филиал
    private boolean _fisDataSendingIndependent;     // Передает данные в ФИС самостоятельно
    private String _login;     // Логин
    private String _password;     // Пароль
    private Integer _fisUid;     // Идентификатор в ФИС

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Филиал. Свойство не может быть null.
     */
    @NotNull
    public EduInstitutionOrgUnit getInstitutionOrgUnit()
    {
        return _institutionOrgUnit;
    }

    /**
     * @param institutionOrgUnit Филиал. Свойство не может быть null.
     */
    public void setInstitutionOrgUnit(EduInstitutionOrgUnit institutionOrgUnit)
    {
        dirty(_institutionOrgUnit, institutionOrgUnit);
        _institutionOrgUnit = institutionOrgUnit;
    }

    /**
     * Для связи с ФИС.
     *
     * @return Передает данные в ФИС самостоятельно. Свойство не может быть null.
     */
    @NotNull
    public boolean isFisDataSendingIndependent()
    {
        return _fisDataSendingIndependent;
    }

    /**
     * @param fisDataSendingIndependent Передает данные в ФИС самостоятельно. Свойство не может быть null.
     */
    public void setFisDataSendingIndependent(boolean fisDataSendingIndependent)
    {
        dirty(_fisDataSendingIndependent, fisDataSendingIndependent);
        _fisDataSendingIndependent = fisDataSendingIndependent;
    }

    /**
     * Для связи с ФИС.
     *
     * @return Логин.
     */
    @Length(max=255)
    public String getLogin()
    {
        return _login;
    }

    /**
     * @param login Логин.
     */
    public void setLogin(String login)
    {
        dirty(_login, login);
        _login = login;
    }

    /**
     * Для связи с ФИС.
     *
     * @return Пароль.
     */
    @Length(max=255)
    public String getPassword()
    {
        return _password;
    }

    /**
     * @param password Пароль.
     */
    public void setPassword(String password)
    {
        dirty(_password, password);
        _password = password;
    }

    /**
     * @return Идентификатор в ФИС.
     */
    public Integer getFisUid()
    {
        return _fisUid;
    }

    /**
     * @param fisUid Идентификатор в ФИС.
     */
    public void setFisUid(Integer fisUid)
    {
        dirty(_fisUid, fisUid);
        _fisUid = fisUid;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrOrgUnitGen)
        {
            if (withNaturalIdProperties)
            {
                setEnrollmentCampaign(((EnrOrgUnit)another).getEnrollmentCampaign());
                setInstitutionOrgUnit(((EnrOrgUnit)another).getInstitutionOrgUnit());
            }
            setFisDataSendingIndependent(((EnrOrgUnit)another).isFisDataSendingIndependent());
            setLogin(((EnrOrgUnit)another).getLogin());
            setPassword(((EnrOrgUnit)another).getPassword());
            setFisUid(((EnrOrgUnit)another).getFisUid());
        }
    }

    public INaturalId<EnrOrgUnitGen> getNaturalId()
    {
        return new NaturalId(getEnrollmentCampaign(), getInstitutionOrgUnit());
    }

    public static class NaturalId extends NaturalIdBase<EnrOrgUnitGen>
    {
        private static final String PROXY_NAME = "EnrOrgUnitNaturalProxy";

        private Long _enrollmentCampaign;
        private Long _institutionOrgUnit;

        public NaturalId()
        {}

        public NaturalId(EnrEnrollmentCampaign enrollmentCampaign, EduInstitutionOrgUnit institutionOrgUnit)
        {
            _enrollmentCampaign = ((IEntity) enrollmentCampaign).getId();
            _institutionOrgUnit = ((IEntity) institutionOrgUnit).getId();
        }

        public Long getEnrollmentCampaign()
        {
            return _enrollmentCampaign;
        }

        public void setEnrollmentCampaign(Long enrollmentCampaign)
        {
            _enrollmentCampaign = enrollmentCampaign;
        }

        public Long getInstitutionOrgUnit()
        {
            return _institutionOrgUnit;
        }

        public void setInstitutionOrgUnit(Long institutionOrgUnit)
        {
            _institutionOrgUnit = institutionOrgUnit;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrOrgUnitGen.NaturalId) ) return false;

            EnrOrgUnitGen.NaturalId that = (NaturalId) o;

            if( !equals(getEnrollmentCampaign(), that.getEnrollmentCampaign()) ) return false;
            if( !equals(getInstitutionOrgUnit(), that.getInstitutionOrgUnit()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEnrollmentCampaign());
            result = hashCode(result, getInstitutionOrgUnit());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEnrollmentCampaign());
            sb.append("/");
            sb.append(getInstitutionOrgUnit());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrOrgUnitGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrOrgUnit.class;
        }

        public T newInstance()
        {
            return (T) new EnrOrgUnit();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "institutionOrgUnit":
                    return obj.getInstitutionOrgUnit();
                case "fisDataSendingIndependent":
                    return obj.isFisDataSendingIndependent();
                case "login":
                    return obj.getLogin();
                case "password":
                    return obj.getPassword();
                case "fisUid":
                    return obj.getFisUid();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrEnrollmentCampaign) value);
                    return;
                case "institutionOrgUnit":
                    obj.setInstitutionOrgUnit((EduInstitutionOrgUnit) value);
                    return;
                case "fisDataSendingIndependent":
                    obj.setFisDataSendingIndependent((Boolean) value);
                    return;
                case "login":
                    obj.setLogin((String) value);
                    return;
                case "password":
                    obj.setPassword((String) value);
                    return;
                case "fisUid":
                    obj.setFisUid((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "institutionOrgUnit":
                        return true;
                case "fisDataSendingIndependent":
                        return true;
                case "login":
                        return true;
                case "password":
                        return true;
                case "fisUid":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "institutionOrgUnit":
                    return true;
                case "fisDataSendingIndependent":
                    return true;
                case "login":
                    return true;
                case "password":
                    return true;
                case "fisUid":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrollmentCampaign":
                    return EnrEnrollmentCampaign.class;
                case "institutionOrgUnit":
                    return EduInstitutionOrgUnit.class;
                case "fisDataSendingIndependent":
                    return Boolean.class;
                case "login":
                    return String.class;
                case "password":
                    return String.class;
                case "fisUid":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrOrgUnit> _dslPath = new Path<EnrOrgUnit>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrOrgUnit");
    }
            

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrOrgUnit#getEnrollmentCampaign()
     */
    public static EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Филиал. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrOrgUnit#getInstitutionOrgUnit()
     */
    public static EduInstitutionOrgUnit.Path<EduInstitutionOrgUnit> institutionOrgUnit()
    {
        return _dslPath.institutionOrgUnit();
    }

    /**
     * Для связи с ФИС.
     *
     * @return Передает данные в ФИС самостоятельно. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrOrgUnit#isFisDataSendingIndependent()
     */
    public static PropertyPath<Boolean> fisDataSendingIndependent()
    {
        return _dslPath.fisDataSendingIndependent();
    }

    /**
     * Для связи с ФИС.
     *
     * @return Логин.
     * @see ru.tandemservice.unienr14.settings.entity.EnrOrgUnit#getLogin()
     */
    public static PropertyPath<String> login()
    {
        return _dslPath.login();
    }

    /**
     * Для связи с ФИС.
     *
     * @return Пароль.
     * @see ru.tandemservice.unienr14.settings.entity.EnrOrgUnit#getPassword()
     */
    public static PropertyPath<String> password()
    {
        return _dslPath.password();
    }

    /**
     * @return Идентификатор в ФИС.
     * @see ru.tandemservice.unienr14.settings.entity.EnrOrgUnit#getFisUid()
     */
    public static PropertyPath<Integer> fisUid()
    {
        return _dslPath.fisUid();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.settings.entity.EnrOrgUnit#getDepartmentTitle()
     */
    public static SupportedPropertyPath<String> departmentTitle()
    {
        return _dslPath.departmentTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.settings.entity.EnrOrgUnit#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EnrOrgUnit> extends EntityPath<E>
    {
        private EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> _enrollmentCampaign;
        private EduInstitutionOrgUnit.Path<EduInstitutionOrgUnit> _institutionOrgUnit;
        private PropertyPath<Boolean> _fisDataSendingIndependent;
        private PropertyPath<String> _login;
        private PropertyPath<String> _password;
        private PropertyPath<Integer> _fisUid;
        private SupportedPropertyPath<String> _departmentTitle;
        private SupportedPropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrOrgUnit#getEnrollmentCampaign()
     */
        public EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Филиал. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrOrgUnit#getInstitutionOrgUnit()
     */
        public EduInstitutionOrgUnit.Path<EduInstitutionOrgUnit> institutionOrgUnit()
        {
            if(_institutionOrgUnit == null )
                _institutionOrgUnit = new EduInstitutionOrgUnit.Path<EduInstitutionOrgUnit>(L_INSTITUTION_ORG_UNIT, this);
            return _institutionOrgUnit;
        }

    /**
     * Для связи с ФИС.
     *
     * @return Передает данные в ФИС самостоятельно. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrOrgUnit#isFisDataSendingIndependent()
     */
        public PropertyPath<Boolean> fisDataSendingIndependent()
        {
            if(_fisDataSendingIndependent == null )
                _fisDataSendingIndependent = new PropertyPath<Boolean>(EnrOrgUnitGen.P_FIS_DATA_SENDING_INDEPENDENT, this);
            return _fisDataSendingIndependent;
        }

    /**
     * Для связи с ФИС.
     *
     * @return Логин.
     * @see ru.tandemservice.unienr14.settings.entity.EnrOrgUnit#getLogin()
     */
        public PropertyPath<String> login()
        {
            if(_login == null )
                _login = new PropertyPath<String>(EnrOrgUnitGen.P_LOGIN, this);
            return _login;
        }

    /**
     * Для связи с ФИС.
     *
     * @return Пароль.
     * @see ru.tandemservice.unienr14.settings.entity.EnrOrgUnit#getPassword()
     */
        public PropertyPath<String> password()
        {
            if(_password == null )
                _password = new PropertyPath<String>(EnrOrgUnitGen.P_PASSWORD, this);
            return _password;
        }

    /**
     * @return Идентификатор в ФИС.
     * @see ru.tandemservice.unienr14.settings.entity.EnrOrgUnit#getFisUid()
     */
        public PropertyPath<Integer> fisUid()
        {
            if(_fisUid == null )
                _fisUid = new PropertyPath<Integer>(EnrOrgUnitGen.P_FIS_UID, this);
            return _fisUid;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.settings.entity.EnrOrgUnit#getDepartmentTitle()
     */
        public SupportedPropertyPath<String> departmentTitle()
        {
            if(_departmentTitle == null )
                _departmentTitle = new SupportedPropertyPath<String>(EnrOrgUnitGen.P_DEPARTMENT_TITLE, this);
            return _departmentTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.settings.entity.EnrOrgUnit#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(EnrOrgUnitGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EnrOrgUnit.class;
        }

        public String getEntityName()
        {
            return "enrOrgUnit";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getDepartmentTitle();

    public abstract String getTitle();
}
