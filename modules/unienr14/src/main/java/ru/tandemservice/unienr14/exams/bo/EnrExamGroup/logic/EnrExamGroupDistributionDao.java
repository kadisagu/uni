package ru.tandemservice.unienr14.exams.bo.EnrExamGroup.logic;

import org.apache.commons.collections15.CollectionUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.DaemonThreadFactory;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.exams.bo.EnrExamGroup.EnrExamGroupManager;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroup;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroupScheduleEvent;
import ru.tandemservice.unienr14.exams.entity.EnrExamScheduleEvent;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unischedule.base.entity.event.ScheduleEvent;

import java.util.*;
import java.util.concurrent.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class EnrExamGroupDistributionDao extends UniBaseDao implements IEnrExamGroupDistributionDao {


    private final ExecutorService service = Executors.newFixedThreadPool(8, new DaemonThreadFactory("fixed-pool-enr14-examgroup"));

    @Override
    public Map<EnrExamPassDiscipline, EnrExamGroup> doDistributeEntrantExamPassDisciplines(final EnrEntrant entrant)
    {
        boolean entranceExamIncorrect = EnrEntrantManager.instance().dao().isEntranceExamIncorrect(entrant);
        if (entranceExamIncorrect) {
            throw new ApplicationException(EnrEntrantManager.instance().getProperty("ui.info.entranceExamIncorrect"));
        }

        boolean examListIncorrect = EnrEntrantManager.instance().dao().isExamListIncorrect(entrant);
        if (examListIncorrect) {
            throw new ApplicationException(EnrEntrantManager.instance().getProperty("ui.info.examListIncorrect"));
        }

        final Session session = lock(entrant.getId() + "-exam-pass-distribute");
        final EnrEnrollmentCampaign ec = entrant.getEnrollmentCampaign();

        // TODO ? // EnrExamGroupDao.lockEnrollmentCampaign(session, ec);

        // первая дата может быть только через день от текущей
        final Date now = CoreDateUtils.add(
            CoreDateUtils.getDayFirstTimeMoment(new Date()),
            Calendar.DATE,
            2
        );


        // находим перечень ЭГ+ДДС, которые подходят для включения ДДС абитуриента (каждой ЭГ будет соответствовать только одна ДДС)
        // в данном запросе будут только те ДДС, которые теоретически можно распределить
        final List<Object[]> pairs = new DQLSelectBuilder()
        .fromEntity(EnrExamGroup.class, "g").column(property("g.id"))
        .fromEntity(EnrExamPassDiscipline.class, "x").column(property("x.id"))

        .where(eq(property(EnrExamPassDiscipline.entrant().fromAlias("x")), value(entrant))) // ДДС должна быть от данного абитуриента
        .where(eq(property(EnrExamGroup.examGroupSet().enrollmentCampaign().fromAlias("g")), value(ec))) // ЭГ должна быть из той же приемки

        .where(isNull(property(EnrExamPassDiscipline.examGroup().fromAlias("x")))) // ДДС не должна быть в ЭГ
        .where(or(isNull(property("x", EnrExamPassDiscipline.markAsLong())), isNull(property("x", EnrExamPassDiscipline.absenceNote()))))
        .where(eq(property(EnrExamGroup.closed().fromAlias("g")), value(Boolean.FALSE))) // ЭГ должна быть не закрытой

        .where(eq(property(EnrExamGroup.territorialOrgUnit().fromAlias("g")), property(EnrExamPassDiscipline.territorialOrgUnit().fromAlias("x")))) // ДДС и ЭГ должны быть из одного ТП
        .where(eq(property(EnrExamGroup.discipline().fromAlias("g")), property(EnrExamPassDiscipline.discipline().fromAlias("x")))) // ДДС и ЭГ должны быть на одну и ту же дисциплину
        .where(eq(property(EnrExamGroup.passForm().fromAlias("g")), property(EnrExamPassDiscipline.passForm().fromAlias("x")))) // ДДС и ЭГ должны быть на одну и ту форму сдачи
        .createStatement(session).list();

        final Map<Long, Long> pairMap = new HashMap<>(pairs.size());
        for (final Object[] pair: pairs) {
            if (null != pairMap.put((Long)pair[0], (Long)pair[1])) {
                throw new IllegalStateException("wtf?"); // предполодение, что для каждой ЭГ будет только одна ДДС более не верно (кто-то что-то поменял)
            }
        }

        int delta = Math.max(0, ec.getSettings().getExamIntervalAuto());

        // получаем по перечню ЭГ набор событий (отсеиваем те, что не подходят)
        Map<EnrExamGroup, List<EnrExamGroupScheduleEvent>> eg2seMap;
        {
            // грузим ЭГ
            final List<EnrExamGroup> groups = getList(EnrExamGroup.class, EnrExamGroup.id(), pairMap.keySet());

            // для найденных ЭГ грузим их события в расписании
            final List<EnrExamGroupScheduleEvent> events = new DQLSelectBuilder()
            .fromEntity(EnrExamGroupScheduleEvent.class, "ege").column(property("ege"))
            .fetchPath(DQLJoinType.inner, EnrExamGroupScheduleEvent.examScheduleEvent().fromAlias("ege"), "ese")
            .fetchPath(DQLJoinType.inner, EnrExamScheduleEvent.scheduleEvent().fromAlias("ese"), "se")
            .where(in(property(EnrExamGroupScheduleEvent.examGroup().fromAlias("ege")), groups))
            .createStatement(session).list();

            eg2seMap = new HashMap<>(groups.size());
            for (final EnrExamGroupScheduleEvent event: events) {
                SafeMap.safeGet(eg2seMap, event.getExamGroup(), LinkedList.class /*обычно 1 значение*/).add(event);
            }

            DQLSelectBuilder distributedDisciplinesExamGroupBuilder = new DQLSelectBuilder()
                .fromEntity(EnrExamGroupScheduleEvent.class, "gse")
                .column(property("gse"))
                .where(exists(new DQLSelectBuilder()
                    .fromEntity(EnrExamPassDiscipline.class, "d")
                    .where(eq(property("d", EnrExamPassDiscipline.entrant()), value(entrant)))
                    .where(eq(property("d", EnrExamPassDiscipline.examGroup().id()), property("gse", EnrExamGroupScheduleEvent.examGroup().id())))
                    .buildQuery()));

            // уже занятые под ВИ дни (нормалтзованные)
            Collection<Integer> distrDates = buildDatesDistribution(this.<EnrExamGroupScheduleEvent>getList(distributedDisciplinesExamGroupBuilder));

            final Iterator<Map.Entry<EnrExamGroup, List<EnrExamGroupScheduleEvent>>> it=eg2seMap.entrySet().iterator();
            while (it.hasNext())
            {
                final Map.Entry<EnrExamGroup, List<EnrExamGroupScheduleEvent>> entry = it.next();

                boolean error = false;

                // проверка первая, что элементов ровно столько, сколько должно быть
                error = error || (entry.getKey().getDays() != entry.getValue().size());

                // проверка вторая, что все даты должны быть позже текущей и аудитория должна позволять включить абитуриента
                for (final EnrExamGroupScheduleEvent event: entry.getValue()) {
                    if (error) { continue; }

                    final ScheduleEvent schEvent = event.getExamScheduleEvent().getScheduleEvent();
                    if (schEvent.getDurationBegin().before(now)) { error = true; continue; }
                    if (schEvent.getDurationEnd().before(now)) { error = true; continue; }

                    final int currentCount = getCount(
                        new DQLSelectBuilder()
                        .fromEntity(EnrExamPassDiscipline.class, "epd")
                        .fromEntity(EnrExamGroupScheduleEvent.class, "ege")
                        .where(eq(property(EnrExamGroupScheduleEvent.examScheduleEvent().fromAlias("ege")), value(event.getExamScheduleEvent())))
                        .where(eq(property(EnrExamGroupScheduleEvent.examGroup().fromAlias("ege")), property(EnrExamPassDiscipline.examGroup().fromAlias("epd"))))
                        .column("epd.id").predicate(DQLPredicateType.distinct)
                    );

                    if (event.getExamScheduleEvent().getExamRoom().getSize() <= currentCount) {
                        error = true;
                    }

                    // проверяем, что дни не пересекаются
                    error |= overlaps(distrDates, buildDatesDistribution(entry.getValue()), delta);
                }

                if (error) {
                    // убираем ЭГ, она нас сразу не подходит
                    it.remove();
                    // break;
                }
            }
        }

        // ЭГ -> ДДС (здесь осталисть толко те ДДС, которые мы теоретически можем распределить)
        final Map<EnrExamGroup, Long> eg2epdMap = new HashMap<>(eg2seMap.size());
        for (final EnrExamGroup g: eg2seMap.keySet()) {
            if (null != eg2epdMap.put(g, pairMap.get(g.getId()))) {
                throw new IllegalStateException("wtf?"); // это вообще что-то немыслимое (я вроде бы как по ключам бегу)
            }
        }

        // из ЭГ делаем адовые обертки
        final List<Group> groups = new ArrayList<>();
        final Map<Long, Set<List<Integer>>> uniqGroupMap = new HashMap<>();
        final Map<EnrExamGroup, List<Integer>> eg2datesMap = buildEg2DatesMap(eg2seMap);
        for (final Map.Entry<EnrExamGroup, List<Integer>> e: eg2datesMap.entrySet()) {
            final ArrayList<Integer> dates = new ArrayList<>(e.getValue());
            Collections.sort(dates);
            final Group g = new Group(e.getKey(), eg2epdMap.get(e.getKey()), dates);

            if (SafeMap.safeGet(uniqGroupMap, g.epdId, HashSet.class).add(g.dates)) {
                // если есть две абсолютно одинаковые группы, то мы возьмем только одну (зачем две проверять)
                groups.add(g);
            }
        }

        // создаем распределятор
        final Distributor distributor = new Distributor(groups, new HashSet<>(eg2epdMap.values()).size(), delta);
        final Future<List<EnrExamGroup>> task = service.submit((Callable<List<EnrExamGroup>>) distributor::distribute);

        // отводим ему 1 минуту жизни, затем убиваем
        try {
            task.get(1, TimeUnit.MINUTES);
        } catch (final Exception t) {
            task.cancel(true);
        } finally {
            task.cancel(true);
        }

        // ждем
        try { Thread.sleep(50); }
        catch (Exception ignored) { }

        // находим распределение (получаем перечень выбранных ЭГ - по ним однозначно определяются ДДС)
        final List<EnrExamGroup> distribution = distributor.getSelected();

        // включаем ДДС в ЭГ
        final Map<EnrExamPassDiscipline, EnrExamGroup> result = new HashMap<>(distribution.size());
        for (final EnrExamGroup g: distribution) {
            final EnrExamPassDiscipline epd = (EnrExamPassDiscipline)session.get(EnrExamPassDiscipline.class, eg2epdMap.get(g));
            EnrExamGroupManager.instance().groupDao().setExamGroupAndUpdateTerritorial(epd, g.getTerritorialOrgUnit(), g, false);
            result.put(epd, g);
        }

        // вот он результат
        return result;
    }


    // преобразует события в расстояния в днях от текущего (так их проще потом сравнивать)
    private Map<EnrExamGroup, List<Integer>> buildEg2DatesMap(final Map<EnrExamGroup, List<EnrExamGroupScheduleEvent>> eg2seMap)
    {
        Calendar nowCal = Calendar.getInstance();
        nowCal.setTime(CoreDateUtils.getDayFirstTimeMoment(new Date()));

        final Map<EnrExamGroup, List<Integer>> result = new HashMap<>(eg2seMap.size());
        for (final Map.Entry<EnrExamGroup, List<EnrExamGroupScheduleEvent>> e: eg2seMap.entrySet())
            SafeMap.safeGet(result, e.getKey(), LinkedList.class /* как правило один элемент */).addAll(buildDatesDistribution(e.getValue()));

        return result;
    }


    @SuppressWarnings("serial")
    private static class CompleteException extends RuntimeException {

    }

    // хранит связанную информацию по группе, ссылке не ДДС и датах
    // используется, чтобы минимизировать обращение к map-ам
    private static class Group
    {
        private final EnrExamGroup group;
        private final Long epdId;
        private final List<Integer> dates;

        public Group(final EnrExamGroup group, final Long epdId, final List<Integer> dates) {
            this.group = group;
            this.epdId = epdId;
            this.dates = dates;
        }
    }

    private static class Distributor
    {

        /* информация о группах */
        private final List<Group> groups;

        /* число ДДС, которые надо распределить */
        private final int size;

        /* минимальне число дней между событиями */
        private final int deltaDays;

        public Distributor(final List<Group> groups, final int size, final int deltaDays) {
            this.groups = groups;
            this.size = size;
            this.deltaDays = deltaDays;
        }

        // текущее состояние (меняется)
        private Group[] current;

        // выбранное состояние (оно выбирается, когда мы получаем некоторое состояние, в которое больше не можем добавлять ЭГ)
        private Group[] selected;
        private List<Integer> selected_dates;

        public List<EnrExamGroup> getSelected() {
            final Group[] selected = this.selected;
            if (null == selected || 0 == selected.length) { return Collections.emptyList(); }

            synchronized (selected) {
                final List<EnrExamGroup> result = new ArrayList<>(selected.length);
                for (final Group g: selected) {
                    result.add(g.group);
                }
                return result;
            }
        }

        public List<EnrExamGroup> distribute()
        {
            if (this.size <= 0) { return Collections.emptyList(); }
            // if (this.size > 10) { throw new ApplicationException(""); }

            this.current = new Group[size];
            this.selected = new Group[] {};
            this.selected_dates = new ArrayList<>(0);

            final ArrayList<Group> possibleGroups = new ArrayList<>(this.groups);
            Collections.sort(possibleGroups, (o1, o2) -> {
                // TODO: придумать более осмысленную сортировку
                int i;

                // по количество дат сдачи
                if (0 != (i = Integer.compare(o1.group.getDays(), o2.group.getDays()))) { return i; }

                // проверяем даты подряд
                final Iterator<Integer> i1 = o1.dates.iterator();
                final Iterator<Integer> i2 = o2.dates.iterator();
                while (i1.hasNext() && i2.hasNext()) {
                    if (0 != (i = Integer.compare(i1.next(), i2.next()))) { return i; }
                }
                if (0 != (i = Boolean.compare(i1.hasNext(), i2.hasNext()))) { return i; }

                // затем по ДДС
                if (0 != (i = Long.compare(o1.epdId, o2.epdId))) { return i; }

                return Long.compare(o1.group.getId(), o2.group.getId());
            });

            try {
                distribute(0, possibleGroups);
            } catch (final CompleteException e) {
                // do nothing (у нас есть результат, его и выводим)
            }

            return getSelected();
        }


        private void select(final int level)
        {
            if (level <= 0) {
                // должен быть хотябы один элемент
                return;
            }

            Group[] selected = this.selected;
            if (level < selected.length) {
                // сразу нет (мы уже обладаем вариантом, в который включено больше ЭГ)
                return;
            }

            if (level > selected.length) {
                // сразу да (этот вариант включает больше ЭГ)
                // здесь не нужно делать синхронизацию, т.к. значение массива будет перетерто полностью
                selected = new Group[level];
                System.arraycopy(this.current, 0, selected, 0, level);
                this.selected = selected;
                this.selected_dates = dates(selected, level);

                return;
            }

            // надо сравнивать :)

            final List<Integer> current_dates = dates(this.current, level);
            final int c = compare_dates(current_dates, selected_dates);
            if (c > 0) {
                // результат не впечатлил
                return;
            }

            if (c == 0) {
                // здесь надо думать, как еще сравнивать ЭГ
                return;
            }

            // копируем в оптимальное значение
            // здесь нужна блокировка, т.к. это изменение существующего значения
            synchronized (selected) {
                System.arraycopy(this.current, 0, selected, 0, level);
            }
            this.selected_dates = current_dates;
        }

        private int compare_dates(final List<Integer> current_dates, final List<Integer> selected_dates)
        {
            int c;

            // по первой дате (предпочитаем те, которые начинаются раньше)
            if (0 != (c = Integer.compare(current_dates.get(0), selected_dates.get(0)))) { return c; }

            // проверяем даты подряд
            final Iterator<Integer> ic = current_dates.iterator();
            final Iterator<Integer> is = selected_dates.iterator();
            while (ic.hasNext() && is.hasNext()) {
                if (0 != (c = Integer.compare(ic.next(), is.next()))) { return c; }
            }

            if (ic.hasNext()) { return 1; /* selected оптимальнее current */ }
            if (is.hasNext()) { return -1; /* current оптимальнее selected */ }

            // выбрать не получилось (что странно, но возможно, если есть две разных ЭГ с одинаковыми событиями)
            // может быть так, что несколько ЭГ будут проходить в одно и то же время (при этом могут быть и в одинаковых аудиториях)
            return 0;
        }

        private List<Integer> dates(final Group[] groups, final int length)
        {
            final List<Integer> l = new ArrayList<>(length+(length/4));
            for (int i=0;i<length;i++) {
                for (final Integer d: groups[i].dates) {
                    l.add(d); // как правило здесь, одна дата
                }
            }

            // сортируем даты
            Collections.sort(l);
            return new ArrayList<>(l);
        }

        private void distribute(final int level, final List<Group> possibleGroups)
        {
            if (possibleGroups.isEmpty()) {
                select(level);

                if (Thread.currentThread().isInterrupted()) {
                    throw new CompleteException();
                }
                return;
            }

            // пробегаемся по доступным ЭГ
            for (final Group g: possibleGroups)
            {
                current[level] = g;
                final List<Group> nextPossibleGroups = findPossibleGroups(possibleGroups, g);
                distribute(1+level, nextPossibleGroups);
            }

            // сбрасываем выбранную ЭГ
            current[level] = null;
        }

        // исключает из списка доступных ЭГ те, которые более не могут быть включены в распределение после включения в него ЭГ g
        private List<Group> findPossibleGroups(final List<Group> possibleGroups, final Group g)
        {
            // убираем все ЭГ, которые подходят для той же ДДС
            // убираем все ЭГ, расстояние между датами которых с датами текущей ЭГ меньше, чем минимальная длина в днях

            final Long epdId = g.epdId;
            final List<Integer> dates = g.dates;

            /* количество элементов неизвестно и будет убывать по экспоненте */
            final List<Group> tmp = new ArrayList<>(4+(possibleGroups.size()/2));
            for (final Group possibleGroup : possibleGroups) {

                // если ДДС совпадает, то исключаем (это отсечет большинство ЭГ до проверки на даты, которая сложная)
                if (epdId.equals(possibleGroup.epdId)) { continue; }

                // если даты не подходят, то исключаем (проверка сложная, поэтому ее нужно проводить после всех остальных)
                if (hasOverlaps(dates, possibleGroup.dates)) { continue; }

                // если все проверки прошли успешно, то группу оставляем
                tmp.add(possibleGroup);
            }

            return new ArrayList<>(tmp);
        }

        private boolean hasOverlaps(final List<Integer> dates1, final List<Integer> dates2)
        {
            return overlaps(dates1, dates2, deltaDays);
        }
    }

    /** Показывает, есть ли пересечения чисел из первой коллекции с числами из второй - расстояния между числами каждой пары меньше delta. */
    public static boolean overlaps(Collection<Integer> ints1, Collection<Integer> ints2, int delta)
    {
        return getOverlapsInts(ints1, ints2, delta) != null;
    }

    /** Возвращает пару чисел (по одному из каждой коллекции), которые "накладываются" - расстояние между ними меньше delta. */
    public static PairKey<Integer, Integer> getOverlapsInts(Collection<Integer> ints1, Collection<Integer> ints2, int delta)
    {
        for (int i1: ints1)
            for (int i2: ints2)
                if ((Math.abs(i1 - i2)-1) < delta)
                    return PairKey.create(i1, i2);

        return null;
    }

    /** Нормализует дни начала события расписания (от текущей даты), ВКЛЮЧАЯ ПРОШЕДШИЕ. */
    public static Collection<Integer> buildDatesDistribution(List<EnrExamGroupScheduleEvent> events)
    {
        Collection<Date> sourceDates = CollectionUtils.collect(events, enrExamGroupScheduleEvent -> enrExamGroupScheduleEvent.getExamScheduleEvent().getScheduleEvent().getDurationBegin());

        final Calendar now = Calendar.getInstance();
        now.setTime(CoreDateUtils.getDayFirstTimeMoment(new Date()));

        Set<Integer> dates = new HashSet<>();
        for (Date date: sourceDates)
        {
            Calendar cal = Calendar.getInstance();
            cal.setTime(CoreDateUtils.getDayFirstTimeMoment(date));

            int d = 0;
            if (cal.after(now))
                while (cal.after(now))
                {
                    d++;
                    cal.add(Calendar.DATE, -1);
                }
            else if (cal.before(now))
                while (cal.before(now))
                {
                    d--;
                    cal.add(Calendar.DATE, 1);
                }

            dates.add(d);
        }

        return dates;
    }
}
