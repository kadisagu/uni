/* $Id:$ */
package ru.tandemservice.unienr14.catalog.bo.EnrCatalogCommons;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.catalog.entity.*;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 4/29/13
 */
@Configuration
public class EnrCatalogCommonsManager extends BusinessObjectManager
{
    public static final String PARAM_BENEFIT_TYPE_CODE = "benefitTypeCode";

    public static EnrCatalogCommonsManager instance()
    {
        return instance(EnrCatalogCommonsManager.class);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> olympiadHonourDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrOlympiadHonour.class)
        .order(EnrOlympiadHonour.code())
        .filter(EnrOlympiadHonour.title());
    }


    @Bean
    public IDefaultComboDataSourceHandler benefitCategoryDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrBenefitCategory.class)
            .where(EnrBenefitCategory.benefitType().code(), PARAM_BENEFIT_TYPE_CODE)
            .order(EnrBenefitCategory.title())
            .filter(EnrBenefitCategory.title())
            .pageable(true);
    }

}
