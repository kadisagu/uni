/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrWSExport.ui.CampaignInfo;/**
 * @author rsizonenko
 * @since 27.06.2014
 */

import org.apache.commons.io.IOUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.ws.campaignInfo.EnrCampaignInfoEnvironmentNode;
import ru.tandemservice.unienr14.ws.campaignInfo.IEnrCampaignInfoServiceDao;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.ByteArrayOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class EnrWSExportCampaignInfoUI extends UIPresenter {

    private EnrEnrollmentCampaign enrEnrollmentCampaign;


    @Override
    public void onComponentRefresh() {
        setEnrEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
    }

    public void onClickSave() {
        EnrCampaignInfoEnvironmentNode node = IEnrCampaignInfoServiceDao.INSTANCE.get().getEnrollmentEnvironmentData(enrEnrollmentCampaign.getTitle());

        ByteArrayOutputStream output = new ByteArrayOutputStream();
        ZipOutputStream zip = new ZipOutputStream(output);

        try {
            ByteArrayOutputStream result = new ByteArrayOutputStream();
            Marshaller marshaller = JAXBContext.newInstance(EnrCampaignInfoEnvironmentNode.class).createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(node, result);

            zip.putNextEntry(new ZipEntry("data.xml"));
            IOUtils.write(result.toByteArray(), zip);
            zip.close();
        } catch (Throwable e)
        {
            throw CoreExceptionUtils.getRuntimeException(e);
        }

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().fileName("EnrollmentCampaignInfo.zip").document(output.toByteArray()).zip(), false);

        deactivate();
    }

    public EnrEnrollmentCampaign getEnrEnrollmentCampaign() {
        return enrEnrollmentCampaign;
    }

    public void setEnrEnrollmentCampaign(EnrEnrollmentCampaign enrEnrollmentCampaign) {
        this.enrEnrollmentCampaign = enrEnrollmentCampaign;
    }
    public boolean isNothingSelected() {
        return getEnrEnrollmentCampaign() == null;
    }
}