/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrOlympiadDiploma.ui.AddEdit;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.apache.tapestry.form.validator.Required;
import org.apache.tapestry.form.validator.Validator;
import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.fias.base.bo.Fias.FiasManager;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.person.base.bo.Person.PersonDefines;
import org.tandemframework.shared.person.base.bo.PersonDocument.PersonDocumentManager;
import org.tandemframework.shared.person.base.bo.PersonDocument.logic.PersonDocumentContext;
import org.tandemframework.shared.person.base.bo.PersonDocument.logic.PersonDocumentTypeSettingsConfig;
import org.tandemframework.shared.person.base.bo.PersonDocument.ui.Add.IPersonDocumentAddEditUI;
import org.tandemframework.shared.person.base.entity.PersonRole;
import org.tandemframework.shared.person.base.util.PersonDocumentUtil;
import org.tandemframework.tapsupport.validator.DigitValidator;
import ru.tandemservice.unienr14.catalog.entity.EnrOlympiad;
import ru.tandemservice.unienr14.catalog.entity.EnrOlympiadType;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrOlympiadTypeCodes;
import ru.tandemservice.unienr14.entrant.bo.EnrOlympiadDiploma.EnrOlympiadDiplomaManager;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.EnrEntrantRequestAddWizardWizardUI;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignEntrantDocument;
import ru.tandemservice.unienr14.settings.entity.gen.EnrCampaignEntrantDocumentGen;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static ru.tandemservice.unienr14.entrant.bo.EnrOlympiadDiploma.ui.AddEdit.EnrOlympiadDiplomaAddEdit.*;
/**
 * @author oleyba
 * @since 5/3/13
 */
@Input({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "diplomaId"),
        @Bind(key = PersonDocumentManager.BIND_PERSON_ROLE_ID, binding = "personRoleId", required = true),
        @Bind(key = PersonDocumentManager.BIND_DOCUMENT_TYPE_ID, binding = "documentTypeId")
})
public class EnrOlympiadDiplomaAddEditUI extends UIPresenter implements IPersonDocumentAddEditUI
{
    private Long _diplomaId;
    private EnrOlympiadDiploma _diploma;
    private IUploadFile _scanCopy;

    private Long _personRoleId;
    private Long _documentTypeId;

    private PersonRole _personRole;

    private PersonDocumentTypeSettingsConfig _documentConfig;

    private AddressCountry country;

    private EnrOlympiadType olympiadType;

    private List<IdentifiableWrapper> _teamTypeList;
    private IdentifiableWrapper _team;

    private boolean _isAchievementDocType = false;

    @Override
    public void onComponentRefresh()
    {

        setPersonRole(DataAccessServices.dao().get(getPersonRoleId()));

        if(isAddForm())
        {
            setDiploma(new EnrOlympiadDiploma());
            getDiploma().setDocumentType(DataAccessServices.dao().get(getDocumentTypeId()));
        }
        else
        {
            setDiploma(DataAccessServices.dao().get(getDiplomaId()));
        }

        PersonDocumentContext context = PersonDocumentManager.instance().documentContext().getItem(PersonDocumentUtil.key(getPersonRole().getEntityMeta().getEntityClass()));

        if(context == null)
        {
            throw new IllegalStateException("Unknown document context for personRole - " + getPersonRole().getEntityMeta().getEntityClass().getSimpleName());
        }

        PersonDocumentTypeSettingsConfig config = PersonDocumentManager.instance().dao().getConfig(context, getDiploma().getDocumentType().getCode());

        setDocumentConfig(PersonDocumentManager.instance().dao().getContextConfig(config, getPersonRole()));

        if(isEntrantDocument())
        {
            EnrCampaignEntrantDocument docSettings = DataAccessServices.dao().getByNaturalId(new EnrCampaignEntrantDocumentGen.NaturalId(getDiploma().getDocumentType(), getEntrant().getEnrollmentCampaign()));
            if (docSettings != null)
            {
                setAchievementDocType(docSettings.isAchievement());
            }
        }
        if (!isAddForm()) {
            setOlympiadType(getDiploma().getOlympiadType());
            if (getDiploma().getCombinedTeam() != null)
                setTeam(_uiConfig.getDataSource(DS_TEAM_TYPE).getRecordById(getDiploma().getCombinedTeam()? RUSSIAN_TEAM_ID : UKRAINE_TEAM_ID));
        }
        else {
            getDiploma().setRegistrationDate(new Date());
        }
        setCountry(FiasManager.instance().kladrDao().getCountry(IKladrDefines.RUSSIA_COUNTRY_CODE));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(EnrOlympiadDiplomaAddEdit.DS_OLYMPIAD.equals(dataSource.getName()))
        {
            if(isEntrantDocument())
            {
                dataSource.put(EnrOlympiad.BIND_EDU_YEAR, getEntrant().getEnrollmentCampaign().getEducationYear());
            }
            dataSource.put(EnrOlympiad.BIND_OLYMPIAD_TYPE, getOlympiadType());
        }
        if(EnrOlympiadDiplomaAddEdit.DS_SETTLEMENT.equals(dataSource.getName()))
        {
            dataSource.put(AddressItem.PARAM_COUNTRY, getCountry());
        }
    }

    public void onClickApply() {

    }

    public void onClickDeactivate()
    {
        deactivate(new ParametersMap().add(PersonDocumentManager.BIND_DO_CLOSE, Boolean.TRUE));
    }


    private ErrorCollector validate()
    {
        ErrorCollector collector = UserContext.getInstance().getErrorCollector();
        if (getDiploma().getTrainingClass() == null)
        {
            return collector;
        }
        if (EnrOlympiadTypeCodes.ALL_RUSSIA.equals(getDiploma().getOlympiadType().getCode()) && (getDiploma().getTrainingClass() < 9 || getDiploma().getTrainingClass() > 11))
            collector.add("Для олимпиады ВОШ «Класс обучения» может быть только «9», «10» или «11».", "class");
        else if (!EnrOlympiadTypeCodes.ALL_RUSSIA.equals(getDiploma().getOlympiadType().getCode()) && (getDiploma().getTrainingClass() < 7 || getDiploma().getTrainingClass() > 11))
            collector.add("«Класс обучения» может быть только «7», «8», «9», «10» или «11».", "class");
        return collector;
    }

    // presenter

    @Override
    public Long saveOrUpdateDocument()
    {
        ErrorCollector errorCollector = validate();
        if (errorCollector.hasErrors())
        {
            throw new ApplicationException();
        }

        return saveDocument();
    }

    public Long saveDocument() {

        if (!isShowTeam())
        {
            getDiploma().setCombinedTeam(null);
        }
        else
        {
            getDiploma().setCombinedTeam(getTeam() != null ? getTeam().getId().equals(RUSSIAN_TEAM_ID) : null);
        }

        getDiploma().setDocumentType(getDiploma().getDocumentType());

        EnrOlympiadDiplomaManager.instance().dao().saveOrUpdateDiploma(getDiploma(), getPersonRole(), _scanCopy);
        return getDiploma().getId();
    }

    public boolean isAddForm() {
        return getDiplomaId() == null;
    }

    public boolean isSeriaRequired() {
        return getOlympiadType() != null && EnrOlympiadTypeCodes.ALL_RUSSIA.equals(getOlympiadType().getCode());
    }

    public boolean isInWizard() {
        return EnrEntrantRequestAddWizardWizardUI.isInWizard(this);
    }

    public boolean isShowTeam()
    {
        return null != getOlympiadType() && EnrOlympiadTypeCodes.INTERNATIONAL_OLYMPIAD.equals(olympiadType.getCode());
    }

    public boolean isShowProfile()
    {
        return null != getOlympiadType() && EnrOlympiadTypeCodes.STATE_LIST.equals(getOlympiadType().getCode());
    }

    public Long getFileMaxSize()
    {
        String maxSizeProperty = ApplicationRuntime.getProperty(PersonDefines.PERSON_EDUCATION_DOCUMENT_UPLOAD_MAX_SIZE_PROPERTY);
        if (!StringUtils.isEmpty(maxSizeProperty))
            return Long.parseLong(maxSizeProperty);
        else
            return 512L * 1024;
    }

    public List<Validator> getClassValidators()
    {
        List<Validator> list = new ArrayList<>();
        list.add(new DigitValidator());
        if (getOlympiadType() != null && (EnrOlympiadTypeCodes.ALL_RUSSIA.equals(getOlympiadType().getCode()) || EnrOlympiadTypeCodes.STATE_LIST.equals(getOlympiadType().getCode())))
            list.add(new Required());
        return list;
    }

    public List<Validator> getCustomRequiredValidators()
    {
        List<Validator> list = new ArrayList<>();
        if (isAchievementDocType())
            list.add(new Required());
        return list;
    }

    public List<Validator> getNumberValidators()
    {
        List<Validator> list = new ArrayList<>();
        if (getOlympiadType() == null)
            return list;

        if (Lists.newArrayList(EnrOlympiadTypeCodes.ALL_RUSSIA, EnrOlympiadTypeCodes.UKRAINIAN_OLYMPIAD, EnrOlympiadTypeCodes.INTERNATIONAL_OLYMPIAD).contains(getOlympiadType().getCode()))
            list.add(new Required());
        return list;
    }


    public boolean isEntrantDocument()
    {
        return getEntrant() != null;
    }

    public EnrEntrant getEntrant()
    {
        if(getPersonRole() instanceof EnrEntrant)
        {
            return (EnrEntrant) getPersonRole();
        }
        return null;
    }
    // getters and setters

    public Long getDiplomaId()
    {
        return _diplomaId;
    }

    public void setDiplomaId(Long diplomaId)
    {
        _diplomaId = diplomaId;
    }

    public EnrOlympiadDiploma getDiploma()
    {
        return _diploma;
    }

    public void setDiploma(EnrOlympiadDiploma diploma)
    {
        _diploma = diploma;
    }

    public Long getPersonRoleId()
    {
        return _personRoleId;
    }

    public void setPersonRoleId(Long personRoleId)
    {
        _personRoleId = personRoleId;
    }

    public Long getDocumentTypeId()
    {
        return _documentTypeId;
    }

    public void setDocumentTypeId(Long documentTypeId)
    {
        _documentTypeId = documentTypeId;
    }

    public PersonRole getPersonRole()
    {
        return _personRole;
    }

    public void setPersonRole(PersonRole personRole)
    {
        _personRole = personRole;
    }

    public PersonDocumentTypeSettingsConfig getDocumentConfig()
    {
        return _documentConfig;
    }

    public void setDocumentConfig(PersonDocumentTypeSettingsConfig documentConfig)
    {
        _documentConfig = documentConfig;
    }

    public AddressCountry getCountry()
    {
        return country;
    }

    public void setCountry(AddressCountry country)
    {
        this.country = country;
    }

    public EnrOlympiadType getOlympiadType()
    {
        return olympiadType;
    }

    public void setOlympiadType(EnrOlympiadType olympiadType)
    {
        this.olympiadType = olympiadType;
    }

    public IUploadFile getScanCopy()
    {
        return _scanCopy;
    }

    public void setScanCopy(IUploadFile scanCopy)
    {
        _scanCopy = scanCopy;
    }

    public List<IdentifiableWrapper> getTeamTypeList()
    {
        return _teamTypeList;
    }

    public void setTeamTypeList(List<IdentifiableWrapper> teamTypeList)
    {
        _teamTypeList = teamTypeList;
    }

    public IdentifiableWrapper getTeam()
    {
        return _team;
    }

    public void setTeam(IdentifiableWrapper team)
    {
        _team = team;
    }

    public boolean isAchievementDocType()
    {
        return _isAchievementDocType;
    }

    public void setAchievementDocType(boolean achievementDocType)
    {
        _isAchievementDocType = achievementDocType;
    }

}
