/* $Id:$ */
package ru.tandemservice.unienr14.order.bo.EnrEnrollmentParagraph.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.IColumnListExtPointBuilder;
import org.tandemframework.caf.ui.config.datasource.column.ActionDSColumn;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.formatter.NoWrapFormatter;
import org.tandemframework.core.view.list.column.DefaultPublisherLinkResolver;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.formatter.EnrEntrantCustomStateCollectionFormatter;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.Pub.EnrEntrantPub;
import ru.tandemservice.unienr14.order.bo.EnrEnrollmentParagraph.logic.EnrEnrollmentExtractDSHandler;
import ru.tandemservice.unienr14.order.bo.EnrOrder.ui.Pub.EnrOrderPub;
import ru.tandemservice.unienr14.order.entity.EnrCancelExtract;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;

/**
 * @author oleyba
 * @since 7/8/14
 */
@Configuration
public class EnrEnrollmentParagraphPub extends BusinessComponentManager
{
    public static final String ENROLLMENT_EXTRACT_DS = "enrollmentExtractDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
        .addDataSource(searchListDS(ENROLLMENT_EXTRACT_DS, enrollmentExtractDS(), enrollmentExtractDSHandler()))
        .create();
    }



    /**
     * Создается точка расширения "Список выписок о зачислении в карточке параграфа", единая
     * для всех публикаторов параграфов о зачислении
     *
     * @return точка расширения "Список выписок о зачислении в карточке параграфа"
     */
    @Bean
    public ColumnListExtPoint enrollmentExtractDS()
    {
        IColumnListExtPointBuilder columnBuilder = ColumnListExtPoint.with(getName(), ENROLLMENT_EXTRACT_DS)
        .addColumn(publisherColumn("fio", EnrRequestedCompetition.request().entrant().person().fullFio())
            .entityListProperty(EnrEnrollmentExtract.entity().s())
//            .primaryKeyPath(EnrAbstractExtract.paragraph().order().id())
            .publisherLinkResolver(new DefaultPublisherLinkResolver() {
                @Override
                public String getComponentName(IEntity entity) { return EnrEntrantPub.class.getSimpleName(); }
                @Override
                public Object getParameters(IEntity entity) {
                    return new ParametersMap()
                    .add(PublisherActivator.PUBLISHER_ID_KEY, ((EnrRequestedCompetition) entity).getRequest().getEntrant().getId())
                    .add("selectedTab", "ratingTab");
                }
            })
            .create()
        )
        .addColumn(textColumn(EnrEnrollmentExtractDSHandler.CUSTOM_STATES, EnrEnrollmentExtractDSHandler.CUSTOM_STATES).formatter(new EnrEntrantCustomStateCollectionFormatter()))
        .addColumn(textColumn("sex", EnrEnrollmentExtract.entity().request().entrant().person().identityCard().sex().shortTitle()).create())
        .addColumn(textColumn("passport", EnrEnrollmentExtract.entity().request().entrant().person().identityCard().fullNumber()).formatter(NoWrapFormatter.INSTANCE).create())
        .addColumn(textColumn("conditions", EnrEnrollmentExtract.entity().parametersTitle()).formatter(NoWrapFormatter.INSTANCE).create())
        .addColumn(textColumn("eduInstitution", EnrEnrollmentExtract.entity().request().eduDocument().eduLevel().shortTitle()).create())
        .addColumn(textColumn("competition", EnrEnrollmentExtract.entity().competition().title()).create());

        if (Debug.isEnabled()) {
            columnBuilder.addColumn(textColumn("extractState", EnrEnrollmentExtract.state().title()));
        }

        return columnBuilder
        .addColumn(booleanColumn("cancelled", EnrEnrollmentExtract.cancelled()))
        .addColumn(publisherColumn("cancelExtract", "shortTitle")
            .entityListProperty(EnrEnrollmentExtractDSHandler.CANCEL_EXTRACT)
//            .primaryKeyPath(EnrAbstractExtract.paragraph().order().id())
            .publisherLinkResolver(new DefaultPublisherLinkResolver() {
                @Override public String getComponentName(IEntity entity) { return EnrOrderPub.class.getSimpleName(); }
                @Override public Object getParameters(IEntity entity) { return ((EnrCancelExtract) entity).getParagraph().getOrder().getId(); }
            })
            .create()
        )
        .addColumn(publisherColumn("otherExtracts", "title")
            .entityListProperty(EnrEnrollmentExtractDSHandler.OTHER_EXTRACTS)
            .formatter(CollectionFormatter.COLLECTION_FORMATTER))
        .addColumn(publisherColumn("student", EnrEnrollmentExtract.student().personalNumber())
            .primaryKeyPath(EnrEnrollmentExtract.student().id().s()))
        .addColumn(ActionDSColumn.with().name("setStudent").icon(CommonDefines.ICON_CHOOSE)
                .listener("onClickSetStudent").visible("ui:chooseStudentVisible")
                .permissionKey("ui:secModel.chooseStudent"))
        .addColumn(ActionDSColumn.with().name("exclude").icon(CommonDefines.ICON_DELETE)
            .listener("onClickDeleteExtract")
            .alert(FormattedMessage.with().template("enrollmentExtractDS.exclude.alert").parameter(EnrEnrollmentExtract.entity().request().entrant().person().identityCard().fullFio().s()).create())
            .permissionKey("ui:secModel.deleteExtract")
            .disabled(EnrEnrollmentExtractDSHandler.NO_EXCLUDE).create()
        )
        .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> enrollmentExtractDSHandler()
    {
        return new EnrEnrollmentExtractDSHandler(getName());
    }
}