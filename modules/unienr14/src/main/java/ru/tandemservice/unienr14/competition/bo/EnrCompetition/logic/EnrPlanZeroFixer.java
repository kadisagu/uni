/* $Id:$ */
package ru.tandemservice.unienr14.competition.bo.EnrCompetition.logic;

import org.tandemframework.core.util.cache.SafeMap;

import java.util.*;

/**
 * @author oleyba
 * @since 4/3/15
 */
public class EnrPlanZeroFixer
{
    public interface IPlanOwner {
        Object planBalanceKey();
        String getEduLevelReqCode();
        String getCompTypeCode();

        int getFixPlan();
        void setFixPlan(int plan);
    }

    @SuppressWarnings("unchecked")
    public static <T extends IPlanOwner> void fix(List<T> planOwners, EnrCompetitionPlanFormula.IPlanCalculationContext context, Comparator<T> comparator) {

        // после пересчета числа мест в конкурсах по указанному алгоритму необходимо
        // запустить распределение мест по конкурсам с нулевым числом мест:
        // 1. Необходимо взять конкурсы с указанными видами приема подразделения, ведущего прием,
        //    у которых число мест равно 0 и усп(конкурс)>0.
        // 2. Отсортировать в порядке возрастания приоритетов их базовых уровней.
        // 3. Для каждого такого конкурса необходимо найти конкурс
        //     того же подразделения
        //     с тем же видом приема
        //     с наибольшим числом мест,
        //     при этом его число мест должно быть больше либо равно 2.
        //    Если такой конкурс найден, то «забрать» у него 1 место в исходный конкурс
        //     (т.е. уменьшить его число мест на 1, а в число мест исходного конкурса сохранить 1).
        //    Если найдено несколько таких конкурсов, то необходимо «забрать место» из конкурса с наибольшим приоритетом базового уровня.

        // после пересчета числа мест по видам ЦП необходимо:
        // 1. Взять все объекты «число мест по виду ЦП» подразделения, ведущего прием, у которых
        //      число мест равно 0
        //      и число абитуриентов, успешно сдавших ВИ (см. выше), по связанному конкурсу со связанным видом ЦП больше 0.
        // 2. Отсортировать в порядке возрастания приоритетов их видов ЦП.
        // 3. Для каждого такого объекта необходимо найти объект «Число мест по виду ЦП»
        //     того же объекта «План приема по виду ЦП»
        //     с наибольшим числом мест,
        //     при этом его число мест должно быть больше либо равно 2.
        //    Если такой объект найден, то «забрать» у него 1 место в исходный объект
        //     (т.е. уменьшить его число мест на 1, а в число мест исходного объекта сохранить 1).
        //    Если найдено несколько таких объектов, то необходимо «забрать место» из объекта с наибольшим приоритетом базового уровня его конкурса.


        Map<Object, List<T>> sourceM = new HashMap<>();
        for (T p: planOwners) {
            if (p.getFixPlan() >=2) {
                SafeMap.safeGet(sourceM, p.planBalanceKey(), ArrayList.class).add(p);
            }
        }
        for (IPlanOwner p: planOwners) {
            if (p.getFixPlan() == 0 && context.s(p.getCompTypeCode(), p.getEduLevelReqCode()) > 0) {
                List<T> sourceL = sourceM.get(p.planBalanceKey());
                if (null == sourceL || sourceL.isEmpty()) continue;

                Collections.sort(sourceL, comparator);
                T source = sourceL.get(0);

                p.setFixPlan(1);
                source.setFixPlan(source.getFixPlan() - 1);
                if (source.getFixPlan() < 2) sourceL.remove(source);
            }
        }
    }
}
