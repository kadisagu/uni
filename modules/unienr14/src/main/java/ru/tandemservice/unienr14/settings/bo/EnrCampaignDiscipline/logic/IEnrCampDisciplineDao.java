package ru.tandemservice.unienr14.settings.bo.EnrCampaignDiscipline.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.catalog.entity.EnrDiscipline;
import ru.tandemservice.unienr14.catalog.entity.EnrStateExamSubject;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

/**
 * User: amakarova
 * Date: 16.04.13
 */
public interface IEnrCampDisciplineDao extends INeedPersistenceSupport {

    /**
     * Сохраняет/обновляет объект Дисциплина, используемая в рамках ПК
     * @param discipline
     * @param campaign
     * @param examSubject
     * @return id
     */
    Long saveOrUpdateCampDiscipline(EnrDiscipline discipline, EnrEnrollmentCampaign campaign, EnrStateExamSubject examSubject);

}
