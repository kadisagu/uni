/* $Id:$ */
package ru.tandemservice.unienr14.ws.order;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author oleyba
 * @since 7/20/14
 */
@XmlRootElement
public class EnrOrderEnvironmentNode
{
    /** Название приемной кампании */
    @XmlAttribute(required = true)
    public String enrollmentCampaignTitle;

    /** Название года обучения приемной кампании */
    @XmlAttribute(required = true)
    public String educationYearTitle;

    /** Справочник "Состояния приказов" */
    public OrderStateNode orderState = new OrderStateNode();

    /** Справочник "Вид возмещения затрат" */
    public CompensationTypeNode compensationType = new CompensationTypeNode();

    /** Справочник "Виды и уровни образования" */
    public EduLevelNode eduLevel = new EduLevelNode();

    /** Справочник "Формы обучения" */
    public EduProgramFormNode eduProgramForm = new EduProgramFormNode();

    /** Справочник "Виды образовательных программ" */
    public EduProgramKindNode eduProgramKind = new EduProgramKindNode();

    /** Справочник "Направления подготовки, профессии и специальности профессионального образования " */
    public EduProgramSubjectNode eduProgramSubject = new EduProgramSubjectNode();

    /** Справочник "Виды заявлений" */
    public RequestTypeNode requestType = new RequestTypeNode();

    /** Справочник "Виды приема" */
    public CompetitionTypeNode competitionType = new CompetitionTypeNode();

    /** Все типы всех подразделений, ведущих прием в указанной приемной кампании */
    public OrgUnitTypeNode orgUnitType = new OrgUnitTypeNode();

    /** Все подразделения, ведущие прием в указанной приемной кампании */
    public OrgUnitNode orgUnit = new OrgUnitNode();

    /**
     * Все приказы о зачислении в указанной приемной кампании
     */
    public EnrollmentOrderNode order = new EnrollmentOrderNode();

    /**
     * Простая строка таблицы
     */
    public static class Row implements Comparable<Row>
    {
        public Row()
        {
        }

        public Row(String title, String id)
        {
            this.title = title;
            this.id = id;
        }

        public Row(String shortTitle, String title, String id)
        {
            this.shortTitle = shortTitle;
            this.title = title;
            this.id = id;
        }

        @Override
        public int compareTo(Row o)
        {
            return o.id.compareTo(o.toString());
        }

        /**
         * Сокращенное название
         */
        @XmlAttribute
        public String shortTitle;

        /**
         * Название
         */
        @XmlAttribute(required = true)
        public String title;

        /**
         * Идентификатор строки
         */
        @XmlAttribute(required = true)
        public String id;
    }

    /** Справочник "Состояния приказов" */
    public static class OrderStateNode { public List<Row> row = new ArrayList<Row>(); }

    /** Справочник "Вид возмещения затрат" */
    public static class CompensationTypeNode { public List<Row> row = new ArrayList<Row>(); }

    /** Справочник "Типы приказов о зачислении абитуриентов" */
    public static class EnrollmentOrderTypeNode { public List<Row> row = new ArrayList<Row>(); }

    /** Справочник "Виды и уровни образования" */
    public static class EduLevelNode { public List<Row> row = new ArrayList<Row>(); }

    /** Справочник "Формы обучения" */
    public static class EduProgramFormNode { public List<Row> row = new ArrayList<Row>(); }

    /** Справочник "Виды образовательных программ" */
    public static class EduProgramKindNode { public List<Row> row = new ArrayList<Row>(); }

    /** Справочник "Направления подготовки, профессии и специальности профессионального образования " */
    public static class EduProgramSubjectNode { public List<Row> row = new ArrayList<Row>(); }

    /** Справочник "Виды заявлений" */
    public static class RequestTypeNode { public List<Row> row = new ArrayList<Row>(); }

    /** Справочник "Виды приема" */
    public static class CompetitionTypeNode { public List<Row> row = new ArrayList<Row>(); }

    /**
     * Все типы всех подразделений, ведущих прием, в указанной приемной кампании
     */
    public static class OrgUnitTypeNode
    {
        public List<Row> row = new ArrayList<Row>();
    }

    /**
     * Все подразделения, ведущие прием, в указанной приемной кампании
     */
    public static class OrgUnitNode
    {
        /**
         * Подразделение
         */
        public static class OrgUnitRow extends Row
        {
            public OrgUnitRow()
            {
            }

            public OrgUnitRow(String shortTitle, String title, String id, String orgUnitType, String nominativeCaseTitle, String settlementTitle, String territorialTitle, String territorialShortTitle)
            {
                super(shortTitle, title, id);
                this.orgUnitType = orgUnitType;
                this.nominativeCaseTitle = nominativeCaseTitle;
                this.settlementTitle = settlementTitle;
                this.territorialTitle = territorialTitle;
                this.territorialShortTitle = territorialShortTitle;
            }

            /**
             * Тип подразделения
             *
             * @see OrgUnitTypeNode
             */
            @XmlAttribute(required = true)
            public String orgUnitType;

            /**
             * Печатное название подразделения в именительном падеже
             */
            @XmlAttribute
            public String nominativeCaseTitle;

            /**
             * Название населенного пункта из фактического адреса подразделения
             */
            @XmlAttribute
            public String settlementTitle;

            /**
             * Название подразделения в контексте территориального подр.
             */
            @XmlAttribute(required = true)
            public String territorialTitle;

            /**
             * Сокращенное название подразделения в контексте территориального подр.
             */
            @XmlAttribute(required = true)
            public String territorialShortTitle;
        }

        public List<OrgUnitRow> row = new ArrayList<OrgUnitRow>();
    }

    /**
     * Все выписки о зачислении
     */
    public static class EnrollmentExtractNode
    {
        /**
         * Выписка о зачислении
         */
        public static class EnrollmentExtractRow
        {
            public EnrollmentExtractRow()
            {
            }

            public EnrollmentExtractRow(String middleName, String firstName, String lastName, String entrantId)
            {
                this.middleName = middleName;
                this.firstName = firstName;
                this.lastName = lastName;
                this.entrantId = entrantId;
            }

            /**
             * Отчество
             */
            @XmlAttribute(required = true)
            public String middleName;

            /**
             * Имя
             */
            @XmlAttribute(required = true)
            public String firstName;

            /**
             * Фамилия
             */
            @XmlAttribute(required = true)
            public String lastName;

            /**
             * Идентификатор абитуриента
             */
            @XmlAttribute(required = true)
            public String entrantId;

            /**
             * Выписка отменена
             */
            @XmlAttribute(required = true)
            public Boolean cancelled;

        }

        public List<EnrollmentExtractRow> row = new ArrayList<EnrollmentExtractRow>();
    }

    /**
     * Все параграфы о зачислении
     */
    public static class EnrollmentParagraphNode
    {
        /**
         * Параграф о зачислении
         */
        public static class EnrollmentParagraphRow
        {
            /**
             * Идентификатор студента предзачисления, который будет студентом-старостой группы после проведения приказа
             */
            @XmlAttribute
            public String groupManager;

            /**
             * Филиал
             *
             * @see OrgUnitNode
             */
            @XmlAttribute(required = true)
            public String enrOrgUnit;

            /**
             * Формирующее подразделение
             *
             * @see OrgUnitNode
             */
            @XmlAttribute(required = true)
            public String formativeOrgUnit;

            /**
             * Направление, профессия, специальность
             *
             * @see EduProgramSubjectNode
             */
            @XmlAttribute(required = true)
            public String programSubject;

            /**
             * Уровень образования
             *
             * @see EduLevelNode
             */
            @XmlAttribute(required = true)
            public String eduLevel;

            /**
             * Вид образовательной программы
             *
             * @see EduProgramKindNode
             */
            @XmlAttribute(required = true)
            public String programKind;

            /**
             * Технология освоения
             *
             * @see EduProgramFormNode
             */
            @XmlAttribute(required = true)
            public String programForm;

            /**
             * Вид приема
             *
             * @see CompetitionTypeNode
             */
            @XmlAttribute(required = true)
            public String competitionType;

            /**
             * Поступающие на параллельное обучение
             */
            @XmlAttribute(required = true)
            public Boolean parallel;

            /**
             * Все выписки о зачислении
             */
            public EnrollmentExtractNode extract = new EnrollmentExtractNode();
        }

        public List<EnrollmentParagraphRow> row = new ArrayList<EnrollmentParagraphRow>();
    }

    /**
     * Все приказы о зачислении
     */
    public static class EnrollmentOrderNode
    {
        /**
         * Приказ о зачислении
         */
        public static class EnrollmentOrderRow
        {
            /**
             * Вид возмещения затрат
             *
             * @see CompensationTypeNode
             */
            @XmlAttribute(required = true)
            public String compensationType;

            /**
             * Вид заявления
             *
             * @see RequestTypeNode
             */
            @XmlAttribute(required = true)
            public String requestType;

            /**
             * Состояние приказа
             *
             * @see OrderStateNode
             */
            @XmlAttribute(required = true)
            public String state;

            /**
             * Дата приказа
             */
            @XmlAttribute(required = true)
            public Date date;

            /**
             * Номер приказа
             */
            @XmlAttribute(required = true)
            public String number;

            /**
             * Печатная форма приказа
             */
            @XmlAttribute
            public byte[] text;

            /**
             * Идентификатор приказа
             */
            @XmlAttribute
            public long id;

            /**
             * Все параграфы о зачислении
             */
            public EnrollmentParagraphNode paragraph = new EnrollmentParagraphNode();
        }

        public List<EnrollmentOrderRow> row = new ArrayList<EnrollmentOrderRow>();
    }

}
