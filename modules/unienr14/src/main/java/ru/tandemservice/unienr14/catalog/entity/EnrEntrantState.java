package ru.tandemservice.unienr14.catalog.entity;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unienr14.catalog.entity.gen.*;
import ru.tandemservice.unienr14.entrant.daemon.EnrEntrantDaemonBean;

import java.util.Date;

/**
 * Состояние абитуриента
 */
public class EnrEntrantState extends EnrEntrantStateGen
{
    @EntityDSLSupport(parts = EnrEntrantState.P_TITLE)
    public String getStateDaemonSafe() {
        // if (null == EnrEntrantDaemonBean.DAEMON.getCompleteStatus()) return "обновляется";
        return getTitle();
    }
}