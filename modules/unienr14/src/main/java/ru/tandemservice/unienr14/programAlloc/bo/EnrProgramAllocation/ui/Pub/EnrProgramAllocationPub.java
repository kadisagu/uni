/* $Id: EcProfileDistributionPub.java 23087 2012-05-31 09:57:28Z vzhukov $ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.FormattedMessage;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.logic.EnrProgramAllocationItemDSHandler;
import ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.logic.EnrProgramDSHandler;
import ru.tandemservice.unienr14.programAlloc.entity.EnrProgramAllocationItem;

/**
 * @author Vasily Zhukov
 * @since 10.05.2012
 */
@Configuration
public class EnrProgramAllocationPub extends BusinessComponentManager
{
    public static final String TAB_PANEL = "tabPanel";
    public static final String TAB_PANEL_PUB = "tabPanelPub";
    public static final String TAB_PANEL_RECOMMENDATION = "tabPanelRecommendation";

    public static final String PROGRAM_DS = "programDS";
    public static final String ENTRANT_DS = "recommendedDS";

    public static final String PARAM_ALLOCATION_ID = "allocationId";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(PROGRAM_DS, programDS(), programDSHandler()))
                .addDataSource(searchListDS(ENTRANT_DS, recommendedDS(), itemDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint programDS()
    {
        return columnListExtPointBuilder(PROGRAM_DS)
                .addColumn(textColumn("title", EnrProgramSetItem.program().title().s()))
                .addColumn(blockColumn("quota", "quotaBlockColumn").width("80").create())
                .addColumn(blockColumn("used", "usedBlockColumn").width("80").create())
                .create();
    }

    @Bean
    public ColumnListExtPoint recommendedDS()
    {
        return columnListExtPointBuilder(ENTRANT_DS)
                .addColumn(textColumn("competitionType", EnrProgramAllocationItem.entrant().competition().type().title().s()).create())
                .addColumn(textColumn("targetAdmissionKind", EnrProgramAllocationItemDSHandler.VIEW_PROP_TA_KIND + ".title").create())
                .addColumn(textColumn("finalMark", EnrProgramAllocationItemDSHandler.VIEW_PROP_FINAL_MARK).create())
                .addColumn(textColumn("eduDocAverageMark", EnrProgramAllocationItem.entrant().request().eduDocument().avgMarkAsLong()).formatter(EnrEntrantManager.DEFAULT_MARK_FORMATTER).create())
                .addColumn(textColumn("fio", EnrProgramAllocationItem.entrant().request().entrant().fullFio().s()))
                .addColumn(textColumn("program", EnrProgramAllocationItem.programSetItem().program().title().s()))
                .addColumn(textColumn("priorities", EnrProgramAllocationItemDSHandler.VIEW_PROP_PRIORITIES).create())
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER)
                    .alert(FormattedMessage.with().template("recommendedDS.delete.alert").parameter(EnrProgramAllocationItem.entrant().request().entrant().fullFio().s()).create())
                    .permissionKey("enr14ProgramAllocationPubFill")
                    .create())
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> programDSHandler()
    {
        return new EnrProgramDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> itemDSHandler()
    {
        return new EnrProgramAllocationItemDSHandler(getName());
    }
}
