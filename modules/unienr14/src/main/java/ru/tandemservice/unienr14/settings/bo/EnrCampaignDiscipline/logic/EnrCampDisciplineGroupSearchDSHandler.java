/**
 *$Id: EnrCampDisciplineGroupSearchDSHandler.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.settings.bo.EnrCampaignDiscipline.logic;

import org.springframework.util.comparator.ComparableComparator;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDisciplineGroup;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDisciplineGroupElement;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 26.04.13
 */
public class EnrCampDisciplineGroupSearchDSHandler extends DefaultSearchDataSourceHandler
{
    public EnrCampDisciplineGroupSearchDSHandler(String ownerId)
    {
        super(ownerId, EnrCampaignDisciplineGroup.class);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final EnrEnrollmentCampaign enrCamp = context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);

        final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrCampaignDisciplineGroup.class, "g").column(property("g"))
                .where(eq(property(EnrCampaignDisciplineGroup.enrollmentCampaign().fromAlias("g")), value(enrCamp)));

        filter(builder, context, "g");

        final DSOutput output = DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(false).order().build();

        if (output.getRecordIds().size() < 5)
            output.setCountRecord(5);

        return wrap(input, output, context);


    }

    protected void filter(DQLSelectBuilder builder, ExecutionContext context, String alias)
    {

    }

    protected DSOutput wrap(DSInput input, DSOutput output, ExecutionContext context)
    {
        final Map<Long, List<String>> elementsMap = SafeMap.get(ArrayList.class);
        BatchUtils.execute(output.getRecordIds(), DQL.MAX_VALUES_ROW_NUMBER, elements -> {

            final List<Object[]> list = new DQLSelectBuilder().fromEntity(EnrCampaignDisciplineGroupElement.class, "e")
                    .column(property(EnrCampaignDisciplineGroupElement.group().id().fromAlias("e")))
                    .column(property(EnrCampaignDisciplineGroupElement.discipline().discipline().title().fromAlias("e")))
                    .where(in(property(EnrCampaignDisciplineGroupElement.group().id().fromAlias("e")), elements))
                    .createStatement(context.getSession()).list();

            for (Object[] o : list)
            {
                final Long groupId = (Long) o[0];
                final String elementTitle = (String) o[1];

                elementsMap.get(groupId)
                        .add(elementTitle);
            }
        });

        for (Map.Entry<Long, List<String>> entry : elementsMap.entrySet())
            Collections.sort(entry.getValue(), new ComparableComparator<>());

        for (DataWrapper wrapper : DataWrapper.<EnrCampaignDisciplineGroup>wrap(output))
        {
            final List<String> elementTitleList = elementsMap.get(wrapper.getId());
            wrapper.setProperty("disciplines", elementTitleList);
        }

        return output;
    }
}
