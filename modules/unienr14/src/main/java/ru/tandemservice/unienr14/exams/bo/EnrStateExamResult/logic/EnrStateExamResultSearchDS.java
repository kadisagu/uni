/**
 *$Id: EnrStateExamResultSearchDS.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.exams.bo.EnrStateExamResult.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unienr14.catalog.entity.EnrStateExamSubject;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.substring;

/**
 * @author Alexander Shaburov
 * @since 13.06.13
 */
public class EnrStateExamResultSearchDS extends DefaultSearchDataSourceHandler
{
    public static final String BIND_LAST_NAME = "lastName";
    public static final String BIND_IC_SERIA = "icSeria";
    public static final String BIND_IC_NUMBER = "icNumber";
    public static final String BIND_ADDED_DATE_FROM = "addedDateFrom";
    public static final String BIND_ADDED_DATE_TO = "addedDateTo";
    public static final String BIND_CERT_NUMB_TERRITORY_CODE = "certTerritoryCode";
    public static final String BIND_CERT_NUMB_NUMBER = "certNumber";
    public static final String BIND_CERT_NUMB_YEAR = "certYear";
    public static final String BIND_ACCEPTED = "accepted";
    public static final String BIND_VERIFIED_BY_USER = "verifiedByUser";
    public static final String BIND_STATE_FIS = "stateFis";
    public static final String BIND_SECOND_WAVE = "secondWave";
    public static final String BIND_STATE_EXAM_SUBJECT = "stateExamSubject";
    public static final String BIND_NOT_VERIFIED = "notVerified";

    public EnrStateExamResultSearchDS(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final EnrEnrollmentCampaign enrCampaign = context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
        if (enrCampaign == null)
            return ListOutputBuilder.get(input, new ArrayList(0)).pageable(true).build();

        final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EnrEntrantStateExamResult.class, "c").column(property("c"));
        filters(dql, "c", input, context);

        return DQLSelectOutputBuilder.get(input, dql, context.getSession()).order().pageable(true).build();
    }

    /**
     * @param dql builder from EnrStateExamSubjectMark
     */
    protected void filters(DQLSelectBuilder dql, String alias, DSInput input, ExecutionContext context)
    {
        final EnrEnrollmentCampaign enrCampaign = context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
        final String lastName = context.get(BIND_LAST_NAME);
        final String icSeria = context.get(BIND_IC_SERIA);
        final String icNumber = context.get(BIND_IC_NUMBER);
        final Date addedDateFrom = context.get(BIND_ADDED_DATE_FROM);
        final Date addedDateTo = context.get(BIND_ADDED_DATE_TO);
        final String certNumTerritoryCode = context.get(BIND_CERT_NUMB_TERRITORY_CODE);
        final String certNumNumber = context.get(BIND_CERT_NUMB_NUMBER);
        final String certNumYear = context.get(BIND_CERT_NUMB_YEAR);
        final Boolean accepted = context.get(BIND_ACCEPTED);
        final Boolean verifiedByUser = context.get(BIND_VERIFIED_BY_USER);
        final String stateFis = context.get(BIND_STATE_FIS);
        final Boolean secondWave = context.get(BIND_SECOND_WAVE);
        final Boolean notVerified = context.get(BIND_NOT_VERIFIED);
        final List<EnrStateExamSubject> subjectList = context.get(BIND_STATE_EXAM_SUBJECT);

        dql.where(eq(property(EnrEntrantStateExamResult.entrant().enrollmentCampaign().fromAlias(alias)), value(enrCampaign)));

        if (StringUtils.isNotEmpty(lastName))
            dql.where(likeUpper(property(EnrEntrantStateExamResult.entrant().person().identityCard().lastName().fromAlias(alias)), value(CoreStringUtils.escapeLike(lastName, true))));
        if (StringUtils.isNotEmpty(icSeria))
            dql.where(eq(property(EnrEntrantStateExamResult.entrant().person().identityCard().seria().fromAlias(alias)), value(icSeria)));
        if (StringUtils.isNotEmpty(icNumber))
            dql.where(eq(property(EnrEntrantStateExamResult.entrant().person().identityCard().number().fromAlias(alias)), value(icNumber)));
        if (addedDateFrom != null)
            dql.where(ge(property(EnrEntrantStateExamResult.registrationDate().fromAlias(alias)), value(CoreDateUtils.getDayFirstTimeMoment(addedDateFrom), PropertyType.TIMESTAMP)));
        if (addedDateTo != null)
        {
            dql.where(lt(property(EnrEntrantStateExamResult.registrationDate().fromAlias(alias)),
                         value(CoreDateUtils.getDayFirstTimeMoment(CoreDateUtils.add(addedDateTo, Calendar.DAY_OF_YEAR, 1)), PropertyType.TIMESTAMP)));
        }
        if (StringUtils.isNotEmpty(certNumTerritoryCode))
            dql.where(like(substring(property(EnrEntrantStateExamResult.documentNumber().fromAlias(alias)), 1, 2), value(certNumTerritoryCode)));
        if (StringUtils.isNotEmpty(certNumNumber))
            dql.where(like(substring(property(EnrEntrantStateExamResult.documentNumber().fromAlias(alias)), 4, 9), value(certNumNumber)));
        if (StringUtils.isNotEmpty(certNumYear))
            dql.where(like(substring(property(EnrEntrantStateExamResult.documentNumber().fromAlias(alias)), 14, 2), value(certNumYear)));
        if (accepted != null)
            dql.where(eq(property(EnrEntrantStateExamResult.accepted().fromAlias(alias)), value(accepted)));
        if (verifiedByUser != null) {
            dql.where(eq(property(EnrEntrantStateExamResult.verifiedByUser().fromAlias(alias)), value(verifiedByUser)));
        }
        if (StringUtils.isNotEmpty(stateFis))
            dql.where(likeUpper(property(EnrEntrantStateExamResult.stateCheckStatus().fromAlias(alias)), value(CoreStringUtils.escapeLike(stateFis, true))));
        if (secondWave != null)
            dql.where(eq(property(EnrEntrantStateExamResult.secondWave().fromAlias(alias)), value(secondWave)));
        if (subjectList != null && subjectList.size() > 0)
            dql.where(in(property(EnrEntrantStateExamResult.subject().fromAlias(alias)), subjectList));

        if (notVerified !=null && notVerified)
            dql.where(isNull(property(EnrEntrantStateExamResult.stateCheckStatus().fromAlias(alias))));
    }
}
