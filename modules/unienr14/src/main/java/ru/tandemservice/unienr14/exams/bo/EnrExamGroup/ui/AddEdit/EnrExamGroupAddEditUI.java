/**
 *$Id: EnrExamGroupAddEditUI.java 34410 2014-05-23 15:14:07Z nfedorovskih $
 */
package ru.tandemservice.unienr14.exams.bo.EnrExamGroup.ui.AddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.*;
import ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm;
import ru.tandemservice.unienr14.competition.entity.EnrExamVariant;
import ru.tandemservice.unienr14.competition.entity.EnrExamVariantPassForm;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.exams.bo.EnrExamGroup.EnrExamGroupManager;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroup;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.*;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 30.05.13
 */
@Input({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "entityHolder.id"),
        @Bind(key = EnrExamGroupAddEditUI.BIND_ENR_ORG_UNIT, binding = "terrOrgUnitId")
})
public class EnrExamGroupAddEditUI extends UIPresenter
{
    public static final String BIND_ENR_ORG_UNIT = "terrOrgUnitId";

    private EntityHolder<EnrExamGroup> _entityHolder = new EntityHolder<>(new EnrExamGroup());

    private Long _terrOrgUnitId;

    private EnrEnrollmentCampaign _enrCampaign;
    private DataWrapper _examGroupDiscipline;

    private ISingleSelectModel _disciplineModel;
    private Map<Long, DataWrapper> _disciplineModelWrapperMap;

    @Override
    public void onComponentRefresh()
    {
        _entityHolder.refresh();

        if (_entityHolder.getId() == null)
        {
            // терр. подр.
            if (_terrOrgUnitId != null) {
                OrgUnit orgUnit = DataAccessServices.dao().getNotNull(OrgUnit.class, _terrOrgUnitId);
                _entityHolder.getValue().setTerritorialOrgUnit(orgUnit);
            }

            // ПК
            _enrCampaign = EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign();

            // Набор ЭГ
            if (EnrExamGroupManager.instance().setDao().hasOpened(_enrCampaign.getId()))
            {
                _entityHolder.getValue().setExamGroupSet((EnrExamGroupSet) DataAccessServices.dao().getList(
                        new DQLSelectBuilder().fromEntity(EnrExamGroupSet.class, "b").column(property("b"))
                                .where(eq(property(EnrExamGroupSet.open().fromAlias("b")), value(true)))
                                .where(eq(property(EnrExamGroupSet.enrollmentCampaign().fromAlias("b")), value(_enrCampaign))))
                        .get(0));
            }
            else
            {
                final List<EnrExamGroupSet> examGroupSetList = DataAccessServices.dao().getList(EnrExamGroupSet.class, EnrExamGroupSet.enrollmentCampaign(), _enrCampaign, EnrExamGroupSet.id().s());
                _entityHolder.getValue().setExamGroupSet(examGroupSetList.isEmpty() ? null : examGroupSetList.get(examGroupSetList.size() - 1));
            }

            _entityHolder.getValue().setDays(1);
            _entityHolder.getValue().setSize(_entityHolder.getValue().getExamGroupSet() != null ? _entityHolder.getValue().getExamGroupSet().getSize() : 0);
        }
        else
        {
            _enrCampaign = _entityHolder.getValue().getExamGroupSet().getEnrollmentCampaign();
        }

        // сначала подготавливаем модель, что бы потом, если форма ред., то из значений модели предзаполнить селект
        prepareDisciplineSelectModel();
        if (_entityHolder.getId() != null)
            _examGroupDiscipline = getDisciplineWrapper();
    }

    public void onClickApply()
    {
        _entityHolder.getValue().setDiscipline((EnrCampaignDiscipline) _examGroupDiscipline.getProperty("campDisc"));
        _entityHolder.getValue().setPassForm((EnrExamPassForm) _examGroupDiscipline.getProperty("passForm"));

        EnrExamGroupManager.instance().groupDao().saveOrUpdate(_entityHolder.getValue());

        deactivate();
    }

    /**
     * Сначала подготавливаем мапу всех значений модели селекта,
     * далее при обращении к селекту значения будут подниматься из неё, а не из базы.
     */
    public void prepareDisciplineSelectModel()
    {
        {
            _disciplineModelWrapperMap = new HashMap<>();

            final DQLSelectBuilder examSetVariantOwnerDQL = new DQLSelectBuilder()
                .fromEntity(EnrProgramSetOrgUnit.class, "psou")
                .column(property(EnrProgramSetOrgUnit.programSet().id().fromAlias("psou")))
                .where(eq(property(EnrProgramSetOrgUnit.programSet().enrollmentCampaign().fromAlias("psou")), value(_enrCampaign)))
                .where(eq(property(EnrProgramSetOrgUnit.orgUnit().institutionOrgUnit().orgUnit().fromAlias("psou")), value(_entityHolder.getValue().getTerritorialOrgUnit())));

            final DQLSelectBuilder discResultDQL = new DQLSelectBuilder()
                .fromEntity(EnrExamVariant.class, "ge")
                .joinPath(DQLJoinType.inner, EnrExamVariant.examSetElement().value().fromAlias("ge"), "cdValue")
                .joinEntity("ge", DQLJoinType.inner, EnrCampaignDiscipline.class, "cd", eq(property(EnrCampaignDiscipline.id().fromAlias("cd")), property("cdValue.id")))
                .joinEntity("ge", DQLJoinType.inner, EnrExamVariantPassForm.class, "ef", eq(property(EnrExamVariantPassForm.examVariant().fromAlias("ef")), property("ge")))
                .where(in(property(EnrExamVariant.examSetVariant().owner().id().fromAlias("ge")), examSetVariantOwnerDQL.buildQuery()))
                .where(eq(property(EnrExamVariantPassForm.passForm().internal().fromAlias("ef")), value(true)));

            final DQLSelectBuilder discGroupResultDQL = new DQLSelectBuilder()
                .fromEntity(EnrExamVariant.class, "ge")
                .joinPath(DQLJoinType.inner, EnrExamVariant.examSetElement().value().fromAlias("ge"), "cdValue")
                .joinEntity("ge", DQLJoinType.inner, EnrCampaignDisciplineGroup.class, "dg", eq(property(EnrCampaignDisciplineGroup.id().fromAlias("dg")), property("cdValue.id")))
                .joinEntity("dg", DQLJoinType.inner, EnrCampaignDisciplineGroupElement.class, "dge", eq(property(EnrCampaignDisciplineGroupElement.group().fromAlias("dge")), property("dg")))
                .joinEntity("ge", DQLJoinType.inner, EnrExamVariantPassForm.class, "ef", eq(property(EnrExamVariantPassForm.examVariant().fromAlias("ef")), property("ge")))
                .where(in(property(EnrExamVariant.examSetVariant().owner().id().fromAlias("ge")), examSetVariantOwnerDQL.buildQuery()))
                .where(eq(property(EnrExamVariantPassForm.passForm().internal().fromAlias("ef")), value(true)));

            discResultDQL
                .column(property("cd"))
                .column(property(EnrExamVariantPassForm.passForm().fromAlias("ef")));

            discGroupResultDQL
                .column(property(EnrCampaignDisciplineGroupElement.discipline().fromAlias("dge")))
                .column(property(EnrExamVariantPassForm.passForm().fromAlias("ef")));

            final List<Object[]> resultDiscList = DataAccessServices.dao().getList(discResultDQL);
            final List<Object[]> resultDiscGroupList = DataAccessServices.dao().getList(discGroupResultDQL);

            long id = 1L;
            final Set<CoreCollectionUtils.Pair<Long, Long>> uniqPair = new LinkedHashSet<>();
            for (Object[] objects : resultDiscList)
            {
                final EnrCampaignDiscipline campDisc = (EnrCampaignDiscipline) objects[0];
                final EnrExamPassForm passForm = (EnrExamPassForm) objects[1];

                if (uniqPair.add(new CoreCollectionUtils.Pair<>(campDisc.getId(), passForm.getId())))
                {
                    final DataWrapper wrapper = new DataWrapper(id, campDisc.getTitle() + " (" + passForm.getTitle() + ")");
                    wrapper.setProperty("campDisc", campDisc);
                    wrapper.setProperty("passForm", passForm);
                    _disciplineModelWrapperMap.put(id++, wrapper);
                }
            }

            for (Object[] objects : resultDiscGroupList)
            {
                final EnrCampaignDiscipline campDisc = (EnrCampaignDiscipline) objects[0];
                final EnrExamPassForm passForm = (EnrExamPassForm) objects[1];

                if (uniqPair.add(new CoreCollectionUtils.Pair<>(campDisc.getId(), passForm.getId())))
                {
                    final DataWrapper wrapper = new DataWrapper(id, campDisc.getTitle() + " (" + passForm.getTitle() + ")");
                    wrapper.setProperty("campDisc", campDisc);
                    wrapper.setProperty("passForm", passForm);
                    _disciplineModelWrapperMap.put(id++, wrapper);
                }
            }
        }

        _disciplineModel = new CommonSingleSelectModel()
        {
            @Override
            @SuppressWarnings({"unchecked", "ConstantConditions"})
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                if (o != null)
                {
                    final Long key = (Long) o;
                    final DataWrapper wrapper = _disciplineModelWrapperMap.get(key);

                    return new SimpleListResultBuilder<>(Collections.singleton(wrapper));
                }

                final Collection<DataWrapper> values = new ArrayList<>(_disciplineModelWrapperMap.values());

                if (StringUtils.isNotEmpty(filter))
                {
                    for (Iterator<DataWrapper> iterator = values.iterator();iterator.hasNext();)
                    {
                        if (!StringUtils.containsIgnoreCase(iterator.next().getTitle(), filter))
                            iterator.remove();
                    }
                }

                return new SimpleListResultBuilder(values)
                {
                    @Override
                    public ListResult findOptions()
                    {
                        final List objects = super.findOptions().getObjects();
                        List list = objects.size() >= 50 ? objects.subList(0, 50) : objects;
                        int count = list==null ? 0 : list.size();
                        if( count==50)
                            count = objects.size();

                        Collections.sort(list, new Comparator<DataWrapper>()
                        {
                            @Override
                            public int compare(DataWrapper o1, DataWrapper o2)
                            {
                                return o1.getTitle().compareTo(o2.getTitle());
                            }
                        });

                        return new ListResult(list, count);
                    }
                };
            }
        };
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, _enrCampaign);
    }

    // Getters & Setters

    public String getSticker()
    {
        if (_entityHolder.getId() == null)
            return getConfig().getProperty("ui.sticker.add");
        else
            return getConfig().getProperty("ui.sticker.edit");
    }

    /** Дизаблить все поля, кроме "Число мест в ЭГ" – если ЭГ закрыта, или в ЭГ уже есть ДДС */
    public boolean isHasExamPassDiscipline()
    {
        return isEditForm() && ISharedBaseDao.instance.get().existsEntity(EnrExamPassDiscipline.class, EnrExamPassDiscipline.L_EXAM_GROUP, _entityHolder.getValue());

    }

    public boolean isEditForm()
    {
        return _entityHolder.getId() != null;
    }

    /** Из модели берем подходящий элемент, если его там нет, то добавляем */
    public DataWrapper getDisciplineWrapper()
    {
        for (DataWrapper wrapper : _disciplineModelWrapperMap.values())
        {
            if (wrapper.get("campDisc").equals(_entityHolder.getValue().getDiscipline())
                    &&
                    wrapper.get("passForm").equals(_entityHolder.getValue().getPassForm()))
                return wrapper;
        }

        final DataWrapper value = new DataWrapper(-1L, _entityHolder.getValue().getDiscipline().getTitle() + " (" + _entityHolder.getValue().getPassForm().getTitle() + ")");
        value.setProperty("campDisc", _entityHolder.getValue().getDiscipline());
        value.setProperty("passForm", _entityHolder.getValue().getPassForm());
        _disciplineModelWrapperMap.put(-1L, value);

        return value;
    }

    // Accessors

    public EntityHolder<EnrExamGroup> getEntityHolder()
    {
        return _entityHolder;
    }

    public void setEntityHolder(EntityHolder<EnrExamGroup> entityHolder)
    {
        _entityHolder = entityHolder;
    }

    public Long getTerrOrgUnitId()
    {
        return _terrOrgUnitId;
    }

    public void setTerrOrgUnitId(Long terrOrgUnitId)
    {
        _terrOrgUnitId = terrOrgUnitId;
    }

    public EnrEnrollmentCampaign getEnrCampaign()
    {
        return _enrCampaign;
    }

    public void setEnrCampaign(EnrEnrollmentCampaign enrCampaign)
    {
        _enrCampaign = enrCampaign;
    }

    public DataWrapper getExamGroupDiscipline()
    {
        return _examGroupDiscipline;
    }

    public void setExamGroupDiscipline(DataWrapper examGroupDiscipline)
    {
        _examGroupDiscipline = examGroupDiscipline;
    }

    public ISingleSelectModel getDisciplineModel()
    {
        return _disciplineModel;
    }

    public void setDisciplineModel(ISingleSelectModel disciplineModel)
    {
        _disciplineModel = disciplineModel;
    }
}
