/* $Id: idaoItemPub.vm 24257 2012-09-28 10:50:28Z vdanilov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unienr14.component.catalog.enrDiscipline.EnrDisciplineItemPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogItemPub.IDefaultCatalogItemPubDAO;
import ru.tandemservice.unienr14.catalog.entity.EnrDiscipline;

/**
 * @author AutoGenerator
 * Created on 10.04.2013
 */
public interface IDAO extends IDefaultCatalogItemPubDAO<EnrDiscipline, Model>
{
}
