package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x8x1_8to9 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrEnrollmentCampaignSettings

        // создано обязательное свойство acceptPeopleResidingInCrimea
        {
            // создать колонку
            tool.createColumn("enr14_campaign_settings_t", new DBColumn("acceptpeopleresidingincrimea_p", DBType.BOOLEAN));

            // задать значение по умолчанию
            java.lang.Boolean defaultAcceptPeopleResidingInCrimea = false;
            tool.executeUpdate("update enr14_campaign_settings_t set acceptpeopleresidingincrimea_p=? where acceptpeopleresidingincrimea_p is null", defaultAcceptPeopleResidingInCrimea);

            // сделать колонку NOT NULL
            tool.setColumnNullable("enr14_campaign_settings_t", "acceptpeopleresidingincrimea_p", false);
        }
    }
}