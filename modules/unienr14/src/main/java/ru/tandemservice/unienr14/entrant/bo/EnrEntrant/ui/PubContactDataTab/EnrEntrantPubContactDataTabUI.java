/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubContactDataTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInline;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInlineConfig;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInlineUI;
import org.tandemframework.shared.person.base.bo.Person.ui.ContactDataEdit.PersonContactDataEdit;
import org.tandemframework.shared.person.base.bo.Person.util.ISecureRoleContext;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.legacy.EnrPersonLegacyUtils;

/**
 * @author oleyba
 * @since 4/22/13
 */
@Input({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "entrant.id", required = true),
    @Bind(key = EnrEntrantPubContactDataTabUI.PARAM_INLINE, binding = "inline")
})
public class EnrEntrantPubContactDataTabUI extends UIPresenter
{
    public static final String PARAM_INLINE = "inline";

    private boolean inline;
    public boolean isInline() { return this.inline; }
    public void setInline(boolean inline) { this.inline = inline; }

    private EnrEntrant entrant = new EnrEntrant();
    private ISecureRoleContext secureRoleContext;

    @Override
    public void onComponentRefresh()
    {
        setEntrant(IUniBaseDao.instance.get().getNotNull(EnrEntrant.class, getEntrant().getId()));
        setSecureRoleContext(EnrPersonLegacyUtils.getSecureRoleContext(getEntrant()));
    }

    public void onClickAddressEdit() {

        AddressBaseEditInlineConfig addressConfig = new AddressBaseEditInlineConfig();
        addressConfig.setEntityId(getPerson().getId());
        addressConfig.setDetailedOnly(true);
        addressConfig.setDetailLevel(4);
        addressConfig.setAreaVisible(true);
        addressConfig.setInline(false);
        addressConfig.setAddressProperty(Person.L_ADDRESS);

        _uiActivation.asRegion(AddressBaseEditInline.class).top()
            .parameter(AddressBaseEditInlineUI.BIND_CONFIG, addressConfig)
            .activate();
    }

    public void onClickContactDataEdit() {
        _uiActivation.asRegionDialog(PersonContactDataEdit.class)
            .parameter(PublisherActivator.PUBLISHER_ID_KEY, getPerson().getId())
            .activate();
    }

    public Person getPerson() {
        return getEntrant().getPerson();
    }

    public EnrEntrant getEntrant()
    {
        return entrant;
    }

    public void setEntrant(EnrEntrant entrant)
    {
        this.entrant = entrant;
    }

    public ISecureRoleContext getSecureRoleContext()
    {
        return secureRoleContext;
    }

    public void setSecureRoleContext(ISecureRoleContext secureRoleContext)
    {
        this.secureRoleContext = secureRoleContext;
    }
}
