package ru.tandemservice.unienr14.competition.entity;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.unienr14.catalog.entity.EnrTargetAdmissionKind;
import ru.tandemservice.unienr14.competition.entity.gen.*;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignTargetAdmissionKind;

/**
 * План приема по виду ЦП
 */
public class EnrTargetAdmissionPlan extends EnrTargetAdmissionPlanGen
{
    public EnrTargetAdmissionPlan()
    {
    }

    public EnrTargetAdmissionPlan(EnrProgramSetOrgUnit psOu, EnrCampaignTargetAdmissionKind kind, int plan)
    {
        setProgramSetOrgUnit(psOu);
        setEnrCampaignTAKind(kind);
        setPlan(plan);
    }


    @EntityDSLSupport(parts = EnrTargetAdmissionPlan.L_ENR_CAMPAIGN_T_A_KIND + "." + EnrCampaignTargetAdmissionKind.L_TARGET_ADMISSION_KIND + EnrTargetAdmissionKind.P_TITLE)
    public String getPlanString() {
        return getEnrCampaignTAKind().getTitle() + " – " + getPlan();
    }
}