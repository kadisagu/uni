/* $Id$ */
package ru.tandemservice.unienr14.catalog.bo.EnrEduLevelRequirement;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

/**
 * @author Nikolay Fedorovskih
 * @since 11.03.2015
 */
@Configuration
public class EnrEduLevelRequirementManager extends BusinessObjectManager
{
}