/* $Id:$ */
package ru.tandemservice.unienr14.ws.order;

import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonCatalogUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentParagraph;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.OrderStates;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 7/20/14
 */
public class EnrOrderServiceDao extends UniBaseDao implements IEnrOrderServiceDao
{
    @Override
    public EnrOrderEnvironmentNode getOrderData(String enrollmentCampaignTitle, Date dateFrom, Date dateTo, String compensationTypeId, List<String> formativeOrgUnitIds, List<String> programFormIds, boolean withPrintData)
    {
        EnrEnrollmentCampaign campaign = get(EnrEnrollmentCampaign.class, EnrEnrollmentCampaign.P_TITLE, enrollmentCampaignTitle);

        if (campaign == null)
            throw new RuntimeException("EnrEnrollmentCampaign with title '" + enrollmentCampaignTitle + "' not found!");

        // выбираем приказы, пустых приказов быть не может, так как требуются только приказы в состоянии "согласовано" и "проведено",
        // а в таких состояниях всегда есть параграфы и выписки -> можно делать селект сразу из выписок и inner join к параграфам и приказам,
        // чтобы можно было накладывать фильтры по формирующим подразделениям и формам освоения по полям выписок и приказов

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EnrEnrollmentExtract.class, "e").column("e")
        .joinEntity("e", DQLJoinType.inner, EnrEnrollmentParagraph.class, "p", eq(property(EnrEnrollmentExtract.paragraph().id().fromAlias("e")), property(EnrEnrollmentParagraph.id().fromAlias("p"))))
        .joinEntity("p", DQLJoinType.inner, EnrOrder.class, "o", eq(property(EnrEnrollmentParagraph.order().id().fromAlias("p")), property(EnrOrder.id().fromAlias("o"))))
        .where(eq(property(EnrOrder.enrollmentCampaign().fromAlias("o")), value(campaign)))
        .where(DQLExpressions.in(property(EnrOrder.state().code().fromAlias("o")), Arrays.asList(UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTED, UnimoveDefines.CATALOG_ORDER_STATE_FINISHED)))
        .order(property(EnrOrder.commitDate().fromAlias("o")))
        .order(property(EnrOrder.id().fromAlias("o")))
        .order(property(EnrEnrollmentParagraph.number().fromAlias("p")))
        .order(property(EnrEnrollmentExtract.number().fromAlias("e")));

        if (dateFrom != null)
            dql.where(DQLExpressions.ge(property(EnrOrder.commitDate().fromAlias("o")), valueDate(CoreDateUtils.getDayFirstTimeMoment(dateFrom))));

        if (dateTo != null)
        {
            dateTo = CoreDateUtils.getDayFirstTimeMoment(CoreDateUtils.add(dateTo, Calendar.DAY_OF_YEAR, 1));
            dql.where(lt(property(EnrOrder.commitDate().fromAlias("o")), valueDate(dateTo)));
        }

        if (compensationTypeId != null)
            dql.where(eq(property(EnrOrder.compensationType().code().fromAlias("o")), value(compensationTypeId)));

        if (formativeOrgUnitIds != null && !formativeOrgUnitIds.isEmpty())
        {
            List<Long> ids = new ArrayList<>();
            for (String stringId : formativeOrgUnitIds) ids.add(Long.parseLong(stringId));
            dql.where(DQLExpressions.in(property(EnrEnrollmentParagraph.formativeOrgUnit().id().fromAlias("p")), ids));
        }

        if (programFormIds != null && !programFormIds.isEmpty())
            dql.where(DQLExpressions.in(property(EnrEnrollmentParagraph.programForm().code().fromAlias("p")), programFormIds));

        List<EnrEnrollmentExtract> enrollmentExtractList = dql.createStatement(getSession()).list();

        return getEnrollmentOrderEnvironmentData(campaign, enrollmentExtractList, withPrintData);
    }

    @Override
    public EnrOrderEnvironmentNode getOrderData(EnrEnrollmentCampaign campaign)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EnrEnrollmentExtract.class, "e").column("e")
        .joinEntity("e", DQLJoinType.inner, EnrEnrollmentParagraph.class, "p", eq(property(EnrEnrollmentExtract.paragraph().id().fromAlias("e")), property(EnrEnrollmentParagraph.id().fromAlias("p"))))
        .joinEntity("p", DQLJoinType.inner, EnrOrder.class, "o", eq(property(EnrEnrollmentParagraph.order().id().fromAlias("p")), property(EnrOrder.id().fromAlias("o"))))
        .where(eq(property(EnrOrder.enrollmentCampaign().fromAlias("o")), value(campaign)))
        .where(DQLExpressions.in(property(EnrOrder.state().code().fromAlias("o")), Arrays.asList(UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTED, UnimoveDefines.CATALOG_ORDER_STATE_FINISHED)))
        .order(property(EnrOrder.commitDate().fromAlias("o")))
        .order(property(EnrOrder.id().fromAlias("o")))
        .order(property(EnrEnrollmentParagraph.number().fromAlias("p")))
        .order(property(EnrEnrollmentExtract.number().fromAlias("e")));

        List<EnrEnrollmentExtract> enrollmentExtractList = dql.createStatement(getSession()).list();

        return getEnrollmentOrderEnvironmentData(campaign, enrollmentExtractList, true);
    }

    private EnrOrderEnvironmentNode getEnrollmentOrderEnvironmentData(EnrEnrollmentCampaign EnrEnrollmentCampaign, List<EnrEnrollmentExtract> enrollmentExtractList, boolean withPrintData)
    {
        EnrOrderEnvironmentNode envNode = new EnrOrderEnvironmentNode();

        envNode.enrollmentCampaignTitle = EnrEnrollmentCampaign.getTitle();
        envNode.educationYearTitle = EnrEnrollmentCampaign.getEducationYear().getTitle();

        // сохраняем нужные справочники

        for (OrderStates row : getList(OrderStates.class, OrderStates.P_CODE))
            envNode.orderState.row.add(new EnrOrderEnvironmentNode.Row(row.getTitle(), row.getCode()));

        for (CompensationType row : getList(CompensationType.class, CompensationType.P_CODE))
            envNode.compensationType.row.add(new EnrOrderEnvironmentNode.Row(row.getShortTitle(), row.getTitle(), row.getCode()));

        for (EduLevel row : getList(EduLevel.class, EduLevel.P_CODE))
            envNode.eduLevel.row.add(new EnrOrderEnvironmentNode.Row(row.getTitle(), row.getCode()));

        for (EduProgramForm row : getList(EduProgramForm.class, EduProgramForm.P_CODE))
            envNode.eduProgramForm.row.add(new EnrOrderEnvironmentNode.Row(row.getShortTitle(), row.getTitle(), row.getCode()));

        for (EduProgramKind row : getList(EduProgramKind.class, EduProgramKind.P_PRIORITY))
            envNode.eduProgramKind.row.add(new EnrOrderEnvironmentNode.Row(row.getShortTitle(), row.getTitle(), row.getCode()));

        for (EnrRequestType row : getList(EnrRequestType.class, EnrRequestType.P_CODE))
            envNode.requestType.row.add(new EnrOrderEnvironmentNode.Row(row.getShortTitle(), row.getTitle(), row.getCode()));

        for (EnrCompetitionType row : getList(EnrCompetitionType.class, EnrCompetitionType.P_CODE))
            envNode.competitionType.row.add(new EnrOrderEnvironmentNode.Row(row.getShortTitle(), row.getTitle(), row.getCode()));

        // создаем структуру данных: приказы в порядке даты приказа -> параграфы в порядке номеров -> выписки в порядке номеров
        final Map<EnrOrder, Map<EnrEnrollmentParagraph, List<EnrEnrollmentExtract>>> map = new LinkedHashMap<>();

        for (EnrEnrollmentExtract extract : enrollmentExtractList)
        {
            EnrEnrollmentParagraph paragraph = (EnrEnrollmentParagraph) extract.getParagraph();
            EnrOrder order = (EnrOrder) paragraph.getOrder();

            Map<EnrEnrollmentParagraph, List<EnrEnrollmentExtract>> subMap = map.get(order);
            if (subMap == null)
                map.put(order, subMap = new LinkedHashMap<>());
            List<EnrEnrollmentExtract> list = subMap.get(paragraph);
            if (list == null)
                subMap.put(paragraph, list = new ArrayList<>());
            list.add(extract);
        }

        Set<Long> usedOrgUnitIds = new HashSet<>();
        Set<OrgUnitType> usedOrgUnitTypes = new HashSet<>();
        Set<EduProgramSubject> subjects = new HashSet<>();

        for (Map.Entry<EnrOrder, Map<EnrEnrollmentParagraph, List<EnrEnrollmentExtract>>> orderMapEntry : map.entrySet())
        {
            EnrOrder order = orderMapEntry.getKey();
            EnrOrderEnvironmentNode.EnrollmentOrderNode.EnrollmentOrderRow orderRow = new EnrOrderEnvironmentNode.EnrollmentOrderNode.EnrollmentOrderRow();

            orderRow.id = order.getId();
            if (withPrintData && order.getPrintForm() != null && order.getPrintForm().getContent() != null) {
                orderRow.text = order.getPrintForm().getContent();
            }
            orderRow.compensationType = order.getCompensationType().getCode();
            orderRow.requestType = order.getRequestType().getCode();
            orderRow.date = order.getCommitDate();
            orderRow.number = order.getNumber();
            orderRow.state = order.getState().getCode();

            for (Map.Entry<EnrEnrollmentParagraph, List<EnrEnrollmentExtract>> paragraphListEntry : orderMapEntry.getValue().entrySet())
            {
                EnrEnrollmentParagraph paragraph = paragraphListEntry.getKey();
                EnrOrderEnvironmentNode.EnrollmentParagraphNode.EnrollmentParagraphRow paragraphRow = new EnrOrderEnvironmentNode.EnrollmentParagraphNode.EnrollmentParagraphRow();

                OrgUnit enrOrgUnit = paragraph.getEnrOrgUnit().getInstitutionOrgUnit().getOrgUnit();
                OrgUnit formativeOrgUnit = paragraph.getFormativeOrgUnit();

                for (OrgUnit orgUnit : new OrgUnit[] {enrOrgUnit, formativeOrgUnit}) {
                    if (null != orgUnit && usedOrgUnitIds.add(orgUnit.getId())) {
                        usedOrgUnitTypes.add(orgUnit.getOrgUnitType());
                        String settlementTitle = null;
                        if (orgUnit.getAddress() != null && orgUnit.getAddress().getSettlement() != null)
                            settlementTitle = orgUnit.getAddress().getSettlement().getTitle();
                        envNode.orgUnit.row.add(new EnrOrderEnvironmentNode.OrgUnitNode.OrgUnitRow(orgUnit.getShortTitle(), orgUnit.getTitle(), orgUnit.getId().toString(), orgUnit.getOrgUnitType().getCode(), orgUnit.getNominativeCaseTitle(), settlementTitle, orgUnit.getTerritorialTitle(), orgUnit.getTerritorialShortTitle()));
                    }
                }

                subjects.add(paragraph.getProgramSubject());

                if (paragraph.getGroupManager() != null)
                    paragraphRow.groupManager = paragraph.getGroupManager().getEntity().getId().toString();

                paragraphRow.enrOrgUnit = enrOrgUnit.getId().toString();
                paragraphRow.formativeOrgUnit = formativeOrgUnit.getId().toString();
                paragraphRow.programSubject = paragraph.getProgramSubject().getCode();
                paragraphRow.eduLevel = paragraph.getProgramSubject().getEduProgramKind().getEduLevel().getCode();
                paragraphRow.programKind = paragraph.getProgramSubject().getEduProgramKind().getCode();
                paragraphRow.programForm = paragraph.getProgramForm().getCode();
                paragraphRow.competitionType = paragraph.getCompetitionType().getCode();
                paragraphRow.parallel = paragraph.isParallel();

                for (EnrEnrollmentExtract extract : paragraphListEntry.getValue())
                {
                    EnrEntrant entrant = extract.getEntity().getRequest().getEntrant();
                    IdentityCard card = entrant.getPerson().getIdentityCard();
                    EnrOrderEnvironmentNode.EnrollmentExtractNode.EnrollmentExtractRow extractRow = new EnrOrderEnvironmentNode.EnrollmentExtractNode.EnrollmentExtractRow(card.getMiddleName(), card.getFirstName(), card.getLastName(), entrant.getId().toString());
                    extractRow.cancelled = extract.isCancelled();
                    paragraphRow.extract.row.add(extractRow);
                }

                orderRow.paragraph.row.add(paragraphRow);
            }

            envNode.order.row.add(orderRow);
        }

        List<EduProgramSubject> sortedSubjects = new ArrayList<>();
        sortedSubjects.addAll(subjects);
        Collections.sort(sortedSubjects, ITitled.TITLED_COMPARATOR);
        for (EduProgramSubject row : sortedSubjects)
            envNode.eduProgramSubject.row.add(new EnrOrderEnvironmentNode.Row(row.getSubjectCode(), row.getTitle(), row.getCode()));


        List<OrgUnitType> orgUnitTypeList = new ArrayList<>(usedOrgUnitTypes);
        Collections.sort(orgUnitTypeList, CommonCatalogUtil.CATALOG_CODE_COMPARATOR);
        for (OrgUnitType type : orgUnitTypeList)
            envNode.orgUnitType.row.add(new EnrOrderEnvironmentNode.Row(type.getTitle(), type.getCode()));

        Collections.sort(envNode.orgUnit.row);

        return envNode;
    }
    
}
