/* $Id:$ */
package ru.tandemservice.unienr14.order.bo.EnrOrder.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.order.bo.EnrOrder.logic.EnrOrderSearchListDSHandler;
import ru.tandemservice.unienr14.order.entity.*;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unimove.IAbstractOrder;


/**
 * @author oleyba
 * @since 5/17/13
 */
@Configuration
public class EnrOrderList extends BusinessComponentManager
{

    public static final String COMPETITION_FILTER_ADDON = CommonFilterAddon.class.getSimpleName();

    public static final String ORDER_LIST_DS = "orderListDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
                .addDataSource(searchListDS(ORDER_LIST_DS, orderListDSColumns(), orderListDSHandler()))
                .addAddon(uiAddon(COMPETITION_FILTER_ADDON, EnrCompetitionFilterAddon.class))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> orderListDSHandler()
    {
        return new EnrOrderSearchListDSHandler(getName());
    }

    @Bean
    public ColumnListExtPoint orderListDSColumns()
    {
        return columnListExtPointBuilder(ORDER_LIST_DS)
                .addColumn(checkboxColumn("selected"))
                .addColumn(textColumn("createDate", EnrOrder.createDate()).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).clickable(true).order())
                .addColumn(textColumn("commitDate", EnrOrder.commitDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order())
                .addColumn(textColumn("number", EnrOrder.number()).order())
                .addColumn(textColumn("type", EnrOrder.type().shortTitle()).order())
                .addColumn(textColumn("kind", EnrOrder.requestType().shortTitle()).order())
                .addColumn(textColumn("compensationType", EnrOrder.compensationType().shortTitle()).order())
                .addColumn(textColumn("programForm", "programForm"))
                .addColumn(textColumn("enrOrgUnit", "enrOrgUnit"))
                .addColumn(textColumn("systemCommitDate", IAbstractOrder.P_COMMIT_DATE_SYSTEM).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).order())
                .addColumn(textColumn("state", EnrOrder.state().title()).order())
                .addColumn(actionColumn("print", CommonDefines.ICON_PRINT, "onClickPrint").permissionKey("enrOrderListPrintOrder").disabled("disabledPrint"))
                .addColumn(actionColumn("edit", CommonDefines.ICON_EDIT, "onClickEditEnrollmentOrder").permissionKey("enrOrderListEditOrder").disabled(EnrOrder.P_READONLY))
                .addColumn(actionColumn("delete", CommonDefines.ICON_DELETE, "onClickDeleteEnrollmentOrder", alert("Удалить «{0}»?", IAbstractOrder.P_TITLE)).permissionKey("enrOrderListDeleteOrder").disabled(EnrOrder.P_READONLY))
                .create();
    }


}