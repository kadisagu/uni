/* $Id:$ */
package ru.tandemservice.unienr14.exams.bo.EnrExamSet.ui.VariantAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author oleyba
 * @since 2/5/14
 */
@Configuration
public class EnrExamSetVariantAddEdit extends BusinessComponentManager
{
    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
            .create();
    }
}

