package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x2_16to17 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
        {
            new ScriptDependency("org.tandemframework", "1.6.15"),
            new ScriptDependency("org.tandemframework.shared", "1.6.2")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {

        {
            // создать колонку
            if (!tool.columnExists("enr14_requested_comp_t", "contractenrollmentavailable_p"))
                tool.createColumn("enr14_requested_comp_t", new DBColumn("contractenrollmentavailable_p", DBType.BOOLEAN));

            // задать значение по умолчанию
            tool.executeUpdate("update enr14_requested_comp_t set contractenrollmentavailable_p=? where contractenrollmentavailable_p is null", Boolean.FALSE);

            // сделать колонку NOT NULL
            tool.setColumnNullable("enr14_requested_comp_t", "contractenrollmentavailable_p", false);

        }

        {
            // создать колонку
            if (!tool.columnExists("enr14_campaign_settings_t", "nrentrntcntrctexstchckmd_p"))
                tool.createColumn("enr14_campaign_settings_t", new DBColumn("nrentrntcntrctexstchckmd_p", DBType.INTEGER));

            // задать значение по умолчанию
            tool.executeUpdate("update enr14_campaign_settings_t set nrentrntcntrctexstchckmd_p=? where nrentrntcntrctexstchckmd_p is null", Integer.valueOf(0));

            // сделать колонку NOT NULL
            tool.setColumnNullable("enr14_campaign_settings_t", "nrentrntcntrctexstchckmd_p", false);

        }


    }
}