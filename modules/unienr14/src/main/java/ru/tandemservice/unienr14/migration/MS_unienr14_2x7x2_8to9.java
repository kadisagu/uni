package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x7x2_8to9 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrEnrollmentCampaignSettings

		// создано обязательное свойство tieBreakRatingRule
		{
			// создать колонку
			tool.createColumn("enr14_campaign_settings_t", new DBColumn("tiebreakratingrule_id", DBType.LONG));

            if (!tool.hasResultRows("select id from enr14_c_tie_rating_rule_t")) {
                short entityCode = tool.entityCodes().ensure("enrTieBreakRatingRule");
                Long id = EntityIDGenerator.generateNewId(entityCode);
                tool.executeUpdate("insert into enr14_c_tie_rating_rule_t (id, discriminator, code_p, title_p) values (?, ?, ?, ?)", id, entityCode, "1", "1");
            }
			tool.executeUpdate("update enr14_campaign_settings_t set tiebreakratingrule_id = (select id from enr14_c_tie_rating_rule_t where code_p=?)", "1");

			tool.setColumnNullable("enr14_campaign_settings_t", "tiebreakratingrule_id", false);
		}
    }
}