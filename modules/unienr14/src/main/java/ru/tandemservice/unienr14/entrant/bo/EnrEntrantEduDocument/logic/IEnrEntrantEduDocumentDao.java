/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrantEduDocument.logic;

import org.apache.tapestry.request.IUploadFile;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;

import java.util.Date;

/**
 * @author oleyba
 * @since 3/18/14
 */
public interface IEnrEntrantEduDocumentDao extends INeedPersistenceSupport
{
    void saveOrUpdateEduDocument(EnrEntrant entrant, PersonEduDocument document, IUploadFile scanCopyFile, Date originalInDate, Date originalOutDate, EnrRequestedCompetition requestedCompetition);

    @Transactional(readOnly = true)
    boolean isNeedReqCompForOriginal(EnrEntrant entrant, Long eduDocumentId);
}