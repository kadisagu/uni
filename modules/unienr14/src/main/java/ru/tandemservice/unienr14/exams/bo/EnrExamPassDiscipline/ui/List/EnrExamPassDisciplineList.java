/* $Id$ */
package ru.tandemservice.unienr14.exams.bo.EnrExamPassDiscipline.ui.List;

import com.google.common.collect.Lists;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.resolver.DefaultLinkResolver;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.commonbase.base.util.SimplePublisherLinkResolver;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.commonbase.caf.logic.handler.SimpleTitledComboDataSourceWithPopupSizeHandler;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.Pub.EnrEntrantPub;
import ru.tandemservice.unienr14.exams.bo.EnrExamPassDiscipline.EnrExamPassDisciplineManager;
import ru.tandemservice.unienr14.exams.bo.EnrExamPassDiscipline.logic.EnrExamPassDisciplineExamSelectDSHandler;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroup;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroupScheduleEvent;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.logic.EnrOrgUnitBaseDSHandler;
import ru.tandemservice.unienr14.settings.entity.EnrExamGroupSet;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 4/6/15
 */
@Configuration
public class EnrExamPassDisciplineList extends BusinessComponentManager
{
    public static final String ENR_CAMP_SELECT_DS = EnrEnrollmentCampaignManager.DS_ENR_CAMPAIGN;
    public static final String ORG_UNIT_DS = "orgUnitDS";
    public static final String EXAM_PASS_DISCIPLINE_SEARCH_DS = "examPassDisciplineSearchDS";
    public static final String ENR_DISCIPLINE_SELECT_DS = "enrDisciplineSelectDS";
    public static final String ABSENCE_NOTE_SELECT_DS = "absenceNoteSelectDS";
	public static final String EXAM_GROUP_SCHEDULE_EVENT_SELECT_DS = "examGroupScheduleEventSelectDS";
    public static final String EXAM_GROUP_SET_SELECT_DS = "examGroupSetSelectDS";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
                .addDataSource(selectDS(ORG_UNIT_DS, orgUnitDSHandler()).addColumn(EnrOrgUnit.institutionOrgUnit().orgUnit().shortTitleWithTopEmphasized().s()))
                .addDataSource(selectDS(ENR_DISCIPLINE_SELECT_DS, enrDisciplineDSHandler()))
                .addDataSource(selectDS(ABSENCE_NOTE_SELECT_DS, EnrExamPassDisciplineManager.instance().absenceNoteDSHandler()))
				.addDataSource(CommonBaseStaticSelectDataSource.selectDS(EXAM_GROUP_SCHEDULE_EVENT_SELECT_DS, getName(), EnrExamGroupScheduleEvent.defaultSelectDSHandler(getName())))
				.addDataSource(selectDS(EXAM_GROUP_SET_SELECT_DS, examGroupSetDSHandler()))
                .addDataSource(searchListDS(EXAM_PASS_DISCIPLINE_SEARCH_DS, examPassDisciplineSearchDSColumns(), examPassDisciplineSearchDSHandler()))
                .addAddon(uiAddon(CommonFilterAddon.class.getSimpleName(), EnrCompetitionFilterAddon.class))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler enrDisciplineDSHandler()
    {
        return new EnrExamPassDisciplineExamSelectDSHandler(getName());
    }

    @Bean
    @SuppressWarnings("unchecked")
    public ColumnListExtPoint examPassDisciplineSearchDSColumns()
    {
        return columnListExtPointBuilder(EXAM_PASS_DISCIPLINE_SEARCH_DS)
                .addColumn(textColumn("personalNumber", EnrExamPassDiscipline.entrant().personalNumber().s()).order())
                .addColumn(publisherColumn("fio", EnrExamPassDiscipline.entrant().person().identityCard().fullFio()).publisherLinkResolver(DefaultLinkResolver.with()
                        .component(EnrEntrantPub.class)
                        .primaryKeyPath(EnrExamPassDiscipline.entrant().id())
                        .primaryKeyAsParameter(UIPresenter.PUBLISHER_ID)
                        .parametersMapBinding("mvel:['selectedTab':'chosenExamTab']")
                        .create()).order().required(true))
                .addColumn(textColumn("identityCard", EnrExamPassDiscipline.entrant().person().identityCard().fullNumber()))
                .addColumn(textColumn("examResult", EnrExamPassDiscipline.shortExamResult()))
                .addColumn(textColumn("markDate", EnrExamPassDiscipline.markDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order())
                .addColumn(textColumn("appeal", "appeal").formatter(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS))
                .addColumn(textColumn("retake", EnrExamPassDiscipline.retake()).formatter(YesNoFormatter.INSTANCE))
                .addColumn(publisherColumn("examGroup", EnrExamPassDiscipline.examGroup().title()).order()
                                   .publisherLinkResolver(new SimplePublisherLinkResolver(EnrExamPassDiscipline.examGroup().id())))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler examPassDisciplineSearchDSHandler()
    {
        return new EnrExamPassDisciplineSearchDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler orgUnitDSHandler()
    {
        return new EnrOrgUnitBaseDSHandler(getName())
                .where(EnrOrgUnit.enrollmentCampaign(), EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
    }

    @Bean
    public IDefaultComboDataSourceHandler examGroupSetDSHandler()
    {
        return new SimpleTitledComboDataSourceWithPopupSizeHandler(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrExamGroupSet.class, "gs")
                        .where(eq(property("gs", EnrExamGroupSet.enrollmentCampaign()), commonValue(context.getNotNull(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN))));

                builder.order(property("gs", EnrExamGroupSet.beginDate()));
                builder.order(property("gs", EnrExamGroupSet.endDate()));

                List<DataWrapper> wrappers = Lists.newArrayList();
                for(EnrExamGroupSet examGroupSet : createStatement(builder).<EnrExamGroupSet>list())
                {
                    wrappers.add(new DataWrapper(examGroupSet.getId(), examGroupSet.getPeriodTitle()));
                }

                context.put(UIDefines.COMBO_OBJECT_LIST, wrappers);
                return super.execute(input, context);
            }
        }.filtered(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler examGroupDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrExamGroup.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                DataWrapper examGroupSet = context.get(EnrExamPassDisciplineListUI.SETTING_EXAM_GROUP_SET);
                if(examGroupSet != null)
                    dql.where(eq(property(alias, EnrExamGroup.examGroupSet().id()), value(examGroupSet.getId())));

            }
        }
                .where(EnrExamGroup.examGroupSet().enrollmentCampaign(), EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN)
                .where(EnrExamGroup.territorialOrgUnit(), EnrExamPassDisciplineListUI.SETTING_ORG_UNIT)
                .filter(EnrExamGroup.title())
                .order(EnrExamGroup.title())
                .pageable(true);
    }



}



    