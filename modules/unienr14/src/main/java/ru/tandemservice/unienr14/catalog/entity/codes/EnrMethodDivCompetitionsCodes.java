package ru.tandemservice.unienr14.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Способ деления конкурсов"
 * Имя сущности : enrMethodDivCompetitions
 * Файл data.xml : unienr.catalog.data.xml
 */
public interface EnrMethodDivCompetitionsCodes
{
    /** Константа кода (code) элемента : Без деления (title) */
    String SEC_NO_DIV = "no_div_spo";
    /** Константа кода (code) элемента : Без деления (title) */
    String MASTER_NO_DIV = "no_div_master";
    /** Константа кода (code) элемента : Без деления (title) */
    String HIGHER_NO_DIV = "no_div_higher";
    /** Константа кода (code) элемента : Без деления (title) */
    String POSTGRADUATE_NO_DIV = "no_div_postgraduate";
    /** Константа кода (code) элемента : Без деления (title) */
    String TRAINEESHIP_NO_DIV = "no_div_traineeship";
    /** Константа кода (code) элемента : Без деления (title) */
    String INTERNSHIP_NO_DIV = "no_div_internship";
    /** Константа кода (code) элемента : Без деления (title) */
    String BS_NO_DIV = "no_div_bs";
    /** Константа кода (code) элемента : СОО, СПО, ВО отдельно (title) */
    String BS_SOO_SPO_VO = "bs_soo_spo_vo";
    /** Константа кода (code) элемента : СОО отдельно от ПО (title) */
    String BS_SOO_PO = "bs_soo_po";
    /** Константа кода (code) элемента : СОО и СПО отдельно от ВО (title) */
    String BS_SOO_AND_SPO_APART_VO = "bs_soo_spo_and_po";
    /** Константа кода (code) элемента : СОО и ВО отдельно от СПО (title) */
    String BS_SOO_AND_VO_APART_SPO = "bs_soo_vo_and_spo";

    Set<String> CODES = ImmutableSet.of(SEC_NO_DIV, MASTER_NO_DIV, HIGHER_NO_DIV, POSTGRADUATE_NO_DIV, TRAINEESHIP_NO_DIV, INTERNSHIP_NO_DIV, BS_NO_DIV, BS_SOO_SPO_VO, BS_SOO_PO, BS_SOO_AND_SPO_APART_VO, BS_SOO_AND_VO_APART_SPO);
}
