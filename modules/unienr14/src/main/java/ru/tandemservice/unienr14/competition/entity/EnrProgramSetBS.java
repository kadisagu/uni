package ru.tandemservice.unienr14.competition.entity;

import ru.tandemservice.unienr14.catalog.entity.codes.EnrMethodDivCompetitionsCodes;
import ru.tandemservice.unienr14.competition.entity.gen.EnrProgramSetBSGen;

import static com.google.common.base.Objects.equal;

/**
 * Набор ОП для приема (бакалавры и специалисты)
 *
 * Определяет перечень ОП, на который формируются конкурсы, для приема на бакалавриат и специалитет.
 */
public class EnrProgramSetBS extends EnrProgramSetBSGen
{
    public static final String[] VARIANT_USAGE_PROPERTIES = new String[]{L_EXAM_SET_VARIANT_MIN_BASE,
        L_EXAM_SET_VARIANT_MIN_PROF,
        L_EXAM_SET_VARIANT_CON_BASE,
        L_EXAM_SET_VARIANT_CON_PROF
    };

    public EnrProgramSetBS()
    {
    }

    public static String getVariantUsageTitle(String property) {
        switch (property) {
            case EnrProgramSetBS.L_EXAM_SET_VARIANT_MIN_BASE: return "Поступающие на базе СОО, бюджет";
            case EnrProgramSetBS.L_EXAM_SET_VARIANT_MIN_PROF: return "Поступающие на базе ПО, бюджет";
            case EnrProgramSetBS.L_EXAM_SET_VARIANT_CON_BASE: return "Поступающие на базе СОО, по договору";
            case EnrProgramSetBS.L_EXAM_SET_VARIANT_CON_PROF: return "Поступающие на базе ПО, по договору";
        }
        throw new IllegalArgumentException();
    }

    /** @return StringBuilder, содержащий названия всех ключевых параметров набора ОП */
    protected StringBuilder titleBuilder() {
        return new StringBuilder()
        .append(this.getProgramSubject().getTitleWithCode()).append(", ")
        .append(this.getProgramForm().getShortTitle())
        ;
    }

    /** @return название конкурса, сгенерированное на основе параметров набора ОП */
    public String generateTitle() {
        return titleBuilder().toString();
    }

    @Override
    public boolean isContractDividedByEduLevel()
    {
        return getExamSetVariantConBase() != null && getExamSetVariantConProf() != null && !getExamSetVariantConBase().equals(getExamSetVariantConProf());
    }

    public boolean isValidExamSetVariants()
    {
        if(getExamSetVariantHigh() == null || getExamSetVariantBase() == null || getExamSetVariantSec() == null) return false;

        switch (getMethodDivCompetitions().getCode()) {
            case EnrMethodDivCompetitionsCodes.BS_NO_DIV:
                return equal(getExamSetVariantBase(), getExamSetVariantSec()) &&
                    equal(getExamSetVariantBase(), getExamSetVariantHigh()) &&
                    equal(getExamSetVariantSec(), getExamSetVariantHigh());
            case EnrMethodDivCompetitionsCodes.BS_SOO_SPO_VO:
                return !equal(getExamSetVariantBase(), getExamSetVariantSec()) &&
                    !equal(getExamSetVariantBase(), getExamSetVariantHigh()) &&
                    !equal(getExamSetVariantSec(), getExamSetVariantHigh());
            case EnrMethodDivCompetitionsCodes.BS_SOO_PO:
                return !equal(getExamSetVariantBase(), getExamSetVariantSec()) &&
                    equal(getExamSetVariantSec(), getExamSetVariantHigh());
            case EnrMethodDivCompetitionsCodes.BS_SOO_AND_SPO_APART_VO:
                return equal(getExamSetVariantBase(), getExamSetVariantSec()) &&
                    !equal(getExamSetVariantBase(), getExamSetVariantHigh());
            case EnrMethodDivCompetitionsCodes.BS_SOO_AND_VO_APART_SPO:
                return equal(getExamSetVariantBase(), getExamSetVariantHigh()) &&
                    !equal(getExamSetVariantBase(), getExamSetVariantSec());
        }

        throw new IllegalStateException();
    }

    @Override
    public boolean isMinisterialDividedByEduLevel()
    {
        return getExamSetVariantMinBase() != null && getExamSetVariantMinProf() != null && !getExamSetVariantMinBase().equals(getExamSetVariantMinProf());
    }
}