/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.logic;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import ru.tandemservice.unienr14.catalog.entity.EnrTargetAdmissionKind;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.ui.CompetitionInStepPub.EnrEnrollmentModelCompetitionInStepPubUI;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.ui.CompetitionPub.EnrEnrollmentModelCompetitionPubUI;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentModel;
import ru.tandemservice.unienr14.enrollment.entity.EnrModelStep;
import ru.tandemservice.unienr14.enrollment.entity.EnrModelStepItem;

import java.util.Map;

/**
 * @author oleyba
 * @since 4/1/15
 */
public class EnrModelCompetitionWrapper extends IdentifiableWrapper
{
    private static final long serialVersionUID = 1L;

    private Long stepId;
    private Long modelId;
    private EnrCompetition competition;
    private EnrTargetAdmissionKind targetAdmissionKind;

    private int plan;
    private int enrolled;
    private int reEnrolled;
    private Double avgStateExamMark;
    private Double passRating;

    EnrModelCompetitionWrapper(EnrModelStep step, EnrEnrollmentModel model, EnrCompetition competition, EnrTargetAdmissionKind targetAdmissionKind)
    {
        super(competition.getId(), competition.getTitle() + (targetAdmissionKind != null ? " (" + targetAdmissionKind.getTitle() + ")" : ""));
        this.stepId = step == null ? null : step.getId();
        this.modelId = model.getId();
        this.competition = competition;
        this.targetAdmissionKind = targetAdmissionKind;
    }

    public Map getPublisherParameters() {
        ParametersMap parametersMap = new ParametersMap()
            .add(EnrEnrollmentModelCompetitionInStepPubUI.PARAM_STEP, stepId)
            .add(EnrEnrollmentModelCompetitionPubUI.PARAM_MODEL, modelId)
            .add(EnrEnrollmentModelCompetitionPubUI.PARAM_COMPETITION, competition.getId());
        if (targetAdmissionKind != null) {
            parametersMap.add(EnrEnrollmentModelCompetitionPubUI.PARAM_TARGET_ADMISSION_KIND, targetAdmissionKind.getId());
        }
        return parametersMap;
    }

    public MultiKey key() {
        return new MultiKey(competition.getId(), targetAdmissionKind);
    }

    public String getAvgStateExamMark()
    {
        return  competition.isNoExams() ? "—" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(avgStateExamMark);
    }


    public String getPassRating()
    {
        return competition.isNoExams() ? "—" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(passRating);
    }

    void updatePassRating(EnrModelStepItem item)
    {
        if (!item.isEnrolled()) return;
        if (passRating == null) {
            passRating = item.getEntity().getTotalMarkAsDouble();
            return;
        }
        passRating = Math.min(passRating, item.getEntity().getTotalMarkAsDouble());
    }

    void calcAvgStateExamMark(long markSum, int markCount)
    {
        if (markSum > 0) avgStateExamMark = markSum / (double) (markCount * 1000);
    }

    static MultiKey key(EnrCompetition competition, EnrTargetAdmissionKind taKind) {
        return new MultiKey(competition.getId(), taKind);
    }

    static MultiKey key(EnrCompetition competition) {
        return new MultiKey(competition.getId(), null);
    }

    // getters and setters

    public int getPlan()
    {
        return plan;
    }

    void setPlan(int plan)
    {
        this.plan = plan;
    }

    public int getEnrolled()
    {
        return enrolled;
    }

    void incEnrolled()
    {
        this.enrolled++;
    }

    public int getReEnrolled()
    {
        return reEnrolled;
    }

    void incReEnrolled()
    {
        this.reEnrolled++;
    }

}



