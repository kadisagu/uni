package ru.tandemservice.unienr14.entrant.entity;

import org.tandemframework.shared.person.base.entity.PersonNextOfKin;
import ru.tandemservice.unienr14.entrant.entity.gen.*;

/**
 * Ближайший родственник онлайн-абитуриента
 */
public class EnrOnlineEntrantNextOfKin extends EnrOnlineEntrantNextOfKinGen
{
    public void fillTo(PersonNextOfKin nextOfKin) {
        nextOfKin.setLastName(getLastname());
        nextOfKin.setFirstName(getFirstname());
        nextOfKin.setMiddleName(getPatronymic());
        nextOfKin.setEmploymentPlace(getEmploymentPlace());
        nextOfKin.setPost(getPost());
        nextOfKin.setRelationDegree(getRelationDegree());
        nextOfKin.setPhones(getPhones());
        nextOfKin.setSex(getRelationDegree().getSex());
    }
}