package ru.tandemservice.unienr14.request.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.request.entity.EnrEntrantForeignRequest;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Направление на поступление иностранного абитуриента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrEntrantForeignRequestGen extends EntityBase
 implements INaturalIdentifiable<EnrEntrantForeignRequestGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.request.entity.EnrEntrantForeignRequest";
    public static final String ENTITY_NAME = "enrEntrantForeignRequest";
    public static final int VERSION_HASH = 1880243884;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTRANT = "entrant";
    public static final String L_COMPETITION = "competition";
    public static final String P_REG_NUMBER = "regNumber";
    public static final String P_REG_DATE = "regDate";
    public static final String P_ENROLLMENT_DATE = "enrollmentDate";
    public static final String P_COMMENT = "comment";
    public static final String L_IDENTITY_CARD = "identityCard";
    public static final String L_EDU_DOCUMENT = "eduDocument";

    private EnrEntrant _entrant;     // Абитуриент
    private EnrCompetition _competition;     // Конкурс
    private String _regNumber;     // Номер
    private Date _regDate;     // Дата добавления
    private Date _enrollmentDate;     // Дата зачисления
    private String _comment;     // Комментарий
    private IdentityCard _identityCard;     // Удостоверение личности
    private PersonEduDocument _eduDocument;     // Документ об образовании

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Абитуриент. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EnrEntrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant Абитуриент. Свойство не может быть null и должно быть уникальным.
     */
    public void setEntrant(EnrEntrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    /**
     * @return Конкурс. Свойство не может быть null.
     */
    @NotNull
    public EnrCompetition getCompetition()
    {
        return _competition;
    }

    /**
     * @param competition Конкурс. Свойство не может быть null.
     */
    public void setCompetition(EnrCompetition competition)
    {
        dirty(_competition, competition);
        _competition = competition;
    }

    /**
     * @return Номер. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getRegNumber()
    {
        return _regNumber;
    }

    /**
     * @param regNumber Номер. Свойство не может быть null.
     */
    public void setRegNumber(String regNumber)
    {
        dirty(_regNumber, regNumber);
        _regNumber = regNumber;
    }

    /**
     * @return Дата добавления. Свойство не может быть null.
     */
    @NotNull
    public Date getRegDate()
    {
        return _regDate;
    }

    /**
     * @param regDate Дата добавления. Свойство не может быть null.
     */
    public void setRegDate(Date regDate)
    {
        dirty(_regDate, regDate);
        _regDate = regDate;
    }

    /**
     * @return Дата зачисления.
     */
    public Date getEnrollmentDate()
    {
        return _enrollmentDate;
    }

    /**
     * @param enrollmentDate Дата зачисления.
     */
    public void setEnrollmentDate(Date enrollmentDate)
    {
        dirty(_enrollmentDate, enrollmentDate);
        _enrollmentDate = enrollmentDate;
    }

    /**
     * @return Комментарий.
     */
    @Length(max=2048)
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    /**
     * @return Удостоверение личности. Свойство не может быть null.
     */
    @NotNull
    public IdentityCard getIdentityCard()
    {
        return _identityCard;
    }

    /**
     * @param identityCard Удостоверение личности. Свойство не может быть null.
     */
    public void setIdentityCard(IdentityCard identityCard)
    {
        dirty(_identityCard, identityCard);
        _identityCard = identityCard;
    }

    /**
     * @return Документ об образовании. Свойство не может быть null.
     */
    @NotNull
    public PersonEduDocument getEduDocument()
    {
        return _eduDocument;
    }

    /**
     * @param eduDocument Документ об образовании. Свойство не может быть null.
     */
    public void setEduDocument(PersonEduDocument eduDocument)
    {
        dirty(_eduDocument, eduDocument);
        _eduDocument = eduDocument;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrEntrantForeignRequestGen)
        {
            if (withNaturalIdProperties)
            {
                setEntrant(((EnrEntrantForeignRequest)another).getEntrant());
            }
            setCompetition(((EnrEntrantForeignRequest)another).getCompetition());
            setRegNumber(((EnrEntrantForeignRequest)another).getRegNumber());
            setRegDate(((EnrEntrantForeignRequest)another).getRegDate());
            setEnrollmentDate(((EnrEntrantForeignRequest)another).getEnrollmentDate());
            setComment(((EnrEntrantForeignRequest)another).getComment());
            setIdentityCard(((EnrEntrantForeignRequest)another).getIdentityCard());
            setEduDocument(((EnrEntrantForeignRequest)another).getEduDocument());
        }
    }

    public INaturalId<EnrEntrantForeignRequestGen> getNaturalId()
    {
        return new NaturalId(getEntrant());
    }

    public static class NaturalId extends NaturalIdBase<EnrEntrantForeignRequestGen>
    {
        private static final String PROXY_NAME = "EnrEntrantForeignRequestNaturalProxy";

        private Long _entrant;

        public NaturalId()
        {}

        public NaturalId(EnrEntrant entrant)
        {
            _entrant = ((IEntity) entrant).getId();
        }

        public Long getEntrant()
        {
            return _entrant;
        }

        public void setEntrant(Long entrant)
        {
            _entrant = entrant;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrEntrantForeignRequestGen.NaturalId) ) return false;

            EnrEntrantForeignRequestGen.NaturalId that = (NaturalId) o;

            if( !equals(getEntrant(), that.getEntrant()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEntrant());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEntrant());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrEntrantForeignRequestGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrEntrantForeignRequest.class;
        }

        public T newInstance()
        {
            return (T) new EnrEntrantForeignRequest();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entrant":
                    return obj.getEntrant();
                case "competition":
                    return obj.getCompetition();
                case "regNumber":
                    return obj.getRegNumber();
                case "regDate":
                    return obj.getRegDate();
                case "enrollmentDate":
                    return obj.getEnrollmentDate();
                case "comment":
                    return obj.getComment();
                case "identityCard":
                    return obj.getIdentityCard();
                case "eduDocument":
                    return obj.getEduDocument();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entrant":
                    obj.setEntrant((EnrEntrant) value);
                    return;
                case "competition":
                    obj.setCompetition((EnrCompetition) value);
                    return;
                case "regNumber":
                    obj.setRegNumber((String) value);
                    return;
                case "regDate":
                    obj.setRegDate((Date) value);
                    return;
                case "enrollmentDate":
                    obj.setEnrollmentDate((Date) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
                case "identityCard":
                    obj.setIdentityCard((IdentityCard) value);
                    return;
                case "eduDocument":
                    obj.setEduDocument((PersonEduDocument) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entrant":
                        return true;
                case "competition":
                        return true;
                case "regNumber":
                        return true;
                case "regDate":
                        return true;
                case "enrollmentDate":
                        return true;
                case "comment":
                        return true;
                case "identityCard":
                        return true;
                case "eduDocument":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entrant":
                    return true;
                case "competition":
                    return true;
                case "regNumber":
                    return true;
                case "regDate":
                    return true;
                case "enrollmentDate":
                    return true;
                case "comment":
                    return true;
                case "identityCard":
                    return true;
                case "eduDocument":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entrant":
                    return EnrEntrant.class;
                case "competition":
                    return EnrCompetition.class;
                case "regNumber":
                    return String.class;
                case "regDate":
                    return Date.class;
                case "enrollmentDate":
                    return Date.class;
                case "comment":
                    return String.class;
                case "identityCard":
                    return IdentityCard.class;
                case "eduDocument":
                    return PersonEduDocument.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrEntrantForeignRequest> _dslPath = new Path<EnrEntrantForeignRequest>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrEntrantForeignRequest");
    }
            

    /**
     * @return Абитуриент. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantForeignRequest#getEntrant()
     */
    public static EnrEntrant.Path<EnrEntrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return Конкурс. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantForeignRequest#getCompetition()
     */
    public static EnrCompetition.Path<EnrCompetition> competition()
    {
        return _dslPath.competition();
    }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantForeignRequest#getRegNumber()
     */
    public static PropertyPath<String> regNumber()
    {
        return _dslPath.regNumber();
    }

    /**
     * @return Дата добавления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantForeignRequest#getRegDate()
     */
    public static PropertyPath<Date> regDate()
    {
        return _dslPath.regDate();
    }

    /**
     * @return Дата зачисления.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantForeignRequest#getEnrollmentDate()
     */
    public static PropertyPath<Date> enrollmentDate()
    {
        return _dslPath.enrollmentDate();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantForeignRequest#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * @return Удостоверение личности. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantForeignRequest#getIdentityCard()
     */
    public static IdentityCard.Path<IdentityCard> identityCard()
    {
        return _dslPath.identityCard();
    }

    /**
     * @return Документ об образовании. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantForeignRequest#getEduDocument()
     */
    public static PersonEduDocument.Path<PersonEduDocument> eduDocument()
    {
        return _dslPath.eduDocument();
    }

    public static class Path<E extends EnrEntrantForeignRequest> extends EntityPath<E>
    {
        private EnrEntrant.Path<EnrEntrant> _entrant;
        private EnrCompetition.Path<EnrCompetition> _competition;
        private PropertyPath<String> _regNumber;
        private PropertyPath<Date> _regDate;
        private PropertyPath<Date> _enrollmentDate;
        private PropertyPath<String> _comment;
        private IdentityCard.Path<IdentityCard> _identityCard;
        private PersonEduDocument.Path<PersonEduDocument> _eduDocument;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Абитуриент. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantForeignRequest#getEntrant()
     */
        public EnrEntrant.Path<EnrEntrant> entrant()
        {
            if(_entrant == null )
                _entrant = new EnrEntrant.Path<EnrEntrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return Конкурс. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantForeignRequest#getCompetition()
     */
        public EnrCompetition.Path<EnrCompetition> competition()
        {
            if(_competition == null )
                _competition = new EnrCompetition.Path<EnrCompetition>(L_COMPETITION, this);
            return _competition;
        }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantForeignRequest#getRegNumber()
     */
        public PropertyPath<String> regNumber()
        {
            if(_regNumber == null )
                _regNumber = new PropertyPath<String>(EnrEntrantForeignRequestGen.P_REG_NUMBER, this);
            return _regNumber;
        }

    /**
     * @return Дата добавления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantForeignRequest#getRegDate()
     */
        public PropertyPath<Date> regDate()
        {
            if(_regDate == null )
                _regDate = new PropertyPath<Date>(EnrEntrantForeignRequestGen.P_REG_DATE, this);
            return _regDate;
        }

    /**
     * @return Дата зачисления.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantForeignRequest#getEnrollmentDate()
     */
        public PropertyPath<Date> enrollmentDate()
        {
            if(_enrollmentDate == null )
                _enrollmentDate = new PropertyPath<Date>(EnrEntrantForeignRequestGen.P_ENROLLMENT_DATE, this);
            return _enrollmentDate;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantForeignRequest#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(EnrEntrantForeignRequestGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * @return Удостоверение личности. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantForeignRequest#getIdentityCard()
     */
        public IdentityCard.Path<IdentityCard> identityCard()
        {
            if(_identityCard == null )
                _identityCard = new IdentityCard.Path<IdentityCard>(L_IDENTITY_CARD, this);
            return _identityCard;
        }

    /**
     * @return Документ об образовании. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantForeignRequest#getEduDocument()
     */
        public PersonEduDocument.Path<PersonEduDocument> eduDocument()
        {
            if(_eduDocument == null )
                _eduDocument = new PersonEduDocument.Path<PersonEduDocument>(L_EDU_DOCUMENT, this);
            return _eduDocument;
        }

        public Class getEntityClass()
        {
            return EnrEntrantForeignRequest.class;
        }

        public String getEntityName()
        {
            return "enrEntrantForeignRequest";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
