package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x8x1_14to15 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность roleAssignmentLocalEnrCommission

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("enr14_ra_local_comm_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_f9c27a4c"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("roleconfig_id", DBType.LONG).setNullable(false), 
				new DBColumn("principalcontext_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("roleAssignmentLocalEnrCommission");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность roleAssignmentTemplateEnrCommission

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("enr14_ra_template_comm_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_79fe7037"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("roleconfig_id", DBType.LONG).setNullable(false), 
				new DBColumn("principalcontext_id", DBType.LONG).setNullable(false), 
				new DBColumn("enrollmentcommission_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("roleAssignmentTemplateEnrCommission");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность roleConfigLocalEnrCommission

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("enr14_lc_template_comm_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_384d5e81"), 
				new DBColumn("enrollmentcommission_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("roleConfigLocalEnrCommission");

		}


    }
}