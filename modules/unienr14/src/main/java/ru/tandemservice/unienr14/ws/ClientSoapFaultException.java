/* $Id$ */
package ru.tandemservice.unienr14.ws;

import org.apache.cxf.binding.soap.SoapFault;
import org.apache.cxf.interceptor.Fault;

import javax.xml.namespace.QName;

/**
 * @author nvankov
 * @since 6/15/15
 */
public class ClientSoapFaultException extends SoapFault
{
    public static final String TANDEM_UNIVERSITY_ERROR_CODE_REQUIRED_DATA = "02";
    public static final String TANDEM_UNIVERSITY_ERROR_CODE_EXIST_DATA = "03";
    public static final String TANDEM_UNIVERSITY_ERROR_CODE_VALID_DATA = "04";
    public static final String TANDEM_UNIVERSITY_ERROR_CODE_OTHER = "05";

    public ClientSoapFaultException(String errorCode, String message)
    {
        super("TUEC: "+ errorCode +".\nDetails:\n" + message, Fault.FAULT_CODE_CLIENT);
    }
}
