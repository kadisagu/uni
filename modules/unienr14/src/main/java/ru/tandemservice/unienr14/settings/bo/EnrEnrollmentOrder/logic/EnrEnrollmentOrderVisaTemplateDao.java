/**
 *$Id: EnrEnrollmentOrderVisaTemplateDao.java 34429 2014-05-26 10:13:50Z oleyba $
 */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentOrder.logic;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentOrder.EnrEnrollmentOrderManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentOrder.ui.VisaTemplateAddEdit.OrderVisaItemsWrapper;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignOrderVisaItem;
import ru.tandemservice.unienr14.settings.entity.EnrVisasTemplate;
import ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 01.08.13
 */
public class EnrEnrollmentOrderVisaTemplateDao extends UniBaseDao implements IEnrEnrollmentOrderVisaTemplate
{
    @Override
    @Transactional
    public Map<GroupsMemberVising, List<EnrCampaignOrderVisaItem>> getOrderVisasByTemplate(EnrVisasTemplate visaTemplate)
    {
        final Map<GroupsMemberVising, List<EnrCampaignOrderVisaItem>> resultMap = new TreeMap<>(new EntityComparator<>(new EntityOrder(GroupsMemberVising.index().s()), new EntityOrder(GroupsMemberVising.id().s())));

        final List<EnrCampaignOrderVisaItem> visaList = new DQLSelectBuilder().fromEntity(EnrCampaignOrderVisaItem.class, "v").column(property("v"))
                .where(eq(property(EnrCampaignOrderVisaItem.visaTemplate().fromAlias("v")), value(visaTemplate)))
                .order(property(EnrCampaignOrderVisaItem.groupsMemberVising().index().fromAlias("v")))
                .order(property(EnrCampaignOrderVisaItem.priority().fromAlias("v")))
                .createStatement(getSession()).list();

        for (EnrCampaignOrderVisaItem visa : visaList)
            SafeMap.safeGet(resultMap, visa.getGroupsMemberVising(), ArrayList.class)
                    .add(visa);

        return resultMap;
    }

    @Override
    public void saveOrUpdateVisas(List<? extends IOrderVisaItemsWrapper> list, EnrVisasTemplate visaTemplate)
    {
        if (CollectionUtils.isEmpty(list))
            throw new ApplicationException(EnrEnrollmentOrderManager.instance().getProperty("visaTemplateAddEdit.validate.emptyException"));

        // если редактируем, то сначала надо все дропнуть, а потом создать, т.к. помержить возможности нет
        final List<EnrCampaignOrderVisaItem> visaList = new DQLSelectBuilder().fromEntity(EnrCampaignOrderVisaItem.class, "v").column(property("v"))
                .where(eq(property(EnrCampaignOrderVisaItem.visaTemplate().fromAlias("v")), value(visaTemplate)))
                .createStatement(getSession()).list();

        for (EnrCampaignOrderVisaItem visa : visaList)
            delete(visa);

        getSession().flush();

        saveOrUpdate(visaTemplate);

        for (IOrderVisaItemsWrapper wrapper : list)
        {
            for (OrderVisaItemsWrapper.PossibleVisaWrapper visaWrapper : wrapper.getVisaList())
            {
                final EnrCampaignOrderVisaItem item = new EnrCampaignOrderVisaItem();
                item.setGroupsMemberVising(wrapper.getGroup());
                item.setPossibleVisa(visaWrapper.getPossibleVisa());
                item.setPriority(visaWrapper.getPriority());
                item.setVisaTemplate(visaTemplate);

                save(item);
            }
        }
    }
}
