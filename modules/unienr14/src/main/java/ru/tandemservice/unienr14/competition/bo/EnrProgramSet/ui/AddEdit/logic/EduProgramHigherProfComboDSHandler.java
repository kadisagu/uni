/* $Id$ */
package ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.AddEdit.logic;

import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.program.entity.EduProgram;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.AddEdit.EnrProgramSetAddEdit;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 22.05.2015
 */
public class EduProgramHigherProfComboDSHandler extends EntityComboDataSourceHandler
{
    public EduProgramHigherProfComboDSHandler(String ownerId, Class<? extends IEntity> entityClass)
    {
        super(ownerId, entityClass);
        pageable(true);
    }

    public DQLSelectBuilder addWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
    {
        EnrEnrollmentCampaign campaign = context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
        EduProgramSubject programSubject = context.get(EnrProgramSetAddEdit.PARAM_SUBJECT);

        if (null == campaign)
            dql.where(nothing());
        else
            dql.where(eq(property(alias, EduProgram.year()), value(campaign.getEducationYear())));

        dql.where(eq(property(alias, EduProgramProf.programSubject()), value(programSubject)));

        return dql;
    }
}
