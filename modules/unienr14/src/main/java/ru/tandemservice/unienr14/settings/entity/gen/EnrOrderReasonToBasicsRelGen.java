package ru.tandemservice.unienr14.settings.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderBasic;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderReason;
import ru.tandemservice.unienr14.settings.entity.EnrOrderReasonToBasicsRel;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь причин с основаниями для приказов по абитуриентам
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrOrderReasonToBasicsRelGen extends EntityBase
 implements INaturalIdentifiable<EnrOrderReasonToBasicsRelGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.settings.entity.EnrOrderReasonToBasicsRel";
    public static final String ENTITY_NAME = "enrOrderReasonToBasicsRel";
    public static final int VERSION_HASH = 170803812;
    private static IEntityMeta ENTITY_META;

    public static final String L_REASON = "reason";
    public static final String L_BASIC = "basic";

    private EnrOrderReason _reason;     // Причина приказа по абитуриентам
    private EnrOrderBasic _basic;     // Основание приказа по абитуриентам

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Причина приказа по абитуриентам. Свойство не может быть null.
     */
    @NotNull
    public EnrOrderReason getReason()
    {
        return _reason;
    }

    /**
     * @param reason Причина приказа по абитуриентам. Свойство не может быть null.
     */
    public void setReason(EnrOrderReason reason)
    {
        dirty(_reason, reason);
        _reason = reason;
    }

    /**
     * @return Основание приказа по абитуриентам. Свойство не может быть null.
     */
    @NotNull
    public EnrOrderBasic getBasic()
    {
        return _basic;
    }

    /**
     * @param basic Основание приказа по абитуриентам. Свойство не может быть null.
     */
    public void setBasic(EnrOrderBasic basic)
    {
        dirty(_basic, basic);
        _basic = basic;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrOrderReasonToBasicsRelGen)
        {
            if (withNaturalIdProperties)
            {
                setReason(((EnrOrderReasonToBasicsRel)another).getReason());
                setBasic(((EnrOrderReasonToBasicsRel)another).getBasic());
            }
        }
    }

    public INaturalId<EnrOrderReasonToBasicsRelGen> getNaturalId()
    {
        return new NaturalId(getReason(), getBasic());
    }

    public static class NaturalId extends NaturalIdBase<EnrOrderReasonToBasicsRelGen>
    {
        private static final String PROXY_NAME = "EnrOrderReasonToBasicsRelNaturalProxy";

        private Long _reason;
        private Long _basic;

        public NaturalId()
        {}

        public NaturalId(EnrOrderReason reason, EnrOrderBasic basic)
        {
            _reason = ((IEntity) reason).getId();
            _basic = ((IEntity) basic).getId();
        }

        public Long getReason()
        {
            return _reason;
        }

        public void setReason(Long reason)
        {
            _reason = reason;
        }

        public Long getBasic()
        {
            return _basic;
        }

        public void setBasic(Long basic)
        {
            _basic = basic;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrOrderReasonToBasicsRelGen.NaturalId) ) return false;

            EnrOrderReasonToBasicsRelGen.NaturalId that = (NaturalId) o;

            if( !equals(getReason(), that.getReason()) ) return false;
            if( !equals(getBasic(), that.getBasic()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getReason());
            result = hashCode(result, getBasic());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getReason());
            sb.append("/");
            sb.append(getBasic());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrOrderReasonToBasicsRelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrOrderReasonToBasicsRel.class;
        }

        public T newInstance()
        {
            return (T) new EnrOrderReasonToBasicsRel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "reason":
                    return obj.getReason();
                case "basic":
                    return obj.getBasic();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "reason":
                    obj.setReason((EnrOrderReason) value);
                    return;
                case "basic":
                    obj.setBasic((EnrOrderBasic) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "reason":
                        return true;
                case "basic":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "reason":
                    return true;
                case "basic":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "reason":
                    return EnrOrderReason.class;
                case "basic":
                    return EnrOrderBasic.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrOrderReasonToBasicsRel> _dslPath = new Path<EnrOrderReasonToBasicsRel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrOrderReasonToBasicsRel");
    }
            

    /**
     * @return Причина приказа по абитуриентам. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrOrderReasonToBasicsRel#getReason()
     */
    public static EnrOrderReason.Path<EnrOrderReason> reason()
    {
        return _dslPath.reason();
    }

    /**
     * @return Основание приказа по абитуриентам. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrOrderReasonToBasicsRel#getBasic()
     */
    public static EnrOrderBasic.Path<EnrOrderBasic> basic()
    {
        return _dslPath.basic();
    }

    public static class Path<E extends EnrOrderReasonToBasicsRel> extends EntityPath<E>
    {
        private EnrOrderReason.Path<EnrOrderReason> _reason;
        private EnrOrderBasic.Path<EnrOrderBasic> _basic;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Причина приказа по абитуриентам. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrOrderReasonToBasicsRel#getReason()
     */
        public EnrOrderReason.Path<EnrOrderReason> reason()
        {
            if(_reason == null )
                _reason = new EnrOrderReason.Path<EnrOrderReason>(L_REASON, this);
            return _reason;
        }

    /**
     * @return Основание приказа по абитуриентам. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrOrderReasonToBasicsRel#getBasic()
     */
        public EnrOrderBasic.Path<EnrOrderBasic> basic()
        {
            if(_basic == null )
                _basic = new EnrOrderBasic.Path<EnrOrderBasic>(L_BASIC, this);
            return _basic;
        }

        public Class getEntityClass()
        {
            return EnrOrderReasonToBasicsRel.class;
        }

        public String getEntityName()
        {
            return "enrOrderReasonToBasicsRel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
