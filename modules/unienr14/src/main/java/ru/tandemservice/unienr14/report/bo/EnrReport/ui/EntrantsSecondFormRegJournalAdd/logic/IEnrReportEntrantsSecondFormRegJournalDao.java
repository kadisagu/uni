/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsSecondFormRegJournalAdd.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsSecondFormRegJournalAdd.EnrReportEntrantsSecondFormRegJournalAddUI;

/**
 * @author rsizonenko
 * @since 25.06.2014
 */
public interface IEnrReportEntrantsSecondFormRegJournalDao extends INeedPersistenceSupport {
    long createReport(EnrReportEntrantsSecondFormRegJournalAddUI model);
}
