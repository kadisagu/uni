package ru.tandemservice.unienr14.settings.entity;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.unienr14.exams.bo.EnrSchedule.EnrScheduleManager;
import ru.tandemservice.unienr14.settings.entity.gen.*;

/**
 * Аудитория для проведения ВИ
 */
public class EnrCampaignExamRoom extends EnrCampaignExamRoomGen
{
    @Override
    @EntityDSLSupport
    public String getPlaceLocationWithSize()
    {
        return EnrScheduleManager.instance().getProperty("enrCampaignExamRoom.placeLocationWithSize", getPlace().getTitleWithLocation(), getSize());
    }
}