package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x0_24to25 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrEntrantCustomState

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("enr14_entrant_custom_state_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("entrant_id", DBType.LONG).setNullable(false), 
				new DBColumn("customstate_id", DBType.LONG).setNullable(false), 
				new DBColumn("begindate_p", DBType.DATE), 
				new DBColumn("enddate_p", DBType.DATE), 
				new DBColumn("description_p", DBType.TEXT)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("enrEntrantCustomState");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrEntrantCustomStateType

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("enr14_c_custom_state_type_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("shorttitle_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("description_p", DBType.TEXT), 
				new DBColumn("htmlcolor_p", DBType.createVarchar(255)), 
				new DBColumn("title_p", DBType.createVarchar(1200))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("enrEntrantCustomStateType");

		}


    }
}