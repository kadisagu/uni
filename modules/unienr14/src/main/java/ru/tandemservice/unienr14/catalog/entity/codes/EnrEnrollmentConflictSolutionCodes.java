package ru.tandemservice.unienr14.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Вариант разрешения конфликтов зачисления"
 * Имя сущности : enrEnrollmentConflictSolution
 * Файл data.xml : unienr.catalog.data.xml
 */
public interface EnrEnrollmentConflictSolutionCodes
{
    /** Константа кода (code) элемента : Не рекомендовать (title) */
    String SKIP = "skip";
    /** Константа кода (code) элемента : Зачислить повторно (title) */
    String ENROLL = "enroll";
    /** Константа кода (code) элемента : Перезачислить (title) */
    String REENROLL = "reenroll";

    Set<String> CODES = ImmutableSet.of(SKIP, ENROLL, REENROLL);
}
