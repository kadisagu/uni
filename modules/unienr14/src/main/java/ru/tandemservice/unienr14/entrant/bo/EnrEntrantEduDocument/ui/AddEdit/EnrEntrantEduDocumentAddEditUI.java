/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrantEduDocument.ui.AddEdit;

import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.person.base.bo.PersonEduDocument.ui.AddEdit.PersonEduDocumentAddEditUI;
import org.tandemframework.tapsupport.component.selection.SingleSelectTextModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrantEduDocument.EnrEntrantEduDocumentManager;
import ru.tandemservice.unienr14.entrant.bo.EnrSubmittedEduDocument.EnrSubmittedEduDocumentManager;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantOriginalDocumentStatus;
import ru.tandemservice.unienr14.entrant.entity.EnrOrganizationOriginalIn;
import ru.tandemservice.unienr14.entrant.entity.gen.EnrEntrantOriginalDocumentStatusGen;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.EnrEntrantRequestManager;
import ru.tandemservice.unienr14.request.bo.EnrRequestedCompetition.EnrRequestedCompetitionManager;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;

import java.util.Date;

/**
 * @author oleyba
 * @since 5/5/14
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "document.id"),
    @Bind(key = PersonEduDocumentAddEditUI.BIND_PERSON, binding = "person.id", required = true),
    @Bind(key = EnrEntrantEduDocumentAddEditUI.BIND_ENTRANT, binding = "entrant.id", required = true)
})
public class EnrEntrantEduDocumentAddEditUI extends PersonEduDocumentAddEditUI
{
    public static final String BIND_ENTRANT = "entrantId";

    private EnrEntrant entrant = new EnrEntrant();

    private Date originalInDate;
    private Date originalOutDate;
    private String orgOriginalInTitle;
    private SingleSelectTextModel _organizationOriginalInModel = EnrSubmittedEduDocumentManager.instance().dao().getOrganizationOriginalInDS();

    private EnrRequestedCompetition _requestedCompetition;
    private boolean _showReqCompSelect = false;

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();

        setEntrant(IUniBaseDao.instance.get().get(EnrEntrant.class, getEntrant().getId()));
        if (getDocument().getId() != null) {
            EnrEntrantOriginalDocumentStatus existingOriginalStatus = IUniBaseDao.instance.get().getByNaturalId(new EnrEntrantOriginalDocumentStatusGen.NaturalId(getEntrant(), getDocument()));
            if (existingOriginalStatus != null) {
                setOriginalInDate(existingOriginalStatus.getRegistrationDate());
                setOriginalOutDate(existingOriginalStatus.getTakeAwayDate());
                setRequestedCompetition(existingOriginalStatus.getRequestedCompetition());
            }
            _showReqCompSelect = isEditForm() && EnrEntrantEduDocumentManager.instance().dao().isNeedReqCompForOriginal(getEntrant(), getDocument().getId());
            EnrOrganizationOriginalIn orgOriginalIn = DataAccessServices.dao().getByNaturalId(new EnrOrganizationOriginalIn.NaturalId(getEntrant(), getDocument()));
            orgOriginalInTitle = orgOriginalIn == null ? null : orgOriginalIn.getTitle();
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        switch (dataSource.getName())
        {
            case EnrEntrantEduDocumentAddEdit.DS_SETTLEMENT:
                dataSource.put(AddressItem.PARAM_COUNTRY, getCountry());
                break;
			case EnrRequestedCompetitionManager.DS_REQ_COMP_ORIGINAL:
				dataSource.put(EnrRequestedCompetitionManager.BIND_EDU_DOC_ID, getDocument().getId());
				dataSource.put(EnrRequestedCompetitionManager.BIND_ENTRANT, getEntrant());
				break;
        }
    }

    public void onClickApply() {
        if (validate()) return;
        EnrEntrantEduDocumentManager.instance().dao().saveOrUpdateEduDocument(getEntrant(), getDocument(), getScanCopyFile(), getOriginalInDate(), getOriginalOutDate(), getRequestedCompetition());
        EnrSubmittedEduDocumentManager.instance().dao().saveOrUpdateOrganizationOriginalIn(getEntrant(), getDocument(), getOrgOriginalInTitle());
        EnrEntrantRequestManager.instance().dao().doUpdateEnrReqCompEnrollAvailableAndEduDocForEntrant(getEntrant());
        deactivate();
    }

    @Override
    public boolean validate()
    {
        boolean hasErr = super.validate();
        if(isShowReqCompSelect() && originalInDate != null && (originalOutDate == null || originalInDate.after(originalOutDate)) && getRequestedCompetition() == null)
        {
            _uiSupport.error("Поле «Подан на» должно быть заполнено, если указана дата приема оригинала.", "originalInDate", "reqComp");
            hasErr = true;
        }

        if(isShowReqCompSelect() && originalInDate == null && getRequestedCompetition() != null)
        {
            _uiSupport.error("Поле «Подан на» не может быть заполнено, если не указана дата приема оригинала.", "originalInDate", "reqComp");
            hasErr = true;
        }

        return hasErr;
    }

    public Date getOriginalInDate()
    {
        return originalInDate;
    }

    public void setOriginalInDate(Date originalInDate)
    {
        this.originalInDate = originalInDate;
    }

    public Date getOriginalOutDate()
    {
        return originalOutDate;
    }

    public void setOriginalOutDate(Date originalOutDate)
    {
        this.originalOutDate = originalOutDate;
    }

    public EnrEntrant getEntrant()
    {
        return entrant;
    }

    public void setEntrant(EnrEntrant entrant)
    {
        this.entrant = entrant;
    }

    public String getOrgOriginalInTitle()
    {
        return orgOriginalInTitle;
    }

    public void setOrgOriginalInTitle(String orgOriginalInTitle)
    {
        this.orgOriginalInTitle = orgOriginalInTitle;
    }

    public SingleSelectTextModel getOrganizationOriginalInModel()
    {
        return _organizationOriginalInModel;
    }

    public EnrRequestedCompetition getRequestedCompetition()
    {
        return _requestedCompetition;
    }

    public void setRequestedCompetition(EnrRequestedCompetition requestedCompetition)
    {
        _requestedCompetition = requestedCompetition;
    }

    public boolean isShowReqCompSelect()
    {
        return _showReqCompSelect;
    }

    public void setShowReqCompSelect(boolean showReqCompSelect)
    {
        _showReqCompSelect = showReqCompSelect;
    }
}
