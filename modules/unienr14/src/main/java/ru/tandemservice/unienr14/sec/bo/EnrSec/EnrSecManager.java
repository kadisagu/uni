/* $Id:$ */
package ru.tandemservice.unienr14.sec.bo.EnrSec;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unienr14.sec.bo.EnrSec.logic.IEnrSecDao;
import ru.tandemservice.unienr14.sec.bo.EnrSec.logic.EnrSecDao;

/**
 * @author oleyba
 * @since 6/11/15
 */
@Configuration
public class EnrSecManager extends BusinessObjectManager
{
    public static EnrSecManager instance()
    {
        return instance(EnrSecManager.class);
    }

    @Bean
    public IEnrSecDao dao()
    {
        return new EnrSecDao();
    }
}
