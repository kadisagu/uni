package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantListAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;

/**
 * @author Alexander Shaburov
 * @since 25.11.13
 */
@Configuration
public class EnrReportEntrantListAdd extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
            .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
            .addAddon(uiAddon(CommonFilterAddon.class.getSimpleName(), EnrCompetitionFilterAddon.class))
            .create();
    }
}
