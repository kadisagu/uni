package ru.tandemservice.unienr14.entrant.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.person.catalog.entity.RelationDegree;
import ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantNextOfKin;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Ближайший родственник онлайн-абитуриента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrOnlineEntrantNextOfKinGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantNextOfKin";
    public static final String ENTITY_NAME = "enrOnlineEntrantNextOfKin";
    public static final int VERSION_HASH = -1058144650;
    private static IEntityMeta ENTITY_META;

    public static final String P_LASTNAME = "lastname";
    public static final String P_FIRSTNAME = "firstname";
    public static final String P_PATRONYMIC = "patronymic";
    public static final String P_EMPLOYMENT_PLACE = "employmentPlace";
    public static final String P_POST = "post";
    public static final String P_PHONES = "phones";
    public static final String L_RELATION_DEGREE = "relationDegree";
    public static final String L_ONLINE_ENTRANT = "onlineEntrant";

    private String _lastname;     // Фамилия
    private String _firstname;     // Имя
    private String _patronymic;     // Отчество
    private String _employmentPlace;     // Место работы
    private String _post;     // Должность
    private String _phones;     // Контактные телефоны
    private RelationDegree _relationDegree;     // Родственник
    private EnrOnlineEntrant _onlineEntrant;     // Абитуриент

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Фамилия.
     */
    @Length(max=255)
    public String getLastname()
    {
        return _lastname;
    }

    /**
     * @param lastname Фамилия.
     */
    public void setLastname(String lastname)
    {
        dirty(_lastname, lastname);
        _lastname = lastname;
    }

    /**
     * @return Имя.
     */
    @Length(max=255)
    public String getFirstname()
    {
        return _firstname;
    }

    /**
     * @param firstname Имя.
     */
    public void setFirstname(String firstname)
    {
        dirty(_firstname, firstname);
        _firstname = firstname;
    }

    /**
     * @return Отчество.
     */
    @Length(max=255)
    public String getPatronymic()
    {
        return _patronymic;
    }

    /**
     * @param patronymic Отчество.
     */
    public void setPatronymic(String patronymic)
    {
        dirty(_patronymic, patronymic);
        _patronymic = patronymic;
    }

    /**
     * @return Место работы.
     */
    @Length(max=255)
    public String getEmploymentPlace()
    {
        return _employmentPlace;
    }

    /**
     * @param employmentPlace Место работы.
     */
    public void setEmploymentPlace(String employmentPlace)
    {
        dirty(_employmentPlace, employmentPlace);
        _employmentPlace = employmentPlace;
    }

    /**
     * @return Должность.
     */
    @Length(max=255)
    public String getPost()
    {
        return _post;
    }

    /**
     * @param post Должность.
     */
    public void setPost(String post)
    {
        dirty(_post, post);
        _post = post;
    }

    /**
     * @return Контактные телефоны.
     */
    @Length(max=255)
    public String getPhones()
    {
        return _phones;
    }

    /**
     * @param phones Контактные телефоны.
     */
    public void setPhones(String phones)
    {
        dirty(_phones, phones);
        _phones = phones;
    }

    /**
     * @return Родственник. Свойство не может быть null.
     */
    @NotNull
    public RelationDegree getRelationDegree()
    {
        return _relationDegree;
    }

    /**
     * @param relationDegree Родственник. Свойство не может быть null.
     */
    public void setRelationDegree(RelationDegree relationDegree)
    {
        dirty(_relationDegree, relationDegree);
        _relationDegree = relationDegree;
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     */
    @NotNull
    public EnrOnlineEntrant getOnlineEntrant()
    {
        return _onlineEntrant;
    }

    /**
     * @param onlineEntrant Абитуриент. Свойство не может быть null.
     */
    public void setOnlineEntrant(EnrOnlineEntrant onlineEntrant)
    {
        dirty(_onlineEntrant, onlineEntrant);
        _onlineEntrant = onlineEntrant;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrOnlineEntrantNextOfKinGen)
        {
            setLastname(((EnrOnlineEntrantNextOfKin)another).getLastname());
            setFirstname(((EnrOnlineEntrantNextOfKin)another).getFirstname());
            setPatronymic(((EnrOnlineEntrantNextOfKin)another).getPatronymic());
            setEmploymentPlace(((EnrOnlineEntrantNextOfKin)another).getEmploymentPlace());
            setPost(((EnrOnlineEntrantNextOfKin)another).getPost());
            setPhones(((EnrOnlineEntrantNextOfKin)another).getPhones());
            setRelationDegree(((EnrOnlineEntrantNextOfKin)another).getRelationDegree());
            setOnlineEntrant(((EnrOnlineEntrantNextOfKin)another).getOnlineEntrant());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrOnlineEntrantNextOfKinGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrOnlineEntrantNextOfKin.class;
        }

        public T newInstance()
        {
            return (T) new EnrOnlineEntrantNextOfKin();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "lastname":
                    return obj.getLastname();
                case "firstname":
                    return obj.getFirstname();
                case "patronymic":
                    return obj.getPatronymic();
                case "employmentPlace":
                    return obj.getEmploymentPlace();
                case "post":
                    return obj.getPost();
                case "phones":
                    return obj.getPhones();
                case "relationDegree":
                    return obj.getRelationDegree();
                case "onlineEntrant":
                    return obj.getOnlineEntrant();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "lastname":
                    obj.setLastname((String) value);
                    return;
                case "firstname":
                    obj.setFirstname((String) value);
                    return;
                case "patronymic":
                    obj.setPatronymic((String) value);
                    return;
                case "employmentPlace":
                    obj.setEmploymentPlace((String) value);
                    return;
                case "post":
                    obj.setPost((String) value);
                    return;
                case "phones":
                    obj.setPhones((String) value);
                    return;
                case "relationDegree":
                    obj.setRelationDegree((RelationDegree) value);
                    return;
                case "onlineEntrant":
                    obj.setOnlineEntrant((EnrOnlineEntrant) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "lastname":
                        return true;
                case "firstname":
                        return true;
                case "patronymic":
                        return true;
                case "employmentPlace":
                        return true;
                case "post":
                        return true;
                case "phones":
                        return true;
                case "relationDegree":
                        return true;
                case "onlineEntrant":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "lastname":
                    return true;
                case "firstname":
                    return true;
                case "patronymic":
                    return true;
                case "employmentPlace":
                    return true;
                case "post":
                    return true;
                case "phones":
                    return true;
                case "relationDegree":
                    return true;
                case "onlineEntrant":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "lastname":
                    return String.class;
                case "firstname":
                    return String.class;
                case "patronymic":
                    return String.class;
                case "employmentPlace":
                    return String.class;
                case "post":
                    return String.class;
                case "phones":
                    return String.class;
                case "relationDegree":
                    return RelationDegree.class;
                case "onlineEntrant":
                    return EnrOnlineEntrant.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrOnlineEntrantNextOfKin> _dslPath = new Path<EnrOnlineEntrantNextOfKin>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrOnlineEntrantNextOfKin");
    }
            

    /**
     * @return Фамилия.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantNextOfKin#getLastname()
     */
    public static PropertyPath<String> lastname()
    {
        return _dslPath.lastname();
    }

    /**
     * @return Имя.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantNextOfKin#getFirstname()
     */
    public static PropertyPath<String> firstname()
    {
        return _dslPath.firstname();
    }

    /**
     * @return Отчество.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantNextOfKin#getPatronymic()
     */
    public static PropertyPath<String> patronymic()
    {
        return _dslPath.patronymic();
    }

    /**
     * @return Место работы.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantNextOfKin#getEmploymentPlace()
     */
    public static PropertyPath<String> employmentPlace()
    {
        return _dslPath.employmentPlace();
    }

    /**
     * @return Должность.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantNextOfKin#getPost()
     */
    public static PropertyPath<String> post()
    {
        return _dslPath.post();
    }

    /**
     * @return Контактные телефоны.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantNextOfKin#getPhones()
     */
    public static PropertyPath<String> phones()
    {
        return _dslPath.phones();
    }

    /**
     * @return Родственник. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantNextOfKin#getRelationDegree()
     */
    public static RelationDegree.Path<RelationDegree> relationDegree()
    {
        return _dslPath.relationDegree();
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantNextOfKin#getOnlineEntrant()
     */
    public static EnrOnlineEntrant.Path<EnrOnlineEntrant> onlineEntrant()
    {
        return _dslPath.onlineEntrant();
    }

    public static class Path<E extends EnrOnlineEntrantNextOfKin> extends EntityPath<E>
    {
        private PropertyPath<String> _lastname;
        private PropertyPath<String> _firstname;
        private PropertyPath<String> _patronymic;
        private PropertyPath<String> _employmentPlace;
        private PropertyPath<String> _post;
        private PropertyPath<String> _phones;
        private RelationDegree.Path<RelationDegree> _relationDegree;
        private EnrOnlineEntrant.Path<EnrOnlineEntrant> _onlineEntrant;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Фамилия.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantNextOfKin#getLastname()
     */
        public PropertyPath<String> lastname()
        {
            if(_lastname == null )
                _lastname = new PropertyPath<String>(EnrOnlineEntrantNextOfKinGen.P_LASTNAME, this);
            return _lastname;
        }

    /**
     * @return Имя.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantNextOfKin#getFirstname()
     */
        public PropertyPath<String> firstname()
        {
            if(_firstname == null )
                _firstname = new PropertyPath<String>(EnrOnlineEntrantNextOfKinGen.P_FIRSTNAME, this);
            return _firstname;
        }

    /**
     * @return Отчество.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantNextOfKin#getPatronymic()
     */
        public PropertyPath<String> patronymic()
        {
            if(_patronymic == null )
                _patronymic = new PropertyPath<String>(EnrOnlineEntrantNextOfKinGen.P_PATRONYMIC, this);
            return _patronymic;
        }

    /**
     * @return Место работы.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantNextOfKin#getEmploymentPlace()
     */
        public PropertyPath<String> employmentPlace()
        {
            if(_employmentPlace == null )
                _employmentPlace = new PropertyPath<String>(EnrOnlineEntrantNextOfKinGen.P_EMPLOYMENT_PLACE, this);
            return _employmentPlace;
        }

    /**
     * @return Должность.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantNextOfKin#getPost()
     */
        public PropertyPath<String> post()
        {
            if(_post == null )
                _post = new PropertyPath<String>(EnrOnlineEntrantNextOfKinGen.P_POST, this);
            return _post;
        }

    /**
     * @return Контактные телефоны.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantNextOfKin#getPhones()
     */
        public PropertyPath<String> phones()
        {
            if(_phones == null )
                _phones = new PropertyPath<String>(EnrOnlineEntrantNextOfKinGen.P_PHONES, this);
            return _phones;
        }

    /**
     * @return Родственник. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantNextOfKin#getRelationDegree()
     */
        public RelationDegree.Path<RelationDegree> relationDegree()
        {
            if(_relationDegree == null )
                _relationDegree = new RelationDegree.Path<RelationDegree>(L_RELATION_DEGREE, this);
            return _relationDegree;
        }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantNextOfKin#getOnlineEntrant()
     */
        public EnrOnlineEntrant.Path<EnrOnlineEntrant> onlineEntrant()
        {
            if(_onlineEntrant == null )
                _onlineEntrant = new EnrOnlineEntrant.Path<EnrOnlineEntrant>(L_ONLINE_ENTRANT, this);
            return _onlineEntrant;
        }

        public Class getEntityClass()
        {
            return EnrOnlineEntrantNextOfKin.class;
        }

        public String getEntityName()
        {
            return "enrOnlineEntrantNextOfKin";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
