/* $Id$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantForeignRequest.ui.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.resolver.DefaultLinkResolver;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantCustomStateType;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.formatter.EnrEntrantCustomStateCollectionFormatter;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.Pub.EnrEntrantPub;
import ru.tandemservice.unienr14.request.bo.EnrEntrantForeignRequest.logic.EnrEntrantForeignRequestDSHandler;
import ru.tandemservice.unienr14.request.entity.EnrEntrantForeignRequest;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;

/**
 * @author nvankov
 * @since 7/15/14
 */
@Configuration
public class EnrEntrantForeignRequestList extends BusinessComponentManager
{
    public static final String FOREIGN_REQ_DS = "foreignRequestDS";
    public static final String ENTRANT_CUSTOM_STATE_DS = "entrantCustomStateDS";
    public static final String PARAM_ENR_COMPETITION_UTIL = EnrCompetitionFilterAddon.class.getSimpleName();

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
                .addDataSource(searchListDS(FOREIGN_REQ_DS, foreignRequestDSColumns(), foreignRequestDSHandler()))
                .addDataSource(selectDS(ENTRANT_CUSTOM_STATE_DS, entrantCustomStateDSHandler()))
                .addAddon(uiAddon(CommonFilterAddon.class.getSimpleName(), EnrCompetitionFilterAddon.class))
                .create();
    }

    @Bean
    public ColumnListExtPoint foreignRequestDSColumns()
    {
        return columnListExtPointBuilder(FOREIGN_REQ_DS)
                .addColumn(publisherColumn("entrant", EnrEntrantForeignRequest.entrant().person().identityCard().fullFio())
                        .publisherLinkResolver(
                                new IPublisherLinkResolver()
                                {
                                    @Override
                                    public Object getParameters(IEntity entity)
                                    {
                                        return new ParametersMap().add("selectedTab", "foreignRequestTab").add(PublisherActivator.PUBLISHER_ID_KEY, ((ViewWrapper<EnrEntrantForeignRequest>) entity).getEntity().getEntrant().getId());
                                    }

                                    @Override
                                    public String getComponentName(IEntity entity)
                                    {
                                        return EnrEntrantPub.class.getSimpleName();
                                    }
                                }

                        )
                        .required(true).order())
                .addColumn(textColumn("requestNumber", EnrEntrantForeignRequest.regNumber()).order().required(true))
                .addColumn(textColumn("competition", EnrEntrantForeignRequest.competition().title()).required(true))
                .addColumn(textColumn("customState", EnrEntrantForeignRequestDSHandler.ENTRANT_CUSTOM_STATE_PROP).formatter(new EnrEntrantCustomStateCollectionFormatter()))
                .addColumn(textColumn("sex", EnrEntrantForeignRequest.entrant().person().identityCard().sex().shortTitle()))
                .addColumn(textColumn("passport", EnrEntrantForeignRequest.entrant().person().fullIdentityCardNumber()))
                .addColumn(textColumn("birthDate", EnrEntrantForeignRequest.entrant().person().identityCard().birthDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER))
                .addColumn(textColumn("regDate", EnrEntrantForeignRequest.regDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order())
                .addColumn(textColumn("enrollDate", EnrEntrantForeignRequest.enrollmentDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER))
                .addColumn(textColumn("comment", EnrEntrantForeignRequest.comment()).width("200px").formatter(new IFormatter<String>()
                {
                    @Override
                    public String format(String source)
                    {
                        if (!StringUtils.isEmpty(source) && source.length() > 200)
                        {
                            return source.substring(0, 199) + "...";
                        }
                        else
                            return source;
                    }
                }))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).permissionKey("enr14EntrantForeignRequestListEdit"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER).permissionKey("enr14EntrantForeignRequestListDelete").alert(alert("Удалить направление?")))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler foreignRequestDSHandler()
    {
        return new EnrEntrantForeignRequestDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler entrantCustomStateDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrEntrantCustomStateType.class)
                .order(EnrEntrantCustomStateType.title())
                .filter(EnrEntrantCustomStateType.title());
    }
}



    