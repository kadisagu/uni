/**
 *$Id: IEnrEnrollmentOrderVisaTemplate.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentOrder.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentOrder.ui.VisaTemplateAddEdit.OrderVisaItemsWrapper;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignOrderVisaItem;
import ru.tandemservice.unienr14.settings.entity.EnrVisasTemplate;
import ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising;

import java.util.List;
import java.util.Map;

/**
 * @author Alexander Shaburov
 * @since 01.08.13
 */
public interface IEnrEnrollmentOrderVisaTemplate extends INeedPersistenceSupport
{
    /**
     * Поднимает Визы для приказа о зачислении указанного Шаблона.
     * @param visaTemplate Шаблон визирования
     * @return сгруппированные по Группам Визы, notNull
     */
    Map<GroupsMemberVising, List<EnrCampaignOrderVisaItem>> getOrderVisasByTemplate(EnrVisasTemplate visaTemplate);

    /**
     * По указанному списку врапперов создает Визы для приказа о зачислении.
     * В случае редактирования существующие визы для шаблона удаляются и создаются новые, по указанным данным.
     * @param list список врапперов
     * @param visaTemplate Шаблон визирования
     */
    void saveOrUpdateVisas(List<? extends IOrderVisaItemsWrapper> list, EnrVisasTemplate visaTemplate);

    /** Обертка Виз для приказа о зачислении одной группы. */
    public interface IOrderVisaItemsWrapper
    {
        /** Группа участников визирования элемента. */
        GroupsMemberVising getGroup();
        /** Визы элемента с указанной группой. */
        List<OrderVisaItemsWrapper.PossibleVisaWrapper> getVisaList();
    }
}
