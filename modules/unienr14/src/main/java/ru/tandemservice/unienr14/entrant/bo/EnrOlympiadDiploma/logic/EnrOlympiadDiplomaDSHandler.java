/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrOlympiadDiploma.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.entity.PersonDocumentRoleRel;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 5/3/13
 */
public class EnrOlympiadDiplomaDSHandler extends DefaultSearchDataSourceHandler
{
    public static final String BIND_ENTRANT = "entrant";

    public EnrOlympiadDiplomaDSHandler(String ownerId)
    {
        super(ownerId, EnrOlympiadDiploma.class);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final EnrEntrant enrCamp = context.get(BIND_ENTRANT);

        final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrOlympiadDiploma.class, "g").column(property("g"))
                .where(exists(new DQLSelectBuilder()
                        .fromEntity(PersonDocumentRoleRel.class, "rr")
                        .where(eq(property("rr", PersonDocumentRoleRel.document()), property("g")))
                        .where(eq(property("rr", PersonDocumentRoleRel.role()), value(enrCamp)))
                        .buildQuery()));

        final DSOutput output = DQLSelectOutputBuilder.get(input, builder, context.getSession()).order().preserveCountRecord().build();

        if (output.getRecordIds().size() < 5)
            output.setCountRecord(5);

        return output;
    }
}

