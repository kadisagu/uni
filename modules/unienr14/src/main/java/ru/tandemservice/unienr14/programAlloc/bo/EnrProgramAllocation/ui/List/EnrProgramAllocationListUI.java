/* $Id: EcProfileAllocationListUI.java 25114 2012-12-03 11:25:11Z vdanilov $ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.ui.List;

import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.PageableSearchListDataSource;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.EnrProgramAllocationManager;
import ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.logic.EnrProgramAllocationDSHandler;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Vasily Zhukov
 * @since 10.05.2012
 */
public class EnrProgramAllocationListUI extends UIPresenter
{
    private EnrEnrollmentCampaign _enrollmentCampaign;

    @Override
    public void onComponentRefresh()
    {
        setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
    }


    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, getEnrollmentCampaign());
    }

    //

    public Object getQuota()
    {
        DynamicListDataSource ds = ((PageableSearchListDataSource) _uiConfig.getDataSource(EnrProgramAllocationList.ALLOCATION_DS)).getLegacyDataSource();
        return ds.getCurrentEntity().getProperty(EnrProgramAllocationDSHandler.VIEW_PROP_QUOTA);
    }

    // Listeners

    public void onClickShow()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
        saveSettings();
    }

    public void onClickClear()
    {
        clearSettings();
        saveSettings();
    }

    public void onClickBulkMake()
    {
        _uiSupport.setRefreshScheduled(true);
        EnrProgramAllocationManager.instance().dao().doBulkMakeAllocation(getSelectedSet());
        clearSelectedSet();
    }

    public void onClickBulkFill()
    {
        _uiSupport.setRefreshScheduled(true);
        EnrProgramAllocationManager.instance().dao().doBulkFillAllocation(getSelectedSet());
        clearSelectedSet();
    }

    public void onClickBulkCreateOrders()
    {
        _uiSupport.setRefreshScheduled(true);
        int count = EnrProgramAllocationManager.instance().dao().doBulkCreateAllocationOrders(getSelectedSet());
        clearSelectedSet();
        _uiSupport.info("Сформировано приказов: " + count);
    }

    public void onClickBulkDelete()
    {
        _uiSupport.setRefreshScheduled(true);
        EnrProgramAllocationManager.instance().dao().doBulkDeleteAllocation(getSelectedSet());
        clearSelectedSet();
    }

    public void onClickAdd()
    {
        _uiSupport.setRefreshScheduled(true);
        EnrProgramAllocationManager.instance().dao().doCreateAllocation(getListenerParameterAsLong());
    }

    public void onDeleteEntityFromList()
    {
        _uiSupport.setRefreshScheduled(true);
        EnrProgramAllocationManager.instance().dao().deleteAllocation(getListenerParameterAsLong());
    }

    public void onEnrCampaignRefresh()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
    }

    // Private

    private Set<Long> getSelectedSet()
    {
        DynamicListDataSource ds = ((PageableSearchListDataSource) _uiConfig.getDataSource(EnrProgramAllocationList.ALLOCATION_DS)).getLegacyDataSource();
        CheckboxColumn checkboxColumn = ((CheckboxColumn) ds.getColumn("checkbox"));

        return new HashSet<>(CollectionUtils.collect(checkboxColumn.getSelectedObjects(), IEntity::getId));
    }

    private void clearSelectedSet()
    {
        ((CheckboxColumn) (((BaseSearchListDataSource) _uiConfig.getDataSource(EnrProgramAllocationList.ALLOCATION_DS)).getLegacyDataSource()).getColumn("checkbox")).getSelectedObjects().clear();
    }

    // getters and setters


    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }

    public boolean isNothingSelected(){ return null == getEnrollmentCampaign(); }
}
