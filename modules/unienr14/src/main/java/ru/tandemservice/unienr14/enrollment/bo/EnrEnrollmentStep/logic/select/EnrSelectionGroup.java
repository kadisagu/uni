/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.select;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;

import java.util.*;

/**
* @author oleyba
* @since 4/2/15
*/ // конкурс (с подвидами, если требуется) в шаге зачисления
public class EnrSelectionGroup
{
    private EnrSelectionContext context;

    // ключ группы (определяет конкурс и вид ЦП, если требуется)
    private final EnrSelectionContext.GroupKey key;

    // план данной группы (с учетом всех настроек)
    private int competitionPlan = 0;

    // абитуриенты в шаге зачисления (все, включая выбывших, распределенных, зачисленных), которые поступают на данную групп
    private final Map<EnrEntrant, EnrSelectionItem> itemMap = new LinkedHashMap<>(); // отсортированы по позиции
    private final Map<EnrEntrant, EnrSelectionItem> itemMapRo = Collections.unmodifiableMap(this.itemMap);

    // все неотмененные выписки о зачислении на данную группу (на момент формирования шага зачисления)
    private final Map<EnrEntrant, IEnrSelectionPrevEnrollmentFact> competitionExtractMap = new LinkedHashMap<>();
    private final Map<EnrEntrant, IEnrSelectionPrevEnrollmentFact> competitionExtractMapRo = Collections.unmodifiableMap(this.competitionExtractMap);

    // перечень выписок, места в которых были освобождены в результате выбора абитуриентом другой группы
    private final Map<IEnrSelectionItemPrevEnrollmentInfo, EnrSelectionItem> reenrollMap = new HashMap<>();

    public EnrSelectionGroup(final EnrSelectionContext.GroupKey key, EnrSelectionContext context) {
        this.key = key;
        this.context = context;
    }

    public EnrSelectionContext.GroupKey getKey() { return this.key; }
    public EnrSelectionContext getContext() { return context; }
    public Map<EnrEntrant, EnrSelectionItem> getItemMap() { return this.itemMapRo; }
    public Map<EnrEntrant, IEnrSelectionPrevEnrollmentFact> getCompetitionExtractMap() { return this.competitionExtractMapRo; }

    public int getCompetitionPlan() { return this.competitionPlan; }
    public int getCompetitionExtractSize() { return this.getCompetitionExtractMap().size(); }

    public Collection<EnrSelectionGroup> getOtherStepItemGroups() {
        return Collections2.filter(context.getStepItemGroupMap().values(), otherGroup -> !EnrSelectionGroup.this.equals(otherGroup));
    }

    // список выписок данного конкурса, для которых есть EnrSelectionItem другой группы того же абитуриента с тем же признаком parallel, отобрынный по условию
    /** return { EnrEnrollmentExtract my_e | exists (EnrSelectionItem w | w.entrant = my_e.entrant, w.parallel = my_e.parallel, otherItemPrdicate(w) = true) }*/
    public Collection<IEnrSelectionPrevEnrollmentFact> getCompetitionExtractUsedByOtherGroups(final Predicate<EnrSelectionItem> otherItemPrdicate) {
        return Collections2.filter(this.getCompetitionExtractMap().values(), myExtract -> {
            final EnrRequestedCompetition myRequestedCompetition = myExtract.getRequestedCompetition();
            final Collection<EnrSelectionItem> otherGroupItems = context.getStepItemWrappers4OtherGroups(myRequestedCompetition.getRequest().getEntrant(), EnrSelectionGroup.this);
            for (final EnrSelectionItem otherItem: otherGroupItems) {
                if (Objects.equals(
                    myRequestedCompetition.isParallel(),
                    otherItem.getRequestedCompetition().isParallel()
                )) {
                    if (otherItemPrdicate.apply(otherItem)) { return true; }
                }
            }
            return false;
        });
    }

    // список EnrSelectionItem конкурса, для которых есть EnrSelectionItem другой группы того же абитуриента с тем же признаком parallel, отобрынный по условию
    /** return { EnrSelectionItem my_w | allowed(my_w), exists (EnrSelectionItem w | w.entrant = my_w.entrant, w.parallel = my_w.parallel, otherItemPrdicate(w) = true) }*/
    public Collection<EnrSelectionItem> getAllowdStepItemsUsedByOtherGroups(final Predicate<EnrSelectionItem> otherItemPrdicate) {
        return Collections2.filter(this.getItemMap().values(), mySelectionItem -> {
            if (!context.isSelectionAllowed(mySelectionItem)) { return false; }
            final Collection<EnrSelectionItem> otherGroupItems = context.getStepItemWrappers4OtherGroups(mySelectionItem.getEntrant(), EnrSelectionGroup.this);
            for (final EnrSelectionItem otherItem: otherGroupItems) {
                if (Objects.equals(
                    mySelectionItem.getRequestedCompetition().isParallel(),
                    otherItem.getRequestedCompetition().isParallel()
                )) {
                    if (otherItemPrdicate.apply(otherItem)) { return true; }
                }
            }
            return false;
        });
    }

    // действие: зарегистрировать информацию о существующей выписке (о зачислении на данный конкурс)
    public void registerExtract(final IEnrSelectionPrevEnrollmentFact extract) {
        final IEnrSelectionPrevEnrollmentFact prev = this.competitionExtractMap.put(extract.getRequestedCompetition().getRequest().getEntrant(), extract);
        if (null != prev && !extract.equals(prev) && context.isCheckPrevEnrollmentDuplicates()) {
            throw new IllegalStateException("duplicate-entrant: groupKey="+this.getKey().toString()+", entrantId="+extract.getRequestedCompetition().getRequest().getEntrant().getId()+", extract="+extract.getId()+","+prev.getId());
        }
    }

    // действие: устанавливает план
    public void setupPlan(final int competitionPlan) {
        this.competitionPlan = competitionPlan;
    }

    // действие: добавляет абутириента в шаге зачисления
    public EnrSelectionItem add(final IEnrEnrollmentStepItem stepItem)
    {
        final EnrSelectionItem wrapper = new EnrSelectionItem(this, stepItem);
        if (null != this.itemMap.put(wrapper.getEntrant(), wrapper)) {
            throw new IllegalStateException("duplicate-entrant: groupKey="+this.getKey().toString()+", entrantId="+wrapper.getEntrant().getId()+", stepItem="+stepItem.getId());
        }
        return wrapper;
    }

    // действие: освободить вакантное место текущей группы (т.к. абитуриент выбран в другой)
    public boolean reenroll(final IEnrSelectionItemPrevEnrollmentInfo orderInfo, final EnrSelectionItem selectionItem)
    {
        if (this.equals(selectionItem.getGroup())) {
            if (orderInfo.isCancelled()) {
                // сюрприз, такое может быть - когда приказ на выбранный конкурс отозванного заявления
                return false; // сами себя мы не скипаем
            }
            throw new IllegalStateException("reenroll-from-same-competition: " + selectionItem.getEntrantFio() + ", " + orderInfo.getInfo()+" -> " + selectionItem.getCompetitionTitle());
        }

        // ищем здесь выписку на того же абитуриента с тем же признаком параллельности (если нет - это не наш клиент - место не освобождаем)
        if (!Objects.equals(orderInfo.getRequestedCompetition().isParallel(), selectionItem.getRequestedCompetition().isParallel())) { return false; }

        // освобождаем место
        final EnrSelectionItem prev = this.reenrollMap.put(orderInfo, selectionItem);
        if (null != prev && !selectionItem.equals(prev)) {
            throw new IllegalStateException("reenroll-not-same-step-item");
        }

        // помечаем абитуриента в данной группе как исключенного (место же освободили)
        final EnrSelectionItem deadWrapper = this.getItemMap().get(selectionItem.getEntrant());
        if (null != deadWrapper) {
            deadWrapper.skip(new EnrSelectionSkipReason.OrderInfoSkipReenrollSolution(orderInfo, selectionItem));
        }

        // если место освободили (первый раз)
        return (null == prev);
    }

    // перечень выбранных абитуриентов
    public List<EnrSelectionItem> getSelectedItems() {
        return new ArrayList<>(Collections2.filter(this.getItemMap().values(), EnrSelectionContext.SELECTED_ITEMS_PREDICATE));
    }

    // число выбранных элементов getSelectedItems().size()
    public int getSelectedItemCount() {
        return Collections2.filter(this.getItemMap().values(), EnrSelectionContext.SELECTED_ITEMS_PREDICATE).size();
    }

    // число выбранных и предвыбранных
    public long getPreAndSelectedEntrantCount() {
        return Collections2.filter(this.getItemMap().values(), input -> {
            if (input.isSkipped()) { return false; /* и не важно, предвыбран ли он */ }
            if (input.isSelected()) { return true; }
            return (null != context.getPreSelectedStepItems()) && (context.getPreSelectedStepItems().contains(input));
        }).size();
    }

    // число выписок на зачисление абитуриентов в данный конкурс за вычитом перезачисленных и предвыбранных в другие конкурсы (с тем же признаком параллельности)
    public long getNonCoveredCompetitionExtractCount() {

        // берем все выписки на зачисление на данный конкурс
        final Map<EnrEntrant, IEnrSelectionPrevEnrollmentFact> extractMap = new HashMap<>(this.getCompetitionExtractMap());

        // вычитаем всех перезачисленных с данного конкурса на другой
        for (final IEnrSelectionItemPrevEnrollmentInfo orderInfo: this.reenrollMap.keySet()) {
            extractMap.remove(orderInfo.getRequestedCompetition().getRequest().getEntrant());
        }

        // убираем всех предвыбранных в другие конкурсы (если человек предвыбран где-то не здесь, то его текущее место освобождается)
        return Collections2.filter(extractMap.entrySet(), e -> {
            if (null != context.getPreSelectedStepItems()) {
                for (final EnrSelectionItem preSelectedStepItem: context.getPreSelectedStepItems()) {
                    if (!preSelectedStepItem.getEntrant().equals(e.getKey())) { continue; }
                    if (Objects.equals(
                        preSelectedStepItem.getRequestedCompetition().isParallel(),
                        e.getValue().getRequestedCompetition().isParallel()
                    )) {
                        return false;
                    }
                }
            }
            return true;
        }).size();
    }

}
