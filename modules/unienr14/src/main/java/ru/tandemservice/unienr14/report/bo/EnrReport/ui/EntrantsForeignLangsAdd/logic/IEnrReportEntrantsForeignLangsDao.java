/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsForeignLangsAdd.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsForeignLangsAdd.EnrReportEntrantsForeignLangsAddUI;

/**
 * @author rsizonenko
 * @since 30.05.2014
 */
public interface IEnrReportEntrantsForeignLangsDao extends INeedPersistenceSupport {
    Long createReport(EnrReportEntrantsForeignLangsAddUI enrReportEntrantsForeignLangsAddUI);
}
