/**
 *$Id: EnrExamGroupListExamSelectDSHandler.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.exams.bo.EnrExamPassDiscipline.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unienr14.exams.bo.EnrExamPassDiscipline.ui.List.EnrExamPassDisciplineListUI;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 29.05.13
 */
public class EnrExamPassDisciplineExamSelectDSHandler extends DefaultComboDataSourceHandler
{
    public EnrExamPassDisciplineExamSelectDSHandler(String ownerId)
    {
        super(ownerId, DataWrapper.class);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final EnrEnrollmentCampaign enrCamp = context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
        final OrgUnit terrOrgUnit = context.get(EnrExamPassDisciplineListUI.SETTING_ORG_UNIT);

        final DQLSelectBuilder subQuery = new DQLSelectBuilder().fromEntity(EnrExamPassDiscipline.class, "pd")
                .column(DQLFunctions.min(property("pd", "id")))
                .where(eq(property("pd", EnrExamPassDiscipline.discipline().enrollmentCampaign()), value(enrCamp)))
                .where(eq(property("pd",EnrExamPassDiscipline.territorialOrgUnit()), value(terrOrgUnit)))
                .group(property("pd", EnrExamPassDiscipline.discipline().id()))
                .group(property("pd", EnrExamPassDiscipline.passForm().id()));

        FilterUtils.applyLikeFilter(
                subQuery, input.getComboFilterByValue(),
                EnrExamPassDiscipline.discipline().discipline().title().fromAlias("pd"),
                EnrExamPassDiscipline.passForm().title().fromAlias("pd")
        );

        final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EnrExamPassDiscipline.class, "e")
                .column(property("e", "id"))
                .column(property("e", EnrExamPassDiscipline.discipline().id()))
                .column(property("e", EnrExamPassDiscipline.passForm().id()))
                .column(property("e", EnrExamPassDiscipline.discipline().discipline().title()))
                .column(property("e", EnrExamPassDiscipline.passForm().title()))
                .where(in(property("e", "id"), subQuery.buildQuery()))
                .order(property("e", EnrExamPassDiscipline.discipline().discipline().title()))
                .order(property("e", EnrExamPassDiscipline.passForm().title()));

        FilterUtils.applySelectFilter(dql, "e", EnrExamPassDiscipline.id(), input.getPrimaryKeys());

        return DQLSelectOutputBuilder.get(input, dql, context.getSession()).pageable(false).build().transform(
                (Object[] item) -> new DisciplineAndPassFormWrapper((Long) item[0], item[3] + " (" + item[4] + ")", (Long) item[1], (Long) item[2]));
    }
}
