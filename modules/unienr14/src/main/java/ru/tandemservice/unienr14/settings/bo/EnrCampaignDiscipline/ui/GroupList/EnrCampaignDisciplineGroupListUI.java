/**
 *$Id: EnrCampaignDisciplineGroupListUI.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.settings.bo.EnrCampaignDiscipline.ui.GroupList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unienr14.settings.bo.EnrCampaignDiscipline.ui.GroupAddEdit.EnrCampaignDisciplineGroupAddEdit;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

/**
 * @author Alexander Shaburov
 * @since 09.04.13
 */
public class EnrCampaignDisciplineGroupListUI extends UIPresenter
{
    @Override
    public void onComponentRefresh()
    {
        getSettings().set(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
    }

    public void onClickAddCompDisciplineGroup()
    {
        getActivationBuilder().asRegionDialog(EnrCampaignDisciplineGroupAddEdit.class)
                .activate();
    }

    public void onClickEditCompDisciplineGroup()
    {
        getActivationBuilder().asRegionDialog(EnrCampaignDisciplineGroupAddEdit.class)
                .parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong())
                .activate();
    }

    public void onClickDeleteCompDisciplineGroup()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public void onEnrCampaignRefresh()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getSettings().<EnrEnrollmentCampaign>get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN));
    }

    public boolean isNothingSelected()
    {
        return getSettings().get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN) == null;
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, getSettings().get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN));
    }
}
