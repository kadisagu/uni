/* $Id:$ */
package ru.tandemservice.unienr14.competition.bo.EnrProgramSet.logic;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.hibernate.Session;
import org.tandemframework.core.common.INaturalId;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.EnrCompetitionManager;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.logic.IEnrCompetitionDao;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.util.EnrProgramSetOrgUnitWrapper;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.util.EnrTargetAdmissionPlanWrapper;
import ru.tandemservice.unienr14.competition.entity.*;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.EnrExamSetManager;
import ru.tandemservice.unienr14.exams.entity.EnrExamSet;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignTargetAdmissionKind;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 2/3/14
 */
public class EnrProgramSetDao extends UniBaseDao implements IEnrProgramSetDao
{
    @Override
    public void saveOrUpdateProgramSet(EnrProgramSetBase programSet, List<EduProgramHigherProf> programList, List<EnrProgramSetOrgUnitWrapper> rowList)
    {
        if (programSet.getId() == null)
        {
            if (programSet instanceof EnrProgramSetBS)
                save(programSet);

            else
                doBindEmptyExamSet(programSet);

        } else
            update(programSet);

        if (!(programSet instanceof EnrProgramSetSecondary))
            doMergeProgramSetEduPrograms(programSet, programList);

        doMergeProgramSetOrgUnits(programSet, rowList);
    }

    /** Указывает специальности СПО, наборам ОП магистратуры и аспиранутры пустой набор ВИ. */
    private void doBindEmptyExamSet(EnrProgramSetBase programSet)
    {
        if (!(programSet instanceof EnrProgramSetSecondary || programSet instanceof EnrProgramSetMaster || programSet instanceof EnrProgramSetHigher))
            throw new IllegalArgumentException(); // настроить пустой набор ВИ можно только для СПО, магистров и кадров высшей квалификации

        Session session = this.getSession();
        EnrExamSet examSet = EnrExamSetManager.instance().dao().doGetEmptySet(programSet.getEnrollmentCampaign());
        EnrExamSetVariant variant = new EnrExamSetVariant();
        variant.setExamSet(examSet);
        save(variant);
        session.flush();

        if (programSet instanceof EnrProgramSetSecondary)
        {
            EnrProgramSetSecondary secondary = (EnrProgramSetSecondary) programSet;
            secondary.setExamSetVariant(variant);

        } else if (programSet instanceof EnrProgramSetMaster)
        {
            EnrExamSetVariant variantCon = new EnrExamSetVariant();
            variantCon.setExamSet(examSet);
            save(variantCon);
            session.flush();

            EnrProgramSetMaster master = (EnrProgramSetMaster) programSet;
            master.setExamSetVariantMin(variant);
            master.setExamSetVariantCon(variantCon);
            master.setExamSetVariant(variant);

        } else
        {
            EnrProgramSetHigher higher = (EnrProgramSetHigher) programSet;
            higher.setExamSetVariant(variant);
        }

        saveOrUpdate(programSet);
        session.flush();

        variant.setOwner(programSet);
        update(variant);
        session.flush();

    }

    private void doMergeProgramSetEduPrograms(final EnrProgramSetBase programSet, List<EduProgramHigherProf> programList)
    {
        new MergeAction.SessionMergeAction<EduProgramHigherProf, EnrProgramSetItem>()
        {
            @Override protected EduProgramHigherProf key(final EnrProgramSetItem source) { return source.getProgram(); }
            @Override protected void fill(final EnrProgramSetItem target, final EnrProgramSetItem source) { target.update(source, false); }
            @Override protected EnrProgramSetItem buildRow(final EnrProgramSetItem source) { return new EnrProgramSetItem(programSet, source.getProgram()); }
        }
        .merge(
            this.getList(EnrProgramSetItem.class, EnrProgramSetItem.programSet(), programSet),
            CollectionUtils.collect(programList, program -> new EnrProgramSetItem(programSet, program))
        );
    }

    private void doMergeProgramSetOrgUnits(final EnrProgramSetBase programSet, List<EnrProgramSetOrgUnitWrapper> rowList)
    {
        new MergeAction.SessionMergeAction<Long, EnrProgramSetOrgUnit>()
        {
            @Override protected Long key(final EnrProgramSetOrgUnit source) { return source.getOrgUnit().getId(); }
            @Override protected void fill(final EnrProgramSetOrgUnit target, final EnrProgramSetOrgUnit source) { target.update(source, false); }
            @Override protected EnrProgramSetOrgUnit buildRow(final EnrProgramSetOrgUnit source) { return new EnrProgramSetOrgUnit(source.getProgramSet(), source.getOrgUnit()); }
        }
        .merge(
            this.getList(EnrProgramSetOrgUnit.class, EnrProgramSetOrgUnit.programSet(), programSet),
            CollectionUtils.collect(rowList, wrapper -> wrapper.createEntity(programSet))
        );
    }

    @Override
    public void deleteProgramSet(Long programSetId)
    {
        if (existsEntity(EnrCompetition.class, EnrCompetition.programSetOrgUnit().programSet().s(), programSetId))
        {
            String errorMessageProgramSet = "По набору образовательных программ уже открыты конкурсы. Чтобы удалить набор, предварительно удалите созданные конкурсы в рамках этого набора.";
            String errorMessageProgramSetSecondary = "По специальности уже открыты конкурсы. Чтобы удалить специальность, предварительно удалите созданные конкурсы в рамках этой специальности.";
            throw new ApplicationException(EnrProgramSetSecondary.class.isAssignableFrom(EntityRuntime.getMeta(programSetId).getEntityClass()) ? errorMessageProgramSetSecondary : errorMessageProgramSet);
        }

        List<EnrExamSetVariant> examSetVariants = this.getList(EnrExamSetVariant.class, EnrExamSetVariant.owner().id(), programSetId);
        for (EnrExamSetVariant examSetVariant: examSetVariants)
            examSetVariant.setOwner(null);

        this.getSession().flush();
        this.delete(programSetId);

        for (EnrExamSetVariant examSetVariant: examSetVariants)
            this.delete(examSetVariant);
    }

    protected void doRefreshCompetition(EnrProgramSetBase programSet, String prefix)
    {
        if (programSet.isLessOrEqualThan2014()) {
            throw new ApplicationException(prefix + ": работа с ПК до 2015 года заблокирована.");
        }

        final List<EnrProgramSetOrgUnit> psOuList = getList(EnrProgramSetOrgUnit.class, EnrProgramSetOrgUnit.programSet(), programSet);
        if (psOuList.isEmpty()) {
            throw new ApplicationException(prefix + ": не выбраны подразделения, ведущие прием.");
        }

        if (!programSet.isValidExamSetVariants()) {
            throw new ApplicationException(prefix + ": установленные настройки наборов ВИ не соответствуют способу деления конкурсов.");
        }

        // генерируем конкурсы, удаляем лишнее
        for (EnrProgramSetOrgUnit psOu: psOuList) {

            if (!psOu.isContractOpen() && !psOu.isMinisterialOpen()) {
                throw new ApplicationException(prefix + ": суммарный план приема по всем видам возмещения затрат равен нулю для подразделения «" + psOu.getOrgUnit().getDepartmentTitle() + "».");
            }

            final Map<MultiKey, EnrCompetition> competitionMap = EnrProgramSetCompetitionFactory.buildCompetitions(psOu);

            new MergeAction.SessionMergeAction<MultiKey, EnrCompetition>() {
                @Override protected MultiKey key(final EnrCompetition source) { return source.buildKey(); }
                @Override protected EnrCompetition buildRow(final EnrCompetition source) { return source; }
                @Override protected void fill(final EnrCompetition target, final EnrCompetition source) {
                    if (!ObjectUtils.equals(source.getRequestType(), target.getRequestType())) {
                        // в базе указан левый вид заявления (это косяк базы, автоматически его исправить невозможно)
                        throw new IllegalStateException("compId="+target.getId()+"(tp="+target.getType().getCode()+", lvlReq="+target.getEduLevelRequirement().getCode()+"), requestType="+target.getRequestType().getCode()+" != "+source.getRequestType().getCode());
                    }
                    target.setRequestType(source.getRequestType());
                    target.setExamSetVariant(source.getExamSetVariant());
                    target.setFisUidPostfix(source.getFisUidPostfix());
                }
            }.merge(
                getList(EnrCompetition.class, EnrCompetition.programSetOrgUnit(), psOu),
                competitionMap.values()
            );
        }

        EnrCompetitionManager.instance().dao().doCalculateProgramSetPlans(programSet.getEnrollmentCampaign(), IEnrCompetitionDao.PlanCalculationRule.EQUAL_PARTS, Arrays.asList(programSet.getId()), true);
    }

    @Override
    public void doRefreshCompetition(EnrProgramSetBase programSet) {
        doRefreshCompetition(programSet, "Невозможно обновить перечень конкурсов");
    }

    @Override
    public void doOpenCompetition(EnrProgramSetBase programSet)
    {
        doRefreshCompetition(programSet, "Невозможно открыть конкурсы");
    }

    @Override
    public boolean isExamSetSettingsDiffer(EnrProgramSetBase programSet)
    {
        List<EnrCompetition> competitions = getList(EnrCompetition.class, EnrCompetition.programSetOrgUnit().programSet(), programSet);
        if (competitions.isEmpty()) return false;

        Set<EnrExamSet> competitionExamSetVariants = new HashSet<>();
        for (EnrCompetition competition : competitions) {
            if (competition.getExamSetVariant() != null) {
                competitionExamSetVariants.add(competition.getExamSetVariant().getExamSet());
            }
        }

        Set<EnrExamSet> psExamSetVariants = new HashSet<>();
        if (programSet instanceof EnrProgramSetBase.ISingleExamSetOwner) {
            psExamSetVariants.add(((EnrProgramSetBase.ISingleExamSetOwner)programSet).getExamSetVariant().getExamSet());
        } else if (programSet instanceof EnrProgramSetBS) {
            EnrProgramSetBS bs = (EnrProgramSetBS) programSet;
            if (bs.getExamSetVariantBase() != null) {
                psExamSetVariants.add(bs.getExamSetVariantBase().getExamSet());
            }
            if (bs.getExamSetVariantSec() != null) {
                psExamSetVariants.add(bs.getExamSetVariantSec().getExamSet());
            }
            if (bs.getExamSetVariantHigh() != null) {
                psExamSetVariants.add(bs.getExamSetVariantHigh().getExamSet());
            }
        } else {
            throw new IllegalStateException();
        }

        return !competitionExamSetVariants.equals(psExamSetVariants);
    }

    @Override
    public boolean isCompetitionValid(EnrProgramSetBase programSet)
    {
        if (programSet.isLessOrEqualThan2014()) {
            return true;
        }

        final List<EnrProgramSetOrgUnit> psOuList = getList(EnrProgramSetOrgUnit.class, EnrProgramSetOrgUnit.programSet(), programSet);
        for (EnrProgramSetOrgUnit psOu: psOuList) {
            final Set<MultiKey> expected = EnrProgramSetCompetitionFactory.buildCompetitions(psOu).keySet();
            final Set<MultiKey> existing = new HashSet<>();
            for (EnrCompetition competition : getList(EnrCompetition.class, EnrCompetition.programSetOrgUnit(), psOu)) {
                existing.add(competition.buildKey());
            }
            if (!existing.isEmpty() && !existing.equals(expected)) {
                return false;
            }
        }

        return true;
    }

    @Override
    public void doCloseCompetition(EnrProgramSetBase programSet)
    {
        for (EnrCompetition competition : getList(EnrCompetition.class, EnrCompetition.programSetOrgUnit().programSet().s(), programSet))
        {
            delete(competition);
        }
    }

    @Override
    public List<EnrTargetAdmissionPlanWrapper> getProgramSetOrgUnitTAPlans(Long programSetOrgUnitId, boolean allKinds)
    {
        EnrProgramSetOrgUnit programSetOrgUnit = this.get(programSetOrgUnitId);

        Map<EnrCampaignTargetAdmissionKind, EnrTargetAdmissionPlanWrapper> wrapperMap = new LinkedHashMap<>();

        if (allKinds) {
            for (EnrCampaignTargetAdmissionKind ta: this.getList(EnrCampaignTargetAdmissionKind.class,
                EnrCampaignTargetAdmissionKind.enrollmentCampaign(), programSetOrgUnit.getProgramSet().getEnrollmentCampaign(),
                EnrCampaignTargetAdmissionKind.targetAdmissionKind().priority().s())) {
                wrapperMap.put(ta, new EnrTargetAdmissionPlanWrapper(ta));
            }
        }

        for (EnrTargetAdmissionPlan plan: DataAccessServices.dao().getList(EnrTargetAdmissionPlan.class,
            EnrTargetAdmissionPlan.programSetOrgUnit(), programSetOrgUnit,
            EnrTargetAdmissionPlan.enrCampaignTAKind().targetAdmissionKind().priority().s())) {

            EnrTargetAdmissionPlanWrapper w = new EnrTargetAdmissionPlanWrapper(plan);
            wrapperMap.put(w.getKind(), w);
        }

        for (EnrTargetAdmissionCompetitionPlan plan: DataAccessServices.dao().getList(EnrTargetAdmissionCompetitionPlan.class,
            EnrTargetAdmissionCompetitionPlan.targetAdmissionPlan().programSetOrgUnit(), programSetOrgUnit)) {
            EnrCampaignTargetAdmissionKind kind = plan.getTargetAdmissionPlan().getEnrCampaignTAKind();
            wrapperMap.get(kind).getCompetitionPlanMap().put(plan.getCompetition().getId(), plan.getPlan());
            wrapperMap.get(kind).getCompetitionCalcPlanMap().put(plan.getCompetition().getId(), plan.getCalculatedPlan());
        }

        return new ArrayList<>(wrapperMap.values());
    }

    @Override
    public void updateProgramSetOrgUnitTargetAdmissionPlans(Long psOuId, Collection<EnrTargetAdmissionPlanWrapper> rows)
    {
        List<EnrTargetAdmissionPlan> plans = new ArrayList<>();

        final EnrProgramSetOrgUnit psOu = this.get(EnrProgramSetOrgUnit.class, psOuId);

        for (EnrTargetAdmissionPlanWrapper wrapper : rows) {
            if (wrapper.getPlan() != null && wrapper.getPlan() > 0) {
                plans.add(new EnrTargetAdmissionPlan(psOu, wrapper.getKind(), wrapper.getPlan()));
            }
        }

        List<EnrTargetAdmissionPlan> mergeResult = new MergeAction.SessionMergeAction<EnrCampaignTargetAdmissionKind, EnrTargetAdmissionPlan>() {
            @Override protected EnrCampaignTargetAdmissionKind key(final EnrTargetAdmissionPlan source) {
                return source.getEnrCampaignTAKind();
            }

            @Override protected void fill(final EnrTargetAdmissionPlan target, final EnrTargetAdmissionPlan source) {
                target.setPlan(source.getPlan());
            }

            @Override protected EnrTargetAdmissionPlan buildRow(final EnrTargetAdmissionPlan source) {
                return new EnrTargetAdmissionPlan(source.getProgramSetOrgUnit(), source.getEnrCampaignTAKind(), source.getPlan());
            }
        }.merge(
            getList(EnrTargetAdmissionPlan.class, EnrTargetAdmissionPlan.programSetOrgUnit(), psOu),
            plans
        );

        getSession().flush();

        Map<EnrCampaignTargetAdmissionKind, EnrTargetAdmissionPlan> planMap = new HashMap<>();
        for (EnrTargetAdmissionPlan p : mergeResult) {
            planMap.put(p.getEnrCampaignTAKind(), p);
        }

        List<EnrTargetAdmissionCompetitionPlan> competitionPlans = new ArrayList<>();
        List<EnrCompetition> taCompetitionList = DataAccessServices.dao().getList(new DQLSelectBuilder()
            .fromEntity(EnrCompetition.class, "c")
            .where(eq(property("c", EnrCompetition.programSetOrgUnit().id()), value(psOuId)))
            .where(eq(property("c", EnrCompetition.type().code()), value(EnrCompetitionTypeCodes.TARGET_ADMISSION)))
            .order(property("c", EnrCompetition.eduLevelRequirement().priority())));

        for (EnrTargetAdmissionPlanWrapper wrapper : rows) {
            if (wrapper.getPlan() != null && wrapper.getPlan() > 0) {
                for (EnrCompetition c : taCompetitionList) {
                    Integer plan = wrapper.getPlan(c.getId());
                    Integer calcPlan = wrapper.getCalculatedPlan(c.getId());
                    competitionPlans.add(new EnrTargetAdmissionCompetitionPlan(planMap.get(wrapper.getKind()), c, plan == null ? 0 : plan, calcPlan == null ? 0 : calcPlan));
                }
            }
        }

        new MergeAction.SessionMergeAction<INaturalId, EnrTargetAdmissionCompetitionPlan>() {
            @Override protected INaturalId key(final EnrTargetAdmissionCompetitionPlan source) {
                return source.getNaturalId();
            }

            @Override protected void fill(final EnrTargetAdmissionCompetitionPlan target, final EnrTargetAdmissionCompetitionPlan source) {
                target.setPlan(source.getPlan());
                target.setCalculatedPlan(source.getCalculatedPlan());
            }

            @Override protected EnrTargetAdmissionCompetitionPlan buildRow(final EnrTargetAdmissionCompetitionPlan source) {
                return new EnrTargetAdmissionCompetitionPlan(source.getTargetAdmissionPlan(), source.getCompetition(), source.getPlan(), source.getCalculatedPlan());
            }
        }.merge(
            getList(EnrTargetAdmissionCompetitionPlan.class, EnrTargetAdmissionCompetitionPlan.competition().programSetOrgUnit(), psOu),
            competitionPlans
        );
    }

    @Override
    public Collection<Long> getTAPlanDifferentProgramSetOrgUnits(Long programSetId)
    {
        return new DQLSelectBuilder()
        .fromEntity(EnrProgramSetOrgUnit.class, "psou")
        .joinEntity("psou", DQLJoinType.left, EnrTargetAdmissionPlan.class, "tap", eq(property("tap", EnrTargetAdmissionPlan.programSetOrgUnit().id()), property("psou", EnrProgramSetOrgUnit.id())))
        .column(property("psou", EnrProgramSetOrgUnit.id()))
        .distinct()
        .where(eq(property("psou", EnrProgramSetOrgUnit.programSet().id()), value(programSetId)))
        .group(property("psou", EnrProgramSetOrgUnit.id()))
        .group(property("psou", EnrProgramSetOrgUnit.targetAdmPlan()))

        // исключаем записи, где разница между планом приема по ЦП и суммой планов по видам ЦП равна нулю  -->  смотрим, есть ли оставшиеся
        .having(ne(property("psou", EnrProgramSetOrgUnit.targetAdmPlan()), DQLFunctions.coalesce(DQLFunctions.sum(property("tap", EnrTargetAdmissionPlan.plan())), value(0))))
        .createStatement(getSession()).list();
    }

    @Override
    public void updateProgramSetOrgUnitPlans(EnrProgramSetOrgUnit orgUnit, Map<Long, Integer> planMap)
    {
        for (EnrProgramSetItemOrgUnitPlan plan: this.getList(EnrProgramSetItemOrgUnitPlan.class, EnrProgramSetItemOrgUnitPlan.programSetOrgUnit().id(), orgUnit.getId()))
        {
            int newPlan = planMap.remove(plan.getProgramSetItem().getId());
            if (newPlan != plan.getPlan())
            {
                plan.setPlan(newPlan);
                this.update(plan);
            }
        }

        for (Map.Entry<Long, Integer> entry: planMap.entrySet())
        {
            EnrProgramSetItem item = this.get(entry.getKey());
            EnrProgramSetItemOrgUnitPlan plan = new EnrProgramSetItemOrgUnitPlan(item, orgUnit);
            plan.setPlan(entry.getValue());
            this.save(plan);
        }
    }

    @Override
    public boolean isCompetitionTaPlanIncorrect(EnrProgramSetBase programSet)
    {
        Map<EnrCompetition, MutableInt> map = new HashMap<>();
        for (EnrCompetition c : getList(EnrCompetition.class, EnrCompetition.programSetOrgUnit().programSet(), programSet)) {
            if (c.isTargetAdmission()) {
                map.put(c, new MutableInt(0));
            }
        }
        for (EnrTargetAdmissionCompetitionPlan p : getList(EnrTargetAdmissionCompetitionPlan.class, EnrTargetAdmissionCompetitionPlan.competition().programSetOrgUnit().programSet(), programSet)) {
            map.get(p.getCompetition()).add(p.getPlan());
        }
        for (Map.Entry<EnrCompetition, MutableInt> e : map.entrySet()) {
            if (e.getKey().getPlan() != e.getValue().intValue()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Обновляет список организаций-заказчиков для целевой контрактной подготовки
     */
    @Override
    public void saveOrUpdateCustomerTargetContractTraining(EnrProgramSetBase programSet, boolean isTargetContract, List<ExternalOrgUnit> orgUnits)
    {
        if (programSet instanceof EnrProgramSetSecondary && (orgUnits != null && !orgUnits.isEmpty()) )
            throw new ApplicationException("Организация-заказчик может быть добавлена только для ОП ВО.");

        new DQLDeleteBuilder(CustomerTargetContractTraining.class).where(eq(property(CustomerTargetContractTraining.programSet()), value(programSet)))
                .createStatement(getSession()).execute();

        if (!isTargetContract || orgUnits == null)
            return;

        for (ExternalOrgUnit orgUnit : orgUnits)
            save(new CustomerTargetContractTraining(programSet, orgUnit));

    }

    @Override
    public List<ExternalOrgUnit> getExternalOrgUnitsForProgramSet(EnrProgramSetBase programSetBase)
    {
        return new DQLSelectBuilder().fromEntity(CustomerTargetContractTraining.class, "cou")
                .column(property("cou", CustomerTargetContractTraining.externalOrgUnit()))
                .where(eq(property("cou", CustomerTargetContractTraining.programSet()), value(programSetBase)))
                .createStatement(getSession()).list();
    }


}