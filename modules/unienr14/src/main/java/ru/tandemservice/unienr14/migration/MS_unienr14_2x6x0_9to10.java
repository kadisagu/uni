package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x0_9to10 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrEntrantRequest

		// создано обязательное свойство filedByTrustee
		{
			// создать колонку
			tool.createColumn("enr14_request_t", new DBColumn("filedbytrustee_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			tool.executeUpdate("update enr14_request_t set filedbytrustee_p=? where filedbytrustee_p is null", Boolean.FALSE);

			// сделать колонку NOT NULL
			tool.setColumnNullable("enr14_request_t", "filedbytrustee_p", false);
		}

		// создано свойство trusteeDetails
		{
			// создать колонку
			tool.createColumn("enr14_request_t", new DBColumn("trusteedetails_p", DBType.TEXT));
		}
    }
}