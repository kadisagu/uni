package ru.tandemservice.unienr14.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * изменение приоритетов в enrEntrantState
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x2_19to20 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
        {
            new ScriptDependency("org.tandemframework", "1.6.15"),
            new ScriptDependency("org.tandemframework.shared", "1.6.2")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {

        /**
    <entity name="enrEnrollmentStepState" synchronize="system" constant-support="true">
        <item id="start" constant-name="start">
            <property name="title" value="Сформирован"/>
        </item>
        <item id="recommended" constant-name="recommended">
            <property name="title" value="Рекомендация зафиксирована"/>
        </item>
        <item id="forming orders" constant-name="forming orders">
            <property name="title" value="Формирование приказов"/>
        </item>
        <item id="closed" constant-name="closed">
            <property name="title" value="Зачисление завершено"/>
        </item>
    </entity>
         */

        tool.executeUpdate(
            "update enr14_enr_step_t set state_id=(select id from enr14_c_enr_step_state_t where code_p=?) " +
            "where state_id not in (select id from enr14_c_enr_step_state_t where code_p in (?, ?, ?, ?))",
            "start", /*EnrEnrollmentStepStateCodes.START*/
            "start", "recommended", "forming orders", "closed"
        );

        tool.executeUpdate("delete from enr14_c_enr_step_state_t where code_p not in (?, ?, ?, ?)", "start", "recommended", "forming orders", "closed");
    }
}