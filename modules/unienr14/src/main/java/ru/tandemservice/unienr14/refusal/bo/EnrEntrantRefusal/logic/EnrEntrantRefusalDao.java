/* $Id:$ */
package ru.tandemservice.unienr14.refusal.bo.EnrEntrantRefusal.logic;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.refusal.entity.EnrEntrantRefusal;

/**
 * @author oleyba
 * @since 5/20/14
 */
public class EnrEntrantRefusalDao extends UniBaseDao implements IEnrEntrantRefusalDao
{
    public void saveOrUpdateEntrantRefusal(EnrEntrantRefusal entrantRefusal)
    {
        saveOrUpdate(entrantRefusal);
    }

    public void deleteEntrantRefusal(Long id)
    {
        delete(id);
    }
}