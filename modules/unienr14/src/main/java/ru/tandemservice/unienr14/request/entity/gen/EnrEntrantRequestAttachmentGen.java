package ru.tandemservice.unienr14.request.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequestAttachment;
import ru.tandemservice.unienr14.request.entity.IEnrEntrantRequestAttachable;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Документ абитуриента, приложенный к заявлению
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrEntrantRequestAttachmentGen extends EntityBase
 implements INaturalIdentifiable<EnrEntrantRequestAttachmentGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.request.entity.EnrEntrantRequestAttachment";
    public static final String ENTITY_NAME = "enrEntrantRequestAttachment";
    public static final int VERSION_HASH = 60745678;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTRANT_REQUEST = "entrantRequest";
    public static final String L_DOCUMENT = "document";
    public static final String P_ORIGINAL_HANDED_IN = "originalHandedIn";

    private EnrEntrantRequest _entrantRequest;     // Заявление
    private IEnrEntrantRequestAttachable _document;     // Документ
    private boolean _originalHandedIn;     // Приложен оригинал документа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Заявление. Свойство не может быть null.
     */
    @NotNull
    public EnrEntrantRequest getEntrantRequest()
    {
        return _entrantRequest;
    }

    /**
     * @param entrantRequest Заявление. Свойство не может быть null.
     */
    public void setEntrantRequest(EnrEntrantRequest entrantRequest)
    {
        dirty(_entrantRequest, entrantRequest);
        _entrantRequest = entrantRequest;
    }

    /**
     * @return Документ. Свойство не может быть null.
     */
    @NotNull
    public IEnrEntrantRequestAttachable getDocument()
    {
        return _document;
    }

    /**
     * @param document Документ. Свойство не может быть null.
     */
    public void setDocument(IEnrEntrantRequestAttachable document)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && document!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IEnrEntrantRequestAttachable.class);
            IEntityMeta actual =  document instanceof IEntity ? EntityRuntime.getMeta((IEntity) document) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_document, document);
        _document = document;
    }

    /**
     * @return Приложен оригинал документа. Свойство не может быть null.
     */
    @NotNull
    public boolean isOriginalHandedIn()
    {
        return _originalHandedIn;
    }

    /**
     * @param originalHandedIn Приложен оригинал документа. Свойство не может быть null.
     */
    public void setOriginalHandedIn(boolean originalHandedIn)
    {
        dirty(_originalHandedIn, originalHandedIn);
        _originalHandedIn = originalHandedIn;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrEntrantRequestAttachmentGen)
        {
            if (withNaturalIdProperties)
            {
                setEntrantRequest(((EnrEntrantRequestAttachment)another).getEntrantRequest());
                setDocument(((EnrEntrantRequestAttachment)another).getDocument());
            }
            setOriginalHandedIn(((EnrEntrantRequestAttachment)another).isOriginalHandedIn());
        }
    }

    public INaturalId<EnrEntrantRequestAttachmentGen> getNaturalId()
    {
        return new NaturalId(getEntrantRequest(), getDocument());
    }

    public static class NaturalId extends NaturalIdBase<EnrEntrantRequestAttachmentGen>
    {
        private static final String PROXY_NAME = "EnrEntrantRequestAttachmentNaturalProxy";

        private Long _entrantRequest;
        private Long _document;

        public NaturalId()
        {}

        public NaturalId(EnrEntrantRequest entrantRequest, IEnrEntrantRequestAttachable document)
        {
            _entrantRequest = ((IEntity) entrantRequest).getId();
            _document = ((IEntity) document).getId();
        }

        public Long getEntrantRequest()
        {
            return _entrantRequest;
        }

        public void setEntrantRequest(Long entrantRequest)
        {
            _entrantRequest = entrantRequest;
        }

        public Long getDocument()
        {
            return _document;
        }

        public void setDocument(Long document)
        {
            _document = document;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrEntrantRequestAttachmentGen.NaturalId) ) return false;

            EnrEntrantRequestAttachmentGen.NaturalId that = (NaturalId) o;

            if( !equals(getEntrantRequest(), that.getEntrantRequest()) ) return false;
            if( !equals(getDocument(), that.getDocument()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEntrantRequest());
            result = hashCode(result, getDocument());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEntrantRequest());
            sb.append("/");
            sb.append(getDocument());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrEntrantRequestAttachmentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrEntrantRequestAttachment.class;
        }

        public T newInstance()
        {
            return (T) new EnrEntrantRequestAttachment();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entrantRequest":
                    return obj.getEntrantRequest();
                case "document":
                    return obj.getDocument();
                case "originalHandedIn":
                    return obj.isOriginalHandedIn();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entrantRequest":
                    obj.setEntrantRequest((EnrEntrantRequest) value);
                    return;
                case "document":
                    obj.setDocument((IEnrEntrantRequestAttachable) value);
                    return;
                case "originalHandedIn":
                    obj.setOriginalHandedIn((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entrantRequest":
                        return true;
                case "document":
                        return true;
                case "originalHandedIn":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entrantRequest":
                    return true;
                case "document":
                    return true;
                case "originalHandedIn":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entrantRequest":
                    return EnrEntrantRequest.class;
                case "document":
                    return IEnrEntrantRequestAttachable.class;
                case "originalHandedIn":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrEntrantRequestAttachment> _dslPath = new Path<EnrEntrantRequestAttachment>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrEntrantRequestAttachment");
    }
            

    /**
     * @return Заявление. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequestAttachment#getEntrantRequest()
     */
    public static EnrEntrantRequest.Path<EnrEntrantRequest> entrantRequest()
    {
        return _dslPath.entrantRequest();
    }

    /**
     * @return Документ. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequestAttachment#getDocument()
     */
    public static IEnrEntrantRequestAttachableGen.Path<IEnrEntrantRequestAttachable> document()
    {
        return _dslPath.document();
    }

    /**
     * @return Приложен оригинал документа. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequestAttachment#isOriginalHandedIn()
     */
    public static PropertyPath<Boolean> originalHandedIn()
    {
        return _dslPath.originalHandedIn();
    }

    public static class Path<E extends EnrEntrantRequestAttachment> extends EntityPath<E>
    {
        private EnrEntrantRequest.Path<EnrEntrantRequest> _entrantRequest;
        private IEnrEntrantRequestAttachableGen.Path<IEnrEntrantRequestAttachable> _document;
        private PropertyPath<Boolean> _originalHandedIn;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Заявление. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequestAttachment#getEntrantRequest()
     */
        public EnrEntrantRequest.Path<EnrEntrantRequest> entrantRequest()
        {
            if(_entrantRequest == null )
                _entrantRequest = new EnrEntrantRequest.Path<EnrEntrantRequest>(L_ENTRANT_REQUEST, this);
            return _entrantRequest;
        }

    /**
     * @return Документ. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequestAttachment#getDocument()
     */
        public IEnrEntrantRequestAttachableGen.Path<IEnrEntrantRequestAttachable> document()
        {
            if(_document == null )
                _document = new IEnrEntrantRequestAttachableGen.Path<IEnrEntrantRequestAttachable>(L_DOCUMENT, this);
            return _document;
        }

    /**
     * @return Приложен оригинал документа. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrEntrantRequestAttachment#isOriginalHandedIn()
     */
        public PropertyPath<Boolean> originalHandedIn()
        {
            if(_originalHandedIn == null )
                _originalHandedIn = new PropertyPath<Boolean>(EnrEntrantRequestAttachmentGen.P_ORIGINAL_HANDED_IN, this);
            return _originalHandedIn;
        }

        public Class getEntityClass()
        {
            return EnrEntrantRequestAttachment.class;
        }

        public String getEntityName()
        {
            return "enrEntrantRequestAttachment";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
