/* $Id$ */
package ru.tandemservice.unienr14.order.daemon;

import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.EntityIDGenerator;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unienr14.order.entity.EnrAbstractExtract;
import ru.tandemservice.unienr14.order.entity.EnrAllocationExtract;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentMinisteryExtract;
import ru.tandemservice.unimove.entity.catalog.codes.ExtractStatesCodes;

import java.util.Collection;
import java.util.Collections;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author azhebko
 * @since 29.08.2014
 */
public class EnrStudentOrderDataDaemonDao extends UniBaseDao implements IEnrStudentOrderDataDaemonDao
{
    private static final SyncDaemon DAEMON = new SyncDaemon()
    {
        @Override
        protected void main()
        {
            IEnrStudentOrderDataDaemonDao.instance.get().updateStudentOrderData();
        }
    };

    @Override
    public void updateStudentOrderData()
    {
        createEnrStudentOrderData();
        updateStudentEnrolmentOrderData();
        updateStudentEnrolmentMinOrderData();
        updateStudentSpecializationOrderData();
    }

    /** Создает недостающие данные о последних приказах студентов, включенных на данный момент в проведенные неотмененные приказы о зачислении или о распределении по ОП. */
    private void createEnrStudentOrderData()
    {
        Collection<Long> studentIds = new DQLSelectBuilder()
            .fromEntity(Student.class, "s")
            .column(property("s", Student.id()))
            .distinct()

            .fromEntity(EnrAbstractExtract.class, "e")
            .joinEntity("e", DQLJoinType.left, EnrEnrollmentExtract.class, "ee", eq(property("e"), property("ee")))
            .joinEntity("e", DQLJoinType.left, EnrAllocationExtract.class, "ae", eq(property("e"), property("ae")))
            .joinEntity("e", DQLJoinType.left, EnrEnrollmentMinisteryExtract.class, "eme", eq(property("e"), property("eme")))
            .where(eq(property("e", EnrAbstractExtract.cancelled()), value(Boolean.FALSE)))
            .where(eq(property("e", EnrAbstractExtract.state().code()), value(ExtractStatesCodes.FINISHED)))

            .where(or(
                eq(property("ee", EnrEnrollmentExtract.student()), property("s")),
                eq(property("ae", EnrAllocationExtract.student()), property("s")),
                eq(property("eme", EnrEnrollmentMinisteryExtract.student()), property("s"))))

            .where(notExists(
                new DQLSelectBuilder()
                    .fromEntity(OrderData.class, "od")
                    .where(eq(property("s"), property("od", OrderData.student())))
                    .buildQuery()))
            .createStatement(this.getSession()).list();

        if (!studentIds.isEmpty())
        {
            BatchUtils.execute(studentIds, DQL.MAX_VALUES_ROW_NUMBER, new BatchUtils.Action<Long>()
            {
                private final short _orderDataEntityCode = EntityRuntime.getMeta(OrderData.class).getEntityCode();

                @Override
                public void execute(Collection<Long> elements)
                {
                    DQLInsertValuesBuilder insertValuesBuilder = new DQLInsertValuesBuilder(OrderData.class);
                    for (Long studentId : elements)
                    {
                        insertValuesBuilder.value(OrderData.P_ID, EntityIDGenerator.generateNewId(_orderDataEntityCode));
                        insertValuesBuilder.value(OrderData.L_STUDENT, studentId);
                        insertValuesBuilder.addBatch();
                    }

                    insertValuesBuilder.createStatement(EnrStudentOrderDataDaemonDao.this.getSession()).execute();
                }
            });
        }
    }

    /** Обновляет данные о последних приказах студентов, включенных на данный момент в проведенные неотмененные приказы о зачислении. */
    private void updateStudentEnrolmentOrderData()
    {
        new DQLUpdateBuilder(OrderData.class)
            .fromEntity(EnrEnrollmentExtract.class, "ae")
            .set(OrderData.eduEnrollmentOrderNumber().s(), property("ae", EnrEnrollmentExtract.paragraph().order().number()))
            .set(OrderData.eduEnrollmentOrderDate().s(), property("ae", EnrEnrollmentExtract.paragraph().order().commitDate()))
            .set(OrderData.eduEnrollmentOrderEnrDate().s(), property("ae", EnrEnrollmentExtract.paragraph().order().actionDate()))

            .where(eq(property(OrderData.student()), property("ae", EnrEnrollmentExtract.student())))
            .where(eq(property("ae", EnrEnrollmentExtract.cancelled()), value(Boolean.FALSE)))
            .where(eq(property("ae", EnrEnrollmentExtract.state().code()), value(ExtractStatesCodes.FINISHED)))

            .where(or(
                neNullSafe(property(OrderData.eduEnrollmentOrderNumber()), property("ae", EnrEnrollmentExtract.paragraph().order().number())),
                neNullSafe(property(OrderData.eduEnrollmentOrderDate()), property("ae", EnrEnrollmentExtract.paragraph().order().commitDate())),
                neNullSafe(property(OrderData.eduEnrollmentOrderEnrDate()), property("ae", EnrEnrollmentExtract.paragraph().order().actionDate()))))

            .createStatement(this.getSession()).execute();
    }

    /** Обновляет данные о последних приказах студентов, включенных на данный момент в проведенные неотмененные приказы о зачислении по направлению Минобрнауки. */
    private void updateStudentEnrolmentMinOrderData()
    {
        new DQLUpdateBuilder(OrderData.class)
                .fromEntity(EnrEnrollmentMinisteryExtract.class, "eme")
                .set(OrderData.eduEnrollmentOrderNumber().s(), property("eme", EnrEnrollmentMinisteryExtract.paragraph().order().number()))
                .set(OrderData.eduEnrollmentOrderDate().s(), property("eme", EnrEnrollmentMinisteryExtract.paragraph().order().commitDate()))
                .set(OrderData.eduEnrollmentOrderEnrDate().s(), property("eme", EnrEnrollmentMinisteryExtract.paragraph().order().actionDate()))

                .where(eq(property(OrderData.student()), property("eme", EnrEnrollmentMinisteryExtract.student())))
                .where(eq(property("eme", EnrEnrollmentMinisteryExtract.cancelled()), value(Boolean.FALSE)))
                .where(eq(property("eme", EnrEnrollmentMinisteryExtract.state().code()), value(ExtractStatesCodes.FINISHED)))

                .where(or(
                        neNullSafe(property(OrderData.eduEnrollmentOrderNumber()), property("eme", EnrEnrollmentMinisteryExtract.paragraph().order().number())),
                        neNullSafe(property(OrderData.eduEnrollmentOrderDate()), property("eme", EnrEnrollmentMinisteryExtract.paragraph().order().commitDate())),
                        neNullSafe(property(OrderData.eduEnrollmentOrderEnrDate()), property("eme", EnrEnrollmentMinisteryExtract.paragraph().order().actionDate()))))

                .createStatement(this.getSession()).execute();
    }

    /** Обновляет данные о последних приказах студентов, включенных на данный момент в проведенные неотмененные приказы о распределении по ОП. */
    private void updateStudentSpecializationOrderData()
    {
        new DQLUpdateBuilder(OrderData.class)
            .fromEntity(EnrAllocationExtract.class, "ae")
            .set(OrderData.splitToSpecializationOrderNumber().s(), property("ae", EnrAllocationExtract.paragraph().order().number()))
            .set(OrderData.splitToSpecializationOrderDate().s(), property("ae", EnrAllocationExtract.paragraph().order().commitDate()))

            .where(eq(property(OrderData.student()), property("ae", EnrAllocationExtract.student())))
            .where(eq(property("ae", EnrAllocationExtract.cancelled()), value(Boolean.FALSE)))
            .where(eq(property("ae", EnrAllocationExtract.state().code()), value(ExtractStatesCodes.FINISHED)))

            .where(or(
                neNullSafe(property(OrderData.splitToSpecializationOrderNumber()), property("ae", EnrAllocationExtract.paragraph().order().number())),
                neNullSafe(property(OrderData.splitToSpecializationOrderDate()), property("ae", EnrAllocationExtract.paragraph().order().commitDate()))))

            .createStatement(this.getSession()).execute();
    }

    @Override
    protected void initDao() throws Exception
    {
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, EnrEnrollmentExtract.class, DAEMON.getAfterCompleteWakeUpListener(), Collections.singleton(EnrEnrollmentExtract.student().s()));
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, EnrEnrollmentMinisteryExtract.class, DAEMON.getAfterCompleteWakeUpListener(), Collections.singleton(EnrEnrollmentMinisteryExtract.student().s()));
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, EnrAllocationExtract.class, DAEMON.getAfterCompleteWakeUpListener(), Collections.singleton(EnrAllocationExtract.student().s()));
    }
}