/* $Id:$ */
package ru.tandemservice.unienr14.extreports.externalView;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.extview.SimpleDQLExternalViewConfig;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.unienr14.catalog.entity.EnrAbsenceNote;
import ru.tandemservice.unienr14.catalog.entity.EnrDiscipline;
import ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm;
import ru.tandemservice.unienr14.catalog.entity.EnrStateExamSubject;
import ru.tandemservice.unienr14.competition.entity.EnrExamVariant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroup;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm;
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSource;
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceAppeal;
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceBenefit;
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceExamPass;
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceStateExam;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassAppeal;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;
import ru.tandemservice.unienr14.request.entity.gen.IEnrEntrantBenefitProofDocumentGen;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline;

/**
 * @author oleyba
 * @since 8/27/13
 */
public class EnrExtViewProvider4ChosenExam extends SimpleDQLExternalViewConfig
{
    @Override
    protected DQLSelectBuilder buildDqlQuery()
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
        .fromEntity(EnrChosenEntranceExam.class, "exam")
        .joinPath(DQLJoinType.inner, EnrChosenEntranceExam.actualExam().fromAlias("exam"), "act")
        .joinPath(DQLJoinType.left, EnrChosenEntranceExam.maxMarkForm().fromAlias("exam"), "maxForm")
        .joinPath(DQLJoinType.left, EnrChosenEntranceExamForm.passForm().fromAlias("maxForm"), "passFormE")
        .joinPath(DQLJoinType.left, EnrChosenEntranceExamForm.markSource().fromAlias("maxForm"), "markSrc")

        .joinEntity("markSrc", DQLJoinType.left, EnrEntrantMarkSourceStateExam.class, "markSrcState", eq(property("markSrc"), property("markSrcState")))
        .joinPath(DQLJoinType.left, EnrEntrantMarkSourceStateExam.stateExamResult().fromAlias("markSrcState"), "stateMark")
        .joinPath(DQLJoinType.left, EnrEntrantStateExamResult.subject().fromAlias("stateMark"), "stateSubj")

        .joinEntity("markSrc", DQLJoinType.left, EnrEntrantMarkSourceBenefit.class, "markSrcOlymp", eq(property("markSrc"), property("markSrcOlymp")))
        .joinPath(DQLJoinType.left, EnrEntrantMarkSourceBenefit.mainProof().fromAlias("markSrcOlymp"), "olympDiploma")

        .joinEntity("markSrc", DQLJoinType.left, EnrEntrantMarkSourceExamPass.class, "markSrcExam", eq(property("markSrc"), property("markSrcExam")))
        .joinPath(DQLJoinType.left, EnrEntrantMarkSourceExamPass.examPassDiscipline().fromAlias("markSrcExam"), "examPass")
        .joinPath(DQLJoinType.left, EnrExamPassDiscipline.absenceNote().fromAlias("examPass"), "absenceNoteCI")
        .joinPath(DQLJoinType.left, EnrExamPassDiscipline.examGroup().fromAlias("examPass"), "examGroup")
        .joinPath(DQLJoinType.left, EnrExamPassDiscipline.territorialOrgUnit().fromAlias("examPass"), "terrOu")

        .joinEntity("markSrc", DQLJoinType.left, EnrEntrantMarkSourceAppeal.class, "markSrcApp", eq(property("markSrc"), property("markSrcApp")))
        .joinPath(DQLJoinType.left, EnrEntrantMarkSourceAppeal.examPassAppeal().fromAlias("markSrcApp"), "app")

        .joinPath(DQLJoinType.left, EnrChosenEntranceExam.discipline().fromAlias("exam"), "disc")
        .joinPath(DQLJoinType.left, EnrCampaignDiscipline.discipline().fromAlias("disc"), "disc_c");



        column(dql, DQLExpressions.property(EnrChosenEntranceExam.requestedCompetition().request().entrant().id().fromAlias("exam")), "entrantId").comment("uid абитуриента");
        column(dql, DQLExpressions.property(EnrChosenEntranceExam.requestedCompetition().id().fromAlias("exam")), "requestedCompetitionId").comment("uid выбранного конкурса");
        column(dql, DQLExpressions.property(EnrChosenEntranceExam.requestedCompetition().competition().id().fromAlias("exam")), "competitionGroupId").comment("вместо этой колонки следует использовать колонку competitionId. в данный момент эта колонка оставлена для совместимости с уже разработанными отчетами, но в дальнейшем будет удалена.");
        column(dql, DQLExpressions.property(EnrChosenEntranceExam.requestedCompetition().competition().id().fromAlias("exam")), "competitionId").comment("uid конкурса");

        column(dql, DQLExpressions.property(EnrDiscipline.title().fromAlias("disc_c")), "discipline").comment("дисциплина ВИ");
        column(dql, DQLExpressions.property(EnrDiscipline.code().fromAlias("disc_c")), "disciplineCode").comment("код дисциплины ВИ");
        column(dql, DQLExpressions.property(EnrCampaignDiscipline.id().fromAlias("disc")), "disciplineId").comment("uid дисциплины ВИ");

        column(dql, EnrExtViewUtil.asLongColumn(EnrExamVariant.passMarkAsLong().fromAlias("act")), "passMark").comment("минимальный балл по ВИ");

        column(dql, 0, PropertyType.INT, "profile").comment("временное, будет удалено");

        column(dql, DQLExpressions.property(EnrExamVariant.examSetElement().type().shortTitle().fromAlias("act")), "type").comment("вид ВИ");
        column(dql, DQLExpressions.property(EnrExamVariant.examSetElement().type().code().fromAlias("act")), "typeCode").comment("код вида ВИ");

        column(dql, DQLExpressions.property(EnrExamPassForm.title().fromAlias("passFormE")), "passForm").comment("форма сдачи");
        column(dql, DQLExpressions.property(EnrExamPassForm.code().fromAlias("passFormE")), "passFormCode").comment("код формы сдачи");
        booleanIntColumn(dql, EnrExamPassForm.internal().fromAlias("passFormE"), "passFormInternal").comment("внутренняя форма сдачи (1 - да, 0 - нет)");

        column(dql, EnrExtViewUtil.asLongColumn(EnrEntrantMarkSource.markAsLong().fromAlias("markSrc")), "mark").comment("балл");

        column(dql, "-", PropertyType.STRING, "certificateId").comment("временное, будет удалено");
        column(dql, EnrExtViewUtil.certNumberColumn(EnrEntrantStateExamResult.documentNumber().fromAlias("stateMark")), "certificate").comment("номер свидетельства ЕГЭ");
        column(dql, DQLExpressions.property(EnrStateExamSubject.shortTitle().fromAlias("stateSubj")), "certificateSubject").comment("предмет ЕГЭ");
        column(dql, DQLExpressions.property(EnrStateExamSubject.code().fromAlias("stateSubj")), "certificateSubjectCode").comment("код предмета ЕГЭ");

        column(dql, DQLExpressions.property(IEnrEntrantBenefitProofDocumentGen.id().fromAlias("olympDiploma")), "olympiadDiplomaId").comment("uid диплома олимпиады");

        booleanIntColumn(dql, EnrExamPassDiscipline.retake().fromAlias("examPass"), "retake").comment("пересдача (1 - да, 0 - нет)");

        column(dql, DQLExpressions.property(EnrAbsenceNote.shortTitle().fromAlias("absenceNoteCI")), "absenceNote").comment("отметка о неявке");
        column(dql, DQLExpressions.property(EnrAbsenceNote.code().fromAlias("absenceNoteCI")), "absenceNoteCode").comment("код отметки о неявке");
        column(dql, DQLExpressions.property(EnrExamGroup.id().fromAlias("examGroup")), "examGroupId").comment("uid экзам. группы");
        column(dql, DQLExpressions.property(EnrExamPassDiscipline.examGroupPosition().fromAlias("examPass")), "examGroupPosition").comment("номер в экзам. группе");
        column(dql, DQLExpressions.property(EnrExamPassDiscipline.examGroupSetDate().fromAlias("examPass")), "examGroupSetDate").comment("дата включения в экзам. группу");
        column(dql, DQLExpressions.property(OrgUnit.territorialFullTitle().fromAlias("terrOu")), "territorialOrgUnit").comment("подразделение, где абитуриент должен сдавать ВИ");

        column(dql, DQLExpressions.property(EnrExamPassAppeal.appealDate().fromAlias("app")), "appealDate").comment("дата апелляции");
        column(dql, DQLExpressions.property(EnrExamPassAppeal.decisionDate().fromAlias("app")), "decisionDate").comment("дата решения по апелляции");

        return dql;
    }
}
