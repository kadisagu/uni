/* $Id$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.ExamsStep;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.catalog.entity.EnrInternalExamReason;

/**
 * @author nvankov
 * @since 6/5/14
 */
@Configuration
public class EnrEntrantRequestAddWizardExamsStep extends BusinessComponentManager
{
    public static final String INTERNAL_EXAM_REASON_DS = "internalExamReasonDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(selectDS(INTERNAL_EXAM_REASON_DS, internalExamReasonDSHandler()))
            .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler internalExamReasonDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), EnrInternalExamReason.class)
            .where(EnrInternalExamReason.enabled(), Boolean.TRUE)
            .filter(EnrInternalExamReason.title())
            .order(EnrInternalExamReason.code());
    }
}



    