/* $Id:$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.ActionsAddon;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrRequestTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.unienr14.entrant.daemon.EnrEntrantDaemonBean;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.EnrEntrantRequestManager;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.AddAppend.EnrEntrantRequestAddAppend;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.DocumentAttach.EnrEntrantRequestDocumentAttach;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.Edit.EnrEntrantRequestEdit;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.Edit.EnrEntrantRequestEditUI;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.EditTakeAwayDate.EnrEntrantRequestEditTakeAwayDate;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.ui.AddEdit.EnrEnrollmentCampaignAddEdit;

/**
 * @author oleyba
 * @since 5/3/13
 */
public class EnrEntrantRequestActionsAddon extends UIAddon
{
    public static final String NAME = "EnrEntrantRequestActionsAddon";
    private CommonPostfixPermissionModel sec;

    public interface IOwner {
        EnrEntrantRequest getRequestForActions();
        boolean isAccessible();
        String getSecPostfix();
    }

    // constructor
    public EnrEntrantRequestActionsAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    private IOwner getOwner() {
        return ((IOwner) getPresenter());
    }

    public EnrEntrantRequest getEntrantRequest() {
        return getOwner().getRequestForActions();
    }

    public EnrEntrant getEntrant() {
        return getEntrantRequest().getEntrant();
    }

    @Override
    public void onComponentRefresh()
    {
        setSec(new CommonPostfixPermissionModel(getOwner().getSecPostfix()));
    }

    // listeners

    public void onClickEditEntrantRequest()
    {
        getEntrant().getEnrollmentCampaign().checkOpen();
        getActivationBuilder().asRegionDialog(EnrEntrantRequestEdit.class)
        .parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong())
        .parameter(EnrEntrantRequestEditUI.SEC_POSTFIX, getOwner().getSecPostfix())
        .activate();
    }

    public void onClickAddRequestedEnrollmentDirection()
    {
        getEntrant().getEnrollmentCampaign().checkOpen();
        getPresenter().getActivationBuilder().asRegion(EnrEntrantRequestAddAppend.class).top()
        .parameter("entrantId", getEntrant().getId())
        .parameter("entrantRequestId", getListenerParameterAsLong())
        .activate();
    }

    public void onClickDeleteEntrantRequest()
    {
        EnrEntrantRequestManager.instance().dao().deleteEntrantRequest(getListenerParameterAsLong());
        ContextLocal.getSite().getDesktop().setRefreshScheduled(true);
    }

    public void onClickDocumentAttach()
    {
        getPresenter().getActivationBuilder().asRegion(EnrEntrantRequestDocumentAttach.class).top()
        .parameter("requestId", getListenerParameterAsLong())
        .activate();
    }

    public void onClickDocumentsReturn()
    {
        try {
            EnrEntrantRequestManager.instance().dao().updateDocumentsReturn(getListenerParameterAsLong());
            EnrEntrantDaemonBean.DAEMON.waitForComplete(60);
        } finally {
            ContextLocal.getSite().getDesktop().setRefreshScheduled(true);
        }
    }

    public void onClickDocumentsTakeAway()
    {
        try {
            EnrEntrantRequestManager.instance().dao().updateDocumentsReturn(getListenerParameterAsLong());
            EnrEntrantDaemonBean.DAEMON.waitForComplete(60);
        } finally {
            ContextLocal.getSite().getDesktop().setRefreshScheduled(true);
        }
    }

    public void onClickPrintEntrantRequest() throws Exception
    {
        String templateCode = EnrScriptItemCodes.COMMON_ENTRANT_REQUEST_BS;
        switch (getEntrantRequest().getType().getCode())
        {
            case EnrRequestTypeCodes.MASTER:
                templateCode = EnrScriptItemCodes.COMMON_ENTRANT_REQUEST_MASTER;
                break;
            case EnrRequestTypeCodes.SPO:
                templateCode = EnrScriptItemCodes.COMMON_ENTRANT_REQUEST_SECONDARY;
                break;
            case EnrRequestTypeCodes.HIGHER:
            case EnrRequestTypeCodes.POSTGRADUATE:
            case EnrRequestTypeCodes.TRAINEESHIP:
            case EnrRequestTypeCodes.INTERNSHIP:
                templateCode = EnrScriptItemCodes.COMMON_ENTRANT_REQUEST_HIGHER;
                break;
        }
        EnrScriptItem scriptItem = DataAccessServices.dao().getByCode(EnrScriptItem.class, templateCode);

        if(scriptItem.isPrintPdf())
            UniRtfUtil.downloadRtfScriptResultAsPdf(CommonManager.instance().scriptDao().getScriptResult(scriptItem, getListenerParameterAsLong()));
        else
            CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(scriptItem, getListenerParameterAsLong());
    }

    public void onClickPrintEnrollmentExamSheet()
    {
        EnrScriptItem scriptItem = null;
        if (EnrEnrollmentCampaignAddEdit.EXAM_LIST_PRINT_TYPE_INTERNAL_EXAMS.equals(getEntrant().getEnrollmentCampaign().getSettings().getEnrEntrantExamListPrintType()))
        {
            scriptItem = DataAccessServices.dao().getByCode(EnrScriptItem.class, EnrScriptItemCodes.COMMON_ENROLLMENT_EXAM_SHEET);
        }
        else if (EnrEnrollmentCampaignAddEdit.EXAM_LIST_PRINT_TYPE_ALL_EXAMS.equals(getEntrant().getEnrollmentCampaign().getSettings().getEnrEntrantExamListPrintType()))
        {
            scriptItem = DataAccessServices.dao().getByCode(EnrScriptItem.class, EnrScriptItemCodes.COMMON_ENROLLMENT_EXAM_SHEET_ALL_EXAMS);
        }
        else
        {
            throw new UnsupportedOperationException();
        }

        if(scriptItem.isPrintPdf())
            UniRtfUtil.downloadRtfScriptResultAsPdf(CommonManager.instance().scriptDao().getScriptResult(scriptItem, IUniBaseDao.instance.get().get(EnrEntrantRequest.class, getListenerParameterAsLong()).getEntrant().getId()));
        else
            CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(scriptItem, IUniBaseDao.instance.get().get(EnrEntrantRequest.class, getListenerParameterAsLong()).getEntrant().getId());

    }

    public void onClickPrintListAndReceipt()
    {
        EnrScriptItem scriptItem = DataAccessServices.dao().getByCode(EnrScriptItem.class, EnrScriptItemCodes.COMMON_DOCUMENT_LIST_AND_RECEIPT);
        if(scriptItem.isPrintPdf())
            UniRtfUtil.downloadRtfScriptResultAsPdf(CommonManager.instance().scriptDao().getScriptResult(scriptItem, getListenerParameterAsLong()));
        else
            CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(scriptItem, getListenerParameterAsLong());
    }

    public void onClickPrintAuthCard()
    {
        // пока не актуально

        //        EnrScriptItem template = getCatalogItem(EnrScriptItem.class, UniecDefines.TEMPLATE_AUTH_CARD);
        //        if (template == null) return;
        //
        //        // создаем отчет (formCreator должен существовать при заданном шаблоне)
        //        IPrintFormCreator<Long> formCreator = (IPrintFormCreator<Long>) ApplicationRuntime.getBean("entrantAuthCardPrint");
        //        byte[] content = RtfUtil.toByteArray(formCreator.createPrintForm(template.getCurrentTemplate(), getListenerParameterAsLong()));
        //        Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(content, "Удостоверение абитуриента " + getEntrant().getPerson().getFullFio() + ".rtf");
        //        activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT, new ParametersMap().add("id", id).add("zip", false).add("extension", "rtf")));
    }

    public void onClickPrintEntrantLetterbox() throws Exception
    {
        EnrScriptItem scriptItem = DataAccessServices.dao().getByCode(EnrScriptItem.class, EnrScriptItemCodes.COMMON_ENTRANT_LETTER_BOX);
        if(scriptItem.isPrintPdf())
            UniRtfUtil.downloadRtfScriptResultAsPdf(CommonManager.instance().scriptDao().getScriptResult(scriptItem, getListenerParameterAsLong()));
        else
            CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(scriptItem, (Long)getListenerParameterAsLong());
    }

    public void onClickEditTakeAwayDate() {
        getActivationBuilder().asRegionDialog(EnrEntrantRequestEditTakeAwayDate.class)
        .parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong())
        .activate();
    }

    //Согласие на зачисление
    public void onClickPrintAcceptEnrollment()
    {
        EnrScriptItem scriptItem = DataAccessServices.dao().getByCode(EnrScriptItem.class, EnrScriptItemCodes.COMMON_ENTRANT_REQUEST_ACCEPTED_ENROLLMENT);
        if(scriptItem.isPrintPdf())
            UniRtfUtil.downloadRtfScriptResultAsPdf(CommonManager.instance().scriptDao().getScriptResult(scriptItem, getListenerParameterAsLong()));
        else
            CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(scriptItem, (Long)getListenerParameterAsLong());
    }

    //Отказ от зачисления
    public void onClickPrintRefusalEnrollment()
    {
        EnrScriptItem scriptItem = DataAccessServices.dao().getByCode(EnrScriptItem.class, EnrScriptItemCodes.COMMON_ENTRANT_REQUEST_REFUSED_ENROLLMENT);
        if(scriptItem.isPrintPdf())
            UniRtfUtil.downloadRtfScriptResultAsPdf(CommonManager.instance().scriptDao().getScriptResult(scriptItem, getListenerParameterAsLong()));
        else
            CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(scriptItem, (Long)getListenerParameterAsLong());
    }

    // alerts

    public String getTakeAwayAlert() {
        return "Вернуть документы абитуриенту и отозвать заявление №" + getEntrantRequest().getStringNumber() + "?";
    }

    public String getReturnAlert() {
        return "Вновь принять документы у абитуриента по заявлению №" + getEntrantRequest().getStringNumber() + '?';
    }

    public String getDeleteAlert() {
        return "Удалить заявление №" + getEntrantRequest().getStringNumber() + " абитуриента «" + getEntrant().getPerson().getFullFio() + "»?";

        // todo
        //List<ExamGroup> groupList = getEntrantRequestGroup(getCurrentEntrantRequest());
        //if (groupList.isEmpty())
        //    return "Удалить заявление №" + getCurrentEntrantRequest().getRegNumber() + " абитуриента «" + getCurrentEntrantRequest().getEntrant().getPerson().getFullFio() + "»?";
        //else
        //    return "Удалить заявление №" + getCurrentEntrantRequest().getRegNumber() + " абитуриента «" + getCurrentEntrantRequest().getEntrant().getPerson().getFullFio() + "», исключая из экзаменационной группы?";
    }

    // getters and setters

    public CommonPostfixPermissionModel getSec()
    {
        return sec;
    }

    public void setSec(CommonPostfixPermissionModel sec)
    {
        this.sec = sec;
    }
}
