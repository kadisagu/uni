package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unienr14_2x9x3_4to5 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.3")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrEnrollmentStepItem

		// создано обязательное свойство enrollmentAvailable
		{
			// создать колонку
			tool.createColumn("enr14_enr_step_item_t", new DBColumn("enrollmentavailable_p", DBType.BOOLEAN));

			tool.executeUpdate("update enr14_enr_step_item_t set enrollmentavailable_p=accepted_p where enrollmentavailable_p is null");

			// сделать колонку NOT NULL
			tool.setColumnNullable("enr14_enr_step_item_t", "enrollmentavailable_p", false);

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrModelStepItem

		// создано обязательное свойство enrollmentAvailable
		{
			// создать колонку
			tool.createColumn("enr14_enr_model_step_item_t", new DBColumn("enrollmentavailable_p", DBType.BOOLEAN));

			tool.executeUpdate("update enr14_enr_model_step_item_t set enrollmentavailable_p=accepted_p where enrollmentavailable_p is null");

			// сделать колонку NOT NULL
			tool.setColumnNullable("enr14_enr_model_step_item_t", "enrollmentavailable_p", false);

		}


    }
}