/* $Id$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.IdentityCardStep;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.datasource.select.ISelectValueStyleSource;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.core.view.list.IViewSelectValueStyle;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.fias.base.bo.util.NonActiveSelectValueStyle;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrSettings.logic.IEnrSettingsDao;

/**
 * @author nvankov
 * @since 5/30/14
 */
@Configuration
public class EnrEntrantRequestAddWizardIdentityCardStep extends BusinessComponentManager
{
    public static final String OPTIONS_DS = "optionsDS";
    public static final String ONLINE_ENTRANT_DS = "onlineEntrantDS";


    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
        .addDataSource(selectDS(OPTIONS_DS, optionsDSHandler()))
        .addDataSource(selectDS(ONLINE_ENTRANT_DS, onlineEntrantDSHandler()).addColumn("personNumberWithFio", null, new IFormatter<EnrOnlineEntrant>()
            {
            @Override
            public String format(EnrOnlineEntrant source)
            {
                IdentityCard identityCard = source.getIdentityCard();
                return source.getPersonalNumber() + " - " + identityCard.getFullFio() +
                " (" + identityCard.getFullNumber() + (identityCard.getBirthDate() != null ? ", д.р. " +
                DateFormatter.DEFAULT_DATE_FORMATTER.format(identityCard.getBirthDate()) : "") + ")";
            }
            }).valueStyleSource(new ISelectValueStyleSource()
            {
                @Override
                public IViewSelectValueStyle getSelectValueStyle(Object value)
                {
                    if (value instanceof EnrOnlineEntrant)
                    {
                        if (((EnrOnlineEntrant) value).getEntrant() != null)
                            return NonActiveSelectValueStyle.INSTANCE;
                    }
                    return null;
                }
            }))
            .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
            .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler onlineEntrantDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrOnlineEntrant.class)
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                DQLSelectBuilder builder = super.query(alias, filter);
                builder.where(DQLExpressions.likeUpper(DQLFunctions.concat(
                        DQLFunctions.cast(DQLExpressions.property(alias, EnrOnlineEntrant.personalNumber()), PropertyType.STRING),
                                DQLExpressions.property(alias, EnrOnlineEntrant.identityCard().lastName()),
                                DQLExpressions.property(alias, EnrOnlineEntrant.identityCard().firstName()),
                                DQLExpressions.caseExpr(
                                        new IDQLExpression[]{DQLExpressions.isNotNull(DQLExpressions.property(alias, EnrOnlineEntrant.identityCard().middleName()))},
                                        new IDQLExpression[]{DQLExpressions.property(alias, EnrOnlineEntrant.identityCard().middleName())},
                                        DQLExpressions.value(" "))
                        ), DQLExpressions.value(CoreStringUtils.escapeLike(filter, true))));

                return builder;
            }

            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                Long enrollmentCampaignId = context.getNotNull(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
                dql.where(DQLExpressions.eq(DQLExpressions.property(alias, EnrOnlineEntrant.enrEnrollmentCampaign().id()), DQLExpressions.value(enrollmentCampaignId)));
            }
        }
                .customize((alias, dql, context, filter) ->
                        DataSettingsFacade.getSettings(IEnrSettingsDao.ENR_SETTINGS).get(IEnrSettingsDao.SETTINGS_ENTRANT_ONLINE_TREATED) == null ||
                                (Boolean) DataSettingsFacade.getSettings(IEnrSettingsDao.ENR_SETTINGS).get(IEnrSettingsDao.SETTINGS_ENTRANT_ONLINE_TREATED) ? dql :
                                dql.where(DQLExpressions.isNull(DQLExpressions.property(alias, EnrOnlineEntrant.entrant()))))
        .order(EnrOnlineEntrant.identityCard().fullFio())
        .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler optionsDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName()).addItemList(EnrEntrantManager.instance().addEntrantWizardFirstStepOptionsExtPoint());
    }
}



