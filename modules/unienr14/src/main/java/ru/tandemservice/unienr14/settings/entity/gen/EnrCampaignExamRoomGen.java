package ru.tandemservice.unienr14.settings.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unienr14.catalog.entity.EnrExamRoomType;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignExamRoom;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Аудитория для проведения ВИ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrCampaignExamRoomGen extends EntityBase
 implements INaturalIdentifiable<EnrCampaignExamRoomGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.settings.entity.EnrCampaignExamRoom";
    public static final String ENTITY_NAME = "enrCampaignExamRoom";
    public static final int VERSION_HASH = -1274455987;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String L_PLACE = "place";
    public static final String L_RESPONSIBLE_ORG_UNIT = "responsibleOrgUnit";
    public static final String L_EXAM_ROOM_TYPE = "examRoomType";
    public static final String P_PREPARED_FOR_DISABLED_PEOPLE = "preparedForDisabledPeople";
    public static final String P_SIZE = "size";
    public static final String P_COMMENT = "comment";
    public static final String P_PLACE_LOCATION_WITH_SIZE = "placeLocationWithSize";

    private EnrEnrollmentCampaign _enrollmentCampaign;     // ПК
    private UniplacesPlace _place;     // Помещение
    private OrgUnit _responsibleOrgUnit;     // Ответственное терр. подразделение
    private EnrExamRoomType _examRoomType;     // Тип аудитории
    private boolean _preparedForDisabledPeople;     // Подготовлена для проведения ВИ лиц с огр. возм.
    private int _size;     // Число мест
    private String _comment;     // Комментарий

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return ПК. Свойство не может быть null.
     */
    @NotNull
    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign ПК. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Помещение. Свойство не может быть null.
     */
    @NotNull
    public UniplacesPlace getPlace()
    {
        return _place;
    }

    /**
     * @param place Помещение. Свойство не может быть null.
     */
    public void setPlace(UniplacesPlace place)
    {
        dirty(_place, place);
        _place = place;
    }

    /**
     * @return Ответственное терр. подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getResponsibleOrgUnit()
    {
        return _responsibleOrgUnit;
    }

    /**
     * @param responsibleOrgUnit Ответственное терр. подразделение. Свойство не может быть null.
     */
    public void setResponsibleOrgUnit(OrgUnit responsibleOrgUnit)
    {
        dirty(_responsibleOrgUnit, responsibleOrgUnit);
        _responsibleOrgUnit = responsibleOrgUnit;
    }

    /**
     * @return Тип аудитории. Свойство не может быть null.
     */
    @NotNull
    public EnrExamRoomType getExamRoomType()
    {
        return _examRoomType;
    }

    /**
     * @param examRoomType Тип аудитории. Свойство не может быть null.
     */
    public void setExamRoomType(EnrExamRoomType examRoomType)
    {
        dirty(_examRoomType, examRoomType);
        _examRoomType = examRoomType;
    }

    /**
     * @return Подготовлена для проведения ВИ лиц с огр. возм.. Свойство не может быть null.
     */
    @NotNull
    public boolean isPreparedForDisabledPeople()
    {
        return _preparedForDisabledPeople;
    }

    /**
     * @param preparedForDisabledPeople Подготовлена для проведения ВИ лиц с огр. возм.. Свойство не может быть null.
     */
    public void setPreparedForDisabledPeople(boolean preparedForDisabledPeople)
    {
        dirty(_preparedForDisabledPeople, preparedForDisabledPeople);
        _preparedForDisabledPeople = preparedForDisabledPeople;
    }

    /**
     * @return Число мест. Свойство не может быть null.
     */
    @NotNull
    public int getSize()
    {
        return _size;
    }

    /**
     * @param size Число мест. Свойство не может быть null.
     */
    public void setSize(int size)
    {
        dirty(_size, size);
        _size = size;
    }

    /**
     * @return Комментарий.
     */
    @Length(max=2000)
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrCampaignExamRoomGen)
        {
            if (withNaturalIdProperties)
            {
                setEnrollmentCampaign(((EnrCampaignExamRoom)another).getEnrollmentCampaign());
                setPlace(((EnrCampaignExamRoom)another).getPlace());
            }
            setResponsibleOrgUnit(((EnrCampaignExamRoom)another).getResponsibleOrgUnit());
            setExamRoomType(((EnrCampaignExamRoom)another).getExamRoomType());
            setPreparedForDisabledPeople(((EnrCampaignExamRoom)another).isPreparedForDisabledPeople());
            setSize(((EnrCampaignExamRoom)another).getSize());
            setComment(((EnrCampaignExamRoom)another).getComment());
        }
    }

    public INaturalId<EnrCampaignExamRoomGen> getNaturalId()
    {
        return new NaturalId(getEnrollmentCampaign(), getPlace());
    }

    public static class NaturalId extends NaturalIdBase<EnrCampaignExamRoomGen>
    {
        private static final String PROXY_NAME = "EnrCampaignExamRoomNaturalProxy";

        private Long _enrollmentCampaign;
        private Long _place;

        public NaturalId()
        {}

        public NaturalId(EnrEnrollmentCampaign enrollmentCampaign, UniplacesPlace place)
        {
            _enrollmentCampaign = ((IEntity) enrollmentCampaign).getId();
            _place = ((IEntity) place).getId();
        }

        public Long getEnrollmentCampaign()
        {
            return _enrollmentCampaign;
        }

        public void setEnrollmentCampaign(Long enrollmentCampaign)
        {
            _enrollmentCampaign = enrollmentCampaign;
        }

        public Long getPlace()
        {
            return _place;
        }

        public void setPlace(Long place)
        {
            _place = place;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrCampaignExamRoomGen.NaturalId) ) return false;

            EnrCampaignExamRoomGen.NaturalId that = (NaturalId) o;

            if( !equals(getEnrollmentCampaign(), that.getEnrollmentCampaign()) ) return false;
            if( !equals(getPlace(), that.getPlace()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEnrollmentCampaign());
            result = hashCode(result, getPlace());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEnrollmentCampaign());
            sb.append("/");
            sb.append(getPlace());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrCampaignExamRoomGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrCampaignExamRoom.class;
        }

        public T newInstance()
        {
            return (T) new EnrCampaignExamRoom();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "place":
                    return obj.getPlace();
                case "responsibleOrgUnit":
                    return obj.getResponsibleOrgUnit();
                case "examRoomType":
                    return obj.getExamRoomType();
                case "preparedForDisabledPeople":
                    return obj.isPreparedForDisabledPeople();
                case "size":
                    return obj.getSize();
                case "comment":
                    return obj.getComment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrEnrollmentCampaign) value);
                    return;
                case "place":
                    obj.setPlace((UniplacesPlace) value);
                    return;
                case "responsibleOrgUnit":
                    obj.setResponsibleOrgUnit((OrgUnit) value);
                    return;
                case "examRoomType":
                    obj.setExamRoomType((EnrExamRoomType) value);
                    return;
                case "preparedForDisabledPeople":
                    obj.setPreparedForDisabledPeople((Boolean) value);
                    return;
                case "size":
                    obj.setSize((Integer) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "place":
                        return true;
                case "responsibleOrgUnit":
                        return true;
                case "examRoomType":
                        return true;
                case "preparedForDisabledPeople":
                        return true;
                case "size":
                        return true;
                case "comment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "place":
                    return true;
                case "responsibleOrgUnit":
                    return true;
                case "examRoomType":
                    return true;
                case "preparedForDisabledPeople":
                    return true;
                case "size":
                    return true;
                case "comment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrollmentCampaign":
                    return EnrEnrollmentCampaign.class;
                case "place":
                    return UniplacesPlace.class;
                case "responsibleOrgUnit":
                    return OrgUnit.class;
                case "examRoomType":
                    return EnrExamRoomType.class;
                case "preparedForDisabledPeople":
                    return Boolean.class;
                case "size":
                    return Integer.class;
                case "comment":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrCampaignExamRoom> _dslPath = new Path<EnrCampaignExamRoom>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrCampaignExamRoom");
    }
            

    /**
     * @return ПК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignExamRoom#getEnrollmentCampaign()
     */
    public static EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Помещение. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignExamRoom#getPlace()
     */
    public static UniplacesPlace.Path<UniplacesPlace> place()
    {
        return _dslPath.place();
    }

    /**
     * @return Ответственное терр. подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignExamRoom#getResponsibleOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> responsibleOrgUnit()
    {
        return _dslPath.responsibleOrgUnit();
    }

    /**
     * @return Тип аудитории. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignExamRoom#getExamRoomType()
     */
    public static EnrExamRoomType.Path<EnrExamRoomType> examRoomType()
    {
        return _dslPath.examRoomType();
    }

    /**
     * @return Подготовлена для проведения ВИ лиц с огр. возм.. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignExamRoom#isPreparedForDisabledPeople()
     */
    public static PropertyPath<Boolean> preparedForDisabledPeople()
    {
        return _dslPath.preparedForDisabledPeople();
    }

    /**
     * @return Число мест. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignExamRoom#getSize()
     */
    public static PropertyPath<Integer> size()
    {
        return _dslPath.size();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignExamRoom#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignExamRoom#getPlaceLocationWithSize()
     */
    public static SupportedPropertyPath<String> placeLocationWithSize()
    {
        return _dslPath.placeLocationWithSize();
    }

    public static class Path<E extends EnrCampaignExamRoom> extends EntityPath<E>
    {
        private EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> _enrollmentCampaign;
        private UniplacesPlace.Path<UniplacesPlace> _place;
        private OrgUnit.Path<OrgUnit> _responsibleOrgUnit;
        private EnrExamRoomType.Path<EnrExamRoomType> _examRoomType;
        private PropertyPath<Boolean> _preparedForDisabledPeople;
        private PropertyPath<Integer> _size;
        private PropertyPath<String> _comment;
        private SupportedPropertyPath<String> _placeLocationWithSize;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return ПК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignExamRoom#getEnrollmentCampaign()
     */
        public EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Помещение. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignExamRoom#getPlace()
     */
        public UniplacesPlace.Path<UniplacesPlace> place()
        {
            if(_place == null )
                _place = new UniplacesPlace.Path<UniplacesPlace>(L_PLACE, this);
            return _place;
        }

    /**
     * @return Ответственное терр. подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignExamRoom#getResponsibleOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> responsibleOrgUnit()
        {
            if(_responsibleOrgUnit == null )
                _responsibleOrgUnit = new OrgUnit.Path<OrgUnit>(L_RESPONSIBLE_ORG_UNIT, this);
            return _responsibleOrgUnit;
        }

    /**
     * @return Тип аудитории. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignExamRoom#getExamRoomType()
     */
        public EnrExamRoomType.Path<EnrExamRoomType> examRoomType()
        {
            if(_examRoomType == null )
                _examRoomType = new EnrExamRoomType.Path<EnrExamRoomType>(L_EXAM_ROOM_TYPE, this);
            return _examRoomType;
        }

    /**
     * @return Подготовлена для проведения ВИ лиц с огр. возм.. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignExamRoom#isPreparedForDisabledPeople()
     */
        public PropertyPath<Boolean> preparedForDisabledPeople()
        {
            if(_preparedForDisabledPeople == null )
                _preparedForDisabledPeople = new PropertyPath<Boolean>(EnrCampaignExamRoomGen.P_PREPARED_FOR_DISABLED_PEOPLE, this);
            return _preparedForDisabledPeople;
        }

    /**
     * @return Число мест. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignExamRoom#getSize()
     */
        public PropertyPath<Integer> size()
        {
            if(_size == null )
                _size = new PropertyPath<Integer>(EnrCampaignExamRoomGen.P_SIZE, this);
            return _size;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignExamRoom#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(EnrCampaignExamRoomGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignExamRoom#getPlaceLocationWithSize()
     */
        public SupportedPropertyPath<String> placeLocationWithSize()
        {
            if(_placeLocationWithSize == null )
                _placeLocationWithSize = new SupportedPropertyPath<String>(EnrCampaignExamRoomGen.P_PLACE_LOCATION_WITH_SIZE, this);
            return _placeLocationWithSize;
        }

        public Class getEntityClass()
        {
            return EnrCampaignExamRoom.class;
        }

        public String getEntityName()
        {
            return "enrCampaignExamRoom";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getPlaceLocationWithSize();
}
