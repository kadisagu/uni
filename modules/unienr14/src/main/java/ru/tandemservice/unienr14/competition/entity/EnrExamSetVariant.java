package ru.tandemservice.unienr14.competition.entity;

import org.tandemframework.core.common.IEntityDebugTitled;

import ru.tandemservice.unienr14.competition.entity.gen.EnrExamSetVariantGen;

/**
 * Настроенный набор ВИ
 */
public class EnrExamSetVariant extends EnrExamSetVariantGen implements IEntityDebugTitled
{
    @Override public String getEntityDebugTitle() {
        final IEnrExamSetVariantOwner owner = getOwner();
        if (null == owner) { return "(no-owner): " + getExamSet().getElementsTitle(); }
        return (owner.getEntityDebugTitle()+": "+ getExamSet().getElementsTitle());
    }

}