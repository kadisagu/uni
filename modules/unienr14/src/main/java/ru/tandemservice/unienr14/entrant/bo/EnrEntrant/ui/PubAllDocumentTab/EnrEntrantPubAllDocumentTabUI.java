/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubAllDocumentTab;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.bo.Person.util.ISecureRoleContext;
import org.tandemframework.shared.person.base.bo.PersonDocument.PersonDocumentManager;
import org.tandemframework.shared.person.base.bo.PersonDocument.ui.Add.PersonDocumentAdd;
import org.tandemframework.shared.person.base.bo.PersonEduDocument.ui.AddEdit.PersonEduDocumentAddEditUI;
import org.tandemframework.tapsupport.component.selection.CommonSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.uni.component.person.util.ISecureRoleContextOwner;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrantEduDocument.ui.AddEdit.EnrEntrantEduDocumentAddEdit;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrantEduDocument.ui.AddEdit.EnrEntrantEduDocumentAddEditUI;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.legacy.EnrPersonLegacyUtils;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 4/29/13
 */
@Input({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "entrant.id", required=true),
    @Bind(key = EnrEntrantPubAllDocumentTabUI.PARAM_INLINE, binding = "inline")
})
@Output({
    @Bind(key = ISecureRoleContext.SECURE_ROLE_CONTEXT, binding = ISecureRoleContextOwner.SECURE_ROLE_CONTEXT)
})
public class EnrEntrantPubAllDocumentTabUI extends UIPresenter implements ISecureRoleContextOwner, EnrEntrantAllDocumentListAddon.IOwner
{
    public static final String VIEW_PROPERTY_REQUEST_ATTACHMENTS = "requests";
    public static final String PARAM_INLINE = "inline";

    private EnrEntrant entrant = new EnrEntrant();
    private ISecureRoleContext secureRoleContext;
    private ISingleSelectModel _entrantRequestModel;

    private boolean inline;
    public boolean isInline() { return this.inline; }
    public void setInline(boolean inline) { this.inline = inline; }

    @Override
    public void onComponentRefresh()
    {
        setEntrant(IUniBaseDao.instance.get().getNotNull(EnrEntrant.class, getEntrant().getId()));
        setSecureRoleContext(EnrPersonLegacyUtils.getSecureRoleContext(getEntrant()));
        setEntrantRequestModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrEntrantRequest.class, "b").column(property("b"))
                    .where(eq(property(EnrEntrantRequest.entrant().fromAlias("b")), value(getEntrant())))
                    .order(property(EnrEntrantRequest.regNumber().fromAlias("b")));

                if (StringUtils.isNotEmpty(filter))
                    builder.where(likeUpper(property(EnrEntrantRequest.regNumber().fromAlias("b")), value(CoreStringUtils.escapeLike(filter, true))));
                if (o != null)
                    builder.where(eq(property(EnrEntrantRequest.id().fromAlias("b")), commonValue(o, PropertyType.LONG)));

                return new DQLListResultBuilder(builder, 50);
            }
        });
        prepareDataSource();
    }

    public void onClickAddEduInstitution()
    {
        _uiActivation.asRegionDialog(EnrEntrantEduDocumentAddEdit.class.getSimpleName())
            .parameter(PublisherActivator.PUBLISHER_ID_KEY, null)
            .parameter(EnrEntrantEduDocumentAddEditUI.BIND_ENTRANT, getEntrant().getId())
            .parameter(PersonEduDocumentAddEditUI.BIND_PERSON, getEntrant().getPerson().getId())
            .activate();
        ContextLocal.getSite().getDesktop().setRefreshScheduled(true);
    }

    public void onClickAddOlympiadDiploma()
    {
//        _uiActivation.asRegionDialog(EnrOlympiadDiplomaAddEdit.class.getSimpleName())
//            .parameter(EnrOlympiadDiplomaAddEditUI.BIND_ENTRANT_ID, getEntrant().getId()) TODO: SH-1904
//            .parameter(PUBLISHER_ID, null)
//            .activate();
    }

    public void onClickAddDocument()
    {
        _uiActivation.asRegionDialog(PersonDocumentAdd.class)
//                .parameter(PersonDocumentManager.BIND_PERSON_ID, getSecureRoleContext().getPersonId())
//                .parameter(PersonDocumentManager.BIND_CONTEXT_KEY, getSecureRoleContext().getPersonRoleName().toLowerCase())
                .parameter(PersonDocumentManager.BIND_PERSON_ROLE_ID, getSecureRoleContext().getSecuredObject().getId())
            .activate();
    }

    public void onClickSearch()
    {
        saveSettings();
        prepareDataSource();
    }

    public void onClickClear()
    {
        clearSettings();
        prepareDataSource();
    }

    // utils

    @Override
    public String getPrintPermissionKey()
    {
        return "enr14EntrantPubAllDocumentTabPrintDoc";
    }

    @Override
    public String getDeletePermissionKey()
    {
        return "enr14EntrantPubAllDocumentTabDeleteDoc";
    }

    @Override
    public String getEditPermissionKey()
    {
        return "enr14EntrantPubAllDocumentTabEditDoc";
    }

    private void prepareDataSource()
    {
        ((EnrEntrantAllDocumentListAddon) getConfig().getAddon(EnrEntrantAllDocumentListAddon.NAME)).prepareDataSource(true, null, null);
    }

    // getters and setters

    public EnrEntrant getEntrant()
    {
        return entrant;
    }

    public void setEntrant(EnrEntrant entrant)
    {
        this.entrant = entrant;
    }

    @Override
    public ISecureRoleContext getSecureRoleContext()
    {
        return secureRoleContext;
    }

    public void setSecureRoleContext(ISecureRoleContext secureRoleContext)
    {
        this.secureRoleContext = secureRoleContext;
    }

    public ISingleSelectModel getEntrantRequestModel()
    {
        return _entrantRequestModel;
    }

    public void setEntrantRequestModel(ISingleSelectModel entrantRequestModel)
    {
        _entrantRequestModel = entrantRequestModel;
    }
}
