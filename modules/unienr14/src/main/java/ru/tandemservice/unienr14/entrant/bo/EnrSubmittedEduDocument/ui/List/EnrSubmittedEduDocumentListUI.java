/* $Id$ */
package ru.tandemservice.unienr14.entrant.bo.EnrSubmittedEduDocument.ui.List;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.CommonBaseSettingsUtil;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilter;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilterType;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterFormConfig;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import org.tandemframework.tapsupport.component.selection.SingleSelectTextModel;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.entrant.bo.EnrSubmittedEduDocument.EnrSubmittedEduDocumentManager;
import ru.tandemservice.unienr14.entrant.bo.EnrSubmittedEduDocument.logic.EnrSubmittedEduDocumentDSHandler;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrOrganizationOriginalIn;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.EnrEntrantRequestManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.EnrEnrollmentCommissionManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.Date;

/**
 * @author azhebko
 * @since 31.07.2014
 */
public class EnrSubmittedEduDocumentListUI extends UIPresenter
{
    private static final String SETTINGS_LAST_NAME = "lastName";
    private static final String SETTINGS_FIRST_NAME = "firstName";
    private static final String SETTINGS_MIDDLE_NAME = "middleName";
    private static final String SETTINGS_PERSONAL_NUMBER = "personalNumber";
    private static final String SETTINGS_REQUEST_NUMBER = "requestNumber";
    private static final String SETTINGS_ORIGINAL_SUBMITTED_FROM = "originalSubmittedFrom";
    private static final String SETTINGS_ORIGINAL_SUBMITTED_TO = "originalSubmittedTo";
    private static final String SETTINGS_ORIGINAL_REJECTED_FROM = "originalRejectedFrom";
    private static final String SETTINGS_ORIGINAL_REJECTED_TO = "originalRejectedTo";
    private static final String SETTINGS_EDU_DOCUMENT_KIND = "eduDocumentKind";
    private static final String SETTINGS_EDU_DOCUMENT_SERIA = "eduDocumentSeria";
    private static final String SETTINGS_EDU_DOCUMENT_NUMBER = "eduDocumentNumber";


    private EnrEnrollmentCampaign _enrollmentCampaign;
    private EnrEnrollmentCommission _enrollmentCommission;
    private EnrEntrant _currentEntrant;
    private PersonEduDocument _currentEditEduDocument;
    private String _currentEditOrgOriginalInTitle;
    private SingleSelectTextModel _organizationOriginalInModel = EnrSubmittedEduDocumentManager.instance().dao().getOrganizationOriginalInDS();
    private boolean _hasGlobalPermissionForEnrollmentCommissions;

    @Override
    public void onComponentRefresh()
    {
        setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
        setHasGlobalPermissionForEnrollmentCommissions(EnrEnrollmentCommissionManager.instance().permissionDao().hasGlobalPermissionForEnrCommission(UserContext.getInstance().getPrincipalContext()));
        setEnrollmentCommission(EnrEnrollmentCommissionManager.instance().dao().getDefaultCommission(!isHasGlobalPermissionForEnrollmentCommissions()));
        CommonFilterAddon util = getCompetitionFilterUtil();
        util
                .configDoubleWidthFilters(false)
                .configUseEnableCheckbox(false)
                .configSettings(getSettingsKey());

        util.clearFilterItems();
        util
//                .addFilterItem(EnrCompetitionFilterAddon.REQUEST_TYPE, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
//                .addFilterItem(EnrCompetitionFilterAddon.COMPENSATION_TYPE, CommonFilterFormConfig.DEFAULT_SELECT_FILTER_CONFIG)
//                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_FORM, CommonFilterFormConfig.DEFAULT_SELECT_FILTER_CONFIG)
//                .addFilterItem(EnrCompetitionFilterAddon.COMPETITION_TYPE, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.ENR_ORG_UNIT, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
//                .addFilterItem(EnrCompetitionFilterAddon.FORMATIVE_ORG_UNIT, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
//                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SUBJECT, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
//                .addFilterItem(EnrCompetitionFilterAddon.EDU_PROGRAM, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
//                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SET, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
        ;

        configWhereFilters();
        onRefreshValues();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource
            .put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, getEnrollmentCampaign())
            .put(EnrSubmittedEduDocumentDSHandler.BIND_LAST_NAME, this.getSettings().get(SETTINGS_LAST_NAME))
            .put(EnrSubmittedEduDocumentDSHandler.BIND_FIRST_NAME, this.getSettings().get(SETTINGS_FIRST_NAME))
            .put(EnrSubmittedEduDocumentDSHandler.BIND_MIDDLE_NAME, this.getSettings().get(SETTINGS_MIDDLE_NAME))
            .put(EnrSubmittedEduDocumentDSHandler.BIND_PERSONAL_NUMBER, this.getSettings().get(SETTINGS_PERSONAL_NUMBER))
            .put(EnrSubmittedEduDocumentDSHandler.BIND_REQUEST_NUMBER, this.getSettings().get(SETTINGS_REQUEST_NUMBER))
            .put(EnrSubmittedEduDocumentDSHandler.BIND_ORIGINAL_SUBMITTED_FROM, this.getSettings().get(SETTINGS_ORIGINAL_SUBMITTED_FROM))
            .put(EnrSubmittedEduDocumentDSHandler.BIND_ORIGINAL_SUBMITTED_TO, this.getSettings().get(SETTINGS_ORIGINAL_SUBMITTED_TO))
            .put(EnrSubmittedEduDocumentDSHandler.BIND_ORIGINAL_REJECTED_FROM, this.getSettings().get(SETTINGS_ORIGINAL_REJECTED_FROM))
            .put(EnrSubmittedEduDocumentDSHandler.BIND_ORIGINAL_REJECTED_TO, this.getSettings().get(SETTINGS_ORIGINAL_REJECTED_TO))
            .put(EnrSubmittedEduDocumentDSHandler.BIND_EDU_DOCUMENT_KIND, this.getSettings().get(SETTINGS_EDU_DOCUMENT_KIND))
            .put(EnrSubmittedEduDocumentDSHandler.BIND_EDU_DOCUMENT_SERIA, this.getSettings().get(SETTINGS_EDU_DOCUMENT_SERIA))
            .put(EnrSubmittedEduDocumentDSHandler.BIND_EDU_DOCUMENT_NUMBER, this.getSettings().get(SETTINGS_EDU_DOCUMENT_NUMBER))
            .put(EnrSubmittedEduDocumentDSHandler.BIND_COMPETITION_UTIL, getCompetitionFilterUtil())
            .put(EnrSubmittedEduDocumentDSHandler.BIND_ENROLLMENT_COMMISSION, getEnrollmentCommission());
    }

    public void onEnrCampaignRefresh()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
    }

    public void onEnrEnrollmentCommissionRefresh()
    {
        EnrEnrollmentCommissionManager.instance().dao().saveDefaultCommission(getEnrollmentCommission());
        configWhereFilters();
        saveSettings();
    }

    public void onClickSearch()
    {
        CommonFilterAddon util = getCompetitionFilterUtil();
        if (util != null)
            util.saveSettings();

        saveSettings();
    }

    public void onClickClear()
    {
        CommonFilterAddon util = getCompetitionFilterUtil();
        if (util != null)
            util.clearSettings();

        CommonBaseSettingsUtil.clearSettingsExcept(getSettings(), EnrSubmittedEduDocumentDSHandler.BIND_ENROLLMENT_COMMISSION);
        configWhereFilters();
        onClickSearch();
    }

    public void onClickSwitchOriginalStatus()
    {
        EnrSubmittedEduDocumentManager.instance().dao().updateEntrantOriginalDocumentStatus(this.getListenerParameterAsLong(), getEnrollmentCampaign());
        EnrEntrant entrant = (EnrEntrant) ((DataWrapper)_uiConfig.getDataSourceRecordByListenerId(EnrSubmittedEduDocumentList.DS_SUBMITTED_EDU_DOCUMENT)).getProperty(EnrSubmittedEduDocumentDSHandler.VIEW_ENTRANT);
        EnrEntrantRequestManager.instance().dao().doUpdateEnrReqCompEnrollAvailableAndEduDocForEntrant(entrant);
    }

    public void onClickEdit()
    {
        DataWrapper wrapper = getConfig().getDataSource(EnrSubmittedEduDocumentList.DS_SUBMITTED_EDU_DOCUMENT).getRecordById(getListenerParameterAsLong());
        _currentEditEduDocument = wrapper.getWrapped();
        _currentEntrant = (EnrEntrant) wrapper.getProperty(EnrSubmittedEduDocumentDSHandler.VIEW_ENTRANT);
        EnrOrganizationOriginalIn orgOriginalIn = (EnrOrganizationOriginalIn) wrapper.getProperty(EnrSubmittedEduDocumentDSHandler.VIEW_ORG_ORIGINAL_IN);
        _currentEditOrgOriginalInTitle = orgOriginalIn == null ? null : orgOriginalIn.getTitle();
    }

    public void onClickSaveRow()
    {
        EnrSubmittedEduDocumentManager.instance().dao().saveOrUpdateOrganizationOriginalIn(getCurrentEntrant(), getCurrentEditEduDocument(), getCurrentEditOrgOriginalInTitle());
        onRefreshValues();
    }

    public void onClickCancelEdit()
    {
        onRefreshValues();
    }

    public void onRefreshValues()
    {
        _currentEditEduDocument = null;
        _currentEditOrgOriginalInTitle = null;
    }


    // Util
    public boolean isNothingSelected() { return null == getEnrollmentCampaign() || (!isHasGlobalPermissionForEnrollmentCommissions() && getEnrollmentCommission() == null); }

    public boolean isEditMode()
    {
        return null != getCurrentEditEduDocument();
    }

    public boolean isPersonEduDocumentInEditMode()
    {
        return null != getCurrentEditEduDocument() && getCurrentRow().getId().equals(getCurrentEditEduDocument().getId());
    }

    public boolean isEduDocumentOriginal()
    {
        return (boolean) getCurrentRow().getProperty(EnrSubmittedEduDocumentDSHandler.VIEW_ORIGINAL);
    }

    public boolean isOriginalVisible()
    {
        return !isReqCompVisible();
    }

    public boolean isReqCompVisible()
    {
        return TwinComboDataSourceHandler.NO_ID.equals(getEnrollmentCampaign().getSettings().getAccountingRulesOrigEduDoc());
    }

    private void configWhereFilters()
    {
        CommonFilterAddon util = getCompetitionFilterUtil();
        if (util != null)
        {
            util.clearWhereFilter();
            util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EnrCompetition.programSetOrgUnit().orgUnit().enrollmentCampaign(), getEnrollmentCampaign()));
        }
    }

    private CommonFilterAddon getCompetitionFilterUtil()
    {
        return (CommonFilterAddon) this.getConfig().getAddon(EnrSubmittedEduDocumentList.ADDON_COMPETITION_FILTER);
    }


    // Getters && Setters
    public EnrEnrollmentCampaign getEnrollmentCampaign() { return _enrollmentCampaign; }
    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign) { _enrollmentCampaign = enrollmentCampaign; }

    public EnrEnrollmentCommission getEnrollmentCommission()
    {
        return _enrollmentCommission;
    }

    public void setEnrollmentCommission(EnrEnrollmentCommission enrollmentCommission)
    {
        _enrollmentCommission = enrollmentCommission;
    }

    public boolean isHasGlobalPermissionForEnrollmentCommissions()
    {
        return _hasGlobalPermissionForEnrollmentCommissions;
    }

    public void setHasGlobalPermissionForEnrollmentCommissions(boolean hasGlobalPermissionForEnrollmentCommissions)
    {
        _hasGlobalPermissionForEnrollmentCommissions = hasGlobalPermissionForEnrollmentCommissions;
    }

    public DataWrapper getCurrentRow()
    {
        return getSupport().getDataSourceCurrentValue(EnrSubmittedEduDocumentList.DS_SUBMITTED_EDU_DOCUMENT);
    }

    public String getCurrentOrgOriginalInTitle()
    {
        EnrOrganizationOriginalIn orgOriginalIn = (EnrOrganizationOriginalIn) getCurrentRow().getProperty(EnrSubmittedEduDocumentDSHandler.VIEW_ORG_ORIGINAL_IN);
        return orgOriginalIn == null || isEduDocumentOriginal() ? "" : orgOriginalIn.getTitle();
    }

    public EnrEntrant getCurrentEntrant()
    {
        return _currentEntrant;
    }

    public void setCurrentEntrant(EnrEntrant currentEntrant)
    {
        _currentEntrant = currentEntrant;
    }

    public PersonEduDocument getCurrentEditEduDocument()
    {
        return _currentEditEduDocument;
    }

    public void setCurrentEditEduDocument(PersonEduDocument currentEditEduDocument)
    {
        _currentEditEduDocument = currentEditEduDocument;
    }

    public String getCurrentEditOrgOriginalInTitle()
    {
        return _currentEditOrgOriginalInTitle;
    }

    public void setCurrentEditOrgOriginalInTitle(String currentEditOrgOriginalInTitle)
    {
        _currentEditOrgOriginalInTitle = currentEditOrgOriginalInTitle;
    }

    public SingleSelectTextModel getOrganizationOriginalInModel()
    {
        return _organizationOriginalInModel;
    }
}