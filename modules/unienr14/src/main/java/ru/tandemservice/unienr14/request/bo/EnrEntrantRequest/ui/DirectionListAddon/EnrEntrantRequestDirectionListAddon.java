/* $Id:$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.DirectionListAddon;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.sec.runtime.SecurityRuntime;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantState;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.EnrEntrantRequestManager;
import ru.tandemservice.unienr14.request.bo.EnrRequestedCompetition.ui.AcceptedDateEdit.EnrRequestedCompetitionAcceptedDateEdit;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.bo.EnrSettings.ui.AdmissionRequirements.EnrSettingsAdmissionRequirements;

/**
 * @author oleyba
 * @since 5/4/13
 */
public class EnrEntrantRequestDirectionListAddon extends UIAddon
{
    public static final String NAME = "EnrEntrantRequestDirectionListAddon";

    private CommonPostfixPermissionModel sec;

    public interface IOwner {
        EnrEntrant getEntrant();
        boolean isAccessible();
        String getSecPostfix();
    }

    // constructor
    public EnrEntrantRequestDirectionListAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    // use it in presenter

    public StaticListDataSource<EnrRequestedCompetition> prepareDataSource(EnrEntrantRequest request)
    {
        setSec(new CommonPostfixPermissionModel(getOwner().getSecPostfix()));

        StaticListDataSource<EnrRequestedCompetition> dataSource = new StaticListDataSource<>();

        dataSource.addColumn(new SimpleColumn("Приоритет", EnrRequestedCompetition.priority().s()).setClickable(false).setOrderable(false).setWidth(5));
        dataSource.addColumn(new SimpleColumn("Вид приема", EnrRequestedCompetition.competition().type().shortTitle().s()).setClickable(false).setOrderable(false).setWidth(5));

        dataSource.addColumn(new PublisherLinkColumn("Набор ОП для приема", EnrRequestedCompetition.competition().programSetOrgUnit().programSet().title().s())
        .setRequired(true).setClickable(true).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Филиал", EnrRequestedCompetition.competition().programSetOrgUnit().orgUnit().institutionOrgUnit().orgUnit().shortTitleWithTopEmphasized().s()).setClickable(false).setOrderable(false).setWidth(5));
        dataSource.addColumn(new SimpleColumn("Форм. подр.", EnrRequestedCompetition.competition().programSetOrgUnit().formativeOrgUnit().shortTitleWithTopEmphasized().s()).setClickable(false).setOrderable(false).setWidth(5));
        dataSource.addColumn(new SimpleColumn("Форма обучения", EnrRequestedCompetition.competition().programSetOrgUnit().programSet().programForm().title().s()).setClickable(false).setOrderable(false).setWidth(5));
        dataSource.addColumn(new SimpleColumn("Вид обр. программы", EnrRequestedCompetition.competition().programSetOrgUnit().programSet().programSubject().subjectIndex().programKind().shortTitle().s()).setClickable(false).setOrderable(false).setWidth(5));
        dataSource.addColumn(new SimpleColumn("На базе", EnrRequestedCompetition.competition().eduLevelRequirement().shortTitle().s()).setClickable(false).setOrderable(false).setWidth(5));
        dataSource.addColumn(new SimpleColumn("Доп. условия", EnrRequestedCompetition.parametersTitle().s()).setClickable(false).setOrderable(false).setWidth(15));

        dataSource.addColumn(new SimpleColumn("Дата добавления", EnrRequestedCompetition.P_REG_DATE, DateFormatter.DATE_FORMATTER_WITH_TIME).setClickable(false).setOrderable(false).setRequired(true).setWidth(7));

        dataSource.addColumn(new SimpleColumn("Состояние", EnrRequestedCompetition.L_STATE + "." + EnrEntrantState.P_TITLE).setClickable(false).setOrderable(false).setWidth(5));
        dataSource.addColumn(new SimpleColumn("Примечание", EnrRequestedCompetition.P_COMMENT).setClickable(false).setOrderable(false).setWidth(20));

        dataSource.addColumn(new ToggleColumn("Отказ от зачисления", EnrRequestedCompetition.refusedToBeEnrolled())
            .setListener(NAME + ":onClickSetRefusedToBeEnrolled")
            .setPermissionKey(getSec().getPermission("refuseToBeEnrolled"))
            .setDisableHandler(entity -> !isAccessible()));
        dataSource.addColumn(new SimpleColumn("Дата отказа от зачисления", EnrRequestedCompetition.P_REFUSED_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false).setWidth(5));

        if(TwinComboDataSourceHandler.NO_ID.equals(getOwner().getEntrant().getEnrollmentCampaign().getSettings().getAccountingRulesOrigEduDoc()))
        {
            dataSource.addColumn(new ToggleColumn("Оригинал", EnrRequestedCompetition.originalDocumentHandedIn())
                    .setListener(NAME + ":onClickSetOriginalDocumentHandedIn")
                    .setPermissionKey(getSec().getPermission("originalDocumentHandedIn"))
                    .setDisableHandler(entity ->
                            !isAccessible()));
        }

        dataSource.addColumn(new ToggleColumn("Согласие на зачисление", EnrRequestedCompetition.accepted())
                .setListener(NAME + ":onClickSetAcceptedEnrollment")
                .setPermissionKey(getSec().getPermission("acceptEnrollment"))
                .setDisableHandler(entity -> !isAccessible()
                        || (EnrSettingsAdmissionRequirements.OPTION_ENROLL_RULES_CONTRACT_FIRST.equals(getOwner().getEntrant().getEnrollmentCampaign().getSettings().getEnrollmentRulesContract())
                        && !((EnrRequestedCompetition) entity).getCompetition().getType().getCompensationType().isBudget())
                        || (TwinComboDataSourceHandler.YES_ID.equals(getOwner().getEntrant().getEnrollmentCampaign().getSettings().getEnrollmentRulesBudget())
                        && ((EnrRequestedCompetition) entity).getCompetition().getType().getCompensationType().isBudget())));

        dataSource.addColumn(new DateColumn("Дата подачи согласия", EnrRequestedCompetition.acceptedDate()).setFormatter(DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false));
        dataSource.addColumn(new BooleanColumn("Согласие на зачисление (итоговое)", EnrRequestedCompetition.enrollmentAvailable()).setClickable(false).setOrderable(false));

        IEntityHandler deleteColumnDisableHandler = entity -> !isAccessible() || Boolean.TRUE.equals(entity.getProperty(EnrRequestedCompetition.request().takeAwayDocument()));
        IEntityHandler parallelColumnDisableHandler = entity -> {
            EnrCompetition competition = (EnrCompetition) entity.getProperty(EnrRequestedCompetition.competition().s());
            if (!EnrCompetitionTypeCodes.CONTRACT.equals(competition.getType().getCode()))
                return true;
            return !isAccessible() || Boolean.TRUE.equals(entity.getProperty(EnrRequestedCompetition.request().takeAwayDocument()));
        };
        dataSource.addColumn(new ToggleColumn("Паралл.", EnrRequestedCompetition.parallel().s())
        .toggleOnListener(NAME + ":onClickSetParallel").toggleOffListener(NAME + ":onClickSetParallel")
        .setDisableHandler(parallelColumnDisableHandler)
        .setPermissionKey(getSec().getPermission("editRequest")));
        dataSource.addColumn(new ToggleColumn("Проф. образ.", EnrRequestedCompetition.profileEducation().s())
        .toggleOnListener(NAME + ":onClickSetProfileEducation").toggleOffListener(NAME + ":onClickSetProfileEducation")
        .setPermissionKey(getSec().getPermission("editRequest")));
        dataSource.addColumn(new ToggleColumn("Индивид. ускоренное обучение", EnrRequestedCompetition.acceleratedLearning().s())
                .toggleOffListener(NAME + ":onClickSetAcceleratedLearning").toggleOnListener(NAME + ":onClickSetAcceleratedLearning")
                .setPermissionKey(getSec().getPermission("editRequest")));

        IEntityHandler directionEditColumnDisableHandler = entity -> !isAccessible()
                || (!CoreServices.securityService().check(getOwner().getEntrant(), ContextLocal.getUserContext().getPrincipalContext(), getSec().getPermission("refuseToBeEnrolled"))
                && (!CoreServices.securityService().check(getOwner().getEntrant(), ContextLocal.getUserContext().getPrincipalContext(), getSec().getPermission("acceptEnrollment"))
                || (EnrSettingsAdmissionRequirements.OPTION_ENROLL_RULES_CONTRACT_FIRST.equals(getOwner().getEntrant().getEnrollmentCampaign().getSettings().getEnrollmentRulesContract())
                && !((EnrRequestedCompetition) entity).getCompetition().getType().getCompensationType().isBudget())
                || (TwinComboDataSourceHandler.YES_ID.equals(getOwner().getEntrant().getEnrollmentCampaign().getSettings().getEnrollmentRulesBudget())
                && ((EnrRequestedCompetition) entity).getCompetition().getType().getCompensationType().isBudget())));

        dataSource.addColumn(new ActionColumn("Редактировать даты", ActionColumn.EDIT, NAME + ":onClickDirectionEdit").setDisableHandler(directionEditColumnDisableHandler));

        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, NAME + ":onClickDirectionDelete", "Удалить выбранный конкурс {0}?", EnrRequestedCompetition.competition().title().s())
        .setPermissionKey(getSec().getPermission("deleteCompetition")).setDisableHandler(deleteColumnDisableHandler));

        return dataSource;
    }

    // utils

    public boolean isAccessible() {
        return getOwner().isAccessible();
    }

    private IOwner getOwner() {
        return ((IOwner) getPresenter());
    }

    // listeners

    public void onClickDirectionEdit()
    {
        ContextLocal.getSite().getDesktop().setRefreshScheduled(true);
        getPresenter().getActivationBuilder().asRegionDialog(EnrRequestedCompetitionAcceptedDateEdit.class).parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong()).activate();
    }

    public void onClickDirectionDelete()
    {
        ContextLocal.getSite().getDesktop().setRefreshScheduled(true);
        EnrEntrantRequestManager.instance().dao().deleteRequestedCompetition(getListenerParameterAsLong());
    }

    public void onClickSetParallel()
    {
        ContextLocal.getSite().getDesktop().setRefreshScheduled(true);
        EnrEntrantRequestManager.instance().dao().doChangeParallel(getListenerParameterAsLong());
    }

    public void onClickSetProfileEducation()
    {
        ContextLocal.getSite().getDesktop().setRefreshScheduled(true);
        EnrEntrantRequestManager.instance().dao().doChangeProfileEducation(getListenerParameterAsLong());
    }


    public void onClickSetAcceleratedLearning()
    {
        ContextLocal.getSite().getDesktop().setRefreshScheduled(true);
        EnrEntrantRequestManager.instance().dao().doChangeAcceleratedLearning(getListenerParameterAsLong());
    }

    public void onClickSetOriginalDocumentHandedIn()
    {
        ContextLocal.getSite().getDesktop().setRefreshScheduled(true);
        EnrEntrantRequestManager.instance().dao().doChangeOriginalForReqComp(getListenerParameterAsLong());
        EnrEntrantRequestManager.instance().dao().doUpdateEnrReqCompEnrollAvailableAndEduDocForEntrant(getOwner().getEntrant());
    }

    public void onClickSetAcceptedEnrollment()
    {
        ContextLocal.getSite().getDesktop().setRefreshScheduled(true);
        EnrEntrantRequestManager.instance().dao().doChangeAccepted(getListenerParameterAsLong());
        EnrEntrantRequestManager.instance().dao().doUpdateEnrReqCompEnrollAvailableAndEduDocForEntrant(getOwner().getEntrant());
    }

    public void onClickSetRefusedToBeEnrolled() {
        ContextLocal.getSite().getDesktop().setRefreshScheduled(true);
        EnrEntrantRequestManager.instance().dao().doChangeRefusedToBeEnrolled(getListenerParameterAsLong());
    }

    // getters and setters

    public CommonPostfixPermissionModel getSec()
    {
        return sec;
    }

    public void setSec(CommonPostfixPermissionModel sec)
    {
        this.sec = sec;
    }
}
