/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EnrollmentCommissionMeetingProtocolAdd;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.process.*;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.*;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import org.tandemframework.tapsupport.component.selection.SimpleListResultBuilder;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantState;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantStateCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.report.bo.EnrReport.EnrReportManager;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportDateSelector;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportParallelSelector;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.ui.Pub.EnrReportBasePub;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.EnrReportPersonAdd;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import java.util.*;

/**
 * @author rsizonenko
 * @since 06.08.2014
 */
public class EnrReportEnrollmentCommissionMeetingProtocolAddUI extends UIPresenter {

    private static EnrEntrantState TAKE_DOUMENTS_AWAY_STATE = DataAccessServices.dao().getByCode(EnrEntrantState.class, EnrEntrantStateCodes.TAKE_DOCUMENTS_AWAY);

    private EnrEnrollmentCampaign enrollmentCampaign;
    private EnrReportDateSelector dateSelector = new EnrReportDateSelector();
    private EnrReportParallelSelector parallelSelector = new EnrReportParallelSelector();

    private boolean skipEmptyList;
    private boolean skipEmptyCompetition;
    private boolean replaceProgramSetTitle;
    private boolean skipProgramSetTitle;
    private boolean doNotShowCommissionResolution;

    private long reportId;

    private List<EnrEntrantState> reqCompStates = new ArrayList<>();

    @Override
    public void onComponentPrepareRender()
    {
        if (reportId != 0)
        {
            activatePublisher();
            reportId = 0L;
        }
    }

    public void setReportId(long reportId)
    {
        this.reportId = reportId;
    }

    @Override
    public void onComponentRefresh()
    {
        setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
        configUtil(getCompetitionFilterAddon());
    }

    private void validate()
    {
        if (dateSelector.getDateFrom().after(dateSelector.getDateTo()))
            _uiSupport.error("Дата, указанная в параметре \"Заявления с\" не должна быть позже даты в параметре \"Заявления по\".", "dateFrom");

        if(getUserContext().getErrorCollector().hasErrors())
            throw new ApplicationException();
    }

    public boolean isPrintEntrantNumberColumn()
    {
        // В РНИМУ нужет ровно тот же отчет, только плюс колонка с номерами абитуриента
        return false;
    }

    public void onClickApply() {
        validate();
        BusinessComponentUtils.runProcess(new BackgroundProcessThread("Формирование отчета", new BackgroundProcessBase() {
            @Override
            public ProcessResult run(final ProcessState state) {
                setReportId(EnrReportManager.instance().enrollmentCommissionMeetingProtocolDao().createReport(EnrReportEnrollmentCommissionMeetingProtocolAddUI.this));
                return null;
            }
        }, ProcessDisplayMode.unknown));
    }

    public void activatePublisher()
    {
        _uiActivation.asDesktopRoot(EnrReportBasePub.class)
                .parameter(PUBLISHER_ID, reportId)
                .activate();
        deactivate();
    }

    public void onChangeEnrollmentCampaign()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
        final EnrCompetitionFilterAddon entrantRequestUtil = getCompetitionFilterAddon();
        configUtilWhere(entrantRequestUtil);
        dateSelector.refreshDates(getEnrollmentCampaign());
    }

    // utils

    private void configUtilWhere(EnrCompetitionFilterAddon util)
    {
        util.clearWhereFilter();
        util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EnrCompetition.programSetOrgUnit().orgUnit().enrollmentCampaign(), getEnrollmentCampaign()));
    }

    private void configUtil(EnrCompetitionFilterAddon util)
    {

        util
                .configDoubleWidthFilters(false)
                .configUseEnableCheckbox(true)
                .configSettings(getSettingsKey());

        util.clearFilterItems();

        util
                .addFilterItem(EnrCompetitionFilterAddon.REQUEST_TYPE, new CommonFilterFormConfig(true, false, true, false, true, true))
                .addFilterItem(EnrCompetitionFilterAddon.COMPENSATION_TYPE, CommonFilterFormConfig.DEFAULT_CHECKBOX_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_FORM, CommonFilterFormConfig.DEFAULT_CHECKBOX_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.COMPETITION_TYPE, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.ENR_ORG_UNIT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.FORMATIVE_ORG_UNIT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SUBJECT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.EDU_PROGRAM, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SET, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG);


        util.configListener("onChangeValuesInUtil");
        util.configBaseUpdateOnChange("reqCompStates");


        util.saveSettings();

        configUtilWhere(util);
    }

    public void onChangeValuesInUtil()
    {
        setReqCompStates(Collections.<EnrEntrantState>emptyList());

        @SuppressWarnings("unchecked")
        final List<EnrEntrantState> values =  getReqCompStateModel().getValues(null);

        if (values.size() == 0) {
            return;
        }

        values.remove(TAKE_DOUMENTS_AWAY_STATE);
        setReqCompStates(values);
    }

    public CommonFilterSelectModel getReqCompStateModel()
    {
        return new CommonFilterSelectModel() {

            @Override
            protected IListResultBuilder createBuilder(String filter, Object o) {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrRequestedCompetition.class, "reqComp");

                builder.joinPath(DQLJoinType.inner, EnrRequestedCompetition.competition().fromAlias("reqComp"),"comp");

                getCompetitionFilterAddon().applyFilters(builder, "comp");


                builder
                        .where(eq(property("reqComp", EnrRequestedCompetition.request().entrant().enrollmentCampaign()), value(getEnrollmentCampaign())))
                        .where(not(eq(property("reqComp", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.ENTRANT_ARCHIVED))))
                        .where(not(eq(property("reqComp", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.OUT_OF_COMPETITION))))
                        .where(not(eq(property("reqComp", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.ACTIVE))))
                        .column(property("reqComp", EnrRequestedCompetition.state()))
                        ;

                if (!StringUtils.isEmpty(filter))
                    builder.where(likeUpper(property("reqComp", EnrRequestedCompetition.state().title()), value(CoreStringUtils.escapeLike(filter.toUpperCase()))));
                if (o != null) {
                    builder.where(in(property("reqComp", EnrRequestedCompetition.state().id()), o));
                }

                builder.order(property("reqComp", EnrRequestedCompetition.state().title()));

                final List<EnrEntrantState> list = DataAccessServices.dao().getList(builder);

                final Set<EnrEntrantState> states = new HashSet<>();

                states.addAll(list);

                return new SimpleListResultBuilder<>(states);
            }

        };
    }

    public EnrCompetitionFilterAddon getCompetitionFilterAddon()
    {
        return (EnrCompetitionFilterAddon) getConfig().getAddon(EnrReportPersonAdd.COMPETITION_FILTERS_ENTRANT_REQUEST);
    }

    // getters and setters


    public boolean isReplaceProgramSetTitle()
    {
        return replaceProgramSetTitle;
    }

    public void setReplaceProgramSetTitle(boolean replaceProgramSetTitle)
    {
        this.replaceProgramSetTitle = replaceProgramSetTitle;
    }

    public boolean isSkipEmptyCompetition()
    {
        return skipEmptyCompetition;
    }

    public void setSkipEmptyCompetition(boolean skipEmptyCompetition)
    {
        this.skipEmptyCompetition = skipEmptyCompetition;
    }

    public boolean isSkipEmptyList()
    {
        return skipEmptyList;
    }

    public void setSkipEmptyList(boolean skipEmptyList)
    {
        this.skipEmptyList = skipEmptyList;
    }

    public boolean isSkipProgramSetTitle()
    {
        return skipProgramSetTitle;
    }

    public void setSkipProgramSetTitle(boolean skipProgramSetTitle)
    {
        this.skipProgramSetTitle = skipProgramSetTitle;
    }

    public EnrReportDateSelector getDateSelector() {
        return dateSelector;
    }

    public EnrReportParallelSelector getParallelSelector() {
        return parallelSelector;
    }

    public boolean isDoNotShowCommissionResolution() {
        return doNotShowCommissionResolution;
    }

    public void setDoNotShowCommissionResolution(boolean doNotShowCommissionResolution) {
        this.doNotShowCommissionResolution = doNotShowCommissionResolution;
    }

    public List<EnrEntrantState> getReqCompStates() {
        return reqCompStates;
    }

    public void setReqCompStates(List<EnrEntrantState> reqCompStates) {
        this.reqCompStates = reqCompStates;
    }

    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        this.enrollmentCampaign = enrollmentCampaign;
    }
}