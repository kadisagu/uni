/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsEduProfileAdd.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsEduProfileAdd.EnrReportEntrantsEduProfileAddUI;

/**
 * @author rsizonenko
 * @since 20.06.2014
 */
public interface IEnrReportEntrantsEduProfileDao extends INeedPersistenceSupport {
    long createReport(EnrReportEntrantsEduProfileAddUI model);
}
