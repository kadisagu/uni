/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.logic;

import org.tandemframework.core.entity.IdentifiableWrapper;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.select.IEnrSelectionPrevEnrollmentFact;
import ru.tandemservice.unienr14.enrollment.entity.EnrModelStepItem;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;

/**
 * @author oleyba
 * @since 4/2/15
 */
public class EnrModelStepPrevEnrollmentFact extends IdentifiableWrapper implements IEnrSelectionPrevEnrollmentFact
{
    private EnrModelStepItem item;

    public EnrModelStepPrevEnrollmentFact(EnrModelStepItem item)
    {
        super(item.getId(), item.getStep().getDateStr());
        this.item = item;
    }

    @Override
    public EnrRequestedCompetition getRequestedCompetition()
    {
        return item.getEntity().getRequestedCompetition();
    }

    @Override
    public String getInfo()
    {
        return item.getStep().getDateStr();
    }
}

