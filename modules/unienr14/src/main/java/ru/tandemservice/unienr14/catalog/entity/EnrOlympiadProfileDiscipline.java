package ru.tandemservice.unienr14.catalog.entity;

import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import ru.tandemservice.unienr14.catalog.entity.gen.*;


/** @see ru.tandemservice.unienr14.catalog.entity.gen.EnrOlympiadProfileDisciplineGen */
public class EnrOlympiadProfileDiscipline extends EnrOlympiadProfileDisciplineGen implements IDynamicCatalogItem
{

    public static EntityComboDataSourceHandler defaultSelectDSHandler(String ownerId)
    {
        return new EntityComboDataSourceHandler(ownerId, EnrOlympiadProfileDiscipline.class)
                .titleProperty(EnrOlympiadProfileDiscipline.P_TITLE)
                .filter(EnrOlympiadProfileDiscipline.title())
                .order(EnrOlympiadProfileDiscipline.title());
    }
}