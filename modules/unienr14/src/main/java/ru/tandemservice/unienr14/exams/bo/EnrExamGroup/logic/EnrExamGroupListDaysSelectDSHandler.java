/**
 *$Id: EnrExamGroupListDaysSelectDSHandler.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.exams.bo.EnrExamGroup.logic;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unienr14.exams.bo.EnrExamGroup.ui.List.EnrExamGroupList;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroup;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 29.05.13
 */
public class EnrExamGroupListDaysSelectDSHandler extends DefaultComboDataSourceHandler
{
    public EnrExamGroupListDaysSelectDSHandler(String ownerId)
    {
        super(ownerId, EnrExamGroup.class);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final EnrEnrollmentCampaign enrCamp = context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
        final OrgUnit terrOrgUnit = context.get(EnrExamGroupList.BIND_ORG_UNIT);

        final List<DataWrapper> resultList = new ArrayList<>();

        final DQLSelectBuilder examGroupBuilder = new DQLSelectBuilder().fromEntity(EnrExamGroup.class, "g")
                .column(property(EnrExamGroup.id().fromAlias("g")))
                .column(property(EnrExamGroup.days().fromAlias("g")))
                .where(eq(property(EnrExamGroup.examGroupSet().enrollmentCampaign().fromAlias("g")), value(enrCamp)))
                .where(eq(property(EnrExamGroup.territorialOrgUnit().fromAlias("g")), value(terrOrgUnit)))
                .order(property(EnrExamGroup.days().fromAlias("g")));

        if (StringUtils.isNotEmpty(input.getComboFilterByValue()))
            examGroupBuilder.where(eq(property(EnrExamGroup.days().fromAlias("g")), value(Integer.valueOf(input.getComboFilterByValue()))));

        if (CollectionUtils.isNotEmpty(input.getPrimaryKeys()))
        {
            examGroupBuilder.where(in(property(EnrExamGroup.id().fromAlias("g")), input.getPrimaryKeys()));
            for (Object[] objects : examGroupBuilder.createStatement(context.getSession()).<Object[]>list())
            {
                final Long id = (Long) objects[0];
                final Integer days = (Integer) objects[1];

                resultList.add(new DataWrapper(id, getWrapperTitle(days)));
            }

            return ListOutputBuilder.get(input, resultList).build();
        }

        final Set<String> uniqSet = new HashSet<>();

        int first = 0;
        int max = 100;
        final DQLExecutionContext executionContext = new DQLExecutionContext(context.getSession());
        long count = examGroupBuilder.createCountStatement(executionContext).uniqueResult();
        while (true)
        {
            for (Object[] objects : examGroupBuilder.createStatement(executionContext).setFirstResult(first).setMaxResults(max).<Object[]>list())
            {
                final Long id = (Long) objects[0];
                final Integer days = (Integer) objects[1];

                final String wrapperTitle = getWrapperTitle(days);

                if (uniqSet.contains(wrapperTitle))
                    continue;

                resultList.add(new DataWrapper(id, wrapperTitle));

                uniqSet.add(wrapperTitle);
                if (resultList.size() == 50)
                    break;
            }

            first = first + max;
            if (first > count)
                break;
        }

        final DSOutput output = ListOutputBuilder.get(input, resultList).build();
        if (count > 50)
            output.setTotalSize((int) count);
        return output;
    }

    protected String getWrapperTitle(Integer days)
    {
        return days.toString();
    }
}
