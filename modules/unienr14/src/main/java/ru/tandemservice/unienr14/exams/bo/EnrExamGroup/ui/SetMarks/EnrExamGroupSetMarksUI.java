/**
 *$Id: EnrExamGroupSetMarksUI.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.exams.bo.EnrExamGroup.ui.SetMarks;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unienr14.catalog.entity.EnrAbsenceNote;
import ru.tandemservice.unienr14.exams.bo.EnrExamPassDiscipline.EnrExamPassDisciplineManager;
import ru.tandemservice.unienr14.exams.bo.EnrExamPassDiscipline.logic.IEnrExamPassDisciplineDao;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroup;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroupScheduleEvent;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;

import java.sql.SQLException;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 09.07.13
 */
@Input({
    @Bind(key = "examGroupId", binding = "examGroupId")
})
public class EnrExamGroupSetMarksUI extends UIPresenter
{
    private Long _examGroupId;
    private EnrExamGroup _examGroup;

    private Map<Long, EnrAbsenceNote> _absenceNoteMap = new HashMap<>();
    private Map<Long, Double> _markMap = new HashMap<>();

    @Override
    public void onComponentRefresh()
    {
        _examGroup = DataAccessServices.dao().getNotNull(EnrExamGroup.class, _examGroupId);

        for (EnrExamPassDiscipline discipline : getConfig().getDataSource(EnrExamGroupSetMarks.EXAM_PASS_DISCIPLINE_SEARCH_DS).<EnrExamPassDiscipline>getRecords())
        {
            _absenceNoteMap.put(discipline.getId(), discipline.getAbsenceNote());
            _markMap.put(discipline.getId(), discipline.getAbsenceNote() == null ? discipline.getMarkAsDouble() : new Double(0.0d));
        }
    }

    public void onClickApply()
    {
        List<Date> dates = DataAccessServices.dao().getList(new DQLSelectBuilder()
            .fromEntity(EnrExamGroupScheduleEvent.class, "e").column(property(EnrExamGroupScheduleEvent.examScheduleEvent().scheduleEvent().durationBegin().fromAlias("e")))
            .where(eq(property(EnrExamGroupScheduleEvent.examGroup().fromAlias("e")), value(_examGroup)))
            .order(property(EnrExamGroupScheduleEvent.examScheduleEvent().scheduleEvent().durationBegin().fromAlias("e")), OrderDirection.desc));

        if (dates.isEmpty()) {
            throw new ApplicationException("Не задана дата проведения вступительного испытания.");
        }

        final Date lastEventDate = dates.get(0);

        List<IEnrExamPassDisciplineDao.IExamPassDisciplineMarkData> wrappers = new ArrayList<>();

        for (final EnrExamPassDiscipline discipline : getConfig().getDataSource(EnrExamGroupSetMarks.EXAM_PASS_DISCIPLINE_SEARCH_DS).<EnrExamPassDiscipline>getRecords()) {
            final EnrAbsenceNote absenceNote = _absenceNoteMap.get(discipline.getId());
            final Double mark = null != absenceNote ? null : _markMap.get(discipline.getId());

            wrappers.add(new IEnrExamPassDisciplineDao.IExamPassDisciplineMarkData() {
                @Override public EnrExamPassDiscipline getDiscipline() { return discipline; }
                @Override public EnrAbsenceNote getAbsenceNote() { return absenceNote; }
                @Override public Long getMarkAsLong() { return mark == null ? null : ((long)(mark * 1000)); }
                @Override public Date getMarkDate() { return mark == null ? null : lastEventDate; }
            });
        }

        EnrExamPassDisciplineManager.instance().dao().updateExamPassDisciplineData(getExamGroup().getExamGroupSet().getEnrollmentCampaign(), wrappers);

        deactivate();
    }

    public void onChangeAbsenceNote()
    {
        final EnrAbsenceNote absenceNote = _absenceNoteMap.get(getListenerParameterAsLong());
        if (absenceNote != null) {
            if (absenceNote.isValid()) {
                _markMap.put(getListenerParameterAsLong(), null);
            } else {
                _markMap.put(getListenerParameterAsLong(), 0.0d);
            }
        }
            
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrExamGroupSetMarks.BIND_EXAM_GROUP_ID, _examGroupId);
    }

    // Getters & Setters

    public String getSticker()
    {
        return getConfig().getProperty("ui.sticker", _examGroup.getTitle());
    }

    public EnrAbsenceNote getAbsenceNote()
    {
        final Long id = getConfig().getDataSource(EnrExamGroupSetMarks.EXAM_PASS_DISCIPLINE_SEARCH_DS).getCurrentId();
        return id == null ? null : _absenceNoteMap.get(id);
    }

    public void setAbsenceNote(EnrAbsenceNote absenceNote)
    {
        final Long id = getConfig().getDataSource(EnrExamGroupSetMarks.EXAM_PASS_DISCIPLINE_SEARCH_DS).getCurrentId();
        if (id != null)
            _absenceNoteMap.put(id, absenceNote);
    }

    public Double getMark()
    {
        final Long id = getConfig().getDataSource(EnrExamGroupSetMarks.EXAM_PASS_DISCIPLINE_SEARCH_DS).getCurrentId();
        return id == null ? null : _markMap.get(id);
    }

    public void setMark(Double mark)
    {
        final Long id = getConfig().getDataSource(EnrExamGroupSetMarks.EXAM_PASS_DISCIPLINE_SEARCH_DS).getCurrentId();
        if (id != null)
            _markMap.put(id, mark);
    }

    public String getAbsenceNoteId()
    {
        return "absenceNoteId" + getConfig().getDataSource(EnrExamGroupSetMarks.EXAM_PASS_DISCIPLINE_SEARCH_DS).getCurrentId();
    }

    public String getMarkId()
    {
        return "markId" + getConfig().getDataSource(EnrExamGroupSetMarks.EXAM_PASS_DISCIPLINE_SEARCH_DS).getCurrentId();
    }

    // Accessors

    public Long getExamGroupId()
    {
        return _examGroupId;
    }

    public void setExamGroupId(Long examGroupId)
    {
        _examGroupId = examGroupId;
    }

    public EnrExamGroup getExamGroup()
    {
        return _examGroup;
    }

    public void setExamGroup(EnrExamGroup examGroup)
    {
        _examGroup = examGroup;
    }
}
