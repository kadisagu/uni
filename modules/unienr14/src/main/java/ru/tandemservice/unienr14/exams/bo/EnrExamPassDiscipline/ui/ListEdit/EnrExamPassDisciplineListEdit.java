/* $Id$ */
package ru.tandemservice.unienr14.exams.bo.EnrExamPassDiscipline.ui.ListEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author nvankov
 * @since 4/6/15
 */
@Configuration
public class EnrExamPassDisciplineListEdit extends BusinessComponentManager
{
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .create();
    }
}



    