/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.util.Icon;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;

/**
 * @author oleyba
 * @since 12/17/13
 */
@Configuration
public class EnrEnrollmentStepList extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
            .addDataSource(searchListDS("stepDS", stepDSColumns(), stepDSHandler()))
            .create();
    }

    @Bean
    public ColumnListExtPoint stepDSColumns()
    {
        return columnListExtPointBuilder("stepDS")
            .addColumn(textColumn("date", EnrEnrollmentStep.enrollmentDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order().clickable(true))
            .addColumn(textColumn("state", EnrEnrollmentStep.state().title()).order())
            .addColumn(textColumn("requestType", EnrEnrollmentStep.requestType().title()).order())
            .addColumn(actionColumn(DELETE_COLUMN_NAME, new Icon(DELETE_COLUMN_NAME), "onClickDelete")
                .alert(new FormattedMessage("stepDS.delete.alert", EnrEnrollmentStep.dateStr().s()))
                .permissionKey("enr14EnrollmentDeleteStage"))
            .create();
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> stepDSHandler()
    {
        return new EnrEnrollmentStepDSHandler(getName());
    }
}