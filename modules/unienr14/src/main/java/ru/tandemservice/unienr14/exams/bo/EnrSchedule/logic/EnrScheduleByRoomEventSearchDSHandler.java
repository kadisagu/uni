/**
 *$Id: EnrScheduleByRoomEventSearchDSHandler.java 33636 2014-04-16 04:31:39Z nfedorovskih $
 */
package ru.tandemservice.unienr14.exams.bo.EnrSchedule.logic;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.NumberAsStringComparator;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unienr14.exams.bo.EnrSchedule.EnrScheduleManager;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroup;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroupScheduleEvent;
import ru.tandemservice.unienr14.exams.entity.EnrExamScheduleEvent;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignExamRoom;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 04.06.13
 */
public class EnrScheduleByRoomEventSearchDSHandler extends DefaultSearchDataSourceHandler
{
    public static final String BIND_DATE_FROM = "dateFrom";
    public static final String BIND_DATE_TO = "dateTo";
    public static final String BIND_EXAM_ROOM_LIST = "examRoomList";

    public static final String V_PROP_DISCIPLINE ="viewPropDiscipline";
    public static final String V_PROP_EXAM_GROUPS = "viewPropExamGroups";
    public static final String V_PROP_OCCUPIED_STR = "viewPropOccupied";
    public static final String V_PROP_OCCUPIED_EXCESS = "viewPropOccupExcess";
    public static final String V_PROP_DISABLE_EDIT = "disableEdit";

    public EnrScheduleByRoomEventSearchDSHandler(String ownerId)
    {
        super(ownerId, EnrExamScheduleEvent.class);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EnrExamScheduleEvent.class, "e").column(property("e"))
                .order(property(EnrExamScheduleEvent.scheduleEvent().durationBegin().fromAlias("e")), OrderDirection.asc);

        filters(dql, "e", input, context);
        final DSOutput output = DQLSelectOutputBuilder.get(input, dql, context.getSession()).pageable(true).build();

        return wrap(output, input, context);
    }

    /**
     * @param dql builder from EnrExamScheduleEvent
     */
    protected void filters(DQLSelectBuilder dql, String alias, DSInput input, ExecutionContext context)
    {
        final Date dateFrom = context.get(BIND_DATE_FROM);
        final Date dateTo = context.get(BIND_DATE_TO);
        final List<EnrCampaignExamRoom> examRoomList = context.get(BIND_EXAM_ROOM_LIST);

        if (CollectionUtils.isNotEmpty(examRoomList)) {
            dql.where(in(property(EnrExamScheduleEvent.examRoom().fromAlias(alias)), examRoomList));
        }

        if (dateFrom != null)
            dql.where(ge(property(EnrExamScheduleEvent.scheduleEvent().durationBegin().fromAlias(alias)), value(CoreDateUtils.getDayFirstTimeMoment(dateFrom), PropertyType.TIMESTAMP)));
        if (dateTo != null)
            dql.where(lt(property(EnrExamScheduleEvent.scheduleEvent().durationBegin().fromAlias(alias)),
                         value(CoreDateUtils.getDayFirstTimeMoment(CoreDateUtils.add(dateTo, Calendar.DAY_OF_YEAR, 1)), PropertyType.TIMESTAMP)));
    }

    protected DSOutput wrap(DSOutput output, DSInput input, ExecutionContext context)
    {
        final Map<Long, List<EnrExamGroup>> event2ExGroupsMap = SafeMap.get(ArrayList.class);
        final Map<Long, MutableInt> examGroup2EnrCountMap = SafeMap.get(MutableInt.class);
        BatchUtils.execute(output.getRecordIds(), 128, new BatchUtils.Action<Long>()
        {
            @Override
            public void execute(Collection<Long> elements)
            {
                final List<Long> examDiscDataList = new DQLSelectBuilder().fromEntity(EnrExamPassDiscipline.class, "b").column(property(EnrExamPassDiscipline.examGroup().id().fromAlias("b")))
                        .where(in(property(EnrExamPassDiscipline.examGroup().id().fromAlias("b")),
                                new DQLSelectBuilder().fromEntity(EnrExamGroupScheduleEvent.class, "b").column(property(EnrExamGroupScheduleEvent.examGroup().id().fromAlias("b")))
                                        .where(in(property(EnrExamGroupScheduleEvent.examScheduleEvent().fromAlias("b")), elements))
                                        .buildQuery()))
                        .createStatement(context.getSession()).list();

                final List<Object[]> groupEventDataList = new DQLSelectBuilder().fromEntity(EnrExamGroupScheduleEvent.class, "b")
                        .column(property(EnrExamGroupScheduleEvent.examScheduleEvent().id().fromAlias("b")))
                        .column(property(EnrExamGroupScheduleEvent.examGroup().fromAlias("b")))
                        .where(in(property(EnrExamGroupScheduleEvent.examScheduleEvent().fromAlias("b")), elements))
                        .createStatement(context.getSession()).list();

                for (Object[] objects : groupEventDataList)
                {
                    final Long eventId = (Long) objects[0];
                    final EnrExamGroup examGroup = (EnrExamGroup) objects[1];

                    event2ExGroupsMap.get(eventId)
                            .add(examGroup);
                }

                for (Long examGroupId :examDiscDataList)
                    examGroup2EnrCountMap.get(examGroupId)
                            .increment();
            }
        });

        final Comparator<EnrExamGroup> examGroupComparator = new Comparator<EnrExamGroup>()
        {
            @Override
            public int compare(EnrExamGroup o1, EnrExamGroup o2)
            {
                return NumberAsStringComparator.INSTANCE.compare(o1.getTitle(), o2.getTitle());
            }
        };

        for (List<EnrExamGroup> list : event2ExGroupsMap.values())
            Collections.sort(list, examGroupComparator);

        for (DataWrapper wrapper : DataWrapper.wrap(output))
        {
            final EnrExamScheduleEvent event = wrapper.getWrapped();

            wrapper.setProperty(V_PROP_DISCIPLINE, event.getDiscipline().getTitle() + " (" + event.getPassForm().getTitle() + ")");
            wrapper.setProperty(V_PROP_EXAM_GROUPS, getEventExamGroupsStr(event2ExGroupsMap.get(event.getId()), examGroup2EnrCountMap));
            wrapper.setProperty(V_PROP_OCCUPIED_STR, getEventOccupiedStr(event, event2ExGroupsMap.get(event.getId()), examGroup2EnrCountMap));
            wrapper.setProperty(V_PROP_OCCUPIED_EXCESS, isEventOccupiedExcess(event, event2ExGroupsMap.get(event.getId()), examGroup2EnrCountMap));
            wrapper.setProperty(V_PROP_DISABLE_EDIT, getDisableEdit(event2ExGroupsMap.get(event.getId()), examGroup2EnrCountMap));
        }

        return output;
    }

    /**
     * Формирует строку: через запятую выводится перечень ЭГ, связанных с ячейкой раписания.
     * ЭГ отсортированы по названию.
     * @param examGroupList список ЭГ связанных с событием
     * @param examGroup2EnrCountMap количество ДДС в ЭГ
     * @return ЭГ выводится в формате "[название ЭГ] ([число ДДС в ЭГ]/[размер ЭГ])"
     */
    protected String getEventExamGroupsStr(List<EnrExamGroup> examGroupList, Map<Long, MutableInt> examGroup2EnrCountMap)
    {
        StringBuilder string = new StringBuilder();
        for (EnrExamGroup examGroup : examGroupList)
        {
            if (string.length() > 0)
                string.append(", ");

            string
                    .append(examGroup.getTitle())
                    .append(" (")
                    .append(examGroup2EnrCountMap.get(examGroup.getId()))
                    .append("/")
                    .append(examGroup.getSize())
                    .append(")");
        }

        return string.toString();
    }

    /**
     * Формируется строка с данными о свободных местах в событии.
     * @param event событие в расписании ВИ
     * @param examGroupList список ЭГ связанных с событием
     * @param examGroup2EnrCountMap количество ДДС в ЭГ
     * @return строка по формату из EnrScheduleManager.instance().getEventRoomSizeStr()
     */
    protected String getEventOccupiedStr(EnrExamScheduleEvent event, List<EnrExamGroup> examGroupList, Map<Long, MutableInt> examGroup2EnrCountMap)
    {
        int discCount = 0;
        for (EnrExamGroup examGroup : examGroupList)
            discCount += examGroup2EnrCountMap.get(examGroup.getId()).intValue();

        return EnrScheduleManager.instance().getEventRoomSizeStr(discCount, event.getSize());
    }

    protected boolean isEventOccupiedExcess(EnrExamScheduleEvent event, List<EnrExamGroup> examGroupList, Map<Long, MutableInt> examGroup2EnrCountMap)
    {
        int discCount = 0;
        for (EnrExamGroup examGroup : examGroupList)
            discCount += examGroup2EnrCountMap.get(examGroup.getId()).intValue();

        return discCount > event.getSize();
    }

    /**
     * Нельзя редактировать событие, если к нему прикреплена хотя бы одна ЭГ с ДДС.
     * @param examGroupList список ЭГ связанных с событием
     * @param examGroup2EnrCountMap количество ДДС в ЭГ
     * @return true, если ДДС в соответстующих ЭГ нет, иначе false
     */
    protected Boolean getDisableEdit(List<EnrExamGroup> examGroupList, Map<Long, MutableInt> examGroup2EnrCountMap)
    {
        for (EnrExamGroup examGroup : examGroupList)
        {
            if (examGroup2EnrCountMap.get(examGroup.getId()).intValue() > 0)
                return true;
        }

        return false;
    }
}
