package ru.tandemservice.unienr14.settings.entity;

import ru.tandemservice.unienr14.settings.entity.gen.*;

/**
 * Настройки мастера добавления заявления приемной кампании
 *
 * Хранит настройки полей, шагов и проверок для мастера добавления заявления в приемной кампании.
 */
public class EnrEnrollmentCampaignRequestWizardSettings extends EnrEnrollmentCampaignRequestWizardSettingsGen
{
    public EnrEnrollmentCampaignRequestWizardSettings() {}
    public EnrEnrollmentCampaignRequestWizardSettings(EnrEnrollmentCampaign enrollmentCampaign) {
        this.setEnrollmentCampaign(enrollmentCampaign);
    }
}