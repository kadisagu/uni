/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantOnExamAdd.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantOnExamAdd.EnrReportEntrantOnExamAddUI;

/**
 * @author rsizonenko
 * @since 23.05.2014
 */
public interface IEnrReportEntrantOnExamDao extends INeedPersistenceSupport
{
    Long createReport(EnrReportEntrantOnExamAddUI enrReportEntrantOnExamAddUI);
}
