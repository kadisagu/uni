/* $Id:$ */
package ru.tandemservice.unienr14.settings.bo.EnrCampaignEnrollmentStage.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.Date;

/**
 * @author oleyba
 * @since 3/17/14
 */
public interface IEnrCampaignEnrollmentStagesDao extends INeedPersistenceSupport
{
    void doAddDefaultDates(EnrEnrollmentCampaign enrEnrollmentCampaign);

    void doSaveEnrollmentStage(EnrEnrollmentCampaign enrollmentCampaign, EnrRequestType requestType, EnrCompetitionType competitionType, EduProgramForm programForm, Date enrollmentDate, boolean isCrimea);
}