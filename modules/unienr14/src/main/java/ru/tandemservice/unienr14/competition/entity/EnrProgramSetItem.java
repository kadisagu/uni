package ru.tandemservice.unienr14.competition.entity;

import org.tandemframework.core.common.ITitled;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;
import ru.tandemservice.unienr14.competition.entity.gen.EnrProgramSetItemGen;

/**
 * Образовательная программа в наборе
 */
public class EnrProgramSetItem extends EnrProgramSetItemGen implements ITitled
{
    public EnrProgramSetItem() { }

    public EnrProgramSetItem(EnrProgramSetBase programSet, EduProgramHigherProf program) {
        setProgramSet(programSet);
        setProgram(program);
    }

    @Override public String getTitle() {
        if (getProgram() == null) {
            return this.getClass().getSimpleName();
        }
        return getProgram().getTitle();
    }
}