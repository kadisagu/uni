package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.event;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.dsl.EntityPath;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import org.tandemframework.shared.commonbase.events.EventListenerLocker;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.entrant.entity.*;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm;
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSource;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;
import ru.tandemservice.unienr14.request.entity.*;

import java.util.HashSet;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class EnrEntrantBlockListenerBean {


    public static class Listener implements IDSetEventListener {

        private final String path;

        private Listener(EntityPath<EnrEntrant> entrantPath) {
            this.path = (null == entrantPath ? null : entrantPath.toString());
        }

        @Override public void onEvent(DSetEvent event)
        {
            final IEntityMeta entityMeta = event.getMultitude().getEntityMeta();
            final Number count = new DQLSelectBuilder().fromEntity(entityMeta.getEntityClass(), "x")
            .where(in(property("x.id"), event.getMultitude().getInExpression()))
            .where(eq(property(EnrEntrant.archival().fromAlias(null == this.path ? "x" : ("x."+this.path))), value(Boolean.TRUE)))
            .column(DQLFunctions.count(property("x.id")))
            .createStatement(event.getContext())
            .uniqueResult();

            if ((null != count) && (count.intValue() > 0)) {
                final EventListenerLocker.Handler<Listener> handler = LOCKER.handler();
                final Set<String> affectedProperties = new HashSet<String>(event.getMultitude().getAffectedProperties());
                affectedProperties.remove("version");

                if (EnrEntrant.class.isAssignableFrom(entityMeta.getEntityClass())) {
                    affectedProperties.remove(EnrEntrant.P_ARCHIVAL);
                }

                if (affectedProperties.size() > 0) {
                    if (!handler.handle(this, entityMeta, affectedProperties, count, event)) {
                        throw new ApplicationException(EnrEntrantManager.instance().getProperty("fatal.archived-entrant-change"), true);
                    }
                }
            }
        }
    }

    public static final EventListenerLocker<Listener> LOCKER = new EventListenerLocker<Listener>();

    @SuppressWarnings("unchecked")
    public void init() {

        Object rows[][] = {
            {EnrEntrant.class, null },
            {EnrEntrantAccessCourse.class, EnrEntrantAccessCourse.entrant() },
            {EnrEntrantAccessDepartment.class, EnrEntrantAccessDepartment.entrant() },
            {EnrEntrantInfoAboutUniversity.class, EnrEntrantInfoAboutUniversity.entrant() },
            {EnrEntrantStateExamResult.class, EnrEntrantStateExamResult.entrant() },
            {EnrEntrantBaseDocument.class, EnrEntrantBaseDocument.entrant() },
            {EnrEntrantRequest.class, EnrEntrantRequest.entrant() },
            {EnrRequestedCompetition.class, EnrRequestedCompetition.request().entrant() },
            {EnrRequestedProgram.class, EnrRequestedProgram.requestedCompetition().request().entrant() },
            {EnrEntrantBenefitProof.class, EnrEntrantBenefitProof.benefitStatement().entrant() },
            {EnrEntrantRequestAttachment.class, EnrEntrantRequestAttachment.entrantRequest().entrant() },
            {EnrChosenEntranceExam.class, EnrChosenEntranceExam.requestedCompetition().request().entrant() },
            {EnrChosenEntranceExamForm.class, EnrChosenEntranceExamForm.chosenEntranceExam().requestedCompetition().request().entrant() },
            {EnrExamPassDiscipline.class, EnrExamPassDiscipline.entrant() },
            {EnrEntrantMarkSource.class, EnrEntrantMarkSource.chosenEntranceExamForm().chosenEntranceExam().requestedCompetition().request().entrant() },
        };

        for (Object[] row: rows) {
            Class<? extends IEntity> entityClass = (Class<? extends IEntity>) row[0];
            EntityPath<EnrEntrant> entrantPath = (EntityPath<EnrEntrant>) row[1];

            Listener listener = new Listener(entrantPath);
            DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, entityClass, listener);
            DSetEventManager.getInstance().registerListener(DSetEventType.beforeUpdate, entityClass, listener);
            DSetEventManager.getInstance().registerListener(DSetEventType.beforeDelete, entityClass, listener);
        }
    }


}
