/* $Id:$ */
package ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;
import ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.EnrProgramAllocationManager;
import ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.ui.Pub.EnrProgramAllocationPub;
import ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.util.IEnrPAQuotaFreeWrapper;
import ru.tandemservice.unienr14.programAlloc.entity.EnrProgramAllocation;

import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 7/30/14
 */
public class EnrProgramDSHandler extends DefaultSearchDataSourceHandler
{
    public static final String VIEW_PROP_QUOTA = "quota";
    public static final String VIEW_PROP_USED = "used";

    public EnrProgramDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        EnrProgramAllocation allocation = DataAccessServices.dao().getNotNull(EnrProgramAllocation.class, (Long) context.get(EnrProgramAllocationPub.PARAM_ALLOCATION_ID));

        final DQLOrderDescriptionRegistry orderDescriptionRegistry = new DQLOrderDescriptionRegistry(EnrProgramSetItem.class, "m");
        final DQLSelectBuilder dql = orderDescriptionRegistry.buildDQLSelectBuilder().column(property("m"));
        dql.where(eq(property(EnrProgramSetItem.programSet().fromAlias("m")), value(allocation.getProgramSetOrgUnit().getProgramSet())));

        final DSOutput output = DQLSelectOutputBuilder.get(input, dql, context.getSession())
            .order(orderDescriptionRegistry)
            .pageable(false)
            .build();

        output.setCountRecord(Math.max(3, output.getTotalSize()));

        IEnrPAQuotaFreeWrapper freeQuotaWrapper = EnrProgramAllocationManager.instance().dao().getFreeQuota(allocation.getId());
        Map<Long, String> quotaHtmlDescMap = EnrProgramAllocationManager.instance().dao().getQuotaHTMLDescription(freeQuotaWrapper);

        for (DataWrapper wrapper: DataWrapper.wrap(output)) {
            wrapper.setProperty(VIEW_PROP_QUOTA, quotaHtmlDescMap.get(wrapper.getId()));
            wrapper.setProperty(VIEW_PROP_USED, freeQuotaWrapper.getQuotaUsed().getUsedMap().get(wrapper.getId()));
        }

        return output;
    }
}
