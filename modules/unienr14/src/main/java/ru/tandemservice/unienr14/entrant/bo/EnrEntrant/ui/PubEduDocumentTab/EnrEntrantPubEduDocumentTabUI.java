/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubEduDocumentTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.person.base.bo.Person.util.ISecureRoleContext;
import org.tandemframework.shared.person.base.bo.Person.util.PersonRoleSecModel;
import org.tandemframework.shared.person.base.bo.PersonEduDocument.PersonEduDocumentManager;
import org.tandemframework.shared.person.base.bo.PersonEduDocument.ui.AddEdit.PersonEduDocumentAddEditUI;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubEduDocumentTab.logic.EnrPersonEduDocumentDSHandler;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrantEduDocument.ui.AddEdit.EnrEntrantEduDocumentAddEdit;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrantEduDocument.ui.AddEdit.EnrEntrantEduDocumentAddEditUI;
import ru.tandemservice.unienr14.entrant.bo.EnrSubmittedEduDocument.EnrSubmittedEduDocumentManager;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.legacy.EnrPersonLegacyUtils;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.EnrEntrantRequestManager;

/**
 * @author oleyba
 * @since 4/22/13
 */
@SuppressWarnings("deprecation")
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "entrant.id")
})
public class EnrEntrantPubEduDocumentTabUI extends UIPresenter
{
    public static String P_MAIN_EDU_INSTITUTION = "mainEduInstitution";
    public static String P_MAIN_EDU_INSTITUTION_DISABLED = "mainEduInstitutionDisabled";

    private EnrEntrant entrant = new EnrEntrant();
    private ISecureRoleContext secureRoleContext;
    private PersonRoleSecModel _secModel;
    
    @Override
    public void onComponentRefresh()
    {
        setEntrant(IUniBaseDao.instance.get().getNotNull(EnrEntrant.class, getEntrant().getId()));
        setSecureRoleContext(EnrPersonLegacyUtils.getSecureRoleContext(getEntrant()));
        setSecModel(PersonRoleSecModel.instance(getEntrant().getPerson(), getSecureRoleContext().getSecuredObject(), getSecureRoleContext().getSecuredPostfix()));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrPersonEduDocumentDSHandler.PARAM_ENTRANT, getEntrant());
        dataSource.put(EnrPersonEduDocumentDSHandler.PARAM_PERSON_ROLE_CONTEXT, getSecureRoleContext());
//        dataSource.put(PersonEduInstitutionDSHandler.PARAM_PERSON, getEntrant().getPerson());
//        dataSource.put(PersonEduInstitutionDSHandler.PARAM_PERSON_ROLE_CONTEXT, getSecureRoleContext());
    }

	public void onToggleOnMainDocument()
	{
		PersonEduDocumentManager.instance().dao().makeDocumentMain(getListenerParameterAsLong(), getEntrant().getPerson());
	}

	public void onToggleOffMainDocument()
	{
		PersonEduDocumentManager.instance().dao().clearMainDocument(getEntrant().getPerson());
	}

    public void onClickSwitchOriginalStatus()
    {
        EnrSubmittedEduDocumentManager.instance().dao().updateEntrantOriginalDocumentStatus(this.getListenerParameterAsLong(), getEntrant().getEnrollmentCampaign());
        EnrEntrantRequestManager.instance().dao().doUpdateEnrReqCompEnrollAvailableAndEduDocForEntrant(getEntrant());
    }

    public void onClickEditDocument() {
        _uiActivation.asRegion(EnrEntrantEduDocumentAddEdit.class).top()
            .parameter(EnrEntrantEduDocumentAddEditUI.BIND_ENTRANT, getEntrant().getId())
            .parameter(PersonEduDocumentAddEditUI.BIND_PERSON, getEntrant().getPerson().getId())
            .parameter(PublisherActivator.PUBLISHER_ID_KEY, getListenerParameterAsLong())
            .activate();
    }

    public void onClickDelete() {
        PersonEduDocumentManager.instance().dao().deleteDocument(getListenerParameterAsLong());
    }

    public void onClickScanCopyDownload() {
        PersonEduDocument eduDocument = CommonDAO.DAO_CACHE.get().getNotNull(PersonEduDocument.class, getListenerParameterAsLong());
        if (eduDocument.getScanCopy() == null) {
            _uiSupport.setRefreshScheduled(true);
            throw new ApplicationException("К документу не приложена скан-копия.");
        }
        if (eduDocument.getScanCopy().getContent() == null)
            throw new ApplicationException("Файл скан-копии пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().document(eduDocument.getScanCopy()), false);
    }

    public void onClickAddEduInstitution()
    {
        _uiActivation.asRegion(EnrEntrantEduDocumentAddEdit.class).top()
            .parameter(PublisherActivator.PUBLISHER_ID_KEY, null)
            .parameter(EnrEntrantEduDocumentAddEditUI.BIND_ENTRANT, getEntrant().getId())
            .parameter(PersonEduDocumentAddEditUI.BIND_PERSON, getEntrant().getPerson().getId())
            .activate();
    }

    public boolean isViewMode()
    {
        return getEntrant() != null && getEntrant().isArchival();
    }

    public boolean isOriginalVisible()
    {
        return !isReqCompVisible();
    }

    public boolean isReqCompVisible()
    {
        return TwinComboDataSourceHandler.NO_ID.equals(getEntrant().getEnrollmentCampaign().getSettings().getAccountingRulesOrigEduDoc());
    }

    public EnrEntrant getEntrant()
    {
        return entrant;
    }

    public void setEntrant(EnrEntrant entrant)
    {
        this.entrant = entrant;
    }

    public ISecureRoleContext getSecureRoleContext()
    {
        return secureRoleContext;
    }

    public void setSecureRoleContext(ISecureRoleContext secureRoleContext)
    {
        this.secureRoleContext = secureRoleContext;
    }

    public PersonRoleSecModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(PersonRoleSecModel secModel)
    {
        _secModel = secModel;
    }
}
