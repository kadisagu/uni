package ru.tandemservice.unienr14.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Категория документа персоны"
 * Имя сущности : personDocumentCategory
 * Файл data.xml : unienr.catalog.data.xml
 */
public interface PersonDocumentCategoryCodes
{
    /** Константа кода (code) элемента : Свидетельство о смерти обоих или единственного родителя (title) */
    String CERTIFICATE_DEATH_PARENTS = "2016.01";
    /** Константа кода (code) элемента : Справка учреждения ЗАГСа о внесении сведений об отце со слов матери (title) */
    String SPRAVKA_UCHREJDENIYA_Z_A_G_SA_O_VNESENII_SVEDENIY_OB_OTTSE_SO_SLOV_MATERI = "2016.02";
    /** Константа кода (code) элемента : Обвинительный приговор суда с назначением наказания в виде лишения свободы обоих или единственного родителя (title) */
    String COURT_VERDICT_PARENTS = "2016.03";
    /** Константа кода (code) элемента : Письменный отказ одного или обоих родителей (title) */
    String PARENTAL_REFUSAL = "2016.04";
    /** Константа кода (code) элемента : Решение суда о лишении обоих или единственного родителя родительских прав (title) */
    String DEPRIVATION_RIGHTS = "2016.05";
    /** Константа кода (code) элемента : Решение суда о признании обоих или единственного родителя безвестно отсутствующими (title) */
    String RECOGNITION_MISSING = "2016.06";
    /** Константа кода (code) элемента : Справка из органов социальной защиты населения (title) */
    String SERTIFICATE_SOCIAL_SECURITY = "2016.07";
    /** Константа кода (code) элемента : Иной документ (title) */
    String ORPHAN_OTHER_DOC = "2016.08";
    /** Константа кода (code) элемента : Военнослужащие, участвовавшие в боевых действиях (title) */
    String SOLDIERS_FIGHTING = "2016.09";
    /** Константа кода (code) элемента : Военнослужащие, участвовавшие в разминировании (title) */
    String DEMINING = "2016.10";
    /** Константа кода (code) элемента : Военнослужащие, участвовавшие в доставке грузов в Афганистан (title) */
    String DELIVERY_AFGHANISTAN = "2016.11";
    /** Константа кода (code) элемента : Военнослужащие, участвовавшие в вылетах на боевые задания в Афганистан (title) */
    String COMBAT_MISSIONS_AFGHANISTAN = "2016.12";
    /** Константа кода (code) элемента : Диплом чемпиона/призера Олимпийских игр (title) */
    String OLYMPIC_MEDALIST = "2016.13";
    /** Константа кода (code) элемента : Диплом чемпиона/призера Паралимпийских игр (title) */
    String PARALYMPIC_MEDALIST = "2016.14";
    /** Константа кода (code) элемента : Диплом чемпиона/призера Сурдлимпийских игр (title) */
    String MEDALIST_DEAFLYMPICS = "2016.15";
    /** Константа кода (code) элемента : Диплом чемпиона мира (title) */
    String WORLD_CHAMPION = "2016.16";
    /** Константа кода (code) элемента : Диплом чемпиона Европы (title) */
    String EUROPEAN_CHAMPION = "2016.17";
    /** Константа кода (code) элемента : Документ, подтверждающий наличие гражданства СССР (title) */
    String SOVIET_CITIZENSHIP = "2016.18";
    /** Константа кода (code) элемента : Документ, подтверждающий проживание в прошлом на территории Российского государства, Российской республики, РСФСР, СССР или РФ (title) */
    String RESIDENCE_TERRITORY_R_F = "2016.19";
    /** Константа кода (code) элемента : Документ, подтверждающий родство по прямой восходящей линии (title) */
    String LINEAL_RELATIONSHIP = "2016.20";
    /** Константа кода (code) элемента : Документ, подтверждающий проживание за рубежом (title) */
    String LIVING_ABROAD = "2016.21";
    /** Константа кода (code) элемента : Иной документ (title) */
    String NATION_OTHER_DOC = "2016.22";
    /** Константа кода (code) элемента : Ребенок-инвалид (title) */
    String CHILD = "2016.23";
    /** Константа кода (code) элемента : I группа (title) */
    String FIRST = "2016.24";
    /** Константа кода (code) элемента : II группа (title) */
    String SECOND = "2016.25";
    /** Константа кода (code) элемента : III группа (title) */
    String THIRD = "2016.26";
    /** Константа кода (code) элемента : Инвалид с детства (title) */
    String CHILDHOOD = "2016.27";
    /** Константа кода (code) элемента : Инвалид вследствие военной травмы (title) */
    String MILITARY_INJURY = "2016.28";

    Set<String> CODES = ImmutableSet.of(CERTIFICATE_DEATH_PARENTS, SPRAVKA_UCHREJDENIYA_Z_A_G_SA_O_VNESENII_SVEDENIY_OB_OTTSE_SO_SLOV_MATERI, COURT_VERDICT_PARENTS, PARENTAL_REFUSAL, DEPRIVATION_RIGHTS, RECOGNITION_MISSING, SERTIFICATE_SOCIAL_SECURITY, ORPHAN_OTHER_DOC, SOLDIERS_FIGHTING, DEMINING, DELIVERY_AFGHANISTAN, COMBAT_MISSIONS_AFGHANISTAN, OLYMPIC_MEDALIST, PARALYMPIC_MEDALIST, MEDALIST_DEAFLYMPICS, WORLD_CHAMPION, EUROPEAN_CHAMPION, SOVIET_CITIZENSHIP, RESIDENCE_TERRITORY_R_F, LINEAL_RELATIONSHIP, LIVING_ABROAD, NATION_OTHER_DOC, CHILD, FIRST, SECOND, THIRD, CHILDHOOD, MILITARY_INJURY);
}
