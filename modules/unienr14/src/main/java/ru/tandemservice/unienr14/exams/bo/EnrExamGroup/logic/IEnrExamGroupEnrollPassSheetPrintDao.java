/**
 *$Id: IEnrExamGroupEnrollPassSheetPrintDao.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.exams.bo.EnrExamGroup.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.rtf.document.RtfDocument;

/**
 * @author Alexander Shaburov
 * @since 08.07.13
 */
public interface IEnrExamGroupEnrollPassSheetPrintDao extends INeedPersistenceSupport
{
    /**
     * Печатать ведомость экзам. группы.
     * @param examGroupId id ЭГ
     * @return печатная форма
     */
    RtfDocument createEnrPassSheetRtf(long examGroupId);

    /**
     * Печатать шифрованную ведомость экзам. группы.
     * @param examGroupId id ЭГ
     * @return печатная форма
     */
    RtfDocument createEnrPassSheetCodedRtf(long examGroupId);

    /**
     * Печатать ведомость экзам. группы с оценками.
     * @param examGroupId id ЭГ
     * @return печатная форма
     */
    RtfDocument createEnrPassSheetMarksRtf(long examGroupId);
}
