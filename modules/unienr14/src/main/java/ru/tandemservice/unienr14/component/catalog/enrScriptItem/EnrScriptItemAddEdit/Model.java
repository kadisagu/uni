/* $Id$ */
// Copyright 2006-2012 Tandem Service Software
package ru.tandemservice.unienr14.component.catalog.enrScriptItem.EnrScriptItemAddEdit;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultScriptCatalogAddEdit.DefaultScriptCatalogAddEditModel;
import ru.tandemservice.unienr14.catalog.entity.IEnrScriptItem;

/**
 * @author Vasily Zhukov
 * @since 22.12.2011
 */
@Input({
        @Bind(key = "useUserTemplate", binding = "useUserTemplate"),
        @Bind(key = "useUserScript", binding = "useUserScript")
})
public class Model<T extends ICatalogItem & IScriptItem & IEnrScriptItem> extends DefaultScriptCatalogAddEditModel<T>
{
}