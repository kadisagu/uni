package ru.tandemservice.unienr14.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x8x1_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrPersonEduDocumentRel

		// свойство registrationDate стало формулой
		{
			// удалить колонку
			tool.dropColumn("enr14_person_edu_doc_rel_t", "registrationdate_p");
		}

		// свойство seria стало формулой
		{
			// удалить колонку
			tool.dropColumn("enr14_person_edu_doc_rel_t", "seria_p");
		}

		// свойство number стало формулой
		{
			// удалить колонку
			tool.dropColumn("enr14_person_edu_doc_rel_t", "number_p");
		}

		// свойство issuanceDate стало формулой
		{
			// удалить колонку
			tool.dropColumn("enr14_person_edu_doc_rel_t", "issuancedate_p");
		}
    }
}