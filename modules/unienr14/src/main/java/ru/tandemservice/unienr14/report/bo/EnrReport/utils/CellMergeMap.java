/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.utils;

import jxl.JXLException;
import jxl.write.WritableSheet;

import java.util.ArrayList;
import java.util.List;

/**
 * @author rsizonenko
 * @since 11.06.2014
 */
public class CellMergeMap {
    private class Mapping
    {
        int c1;
        int c2;
        int r1;
        int r2;

        public Mapping(int c1, int r1, int c2,  int r2) {
            this.c1 = c1;
            this.c2 = c2;
            this.r1 = r1;
            this.r2 = r2;
        }

        public void makeMerge(WritableSheet sheet) throws JXLException
        {
            sheet.mergeCells(c1, r1, c2, r2);
        }
    }

    private List<Mapping> mappings = new ArrayList<>();

    public void addMapping(int c1, int r1, int c2, int r2) {
        mappings.add(new Mapping(c1, r1, c2, r2));
    }

    public List<Mapping> getMappings() {
        return mappings;
    }

    public void makeMerges(WritableSheet sheet) throws JXLException
    {
        for (Mapping mapping : mappings)
            mapping.makeMerge(sheet);
    }

}
