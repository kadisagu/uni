package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x3_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.3")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrEnrollmentCampaignSettings

		// создано обязательное свойство enrEntrantExamListPrintType
        if(tool.tableExists("enr14_campaign_settings_t") && !tool.columnExists("enr14_campaign_settings_t", "enrentrantexamlistprinttype_p"))
		{
			// создать колонку
			tool.createColumn("enr14_campaign_settings_t", new DBColumn("enrentrantexamlistprinttype_p", DBType.LONG));

			// задать значение по умолчанию
			java.lang.Long defaultEnrEntrantExamListPrintType = 1L;
			tool.executeUpdate("update enr14_campaign_settings_t set enrentrantexamlistprinttype_p=? where enrentrantexamlistprinttype_p is null", defaultEnrEntrantExamListPrintType);

			// сделать колонку NOT NULL
			tool.setColumnNullable("enr14_campaign_settings_t", "enrentrantexamlistprinttype_p", false);

		}


    }
}