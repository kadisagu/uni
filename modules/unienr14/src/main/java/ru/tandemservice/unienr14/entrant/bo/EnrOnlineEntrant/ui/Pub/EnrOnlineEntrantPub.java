/* $Id$ */
package ru.tandemservice.unienr14.entrant.bo.EnrOnlineEntrant.ui.Pub;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;

/**
 * @author Denis Katkov
 * @since 24.06.2016
 */
@Configuration
public class EnrOnlineEntrantPub extends BusinessComponentManager
{
}