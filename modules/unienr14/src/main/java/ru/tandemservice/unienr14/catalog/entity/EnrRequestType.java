package ru.tandemservice.unienr14.catalog.entity;

import com.google.common.collect.ImmutableList;
import org.tandemframework.common.catalog.entity.IActiveCatalogItem;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.BaseDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IPrioritizedCatalogItem;
import ru.tandemservice.unienr14.catalog.bo.EnrCatalogCommons.ui.RequestTypeItemPub.EnrCatalogCommonsRequestTypeItemPub;
import ru.tandemservice.unienr14.catalog.bo.EnrRequestType.EnrRequestTypeManager;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrRequestTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.gen.EnrRequestTypeGen;
import ru.tandemservice.unienr14.competition.entity.*;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Вид заявления
 */
public class EnrRequestType extends EnrRequestTypeGen implements IDynamicCatalogItem, IPrioritizedCatalogItem, IActiveCatalogItem
{
    public static final String PARAM_ENR_CAMPAIGN = EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN;

    public Class<? extends EnrProgramSetBase> getProgramSetClass() {
        switch (getCode()) {
            case EnrRequestTypeCodes.BS : return EnrProgramSetBS.class;
            case EnrRequestTypeCodes.MASTER : return EnrProgramSetMaster.class;
            case EnrRequestTypeCodes.HIGHER:
            case EnrRequestTypeCodes.POSTGRADUATE:
            case EnrRequestTypeCodes.TRAINEESHIP:
            case EnrRequestTypeCodes.INTERNSHIP: return EnrProgramSetHigher.class;
            case EnrRequestTypeCodes.SPO : return EnrProgramSetSecondary.class;
        }
        throw new IllegalArgumentException();
    }

    private static final List<String> HIDDEN_FIELDS = ImmutableList.of(P_USER_CODE);

    public static IDynamicCatalogDesc getUiDesc() {
        return new BaseDynamicCatalogDesc() {
            @Override public Set<Long> getEditDisabledItems(Set<Long> itemIds) {
                return itemIds;
            }
            @Override public boolean isActiveToggleEnabled(ICatalogItem item) {
                //return !item.getCode().equals(EnrRequestTypeCodes.HIGHER); // Утсаревший элемент, всегда дизаблим
                return true; // Сказали пока раздизаблить переключатель (DEV-6873)
            }
            @Override public String getViewComponentName() {
                return EnrCatalogCommonsRequestTypeItemPub.class.getSimpleName();
            }
            @Override public Collection<String> getHiddenFields() {
                return HIDDEN_FIELDS;
            }
        };
    }

    public static EntityComboDataSourceHandler defaultSelectDSHandler(String name, boolean onlyActive) {
        EntityComboDataSourceHandler defaultSelectDSHandler = new EntityComboDataSourceHandler(name, EnrRequestType.class)
                .filter(EnrRequestType.title())
                .order(EnrRequestType.priority());

        if(onlyActive)
        {
            defaultSelectDSHandler.where(EnrRequestType.enabled(), true);
        }

        return defaultSelectDSHandler;
    }

    public static EntityComboDataSourceHandler requestTypeEnrCampaignDSHandler(String name, boolean onlyActive) {
        return defaultSelectDSHandler(name, onlyActive)
                .customize((alias, dql, context, filter) ->
                {
                    EnrEnrollmentCampaign enrollmentCampaign = context.get(PARAM_ENR_CAMPAIGN);

                    if(enrollmentCampaign != null)
                    {
                        dql.where(in(property(alias), EnrRequestTypeManager.instance().dao().getRequestTypeList(enrollmentCampaign)));
                    }
                    else
                    {
                        dql.where(nothing());
                    }

                    return dql;
                });
    }

    public static EntityComboDataSourceHandler requestTypeProgramKindDSHandler(String name, boolean onlyActive) {
        return defaultSelectDSHandler(name, onlyActive)
                .customize((alias, dql, context, filter) ->
                {

                    dql.where(exists(
                            EnrRequestType2eduProgramKindRel.class,
                            EnrRequestType2eduProgramKindRel.programKind().programHigherProf().s(), true,
                            EnrRequestType2eduProgramKindRel.requestType().s(), property(alias)
                    ));

                    return dql;
                });
    }
}