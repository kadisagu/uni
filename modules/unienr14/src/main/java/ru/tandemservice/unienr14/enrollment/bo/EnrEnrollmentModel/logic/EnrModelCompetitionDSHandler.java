/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.logic;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.unienr14.catalog.entity.EnrTargetAdmissionKind;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.EnrEnrollmentModelManager;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentModel;
import ru.tandemservice.unienr14.enrollment.entity.EnrModelStep;
import ru.tandemservice.unienr14.enrollment.entity.EnrModelStepItem;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignTargetAdmissionKind;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 12/17/13
 */
public class EnrModelCompetitionDSHandler extends DefaultSearchDataSourceHandler
{
    public static final String VIEW_PROP_PLAN = "plan";
    public static final String VIEW_PROP_ENROLLED = "enrolled";
    public static final String VIEW_PROP_AVG_STATE_EXAM_MARK = "avgStateExamMark";
    public static final String VIEW_PROP_PASS_RATING = "passRating";


    public EnrModelCompetitionDSHandler(String ownerId)
    {
        super(ownerId);
    }

    public static final String BIND_ENR_MODEL = "model";
    public static final String BIND_ENR_STEP = "step";
    public static final String BIND_ENR_COMPETITION_UTIL = CommonFilterAddon.class.getSimpleName();

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context) {

        EnrEnrollmentModel model = context.get(BIND_ENR_MODEL);
        EnrModelStep step = context.get(BIND_ENR_STEP);
        CommonFilterAddon util = context.get(BIND_ENR_COMPETITION_UTIL);

        if (null == model) {
            if (null == step) throw new IllegalArgumentException();
            model = step.getModel();
        }

        if (model.getEnrollmentCampaign().getSettings().isTargetAdmissionCompetition()) {

            final DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EnrModelStepItem.class, "i")
                .joinPath(DQLJoinType.inner, EnrModelStepItem.entity().fromAlias("i"), "r")
                .joinEntity("i", DQLJoinType.left, EnrRequestedCompetitionTA.class, "rta", eq(property("rta"), property(EnrRatingItem.requestedCompetition().fromAlias("r"))))
                .joinPath(DQLJoinType.left, EnrRequestedCompetitionTA.targetAdmissionKind().fromAlias("rta"), "eta")
                .joinPath(DQLJoinType.left, EnrCampaignTargetAdmissionKind.targetAdmissionKind().fromAlias("eta"), "ta")
                .where(eq(property(EnrModelStepItem.step().model().fromAlias("i")), value(model)))
                .column(property(EnrRequestedCompetition.competition().fromAlias("r")))
                .column("ta");

            if (step != null) dql.where(eq(property(EnrModelStepItem.step().fromAlias("i")), value(step)));
            if (util != null) dql.where(in(property("r", EnrRequestedCompetition.competition().id()), util.getEntityIdsFilteredBuilder("alias").buildQuery()));

            Set<PairKey<EnrCompetition, EnrTargetAdmissionKind>> pairs = new HashSet<>();
            for (Object[] row : dql.createStatement(context.getSession()).<Object[]>list()) {
                pairs.add(new PairKey<>((EnrCompetition) row[0], (EnrTargetAdmissionKind) row[1]));
            }
            List<PairKey<EnrCompetition, EnrTargetAdmissionKind>> pairList = new ArrayList<>(pairs);
            Collections.sort(pairList, (o1, o2) -> {
                if (o1.equals(o2)) return 0;
                if (o1.getFirst().equals(o2.getFirst())) {
                    if (o1.getSecond() == null || o2.getSecond() == null) return 0;
                    return o1.getSecond().getTitle().compareTo(o2.getSecond().getTitle());
                }
                return o1.getFirst().getTitle().compareTo(o2.getFirst().getTitle());
            });

            DSOutput output = new DSOutput(input);
            output.setTotalSize(pairList.size());

            Map<MultiKey, EnrModelCompetitionWrapper> wrapperMap = new HashMap<>();
            List<Object> wrappers = new ArrayList<>();
            for (PairKey<EnrCompetition, EnrTargetAdmissionKind> pair : pairList.subList(output.getStartRecord(), Math.min(output.getStartRecord() + output.getCountRecord(), pairList.size()))) {
                EnrModelCompetitionWrapper wrapper = new EnrModelCompetitionWrapper(step, model, pair.getFirst(), pair.getSecond());
                wrappers.add(wrapper);
                wrapperMap.put(wrapper.key(), wrapper);
            }

            EnrEnrollmentModelManager.instance().dao().calcStats(step, model, wrapperMap);

            output.setRecordList(wrappers);
            return output;
        } else {
            DQLSelectBuilder itemBuilder = new DQLSelectBuilder()
                .fromEntity(EnrModelStepItem.class, "i")
                .where(eq(property(EnrModelStepItem.step().model().fromAlias("i")), value(model)))
                .where(eq(property(EnrModelStepItem.entity().requestedCompetition().competition().fromAlias("i")), property("k")));

            if (step != null) itemBuilder.where(eq(property(EnrModelStepItem.step().fromAlias("i")), value(step)));

            final DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EnrCompetition.class, "k")
                .column(property("k"))
                .where(exists(itemBuilder
                    .buildQuery()))
                .order(property(EnrCompetition.programSetOrgUnit().programSet().title().fromAlias("k")))
                .order(property(EnrCompetition.programSetOrgUnit().orgUnit().institutionOrgUnit().orgUnit().parent().fromAlias("k")), OrderDirection.desc)
                .order(property(EnrCompetition.programSetOrgUnit().orgUnit().institutionOrgUnit().orgUnit().territorialShortTitle().fromAlias("k")))
                .order(property(EnrCompetition.programSetOrgUnit().programSet().programForm().title().fromAlias("k")))
                .order(property(EnrCompetition.type().code().fromAlias("k")))
                .order(property(EnrCompetition.eduLevelRequirement().code().fromAlias("k")))
                .order(property(EnrCompetition.id().fromAlias("k")));

            if (util != null) dql.where(in(property("k", EnrCompetition.id()), util.getEntityIdsFilteredBuilder("alias").buildQuery()));

            DSOutput output = DQLSelectOutputBuilder.get(input, dql, context.getSession()).pageable(true).order().build();

            Map<MultiKey, EnrModelCompetitionWrapper> wrapperMap = new HashMap<>();
            List<Object> wrappers = new ArrayList<>();
            for (Object row : output.getRecordList()) {
                EnrCompetition competition = (EnrCompetition) row;
                EnrModelCompetitionWrapper wrapper = new EnrModelCompetitionWrapper(step, model, competition, null);
                wrappers.add(wrapper);
                wrapperMap.put(wrapper.key(), wrapper);
            }
            output.setRecordList(wrappers);

            EnrEnrollmentModelManager.instance().dao().calcStats(step, model, wrapperMap);

            return output;
        }
    }
}


