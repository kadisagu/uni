/**
 *$Id: EnrExamSetAddEditUI.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.exams.bo.EnrExamSet.ui.AddEdit;

import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.unienr14.catalog.entity.EnrExamType;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrExamTypeCodes;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.EnrExamSetManager;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.logic.ExamSetElementValuesSelectDSHandler;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.logic.IEnrExamSetDao;
import ru.tandemservice.unienr14.exams.entity.EnrExamSet;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.IEnrExamSetElementValue;

import java.util.*;

/**
 * @author Alexander Shaburov
 * @since 17.04.13
 */
@Input({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "entityHolder.id")
})
public class EnrExamSetAddEditUI extends UIPresenter
{
    private EntityHolder<EnrExamSet> _entityHolder = new EntityHolder<>(new EnrExamSet());

    private List<ExamSetElementWrapper> _rowList;
    private ExamSetElementWrapper _currentRow;
    private ExamSetElementWrapper _editedRow;
    private Integer _editedRowIndex;
    private boolean _rowAdded;

    @Override
    public void onComponentRefresh()
    {
        EnrExamSet examSet = _entityHolder.refresh();
        if (null == examSet.getEnrollmentCampaign()) {
            examSet.setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
        }

        if (_rowList == null)
            refreshExamSetElemetList(examSet);
    }

    private void refreshExamSetElemetList(EnrExamSet examSet)
    {
        _rowList = new ArrayList<>();
        if (null != examSet.getId()) {
            _rowList.addAll(CollectionUtils.collect(examSet.getElementList(), ExamSetElementWrapper::new));
        }

        _editedRow = null;
        _editedRowIndex = null;
        _rowAdded = false;
    }

    public void onClickApply()
    {
        EnrExamSetManager.instance().dao().saveOrUpdateExamSet(_entityHolder.getValue(), _rowList);
        deactivate();
    }

    public void onClickAddRow()
    {
        if (_editedRow != null)
            return;

        _editedRow = new ExamSetElementWrapper();
        _editedRow.setExamType(DataAccessServices.dao().getByCode(EnrExamType.class, EnrExamTypeCodes.USUAL));

        _rowList.add(new ExamSetElementWrapper());
        _editedRowIndex = _rowList.size() - 1;

        _rowAdded = true;
    }

    public void onClickEditRow()
    {
        if (_editedRow != null)
            return;

        ExamSetElementWrapper baseRow = findRow();

        _editedRow = new ExamSetElementWrapper(baseRow);
        _editedRowIndex = _rowList.indexOf(baseRow);
        _rowAdded = false;
    }

    public void onClickDeleteRow()
    {
        ExamSetElementWrapper row = findRow();
        if (null != row)
            _rowList.remove(row);
    }


    public void onClickMoveRowUp()
    {
        List<ExamSetElementWrapper> rows = getRowList();
        ExamSetElementWrapper baseRow = findRow();

        int index = rows.indexOf(baseRow);
        if (index > 0) { Collections.swap(rows, index-1, index); }
    }

    public void onClickMoveRowDown()
    {
        List<ExamSetElementWrapper> rows = getRowList();
        ExamSetElementWrapper baseRow = findRow();

        int index = rows.indexOf(baseRow);
        if (index < rows.size()-1) { Collections.swap(rows, index, index+1); }
    }


    public void onClickSaveRow()
    {
        if (validateRows().hasErrors())
            return;

        if (_editedRowIndex != null && _rowList.size() > _editedRowIndex && _editedRowIndex >= 0)
            _rowList.set(_editedRowIndex, _editedRow);

        _editedRow = null;
        _rowAdded = false;
    }

    public void onClickCancelEdit()
    {
        if (_rowAdded && !_rowList.isEmpty())
            _rowList.remove(_rowList.size() - 1);

        _editedRowIndex = null;
        _editedRow = null;
        _rowAdded = false;
    }

    public ErrorCollector validateRows()
    {
        final ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();

        final ArrayList<IEnrExamSetDao.IExamSetElementWrapper> rowList = new ArrayList<>(_rowList);
        if (_rowAdded)
        {
            rowList.add(_editedRow);
        }
        else
        {
            rowList.set(_editedRowIndex, _editedRow);
        }

        final Set<IEnrExamSetElementValue> wrapperSet = new HashSet<>();
        for (IEnrExamSetDao.IExamSetElementWrapper wrapper : rowList)
            if (!wrapperSet.add(wrapper.getElementValue()))
            {
                errorCollector.add(getConfig().getProperty("examSet.validate.uniqElementValue"), "elementValue", "examType");
                break;
            }

        return errorCollector;
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(ExamSetElementValuesSelectDSHandler.PROP_ENR_CAMP, getEnrCampaign());
    }

    // Getters & Setters

    public boolean isCurrentRowFirst() {
        ExamSetElementWrapper currentRow = getCurrentRow();
        if (null == currentRow) { return false; }

        List<ExamSetElementWrapper> rowList = getRowList();
        if (rowList.isEmpty()) { return false; }

        return currentRow.equals(rowList.get(0));
    }

    public boolean isCurrentRowLast() {
        ExamSetElementWrapper currentRow = getCurrentRow();
        if (null == currentRow) { return false; }

        List<ExamSetElementWrapper> rowList = getRowList();
        if (rowList.isEmpty()) { return false; }

        return currentRow.equals(rowList.get(rowList.size()-1));
    }


    private boolean isAddForm()
    {
        return _entityHolder.getId() == null;
    }

    public boolean isEditMode()
    {
        return _editedRow != null;
    }

    public boolean isCurrentRowInEditMode()
    {
        return _editedRow != null && _editedRowIndex.equals(_rowList.indexOf(_currentRow));
    }

    private ExamSetElementWrapper findRow()
    {
        final Long id = getListenerParameterAsLong();
        return CollectionUtils.find(getRowList(), object -> object.getId().equals(id));
    }

    public String getSticker()
    {
        if (isAddForm())
            return getConfig().getProperty("ui.sticker.add");
        else
            return getConfig().getProperty("ui.sticker.edit");
    }

    /* Generated */

    public EntityHolder<EnrExamSet> getEntityHolder()
    {
        return _entityHolder;
    }

    public void setEntityHolder(EntityHolder<EnrExamSet> entityHolder)
    {
        _entityHolder = entityHolder;
    }

    public EnrEnrollmentCampaign getEnrCampaign()
    {
        return getEntityHolder().getValue().getEnrollmentCampaign();
    }


    public List<ExamSetElementWrapper> getRowList()
    {
        return _rowList;
    }

    public void setRowList(List<ExamSetElementWrapper> rowList)
    {
        _rowList = rowList;
    }

    public ExamSetElementWrapper getCurrentRow()
    {
        return _currentRow;
    }

    public void setCurrentRow(ExamSetElementWrapper currentRow)
    {
        _currentRow = currentRow;
    }

    public ExamSetElementWrapper getEditedRow()
    {
        return _editedRow;
    }

    public void setEditedRow(ExamSetElementWrapper editedRow)
    {
        _editedRow = editedRow;
    }

    public Integer getEditedRowIndex()
    {
        return _editedRowIndex;
    }

    public void setEditedRowIndex(Integer editedRowIndex)
    {
        _editedRowIndex = editedRowIndex;
    }

    public boolean isRowAdded()
    {
        return _rowAdded;
    }

    public void setRowAdded(boolean rowAdded)
    {
        _rowAdded = rowAdded;
    }
}
