package ru.tandemservice.unienr14.competition.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unienr14.competition.entity.EnrExamSetVariant;
import ru.tandemservice.unienr14.competition.entity.IEnrExamSetVariantOwner;
import ru.tandemservice.unienr14.exams.entity.EnrExamSet;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Настроенный набор ВИ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrExamSetVariantGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.competition.entity.EnrExamSetVariant";
    public static final String ENTITY_NAME = "enrExamSetVariant";
    public static final int VERSION_HASH = -1774195505;
    private static IEntityMeta ENTITY_META;

    public static final String L_OWNER = "owner";
    public static final String L_EXAM_SET = "examSet";

    private IEnrExamSetVariantOwner _owner;     // Владелец настройки набора ВИ
    private EnrExamSet _examSet;     // Набор ВИ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * Поле неизменяемое: владелец остается за настроенным набором
     *
     * @return Владелец настройки набора ВИ.
     */
    public IEnrExamSetVariantOwner getOwner()
    {
        return _owner;
    }

    /**
     * @param owner Владелец настройки набора ВИ.
     */
    public void setOwner(IEnrExamSetVariantOwner owner)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && owner!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IEnrExamSetVariantOwner.class);
            IEntityMeta actual =  owner instanceof IEntity ? EntityRuntime.getMeta((IEntity) owner) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_owner, owner);
        _owner = owner;
    }

    /**
     * Поле изменяемое: настроенный набор можно перекинуть на другой набор ВИ
     *
     * @return Набор ВИ. Свойство не может быть null.
     */
    @NotNull
    public EnrExamSet getExamSet()
    {
        return _examSet;
    }

    /**
     * @param examSet Набор ВИ. Свойство не может быть null.
     */
    public void setExamSet(EnrExamSet examSet)
    {
        dirty(_examSet, examSet);
        _examSet = examSet;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrExamSetVariantGen)
        {
            setOwner(((EnrExamSetVariant)another).getOwner());
            setExamSet(((EnrExamSetVariant)another).getExamSet());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrExamSetVariantGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrExamSetVariant.class;
        }

        public T newInstance()
        {
            return (T) new EnrExamSetVariant();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "owner":
                    return obj.getOwner();
                case "examSet":
                    return obj.getExamSet();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "owner":
                    obj.setOwner((IEnrExamSetVariantOwner) value);
                    return;
                case "examSet":
                    obj.setExamSet((EnrExamSet) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "owner":
                        return true;
                case "examSet":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "owner":
                    return true;
                case "examSet":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "owner":
                    return IEnrExamSetVariantOwner.class;
                case "examSet":
                    return EnrExamSet.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrExamSetVariant> _dslPath = new Path<EnrExamSetVariant>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrExamSetVariant");
    }
            

    /**
     * Поле неизменяемое: владелец остается за настроенным набором
     *
     * @return Владелец настройки набора ВИ.
     * @see ru.tandemservice.unienr14.competition.entity.EnrExamSetVariant#getOwner()
     */
    public static IEnrExamSetVariantOwnerGen.Path<IEnrExamSetVariantOwner> owner()
    {
        return _dslPath.owner();
    }

    /**
     * Поле изменяемое: настроенный набор можно перекинуть на другой набор ВИ
     *
     * @return Набор ВИ. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrExamSetVariant#getExamSet()
     */
    public static EnrExamSet.Path<EnrExamSet> examSet()
    {
        return _dslPath.examSet();
    }

    public static class Path<E extends EnrExamSetVariant> extends EntityPath<E>
    {
        private IEnrExamSetVariantOwnerGen.Path<IEnrExamSetVariantOwner> _owner;
        private EnrExamSet.Path<EnrExamSet> _examSet;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * Поле неизменяемое: владелец остается за настроенным набором
     *
     * @return Владелец настройки набора ВИ.
     * @see ru.tandemservice.unienr14.competition.entity.EnrExamSetVariant#getOwner()
     */
        public IEnrExamSetVariantOwnerGen.Path<IEnrExamSetVariantOwner> owner()
        {
            if(_owner == null )
                _owner = new IEnrExamSetVariantOwnerGen.Path<IEnrExamSetVariantOwner>(L_OWNER, this);
            return _owner;
        }

    /**
     * Поле изменяемое: настроенный набор можно перекинуть на другой набор ВИ
     *
     * @return Набор ВИ. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrExamSetVariant#getExamSet()
     */
        public EnrExamSet.Path<EnrExamSet> examSet()
        {
            if(_examSet == null )
                _examSet = new EnrExamSet.Path<EnrExamSet>(L_EXAM_SET, this);
            return _examSet;
        }

        public Class getEntityClass()
        {
            return EnrExamSetVariant.class;
        }

        public String getEntityName()
        {
            return "enrExamSetVariant";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
