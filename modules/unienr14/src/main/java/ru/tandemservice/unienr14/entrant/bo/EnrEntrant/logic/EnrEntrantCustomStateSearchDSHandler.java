/* $Id$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantCustomState;

import java.util.Date;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 3/25/13
 */
public class EnrEntrantCustomStateSearchDSHandler extends DefaultSearchDataSourceHandler
{
    public EnrEntrantCustomStateSearchDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Long entrantCustomStateCI = context.get("customState");
        Long entrantId = context.get("entrantId");
        Date activeDateFrom = context.get("activeDateFrom");
        Date activeDateTo = context.get("activeDateTo");

        if (activeDateFrom != null && activeDateTo != null && activeDateFrom.after(activeDateTo))
            return new DSOutput(input);

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EnrEntrantCustomState.class, "st")
                .column("st")
                .fetchPath(DQLJoinType.inner, EnrEntrantCustomState.customState().fromAlias("st"), "customState")
                .where(eq(property("st", EnrEntrantCustomState.entrant()), value(entrantId)));

        FilterUtils.applyIntersectPeriodFilter(
                builder, "st", EnrEntrantCustomState.P_BEGIN_DATE, EnrEntrantCustomState.P_END_DATE, activeDateFrom, activeDateTo);

        if (null != entrantCustomStateCI)
            builder.where(eq(property("", EnrEntrantCustomState.customState()), value(entrantCustomStateCI)));

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).order().build();
    }
}
