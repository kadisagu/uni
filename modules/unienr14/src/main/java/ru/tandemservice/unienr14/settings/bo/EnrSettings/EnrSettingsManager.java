/* $Id:$ */
package ru.tandemservice.unienr14.settings.bo.EnrSettings;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import ru.tandemservice.unienr14.settings.bo.EnrSettings.logic.IEnrSettingsDao;
import ru.tandemservice.unienr14.settings.bo.EnrSettings.logic.EnrSettingsDao;
import ru.tandemservice.unienr14.settings.bo.EnrSettings.logic.SettingsOptionGroup;
import ru.tandemservice.unienr14.settings.entity.gen.EnrEnrollmentCampaignSettingsGen;

/**
 * @author oleyba
 * @since 8/15/14
 */
@Configuration
public class EnrSettingsManager extends BusinessObjectManager
{
    public static EnrSettingsManager instance()
    {
        return instance(EnrSettingsManager.class);
    }

    @Bean
    public IEnrSettingsDao dao()
    {
        return new EnrSettingsDao();
    }

    @Bean
    public ItemListExtPoint<SettingsOptionGroup> settingsOptionsGroupExtPoint()
    {
        return itemList(SettingsOptionGroup.class).create();
    }
}
