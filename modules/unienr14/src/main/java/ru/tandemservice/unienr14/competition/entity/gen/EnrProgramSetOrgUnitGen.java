package ru.tandemservice.unienr14.competition.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.competition.entity.IEnrollmentStudentSettings;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Подразделение, ведущее прием по набору ОП
 *
 * Указывает подразделения, ведущие прием, для наборов ОП - для создания конкурсов уже на конкретных подразделениях.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrProgramSetOrgUnitGen extends EntityBase
 implements IEnrollmentStudentSettings, INaturalIdentifiable<EnrProgramSetOrgUnitGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit";
    public static final String ENTITY_NAME = "enrProgramSetOrgUnit";
    public static final int VERSION_HASH = -996159827;
    private static IEntityMeta ENTITY_META;

    public static final String L_PROGRAM_SET = "programSet";
    public static final String L_ORG_UNIT = "orgUnit";
    public static final String L_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String L_EDUCATION_ORG_UNIT = "educationOrgUnit";
    public static final String P_MINISTERIAL_PLAN = "ministerialPlan";
    public static final String P_EXCLUSIVE_PLAN = "exclusivePlan";
    public static final String P_TARGET_ADM_PLAN = "targetAdmPlan";
    public static final String P_CONTRACT_PLAN = "contractPlan";
    public static final String P_MINISTERIAL_OPEN = "ministerialOpen";
    public static final String P_TARGET_ADM_OPEN = "targetAdmOpen";
    public static final String P_CONTRACT_OPEN = "contractOpen";
    public static final String L_PROGRAM = "program";

    private EnrProgramSetBase _programSet;     // Набор ОП
    private EnrOrgUnit _orgUnit;     // Подразделение, ведущее прием
    private OrgUnit _formativeOrgUnit;     // Формирующее подразделение
    private EducationOrgUnit _educationOrgUnit;     // НПП
    private int _ministerialPlan;     // КЦП
    private int _exclusivePlan;     // План приема по квоте особых прав
    private int _targetAdmPlan;     // План приема по ЦП
    private int _contractPlan;     // План приема на договор
    private boolean _ministerialOpen;     // Есть прием на КЦП
    private boolean _targetAdmOpen;     // Есть прием на ЦП
    private boolean _contractOpen;     // Есть прием на договор
    private EduProgramHigherProf _program;     // ОП

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Набор ОП. Свойство не может быть null.
     */
    @NotNull
    public EnrProgramSetBase getProgramSet()
    {
        return _programSet;
    }

    /**
     * @param programSet Набор ОП. Свойство не может быть null.
     */
    public void setProgramSet(EnrProgramSetBase programSet)
    {
        dirty(_programSet, programSet);
        _programSet = programSet;
    }

    /**
     * @return Подразделение, ведущее прием. Свойство не может быть null.
     */
    @NotNull
    public EnrOrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение, ведущее прием. Свойство не может быть null.
     */
    public void setOrgUnit(EnrOrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Формирующее подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    /**
     * @param formativeOrgUnit Формирующее подразделение. Свойство не может быть null.
     */
    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        dirty(_formativeOrgUnit, formativeOrgUnit);
        _formativeOrgUnit = formativeOrgUnit;
    }

    /**
     * Временное поле. Констрейнта на совпадение formativeOrgUnit нет, потому что в НПП это поле можно менять.
     *
     * @return НПП.
     */
    public EducationOrgUnit getEducationOrgUnit()
    {
        return _educationOrgUnit;
    }

    /**
     * @param educationOrgUnit НПП.
     */
    public void setEducationOrgUnit(EducationOrgUnit educationOrgUnit)
    {
        dirty(_educationOrgUnit, educationOrgUnit);
        _educationOrgUnit = educationOrgUnit;
    }

    /**
     * Общий (суммарный) план по бюджету, включает в себя план по квоте и ЦП.
     *
     * @return КЦП. Свойство не может быть null.
     */
    @NotNull
    public int getMinisterialPlan()
    {
        return _ministerialPlan;
    }

    /**
     * @param ministerialPlan КЦП. Свойство не может быть null.
     */
    public void setMinisterialPlan(int ministerialPlan)
    {
        dirty(_ministerialPlan, ministerialPlan);
        _ministerialPlan = ministerialPlan;
    }

    /**
     * Является составной частью плана на бюджет.
     *
     * @return План приема по квоте особых прав. Свойство не может быть null.
     */
    @NotNull
    public int getExclusivePlan()
    {
        return _exclusivePlan;
    }

    /**
     * @param exclusivePlan План приема по квоте особых прав. Свойство не может быть null.
     */
    public void setExclusivePlan(int exclusivePlan)
    {
        dirty(_exclusivePlan, exclusivePlan);
        _exclusivePlan = exclusivePlan;
    }

    /**
     * Является составной частью плана на бюджет.
     *
     * @return План приема по ЦП. Свойство не может быть null.
     */
    @NotNull
    public int getTargetAdmPlan()
    {
        return _targetAdmPlan;
    }

    /**
     * @param targetAdmPlan План приема по ЦП. Свойство не может быть null.
     */
    public void setTargetAdmPlan(int targetAdmPlan)
    {
        dirty(_targetAdmPlan, targetAdmPlan);
        _targetAdmPlan = targetAdmPlan;
    }

    /**
     * План по договору (самостоятельная цифра).
     *
     * @return План приема на договор. Свойство не может быть null.
     */
    @NotNull
    public int getContractPlan()
    {
        return _contractPlan;
    }

    /**
     * @param contractPlan План приема на договор. Свойство не может быть null.
     */
    public void setContractPlan(int contractPlan)
    {
        dirty(_contractPlan, contractPlan);
        _contractPlan = contractPlan;
    }

    /**
     * @return Есть прием на КЦП. Свойство не может быть null.
     *
     * Это формула "case when (ministerialPlan > 0) then true else false end".
     */
    // @NotNull
    public boolean isMinisterialOpen()
    {
        return _ministerialOpen;
    }

    /**
     * @param ministerialOpen Есть прием на КЦП. Свойство не может быть null.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setMinisterialOpen(boolean ministerialOpen)
    {
        dirty(_ministerialOpen, ministerialOpen);
        _ministerialOpen = ministerialOpen;
    }

    /**
     * @return Есть прием на ЦП. Свойство не может быть null.
     *
     * Это формула "case when (targetAdmPlan > 0) then true else false end".
     */
    // @NotNull
    public boolean isTargetAdmOpen()
    {
        return _targetAdmOpen;
    }

    /**
     * @param targetAdmOpen Есть прием на ЦП. Свойство не может быть null.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setTargetAdmOpen(boolean targetAdmOpen)
    {
        dirty(_targetAdmOpen, targetAdmOpen);
        _targetAdmOpen = targetAdmOpen;
    }

    /**
     * @return Есть прием на договор. Свойство не может быть null.
     *
     * Это формула "case when (contractPlan > 0) then true else false end".
     */
    // @NotNull
    public boolean isContractOpen()
    {
        return _contractOpen;
    }

    /**
     * @param contractOpen Есть прием на договор. Свойство не может быть null.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setContractOpen(boolean contractOpen)
    {
        dirty(_contractOpen, contractOpen);
        _contractOpen = contractOpen;
    }

    /**
     * Поле для поддержки интерфейса «Настройка НПП для создания студентов» (ru.tandemservice.unienr14.competition.entity.IEnrollmentStudentSettings). В этой сущности всегда пустое. Подробнее см. в документации по интерфейсу.
     *
     * @return ОП.
     *
     * Это формула "null".
     */
    public EduProgramHigherProf getProgram()
    {
        return _program;
    }

    /**
     * @param program ОП.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setProgram(EduProgramHigherProf program)
    {
        dirty(_program, program);
        _program = program;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrProgramSetOrgUnitGen)
        {
            if (withNaturalIdProperties)
            {
                setProgramSet(((EnrProgramSetOrgUnit)another).getProgramSet());
                setOrgUnit(((EnrProgramSetOrgUnit)another).getOrgUnit());
            }
            setFormativeOrgUnit(((EnrProgramSetOrgUnit)another).getFormativeOrgUnit());
            setEducationOrgUnit(((EnrProgramSetOrgUnit)another).getEducationOrgUnit());
            setMinisterialPlan(((EnrProgramSetOrgUnit)another).getMinisterialPlan());
            setExclusivePlan(((EnrProgramSetOrgUnit)another).getExclusivePlan());
            setTargetAdmPlan(((EnrProgramSetOrgUnit)another).getTargetAdmPlan());
            setContractPlan(((EnrProgramSetOrgUnit)another).getContractPlan());
            setMinisterialOpen(((EnrProgramSetOrgUnit)another).isMinisterialOpen());
            setTargetAdmOpen(((EnrProgramSetOrgUnit)another).isTargetAdmOpen());
            setContractOpen(((EnrProgramSetOrgUnit)another).isContractOpen());
            setProgram(((EnrProgramSetOrgUnit)another).getProgram());
        }
    }

    public INaturalId<EnrProgramSetOrgUnitGen> getNaturalId()
    {
        return new NaturalId(getProgramSet(), getOrgUnit());
    }

    public static class NaturalId extends NaturalIdBase<EnrProgramSetOrgUnitGen>
    {
        private static final String PROXY_NAME = "EnrProgramSetOrgUnitNaturalProxy";

        private Long _programSet;
        private Long _orgUnit;

        public NaturalId()
        {}

        public NaturalId(EnrProgramSetBase programSet, EnrOrgUnit orgUnit)
        {
            _programSet = ((IEntity) programSet).getId();
            _orgUnit = ((IEntity) orgUnit).getId();
        }

        public Long getProgramSet()
        {
            return _programSet;
        }

        public void setProgramSet(Long programSet)
        {
            _programSet = programSet;
        }

        public Long getOrgUnit()
        {
            return _orgUnit;
        }

        public void setOrgUnit(Long orgUnit)
        {
            _orgUnit = orgUnit;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrProgramSetOrgUnitGen.NaturalId) ) return false;

            EnrProgramSetOrgUnitGen.NaturalId that = (NaturalId) o;

            if( !equals(getProgramSet(), that.getProgramSet()) ) return false;
            if( !equals(getOrgUnit(), that.getOrgUnit()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getProgramSet());
            result = hashCode(result, getOrgUnit());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getProgramSet());
            sb.append("/");
            sb.append(getOrgUnit());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrProgramSetOrgUnitGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrProgramSetOrgUnit.class;
        }

        public T newInstance()
        {
            return (T) new EnrProgramSetOrgUnit();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "programSet":
                    return obj.getProgramSet();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "formativeOrgUnit":
                    return obj.getFormativeOrgUnit();
                case "educationOrgUnit":
                    return obj.getEducationOrgUnit();
                case "ministerialPlan":
                    return obj.getMinisterialPlan();
                case "exclusivePlan":
                    return obj.getExclusivePlan();
                case "targetAdmPlan":
                    return obj.getTargetAdmPlan();
                case "contractPlan":
                    return obj.getContractPlan();
                case "ministerialOpen":
                    return obj.isMinisterialOpen();
                case "targetAdmOpen":
                    return obj.isTargetAdmOpen();
                case "contractOpen":
                    return obj.isContractOpen();
                case "program":
                    return obj.getProgram();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "programSet":
                    obj.setProgramSet((EnrProgramSetBase) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((EnrOrgUnit) value);
                    return;
                case "formativeOrgUnit":
                    obj.setFormativeOrgUnit((OrgUnit) value);
                    return;
                case "educationOrgUnit":
                    obj.setEducationOrgUnit((EducationOrgUnit) value);
                    return;
                case "ministerialPlan":
                    obj.setMinisterialPlan((Integer) value);
                    return;
                case "exclusivePlan":
                    obj.setExclusivePlan((Integer) value);
                    return;
                case "targetAdmPlan":
                    obj.setTargetAdmPlan((Integer) value);
                    return;
                case "contractPlan":
                    obj.setContractPlan((Integer) value);
                    return;
                case "ministerialOpen":
                    obj.setMinisterialOpen((Boolean) value);
                    return;
                case "targetAdmOpen":
                    obj.setTargetAdmOpen((Boolean) value);
                    return;
                case "contractOpen":
                    obj.setContractOpen((Boolean) value);
                    return;
                case "program":
                    obj.setProgram((EduProgramHigherProf) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "programSet":
                        return true;
                case "orgUnit":
                        return true;
                case "formativeOrgUnit":
                        return true;
                case "educationOrgUnit":
                        return true;
                case "ministerialPlan":
                        return true;
                case "exclusivePlan":
                        return true;
                case "targetAdmPlan":
                        return true;
                case "contractPlan":
                        return true;
                case "ministerialOpen":
                        return true;
                case "targetAdmOpen":
                        return true;
                case "contractOpen":
                        return true;
                case "program":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "programSet":
                    return true;
                case "orgUnit":
                    return true;
                case "formativeOrgUnit":
                    return true;
                case "educationOrgUnit":
                    return true;
                case "ministerialPlan":
                    return true;
                case "exclusivePlan":
                    return true;
                case "targetAdmPlan":
                    return true;
                case "contractPlan":
                    return true;
                case "ministerialOpen":
                    return true;
                case "targetAdmOpen":
                    return true;
                case "contractOpen":
                    return true;
                case "program":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "programSet":
                    return EnrProgramSetBase.class;
                case "orgUnit":
                    return EnrOrgUnit.class;
                case "formativeOrgUnit":
                    return OrgUnit.class;
                case "educationOrgUnit":
                    return EducationOrgUnit.class;
                case "ministerialPlan":
                    return Integer.class;
                case "exclusivePlan":
                    return Integer.class;
                case "targetAdmPlan":
                    return Integer.class;
                case "contractPlan":
                    return Integer.class;
                case "ministerialOpen":
                    return Boolean.class;
                case "targetAdmOpen":
                    return Boolean.class;
                case "contractOpen":
                    return Boolean.class;
                case "program":
                    return EduProgramHigherProf.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrProgramSetOrgUnit> _dslPath = new Path<EnrProgramSetOrgUnit>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrProgramSetOrgUnit");
    }
            

    /**
     * @return Набор ОП. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit#getProgramSet()
     */
    public static EnrProgramSetBase.Path<EnrProgramSetBase> programSet()
    {
        return _dslPath.programSet();
    }

    /**
     * @return Подразделение, ведущее прием. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit#getOrgUnit()
     */
    public static EnrOrgUnit.Path<EnrOrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Формирующее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit#getFormativeOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> formativeOrgUnit()
    {
        return _dslPath.formativeOrgUnit();
    }

    /**
     * Временное поле. Констрейнта на совпадение formativeOrgUnit нет, потому что в НПП это поле можно менять.
     *
     * @return НПП.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit#getEducationOrgUnit()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
    {
        return _dslPath.educationOrgUnit();
    }

    /**
     * Общий (суммарный) план по бюджету, включает в себя план по квоте и ЦП.
     *
     * @return КЦП. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit#getMinisterialPlan()
     */
    public static PropertyPath<Integer> ministerialPlan()
    {
        return _dslPath.ministerialPlan();
    }

    /**
     * Является составной частью плана на бюджет.
     *
     * @return План приема по квоте особых прав. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit#getExclusivePlan()
     */
    public static PropertyPath<Integer> exclusivePlan()
    {
        return _dslPath.exclusivePlan();
    }

    /**
     * Является составной частью плана на бюджет.
     *
     * @return План приема по ЦП. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit#getTargetAdmPlan()
     */
    public static PropertyPath<Integer> targetAdmPlan()
    {
        return _dslPath.targetAdmPlan();
    }

    /**
     * План по договору (самостоятельная цифра).
     *
     * @return План приема на договор. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit#getContractPlan()
     */
    public static PropertyPath<Integer> contractPlan()
    {
        return _dslPath.contractPlan();
    }

    /**
     * @return Есть прием на КЦП. Свойство не может быть null.
     *
     * Это формула "case when (ministerialPlan > 0) then true else false end".
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit#isMinisterialOpen()
     */
    public static PropertyPath<Boolean> ministerialOpen()
    {
        return _dslPath.ministerialOpen();
    }

    /**
     * @return Есть прием на ЦП. Свойство не может быть null.
     *
     * Это формула "case when (targetAdmPlan > 0) then true else false end".
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit#isTargetAdmOpen()
     */
    public static PropertyPath<Boolean> targetAdmOpen()
    {
        return _dslPath.targetAdmOpen();
    }

    /**
     * @return Есть прием на договор. Свойство не может быть null.
     *
     * Это формула "case when (contractPlan > 0) then true else false end".
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit#isContractOpen()
     */
    public static PropertyPath<Boolean> contractOpen()
    {
        return _dslPath.contractOpen();
    }

    /**
     * Поле для поддержки интерфейса «Настройка НПП для создания студентов» (ru.tandemservice.unienr14.competition.entity.IEnrollmentStudentSettings). В этой сущности всегда пустое. Подробнее см. в документации по интерфейсу.
     *
     * @return ОП.
     *
     * Это формула "null".
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit#getProgram()
     */
    public static EduProgramHigherProf.Path<EduProgramHigherProf> program()
    {
        return _dslPath.program();
    }

    public static class Path<E extends EnrProgramSetOrgUnit> extends EntityPath<E>
    {
        private EnrProgramSetBase.Path<EnrProgramSetBase> _programSet;
        private EnrOrgUnit.Path<EnrOrgUnit> _orgUnit;
        private OrgUnit.Path<OrgUnit> _formativeOrgUnit;
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnit;
        private PropertyPath<Integer> _ministerialPlan;
        private PropertyPath<Integer> _exclusivePlan;
        private PropertyPath<Integer> _targetAdmPlan;
        private PropertyPath<Integer> _contractPlan;
        private PropertyPath<Boolean> _ministerialOpen;
        private PropertyPath<Boolean> _targetAdmOpen;
        private PropertyPath<Boolean> _contractOpen;
        private EduProgramHigherProf.Path<EduProgramHigherProf> _program;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Набор ОП. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit#getProgramSet()
     */
        public EnrProgramSetBase.Path<EnrProgramSetBase> programSet()
        {
            if(_programSet == null )
                _programSet = new EnrProgramSetBase.Path<EnrProgramSetBase>(L_PROGRAM_SET, this);
            return _programSet;
        }

    /**
     * @return Подразделение, ведущее прием. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit#getOrgUnit()
     */
        public EnrOrgUnit.Path<EnrOrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new EnrOrgUnit.Path<EnrOrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Формирующее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit#getFormativeOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> formativeOrgUnit()
        {
            if(_formativeOrgUnit == null )
                _formativeOrgUnit = new OrgUnit.Path<OrgUnit>(L_FORMATIVE_ORG_UNIT, this);
            return _formativeOrgUnit;
        }

    /**
     * Временное поле. Констрейнта на совпадение formativeOrgUnit нет, потому что в НПП это поле можно менять.
     *
     * @return НПП.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit#getEducationOrgUnit()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
        {
            if(_educationOrgUnit == null )
                _educationOrgUnit = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT, this);
            return _educationOrgUnit;
        }

    /**
     * Общий (суммарный) план по бюджету, включает в себя план по квоте и ЦП.
     *
     * @return КЦП. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit#getMinisterialPlan()
     */
        public PropertyPath<Integer> ministerialPlan()
        {
            if(_ministerialPlan == null )
                _ministerialPlan = new PropertyPath<Integer>(EnrProgramSetOrgUnitGen.P_MINISTERIAL_PLAN, this);
            return _ministerialPlan;
        }

    /**
     * Является составной частью плана на бюджет.
     *
     * @return План приема по квоте особых прав. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit#getExclusivePlan()
     */
        public PropertyPath<Integer> exclusivePlan()
        {
            if(_exclusivePlan == null )
                _exclusivePlan = new PropertyPath<Integer>(EnrProgramSetOrgUnitGen.P_EXCLUSIVE_PLAN, this);
            return _exclusivePlan;
        }

    /**
     * Является составной частью плана на бюджет.
     *
     * @return План приема по ЦП. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit#getTargetAdmPlan()
     */
        public PropertyPath<Integer> targetAdmPlan()
        {
            if(_targetAdmPlan == null )
                _targetAdmPlan = new PropertyPath<Integer>(EnrProgramSetOrgUnitGen.P_TARGET_ADM_PLAN, this);
            return _targetAdmPlan;
        }

    /**
     * План по договору (самостоятельная цифра).
     *
     * @return План приема на договор. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit#getContractPlan()
     */
        public PropertyPath<Integer> contractPlan()
        {
            if(_contractPlan == null )
                _contractPlan = new PropertyPath<Integer>(EnrProgramSetOrgUnitGen.P_CONTRACT_PLAN, this);
            return _contractPlan;
        }

    /**
     * @return Есть прием на КЦП. Свойство не может быть null.
     *
     * Это формула "case when (ministerialPlan > 0) then true else false end".
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit#isMinisterialOpen()
     */
        public PropertyPath<Boolean> ministerialOpen()
        {
            if(_ministerialOpen == null )
                _ministerialOpen = new PropertyPath<Boolean>(EnrProgramSetOrgUnitGen.P_MINISTERIAL_OPEN, this);
            return _ministerialOpen;
        }

    /**
     * @return Есть прием на ЦП. Свойство не может быть null.
     *
     * Это формула "case when (targetAdmPlan > 0) then true else false end".
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit#isTargetAdmOpen()
     */
        public PropertyPath<Boolean> targetAdmOpen()
        {
            if(_targetAdmOpen == null )
                _targetAdmOpen = new PropertyPath<Boolean>(EnrProgramSetOrgUnitGen.P_TARGET_ADM_OPEN, this);
            return _targetAdmOpen;
        }

    /**
     * @return Есть прием на договор. Свойство не может быть null.
     *
     * Это формула "case when (contractPlan > 0) then true else false end".
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit#isContractOpen()
     */
        public PropertyPath<Boolean> contractOpen()
        {
            if(_contractOpen == null )
                _contractOpen = new PropertyPath<Boolean>(EnrProgramSetOrgUnitGen.P_CONTRACT_OPEN, this);
            return _contractOpen;
        }

    /**
     * Поле для поддержки интерфейса «Настройка НПП для создания студентов» (ru.tandemservice.unienr14.competition.entity.IEnrollmentStudentSettings). В этой сущности всегда пустое. Подробнее см. в документации по интерфейсу.
     *
     * @return ОП.
     *
     * Это формула "null".
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit#getProgram()
     */
        public EduProgramHigherProf.Path<EduProgramHigherProf> program()
        {
            if(_program == null )
                _program = new EduProgramHigherProf.Path<EduProgramHigherProf>(L_PROGRAM, this);
            return _program;
        }

        public Class getEntityClass()
        {
            return EnrProgramSetOrgUnit.class;
        }

        public String getEntityName()
        {
            return "enrProgramSetOrgUnit";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
