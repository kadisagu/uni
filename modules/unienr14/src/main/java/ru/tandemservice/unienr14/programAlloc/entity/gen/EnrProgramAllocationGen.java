package ru.tandemservice.unienr14.programAlloc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.programAlloc.entity.EnrProgramAllocation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Распределение по ОП
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrProgramAllocationGen extends EntityBase
 implements INaturalIdentifiable<EnrProgramAllocationGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.programAlloc.entity.EnrProgramAllocation";
    public static final String ENTITY_NAME = "enrProgramAllocation";
    public static final int VERSION_HASH = 390722036;
    private static IEntityMeta ENTITY_META;

    public static final String L_PROGRAM_SET_ORG_UNIT = "programSetOrgUnit";
    public static final String P_FORMING_DATE = "formingDate";

    private EnrProgramSetOrgUnit _programSetOrgUnit;     // Подразделение, ведущее прием по набору ОП
    private Date _formingDate;     // Дата формирования

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Подразделение, ведущее прием по набору ОП. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EnrProgramSetOrgUnit getProgramSetOrgUnit()
    {
        return _programSetOrgUnit;
    }

    /**
     * @param programSetOrgUnit Подразделение, ведущее прием по набору ОП. Свойство не может быть null и должно быть уникальным.
     */
    public void setProgramSetOrgUnit(EnrProgramSetOrgUnit programSetOrgUnit)
    {
        dirty(_programSetOrgUnit, programSetOrgUnit);
        _programSetOrgUnit = programSetOrgUnit;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата формирования. Свойство не может быть null.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrProgramAllocationGen)
        {
            if (withNaturalIdProperties)
            {
                setProgramSetOrgUnit(((EnrProgramAllocation)another).getProgramSetOrgUnit());
            }
            setFormingDate(((EnrProgramAllocation)another).getFormingDate());
        }
    }

    public INaturalId<EnrProgramAllocationGen> getNaturalId()
    {
        return new NaturalId(getProgramSetOrgUnit());
    }

    public static class NaturalId extends NaturalIdBase<EnrProgramAllocationGen>
    {
        private static final String PROXY_NAME = "EnrProgramAllocationNaturalProxy";

        private Long _programSetOrgUnit;

        public NaturalId()
        {}

        public NaturalId(EnrProgramSetOrgUnit programSetOrgUnit)
        {
            _programSetOrgUnit = ((IEntity) programSetOrgUnit).getId();
        }

        public Long getProgramSetOrgUnit()
        {
            return _programSetOrgUnit;
        }

        public void setProgramSetOrgUnit(Long programSetOrgUnit)
        {
            _programSetOrgUnit = programSetOrgUnit;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrProgramAllocationGen.NaturalId) ) return false;

            EnrProgramAllocationGen.NaturalId that = (NaturalId) o;

            if( !equals(getProgramSetOrgUnit(), that.getProgramSetOrgUnit()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getProgramSetOrgUnit());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getProgramSetOrgUnit());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrProgramAllocationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrProgramAllocation.class;
        }

        public T newInstance()
        {
            return (T) new EnrProgramAllocation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "programSetOrgUnit":
                    return obj.getProgramSetOrgUnit();
                case "formingDate":
                    return obj.getFormingDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "programSetOrgUnit":
                    obj.setProgramSetOrgUnit((EnrProgramSetOrgUnit) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "programSetOrgUnit":
                        return true;
                case "formingDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "programSetOrgUnit":
                    return true;
                case "formingDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "programSetOrgUnit":
                    return EnrProgramSetOrgUnit.class;
                case "formingDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrProgramAllocation> _dslPath = new Path<EnrProgramAllocation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrProgramAllocation");
    }
            

    /**
     * @return Подразделение, ведущее прием по набору ОП. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.programAlloc.entity.EnrProgramAllocation#getProgramSetOrgUnit()
     */
    public static EnrProgramSetOrgUnit.Path<EnrProgramSetOrgUnit> programSetOrgUnit()
    {
        return _dslPath.programSetOrgUnit();
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.programAlloc.entity.EnrProgramAllocation#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    public static class Path<E extends EnrProgramAllocation> extends EntityPath<E>
    {
        private EnrProgramSetOrgUnit.Path<EnrProgramSetOrgUnit> _programSetOrgUnit;
        private PropertyPath<Date> _formingDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Подразделение, ведущее прием по набору ОП. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.programAlloc.entity.EnrProgramAllocation#getProgramSetOrgUnit()
     */
        public EnrProgramSetOrgUnit.Path<EnrProgramSetOrgUnit> programSetOrgUnit()
        {
            if(_programSetOrgUnit == null )
                _programSetOrgUnit = new EnrProgramSetOrgUnit.Path<EnrProgramSetOrgUnit>(L_PROGRAM_SET_ORG_UNIT, this);
            return _programSetOrgUnit;
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.programAlloc.entity.EnrProgramAllocation#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(EnrProgramAllocationGen.P_FORMING_DATE, this);
            return _formingDate;
        }

        public Class getEntityClass()
        {
            return EnrProgramAllocation.class;
        }

        public String getEntityName()
        {
            return "enrProgramAllocation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
