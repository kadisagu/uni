package ru.tandemservice.unienr14.exams.entity;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.unienr14.exams.entity.gen.*;

import java.util.List;

/**
 * Экзаменационная группа (ЭГ)
 */
public class EnrExamGroup extends EnrExamGroupGen
{
    /**
     * Вступительное испытание в формате:
     * @return [название дисциплины ПК] ([название формы сдачи])
     */
    @Override
    @EntityDSLSupport
    public String getDisciplineTitle()
    {
        return getDiscipline().getTitle() + " (" + getPassForm().getTitle() + ")";
    }

    @Override
    @EntityDSLSupport
    public boolean isOpened()
    {
        return !isClosed();
    }


    /**
     * Название экзаменационной группы с данными расписания ВИ
     * http://wiki.tandemservice.ru/pages/viewpage.action?pageId=25101731
     * @return
     */
    @Override
    @EntityDSLSupport
    public String getExamTitleWithSchedule() {
        List<EnrExamGroupScheduleEvent> events = IUniBaseDao.instance.get().getList(EnrExamGroupScheduleEvent.class, EnrExamGroupScheduleEvent.examGroup(), this, EnrExamGroupScheduleEvent.examScheduleEvent().scheduleEvent().durationBegin().s());
        return getTitle() + " (" + UniStringUtils.join(events, EnrExamGroupScheduleEvent.P_SCHEDULE_TITLE, "; ") + ")";
    }
}