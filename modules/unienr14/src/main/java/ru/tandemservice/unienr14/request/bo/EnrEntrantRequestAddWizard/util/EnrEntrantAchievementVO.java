/* $Id$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.util;

import ru.tandemservice.unienr14.entrant.bo.EnrEntrantBaseDocument.util.EnrEntrantDocumentViewWrapper;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement;

import java.util.Comparator;

/**
 * @author nvankov
 * @since 6/14/14
 */
public class EnrEntrantAchievementVO
{
    public static final Comparator<EnrEntrantAchievementVO> COMPARATOR = new Comparator<EnrEntrantAchievementVO>() {
        @Override public int compare(EnrEntrantAchievementVO o1, EnrEntrantAchievementVO o2) {
            if(o1.getAchievement().getType() == null || o2.getAchievement().getType() == null)
            {
                return o1.getAchievement().getType() == null ? (o2.getAchievement().getType() != null ? 1 : 0) : -1;
            }
            else
            {
                return o1.getAchievement().getType().getAchievementKind().getTitle().compareTo(o2.getAchievement().getType().getAchievementKind().getTitle());
            }
        }
    };

    private EnrEntrantAchievement _achievement;
    private EnrEntrantDocumentViewWrapper _documentWrapper;
    private boolean _createdInCurrentWizard;
    private String _markFieldId;
    private String _documentFieldId;
    private String _requestFieldId;
    private String _id;

    public EnrEntrantAchievementVO(EnrEntrantAchievement achievement, EnrEntrantDocumentViewWrapper documentWrapper, boolean createdInCurrentWizard)
    {
        _achievement = achievement;
        _documentWrapper = documentWrapper;
        _createdInCurrentWizard = createdInCurrentWizard;
        _id = String.valueOf(System.identityHashCode(this));
        _markFieldId = "mark_" + getId();
        _documentFieldId = "document_" + getId();
        _requestFieldId = "request_" + getId();
    }

    public EnrEntrantAchievement getAchievement()
    {
        return _achievement;
    }

    public void setDocumentWrapper(EnrEntrantDocumentViewWrapper documentWrapper)
    {
        _documentWrapper = documentWrapper;
    }

    public EnrEntrantDocumentViewWrapper getDocumentWrapper()
    {
        return _documentWrapper;
    }

    public boolean isCreatedInCurrentWizard()
    {
        return _createdInCurrentWizard;
    }

    public boolean isDisabled()
    {
        return !_createdInCurrentWizard;
    }

    public String getMarkFieldId()
    {
        return _markFieldId;
    }

    public String getDocumentFieldId()
    {
        return _documentFieldId;
    }

    public String getRequestFieldId()
    {
        return _requestFieldId;
    }

    public String getId()
    {
        return _id;
    }
}
