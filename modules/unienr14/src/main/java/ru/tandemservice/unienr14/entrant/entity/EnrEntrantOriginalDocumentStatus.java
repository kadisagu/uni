package ru.tandemservice.unienr14.entrant.entity;

import org.tandemframework.core.common.IEntityDebugTitled;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import ru.tandemservice.unienr14.entrant.entity.gen.EnrEntrantOriginalDocumentStatusGen;

/**
 * Оригинал документов
 */
public class EnrEntrantOriginalDocumentStatus extends EnrEntrantOriginalDocumentStatusGen implements IEntityDebugTitled
{
    public EnrEntrantOriginalDocumentStatus()
    {
    }

    public EnrEntrantOriginalDocumentStatus(EnrEntrant entrant, PersonEduDocument personEduDocument)
    {
        setEduDocument(personEduDocument);
        setEntrant(entrant);
    }

    @Override
    public String getEntityDebugTitle() {
        return getEntrant().getTitle() + ", оригинал " + getEduDocument().getEntityDebugTitle();
    }
}