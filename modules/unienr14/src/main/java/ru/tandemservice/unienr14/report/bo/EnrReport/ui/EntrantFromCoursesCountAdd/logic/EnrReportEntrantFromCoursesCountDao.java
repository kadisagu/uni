/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantFromCoursesCountAdd.logic;

import org.tandemframework.core.CoreCollectionUtils.Pair;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.rtf.RtfRowIntercepterRawText;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantAccessCourse;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantFromCoursesCountAdd.EnrReportEntrantFromCoursesCountAddUI;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportStageSelector;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportUtil;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.FilterParametersPrinter;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder;
import ru.tandemservice.unienr14.report.entity.EnrReportEntrantFromCoursesCount;
import ru.tandemservice.unienr14.report.entity.EnrReportRequestCount;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 29.05.2014
 */
public class EnrReportEntrantFromCoursesCountDao extends UniBaseDao implements IEnrReportEntrantFromCoursesCountDao
{

    public static final String RTF_LINE = "\\line";

    @Override
    public Long createReport(EnrReportEntrantFromCoursesCountAddUI model)
    {
        EnrReportEntrantFromCoursesCount report = new EnrReportEntrantFromCoursesCount();
        report.setFormingDate(new Date());

        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateSelector().getDateFrom());
        report.setDateTo(model.getDateSelector().getDateTo());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, EnrReportRequestCount.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, EnrReportRequestCount.P_PROGRAM_FORM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, EnrReportRequestCount.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, EnrReportRequestCount.P_FORMATIVE_ORG_UNIT, "fullTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, EnrReportRequestCount.P_PROGRAM_SUBJECT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, EnrReportRequestCount.P_EDU_PROGRAM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, EnrReportRequestCount.P_PROGRAM_SET, "title");
        if (model.isParallelActive())
            report.setParallel(model.getParallel().getTitle());

        DatabaseFile content = new DatabaseFile();

        content.setContent(buildReport(model));
        content.setFilename("EnrReportEntrantFromCoursesCount.rtf");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        save(content);

        report.setContent(content);
        save(report);

        return report.getId();
    }

    private byte[] buildReport(EnrReportEntrantFromCoursesCountAddUI model)
    {
        //rtf
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(EnrScriptItem.class, EnrScriptItemCodes.REPORT_ENTRANT_FROM_COURSES_COUNT);
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();



        EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder()
                .notArchiveOnly()
                .filter(model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo())
                .filter(model.getEnrollmentCampaign());
        filterAddon.applyFilters(requestedCompDQL, requestedCompDQL.competition());

        EnrReportStageSelector.docAcceptanceStageFilter().applyFilter(requestedCompDQL, "reqComp");

        // Убираем отозванные заявления
        requestedCompDQL
                .where(eq(property("request", EnrEntrantRequest.takeAwayDocument()), value(Boolean.FALSE)))
                .column(property("reqComp"));
        if (model.isParallelOnly())
            requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.TRUE)));
        if (model.isSkipParallel())
            requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.FALSE)));
        List<EnrRequestedCompetition> requestedCompetitionList = getList(requestedCompDQL);

        requestedCompDQL
                .joinEntity("entrant", DQLJoinType.inner, EnrEntrantAccessCourse.class, "course", eq(property("entrant", EnrEntrant.id()), property("course", EnrEntrantAccessCourse.entrant().id())));
        List<EnrRequestedCompetition> requestedCompetitionsWithCourse = getList(requestedCompDQL);
        requestedCompDQL.resetColumns();
        requestedCompDQL
                .joinEntity("reqComp", DQLJoinType.full, EnrChosenEntranceExam.class, "chosenExam", eq(property("reqComp", EnrRequestedCompetition.id()), property("chosenExam", EnrChosenEntranceExam.requestedCompetition().id())) )
                .column(property("course"))
                .column(property("reqComp"))
                .column(property("chosenExam"));

        List<Object[]> courseList = getList(requestedCompDQL);

        // Данные для таблицы 2  Entrant - Set<EnrChosenEntraceExam> - Set<EnrEntrantAccessCourse> - Set<EnrRequestedCompetition>
        Set<EnrEntrant> entrants = new HashSet<>();
        Map<EnrRequestedCompetition, Set<EnrChosenEntranceExam>> examMap = new HashMap<>();
        Map<EnrEntrant, Set<EnrEntrantAccessCourse>> courseMap = new HashMap<>();
        Map<EnrEntrant, Set<EnrRequestedCompetition>> reqCompMap = new HashMap<>();

        for (Object[] columns : courseList)
        {

            EnrRequestedCompetition reqComp = (EnrRequestedCompetition) columns[1];
            if (!EnrReportStageSelector.enrollmentResultsCheckByPriority(reqComp.getState().getPriority()))
                continue;

            EnrEntrantAccessCourse course = (EnrEntrantAccessCourse) columns[0];
            EnrChosenEntranceExam exam = null;
            if (columns.length > 2)
                 exam = (EnrChosenEntranceExam) columns[2];


            EnrEntrant entrant = reqComp.getRequest().getEntrant();
            if (entrants.contains(entrant))
            {
                courseMap.get(entrant).add(course);
                reqCompMap.get(entrant).add(reqComp);
            }
            else
            {
                entrants.add(entrant);

                Set<EnrEntrantAccessCourse> temporaryCourseSet = new TreeSet<>(new Comparator<EnrEntrantAccessCourse>() {
                    @Override
                    public int compare(EnrEntrantAccessCourse o1, EnrEntrantAccessCourse o2) {
                        return o1.getCourse().getShortTitle().compareTo(o2.getCourse().getShortTitle());
                    }
                });
                temporaryCourseSet.add(course);
                Set<EnrRequestedCompetition> temporaryReqCompSet = new HashSet<>();
                temporaryReqCompSet.add(reqComp);

                courseMap.put(entrant, temporaryCourseSet);
                reqCompMap.put(entrant, temporaryReqCompSet);
            }
            if (exam != null)
                if (examMap.containsKey(reqComp))
                    examMap.get(reqComp).add(exam);
                else
                {
                    Set<EnrChosenEntranceExam> temporaryExamSet = new HashSet<>();
                    temporaryExamSet.add(exam);
                    examMap.put(reqComp, temporaryExamSet);
                }
        }

        Comparator<EnrEntrant> entrantComparator = new Comparator<EnrEntrant>() {
            @Override
            public int compare(EnrEntrant o1, EnrEntrant o2) {
                return CommonCollator.RUSSIAN_COLLATOR.compare(o1.getFullFio(), o2.getFullFio());
            }
        };

        List<EnrEntrant> entrantList = new ArrayList<>();
        entrantList.addAll(entrants);
        Collections.sort(entrantList, entrantComparator);

        // Заполнение таблицы 2.
        ArrayList<String[]> t2Table = new ArrayList<>();
        int number = 1;
        for (EnrEntrant entrant : entrantList)
        {
            ArrayList<Pair<Double, String[]>> rows = new ArrayList<>();
            String[] row = new String[6];

            // Повторяющиеся для одного абитуриента ячейки не перезаполняются
            row[1] = UniStringUtils.join(courseMap.get(entrant), EnrEntrantAccessCourse.course().shortTitle().s(), ", ");
            row[2] = entrant.getFullFio();

            // Заполнение ячеек, индивидуальных для каждого конкурса
            for (EnrRequestedCompetition reqComp : reqCompMap.get(entrant))
            {

                row[3] = reqComp.getRequest().getStringNumber();
                row[4] = "";
                double sum = 0;
                if (examMap.containsKey(reqComp))
                    for (EnrChosenEntranceExam exam : examMap.get(reqComp)) {
                        StringBuilder stringBuilder = new StringBuilder(exam.getDiscipline().getTitle());
                        if (exam.getMarkSource() != null)
                        {
                            stringBuilder.append(" - ");
                            stringBuilder.append(exam.getMarkSource().getMarkAsString());
                            sum += exam.getMarkAsDouble();
                        }
                        else stringBuilder.append(" —");
                        stringBuilder.append(RTF_LINE);
                        row[4] += stringBuilder.toString();
                    }
                row[5] = String.valueOf(sum);
                rows.add(new Pair(sum, row));
            }

            // Сортировка по баллам
            Collections.sort(rows, new Comparator<Pair<Double, String[]>>() {
                @Override
                public int compare(Pair<Double, String[]> o1, Pair<Double, String[]> o2) {
                    return o2.getX().compareTo(o1.getX());
                }
            });

            // Перенос в таблицу, нумерация
            for (Pair<Double, String[]> pair : rows) {
                pair.getY()[0] = String.valueOf(number++);
                t2Table.add(pair.getY());
            }
        }

        // Подсчет результатов для таблицы 1
        HashSet<EnrEntrantRequest> requestsBudget = new HashSet<>();
        HashSet<EnrEntrantRequest> requestsBudgetWC = new HashSet<>();
        HashSet<EnrEntrantRequest> requestsCommercial = new HashSet<>();
        HashSet<EnrEntrantRequest> requestsCommercialWC = new HashSet<>();
        HashSet<EnrEntrant> entrantsBudgetCompletedComp = new HashSet<>();
        HashSet<EnrEntrant> entrantsBudgetCompletedCompWC = new HashSet<>();
        HashSet<EnrEntrant> entrantsBudgetAccepted = new HashSet<>();
        HashSet<EnrEntrant> entrantsBudgetAcceptedWC = new HashSet<>();
        HashSet<EnrEntrant> entrantsCommercialCompletedComp = new HashSet<>();
        HashSet<EnrEntrant> entrantsCommercialCompletedCompWC = new HashSet<>();
        HashSet<EnrEntrant> entrantsCommercialAccepted = new HashSet<>();
        HashSet<EnrEntrant> entrantsCommercialAcceptedWC = new HashSet<>();

        // Общие результаты
        for (EnrRequestedCompetition competition : requestedCompetitionList)
        {
            if (competition.getCompetition().getType().getCompensationType().isBudget())
            {
                requestsBudget.add(competition.getRequest());
                if (EnrReportStageSelector.examPassedCheckByPriority(competition.getState().getPriority()))
                    entrantsBudgetCompletedComp.add(competition.getRequest().getEntrant());
                if (EnrReportStageSelector.enrollmentResultsCheckByPriority(competition.getState().getPriority()))
                    entrantsBudgetAccepted.add(competition.getRequest().getEntrant());
            }
            else
            {
                requestsCommercial.add(competition.getRequest());
                if (EnrReportStageSelector.examPassedCheckByPriority(competition.getState().getPriority()))
                    entrantsCommercialCompletedComp.add(competition.getRequest().getEntrant());
                if (EnrReportStageSelector.enrollmentResultsCheckByPriority(competition.getState().getPriority()))
                    entrantsCommercialAccepted.add(competition.getRequest().getEntrant());
            }
        }

        // Для тех, у кого были подготовительные курсы
        for (EnrRequestedCompetition competition : requestedCompetitionsWithCourse)
        {
            if (competition.getCompetition().getType().getCompensationType().isBudget())
            {
                requestsBudgetWC.add(competition.getRequest());
                if (EnrReportStageSelector.examPassedCheckByPriority(competition.getState().getPriority()))
                    entrantsBudgetCompletedCompWC.add(competition.getRequest().getEntrant());
                if (EnrReportStageSelector.enrollmentResultsCheckByPriority(competition.getState().getPriority()))
                    entrantsBudgetAcceptedWC.add(competition.getRequest().getEntrant());
            }
            else
            {
                requestsCommercialWC.add(competition.getRequest());
                if (EnrReportStageSelector.examPassedCheckByPriority(competition.getState().getPriority()))
                    entrantsCommercialCompletedCompWC.add(competition.getRequest().getEntrant());
                if (EnrReportStageSelector.enrollmentResultsCheckByPriority(competition.getState().getPriority()))
                    entrantsCommercialAcceptedWC.add(competition.getRequest().getEntrant());
            }
        }

        // Таблица 1, первая и вторая строки.
        String[][] t1Table = {
                {
                        String.valueOf(requestsBudget.size()),
                        String.valueOf(requestsCommercial.size()),
                        String.valueOf(requestsBudgetWC.size()),
                        String.valueOf(requestsCommercialWC.size()),
                        String.valueOf(entrantsBudgetCompletedComp.size()),
                        String.valueOf(entrantsCommercialCompletedComp.size()),
                        String.valueOf(entrantsBudgetCompletedCompWC.size()),
                        String.valueOf(entrantsCommercialCompletedCompWC.size()),
                        String.valueOf(entrantsBudgetAccepted.size()),
                        String.valueOf(entrantsCommercialAccepted.size()),
                        String.valueOf(entrantsBudgetAcceptedWC.size()),
                        String.valueOf(entrantsCommercialAcceptedWC.size())
                }
        };
        String[][] t12Table ={
                {
                        "в %",
                        String.valueOf(percentage(requestsBudgetWC.size(), requestsBudget.size())),
                        String.valueOf(percentage(requestsCommercialWC.size(), requestsCommercial.size())),
                        "",
                        String.valueOf(percentage(entrantsBudgetCompletedCompWC.size(), entrantsBudgetCompletedComp.size())),
                        String.valueOf(percentage(entrantsCommercialCompletedCompWC.size(), entrantsCommercialCompletedComp.size())),
                        "",
                        String.valueOf(percentage(entrantsBudgetAcceptedWC.size(), entrantsBudgetAccepted.size())),
                        String.valueOf(percentage(entrantsCommercialAcceptedWC.size(), entrantsCommercialAccepted.size()))
                }
        };


        // H - таблица
        List<String[]> hTable = new FilterParametersPrinter().getTable(filterAddon, model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo());
        if (model.isParallelActive())
            hTable.add(new String [] {"Поступающие параллельно",model.getParallel().getTitle()});

        // Запись в документ
        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("T1", t1Table);
        tableModifier.put("T1.2", t12Table);
        tableModifier.put("T2", t2Table.toArray(new String[t2Table.size()][]));
        tableModifier.put("H", hTable.toArray(new String[hTable.size()][]));
        tableModifier.put("H", new RtfRowIntercepterRawText());
        tableModifier.put("T2", new RtfRowIntercepterRawText());
        tableModifier.modify(document);

        return RtfUtil.toByteArray(document);
    }


    // возвращает какой процент составляет a от b.
    private int percentage(int a, int b)
    {
        return Math.round(100*(float)a/(float)b);
    }
}
