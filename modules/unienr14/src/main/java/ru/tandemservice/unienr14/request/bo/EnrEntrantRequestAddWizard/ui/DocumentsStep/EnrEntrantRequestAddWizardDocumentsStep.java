/* $Id$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.DocumentsStep;

import com.google.common.collect.Lists;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.SimplePublisherLinkResolver;
import org.tandemframework.shared.person.base.bo.Person.util.SecureRoleContext;
import org.tandemframework.shared.person.base.bo.PersonDocument.PersonDocumentManager;
import org.tandemframework.shared.person.base.entity.*;
import org.tandemframework.shared.person.base.util.PersonDocumentUtil;
import org.tandemframework.shared.person.base.util.PersonDocumentViewWrapper;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubDocumentRoleTab.EnrEntrantPubDocumentRoleTab;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrantBaseDocument.util.EnrEntrantDocumentViewWrapper;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantBaseDocument;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequestAttachment;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 6/5/14
 */
@Configuration
public class EnrEntrantRequestAddWizardDocumentsStep extends BusinessComponentManager
{
    public static final String PARAM_CREATED_OBJECTS_IDS = "createdObjectsIds";
    public static final String VIEW_PROPERTY_DISABLED_EDIT_DEL_DOCS = "disabledEditDeleteDocs";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(PersonDocumentManager.DOCUMENT_LIST_DS, documentListDSColumns(), documentListDSHandler()))
                .addDataSource(searchListDS(PersonDocumentManager.DOCUMENT_NOT_LINKED_LIST_DS, PersonDocumentManager.instance().documentNotLinkedListDSColumns(), PersonDocumentManager.instance().documentNotLinkedListDSHandler()))
            .create();
    }

    @Bean
    public ColumnListExtPoint documentListDSColumns() {
        return columnListExtPointBuilder(PersonDocumentManager.DOCUMENT_LIST_DS)
                .addColumn(publisherColumn("docType", PersonDocumentViewWrapper.VIEW_PROPERTY_DOCUMENT_TYPE_TITLE).publisherLinkResolver(PersonDocumentUtil.getPubResolver()))
                .addColumn(textColumn("seriaAndNumber", PersonDocumentViewWrapper.VIEW_PROPERTY_SERIA_AND_NUMBER).clickable(false))
                .addColumn(dateColumn("issuanceDate", PersonDocumentViewWrapper.VIEW_PROPERTY_ISSUANCE_DATE).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).clickable(false))

                .addColumn(publisherColumn("request", "title").publisherLinkResolver(new SimplePublisherLinkResolver(DataWrapper.ID)).entityListProperty(EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_REQUEST_ATTACHMENTS))

                .addColumn(textColumn("info", PersonDocumentViewWrapper.VIEW_PROPERTY_INFO).clickable(false))
                .addColumn(dateColumn("regDate", PersonDocumentViewWrapper.VIEW_PROPERTY_REG_DATE).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).clickable(false))
                .addColumn(actionColumn("scanCopy", CommonDefines.ICON_PRINT, "onClickDownloadScanCopy")
                        .disableHandler(entity -> {
                            Boolean withoutScanCopy = (Boolean) ((PersonDocumentViewWrapper) entity).getProperty(PersonDocumentViewWrapper.VIEW_PROPERTY_WITHOUT_SCAN_COPY);
                            return withoutScanCopy != null ? withoutScanCopy : true;
                        }))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER)
                        .disableHandler(entity ->
                        {
                            PersonDocumentViewWrapper wrapper = ((PersonDocumentViewWrapper) entity);
                            Set<Long> createdObjectsIds = (Set<Long>) wrapper.getProperty(VIEW_PROPERTY_DISABLED_EDIT_DEL_DOCS);
                            return createdObjectsIds == null || !createdObjectsIds.contains(wrapper.getId());
                        }))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER,
                        FormattedMessage.with().template("documentListDS.delete.alert")
                                .parameter(PersonDocumentViewWrapper.VIEW_PROPERTY_DOCUMENT_TYPE_TITLE)
                                .create())
                        .disableHandler(entity ->
                        {
                            PersonDocumentViewWrapper wrapper = ((PersonDocumentViewWrapper) entity);
                            Set<Long> createdObjectsIds = (Set<Long>) wrapper.getProperty(VIEW_PROPERTY_DISABLED_EDIT_DEL_DOCS);
                            return createdObjectsIds == null || !createdObjectsIds.contains(wrapper.getId());
                        }))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler documentListDSHandler() {
        return new DefaultSearchDataSourceHandler(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                Long securedObjectId = context.getNotNull(PersonDocumentManager.PARAM_PERSON_ROLE_ID);
                SecureRoleContext secureRoleContext = context.getNotNull(PersonDocumentManager.PARAM_SECURE_ROLE_CONTEXT);
                String personRoleName = context.getNotNull(PersonDocumentManager.PARAM_PERSON_ROLE_NAME);
                EnrEntrantRequest request = context.get(EnrEntrantPubDocumentRoleTab.PARAM_ENTRANT_REQUEST);
                Set<Long> createdObjectsIds = context.get(PARAM_CREATED_OBJECTS_IDS);

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(PersonDocumentRoleRel.class, "pdr")
                        .joinEntity("pdr", DQLJoinType.inner, PersonDocument.class, "pd", true, eq(property("pd", PersonDocument.id()), property("pdr", PersonDocumentRoleRel.document().id())))
                        .joinEntity("pd", DQLJoinType.left, PersonDocumentTypeRight.class, "aclviewandlink", true, eq(property("aclviewandlink", PersonDocumentTypeRight.documentType()), property("pd", PersonDocument.documentType())))
                        .joinEntity("pd", DQLJoinType.left, PersonDocumentTypeRight.class, "aclcreateandedit", true, eq(property("aclcreateandedit", PersonDocumentTypeRight.documentType()), property("pd", PersonDocument.documentType())))
                        .where(eq(property("pdr", PersonDocumentRoleRel.role().id()), value(securedObjectId)))
                        .where(or(
                                isNull(property("aclviewandlink")),
                                and(
                                        isNotNull(property("aclviewandlink")),
                                        eq(property("aclviewandlink", PersonDocumentTypeRight.documentContext()), value(personRoleName.toLowerCase()))
                                )
                        ))
                        .where(or(
                                isNull(property("aclcreateandedit")),
                                and(
                                        isNotNull(property("aclcreateandedit")),
                                        eq(property("aclcreateandedit", PersonDocumentTypeRight.documentContext()), value(personRoleName.toLowerCase()))
                                )
                        ))
                        .order(property("pdr", PersonDocumentRoleRel.document().documentType().priority()))
                        .order(property("pdr", PersonDocumentRoleRel.document().registrationDate()));

                List<PersonDocumentViewWrapper> result = Lists.newArrayList();

                result.add(new PersonDocumentViewWrapper(DataAccessServices.dao().getNotNull(Person.class, secureRoleContext.getPersonId()).getIdentityCard())
                        .viewProperty(PersonDocumentManager.WRAP_PROP_PERSON_ROLE_ID, securedObjectId)
                        .viewProperty(PersonDocumentManager.WRAP_PROP_SECURE_ROLE_CONTEXT, secureRoleContext)
                        .viewProperty(PersonDocumentManager.WRAP_PROP_PERSON_ROLE_NAME, personRoleName)
                        .viewProperty(VIEW_PROPERTY_DISABLED_EDIT_DEL_DOCS, createdObjectsIds));

                for(PersonEduDocument eduDocument : DataAccessServices.dao().getList(PersonEduDocument.class, PersonEduDocument.person().id(), secureRoleContext.getPersonId()))
                {
                    result.add(new PersonDocumentViewWrapper(eduDocument)
                            .viewProperty(PersonDocumentManager.WRAP_PROP_PERSON_ROLE_ID, securedObjectId)
                            .viewProperty(PersonDocumentManager.WRAP_PROP_SECURE_ROLE_CONTEXT, secureRoleContext)
                            .viewProperty(PersonDocumentManager.WRAP_PROP_PERSON_ROLE_NAME, personRoleName)
                            .viewProperty(VIEW_PROPERTY_DISABLED_EDIT_DEL_DOCS, createdObjectsIds));
                }

                for(Object[] obj : createStatement(builder).<Object[]>list())
                {
                    if(obj != null)
                    {
                        boolean viewAndLink = obj[2] != null;
                        boolean createAndEdit = obj[3] != null;

                        result.add(new PersonDocumentViewWrapper((PersonDocument) obj[1])
                                .viewProperty(PersonDocumentManager.WRAP_PROP_PERSON_ROLE_ID, securedObjectId)
                                .viewProperty(PersonDocumentManager.WRAP_PROP_SECURE_ROLE_CONTEXT, secureRoleContext)
                                .viewProperty(PersonDocumentManager.WRAP_PROP_PERSON_ROLE_NAME, personRoleName)
                                .viewProperty(PersonDocumentManager.WRAP_PROP_ACL_VIEW_AND_LINK, viewAndLink)
                                .viewProperty(PersonDocumentManager.WRAP_PROP_ACL_CREATE_AND_EDIT, createAndEdit)
                                .viewProperty(VIEW_PROPERTY_DISABLED_EDIT_DEL_DOCS, createdObjectsIds)

                        );
                    }
                }

                ///////////////////////////

                final Map<Long, List<DataWrapper>> doc2AttachmentsAllMap = SafeMap.get(ArrayList.class);
                for (EnrEntrantRequestAttachment attachment : DataAccessServices.dao().getList(EnrEntrantRequestAttachment.class, EnrEntrantRequestAttachment.entrantRequest().entrant().id(), securedObjectId))
                {
                    doc2AttachmentsAllMap.get(((EnrEntrantBaseDocument)attachment.getDocument()).getDocRelation().getDocument().getId())
                            .add(new DataWrapper(attachment.getEntrantRequest().getId(), attachment.getEntrantRequest().getRegNumber() + " (" + (attachment.isOriginalHandedIn() ? "оригинал" : "копия") + ")"));
                }

                for (Object[] objects : IUniBaseDao.instance.get().<Object[]>getList(new DQLSelectBuilder().fromEntity(EnrEntrantRequest.class, "r")
                        .column(property(EnrEntrantRequest.identityCard().id().fromAlias("r")))
                        .column(property(EnrEntrantRequest.id().fromAlias("r")))
                        .column(property(EnrEntrantRequest.regNumber().fromAlias("r")))
                        .where(eq(property(EnrEntrantRequest.identityCard().person().id().fromAlias("r")), value(secureRoleContext.getPersonId())))))
                {
                    final Long icId = (Long) objects[0];
                    final Long reqId = (Long) objects[1];
                    final String regNumber = (String) objects[2];

                    doc2AttachmentsAllMap.get(icId)
                            .add(new DataWrapper(reqId, regNumber + " (копия)"));
                }

                for (EnrEntrantRequest entrantRequest : IUniBaseDao.instance.get().<EnrEntrantRequest>getList(new DQLSelectBuilder()
                        .fromEntity(EnrEntrantRequest.class, "r").column("r")
                        .where(eq(property(EnrEntrantRequest.entrant().id().fromAlias("r")), value(securedObjectId)))))
                {
                    doc2AttachmentsAllMap.get(entrantRequest.getEduDocument().getId())
                            .add(new DataWrapper(entrantRequest.getId(), entrantRequest.getRegNumber() + " (" + (entrantRequest.isEduInstDocOriginalHandedIn() ? "оригинал" : "копия") + ")"));
                }

                Map<Long, List<DataWrapper>> doc2AttachmentsMap = SafeMap.get(ArrayList.class);

                if(request != null)
                {
                    for(Map.Entry<Long, List<DataWrapper>> entry: doc2AttachmentsAllMap.entrySet())
                    {
                        DataWrapper wrapper = null;
                        for(DataWrapper wrap : entry.getValue())
                        {
                            if(request.getId().equals(wrap.getId()))
                            {
                                wrapper = wrap;
                                break;
                            }
                        }
                        if(wrapper != null)
                        {
                            doc2AttachmentsMap.put(entry.getKey(), entry.getValue());
                        }

                    }

                    Iterator<PersonDocumentViewWrapper> iterator = result.iterator();

                    while (iterator.hasNext())
                    {
                        PersonDocumentViewWrapper wrapper = iterator.next();
                        if (!doc2AttachmentsMap.containsKey(wrapper.getId()))
                        {
                            iterator.remove();
                        }
                    }

                }
                else
                {
                    doc2AttachmentsMap = doc2AttachmentsAllMap;
                }



                for (ViewWrapper<IEntity> wrapper : result) {
                    wrapper.setViewProperty(EnrEntrantPubDocumentRoleTab.VIEW_PROPERTY_REQUEST_ATTACHMENTS, doc2AttachmentsMap.get(wrapper.getId()));
                }

                ///////////////////////////



                DSOutput output = ListOutputBuilder.get(input, result).pageable(false).build();

                output.setCountRecord(result.size() + 1);

                return output;
            }
        };
    }
}



    