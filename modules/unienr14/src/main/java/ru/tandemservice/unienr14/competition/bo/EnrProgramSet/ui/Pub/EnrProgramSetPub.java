/* $Id:$ */
package ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.button.ButtonListExtPoint;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.core.view.formatter.NewLineFormatter;

import org.tandemframework.shared.commonbase.base.util.SimplePublisherLinkResolver;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.logic.EnrCompetitionDSHandler;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.Pub.EnrCompetitionPub;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.ItemOrgUnitPlanTab.EnrProgramSetItemOrgUnitPlanTab;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.TargetAdmissionPlanTab.EnrProgramSetTargetAdmissionPlanTab;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;

/**
 * @author oleyba
 * @since 2/4/14
 */
@Configuration
public class EnrProgramSetPub extends BusinessComponentManager
{
    public static final String PROGRAM_SET_COMPETITION_DS = "programSetCompetitionDS";
    public static final String PROGRAM_SET_SECONDARY_COMPETITION_DS = "programSetSecondaryCompetitionDS";

    public static final String PROGRAM_SET_PUB_TAB_PANEL = "programSetPubTabPanel";
    public static final String PROGRAM_SET_INFO_TAB = "programSetInfoTab";
    public static final String PROGRAM_SET_TARGET_ADMISSION_PLAN_TAB = "programSetTargetAdmissionPlanTab";
    public static final String PROGRAM_SET_ITEM_ORG_UNIT_PLAN_TAB = "programSetItemOrgUnitPlanTab";

    public static final String BS_INFO_BLOCK_BUTTONS = "bsInfoBlockButtons";
    public static final String MASTER_INFO_BLOCK_BUTTONS = "masterInfoBlockButtons";
    public static final String HIGHER_INFO_BLOCK_BUTTONS = "higherInfoBlockButtons";
    public static final String SECONDARY_INFO_BLOCK_BUTTONS = "secondaryInfoBlockButtons";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(searchListDS(PROGRAM_SET_SECONDARY_COMPETITION_DS, programSetSecondaryCompetitionDSColumns(), competitionDSHandler()))
            .addDataSource(searchListDS(PROGRAM_SET_COMPETITION_DS, programSetCompetitionDSColumns(), competitionDSHandler()))
            .create();
    }

    @Bean
    public TabPanelExtPoint tabPanelExtPoint()
    {
        return tabPanelExtPointBuilder(PROGRAM_SET_PUB_TAB_PANEL)
            .addTab(htmlTab(PROGRAM_SET_INFO_TAB, "ProgramSetInfoTab"))
            .addTab(componentTab(PROGRAM_SET_TARGET_ADMISSION_PLAN_TAB, EnrProgramSetTargetAdmissionPlanTab.class)
                .visible("ui:programSetTargetAdmissionPlanTabVisible")
                .permissionKey("enr14ProgramSetPubTAPlanTabView"))
            .addTab(componentTab(PROGRAM_SET_ITEM_ORG_UNIT_PLAN_TAB, EnrProgramSetItemOrgUnitPlanTab.class)
                .visible("ui:programSetItemOrgUnitPlanTabVisible")
                .permissionKey("enr14ProgramSetPubItemOUPlanTabView"))
            .create();
    }

    @Bean
    @SuppressWarnings("unchecked")
    public IBusinessHandler<DSInput, DSOutput> competitionDSHandler()
    {
        return new EnrCompetitionDSHandler(getName()).setPageable(false);
    }

    @Bean
    public ColumnListExtPoint programSetSecondaryCompetitionDSColumns()
    {
        return columnListExtPointBuilder(PROGRAM_SET_SECONDARY_COMPETITION_DS)
            .addColumn(textColumn("orgUnit", EnrCompetition.programSetOrgUnit().orgUnit().institutionOrgUnit().orgUnit().shortTitleWithTopEmphasized().s()))
            .addColumn(textColumn("formativeOrgUnit", EnrCompetition.programSetOrgUnit().formativeOrgUnit().shortTitleWithTopEmphasized().s()))
            .addColumn(textColumn("eduLevelRequirement", EnrCompetition.eduLevelRequirement().shortTitle().s()))
            .addColumn(textColumn("competitionType", EnrCompetition.type().shortTitle().s()))
            .addColumn(textColumn("plan", EnrCompetition.plan().s()))
            .addColumn(publisherColumn("chosen", EnrCompetitionDSHandler.VIEW_PROP_REQUEST_COUNT)
                .publisherLinkResolver(new SimplePublisherLinkResolver(EnrCompetition.id())
                    .setComponent(EnrCompetitionPub.class))
            )
            .addColumn(textColumn("passed", EnrCompetitionDSHandler.VIEW_PROP_PASSED))
            .addColumn(textColumn("exams", EnrCompetitionDSHandler.VIEW_PROP_EXAMS).formatter(NewLineFormatter.NOBR_IN_LINES))
            .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, "onEditCompetitionPlan").permissionKey("enr14CompetitionSecondaryPubEditPlan"))
            .create();
    }

    @Bean
    public ColumnListExtPoint programSetCompetitionDSColumns()
    {
        return columnListExtPointBuilder(PROGRAM_SET_COMPETITION_DS)
            .addColumn(textColumn("orgUnit", EnrCompetition.programSetOrgUnit().orgUnit().institutionOrgUnit().orgUnit().shortTitleWithTopEmphasized().s()))
            .addColumn(textColumn("formativeOrgUnit", EnrCompetition.programSetOrgUnit().formativeOrgUnit().shortTitleWithTopEmphasized().s()))
            .addColumn(textColumn("eduLevelRequirement", EnrCompetition.eduLevelRequirement().shortTitle().s()))
            .addColumn(textColumn("competitionType", EnrCompetition.type().shortTitle().s()))
            .addColumn(textColumn("plan", EnrCompetition.plan().s()))
            .addColumn(publisherColumn("chosen", EnrCompetitionDSHandler.VIEW_PROP_REQUEST_COUNT)
                .publisherLinkResolver(new SimplePublisherLinkResolver(EnrCompetition.id())
                    .setComponent(EnrCompetitionPub.class))
            )
            .addColumn(textColumn("passed", EnrCompetitionDSHandler.VIEW_PROP_PASSED))
            .addColumn(textColumn("exams", EnrCompetitionDSHandler.VIEW_PROP_EXAMS).formatter(NewLineFormatter.NOBR_IN_LINES))
            .addColumn(toggleColumn("allowProgramPriorities", EnrCompetition.P_ALLOW_PROGRAM_PRIORITIES)
                .toggleOnListener("onToggleAllowProgramPriorities")
                .toggleOffListener("onToggleAllowProgramPriorities"))
            .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, "onEditCompetitionPlan").permissionKey("enr14ProgramSetPubEditPlan"))
            .create();
    }

    @Bean
    public ButtonListExtPoint bsInfoBlockButtons()
    {
        return buttonListExtPointBuilder(BS_INFO_BLOCK_BUTTONS).create();
    }

    @Bean
    public ButtonListExtPoint masterInfoBlockButtons()
    {
        return buttonListExtPointBuilder(MASTER_INFO_BLOCK_BUTTONS).create();
    }

    @Bean
    public ButtonListExtPoint higherInfoBlockButtons()
    {
        return buttonListExtPointBuilder(HIGHER_INFO_BLOCK_BUTTONS).create();
    }

    @Bean
    public ButtonListExtPoint secondaryInfoBlockButtons()
    {
        return buttonListExtPointBuilder(SECONDARY_INFO_BLOCK_BUTTONS).create();
    }
}