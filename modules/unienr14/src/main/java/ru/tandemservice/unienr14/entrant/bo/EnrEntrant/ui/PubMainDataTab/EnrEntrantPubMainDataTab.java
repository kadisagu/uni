/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubMainDataTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.block.BlockListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author oleyba
 * @since 4/10/13
 */
@Configuration
public class EnrEntrantPubMainDataTab extends BusinessComponentManager
{
    public static final String BLOCK_LIST_ENTRANT_DATA = "enrEntrantDataBlockList";
    public static final String BLOCK_MAIN = "mainEntrantBlock";
    public static final String BLOCK_PREPARATION = "preparationEntrantBlock";
    public static final String BLOCK_DORMITORY = "dormitoryEntrantBlock";
    private static final String BLOCK_PERSONAL = "personalEntrantBlock";
    private static final String BLOCK_WORKPLACE = "workplaceEntrantBlock";
    private static final String BLOCK_LANGUAGE = "languageEntrantBlock";
    private static final String BLOCK_SPORT = "sportEntrantBlock";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
        .create();
    }

    @Bean
    public BlockListExtPoint enrEntrantDataBlockList()
    {
        return blockListExtPointBuilder(BLOCK_LIST_ENTRANT_DATA)
            .addBlock(htmlBlock(BLOCK_MAIN, "MainDataBlock").permissionKey("enr14EntrantPubContactDataTabMainDataBlockView"))
            .addBlock(htmlBlock(BLOCK_LANGUAGE, "ForeignLangDataBlock").permissionKey("viewPersonForeignLanguageList_enrEntrant"))
            .addBlock(htmlBlock(BLOCK_PREPARATION, "PreparationDataBlock").permissionKey("enr14EntrantPubContactDataTabPreparationDataBlockView"))
            .addBlock(htmlBlock(BLOCK_WORKPLACE, "WorkplaceDataBlock").permissionKey("enr14EntrantPubContactDataTabWorkplaceDataBlockView"))
            .addBlock(htmlBlock(BLOCK_PERSONAL, "PersonalDataBlock").permissionKey("enr14EntrantPubContactDataTabPersonalDataBlockView"))
            .addBlock(htmlBlock(BLOCK_DORMITORY, "DormitoryDataBlock").permissionKey("enr14EntrantPubContactDataTabDormitoryDataBlockView"))
            .addBlock(htmlBlock(BLOCK_SPORT, "SportAchievDataBlock").permissionKey("viewPersonSportAchievementList_enrEntrant"))
            .create();
    }
}
