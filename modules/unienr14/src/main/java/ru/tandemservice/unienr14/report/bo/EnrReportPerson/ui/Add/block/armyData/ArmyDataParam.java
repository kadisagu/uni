/* $Id$ */
package ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.block.armyData;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.*;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.EnrReportPersonAddUI;

import java.util.Date;

/**
 * @author Nikolay Fedorovskih
 * @since 04.07.2015
 */
public class ArmyDataParam implements IReportDQLModifier
{
    private IReportParam<DataWrapper> _passArmy = new ReportParam<>();
    private IReportParam<Date> _beginArmyFrom = new ReportParam<>();
    private IReportParam<Date> _beginArmyTo = new ReportParam<>();
    private IReportParam<Date> _endArmyFrom = new ReportParam<>();
    private IReportParam<Date> _endArmyTo = new ReportParam<>();
    private IReportParam<Integer> _endArmyYearFrom = new ReportParam<>();
    private IReportParam<Integer> _endArmyYearTo = new ReportParam<>();

    private String entrantAlias(ReportDQL dql)
    {
        // соединяем абитуриента
        String entrantAlias = dql.innerJoinEntity(EnrEntrant.class, EnrEntrant.person());

        // добавляем сортировку
        dql.order(entrantAlias, EnrEntrant.enrollmentCampaign().id());

        return entrantAlias;
    }

    @Override
    public void validateReportParams(ErrorCollector errorCollector)
    {
        ReportValidationUtil.validateDatePeriod(getBeginArmyFrom(), getBeginArmyTo(), errorCollector, "«Дата начала службы с» должна быть не позднее «Даты начала службы по».", "beginArmyFrom", "beginArmyTo");
        ReportValidationUtil.validateDatePeriod(getBeginArmyFrom(), getBeginArmyTo(), errorCollector, "«Дата окончания службы с» должна быть не позднее «Даты окончания службы по».", "endArmyFrom", "endArmyTo");
        ReportValidationUtil.validateIntPeriod(getEndArmyYearFrom(), getEndArmyYearTo(), errorCollector, "«Год увольнения в запас с» должен быть не больше «Года увольнения в запас по».", "endArmyYearFrom", "endArmyYearTo");
    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        if (_passArmy.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "armyData.passArmy", ((ITitled)_passArmy.getData()).getTitle());

            FilterUtils.applySelectFilter(dql.builder, entrantAlias(dql), EnrEntrant.passArmy(), TwinComboDataSourceHandler.getSelectedValue(_passArmy.getData()));
        }

        if (_beginArmyFrom.isActive() || _beginArmyTo.isActive())
        {
            if (_beginArmyFrom.isActive()) {
                printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "armyData.beginArmyFrom", DateFormatter.DEFAULT_DATE_FORMATTER.format(_beginArmyFrom.getData()));
            }

            if (_beginArmyTo.isActive()) {
                printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "armyData.beginArmyTo", DateFormatter.DEFAULT_DATE_FORMATTER.format(_beginArmyTo.getData()));
            }

            final Date dateFrom = _beginArmyFrom.isActive() ? _beginArmyFrom.getData() : null;
            final Date dateTo = _beginArmyTo.isActive() ? _beginArmyTo.getData() : null;
            FilterUtils.applyBetweenFilter(dql.builder, entrantAlias(dql), EnrEntrant.beginArmy().s(), dateFrom, dateTo);
        }

        if (_endArmyFrom.isActive() || _endArmyTo.isActive())
        {
            if (_endArmyFrom.isActive()) {
                printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "armyData.endArmyFrom", DateFormatter.DEFAULT_DATE_FORMATTER.format(_endArmyFrom.getData()));
            }

            if (_endArmyTo.isActive()) {
                printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "armyData.endArmyTo", DateFormatter.DEFAULT_DATE_FORMATTER.format(_endArmyTo.getData()));
            }

            final Date dateFrom = _endArmyFrom.isActive() ? _endArmyFrom.getData() : null;
            final Date dateTo = _endArmyTo.isActive() ? _endArmyTo.getData() : null;
            FilterUtils.applyBetweenFilter(dql.builder, entrantAlias(dql), EnrEntrant.endArmy().s(), dateFrom, dateTo);
        }

        if (_endArmyYearFrom.isActive() || _endArmyYearTo.isActive())
        {
            if (_endArmyYearFrom.isActive()) {
                printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "armyData.endArmyYearFrom", String.valueOf(_endArmyYearFrom.getData()));
            }

            if (_endArmyYearTo.isActive()) {
                printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "armyData.endArmyYearTo", String.valueOf(_endArmyYearTo.getData()));
            }

            final Integer yearFrom = _endArmyYearFrom.isActive() ? _endArmyYearFrom.getData() : null;
            final Integer yearTo = _endArmyYearTo.isActive() ? _endArmyYearTo.getData() : null;
            FilterUtils.applyBetweenFilter(dql.builder, entrantAlias(dql), EnrEntrant.endArmyYear().s(), yearFrom, yearTo);
        }
    }

    public IReportParam<DataWrapper> getPassArmy()
    {
        return _passArmy;
    }

    public IReportParam<Date> getBeginArmyFrom()
    {
        return _beginArmyFrom;
    }

    public IReportParam<Date> getBeginArmyTo()
    {
        return _beginArmyTo;
    }

    public IReportParam<Date> getEndArmyFrom()
    {
        return _endArmyFrom;
    }

    public IReportParam<Date> getEndArmyTo()
    {
        return _endArmyTo;
    }

    public IReportParam<Integer> getEndArmyYearFrom()
    {
        return _endArmyYearFrom;
    }

    public IReportParam<Integer> getEndArmyYearTo()
    {
        return _endArmyYearTo;
    }
}