/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReportBase.logic;

import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.core.common.ITitled;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.EnrReportBaseManager;

/**
 * @author oleyba
 * @since 5/12/14
 */
public class EnrReportDefinition implements ITitled
{
    private final String name;
    private final Class<? extends BusinessComponentManager> businessComponent;
    private final IEnrStorableReportDesc reportDesc;

    public EnrReportDefinition(String name, Class<? extends BusinessComponentManager> businessComponent, IEnrStorableReportDesc reportDesc)
    {
        this.businessComponent = businessComponent;
        this.name = name;
        this.reportDesc = reportDesc;
    }

    public Class<? extends BusinessComponentManager> getBusinessComponent()
    {
        return businessComponent;
    }

    public String getName()
    {
        return name;
    }

    public IEnrStorableReportDesc getReportDesc()
    {
        return reportDesc;
    }

    @Override
    public String getTitle()
    {
        return EnrReportBaseManager.instance().getProperty(name + ".title");
    }

    public String getVisibleExpression()
    {
        final String property = name + ".visible";
        if (EnrReportBaseManager.instance().hasProperty(property)) {
            return EnrReportBaseManager.instance().getProperty(property);
        } else {
            return null;
        }
    }
}
