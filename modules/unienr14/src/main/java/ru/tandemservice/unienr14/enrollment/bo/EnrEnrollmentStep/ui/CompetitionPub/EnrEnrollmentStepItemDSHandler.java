/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.ui.CompetitionPub;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrTargetAdmissionKind;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItem;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItemOrderInfo;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantCustomState;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 12/17/13
 */
public class EnrEnrollmentStepItemDSHandler extends DefaultSearchDataSourceHandler
{
    public EnrEnrollmentStepItemDSHandler(String ownerId)
    {
        super(ownerId);
    }

    public static final String BIND_ENR_STEP = "step";
    public static final String BIND_COMPETITION = "competition";
    public static final String BIND_TA_KIND = "taKind";

    public static final String VIEW_PROP_PREV_ORDERS = "prevOrders";
    public static final String VIEW_PROP_CURRENT_ORDER = "currentOrder";
    public static final String VIEW_PROP_OTHER_RECOMMENDATION = "otherRecommendation";
    public static final String VIEW_PROP_OTHER_ENR_SELECTION = "otherSelection";
    public static final String VIEW_PROP_CUSTOM_STATES = "customStates";
    public static final String VIEW_PROP_RELATIVE_POSITION = "relativePosition";
    public static final String VIEW_PROP_RELATIVE_RANK = "relativeRank";


    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context) {

        EnrEnrollmentStep step = context.get(BIND_ENR_STEP);

        final DQLSelectBuilder dql = new DQLSelectBuilder()
        .fromEntity(EnrEnrollmentStepItem.class, "i")
        .column(property("i"))
        .order(property(EnrEnrollmentStepItem.entity().position().fromAlias("i")))
        .where(eq(property(EnrEnrollmentStepItem.step().fromAlias("i")), value(step)))
        .where(eq(property(EnrEnrollmentStepItem.entity().requestedCompetition().competition().fromAlias("i")), commonValue(context.get(BIND_COMPETITION))))
        ;

        Map<Long, Integer> relativeRankMap = Maps.newHashMap();
        EnrTargetAdmissionKind taKind = context.get(BIND_TA_KIND);
        if (null != taKind) {
            dql
                .joinPath(DQLJoinType.inner, EnrEnrollmentStepItem.entity().fromAlias("i"), "r")
                .joinEntity("i", DQLJoinType.inner, EnrRequestedCompetitionTA.class, "rta", eq(property("rta"), property(EnrRatingItem.requestedCompetition().fromAlias("r"))))
                .where(eq(property(EnrRequestedCompetitionTA.targetAdmissionKind().targetAdmissionKind().fromAlias("rta")), value(taKind)));

            int relativeRank = 0;
            Integer prevRank = null;
            for (EnrEnrollmentStepItem item : dql.createStatement(context.getSession()).<EnrEnrollmentStepItem>list())
            {
                Integer currRank = item.getEntity().getPositionAbsolute();
                relativeRank = prevRank == null ? 1 : currRank.equals(prevRank) ? relativeRank : relativeRank + 1;
                relativeRankMap.put(item.getId(), relativeRank);

                prevRank = item.getEntity().getPositionAbsolute();
            }
        }

        DSOutput output = DQLSelectOutputBuilder.get(input, dql, context.getSession()).order().build();

        List<EnrEnrollmentStepItem> itemList = new ArrayList<>();
        for (Object item: output.getRecordList())
            itemList.add((EnrEnrollmentStepItem) item);

        Map<EnrEnrollmentStepItem, List<EnrEnrollmentStepItemOrderInfo>> infoMap = SafeMap.get(ArrayList.class);

        for (EnrEnrollmentStepItemOrderInfo orderInfo : IUniBaseDao.instance.get().getList(EnrEnrollmentStepItemOrderInfo.class, EnrEnrollmentStepItemOrderInfo.item().step(), step, EnrEnrollmentStepItemOrderInfo.orderInfo().s())) {
            infoMap.get(orderInfo.getItem()).add(orderInfo);
        }

        Map<EnrEntrant, List<EnrEnrollmentStepItem>> otherRecommendations = SafeMap.get(ArrayList.class);
        Map<EnrEntrant, List<EnrEnrollmentStepItem>> otherEnrollmentMarks = SafeMap.get(ArrayList.class);

        final DQLSelectBuilder otherRecDQL = new DQLSelectBuilder()
            .fromEntity(EnrEnrollmentStepItem.class, "i").column(property("i"))
            .order(property(EnrEnrollmentStepItem.entity().position().fromAlias("i")))
            .where(eq(property(EnrEnrollmentStepItem.step().fromAlias("i")), value(step)))
            .where(or(
                eq(property(EnrEnrollmentStepItem.recommended().fromAlias("i")), value(Boolean.TRUE)),
                eq(property(EnrEnrollmentStepItem.shouldBeEnrolled().fromAlias("i")), value(Boolean.TRUE))
            ))
            .where(ne(property(EnrEnrollmentStepItem.entity().requestedCompetition().competition().fromAlias("i")), commonValue(context.get(BIND_COMPETITION))))
            .where(exists(new DQLSelectBuilder()
                .fromEntity(EnrEnrollmentStepItem.class, "i1")
                .column(property("i1"))
                .order(property(EnrEnrollmentStepItem.entity().position().fromAlias("i1")))
                .where(eq(property(EnrEnrollmentStepItem.step().fromAlias("i1")), value(step)))
                .where(eq(property(EnrEnrollmentStepItem.entity().requestedCompetition().competition().fromAlias("i1")), commonValue(context.get(BIND_COMPETITION))))
                .where(eq(property(EnrEnrollmentStepItem.entity().requestedCompetition().request().entrant().fromAlias("i1")), property(EnrEnrollmentStepItem.entity().requestedCompetition().request().entrant().fromAlias("i"))))
                .buildQuery()))
            ;
        for (EnrEnrollmentStepItem otherRec : otherRecDQL.createStatement(context.getSession()).<EnrEnrollmentStepItem>list()) {
            if (otherRec.isRecommended()) {
                otherRecommendations.get(otherRec.getEntity().getEntrant()).add(otherRec);
            }
            if (otherRec.isShouldBeEnrolled()) {
                otherEnrollmentMarks.get(otherRec.getEntity().getEntrant()).add(otherRec);
            }
        }

        List<EnrEntrant> entrantList = Lists.newArrayList();
        for (DataWrapper wrapper : DataWrapper.wrap(output))
            entrantList.add(((EnrEnrollmentStepItem) wrapper.getWrapped()).getEntity().getEntrant());
        Map<EnrEntrant, List<EnrEntrantCustomState>> customStatesMap = EnrEntrantManager.instance().dao().getActiveCustomStatesMap(entrantList, new Date());

        int position = 0;
        int relativePosition = output.getStartRecord();
        int relativeRank;
        for (DataWrapper wrapper : DataWrapper.wrap(output))
        {
            EnrEnrollmentStepItem item = IUniBaseDao.instance.get().get(EnrEnrollmentStepItem.class, wrapper.getId());

            List<IdentifiableWrapper> prevOrders = new ArrayList<>();
            long tempId = -1;
            for (EnrEnrollmentStepItemOrderInfo orderInfo : infoMap.get(item)) {
                IdentifiableWrapper orderWrapper = new IdentifiableWrapper(
                    orderInfo.getExtract() == null ? tempId-- : orderInfo.getExtract().getParagraph().getOrder().getId(),
                        orderInfo.getExtract().getShortTitle() + "  - " +  orderInfo.getSolution().getTitle()
                );
                prevOrders.add(orderWrapper);
            }

            EnrEnrollmentExtract extract = IUniBaseDao.instance.get().get(EnrEnrollmentExtract.class, EnrEnrollmentExtract.entity(), item.getEntity().getRequestedCompetition());
            List<IdentifiableWrapper> currentOrder = new ArrayList<>();
            if (null != extract) {
                IdentifiableWrapper orderWrapper = new IdentifiableWrapper(
                    extract.getOrder().getId(),
                    extract.getOrderInfo()
                );
                currentOrder.add(orderWrapper);
                prevOrders.remove(orderWrapper);
            }

            List<OtherRecommendationWrapper> otherRecWrapper = new ArrayList<>();
            for (EnrEnrollmentStepItem otherRec : otherRecommendations.get(item.getEntity().getEntrant())) {
                otherRecWrapper.add(new OtherRecommendationWrapper(step, otherRec.getEntity().getRequestedCompetition()));
            }

            List<OtherRecommendationWrapper> otherEnrWrapper = new ArrayList<>();
            for (EnrEnrollmentStepItem otherEnr : otherEnrollmentMarks.get(item.getEntity().getEntrant())) {
                otherEnrWrapper.add(new OtherRecommendationWrapper(step, otherEnr.getEntity().getRequestedCompetition()));
            }

            EnrEnrollmentStepItem prev = position == 0 ? null : itemList.get(position - 1);
            EnrEnrollmentStepItem next = position + 2 > itemList.size() ? null : itemList.get(position + 1);

            if (null != taKind)
            {
                relativePosition++;
                relativeRank = relativeRankMap.get(wrapper.getId());
            }
            else
            {
                relativePosition = item.getEntity().getPosition();
                relativeRank = item.getEntity().getPositionAbsolute();
            }

            wrapper.setProperty(VIEW_PROP_OTHER_RECOMMENDATION, otherRecWrapper);
            wrapper.setProperty(VIEW_PROP_OTHER_ENR_SELECTION, otherEnrWrapper);
            wrapper.setProperty(VIEW_PROP_PREV_ORDERS, prevOrders);
            wrapper.setProperty(VIEW_PROP_CURRENT_ORDER, currentOrder);
            wrapper.setProperty(VIEW_PROP_CUSTOM_STATES, customStatesMap.get(item.getEntity().getEntrant()));
            wrapper.setProperty(VIEW_PROP_RELATIVE_POSITION, relativePosition);
            wrapper.setProperty(VIEW_PROP_RELATIVE_RANK, relativeRank);
            wrapper.setProperty("editDisabled", prevOrders.isEmpty());
            wrapper.setProperty("upDisabled", prev == null || item.getEntity().getPositionAbsolute() != prev.getEntity().getPositionAbsolute());
            wrapper.setProperty("downDisabled", next == null || item.getEntity().getPositionAbsolute() != next.getEntity().getPositionAbsolute());
            position++;
        }

        return output;
    }

    public static class OtherRecommendationWrapper extends IdentifiableWrapper {
        private static final long serialVersionUID = 1L;

        private EnrEnrollmentStep step;
        private EnrRequestedCompetition requestedCompetition;

        public OtherRecommendationWrapper(EnrEnrollmentStep step, EnrRequestedCompetition requestedCompetition)
        {
            super(requestedCompetition.getId(), requestedCompetition.getTitle() + " (Пр: " + requestedCompetition.getPriority() + (requestedCompetition.isParallel() ? ", ПАРАЛЛ." : "") + ")");
            this.step = step;
            this.requestedCompetition = requestedCompetition;
        }

        public Map getPublisherParameters() {
            ParametersMap parametersMap = new ParametersMap()
                .add(EnrEnrollmentStepCompetitionPubUI.PARAM_STEP, step.getId())
                .add(EnrEnrollmentStepCompetitionPubUI.PARAM_COMPETITION, requestedCompetition.getCompetition().getId());
            if (requestedCompetition instanceof EnrRequestedCompetitionTA && step.getEnrollmentCampaign().getSettings().isTargetAdmissionCompetition()) {
                parametersMap.add(EnrEnrollmentStepCompetitionPubUI.PARAM_TARGET_ADMISSION_KIND, ((EnrRequestedCompetitionTA)requestedCompetition).getTargetAdmissionKind().getTargetAdmissionKind().getId());
            }
            return parametersMap;
        }
    }
}



