package ru.tandemservice.unienr14.report.entity;

import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.ForEnrollmentCommissionAdd.EnrReportForEnrollmentCommissionAdd;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrReport;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrStorableReportDesc;
import ru.tandemservice.unienr14.report.entity.gen.EnrReportForEnrollmentCommissionGen;

import java.util.Arrays;
import java.util.List;

/**
 * Сводка для отборочных комиссий
 */
public class EnrReportForEnrollmentCommission extends EnrReportForEnrollmentCommissionGen implements IEnrReport
{

    public static final String REPORT_KEY = "enr14ReportForEnrollmentCommission";

    @SuppressWarnings("unchecked")
    private static List<String> properties = Arrays.asList(new String[]{
            P_ENROLLMENT_STAGE,
            P_REQUEST_TYPE,
            P_COMPENSATION_TYPE,
            P_PROGRAM_FORM,
            P_COMPETITION_TYPE,
            P_ENR_ORG_UNIT,
            P_FORMATIVE_ORG_UNIT,
            P_PROGRAM_SUBJECT,
            P_EDU_PROGRAM,
            P_PROGRAM_SET,
            P_ENROLLMENT_COMISSION,
            P_PARALLEL});


    public static IEnrStorableReportDesc getDescripton() {
        return new IEnrStorableReportDesc() {
            @Override public String getReportKey() { return REPORT_KEY; }
            @Override public Class<? extends IEnrReport> getReportClass() { return EnrReportForEnrollmentCommission.class; }
            @Override public List<String> getPropertyList() { return properties; }

            @Override public Class<? extends BusinessComponentManager> getAddFormComponent() { return EnrReportForEnrollmentCommissionAdd.class; }
            @Override public String getPubTitle() { return "Отчет «Сводка для отборочных комиссий»"; }
            @Override public String getListTitle() { return "Список отчетов «Сводка для отборочных комиссий»"; }
        };
    }

    @Override
    public IEnrStorableReportDesc getDesc() {
        return getDescripton();
    }

    @Override
    public String getPeriodTitle() {
        return "с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateFrom()) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateTo());
    }
}