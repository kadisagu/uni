package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.select;

import com.google.common.base.Objects;
import com.google.common.collect.Collections2;
import com.google.common.collect.Ordering;
import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.shared.commonbase.base.util.Wiki;
import org.tandemframework.shared.sandbox.utils.FastBeanFunction;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.xml.EnrXmlEnrollmentStepState;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItem;

import java.util.*;

/**
 * @author vdanilov
 */
@Wiki(url="http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20686888")
public class EnrEnrollmentStepRecommendationContext extends EnrEnrollmentStepBaseContext {

    protected EnrEnrollmentStepRecommendationContext(EnrEnrollmentStep step, boolean debug) {
        super(step, debug);
    }

    @Override
    public void applyResults() {
        IUniBaseDao.instance.get().doInTransaction(session -> {

            // выставляем признак в объекты
            for (final EnrSelectionGroup group: EnrEnrollmentStepRecommendationContext.this.getStepItemGroupMap().values()) {
                for (final EnrSelectionItem selectionItem : group.getItemMap().values()) {
                    final EnrEnrollmentStepItem item = (EnrEnrollmentStepItem) selectionItem.getStepItem();
                    item.setRecommended(selectionItem.isSelected());
                    session.saveOrUpdate(item);
                }
            }

            // сохраняем состояние и признак
            final EnrEnrollmentStep step = EnrEnrollmentStepRecommendationContext.this.getStep();
            step.setAutoRecommended(true);
            session.saveOrUpdate(step);

            // сохраняем в базу
            session.flush();
            return null;
        });
    }

    private class GroupCheckWrapper
    {
        private final EnrSelectionGroup selectionGroup;
        public EnrSelectionGroup getSelectionGroup() { return this.selectionGroup; }

        private final List<String> errors = new ArrayList<>(4);
        private final List<IEnrSelectionPrevEnrollmentFact> extractsRecommendedByOther;
        private final List<EnrSelectionItem> stepItemsAvailable;
        private final List<EnrSelectionItem> stepItemsRecommendedByThis;
        private final List<EnrSelectionItem> stepItemsRecommendedByOther;
        private final List<EnrSelectionItem> stepItemsRecommendedByThisOrOther;
        private final int planBalance;
        private final int entrantBalance;

        private GroupCheckWrapper(final EnrSelectionGroup selectionGroup)
        {
            this.selectionGroup = selectionGroup;

            final Collection<EnrSelectionItem> allItems = this.getSelectionGroup().getItemMap().values();

            this.extractsRecommendedByOther = new ArrayList<>(this.getSelectionGroup().getCompetitionExtractUsedByOtherGroups(input -> input.getStepItem().isRecommended()));

            // только те, кого можно
            this.stepItemsAvailable = new ArrayList<>(Collections2.filter(allItems, EnrEnrollmentStepRecommendationContext.this::isSelectionAllowed));

            // даже те, кого было нельзя
            this.stepItemsRecommendedByThis = new ArrayList<>(Collections2.filter(allItems, input -> input.getStepItem().isRecommended()));

            // даже те, кого было нельзя
            this.stepItemsRecommendedByOther = new ArrayList<>(this.getSelectionGroup().getAllowdStepItemsUsedByOtherGroups(input -> input.getStepItem().isRecommended()));

            final Set<EnrSelectionItem> s = new HashSet<>();
            s.addAll(this.stepItemsRecommendedByThis);
            s.addAll(this.stepItemsRecommendedByOther);
            this.stepItemsRecommendedByThisOrOther = new ArrayList<>(s);

            this.planBalance =
            (
            this.getSelectionGroup().getCompetitionPlan()
            - this.getSelectionGroup().getCompetitionExtractSize()
            + this.extractsRecommendedByOther.size()
            - this.stepItemsRecommendedByThis.size()
            );

            this.entrantBalance = (
            this.stepItemsAvailable.size()
            - this.stepItemsRecommendedByThisOrOther.size()
            );


            // messages

            // план
            if (this.planBalance < 0) {
                this.errors.add("Превышен план приема.");
            }

            // остатки места, но есть абитуриентны
            if (this.planBalance > 0 && this.entrantBalance > 0) {
                this.errors.add("Остались нераспределенные места.");
            }

            // рекомендован, но не доступен для рекомендации
            {
                final Collection<EnrSelectionItem> nonAvailableRecommendaton = CollectionUtils.subtract(this.stepItemsRecommendedByThis, this.stepItemsAvailable);
                for (final EnrSelectionItem w: nonAvailableRecommendaton) {
                    this.errors.add("Абитуриент рекомендован с нарушением правил выбора: " + w.getEntrantFio());
                }
            }

            // рекомендованные параллельщики
            for (final EnrSelectionItem w: this.stepItemsRecommendedByThis) {
                if (!w.getRequestedCompetition().isParallel()) { continue; }

                boolean existsNonParallel = false;
                if (!existsNonParallel) {
                    for (final IEnrSelectionItemPrevEnrollmentInfo info: w.getAddEnrollList()) {
                        if (info.getRequestedCompetition().isParallel()) { continue; }
                        if (info.isCancelled()) { continue; }
                        existsNonParallel = true;
                        break;
                    }
                }
                if (!existsNonParallel) {
                    for (final EnrSelectionGroup otherGroup: selectionGroup.getOtherStepItemGroups()) {
                        final IEnrSelectionPrevEnrollmentFact otherExtract = otherGroup.getCompetitionExtractMap().get(w.getEntrant());
                        if (null == otherExtract) { continue; }
                        if (otherExtract.getRequestedCompetition().isParallel()) { continue; }
                        existsNonParallel = true;
                        break;
                    }
                }
                if (!existsNonParallel) {
                    for (final EnrSelectionGroup otherGroup: selectionGroup.getOtherStepItemGroups()) {
                        final EnrSelectionItem otherItem = otherGroup.getItemMap().get(w.getEntrant());
                        if (null == otherItem) { continue; }
                        if (otherItem.getRequestedCompetition().isParallel()) { continue; }
                        if (!otherItem.getStepItem().isRecommended()) { continue; }
                        existsNonParallel = true;
                        break;
                    }
                }

                if (!existsNonParallel) {
                    this.errors.add("Абитуриент рекомендован на параллельное обучение при отсутствии рекомендации/зачисления на основное: " + w.getEntrantFio());
                }
            }

            // повторно рекомендованные
            {
                final Collection<EnrSelectionItem> duplicateRecommendation = CollectionUtils.intersection(this.stepItemsRecommendedByThis, this.stepItemsRecommendedByOther);
                for (final EnrSelectionItem w: duplicateRecommendation) {
                    if (w.getRequestedCompetition().isParallel()) {
                        this.errors.add("Абитуриент рекомендован более одного раза (на параллельное обучение): " + w.getEntrantFio());
                    } else {
                        this.errors.add("Абитуриент рекомендован более одного раза: " + w.getEntrantFio());
                    }
                }
            }

            // пропущен
            if (this.planBalance <= 0) {
                final Set<EnrSelectionItem> set = new HashSet<>(this.stepItemsAvailable);
                set.removeAll(this.stepItemsRecommendedByThis);

                // убираем всех, кто рекомендован на лучший приоритет в другие конкурсы
                for (final Iterator<EnrSelectionItem> it = set.iterator(); it.hasNext(); ) {
                    final EnrSelectionItem item = it.next();
                    boolean recommendedOther = false;
                    final Collection<EnrSelectionItem> selectionItemsOtherGroup = getStepItemWrappers4OtherGroups(item.getEntrant(), selectionGroup);
                    for (final EnrSelectionItem otherItem: selectionItemsOtherGroup) {
                        if (!Objects.equal(item.getRequestedCompetition().isParallel(), otherItem.getRequestedCompetition().isParallel())) { continue; }
                        if (!otherItem.getStepItem().isRecommended()) { continue; }
                        if (otherItem.getRequestedCompetition().getPriority() < item.getRequestedCompetition().getPriority()) {
                            recommendedOther = true;
                            break;
                        }
                    }
                    if (recommendedOther) {
                        it.remove();
                    }
                }

                // убираем всех, у которых нет рекоммендованных после него
                {
                    for (final Iterator<EnrSelectionItem> it = set.iterator(); it.hasNext(); ) {
                        final EnrSelectionItem w = it.next();

                        final Iterator<EnrSelectionItem> tmp = allItems.iterator();
                        while (tmp.hasNext()) {
                            final EnrSelectionItem tmpItem = tmp.next();
                            if (w.equals(tmpItem)) { break; }
                        }

                        boolean error = false;
                        while (tmp.hasNext()) {
                            final EnrSelectionItem tmpItem = tmp.next();
                            if (tmpItem.getStepItem().isRecommended()) {
                                error = true;
                                break;
                            }
                        }

                        if (!error) {
                            it.remove();
                        }
                    }
                }

                // остальных показываем
                for (final EnrSelectionItem w: set) {
                    this.errors.add("Пропущен: " + w.getEntrantFio());
                }
            }
        }
    }

    private List<GroupCheckWrapper> buildCheckWrapperList()
    {
        final List<GroupCheckWrapper> stepItemGroupList = new ArrayList<>(Collections2.transform(
            this.getStepItemGroupMap().values(),
            GroupCheckWrapper::new
        ));

        final Comparator<GroupCheckWrapper> comparator = Ordering.compound(Arrays.asList(
            Ordering.natural().onResultOf(new FastBeanFunction<GroupCheckWrapper, String>("selectionGroup.key.competition.programSetOrgUnit.programSet.title")),
            Ordering.natural().onResultOf(new FastBeanFunction<GroupCheckWrapper, String>("selectionGroup.key.competition.programSetOrgUnit.orgUnit.title")),
            Ordering.natural().onResultOf(new FastBeanFunction<GroupCheckWrapper, String>("selectionGroup.key.competition.type.code")),
            Ordering.natural().onResultOf(new FastBeanFunction<GroupCheckWrapper, String>("selectionGroup.key.competition.eduLevelRequirement.code")),
            Ordering.natural().onResultOf(new FastBeanFunction<GroupCheckWrapper, String>("selectionGroup.key.competition.title")),
            Ordering.natural().onResultOf(new FastBeanFunction<GroupCheckWrapper, Long>("selectionGroup.key.competition.id")),
            Ordering.natural().nullsLast().onResultOf(new FastBeanFunction<GroupCheckWrapper, String>("selectionGroup.key.targetAdmissionKind.title"))
        ));

        Collections.sort(stepItemGroupList, comparator);
        return stepItemGroupList;
    }

    public EnrXmlEnrollmentStepState buildEnrXmlEnrollmentStepState()
    {
        final List<GroupCheckWrapper> stepItemGroupList = buildCheckWrapperList();
        final EnrXmlEnrollmentStepState root = new EnrXmlEnrollmentStepState();

        Map<EnrCompetition, List<GroupCheckWrapper>> comp2grpupMap = new LinkedHashMap<EnrCompetition, List<GroupCheckWrapper>>();
        for (GroupCheckWrapper g: stepItemGroupList) {
            SafeMap.safeGet(comp2grpupMap, g.getSelectionGroup().getKey().getCompetition(), ArrayList.class).add(g);
        }
        for (Map.Entry<EnrCompetition, List<GroupCheckWrapper>> e: comp2grpupMap.entrySet()) {
            EnrXmlEnrollmentStepState.Competition xmlc = new EnrXmlEnrollmentStepState.Competition(e.getKey());
            for (GroupCheckWrapper g: e.getValue()) {
                EnrXmlEnrollmentStepState.Group xmlg = new EnrXmlEnrollmentStepState.Group(g.getSelectionGroup());
                xmlc.groupList.add(xmlg);

                EnrXmlEnrollmentStepState.GroupRecCheckStatus xmlrc = new EnrXmlEnrollmentStepState.GroupRecCheckStatus();
                xmlrc.errorList = new ArrayList<>(g.errors);
                xmlrc.stepItemAvailable = g.stepItemsAvailable.size();
                xmlrc.planBalance = g.planBalance;
                xmlrc.entrantBalance = g.entrantBalance;
                xmlrc.extractRecByOther = g.extractsRecommendedByOther.size();
                xmlrc.stepItemRecByOther = g.stepItemsRecommendedByOther.size();
                xmlrc.stepItemRecByThis = g.stepItemsRecommendedByThis.size();
                xmlrc.stepItemRecByThisOrOther = g.stepItemsRecommendedByThisOrOther.size();

                xmlg.checkRec = xmlrc;

                for (IEnrSelectionPrevEnrollmentFact extract: g.getSelectionGroup().getCompetitionExtractMap().values()) {
                    EnrXmlEnrollmentStepState.ExtractInfo xmle = new EnrXmlEnrollmentStepState.ExtractInfo(extract);
                    xmlg.enrolledList.add(xmle);
                }

                for (EnrSelectionItem item: g.getSelectionGroup().getItemMap().values()) {
                    EnrXmlEnrollmentStepState.StepItem xmli = new EnrXmlEnrollmentStepState.StepItem(item);
                    xmlg.itemList.add(xmli);

                    for (IEnrSelectionItemPrevEnrollmentInfo reenrollFrom: item.getReenrollFromList()) {
                        EnrSelectionGroup otherG = getStepItemGroup(reenrollFrom); // nullable
                        EnrXmlEnrollmentStepState.ExtractInfo xmle = new EnrXmlEnrollmentStepState.ExtractInfo(otherG, reenrollFrom);
                        xmli.reenrollFromList.add(xmle);
                    }

                    for (IEnrSelectionItemPrevEnrollmentInfo addEnroll: item.getAddEnrollList()) {
                        EnrSelectionGroup otherG = getStepItemGroup(addEnroll); // nullable
                        EnrXmlEnrollmentStepState.ExtractInfo xmle = new EnrXmlEnrollmentStepState.ExtractInfo(otherG, addEnroll);
                        xmli.addEnrollList.add(xmle);
                    }
                }
            }
            root.competitionList.add(xmlc);
        }

        return root;
    }


}
