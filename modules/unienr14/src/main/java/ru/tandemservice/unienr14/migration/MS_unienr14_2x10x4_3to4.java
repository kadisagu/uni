package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MergeBuilder;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

import java.sql.SQLException;

import static org.tandemframework.shared.commonbase.utils.MergeBuilder.stopRule;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unienr14_2x10x4_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.10.4")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrOlympiadDiploma

        // удалено свойство olimpiadProfile
        if (tool.columnExists("enr14_olymp_diploma_t", "olimpiadprofile_id"))
        {
            // удалить колонку
            tool.dropColumn("enr14_olymp_diploma_t", "olimpiadprofile_id");

        }
        // переименовано свойство issuancePlace
        {
            // переименовать колонку
            tool.renameColumn("enr14_olymp_diploma_t", "issuanceplace_p", "issuedby_p");

        }


        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrOlympiadProfile

        // сущность была удалена
        if (tool.tableExists("enr14_c_olimp_profile_t"))
        {
            // удалить таблицу
            tool.dropTable("enr14_c_olimp_profile_t", true /* - не удалять, если есть ссылающиеся таблицы */);

            // удалить код сущности
            tool.entityCodes().delete("enrOlympiadProfile");

        }



        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrOlympiadSubject

        // создано свойство userCode
        if (!tool.columnExists("enr14_c_olymp_subj_t", "usercode_p"))
        {
            // создать колонку
            tool.createColumn("enr14_c_olymp_subj_t", new DBColumn("usercode_p", DBType.createVarchar(255)));

        }

        // создано свойство disabledDate
        if (!tool.columnExists("enr14_c_olymp_subj_t", "disableddate_p"))
        {
            // создать колонку
            tool.createColumn("enr14_c_olymp_subj_t", new DBColumn("disableddate_p", DBType.TIMESTAMP));

        }

        //  свойство title стало обязательным
        {
            tool.executeUpdate("update enr14_c_olymp_subj_t set title_p=code_p where title_p is null");

            // сделать колонку NOT NULL
            tool.setColumnNullable("enr14_c_olymp_subj_t", "title_p", false);

        }

        // Добавляем новые элементы
        insertNewOlympiadSubject(tool);

    }


    private void insertNewOlympiadSubject(DBTool tool) throws SQLException
    {

        String[] values = {"Нанотехнологии", "Естесственные науки", "Инженерные науки", "Информатика", "Гуманитарные и социальные науки", "Востоковедение", "Дизайн","Журналистика",
                "История мировых цивилизаций", "Психология", "Электроника", "Графика", "Композиция", "Рисунок", "Техника и технологии", "Геология", "Филология","Основы православной культуры",
                "Академический рисунок, живопись, композиция, история искусства и культуры", "Технический рисунок и декоративная композиция", "Механика и математическое моделирование",
                "Международные отношения и глобалистика", "Политология", "Философия", "История российской государственности", "Робототехника", "Архитектурная графика", "Инженерное дело",
                "Медицина", "Проба пера", "Социология", "Комплекс предметов (рисунок, композиция)", "Рисунок, архитектурная композиция, живопись, дизайн", "Рисунок, живопись, скульптура",
                "Астрономия и науки о Земле", "Рисунок, композиция, живопись, черчение", "Техника и технология", "Экономика и управление", "Теория и история музыки", "Струнные инструменты",
                "Хоровое дирижирование", "Физика и астрономия", "Биология и экология", "Государственный аудит", "Рисунок, живопись, композиция, история искусства и культуры",
                "Рисунок и композиция", "Механика", "Рисунок, архитектурная графика", "Иностранные языки", "Инструменты народного оркестра", "Астрономия и физика космоса",
                "Математика, физика, астрономия и науки о земле, лингвистика, история, литература, биология, химия", "Химия, физика, математика, биология", "Фортепиано",
                "Народные инструменты", "Право (обществознание)", "Иностранные языки (английский, испанский, китайский, немецкий, французский)", "Экономика, обществознание",
                "Лингвистика (русский язык, иностранные языки)", "Иностранный язык (английский)", "Медицина (химия, биология)", "Техника и технология (физика)",
                "Иностранные языки (английский, испанский, немецкий, французский)", "Биохимия и биотехнология", "Математика, физика, информатика и ИКТ, химия", "Химия, биология",
                "Живопись, рисунок, композиция", "Металлургия и материаловедение", "Рисунок, живопись, композиция, дизайн", "Черчение (архитектурная графика)", "Композиция (рисунок)",
                "Техника и технологии кораблестроения и водного транспорта", "Машиностроение", "Авиационная и ракетно-космическая техника", "Ядерная энергетика и технологии",
                "Технологии материалов", "Духовые инструменты"};

        String code = "2016_";
        tool.table("enr14_c_olymp_subj_t").uniqueKeys().clear();

        //Добавляем новые
        short entityCode = tool.entityCodes().ensure("enrOlympiadSubject");
        int number = 1;

        final MigrationUtils.BatchInsertBuilder insertBuilder = new MigrationUtils.BatchInsertBuilder(entityCode, "code_p", "title_p", "usercode_p", "disableddate_p");

        for (String item : values)
        {
            insertBuilder.addRow(code + number++, item, null, null);
        }
        insertBuilder.executeInsert(tool, "enr14_c_olymp_subj_t");

        MigrationUtils.mergeBuilder(tool, new MergeBuilder.MergeRuleBuilder("enr14_c_olymp_subj_t")
                .key("title_p")
                .order("id", OrderDirection.desc)
                .build())
                .addRule(stopRule("enr14fis_conv_olym_subj_t", "olympiadsubject_id"))
                .addRule(stopRule("enr14_c_olymp_subj_t", "code_p"))
                .merge("enrOlympiadSubject");


    }
}