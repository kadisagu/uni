/**
 *$Id$
 */
package ru.tandemservice.unienr14.debug.bo.EnrDebug.ui.EduRandomAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.event.IEventServiceLock;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unienr14.debug.bo.EnrDebug.EnrDebugManager;

/**
 * @author Alexander Zhebko
 * @since 17.03.2014
 */
public class EnrDebugEduRandomAddUI extends UIPresenter
{
    private EducationYear _eduYear;
    public EducationYear getEduYear(){ return _eduYear; }
    public void setEduYear(EducationYear eduYear){ _eduYear = eduYear; }

    public void onClickApply()
    {
        final IEventServiceLock eventLock = CoreServices.eventService().lock();
        try { EnrDebugManager.instance().dao().doFullEduRandom(getEduYear()); }
        finally { eventLock.release(); }

        deactivate();
    }
}