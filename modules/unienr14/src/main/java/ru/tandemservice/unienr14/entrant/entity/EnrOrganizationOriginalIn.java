package ru.tandemservice.unienr14.entrant.entity;

import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import ru.tandemservice.unienr14.entrant.entity.gen.EnrOrganizationOriginalInGen;

/** @see ru.tandemservice.unienr14.entrant.entity.gen.EnrOrganizationOriginalInGen */
public class EnrOrganizationOriginalIn extends EnrOrganizationOriginalInGen
{
    public EnrOrganizationOriginalIn()
    {
    }

    public EnrOrganizationOriginalIn(EnrEntrant entrant, PersonEduDocument eduDocument, String title)
    {
        setEntrant(entrant);
        setEduDocument(eduDocument);
        setTitle(title);
    }
}