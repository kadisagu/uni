/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.ui.StatsTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilter;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilterType;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterFormConfig;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.logic.EnrModelCompetitionDSHandler;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.ui.CompetitionPub.EnrEnrollmentModelCompetitionPubUI;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentModel;
import ru.tandemservice.unienr14.enrollment.entity.EnrModelStep;

import java.util.Map;

/**
 * @author oleyba
 * @since 3/27/15
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "model.id")
})
public class EnrEnrollmentModelStatsTabUI extends UIPresenter
{
    private EnrEnrollmentModel model = new EnrEnrollmentModel();
    private String selectedTab;

    @Override
    public void onComponentRefresh()
    {
        setModel(IUniBaseDao.instance.get().getNotNull(EnrEnrollmentModel.class, getModel().getId()));
        CommonFilterAddon util = getFilterUtil();
        if (util != null)
        {
            util
            .configDoubleWidthFilters(false)
            .configUseEnableCheckbox(false)
            .configSettings(getSettingsKey());

            util.clearFilterItems();
            util
            .addFilterItem(EnrCompetitionFilterAddon.COMPENSATION_TYPE, CommonFilterFormConfig.DEFAULT_SELECT_FILTER_CONFIG)
            .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_FORM, CommonFilterFormConfig.DEFAULT_SELECT_FILTER_CONFIG)
            .addFilterItem(EnrCompetitionFilterAddon.COMPETITION_TYPE, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
            .addFilterItem(EnrCompetitionFilterAddon.ENR_ORG_UNIT, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
            .addFilterItem(EnrCompetitionFilterAddon.FORMATIVE_ORG_UNIT, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
            .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SUBJECT, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
            .addFilterItem(EnrCompetitionFilterAddon.EDU_PROGRAM, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
            .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SET, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG);

            configWhereFilters();
        }
    }

    private CommonFilterAddon getFilterUtil(){return (CommonFilterAddon) this.getConfig().getAddon(CommonFilterAddon.class.getSimpleName());}

    private void configWhereFilters()
    {
        CommonFilterAddon util = getFilterUtil();
        if (util != null)
        {
            util
            .clearWhereFilter()
            .configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EnrCompetition.programSetOrgUnit().programSet().enrollmentCampaign(), getModel().getEnrollmentCampaign()))
            .configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EnrCompetition.requestType(), getModel().getRequestType()));
        }
    }

    public void onClickSearch()
    {
        CommonFilterAddon util = getFilterUtil();
        if (util != null)
            util.saveSettings();

        saveSettings();
    }

    public void onClickClear()
    {
        CommonFilterAddon util = getFilterUtil();
        if (util != null)
            util.clearSettings();

        clearSettings();
        configWhereFilters();
        onClickSearch();
    }

    // presenter

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrModelCompetitionDSHandler.BIND_ENR_MODEL, getModel());
        dataSource.put(EnrModelCompetitionDSHandler.BIND_ENR_COMPETITION_UTIL, getConfig().getAddon(CommonFilterAddon.class.getSimpleName()));
    }

    // getters and setters

    public EnrEnrollmentModel getModel()
    {
        return model;
    }

    public void setModel(EnrEnrollmentModel model)
    {
        this.model = model;
    }

    public String getSelectedTab()
    {
        return this.selectedTab;
    }

    public void setSelectedTab(String selectedTab)
    {
        this.selectedTab = selectedTab;
    }
}