/* $Id:$ */
package ru.tandemservice.unienr14.settings.entity;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;

/**
 * @author oleyba
 * @since 1/30/14
 */
public interface IEnrExamSetElementValue extends IEntity, ITitled
{
    public static final String P_DEFAULT_PASS_MARK_AS_LONG = "defaultPassMarkAsLong";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_TITLE = "title";

    /**
     * @return Зачетный балл по умолчанию (три знака для дробной части).
     */
    public long getDefaultPassMarkAsLong();

    public String getShortTitle();
}
