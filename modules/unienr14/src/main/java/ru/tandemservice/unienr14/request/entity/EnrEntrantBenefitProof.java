package ru.tandemservice.unienr14.request.entity;

import org.tandemframework.core.common.IEntityDebugTitled;
import ru.tandemservice.unienr14.request.entity.gen.EnrEntrantBenefitProofGen;

/**
 * Документ абитуриента, подтверждающий особое право (2013)
 */
public class EnrEntrantBenefitProof extends EnrEntrantBenefitProofGen implements IEntityDebugTitled
{
    public EnrEntrantBenefitProof()
    {
    }

    public EnrEntrantBenefitProof(IEnrEntrantBenefitStatement statement, IEnrEntrantBenefitProofDocument document)
    {
        setBenefitStatement(statement);
        setDocument(document);
    }

    @Override
    public String getEntityDebugTitle()
    {
        return getBenefitStatement().getBenefitStatementTitle() + " - " + getDocument().getDisplayableTitle();
    }
}