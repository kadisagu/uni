/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantRefusalAdd.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantRefusalAdd.EnrReportEntrantRefusalAddUI;

/**
 * @author rsizonenko
 * @since 28.05.2014
 */
public interface IEnrReportEntrantRefusalDao extends INeedPersistenceSupport
{
    Long createReport(EnrReportEntrantRefusalAddUI enrReportEntrantRefusalAddUI);
}
