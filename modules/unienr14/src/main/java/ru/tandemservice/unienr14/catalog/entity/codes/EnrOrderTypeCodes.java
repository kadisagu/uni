package ru.tandemservice.unienr14.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Тип приказа по абитуриентам"
 * Имя сущности : enrOrderType
 * Файл data.xml : unienr.order.data.xml
 */
public interface EnrOrderTypeCodes
{
    /** Константа кода (code) элемента : Приказ о зачислении (title) */
    String ENROLLMENT = "1";
    /** Константа кода (code) элемента : Приказ об отмене приказа по абитуриентам (title) */
    String CANCEL = "2";
    /** Константа кода (code) элемента : Приказ о распределении по образовательным программам (title) */
    String ALLOCATION = "3";
    /** Константа кода (code) элемента : Приказ о зачислении по направлению от Минобрнауки (title) */
    String ENROLLMENT_MIN = "4";

    Set<String> CODES = ImmutableSet.of(ENROLLMENT, CANCEL, ALLOCATION, ENROLLMENT_MIN);
}
