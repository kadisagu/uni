package ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.logic.EnrProgramAllocationDSHandler;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;

/**
 * @author Vasily Zhukov
 * @since 10.05.2012
 */
@Configuration
public class EnrProgramAllocationList extends BusinessComponentManager
{
    public static final String ALLOCATION_DS = "allocationDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
            .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
            .addDataSource(searchListDS(ALLOCATION_DS, allocationDS(), allocationDSHandler()))
            .create();
    }

    @Bean
    public ColumnListExtPoint allocationDS()
    {
        return columnListExtPointBuilder(ALLOCATION_DS)
            .addColumn(checkboxColumn("checkbox").create())
            .addColumn(publisherColumn("programSet", EnrProgramSetOrgUnit.programSet().title().s())
                .primaryKeyPath(EnrProgramAllocationDSHandler.VIEW_PROP_ALLOCATION_ID)
                .required(true).create())
            .addColumn(textColumn("eduProgramForm", EnrProgramSetOrgUnit.programSet().programForm().title().s()).create())
            .addColumn(textColumn("filial", EnrProgramSetOrgUnit.orgUnit().departmentTitle().s()).required(true).create())
            .addColumn(dateColumn("formingDate", EnrProgramAllocationDSHandler.VIEW_PROP_FORMING_DATE).width("5").create())
            .addColumn(blockColumn("quota", "quotaBlockColumn").width("10").create())
            .addColumn(indicatorColumn("add")
                .addIndicator(Boolean.FALSE, new IndicatorColumn.Item(CommonDefines.ICON_ADD.getName(), CommonDefines.ICON_ADD.getLabel(), "onClickAdd"))
                .path(EnrProgramAllocationDSHandler.VIEW_PROP_ADD_DISABLED)
                .clickable(true)
                .required(true)
                .permissionKey("enr14ProgramAllocationListAdd")
                .create())
            .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER)
                .alert(FormattedMessage.with().template("allocationDS.delete.alert").parameter(EnrProgramSetOrgUnit.programSet().title().s()).parameter(EnrProgramSetOrgUnit.orgUnit().departmentTitle().s()).create())
                .disabled(EnrProgramAllocationDSHandler.VIEW_PROP_DELETE_DISABLED)
                .permissionKey("enr14ProgramAllocationListDelete")
                .create())
            .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> allocationDSHandler()
    {
        return new EnrProgramAllocationDSHandler(getName());
    }
}
