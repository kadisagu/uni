/**
 *$Id: EnrRequestDirectionBlockListenerBean.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.request.bo.EnrRequestedCompetition.event;

import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * Листенер, который запрещает изменять данные ВНП и связанные объекты (Особое право и Подтверждающие его документы) при наличии студента предзачисления, ссылающегося на данное ВНП.
 *
 * @author Alexander Shaburov
 * @since 23.08.13
 */
public class EnrRequestedCompetitionBlockListenerBean
{
    public static class Listener implements IDSetEventListener
    {
        private final String _path;
        private final Collection<String> immutableFields = Arrays.asList(
            EnrRequestedCompetition.L_COMPETITION,
            EnrRequestedCompetition.L_REQUEST
            // todo
        );

        private Listener(String reqDirPath)
        {
            _path = reqDirPath;
        }

        @Override
        public void onEvent(DSetEvent event)
        {
            final IEntityMeta entityMeta = event.getMultitude().getEntityMeta();
            
            if (entityMeta.getClassName().equals(EnrRequestedCompetition.ENTITY_CLASS)) {
                final Set<String> affectedProperties = event.getMultitude().getAffectedProperties();
                final Collection<String> properties = CollectionUtils.intersection(affectedProperties, this.immutableFields);
                if (properties.isEmpty()) { return; }
                
            }

//            final String propertyPath = _path == null ? "x.id" : "x." + _path + ".id";

//            final DQLSelectBuilder xDQL = new DQLSelectBuilder().fromEntity(entityMeta.getEntityClass(), "x").column(property(propertyPath))
//                    .where(in(property("x.id"), event.getMultitude().getInExpression()));

            // todo
            // находим Студента пред. зачисленя на ВНП данные которого обновляются
//            final long count = new DQLSelectBuilder().fromEntity(EnrPreliminaryEnrollmentStudent.class, "ps").column(property("ps.id"))
//                    .where(in(property(EnrPreliminaryEnrollmentStudent.requestedDirection().id().fromAlias("ps")), xDQL.buildQuery()))
//                    .createCountStatement(event.getContext())
//                    .uniqueResult();
//
//            if (count > 0)
//                throw new ApplicationException(EnrRequestedCompetitionManager.instance().getProperty("enrRequestedCompetition.update.exception"), true);
        }
    }


    /**
     * Подготавливаем листенер.
     * Определяем для каких объектов он будет работать.
     */
    @SuppressWarnings("unchecked")
    public void init()
    {
        // объект и путь до соответствующего ВНП
        Object rows[][] = {
                {EnrRequestedCompetition.class, null }
                // todo
        };

        for (Object[] row: rows)
        {
            final Class<? extends IEntity> entityClass = (Class<? extends IEntity>) row[0];
            final String requestPath = (String) row[1];

            final Listener listener = new Listener(requestPath);
            DSetEventManager.getInstance().registerListener(DSetEventType.beforeUpdate, entityClass, listener);
            DSetEventManager.getInstance().registerListener(DSetEventType.beforeDelete, entityClass, listener);
        }

        // todo
        // DSetEventManager.getInstance().registerListener(DSetEventType.beforeInsert, EnrEntrantBenefit.class, new Listener(EnrEntrantBenefit.requestedDirection().s()));
    }
}
