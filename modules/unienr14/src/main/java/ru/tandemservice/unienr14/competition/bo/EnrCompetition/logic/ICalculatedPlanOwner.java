/* $Id:$ */
package ru.tandemservice.unienr14.competition.bo.EnrCompetition.logic;

/**
 * @author oleyba
 * @since 2/2/15
 */
public interface ICalculatedPlanOwner
{
    int getPlan();
    void setPlan(int plan);

    int getCalculatedPlan();
    void setCalculatedPlan(int plan);
}
