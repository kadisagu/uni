/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsForeignLangsAdd.logic;


import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.RtfRowIntercepterRawText;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.RtfElement;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.PersonForeignLanguage;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsForeignLangsAdd.EnrReportEntrantsForeignLangsAddUI;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportUtil;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.FilterParametersPrinter;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder;
import ru.tandemservice.unienr14.report.entity.EnrReportEntrantsForeignLangs;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.text.SimpleDateFormat;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 30.05.2014
 */
public class EnrReportEntrantsForeignLangsDao extends UniBaseDao implements IEnrReportEntrantsForeignLangsDao
{
    @Override
    public Long createReport(EnrReportEntrantsForeignLangsAddUI model) {

        EnrReportEntrantsForeignLangs report = new EnrReportEntrantsForeignLangs();

        report.setFormingDate(new Date());

        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateSelector().getDateFrom());
        report.setDateTo(model.getDateSelector().getDateTo());
        report.setEnrollmentStage(model.getStage().getTitle());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, EnrReportEntrantsForeignLangs.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE, EnrReportEntrantsForeignLangs.P_COMPENSATION_TYPE, "shortTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, EnrReportEntrantsForeignLangs.P_PROGRAM_FORM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPETITION_TYPE, EnrReportEntrantsForeignLangs.P_COMPETITION_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, EnrReportEntrantsForeignLangs.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, EnrReportEntrantsForeignLangs.P_FORMATIVE_ORG_UNIT, "fullTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, EnrReportEntrantsForeignLangs.P_PROGRAM_SUBJECT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, EnrReportEntrantsForeignLangs.P_EDU_PROGRAM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, EnrReportEntrantsForeignLangs.P_PROGRAM_SET, "title");
        if (model.isParallelActive())
            report.setParallel(model.getParallel().getTitle());
        report.setSplitted(model.isSplitByEduProgramSubject() ? "Да" : "Нет");
        DatabaseFile content = new DatabaseFile();

        content.setContent(buildReport(model));
        content.setFilename("EnrReportEntrantsForeignLangs.rtf");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        save(content);

        report.setContent(content);
        save(report);

        return report.getId();

    }

    public byte[] buildReport(EnrReportEntrantsForeignLangsAddUI model)
    {

        // rtf
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(EnrScriptItem.class, EnrScriptItemCodes.REPORT_ENTRANTS_FOREIGN_LANGS);
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());

        // Дата

        new RtfInjectModifier().put("dateForm", new SimpleDateFormat("dd.MM.yyyy").format(new Date())).modify(document);

        // Фильтры
        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder()
                .notArchiveOnly()
                .filter(model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo())
                .filter(model.getEnrollmentCampaign());
        filterAddon.applyFilters(requestedCompDQL, requestedCompDQL.competition());

        if (model.isParallelActive())
        {
            if (model.isParallelOnly())
                requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.TRUE)));
            if (model.isSkipParallel())
                requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.FALSE)));
        }

        model.getStageSelector().applyFilter(requestedCompDQL, "reqComp");

        // Присоединяем в запросе языки
        requestedCompDQL
                .joinEntity("entrant", DQLJoinType.inner, PersonForeignLanguage.class, "lang", eq(property("entrant", EnrEntrant.person().id()), property("lang", PersonForeignLanguage.person().id())))
                .column(property("reqComp"))
                .column(property("lang"));

        // Компараторы для сортировки таблиц

        Comparator<EnrOrgUnit> enrOrgUnitComparator = new Comparator<EnrOrgUnit>() {
            @Override
            public int compare(EnrOrgUnit o1, EnrOrgUnit o2) {
                int i = Boolean.compare(null == o1.getInstitutionOrgUnit().getOrgUnit().getParent(), null == o2.getInstitutionOrgUnit().getOrgUnit().getParent());
                if (0 != i) { return -i; }

                return o1.getInstitutionOrgUnit().getOrgUnit().getPrintTitle().compareToIgnoreCase(o2.getInstitutionOrgUnit().getOrgUnit().getPrintTitle());
            }
        };

        Comparator<OrgUnit> formativeOUComparator = new Comparator<OrgUnit>() {
            @Override
            public int compare(OrgUnit o1, OrgUnit o2) {
                return o1.getPrintTitle().compareTo(o2.getPrintTitle());
            }
        };

        Comparator<EnrProgramSetBase> programSetComparator = new Comparator<EnrProgramSetBase>() {
            @Override
            public int compare(EnrProgramSetBase o1, EnrProgramSetBase o2) {
                return o1.getProgramSubject().getCode().compareTo(o2.getProgramSubject().getCode());
            }
        };

        Comparator<PersonForeignLanguage> langComparator = new Comparator<PersonForeignLanguage>() {
            @Override
            public int compare(PersonForeignLanguage o1, PersonForeignLanguage o2) {
                int result;
                if (o1.getId().equals(o2.getId())) return 0;
                result = o1.getLanguage().getTitle().compareTo(o2.getLanguage().getTitle());
                if (result != 0) return result;
                result = Boolean.compare(o1.isMain(), o2.isMain());
                if (result != 0) return -result;
                if (o1.getSkill()== o2.getSkill())
                    return CommonCollator.RUSSIAN_COLLATOR.compare(o1.getPerson().getFullFio(), o2.getPerson().getFullFio());
                if (o1.getSkill() == null)
                    return -1;
                if (o2.getSkill() == null)
                    return 1;
                result = o1.getSkill().getCode().compareTo(o2.getSkill().getCode());
                if (result != 0) return result;
                return CommonCollator.RUSSIAN_COLLATOR.compare(o1.getPerson().getFullFio(), o2.getPerson().getFullFio());
            }
        };

        // Получение данных для таблиц
        Map<EnrOrgUnit, Map<OrgUnit, Map<EnrProgramSetBase, Set<PersonForeignLanguage>>>> dataMap = new TreeMap<>(enrOrgUnitComparator);

        List<Object[]> columns = getList(requestedCompDQL);

        for (Object[] line : columns)
        {
            EnrRequestedCompetition reqComp = (EnrRequestedCompetition) line[0];
            PersonForeignLanguage lang = (PersonForeignLanguage) line[1];
            OrgUnit formativeOU = reqComp.getCompetition().getProgramSetOrgUnit().getEffectiveFormativeOrgUnit();
            EnrOrgUnit enrOrgUnit = reqComp.getCompetition().getProgramSetOrgUnit().getOrgUnit();
            EnrProgramSetBase programSet = reqComp.getCompetition().getProgramSetOrgUnit().getProgramSet();

            final Map<OrgUnit, Map<EnrProgramSetBase, Set<PersonForeignLanguage>>> orgUnitMap = SafeMap.safeGet(dataMap, enrOrgUnit, TreeMap.class, formativeOUComparator);
            final Map<EnrProgramSetBase, Set<PersonForeignLanguage>> programSetMap = SafeMap.safeGet(orgUnitMap, formativeOU, TreeMap.class, programSetComparator);
            final Set<PersonForeignLanguage> languageSet = SafeMap.safeGet(programSetMap, programSet, TreeSet.class, langComparator);
            languageSet.add(lang);
        }

        // Разбираем документ на элементы, которые будем менять
        RtfElement filialComment = (RtfElement) UniRtfUtil.findElement(document.getElementList(), "filial");
        RtfElement formativeComment = (RtfElement) UniRtfUtil.findElement(document.getElementList(), "formativeOrgUnit");
        RtfElement programSubComment = (RtfElement) UniRtfUtil.findElement(document.getElementList(), "programSubject");
        RtfTable tTableForm = (RtfTable) UniRtfUtil.findElement(document.getElementList(), "T");

        // Чистим документ от тех элементов, которые в будущем будем вставлять в него
        document.getElementList().remove(filialComment);
        document.getElementList().remove(formativeComment);
        document.getElementList().remove(programSubComment);
        document.getElementList().remove(tTableForm);

        boolean notFirstIteration = false;

        // Цикл заполнения документа Т-таблицами
        for (Map.Entry<EnrOrgUnit, Map<OrgUnit, Map<EnrProgramSetBase, Set<PersonForeignLanguage>>>> orgUnitEntry : dataMap.entrySet())
        {
            if (notFirstIteration)
            {
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));

                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));

                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
            }
            document.getElementList().add(filialComment.getClone());
            new RtfInjectModifier().put("filial", orgUnitEntry.getKey().getInstitutionOrgUnit().getOrgUnit().getPrintTitle()).modify(document);

            for (Map.Entry<OrgUnit, Map<EnrProgramSetBase, Set<PersonForeignLanguage>>> formativeEntry : orgUnitEntry.getValue().entrySet())
            {
                document.getElementList().add(formativeComment.getClone());
                new RtfInjectModifier().put("formativeOrgUnit", formativeEntry.getKey().getPrintTitle()).modify(document);


                // Если не надо делить по направлениям, то объединяем всё в один сет
                if (!model.isSplitByEduProgramSubject()) {
                    List<String[]> tTable = new ArrayList<>();
                    Set<PersonForeignLanguage> langFullSet = new TreeSet<>(langComparator);
                    for (Map.Entry<EnrProgramSetBase, Set<PersonForeignLanguage>> programSetEntry : formativeEntry.getValue().entrySet())
                    {
                        langFullSet.addAll(programSetEntry.getValue());
                    }
                    int numb = 1;
                    for (PersonForeignLanguage lang : langFullSet)
                    {
                        String[] row = new String[]{
                                String.valueOf(numb++),
                                lang.getPerson().getFullFio(),
                                lang.getLanguage().getTitle(),
                                lang.getSkill() == null ? "—" : lang.getSkill().getTitle(),
                                lang.isMain() ? "Да" : "Нет"
                        };
                        tTable.add(row);
                    }
                    document.getElementList().add(tTableForm.getClone());
                    RtfTableModifier modifier = new RtfTableModifier();
                    modifier.put("T", tTable.toArray(new String[tTable.size()][]));
                    modifier.put("T", new RtfRowIntercepterRawText());
                    modifier.modify(document);
                }

                // иначе выводим каждый по отдельности
                else {

                    for (Map.Entry<EnrProgramSetBase, Set<PersonForeignLanguage>> programSetEntry : formativeEntry.getValue().entrySet()) {

                        document.getElementList().add(programSubComment.getClone());
                        new RtfInjectModifier().put("programSubject", programSetEntry.getKey().getProgramSubject().getTitleWithCode()).modify(document);

                        List<String[]> tTable = new ArrayList<>();
                        int numb = 1;
                        for (PersonForeignLanguage lang : programSetEntry.getValue()) {
                            String[] row = new String[]{
                                    String.valueOf(numb++),
                                    lang.getPerson().getFullFio(),
                                    lang.getLanguage().getTitle(),
                                    lang.getSkill() == null ? "" : lang.getSkill().getTitle(),
                                    lang.isMain() ? "Да" : "Нет"
                            };
                            tTable.add(row);
                        }

                        document.getElementList().add(tTableForm.getClone());
                        RtfTableModifier modifier = new RtfTableModifier();
                        modifier.put("T", tTable.toArray(new String[tTable.size()][]));
                        modifier.put("T", new RtfRowIntercepterRawText());
                        modifier.modify(document);
                        document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
                        document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
                    }
                }
            }
            notFirstIteration = true;
        }

        // Заполняем Н-таблицу
        List<String[]> hTable = new FilterParametersPrinter().getTable(model.getCompetitionFilterAddon(), model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo());
        if (model.isParallelActive())
            hTable.add(new String[]{"Поступающие параллельно", model.getParallel().getTitle()});

        hTable.add(2, new String[]{"Стадия приемной кампании", model.getStage().getTitle()});
        RtfTableModifier modifier = new RtfTableModifier();
        modifier.put("H", hTable.toArray(new String[hTable.size()][]));
        modifier.put("H", new RtfRowIntercepterRawText());
        modifier.modify(document);

        return RtfUtil.toByteArray(document);
    }
}
