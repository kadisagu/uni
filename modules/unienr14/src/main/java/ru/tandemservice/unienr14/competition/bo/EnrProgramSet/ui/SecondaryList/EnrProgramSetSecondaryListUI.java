/* $Id:$ */
package ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.SecondaryList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseSettingsUtil;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrRequestTypeCodes;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.EnrProgramSetManager;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.AddEdit.EnrProgramSetAddEdit;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.AddEdit.EnrProgramSetAddEditUI;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.List.EnrProgramSetList;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

/**
 * @author oleyba
 * @since 2/18/14
 */
public class EnrProgramSetSecondaryListUI extends UIPresenter
{
    @Override
    public void onComponentRefresh()
    {
        getSettings().set(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
    }

    public void onEnrCampaignRefresh()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, getEnrollmentCampaign());
        dataSource.put(EnrProgramSetSecondaryList.BIND_PROGRAM_FORM, this.getSettings().get(EnrProgramSetSecondaryList.BIND_PROGRAM_FORM));
        dataSource.put(EnrProgramSetSecondaryList.BIND_ORG_UNITS, this.getSettings().get(EnrProgramSetSecondaryList.BIND_ORG_UNITS));
        dataSource.put(EnrProgramSetSecondaryList.BIND_SUBJECT_CODE, this.getSettings().get(EnrProgramSetSecondaryList.BIND_SUBJECT_CODE));
        dataSource.put(EnrProgramSetSecondaryList.BIND_PROGRAM_SUBJECTS, this.getSettings().get(EnrProgramSetSecondaryList.BIND_PROGRAM_SUBJECTS));
        dataSource.put(EnrProgramSetSecondaryList.BIND_TITLE, this.getSettings().get(EnrProgramSetSecondaryList.BIND_TITLE));
    }

    public void onClickSearch()
    {
        this.saveSettings();
    }

    public void onClickClear()
    {
        CommonBaseSettingsUtil.clearAndSaveSettingsExcept(this.getSettings(), EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
    }

    public void onClickAddCompetitionSecondary()
    {
        EnrEnrollmentCampaign ec = getSettings().get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
        EnrRequestType requestType = DataAccessServices.dao().getByCode(EnrRequestType.class, EnrRequestTypeCodes.SPO);

        if (null != ec) ec.checkOpen();
        getActivationBuilder()
                .asRegion(EnrProgramSetAddEdit.class)
                .parameter(EnrProgramSetAddEditUI.SECONDARY, true)
                .parameter(EnrProgramSetAddEditUI.REQUEST_TYPE_BIND, requestType)
                .activate();
    }

    public void onClickEditCompetitionSecondary()
    {
        getActivationBuilder().asRegion(EnrProgramSetAddEdit.class)
            .parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong())
            .parameter(EnrProgramSetAddEditUI.SECONDARY, true)
            .activate();
    }

    public void onClickDeleteCompetitionSecondary()
    {
        EnrProgramSetManager.instance().dao().deleteProgramSet(getListenerParameterAsLong());
    }

    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return getSettings().get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
    }

    public boolean isNothingSelected()
    {
        return getSettings().get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN) == null;
    }
}