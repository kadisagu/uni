package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x6_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.6")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrOrderPrintFormType

		// создано обязательное свойство printPdf
        if(tool.tableExists("enr14_c_order_form_type_t") && !tool.columnExists("enr14_c_order_form_type_t", "printpdf_p"))
		{
			// создать колонку
			tool.createColumn("enr14_c_order_form_type_t", new DBColumn("printpdf_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			java.lang.Boolean defaultPrintPdf = false;
			tool.executeUpdate("update enr14_c_order_form_type_t set printpdf_p=? where printpdf_p is null", defaultPrintPdf);

			// сделать колонку NOT NULL
			tool.setColumnNullable("enr14_c_order_form_type_t", "printpdf_p", false);

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrScriptItem

		// создано обязательное свойство printPdf
        if(tool.tableExists("enr14_c_script_t") && !tool.columnExists("enr14_c_script_t", "printpdf_p"))
        {
			// создать колонку
			tool.createColumn("enr14_c_script_t", new DBColumn("printpdf_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			java.lang.Boolean defaultPrintPdf = false;
			tool.executeUpdate("update enr14_c_script_t set printpdf_p=? where printpdf_p is null", defaultPrintPdf);

			// сделать колонку NOT NULL
			tool.setColumnNullable("enr14_c_script_t", "printpdf_p", false);
		}

        // создано обязательное свойство printableToPdf
        if(tool.tableExists("enr14_c_script_t") && !tool.columnExists("enr14_c_script_t", "printabletopdf_p"))
        {
            // создать колонку
            tool.createColumn("enr14_c_script_t", new DBColumn("printabletopdf_p", DBType.BOOLEAN));

            // задать значение по умолчанию
            java.lang.Boolean defaultPrintableToPdf = false;
            tool.executeUpdate("update enr14_c_script_t set printabletopdf_p=? where printabletopdf_p is null", defaultPrintableToPdf);

            // сделать колонку NOT NULL
            tool.setColumnNullable("enr14_c_script_t", "printabletopdf_p", false);

        }
    }
}