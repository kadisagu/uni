package ru.tandemservice.unienr14.catalog.entity;

import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.catalog.entity.gen.*;

/**
 * Предмет ЕГЭ
 */
public class EnrStateExamSubject extends EnrStateExamSubjectGen
{
    public static EntityComboDataSourceHandler defaultSelectDSHandler(String ownerId)
    {
        return new EntityComboDataSourceHandler(ownerId, EnrStateExamSubject.class)
                .titleProperty(EnrStateExamSubject.P_TITLE)
                .filter(EnrStateExamSubject.title())
                .order(EnrStateExamSubject.title());
    }
}