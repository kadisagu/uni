/* $Id$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.NextOfKinStep;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author nvankov
 * @since 6/5/14
 */
@Configuration
public class EnrEntrantRequestAddWizardNextOfKinStep extends BusinessComponentManager
{
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .create();
    }
}



    