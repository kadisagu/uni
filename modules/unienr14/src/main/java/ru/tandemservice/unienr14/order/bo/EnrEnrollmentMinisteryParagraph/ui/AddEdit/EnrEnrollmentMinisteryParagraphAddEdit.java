/* $Id$ */
package ru.tandemservice.unienr14.order.bo.EnrEnrollmentMinisteryParagraph.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.column.CheckboxDSColumn;
import org.tandemframework.caf.ui.config.datasource.column.TextDSColumn;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.NoWrapFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.formatter.EnrEntrantCustomStateCollectionFormatter;
import ru.tandemservice.unienr14.order.bo.EnrEnrollmentMinisteryParagraph.logic.EnrParagraphEntrantSelectionDSHandler;
import ru.tandemservice.unienr14.request.entity.EnrEntrantForeignRequest;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.logic.EnrOrgUnitBaseDSHandler;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 9/1/14
 */
@Configuration
public class EnrEnrollmentMinisteryParagraphAddEdit extends BusinessComponentManager
{
    public static final String BIND_REQUEST_TYPE = "requestType";
    public static final String BIND_COMPENSATION_TYPE = "compensationType";
    public static final String BIND_FORM = "eduForm";
    public static final String BIND_ORG_UNIT = "orgUnit";
    public static final String BIND_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String BIND_SUBJECT = "programSubject";
    public static final String BIND_PROGRAM_SET = "programSet";
    public static final String BIND_COMPETITION_TYPE = "competitionType";
    public static final String BIND_COMPETITION = "competition";
    public static final String BIND_PARAGRAPH = "paragraph";


    public static final String DS_ENTRANT = "entrantDS";
    public static final String DS_FORM = "formDS";
    public static final String DS_ORG_UNIT = "orgUnitDS";
    public static final String DS_FORMATIVE_ORG_UNIT = "formativeOrgUnitDS";
    public static final String DS_SUBJECT = "subjectDS";
    public static final String DS_COMPETITION_TYPE = "competitionTypeDS";
    public static final String DS_PROGRAM_SET = "programSetDS";
    public static final String DS_COMPETITION = "competitionDS";

    public static final String CHECKBOX_COLUMN = "checkbox";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(searchListDS(DS_ENTRANT, entrantDS(), entrantDSHandler()))
                .addDataSource(selectDS(DS_FORM, eduProgramFormDSHandler()))
                .addDataSource(selectDS(DS_ORG_UNIT, orgUnitDSHandler()).addColumn(EnrOrgUnit.institutionOrgUnit().orgUnit().shortTitleWithTopEmphasized().s()))
                .addDataSource(selectDS(DS_FORMATIVE_ORG_UNIT, formativeOrgUnitDSHandler()).addColumn(OrgUnit.fullTitle().s()))
                .addDataSource(selectDS(DS_SUBJECT, subjectDSHandler()).addColumn(EduProgramSubject.titleWithCode().s()))
                .addDataSource(selectDS(DS_COMPETITION_TYPE, competitionTypeDSHandler()))
                .addDataSource(selectDS(DS_PROGRAM_SET, programSetDSHandler()))
                .addDataSource(selectDS(DS_COMPETITION, competitionDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint entrantDS()
    {
        return ColumnListExtPoint.with(getName(), "entrantDS")
                .addColumn(CheckboxDSColumn.with().name(CHECKBOX_COLUMN).create())
                .addColumn(TextDSColumn.with().name("fio").path(EnrEntrantForeignRequest.entrant().person().identityCard().fullFio()).formatter(NoWrapFormatter.INSTANCE).create())
                .addColumn(TextDSColumn.with().name(EnrParagraphEntrantSelectionDSHandler.CUSTOM_STATES).path(EnrParagraphEntrantSelectionDSHandler.CUSTOM_STATES).formatter(new EnrEntrantCustomStateCollectionFormatter()).create())
                .addColumn(TextDSColumn.with().name("sex").path(EnrEntrantForeignRequest.entrant().person().identityCard().sex().shortTitle()).create())
                .addColumn(TextDSColumn.with().name("passport").path(EnrEntrantForeignRequest.entrant().person().identityCard().fullNumber()).formatter(NoWrapFormatter.INSTANCE).create())
                .addColumn(TextDSColumn.with().name("citizenship").path(EnrEntrantForeignRequest.entrant().person().identityCard().citizenship().title()).formatter(NoWrapFormatter.INSTANCE).create())
                .addColumn(TextDSColumn.with().name("reqNumber").path(EnrEntrantForeignRequest.regNumber()).create())
                .addColumn(TextDSColumn.with().name("competition").path(EnrEntrantForeignRequest.competition().title()).formatter(NoWrapFormatter.INSTANCE).create())
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> entrantDSHandler()
    {
        return new EnrParagraphEntrantSelectionDSHandler(getName());
    }

    /* Выбор конкурсов */

    @Bean
    public IDefaultComboDataSourceHandler competitionTypeDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrCompetitionType.class) {
            @Override protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context) {
                super.applyWhereConditions(alias, dql, context);

                OrgUnit formativeOrgUnit = context.get(BIND_FORMATIVE_ORG_UNIT);

                DQLSelectBuilder compDQL = new DQLSelectBuilder()
                        .fromEntity(EnrEntrantForeignRequest.class, "r").column("r.id")
                        .joinPath(DQLJoinType.inner, EnrEntrantForeignRequest.competition().fromAlias("r"), "comp")
                        .where(eq(property(EnrCompetition.type().fromAlias("comp")), property(alias)))
                        .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().enrollmentCampaign().fromAlias("comp")), commonValue(context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN))))
                        .where(eq(property("comp", EnrCompetition.requestType()), commonValue(context.get(BIND_REQUEST_TYPE))))
                        .where(eq(property("comp", EnrCompetition.type().compensationType()), commonValue(context.get(BIND_COMPENSATION_TYPE))))
                        .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().programForm().fromAlias("comp")), commonValue(context.get(BIND_FORM))))
                        .where(eq(property("comp", EnrCompetition.programSetOrgUnit().orgUnit()), commonValue(context.get(BIND_ORG_UNIT))))
                        .where(eq(property("comp", EnrCompetition.programSetOrgUnit().programSet().programSubject()), commonValue(context.get(BIND_SUBJECT))))
                        ;

                if (formativeOrgUnit != null) {
                    compDQL.where(eq(property("comp", EnrCompetition.programSetOrgUnit().formativeOrgUnit()), value(formativeOrgUnit)));
                }

                dql.where(exists(compDQL.buildQuery()));
            }
        }
                .where(EnrCompetitionType.compensationType(), BIND_COMPENSATION_TYPE)
                .order(EnrCompetitionType.code())
                .filter(EnrCompetitionType.title())
                .pageable(false);
    }

    @Bean
    public IDefaultComboDataSourceHandler eduProgramFormDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramForm.class) {
            @Override protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context) {
                super.applyWhereConditions(alias, dql, context);

                DQLSelectBuilder compDQL = new DQLSelectBuilder()
                        .fromEntity(EnrEntrantForeignRequest.class, "r").column("r.id")
                        .joinPath(DQLJoinType.inner, EnrEntrantForeignRequest.competition().fromAlias("r"), "comp")
                        .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().programForm().fromAlias("comp")), property(alias)))
                        .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().enrollmentCampaign().fromAlias("comp")), commonValue(context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN))))
                        .where(eq(property("comp", EnrCompetition.type().compensationType()), commonValue(context.get(BIND_COMPENSATION_TYPE))))
                        .where(eq(property("comp", EnrCompetition.requestType()), value(context.<EnrRequestType>get(BIND_REQUEST_TYPE))));
                dql.where(exists(compDQL.buildQuery()));
            }
        }
                .order(EduProgramForm.code())
                .filter(EduProgramForm.title())
                .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler orgUnitDSHandler()
    {
        return new EnrOrgUnitBaseDSHandler(getName())
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                DQLSelectBuilder compDQL = new DQLSelectBuilder()
                        .fromEntity(EnrEntrantForeignRequest.class, "r").column("r.id")
                        .joinPath(DQLJoinType.inner, EnrEntrantForeignRequest.competition().fromAlias("r"), "comp")
                        .where(eq(property(EnrCompetition.programSetOrgUnit().orgUnit().fromAlias("comp")), property(alias)))
                        .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().enrollmentCampaign().fromAlias("comp")), commonValue(context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN))))
                        .where(eq(property("comp", EnrCompetition.requestType()), commonValue(context.get(BIND_REQUEST_TYPE))))
                        .where(eq(property("comp", EnrCompetition.type().compensationType()), commonValue(context.get(BIND_COMPENSATION_TYPE))))
                        .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().programForm().fromAlias("comp")), commonValue(context.get(BIND_FORM))));

                dql.where(exists(compDQL.buildQuery()));
            }
        }
                .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler formativeOrgUnitDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), OrgUnit.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                DQLSelectBuilder compDQL = new DQLSelectBuilder()
                        .fromEntity(EnrEntrantForeignRequest.class, "r").column("r.id")
                        .joinPath(DQLJoinType.inner, EnrEntrantForeignRequest.competition().fromAlias("r"), "comp")
                        .where(eq(property(EnrCompetition.programSetOrgUnit().formativeOrgUnit().fromAlias("comp")), property(alias)))
                        .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().enrollmentCampaign().fromAlias("comp")), commonValue(context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN))))
                        .where(eq(property("comp", EnrCompetition.requestType()), value(context.<EnrRequestType>get(BIND_REQUEST_TYPE))))
                        .where(eq(property("comp", EnrCompetition.type().compensationType()), commonValue(context.get(BIND_COMPENSATION_TYPE))))
                        .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().programForm().fromAlias("comp")), commonValue(context.get(BIND_FORM))))
                        .where(eq(property("comp", EnrCompetition.programSetOrgUnit().orgUnit()), commonValue(context.get(BIND_ORG_UNIT))));

                dql.where(exists(compDQL.buildQuery()));
            }
        }
                .filter(OrgUnit.fullTitle())
                .order(OrgUnit.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler subjectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSubject.class) {
            @Override protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context) {
                super.applyWhereConditions(alias, dql, context);

                OrgUnit formativeOrgUnit = context.get(BIND_FORMATIVE_ORG_UNIT);

                DQLSelectBuilder compDQL = new DQLSelectBuilder()
                        .fromEntity(EnrEntrantForeignRequest.class, "r").column("r.id")
                        .joinPath(DQLJoinType.inner, EnrEntrantForeignRequest.competition().fromAlias("r"), "comp")
                        .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().programSubject().fromAlias("comp")), property(alias)))
                        .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().enrollmentCampaign().fromAlias("comp")), commonValue(context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN))))
                        .where(eq(property("comp", EnrCompetition.requestType()), commonValue(context.get(BIND_REQUEST_TYPE))))
                        .where(eq(property("comp", EnrCompetition.type().compensationType()), commonValue(context.get(BIND_COMPENSATION_TYPE))))
                        .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().programForm().fromAlias("comp")), commonValue(context.get(BIND_FORM))))
                        .where(eq(property("comp", EnrCompetition.programSetOrgUnit().orgUnit()), commonValue(context.get(BIND_ORG_UNIT))));

                if (formativeOrgUnit != null) {
                    compDQL.where(eq(property("comp", EnrCompetition.programSetOrgUnit().formativeOrgUnit()), value(formativeOrgUnit)));
                }

                dql.where(exists(compDQL.buildQuery()));
            }
        }
                .order(EduProgramSubject.code())
                .order(EduProgramSubject.title())
                .filter(EduProgramSubject.code())
                .filter(EduProgramSubject.title())
                .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler programSetDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrProgramSetBase.class) {
            @Override protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context) {
                super.applyWhereConditions(alias, dql, context);

                OrgUnit formativeOrgUnit = context.get(BIND_FORMATIVE_ORG_UNIT);

                DQLSelectBuilder compDQL = new DQLSelectBuilder()
                        .fromEntity(EnrEntrantForeignRequest.class, "r").column("r.id")
                        .joinPath(DQLJoinType.inner, EnrEntrantForeignRequest.competition().fromAlias("r"), "comp")
                        .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().fromAlias("comp")), property(alias)))
                        .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().enrollmentCampaign().fromAlias("comp")), commonValue(context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN))))
                        .where(eq(property("comp", EnrCompetition.requestType()), commonValue(context.get(BIND_REQUEST_TYPE))))
                        .where(eq(property("comp", EnrCompetition.type().compensationType()), commonValue(context.get(BIND_COMPENSATION_TYPE))))
                        .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().programForm().fromAlias("comp")), commonValue(context.get(BIND_FORM))))
                        .where(eq(property("comp", EnrCompetition.programSetOrgUnit().orgUnit()), commonValue(context.get(BIND_ORG_UNIT))))
                        .where(eq(property("comp", EnrCompetition.programSetOrgUnit().programSet().programSubject()), commonValue(context.get(BIND_SUBJECT))))
                        .where(eq(property("comp", EnrCompetition.type()), commonValue(context.get(BIND_COMPETITION_TYPE))))
                        ;

                if (formativeOrgUnit != null) {
                    compDQL.where(eq(property("comp", EnrCompetition.programSetOrgUnit().formativeOrgUnit()), value(formativeOrgUnit)));
                }

                dql.where(exists(compDQL.buildQuery()));
            }
        }
                .order(EnrProgramSetBase.title())
                .filter(EnrProgramSetBase.title())
                .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler competitionDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrCompetition.class) {
            @Override protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context) {
                super.applyWhereConditions(alias, dql, context);

                OrgUnit formativeOrgUnit = context.get(BIND_FORMATIVE_ORG_UNIT);
                EnrProgramSetBase programSet = context.get(BIND_PROGRAM_SET);

                dql
                        .where(eq(property(alias, EnrCompetition.programSetOrgUnit().programSet().enrollmentCampaign()), commonValue(context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN))))
                        .where(eq(property(alias, EnrCompetition.requestType()), commonValue(context.get(BIND_REQUEST_TYPE))))
                        .where(eq(property(alias, EnrCompetition.type().compensationType()), commonValue(context.get(BIND_COMPENSATION_TYPE))))
                        .where(eq(property(alias, EnrCompetition.programSetOrgUnit().programSet().programForm()), commonValue(context.get(BIND_FORM))))
                        .where(eq(property(alias, EnrCompetition.programSetOrgUnit().orgUnit()), commonValue(context.get(BIND_ORG_UNIT))))
                        .where(eq(property(alias, EnrCompetition.programSetOrgUnit().programSet().programSubject()), commonValue(context.get(BIND_SUBJECT))))
                        .where(eq(property(alias, EnrCompetition.type()), commonValue(context.get(BIND_COMPETITION_TYPE))))
                ;

                if (formativeOrgUnit != null) {
                    dql.where(eq(property(alias, EnrCompetition.programSetOrgUnit().formativeOrgUnit()), value(formativeOrgUnit)));
                }
                if (programSet != null) {
                    dql.where(eq(property(alias, EnrCompetition.programSetOrgUnit().programSet()), value(programSet)));
                }


                DQLSelectBuilder compDQL = new DQLSelectBuilder()
                        .fromEntity(EnrEntrantForeignRequest.class, "r").column("r.id")
                        .where(eq(property(EnrEntrantForeignRequest.competition().fromAlias("r")), property(alias)));

                dql.where(exists(compDQL.buildQuery()));
            }
        }
                .order(EnrCompetition.programSetOrgUnit().programSet().title())
                .order(EnrCompetition.eduLevelRequirement().code())
                .filter(EnrCompetition.programSetOrgUnit().programSet().title())
                .pageable(true);
    }
}



    