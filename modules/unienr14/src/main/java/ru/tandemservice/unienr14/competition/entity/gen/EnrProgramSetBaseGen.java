package ru.tandemservice.unienr14.competition.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unienr14.catalog.entity.EnrMethodDivCompetitions;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.competition.entity.IEnrExamSetVariantOwner;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Набор ОП для приема
 *
 * Определяет направление (проф., спец.) и перечень ОП, на который формируются конкурсы. Базовый класс.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrProgramSetBaseGen extends EntityBase
 implements IEnrExamSetVariantOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase";
    public static final String ENTITY_NAME = "enrProgramSetBase";
    public static final int VERSION_HASH = 1782361127;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String L_PROGRAM_SUBJECT = "programSubject";
    public static final String L_PROGRAM_FORM = "programForm";
    public static final String L_METHOD_DIV_COMPETITIONS = "methodDivCompetitions";
    public static final String P_TITLE = "title";
    public static final String P_PRINT_TITLE = "printTitle";
    public static final String L_REQUEST_TYPE = "requestType";
    public static final String P_ACCEPT_PEOPLE_RESIDING_IN_CRIMEA = "acceptPeopleResidingInCrimea";
    public static final String P_TARGET_CONTRACT_TRAINING = "targetContractTraining";

    private EnrEnrollmentCampaign _enrollmentCampaign;     // ПК
    private EduProgramSubject _programSubject;     // Направление подготовки
    private EduProgramForm _programForm;     // Форма обучения
    private EnrMethodDivCompetitions _methodDivCompetitions;     // Способ деления конкурсов
    private String _title;     // Название
    private String _printTitle;     // Печатное название
    private EnrRequestType _requestType;     // Вид заявления
    private boolean _acceptPeopleResidingInCrimea = false;     // Прием лиц, постоянно проживающих в Крыму
    private boolean _targetContractTraining = false;     // Целевая контрактная подготовка

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return ПК. Свойство не может быть null.
     */
    @NotNull
    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign ПК. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Направление подготовки. Свойство не может быть null.
     */
    @NotNull
    public EduProgramSubject getProgramSubject()
    {
        return _programSubject;
    }

    /**
     * @param programSubject Направление подготовки. Свойство не может быть null.
     */
    public void setProgramSubject(EduProgramSubject programSubject)
    {
        dirty(_programSubject, programSubject);
        _programSubject = programSubject;
    }

    /**
     * @return Форма обучения. Свойство не может быть null.
     */
    @NotNull
    public EduProgramForm getProgramForm()
    {
        return _programForm;
    }

    /**
     * @param programForm Форма обучения. Свойство не может быть null.
     */
    public void setProgramForm(EduProgramForm programForm)
    {
        dirty(_programForm, programForm);
        _programForm = programForm;
    }

    /**
     * @return Способ деления конкурсов. Свойство не может быть null.
     */
    @NotNull
    public EnrMethodDivCompetitions getMethodDivCompetitions()
    {
        return _methodDivCompetitions;
    }

    /**
     * @param methodDivCompetitions Способ деления конкурсов. Свойство не может быть null.
     */
    public void setMethodDivCompetitions(EnrMethodDivCompetitions methodDivCompetitions)
    {
        dirty(_methodDivCompetitions, methodDivCompetitions);
        _methodDivCompetitions = methodDivCompetitions;
    }

    /**
     * Название набора ОП
     *
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * Печатное название набора ОП
     *
     * @return Печатное название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPrintTitle()
    {
        return _printTitle;
    }

    /**
     * @param printTitle Печатное название. Свойство не может быть null.
     */
    public void setPrintTitle(String printTitle)
    {
        dirty(_printTitle, printTitle);
        _printTitle = printTitle;
    }

    /**
     * @return Вид заявления. Свойство не может быть null.
     */
    @NotNull
    public EnrRequestType getRequestType()
    {
        return _requestType;
    }

    /**
     * @param requestType Вид заявления. Свойство не может быть null.
     */
    public void setRequestType(EnrRequestType requestType)
    {
        dirty(_requestType, requestType);
        _requestType = requestType;
    }

    /**
     * @return Прием лиц, постоянно проживающих в Крыму. Свойство не может быть null.
     */
    @NotNull
    public boolean isAcceptPeopleResidingInCrimea()
    {
        return _acceptPeopleResidingInCrimea;
    }

    /**
     * @param acceptPeopleResidingInCrimea Прием лиц, постоянно проживающих в Крыму. Свойство не может быть null.
     */
    public void setAcceptPeopleResidingInCrimea(boolean acceptPeopleResidingInCrimea)
    {
        dirty(_acceptPeopleResidingInCrimea, acceptPeopleResidingInCrimea);
        _acceptPeopleResidingInCrimea = acceptPeopleResidingInCrimea;
    }

    /**
     * @return Целевая контрактная подготовка. Свойство не может быть null.
     */
    @NotNull
    public boolean isTargetContractTraining()
    {
        return _targetContractTraining;
    }

    /**
     * @param targetContractTraining Целевая контрактная подготовка. Свойство не может быть null.
     */
    public void setTargetContractTraining(boolean targetContractTraining)
    {
        dirty(_targetContractTraining, targetContractTraining);
        _targetContractTraining = targetContractTraining;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrProgramSetBaseGen)
        {
            setEnrollmentCampaign(((EnrProgramSetBase)another).getEnrollmentCampaign());
            setProgramSubject(((EnrProgramSetBase)another).getProgramSubject());
            setProgramForm(((EnrProgramSetBase)another).getProgramForm());
            setMethodDivCompetitions(((EnrProgramSetBase)another).getMethodDivCompetitions());
            setTitle(((EnrProgramSetBase)another).getTitle());
            setPrintTitle(((EnrProgramSetBase)another).getPrintTitle());
            setRequestType(((EnrProgramSetBase)another).getRequestType());
            setAcceptPeopleResidingInCrimea(((EnrProgramSetBase)another).isAcceptPeopleResidingInCrimea());
            setTargetContractTraining(((EnrProgramSetBase)another).isTargetContractTraining());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrProgramSetBaseGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrProgramSetBase.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("EnrProgramSetBase is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "programSubject":
                    return obj.getProgramSubject();
                case "programForm":
                    return obj.getProgramForm();
                case "methodDivCompetitions":
                    return obj.getMethodDivCompetitions();
                case "title":
                    return obj.getTitle();
                case "printTitle":
                    return obj.getPrintTitle();
                case "requestType":
                    return obj.getRequestType();
                case "acceptPeopleResidingInCrimea":
                    return obj.isAcceptPeopleResidingInCrimea();
                case "targetContractTraining":
                    return obj.isTargetContractTraining();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrEnrollmentCampaign) value);
                    return;
                case "programSubject":
                    obj.setProgramSubject((EduProgramSubject) value);
                    return;
                case "programForm":
                    obj.setProgramForm((EduProgramForm) value);
                    return;
                case "methodDivCompetitions":
                    obj.setMethodDivCompetitions((EnrMethodDivCompetitions) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "printTitle":
                    obj.setPrintTitle((String) value);
                    return;
                case "requestType":
                    obj.setRequestType((EnrRequestType) value);
                    return;
                case "acceptPeopleResidingInCrimea":
                    obj.setAcceptPeopleResidingInCrimea((Boolean) value);
                    return;
                case "targetContractTraining":
                    obj.setTargetContractTraining((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "programSubject":
                        return true;
                case "programForm":
                        return true;
                case "methodDivCompetitions":
                        return true;
                case "title":
                        return true;
                case "printTitle":
                        return true;
                case "requestType":
                        return true;
                case "acceptPeopleResidingInCrimea":
                        return true;
                case "targetContractTraining":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "programSubject":
                    return true;
                case "programForm":
                    return true;
                case "methodDivCompetitions":
                    return true;
                case "title":
                    return true;
                case "printTitle":
                    return true;
                case "requestType":
                    return true;
                case "acceptPeopleResidingInCrimea":
                    return true;
                case "targetContractTraining":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrollmentCampaign":
                    return EnrEnrollmentCampaign.class;
                case "programSubject":
                    return EduProgramSubject.class;
                case "programForm":
                    return EduProgramForm.class;
                case "methodDivCompetitions":
                    return EnrMethodDivCompetitions.class;
                case "title":
                    return String.class;
                case "printTitle":
                    return String.class;
                case "requestType":
                    return EnrRequestType.class;
                case "acceptPeopleResidingInCrimea":
                    return Boolean.class;
                case "targetContractTraining":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrProgramSetBase> _dslPath = new Path<EnrProgramSetBase>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrProgramSetBase");
    }
            

    /**
     * @return ПК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase#getEnrollmentCampaign()
     */
    public static EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Направление подготовки. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase#getProgramSubject()
     */
    public static EduProgramSubject.Path<EduProgramSubject> programSubject()
    {
        return _dslPath.programSubject();
    }

    /**
     * @return Форма обучения. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase#getProgramForm()
     */
    public static EduProgramForm.Path<EduProgramForm> programForm()
    {
        return _dslPath.programForm();
    }

    /**
     * @return Способ деления конкурсов. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase#getMethodDivCompetitions()
     */
    public static EnrMethodDivCompetitions.Path<EnrMethodDivCompetitions> methodDivCompetitions()
    {
        return _dslPath.methodDivCompetitions();
    }

    /**
     * Название набора ОП
     *
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * Печатное название набора ОП
     *
     * @return Печатное название. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase#getPrintTitle()
     */
    public static PropertyPath<String> printTitle()
    {
        return _dslPath.printTitle();
    }

    /**
     * @return Вид заявления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase#getRequestType()
     */
    public static EnrRequestType.Path<EnrRequestType> requestType()
    {
        return _dslPath.requestType();
    }

    /**
     * @return Прием лиц, постоянно проживающих в Крыму. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase#isAcceptPeopleResidingInCrimea()
     */
    public static PropertyPath<Boolean> acceptPeopleResidingInCrimea()
    {
        return _dslPath.acceptPeopleResidingInCrimea();
    }

    /**
     * @return Целевая контрактная подготовка. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase#isTargetContractTraining()
     */
    public static PropertyPath<Boolean> targetContractTraining()
    {
        return _dslPath.targetContractTraining();
    }

    public static class Path<E extends EnrProgramSetBase> extends EntityPath<E>
    {
        private EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> _enrollmentCampaign;
        private EduProgramSubject.Path<EduProgramSubject> _programSubject;
        private EduProgramForm.Path<EduProgramForm> _programForm;
        private EnrMethodDivCompetitions.Path<EnrMethodDivCompetitions> _methodDivCompetitions;
        private PropertyPath<String> _title;
        private PropertyPath<String> _printTitle;
        private EnrRequestType.Path<EnrRequestType> _requestType;
        private PropertyPath<Boolean> _acceptPeopleResidingInCrimea;
        private PropertyPath<Boolean> _targetContractTraining;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return ПК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase#getEnrollmentCampaign()
     */
        public EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Направление подготовки. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase#getProgramSubject()
     */
        public EduProgramSubject.Path<EduProgramSubject> programSubject()
        {
            if(_programSubject == null )
                _programSubject = new EduProgramSubject.Path<EduProgramSubject>(L_PROGRAM_SUBJECT, this);
            return _programSubject;
        }

    /**
     * @return Форма обучения. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase#getProgramForm()
     */
        public EduProgramForm.Path<EduProgramForm> programForm()
        {
            if(_programForm == null )
                _programForm = new EduProgramForm.Path<EduProgramForm>(L_PROGRAM_FORM, this);
            return _programForm;
        }

    /**
     * @return Способ деления конкурсов. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase#getMethodDivCompetitions()
     */
        public EnrMethodDivCompetitions.Path<EnrMethodDivCompetitions> methodDivCompetitions()
        {
            if(_methodDivCompetitions == null )
                _methodDivCompetitions = new EnrMethodDivCompetitions.Path<EnrMethodDivCompetitions>(L_METHOD_DIV_COMPETITIONS, this);
            return _methodDivCompetitions;
        }

    /**
     * Название набора ОП
     *
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EnrProgramSetBaseGen.P_TITLE, this);
            return _title;
        }

    /**
     * Печатное название набора ОП
     *
     * @return Печатное название. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase#getPrintTitle()
     */
        public PropertyPath<String> printTitle()
        {
            if(_printTitle == null )
                _printTitle = new PropertyPath<String>(EnrProgramSetBaseGen.P_PRINT_TITLE, this);
            return _printTitle;
        }

    /**
     * @return Вид заявления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase#getRequestType()
     */
        public EnrRequestType.Path<EnrRequestType> requestType()
        {
            if(_requestType == null )
                _requestType = new EnrRequestType.Path<EnrRequestType>(L_REQUEST_TYPE, this);
            return _requestType;
        }

    /**
     * @return Прием лиц, постоянно проживающих в Крыму. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase#isAcceptPeopleResidingInCrimea()
     */
        public PropertyPath<Boolean> acceptPeopleResidingInCrimea()
        {
            if(_acceptPeopleResidingInCrimea == null )
                _acceptPeopleResidingInCrimea = new PropertyPath<Boolean>(EnrProgramSetBaseGen.P_ACCEPT_PEOPLE_RESIDING_IN_CRIMEA, this);
            return _acceptPeopleResidingInCrimea;
        }

    /**
     * @return Целевая контрактная подготовка. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase#isTargetContractTraining()
     */
        public PropertyPath<Boolean> targetContractTraining()
        {
            if(_targetContractTraining == null )
                _targetContractTraining = new PropertyPath<Boolean>(EnrProgramSetBaseGen.P_TARGET_CONTRACT_TRAINING, this);
            return _targetContractTraining;
        }

        public Class getEntityClass()
        {
            return EnrProgramSetBase.class;
        }

        public String getEntityName()
        {
            return "enrProgramSetBase";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
