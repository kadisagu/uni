/* $Id:$ */
package ru.tandemservice.unienr14.exams.bo.EnrSchedule;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unienr14.exams.bo.EnrSchedule.logic.EnrScheduleDao;
import ru.tandemservice.unienr14.exams.bo.EnrSchedule.logic.IEnrScheduleDao;

/**
 * @author oleyba
 * @since 5/16/13
 */
@Configuration
public class EnrScheduleManager extends BusinessObjectManager
{
    public static EnrScheduleManager instance()
    {
        return instance(EnrScheduleManager.class);
    }

    @Bean
    public IEnrScheduleDao dao()
    {
        return new EnrScheduleDao();
    }

    /**
     * Выводятся данные о свободных местах в событии.
     * @param discCount сумма ДДС по ЭГ-ам события
     * @param roomSize вместимость аудитории
     * @return
     * <li>если сумма ДДС по ЭГ меньше вместимости аудитории, то выводим "[сумма ДДС по ЭГ] (свободно: [вместимость - сумма ДДС по ЭГ])"</li>
     * <li>если сумма ДДС по ЭГ равна вместимости аудитории, то выводим "максимум"</li>
     * <li>если сумма ДДС по ЭГ больше вместимости аудитории, то выводим красным шрифтом (или с розовой заливкой ячейки)"[сумма ДДС по ЭГ] (превышение: [сумма ДДС по ЭГ - вместимость])"</li>
     */
    public String getEventRoomSizeStr(int discCount, int roomSize)
    {
        if (discCount < roomSize)
            return getProperty("roomSizeExceedStr.less", discCount, roomSize - discCount);
        if (discCount == roomSize)
            return getProperty("roomSizeExceedStr.equals");
        if (discCount > roomSize)
            return getProperty("roomSizeExceedStr.great", discCount, discCount - roomSize);

        return null;
    }
}
