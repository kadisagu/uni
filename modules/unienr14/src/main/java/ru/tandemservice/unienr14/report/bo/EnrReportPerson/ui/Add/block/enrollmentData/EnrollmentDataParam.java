/* $Id$ */
package ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.block.enrollmentData;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLSelectableQuery;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.*;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.order.entity.EnrCancelExtract;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.EnrReportPersonAddUI;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unimove.entity.catalog.OrderStates;
import ru.tandemservice.unimove.entity.catalog.codes.OrderStatesCodes;

import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author azhebko
 * @since 05.08.2014
 */
public class EnrollmentDataParam implements IReportDQLModifier
{
    private final IReportParam<DataWrapper> _enrollmentOrder = new ReportParam<>();
    private final IReportParam<String> _enrollmentOrderNumber = new ReportParam<>();
    private final IReportParam<Date> _enrollmentOrderDate = new ReportParam<>();
    private final IReportParam<List<OrderStates>> _enrollmentOrderState = new ReportParam<>();
    private final IReportParam<DataWrapper> _cancelledExtracts = new ReportParam<>();

    @Override
    public void validateReportParams(ErrorCollector errorCollector)
    {

    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        if (_enrollmentOrder.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "enrollmentData.enrollmentOrder", _enrollmentOrder.getData().getTitle());

            String eAlias = dql.nextAlias();

            IDQLSelectableQuery extractEntrants = new DQLSelectBuilder()
                .fromEntity(EnrEnrollmentExtract.class, eAlias)
                .where(eq(property(eAlias, EnrEnrollmentExtract.entity().request().entrant().id()), property(entrantAlias(dql), EnrEntrant.id())))
                .buildQuery();

            dql.builder.where(TwinComboDataSourceHandler.getSelectedValueNotNull(_enrollmentOrder.getData()) ? exists(extractEntrants) : notExists(extractEntrants));
        }

        if (_enrollmentOrderNumber.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "enrollmentData.enrollmentOrderNumber", _enrollmentOrderNumber.getData());

            dql.builder.where(eq(DQLFunctions.upper(property(extractAlias(dql), EnrEnrollmentExtract.paragraph().order().number())), value(_enrollmentOrderNumber.getData().toUpperCase())));
        }

        if (_enrollmentOrderDate.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "enrollmentData.enrollmentOrderDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(_enrollmentOrderDate.getData()));

            FilterUtils.applyBetweenFilter(dql.builder, extractAlias(dql), EnrEnrollmentExtract.paragraph().order().commitDate().s(), _enrollmentOrderDate.getData(), _enrollmentOrderDate.getData());
        }

        if (_enrollmentOrderState.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "enrollmentData.enrollmentOrderState", CommonBaseStringUtil.join(_enrollmentOrderState.getData(), OrderStates.P_TITLE, ", "));

            FilterUtils.applySelectFilter(dql.builder, extractAlias(dql), EnrEnrollmentExtract.paragraph().order().state(), _enrollmentOrderState.getData());
        }

        if (_cancelledExtracts.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "enrollmentData.cancelledExtracts", _cancelledExtracts.getData().getTitle());

            String eAlias = dql.nextAlias();
            IDQLSelectableQuery cancelExtracts = new DQLSelectBuilder()
                .fromEntity(EnrCancelExtract.class, eAlias)
                .where(eq(property(eAlias, EnrCancelExtract.entity().id()), property(extractAlias(dql), EnrEnrollmentExtract.id())))
                .where(eq(property(eAlias, EnrCancelExtract.paragraph().order().state().code()), value(OrderStatesCodes.FINISHED)))
                .buildQuery();

            if (EnrollmentDataBlock.CANCELLED_EXTRACTS_ONLY.equals(_cancelledExtracts.getData().getId()))
            {
                dql.builder.where(exists(cancelExtracts));

            } else if (EnrollmentDataBlock.CANCELLED_EXTRACTS_NOT_INCLUDED.equals(_cancelledExtracts.getData().getId()))
            {
                dql.builder.where(notExists(cancelExtracts));
            }
        }
    }

    private String entrantAlias(ReportDQL dql)
    {
        return dql.innerJoinEntity(EnrEntrant.class, EnrEntrant.person());
    }

    private String extractAlias(ReportDQL dql)
    {
        String requestAlias = dql.innerJoinEntity(entrantAlias(dql), EnrEntrantRequest.class, EnrEntrantRequest.entrant());
        String reqCompetitionAlias = dql.innerJoinEntity(requestAlias, EnrRequestedCompetition.class, EnrRequestedCompetition.request());

        return dql.innerJoinEntity(reqCompetitionAlias, EnrEnrollmentExtract.class, EnrEnrollmentExtract.entity());
    }

    // Getters
    public IReportParam<DataWrapper> getEnrollmentOrder() { return _enrollmentOrder; }
    public IReportParam<String> getEnrollmentOrderNumber() { return _enrollmentOrderNumber; }
    public IReportParam<Date> getEnrollmentOrderDate() { return _enrollmentOrderDate; }
    public IReportParam<List<OrderStates>> getEnrollmentOrderState() { return _enrollmentOrderState; }
    public IReportParam<DataWrapper> getCancelledExtracts() { return _cancelledExtracts; }
}