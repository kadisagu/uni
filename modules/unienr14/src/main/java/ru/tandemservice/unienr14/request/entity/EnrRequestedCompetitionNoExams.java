package ru.tandemservice.unienr14.request.entity;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.request.entity.gen.*;

/**
 * Выбранный конкурс без ВИ
 */
public class EnrRequestedCompetitionNoExams extends EnrRequestedCompetitionNoExamsGen
{
    public EnrRequestedCompetitionNoExams()
    {
    }

    public EnrRequestedCompetitionNoExams(EnrEntrantRequest entrantRequest, EnrCompetition competition)
    {
        setRequest(entrantRequest);
        setCompetition(competition);
    }

    @Override
    @EntityDSLSupport
    public String getParametersTitle()
    {
        StringBuilder builder = new StringBuilder("Без ВИ (")
                .append(getProgramSetItem().getProgram().getTitle())
                .append("):")
                .append(getBenefitCategory().getShortTitle());
        if (this.getCompetition().isContract())
        {
            StringBuilder contractStr = new StringBuilder();
            if (getContractCommonNumber() != null || getContractCommonDate() != null)
            {
                contractStr.append("договор");
                if (getContractCommonNumber() != null)
                    contractStr.append(" №").append(getContractCommonNumber());

                if (getContractCommonDate() != null)
                    contractStr.append(" от ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getContractCommonDate()));
            }

            builder.append(getExternalOrgUnit() == null ? "" : ", " + getExternalOrgUnit().getTitleWithLegalForm())
                    .append(contractStr.length() > 0 ? ", " : "")
                    .append(contractStr.toString());
        }
        return builder.toString();
    }

    @Override
    public String getBenefitStatementTitle()
    {
        return
            "Конкурс: " + this.getTitle() + "\n" +
            "ОП: " + this.getProgramSetItem().getProgram().getTitleWithCodeAndConditionsShortWithForm();
    }
}