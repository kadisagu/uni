/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.ui.CompetitionInStepPub;

import com.google.common.collect.Maps;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrTargetAdmissionKind;
import ru.tandemservice.unienr14.enrollment.entity.EnrModelStep;
import ru.tandemservice.unienr14.enrollment.entity.EnrModelStepItem;
import ru.tandemservice.unienr14.enrollment.entity.gen.EnrModelStepItemGen;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 3/31/15
 */
public class EnrModelStepItemDSHandler extends DefaultSearchDataSourceHandler
{
    public EnrModelStepItemDSHandler(String ownerId)
    {
        super(ownerId);
    }

    public static final String BIND_ENR_STEP = "step";
    public static final String BIND_COMPETITION = "competition";
    public static final String BIND_TA_KIND = "taKind";
    public static final String BIND_ENR_MODEL = "model";

    public static final String VIEW_PROP_OTHER_ENR_SELECTION = "otherSelection";
    public static final String VIEW_PROP_RELATIVE_POSITION = "relativePosition";
    public static final String VIEW_PROP_RELATIVE_RANK = "relativeRank";


    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context) {

        EnrModelStep step = context.get(BIND_ENR_STEP);

        final DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(EnrModelStepItem.class, "i")
            .column(property("i"))
            .order(property(EnrModelStepItem.entity().position().fromAlias("i")))
            .where(eq(property(EnrModelStepItem.step().fromAlias("i")), value(step)))
            .where(eq(property(EnrModelStepItem.entity().requestedCompetition().competition().fromAlias("i")), commonValue(context.get(BIND_COMPETITION))))
            ;

        Map<Long, Integer> relativeRankMap = Maps.newHashMap();
        EnrTargetAdmissionKind taKind = context.get(BIND_TA_KIND);
        if (null != taKind) {
            dql
                .joinPath(DQLJoinType.inner, EnrModelStepItem.entity().fromAlias("i"), "r")
                .joinEntity("i", DQLJoinType.inner, EnrRequestedCompetitionTA.class, "rta", eq(property("rta"), property(EnrRatingItem.requestedCompetition().fromAlias("r"))))
                .where(eq(property(EnrRequestedCompetitionTA.targetAdmissionKind().targetAdmissionKind().fromAlias("rta")), value(taKind)));

            int relativeRank = 0;
            Integer prevRank = null;
            for (EnrModelStepItem item : dql.createStatement(context.getSession()).<EnrModelStepItem>list())
            {
                Integer currRank = item.getEntity().getPositionAbsolute();
                relativeRank = prevRank == null ? 1 : currRank.equals(prevRank) ? relativeRank : relativeRank + 1;
                relativeRankMap.put(item.getId(), relativeRank);

                prevRank = item.getEntity().getPositionAbsolute();
            }
        }

        DSOutput output = DQLSelectOutputBuilder.get(input, dql, context.getSession()).order().build();

        List<EnrModelStepItem> itemList = output.getRecordList().stream()
            .map(item -> (EnrModelStepItem) item)
            .collect(Collectors.toList());

        Map<EnrEntrant, List<OtherEnrollmentWrapper>> otherEnrollmentMarks = new HashMap<>();

        final DQLSelectBuilder otherRecDQL = new DQLSelectBuilder()
            .fromEntity(EnrModelStepItem.class, "i").column(property("i"))
            .order(property(EnrModelStepItem.entity().position().fromAlias("i")))
            .where(eq(property(EnrModelStepItem.step().model().fromAlias("i")), value(step.getModel())))
            .where(eq(property(EnrModelStepItem.enrolled().fromAlias("i")), value(Boolean.TRUE)))
            .where(ne(property(EnrModelStepItem.entity().requestedCompetition().competition().fromAlias("i")), commonValue(context.get(BIND_COMPETITION))))
            .where(exists(new DQLSelectBuilder()
                .fromEntity(EnrModelStepItem.class, "i1")
                .column(property("i1"))
                .order(property(EnrModelStepItem.entity().position().fromAlias("i1")))
                .where(eq(property(EnrModelStepItem.step().fromAlias("i1")), value(step)))
                .where(eq(property(EnrModelStepItem.entity().requestedCompetition().competition().fromAlias("i1")), commonValue(context.get(BIND_COMPETITION))))
                .where(eq(property(EnrModelStepItem.entity().requestedCompetition().request().entrant().fromAlias("i1")), property(EnrModelStepItem.entity().requestedCompetition().request().entrant().fromAlias("i"))))
                .buildQuery()))
            ;

        otherRecDQL.createStatement(context.getSession()).<EnrModelStepItem>list().stream()
            .filter(EnrModelStepItemGen::isEnrolled)
            .forEach(other -> otherEnrollmentMarks
                .computeIfAbsent(other.getEntity().getEntrant(), enrEntrant -> new ArrayList<>())
                .add(new OtherEnrollmentWrapper(other)));

        int position = 0;
        int relativePosition = output.getStartRecord();
        int relativeRank;
        for (DataWrapper wrapper : DataWrapper.wrap(output)) {

            EnrModelStepItem item = IUniBaseDao.instance.get().get(EnrModelStepItem.class, wrapper.getId());
            EnrModelStepItem prev = position == 0 ? null : itemList.get(position - 1);
            EnrModelStepItem next = position + 2 > itemList.size() ? null : itemList.get(position + 1);

            if (null != taKind)
            {
                relativePosition++;
                relativeRank = relativeRankMap.get(wrapper.getId());
            }
            else
            {
                relativePosition = item.getEntity().getPosition();
                relativeRank = item.getEntity().getPositionAbsolute();
            }

            wrapper.setProperty(VIEW_PROP_OTHER_ENR_SELECTION, otherEnrollmentMarks.getOrDefault(item.getEntity().getEntrant(), Collections.emptyList()));
            wrapper.setProperty(VIEW_PROP_RELATIVE_POSITION, relativePosition);
            wrapper.setProperty(VIEW_PROP_RELATIVE_RANK, relativeRank);
            wrapper.setProperty("upDisabled", prev == null || item.getEntity().getPositionAbsolute() != prev.getEntity().getPositionAbsolute());
            wrapper.setProperty("downDisabled", next == null || item.getEntity().getPositionAbsolute() != next.getEntity().getPositionAbsolute());
            position++;
        }

        return output;
    }

    public static class OtherEnrollmentWrapper extends IdentifiableWrapper {
        private static final long serialVersionUID = 1L;

        private EnrModelStep step;
        private EnrRequestedCompetition requestedCompetition;

        public OtherEnrollmentWrapper(EnrModelStepItem stepItem)
        {
            super(stepItem.getEntity().getRequestedCompetition().getId(), getTitle(stepItem));
            this.step = stepItem.getStep();
            this.requestedCompetition = stepItem.getEntity().getRequestedCompetition();
        }

        static String getTitle(EnrModelStepItem stepItem)
        {
            EnrRequestedCompetition rc = stepItem.getEntity().getRequestedCompetition();
            boolean taCompOn = stepItem.getStep().getModel().getEnrollmentCampaign().getSettings().isTargetAdmissionCompetition();
            return stepItem.getStep().getDateStr()
                + " — "
                + rc.getTitle()
                + (taCompOn && rc instanceof EnrRequestedCompetitionTA ? ", " + ((EnrRequestedCompetitionTA) rc).getTargetAdmissionKind().getTitle() : "")
                + ", Пр: " + rc.getPriority()
                + (rc.isParallel() ? ", ПАРАЛЛ." : "");
        }

        public Map getPublisherParameters() {
            ParametersMap parametersMap = new ParametersMap()
                .add(EnrEnrollmentModelCompetitionInStepPubUI.PARAM_STEP, step.getId())
                .add(EnrEnrollmentModelCompetitionInStepPubUI.PARAM_COMPETITION, requestedCompetition.getCompetition().getId());
            if (requestedCompetition instanceof EnrRequestedCompetitionTA && step.getModel().getEnrollmentCampaign().getSettings().isTargetAdmissionCompetition()) {
                parametersMap.add(EnrEnrollmentModelCompetitionInStepPubUI.PARAM_TARGET_ADMISSION_KIND, ((EnrRequestedCompetitionTA)requestedCompetition).getTargetAdmissionKind().getTargetAdmissionKind().getId());
            }
            return parametersMap;
        }
    }
}




