package ru.tandemservice.unienr14.competition.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unienr14.competition.entity.EnrExamSetVariant;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetHigher;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Набор ОП для приема (кадры высшей квалификации)
 *
 * Определяет перечень ОП, на который формируются конкурсы, для приема в аспирантуру.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrProgramSetHigherGen extends EnrProgramSetBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.competition.entity.EnrProgramSetHigher";
    public static final String ENTITY_NAME = "enrProgramSetHigher";
    public static final int VERSION_HASH = -177411581;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXAM_SET_VARIANT = "examSetVariant";

    private EnrExamSetVariant _examSetVariant;     // Набор ВИ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Набор ВИ. Свойство не может быть null.
     */
    @NotNull
    public EnrExamSetVariant getExamSetVariant()
    {
        return _examSetVariant;
    }

    /**
     * @param examSetVariant Набор ВИ. Свойство не может быть null.
     */
    public void setExamSetVariant(EnrExamSetVariant examSetVariant)
    {
        dirty(_examSetVariant, examSetVariant);
        _examSetVariant = examSetVariant;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EnrProgramSetHigherGen)
        {
            setExamSetVariant(((EnrProgramSetHigher)another).getExamSetVariant());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrProgramSetHigherGen> extends EnrProgramSetBase.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrProgramSetHigher.class;
        }

        public T newInstance()
        {
            return (T) new EnrProgramSetHigher();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "examSetVariant":
                    return obj.getExamSetVariant();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "examSetVariant":
                    obj.setExamSetVariant((EnrExamSetVariant) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "examSetVariant":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "examSetVariant":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "examSetVariant":
                    return EnrExamSetVariant.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrProgramSetHigher> _dslPath = new Path<EnrProgramSetHigher>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrProgramSetHigher");
    }
            

    /**
     * @return Набор ВИ. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetHigher#getExamSetVariant()
     */
    public static EnrExamSetVariant.Path<EnrExamSetVariant> examSetVariant()
    {
        return _dslPath.examSetVariant();
    }

    public static class Path<E extends EnrProgramSetHigher> extends EnrProgramSetBase.Path<E>
    {
        private EnrExamSetVariant.Path<EnrExamSetVariant> _examSetVariant;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Набор ВИ. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetHigher#getExamSetVariant()
     */
        public EnrExamSetVariant.Path<EnrExamSetVariant> examSetVariant()
        {
            if(_examSetVariant == null )
                _examSetVariant = new EnrExamSetVariant.Path<EnrExamSetVariant>(L_EXAM_SET_VARIANT, this);
            return _examSetVariant;
        }

        public Class getEntityClass()
        {
            return EnrProgramSetHigher.class;
        }

        public String getEntityName()
        {
            return "enrProgramSetHigher";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
