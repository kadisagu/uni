package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * TODO: переместить
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x3_9to10 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
        {
            new ScriptDependency("org.tandemframework", "1.6.15"),
            new ScriptDependency("org.tandemframework.shared", "1.6.3")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrEnrollmentStepItemOrderInfo

        // создано обязательное свойство extractCancelled
        {
            // создать колонку
            tool.createColumn("enr14_enr_step_item_ordinfo_t", new DBColumn("extractcancelled_p", DBType.BOOLEAN));

            // задать значение по умолчанию
            java.lang.Boolean defaultExtractCancelled = Boolean.FALSE;
            tool.executeUpdate("update enr14_enr_step_item_ordinfo_t set extractcancelled_p=? where extractcancelled_p is null", Boolean.FALSE);
            tool.executeUpdate("update enr14_enr_step_item_ordinfo_t set extractcancelled_p=? where extract_id in (select a.id from enr14_order_enr_extract_t a inner join enr14_order_extract_abs_t b on (a.id=b.id) where b.cancelled_p=?)", Boolean.TRUE, Boolean.TRUE);

            // сделать колонку NOT NULL
            tool.setColumnNullable("enr14_enr_step_item_ordinfo_t", "extractcancelled_p", false);

        }


    }
}