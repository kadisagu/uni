/**
 *$Id: IEnrExamRoomDao.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.exams.bo.EnrExamRoom.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignExamRoom;

/**
 * @author Alexander Shaburov
 * @since 21.05.13
 */
public interface IEnrExamRoomDao extends INeedPersistenceSupport
{
    /**
     * Сохраняет или обновляет Аудиторию для проведения ВИ.
     * @param examRoom аудитория
     * @return id аудитории
     */
    Long saveOrUpdate(EnrCampaignExamRoom examRoom);
}
