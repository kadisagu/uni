/* $Id:$ */
package ru.tandemservice.unienr14.exams.bo.EnrStateExamResult.ui.AddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.sec.runtime.SecurityRuntime;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrSecondWaveStateExamPlace;
import ru.tandemservice.unienr14.catalog.entity.EnrStateExamSubject;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

/**
 * @author oleyba
 * @since 4/12/14
 */
@Input({
        @Bind(key = "entrantId", binding = "entrant.id", required = true),
        @Bind(key = "resultId", binding = "result.id"),
        @Bind(key = "entrantPub", binding = "entrantPub")
})
public class EnrStateExamResultAddEditUI extends UIPresenter
{
    private static final String STATE_EXAM_ACCEPTED_PERMISSION_KEY_ENTRANT = "enr14EntrantPubStateExamResultTabAccept";
    private static final String STATE_EXAM_ACCEPTED_PERMISSION_KEY_LIST = "enr14StateExamCertificateListAcceptCert";

    private static final String STATE_EXAM_VERIFIED_BY_USER_PERMISSION_KEY_ENTRANT = "enr14EntrantPubStateExamResultTabVerifyByUser";
    private static final String STATE_EXAM_VERIFIED_BY_USER_PERMISSION_KEY_LIST = "enr14StateExamCertificateListVerifyByUserCert";

    public static final String BIND_ENTRANT = "entrantId";
    public static final String BIND_RESULT = "resultId";

    private EnrEntrant entrant = new EnrEntrant();
    private EnrEntrantStateExamResult result = new EnrEntrantStateExamResult();
    private Long _oldMark;
    private String _oldDocumentNumber;

    private ISelectModel subjectModel;
    private ISelectModel secondWaveExamPlaceModel;
    private boolean entrantPub;


    @Override
    public ISecured getSecuredObject(){ return isEntrantPub() ? getEntrant() : SecurityRuntime.getInstance().getCommonSecurityObject(); }

    @Override
    public void onComponentRefresh()
    {
        setEntrant(IUniBaseDao.instance.get().get(EnrEntrant.class, getEntrant().getId()));
        if (!isAddForm()) {
            setResult(IUniBaseDao.instance.get().get(EnrEntrantStateExamResult.class, getResult().getId()));
            _oldDocumentNumber = getResult().getDocumentNumber();
            _oldMark = getResult().getMarkAsLong();
        }
        else {
            EnrEnrollmentCampaign campaign = getEntrant().getEnrollmentCampaign();

            getResult().setEntrant(getEntrant());
            getResult().setYear(campaign.getEducationYear().getIntValue());
            getResult().setAccepted(campaign.getSettings().isStateExamAccepted());
        }
        setSubjectModel(new LazySimpleSelectModel<>(IUniBaseDao.instance.get().getCatalogItemList(EnrStateExamSubject.class)));
        setSecondWaveExamPlaceModel(new LazySimpleSelectModel<>(EnrSecondWaveStateExamPlace.class));
    }

    public void onClickApply() {
        if (!getResult().isVerifiedByUser())
            getResult().setVerifiedByUserComment(null);
        if(!isAddForm())
        {
            boolean changed = false;
            if(_oldDocumentNumber != null)
            {
                if(getResult().getDocumentNumber() == null)
                    changed = true;
                else if(!_oldDocumentNumber.equals(getResult().getDocumentNumber()))
                    changed = true;
            }
            else
            {
                if(getResult().getDocumentNumber() != null)
                    changed = true;
            }

            if(_oldMark != null)
            {
                if(getResult().getMarkAsLong() == null)
                    changed = true;
                else if(!_oldMark.equals(getResult().getMarkAsLong()))
                    changed = true;
            }
            else
            {
                if(getResult().getMarkAsLong() != null)
                    changed = true;
            }

            if(changed)
                getResult().setStateCheckStatus(null);
        }

        IUniBaseDao.instance.get().saveOrUpdate(getResult());
        deactivate();
    }

    public boolean isAddForm() {
        return getResult().getId() == null;
    }
    
    public EnrEntrant getEntrant()
    {
        return entrant;
    }

    public void setEntrant(EnrEntrant entrant)
    {
        this.entrant = entrant;
    }

    public EnrEntrantStateExamResult getResult()
    {
        return result;
    }

    public void setResult(EnrEntrantStateExamResult result)
    {
        this.result = result;
    }

    public ISelectModel getSubjectModel()
    {
        return subjectModel;
    }

    public void setSubjectModel(ISelectModel subjectModel)
    {
        this.subjectModel = subjectModel;
    }

    public ISelectModel getSecondWaveExamPlaceModel()
    {
        return secondWaveExamPlaceModel;
    }

    public void setSecondWaveExamPlaceModel(ISelectModel secondWaveExamPlaceModel)
    {
        this.secondWaveExamPlaceModel = secondWaveExamPlaceModel;
    }

    public boolean isEntrantPub(){ return entrantPub; }
    public void setEntrantPub(boolean entrantPub){ this.entrantPub = entrantPub; }

    public String getStateExamAcceptedPermissionKey(){ return isEntrantPub() ? STATE_EXAM_ACCEPTED_PERMISSION_KEY_ENTRANT : STATE_EXAM_ACCEPTED_PERMISSION_KEY_LIST; }
    public String getStateExamVerifiedByUserPermissionKey(){ return isEntrantPub() ? STATE_EXAM_VERIFIED_BY_USER_PERMISSION_KEY_ENTRANT : STATE_EXAM_VERIFIED_BY_USER_PERMISSION_KEY_LIST;}
}
