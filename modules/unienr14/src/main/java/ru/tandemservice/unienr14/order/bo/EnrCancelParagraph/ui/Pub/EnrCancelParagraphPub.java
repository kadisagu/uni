/* $Id:$ */
package ru.tandemservice.unienr14.order.bo.EnrCancelParagraph.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.IColumnListExtPointBuilder;
import org.tandemframework.caf.ui.config.datasource.column.ActionDSColumn;
import org.tandemframework.caf.ui.config.datasource.column.TextDSColumn;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.formatter.NoWrapFormatter;
import org.tandemframework.core.view.list.column.DefaultPublisherLinkResolver;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.formatter.EnrEntrantCustomStateCollectionFormatter;
import ru.tandemservice.unienr14.order.bo.EnrCancelParagraph.logic.EnrCancelExtractDSHandler;
import ru.tandemservice.unienr14.order.bo.EnrEnrollmentParagraph.logic.EnrEnrollmentExtractDSHandler;
import ru.tandemservice.unienr14.order.bo.EnrOrder.ui.Pub.EnrOrderPub;
import ru.tandemservice.unienr14.order.entity.EnrCancelExtract;

/**
 * @author oleyba
 * @since 7/10/14
 */
@Configuration
public class EnrCancelParagraphPub extends BusinessComponentManager
{
    public static final String DS_EXTRACT = "extractDS";
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
        .addDataSource(searchListDS(DS_EXTRACT, extractDS(), extractDSHandler()))
        .create();
    }

    @Bean
    public ColumnListExtPoint extractDS()
    {
        IColumnListExtPointBuilder columnBuilder = ColumnListExtPoint.with(getName(), DS_EXTRACT)
        .addColumn(TextDSColumn.with().name("fio").path(EnrCancelExtract.requestedCompetition().request().entrant().person().identityCard().fullFio()).formatter(NoWrapFormatter.INSTANCE).create())
        .addColumn(textColumn(EnrCancelExtractDSHandler.CUSTOM_STATES, EnrCancelExtractDSHandler.CUSTOM_STATES).formatter(new EnrEntrantCustomStateCollectionFormatter()))
        .addColumn(TextDSColumn.with().name("sex").path(EnrCancelExtract.requestedCompetition().request().entrant().person().identityCard().sex().shortTitle()).create())
        .addColumn(TextDSColumn.with().name("passport").path(EnrCancelExtract.requestedCompetition().request().entrant().person().identityCard().fullNumber()).formatter(NoWrapFormatter.INSTANCE).create())
        .addColumn(TextDSColumn.with().name("conditions").path(EnrCancelExtract.requestedCompetition().parametersTitle()).formatter(NoWrapFormatter.INSTANCE).create())
        .addColumn(TextDSColumn.with().name("eduInstitution").path(EnrCancelExtract.requestedCompetition().request().eduDocument().eduLevel().shortTitle()).create())
        .addColumn(TextDSColumn.with().name("competition").path(EnrCancelExtract.requestedCompetition().competition().title()).create());

        if (Debug.isEnabled()) {
            columnBuilder.addColumn(TextDSColumn.with().name("extractState").path(EnrCancelExtract.state().title()));
        }

        return columnBuilder
        .addColumn(booleanColumn("cancelled", EnrCancelExtract.cancelled()))
        .addColumn(publisherColumn("cancelExtract", "shortTitle")
            .entityListProperty(EnrEnrollmentExtractDSHandler.CANCEL_EXTRACT)
//            .primaryKeyPath(EnrAbstractExtract.paragraph().order().id())
            .publisherLinkResolver(new DefaultPublisherLinkResolver() {
                @Override public String getComponentName(IEntity entity) { return EnrOrderPub.class.getSimpleName(); }
                @Override public Object getParameters(IEntity entity) { return ((EnrCancelExtract) entity).getParagraph().getOrder().getId(); }
            })
            .create()
        )
        .addColumn(publisherColumn("otherExtracts", "title")
            .entityListProperty(EnrEnrollmentExtractDSHandler.OTHER_EXTRACTS)
            .formatter(CollectionFormatter.COLLECTION_FORMATTER))
        .addColumn(ActionDSColumn.with().name("exclude").icon(CommonDefines.ICON_DELETE)
            .listener("onClickDeleteExtract")
            .alert(FormattedMessage.with().template("enrollmentExtractDS.exclude.alert").parameter(EnrCancelExtract.requestedCompetition().request().entrant().person().identityCard().fullFio().s()).create())
            .permissionKey("ui:secModel.deleteExtract")
            .disabled(EnrCancelExtractDSHandler.NO_EXCLUDE).create()
        )
        .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> extractDSHandler()
    {
        return new EnrCancelExtractDSHandler(getName());
    }
}