/* $Id$ */
package ru.tandemservice.unienr14.ws.onlineentrant;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author nvankov
 * @since 5/8/14
 */
@XmlRootElement
public class CatalogsData
{
    /** Справочник "Пол" */
    public SexNode sex = new SexNode();

    /** Справочник "Страны" */
    public AddressCountryNode addressCountry = new AddressCountryNode();

    /** Справочник "Виды и уровни образования" */
    public EduLevelNode eduLevel = new EduLevelNode();

    /**
     * Справочник "Степени родства"
     */
    public FamilyStatusNode familyStatus = new FamilyStatusNode();

    /**
     * Справочник "Виды спорта"
     */
    public SportTypeNode sportType = new SportTypeNode();

    /**
     * Справочник "Спортивные разряды и звания"
     */
    public SportRankNode sportRank = new SportRankNode();

    /**
     * Справочник "Иностранные языки"
     */
    public ForeignLanguageNode foreignLanguage = new ForeignLanguageNode();

    /**
     * Справочник "Источники информации об ОУ"
     */
    public EnrSourceInfoAboutUniversityNode enrSourceInfoAboutUniversityNode = new EnrSourceInfoAboutUniversityNode();

    /**
     * Справочник "Подготовительные курсы"
     */
    public EnrAccessCourseNode enrAccessCourseNode = new EnrAccessCourseNode();

    /**
     * Справочник "Подготовительные отделения"
     */
    public EnrAccessDepartmentNode enrAccessDepartmentNode = new EnrAccessDepartmentNode();

    /**
     * Справочник "Степени отличия по окончании образовательного учреждения"
     */
    public GraduationHonourNode graduationHonourNode = new GraduationHonourNode();

    /**
     * Справочник "Виды документов о полученном образовании"
     */
    public EduDocumentKindNode eduDocumentKindNode = new EduDocumentKindNode();

    /**
     * Справочник "Тип удостоверения личности"
     */
    public IdentityCardTypeNode identityCardTypeNode = new IdentityCardTypeNode();

    /**
     * Настройка "Степень отличия для вида док. об образ."
     */
    public GraduationHonour2EduDocumentKindNode graduationHonour2EduDocumentKindNode = new GraduationHonour2EduDocumentKindNode();

    /**
     * Индивидуальные достижения
     */
    public EnrEntrantAchievementTypeNode enrEntrantAchievementTypeNode = new EnrEntrantAchievementTypeNode();


    /** Справочник "Пол" */
    public static class SexNode { public List<CatalogNode> row = new ArrayList<CatalogNode>(); }

    /** Справочник "Страны" */
    public static class AddressCountryNode { public List<CatalogNode> row = new ArrayList<CatalogNode>(); }

    /** Справочник "Виды и уровни образования" */
    public static class EduLevelNode { public List<CatalogNode> row = new ArrayList<CatalogNode>(); }

    /**
     * Справочник "Степени родства"
     */
    public static class FamilyStatusNode
    {
        public List<CatalogNode> catalogNodes = new ArrayList<CatalogNode>();
    }

    /**
     * Справочник "Виды спорта"
     */
    public static class SportTypeNode
    {
        public List<CatalogNode> catalogNodes = new ArrayList<CatalogNode>();
    }

    /**
     * Справочник "Спортивные разряды и звания"
     */
    public static class SportRankNode
    {
        public List<CatalogNode> catalogNodes = new ArrayList<CatalogNode>();
    }

    /**
     * Справочник "Иностранные языки"
     */
    public static class ForeignLanguageNode
    {
        public List<CatalogNode> catalogNodes = new ArrayList<CatalogNode>();
    }

    /**
     * Справочник "Источники информации об ОУ"
     */
    public static class EnrSourceInfoAboutUniversityNode
    {
        public List<CatalogNode> catalogNodes = new ArrayList<CatalogNode>();
    }

    /**
     * Справочник "Подготовительные курсы"
     */
    public static class EnrAccessCourseNode
    {
        public List<CatalogNode> catalogNodes = new ArrayList<CatalogNode>();
    }

    /**
     * Справочник "Подготовительные отделения"
     */
    public static class EnrAccessDepartmentNode
    {
        public List<CatalogNode> catalogNodes = new ArrayList<CatalogNode>();
    }

    /**
     * Справочник "Виды документов о полученном образовании"
     */
    public static class EduDocumentKindNode
    {
        public static class EduDocumentKindRow
        {
            public EduDocumentKindRow()
            {

            }

            @XmlAttribute
            public String id; //  Внутренний код
            @XmlAttribute
            public String shortTitle;     // Сокращенное название
            @XmlAttribute
            public String description;     // Описание
            @XmlAttribute
            public boolean certifyEducationLevel;     // Подтверждает образование
            @XmlAttribute
            public boolean certifyQualification;     // Подтверждает квалификацию
            @XmlAttribute
            public String fixedEducationLevel;     // Уровень образования, соотв. документам данного типа
            @XmlAttribute
            public String defaultQualification;     // Квалификация по умолчанию
            @XmlAttribute
            public boolean seriaAllowed;     // Разрешать ввод серии
            @XmlAttribute
            public boolean seriaRequired;     // Серия обязательна
            @XmlAttribute
            public boolean numberOnlyDigits;     // В номере только цифры
            @XmlAttribute
            public boolean eduProgramSubjectAllowed;     // Разрешать ввод направления (профессии, специальности)
            @XmlAttribute
            public boolean eduProgramSubjectRequired;     // Ввод направления (профессии, специальности) обязателен
            @XmlAttribute
            public boolean avgMarkAllowed;     // Разрешать ввод среднего балла
            @XmlAttribute
            public boolean avgMarkRequired;     // Средний балл обязателен
            @XmlAttribute
            public boolean ruOnly;     // Выдается только в РФ
            @XmlAttribute
            public String title;     // Название
            @XmlAttribute
            public String userCode;     // Код
            @XmlAttribute
            public Date disabledDate;     // Дата запрещения
            @XmlAttribute
            public boolean markStatisticAllowed;     // Разрешать ввод числа оценок 3,4,5
            @XmlAttribute
            public boolean markStatisticRequired;     // Ввод числа оценок 3,4,5 обязателен
            @XmlAttribute
            public boolean seriaOnlyLetters;     // В серии только буквы
            @XmlAttribute
            public boolean issuanceDateRequired;     // Дата выдачи обязательна
            @XmlAttribute
            public boolean registrationNumberAllowed;     // Разрешать ввод регистрационного номера
            @XmlAttribute
            public boolean registrationNumberRequired;     // Ввод регистрационного номера обязателен
        }

        public List<EduDocumentKindRow> catalogNodes = new ArrayList<EduDocumentKindRow>();
    }

    /**
     * Справочник "Тип удостоверения личности"
     */
    public static class IdentityCardTypeNode
    {
        public static class IdentityCardTypeRow
        {
            public IdentityCardTypeRow()
            {

            }

            @XmlAttribute
            public String id; //  Внутренний код
            @XmlAttribute
            public String shortTitle;     // Сокращенное название
            @XmlAttribute
            public boolean showSeria;     // Отображать серию
            @XmlAttribute
            public boolean seriaRequired;     // Обязательность серии
            @XmlAttribute
            public boolean numberRequired;     // Обязательность номера
            @XmlAttribute
            public int minSeriaLength;     // Мин. длина серии
            @XmlAttribute
            public int maxSeriaLength;     // Макс. длина серии
            @XmlAttribute
            public int minNumberLength;     // Мин. длина номера
            @XmlAttribute
            public int maxNumberLength;     // Макс. длина номера
            @XmlAttribute
            public boolean seriaOnlyDigits;     // В серии только цифры
            @XmlAttribute
            public boolean numberOnlyDigits;     // В номере только цифры
            @XmlAttribute
            public boolean onlyCyrillicInFio;     // В ФИО только кириллица
            @XmlAttribute
            public String seriaMask;     // Шаблон серии
            @XmlAttribute
            public String numberMask;     // Шаблон номера
            @XmlAttribute
            public Integer citizenshipDefault;     // Гражданство по умолчанию (код страны)
            @XmlAttribute
            public String title;     // Название
            @XmlAttribute
            public String userCode;     // Код
            @XmlAttribute
            public Date disabledDate;     // Дата запрещения
            // new
            @XmlAttribute
            public boolean numberEnabled;     // Разрешить ввод номера
            @XmlAttribute
            public boolean issuancePlaceEnabled;     // Разрешить ввод кем выдано удостоверение
            @XmlAttribute
            public boolean issuancePlaceRequired;     // Обязательность ввода кем выдано удостоверение
            @XmlAttribute
            public boolean issuanceCodeEnabled;     // Разрешить ввод кода подразделения
            @XmlAttribute
            public boolean issuanceCodeRequired;     // Обязательность кода подразделения
            @XmlAttribute
            public boolean issuanceDateEnabled;     // Разрешить ввод даты выдачи удостоверения
            @XmlAttribute
            public boolean issuanceDateRequired;     // Обязательность даты выдачи удостоверения
            @XmlAttribute
            public boolean birthDateEnabled;     // Разрешить ввод даты рождения
            @XmlAttribute
            public boolean birthDateRequired;     // Обязательность даты рождения
            @XmlAttribute
            public boolean birthPlaceEnabled;     // Разрешить ввод места рождения
            @XmlAttribute
            public boolean birthPlaceRequired;     // Обязательность места рождения
        }

        public List<IdentityCardTypeRow> catalogNodes = new ArrayList<IdentityCardTypeRow>();
    }



    /**
     * Справочник "Степени отличия по окончании образовательного учреждения"
     */
    public static class GraduationHonourNode
    {
        public static class GraduationHonourRow
        {
            @XmlAttribute
            public String id;     // Внутренний код
            @XmlAttribute
            public String title;     // Название
            @XmlAttribute
            public String userCode;     // Код
            @XmlAttribute
            public Date disabledDate;
        }

        public List<GraduationHonourRow> catalogNodes = new ArrayList<GraduationHonourRow>();
    }


    /**
     * Настройка "Степень отличия для вида док. об образ."
     */
    public static class GraduationHonour2EduDocumentKindNode
    {
        public static class GraduationHonour2EduDocumentKindRow
        {
            @XmlAttribute(required = true)
            public String graduationHonour; // Степень отличия по окончании образовательного учреждения
            @XmlAttribute(required = true)
            public String documentKind; // Вид документа о полученном образовании
        }

        public List<GraduationHonour2EduDocumentKindRow> catalogNodes = new ArrayList<GraduationHonour2EduDocumentKindRow>();
    }

    /**
     * Индивидуальные достижения
     */
    public static class EnrEntrantAchievementTypeNode
    {
        public static class EnrEntrantAchievementTypeRow
        {
            public EnrEntrantAchievementTypeRow()
            {

            }

            /**
             * Идентификатор
             */
            @XmlAttribute(required = true)
            public String id;

            /**
             * Название
             */
            @XmlAttribute(required = true)
            public String title;

            /**
             * Описание
             */
            @XmlAttribute
            public String description;

            /**
             * Балл за достижение
             */
            @XmlAttribute
            public Long achievementMarkAsLong;

            /**
             * Вид заявления
             */
            @XmlAttribute
            public String requestType;
        }

        public List<EnrEntrantAchievementTypeRow> enrEntrantAchievementTypes = new ArrayList<EnrEntrantAchievementTypeRow>();
    }

    public static class CatalogNode implements Comparable<CatalogNode>
    {
        public CatalogNode() { }

        public CatalogNode(String title, String id)
        {
            this.title = title;
            this.id = id;
        }

        public CatalogNode(String shortTitle, String title, String id)
        {
            this.shortTitle = shortTitle;
            this.title = title;
            this.id = id;
        }

        @Override
        public int compareTo(CatalogNode o)
        {
            return o.id.compareTo(o.id);
        }

        /**
         * Сокращенное название
         */
        @XmlAttribute
        public String shortTitle;

        /**
         * Название
         */
        @XmlAttribute(required = true)
        public String title;

        /**
         * Идентификатор
         */
        @XmlAttribute(required = true)
        public String id;
    }
}
