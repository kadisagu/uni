/* $Id$ */
package ru.tandemservice.unienr14.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.PreparedStatement;

/**
 * @author Alexey Lopatin
 * @since 01.06.2015
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x8x1_7to8 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
        {
                new ScriptDependency("org.tandemframework", "1.6.17"),
                new ScriptDependency("org.tandemframework.shared", "1.8.1")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        PreparedStatement update = tool.prepareStatement("update enr14_c_benefit_category_t set title_p = ?, shorttitle_p = ? where code_p = ?");

        String[] codes = {"002", "003", "013", "023"};
        String[] titles = {
                "Победители всероссийской олимпиады школьников (IV этапа всеукраинских ученических олимпиад)",
                "Призеры всероссийской олимпиады школьников (IV этапа всеукраинских ученических олимпиад)",
                "Дети погибших военнослужащих",
                "Победители и призеры всероссийской олимпиады школьников (IV этапа всеукраинских ученических олимпиад)"
        };
        String[] shortTitles = {
                "победители ВОШ (всеукр. олимп)",
                "призеры ВОШ (всеукр. олимп)",
                "дети погиб. воен.",
                "победители (призеры) ВОШ (всеукр. олимп)"
        };

        for (int i = 0; i < codes.length; i++)
        {
            update.setString(1, titles[i]);
            update.setString(2, shortTitles[i]);
            update.setString(3, codes[i]);
            update.executeUpdate();
        }

        tool.executeUpdate("update enr14_c_int_exam_reason_t set title_p = ? where code_p = ?", "Лица, постоянно проживающие в Крыму", "7");
    }
}
