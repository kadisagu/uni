/* $Id$ */
package ru.tandemservice.unienr14.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;


/**
 * @author Ekaterina Zvereva
 * @since 15.07.2016
 */
public class MS_unienr14_2x10x4_5to6 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.10.4")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        // свойство refusedToBeEnrolled стало формулой
        if (tool.columnExists("enr14_requested_comp_t", "refusedtobeenrolled_p")) {
            // удалить колонку
            tool.dropColumn("enr14_requested_comp_t", "refusedtobeenrolled_p");
        }
    }
}