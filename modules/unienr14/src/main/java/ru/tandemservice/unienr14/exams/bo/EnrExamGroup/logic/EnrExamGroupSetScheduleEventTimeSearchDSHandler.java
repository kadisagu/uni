/**
 *$Id: EnrExamGroupSetScheduleEventTimeSearchDSHandler.java 33636 2014-04-16 04:31:39Z nfedorovskih $
 */
package ru.tandemservice.unienr14.exams.bo.EnrExamGroup.logic;

import org.apache.commons.lang.mutable.MutableInt;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.NumberAsStringComparator;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unienr14.exams.bo.EnrSchedule.EnrScheduleManager;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroup;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroupScheduleEvent;
import ru.tandemservice.unienr14.exams.entity.EnrExamScheduleEvent;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 03.06.13
 */
public class EnrExamGroupSetScheduleEventTimeSearchDSHandler extends DefaultSearchDataSourceHandler
{

    public EnrExamGroupSetScheduleEventTimeSearchDSHandler(String ownerId)
    {
        super(ownerId, EnrExamScheduleEvent.class);
    }

    public static final String BIND_DATE_FROM = "dateFrom";
    public static final String BIND_DATE_TO = "dateTo";
    public static final String BIND_EXAM_GROUP = "examGroup";

    public static final String V_PROP_EXAM_GROUPS = "examGroups";
    public static final String V_PROP_SIZE_EXCEED = "sizeExceed";
    public static final String V_PROP_ROOM_OCCUPIED_STR = "sizeStr";

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EnrExamScheduleEvent.class, "b").column(property("b"))
                .order(property(EnrExamScheduleEvent.scheduleEvent().durationBegin().fromAlias("b")));

        filter(dql, "b", input, context);
        final DSOutput output = DQLSelectOutputBuilder.get(input, dql, context.getSession()).pageable(false).build();

        return wrap(output, input, context);
    }

    /**
     * @param dql builder from EnrExamScheduleEvent
     */
    protected void filter(DQLSelectBuilder dql, String alias, DSInput input, ExecutionContext context)
    {
        final Date dateFrom = context.get(BIND_DATE_FROM);
        final Date dateTo = context.get(BIND_DATE_TO);
        final EnrExamGroup examGroup = context.get(BIND_EXAM_GROUP);

        if (dateFrom != null)
            dql.where(ge(property(EnrExamScheduleEvent.scheduleEvent().durationBegin().fromAlias(alias)), value(CoreDateUtils.getDayFirstTimeMoment(dateFrom), PropertyType.TIMESTAMP)));
        if (dateTo != null)
            dql.where(lt(property(EnrExamScheduleEvent.scheduleEvent().durationEnd().fromAlias(alias)),
                         value(CoreDateUtils.getDayFirstTimeMoment(CoreDateUtils.add(dateTo, Calendar.DAY_OF_YEAR, 1)) , PropertyType.TIMESTAMP)));

        dql
                .where(eq(property(EnrExamScheduleEvent.discipline().fromAlias(alias)), value(examGroup.getDiscipline())))
                .where(eq(property(EnrExamScheduleEvent.passForm().fromAlias(alias)), value(examGroup.getPassForm())))
                .where(notExists(
                        new DQLSelectBuilder().fromEntity(EnrExamGroupScheduleEvent.class, "ge").column(property(EnrExamGroupScheduleEvent.id().fromAlias("ge")))
                                .where(eq(property(EnrExamGroupScheduleEvent.examScheduleEvent().fromAlias("ge")), property(alias)))
                                .where(ne(property(EnrExamGroupScheduleEvent.examGroup().days().fromAlias("ge")), value(examGroup.getDays())))
                                .buildQuery()));
    }

    protected DSOutput wrap(DSOutput output, DSInput input, ExecutionContext context)
    {
        final Map<Long, MutableInt> schEvent2DiscCountMap = SafeMap.get(MutableInt.class);
        final Map<Long, List<String>> schEvent2ExamGroupsTitleMap = SafeMap.get(ArrayList.class);
        BatchUtils.execute(output.<Long>getRecordList(), 128, new BatchUtils.Action<Long>()
        {
            @Override
            public void execute(Collection<Long> elements)
            {
                final List<Long> groupIdWhithDiscList = new DQLSelectBuilder().fromEntity(EnrExamPassDiscipline.class, "ed")
                        .column(property(EnrExamGroupScheduleEvent.examScheduleEvent().id().fromAlias("ge")))
                        .joinEntity("ed", DQLJoinType.inner, EnrExamGroupScheduleEvent.class, "ge", eq(property(EnrExamGroupScheduleEvent.examGroup().fromAlias("ge")), property(EnrExamPassDiscipline.examGroup().fromAlias("ed"))))
                        .where(in(property(EnrExamGroupScheduleEvent.examScheduleEvent().id().fromAlias("ge")), elements))
                        .createStatement(context.getSession()).list();


                final List<Object[]> groupEventList = new DQLSelectBuilder().fromEntity(EnrExamGroupScheduleEvent.class, "ee")
                        .column(property(EnrExamGroupScheduleEvent.examScheduleEvent().id().fromAlias("ee")))
                        .column(property(EnrExamGroupScheduleEvent.examGroup().title().fromAlias("ee")))
                        .where(in(property(EnrExamGroupScheduleEvent.examScheduleEvent().id().fromAlias("ee")), elements))
                        .createStatement(context.getSession()).list();

                for (Long id : groupIdWhithDiscList)
                {
                    schEvent2DiscCountMap.get(id)
                            .increment();
                }

                for (Object[] objects : groupEventList)
                {
                    final Long schEventId = (Long) objects[0];
                    final String title = (String) objects[1];

                    schEvent2ExamGroupsTitleMap.get(schEventId)
                            .add(title);
                }
            }
        });

        for (List<String> list : schEvent2ExamGroupsTitleMap.values())
            Collections.sort(list, NumberAsStringComparator.INSTANCE);

        for (DataWrapper wrapper : DataWrapper.wrap(output))
        {
            final EnrExamScheduleEvent examScheduleEvent = wrapper.getWrapped();

            wrapper.setProperty(V_PROP_EXAM_GROUPS, schEvent2ExamGroupsTitleMap.get(examScheduleEvent.getId()));
            wrapper.setProperty(V_PROP_ROOM_OCCUPIED_STR, EnrScheduleManager.instance().getEventRoomSizeStr(schEvent2DiscCountMap.get(examScheduleEvent.getId()).intValue(), examScheduleEvent.getSize()));
            wrapper.setProperty(V_PROP_SIZE_EXCEED, schEvent2DiscCountMap.get(examScheduleEvent.getId()).intValue() > examScheduleEvent.getSize());
        }

        return output;
    }
}
