/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import org.tandemframework.shared.person.base.bo.Person.ui.IdentityCardTab.PersonIdentityCardTab;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubAchievementTab.EnrEntrantPubAchievementTab;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubBenefitTab.EnrEntrantPubBenefitTab;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubCustomStateTab.EnrEntrantPubCustomStateTab;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubDataTab.EnrEntrantPubDataTab;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubDocumentTab.EnrEntrantPubDocumentTab;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubExamTab.EnrEntrantPubExamTab;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubLogTab.EnrEntrantPubLogTab;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubPriorityTab.EnrEntrantPubPriorityTab;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubRatingTab.EnrEntrantPubRatingTab;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubRequestTab.EnrEntrantPubRequestTab;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubStateExamResultTab.EnrEntrantPubStateExamResultTab;
import ru.tandemservice.unienr14.request.bo.EnrEntrantForeignRequest.ui.PubTab.EnrEntrantForeignRequestPubTab;

/**
 * @author oleyba
 * @since 4/10/13
 */
@Configuration
public class EnrEntrantPub extends BusinessComponentManager
{
    public static final String TAB_PANEL = "entrantPubTabPanel";
    public static final String TAB_PANEL_REGION_NAME = "entrantPubMainRegion";
    public static final String TAB_PANEL_SUB_REGION_NAME = "entrantDataRegion";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
        .create();
    }

    @Bean
    public TabPanelExtPoint entrantPubTabPanelExtPoint()
    {
        return tabPanelExtPointBuilder(TAB_PANEL)
            .addTab(componentTab("identityCardTab", PersonIdentityCardTab.class).permissionKey("enr14EntrantPubIdentityCardTabView"))
            .addTab(componentTab("dataTab", EnrEntrantPubDataTab.class).permissionKey("enr14EntrantPubDataTabView"))
            .addTab(componentTab("documentTab", EnrEntrantPubDocumentTab.class).permissionKey("enr14EntrantPubDocumentTabView"))
            .addTab(componentTab("stateExamResultTab", EnrEntrantPubStateExamResultTab.class).permissionKey("enr14EntrantPubStateExamResultTabView"))
            .addTab(componentTab("foreignRequestTab", EnrEntrantForeignRequestPubTab.class).visible("ui:foreignTabVisible").permissionKey("enr14EntrantForeignRequestPubTabView"))
            .addTab(componentTab("requestTab", EnrEntrantPubRequestTab.class).permissionKey("enr14EntrantPubRequestTabView"))
            .addTab(componentTab("chosenExamTab", EnrEntrantPubExamTab.class).permissionKey("enr14EntrantPubChosenExamTabView"))
            .addTab(componentTab("priorityTab", EnrEntrantPubPriorityTab.class).permissionKey("enr14EntrantPubPriorityTabView"))
            .addTab(componentTab("ratingTab", EnrEntrantPubRatingTab.class).permissionKey("enr14EntrantPubRatingTabView"))
            .addTab(componentTab("benefitTab", EnrEntrantPubBenefitTab.class).permissionKey("enr14EntrantPubBenefitTabView"))
            .addTab(componentTab("achievementTab", EnrEntrantPubAchievementTab.class).permissionKey("enr14EntrantPubAchievementTabView"))
            .addTab(componentTab("customStateTab", EnrEntrantPubCustomStateTab.class).permissionKey("enr14EntrantPubCustomStatesTabView"))
            .addTab(componentTab("logTab", EnrEntrantPubLogTab.class).permissionKey("enr14EntrantPubLogTabView"))
            .create();
    }
}
