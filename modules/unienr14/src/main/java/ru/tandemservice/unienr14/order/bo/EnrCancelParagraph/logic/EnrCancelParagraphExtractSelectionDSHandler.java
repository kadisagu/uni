/* $Id:$ */
package ru.tandemservice.unienr14.order.bo.EnrCancelParagraph.logic;

import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantCustomState;
import ru.tandemservice.unienr14.order.bo.EnrCancelParagraph.ui.AddEdit.EnrCancelParagraphAddEdit;
import ru.tandemservice.unienr14.order.bo.EnrEnrollmentParagraph.EnrEnrollmentParagraphManager;
import ru.tandemservice.unienr14.order.entity.EnrAbstractExtract;
import ru.tandemservice.unienr14.order.entity.EnrAbstractParagraph;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 7/10/14
 */
public class EnrCancelParagraphExtractSelectionDSHandler extends DefaultSearchDataSourceHandler
{
    public static final String OTHER_EXTRACTS = "otherExtracts";
    public static final String CUSTOM_STATES = "customStates";

    public EnrCancelParagraphExtractSelectionDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final DQLOrderDescriptionRegistry orderDescriptionRegistry = new DQLOrderDescriptionRegistry(EnrAbstractExtract.class, "r");
        final DQLSelectBuilder dql = orderDescriptionRegistry.buildDQLSelectBuilder().column(property("r"))
            .where(eq(property("r", EnrAbstractExtract.paragraph().order()), commonValue(context.get(EnrCancelParagraphAddEdit.BIND_CANCELLED_ORDER))))
            .order(property("r", EnrAbstractExtract.paragraph().number()))
            .order(property("r", EnrAbstractExtract.number()))
        ;

        orderDescriptionRegistry.applyOrder(dql, input.getEntityOrder());

        List<EnrAbstractExtract> extracts = dql.createStatement(context.getSession()).list();
        Collection<EnrEntrant> entrants = CollectionUtils.collect(extracts, EnrAbstractExtract::getEntrant);

        Collection<EnrAbstractExtract> otherExtracts = EnrEnrollmentParagraphManager.instance().dao().getOtherExtracts(entrants, context.<EnrAbstractParagraph>get(EnrCancelParagraphAddEdit.BIND_PARAGRAPH));
        Map<Long, List<String>> otherExtractMap = SafeMap.get(ArrayList.class);
        for (EnrAbstractExtract extract: otherExtracts)
        {
            otherExtractMap.get(extract.getId()).add(extract.getShortTitle());
        }

        Map<EnrEntrant, List<EnrEntrantCustomState>> customStatesMap = EnrEntrantManager.instance().dao().getActiveCustomStatesMap(entrants, new Date());

        List<DataWrapper> result = new ArrayList<>();
        for (EnrAbstractExtract extract: extracts)
        {
            DataWrapper record = new DataWrapper(extract);
            record.setProperty(OTHER_EXTRACTS, otherExtractMap.get(extract.getId()));
            record.setProperty(CUSTOM_STATES, customStatesMap.get(extract.getEntrant()));
            result.add(record);
        }

        DSOutput output = ListOutputBuilder.get(input, result).pageable(false).build().ordering(new EntityComparator(
            new EntityOrder(EnrRequestedCompetition.request().entrant().person().fullFio(), OrderDirection.asc),
            new EntityOrder(EnrRequestedCompetition.id(), OrderDirection.asc)));

        output.setCountRecord(Math.max(1, output.getRecordList().size()));

        return output;
    }

}

