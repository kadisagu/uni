/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubRequestTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubDataTab.EnrEntrantPubDataTab;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.ActionsAddon.EnrEntrantRequestActionsAddon;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.AddAppend.EnrEntrantRequestAddAppend;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.DirectionListAddon.EnrEntrantRequestDirectionListAddon;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author oleyba
 * @since 4/24/13
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "entrant.id"),
    @Bind(key = "selectedDataTab")
})
public class EnrEntrantPubRequestTabUI extends UIPresenter implements EnrEntrantRequestActionsAddon.IOwner, EnrEntrantRequestDirectionListAddon.IOwner
{
    private EnrEntrant entrant = new EnrEntrant();
    private String selectedDataTab;

    private List<EnrEntrantRequest> entrantRequestList;
    private EnrEntrantRequest currentEntrantRequest;

    private Map<EnrEntrantRequest, StaticListDataSource<EnrRequestedCompetition>> directionDsMap = new HashMap<>();

    private boolean entranceExamIncorrect;
    private boolean profileNotSelected;
    private boolean olympiadNotUsed;
    private boolean examListIncorrect;

    private String updateOnChangeColumns;

    private Map<EnrEntrantRequest, Date> originalHandedInMap = new HashMap<>();

    // EnrEntrantRequestActionsAddon.IOwner

    @Override
    public EnrEntrantRequest getRequestForActions()
    {
        if (getListenerParameterAsLong() != null) {
            IEntity current =  IUniBaseDao.instance.get().get(getListenerParameterAsLong());
            if (current instanceof EnrEntrantRequest) return (EnrEntrantRequest) current;
        }
        return getCurrentEntrantRequest();
    }

    // actions

    @Override
    public void onComponentRefresh()
    {
        setEntrant(IUniBaseDao.instance.get().getNotNull(EnrEntrant.class, getEntrant().getId()));

        setEntranceExamIncorrect(EnrEntrantManager.instance().dao().isEntranceExamIncorrect(getEntrant()));
        setProfileNotSelected(EnrEntrantManager.instance().dao().isProfileNotSelected(getEntrant()));
        setOlympiadNotUsed(EnrEntrantManager.instance().dao().isOlympiadNotUsed(getEntrant()));
        setExamListIncorrect(EnrEntrantManager.instance().dao().isExamListIncorrect(getEntrant()));

        prepareEntrantRequestList();

        getOriginalHandedInMap().clear();
        for (EnrEntrantRequest request : IUniBaseDao.instance.get().getList(EnrEntrantRequest.class, EnrEntrantRequest.entrant(), getEntrant())) {
            if (request.isEduInstDocOriginalHandedIn()) {
                getOriginalHandedInMap().put(request, request.getEduInstDocOriginalRef().getRegistrationDate());
            }
        }
    }

    public void onClickAddEntrantRequest()
    {
        getEntrant().getEnrollmentCampaign().checkOpen();
        _uiActivation.asRegion(EnrEntrantRequestAddAppend.class).top()
        .parameter("entrantId", getEntrant().getId())
        .activate();
    }

    // presenter

    public String getRegionName()
    {
        return EnrEntrantPubDataTab.TAB_PANEL_REGION_NAME;
    }

    public String getRequestRegDate()
    {
        return DateFormatter.DATE_FORMATTER_WITH_TIME.format(getCurrentEntrantRequest().getRegDate());
    }

    @Override
    public boolean isAccessible()
    {
        return getEntrant().isAccessible();
    }

    @Override
    public String getSecPostfix()
    {
        return "enrEntrantRequestPub"; // теперь на карточке абитуриента права должны быть такие же, как на карточке заявления
    }

    public boolean isFirstEntrantRequest()
    {
        return getEntrantRequestList() != null && !getEntrantRequestList().isEmpty() && getEntrantRequestList().get(0).equals(getCurrentEntrantRequest());
    }

    public boolean isOriginalHandedIn() {
        return null != getOriginalHandedInMap().get(getCurrentEntrantRequest());
    }

    public String getRequestTakeAwayDocumentDate() {
        Date date = getCurrentEntrantRequest().getTakeAwayDocumentDate();
        return date == null ? "-" : DateFormatter.DEFAULT_DATE_FORMATTER.format(date);
    }

    public String getRequestOriginalReceivedDate() {
        Date date = getOriginalHandedInMap().get(getCurrentEntrantRequest());
        return date == null ? "-" : DateFormatter.DEFAULT_DATE_FORMATTER.format(date);
    }

    public StaticListDataSource<EnrRequestedCompetition> getCurrentEntrantRequestDirectionDS() {
        return getDirectionDsMap().get(getCurrentEntrantRequest());
    }

    public boolean isDisplayAnyInfo() {
        return isProfileNotSelected() || isOlympiadNotUsed();
    }

    public boolean isDisplayAnyError() {
        return isEntranceExamIncorrect() || isExamListIncorrect();
    }

    // utils

    private void prepareEntrantRequestList()
    {
        setEntrantRequestList(IUniBaseDao.instance.get().getList(EnrEntrantRequest.class, EnrEntrantRequest.entrant(), getEntrant(), EnrEntrantRequest.regDate().s(), EnrEntrantRequest.id().s()));
        setUpdateOnChangeColumns(UniStringUtils.join(getEntrantRequestList(), EnrEntrantRequest.P_ID, ","));

        // датасурсы ВНП
        Map<EnrEntrantRequest, StaticListDataSource<EnrRequestedCompetition>> dsMap = new HashMap<>();
        for (EnrEntrantRequest request : getEntrantRequestList())
            dsMap.put(request, reshreshDirectionDS(request, getDirectionDsMap().get(request)));
        setDirectionDsMap(dsMap);
    }

    private StaticListDataSource<EnrRequestedCompetition> reshreshDirectionDS(EnrEntrantRequest request, StaticListDataSource<EnrRequestedCompetition> dataSource) {
        dataSource = getDirectionListAddon().prepareDataSource(request);
        dataSource.setupRows(IUniBaseDao.instance.get().getList(EnrRequestedCompetition.class, EnrRequestedCompetition.request(), request, EnrRequestedCompetition.priority().s()));
        return dataSource;
    }

    private EnrEntrantRequestDirectionListAddon getDirectionListAddon()
    {
        return (EnrEntrantRequestDirectionListAddon) getConfig().getAddon(EnrEntrantRequestDirectionListAddon.NAME);
    }

    // getters and setters

    @Override
    public EnrEntrant getEntrant()
    {
        return entrant;
    }

    public void setEntrant(EnrEntrant entrant)
    {
        this.entrant = entrant;
    }

    public String getSelectedDataTab()
    {
        return selectedDataTab;
    }

    public void setSelectedDataTab(String selectedDataTab)
    {
        this.selectedDataTab = selectedDataTab;
    }

    public List<EnrEntrantRequest> getEntrantRequestList()
    {
        return entrantRequestList;
    }

    public void setEntrantRequestList(List<EnrEntrantRequest> entrantRequestList)
    {
        this.entrantRequestList = entrantRequestList;
    }

    public EnrEntrantRequest getCurrentEntrantRequest()
    {
        return currentEntrantRequest;
    }

    public void setCurrentEntrantRequest(EnrEntrantRequest currentEntrantRequest)
    {
        this.currentEntrantRequest = currentEntrantRequest;
    }

    public Map<EnrEntrantRequest, Date> getOriginalHandedInMap()
    {
        return originalHandedInMap;
    }

    public Map<EnrEntrantRequest, StaticListDataSource<EnrRequestedCompetition>> getDirectionDsMap()
    {
        return directionDsMap;
    }

    public boolean isEntranceExamIncorrect()
    {
        return entranceExamIncorrect;
    }

    public void setEntranceExamIncorrect(boolean entranceExamIncorrect)
    {
        this.entranceExamIncorrect = entranceExamIncorrect;
    }

    public boolean isOlympiadNotUsed()
    {
        return olympiadNotUsed;
    }

    public void setOlympiadNotUsed(boolean olympiadNotUsed)
    {
        this.olympiadNotUsed = olympiadNotUsed;
    }

    public boolean isProfileNotSelected()
    {
        return profileNotSelected;
    }

    public void setProfileNotSelected(boolean profileNotSelected)
    {
        this.profileNotSelected = profileNotSelected;
    }

    public String getUpdateOnChangeColumns()
    {
        return updateOnChangeColumns;
    }

    public void setUpdateOnChangeColumns(String updateOnChangeColumns)
    {
        this.updateOnChangeColumns = updateOnChangeColumns;
    }

    public void setDirectionDsMap(Map<EnrEntrantRequest, StaticListDataSource<EnrRequestedCompetition>> directionDsMap)
    {
        this.directionDsMap = directionDsMap;
    }

    public boolean isExamListIncorrect()
    {
        return examListIncorrect;
    }

    public void setExamListIncorrect(boolean examListIncorrect)
    {
        this.examListIncorrect = examListIncorrect;
    }
}
