package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
public class MS_unienr14_2x8x1_21to22 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[] {
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrStateExamSubject
        tool.executeUpdate("update enr14_c_st_exam_subj_t set shorttitle_p=? where code_p=?", "ИКТ", "14"); // code = "14", title = "Информатика и ИКТ"
        tool.executeUpdate("update enr14_c_st_exam_subj_t set shorttitle_p=? where code_p=?", "И", "6"); // code = "6", title = "История"
    }
}