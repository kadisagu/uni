/* $Id$ */
package ru.tandemservice.unienr14.competition.bo.EnrProgramSet.util;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

/**
 * Класс-обертка для хранения плана приема подразделенеия.
 *
 * @author azhebko
 * @since 11.04.2014
 */
public class EnrProgramSetOrgUnitWrapper extends IdentifiableWrapper<EnrOrgUnit>
{
    private EnrOrgUnit orgUnit;
    private OrgUnit formativeOrgUnit;
    private EducationOrgUnit educationOrgUnit;
    private int _exclusivePlan;
    private int _targetAdmPlan;
    private int _ministerialPlan;
    private int _contractPlan;

    public EnrProgramSetOrgUnitWrapper(Long id, String title) throws ClassCastException
    {
        super(id, title);
    }

    public EnrProgramSetOrgUnitWrapper(EnrOrgUnit i) throws ClassCastException
    {
        super(i);
        setOrgUnit(i);
    }

    public EnrProgramSetOrgUnitWrapper(EnrProgramSetOrgUnit input)
    {
        this(input.getOrgUnit());
        setFormativeOrgUnit(input.getFormativeOrgUnit());
        setMinisterialPlan(input.getMinisterialPlan());
        setExclusivePlan(input.getExclusivePlan());
        setTargetAdmPlan(input.getTargetAdmPlan());
        setContractPlan(input.getContractPlan());
        setEducationOrgUnit(input.getEducationOrgUnit());
    }

    public int getContractPlan() { return _contractPlan; }
    public void setContractPlan(int contractPlan) { _contractPlan = contractPlan; }
    public int getExclusivePlan() { return _exclusivePlan; }
    public void setExclusivePlan(int exclusivePlan) { _exclusivePlan = exclusivePlan; }
    public int getMinisterialPlan() { return _ministerialPlan; }
    public void setMinisterialPlan(int ministerialPlan) {
        _ministerialPlan = ministerialPlan;
    }
    public int getTargetAdmPlan() { return _targetAdmPlan; }
    public void setTargetAdmPlan(int targetAdmPlan) { _targetAdmPlan = targetAdmPlan; }
    public EnrOrgUnit getOrgUnit() { return orgUnit; }
    public void setOrgUnit(EnrOrgUnit orgUnit) { this.orgUnit = orgUnit; }
    public OrgUnit getFormativeOrgUnit() { return formativeOrgUnit; }
    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit) { this.formativeOrgUnit = formativeOrgUnit; }
    public EducationOrgUnit getEducationOrgUnit() { return educationOrgUnit; }
    public void setEducationOrgUnit(EducationOrgUnit educationOrgUnit) { this.educationOrgUnit = educationOrgUnit; }

    public EnrProgramSetOrgUnit createEntity(EnrProgramSetBase programSet)
    {
        EnrProgramSetOrgUnit ouSet = new EnrProgramSetOrgUnit(programSet, getOrgUnit());
        ouSet.setFormativeOrgUnit(getFormativeOrgUnit());
        ouSet.setMinisterialPlan(getMinisterialPlan());
        ouSet.setContractPlan(getContractPlan());
        ouSet.setExclusivePlan(getExclusivePlan());
        ouSet.setTargetAdmPlan(getTargetAdmPlan());
        ouSet.setEducationOrgUnit(getEducationOrgUnit());
        return ouSet;
    }

    public EnrProgramSetOrgUnitWrapper getCopy()
    {
        EnrProgramSetOrgUnitWrapper wrapper = new EnrProgramSetOrgUnitWrapper(getOrgUnit());
        wrapper.setFormativeOrgUnit(getFormativeOrgUnit());
        wrapper.setContractPlan(getContractPlan());
        wrapper.setMinisterialPlan(getMinisterialPlan());
        wrapper.setTargetAdmPlan(getTargetAdmPlan());
        wrapper.setExclusivePlan(getExclusivePlan());
        wrapper.setEducationOrgUnit(getEducationOrgUnit());
        return wrapper;
    }
}
