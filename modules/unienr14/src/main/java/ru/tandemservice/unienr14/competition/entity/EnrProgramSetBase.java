package ru.tandemservice.unienr14.competition.entity;

import org.tandemframework.core.common.IEntityDebugTitled;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.unienr14.competition.entity.gen.EnrProgramSetBaseGen;

/**
 * Набор ОП для приема
 *
 * Определяет направление (проф., спец.) и перечень ОП, на который формируются конкурсы. Базовый класс.
 */
public abstract class EnrProgramSetBase extends EnrProgramSetBaseGen implements ITitled, IEntityDebugTitled
{
    public interface ISingleExamSetOwner {
        EnrExamSetVariant getExamSetVariant();
    }

    public abstract boolean isMinisterialDividedByEduLevel();
    public abstract boolean isContractDividedByEduLevel();

    public abstract boolean isValidExamSetVariants();

    @Override public String getEntityDebugTitle() {
        return getTitle();
    }

    public EduProgramKind getProgramKind() {
        return getProgramSubject().getSubjectIndex().getProgramKind();
    }

    public boolean isGreaterThan2014()
    {
        return getEnrollmentCampaign().isGreaterThan2014();
    }

    public boolean isLessOrEqualThan2014()
    {
        return getEnrollmentCampaign().isLessOrEqualThan2014();
    }

    public static EntityComboDataSourceHandler defaultSelectDSHandler(String name)
    {
        return new EntityComboDataSourceHandler(name, EnrProgramSetBase.class)
                .titleProperty(EnrProgramSetBase.P_TITLE)
                .filter(EnrProgramSetBase.title())
                .order(EnrProgramSetBase.title());
    }
}