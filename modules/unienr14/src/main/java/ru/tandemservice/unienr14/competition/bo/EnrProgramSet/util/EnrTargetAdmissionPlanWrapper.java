/* $Id$ */
package ru.tandemservice.unienr14.competition.bo.EnrProgramSet.util;

import org.tandemframework.core.entity.IdentifiableWrapper;
import ru.tandemservice.unienr14.competition.entity.EnrTargetAdmissionPlan;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignTargetAdmissionKind;

import java.util.HashMap;
import java.util.Map;

/**
 * План приема по виду ЦП.
 * @author azhebko
 * @since 05.05.2014
 */
public class EnrTargetAdmissionPlanWrapper extends IdentifiableWrapper<EnrCampaignTargetAdmissionKind>
{
    private EnrCampaignTargetAdmissionKind kind;
    private Integer _plan;
    private Map<Long, Integer> competitionPlanMap = new HashMap<>();
    private Map<Long, Integer> competitionCalcPlanMap = new HashMap<>();

    public EnrTargetAdmissionPlanWrapper(EnrCampaignTargetAdmissionKind kind) {
        super(kind);
        this.kind = kind;
    }

    public EnrTargetAdmissionPlanWrapper(EnrTargetAdmissionPlan plan)
    {
        this(plan.getEnrCampaignTAKind());
        setPlan(plan.getPlan());
    }

    public EnrCampaignTargetAdmissionKind getKind() { return kind; }
    public void setKind(EnrCampaignTargetAdmissionKind kind) { this.kind = kind; }

    public Integer getPlan(){ return _plan; }
    public void setPlan(Integer plan){ _plan = plan; }

    public Map<Long, Integer> getCompetitionCalcPlanMap()
    {
        return competitionCalcPlanMap;
    }

    public Map<Long, Integer> getCompetitionPlanMap()
    {
        return competitionPlanMap;
    }

    public Integer getPlan(Long currentTaCompetition)
    {
        return competitionPlanMap.get(currentTaCompetition);
    }

    public Integer getCalculatedPlan(Long currentTaCompetition)
    {
        return competitionCalcPlanMap.get(currentTaCompetition);
    }
}