/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.select;

import org.hibernate.Session;
import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;

/**
 * @author oleyba
 * @since 4/2/15
 */
public interface IEnrEnrollmentStepItem extends IEntity
{
    public EnrRatingItem getEntity();

    boolean isIncluded();

    boolean isEntrantArchived();

    boolean isEntrantRequestTakeAwayDocuments();

    boolean isOriginalIn();

    boolean isAccepted();

    boolean isEnrollmentAvailable();

    boolean isRecommended();

    boolean isShouldBeEnrolled();
}
