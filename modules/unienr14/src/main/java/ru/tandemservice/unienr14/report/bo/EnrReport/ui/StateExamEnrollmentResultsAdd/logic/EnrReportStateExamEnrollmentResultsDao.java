/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.StateExamEnrollmentResultsAdd.logic;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfRowIntercepterRawText;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.RtfText;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2009;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2013;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectOkso;
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType;
import ru.tandemservice.unienr14.catalog.entity.EnrDiscipline;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrExamPassFormCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.StateExamEnrollmentResultsAdd.EnrReportStateExamEnrollmentResultsAddUI;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportStageSelector;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportUtil;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.FilterParametersPrinter;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder;
import ru.tandemservice.unienr14.report.entity.EnrReportStateExamEnrollmentResults;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 22.07.2014
 */
public class EnrReportStateExamEnrollmentResultsDao extends UniBaseDao implements IEnrReportStateExamEnrollmentResultsDao {

    @Override
    public long createReport(EnrReportStateExamEnrollmentResultsAddUI model) {

        EnrReportStateExamEnrollmentResults report = new EnrReportStateExamEnrollmentResults();

        report.setFormingDate(new Date());

        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateSelector().getDateFrom());
        report.setDateTo(model.getDateSelector().getDateTo());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, EnrReportStateExamEnrollmentResults.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE, EnrReportStateExamEnrollmentResults.P_COMPENSATION_TYPE, "shortTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, EnrReportStateExamEnrollmentResults.P_PROGRAM_FORM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPETITION_TYPE, EnrReportStateExamEnrollmentResults.P_COMPETITION_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, EnrReportStateExamEnrollmentResults.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, EnrReportStateExamEnrollmentResults.P_FORMATIVE_ORG_UNIT, "fullTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, EnrReportStateExamEnrollmentResults.P_PROGRAM_SUBJECT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, EnrReportStateExamEnrollmentResults.P_EDU_PROGRAM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, EnrReportStateExamEnrollmentResults.P_PROGRAM_SET, "title");
        if (model.getParallelSelector().isParallelActive())
            report.setParallel(model.getParallelSelector().getParallel().getTitle());

        DatabaseFile content = new DatabaseFile();

        content.setContent(buildReport(model));
        content.setFilename("EnrReportOrgUnitEnrollmentResults.rtf");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        save(content);

        report.setContent(content);
        save(report);

        return report.getId();

    }

    private byte[] buildReport(EnrReportStateExamEnrollmentResultsAddUI model){

        // rtf
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(EnrScriptItem.class, EnrScriptItemCodes.REPORT_STATE_EXAM_ENROLLMENT_RESULTS);
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());


        // Фильтры
        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder()
                .notArchiveOnly()
                .filter(model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo())
                .filter(model.getEnrollmentCampaign());
        filterAddon.applyFilters(requestedCompDQL, requestedCompDQL.competition());

        if (model.getParallelSelector().isParallelActive())
        {
            if (model.getParallelSelector().isParallelOnly())
                requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.TRUE)));
            if (model.getParallelSelector().isSkipParallel())
                requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.FALSE)));
        }

        EnrReportStageSelector.enrollmentResultsStageFilter().applyFilter(requestedCompDQL, "reqComp");

        requestedCompDQL
                .column(property("reqComp"));
        List<EnrRequestedCompetition> fromQueryReqComps = getList(requestedCompDQL);


        requestedCompDQL
                .resetColumns()
                .joinEntity("reqComp", DQLJoinType.inner, EnrChosenEntranceExam.class, "vvi", eq(property(EnrChosenEntranceExam.requestedCompetition().fromAlias("vvi")), property("reqComp")))
                .column(property("vvi"))
                .where(eq(property(EnrChosenEntranceExam.maxMarkForm().passForm().code().fromAlias("vvi")), value(EnrExamPassFormCodes.STATE_EXAM)));

        List<EnrChosenEntranceExam> fromQueryVviList = getList(requestedCompDQL);

        Comparator<EduProgramSubject> groupComparator = new Comparator<EduProgramSubject>() {
            @Override
            public int compare(EduProgramSubject o1, EduProgramSubject o2) {

                // сначала 2013 года
                int res = Boolean.compare(o1 instanceof EduProgramSubject2013, o2 instanceof EduProgramSubject2013);
                if (res != 0) return -res;

                // затем 2009
                res = Boolean.compare(o1 instanceof EduProgramSubject2009, o2 instanceof EduProgramSubject2009);
                if (res != 0) return -res;

                // затем 2005 (оксо)
                res = Boolean.compare(o1 instanceof EduProgramSubjectOkso, o2 instanceof EduProgramSubjectOkso);
                if (res != 0) return -res;

                String o1KindCode = o1.getEduProgramKind().getCode().equals(EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV) ? EduProgramKindCodes.PROGRAMMA_BAKALAVRIATA : o1.getEduProgramKind().getCode();
                String o2KindCode = o2.getEduProgramKind().getCode().equals(EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV) ? EduProgramKindCodes.PROGRAMMA_BAKALAVRIATA : o2.getEduProgramKind().getCode();
                res = o1KindCode.compareTo(o2KindCode);
                if (res != 0) return res;

                res = o1.getSubjectIndex().getPriority() - o2.getSubjectIndex().getPriority();
                if (res != 0) return res;

                res = o1.getSubjectIndex().getCode().compareTo(o2.getSubjectIndex().getCode());
                if (res != 0) return res;

                return o1.getGroupTitle().compareToIgnoreCase(o2.getGroupTitle());
            }
        };

        Comparator<EduProgramSubject> subjectComparator = new Comparator<EduProgramSubject>() {
            @Override
            public int compare(EduProgramSubject o1, EduProgramSubject o2) {
                return o1.getTitleWithCode().compareToIgnoreCase(o2.getTitleWithCode());
            }
        };




        Map<EduProgramSubject, Map<EduProgramSubject, Map<EnrProgramSetBase, Map<EnrRequestedCompetition, Set<EnrChosenEntranceExam>>>>> dataMap = new TreeMap<>(groupComparator);
        Set<EnrProgramSetBase> programSets = new HashSet<>();
        Set<EnrRequestedCompetition> filledCompetitions = new HashSet<>();

        for (EnrChosenEntranceExam exam : fromQueryVviList) {
            EduProgramSubject subject = exam.getRequestedCompetition().getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramSubject();
            EnrProgramSetBase programSet = exam.getRequestedCompetition().getCompetition().getProgramSetOrgUnit().getProgramSet();

            programSets.add(programSet);
            filledCompetitions.add(exam.getRequestedCompetition());

            Map<EduProgramSubject, Map<EnrProgramSetBase, Map<EnrRequestedCompetition, Set<EnrChosenEntranceExam>>>> eduProgramSubjectMap = SafeMap.safeGet(dataMap, subject, TreeMap.class, subjectComparator);
            Map<EnrProgramSetBase, Map<EnrRequestedCompetition, Set<EnrChosenEntranceExam>>> enrProgramSetBaseMap = SafeMap.safeGet(eduProgramSubjectMap, subject, TreeMap.class, ITitled.TITLED_COMPARATOR);
            Map<EnrRequestedCompetition, Set<EnrChosenEntranceExam>> enrRequestedCompetitionMap = SafeMap.safeGet(enrProgramSetBaseMap, programSet, HashMap.class);
            Set<EnrChosenEntranceExam> exams = SafeMap.safeGet(enrRequestedCompetitionMap, exam.getRequestedCompetition(), HashSet.class);
            exams.add(exam);

        }

        for (EnrRequestedCompetition reqComp : fromQueryReqComps) {
            if (filledCompetitions.contains(reqComp))
                continue;
            EduProgramSubject subject = reqComp.getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramSubject();
            EnrProgramSetBase programSet = reqComp.getCompetition().getProgramSetOrgUnit().getProgramSet();

            Map<EduProgramSubject, Map<EnrProgramSetBase, Map<EnrRequestedCompetition, Set<EnrChosenEntranceExam>>>> eduProgramSubjectMap = SafeMap.safeGet(dataMap, subject, TreeMap.class, subjectComparator);
            Map<EnrProgramSetBase, Map<EnrRequestedCompetition, Set<EnrChosenEntranceExam>>> enrProgramSetBaseMap = SafeMap.safeGet(eduProgramSubjectMap, subject, TreeMap.class, ITitled.TITLED_COMPARATOR);
            Map<EnrRequestedCompetition, Set<EnrChosenEntranceExam>> enrRequestedCompetitionMap = SafeMap.safeGet(enrProgramSetBaseMap, programSet, HashMap.class);
            Set<EnrChosenEntranceExam> exams = SafeMap.safeGet(enrRequestedCompetitionMap, reqComp, HashSet.class);
            programSets.add(programSet);
        }



        // Для формирования второй колонки нужно знать количество ОП в наборе. Формируем карту набор ОП - сет ОП, входящих в набор.
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EnrProgramSetItem.class, "programSetItem")
                .where(in(property(EnrProgramSetItem.programSet().fromAlias("programSetItem")), programSets))
                .column(property("programSetItem"));

        List<EnrProgramSetItem> fromQueryProgramSetItems = getList(builder);

        Map<EnrProgramSetBase, Set<EnrProgramSetItem>> programSetItemsMap = SafeMap.get(HashSet.class);

        for (EnrProgramSetItem programSetItem : fromQueryProgramSetItems) {
            programSetItemsMap.get(programSetItem.getProgramSet()).add(programSetItem);
        }

        NumberFormat formatter = new DecimalFormat("#0.00");

        boolean budget = ((CompensationType)model.getCompetitionFilterAddon().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE).getValue()).isBudget();


        List<String[]> tTable = new ArrayList<>();

        for (Map.Entry<EduProgramSubject, Map<EduProgramSubject, Map<EnrProgramSetBase, Map<EnrRequestedCompetition, Set<EnrChosenEntranceExam>>>>> groupEntry : dataMap.entrySet())
        {
            Map<EnrEntrant, Map<EnrRequestedCompetition, Map<EnrDiscipline, Map<EnrCompetitionType, EnrChosenEntranceExam>>>> groupUniqueResultsMap = new HashMap<>();

            List<String[]> groupTable = new ArrayList<>();

            for (Map.Entry<EduProgramSubject, Map<EnrProgramSetBase,Map<EnrRequestedCompetition, Set<EnrChosenEntranceExam>>>> subjectEntry : groupEntry.getValue().entrySet())
            {
                Map<EnrEntrant, Map<EnrRequestedCompetition, Map<EnrDiscipline, Map<EnrCompetitionType, EnrChosenEntranceExam>>>> subjectUniqueResultsMap = new HashMap<>();

                List<String[]> subjectTable = new ArrayList<>();

                for (Map.Entry<EnrProgramSetBase, Map<EnrRequestedCompetition, Set<EnrChosenEntranceExam>>> programSetEntry : subjectEntry.getValue().entrySet()) {

                    Map<EnrEntrant, Map<EnrRequestedCompetition, Map<EnrDiscipline, Map<EnrCompetitionType, EnrChosenEntranceExam>>>> uniqueResultsMap = new HashMap<>();

                    for (Map.Entry<EnrRequestedCompetition, Set<EnrChosenEntranceExam>> reqCompEntry : programSetEntry.getValue().entrySet()) {

                        EnrRequestedCompetition reqComp = reqCompEntry.getKey();
                        EnrEntrant entrant = reqComp.getRequest().getEntrant();

                        Map<EnrRequestedCompetition, Map<EnrDiscipline, Map<EnrCompetitionType, EnrChosenEntranceExam>>> uniqueEnrRequestedCompetitionMap = SafeMap.safeGet(uniqueResultsMap, entrant, HashMap.class);
                        Map<EnrRequestedCompetition, Map<EnrDiscipline, Map<EnrCompetitionType, EnrChosenEntranceExam>>> subjEnrRequestedCompetitionMap = SafeMap.safeGet(subjectUniqueResultsMap, entrant, HashMap.class);
                        Map<EnrRequestedCompetition, Map<EnrDiscipline, Map<EnrCompetitionType, EnrChosenEntranceExam>>> groupEnrRequestedCompetitionMap = SafeMap.safeGet(groupUniqueResultsMap, entrant, HashMap.class);

                        Map<EnrDiscipline, Map<EnrCompetitionType, EnrChosenEntranceExam>> uniqueEnrDisciplineMapMap = SafeMap.safeGet(uniqueEnrRequestedCompetitionMap, reqComp, HashMap.class);
                        Map<EnrDiscipline, Map<EnrCompetitionType, EnrChosenEntranceExam>> subjEnrDisciplineMap = SafeMap.safeGet(subjEnrRequestedCompetitionMap, reqComp, HashMap.class);
                        Map<EnrDiscipline, Map<EnrCompetitionType, EnrChosenEntranceExam>> groupEnrDisciplineMap = SafeMap.safeGet(groupEnrRequestedCompetitionMap, reqComp, HashMap.class);


                        for (EnrChosenEntranceExam exam : reqCompEntry.getValue()) {


                            EnrDiscipline discipline = exam.getDiscipline().getDiscipline();
                            EnrCompetitionType competitionType = exam.getRequestedCompetition().getCompetition().getType();

                            // на ОП
                            Map<EnrCompetitionType, EnrChosenEntranceExam> uniqueCompetitionTypeMap = SafeMap.safeGet(uniqueEnrDisciplineMapMap, discipline, HashMap.class);
                            if (!uniqueCompetitionTypeMap.containsKey(competitionType))
                                uniqueCompetitionTypeMap.put(competitionType, exam);

                            // на направление
                            Map<EnrCompetitionType, EnrChosenEntranceExam> subjCompetitionTypeMap = SafeMap.safeGet(subjEnrDisciplineMap, discipline, HashMap.class);
                            if (!subjCompetitionTypeMap.containsKey(competitionType))
                                subjCompetitionTypeMap.put(competitionType, exam);

                            // на группу
                            Map<EnrCompetitionType, EnrChosenEntranceExam> groupEnrCompetitionTypeMap = SafeMap.safeGet(groupEnrDisciplineMap, discipline, HashMap.class);
                            if (!groupEnrCompetitionTypeMap.containsKey(competitionType))
                                groupEnrCompetitionTypeMap.put(competitionType, exam);


                        }




                    }

                    EnrProgramSetBase programSet = programSetEntry.getKey();

                    List<String> row = new ArrayList<>();
                    if (programSetItemsMap.get(programSet).size() > 1)
                        row.add(programSet.getTitle());
                    else for (EnrProgramSetItem enrProgramSetItem : programSetItemsMap.get(programSet)) {
                        row.add(enrProgramSetItem.getProgram().getTitleAndConditionsShort());
                    }
                    for (Double value : countValues(uniqueResultsMap, budget)) {
                        if (value != 0) {
                            if ((value.longValue()) < value)
                                row.add(formatter.format(value));
                            else row.add(String.valueOf(value.intValue()));
                        } else row.add("");
                    }

                    subjectTable.add(row.toArray(new String[row.size()]));

                }

                List<String> row = new ArrayList<>();
                row.add("\\cf1 " + subjectEntry.getKey().getTitleWithCode());
                for (Double value : countValues(subjectUniqueResultsMap, budget)) {
                    if (value != 0) {
                        if ((value.longValue()) < value)
                            row.add("\\cf1 " + formatter.format(value));
                        else row.add(String.valueOf("\\cf1 " + value.intValue()));
                    } else row.add("");
                }

                groupTable.add(row.toArray(new String[row.size()]));
                groupTable.addAll(subjectTable);


            }
            List<String> row = new ArrayList<>();
            row.add("\\cf1\\b " + groupEntry.getKey().getGroupTitle());

            for (Double value : countValues(groupUniqueResultsMap, budget)) {
                if (value != 0) {
                    if ((value.longValue()) < value)
                        row.add("\\cf1\\b " + formatter.format(value));
                    else row.add("\\cf1\\b " + String.valueOf(value.intValue()));
                } else row.add("");
            }

            tTable.add(row.toArray(new String[row.size()]));
            tTable.addAll(groupTable);
        }

        RtfTableModifier modifier = new RtfTableModifier();

        FilterParametersPrinter filterParametersPrinter = new FilterParametersPrinter();
        filterParametersPrinter.setParallelSelector(model.getParallelSelector());
        List<String[]> hTable = filterParametersPrinter.getTable(filterAddon, model.getDateSelector());

        modifier.put("T", tTable.toArray(new String[tTable.size()][]));
        modifier.put("H", hTable.toArray(new String[hTable.size()][]));
        modifier.put("H", new RtfRowIntercepterRawText());

        modifier.put("T", new RtfRowIntercepterRawText());


        RtfText text = new RtfText();
        text.setString("{\\colortbl;\\red150\\green150\\blue150;}");
        text.setRaw(true);
        document.getElementList().add(0, text);

        modifier.modify(document);

        new RtfInjectModifier().put("year", DateFormatter.DATE_FORMATTER_JUST_YEAR.format(new Date())).modify(document);

        return RtfUtil.toByteArray(document);
    }


    private Double countAverage(List<Double> list)
    {
        Double average = new Double(0);
        if (!list.isEmpty()) {
            int count = list.size();
            for (Double value : list)
                if (value != 0)
                    average += value;
                else count--;

            if (count != 0)
                average = average / count;
            else return 0.0;
        }

        return average;
    }

    private List<Double> countValues(Map<EnrEntrant, Map<EnrRequestedCompetition, Map<EnrDiscipline, Map<EnrCompetitionType, EnrChosenEntranceExam>>>> uniqueResultsMap, boolean budget)
    {

        List<Double> commonCompetitionMarks = new ArrayList<>();
        List<Double> exclusiveCompetitionMarks = new ArrayList<>();
        List<Double> targetAdmissionMarks = new ArrayList<>();
        List<Double> benefitCompetitionMarks = new ArrayList<>();

        Set<EnrRequestedCompetition> totalReqComps = new HashSet<>();
        Set<EnrRequestedCompetition> noExamReqComps = new HashSet<>();
        Set<EnrRequestedCompetition> exclusiveReqComps = new HashSet<>();
        Set<EnrRequestedCompetition> targetAdmReqComps = new HashSet<>();
        Set<EnrRequestedCompetition> benefitReqComps = new HashSet<>();

        String commonCode = budget ? EnrCompetitionTypeCodes.MINISTERIAL : EnrCompetitionTypeCodes.CONTRACT;
        String exclusiveCode = EnrCompetitionTypeCodes.EXCLUSIVE;
        String targetCode = EnrCompetitionTypeCodes.TARGET_ADMISSION;
        String noExamKCPCode = EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL;
        String noExamContractCode = EnrCompetitionTypeCodes.NO_EXAM_CONTRACT;

        for (Map.Entry<EnrEntrant, Map<EnrRequestedCompetition, Map<EnrDiscipline, Map<EnrCompetitionType, EnrChosenEntranceExam>>>> entrantEntry : uniqueResultsMap.entrySet())
        {

            for (Map.Entry<EnrRequestedCompetition, Map<EnrDiscipline, Map<EnrCompetitionType, EnrChosenEntranceExam>>> reqCompEntry : entrantEntry.getValue().entrySet())
            {
                EnrRequestedCompetition reqComp = reqCompEntry.getKey();

                if (reqComp.getRequest().getBenefitCategory() != null) {
                    benefitReqComps.add(reqComp);
                }
                if (reqComp.getCompetition().getType().getCode().equals(exclusiveCode)) {
                    exclusiveReqComps.add(reqComp);
                } else if (reqComp.getCompetition().getType().getCode().equals(targetCode)) {
                    targetAdmReqComps.add(reqComp);
                } else if (reqComp.getCompetition().getType().getCode().equals(commonCode)) {
                }
                if (reqComp.getCompetition().getType().getCode().equals(noExamContractCode) || reqComp.getCompetition().getType().getCode().equals(noExamKCPCode))
                    noExamReqComps.add(reqComp);
                totalReqComps.add(reqComp);

                for (Map.Entry<EnrDiscipline, Map<EnrCompetitionType, EnrChosenEntranceExam>> disciplineEntry : reqCompEntry.getValue().entrySet()) {

                    for (Map.Entry<EnrCompetitionType, EnrChosenEntranceExam> typeEntry : disciplineEntry.getValue().entrySet()) {
                        EnrChosenEntranceExam exam = typeEntry.getValue();


                        if (reqComp.getRequest().getBenefitCategory() != null) {
                            benefitCompetitionMarks.add(exam.getMarkAsDouble());
                            benefitReqComps.add(reqComp);
                        }

                        if (reqComp.getCompetition().getType().getCode().equals(exclusiveCode)) {
                            exclusiveCompetitionMarks.add(exam.getMarkAsDouble());
                            exclusiveReqComps.add(reqComp);
                        } else if (reqComp.getCompetition().getType().getCode().equals(targetCode)) {
                            targetAdmissionMarks.add(exam.getMarkAsDouble());
                            targetAdmReqComps.add(reqComp);
                        } else if (reqComp.getCompetition().getType().getCode().equals(commonCode)) {
                            commonCompetitionMarks.add(exam.getMarkAsDouble());
                        }
                    }
                }
            }
        }

        List<Double> result = new ArrayList<>();

        result.add(countAverage(commonCompetitionMarks));
        result.add(countAverage(exclusiveCompetitionMarks));
        result.add(countAverage(targetAdmissionMarks));
        result.add(countAverage(benefitCompetitionMarks));

        result.add(Double.valueOf(totalReqComps.size()));
        result.add(Double.valueOf(noExamReqComps.size()));
        result.add(Double.valueOf(exclusiveReqComps.size()));
        result.add(Double.valueOf(targetAdmReqComps.size()));
        result.add(Double.valueOf(benefitReqComps.size()));

        return result;
    }

}
