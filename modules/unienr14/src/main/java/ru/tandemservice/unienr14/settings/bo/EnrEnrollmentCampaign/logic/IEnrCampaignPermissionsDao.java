package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.List;

/**
 * dao для работы с приемной кампанией и ее настройками
 *
 * @author nvankov
 * @since 3/18/16
 */
public interface IEnrCampaignPermissionsDao extends INeedPersistenceSupport
{

    /**
     * Список id ПК, на которые есть глобальные или локальные права у пользовательского контекста
     *
     * @param principalContext Контекст, под которым в данный момент выступает пользователь
     */
    List<Long> getEnrollmentCampaignIds(IPrincipalContext principalContext);

    /**
     * Список ПК, на которые есть глобальные или локальные права у пользовательского контекста
     *
     * @param principalContext Контекст, под которым в данный момент выступает пользователь
     */
    List<EnrEnrollmentCampaign> getEnrollmentCampaignList(IPrincipalContext principalContext);

    /**
     * Модель для выбора приемной кампании
     * Для совместимости со старыми компонентами
     * Для caf пользуйтесь
     *
     * @return модель для выбора ПК
     * @see ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager#enrCampaignDSConfig()
     */
    ISelectModel getEnrollmentCampaignModel();

    /**
     * true, если у пользовательского контекста есть глобальнок или локальное право на ПК
     *
     * @param enrollmentCampaign ПК
     * @param principalContext Контекст, под которым в данный момент выступает пользователь
     */
    boolean hasPermissionForEnrCampaign(EnrEnrollmentCampaign enrollmentCampaign, IPrincipalContext principalContext);
}
