/* $Id:$ */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.ui.SecTab;

import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.View.OrgUnitViewUI;
import org.tandemframework.shared.organization.sec.bo.Sec.ui.AccessMatrixEdit.SecAccessMatrixEditUI;
import org.tandemframework.shared.organization.sec.bo.Sec.util.BasePermissionGroupListUI;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.unienr14.catalog.entity.codes.LocalRoleScopeCodes;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.ui.AccessMatrixEdit.EnrEnrollmentCommissionAccessMatrixEdit;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.ui.AccessMatrixEdit.EnrEnrollmentCommissionAccessMatrixEditUI;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.ui.LocalRoleAddEdit.EnrEnrollmentCommissionLocalRoleAddEdit;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.ui.LocalRoleAddEdit.EnrEnrollmentCommissionLocalRoleAddEditUI;

import java.util.Map;

/**
 * @author oleyba
 * @since 5/19/15
 */
@Input({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "enrollmentCommission.id"),
    @Bind(key = OrgUnitViewUI.SELECTED_SUB_PAGE, binding = "selectedSubPage")
})
public class EnrEnrollmentCommissionSecTabUI extends BasePermissionGroupListUI
{
    public static final String TITLE = "title";
    public static final String ROLE_STATE = "roleState";

    private EnrEnrollmentCommission _enrollmentCommission = new EnrEnrollmentCommission();
    private String selectedSubPage;

    @Override
    protected String getLocalRoleScopeCode()
    {
        return LocalRoleScopeCodes.ENROLLMENT_COMMISSION;
    }

    @Override
    public String getAccessMatrixBC()
    {
        return EnrEnrollmentCommissionAccessMatrixEdit.class.getSimpleName();
    }

    @Override
    public void onComponentRefresh()
    {
        setEnrollmentCommission(DataAccessServices.dao().getNotNull(EnrEnrollmentCommission.class, getEnrollmentCommission().getId()));
        super.onComponentRefresh();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(LocalRoleDSHandler.EC_ID, getEnrollmentCommission().getId());
        dataSource.put(LocalRoleDSHandler.TITLE, _uiSettings.get(TITLE));
        dataSource.put(LocalRoleDSHandler.ROLE_STATE, _uiSettings.get(ROLE_STATE));
    }

    // Util

    @Override
    public Map<String, Object> getGroupLinkParams()
    {
        return ParametersMap
            .createWith(EnrEnrollmentCommissionAccessMatrixEditUI.BIND_ENROLLMENT_COMMISSION_ID, getEnrollmentCommission().getId())
            .add(SecAccessMatrixEditUI.BIND_GROUP_NAME, getCurrentGroup().getName());
    }

    // Listeners

    public void onClickAddLocalRole()
    {
        _uiActivation.asRegionDialog(EnrEnrollmentCommissionLocalRoleAddEdit.class)
            .parameter(EnrEnrollmentCommissionLocalRoleAddEditUI.ENROLLMENT_COMMISSION_ID, getEnrollmentCommission().getId())
            .activate();
    }

    // Getters & Setters


    public String getAddLocalRolePermissionKey()
    {
        return "permissionEnr14Commissions";
    }

    public String getEditPermissionKey()
    {
        return "permissionEnr14Commissions";
    }

    public String getDeletePermissionKey()
    {
        return "permissionEnr14Commissions";
    }

    public String getSelectedSubPage()
    {
        return selectedSubPage;
    }

    public void setSelectedSubPage(String selectedSubPage)
    {
        this.selectedSubPage = selectedSubPage;
    }

    public EnrEnrollmentCommission getEnrollmentCommission()
    {
        return _enrollmentCommission;
    }

    public void setEnrollmentCommission(EnrEnrollmentCommission enrollmentCommission)
    {
        _enrollmentCommission = enrollmentCommission;
    }
}
