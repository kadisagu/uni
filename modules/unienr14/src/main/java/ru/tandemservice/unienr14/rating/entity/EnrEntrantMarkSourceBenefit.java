package ru.tandemservice.unienr14.rating.entity;

import org.tandemframework.core.common.ITitled;

import ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm;
import ru.tandemservice.unienr14.rating.entity.gen.EnrEntrantMarkSourceBenefitGen;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.IEnrEntrantSingleProofBenefitStatement;

/**
 * Основание балла (особое право)
 *
 * Создается при зачтении документа, предоставляющего особое право за 100 баллов по ВВИ, с учетом формы.
 * Обновляется демоном IEnrRatingDaemonDao#doRefreshSource4Benefit (обновление баллов по особым правам).
 */
public class EnrEntrantMarkSourceBenefit extends EnrEntrantMarkSourceBenefitGen implements ITitled, IEnrEntrantSingleProofBenefitStatement
{
    public EnrExamPassForm getPassForm() {
        return getChosenEntranceExamForm().getPassForm();
    }

    @Override
    public String getTitle()
    {
        if (getChosenEntranceExamForm() == null) {
            return this.getClass().getSimpleName();
        }
        return "Зачтение за 100 баллов (" + getChosenEntranceExamForm().getChosenEntranceExam().getDiscipline().getTitle() + ", " + getMainProof().getDisplayableTitle() + ")";
    }

    @Override
    public String getInfoString() {
        return "100 — особое право (" + getMainProof().getExtendedTitle() + ")";
    }

    @Override
    public String getDocumentTitleForPrint() {
        return getMainProof().getTitle();
    }

    @Override
    public String getBenefitStatementTitle() {
        final EnrChosenEntranceExam chosenEntranceExam = getChosenEntranceExamForm().getChosenEntranceExam();
        return
        "Конкурс: "+chosenEntranceExam.getRequestedCompetition().getTitle() + "\n" +
        "ВВИ: "+chosenEntranceExam.getDiscipline().getTitle();
    }

    @Override
    public EnrEntrantRequest getRequest() {
        return getChosenEntranceExamForm().getChosenEntranceExam().getRequestedCompetition().getRequest();
    }
}