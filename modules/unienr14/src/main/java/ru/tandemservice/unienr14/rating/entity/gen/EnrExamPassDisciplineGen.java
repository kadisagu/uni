package ru.tandemservice.unienr14.rating.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unienr14.catalog.entity.EnrAbsenceNote;
import ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroup;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Дисциплина для сдачи
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrExamPassDisciplineGen extends EntityBase
 implements INaturalIdentifiable<EnrExamPassDisciplineGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline";
    public static final String ENTITY_NAME = "enrExamPassDiscipline";
    public static final int VERSION_HASH = -1757737501;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTRANT = "entrant";
    public static final String L_DISCIPLINE = "discipline";
    public static final String L_PASS_FORM = "passForm";
    public static final String P_RETAKE = "retake";
    public static final String P_MODIFICATION_DATE = "modificationDate";
    public static final String L_ABSENCE_NOTE = "absenceNote";
    public static final String P_MARK_DATE = "markDate";
    public static final String P_MARK_AS_LONG = "markAsLong";
    public static final String L_TERRITORIAL_ORG_UNIT = "territorialOrgUnit";
    public static final String L_EXAM_GROUP = "examGroup";
    public static final String P_EXAM_GROUP_SET_DATE = "examGroupSetDate";
    public static final String P_EXAM_GROUP_POSITION = "examGroupPosition";
    public static final String P_CODE = "code";
    public static final String P_DISCIPLINE_AND_PASS_FORM_TITLE = "disciplineAndPassFormTitle";
    public static final String P_EXAM_RESULT = "examResult";
    public static final String P_EXAM_RESULT_FULL = "examResultFull";
    public static final String P_EXAM_SCHEDULE_TITLE = "examScheduleTitle";
    public static final String P_MARK_AS_DOUBLE = "markAsDouble";
    public static final String P_MARK_AS_STRING = "markAsString";
    public static final String P_SHORT_EXAM_RESULT = "shortExamResult";
    public static final String P_TITLE = "title";

    private EnrEntrant _entrant;     // Абитуриент
    private EnrCampaignDiscipline _discipline;     // Дисциплина ПК
    private EnrExamPassForm _passForm;     // Форма сдачи
    private boolean _retake;     // Пересдача
    private Date _modificationDate;     // Дата изменения
    private EnrAbsenceNote _absenceNote;     // Отметка о неявке
    private Date _markDate;     // Дата выставления оценки
    private Long _markAsLong;     // Балл
    private OrgUnit _territorialOrgUnit;     // Территориальное подразделение
    private EnrExamGroup _examGroup;     // ЭГ
    private Date _examGroupSetDate;     // Дата включения в ЭГ
    private Integer _examGroupPosition;     // Номер в ЭГ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     */
    @NotNull
    public EnrEntrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant Абитуриент. Свойство не может быть null.
     */
    public void setEntrant(EnrEntrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    /**
     * @return Дисциплина ПК. Свойство не может быть null.
     */
    @NotNull
    public EnrCampaignDiscipline getDiscipline()
    {
        return _discipline;
    }

    /**
     * @param discipline Дисциплина ПК. Свойство не может быть null.
     */
    public void setDiscipline(EnrCampaignDiscipline discipline)
    {
        dirty(_discipline, discipline);
        _discipline = discipline;
    }

    /**
     * @return Форма сдачи. Свойство не может быть null.
     */
    @NotNull
    public EnrExamPassForm getPassForm()
    {
        return _passForm;
    }

    /**
     * @param passForm Форма сдачи. Свойство не может быть null.
     */
    public void setPassForm(EnrExamPassForm passForm)
    {
        dirty(_passForm, passForm);
        _passForm = passForm;
    }

    /**
     * @return Пересдача. Свойство не может быть null.
     */
    @NotNull
    public boolean isRetake()
    {
        return _retake;
    }

    /**
     * @param retake Пересдача. Свойство не может быть null.
     */
    public void setRetake(boolean retake)
    {
        dirty(_retake, retake);
        _retake = retake;
    }

    /**
     * @return Дата изменения. Свойство не может быть null.
     */
    @NotNull
    public Date getModificationDate()
    {
        return _modificationDate;
    }

    /**
     * @param modificationDate Дата изменения. Свойство не может быть null.
     */
    public void setModificationDate(Date modificationDate)
    {
        dirty(_modificationDate, modificationDate);
        _modificationDate = modificationDate;
    }

    /**
     * @return Отметка о неявке.
     */
    public EnrAbsenceNote getAbsenceNote()
    {
        return _absenceNote;
    }

    /**
     * @param absenceNote Отметка о неявке.
     */
    public void setAbsenceNote(EnrAbsenceNote absenceNote)
    {
        dirty(_absenceNote, absenceNote);
        _absenceNote = absenceNote;
    }

    /**
     * @return Дата выставления оценки.
     */
    public Date getMarkDate()
    {
        return _markDate;
    }

    /**
     * @param markDate Дата выставления оценки.
     */
    public void setMarkDate(Date markDate)
    {
        dirty(_markDate, markDate);
        _markDate = markDate;
    }

    /**
     * Хранится со смещением в три знака для дробной части.
     * Если балл задан, то считается что балл есть (на основе этой записи будут создаваться enrEntrantMarkSourceExamPass).
     * Если задан absenceNote и это неуважительная причина, то считается, что балла есть и равен 0.
     * Если задан absenceNote и это уважительная причина, то считается, что балла нет (null).
     *
     * @return Балл.
     */
    public Long getMarkAsLong()
    {
        return _markAsLong;
    }

    /**
     * @param markAsLong Балл.
     */
    public void setMarkAsLong(Long markAsLong)
    {
        dirty(_markAsLong, markAsLong);
        _markAsLong = markAsLong;
    }

    /**
     * @return Территориальное подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    /**
     * @param territorialOrgUnit Территориальное подразделение. Свойство не может быть null.
     */
    public void setTerritorialOrgUnit(OrgUnit territorialOrgUnit)
    {
        dirty(_territorialOrgUnit, territorialOrgUnit);
        _territorialOrgUnit = territorialOrgUnit;
    }

    /**
     * @return ЭГ.
     */
    public EnrExamGroup getExamGroup()
    {
        return _examGroup;
    }

    /**
     * @param examGroup ЭГ.
     */
    public void setExamGroup(EnrExamGroup examGroup)
    {
        dirty(_examGroup, examGroup);
        _examGroup = examGroup;
    }

    /**
     * @return Дата включения в ЭГ.
     */
    public Date getExamGroupSetDate()
    {
        return _examGroupSetDate;
    }

    /**
     * @param examGroupSetDate Дата включения в ЭГ.
     */
    public void setExamGroupSetDate(Date examGroupSetDate)
    {
        dirty(_examGroupSetDate, examGroupSetDate);
        _examGroupSetDate = examGroupSetDate;
    }

    /**
     * @return Номер в ЭГ.
     */
    public Integer getExamGroupPosition()
    {
        return _examGroupPosition;
    }

    /**
     * @param examGroupPosition Номер в ЭГ.
     */
    public void setExamGroupPosition(Integer examGroupPosition)
    {
        dirty(_examGroupPosition, examGroupPosition);
        _examGroupPosition = examGroupPosition;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrExamPassDisciplineGen)
        {
            if (withNaturalIdProperties)
            {
                setEntrant(((EnrExamPassDiscipline)another).getEntrant());
                setDiscipline(((EnrExamPassDiscipline)another).getDiscipline());
                setPassForm(((EnrExamPassDiscipline)another).getPassForm());
                setRetake(((EnrExamPassDiscipline)another).isRetake());
            }
            setModificationDate(((EnrExamPassDiscipline)another).getModificationDate());
            setAbsenceNote(((EnrExamPassDiscipline)another).getAbsenceNote());
            setMarkDate(((EnrExamPassDiscipline)another).getMarkDate());
            setMarkAsLong(((EnrExamPassDiscipline)another).getMarkAsLong());
            setTerritorialOrgUnit(((EnrExamPassDiscipline)another).getTerritorialOrgUnit());
            setExamGroup(((EnrExamPassDiscipline)another).getExamGroup());
            setExamGroupSetDate(((EnrExamPassDiscipline)another).getExamGroupSetDate());
            setExamGroupPosition(((EnrExamPassDiscipline)another).getExamGroupPosition());
        }
    }

    public INaturalId<EnrExamPassDisciplineGen> getNaturalId()
    {
        return new NaturalId(getEntrant(), getDiscipline(), getPassForm(), isRetake());
    }

    public static class NaturalId extends NaturalIdBase<EnrExamPassDisciplineGen>
    {
        private static final String PROXY_NAME = "EnrExamPassDisciplineNaturalProxy";

        private Long _entrant;
        private Long _discipline;
        private Long _passForm;
        private boolean _retake;

        public NaturalId()
        {}

        public NaturalId(EnrEntrant entrant, EnrCampaignDiscipline discipline, EnrExamPassForm passForm, boolean retake)
        {
            _entrant = ((IEntity) entrant).getId();
            _discipline = ((IEntity) discipline).getId();
            _passForm = ((IEntity) passForm).getId();
            _retake = retake;
        }

        public Long getEntrant()
        {
            return _entrant;
        }

        public void setEntrant(Long entrant)
        {
            _entrant = entrant;
        }

        public Long getDiscipline()
        {
            return _discipline;
        }

        public void setDiscipline(Long discipline)
        {
            _discipline = discipline;
        }

        public Long getPassForm()
        {
            return _passForm;
        }

        public void setPassForm(Long passForm)
        {
            _passForm = passForm;
        }

        public boolean isRetake()
        {
            return _retake;
        }

        public void setRetake(boolean retake)
        {
            _retake = retake;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrExamPassDisciplineGen.NaturalId) ) return false;

            EnrExamPassDisciplineGen.NaturalId that = (NaturalId) o;

            if( !equals(getEntrant(), that.getEntrant()) ) return false;
            if( !equals(getDiscipline(), that.getDiscipline()) ) return false;
            if( !equals(getPassForm(), that.getPassForm()) ) return false;
            if( !equals(isRetake(), that.isRetake()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEntrant());
            result = hashCode(result, getDiscipline());
            result = hashCode(result, getPassForm());
            result = hashCode(result, isRetake());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEntrant());
            sb.append("/");
            sb.append(getDiscipline());
            sb.append("/");
            sb.append(getPassForm());
            sb.append("/");
            sb.append(isRetake());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrExamPassDisciplineGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrExamPassDiscipline.class;
        }

        public T newInstance()
        {
            return (T) new EnrExamPassDiscipline();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entrant":
                    return obj.getEntrant();
                case "discipline":
                    return obj.getDiscipline();
                case "passForm":
                    return obj.getPassForm();
                case "retake":
                    return obj.isRetake();
                case "modificationDate":
                    return obj.getModificationDate();
                case "absenceNote":
                    return obj.getAbsenceNote();
                case "markDate":
                    return obj.getMarkDate();
                case "markAsLong":
                    return obj.getMarkAsLong();
                case "territorialOrgUnit":
                    return obj.getTerritorialOrgUnit();
                case "examGroup":
                    return obj.getExamGroup();
                case "examGroupSetDate":
                    return obj.getExamGroupSetDate();
                case "examGroupPosition":
                    return obj.getExamGroupPosition();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entrant":
                    obj.setEntrant((EnrEntrant) value);
                    return;
                case "discipline":
                    obj.setDiscipline((EnrCampaignDiscipline) value);
                    return;
                case "passForm":
                    obj.setPassForm((EnrExamPassForm) value);
                    return;
                case "retake":
                    obj.setRetake((Boolean) value);
                    return;
                case "modificationDate":
                    obj.setModificationDate((Date) value);
                    return;
                case "absenceNote":
                    obj.setAbsenceNote((EnrAbsenceNote) value);
                    return;
                case "markDate":
                    obj.setMarkDate((Date) value);
                    return;
                case "markAsLong":
                    obj.setMarkAsLong((Long) value);
                    return;
                case "territorialOrgUnit":
                    obj.setTerritorialOrgUnit((OrgUnit) value);
                    return;
                case "examGroup":
                    obj.setExamGroup((EnrExamGroup) value);
                    return;
                case "examGroupSetDate":
                    obj.setExamGroupSetDate((Date) value);
                    return;
                case "examGroupPosition":
                    obj.setExamGroupPosition((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entrant":
                        return true;
                case "discipline":
                        return true;
                case "passForm":
                        return true;
                case "retake":
                        return true;
                case "modificationDate":
                        return true;
                case "absenceNote":
                        return true;
                case "markDate":
                        return true;
                case "markAsLong":
                        return true;
                case "territorialOrgUnit":
                        return true;
                case "examGroup":
                        return true;
                case "examGroupSetDate":
                        return true;
                case "examGroupPosition":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entrant":
                    return true;
                case "discipline":
                    return true;
                case "passForm":
                    return true;
                case "retake":
                    return true;
                case "modificationDate":
                    return true;
                case "absenceNote":
                    return true;
                case "markDate":
                    return true;
                case "markAsLong":
                    return true;
                case "territorialOrgUnit":
                    return true;
                case "examGroup":
                    return true;
                case "examGroupSetDate":
                    return true;
                case "examGroupPosition":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entrant":
                    return EnrEntrant.class;
                case "discipline":
                    return EnrCampaignDiscipline.class;
                case "passForm":
                    return EnrExamPassForm.class;
                case "retake":
                    return Boolean.class;
                case "modificationDate":
                    return Date.class;
                case "absenceNote":
                    return EnrAbsenceNote.class;
                case "markDate":
                    return Date.class;
                case "markAsLong":
                    return Long.class;
                case "territorialOrgUnit":
                    return OrgUnit.class;
                case "examGroup":
                    return EnrExamGroup.class;
                case "examGroupSetDate":
                    return Date.class;
                case "examGroupPosition":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrExamPassDiscipline> _dslPath = new Path<EnrExamPassDiscipline>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrExamPassDiscipline");
    }
            

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline#getEntrant()
     */
    public static EnrEntrant.Path<EnrEntrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return Дисциплина ПК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline#getDiscipline()
     */
    public static EnrCampaignDiscipline.Path<EnrCampaignDiscipline> discipline()
    {
        return _dslPath.discipline();
    }

    /**
     * @return Форма сдачи. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline#getPassForm()
     */
    public static EnrExamPassForm.Path<EnrExamPassForm> passForm()
    {
        return _dslPath.passForm();
    }

    /**
     * @return Пересдача. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline#isRetake()
     */
    public static PropertyPath<Boolean> retake()
    {
        return _dslPath.retake();
    }

    /**
     * @return Дата изменения. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline#getModificationDate()
     */
    public static PropertyPath<Date> modificationDate()
    {
        return _dslPath.modificationDate();
    }

    /**
     * @return Отметка о неявке.
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline#getAbsenceNote()
     */
    public static EnrAbsenceNote.Path<EnrAbsenceNote> absenceNote()
    {
        return _dslPath.absenceNote();
    }

    /**
     * @return Дата выставления оценки.
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline#getMarkDate()
     */
    public static PropertyPath<Date> markDate()
    {
        return _dslPath.markDate();
    }

    /**
     * Хранится со смещением в три знака для дробной части.
     * Если балл задан, то считается что балл есть (на основе этой записи будут создаваться enrEntrantMarkSourceExamPass).
     * Если задан absenceNote и это неуважительная причина, то считается, что балла есть и равен 0.
     * Если задан absenceNote и это уважительная причина, то считается, что балла нет (null).
     *
     * @return Балл.
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline#getMarkAsLong()
     */
    public static PropertyPath<Long> markAsLong()
    {
        return _dslPath.markAsLong();
    }

    /**
     * @return Территориальное подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline#getTerritorialOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> territorialOrgUnit()
    {
        return _dslPath.territorialOrgUnit();
    }

    /**
     * @return ЭГ.
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline#getExamGroup()
     */
    public static EnrExamGroup.Path<EnrExamGroup> examGroup()
    {
        return _dslPath.examGroup();
    }

    /**
     * @return Дата включения в ЭГ.
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline#getExamGroupSetDate()
     */
    public static PropertyPath<Date> examGroupSetDate()
    {
        return _dslPath.examGroupSetDate();
    }

    /**
     * @return Номер в ЭГ.
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline#getExamGroupPosition()
     */
    public static PropertyPath<Integer> examGroupPosition()
    {
        return _dslPath.examGroupPosition();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline#getCode()
     */
    public static SupportedPropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline#getDisciplineAndPassFormTitle()
     */
    public static SupportedPropertyPath<String> disciplineAndPassFormTitle()
    {
        return _dslPath.disciplineAndPassFormTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline#getExamResult()
     */
    public static SupportedPropertyPath<String> examResult()
    {
        return _dslPath.examResult();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline#getExamResultFull()
     */
    public static SupportedPropertyPath<String> examResultFull()
    {
        return _dslPath.examResultFull();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline#getExamScheduleTitle()
     */
    public static SupportedPropertyPath<String> examScheduleTitle()
    {
        return _dslPath.examScheduleTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline#getMarkAsDouble()
     */
    public static SupportedPropertyPath<Double> markAsDouble()
    {
        return _dslPath.markAsDouble();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline#getMarkAsString()
     */
    public static SupportedPropertyPath<String> markAsString()
    {
        return _dslPath.markAsString();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline#getShortExamResult()
     */
    public static SupportedPropertyPath<String> shortExamResult()
    {
        return _dslPath.shortExamResult();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EnrExamPassDiscipline> extends EntityPath<E>
    {
        private EnrEntrant.Path<EnrEntrant> _entrant;
        private EnrCampaignDiscipline.Path<EnrCampaignDiscipline> _discipline;
        private EnrExamPassForm.Path<EnrExamPassForm> _passForm;
        private PropertyPath<Boolean> _retake;
        private PropertyPath<Date> _modificationDate;
        private EnrAbsenceNote.Path<EnrAbsenceNote> _absenceNote;
        private PropertyPath<Date> _markDate;
        private PropertyPath<Long> _markAsLong;
        private OrgUnit.Path<OrgUnit> _territorialOrgUnit;
        private EnrExamGroup.Path<EnrExamGroup> _examGroup;
        private PropertyPath<Date> _examGroupSetDate;
        private PropertyPath<Integer> _examGroupPosition;
        private SupportedPropertyPath<String> _code;
        private SupportedPropertyPath<String> _disciplineAndPassFormTitle;
        private SupportedPropertyPath<String> _examResult;
        private SupportedPropertyPath<String> _examResultFull;
        private SupportedPropertyPath<String> _examScheduleTitle;
        private SupportedPropertyPath<Double> _markAsDouble;
        private SupportedPropertyPath<String> _markAsString;
        private SupportedPropertyPath<String> _shortExamResult;
        private SupportedPropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline#getEntrant()
     */
        public EnrEntrant.Path<EnrEntrant> entrant()
        {
            if(_entrant == null )
                _entrant = new EnrEntrant.Path<EnrEntrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return Дисциплина ПК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline#getDiscipline()
     */
        public EnrCampaignDiscipline.Path<EnrCampaignDiscipline> discipline()
        {
            if(_discipline == null )
                _discipline = new EnrCampaignDiscipline.Path<EnrCampaignDiscipline>(L_DISCIPLINE, this);
            return _discipline;
        }

    /**
     * @return Форма сдачи. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline#getPassForm()
     */
        public EnrExamPassForm.Path<EnrExamPassForm> passForm()
        {
            if(_passForm == null )
                _passForm = new EnrExamPassForm.Path<EnrExamPassForm>(L_PASS_FORM, this);
            return _passForm;
        }

    /**
     * @return Пересдача. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline#isRetake()
     */
        public PropertyPath<Boolean> retake()
        {
            if(_retake == null )
                _retake = new PropertyPath<Boolean>(EnrExamPassDisciplineGen.P_RETAKE, this);
            return _retake;
        }

    /**
     * @return Дата изменения. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline#getModificationDate()
     */
        public PropertyPath<Date> modificationDate()
        {
            if(_modificationDate == null )
                _modificationDate = new PropertyPath<Date>(EnrExamPassDisciplineGen.P_MODIFICATION_DATE, this);
            return _modificationDate;
        }

    /**
     * @return Отметка о неявке.
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline#getAbsenceNote()
     */
        public EnrAbsenceNote.Path<EnrAbsenceNote> absenceNote()
        {
            if(_absenceNote == null )
                _absenceNote = new EnrAbsenceNote.Path<EnrAbsenceNote>(L_ABSENCE_NOTE, this);
            return _absenceNote;
        }

    /**
     * @return Дата выставления оценки.
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline#getMarkDate()
     */
        public PropertyPath<Date> markDate()
        {
            if(_markDate == null )
                _markDate = new PropertyPath<Date>(EnrExamPassDisciplineGen.P_MARK_DATE, this);
            return _markDate;
        }

    /**
     * Хранится со смещением в три знака для дробной части.
     * Если балл задан, то считается что балл есть (на основе этой записи будут создаваться enrEntrantMarkSourceExamPass).
     * Если задан absenceNote и это неуважительная причина, то считается, что балла есть и равен 0.
     * Если задан absenceNote и это уважительная причина, то считается, что балла нет (null).
     *
     * @return Балл.
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline#getMarkAsLong()
     */
        public PropertyPath<Long> markAsLong()
        {
            if(_markAsLong == null )
                _markAsLong = new PropertyPath<Long>(EnrExamPassDisciplineGen.P_MARK_AS_LONG, this);
            return _markAsLong;
        }

    /**
     * @return Территориальное подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline#getTerritorialOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> territorialOrgUnit()
        {
            if(_territorialOrgUnit == null )
                _territorialOrgUnit = new OrgUnit.Path<OrgUnit>(L_TERRITORIAL_ORG_UNIT, this);
            return _territorialOrgUnit;
        }

    /**
     * @return ЭГ.
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline#getExamGroup()
     */
        public EnrExamGroup.Path<EnrExamGroup> examGroup()
        {
            if(_examGroup == null )
                _examGroup = new EnrExamGroup.Path<EnrExamGroup>(L_EXAM_GROUP, this);
            return _examGroup;
        }

    /**
     * @return Дата включения в ЭГ.
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline#getExamGroupSetDate()
     */
        public PropertyPath<Date> examGroupSetDate()
        {
            if(_examGroupSetDate == null )
                _examGroupSetDate = new PropertyPath<Date>(EnrExamPassDisciplineGen.P_EXAM_GROUP_SET_DATE, this);
            return _examGroupSetDate;
        }

    /**
     * @return Номер в ЭГ.
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline#getExamGroupPosition()
     */
        public PropertyPath<Integer> examGroupPosition()
        {
            if(_examGroupPosition == null )
                _examGroupPosition = new PropertyPath<Integer>(EnrExamPassDisciplineGen.P_EXAM_GROUP_POSITION, this);
            return _examGroupPosition;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline#getCode()
     */
        public SupportedPropertyPath<String> code()
        {
            if(_code == null )
                _code = new SupportedPropertyPath<String>(EnrExamPassDisciplineGen.P_CODE, this);
            return _code;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline#getDisciplineAndPassFormTitle()
     */
        public SupportedPropertyPath<String> disciplineAndPassFormTitle()
        {
            if(_disciplineAndPassFormTitle == null )
                _disciplineAndPassFormTitle = new SupportedPropertyPath<String>(EnrExamPassDisciplineGen.P_DISCIPLINE_AND_PASS_FORM_TITLE, this);
            return _disciplineAndPassFormTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline#getExamResult()
     */
        public SupportedPropertyPath<String> examResult()
        {
            if(_examResult == null )
                _examResult = new SupportedPropertyPath<String>(EnrExamPassDisciplineGen.P_EXAM_RESULT, this);
            return _examResult;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline#getExamResultFull()
     */
        public SupportedPropertyPath<String> examResultFull()
        {
            if(_examResultFull == null )
                _examResultFull = new SupportedPropertyPath<String>(EnrExamPassDisciplineGen.P_EXAM_RESULT_FULL, this);
            return _examResultFull;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline#getExamScheduleTitle()
     */
        public SupportedPropertyPath<String> examScheduleTitle()
        {
            if(_examScheduleTitle == null )
                _examScheduleTitle = new SupportedPropertyPath<String>(EnrExamPassDisciplineGen.P_EXAM_SCHEDULE_TITLE, this);
            return _examScheduleTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline#getMarkAsDouble()
     */
        public SupportedPropertyPath<Double> markAsDouble()
        {
            if(_markAsDouble == null )
                _markAsDouble = new SupportedPropertyPath<Double>(EnrExamPassDisciplineGen.P_MARK_AS_DOUBLE, this);
            return _markAsDouble;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline#getMarkAsString()
     */
        public SupportedPropertyPath<String> markAsString()
        {
            if(_markAsString == null )
                _markAsString = new SupportedPropertyPath<String>(EnrExamPassDisciplineGen.P_MARK_AS_STRING, this);
            return _markAsString;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline#getShortExamResult()
     */
        public SupportedPropertyPath<String> shortExamResult()
        {
            if(_shortExamResult == null )
                _shortExamResult = new SupportedPropertyPath<String>(EnrExamPassDisciplineGen.P_SHORT_EXAM_RESULT, this);
            return _shortExamResult;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(EnrExamPassDisciplineGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EnrExamPassDiscipline.class;
        }

        public String getEntityName()
        {
            return "enrExamPassDiscipline";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getCode();

    public abstract String getDisciplineAndPassFormTitle();

    public abstract String getExamResult();

    public abstract String getExamResultFull();

    public abstract String getExamScheduleTitle();

    public abstract Double getMarkAsDouble();

    public abstract String getMarkAsString();

    public abstract String getShortExamResult();

    public abstract String getTitle();
}
