/* $Id:$ */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.logic;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.util.Collections;
import java.util.List;

/**
 * @author oleyba
 * @since 5/23/14
 */
public class EnrOrgUnitBaseDSHandler extends EntityComboDataSourceHandler
{
    public EnrOrgUnitBaseDSHandler(String ownerId)
    {
        super(ownerId, EnrOrgUnit.class);
        pageable(true);
    }

    @Override
    protected boolean isExternalSortRequired() {
        return true;
    }

    @Override
    protected List<IEntity> sortOptions(List<IEntity> list) {
        Collections.sort(list, EnrOrgUnit.ENR_ORG_UNIT_DEPARTMENT_TITLE_UNCHECKED_COMPARATOR);
        return list;
    }

    @Override
    protected DQLSelectBuilder query(String alias, String filter) {
        DQLSelectBuilder query = super.query(alias, filter);
        query.where(EnrCompetitionFilterAddon.ENR_ORG_UNIT_SUBSTRING_FILTER_RULE.whereExpression(alias, filter));
        return query;
    }

}

