/* $Id$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.EdiSpecialExamConditions;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;

/**
 * @author azhebko
 * @since 04.06.2014
 */
@Input(@Bind(key = IUIPresenter.PUBLISHER_ID, binding = "entrant.id", required = true))
public class EnrEntrantEdiSpecialExamConditionsUI extends UIPresenter
{
    private EnrEntrant _entrant = new EnrEntrant();

    @Override
    public void onComponentRefresh()
    {
        setEntrant(IUniBaseDao.instance.get().get(EnrEntrant.class, getEntrant().getId()));
    }

    public void onClickApply()
    {
        IUniBaseDao.instance.get().update(getEntrant());
        deactivate();
    }

    public EnrEntrant getEntrant(){ return _entrant; }
    public void setEntrant(EnrEntrant entrant){ _entrant = entrant; }
}