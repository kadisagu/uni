package ru.tandemservice.unienr14.request.bo.EnrRequestedCompetition.logic;

import java.util.Map;

import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;

/**
 * Правило расчета значения свойства в демоне на основе условий
 * TODO: перенести в базовый механизм вместе с EnrRequestExtParamsDaemonBean
 * @author vdanilov
 */
public interface IPropertyUpdateRule<T> {

    /**
     * запускается при станте, регистрирует события пробуждения демона (если требуется)
     * @param wakeUpListener листенер пробуждения демона (его надо либо запустить из своего листенера, либо зарегистрировать)
     */
    void registerWakeUp(IDSetEventListener wakeUpListener);

    /**
     * Формирует перечень значений и необходимых условий, к ним приводящих
     * @param alias алиас сущности (чье свойство надо поменять)
     * @param params внешние параметры
     * @return { value -> expression } условие попадания в указанное значение (все условия будут объединяться по and)
     */
    Map<T, IDQLExpression> buildValueExpressionMap(String alias, Map<String, Object> params);

}
