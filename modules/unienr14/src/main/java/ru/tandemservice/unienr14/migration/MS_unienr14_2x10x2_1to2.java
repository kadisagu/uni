package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unienr14_2x10x2_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrCampaignEntrantDocument

		// создано обязательное свойство sendFIS
		{
			// создать колонку
			tool.createColumn("enr14_camp_entrant_doc_t", new DBColumn("sendfis_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			tool.executeUpdate("update enr14_camp_entrant_doc_t set sendfis_p=? where sendfis_p is null", true);

			// сделать колонку NOT NULL
			tool.setColumnNullable("enr14_camp_entrant_doc_t", "sendfis_p", false);

		}
    }
}