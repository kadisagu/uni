/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EnrollmentCommissionMeetingProtocolAdd.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EnrollmentCommissionMeetingProtocolAdd.EnrReportEnrollmentCommissionMeetingProtocolAddUI;

/**
 * @author rsizonenko
 * @since 06.08.2014
 */
public interface IEnrReportEnrollmentCommissionMeetingProtocolDao extends INeedPersistenceSupport {
    Long createReport(EnrReportEnrollmentCommissionMeetingProtocolAddUI model);
}
