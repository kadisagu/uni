package ru.tandemservice.unienr14.refusal.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantRefusalReason;
import ru.tandemservice.unienr14.refusal.entity.EnrEntrantRefusal;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Отказ в приеме документов
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrEntrantRefusalGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.refusal.entity.EnrEntrantRefusal";
    public static final String ENTITY_NAME = "enrEntrantRefusal";
    public static final int VERSION_HASH = -1721830386;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String L_ENR_ORG_UNIT = "enrOrgUnit";
    public static final String P_FIRST_NAME = "firstName";
    public static final String P_LAST_NAME = "lastName";
    public static final String P_MIDDLE_NAME = "middleName";
    public static final String P_COMPETITION_DATA = "competitionData";
    public static final String P_REGISTRATION_DATE = "registrationDate";
    public static final String L_REFUSAL_REASON = "refusalReason";
    public static final String P_COMMENT = "comment";
    public static final String P_FULL_NAME = "fullName";

    private EnrEnrollmentCampaign _enrollmentCampaign;     // ПК
    private EnrOrgUnit _enrOrgUnit;     // Лицензированное подразделение
    private String _firstName;     // Имя
    private String _lastName;     // Фамилия
    private String _middleName;     // Отчество
    private String _competitionData;     // Выбранные условия поступления
    private Date _registrationDate;     // Дата добавления
    private EnrEntrantRefusalReason _refusalReason;     // Причина отказа
    private String _comment;     // Примечание к отказу

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return ПК. Свойство не может быть null.
     */
    @NotNull
    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign ПК. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Лицензированное подразделение. Свойство не может быть null.
     */
    @NotNull
    public EnrOrgUnit getEnrOrgUnit()
    {
        return _enrOrgUnit;
    }

    /**
     * @param enrOrgUnit Лицензированное подразделение. Свойство не может быть null.
     */
    public void setEnrOrgUnit(EnrOrgUnit enrOrgUnit)
    {
        dirty(_enrOrgUnit, enrOrgUnit);
        _enrOrgUnit = enrOrgUnit;
    }

    /**
     * @return Имя. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getFirstName()
    {
        return _firstName;
    }

    /**
     * @param firstName Имя. Свойство не может быть null.
     */
    public void setFirstName(String firstName)
    {
        dirty(_firstName, firstName);
        _firstName = firstName;
    }

    /**
     * @return Фамилия. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getLastName()
    {
        return _lastName;
    }

    /**
     * @param lastName Фамилия. Свойство не может быть null.
     */
    public void setLastName(String lastName)
    {
        dirty(_lastName, lastName);
        _lastName = lastName;
    }

    /**
     * @return Отчество.
     */
    @Length(max=255)
    public String getMiddleName()
    {
        return _middleName;
    }

    /**
     * @param middleName Отчество.
     */
    public void setMiddleName(String middleName)
    {
        dirty(_middleName, middleName);
        _middleName = middleName;
    }

    /**
     * @return Выбранные условия поступления. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getCompetitionData()
    {
        return _competitionData;
    }

    /**
     * @param competitionData Выбранные условия поступления. Свойство не может быть null.
     */
    public void setCompetitionData(String competitionData)
    {
        dirty(_competitionData, competitionData);
        _competitionData = competitionData;
    }

    /**
     * @return Дата добавления. Свойство не может быть null.
     */
    @NotNull
    public Date getRegistrationDate()
    {
        return _registrationDate;
    }

    /**
     * @param registrationDate Дата добавления. Свойство не может быть null.
     */
    public void setRegistrationDate(Date registrationDate)
    {
        dirty(_registrationDate, registrationDate);
        _registrationDate = registrationDate;
    }

    /**
     * @return Причина отказа. Свойство не может быть null.
     */
    @NotNull
    public EnrEntrantRefusalReason getRefusalReason()
    {
        return _refusalReason;
    }

    /**
     * @param refusalReason Причина отказа. Свойство не может быть null.
     */
    public void setRefusalReason(EnrEntrantRefusalReason refusalReason)
    {
        dirty(_refusalReason, refusalReason);
        _refusalReason = refusalReason;
    }

    /**
     * @return Примечание к отказу.
     */
    @Length(max=255)
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Примечание к отказу.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrEntrantRefusalGen)
        {
            setEnrollmentCampaign(((EnrEntrantRefusal)another).getEnrollmentCampaign());
            setEnrOrgUnit(((EnrEntrantRefusal)another).getEnrOrgUnit());
            setFirstName(((EnrEntrantRefusal)another).getFirstName());
            setLastName(((EnrEntrantRefusal)another).getLastName());
            setMiddleName(((EnrEntrantRefusal)another).getMiddleName());
            setCompetitionData(((EnrEntrantRefusal)another).getCompetitionData());
            setRegistrationDate(((EnrEntrantRefusal)another).getRegistrationDate());
            setRefusalReason(((EnrEntrantRefusal)another).getRefusalReason());
            setComment(((EnrEntrantRefusal)another).getComment());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrEntrantRefusalGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrEntrantRefusal.class;
        }

        public T newInstance()
        {
            return (T) new EnrEntrantRefusal();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "enrOrgUnit":
                    return obj.getEnrOrgUnit();
                case "firstName":
                    return obj.getFirstName();
                case "lastName":
                    return obj.getLastName();
                case "middleName":
                    return obj.getMiddleName();
                case "competitionData":
                    return obj.getCompetitionData();
                case "registrationDate":
                    return obj.getRegistrationDate();
                case "refusalReason":
                    return obj.getRefusalReason();
                case "comment":
                    return obj.getComment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrEnrollmentCampaign) value);
                    return;
                case "enrOrgUnit":
                    obj.setEnrOrgUnit((EnrOrgUnit) value);
                    return;
                case "firstName":
                    obj.setFirstName((String) value);
                    return;
                case "lastName":
                    obj.setLastName((String) value);
                    return;
                case "middleName":
                    obj.setMiddleName((String) value);
                    return;
                case "competitionData":
                    obj.setCompetitionData((String) value);
                    return;
                case "registrationDate":
                    obj.setRegistrationDate((Date) value);
                    return;
                case "refusalReason":
                    obj.setRefusalReason((EnrEntrantRefusalReason) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "enrOrgUnit":
                        return true;
                case "firstName":
                        return true;
                case "lastName":
                        return true;
                case "middleName":
                        return true;
                case "competitionData":
                        return true;
                case "registrationDate":
                        return true;
                case "refusalReason":
                        return true;
                case "comment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "enrOrgUnit":
                    return true;
                case "firstName":
                    return true;
                case "lastName":
                    return true;
                case "middleName":
                    return true;
                case "competitionData":
                    return true;
                case "registrationDate":
                    return true;
                case "refusalReason":
                    return true;
                case "comment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrollmentCampaign":
                    return EnrEnrollmentCampaign.class;
                case "enrOrgUnit":
                    return EnrOrgUnit.class;
                case "firstName":
                    return String.class;
                case "lastName":
                    return String.class;
                case "middleName":
                    return String.class;
                case "competitionData":
                    return String.class;
                case "registrationDate":
                    return Date.class;
                case "refusalReason":
                    return EnrEntrantRefusalReason.class;
                case "comment":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrEntrantRefusal> _dslPath = new Path<EnrEntrantRefusal>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrEntrantRefusal");
    }
            

    /**
     * @return ПК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.refusal.entity.EnrEntrantRefusal#getEnrollmentCampaign()
     */
    public static EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Лицензированное подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.refusal.entity.EnrEntrantRefusal#getEnrOrgUnit()
     */
    public static EnrOrgUnit.Path<EnrOrgUnit> enrOrgUnit()
    {
        return _dslPath.enrOrgUnit();
    }

    /**
     * @return Имя. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.refusal.entity.EnrEntrantRefusal#getFirstName()
     */
    public static PropertyPath<String> firstName()
    {
        return _dslPath.firstName();
    }

    /**
     * @return Фамилия. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.refusal.entity.EnrEntrantRefusal#getLastName()
     */
    public static PropertyPath<String> lastName()
    {
        return _dslPath.lastName();
    }

    /**
     * @return Отчество.
     * @see ru.tandemservice.unienr14.refusal.entity.EnrEntrantRefusal#getMiddleName()
     */
    public static PropertyPath<String> middleName()
    {
        return _dslPath.middleName();
    }

    /**
     * @return Выбранные условия поступления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.refusal.entity.EnrEntrantRefusal#getCompetitionData()
     */
    public static PropertyPath<String> competitionData()
    {
        return _dslPath.competitionData();
    }

    /**
     * @return Дата добавления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.refusal.entity.EnrEntrantRefusal#getRegistrationDate()
     */
    public static PropertyPath<Date> registrationDate()
    {
        return _dslPath.registrationDate();
    }

    /**
     * @return Причина отказа. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.refusal.entity.EnrEntrantRefusal#getRefusalReason()
     */
    public static EnrEntrantRefusalReason.Path<EnrEntrantRefusalReason> refusalReason()
    {
        return _dslPath.refusalReason();
    }

    /**
     * @return Примечание к отказу.
     * @see ru.tandemservice.unienr14.refusal.entity.EnrEntrantRefusal#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.refusal.entity.EnrEntrantRefusal#getFullName()
     */
    public static SupportedPropertyPath<String> fullName()
    {
        return _dslPath.fullName();
    }

    public static class Path<E extends EnrEntrantRefusal> extends EntityPath<E>
    {
        private EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> _enrollmentCampaign;
        private EnrOrgUnit.Path<EnrOrgUnit> _enrOrgUnit;
        private PropertyPath<String> _firstName;
        private PropertyPath<String> _lastName;
        private PropertyPath<String> _middleName;
        private PropertyPath<String> _competitionData;
        private PropertyPath<Date> _registrationDate;
        private EnrEntrantRefusalReason.Path<EnrEntrantRefusalReason> _refusalReason;
        private PropertyPath<String> _comment;
        private SupportedPropertyPath<String> _fullName;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return ПК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.refusal.entity.EnrEntrantRefusal#getEnrollmentCampaign()
     */
        public EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Лицензированное подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.refusal.entity.EnrEntrantRefusal#getEnrOrgUnit()
     */
        public EnrOrgUnit.Path<EnrOrgUnit> enrOrgUnit()
        {
            if(_enrOrgUnit == null )
                _enrOrgUnit = new EnrOrgUnit.Path<EnrOrgUnit>(L_ENR_ORG_UNIT, this);
            return _enrOrgUnit;
        }

    /**
     * @return Имя. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.refusal.entity.EnrEntrantRefusal#getFirstName()
     */
        public PropertyPath<String> firstName()
        {
            if(_firstName == null )
                _firstName = new PropertyPath<String>(EnrEntrantRefusalGen.P_FIRST_NAME, this);
            return _firstName;
        }

    /**
     * @return Фамилия. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.refusal.entity.EnrEntrantRefusal#getLastName()
     */
        public PropertyPath<String> lastName()
        {
            if(_lastName == null )
                _lastName = new PropertyPath<String>(EnrEntrantRefusalGen.P_LAST_NAME, this);
            return _lastName;
        }

    /**
     * @return Отчество.
     * @see ru.tandemservice.unienr14.refusal.entity.EnrEntrantRefusal#getMiddleName()
     */
        public PropertyPath<String> middleName()
        {
            if(_middleName == null )
                _middleName = new PropertyPath<String>(EnrEntrantRefusalGen.P_MIDDLE_NAME, this);
            return _middleName;
        }

    /**
     * @return Выбранные условия поступления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.refusal.entity.EnrEntrantRefusal#getCompetitionData()
     */
        public PropertyPath<String> competitionData()
        {
            if(_competitionData == null )
                _competitionData = new PropertyPath<String>(EnrEntrantRefusalGen.P_COMPETITION_DATA, this);
            return _competitionData;
        }

    /**
     * @return Дата добавления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.refusal.entity.EnrEntrantRefusal#getRegistrationDate()
     */
        public PropertyPath<Date> registrationDate()
        {
            if(_registrationDate == null )
                _registrationDate = new PropertyPath<Date>(EnrEntrantRefusalGen.P_REGISTRATION_DATE, this);
            return _registrationDate;
        }

    /**
     * @return Причина отказа. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.refusal.entity.EnrEntrantRefusal#getRefusalReason()
     */
        public EnrEntrantRefusalReason.Path<EnrEntrantRefusalReason> refusalReason()
        {
            if(_refusalReason == null )
                _refusalReason = new EnrEntrantRefusalReason.Path<EnrEntrantRefusalReason>(L_REFUSAL_REASON, this);
            return _refusalReason;
        }

    /**
     * @return Примечание к отказу.
     * @see ru.tandemservice.unienr14.refusal.entity.EnrEntrantRefusal#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(EnrEntrantRefusalGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.refusal.entity.EnrEntrantRefusal#getFullName()
     */
        public SupportedPropertyPath<String> fullName()
        {
            if(_fullName == null )
                _fullName = new SupportedPropertyPath<String>(EnrEntrantRefusalGen.P_FULL_NAME, this);
            return _fullName;
        }

        public Class getEntityClass()
        {
            return EnrEntrantRefusal.class;
        }

        public String getEntityName()
        {
            return "enrEntrantRefusal";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getFullName();
}
