/* $Id$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.AdmissionResultsByEnrollmentStagesAdd.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLCaseExpressionBuilder;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.ICommonFilterItem;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.subjects.*;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.codes.*;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam;
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSource;
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceBenefit;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.AdmissionResultsByEnrollmentStagesAdd.EnrReportAdmissionResultsByEnrollmentStagesAddUI;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportUtil;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder;
import ru.tandemservice.unienr14.report.entity.EnrReportAdmissionResultsByEnrollmentStages;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import ru.tandemservice.unimove.entity.catalog.codes.OrderStatesCodes;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 8/19/14
 */
public class EnrReportAdmissionResultsByEnrollmentStagesDAO extends UniBaseDao implements IEnrReportAdmissionResultsByEnrollmentStagesDAO
{
    private static final String T1 = "T1";
    private static final String T2 = "T2";

    private final static Comparator<GroupBudget> GROUP_BUDGET_COMPARATOR = new Comparator<GroupBudget>()
    {
        @Override
        public int compare(GroupBudget o1, GroupBudget o2)
        {
            return CommonCollator.RUSSIAN_COLLATOR.compare(o1._groupTitle, o2._groupTitle);
        }
    };

    private final static Comparator<GroupContract> GROUP_CONTRACT_COMPARATOR = new Comparator<GroupContract>()
    {
        @Override
        public int compare(GroupContract o1, GroupContract o2)
        {
            return CommonCollator.RUSSIAN_COLLATOR.compare(o1._groupTitle, o2._groupTitle);
        }
    };

    @Override
    public Long createReport(EnrReportAdmissionResultsByEnrollmentStagesAddUI model)
    {
        EnrReportAdmissionResultsByEnrollmentStages report = new EnrReportAdmissionResultsByEnrollmentStages();
        report.setFormingDate(new Date());

        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateSelector().getDateFrom());
        report.setDateTo(model.getDateSelector().getDateTo());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, EnrReportAdmissionResultsByEnrollmentStages.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE, EnrReportAdmissionResultsByEnrollmentStages.P_COMPENSATION_TYPE, "shortTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, EnrReportAdmissionResultsByEnrollmentStages.P_PROGRAM_FORM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, EnrReportAdmissionResultsByEnrollmentStages.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, EnrReportAdmissionResultsByEnrollmentStages.P_FORMATIVE_ORG_UNIT, "fullTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, EnrReportAdmissionResultsByEnrollmentStages.P_PROGRAM_SUBJECT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, EnrReportAdmissionResultsByEnrollmentStages.P_EDU_PROGRAM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, EnrReportAdmissionResultsByEnrollmentStages.P_PROGRAM_SET, "title");
        if (model.getParallelSelector().isParallelActive()) {
            report.setParallel(model.getParallelSelector().getParallel().getTitle());
        }

        report.setFirstStageDateFrom(model.getFirstStageDateFrom());
        report.setFirstStageDateTo(model.getFirstStageDateTo());
        report.setSecondStageDateFrom(model.getSecondStageDateFrom());
        report.setSecondStageDateTo(model.getSecondStageDateTo());
        report.setExtStageDateFrom(model.getExtStageDateFrom());
        report.setExtStageDateTo(model.getExtStageDateTo());

        DatabaseFile content = new DatabaseFile();
        content.setContent(buildReport(model, EnrScriptItemCodes.REPORT_ENROLLMENT_RESULT_STAGE));
        content.setFilename("EnrReportAdmissionResultsByEnrollmentStages.rtf");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        save(content);

        report.setContent(content);
        save(report);

        return report.getId();
    }

    protected byte[] buildReport(final EnrReportAdmissionResultsByEnrollmentStagesAddUI model, String templateCatalogCode)
    {
        //rtf
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(EnrScriptItem.class, templateCatalogCode);
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();

        // Выбираем абитуриентов, подходящих под параметры отчета
        EnrEntrantDQLSelectBuilder requestedCompDQL = prepareEntrantDQL(model, filterAddon, false);
        requestedCompDQL.joinEntity(requestedCompDQL.reqComp(), DQLJoinType.left, EnrEnrollmentExtract.class, "ee",
                eq(property(requestedCompDQL.reqComp(), EnrRequestedCompetition.id()), property("ee", EnrEnrollmentExtract.entity().id())));

        requestedCompDQL.column(property("reqComp", EnrRequestedCompetition.competition().programSetOrgUnit().programSet().programSubject()));
        requestedCompDQL.column(property("reqComp", EnrRequestedCompetition.competition().type().code()));
        requestedCompDQL.column(property("ee"));
        requestedCompDQL.column(new DQLCaseExpressionBuilder().when(
                and(
                        notExists(new DQLSelectBuilder().fromEntity(EnrChosenEntranceExam.class, "ce")
                                .where(eq(property("ce", EnrChosenEntranceExam.requestedCompetition().id()), property("reqComp", EnrRequestedCompetition.id())))
                                .where(ne(property("ce", EnrChosenEntranceExam.maxMarkForm().passForm().code()), value(EnrExamPassFormCodes.STATE_EXAM)))
                                .buildQuery()),
                        exists(new DQLSelectBuilder().fromEntity(EnrChosenEntranceExam.class, "ce")
                                .where(eq(property("ce", EnrChosenEntranceExam.requestedCompetition().id()), property("reqComp", EnrRequestedCompetition.id())))
                                .where(eq(property("ce", EnrChosenEntranceExam.maxMarkForm().passForm().code()), value(EnrExamPassFormCodes.STATE_EXAM)))
                                .buildQuery())), value(true)
        ).otherwise(value(false)).build());
        requestedCompDQL.column(new DQLCaseExpressionBuilder().when(
                exists(new DQLSelectBuilder().fromEntity(EnrChosenEntranceExam.class, "ce")
                        .joinPath(DQLJoinType.inner, EnrChosenEntranceExam.maxMarkForm().markSource().fromAlias("ce"), "markSource")
                        .joinEntity("ce", DQLJoinType.inner, EnrEntrantMarkSourceBenefit.class, "sb", eq(property("sb", EnrEntrantMarkSourceBenefit.id()), property("markSource", EnrEntrantMarkSource.id())))
                        .where(eq(property("ce", EnrChosenEntranceExam.requestedCompetition().id()), property("reqComp", EnrRequestedCompetition.id())))
                        .where(in(property("sb", EnrEntrantMarkSourceBenefit.benefitCategory().code()), Lists.newArrayList(EnrBenefitCategoryCodes.STO_BALLOV_POBEDITELI_I_PRIZERY_OLIMPIAD_SHKOLNIKOV, EnrBenefitCategoryCodes.STO_BALLOV_POBEDITELI_I_PRIZERY_VSEROSSIYSKOY_OLIMPIADY_SHKOLNIKOV)))
                        .buildQuery()), value(true)
        ).otherwise(value(false)).build());

        Map<EduProgramSubjectOksoGroup, GroupBudget> groupsOksoBudget = Maps.newHashMap();
        Map<EduProgramSubject2013Group, GroupBudget> groups2013Budget = Maps.newHashMap();
        Map<EduProgramSubjectOksoGroup, GroupBudget> groups2009Budget = Maps.newHashMap();

        Map<EduProgramSubjectOksoGroup, GroupContract> groupsOksoContract = Maps.newHashMap();
        Map<EduProgramSubject2013Group, GroupContract> groups2013Contract = Maps.newHashMap();
        Map<EduProgramSubjectOksoGroup, GroupContract> groups2009Contract = Maps.newHashMap();

        ICommonFilterItem compensationTypeFilterItem = filterAddon.getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE);
        CompensationType compensationType = (CompensationType) compensationTypeFilterItem.getValue();
        if(compensationType == null) throw new ApplicationException("Не указан вид возмещения затрат");

        ICommonFilterItem programFormFilterItem = filterAddon.getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM);
        EduProgramForm programForm = (EduProgramForm) programFormFilterItem.getValue();
        if(programForm == null) throw new ApplicationException("Не указана форма обучения");

        ICommonFilterItem requestTypeFilterItem = filterAddon.getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE);
        EnrRequestType requestType = (EnrRequestType) requestTypeFilterItem.getValue();
        if(requestType == null) throw new ApplicationException("Не указан вид заявления");

        for(Object[] obj : this.<Object[]>getList(requestedCompDQL))
        {
            EduProgramSubject subject = (EduProgramSubject) obj[0];
            String compType = (String) obj[1];
            EnrEnrollmentExtract extract = (EnrEnrollmentExtract) obj[2];
            Boolean stateExam = (Boolean) obj[3];
            Boolean markExclusive = (Boolean) obj[4];
            if(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(compensationType.getCode()))
            {
                GroupBudget group = null;
                if (subject instanceof EduProgramSubject2013)
                {
                    EduProgramSubject2013Group subjectGroup = ((EduProgramSubject2013) subject).getGroup();
                    group = groups2013Budget.get(subjectGroup);
                    if(group == null)
                    {
                        group = new GroupBudget(subjectGroup.getCode(), subjectGroup.getTitle());
                        groups2013Budget.put(subjectGroup, group);
                    }
                }
                else if (subject instanceof EduProgramSubjectOkso)
                {
                    EduProgramSubjectOksoGroup subjectGroup = ((EduProgramSubjectOkso) subject).getItem().getGroup();
                    group = groupsOksoBudget.get(subjectGroup);
                    if(group == null)
                    {
                        group = new GroupBudget(subjectGroup.getCode(), subjectGroup.getTitle());
                        groupsOksoBudget.put(subjectGroup, group);
                    }
                }
                else if (subject instanceof EduProgramSubject2009)
                {
                    EduProgramSubjectOksoGroup subjectGroup = ((EduProgramSubject2009) subject).getGroup();
                    group = groups2009Budget.get(subjectGroup);
                    if(group == null)
                    {
                        group = new GroupBudget(subjectGroup.getCode(), subjectGroup.getTitle());
                        groups2009Budget.put(subjectGroup, group);
                    }
                }
                if(group == null) throw new IllegalStateException();
                group.competitions += 1;

                if(extract != null)
                {
                    fillGroupBudgetByExtract(group, extract, model, compType, stateExam, markExclusive);
                }
            }
            else if(CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT.equals(compensationType.getCode()))
            {
                GroupContract group = null;
                if (subject instanceof EduProgramSubject2013)
                {
                    EduProgramSubject2013Group subjectGroup = ((EduProgramSubject2013) subject).getGroup();
                    group = groups2013Contract.get(subjectGroup);
                    if(group == null)
                    {
                        group = new GroupContract(subjectGroup.getCode(), subjectGroup.getTitle());
                        groups2013Contract.put(subjectGroup, group);
                    }
                }
                else if (subject instanceof EduProgramSubjectOkso)
                {
                    EduProgramSubjectOksoGroup subjectGroup = ((EduProgramSubjectOkso) subject).getItem().getGroup();
                    group = groupsOksoContract.get(subjectGroup);
                    if(group == null)
                    {
                        group = new GroupContract(subjectGroup.getCode(), subjectGroup.getTitle());
                        groupsOksoContract.put(subjectGroup, group);
                    }
                }
                else if (subject instanceof EduProgramSubject2009)
                {
                    EduProgramSubjectOksoGroup subjectGroup = ((EduProgramSubject2009) subject).getGroup();
                    group = groups2009Contract.get(subjectGroup);
                    if(group == null)
                    {
                        group = new GroupContract(subjectGroup.getCode(), subjectGroup.getTitle());
                        groups2009Contract.put(subjectGroup, group);
                    }
                }
                if(group == null) throw new IllegalStateException();
                group.competitions += 1;
                if(extract != null)
                {
                    fillGroupContractByExtract(group, extract, model, compType, stateExam, markExclusive);
                }
            }
        }

        // берем из шаблона таблицы
        Map<String, RtfTable> tables = new HashMap<>();
        tables.put(T1, removeTable(document, T1));
        tables.put(T2, removeTable(document, T2));

        if(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(compensationType.getCode()))
        {
            if(groups2009Budget.isEmpty() && groups2013Budget.isEmpty() && groupsOksoBudget.isEmpty())
                throw new ApplicationException("Нет данных для отчета");

            List<GroupBudget> groups2009BudgetList = Lists.newArrayList(groups2009Budget.values());
            Collections.sort(groups2009BudgetList, GROUP_BUDGET_COMPARATOR);
            List<GroupBudget> groups2013BudgetList = Lists.newArrayList(groups2013Budget.values());
            Collections.sort(groups2013BudgetList, GROUP_BUDGET_COMPARATOR);
            List<GroupBudget> groupsOksoBudgetList = Lists.newArrayList(groupsOksoBudget.values());
            Collections.sort(groupsOksoBudgetList, GROUP_BUDGET_COMPARATOR);

            RtfTable table = tables.get(T1);
            document.getElementList().add(table);

            List<String[]> data = Lists.newArrayList();
            for(GroupBudget group : groups2013BudgetList)
            {
                data.add(group.getRow());
            }
            for(GroupBudget group : groups2009BudgetList)
            {
                data.add(group.getRow());
            }
            for(GroupBudget group : groupsOksoBudgetList)
            {
                data.add(group.getRow());
            }

            RtfInjectModifier injectModifier = new RtfInjectModifier();
            injectModifier.put("academyTitle", TopOrgUnit.getInstance().getShortTitle());
            injectModifier.put("year", String.valueOf(model.getEnrollmentCampaign().getEducationYear().getIntValue()));
            injectModifier.put("programForm", programForm.getTitle());
            injectModifier.put("compensationType", "на бюджетные места");
            injectModifier.put("requestType", requestType.getTitle().toLowerCase());

            RtfTableModifier tableModifier = new RtfTableModifier();
            tableModifier.put(T1, data.toArray(new String[0][0]));

            injectModifier.modify(document);
            tableModifier.modify(document);
        }
        else if(CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT.equals(compensationType.getCode()))
        {
            if(groups2009Contract.isEmpty() && groups2013Contract.isEmpty() && groupsOksoContract.isEmpty())
                throw new ApplicationException("Нет данных для отчета");

            List<GroupContract> groups2009ContractList = Lists.newArrayList(groups2009Contract.values());
            Collections.sort(groups2009ContractList, GROUP_CONTRACT_COMPARATOR);
            List<GroupContract> groups2013ContractList = Lists.newArrayList(groups2013Contract.values());
            Collections.sort(groups2013ContractList, GROUP_CONTRACT_COMPARATOR);
            List<GroupContract> groupsOksoContractList = Lists.newArrayList(groupsOksoContract.values());
            Collections.sort(groupsOksoContractList, GROUP_CONTRACT_COMPARATOR);

            List<String[]> data = Lists.newArrayList();
            for(GroupContract group : groups2013ContractList)
            {
                data.add(group.getRow());
            }
            for(GroupContract group : groups2009ContractList)
            {
                data.add(group.getRow());
            }
            for(GroupContract group : groupsOksoContractList)
            {
                data.add(group.getRow());
            }

            RtfInjectModifier injectModifier = new RtfInjectModifier();
            injectModifier.put("academyTitle", TopOrgUnit.getInstance().getShortTitle());
            injectModifier.put("year", String.valueOf(model.getEnrollmentCampaign().getEducationYear().getIntValue()));
            injectModifier.put("programForm", programForm.getTitle());
            injectModifier.put("compensationType", "на места по договорам об оказании платных образовательных услуг");
            injectModifier.put("requestType", requestType.getTitle().toLowerCase());

            RtfTable table = tables.get(T2);
            document.getElementList().add(table);

            RtfTableModifier tableModifier = new RtfTableModifier();
            tableModifier.put(T2, data.toArray(new String[0][0]));

            injectModifier.modify(document);
            tableModifier.modify(document);
        }

//        throw new ApplicationException("under dev");
         return RtfUtil.toByteArray(document);
    }

    private void fillGroupBudgetByExtract(GroupBudget group, EnrEnrollmentExtract extract, EnrReportAdmissionResultsByEnrollmentStagesAddUI model, String compType, Boolean stateExam, Boolean markExclusive)
    {
        if (model.getParallelSelector().isParallelOnly() && !extract.getRequestedCompetition().isParallel()) return;
        if (model.getParallelSelector().isSkipParallel() && extract.getRequestedCompetition().isParallel()) return;

        if(extract.isCancelled() || !OrderStatesCodes.FINISHED.equals(extract.getOrder().getState().getCode()) || !EnrEntrantStateCodes.ENROLLED.equals(extract.getRequestedCompetition().getState().getCode())) return;

        Date commitDate = CoreDateUtils.getDayFirstTimeMoment(extract.getOrder().getCommitDate());
        if(model.getFirstStageDateFrom().equals(commitDate) || model.getFirstStageDateTo().equals(commitDate) ||
                (model.getFirstStageDateFrom().before(commitDate) && model.getFirstStageDateTo().after(commitDate)))
        {
            if(EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL.equals(compType))
            {
                group.firstStageWithoutExamMinisterial += 1;
            }
            else if(EnrCompetitionTypeCodes.EXCLUSIVE.equals(compType))
            {
                group.firstStageExclusive += 1;
                if(stateExam)
                    group.firstStageExclusiveStateExams += 1;
                if(markExclusive)
                    group.firstStageExclusiveMarkExclusive += 1;
            }
            else if(EnrCompetitionTypeCodes.TARGET_ADMISSION.equals(compType))
            {
                group.firstStageTA += 1;
                if(stateExam)
                    group.firstStageTAStateExams += 1;
                if(markExclusive)
                    group.firstStageTAMarkExclusive += 1;
            }
            else if(EnrCompetitionTypeCodes.MINISTERIAL.equals(compType))
            {
                group.firstStageMinisterial += 1;
                if(stateExam)
                    group.firstStageMinisterialStateExams += 1;
                if(markExclusive)
                    group.firstStageMinisterialMarkExclusive += 1;
            }
        }
        else if((model.getSecondStageDateFrom().equals(commitDate) || model.getSecondStageDateTo().equals(commitDate) ||
                (model.getSecondStageDateFrom().before(commitDate) && model.getSecondStageDateTo().after(commitDate))) && EnrCompetitionTypeCodes.MINISTERIAL.equals(compType))
        {
            group.secondStageMinisterial += 1;
            if(stateExam)
                group.secondStageMinisterialStateExams += 1;
            if(markExclusive)
                group.secondStageMinisterialMarkExclusive += 1;
        }
        else if((model.getExtStageDateFrom().equals(commitDate) || model.getExtStageDateTo().equals(commitDate) ||
                (model.getExtStageDateFrom().before(commitDate) && model.getExtStageDateTo().after(commitDate))) && EnrCompetitionTypeCodes.MINISTERIAL.equals(compType))
        {
            group.extStageMinisterial += 1;
            if(stateExam)
                group.extStageMinisterialStateExams += 1;
            if(markExclusive)
                group.extStageMinisterialMarkExclusive += 1;
        }
    }

    private void fillGroupContractByExtract(GroupContract group, EnrEnrollmentExtract extract, EnrReportAdmissionResultsByEnrollmentStagesAddUI model, String compType, Boolean stateExam, Boolean markExclusive)
    {
        if (model.getParallelSelector().isParallelOnly() && !extract.getRequestedCompetition().isParallel()) return;
        if (model.getParallelSelector().isSkipParallel() && extract.getRequestedCompetition().isParallel()) return;

        if(extract.isCancelled() || !OrderStatesCodes.FINISHED.equals(extract.getOrder().getState().getCode()) || !EnrEntrantStateCodes.ENROLLED.equals(extract.getRequestedCompetition().getState().getCode())) return;

        Date commitDate = CoreDateUtils.getDayFirstTimeMoment(extract.getOrder().getCommitDate());
        if(model.getFirstStageDateFrom().equals(commitDate) || model.getFirstStageDateTo().equals(commitDate) ||
                (model.getFirstStageDateFrom().before(commitDate) && model.getFirstStageDateTo().after(commitDate)))
        {
            if(EnrCompetitionTypeCodes.NO_EXAM_CONTRACT.equals(compType))
            {
                group.firstStageWithoutExams += 1;
            }
            else if(EnrCompetitionTypeCodes.CONTRACT.equals(compType))
            {
                group.firstStage += 1;
                if(stateExam)
                    group.firstStageStateExams += 1;
                if(markExclusive)
                    group.firstStageMarkExclusive += 1;
            }
        }
        else if(model.getSecondStageDateFrom().equals(commitDate) || model.getSecondStageDateTo().equals(commitDate) ||
                (model.getSecondStageDateFrom().before(commitDate) && model.getSecondStageDateTo().after(commitDate)))
        {
            if(EnrCompetitionTypeCodes.NO_EXAM_CONTRACT.equals(compType))
            {
                group.secondStageWithoutExams += 1;
            }
            else if(EnrCompetitionTypeCodes.CONTRACT.equals(compType))
            {
                group.secondStage += 1;
                if(stateExam)
                    group.secondStageStateExams += 1;
                if(markExclusive)
                    group.secondStageMarkExclusive += 1;
            }
        }
        else if(model.getExtStageDateFrom().equals(commitDate) || model.getExtStageDateTo().equals(commitDate) ||
                (model.getExtStageDateFrom().before(commitDate) && model.getExtStageDateTo().after(commitDate)))
        {
            if(EnrCompetitionTypeCodes.NO_EXAM_CONTRACT.equals(compType))
            {
                group.extStageWithoutExams += 1;
            }
            else if(EnrCompetitionTypeCodes.CONTRACT.equals(compType))
            {
                group.extStage += 1;
                if(stateExam)
                    group.extStageStateExams += 1;
                if(markExclusive)
                    group.extStageMarkExclusive += 1;
            }
        }
    }

    private EnrEntrantDQLSelectBuilder prepareEntrantDQL(EnrReportAdmissionResultsByEnrollmentStagesAddUI model, EnrCompetitionFilterAddon filterAddon, boolean fetch)
    {


        EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder(fetch, false)
                .filter(model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo())
                .filter(model.getEnrollmentCampaign())
                ;

        filterAddon.applyFilters(requestedCompDQL, requestedCompDQL.competition());

        requestedCompDQL.where(notIn(property(requestedCompDQL.reqComp(), EnrRequestedCompetition.state().code()), Lists.newArrayList(EnrEntrantStateCodes.TAKE_DOCUMENTS_AWAY, EnrEntrantStateCodes.ENTRANT_ARCHIVED)));

        return requestedCompDQL;
    }

    class GroupBudget
    {
        public GroupBudget(String groupCode, String groupTitle)
        {
            _groupCode = groupCode;
            _groupTitle = groupTitle;
        }

        public String _groupCode;
        public String _groupTitle; // Столбец 1 - название укрупненной группы

        public int competitions = 0; // Столбец 2 - количество выбранных конкурсов с видом возмещения затрат "бюджет";

        public int firstStageWithoutExamMinisterial = 0; //Столбец 3 - количество выбранных конкурсов с видом приема "без ВИ (КЦП)" (код 01) с датой подписания приказа, удовлетворяющей параметру "Первый этап зачисления"
        public int firstStageExclusive = 0; // Столбец 4 - количество выбранных конкурсов с видом приема "особ.права" (код 02) с датой подписания приказа, удовлетворяющей параметру "Первый этап зачисления"
        public int firstStageExclusiveStateExams = 0; // Столбец 5 - количество выбранных конкурсов из столбца 4, для которых все ВВИ сданы в форме "ЕГЭ" (т.е. у всех ВВИ источник балла (maxMarkForm) "ЕГЭ")
        public int firstStageExclusiveMarkExclusive = 0;// Столбец 6 - количество выбранных конкурсов из столбца 4, для которых есть хотя бы одно ВВИ с источником балла (maxMarkForm), для которого основание балла (особое право) имеет категорию "победители (призеры) ОШ" (код 025) или "победители (призеры) ВОШ" (код 023)

        public int firstStageTA = 0; // Столбец 7 - количество выбранных конкурсов с видом приема "ЦП" (код 03) с датой подписания приказа, удовлетворяющей параметру "Первый этап зачисления"
        public int firstStageTAStateExams = 0; // Столбец 8 - количество выбранных конкурсов из столбца 7, для которых все ВВИ сданы в форме "ЕГЭ" (т.е. у всех ВВИ источник балла (maxMarkForm) "ЕГЭ")
        public int firstStageTAMarkExclusive = 0; // Столбец 9 - количество выбранных конкурсов из столбца 7, для которых есть хотя бы одно ВВИ с источником балла (maxMarkForm), для которого основание балла (особое право) имеет категорию "победители (призеры) ОШ" (код 025) или "победители (призеры) ВОШ" (код 023)

        public int firstStageMinisterial = 0; // Столбец 10 - количество выбранных конкурсов с видом приема "общий" (код 04) с датой подписания приказа, удовлетворяющей параметру "Первый этап зачисления"
        public int firstStageMinisterialStateExams = 0; // Столбец 11 - количество выбранных конкурсов из столбца 10, для которых все ВВИ сданы в форме "ЕГЭ" (т.е. у всех ВВИ источник балла (maxMarkForm) "ЕГЭ")
        public int firstStageMinisterialMarkExclusive = 0; // Столбец 12 - количество выбранных конкурсов из столбца 10, для которых есть хотя бы одно ВВИ с источником балла (maxMarkForm), для которого основание балла (особое право) имеет категорию "победители (призеры) ОШ" (код 025) или "победители (призеры) ВОШ" (код 023)


        public int secondStageMinisterial = 0; // Столбец 13 - количество выбранных конкурсов с видом приема "общий" (код 04) с датой подписания приказа, удовлетворяющей параметру "Второй этап зачисления"
        public int secondStageMinisterialStateExams = 0; // Столбец 14 - количество выбранных конкурсов из столбца 13, для которых все ВВИ сданы в форме "ЕГЭ" (т.е. у всех ВВИ источник балла (maxMarkForm) "ЕГЭ")
        public int secondStageMinisterialMarkExclusive = 0; // Столбец 15 - количество выбранных конкурсов из столбца 13, для которых есть хотя бы одно ВВИ с источником балла (maxMarkForm), для которого основание балла (особое право) имеет категорию "победители (призеры) ОШ" (код 025) или "победители (призеры) ВОШ" (код 023)

        public int extStageMinisterial = 0; // Столбец 16 - количество выбранных конкурсов с видом приема "общий" (код 04) с датой подписания приказа, удовлетворяющей параметру "Дополнительный прием"
        public int extStageMinisterialStateExams = 0; // Столбец 17 - количество выбранных конкурсов из столбца 16, для которых все ВВИ сданы в форме "ЕГЭ" (т.е. у всех ВВИ источник балла (maxMarkForm) "ЕГЭ")
        public int extStageMinisterialMarkExclusive = 0; // Столбец 18 - количество выбранных конкурсов из столбца 16, для которых есть хотя бы одно ВВИ с источником балла (maxMarkForm), для которого основание балла (особое право) имеет категорию "победители (призеры) ОШ" (код 025) или "победители (призеры) ВОШ" (код 023)

        public String[] getRow()
        {
            List<String> columns = Lists.newArrayList(
                    _groupTitle,
                    String.valueOf(competitions),
                    String.valueOf(firstStageWithoutExamMinisterial),
                    String.valueOf(firstStageExclusive),
                    String.valueOf(firstStageExclusiveStateExams),
                    String.valueOf(firstStageExclusiveMarkExclusive),
                    String.valueOf(firstStageTA),
                    String.valueOf(firstStageTAStateExams),
                    String.valueOf(firstStageTAMarkExclusive),
                    String.valueOf(firstStageMinisterial),
                    String.valueOf(firstStageMinisterialStateExams),
                    String.valueOf(firstStageMinisterialMarkExclusive),
                    String.valueOf(secondStageMinisterial),
                    String.valueOf(secondStageMinisterialStateExams),
                    String.valueOf(secondStageMinisterialMarkExclusive),
                    String.valueOf(extStageMinisterial),
                    String.valueOf(extStageMinisterialStateExams),
                    String.valueOf(extStageMinisterialMarkExclusive));
            return columns.toArray(new String[18]);
        }
    }

    class GroupContract
    {
        public GroupContract(String groupCode, String groupTitle)
        {
            _groupCode = groupCode;
            _groupTitle = groupTitle;
        }

        public String _groupCode;
        public String _groupTitle; // Столбец 1 - название укрупненной группы

        public int competitions = 0; // Столбец 2 - количество выбранных конкурсов с видом возмещения затрат "бюджет";


        public int firstStageWithoutExams = 0; // Столбец 3 - количество выбранных конкурсов с видом приема "без ВИ (договор)" (код 05) с датой подписания приказа, удовлетворяющей параметру "Первый этап зачисления"
        public int firstStage = 0; // Столбец 4 - количество выбранных конкурсов с видом приема "договор" (код 06) с датой подписания приказа, удовлетворяющей параметру "Первый этап зачисления"
        public int firstStageStateExams = 0; // Столбец 5 - количество выбранных конкурсов из столбца 4, для которых все ВВИ сданы в форме "ЕГЭ" (т.е. у всех ВВИ источник балла (maxMarkForm) "ЕГЭ")
        public int firstStageMarkExclusive = 0; // Столбец 6 - количество выбранных конкурсов из столбца 4, для которых есть хотя бы одно ВВИ с источником балла (maxMarkForm), для которого основание балла (особое право) имеет категорию "победители (призеры) ОШ" (код 025) или "победители (призеры) ВОШ" (код 023)

        public int secondStageWithoutExams = 0; // Столбец 7 - количество выбранных конкурсов с видом приема "без ВИ (договор)" (код 05) с датой подписания приказа, удовлетворяющей параметру "Второй этап зачисления"
        public int secondStage = 0; // Столбец 8 - количество выбранных конкурсов с видом приема "договор" (код 06) с датой подписания приказа, удовлетворяющей параметру "Второй этап зачисления"
        public int secondStageStateExams = 0; // Столбец 9 - количество выбранных конкурсов из столбца 8, для которых все ВВИ сданы в форме "ЕГЭ" (т.е. у всех ВВИ источник балла (maxMarkForm) "ЕГЭ")
        public int secondStageMarkExclusive = 0; // Столбец 10 - количество выбранных конкурсов из столбца 7, для которых есть хотя бы одно ВВИ с источником балла (maxMarkForm), для которого основание балла (особое право) имеет категорию "победители (призеры) ОШ" (код 025) или "победители (призеры) ВОШ" (код 023)

        public int extStageWithoutExams = 0; // Столбец 11 - количество выбранных конкурсов с видом приема "без ВИ (договор)" (код 05) с датой подписания приказа, удовлетворяющей параметру "Дополнительный прием"
        public int extStage = 0; // Столбец 12 - количество выбранных конкурсов с видом приема "договор" (код 06) с датой подписания приказа, удовлетворяющей параметру "Дополнительный прием"
        public int extStageStateExams = 0; // Столбец 13 - количество выбранных конкурсов из столбца 12, для которых все ВВИ сданы в форме "ЕГЭ" (т.е. у всех ВВИ источник балла (maxMarkForm) "ЕГЭ")
        public int extStageMarkExclusive = 0; // Столбец 14 - количество выбранных конкурсов из столбца 12, для которых есть хотя бы одно ВВИ с источником балла (maxMarkForm), для которого основание балла (особое право) имеет категорию "победители (призеры) ОШ" (код 025) или "победители (призеры) ВОШ" (код 023)

        public String[] getRow()
        {
            List<String> columns = Lists.newArrayList(
                    _groupTitle,
                    String.valueOf(competitions),
                    String.valueOf(firstStageWithoutExams),
                    String.valueOf(firstStage),
                    String.valueOf(firstStageStateExams),
                    String.valueOf(firstStageMarkExclusive),
                    String.valueOf(secondStageWithoutExams),
                    String.valueOf(secondStage),
                    String.valueOf(secondStageStateExams),
                    String.valueOf(secondStageMarkExclusive),
                    String.valueOf(extStageWithoutExams),
                    String.valueOf(extStage),
                    String.valueOf(extStageStateExams),
                    String.valueOf(extStageMarkExclusive)
            );

            return columns.toArray(new String[14]);
        }
    }

    private RtfTable removeTable(RtfDocument document, String label)
    {
        RtfTable table = (RtfTable) UniRtfUtil.findElement(document.getElementList(), label);
        document.getElementList().remove(table);
        return table;
    }
}
