/**
 *$Id: IEnrEnrollmentOrderBasicDao.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentOrder.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;

import java.util.Collection;

/**
 * @author Alexander Shaburov
 * @since 05.07.13
 */
public interface IEnrEnrollmentOrderBasicDao extends INeedPersistenceSupport
{
    /**
     * Обновляет Основания для причин приказов о зачислении.
     * @param orderReasonId id Причины приказа
     * @param orderBasicIds список id'ов Оснований причины приказа
     */
    void updateOrderReasonToBasicsRel(long orderReasonId, Collection<Long> orderBasicIds);
}
