package ru.tandemservice.unienr14.rating.entity;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantOriginalDocumentStatus;
import ru.tandemservice.unienr14.rating.entity.gen.EnrRatingItemGen;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;

/**
 * Абитуриент в рейтинговом списке по конкурсу
 *
 * Определяет положение абитуриента в списке по конкурсу (согласно правилам ранжирования).
 */
public class EnrRatingItem extends EnrRatingItemGen implements ITitled
{

    public EnrRatingItem() {}

    public EnrRatingItem(EnrRequestedCompetition requestedCompetition) {
        this.setRequestedCompetition(requestedCompetition);
        this.setEntrant(requestedCompetition.getRequest().getEntrant());
        this.setCompetition(requestedCompetition.getCompetition());
    }

    public String getRatingAsString() {
        StringBuilder builder = new StringBuilder();
        builder.append(getPosition()).append(" (").append(getTotalMarkAsString()).append(")");
        return builder.toString();
    }

    @Override
    @EntityDSLSupport
    public String getTotalMarkAsString() {
        return EnrEntrantManager.DEFAULT_MARK_FORMATTER.format(getTotalMarkAsLong());
    }

    @Override
    @EntityDSLSupport
    public String getAchievementMarkAsString() {
        return EnrEntrantManager.DEFAULT_MARK_FORMATTER.format(getAchievementMarkAsLong());
    }

    @Override
    @EntityDSLSupport
    public String getTotalMarkAsStringReplacingZero() {
        return !isStatusEntranceExamsCorrect() || getTotalMarkAsLong() == 0 ? "—" : EnrEntrantManager.DEFAULT_MARK_FORMATTER.format(getTotalMarkAsLong());
    }

    @Override
    @EntityDSLSupport
    public String getEntranceSumAsStringReplacingZero() {
        return !isStatusEntranceExamsCorrect() || getTotalMarkAsLong() == 0 ? "—" : EnrEntrantManager.DEFAULT_MARK_FORMATTER.format(getTotalMarkAsLong() - getAchievementMarkAsLong());
    }


    public double getTotalMarkAsDouble() {
        return (double) getTotalMarkAsLong() / (double) 1000;
    }

    public double getAchievementMarkAsDouble() {
        return (double) getAchievementMarkAsLong() / (double) 1000;
    }

    // оставленно для совместимости (старое поле, возможно где-то в html осталось)
    public EnrEntrantOriginalDocumentStatus getOriginalDocRef() {
        return getRequestedCompetition().getRequest().getEduInstDocOriginalRef();
    }

    public boolean isOriginalIn() {
        return getRequestedCompetition().isOriginalDocumentHandedIn();
    }

    public boolean isStatusRatingPositiveSafe() {
        return Boolean.TRUE.equals(getStatusRatingPositive());
    }

    @Override
    @EntityDSLSupport
    public String getTitle() {
        if (getRequestedCompetition() == null) {
            return this.getClass().getSimpleName();
        }
        return getRequestedCompetition().getTitle()+", "+getEntrant().getFio() +", приоритет " + getRequestedCompetition().getPriority();
    }



}