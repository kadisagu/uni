/* $Id: SecLocalRoleAssign.java 6520 2015-05-18 12:42:03Z oleyba $ */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.ui.LocalRoleAssign;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.sec.bo.Sec.ui.RoleAssign.SecRoleAssignUI;
import org.tandemframework.shared.organization.sec.entity.RoleConfig;
import ru.tandemservice.unienr14.sec.entity.RoleAssignmentLocalEnrCampaign;
import ru.tandemservice.unienr14.sec.entity.RoleConfigLocalEnrCampaign;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Vasily Zhukov
 * @since 09.11.2011
 */
@Configuration
public class EnrEnrollmentCampaignLocalRoleAssign extends BusinessComponentManager
{
    public static final String LOCAL_ROLE_DS = "localRoleDS";
    public static final String DS_ENR_CAMPAIGN = EnrEnrollmentCampaignManager.DS_ENR_CAMPAIGN;

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
            .addDataSource(CommonBaseStaticSelectDataSource.selectDS(DS_ENR_CAMPAIGN, getName(), EnrEnrollmentCampaign.openSelectDSHandler(getName())))
            .addDataSource(selectDS(LOCAL_ROLE_DS, localRoleComboDSHandler()).addColumn(RoleConfigLocalEnrCampaign.role().title().s()))
            .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler localRoleComboDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), RoleConfigLocalEnrCampaign.class) {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(notIn(property(RoleAssignmentLocalEnrCampaign.id().fromAlias(alias)), new DQLSelectBuilder()
                    .fromEntity(RoleAssignmentLocalEnrCampaign.class, "r")
                    .column(property(RoleAssignmentLocalEnrCampaign.roleConfig().id().fromAlias("r")))
                    .where(eq(property(RoleAssignmentLocalEnrCampaign.principalContext().id().fromAlias("r")), value(context.<Long>get(SecRoleAssignUI.BIND_PRINCIPAL_CONTEXT_ID))))
                    .buildQuery()
                ));
            }
        }
            .where(RoleConfigLocalEnrCampaign.role().active(), Boolean.TRUE)
            .where(RoleConfigLocalEnrCampaign.enrollmentCampaign(), EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, true)
            .filter(RoleConfig.role().title())
            .order(RoleConfigLocalEnrCampaign.role().title());
    }
}
