/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.utils;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.ICommonFilterItem;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * @author rsizonenko
 * @since 30.05.2014
 */
public class FilterParametersPrinter {

    private EnrReportDateSelector dateSelector;
    private EnrReportStageSelector stageSelector;
    private EnrReportParallelSelector parallelSelector;

    public static final String RTF_LINE = "\\line ";

    private List<String[]> variableParameters = new ArrayList<>();

    /** @return Возвращает таблицу без указания приемной кампании:
     * Начиная с "даты с...", заканчивая фильтрами
     * Если у объекта FilterParameterPrinter определены селекторы либо доп. параметры, они так же будут напечатаны.
     *
    * */
    public List<String[]> getTable(EnrCompetitionFilterAddon filterAddon, Date dateFrom, Date dateTo)
    {
        ArrayList<String[]> hTable = new ArrayList<>();

        hTable.add(new String[]{"Заявления добавлены с", new SimpleDateFormat("dd.MM.yyyy").format(dateFrom)});
        hTable.add(new String[]{"Заявления добавлены по", new SimpleDateFormat("dd.MM.yyyy").format(dateTo)});

        if (getStageSelector() != null)
            hTable.add(new String[]{"Стадия приемной кампании", getStageSelector().getStage().getTitle()});



        for (ICommonFilterItem item : filterAddon.getFilterItemList())
        {
            if (item.getValue() == null)
                continue;
            if (!item.isEnableCheckboxChecked())
                continue;


            // EduProgramSubject выводится не тайтл, а тайтл с кодом, поэтому для них особый способ обработки
            if (item.getSettingsName().equals(EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT))
            {
                if (item.getValue() instanceof EduProgramSubject) {
                    hTable.add(new String[]{item.getDisplayName(),new RtfBackslashScreener().screenBackslashes(((EduProgramSubject) item.getValue()).getTitleWithCode()) + " "});
                }
                if (item.getValue() instanceof Collection)
                    if (((Collection) item.getValue()).size() > 0) {
                        hTable.add(new String[]{item.getDisplayName(), new RtfBackslashScreener().screenBackslashes(UniStringUtils.join((List<EduProgramSubject>) item.getValue(), EduProgramSubject.titleWithCode().s(), RTF_LINE)) + " "});
                    }
            }
            else if (item.getSettingsName().equals(EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM))
            {
                if (item.getValue() instanceof EduProgramProf) {
                    hTable.add(new String[]{item.getDisplayName(), new RtfBackslashScreener().screenBackslashes(((EduProgramProf) item.getValue()).getTitleWithCodeAndConditionsShortWithForm()) + " "});
                }
                if (item.getValue() instanceof Collection)
                    if (((Collection) item.getValue()).size() > 0) {
                        hTable.add(new String[]{item.getDisplayName(), new RtfBackslashScreener().screenBackslashes(UniStringUtils.join((List<EduProgramProf>) item.getValue(), EduProgramProf.titleWithCodeAndConditionsShortWithForm().s(), RTF_LINE)) + " "});
                    }
            }
            else if (item.getSettingsName().equals(EnrCompetitionFilterAddon.SETTING_NAME_COMPETITION_TYPE))
            {
                if (item.getValue() instanceof EnrCompetitionType) {
                    hTable.add(new String[]{item.getDisplayName(), new RtfBackslashScreener().screenBackslashes(((EnrCompetitionType) item.getValue()).getPrintTitle()) + " "});
                }
                if (item.getValue() instanceof Collection)
                    if (((Collection) item.getValue()).size() > 0) {
                        hTable.add(new String[]{item.getDisplayName(), new RtfBackslashScreener().screenBackslashes(UniStringUtils.join((List<EnrCompetitionType>) item.getValue(), EnrCompetitionType.printTitle().s(), RTF_LINE)) + " "});
                    }
            }
            else
            {
                if (item.getValue() instanceof ITitled) {
                    StringBuilder builder = new StringBuilder().append(((ITitled) item.getValue()).getTitle());
                    for (int i = 0; i < builder.length(); i++) {
                        if (builder.charAt(i) == '\\') {
                            builder.insert(i, '\\');
                            i++;
                        }
                    }
                    hTable.add(new String[]{item.getDisplayName(), builder.toString() + " "});
                }
                if (item.getValue() instanceof Collection)
                    if (((Collection) item.getValue()).size() > 0) {
                        hTable.add(new String[]{item.getDisplayName(), new RtfBackslashScreener().screenBackslashes(UniStringUtils.join((List<IEntity>) item.getValue(), "title", RTF_LINE)) + " "});
                    }
            }
        }


        if (getParallelSelector() != null && getParallelSelector().isParallelActive())
            hTable.add(new String[]{"Поступающие параллельно", getParallelSelector().getParallel().getTitle()});

        for (String[] row : variableParameters)
        {
            hTable.add(row);
        }
        return hTable;
    }

    public List<String[]> getTable(EnrCompetitionFilterAddon filterAddon, EnrReportDateSelector dateSelector)
    {
        return getTable(filterAddon, dateSelector.getDateFrom(), dateSelector.getDateTo());
    }

    /** @return Возвращает таблицу с указанием приемной компании
     * Если у объекта FilterParameterPrinter определены селекторы либо доп. параметры, они так же будут напечатаны.
    */
    public List<String[]> getTable(EnrCompetitionFilterAddon filterAddon, Date dateFrom, Date dateTo, EnrEnrollmentCampaign campaign)
    {
        List<String[]> hTable = getTable(filterAddon, dateFrom, dateTo);
        hTable.add(0, new String[] {"Приемная Кампания", campaign.getTitle()});
        return hTable;
    }


/** @return Возвращает таблицу для НЕ RTF документов с указанием приемной компании
 * Если у объекта FilterParameterPrinter определены селекторы либо доп. параметры, они так же будут напечатаны.
    */
    public List<String[]> getTableForNonRtf(EnrCompetitionFilterAddon filterAddon, Date dateFrom, Date dateTo, EnrEnrollmentCampaign campaign)
    {
        List<String[]> hTable = new ArrayList<>();
        hTable.add(new String[] {"Приемная Кампания", campaign.getTitle()});
        hTable.addAll(getTableForNonRtf(filterAddon, dateFrom, dateTo));
        return hTable;
    }

    /** @return возвращает таблицу без переносов кодом \line, а разделенную просто ";"
        Если у объекта FilterParameterPrinter определены селекторы либо доп. параметры, они так же будут напечатаны.
    */
    public List<String[]> getTableForNonRtf(EnrCompetitionFilterAddon filterAddon, Date dateFrom, Date dateTo)
    {
        ArrayList<String[]> hTable = new ArrayList<>();


        hTable.add(new String[]{"Заявления добавлены с", new SimpleDateFormat("dd.MM.yyyy").format(dateFrom)});
        hTable.add(new String[]{"Заявления добавлены по", new SimpleDateFormat("dd.MM.yyyy").format(dateTo)});

        if (getStageSelector() != null)
            hTable.add(new String[]{"Стадия приемной кампании", getStageSelector().getStage().getTitle()});


        for (ICommonFilterItem item : filterAddon.getFilterItemList())
        {
            if (item.getValue() == null)
                continue;
            if (!item.isEnableCheckboxChecked())
                continue;
            if (item.getSettingsName().equals(EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT))
            {
                if (item.getValue() instanceof EduProgramSubject) {
                    hTable.add(new String[]{item.getDisplayName(), ((EduProgramSubject) item.getValue()).getTitleWithCode()});
                }
                if (item.getValue() instanceof Collection)
                    if (((Collection) item.getValue()).size() > 0) {
                        hTable.add(new String[]{item.getDisplayName(), (UniStringUtils.join((List<EduProgramSubject>) item.getValue(), EduProgramSubject.titleWithCode().s(), "; "))});
                    }
            }
            else if (item.getSettingsName().equals(EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM))
            {
                if (item.getValue() instanceof EduProgramProf) {
                    hTable.add(new String[]{item.getDisplayName(),((EduProgramProf) item.getValue()).getTitleWithCodeAndConditionsShortWithForm()});
                }
                if (item.getValue() instanceof Collection)
                    if (((Collection) item.getValue()).size() > 0) {
                        hTable.add(new String[]{item.getDisplayName(), (UniStringUtils.join((List<EduProgramProf>) item.getValue(), EduProgramProf.titleWithCodeAndConditionsShortWithForm().s(), "; "))});
                    }
            }
            else if (item.getSettingsName().equals(EnrCompetitionFilterAddon.SETTING_NAME_COMPETITION_TYPE))
            {
                if (item.getValue() instanceof EnrCompetitionType) {
                    hTable.add(new String[]{item.getDisplayName(), (((EnrCompetitionType) item.getValue()).getPrintTitle())});
                }
                if (item.getValue() instanceof Collection)
                    if (((Collection) item.getValue()).size() > 0) {
                        hTable.add(new String[]{item.getDisplayName(), (UniStringUtils.join((List<EnrCompetitionType>) item.getValue(), EnrCompetitionType.printTitle().s(), "; "))});
                    }
            }
            else {
                if (item.getValue() instanceof ITitled)
                    hTable.add(new String[]{item.getDisplayName(), ((ITitled) item.getValue()).getTitle()});
                if (item.getValue() instanceof Collection)
                    if (((Collection) item.getValue()).size() > 0)
                        hTable.add(new String[]{item.getDisplayName(), UniStringUtils.join((List<IEntity>) item.getValue(), "title", "; ")});
            }
        }

        if (getParallelSelector() != null && getParallelSelector().isParallelActive())
            hTable.add(new String[]{"Поступающие параллельно", getParallelSelector().getParallel().getTitle()});

        for (String[] row : variableParameters)
        {
            hTable.add(row);
        }

        return hTable;
    }

    public List<String[]> getTableForNonRtf(EnrCompetitionFilterAddon filterAddon, EnrReportDateSelector dateSelector)
    {
        return getTableForNonRtf(filterAddon, dateSelector.getDateFrom(), dateSelector.getDateTo());
    }

    public EnrReportDateSelector getDateSelector() {
        return dateSelector;
    }

    public FilterParametersPrinter setDateSelector(EnrReportDateSelector dateSelector) {
        this.dateSelector = dateSelector;
        return this;
    }

    public EnrReportStageSelector getStageSelector() {
        return stageSelector;
    }

    public FilterParametersPrinter setStageSelector(EnrReportStageSelector stageSelector) {
        this.stageSelector = stageSelector;
        return this;
    }

    public EnrReportParallelSelector getParallelSelector() {
        return parallelSelector;
    }

    public FilterParametersPrinter setParallelSelector(EnrReportParallelSelector parallelSelector) {
        this.parallelSelector = parallelSelector;
        return this;
    }

    // Добавляет в конец таблицы дополнительные строки
    public FilterParametersPrinter addVariableParameter(String parameter, String value)
    {
        variableParameters.add(new String[]{parameter, value});
        return this;
    }
}
