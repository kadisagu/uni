/* $Id:$ */
package ru.tandemservice.unienr14.competition.entity;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

/**
 * @author oleyba
 * @since 8/15/14
 */
public interface IEnrollmentStudentSettings extends ITitled, IEntity
{
    String getProgramTitle();

    EnrProgramSetBase getProgramSet();
    EnrOrgUnit getOrgUnit();
    EduProgramHigherProf getProgram();
    OrgUnit getFormativeOrgUnit();

    EducationOrgUnit getEducationOrgUnit();
    void setEducationOrgUnit(EducationOrgUnit orgUnit);
}
