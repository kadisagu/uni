/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsTargetAdmInfoAdd.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsTargetAdmInfoAdd.EnrReportEntrantsTargetAdmInfoAddUI;

/**
 * @author rsizonenko
 * @since 20.06.2014
 */
public interface IEnrReportEntrantsTargetAdmInfoDao extends INeedPersistenceSupport {
    long createReport(EnrReportEntrantsTargetAdmInfoAddUI model);
}
