/* $Id$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import ru.tandemservice.unienr14.competition.entity.CustomerTargetContractTraining;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 12.05.2016
 */
public class ExternalOrgUnitDSHandler extends DefaultComboDataSourceHandler
{
    public static final String PROGRAM_SET = "targetContractProgramSet";

    public ExternalOrgUnitDSHandler(String ownerId)
    {
        super(ownerId, ExternalOrgUnit.class);
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {
        super.prepareConditions(ep);
        List<EnrProgramSetBase> programSets = ep.context.get(PROGRAM_SET);

        if (programSets != null && !programSets.isEmpty())
        {
            ep.dqlBuilder.where(in(property(ExternalOrgUnit.id().fromAlias("e")),
                                   new DQLSelectBuilder().fromEntity(CustomerTargetContractTraining.class, "cou")
                                           .where(in(property("cou", CustomerTargetContractTraining.programSet()), programSets))
                                           .column(property("cou", CustomerTargetContractTraining.externalOrgUnit().id()))
                                           .buildQuery()));
        }


    }
}