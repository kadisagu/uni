package ru.tandemservice.unienr14.competition.daemon;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;

/**
 * @author vdanilov
 */
public interface IEnrCompetitionDaemonDao {

    final SpringBeanCache<IEnrCompetitionDaemonDao> instance = new SpringBeanCache<IEnrCompetitionDaemonDao>(IEnrCompetitionDaemonDao.class.getName());

    /**
     * Обновляет настройки наборов ВИ по данным наборов ВИ
     * добавляет недостающие элементы в настройку, зачетный бал берется из дисциплины, формы сдачи не указываются
     * @return true, если что-то было изменено
     */
    @Transactional(propagation=Propagation.REQUIRED)
    boolean doUpdateExamSetVariant();


}
