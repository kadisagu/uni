package ru.tandemservice.unienr14.rating.entity;

import org.tandemframework.core.common.IEntityDebugTitled;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.fis.IFisUidByIdOwner;
import ru.tandemservice.unienr14.rating.entity.gen.EnrChosenEntranceExamGen;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline;

/**
 * Выбранное вступительное испытание (ВВИ)
 */
public class EnrChosenEntranceExam extends EnrChosenEntranceExamGen implements IEntityDebugTitled, IFisUidByIdOwner
{
    public EnrChosenEntranceExam()
    {
    }

    public EnrChosenEntranceExam(EnrRequestedCompetition requestedCompetition, EnrCampaignDiscipline discipline)
    {
        setRequestedCompetition(requestedCompetition);
        setDiscipline(discipline);
    }

    public EnrEntrantMarkSource getMarkSource() {
        return getMaxMarkForm() == null ? null : getMaxMarkForm().getMarkSource();
    }

    public Double getMarkAsDouble() {
        if (getMaxMarkForm() == null)
            return null;
        else if (getMaxMarkForm().getMarkSource() == null)
            return null;
        return getMaxMarkForm().getMarkAsDoubleNullSafe();
    }

    @Override public String getEntityDebugTitle() {
        return getRequestedCompetition().getTitle() + ":  " + getDiscipline().getTitle();
    }

    @Override
    public String getFisUidTitle()
    {
        return "ВВИ: " + getRequestedCompetition().getTitle() + ", " + getDiscipline().getTitle() + ", " + getEntrant().getFullFio();
    }

    public EnrEntrant getEntrant() {
        return getRequestedCompetition().getRequest().getEntrant();
    }
}