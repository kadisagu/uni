/* $Id:$ */
package ru.tandemservice.unienr14.base.ext.GlobalReport.ui.List;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.View.EnrReportPersonView;


/**
 * @author oleyba
 * @since 12/11/13
 */
public class EnrGlobalReportListAddon extends UIAddon
{
    public static final String NAME = "enr14GlobalReportListAddon";

    public EnrGlobalReportListAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }
}
