/* $Id: IEcgpQuotaFreeDTO.java 25114 2012-12-03 11:25:11Z vdanilov $ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.util;

import java.util.Map;

/**
 * Свободные места по профилям
 *
 * @author Vasily Zhukov
 * @since 10.05.2012
 */
public interface IEnrPAQuotaFreeWrapper
{
    /**
     * @return планы приема по профилям
     */
    IEnrPAQuotaWrapper getQuota();

    /**
     * @return занятые места по профилям
     */
    IEnrPAQuotaUsedWrapper getQuotaUsed();

    /**
     * @return суммарное количество свободных мест
     */
    int getTotalFree();

    /**
     * @return профиль -> свободно мест
     *         если свободно мест равно null, то значит план по профилю не ограничен (свободные места всегда есть)
     */
    Map<Long, Integer> getFreeMap();
}
