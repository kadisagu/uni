/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubEduDocumentTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.DefaultPublisherLinkResolver;
import org.tandemframework.core.view.util.Icon;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubEduDocumentTab.logic.EnrPersonEduDocumentDSHandler;

/**
 * @author oleyba
 * @since 4/22/13
 */
@Configuration
public class EnrEntrantPubEduDocumentTab extends BusinessComponentManager
{
    public static final String DS_EDU_DOCUMENT = "eduDocumentDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
            .addDataSource(searchListDS(DS_EDU_DOCUMENT, eduDocumentDSColumns(), eduDocumentDSHandler()))
            .create();
    }

    @Bean
    public ColumnListExtPoint eduDocumentDSColumns()
    {
        return columnListExtPointBuilder(DS_EDU_DOCUMENT)
            .addColumn(textColumn("documentKind", PersonEduDocument.documentKindTitle()))
            .addColumn(publisherColumn("fullNumber").path(PersonEduDocument.fullNumber()).publisherLinkResolver(
                    new DefaultPublisherLinkResolver() {
                        @Override public Object getParameters(IEntity entity) {
                            return entity.getProperty("pubParameters");
                        }
                }))
            .addColumn(textColumn("eduOrganizationAddressItem", PersonEduDocument.eduOrganizationAddressItem().addressTitle()))
            .addColumn(textColumn("eduOrganization", PersonEduDocument.eduOrganization()))
            .addColumn(textColumn("eduLevel", PersonEduDocument.eduLevel().title()))
            .addColumn(textColumn("documentEducationLevel", PersonEduDocument.documentEducationLevel()))
            .addColumn(textColumn("qualification", PersonEduDocument.qualification()))
            .addColumn(textColumn("yearEnd", PersonEduDocument.yearEnd()))
            .addColumn(textColumn("issuanceDate", PersonEduDocument.issuanceDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER))
			.addColumn(toggleColumn("toggleMainDocument", EnrPersonEduDocumentDSHandler.VIEW_MAIN_DOCUMENT).permissionKey("ui:secModel.toggleMainEduDocument")
							   .toggleOnListener("onToggleOnMainDocument").toggleOnListenerAlert(alert("Сделать основным документом об образовании?"))
							   .toggleOffListener("onToggleOffMainDocument"))

			.addColumn(publisherColumn("requestList", "title")
							   .entityListProperty(EnrPersonEduDocumentDSHandler.VIEW_REQUEST_NUMBER)
							   .parameters("mvel:['selectedTab':'requestTab']")
							   .formatter(CollectionFormatter.COLLECTION_FORMATTER))
            .addColumn(toggleColumn("original", EnrPersonEduDocumentDSHandler.VIEW_ORIGINAL).required(Boolean.TRUE)
                    .toggleOnLabel("eduDocumentDS.original.yes").toggleOnListener("onClickSwitchOriginalStatus")
							   .toggleOffLabel("eduDocumentDS.original.no").toggleOffListener("onClickSwitchOriginalStatus")
							   .permissionKey("ui:secModel.editEduInstitution").visible("ui:originalVisible"))
            .addColumn(booleanColumn("originalVal", EnrPersonEduDocumentDSHandler.VIEW_ORIGINAL).required(Boolean.TRUE).visible("ui:reqCompVisible"))
			.addColumn(textColumn("reqComp", EnrPersonEduDocumentDSHandler.VIEW_REQ_COMP + ".title").visible("ui:reqCompVisible"))
			.addColumn(textColumn("organizationOriginalIn", EnrPersonEduDocumentDSHandler.VIEW_ORG_ORIGINAL_IN))

			.addColumn(actionColumn("print", new Icon("printer"), "onClickScanCopyDownload").disabled(PersonEduDocument.P_NO_SCAN_COPY).permissionKey("ui:secModel.printEduInstitution"))
			.addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, "onClickEditDocument").permissionKey("ui:secModel.editEduInstitution"))
			.addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, "onClickDelete")
							   .alert(alert("eduDocumentDS.delete.alert", "title"))
							   .permissionKey("ui:secModel.deleteEduInstitution"))
            .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler eduDocumentDSHandler()
    {
        return new EnrPersonEduDocumentDSHandler(getName());
    }
}
