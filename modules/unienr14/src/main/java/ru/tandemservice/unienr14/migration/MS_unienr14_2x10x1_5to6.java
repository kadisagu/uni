/* $Id$ */
package ru.tandemservice.unienr14.migration;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;

import java.sql.PreparedStatement;

/**
 * @author Ekaterina Zvereva
 * @since 16.05.2016
 */
public class MS_unienr14_2x10x1_5to6 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.10.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {

        String[] titles = {
                "Всероссийская олимпиада школьников \"Нанотехнологии - прорыв в будущее\"",
                "Всероссийский конкурс научных работ школьников \"Юниор\"",
                "Всероссийский турнир юных физиков",
                "Всесибирская открытая олимпиада школьников",
                "Герценовская олимпиада школьников",
                "Городская открытая олимпиада школьников по физике",
                "Инженерная олимпиада школьников",
                "Интернет-олимпиада школьников по физике",
                "Кутафинская олимпиада школьников по праву",
                "Междисциплинарная олимпиада школьников имени В.И. Вернадского",
                "Межрегиональная олимпиада Казанского (Приволжского) федерального университета",
                "Межрегиональная олимпиада МПГУ для школьников",
                "Межрегиональная олимпиада по информатике и компьютерной безопасности",
                "Межрегиональная олимпиада по праву \"ФЕМИДА\"",
                "Межрегиональная олимпиада школьников \"Будущие исследователи - будущее науки\"",
                "Межрегиональная олимпиада школьников \"Высшая проба\"",
                "Межрегиональная олимпиада школьников \"Евразийская лингвистическая олимпиада\"",
                "Межрегиональная олимпиада школьников им. В.Е. Татлина",
                "Межрегиональная олимпиада школьников на базе ведомственных образовательных учреждений",
                "Межрегиональная олимпиада школьников по математике и криптографии",
                "Межрегиональная отраслевая олимпиада школьников \"Паруса надежды\"",
                "Межрегиональная химическая олимпиада школьников имени академика П.Д. Саркисова",
                "Межрегиональная экономическая олимпиада школьников имени Н.Д. Кондратьева",
                "Межрегиональный экономический фестиваль школьников \"Сибириада. Шаг в мечту\"",
                "Многопредметная олимпиада \"Юные таланты\"",
                "Многопредметная олимпиада СКФУ \"45 параллель\"",
                "Многопрофильная инженерная олимпиада \"Звезда\"",
                "Многопрофильная олимпиада \"Аксиос\"",
                "Московская олимпиада школьников",
                "Общероссийская олимпиада школьников \"Основы православной культуры\"",
                "Объединенная межвузовская математическая олимпиада школьников",
                "Объединенная международная математическая олимпиада \"Формула Единства\"/\"Третье тысячелетие\"",
                "Олимпиада Курчатов",
                "Олимпиада МГИМО МИД России для школьников",
                "Олимпиада по дискретной математике и теоретической информатике",
                "Олимпиада по комплексу предметов \"Культура и искусство\"",
                "Олимпиада Российской академии народного хозяйства и государственной службы при Президенте Российской Федерации",
                "Олимпиада школьников \"Будущее с нами\"",
                "Олимпиада школьников \"Государственный аудит\"",
                "Олимпиада школьников \"Кодекс знаний\"",
                "Олимпиада школьников \"Ломоносов\"",
                "Олимпиада школьников \"Надежда энергетики\"",
                "Олимпиада школьников \"Покори Воробьевы горы!\"",
                "Олимпиада школьников \"Россия в электронном мире\"",
                "Олимпиада школьников \"Учись строить будущее\"",
                "Олимпиада школьников \"Физтех\"",
                "Олимпиада школьников \"Шаг в будущее\"",
                "Олимпиада школьников по информатике и программированию",
                "Олимпиада школьников Санкт-Петербургского государственного университета",
                "Олимпиада юношеской математической школы",
                "Открытая всероссийская интеллектуальная олимпиада \"Наше наследие\"",
                "Открытая межвузовская олимпиада школьников Сибирского Федерального округа \"Будущее Сибири\"",
                "Открытая олимпиада школьников \"Информационные технологии\"",
                "Открытая олимпиада школьников по математике",
                "Открытая олимпиада школьников по программированию",
                "Открытая региональная межвузовская олимпиада вузов Томской области (ОРМО)",
                "Отраслевая физико-математическая олимпиада школьников \"Росатом\"",
                "Плехановская олимпиада школьников",
                "Региональная олимпиада школьников \"Архитектура и искусство\"",
                "Региональный конкурс школьников Челябинского университетского образовательного округа",
                "Санкт-Петербургская астрономическая олимпиада",
                "Санкт-Петербургская олимпиада школьников",
                "Северо-Восточная олимпиада школьников",
                "Сибирская межрегиональная олимпиада школьников \"Архитектурно-дизайнерское творчество\"",
                "Строгановская олимпиада на базе МГХПА им. С.Г. Строганова",
                "Телевизионная гуманитарная олимпиада школьников \"Умницы и умники\"",
                "Турнир городов",
                "Турнир имени М.В. Ломоносова",
                "Учитель школы будущего",
                "Филологическая олимпиада школьников",
                "Южно-Российская межрегиональная олимпиада школьников \"Архитектура и искусство\""

        };

        PreparedStatement insert = tool.prepareStatement("insert into enr14_c_olymp_t (id, discriminator, code_p, title_p ,eduyear_id, olympiadtype_id) values (?, ?, ?, ?, ?, ?)");
        PreparedStatement update = tool.prepareStatement("update enr14_c_olymp_t set code_p = ?, olympiadtype_id = ? where title_p=? and eduyear_id = ?");

        short entityCode = tool.entityCodes().ensure("enrOlympiad");
        Long eduYearId = (Long) tool.getUniqueResult("select id from educationyear_t where code_p='16'");

        //Всероссийская олимпиада школьников
        Long olympiadTypeId = (Long) tool.getUniqueResult("select id from enr14_c_olymp_type_t where code_p='1'");
        String code = "2016_01";
        String title = "Всероссийская олимпиада школьников";

        Long id = (Long) tool.getUniqueResult("select id from enr14_c_olymp_t where title_p=? and eduyear_id = ?", title, eduYearId);
        if (null != id)
        {
            update.setString(1, code);
            update.setLong(2, olympiadTypeId);
            update.setString(3, title);
            update.setLong(4, eduYearId);
            update.executeUpdate();
        }
        else
        {
            insert.setLong(1, EntityIDGenerator.generateNewId(entityCode));
            insert.setShort(2, entityCode);
            insert.setString(3, code);
            insert.setString(4, title);
            insert.setLong(5, eduYearId);
            insert.setLong(6, olympiadTypeId);
            insert.executeUpdate();
        }

         olympiadTypeId = (Long) tool.getUniqueResult("select id from enr14_c_olymp_type_t where code_p='2'");

        //Олимпиады школьников из перечня Минобрнауки
        for (int i = 0; i < titles.length; i++)
        {
            code = "2016_" + StringUtils.leftPad(String.valueOf(i + 2), 2, "0");

            id = (Long) tool.getUniqueResult("select id from enr14_c_olymp_t where title_p=? and eduyear_id = ?", titles[i], eduYearId);
            if (null != id)
            {
                update.setString(1, code);
                update.setLong(2, olympiadTypeId);
                update.setString(3, titles[i]);
                update.setLong(4, eduYearId);
                update.executeUpdate();
            }
            else
            {
                insert.setLong(1, EntityIDGenerator.generateNewId(entityCode));
                insert.setShort(2, entityCode);
                insert.setString(3, code);
                insert.setString(4, titles[i]);
                insert.setLong(5, eduYearId);
                insert.setLong(6, olympiadTypeId);
                insert.executeUpdate();
            }
        }


    }

}