/**
 *$Id: IDAO.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.component.catalog.enrEntrantState.EnrEntrantStatePub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.IDefaultCatalogPubDAO;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantState;

/**
 * @author Alexander Shaburov
 * @since 14.06.13
 */
public interface IDAO extends IDefaultCatalogPubDAO<EnrEntrantState, Model>
{
}
