package ru.tandemservice.unienr14.migration;

import com.google.common.collect.Maps;
import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.*;
import org.tandemframework.hibsupport.EntityIDGenerator;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;
import org.tandemframework.shared.person.migration.MS_person_1x11x4_0to1;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unienr14_2x11x4_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18")
                };
    }

    @Override
    public ScriptDependency[] getAfterDependencies()
    {
        return new ScriptDependency[]
                {
                        MigrationUtils.createScriptDependency(MS_person_1x11x4_0to1.class)
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        /** Константа кода (code) элемента : Удостоверение личности (title) */
        String INDENTITY_CARD = "1";
        /** Константа кода (code) элемента : Документ о полученном образовании (title) */
        String EDU_INSTITUTION = "4";

        ////////////////////////////////////////////////////////////////////////////////
        // сущность personDocument

        // создана новая сущность
        {
            if (!tool.tableExists("p_doc_t"))
            {
                // создать таблицу
                // создать таблицу
                DBTable dbt = new DBTable("p_doc_t",
                        new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_persondocument"),
                        new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                        new DBColumn("version", DBType.INTEGER).setNullable(false),
                        new DBColumn("person_id", DBType.LONG).setNullable(false),
                        new DBColumn("documenttype_id", DBType.LONG).setNullable(false),
                        new DBColumn("seria_p", DBType.createVarchar(255)),
                        new DBColumn("number_p", DBType.createVarchar(255)),
                        new DBColumn("registrationdate_p", DBType.TIMESTAMP).setNullable(false),
                        new DBColumn("expirationdate_p", DBType.DATE),
                        new DBColumn("issuancedate_p", DBType.DATE),
                        new DBColumn("issuanceplace_p", DBType.createVarchar(255)),
                        new DBColumn("description_p", DBType.createVarchar(255)),
                        new DBColumn("comment_p", DBType.createVarchar(2000)),
                        new DBColumn("scancopy_p", DBType.LONG),
                        new DBColumn("documentcategory_id", DBType.LONG)
                );
                tool.createTable(dbt);
            }

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("personDocument");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность personDocumentCategory

        // создана новая сущность
        {
            if (!tool.tableExists("p_doc_cat_t"))
            {
                // создать таблицу
                DBTable dbt = new DBTable("p_doc_cat_t",
                        new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_persondocumentcategory"),
                        new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                        new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
                        new DBColumn("shorttitle_p", DBType.createVarchar(255)).setNullable(false),
                        new DBColumn("priority_p", DBType.INTEGER).setNullable(false),
                        new DBColumn("documenttype_id", DBType.LONG).setNullable(false),
                        new DBColumn("title_p", DBType.createVarchar(1200)).setNullable(false),
                        new DBColumn("usercode_p", DBType.createVarchar(255)),
                        new DBColumn("disableddate_p", DBType.TIMESTAMP)
                );
                tool.createTable(dbt);
            }

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("personDocumentCategory");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность personDocumentRoleRel

        // создана новая сущность
        {
            if (!tool.tableExists("p_doc_role_rel_t"))
            {
                // создать таблицу
                DBTable dbt = new DBTable("p_doc_role_rel_t",
                        new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_persondocumentrolerel"),
                        new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                        new DBColumn("document_id", DBType.LONG).setNullable(false),
                        new DBColumn("role_id", DBType.LONG).setNullable(false)
                );
                tool.createTable(dbt);
            }

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("personDocumentRoleRel");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность personDocumentType

        // создана новая сущность
        {
            if (!tool.tableExists("p_doc_type_t"))
            {
                // создать таблицу
                DBTable dbt = new DBTable("p_doc_type_t",
                        new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_persondocumenttype"),
                        new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                        new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
                        new DBColumn("shorttitle_p", DBType.createVarchar(255)).setNullable(false),
                        new DBColumn("priority_p", DBType.INTEGER).setNullable(false),
                        new DBColumn("title_p", DBType.createVarchar(1200)).setNullable(false),
                        new DBColumn("fake_p", DBType.BOOLEAN).setNullable(false),
                        new DBColumn("usercode_p", DBType.createVarchar(255)),
                        new DBColumn("disableddate_p", DBType.TIMESTAMP)
                );
                tool.createTable(dbt);
            }

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("personDocumentType");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность personDocumentTypeRight

        // создана новая сущность
        {
            if (!tool.tableExists("p_doc_t_r_t"))
            {
                // создать таблицу
                DBTable dbt = new DBTable("p_doc_t_r_t",
                        new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_persondocumenttyperight"),
                        new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                        new DBColumn("documentcontext_p", DBType.createVarchar(255)).setNullable(false),
                        new DBColumn("documenttype_id", DBType.LONG).setNullable(false),
                        new DBColumn("viewandlink_p", DBType.BOOLEAN).setNullable(false),
                        new DBColumn("createandedit_p", DBType.BOOLEAN).setNullable(false)
                );
                tool.createTable(dbt);
            }
            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("personDocumentTypeRight");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность personDocumentTypeProperty

        // создана новая сущность
        {
            if(!tool.tableExists("p_doc_t_prop_t"))
            {
                // создать таблицу
                DBTable dbt = new DBTable("p_doc_t_prop_t",
                        new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_persondocumenttypeproperty"),
                        new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                        new DBColumn("documentcontext_p", DBType.createVarchar(255)).setNullable(false),
                        new DBColumn("property_p", DBType.createVarchar(255)).setNullable(false),
                        new DBColumn("used_p", DBType.BOOLEAN).setNullable(false),
                        new DBColumn("required_p", DBType.BOOLEAN).setNullable(false),
                        new DBColumn("documenttype_id", DBType.LONG).setNullable(false)
                );
                tool.createTable(dbt);
            }

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("personDocumentTypeProperty");
        }

        ////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////

        ISQLTranslator translator = tool.getDialect().getSQLTranslator();

        SQLUpdateQuery updateTaskQuery = new SQLUpdateQuery("remotedocumenttaskservice_t")
                .set("taskname_p", "?")
                .where("taskname_p=?");

        tool.executeUpdate(translator.toSql(updateTaskQuery), "personDocuments", "unienr14EntrantDocs");

        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrEntrantDocumentType

        Map<Long, Long> docTypeIdMap = Maps.newHashMap();

        Long docTypeDisablement = null;
        // сущность была удалена
        {
            short typeEntityCode = tool.entityCodes().ensure("personDocumentType");
            short aclEntityCode = tool.entityCodes().ensure("personDocumentTypeRight");

            SQLSelectQuery selectQuery = new SQLSelectQuery().from(SQLFrom.table("enr14_c_entrant_doc_type_t"))
                    .column("id", "id")
                    .column("code_p", "code")
                    .column("shorttitle_p", "stitle")
                    .column("priority_p", "priority")
                    .column("title_p", "title");


            SQLInsertQuery insertDocTypeQuery = new SQLInsertQuery("p_doc_type_t")
                    .set("id", "?")
                    .set("discriminator", "?")
                    .set("code_p", "?")
                    .set("fake_p", "?")
                    .set("shorttitle_p", "?")
                    .set("priority_p", "?")
                    .set("title_p", "?");

            SQLInsertQuery insertEntrantDocTypeACLQuery = new SQLInsertQuery("p_doc_t_r_t")
                    .set("id", "?")
                    .set("discriminator", "?")
                    .set("documentcontext_p", "?")
                    .set("documenttype_id", "?")
                    .set("viewandlink_p", "?")
                    .set("createandedit_p", "?");

            Statement stmt = tool.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(translator.toSql(selectQuery));

            while (rs.next())
            {
                Long oldId = rs.getLong("id");
                String code = rs.getString("code");
                boolean fake = INDENTITY_CARD.equals(code) || EDU_INSTITUTION.equals(code);
                String shortTitle = rs.getString("stitle");
                int priority = rs.getInt("priority");
                String title = rs.getString("title");

                Long id = EntityIDGenerator.generateNewId(typeEntityCode);
                Long aclId = EntityIDGenerator.generateNewId(aclEntityCode);

                docTypeIdMap.put(oldId, id);
                if ("5".equals(code))
                {
                    docTypeDisablement = id;
                }

                tool.executeUpdate(translator.toSql(insertDocTypeQuery), id, typeEntityCode, code, fake, shortTitle, priority, title);
                tool.executeUpdate(translator.toSql(insertEntrantDocTypeACLQuery), aclId, aclEntityCode, EnrEntrant.class.getSimpleName().toLowerCase(), id, true, true);
            }
        }

        if (docTypeDisablement == null)
        {
            throw new RuntimeException("Type for disabled cert not exist. Drop migration.");
        }

        Map<Long, Long> docCategoryIdMap = Maps.newHashMap();

        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrDocumentCategory

        // сущность была удалена
        {
            Map<String, String> shortTitleMap = new HashMap<String, String>()
            {
                {
                    put("2016.01", "свидетельство о смерти");
                    put("2016.02", "справка ЗАГСа");
                    put("2016.03", "приговор лишения свободы");
                    put("2016.04", "отказ родителя");
                    put("2016.05", "решение о лишении прав");
                    put("2016.06", "признание безвестно отсутствующим");
                    put("2016.07", "справка из соц. защиты");
                    put("2016.08", "иной документ");
                    put("2016.09", "удостоверение боевых действий");
                    put("2016.10", "подтверждение участия в разминировании");
                    put("2016.11", "подтверждение доставки грузов в Афганистан");
                    put("2016.12", "подтверждение боевых вылетов в Афганистан");
                    put("2016.13", "диплом Олимпийских игр");
                    put("2016.14", "диплом Паралимпийских игр");
                    put("2016.15", "диплом Сурдлимпийских игр");
                    put("2016.16", "диплом чемпиона мира");
                    put("2016.17", "диплом чемпиона Европы");
                    put("2016.18", "подтверждение гражданства СССР");
                    put("2016.19", "подтверждение проживания в России");
                    put("2016.20", "подтверждение родства по прямой линии");
                    put("2016.21", "подтверждение проживания за рубежом");
                    put("2016.22", "иной документ");
                }
            };

            short entityCode = tool.entityCodes().ensure("personDocumentCategory");

            SQLSelectQuery selectQuery = new SQLSelectQuery().from(SQLFrom.table("enr14_document_category"))
                    .column("id", "id")
                    .column("code_p", "code")
                    .column("entrantdocumenttype_id", "doctype")
                    .column("title_p", "title")
                    .column("usercode_p", "usercode")
                    .column("disableddate_p", "disableddate");

            SQLInsertQuery insertDocCategoryQuery = new SQLInsertQuery("p_doc_cat_t")
                    .set("id", "?")
                    .set("discriminator", "?")
                    .set("code_p", "?")
                    .set("shorttitle_p", "?")
                    .set("documenttype_id", "?")
                    .set("priority_p", "?")
                    .set("title_p", "?")
                    .set("usercode_p", "?")
                    .set("disableddate_p", "?");

            Statement stmt = tool.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(translator.toSql(selectQuery));

            int priority = 1;

            while (rs.next())
            {
                Long oldId = rs.getLong("id");
                String code = rs.getString("code");
                Long oldDocType = rs.getLong("doctype");
                String title = rs.getString("title");
                String userCode = rs.getString("usercode");
                Date disableddate = rs.getDate("disableddate");

                String shortTitle = shortTitleMap.containsKey(code) ? shortTitleMap.get(code) : title;

                Long id = EntityIDGenerator.generateNewId(entityCode);
                Long newDocType = docTypeIdMap.get(oldDocType);

                if (newDocType == null)
                {
                    throw new RuntimeException("personDocumentType for enrEntrantDocumentType with id = " + oldDocType + " not exist. Drop migration.");
                }

                docCategoryIdMap.put(oldId, id);

                tool.executeUpdate(translator.toSql(insertDocCategoryQuery), id, entityCode, code, shortTitle, newDocType, priority, title, userCode, disableddate);
                priority++;
            }

            SQLSelectQuery selectDisCategoryQuery = new SQLSelectQuery().from(SQLFrom.table("enr14_c_disablement_type_t"))
                    .column("id", "id")
                    .column("code_p", "code")
                    .column("shorttitle_p", "stitle")
                    .column("priority_p", "priority")
                    .column("title_p", "title");


            SQLInsertQuery insertDisCategoryQuery = new SQLInsertQuery("p_doc_cat_t")
                    .set("id", "?")
                    .set("discriminator", "?")
                    .set("code_p", "?")
                    .set("shorttitle_p", "?")
                    .set("documenttype_id", "?")
                    .set("priority_p", "?")
                    .set("title_p", "?");

            Statement disStmt = tool.getConnection().createStatement();
            ResultSet disRs = disStmt.executeQuery(translator.toSql(selectDisCategoryQuery));

            while (disRs.next())
            {
                Long oldId = disRs.getLong("id");
                String code = disRs.getString("code");
                String shortTitle = disRs.getString("stitle");
                int disPriority = disRs.getInt("priority");
                String title = disRs.getString("title");

                Long id = EntityIDGenerator.generateNewId(entityCode);

                docCategoryIdMap.put(oldId, id);

                tool.executeUpdate(translator.toSql(insertDisCategoryQuery), id, entityCode, code, shortTitle, docTypeDisablement, disPriority, title);
            }
        }


        // создано обязательное свойство docRelation
        {
            // создать колонку
            tool.createColumn("enr14_enrtant_base_doc_t", new DBColumn("docrelation_id", DBType.LONG));
        }

        ////////////////////////////////////////////////////////////////////////////////
        // DROP CONSTRAINTS & TRIGGERS

        tool.dropConstraint("enr14_enrtant_base_doc_t", "fk_documenttype_8e8e8323");
        tool.dropConstraint("enr14_enrtant_base_doc_t", "fk_documentcategory_8e8e8323");
        tool.dropConstraint("enr14_camp_entrant_doc_t", "fk_documenttype_548aca82");
        tool.dropConstraint("enr14_person_edu_doc_rel_t", "fk_documenttype_77800526");

        if(tool.tableExists("enr14fis_conv_doc_c_cv_t"))
        {
            tool.dropConstraint("enr14fis_conv_doc_c_cv_t", "fk_doccategory_44c0f7fa");
        }

        if(tool.tableExists("enr14fis_conv_doc_c_bn_t"))
        {
            tool.dropConstraint("enr14fis_conv_doc_c_bn_t", "fk_doccategory_9464ca58");
        }

        if(tool.tableExists("enr14fis_conv_doc_c_sd_t"))
        {
            tool.dropConstraint("enr14fis_conv_doc_c_sd_t", "fk_doccategory_a2f20f51");
        }

        if(tool.tableExists("enr14fis_conv_doc_c_orphan_t"))
        {
            tool.dropConstraint("enr14fis_conv_doc_c_orphan_t", "fk_doccategory_a6ef51da");
        }

        if(tool.triggerExists("enr14_olymp_diploma_t", "tg_aur_5a599a87_44514c04"))
        {
            tool.dropTrigger("enr14_olymp_diploma_t", "tg_aur_5a599a87_44514c04");
        }

        if(tool.triggerExists("enr14_olymp_diploma_t", "tg_air_44514c04"))
        {
            tool.dropTrigger("enr14_olymp_diploma_t", "tg_air_44514c04");
        }

//
//        if(tool.triggerExists("enr14_entrant_benefit_proof_t", "tg_air_d0723f7f"))
//        {
//            tool.dropTrigger("enr14_entrant_benefit_proof_t", "tg_air_d0723f7f");
//        }
//
//        if(tool.triggerExists("enr14_entrant_benefit_proof_t", "tg_aur_1e3346bf_d0723f7f"))
//        {
//            tool.dropTrigger("enr14_entrant_benefit_proof_t", "tg_aur_1e3346bf_d0723f7f");
//        }
//
//        if(tool.triggerExists("enr14_entr_req_attachment_t", "tg_air_6f877b7e"))
//        {
//            tool.dropTrigger("enr14_entr_req_attachment_t", "tg_air_6f877b7e");
//        }
//
//        if(tool.triggerExists("enr14_entr_req_attachment_t", "tg_aur_1e3346bf_6f877b7e"))
//        {
//            tool.dropTrigger("enr14_entr_req_attachment_t", "tg_aur_1e3346bf_6f877b7e");
//        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrEntrantDisabilityDocument
        // сущность была удалена

        // выставить нуллабельность
        {
            tool.setColumnNullable("enr14_enrtant_base_doc_t", "registrationdate_p", true);
        }

        {
            short baseDocumentEntityCode = tool.entityCodes().ensure("personDocument");
            short documentRoleRelEntityCode = tool.entityCodes().ensure("personDocumentRoleRel");
            short entrantBaseDocumentEntityCode = tool.entityCodes().ensure("enrEntrantBaseDocument");


            SQLSelectQuery selectQuery = new SQLSelectQuery().from(SQLFrom.table("enr14_enrtant_base_doc_t", "bd")
                    .leftJoin(SQLFrom.table("enr14_entrant_dis_doc_t", "dd"), "bd.id=dd.id")
                    .innerJoin(SQLFrom.table("enr14_entrant_t", "e"), "e.id=bd.entrant_id")
                    .innerJoin(SQLFrom.table("personrole_t", "pr"), "pr.id=e.id")
            )
                    .column("bd.id", "id")
                    .column("bd.entrant_id", "entrant")
                    .column("pr.person_id", "person")
                    .column("bd.documenttype_id", "doctype")
                    .column("bd.registrationdate_p", "regdate")
                    .column("bd.seria_p", "seria")
                    .column("bd.number_p", "number")
                    .column("bd.issuancedate_p", "issdate")
                    .column("bd.expirationdate_p", "expdate")
                    .column("bd.issuanceplace_p", "issplace")
                    .column("bd.description_p", "docdesc")
                    .column("bd.comment_p", "comm")
                    .column("bd.scancopy_p", "scan")
                    .column("bd.documentcategory_id", "doccat")
                    .column("dd.id", "disid")
                    .column("dd.disablementtype_id", "distype");

            SQLInsertQuery insertBaseDocQuery = new SQLInsertQuery("p_doc_t")
                    .set("id", "?")
                    .set("discriminator", "?")
                    .set("version", "?")
                    .set("person_id", "?")
                    .set("documenttype_id", "?")
                    .set("seria_p", "?")
                    .set("number_p", "?")
                    .set("registrationdate_p", "?")
                    .set("expirationdate_p", "?")
                    .set("issuancedate_p", "?")
                    .set("description_p", "?")
                    .set("comment_p", "?")
                    .set("scancopy_p", "?")
                    .set("documentcategory_id", "?");



            SQLInsertQuery insertDocRelQuery = new SQLInsertQuery("p_doc_role_rel_t")
                    .set("id", "?")
                    .set("discriminator", "?")
                    .set("document_id", "?")
                    .set("role_id", "?");

            SQLUpdateQuery updateQuery = new SQLUpdateQuery("enr14_enrtant_base_doc_t")
                    .set("docrelation_id", "?")
                    .set("documenttype_id", "?")
                    .where("id=?");

            // for dis

            SQLInsertQuery insertEntrantBaseDocQuery = new SQLInsertQuery("enr14_enrtant_base_doc_t")
                    .set("id", "?")
                    .set("discriminator", "?")
                    .set("entrant_id", "?")
                    .set("docrelation_id", "?")
                    .set("documenttype_id", "?");

            SQLUpdateQuery updateAchievementQuery = new SQLUpdateQuery("enr14_entrant_achievement_t")
                    .set("document_id", "?")
                    .where("document_id=?");

            SQLUpdateQuery updateMarkSrcQuery = new SQLUpdateQuery("enr14_mark_src_benefit_t")
                    .set("mainproof_id", "?")
                    .where("mainproof_id=?");

            SQLUpdateQuery updateBenefitProofQuery = new SQLUpdateQuery("enr14_entrant_benefit_proof_t")
                    .set("document_id", "?")
                    .where("document_id=?");

            SQLUpdateQuery updateReqAttachmentQuery = new SQLUpdateQuery("enr14_entr_req_attachment_t")
                    .set("document_id", "?")
                    .where("document_id=?");

            Statement stmt = tool.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(translator.toSql(selectQuery));

            while (rs.next())
            {
                Long oldId = rs.getLong("id");
                Long disId = rs.getLong("disId") != 0 ? rs.getLong("disId") : null;
                Long entrantId = rs.getLong("entrant");
                Long personId = rs.getLong("person");
                Long docTypeId = rs.getLong("doctype");
                Date regDate = rs.getDate("regdate");
                String seria = rs.getString("seria");
                String number = rs.getString("number");
                Date issuanceDate = rs.getDate("issdate");
                Date expirationDate = rs.getDate("expdate");
                String issuancePlace = rs.getString("issplace");
                String description = rs.getString("docdesc");
                String comment = rs.getString("comm");
                Long scanCopy = rs.getLong("scan") != 0 ? rs.getLong("scan") : null;
                Long docCatId = rs.getLong("doccat") != 0 ? rs.getLong("doccat") : null;
                Long disTypeId = rs.getLong("distype") != 0 ? rs.getLong("distype") : null;

                Long baseDocId = EntityIDGenerator.generateNewId(baseDocumentEntityCode);
                Long docRelId = EntityIDGenerator.generateNewId(documentRoleRelEntityCode);
                Long entrantBaseDocId = EntityIDGenerator.generateNewId(entrantBaseDocumentEntityCode);

                Long docTypeNewId = disTypeId != null ? docTypeDisablement : docTypeIdMap.get(docTypeId);
                Long docCatNewId = disTypeId != null ? docCategoryIdMap.get(disTypeId) : docCategoryIdMap.get(docCatId);

                if(disId == null)
                {

                    tool.executeUpdate(translator.toSql(insertBaseDocQuery), baseDocId, baseDocumentEntityCode, 0, personId, docTypeNewId,
                            seria, number, regDate, expirationDate, issuanceDate, description, comment, scanCopy, docCatNewId);

                    tool.executeUpdate(translator.toSql(insertDocRelQuery), docRelId, documentRoleRelEntityCode, baseDocId, entrantId);

                    tool.executeUpdate(translator.toSql(updateQuery), docRelId, docTypeNewId, oldId);
                }
                else
                {
                    tool.executeUpdate(translator.toSql(insertBaseDocQuery), baseDocId, baseDocumentEntityCode, 0, personId, docTypeNewId,
                            seria, number, regDate, expirationDate, issuanceDate, description, comment, scanCopy, docCatNewId);

                    tool.executeUpdate(translator.toSql(insertDocRelQuery), docRelId, documentRoleRelEntityCode, baseDocId, entrantId);

                    tool.executeUpdate(translator.toSql(insertEntrantBaseDocQuery), entrantBaseDocId, entrantBaseDocumentEntityCode, entrantId, docRelId, docTypeNewId);

                    tool.executeUpdate(translator.toSql(updateAchievementQuery), entrantBaseDocId, oldId);

                    tool.executeUpdate(translator.toSql(updateMarkSrcQuery), entrantBaseDocId, oldId);

                    tool.executeUpdate(translator.toSql(updateBenefitProofQuery), entrantBaseDocId, oldId);

                    tool.executeUpdate(translator.toSql(updateReqAttachmentQuery), entrantBaseDocId, oldId);

                }

//                tool.executeUpdate(translator.toSql(updateBenefitProofQuery), docRelId, oldId);
//                tool.executeUpdate(translator.toSql(updateReqAttachmentQuery), docRelId, oldId);
            }
        }


        /// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        /// !!!!!!!!              CURRENT                      !!!!!!
        /// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        ////////////////////////////////////////////////////////////////////////////////
        // персистентный интерфейс ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitProofDocument
        // персистентный интерфейс ru.tandemservice.unienr14.request.entity.IEnrEntrantRequestAttachable
        // персистентный интерфейс ru.tandemservice.unienr14.entrant.entity.IEnrEntrantAchievementProofDocument

        // персистентный интерфейс изменился, он будет создан заново после выполнения всех миграций
        {
            // удалить view
            tool.dropView("enrentrntbnftprfdcmnt_v");
            tool.dropView("ienrentrantrequestattachable_v");
            tool.dropView("enrentrntachvmntprfdcmnt_v");
            tool.dropView("enr_entrant_doc_ext_view");
        }



        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrOlympiadDiploma

        // сущность стала дочерней, поэтому удалено свойство class
        // удалено свойство version
        {
            tool.dropColumn("enr14_olymp_diploma_t", "discriminator");
            tool.dropColumn("enr14_olymp_diploma_t", "version");
        }

        // выставить нуллабельность
        {
            tool.setColumnNullable("enr14_olymp_diploma_t", "entrant_id", true);
            tool.setColumnNullable("enr14_olymp_diploma_t", "registrationdate_p", true);
            tool.setColumnNullable("enr14_olymp_diploma_t", "documenttype_id", true);

            tool.setColumnNullable("enr14_enrtant_base_doc_t", "registrationdate_p", true);

        }

        {
            short baseDocumentEntityCode = tool.entityCodes().ensure("personDocument");
            short entrantBaseDocumentEntityCode = tool.entityCodes().ensure("enrEntrantBaseDocument");
            short documentRoleRelEntityCode = tool.entityCodes().ensure("personDocumentRoleRel");
            short olympDiplomaEntityCode = tool.entityCodes().ensure("enrOlympiadDiploma");

            SQLSelectQuery selectQuery = new SQLSelectQuery().from(SQLFrom.table("enr14_olymp_diploma_t", "od")
                    .innerJoin(SQLFrom.table("enr14_entrant_t", "e"), "e.id=od.entrant_id")
                    .innerJoin(SQLFrom.table("personrole_t", "pr"), "pr.id=e.id")
            )
                    .column("od.id", "id")
                    .column("od.entrant_id", "entrant")
                    .column("pr.person_id", "person")
                    .column("od.registrationdate_p", "regdate")
                    .column("od.seria_p", "seria")
                    .column("od.number_p", "number")
                    .column("od.issuancedate_p", "issdate")
                    .column("od.scancopy_p", "scancopy")
                    .column("od.settlement_id", "settlement")
                    .column("od.olympiad_id", "olympiad")
                    .column("od.subject_id", "subject")
                    .column("od.honour_id", "honour")
                    .column("od.documenttype_id", "doctype")
                    .column("od.trainingclass_p", "trainingclass")
                    .column("od.combinedteam_p", "combteam")
                    .column("od.issuedby_p", "issuedby")
                    .column("od.stateexamsubject_id", "stateexamsubject")
                    .column("od.olympiadprofilediscipline_id", "olympprofiledisc");

            SQLInsertQuery insertBaseDocQuery = new SQLInsertQuery("p_doc_t")
                    .set("id", "?")
                    .set("discriminator", "?")
                    .set("version", "?")
                    .set("person_id", "?")
                    .set("documenttype_id", "?")
                    .set("seria_p", "?")
                    .set("number_p", "?")
                    .set("registrationdate_p", "?")
//					.set("expirationdate_p", "?")
                    .set("issuancedate_p", "?")
//					.set("description_p", "?")
//					.set("comment_p", "?")
                    .set("scancopy_p", "?");
//					.set("documentcategory_id", "?");

            SQLInsertQuery insertOlympDiplomaQuery = new SQLInsertQuery("enr14_olymp_diploma_t")
                    .set("id", "?")
                    .set("settlement_id", "?")
                    .set("olympiad_id", "?")
                    .set("subject_id", "?")
                    .set("honour_id", "?")
                    .set("trainingclass_p", "?")
                    .set("combinedteam_p", "?")
                    .set("stateexamsubject_id", "?")
                    .set("olympiadprofilediscipline_id", "?");

            SQLInsertQuery insertDocRelQuery = new SQLInsertQuery("p_doc_role_rel_t")
                    .set("id", "?")
                    .set("discriminator", "?")
                    .set("document_id", "?")
                    .set("role_id", "?");

            SQLInsertQuery insertEntrantBaseDocQuery = new SQLInsertQuery("enr14_enrtant_base_doc_t")
                    .set("id", "?")
                    .set("discriminator", "?")
                    .set("entrant_id", "?")
                    .set("docrelation_id", "?")
                    .set("documenttype_id", "?");

            SQLUpdateQuery updateAchievementQuery = new SQLUpdateQuery("enr14_entrant_achievement_t")
                    .set("document_id", "?")
                    .where("document_id=?");

            SQLUpdateQuery updateMarkSrcQuery = new SQLUpdateQuery("enr14_mark_src_benefit_t")
                    .set("mainproof_id", "?")
                    .where("mainproof_id=?");

            SQLUpdateQuery updateBenefitProofQuery = new SQLUpdateQuery("enr14_entrant_benefit_proof_t")
                    .set("document_id", "?")
                    .where("document_id=?");

            SQLUpdateQuery updateReqAttachmentQuery = new SQLUpdateQuery("enr14_entr_req_attachment_t")
                    .set("document_id", "?")
                    .where("document_id=?");

            SQLDeleteQuery deleteOlympDiplomaQuery = new SQLDeleteQuery("enr14_olymp_diploma_t")
                    .where("id=?");

            Statement stmt = tool.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(translator.toSql(selectQuery));

            while (rs.next())
            {
                Long oldId = rs.getLong("id");
                Long entrantId = rs.getLong("entrant");
                Long personId = rs.getLong("person");
                Long docTypeId = rs.getLong("doctype");
                Date regDate = rs.getDate("regdate");
                String seria = rs.getString("seria");
                String number = rs.getString("number");
                Date issuanceDate = rs.getDate("issdate");
                Long scanCopy = rs.getLong("scancopy") != 0 ? rs.getLong("scancopy") : null;
                Long settlement = rs.getLong("settlement") != 0 ? rs.getLong("settlement") : null;
                Long olympiad = rs.getLong("olympiad") != 0 ? rs.getLong("olympiad") : null;
                Long subject = rs.getLong("subject") != 0 ? rs.getLong("subject") : null;
                Long honour = rs.getLong("honour") != 0 ? rs.getLong("honour") : null;
                Long stateExamSubject = rs.getLong("stateexamsubject") != 0 ? rs.getLong("stateexamsubject") : null;
                Long olympiadProfileDiscipline = rs.getLong("olympprofiledisc") != 0 ? rs.getLong("olympprofiledisc") : null;
                String issuancePlace = rs.getString("issuedby");
                Integer trainingClass = rs.getInt("trainingclass") != 0 ? rs.getInt("trainingclass") : null;
                String combinedTeam = rs.getString("combteam");

                Long baseDocId = EntityIDGenerator.generateNewId(olympDiplomaEntityCode);
                Long entrantBaseDocId = EntityIDGenerator.generateNewId(entrantBaseDocumentEntityCode);
                Long docRelId = EntityIDGenerator.generateNewId(documentRoleRelEntityCode);

                Long docTypeNewId = docTypeIdMap.get(docTypeId);

                tool.executeUpdate(translator.toSql(insertBaseDocQuery), baseDocId, olympDiplomaEntityCode, 0, personId, docTypeNewId,
                        seria, number, regDate, issuanceDate, scanCopy);

                tool.executeUpdate(translator.toSql(insertOlympDiplomaQuery), baseDocId, settlement, olympiad, subject, honour, trainingClass, combinedTeam, stateExamSubject, olympiadProfileDiscipline);

                tool.executeUpdate(translator.toSql(insertDocRelQuery), docRelId, documentRoleRelEntityCode, baseDocId, entrantId);

                tool.executeUpdate(translator.toSql(insertEntrantBaseDocQuery), entrantBaseDocId, entrantBaseDocumentEntityCode, entrantId, docRelId, docTypeNewId);

                tool.executeUpdate(translator.toSql(updateAchievementQuery), entrantBaseDocId, oldId);

                tool.executeUpdate(translator.toSql(updateMarkSrcQuery), entrantBaseDocId, oldId);

                tool.executeUpdate(translator.toSql(updateBenefitProofQuery), entrantBaseDocId, oldId);

                tool.executeUpdate(translator.toSql(updateReqAttachmentQuery), entrantBaseDocId, oldId);

                tool.executeUpdate(translator.toSql(deleteOlympDiplomaQuery), oldId);
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrCampaignEntrantDocument

        // раньше свойство documentType ссылалось на сущность enrEntrantDocumentType
        // теперь оно ссылается на сущность personDocumentType
        {


            SQLSelectQuery selectQuery = new SQLSelectQuery().from(SQLFrom.table("enr14_camp_entrant_doc_t", "ced"))
                    .column("ced.id", "id")
                    .column("ced.documenttype_id", "doctype");

            SQLUpdateQuery updateQuery = new SQLUpdateQuery("enr14_camp_entrant_doc_t")
                    .set("documenttype_id", "?")
                    .where("id=?");

            Statement stmt = tool.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(translator.toSql(selectQuery));

            while (rs.next())
            {
                Long id = rs.getLong("id");
                Long docTypeId = rs.getLong("doctype") != 0 ? rs.getLong("doctype") : null;

                if(docTypeId != null)
                {
                    Long newDocTypeId = docTypeIdMap.get(docTypeId);

                    if(newDocTypeId == null)
                    {
                        throw new RuntimeException("Unknown document type.");
                    }

                    tool.executeUpdate(translator.toSql(updateQuery), newDocTypeId, id);
                }
            }

        }


        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrPersonEduDocumentRel

        // раньше свойство documentType ссылалось на сущность enrEntrantDocumentType
        // теперь оно ссылается на сущность personDocumentType
        {

            SQLSelectQuery selectQuery = new SQLSelectQuery().from(SQLFrom.table("enr14_person_edu_doc_rel_t", "ced"))
                    .column("ced.id", "id")
                    .column("ced.documenttype_id", "doctype");

            SQLUpdateQuery updateQuery = new SQLUpdateQuery("enr14_person_edu_doc_rel_t")
                    .set("documenttype_id", "?")
                    .where("id=?");

            Statement stmt = tool.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(translator.toSql(selectQuery));

            while (rs.next())
            {
                Long id = rs.getLong("id");
                Long docTypeId = rs.getLong("doctype") != 0 ? rs.getLong("doctype") : null;

                if(docTypeId != null)
                {
                    Long newDocTypeId = docTypeIdMap.get(docTypeId);

                    if(newDocTypeId == null)
                    {
                        throw new RuntimeException("Unknown document type.");
                    }

                    tool.executeUpdate(translator.toSql(updateQuery), newDocTypeId, id);
                }
            }
        }




        // раньше свойство docCategory ссылалось на сущность enrEntrantDocumentCategory
        // теперь оно ссылается на сущность personDocumentCategory
        {
            // сущность enrFisConv4DocumentCategoryBelongingNation enr14fis_conv_doc_c_bn_t
            if(tool.tableExists("enr14fis_conv_doc_c_bn_t"))
            {
                SQLSelectQuery selectQuery = new SQLSelectQuery().from(SQLFrom.table("enr14fis_conv_doc_c_bn_t", "ced"))
                        .column("ced.id", "id")
                        .column("ced.doccategory_id", "doccategory");

                SQLUpdateQuery updateQuery = new SQLUpdateQuery("enr14fis_conv_doc_c_bn_t")
                        .set("doccategory_id", "?")
                        .where("id=?");

                Statement stmt = tool.getConnection().createStatement();
                ResultSet rs = stmt.executeQuery(translator.toSql(selectQuery));

                while (rs.next())
                {
                    Long id = rs.getLong("id");
                    Long docCategoryId = rs.getLong("doccategory") != 0 ? rs.getLong("doccategory") : null;

                    if(docCategoryId != null)
                    {
                        Long newDocCategoryId = docCategoryIdMap.get(docCategoryId);

                        if(newDocCategoryId == null)
                        {
                            throw new RuntimeException("Unknown document category.");
                        }

                        tool.executeUpdate(translator.toSql(updateQuery), newDocCategoryId, id);
                    }
                }
            }

            // сущность enrFisConv4DocumentCategoryCombatVeterans enr14fis_conv_doc_c_cv_t
            if(tool.tableExists("enr14fis_conv_doc_c_cv_t"))
            {
                SQLSelectQuery selectQuery = new SQLSelectQuery().from(SQLFrom.table("enr14fis_conv_doc_c_cv_t", "ced"))
                        .column("ced.id", "id")
                        .column("ced.doccategory_id", "doccategory");

                SQLUpdateQuery updateQuery = new SQLUpdateQuery("enr14fis_conv_doc_c_cv_t")
                        .set("doccategory_id", "?")
                        .where("id=?");

                Statement stmt = tool.getConnection().createStatement();
                ResultSet rs = stmt.executeQuery(translator.toSql(selectQuery));

                while (rs.next())
                {
                    Long id = rs.getLong("id");
                    Long docCategoryId = rs.getLong("doccategory") != 0 ? rs.getLong("doccategory") : null;

                    if(docCategoryId != null)
                    {
                        Long newDocCategoryId = docCategoryIdMap.get(docCategoryId);

                        if(newDocCategoryId == null)
                        {
                            throw new RuntimeException("Unknown document category.");
                        }

                        tool.executeUpdate(translator.toSql(updateQuery), newDocCategoryId, id);
                    }
                }
            }

            // сущность enrFisConv4DocumentCategorySportsDiploma enr14fis_conv_doc_c_sd_t
            if(tool.tableExists("enr14fis_conv_doc_c_sd_t"))
            {
                SQLSelectQuery selectQuery = new SQLSelectQuery().from(SQLFrom.table("enr14fis_conv_doc_c_sd_t", "ced"))
                        .column("ced.id", "id")
                        .column("ced.doccategory_id", "doccategory");

                SQLUpdateQuery updateQuery = new SQLUpdateQuery("enr14fis_conv_doc_c_sd_t")
                        .set("doccategory_id", "?")
                        .where("id=?");

                Statement stmt = tool.getConnection().createStatement();
                ResultSet rs = stmt.executeQuery(translator.toSql(selectQuery));

                while (rs.next())
                {
                    Long id = rs.getLong("id");
                    Long docCategoryId = rs.getLong("doccategory") != 0 ? rs.getLong("doccategory") : null;

                    if(docCategoryId != null)
                    {
                        Long newDocCategoryId = docCategoryIdMap.get(docCategoryId);

                        if(newDocCategoryId == null)
                        {
                            throw new RuntimeException("Unknown document category.");
                        }

                        tool.executeUpdate(translator.toSql(updateQuery), newDocCategoryId, id);
                    }
                }
            }

            // сущность enrFisConv4DocumentCategoryOrphan enr14fis_conv_doc_c_orphan_t
            if(tool.tableExists("enr14fis_conv_doc_c_orphan_t"))
            {
                SQLSelectQuery selectQuery = new SQLSelectQuery().from(SQLFrom.table("enr14fis_conv_doc_c_orphan_t", "ced"))
                        .column("ced.id", "id")
                        .column("ced.doccategory_id", "doccategory");

                SQLUpdateQuery updateQuery = new SQLUpdateQuery("enr14fis_conv_doc_c_orphan_t")
                        .set("doccategory_id", "?")
                        .where("id=?");

                Statement stmt = tool.getConnection().createStatement();
                ResultSet rs = stmt.executeQuery(translator.toSql(selectQuery));

                while (rs.next())
                {
                    Long id = rs.getLong("id");
                    Long docCategoryId = rs.getLong("doccategory") != 0 ? rs.getLong("doccategory") : null;

                    if(docCategoryId != null)
                    {
                        Long newDocCategoryId = docCategoryIdMap.get(docCategoryId);

                        if(newDocCategoryId == null)
                        {
                            throw new RuntimeException("Unknown document category.");
                        }

                        tool.executeUpdate(translator.toSql(updateQuery), newDocCategoryId, id);
                    }
                }
            }
        }

        // удалено свойство entrant
        // удалено свойство registrationDate
        // удалено свойство seria
        // удалено свойство number
        // удалено свойство issuanceDate
        // удалено свойство scanCopy
        // удалено свойство documentType
        // удалено свойство issuedBy
        {
            // удалить колонки
            tool.dropColumn("enr14_olymp_diploma_t", "entrant_id");
            tool.dropColumn("enr14_olymp_diploma_t", "registrationdate_p");
            tool.dropColumn("enr14_olymp_diploma_t", "seria_p");
            tool.dropColumn("enr14_olymp_diploma_t", "number_p");
            tool.dropColumn("enr14_olymp_diploma_t", "issuancedate_p");
            tool.dropColumn("enr14_olymp_diploma_t", "scancopy_p");
            tool.dropColumn("enr14_olymp_diploma_t", "documenttype_id");
            tool.dropColumn("enr14_olymp_diploma_t", "issuedby_p");
        }

        // удаление
        {
            // удалить записи из базовых таблиц
            tool.deleteRowsFromParentTables("enr14_entrant_dis_doc_t", "enr14_enrtant_base_doc_t");

            // удалить таблицу
            tool.dropTable("enr14_entrant_dis_doc_t", false /* - не удалять, если есть ссылающиеся таблицы */);

            // удалить код сущности
            tool.entityCodes().delete("enrEntrantDisabilityDocument");

            ////////////////////////////////////////////////////////////////////////////////////////////////////

            // удалить таблицу
            tool.dropTable("enr14_c_disablement_type_t", false /* - не удалять, если есть ссылающиеся таблицы */);

            // удалить код сущности
            tool.entityCodes().delete("enrDisablementType");

            ////////////////////////////////////////////////////////////////////////////////////////////////////

            // удалить таблицу
            tool.dropTable("enr14_document_category", false /* - не удалять, если есть ссылающиеся таблицы */);

            // удалить код сущности
            tool.entityCodes().delete("enrDocumentCategory");

            ////////////////////////////////////////////////////////////////////////////////////////////////////

            // удалить таблицу
            tool.dropTable("enr14_c_entrant_doc_type_t", false /* - не удалять, если есть ссылающиеся таблицы */);

            // удалить код сущности
            tool.entityCodes().delete("enrEntrantDocumentType");

            ////////////////////////////////////////////////////////////////////////////////////////////////////

            // сделать колонку NOT NULL
            tool.setColumnNullable("enr14_enrtant_base_doc_t", "docrelation_id", false);

        }




        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrEntrantBaseDocument

        // удалено свойство issuanceDate
        // удалено свойство expirationDate
        // удалено свойство issuancePlace
        // удалено свойство description
        // удалено свойство comment
        // удалено свойство scanCopy
        // удалено свойство documentCategory
        // свойство registrationDate стало формулой
        // свойство seria стало формулой
        // свойство number стало формулой
        {
            // удалить колонки
            tool.dropColumn("enr14_enrtant_base_doc_t", "issuancedate_p");
            tool.dropColumn("enr14_enrtant_base_doc_t", "expirationdate_p");
            tool.dropColumn("enr14_enrtant_base_doc_t", "issuanceplace_p");
            tool.dropColumn("enr14_enrtant_base_doc_t", "description_p");
            tool.dropColumn("enr14_enrtant_base_doc_t", "comment_p");
            tool.dropColumn("enr14_enrtant_base_doc_t", "scancopy_p");
            tool.dropColumn("enr14_enrtant_base_doc_t", "documentcategory_id");
            tool.dropColumn("enr14_enrtant_base_doc_t", "registrationdate_p");
            tool.dropColumn("enr14_enrtant_base_doc_t", "seria_p");
            tool.dropColumn("enr14_enrtant_base_doc_t", "number_p");
        }


        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrPersonEduDocumentRel

        // раньше свойство documentType ссылалось на сущность enrEntrantDocumentType
        // теперь оно ссылается на сущность personDocumentType
//        {
//            // TODO: программист должен подтвердить это действие
//            if (true)
//            {
//                throw new UnsupportedOperationException("Confirm me: relation changed");
//            }
//
//        }


//        if (true)
//        {
//            throw new RuntimeException("unienr14");
//        }
    }
}