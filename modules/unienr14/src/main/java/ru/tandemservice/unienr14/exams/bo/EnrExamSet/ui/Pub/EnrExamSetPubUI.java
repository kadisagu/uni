/* $Id:$ */
package ru.tandemservice.unienr14.exams.bo.EnrExamSet.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.logic.EnrCompetitionDSHandler;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.ui.AddEdit.EnrExamSetAddEdit;
import ru.tandemservice.unienr14.exams.entity.EnrExamSet;
import ru.tandemservice.unienr14.exams.entity.EnrExamSetElement;

/**
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "examSetHolder.id", required = true)
})
public class EnrExamSetPubUI extends UIPresenter
{
    private final EntityHolder<EnrExamSet> examSetHolder = new EntityHolder<>();
    public EntityHolder<EnrExamSet> getExamSetHolder() { return this.examSetHolder; }

    @Override
    public void onComponentRefresh() {
        getExamSetHolder().refresh();
    }

    public void onClickEdit() {
        getActivationBuilder().asRegion(EnrExamSetAddEdit.class)
        .parameter(UIPresenter.PUBLISHER_ID, getExamSetHolder().getId())
        .activate();
    }

    public void onClickDelete() {
        DataAccessServices.dao().delete(getExamSetHolder().getId());
        deactivate();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        if (EnrExamSetPub.COMPETITION_DS.equals(dataSource.getName())) {
            dataSource.put(EnrCompetitionDSHandler.PARAM_EXAM_SET, getExamSet());
            EnrCompetitionType competitionType = getSettings().get("competitionTypeFilter");
            if (null != competitionType) {
                dataSource.put(EnrCompetitionDSHandler.PARAM_COMP_TYPE, competitionType);
            }
        }
    }

    // Getters & Setters

    public EnrExamSet getExamSet() { return getExamSetHolder().getValue(); }

    public String getExamSetTitle() {
        return getExamSet().getElementsTitle();
    }

    public String getExamSetDebugDefaultPassMarkTitle() {
        final StringBuilder resultBuilder = new StringBuilder();
        for (EnrExamSetElement element: getExamSet().getElementList()) {
            if (resultBuilder.length() > 0) { resultBuilder.append('\n'); }
            resultBuilder
            .append(element.getValue().getTitle())
            .append(' ')
            .append(EnrEntrantManager.DEFAULT_MARK_FORMATTER.format(element.getValue().getDefaultPassMarkAsLong()))
            ;
        }
        return resultBuilder.toString();
    }
}
