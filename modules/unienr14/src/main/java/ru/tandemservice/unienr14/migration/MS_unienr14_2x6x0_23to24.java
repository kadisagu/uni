/* $Id$ */
package ru.tandemservice.unienr14.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.PreparedStatement;
import java.util.HashMap;
import java.util.Map;

/**
 * @author azhebko
 * @since 10.06.2014
 */
public class MS_unienr14_2x6x0_23to24 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        Map<String, String> enrScriptItemTitleUpdateMap = new HashMap<>(3);
        {
            enrScriptItemTitleUpdateMap.put("common.entrantRequest.competitionNoExams", "Заявление абитуриента (выбранные конкурсы без ВИ)");
            enrScriptItemTitleUpdateMap.put("common.entrantRequest.requestedPrograms", "Заявление абитуриента (выбранные ОП)");
            enrScriptItemTitleUpdateMap.put("common.entrantRequest.acceptedContract", "Заявление абитуриента (согласие на зачисление по договору)");
        }

        PreparedStatement preparedStatement = tool.getConnection().prepareStatement("update scriptitem_t set title_p = ? where code_p = ? and catalogcode_p = ?");
        preparedStatement.setString(3, "unienrScriptItem");
        for (Map.Entry<String, String> entry: enrScriptItemTitleUpdateMap.entrySet())
        {
            preparedStatement.setString(1, entry.getValue());
            preparedStatement.setString(2, entry.getKey());
            preparedStatement.execute();
        }
    }
}