/* $Id:$ */
package ru.tandemservice.unienr14.base.ext.Common;

import org.apache.commons.collections15.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.ModuleStatusReportBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.util.IModuleStatusReporter;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrMethodDivCompetitionsCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrOrderTypeCodes;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement;
import ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroup;
import ru.tandemservice.unienr14.exams.entity.EnrExamSet;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;
import ru.tandemservice.unienr14.refusal.entity.EnrEntrantRefusal;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.EnrReportBaseManager;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrStorableReportDesc;
import ru.tandemservice.unienr14.request.entity.EnrEntrantForeignRequest;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignTargetAdmissionKind;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 2/11/14
 */
@Configuration
public class CommonExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private CommonManager _commonManager;

    @Bean
    public ItemListExtension<IModuleStatusReporter> moduleStatusExtPoint()
    {
        return itemListExtension(_commonManager.moduleStatusExtPoint())
            .add("unienr14", () -> {

                IUniBaseDao dao = IUniBaseDao.instance.get();
                List<String> result = new ArrayList<>();

                List<EducationYear> years = dao.getList(new DQLSelectBuilder().fromEntity(EducationYear.class, "e")
                                .joinEntity("e", DQLJoinType.inner, EnrEnrollmentCampaign.class, "c", eq(property("e"), property("c", EnrEnrollmentCampaign.educationYear())))
                                .column(property("e"))
                                .order(property("e", EducationYear.intValue()))
                                .distinct()
                );
                for (EducationYear year : years) {
                    result.add(year.getTitle());
                    final List<Object[]> competitionList = dao.getList(new DQLSelectBuilder().fromEntity(EnrCompetition.class, "e")
                                    .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().enrollmentCampaign().educationYear().fromAlias("e")), value(year)))
                                    .column(property(EnrCompetition.requestType().title().fromAlias("e")))
                                    .column(DQLFunctions.count(property(EnrCompetition.programSetOrgUnit().programSet().id().fromAlias("e"))))
                                    .distinct()
                                    .group(property(EnrCompetition.requestType().title().fromAlias("e")))
                    );
                    final List<Object> list = dao.getList(new DQLSelectBuilder().fromEntity(EnrCampaignTargetAdmissionKind.class, "e")
                            .where(eq(property(EnrCampaignTargetAdmissionKind.enrollmentCampaign().educationYear().fromAlias("e")), value(year)))
                            .column(DQLFunctions.count(property(EnrCampaignTargetAdmissionKind.id().fromAlias("e"))))
                            .group(property(EnrCampaignTargetAdmissionKind.enrollmentCampaign().title().fromAlias("e")))
                            .order(DQLFunctions.count(property(EnrCampaignTargetAdmissionKind.id().fromAlias("e"))), OrderDirection.desc)
                            .top(1));
                    Long tAInECKind = list.size() > 0 ? (Long) list.get(0) : null;

                    final Map<String, IEnrStorableReportDesc> reportItems = EnrReportBaseManager.instance().storableReportDescExtPoint().getItems();


                    List<Map.Entry<String, Integer>> reportStats = new ArrayList<>();
                    for (Map.Entry<String, IEnrStorableReportDesc> entry : reportItems.entrySet()) {
                        int count = dao.getCount(entry.getValue().getReportClass(), "enrollmentCampaign.educationYear", year);
                        if (count == 0) continue;
                        reportStats.add(new AbstractMap.SimpleEntry<>(entry.getValue().getPubTitle(), count));
                    }
                    Collections.sort(reportStats, (o1, o2) -> - Integer.compare(o1.getValue(), o2.getValue()));
                    Collection<String> reports = CollectionUtils.collect(reportStats, e -> e.getKey() + " - " + e.getValue());


                    final Collection<String> competitionsTotal = CollectionUtils.collect(competitionList, ModuleStatusReportBuilder.QUERY_RESULT_TRANSFORMER);

                    result.add("Число ПК: " + dao.getCount(EnrEnrollmentCampaign.class, EnrEnrollmentCampaign.L_EDUCATION_YEAR, year));
                    result.add("Число наборов ОП (с конкурсами): ");

                    List<EnrRequestType> requestTypes = dao.getList(EnrRequestType.class);
                    for (EnrRequestType requestType : requestTypes) {
                        result.add(requestType.getTitle() + " - " + dao.getCount(new DQLSelectBuilder()
                                .fromEntity(EnrCompetition.class, "e")
                                .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().enrollmentCampaign().educationYear().fromAlias("e")), value(year)))
                                .where(eq(property(EnrCompetition.requestType().fromAlias("e")), value(requestType)))
                                .distinct()
                                .column(property(EnrCompetition.programSetOrgUnit().programSet().id().fromAlias("e")))));
                        result.add("Из них с делением приема по уровню образования: " + dao.getCount(new DQLSelectBuilder().fromEntity(EnrCompetition.class, "e")
                                .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().enrollmentCampaign().educationYear().fromAlias("e")), value(year)))
                                .where(eq(property(EnrCompetition.requestType().fromAlias("e")), value(requestType)))
                                .where(notIn(property(EnrCompetition.programSetOrgUnit().programSet().methodDivCompetitions().code().fromAlias("e")),
                                             EnrMethodDivCompetitionsCodes.BS_NO_DIV,
                                             EnrMethodDivCompetitionsCodes.HIGHER_NO_DIV,
                                             EnrMethodDivCompetitionsCodes.POSTGRADUATE_NO_DIV,
                                             EnrMethodDivCompetitionsCodes.TRAINEESHIP_NO_DIV,
                                             EnrMethodDivCompetitionsCodes.INTERNSHIP_NO_DIV,
                                             EnrMethodDivCompetitionsCodes.MASTER_NO_DIV,
                                             EnrMethodDivCompetitionsCodes.SEC_NO_DIV))
                                .distinct()
                                .column(DQLFunctions.count(property(EnrCompetition.programSetOrgUnit().programSet().id().fromAlias("e"))))));

                        final List<Object[]> programSetsWithManyPSOU = dao.getList(new DQLSelectBuilder().fromEntity(EnrCompetition.class, "e")
                                .joinPath(DQLJoinType.inner, EnrCompetition.programSetOrgUnit().fromAlias("e"), "f")
                                .joinEntity("e", DQLJoinType.inner, EnrProgramSetOrgUnit.class, "d", eq(property("d", EnrProgramSetOrgUnit.programSet()), property("e", EnrCompetition.programSetOrgUnit().programSet())))
                                .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().enrollmentCampaign().educationYear().fromAlias("e")), value(year)))
                                .group(property("e", EnrCompetition.programSetOrgUnit().programSet().id()))
                                .where(eq(property(EnrCompetition.requestType().fromAlias("e")), value(requestType)))
                                .column(DQLFunctions.count(property("d", EnrProgramSetOrgUnit.id())))
                                .column(property("e", EnrCompetition.programSetOrgUnit().programSet().id()))
                                .distinct());

                        int countPSwithManyPSOU = 0;

                        for (Object[] objects : programSetsWithManyPSOU) {
                           if (((long) objects[0]) > 1L)
                               countPSwithManyPSOU++;
                        }

                        result.add("Из них с несколькими филиалами: " + countPSwithManyPSOU);

                    }


                    result.addAll(competitionsTotal);

                    result.add("Число используемых видов ЦП (максимальное в ПК): " + tAInECKind);
                    result.add("Число конкурсов: " + dao.getCount(new DQLSelectBuilder().fromEntity(EnrCompetition.class, "e")
                            .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().enrollmentCampaign().educationYear().fromAlias("e")), value(year)))));
                    result.add("Число наборов ВИ: " + dao.getCount(new DQLSelectBuilder().fromEntity(EnrExamSet.class, "e")
                            .where(eq(property(EnrExamSet.enrollmentCampaign().educationYear().fromAlias("e")), value(year)))));
                    result.add("Число абитуриентов: " + dao.getCount(new DQLSelectBuilder().fromEntity(EnrEntrant.class, "e")
                            .where(eq(property(EnrEntrant.enrollmentCampaign().educationYear().fromAlias("e")), value(year)))));
                    result.add("Число заявлений: " + dao.getCount(new DQLSelectBuilder().fromEntity(EnrEntrantRequest.class, "e")
                            .where(eq(property(EnrEntrantRequest.entrant().enrollmentCampaign().educationYear().fromAlias("e")), value(year)))));
                    result.add("Индивидуальных достижений: " + dao.getCount(new DQLSelectBuilder().fromEntity(EnrEntrantAchievement.class, "e")
                            .where(eq(property(EnrEntrantAchievement.entrant().enrollmentCampaign().educationYear().fromAlias("e")), value(year)))));
                    result.add("Приказов о зачислении: " + dao.getCount(new DQLSelectBuilder().fromEntity(EnrOrder.class, "e")
                            .where(eq(property(EnrOrder.enrollmentCampaign().educationYear().fromAlias("e")), value(year)))
                            .where(eq(property(EnrOrder.type().code().fromAlias("e")), value(EnrOrderTypeCodes.ENROLLMENT)))));
                    result.add("Приказов об отмене зачисления: " + dao.getCount(new DQLSelectBuilder().fromEntity(EnrOrder.class, "e")
                            .where(eq(property(EnrOrder.enrollmentCampaign().educationYear().fromAlias("e")), value(year)))
                            .where(eq(property(EnrOrder.type().code().fromAlias("e")), value(EnrOrderTypeCodes.CANCEL)))));
                    result.add("Распределений по профилям: " + dao.getCount(new DQLSelectBuilder().fromEntity(EnrEntrantAchievement.class, "e")
                            .where(eq(property(EnrEntrantAchievement.entrant().enrollmentCampaign().educationYear().fromAlias("e")), value(year)))));
                    result.add("ЭГ с включенными ДДС: " + dao.getCount(new DQLSelectBuilder().fromEntity(EnrExamGroup.class, "e")
                            .joinEntity("e", DQLJoinType.inner, EnrExamPassDiscipline.class, "d", eq(property("e"), property(EnrExamPassDiscipline.examGroup().fromAlias("d"))))
                            .where(eq(property(EnrExamPassDiscipline.discipline().enrollmentCampaign().educationYear().fromAlias("d")), value(year)))
                            .column(property("e"))
                            .distinct()));
                    result.add("Число сохраненных отчетов:");
                    result.addAll(reports);
                    result.add("Число онлайн-абитуриентов: " + dao.getCount(new DQLSelectBuilder().fromEntity(EnrOnlineEntrant.class, "e")
                            .where(eq(property(EnrOnlineEntrant.entrant().enrollmentCampaign().educationYear().fromAlias("e")), value(year)))));
                    result.add("Число отказов в приеме: " + dao.getCount(new DQLSelectBuilder().fromEntity(EnrEntrantRefusal.class, "e")
                            .where(eq(property(EnrEntrantRefusal.enrollmentCampaign().educationYear().fromAlias("e")), value(year)))));
                    result.add("Число абитуриентов по направлению Минобрнауки: " + dao.getCount(new DQLSelectBuilder().fromEntity(EnrEntrantForeignRequest.class, "e")
                            .where(eq(property(EnrEntrantForeignRequest.entrant().enrollmentCampaign().educationYear().fromAlias("e")), value(year)))));
                    result.add("Приказов по абитуриентам, поступающих по направлению Минобрнауки: " + dao.getCount(new DQLSelectBuilder().fromEntity(EnrOrder.class, "e")
                            .where(eq(property(EnrOrder.enrollmentCampaign().educationYear().fromAlias("e")), value(year)))
                            .where(eq(property(EnrOrder.type().code().fromAlias("e")), value(EnrOrderTypeCodes.ENROLLMENT_MIN)))));


                    result.add("");
                }


                return result;
            })
            .create();
    }
}
