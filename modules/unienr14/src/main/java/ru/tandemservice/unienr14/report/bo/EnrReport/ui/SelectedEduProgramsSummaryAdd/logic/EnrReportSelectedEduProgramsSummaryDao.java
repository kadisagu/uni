/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.SelectedEduProgramsSummaryAdd.logic;

import jxl.JXLException;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.SelectedEduProgramsSummaryAdd.EnrReportSelectedEduProgramsSummaryAddUI;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.CellMergeMap;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportUtil;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.ExcelReportUtils;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.FilterParametersPrinter;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder;
import ru.tandemservice.unienr14.report.entity.EnrReportSelectedEduProgramsSummary;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.EnrRequestedProgram;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.Boolean;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 07.07.2014
 */
public class EnrReportSelectedEduProgramsSummaryDao extends UniBaseDao implements IEnrReportSelectedEduProgramsSummaryDao {
    @Override
    public long createReport(EnrReportSelectedEduProgramsSummaryAddUI model) {

        EnrReportSelectedEduProgramsSummary report = new EnrReportSelectedEduProgramsSummary();
        
        
        report.setFormingDate(new Date());

        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateSelector().getDateFrom());
        report.setDateTo(model.getDateSelector().getDateTo());
        report.setStage(model.getStageSelector().getStage().getTitle());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, EnrReportSelectedEduProgramsSummary.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE, EnrReportSelectedEduProgramsSummary.P_COMPENSATION_TYPE, "shortTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, EnrReportSelectedEduProgramsSummary.P_PROGRAM_FORM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, EnrReportSelectedEduProgramsSummary.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, EnrReportSelectedEduProgramsSummary.P_FORMATIVE_ORG_UNIT, "fullTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, EnrReportSelectedEduProgramsSummary.P_PROGRAM_SUBJECT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, EnrReportSelectedEduProgramsSummary.P_EDU_PROGRAM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, EnrReportSelectedEduProgramsSummary.P_PROGRAM_SET, "title");
        if (model.getParallelSelector().isParallelActive())
            report.setParallel(model.getParallelSelector().getParallel().getTitle());
        report.setFirstPriorityOnly(model.isFirstPriorityOnly() ? "Да" : "Нет");
        report.setIncludeDetailSheets(model.isIncludeDetailSheets() ? "Да" : "Нет");
        report.setRowFormingType(model.getRowFormingType().getTitle());
        DatabaseFile content = new DatabaseFile();

        try {
            content.setContent(buildReport(model));
        }
        catch (JXLException e)
        {
            CoreExceptionUtils.getRuntimeException(e);
        }
        catch (IOException e)
        {
            CoreExceptionUtils.getRuntimeException(e);
        }
        content.setFilename("EnrReport.xls");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_VND_MS_EXCEL);
        save(content);

        report.setContent(content);
        save(report);


        return report.getId();
    }

    private byte[] buildReport(EnrReportSelectedEduProgramsSummaryAddUI model) throws JXLException, IOException
    {
        // документ
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        WritableWorkbook workbook = Workbook.createWorkbook(out);


        // Фильтры
        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder()
                .notArchiveOnly()
                .filter(model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo())
                .filter(model.getEnrollmentCampaign());
        filterAddon.applyFilters(requestedCompDQL, requestedCompDQL.competition());

        if (model.getParallelSelector().isParallelActive())
        {
            if (model.getParallelSelector().isParallelOnly())
                requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.TRUE)));
            if (model.getParallelSelector().isSkipParallel())
                requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.FALSE)));
        }

        model.getStageSelector().applyFilter(requestedCompDQL, "reqComp");

        if (model.isFirstPriorityOnly())
        {
            requestedCompDQL
                    .where(eq(property("reqComp", EnrRequestedCompetition.priority()), value(1)));
        }

        requestedCompDQL
                .joinEntity("reqComp", DQLJoinType.left, EnrRequestedProgram.class, "reqProg", eq(property("reqComp"), property(EnrRequestedProgram.requestedCompetition().fromAlias("reqProg"))))
                .resetColumns()
                .column(property("reqComp"))
                .column(property("reqProg"));
        List<Object[]> fromQueryPairList = getList(requestedCompDQL);

        WritableSheet sheet = workbook.createSheet("Отчет", 0);

        if (model.getRowFormingType().getId() == 0L)
            printContentsSubjectFormingType(model, sheet, fromQueryPairList);
        else
            printContentsOPFormingType(model, sheet, fromQueryPairList);

        requestedCompDQL.resetColumns().column(property("reqProg")).where(isNotNull(property("reqProg")));

        List<EnrRequestedProgram> fromQueryReqProgramList = getList(requestedCompDQL);

        requestedCompDQL
                .joinEntity("reqComp", DQLJoinType.inner, EnrRatingItem.class, "rating", eq(property("reqComp"), property(EnrRatingItem.requestedCompetition().fromAlias("rating"))))
                .resetColumns()
                .column(property("rating"));

        int[] widths = new int[]{80, 10, 10, 10, 10, 10, 10, 10};

        ExcelReportUtils.setWidths(widths, sheet);

        if (model.isIncludeDetailSheets()) {
            List<EnrRatingItem> ratingItemList = getList(requestedCompDQL);
            Map<EnrRequestedCompetition, EnrRatingItem> ratingItemMap = new HashMap<>();

            for (EnrRatingItem item : ratingItemList) {
                if (!ratingItemList.contains(item.getRequestedCompetition()))
                    ratingItemMap.put(item.getRequestedCompetition(), item);
            }

            buildDetails(model, workbook, fromQueryReqProgramList, ratingItemMap);
        }
        workbook.write();
        workbook.close();
        byte[] bytes = out.toByteArray();
        out.close();

        return bytes;
    }

    // Строит отчет для выбранного способа формирования по направлениям
    private void printContentsSubjectFormingType(EnrReportSelectedEduProgramsSummaryAddUI model, WritableSheet sheet, List<Object[]> fromQueryList) throws JXLException
    {
        boolean budget =((CompensationType)model.getCompetitionFilterAddon().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE).getValue()).isBudget();


        // JXL форматы и шрифты
        WritableFont contentFont = new WritableFont(WritableFont.createFont("Calibri"), 11);

        WritableFont groupFont = new WritableFont(WritableFont.createFont("Calibri"), 11);
        groupFont.setBoldStyle(WritableFont.BOLD);
        groupFont.setColour(Colour.GRAY_50);

        WritableCellFormat contentFormat = new WritableCellFormat(contentFont);
        contentFormat.setBorder(Border.ALL, BorderLineStyle.THIN);

        WritableFont subjectFont = new WritableFont(WritableFont.createFont("Calibri"), 11);
        subjectFont.setBoldStyle(WritableFont.BOLD);

        WritableCellFormat subjectFormat = new WritableCellFormat(subjectFont);
        subjectFormat.setBorder(Border.ALL, BorderLineStyle.THIN);

        WritableCellFormat groupFormat = new WritableCellFormat(groupFont);
        groupFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
        groupFormat.setAlignment(Alignment.CENTRE);
        groupFormat.setVerticalAlignment(VerticalAlignment.CENTRE);


        Comparator<EduProgramSubject> subjectComparator = new Comparator<EduProgramSubject>() {
            @Override
            public int compare(EduProgramSubject o1, EduProgramSubject o2) {
                int res = o1.getGroupTitle().compareTo(o2.getGroupTitle());
                if (res != 0) return res;
                return o1.getTitleWithCode().compareTo(o2.getTitleWithCode());
            }
        };

        Comparator<EduProgramSpecialization> specializationComparator = new Comparator<EduProgramSpecialization>() {
            @Override
            public int compare(EduProgramSpecialization o1, EduProgramSpecialization o2) {
                if (o1 == o2) return 0;
                if (o1 == null) return 1;
                if (o2 == null) return -1;


                return o1.getTitle().compareToIgnoreCase(o2.getTitle());
            }
        };


        // Максимальный приоритет (в числовом смысле)
        int max = 0;

        // Заполняем карту данными
        Map<EduProgramSubject, Map<EduProgramSpecialization, Map<Integer, Set<EnrRequestedCompetition>>>> dataMap = new TreeMap<>(subjectComparator);
        for (Object[] pair : fromQueryList)
        {
            EnrRequestedCompetition reqComp = (EnrRequestedCompetition) pair[0];
            EnrRequestedProgram reqProgram = (EnrRequestedProgram) pair[1];
            EduProgramHigherProf eduProgram = null;
            if (reqProgram != null)
                eduProgram = reqProgram.getProgramSetItem().getProgram();
            int priority = reqProgram == null ? 1 : reqProgram.getPriority();


            EduProgramSpecialization specialization = eduProgram == null ? null : eduProgram.getProgramSpecialization();
            EduProgramSubject subject = reqComp.getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramSubject();

            Map<EduProgramSpecialization, Map<Integer, Set<EnrRequestedCompetition>>> eduProgramSpecializationMap = SafeMap.safeGet(dataMap, subject, TreeMap.class, specializationComparator);
            Map<Integer, Set<EnrRequestedCompetition>> priorityMap = SafeMap.safeGet(eduProgramSpecializationMap, specialization, HashMap.class);
            Set<EnrRequestedCompetition> requestedCompetitionSet = SafeMap.safeGet(priorityMap, priority, HashSet.class);
            requestedCompetitionSet.add(reqComp);

            if (reqComp.getPriority() > max) max = reqComp.getPriority();
            if (reqProgram != null && priority > max) max = priority;
        }

        // Печатаем header таблицы
        int rowIndex = printHeader(model, sheet, max);

        // Переменные для детализации по группе и по всему ОУ
        String prevGroupTitle = null;
        List<String[]> groupTable = new ArrayList<>();
        List<Boolean> groupSubjectRowMarkers = new ArrayList<>();
        Integer groupPlacesCount = 0;
        Integer totalPlacesCount = 0;
        Integer groupTotalRequested = 0;
        Integer totalRequested = 0;



        for (Map.Entry<EduProgramSubject, Map<EduProgramSpecialization, Map<Integer, Set<EnrRequestedCompetition>>>> subjectEntry : dataMap.entrySet())
        {
            if (prevGroupTitle == null)
                prevGroupTitle = subjectEntry.getKey().getGroupTitle();

            if (!prevGroupTitle.equals(subjectEntry.getKey().getGroupTitle()))
            {
                List<String> row = new ArrayList<>();
                row.add(prevGroupTitle);
                row.add(String.valueOf(groupPlacesCount));
                row.add(String.valueOf(groupTotalRequested));
                for (int i = 0; i < max; i++) {
                    row.add("");
                }
                sheet.setRowView(rowIndex, 450);
                ExcelReportUtils.printrow(sheet, row, rowIndex++, groupFormat);
                for (int i = 0; i < groupTable.size(); i++) {
                    if (groupSubjectRowMarkers.get(i))
                        ExcelReportUtils.printrow(sheet, groupTable.get(i), rowIndex++, subjectFormat);
                    else
                        ExcelReportUtils.printrow(sheet, groupTable.get(i), rowIndex++, contentFormat);
                }

                totalRequested += groupTotalRequested;
                totalPlacesCount += groupPlacesCount;
                groupTable.clear();
                groupSubjectRowMarkers.clear();
                groupPlacesCount = 0;
                groupTotalRequested = 0;
                prevGroupTitle = subjectEntry.getKey().getGroupTitle();
            }

            Map<Integer, Set<EnrRequestedCompetition>> subjectDetails = SafeMap.get(HashSet.class);
            List<String[]> tTable = new ArrayList<>();

            Set<EnrProgramSetOrgUnit> orgUnits = new HashSet<>();

            for (Map.Entry<EduProgramSpecialization, Map<Integer, Set<EnrRequestedCompetition>>> specializationEntry : subjectEntry.getValue().entrySet())
            {
                Map<Integer, Set<EnrRequestedCompetition>> priorityMap = specializationEntry.getValue();

                int total = 0;

                List<Integer> values = new ArrayList<>();

                for (int i = 1; i <= max; i++) {
                    if (priorityMap.containsKey(i)) {
                        values.add(priorityMap.get(i).size());
                        total += priorityMap.get(i).size();
                        for (EnrRequestedCompetition reqComp : priorityMap.get(i)) {
                            subjectDetails.get(reqComp.getPriority()).add(reqComp);
                            orgUnits.add(reqComp.getCompetition().getProgramSetOrgUnit());
                        }
                    }
                    else values.add(0);
                }

                if (specializationEntry.getKey() == null)
                    continue;

                String title = specializationEntry.getKey().getTitle();
                List<String> row = new ArrayList<>();
                // Title - cell 0;
                row.add(title);
                // Plan - cell 1;
                row.add("");
                // Total requested cell 2;
                row.add(String.valueOf(total));
                // Cells >2 priority details;
                for (Integer i : values)
                {
                    if (i != 0)
                        row.add(String.valueOf(i));
                    else row.add("");
                }

                tTable.add(row.toArray(new String[row.size()]));
            }


            List<String> row = new ArrayList<>();
            int total = 0;
            List<Integer> values = new ArrayList<>();
            for (int i = 1; i <= max; i++) {
                if (subjectDetails.containsKey(i)) {
                    values.add(subjectDetails.get(i).size());
                    total += subjectDetails.get(i).size();
                }
                else values.add(0);
            }
            groupTotalRequested += total;
            row.add(subjectEntry.getKey().getTitleWithCode());
            Integer placesCount = 0;
            for (EnrProgramSetOrgUnit orgUnit : orgUnits)
                if (budget)
                    placesCount += orgUnit.getMinisterialPlan();
                else placesCount += orgUnit.getContractPlan();

            row.add(String.valueOf(placesCount));

            groupPlacesCount += placesCount;

            row.add(String.valueOf(total));
            for (Integer i : values)
            {
                if (i != 0)
                    row.add(String.valueOf(i));
                else row.add("");
            }

            groupTable.add(row.toArray(new String[row.size()]));
            groupSubjectRowMarkers.add(true);
            if (tTable.size() > 1) {
                groupTable.addAll(tTable);
                for (String[] s : tTable)
                    groupSubjectRowMarkers.add(false);
            }

        }
        List<String> row = new ArrayList<>();
        row.add(prevGroupTitle);
        row.add(String.valueOf(groupPlacesCount));
        row.add(String.valueOf(groupTotalRequested));
        for (int i = 0; i < max; i++) {
            row.add("");
        }
        sheet.setRowView(rowIndex, 450);
        ExcelReportUtils.printrow(sheet, row, rowIndex++, groupFormat);
        for (int i = 0; i < groupTable.size(); i++) {
            if (groupSubjectRowMarkers.get(i))
                ExcelReportUtils.printrow(sheet, groupTable.get(i), rowIndex++, subjectFormat);
            else
                ExcelReportUtils.printrow(sheet, groupTable.get(i), rowIndex++, contentFormat);
        }

        totalRequested += groupTotalRequested;
        totalPlacesCount += groupPlacesCount;


        WritableCellFormat totalFormat = new WritableCellFormat(subjectFont);

        totalFormat.setAlignment(Alignment.CENTRE);
        totalFormat.setBorder(Border.ALL, BorderLineStyle.THIN);

        ExcelReportUtils.printrow(sheet,
                new String[]{
                    "Итого по ОУ",
                    String.valueOf(totalPlacesCount),
                    String.valueOf(totalRequested)},
                rowIndex, totalFormat);

    }

    // Строит отчет  для выбранного способа формирования по ОП
    private void printContentsOPFormingType(EnrReportSelectedEduProgramsSummaryAddUI model, WritableSheet sheet, List<Object[]> fromQueryList) throws JXLException
    {
        boolean budget =((CompensationType)model.getCompetitionFilterAddon().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE).getValue()).isBudget();


        // JXL форматы и шрифты
        WritableFont contentFont = new WritableFont(WritableFont.createFont("Calibri"), 11);

        WritableFont groupFont = new WritableFont(WritableFont.createFont("Calibri"), 11);
        groupFont.setBoldStyle(WritableFont.BOLD);
        groupFont.setColour(Colour.GRAY_50);

        WritableCellFormat contentFormat = new WritableCellFormat(contentFont);
        contentFormat.setBorder(Border.ALL, BorderLineStyle.THIN);

        WritableFont subjectFont = new WritableFont(WritableFont.createFont("Calibri"), 11);
        subjectFont.setBoldStyle(WritableFont.BOLD);

        WritableCellFormat subjectFormat = new WritableCellFormat(subjectFont);
        subjectFormat.setBorder(Border.ALL, BorderLineStyle.THIN);

        WritableCellFormat groupFormat = new WritableCellFormat(groupFont);
        groupFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
        groupFormat.setAlignment(Alignment.CENTRE);
        groupFormat.setVerticalAlignment(VerticalAlignment.CENTRE);


        // Максимальный приоритет (в числовом смысле)
        int max = 0;

        Comparator<EnrProgramSetBase> programSetBaseComparator = new Comparator<EnrProgramSetBase>() {
            @Override
            public int compare(EnrProgramSetBase o1, EnrProgramSetBase o2) {
                int res = o1.getProgramSubject().getGroupTitle().compareToIgnoreCase(o2.getProgramSubject().getGroupTitle());
                if (res!=0) return res;
                return o1.getTitle().compareToIgnoreCase(o2.getTitle());
            }
        };

        Comparator<EduProgramHigherProf> eduProgramHigherProfComparator = new Comparator<EduProgramHigherProf>() {
            @Override
            public int compare(EduProgramHigherProf o1, EduProgramHigherProf o2) {
                if (o1 == o2) return 0;
                if (o1 == null) return 1;
                if (o2 == null) return -1;
                return o1.getTitle().compareToIgnoreCase(o2.getTitle());
            }
        };


        // Заполняем карту данными
        Map<EnrProgramSetBase, Map<EduProgramHigherProf, Map<Integer, Set<EnrRequestedCompetition>>>> dataMap = new TreeMap<>(programSetBaseComparator);
        for (Object[] pair : fromQueryList)
        {
            EnrRequestedCompetition reqComp = (EnrRequestedCompetition) pair[0];
            EnrRequestedProgram reqProgram = (EnrRequestedProgram) pair[1];
            EduProgramHigherProf higherProf = null;
            if (reqProgram != null)
                higherProf = reqProgram.getProgramSetItem().getProgram();
            int priority = reqProgram == null ? 1 : reqProgram.getPriority();


            EnrProgramSetBase programSetBase = reqComp.getCompetition().getProgramSetOrgUnit().getProgramSet();

            Map<EduProgramHigherProf, Map<Integer, Set<EnrRequestedCompetition>>> eduProgramHigherProfMap = SafeMap.safeGet(dataMap, programSetBase, TreeMap.class, eduProgramHigherProfComparator);
            Map<Integer, Set<EnrRequestedCompetition>> priorityMap = SafeMap.safeGet(eduProgramHigherProfMap, higherProf, HashMap.class);
            Set<EnrRequestedCompetition> requestedCompetitionSet = SafeMap.safeGet(priorityMap, priority, HashSet.class);
            requestedCompetitionSet.add(reqComp);



            if (reqComp.getPriority() > max) max = reqComp.getPriority();
            if (reqProgram != null && priority > max) max = priority;
        }

        // Печатаем header таблицы
        int rowIndex = printHeader(model, sheet, max);

        // Переменные для детализации по группе и по всему ОУ
        String prevGroupTitle = null;
        List<String[]> groupTable = new ArrayList<>();
        List<Boolean> groupSubjectRowMarkers = new ArrayList<>();
        Integer groupPlacesCount = 0;
        Integer totalPlacesCount = 0;
        Integer groupTotalRequested = 0;
        Integer totalRequested = 0;

        for (Map.Entry<EnrProgramSetBase, Map<EduProgramHigherProf, Map<Integer, Set<EnrRequestedCompetition>>>> programSetBaseEntry : dataMap.entrySet())
        {
            if (prevGroupTitle == null)
                prevGroupTitle = programSetBaseEntry.getKey().getProgramSubject().getGroupTitle();

            if (!prevGroupTitle.equals(programSetBaseEntry.getKey().getProgramSubject().getGroupTitle()))
            {
                List<String> row = new ArrayList<>();
                row.add(prevGroupTitle);
                row.add(String.valueOf(groupPlacesCount));
                row.add(String.valueOf(groupTotalRequested));
                for (int i = 0; i < max; i++) {
                    row.add("");
                }
                sheet.setRowView(rowIndex, 450);
                ExcelReportUtils.printrow(sheet, row, rowIndex++, groupFormat);
                for (int i = 0; i < groupTable.size(); i++) {
                    if (groupSubjectRowMarkers.get(i))
                        ExcelReportUtils.printrow(sheet, groupTable.get(i), rowIndex++, subjectFormat);
                    else
                        ExcelReportUtils.printrow(sheet, groupTable.get(i), rowIndex++, contentFormat);
                }

                totalRequested += groupTotalRequested;
                totalPlacesCount += groupPlacesCount;
                groupTable.clear();
                groupSubjectRowMarkers.clear();
                groupPlacesCount = 0;
                groupTotalRequested = 0;
                prevGroupTitle = programSetBaseEntry.getKey().getProgramSubject().getGroupTitle();
            }

            Map<Integer, Set<EnrRequestedCompetition>> programSetBaseDetails = SafeMap.get(HashSet.class);
            List<String[]> tTable = new ArrayList<>();

            Set<EnrProgramSetOrgUnit> orgUnits = new HashSet<>();

            // Строки с ОП
            for (Map.Entry<EduProgramHigherProf, Map<Integer, Set<EnrRequestedCompetition>>> higherProfEntry : programSetBaseEntry.getValue().entrySet())
            {
                Map<Integer, Set<EnrRequestedCompetition>> priorityMap = higherProfEntry.getValue();

                int total = 0;
                List<Integer> values = new ArrayList<>();

                for (int i = 1; i <= max; i++) {
                    if (priorityMap.containsKey(i)) {
                        values.add(priorityMap.get(i).size());
                        total += priorityMap.get(i).size();
                        for (EnrRequestedCompetition reqComp : priorityMap.get(i)) {
                            programSetBaseDetails.get(reqComp.getPriority()).add(reqComp);
                            orgUnits.add(reqComp.getCompetition().getProgramSetOrgUnit());
                        }
                    }
                    else values.add(0);
                }

                if (higherProfEntry.getKey() == null)
                    continue;


                String title = higherProfEntry.getKey().getTitleAndConditionsShort();
                List<String> row = new ArrayList<>();
                // Title - cell 0;
                row.add(title);


                // Plan - cell 1;
                row.add("");
                // Total requested cell 2;
                row.add(String.valueOf(total));
                // Cells >2 priority details;
                for (Integer i : values)
                {
                    if (i != 0)
                        row.add(String.valueOf(i));
                    else row.add("");
                }

                tTable.add(row.toArray(new String[row.size()]));
            }


            List<String> row = new ArrayList<>();
            int total = 0;
            List<Integer> values = new ArrayList<>();
            for (int i = 1; i <= max; i++) {
                if (programSetBaseDetails.containsKey(i)) {
                    values.add(programSetBaseDetails.get(i).size());
                    total += programSetBaseDetails.get(i).size();
                }
                else values.add(0);
            }
            groupTotalRequested += total;
            row.add(programSetBaseEntry.getKey().getTitle());
            Integer placesCount = 0;
            for (EnrProgramSetOrgUnit orgUnit : orgUnits)
                if (budget)
                    placesCount += orgUnit.getMinisterialPlan();
                else placesCount += orgUnit.getContractPlan();

            row.add(String.valueOf(placesCount));

            groupPlacesCount += placesCount;

            row.add(String.valueOf(total));
            for (Integer i : values)
            {
                if (i != 0)
                    row.add(String.valueOf(i));
                else row.add("");
            }

            groupTable.add(row.toArray(new String[row.size()]));
            groupSubjectRowMarkers.add(true);
            if (tTable.size() > 1) {
                groupTable.addAll(tTable);
                for (String[] s : tTable)
                    groupSubjectRowMarkers.add(false);
            }
        }


        List<String> row = new ArrayList<>();
        row.add(prevGroupTitle);
        row.add(String.valueOf(groupPlacesCount));
        row.add(String.valueOf(groupTotalRequested));
        for (int i = 0; i < max; i++) {
            row.add("");
        }
        sheet.setRowView(rowIndex, 450);
        ExcelReportUtils.printrow(sheet, row, rowIndex++, groupFormat);
        for (int i = 0; i < groupTable.size(); i++) {
            if (groupSubjectRowMarkers.get(i))
                ExcelReportUtils.printrow(sheet, groupTable.get(i), rowIndex++, subjectFormat);
            else
                ExcelReportUtils.printrow(sheet, groupTable.get(i), rowIndex++, contentFormat);
        }

        totalRequested += groupTotalRequested;
        totalPlacesCount += groupPlacesCount;


        WritableCellFormat totalFormat = new WritableCellFormat(subjectFont);

        totalFormat.setAlignment(Alignment.CENTRE);
        totalFormat.setBorder(Border.ALL, BorderLineStyle.THIN);

        ExcelReportUtils.printrow(sheet,
                new String[]{
                    "Итого по ОУ",
                    String.valueOf(totalPlacesCount),
                    String.valueOf(totalRequested)},
                rowIndex, totalFormat);

    }

    // Строит детализацию для отчета. В случае, если листов с детализацией > 25, выдает ошибку.
    private void buildDetails(EnrReportSelectedEduProgramsSummaryAddUI model,
                              WritableWorkbook workbook,
                              List<EnrRequestedProgram> programs,
                              Map<EnrRequestedCompetition,
                              EnrRatingItem> ratingItemMap) throws JXLException
    {

        // JXL форматы и шрифты
        WritableFont contentFont = new WritableFont(WritableFont.createFont("Calibri"), 11);
        WritableCellFormat contentFormat = new WritableCellFormat(contentFont);
        contentFormat.setBorder(Border.ALL, BorderLineStyle.THIN);

        WritableCellFormat headerFormat = new WritableCellFormat(contentFont);
        headerFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
        headerFormat.setBackground(Colour.GREY_25_PERCENT);
        headerFormat.setWrap(true);
        headerFormat.setAlignment(Alignment.CENTRE);
        headerFormat.setVerticalAlignment(VerticalAlignment.CENTRE);


        WritableFont programSetBaseFont = new WritableFont(WritableFont.createFont("Calibri"), 11);
        programSetBaseFont.setBoldStyle(WritableFont.BOLD);
        WritableCellFormat programSetBaseFormat = new WritableCellFormat(programSetBaseFont);


        WritableFont titleFont = new WritableFont(WritableFont.createFont("Calibri"), 12);
        WritableCellFormat titleFormat = new WritableCellFormat(titleFont);

        WritableFont titleBoldFont = new WritableFont(WritableFont.createFont("Calibri"), 12);
        titleBoldFont.setBoldStyle(WritableFont.BOLD);
        WritableCellFormat titleBoldFormat = new WritableCellFormat(titleBoldFont);

        // Массив с ширинами ячеек
        int[] widths = new int[]{4, 15, 30, 15, 12, 12, 12};


        if (model.getRowFormingType().getId() == 0L) {
            Map<EduProgramSubject, Map<EduProgramSpecialization, Map<EnrProgramSetItem, Set<EnrRequestedProgram>>>> dataMap = new HashMap<>();
            for (EnrRequestedProgram program : programs)
            {
                EduProgramSpecialization specialization = program.getProgramSetItem().getProgram().getProgramSpecialization();
                EnrProgramSetItem programSetItem = program.getProgramSetItem();
                EduProgramSubject subject = programSetItem.getProgramSet().getProgramSubject();
                Map<EduProgramSpecialization, Map<EnrProgramSetItem, Set<EnrRequestedProgram>>> eduProgramSpecializationMap = SafeMap.safeGet(dataMap, subject, HashMap.class);
                Map<EnrProgramSetItem, Set<EnrRequestedProgram>> enrProgramSetItemMap = SafeMap.safeGet(eduProgramSpecializationMap, specialization, HashMap.class);
                Set<EnrRequestedProgram> enrRequestedPrograms = SafeMap.safeGet(enrProgramSetItemMap, programSetItem, HashSet.class);
                enrRequestedPrograms.add(program);
            }


            int count = 0;
            for (Map.Entry<EduProgramSubject, Map<EduProgramSpecialization, Map<EnrProgramSetItem, Set<EnrRequestedProgram>>>> subjectEntry : dataMap.entrySet()) {
                count+=subjectEntry.getValue().size();
            }

            if (count > 25)
                model.getSupport().error("Отчет не может быть построен, т.к. число попавших в статистику отчета направленностей (обр. программ.) превышает 25. Выберите дополнительные параметры для получения меньшего числа направленностей (ОП)", "details");

            for (Map.Entry<EduProgramSubject, Map<EduProgramSpecialization, Map<EnrProgramSetItem, Set<EnrRequestedProgram>>>> subjectEntry : dataMap.entrySet()) {
                for (Map.Entry<EduProgramSpecialization, Map<EnrProgramSetItem, Set<EnrRequestedProgram>>> specializationEntry : subjectEntry.getValue().entrySet()) {
                    int rowIndex = 1;
                    StringBuilder builder = new StringBuilder().append(specializationEntry.getKey().getTitle());
                    if (builder.length() > 25)
                        builder.setLength(25);
                    WritableSheet sheet = workbook.createSheet(builder.toString(), workbook.getNumberOfSheets());
                    ExcelReportUtils.setWidths(widths, sheet);
                    ExcelReportUtils.printrow(sheet, new String[]{"Направление: " + subjectEntry.getKey().getTitleWithCode()}, rowIndex++, titleBoldFormat);
                    ExcelReportUtils.printrow(sheet, new String[]{"Направленность: " + specializationEntry.getKey().getTitle()}, rowIndex++, titleBoldFormat);
                    rowIndex++;

                    for (Map.Entry<EnrProgramSetItem, Set<EnrRequestedProgram>> programSetItemEntry : specializationEntry.getValue().entrySet()) {
                        List<String[]> content = buildEduProgramDetailsList(programSetItemEntry.getKey(), programSetItemEntry.getValue(), ratingItemMap);
                        boolean prevCellWasBold = false;
                        boolean headerIsPrinted = false;
                        for (String[] row : content) {
                            if (row == null)
                            {
                                rowIndex++;
                                headerIsPrinted = false;
                                continue;
                            }
                            if (row.length == 1 && !prevCellWasBold)
                            {
                                ExcelReportUtils.printrow(sheet, row, rowIndex++, titleBoldFormat);
                                prevCellWasBold = true;
                                continue;
                            }
                            if (row.length == 1 && prevCellWasBold)
                            {
                                ExcelReportUtils.printrow(sheet, row, rowIndex++, titleFormat);
                                prevCellWasBold = false;
                                continue;
                            }
                            if (!headerIsPrinted)
                            {
                                ExcelReportUtils.printrow(sheet, row, rowIndex++, headerFormat);
                                headerIsPrinted = true;
                            }
                            else ExcelReportUtils.printrow(sheet, row, rowIndex++, contentFormat);
                        }

                    }


                }
            }
        }
        else {
            Map<EnrProgramSetItem, Set<EnrRequestedProgram>> dataMap = new HashMap<>();
            for (EnrRequestedProgram program : programs)
            {
                EnrProgramSetItem programSetItem = program.getProgramSetItem();
                final Set<EnrRequestedProgram> enrRequestedPrograms = SafeMap.safeGet(dataMap, programSetItem, HashSet.class);
                enrRequestedPrograms.add(program);
            }

            if (dataMap.size() > 25)
                model.getSupport().error("Отчет не может быть построен, т.к. число попавших в статистику отчета направленностей (обр. программ.) превышает 25. Выберите дополнительные параметры для получения меньшего числа направленностей (ОП)", "details");

            for (Map.Entry<EnrProgramSetItem, Set<EnrRequestedProgram>> programSetItemEntry : dataMap.entrySet()) {
                int rowIndex = 1;
                StringBuilder builder = new StringBuilder().append(programSetItemEntry.getKey().getTitle());
                if (builder.length() > 25)
                    builder.setLength(25);
                WritableSheet sheet = workbook.createSheet(builder.toString(), workbook.getNumberOfSheets());
                ExcelReportUtils.setWidths(widths, sheet);
                List<String[]> content = buildEduProgramDetailsList(programSetItemEntry.getKey(), programSetItemEntry.getValue(), ratingItemMap);
                boolean prevCellWasBold = false;
                boolean headerIsPrinted = false;
                for (String[] row : content) {
                    if (row == null)
                    {
                        rowIndex++;
                        headerIsPrinted = false;
                        continue;
                    }
                    if (row.length == 1 && !prevCellWasBold)
                    {
                        ExcelReportUtils.printrow(sheet, row, rowIndex++, titleBoldFormat);
                        prevCellWasBold = true;
                        continue;
                    }
                    if (row.length == 1 && prevCellWasBold)
                    {
                        ExcelReportUtils.printrow(sheet, row, rowIndex++, titleFormat);
                        prevCellWasBold = false;
                        continue;
                    }
                    if (!headerIsPrinted)
                    {
                        ExcelReportUtils.printrow(sheet, row, rowIndex++, headerFormat);
                        headerIsPrinted = true;
                    }
                    else ExcelReportUtils.printrow(sheet, row, rowIndex++, contentFormat);
                }
            }
        }
    }

    // Строит детализацию по ОП
    private List<String[]> buildEduProgramDetailsList(EnrProgramSetItem programSetItem, Set<EnrRequestedProgram> programSet, Map<EnrRequestedCompetition, EnrRatingItem> ratingItemMap)
    {
        Comparator<EnrCompetition> competitionComparator = new Comparator<EnrCompetition>() {
            @Override
            public int compare(EnrCompetition o1, EnrCompetition o2) {
                int res = o1.getType().getCode().compareTo(o2.getType().getCode());
                if (res != 0) return res;
                res = o1.getEduLevelRequirement().getCode().compareTo(o2.getEduLevelRequirement().getCode());
                if (res != 0) return res;
                return o1.getId().compareTo(o2.getId());
            }
        };

        List<String[]> output = new ArrayList<>();
        Map<EnrCompetition, Set<EnrRequestedProgram>> dataMap = new TreeMap<>(competitionComparator);

        for (EnrRequestedProgram program : programSet)
        {
            EnrCompetition competition = program.getRequestedCompetition().getCompetition();
            SafeMap.safeGet(dataMap, competition, HashSet.class).add(program);
        }

        output.add(new String[]{"Набор ОП: " + programSetItem.getProgramSet().getTitle()});
        output.add(new String[]{"Обр. программа: " + programSetItem.getProgram().getTitleAndConditionsShort()});

        for (Map.Entry<EnrCompetition, Set<EnrRequestedProgram>> entry : dataMap.entrySet())
        {
            output.add(null);
            output.add(new String[]{"Вид приема: " + entry.getKey().getType().getTitle()});
            output.add(new String[]{"Огран. по ур. обр: " + entry.getKey().getEduLevelRequirement().getTitle()});
            output.add(new String[]{"№ п/п", "№ заявления", "Фамилия Имя Отчество", "Док. об образ.", "Сумма баллов", "Приоритет", "Приоритет конкурса"});
            int num = 0;
            List<PairKey<Integer, String[]>> rows = new ArrayList<>();
            for (EnrRequestedProgram program : entry.getValue())
            {
                List<String> row = new ArrayList<>();
                row.add(String.valueOf(++num));
                row.add(program.getRequestedCompetition().getRequest().getStringNumber());
                row.add(program.getRequestedCompetition().getRequest().getIdentityCard().getFullFio());
                row.add(program.getRequestedCompetition().isOriginalDocumentHandedIn() ? "оригиналы" : "копии");
                row.add(String.valueOf(ratingItemMap.get(program.getRequestedCompetition()).getTotalMarkAsDouble()));
                row.add(String.valueOf(program.getPriority()));
                row.add(String.valueOf(program.getCompetitionPriority()));
                PairKey<Integer, String[]> rowWithPosition = new PairKey<>(ratingItemMap.get(program.getRequestedCompetition()).getPosition(), row.toArray(new String[row.size()]));
                rows.add(rowWithPosition);
            }
            Collections.sort(rows, new Comparator<PairKey<Integer, String[]>>() {
                @Override
                public int compare(PairKey<Integer, String[]> o1, PairKey<Integer, String[]> o2) {
                    return o1.getFirst().compareTo(o2.getFirst());
                }
            });
            for (PairKey<Integer, String[]> row : rows) {
                output.add(row.getSecond());
            }

        }

        return output;

    }


    // Печатает header в переданном ему листе
    private int printHeader(EnrReportSelectedEduProgramsSummaryAddUI model, WritableSheet sheet, int max) throws JXLException
    {
        FilterParametersPrinter parametersPrinter = new FilterParametersPrinter();
        parametersPrinter.setParallelSelector(model.getParallelSelector());
        parametersPrinter.setStageSelector(model.getStageSelector());
        parametersPrinter.addVariableParameter("Формирование строк отчета", model.getRowFormingType().getTitle());
        parametersPrinter.addVariableParameter("Детализировать по профилям (образовательным программам)", model.isIncludeDetailSheets() ? "Да" : "Нет");
        parametersPrinter.addVariableParameter("Только по выбранным конкурсам с первым приоритетом", model.isFirstPriorityOnly() ? "Да" : "Нет");

        List<String[]> hTable = parametersPrinter.getTableForNonRtf(model.getCompetitionFilterAddon(), model.getDateSelector());

        int rowIndex = 1;
        WritableFont labelFont = new WritableFont(WritableFont.createFont("Calibri"), 14, WritableFont.BOLD);
        WritableFont headerFont = new WritableFont(WritableFont.createFont("Calibri"), 11, WritableFont.BOLD);

        WritableFont contentFont = new WritableFont(WritableFont.createFont("Calibri"), 11);


        WritableCellFormat labelFormat = new WritableCellFormat(labelFont);
        WritableCellFormat headerFormat = new WritableCellFormat(headerFont);
        WritableCellFormat filterTitlesFormat = new WritableCellFormat(headerFont);
        headerFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
        headerFormat.setBackground(Colour.GREY_25_PERCENT);
        headerFormat.setAlignment(Alignment.CENTRE);
        headerFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        filterTitlesFormat.setBorder(Border.ALL, BorderLineStyle.THIN);

        CellMergeMap mergeMap = new CellMergeMap();
        WritableCellFormat contentFormat = new WritableCellFormat(contentFont);

        ExcelReportUtils.printrow(sheet,
                new String[] { "Сводка по выбранным образовательным программам (профилям, специализациям) на" + DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()) },
                rowIndex++,
                labelFormat);

        for (int i = 0; i < hTable.size(); i++) {
            sheet.addCell(new Label(0, rowIndex, hTable.get(i)[0], filterTitlesFormat));
            sheet.addCell(new Label(1, rowIndex++, hTable.get(i)[1], contentFormat));
        }
        rowIndex++;

        int column = 0;

        mergeMap.addMapping(column, rowIndex, column++, rowIndex+1);
        mergeMap.addMapping(column, rowIndex, column++, rowIndex+1);
        mergeMap.addMapping(column, rowIndex, column++, rowIndex+1);
        mergeMap.addMapping(column, rowIndex, max + 2 , rowIndex);

        ExcelReportUtils.printrow(sheet, new String[]{
                model.getRowFormingType().getId() == 0L ? "Укрупненная группа/направление подготовки" : "Укрупненная группа / Набор образовательных программ",
                "План",
                "Всего",
                "Распределение по приоритетам"
        }, rowIndex++, headerFormat);

        List<String> lowerRow = new ArrayList<>();

        for (int i = 0; i < max + 3; i++) {
            if (i < 3)
                lowerRow.add("");
            else lowerRow.add(String.valueOf(i-2));
        }

        ExcelReportUtils.printrow(sheet, lowerRow, rowIndex++, headerFormat);
        mergeMap.makeMerges(sheet);

        return rowIndex;
    }
}
