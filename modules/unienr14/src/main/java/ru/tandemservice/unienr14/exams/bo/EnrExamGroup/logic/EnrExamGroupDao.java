/**
 *$Id: EnrExamGroupDao.java 34429 2014-05-26 10:13:50Z oleyba $
 */
package ru.tandemservice.unienr14.exams.bo.EnrExamGroup.logic;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Session;
import org.joda.time.DateTime;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.exams.bo.EnrExamGroup.EnrExamGroupManager;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroup;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroupScheduleEvent;
import ru.tandemservice.unienr14.exams.entity.EnrExamScheduleEvent;
import ru.tandemservice.unienr14.exams.entity.gen.EnrExamGroupScheduleEventGen;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 29.05.13
 */
public class EnrExamGroupDao extends UniBaseDao implements IEnrExamGroupDao
{
    protected static void lockEnrollmentCampaign(Session session, EnrEnrollmentCampaign enrollmentCampaign) {
        NamedSyncInTransactionCheckLocker.register(session, "enrollmentCampaignExamSetLock-" + enrollmentCampaign.getId());
    }

    protected void lockEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign) {
        lockEnrollmentCampaign(getSession(), enrollmentCampaign);
    }

    protected boolean calculateClosed(EnrExamGroup examGroup) {
        return examGroup.getSize() == getCount(EnrExamPassDiscipline.class, EnrExamPassDiscipline.examGroup().s(), examGroup);
    }

    /**
     * Формирует название ЭГ автоматически:
     * представляют из себя натуральные числа, начиная с 1, при добавлении ЭГ в качестве ее названия берется наибольшее число из названий уже созданных ЭГ в ПК +1
     * @param enrollmentCampaign ПК, в рамках кототорой добавляется ЭГ
     * @return сформированное название
     */
    protected String formExamGroupTitle(EnrEnrollmentCampaign enrollmentCampaign)
    {
        Integer title = 1;
        final List<String> usedTitleList = new DQLSelectBuilder().fromEntity(EnrExamGroup.class, "b").column(property(EnrExamGroup.title().fromAlias("b")))
            .where(eq(property(EnrExamGroup.examGroupSet().enrollmentCampaign().fromAlias("b")), value(enrollmentCampaign)))
            .createStatement(getSession()).list();

        while (usedTitleList.contains(title.toString()))
        {
            title++;
        }

        return title.toString();
    }

    protected ErrorCollector validateSetScheduleEvent(EnrExamGroup examGroup, Collection<EnrExamScheduleEvent> examScheduleEventList)
    {
        final ErrorCollector errorCollector = ContextLocal.getErrorCollector();

        final Set<Date> uniqDateSet = new HashSet<>();
        for (EnrExamScheduleEvent scheduleEvent : examScheduleEventList)
            uniqDateSet.add(CoreDateUtils.getDayFirstTimeMoment(scheduleEvent.getScheduleEvent().getDurationBegin()));
        if (uniqDateSet.size() != examScheduleEventList.size())
            errorCollector.add(EnrExamGroupManager.instance().getProperty("examGroupDao.doSetScheduleEvent.doubleEventDateException"));

        if (examGroup.getDays() != examScheduleEventList.size())
            errorCollector.add(EnrExamGroupManager.instance().getProperty("examGroupDao.doSetScheduleEvent.wrongEventCountException", examGroup.getDays()));

        return errorCollector;
    }


    @Override
    public Long saveOrUpdate(EnrExamGroup examGroup)
    {
        lockEnrollmentCampaign(examGroup.getExamGroupSet().getEnrollmentCampaign());

        // validate
        if (examGroup.getId() != null)
        {
            final Long examDiscCount = new DQLSelectBuilder().fromEntity(EnrExamPassDiscipline.class, "b")
                .where(eq(property(EnrExamPassDiscipline.examGroup().fromAlias("b")), value(examGroup)))
                .createCountStatement(new DQLExecutionContext(getSession())).uniqueResult();

            if (examGroup.getSize() < examDiscCount)
                ContextLocal.getErrorCollector().add(EnrExamGroupManager.instance().getProperty("examGroup.save.validate.sizeException"));
        }

        // save
        if (!examGroup.getExamGroupSet().getEnrollmentCampaign().getSettings().isExamGroupTitleFormingManual() && examGroup.getId() == null)
            examGroup.setTitle(formExamGroupTitle(examGroup.getExamGroupSet().getEnrollmentCampaign()));

        getSession().saveOrUpdate(examGroup);

        getSession().flush();

        examGroup.setClosed(calculateClosed(examGroup));

        return examGroup.getId();
    }

    @Override
    public boolean hasExamPassDiscipline(Long examGroupId)
    {
        lockEnrollmentCampaign(get(EnrExamGroup.class, examGroupId).getExamGroupSet().getEnrollmentCampaign());

        final Long count = new DQLSelectBuilder().fromEntity(EnrExamPassDiscipline.class, "b")
            .where(eq(property(EnrExamPassDiscipline.examGroup().id().fromAlias("b")), value(examGroupId)))
            .createCountStatement(new DQLExecutionContext(getSession())).uniqueResult();

        return count > 0;
    }

    @Override
    public void doExcludeEntrant(Long examPassDisciplineId)
    {
        final EnrExamPassDiscipline examPassDiscipline = getNotNull(EnrExamPassDiscipline.class, examPassDisciplineId);
        EnrExamGroup examGroup = examPassDiscipline.getExamGroup();
        if (null == examGroup)
            return;

        lockEnrollmentCampaign(get(EnrExamGroup.class, examGroup.getId()).getExamGroupSet().getEnrollmentCampaign());

        examPassDiscipline.setExamGroup(null);
        examPassDiscipline.setExamGroupPosition(null);
        examPassDiscipline.setExamGroupSetDate(null);

        update(examPassDiscipline);

        getSession().flush();

        examGroup.setClosed(calculateClosed(examGroup));
        update(examGroup);
    }

    @Override
    public void doSetScheduleEvent(Long examGroupId, Collection<EnrExamScheduleEvent> examScheduleEventList)
    {
        final EnrExamGroup examGroup = getNotNull(EnrExamGroup.class, examGroupId);
        lockEnrollmentCampaign(get(EnrExamGroup.class, examGroup.getId()).getExamGroupSet().getEnrollmentCampaign());

        if (EnrExamGroupManager.instance().groupDao().hasExamPassDiscipline(examGroupId))
            throw new ApplicationException(EnrExamGroupManager.instance().getProperty("setScheduleEventTime.fillException"));

        if (validateSetScheduleEvent(examGroup, examScheduleEventList).hasErrors())
            return;

        final List<EnrExamGroupScheduleEvent> targetExamGroupScheduleEventList = new ArrayList<>();
        for (EnrExamScheduleEvent scheduleEvent : examScheduleEventList)
            targetExamGroupScheduleEventList.add(new EnrExamGroupScheduleEvent(examGroup, scheduleEvent));

        new MergeAction.SessionMergeAction<EnrExamGroupScheduleEvent.NaturalId, EnrExamGroupScheduleEvent>()
        {
            @Override
            protected EnrExamGroupScheduleEventGen.NaturalId key(EnrExamGroupScheduleEvent source)
            {
                return (EnrExamGroupScheduleEventGen.NaturalId) source.getNaturalId();
            }

            @Override
            protected EnrExamGroupScheduleEvent buildRow(EnrExamGroupScheduleEvent source)
            {
                return new EnrExamGroupScheduleEvent(source.getExamGroup(), source.getExamScheduleEvent());
            }

            @Override
            protected void fill(EnrExamGroupScheduleEvent target, EnrExamGroupScheduleEvent source)
            {

            }
        }
            .merge(getList(EnrExamGroupScheduleEvent.class, EnrExamGroupScheduleEvent.examGroup(), examGroup), targetExamGroupScheduleEventList);
    }

    @Override
    public void setExamGroupAndUpdateTerritorial(EnrExamPassDiscipline examPassDiscipline, OrgUnit terrOu, EnrExamGroup examGroup, boolean manual)
    {
        lockEnrollmentCampaign(examPassDiscipline.getEntrant().getEnrollmentCampaign());

        examPassDiscipline = this.getNotNull(examPassDiscipline.getId());

        if (!examPassDiscipline.isCanChangeExamGroup()) {
            throw new ApplicationException(EnrExamGroupManager.instance().getProperty("examGroup.addDisc.cannotChangeExamGroup"));
        }

        if (examGroup != null) {
            List<EnrExamGroupScheduleEvent> newExamGroupEvents = getList(EnrExamGroupScheduleEvent.class, EnrExamGroupScheduleEvent.examGroup().s(), examGroup);

            if (CollectionUtils.isEmpty(newExamGroupEvents) || newExamGroupEvents.size() != examGroup.getDays()) {
                throw new ApplicationException(EnrExamGroupManager.instance().getProperty("examGroup.addDisc.scheduleIsNotSetup"));
            }

            for (EnrExamGroupScheduleEvent event : newExamGroupEvents) {
                DQLSelectBuilder dql = new DQLSelectBuilder()
                    .fromEntity(EnrExamPassDiscipline.class, "e").column("e.id").predicate(DQLPredicateType.distinct)
                    .joinPath(DQLJoinType.inner, EnrExamPassDiscipline.examGroup().fromAlias("e"), "grp")
                    .joinEntity("grp", DQLJoinType.inner, EnrExamGroupScheduleEvent.class, "ev", eq(property(EnrExamGroupScheduleEvent.examGroup().fromAlias("ev")), property("grp")))
                    .where(eq(property(EnrExamGroupScheduleEvent.examScheduleEvent().fromAlias("ev")), value(event.getExamScheduleEvent())))
                    ;
                if (event.getExamScheduleEvent().getExamRoom().getSize() <= getCount(dql)) {
                    throw new ApplicationException(EnrExamGroupManager.instance().getProperty("examGroup.addDisc.roomFull"));
                }
            }

            EnrEnrollmentCampaignSettings settings = examPassDiscipline.getEntrant().getEnrollmentCampaign().getSettings();
            int delta = Math.max(0, manual ? settings.getExamIntervalManual() : settings.getExamIntervalAuto());

            // проверяем, нет ли среди ЭГ остальных ДДС событий в расписании в тот же день и тоже время, что и у некоторого события новой ЭГ
            if (delta > 0) {
                DQLSelectBuilder oldSEDaysBuilder = new DQLSelectBuilder()
                    .fromEntity(EnrExamGroupScheduleEvent.class, "egse")
                    .where(exists(
                        new DQLSelectBuilder()
                            .fromEntity(EnrExamPassDiscipline.class, "pd")
                            .where(eq(property("pd", EnrExamPassDiscipline.entrant()), value(examPassDiscipline.getEntrant())))
                            .where(ne(property("pd"), value(examPassDiscipline)))
                            .where(eq(property("pd", EnrExamPassDiscipline.examGroup()), property("egse", EnrExamGroupScheduleEvent.examGroup())))
                            .buildQuery()
                    ));

                for (EnrExamGroupScheduleEvent newEvent : newExamGroupEvents)
                {
                    Date newEventStartTime = newEvent.getExamScheduleEvent().getScheduleEvent().getDurationBegin();
                    Date newEventEndTime = newEvent.getExamScheduleEvent().getScheduleEvent().getDurationEnd();

                    Date newEventStartTimeWithDelta = new DateTime(newEventStartTime).minusDays(delta).toDate();
                    Date newEventEndTimeWithDelta = new DateTime(newEventEndTime).plusDays(delta).toDate();

                    for (EnrExamGroupScheduleEvent oldEvent : this.<EnrExamGroupScheduleEvent>getList(oldSEDaysBuilder))
                    {
                        Date oldEventStartTime = oldEvent.getExamScheduleEvent().getScheduleEvent().getDurationBegin();
                        Date oldEventEndTime = oldEvent.getExamScheduleEvent().getScheduleEvent().getDurationEnd();

//                            if((newEventStartTime.getTime() >= oldEventStartTime.getTime() && newEventStartTime.getTime() <= oldEventEndTime.getTime())
//                                    ||(newEventEndTime.getTime() >= oldEventStartTime.getTime() && newEventEndTime.getTime() <= oldEventEndTime.getTime())
//                                    ||(newEventStartTime.getTime() < oldEventStartTime.getTime() && newEventEndTime.getTime() > oldEventEndTime.getTime()))
//                                throw new ApplicationException(EnrExamGroupManager.instance().getProperty("examGroup.addDisc.sameDayAndTime"));

                        if((newEventStartTimeWithDelta.getTime() >= oldEventStartTime.getTime() && newEventStartTimeWithDelta.getTime() <= oldEventEndTime.getTime())
                            ||(newEventEndTimeWithDelta.getTime() >= oldEventStartTime.getTime() && newEventEndTimeWithDelta.getTime() <= oldEventEndTime.getTime())
                            ||(newEventStartTimeWithDelta.getTime() < oldEventStartTime.getTime() && newEventEndTimeWithDelta.getTime() > oldEventEndTime.getTime()))
                            throw new ApplicationException(EnrExamGroupManager.instance().getProperty("examGroup.addDisc.eventsIntersection", String.valueOf(delta)));
                    }
                }
            }
        }

        EnrExamGroup oldExamGroup = examPassDiscipline.getExamGroup();

        if (oldExamGroup != null && !oldExamGroup.equals(examGroup)) {
            doExcludeEntrant(examPassDiscipline.getId());
            getSession().flush();
        }

        if (examGroup != null && !examGroup.equals(oldExamGroup)) {
            examGroup = getNotNull(EnrExamGroup.class, examGroup.getId());
            if (examGroup.isClosed()) {
                throw new ApplicationException(EnrExamGroupManager.instance().getProperty("examGroup.addDisc.full"));
            }

            int position = 1;
            while (true) {
                if (!existsEntity(EnrExamPassDiscipline.class,
                    EnrExamPassDiscipline.L_EXAM_GROUP, examGroup,
                    EnrExamPassDiscipline.P_EXAM_GROUP_POSITION, position))
                {
                    examPassDiscipline.setExamGroupPosition(position);
                    examPassDiscipline.setExamGroup(examGroup);
                    examPassDiscipline.setExamGroupSetDate(new Date());
                    break;
                }
                position++;
            }
        }

        examPassDiscipline.setTerritorialOrgUnit(terrOu);
        saveOrUpdate(examPassDiscipline);

        getSession().flush();

        if (examGroup != null) {
            examGroup.setClosed(calculateClosed(examGroup));
            update(examGroup);
        }
    }
}
