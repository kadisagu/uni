package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x2_29to30 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrEnrollmentOrderParagraphPrintFormType

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("enr14_c_enr_par_form_type_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("requesttype_id", DBType.LONG), 
				new DBColumn("competitiontype_id", DBType.LONG), 
				new DBColumn("parallel_p", DBType.BOOLEAN), 
				new DBColumn("title_p", DBType.createVarchar(1200)), 
				new DBColumn("path_p", DBType.createVarchar(255)).setNullable(true),
				new DBColumn("document_p", DBType.BLOB), 
				new DBColumn("editdate_p", DBType.TIMESTAMP), 
				new DBColumn("comment_p", DBType.TEXT)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("enrEnrollmentOrderParagraphPrintFormType");
		}
    }
}