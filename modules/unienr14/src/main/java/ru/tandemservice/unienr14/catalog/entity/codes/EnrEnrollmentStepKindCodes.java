package ru.tandemservice.unienr14.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Вариант работы с шагом зачисления"
 * Имя сущности : enrEnrollmentStepKind
 * Файл data.xml : unienr.catalog.data.xml
 */
public interface EnrEnrollmentStepKindCodes
{
    /** Константа кода (code) элемента : Зачислять до заполнения всех мест (title) */
    String NO_REC = "no_rec";
    /** Константа кода (code) элемента : Зачислять на указанный процент от числа мест (title) */
    String NO_REC_PERCENTAGE = "no_rec_percentage";
    /** Константа кода (code) элемента : С рекомендацией, зачислять только рекомендованных (title) */
    String REC_AND_ADHERE = "rec_and_adhere";
    /** Константа кода (code) элемента : С рекомендацией, зачислять до заполнения мест (title) */
    String REC_AND_IGNORE = "rec_and_ignore";

    Set<String> CODES = ImmutableSet.of(NO_REC, NO_REC_PERCENTAGE, REC_AND_ADHERE, REC_AND_IGNORE);
}
