/* $Id:$ */
package ru.tandemservice.unienr14.settings.bo.EnrSettings.ui.SelectEduOu;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.NewLineFormatter;
import org.tandemframework.core.view.list.column.RadioButtonColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.unienr14.competition.entity.IEnrollmentStudentSettings;
import ru.tandemservice.unienr14.competition.entity.gen.IEnrollmentStudentSettingsGen;
import ru.tandemservice.unienr14.exams.entity.EnrExamSet;
import ru.tandemservice.unienr14.settings.bo.EnrSettings.EnrSettingsManager;

import java.util.List;

/**
 * @author oleyba
 * @since 8/15/14
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "holder.id")
})
public class EnrSettingsSelectEduOuUI extends UIPresenter
{
    private EntityHolder<IEnrollmentStudentSettings> holder = new EntityHolder<>();
    private StaticListDataSource<EducationOrgUnit> eduOuDs;

    @Override
    public void onComponentRefresh()
    {
        List<EducationOrgUnit> eduOuList = EnrSettingsManager.instance().dao().getEduOuList(getHolder().getValue());
        if (eduOuList.isEmpty()) {
            throw new ApplicationException("Нет подходящих для выбора НПП.");
        }

        getHolder().refresh();
        setEduOuDs(new StaticListDataSource<EducationOrgUnit>());
        getEduOuDs().setupRows(eduOuList);
        RadioButtonColumn radioButtonColumn = new RadioButtonColumn();
        getEduOuDs().addColumn(radioButtonColumn);
        getEduOuDs().addColumn(new SimpleColumn("Название", EducationOrgUnit.educationLevelHighSchool().fullTitleExtended().s()).setClickable(false).setOrderable(false));
        getEduOuDs().addColumn(new SimpleColumn("Форм. подр.", EducationOrgUnit.formativeOrgUnit().shortTitle().s()).setClickable(false).setOrderable(false));
        getEduOuDs().addColumn(new SimpleColumn("Терр. подр.", EducationOrgUnit.territorialOrgUnit().shortTitle().s()).setClickable(false).setOrderable(false));
        getEduOuDs().addColumn(new SimpleColumn("Форма освоения", EducationOrgUnit.developForm().title().s()).setClickable(false).setOrderable(false));
        getEduOuDs().addColumn(new SimpleColumn("Условие освоения", EducationOrgUnit.developCondition().title().s()).setClickable(false).setOrderable(false));
        getEduOuDs().addColumn(new SimpleColumn("Технология освоения", EducationOrgUnit.developTech().title().s()).setClickable(false).setOrderable(false));
        getEduOuDs().addColumn(new SimpleColumn("Срок освоения", EducationOrgUnit.developPeriod().title().s()).setClickable(false).setOrderable(false));

        if (getHolder().getValue().getEducationOrgUnit() != null) {
            radioButtonColumn.setSelectedEntity(getHolder().getValue().getEducationOrgUnit());
        }
    }

    public void onClickApply() {
        EducationOrgUnit eduOu = (EducationOrgUnit) ((RadioButtonColumn) getEduOuDs().getColumn(0)).getSelectedEntity();
        EnrSettingsManager.instance().dao().updateSettingsItem(getHolder().getValue(), eduOu);
        deactivate();
    }

    // getters and setters

    public EntityHolder<IEnrollmentStudentSettings> getHolder()
    {
        return holder;
    }

    public StaticListDataSource<EducationOrgUnit> getEduOuDs()
    {
        return eduOuDs;
    }

    public void setEduOuDs(StaticListDataSource<EducationOrgUnit> eduOuDs)
    {
        this.eduOuDs = eduOuDs;
    }
}