/* $Id$ */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.ui.AddEdit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;

/**
 * @author Alexey Lopatin
 * @since 29.05.2015
 */
@Input({@Bind(key = IUIPresenter.PUBLISHER_ID, binding = "enrollmentCommission.id")})
public class EnrEnrollmentCommissionAddEditUI extends UIPresenter
{
    private EnrEnrollmentCommission _enrollmentCommission = new EnrEnrollmentCommission();

    @Override
    public void onComponentRefresh()
    {
        if (!isAddForm())
            _enrollmentCommission = DataAccessServices.dao().getNotNull(_enrollmentCommission.getId());
    }

    public void onClickApply()
    {
        if (isAddForm())
            _enrollmentCommission.setCode(DefaultCatalogAddEditDAO.getNewCatalogItemCode(EnrEnrollmentCommission.class));

        DataAccessServices.dao().saveOrUpdate(_enrollmentCommission);
        deactivate();
    }

    public boolean isAddForm()
    {
        return getEnrollmentCommission().getId() == null;
    }

    public EnrEnrollmentCommission getEnrollmentCommission()
    {
        return _enrollmentCommission;
    }

    public void setEnrollmentCommission(EnrEnrollmentCommission enrollmentCommission)
    {
        _enrollmentCommission = enrollmentCommission;
    }
}
