/* $Id$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantForeignRequest.logic;

import org.tandemframework.shared.commonbase.base.util.INumberQueueDAO;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.request.bo.EnrEntrantForeignRequest.EnrEntrantForeignRequestManager;
import ru.tandemservice.unienr14.request.entity.EnrEntrantForeignRequest;

import java.util.Date;

/**
 * @author nvankov
 * @since 7/15/14
 */
public class EnrEntrantForeignRequestDAO extends UniBaseDao implements IEnrEntrantForeignRequestDAO
{
    @Override
    public Long createOrUpdate(EnrEntrantForeignRequest request)
    {
        saveOrUpdate(request);

        return request.getId();
    }

    @Override
    public boolean entrantTabVisible(Long entrantId)
    {
        return existsEntity(EnrEntrantForeignRequest.class, EnrEntrantForeignRequest.entrant().id().s(), entrantId);
    }


}
