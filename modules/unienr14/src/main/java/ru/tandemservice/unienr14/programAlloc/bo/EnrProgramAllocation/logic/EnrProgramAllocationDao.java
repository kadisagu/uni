/* $Id: EcProfileDistributionDao.java 36315 2014-07-15 08:37:38Z nfedorovskih $ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.logic;

import org.hibernate.Session;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantStateCodes;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItemOrgUnitPlan;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.order.bo.EnrAllocationParagraph.EnrAllocationParagraphManager;
import ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.EnrProgramAllocationManager;
import ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.util.*;
import ru.tandemservice.unienr14.programAlloc.entity.EnrProgramAllocation;
import ru.tandemservice.unienr14.programAlloc.entity.EnrProgramAllocationItem;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.EnrRequestedProgram;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Vasily Zhukov
 * @since 10.05.2012
 */
public class EnrProgramAllocationDao extends CommonDAO implements IEnrProgramAllocationDao
{
    @Override
    public IEnrPAQuotaWrapper getCurrentQuota(Long programSetOrgUnitId)
    {
        EnrProgramSetOrgUnit psOu = getNotNull(EnrProgramSetOrgUnit.class, programSetOrgUnitId);
        List<EnrProgramSetItem> programList = getList(EnrProgramSetItem.class, EnrProgramSetItem.programSet(), psOu.getProgramSet());
        List<EnrProgramSetItemOrgUnitPlan> list = getList(EnrProgramSetItemOrgUnitPlan.class, EnrProgramSetItemOrgUnitPlan.programSetOrgUnit(), psOu);

        Map<Long, Integer> quotaMap = new HashMap<Long, Integer>();

        int sumProfilePlan = 0;
        for (EnrProgramSetItemOrgUnitPlan item : list) {
            int plan = item.getPlan();
            sumProfilePlan += plan;
            quotaMap.put(item.getProgramSetItem().getId(), plan);
        }

        boolean allProfileHasPlan = quotaMap.size() == programList.size();
        int totalQuota = psOu.getMinisterialPlan() + psOu.getContractPlan();

        boolean totalQuotaValid = allProfileHasPlan && sumProfilePlan == totalQuota;

        return new EnrPAQuotaWrapper(totalQuota, totalQuotaValid, quotaMap);
    }

    @Override
    public IEnrPAQuotaUsedWrapper getUsedQuota(Long allocationId)
    {
        List<Object[]> data = new DQLSelectBuilder().fromEntity(EnrProgramAllocationItem.class, "e")
                .column(property(EnrProgramAllocationItem.programSetItem().id().fromAlias("e")))
                .column(DQLFunctions.countStar())
                .where(eq(property(EnrProgramAllocationItem.allocation().id().fromAlias("e")), value(allocationId)))
                .group(property(EnrProgramAllocationItem.programSetItem().id().fromAlias("e")))
                .createStatement(getSession()).list();

        Map<Long, Integer> id2count = new HashMap<Long, Integer>();
        for (Object[] row : data) {
            Long id = (Long) row[0];
            Number number = (Number) row[1];
            id2count.put(id, number == null ? 0 : number.intValue());
        }

        int totalCount = 0;
        Map<Long, Integer> usedMap = new HashMap<Long, Integer>();

        EnrProgramAllocation allocation = getNotNull(EnrProgramAllocation.class, allocationId);
        for (EnrProgramSetItem item : getList(EnrProgramSetItem.class, EnrProgramSetItem.programSet(), allocation.getProgramSetOrgUnit().getProgramSet())) {
            Integer count = id2count.get(item.getId());
            int countInt = count == null ? 0 : count;
            totalCount += countInt;
            usedMap.put(item.getId(), countInt);
        }

        return new EnrPAQuotaUsedWrapper(totalCount, usedMap);
    }

    @Override
    public IEnrPAQuotaFreeWrapper getFreeQuota(Long allocationId)
    {
        EnrProgramAllocation allocation = getNotNull(EnrProgramAllocation.class, allocationId);
        IEnrPAQuotaWrapper quotaDTO = getCurrentQuota(allocation.getProgramSetOrgUnit().getId());
        IEnrPAQuotaUsedWrapper quotaUsedDTO = getUsedQuota(allocationId);

        int totalFree = quotaDTO.getTotalQuota() - quotaUsedDTO.getTotalUsed();
        Map<Long, Integer> freeMap = new HashMap<Long, Integer>();

        for (Map.Entry<Long, Integer> entry : quotaDTO.getQuotaMap().entrySet())
        {
            Long id = entry.getKey();
            Integer quota = entry.getValue();
            if (quota == null)
                freeMap.put(id, null); // план по профилю не ограничен
            else
                freeMap.put(id, quota - quotaUsedDTO.getUsedMap().get(id));
        }

        return new EnrPAQuotaFreeWrapper(quotaDTO, quotaUsedDTO, totalFree, freeMap);
    }

    @Override
    public void addItems(EnrProgramAllocation allocation, Map<EnrRequestedCompetition, EnrProgramSetItem> choiceResult)
    {
        Session session = getSession();

        Set<Long> usedIds = new HashSet<Long>(new DQLSelectBuilder().fromEntity(EnrProgramAllocationItem.class, "e")
                .column(DQLExpressions.property(EnrProgramAllocationItem.entrant().id().fromAlias("e")))
                .where(DQLExpressions.eq(DQLExpressions.property(EnrProgramAllocationItem.allocation().fromAlias("e")), DQLExpressions.value(allocation)))
                .createStatement(session).<Long>list());

        for (Map.Entry<EnrRequestedCompetition, EnrProgramSetItem> entry : choiceResult.entrySet())
        {
            EnrRequestedCompetition entrant = entry.getKey();
            EnrProgramSetItem choice = entry.getValue();

            session.refresh(entrant);

            // абитуриент должен быть зачислен
            String fio = entrant.getRequest().getEntrant().getFio();
            if (entrant.getState() == null || !EnrEntrantStateCodes.ENROLLED.equals(entrant.getState().getCode()))
                throw new ApplicationException("Абитуриент «" + fio + "» должен быть зачислен для участия в распрелении.");

            // можно выбирать только ОП из набора
            if (!entrant.getCompetition().getProgramSetOrgUnit().getProgramSet().equals(choice.getProgramSet()))
                throw new ApplicationException("Выбранная образовательная программа должна относиться к набору ОП конкурса, на который был зачислен абитуриент (" + fio + ").");

            // абитуриент не может быть включен в это же распределение
            if (usedIds.contains(entrant.getId()))
                throw new ApplicationException("Зачисленного абитуриента «" + fio + "» уже добавили в это распределение.");

            EnrProgramAllocationItem item = new EnrProgramAllocationItem();
            item.setAllocation(allocation);
            item.setEntrant(entrant);
            item.setProgramSetItem(choice);
            session.save(item);    
        }
    }

    @Override
    public Map<Long, String> getQuotaHTMLDescription(IEnrPAQuotaFreeWrapper wrapper)
    {
        String style = " style='white-space:nowrap;'";
        String styleRed = " style='white-space:nowrap;color:red;'";

        Map<Long, String> quotaMap = new HashMap<Long, String>();

        for (Map.Entry<Long, Integer> entry : wrapper.getFreeMap().entrySet())
        {
            Long id = entry.getKey();
            Integer free = entry.getValue();
            if (free == null)
                quotaMap.put(id, "");
            else
                quotaMap.put(id, "<div" + (free < 0 ? styleRed : style) + ">" + free + " / " + wrapper.getQuota().getQuotaMap().get(id) + "</div>");
        }
        quotaMap.put(0L, "<div" + (wrapper.getTotalFree() < 0 || !wrapper.getQuota().isTotalQuotaValid() ? styleRed : style) + ">" + wrapper.getTotalFree() + " / " + wrapper.getQuota().getTotalQuota() + "</div>");

        return quotaMap;
    }

    @Override
    public void doFillAllocation(Long programSetOrgUnitId)
    {
        EnrProgramSetOrgUnit programSetOrgUnit = getNotNull(EnrProgramSetOrgUnit.class, programSetOrgUnitId);
        EnrProgramAllocation allocation = get(EnrProgramAllocation.class, EnrProgramAllocation.programSetOrgUnit(), programSetOrgUnit);
        if (allocation == null) {
            return;
        }

        Session session = getSession();

        // свободные места в распределении
        IEnrPAQuotaFreeWrapper freeQuotaDTO = EnrProgramAllocationManager.instance().dao().getFreeQuota(allocation.getId());

        // получаем алгоритм заполнения планов приема
        EnrPAQuotaManager quotaManager = new EnrPAQuotaManager(freeQuotaDTO);

        List<EnrCompetitionType> competitionTypes = getList(EnrCompetitionType.class);
        Collections.sort(competitionTypes, new EntityComparator<EnrCompetitionType>(new EntityOrder(EnrCompetitionType.P_PRIORITY_FOR_PROGRAM_ALLOC)));

        Map<EnrRequestedCompetition, EnrProgramSetItem> choiceResultMap = new HashMap<>();

        for (EnrCompetitionType competitionType : competitionTypes) {
            allocateEntrants(allocation, competitionType, choiceResultMap, quotaManager);
        }

        addItems(allocation, choiceResultMap);
    }

    @Override
    public void doBulkMakeAllocation(Set<Long> programSetOrgUnitIds)
    {
        for (Long id : programSetOrgUnitIds) doCreateAllocation(id);
    }

    @Override
    public void doBulkFillAllocation(Set<Long> programSetOrgUnitIds)
    {
        for (Long id : programSetOrgUnitIds) {
            doFillAllocation(id);
        }
    }

    @Override
    public void doBulkDeleteAllocation(Set<Long> programSetOrgUnitIds)
    {
        for (Long id : programSetOrgUnitIds) {
            deleteAllocation(id);
        }
    }

    @Override
    public EnrProgramAllocation doCreateAllocation(Long programSetOrgUnitId)
    {
        EnrProgramSetOrgUnit psOu = getNotNull(EnrProgramSetOrgUnit.class, programSetOrgUnitId);
        EnrProgramAllocation allocation = getByNaturalId(new EnrProgramAllocation.NaturalId(psOu));

        if (allocation == null)
        {
            // создаем распределение
            allocation = new EnrProgramAllocation();
            allocation.setProgramSetOrgUnit(psOu);
            allocation.setFormingDate(new Date());
            save(allocation);
        }

        return allocation;
    }

    @Override
    public void deleteAllocation(Long programSetOrgUnitId)
    {
        EnrProgramAllocation allocation = get(EnrProgramAllocation.class, EnrProgramAllocation.programSetOrgUnit().id(), programSetOrgUnitId);
        if (null != allocation) {
            delete(allocation);
        }
    }

    @Override
    public void deleteAllocItem(Long allocationItemId)
    {
        delete(allocationItemId);
    }

    @Override
    public List<EnrPAItemWrapper> getChoiceSource(EnrProgramAllocation allocation, EnrCompetitionType competitionType)
    {
        final List<EnrPAItemWrapper> wrappers = new ArrayList<>();

        DQLSelectBuilder entrantDQL = new DQLSelectBuilder()
            .fromEntity(EnrRatingItem.class, "r").column("r")
            .order(property("r", EnrRatingItem.position()))
            .where(eq(property("r", EnrRatingItem.requestedCompetition().state().code()), value(EnrEntrantStateCodes.ENROLLED)))
            .where(eq(property("r", EnrRatingItem.requestedCompetition().competition().type()), value(competitionType)))
            .where(eq(property("r", EnrRatingItem.requestedCompetition().competition().programSetOrgUnit()), value(allocation.getProgramSetOrgUnit())))
            .where(notExists(new DQLSelectBuilder()
                .fromEntity(EnrProgramAllocationItem.class, "i")
                .where(eq(property("i", EnrProgramAllocationItem.entrant()), property("r", EnrRatingItem.requestedCompetition())))
                .buildQuery()))
            ;

        for (EnrRatingItem ratingItem : entrantDQL.createStatement(getSession()).<EnrRatingItem>list()) {
            EnrPAItemWrapper item = new EnrPAItemWrapper(ratingItem);
            wrappers.add(item);
        }

        // для распределения по конкурсным группам каждому выделенному абитуриенту сохраним
        // список возможных для выбора направлений
        final Map<Long, List<EnrProgramSetItem>> choiceMap = SafeMap.get(ArrayList.class);
        DQLSelectBuilder choiceDQL = new DQLSelectBuilder()
            .fromEntity(EnrRequestedProgram.class, "p").column("p")
            .order(property("p", EnrRequestedProgram.priority()))
            .fromEntity(EnrRatingItem.class, "r")
            .where(eq(property("p", EnrRequestedProgram.requestedCompetition()), property("r", EnrRatingItem.requestedCompetition())))
            .where(eq(property("r", EnrRatingItem.requestedCompetition().state().code()), value(EnrEntrantStateCodes.ENROLLED)))
            .where(eq(property("r", EnrRatingItem.requestedCompetition().competition().type()), value(competitionType)))
            .where(eq(property("r", EnrRatingItem.requestedCompetition().competition().programSetOrgUnit()), value(allocation.getProgramSetOrgUnit())))
            .where(notExists(new DQLSelectBuilder()
                .fromEntity(EnrProgramAllocationItem.class, "i")
                .where(eq(property("i", EnrProgramAllocationItem.entrant()), property("r", EnrRatingItem.requestedCompetition())))
                .buildQuery()))
            ;
        for (EnrRequestedProgram p : DataAccessServices.dao().<EnrRequestedProgram>getList(choiceDQL)) {
            choiceMap.get(p.getRequestedCompetition().getId()).add(p.getProgramSetItem());
        }

        for (EnrPAItemWrapper wrapper : wrappers) {
            wrapper.setProgramSetItems(choiceMap.get(wrapper.getEntrant().getId()));
        }

        return wrappers;
    }

    @Override
    public int doBulkCreateAllocationOrders(Set<Long> programSetOrgUnitIds)
    {
        return EnrAllocationParagraphManager.instance().dao().doCreateOrders(programSetOrgUnitIds);
    }

    private void allocateEntrants(EnrProgramAllocation allocation, EnrCompetitionType competitionType, Map<EnrRequestedCompetition, EnrProgramSetItem> choiceResultMap, EnrPAQuotaManager quotaManager)
    {
        // получаем строки рейтинга
        List<EnrPAItemWrapper> entrantList = getChoiceSource(allocation, competitionType);

        // общее число строк
        int len = entrantList.size();

        int i = 0;

        // общее число свободных мест
        int freeTotal = quotaManager.getFreeTotal();

        while (i < len && freeTotal > 0)
        {
            EnrPAItemWrapper item = entrantList.get(i);

            EnrProgramSetItem choiceResult = quotaManager.doChoose(item.getProgramList(), item.getProgram());

            if (choiceResult != null)
            {
                freeTotal--;
                choiceResultMap.put(item.getEntrant(), choiceResult);
            }
            i++;
        }
   }
}
