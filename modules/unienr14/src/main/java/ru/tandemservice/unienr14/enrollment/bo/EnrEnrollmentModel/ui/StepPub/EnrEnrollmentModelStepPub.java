/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.ui.StepPub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.config.tab.ITabPanelExtPointBuilder;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.logic.EnrModelCompetitionDSHandler;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.logic.EnrModelCompetitionWrapper;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.ui.CompetitionInStepPub.EnrEnrollmentModelCompetitionInStepPub;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.ui.StatsTab.EnrEnrollmentModelStatsTab;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.ui.StepCheckTab.EnrEnrollmentModelStepCheckTab;

/**
 * @author oleyba
 * @since 3/27/15
 */
@Configuration
public class EnrEnrollmentModelStepPub extends BusinessComponentManager
{
    public static final String ACTION_BUTTON_LIST = "enrollmentStepActionButtons";
    public static final String COMMON_FILTER_ADDON = CommonFilterAddon.class.getSimpleName();

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
        .addAddon(uiAddon(COMMON_FILTER_ADDON, EnrCompetitionFilterAddon.class))
        .addDataSource(searchListDS("competitionDS", competitionDSColumns(), competitionDSHandler()))
        .create();
    }

    @Bean
    public TabPanelExtPoint enrollmentModelStepPubTabPanelExtPoint()
    {

        return tabPanelExtPointBuilder("enrollmentModelStepPubTabPanel")
            .addTab(htmlTab("mainTab", "MainTab"))
            .addTab(componentTab("checkTab", EnrEnrollmentModelStepCheckTab.class))
            .create();
    }

    @Bean
    public ColumnListExtPoint competitionDSColumns()
    {
        return columnListExtPointBuilder("competitionDS")
            .addColumn(publisherColumn("title", EnrCompetition.title())
                .publisherLinkResolver(new IPublisherLinkResolver() {
                    @Override public Object getParameters(IEntity entity) { return ((EnrModelCompetitionWrapper) entity).getPublisherParameters(); }
                    @Override public String getComponentName(IEntity entity) { return EnrEnrollmentModelCompetitionInStepPub.class.getSimpleName(); }
                }))
            .addColumn(textColumn("plan", EnrModelCompetitionDSHandler.VIEW_PROP_PLAN))
//            .addColumn(actionColumn(EDIT_COLUMN_NAME, new Icon(EDIT_COLUMN_NAME), "onClickPlanEdit")
//                .permissionKey("enr14EnrollmentModelEverything")
//            )
            .create();
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> competitionDSHandler()
    {
        return new EnrModelCompetitionDSHandler(getName());
    }
}