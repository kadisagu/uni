/**
 *$Id: IEnrExamSetDao.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.exams.bo.EnrExamSet.logic;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.entity.IIdentifiableWrapper;
import org.tandemframework.shared.commonbase.base.util.Wiki;

import ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm;
import ru.tandemservice.unienr14.catalog.entity.EnrExamType;
import ru.tandemservice.unienr14.competition.entity.*;
import ru.tandemservice.unienr14.exams.entity.EnrExamSet;
import ru.tandemservice.unienr14.exams.entity.EnrExamSetElement;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.IEnrExamSetElementValue;

/**
 * @author Alexander Shaburov
 * @since 16.04.13
 */
public interface IEnrExamSetDao extends INeedPersistenceSupport
{
    // работа с настройками набора ВИ

    /**
     * Инициализирует новую, незаполненную обертку настроек набора ВИ
     * в соответствии с задачей DEV-2778
     * В частности, заполняет зачетный балл по умолчанию, и выставляет галочки для олимпиады
     * Ничего не сохраняет
     * Используется для работы формы настройки набора ВИ:
     * @see ru.tandemservice.unienr14.exams.bo.EnrExamSet.ui.VariantAddEdit.EnrExamSetVariantAddEdit
     * @param examSet набор ВИ
     * @return подготовленные EnrExamSetDao.IExamSetSettings
     */
    EnrExamSetDao.IExamSetSettings getNewExamSettings(EnrExamSet examSet);

    /**
     * Подготавливает информацию о настройках наборов ВИ
     * @param variantIds набор id настроек {@link EnrExamSetVariant}
     * @return id настройки - обертка настройки набора
     */
    Map<Long, IExamSetSettings> getExamSetContent(Collection<Long> variantIds);

    /**
     * Сохраняет настройки набора ВИ
     * Для загрузки текущей настройки используйте метод {@link IEnrExamSetDao#getExamSetContent}
     * Для получения новой обертки настройки для заполнения используйте метод {@link IEnrExamSetDao#getNewExamSettings}
     *
     * @param examSetSettings настройки набора ВИ - обертка
     * @param variantKind владелец
     * @param variantKind вид набора у владельца
     */
    void saveExamSetSettings(IExamSetSettings examSetSettings, EnrProgramSetBase owner, ExamSetVariantKind variantKind);

    // работа с наборами ВИ

    /**
     * Подготавливает сгруппированные Элементы набора ВИ по их Наборам ВИ.
     * Элементы напора ВИ сортируются по номеру.
     * @param examSets наборы ВИ для которых будут подниматься данные
     * @return examSet -> список examSetElement
     */
    Map<EnrExamSet, List<EnrExamSetElement>> examSet2ElementsMap(EnrExamSet... examSets);

    /**
     * Сохраняет\обновляет Набор вступительных испытаний.
     * Создает\обновляет Элементы набора ВИ.
     * @param examSet набор вступительных испытаний
     * @param elementWrapperList список оберток Элементов набора ВИ.
     * @return id сохраненного\обновленного Набора ВИ
     */
    Long saveOrUpdateExamSet(EnrExamSet examSet, List<? extends IExamSetElementWrapper> elementWrapperList);

    /**
     * Предоставляет пустой набор ВИ в рамках данной ПК. Создает, если сет еще не был создан.
     * @param campaign ПК
     * @return пустой набор ВИ
     */
    EnrExamSet doGetEmptySet(EnrEnrollmentCampaign campaign);

    // интерфейсы для оборачивания данных

    /**
     * Обертка для Элеманта набора ВИ. Для передачи данных Элемента.
     */
    public interface IExamSetElementWrapper
    {
        /** Объектов, включеный в набор ВИ. */
        IEnrExamSetElementValue getElementValue();
        /** Типы вступительных испытаний */
        EnrExamType getExamType();
    }

    /** Обертка для настроек набора ВИ для КГ */
    public interface IExamSetSettings {

        EnrExamSet getExamSet();
        List<IExamSetElementSettings> getElementList();

        /**
         * Настройки заполнены не полностью - нет настройки (EnrCompetitionGroupExam) для какого-то из элементов набора ВИ
         * @return индикация ошибки
         */
        boolean isNotFullySetup();

        /** @return Отображаемое название настроек набора ВИ в КГ */
        @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=15010378")
        String getTitle();
    }

    /** Обертка для настройки отдельного ВИ (EnrCompetitionGroupExam) для КГ */
    public interface IExamSetElementSettings extends IIdentifiableWrapper
    {
        EnrExamSetElement getElement();
        EnrExamVariant getCgExam();

        long getPassMarkAsLong();
        void setPassMarkAsLong(long passMark);

        int getPriority();
        void setPriority(int priority);

        Map<EnrExamPassForm, Boolean> getPassFormMap();
        String getTitle();
    }
}
