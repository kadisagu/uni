package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x8x1_13to14 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrOnlineEntrantEduDocument

		// создано свойство mark5
        if(!tool.columnExists("enronlineentrantedudocument_t", "mark5_p"))
		{
			// создать колонку
			tool.createColumn("enronlineentrantedudocument_t", new DBColumn("mark5_p", DBType.INTEGER));

		}

		// создано свойство mark4
        if(!tool.columnExists("enronlineentrantedudocument_t", "mark4_p"))
		{
			// создать колонку
			tool.createColumn("enronlineentrantedudocument_t", new DBColumn("mark4_p", DBType.INTEGER));

		}

		// создано свойство mark3
        if(!tool.columnExists("enronlineentrantedudocument_t", "mark3_p"))
		{
			// создать колонку
			tool.createColumn("enronlineentrantedudocument_t", new DBColumn("mark3_p", DBType.INTEGER));

		}

		// создано свойство registrationNumber
        if(!tool.columnExists("enronlineentrantedudocument_t", "registrationnumber_p"))
		{
			// создать колонку
			tool.createColumn("enronlineentrantedudocument_t", new DBColumn("registrationnumber_p", DBType.createVarchar(255)));

		}


    }
}