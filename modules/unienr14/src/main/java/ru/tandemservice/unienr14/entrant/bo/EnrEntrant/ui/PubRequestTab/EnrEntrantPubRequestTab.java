/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubRequestTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.ActionsAddon.EnrEntrantRequestActionsAddon;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.DirectionListAddon.EnrEntrantRequestDirectionListAddon;

/**
 * @author oleyba
 * @since 4/24/13
 */
@Configuration
public class EnrEntrantPubRequestTab extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
            .addAddon(uiAddon(EnrEntrantRequestActionsAddon.NAME, EnrEntrantRequestActionsAddon.class))
            .addAddon(uiAddon(EnrEntrantRequestDirectionListAddon.NAME, EnrEntrantRequestDirectionListAddon.class))
            .create();
    }
}