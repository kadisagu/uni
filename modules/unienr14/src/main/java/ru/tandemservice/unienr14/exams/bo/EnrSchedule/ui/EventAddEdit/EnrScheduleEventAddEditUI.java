/* $Id:$ */
package ru.tandemservice.unienr14.exams.bo.EnrSchedule.ui.EventAddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.*;
import ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm;
import ru.tandemservice.unienr14.competition.entity.EnrExamVariant;
import ru.tandemservice.unienr14.competition.entity.EnrExamVariantPassForm;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.exams.bo.EnrExamGroup.EnrExamGroupManager;
import ru.tandemservice.unienr14.exams.bo.EnrSchedule.EnrScheduleManager;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroup;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroupScheduleEvent;
import ru.tandemservice.unienr14.exams.entity.EnrExamScheduleEvent;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.*;
import ru.tandemservice.unischedule.base.entity.event.ScheduleEvent;
import ru.tandemservice.unischedule.base.entity.event.ScheduleEventPlace;

import java.text.DateFormatSymbols;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 5/16/13
 */
@Input({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "entityHolder.id"),
        @Bind(key = "terrOrgUnitId", binding = "terrOrgUnitId"),
        @Bind(key = "examRoomId", binding = "examRoomId"),
        @Bind(key = "subAdd", binding = "subAdd")
})
public class EnrScheduleEventAddEditUI extends UIPresenter
{
    private EntityHolder<EnrExamScheduleEvent> _entityHolder = new EntityHolder<>(new EnrExamScheduleEvent());

    private Long _terrOrgUnitId;
    private Long _examRoomId;
    private boolean  _subAdd;

    private EnrEnrollmentCampaign _enrCampaign;
    private OrgUnit _territorialOrgUnit;
    private Date _eventDate;
    private String _eventPeriodFrom;
    private String _eventPeriodTo;
    private DataWrapper _discipline;
    private EnrExamGroupSet _examGroupSet;
    private List<EnrExamGroup> _examGroupList;

    private ISingleSelectModel _disciplineModel;
    private Map<Long, DataWrapper> _disciplineModelWrapperMap;

    @Override
    public void onComponentRefresh()
    {
        // добавление события в рамках даты ИЛИ редактирование события
        if (_entityHolder.getId() != null)
        {
            _entityHolder.refresh();
            _enrCampaign = _entityHolder.getValue().getExamRoom().getEnrollmentCampaign();
            _territorialOrgUnit = _entityHolder.getValue().getExamRoom().getResponsibleOrgUnit();
            prepareDisciplineSelectModel();
            _eventDate = eventDate();

            // добавление в рамках даты
            if (isSubAdd())
            {
                EnrCampaignExamRoom examRoom = _entityHolder.getValue().getExamRoom();
                _entityHolder = new EntityHolder<>(new EnrExamScheduleEvent());
                _entityHolder.refresh();
                if (_examRoomId != null) {
                    _entityHolder.getValue().setExamRoom(DataAccessServices.dao().getNotNull(EnrCampaignExamRoom.class, _examRoomId));
                } else {
                    _entityHolder.getValue().setExamRoom(examRoom);
                }
                if (EnrExamGroupManager.instance().setDao().hasOpened(_enrCampaign.getId()))
                {
                    _examGroupSet = (EnrExamGroupSet) DataAccessServices.dao().getList(
                            new DQLSelectBuilder().fromEntity(EnrExamGroupSet.class, "b").column(property("b"))
                                    .where(eq(property(EnrExamGroupSet.open().fromAlias("b")), value(true)))
                                    .where(eq(property(EnrExamGroupSet.enrollmentCampaign().fromAlias("b")), value(_enrCampaign))))
                            .get(0);
                }
                else
                {
                    final List<EnrExamGroupSet> examGroupSetList = DataAccessServices.dao().getList(EnrExamGroupSet.class, EnrExamGroupSet.enrollmentCampaign(), _enrCampaign, EnrExamGroupSet.id().s());
                    _examGroupSet = examGroupSetList.isEmpty() ? null : examGroupSetList.get(examGroupSetList.size() - 1);
                }
            }
            // редактирование
            else
            {
                _examGroupList = DataAccessServices.dao().getList(
                        new DQLSelectBuilder().fromEntity(EnrExamGroupScheduleEvent.class, "b").column(property(EnrExamGroupScheduleEvent.examGroup().fromAlias("b")))
                                .where(eq(property(EnrExamGroupScheduleEvent.examScheduleEvent().fromAlias("b")), value(_entityHolder.getValue()))));
                _examGroupSet = _examGroupList.isEmpty() ? null : _examGroupList.get(0).getExamGroupSet();
                if (_examGroupSet == null)
                {
                    if (EnrExamGroupManager.instance().setDao().hasOpened(_enrCampaign.getId()))
                    {
                        _examGroupSet = (EnrExamGroupSet) DataAccessServices.dao().getList(
                                new DQLSelectBuilder().fromEntity(EnrExamGroupSet.class, "b").column(property("b"))
                                        .where(eq(property(EnrExamGroupSet.open().fromAlias("b")), value(true)))
                                        .where(eq(property(EnrExamGroupSet.enrollmentCampaign().fromAlias("b")), value(_enrCampaign))))
                                .get(0);
                    }
                    else
                    {
                        final List<EnrExamGroupSet> examGroupSetList = DataAccessServices.dao().getList(EnrExamGroupSet.class, EnrExamGroupSet.enrollmentCampaign(), _enrCampaign, EnrExamGroupSet.id().s());
                        _examGroupSet = examGroupSetList.isEmpty() ? null : examGroupSetList.get(examGroupSetList.size() - 1);
                    }
                }
                _discipline = disciplineWrapper();
                _eventPeriodFrom = eventPeriodFrom();
                _eventPeriodTo = eventPeriodTo();
            }

        }
        // добавление события
        else if (_entityHolder.getId() == null && !isSubAdd())
        {
            _entityHolder.refresh();
            _enrCampaign = EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign();
            if (_terrOrgUnitId != null)
                _territorialOrgUnit = DataAccessServices.dao().getNotNull(OrgUnit.class, _terrOrgUnitId);
            prepareDisciplineSelectModel();
            if (EnrExamGroupManager.instance().setDao().hasOpened(_enrCampaign.getId()))
            {
                _examGroupSet = (EnrExamGroupSet) DataAccessServices.dao().getList(
                        new DQLSelectBuilder().fromEntity(EnrExamGroupSet.class, "b").column(property("b"))
                                .where(eq(property(EnrExamGroupSet.open().fromAlias("b")), value(true)))
                                .where(eq(property(EnrExamGroupSet.enrollmentCampaign().fromAlias("b")), value(_enrCampaign))))
                        .get(0);
            }
            else
            {
                final List<EnrExamGroupSet> examGroupSetList = DataAccessServices.dao().getList(EnrExamGroupSet.class, EnrExamGroupSet.enrollmentCampaign(), _enrCampaign, EnrExamGroupSet.id().s());
                _examGroupSet = examGroupSetList.isEmpty() ? null : examGroupSetList.get(examGroupSetList.size() - 1);
            }
            if (_examRoomId != null)
                _entityHolder.getValue().setExamRoom(DataAccessServices.dao().getNotNull(EnrCampaignExamRoom.class, _examRoomId));

        }
        else
        {
            throw new IllegalStateException();
        }
    }

    /**
     * Сначала подготавливаем мапу всех значений модели селекта, для этого нужны ПК и Терр. подр.
     * Далее при обращении к селекту значения будут подниматься из неё, а не из базы.
     */
    public void prepareDisciplineSelectModel()
    {
        {
            _disciplineModelWrapperMap = new HashMap<>();

            final DQLSelectBuilder examSetVariantOwnerDQL = new DQLSelectBuilder()
                .fromEntity(EnrProgramSetOrgUnit.class, "psou")
                .column(property(EnrProgramSetOrgUnit.programSet().id().fromAlias("psou")))
                .where(eq(property(EnrProgramSetOrgUnit.programSet().enrollmentCampaign().fromAlias("psou")), value(_enrCampaign)))
                .where(eq(property(EnrProgramSetOrgUnit.orgUnit().institutionOrgUnit().orgUnit().fromAlias("psou")), value(_territorialOrgUnit)));

            final DQLSelectBuilder discResultDQL = new DQLSelectBuilder()
                .fromEntity(EnrExamVariant.class, "ge")
                .joinPath(DQLJoinType.inner, EnrExamVariant.examSetElement().value().fromAlias("ge"), "cdValue")
                .joinEntity("ge", DQLJoinType.inner, EnrCampaignDiscipline.class, "cd", eq(property(EnrCampaignDiscipline.id().fromAlias("cd")), property("cdValue.id")))
                .joinEntity("ge", DQLJoinType.inner, EnrExamVariantPassForm.class, "ef", eq(property(EnrExamVariantPassForm.examVariant().fromAlias("ef")), property("ge")))
                .where(in(property(EnrExamVariant.examSetVariant().owner().id().fromAlias("ge")), examSetVariantOwnerDQL.buildQuery()))
                .where(eq(property(EnrExamVariantPassForm.passForm().internal().fromAlias("ef")), value(true)));

            final DQLSelectBuilder discGroupResultDQL = new DQLSelectBuilder()
                .fromEntity(EnrExamVariant.class, "ge")
                .joinPath(DQLJoinType.inner, EnrExamVariant.examSetElement().value().fromAlias("ge"), "cdValue")
                .joinEntity("ge", DQLJoinType.inner, EnrCampaignDisciplineGroup.class, "dg", eq(property(EnrCampaignDisciplineGroup.id().fromAlias("dg")), property("cdValue.id")))
                .joinEntity("dg", DQLJoinType.inner, EnrCampaignDisciplineGroupElement.class, "dge", eq(property(EnrCampaignDisciplineGroupElement.group().fromAlias("dge")), property("dg")))
                .joinEntity("ge", DQLJoinType.inner, EnrExamVariantPassForm.class, "ef", eq(property(EnrExamVariantPassForm.examVariant().fromAlias("ef")), property("ge")))
                .where(in(property(EnrExamVariant.examSetVariant().owner().id().fromAlias("ge")), examSetVariantOwnerDQL.buildQuery()))
                .where(eq(property(EnrExamVariantPassForm.passForm().internal().fromAlias("ef")), value(true)));

            discResultDQL
                    .column(property("cd"))
                    .column(property(EnrExamVariantPassForm.passForm().fromAlias("ef")));

            discGroupResultDQL
                    .column(property(EnrCampaignDisciplineGroupElement.discipline().fromAlias("dge")))
                    .column(property(EnrExamVariantPassForm.passForm().fromAlias("ef")));

            final List<Object[]> resultDiscList = DataAccessServices.dao().getList(discResultDQL);
            final List<Object[]> resultDiscGroupList = DataAccessServices.dao().getList(discGroupResultDQL);

            long id = 1L;
            final Set<CoreCollectionUtils.Pair<Long, Long>> uniqPair = new LinkedHashSet<>();
            for (Object[] objects : resultDiscList)
            {
                final EnrCampaignDiscipline campDisc = (EnrCampaignDiscipline) objects[0];
                final EnrExamPassForm passForm = (EnrExamPassForm) objects[1];

                if (uniqPair.add(new CoreCollectionUtils.Pair<>(campDisc.getId(), passForm.getId())))
                {
                    final DataWrapper wrapper = new DataWrapper(id, campDisc.getTitle() + " (" + passForm.getTitle() + ")");
                    wrapper.setProperty("campDisc", campDisc);
                    wrapper.setProperty("passForm", passForm);
                    _disciplineModelWrapperMap.put(id++, wrapper);
                }
            }

            for (Object[] objects : resultDiscGroupList)
            {
                final EnrCampaignDiscipline campDisc = (EnrCampaignDiscipline) objects[0];
                final EnrExamPassForm passForm = (EnrExamPassForm) objects[1];

                if (uniqPair.add(new CoreCollectionUtils.Pair<>(campDisc.getId(), passForm.getId())))
                {
                    final DataWrapper wrapper = new DataWrapper(id, campDisc.getTitle() + " (" + passForm.getTitle() + ")");
                    wrapper.setProperty("campDisc", campDisc);
                    wrapper.setProperty("passForm", passForm);
                    _disciplineModelWrapperMap.put(id++, wrapper);
                }
            }
        }

        _disciplineModel = new CommonSingleSelectModel()
        {
            @Override
            @SuppressWarnings({"unchecked", "ConstantConditions"})
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                if (o != null)
                {
                    final Long key = (Long) o;
                    final DataWrapper wrapper = _disciplineModelWrapperMap.get(key);

                    return new SimpleListResultBuilder<>(Collections.singleton(wrapper));
                }

                final Collection<DataWrapper> values = new ArrayList<>(_disciplineModelWrapperMap.values());

                if (StringUtils.isNotEmpty(filter))
                {
                    for (Iterator<DataWrapper> iterator = values.iterator();iterator.hasNext();)
                    {
                        if (!StringUtils.containsIgnoreCase(iterator.next().getTitle(), filter))
                            iterator.remove();
                    }
                }

                return new SimpleListResultBuilder(values)
                {
                    @Override
                    public ListResult findOptions()
                    {
                        final List objects = super.findOptions().getObjects();
                        List list = objects.size() >= 50 ? objects.subList(0, 50) : objects;
                        int count = list==null ? 0 : list.size();
                        if( count==50)
                            count = objects.size();

                        Collections.sort(list, new Comparator<DataWrapper>()
                        {
                            @Override
                            public int compare(DataWrapper o1, DataWrapper o2)
                            {
                                return o1.getTitle().compareTo(o2.getTitle());
                            }
                        });

                        return new ListResult(list, count);
                    }
                };
            }
        };
    }

    public void onClickApply()
    {
        DataAccessServices.dao().doInTransaction(session -> {
            EnrExamScheduleEvent examEvent;
            if (_entityHolder.getId() == null)
                examEvent = createdExamEvent();
            else
                examEvent = updatedExamEvent();
            EnrScheduleManager.instance().dao().saveOrUpdateExamEvent(examEvent);

            EnrScheduleManager.instance().dao().doAttachExamGroupEvent(examEvent, _examGroupList);

            return null;
        });

        deactivate();
    }

    public void onChangeExamRoom()
    {
        if (_entityHolder.getValue().getExamRoom() != null)
            _entityHolder.getValue().setSize(_entityHolder.getValue().getExamRoom().getSize());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, _enrCampaign);
        dataSource.put(EnrScheduleEventAddEdit.BIND_TERR_ORG_UNIT, _territorialOrgUnit);
        dataSource.put(EnrScheduleEventAddEdit.BIND_DISCIPLINE, _discipline != null ? _discipline.getProperty("campDisc") : null);
        dataSource.put(EnrScheduleEventAddEdit.BIND_PASS_FORM, _discipline != null ? _discipline.getProperty("passForm") : null);
        dataSource.put(EnrScheduleEventAddEdit.BIND_EXAM_GROUP_SET, _examGroupSet);
    }

    // Getters & Setters

    public boolean isEditForm()
    {
        return _entityHolder.getId() != null;
    }

    public String getSticker()
    {
        if (_entityHolder.getId() == null)
            return getConfig().getProperty("ui.sticker.add");
        else
            return getConfig().getProperty("ui.sticker.edit");
    }

    /**
     * Из модели берем подходящий элемент, если его там нет, то добавляем
     */
    public DataWrapper disciplineWrapper()
    {
        for (DataWrapper wrapper : _disciplineModelWrapperMap.values())
        {
            if (wrapper.get("campDisc").equals(_entityHolder.getValue().getDiscipline())
                    &&
                    wrapper.get("passForm").equals(_entityHolder.getValue().getPassForm()))
                return wrapper;
        }

        final DataWrapper value = new DataWrapper(-1L, _entityHolder.getValue().getDiscipline().getTitle() + " (" + _entityHolder.getValue().getPassForm().getTitle() + ")");
        value.setProperty("campDisc", _entityHolder.getValue().getDiscipline());
        value.setProperty("passForm", _entityHolder.getValue().getPassForm());
        _disciplineModelWrapperMap.put(-1L, value);

        return value;
    }

    public Date eventDate()
    {
        return _entityHolder.getValue().getScheduleEvent().getDurationBegin();
    }

    public String eventPeriodFrom()
    {
        Calendar date = new GregorianCalendar();
        date.setTime(_entityHolder.getValue().getScheduleEvent().getDurationBegin());

        return (date.get(Calendar.HOUR_OF_DAY) < 10 ? "0" : "") + date.get(Calendar.HOUR_OF_DAY) + ":" + (date.get(Calendar.MINUTE) < 10 ? "0" : "") + date.get(Calendar.MINUTE);
    }

    public String eventPeriodTo()
    {
        Calendar date = new GregorianCalendar();
        date.setTime(_entityHolder.getValue().getScheduleEvent().getDurationEnd());

        return (date.get(Calendar.HOUR_OF_DAY) < 10 ? "0" : "") + date.get(Calendar.HOUR_OF_DAY) + ":" + (date.get(Calendar.MINUTE) < 10 ? "0" : "") + date.get(Calendar.MINUTE);
    }

    /**
     * Подготавливает дату с указаным временем.
     * @param eventDate дата
     * @param eventTime время, в формате "HH:mm"
     * @return дата
     */
    public Date dateWithTime(Date eventDate, String eventTime)
    {
        final String source = DateFormatter.DEFAULT_DATE_FORMATTER.format(eventDate) + " " + eventTime;

        final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm", new DateFormatSymbols(new Locale("ru")));
        dateFormat.setLenient(false);

        ParsePosition pos = new ParsePosition(0);
        Date date = (Date) dateFormat.parseObject(source, pos);
        if (pos.getIndex() == 0 || pos.getIndex() < source.length())
        {
            ContextLocal.getErrorCollector().add(getConfig().getProperty("validate.timeInvalidException"), "eventPeriodFrom", "eventPeriodTo");
            throw new ApplicationException();
        }

        return date;
    }

    /**
     * Создает по данным с формы: Событие, Помещение, в котором проводится событие и Событие в расписании ВИ.
     */
    public EnrExamScheduleEvent createdExamEvent()
    {
        final ScheduleEvent event = new ScheduleEvent();
        event.setDurationBegin(dateWithTime(_eventDate, _eventPeriodFrom));
        event.setDurationEnd(dateWithTime(_eventDate, _eventPeriodTo));

        final ScheduleEventPlace eventPlace = new ScheduleEventPlace();
        eventPlace.setPlace(_entityHolder.getValue().getExamRoom().getPlace());
        eventPlace.setScheduleEvent(event);

        final EnrExamScheduleEvent examEvent = _entityHolder.getValue();
        examEvent.setScheduleEvent(event);
        examEvent.setDiscipline((EnrCampaignDiscipline) _discipline.getProperty("campDisc"));
        examEvent.setPassForm((EnrExamPassForm) _discipline.getProperty("passForm"));
        examEvent.setScheduleEventPlace(eventPlace);

        return examEvent;
    }

    /**
     * Обновляет по данным с формы: Событие, Помещение, в котором проводится событие и Событие в расписании ВИ.
     */
    public EnrExamScheduleEvent updatedExamEvent()
    {
        final EnrExamScheduleEvent examEvent = _entityHolder.getValue();

        examEvent.getScheduleEvent().setDurationBegin(dateWithTime(_eventDate, _eventPeriodFrom));
        examEvent.getScheduleEvent().setDurationEnd(dateWithTime(_eventDate, _eventPeriodTo));

        examEvent.getScheduleEventPlace().setPlace(_entityHolder.getValue().getExamRoom().getPlace());

        examEvent.setDiscipline((EnrCampaignDiscipline) _discipline.getProperty("campDisc"));
        examEvent.setPassForm((EnrExamPassForm) _discipline.getProperty("passForm"));

        return examEvent;
    }

    public boolean isEventDateDisable()
    {
        return isEditForm() || isSubAdd();
    }

    // Accessors

    public EntityHolder<EnrExamScheduleEvent> getEntityHolder()
    {
        return _entityHolder;
    }

    public void setEntityHolder(EntityHolder<EnrExamScheduleEvent> entityHolder)
    {
        _entityHolder = entityHolder;
    }

    public EnrEnrollmentCampaign getEnrCampaign()
    {
        return _enrCampaign;
    }

    public void setEnrCampaign(EnrEnrollmentCampaign enrCampaign)
    {
        _enrCampaign = enrCampaign;
    }

    public OrgUnit getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    public void setTerritorialOrgUnit(OrgUnit territorialOrgUnit)
    {
        _territorialOrgUnit = territorialOrgUnit;
    }

    public Date getEventDate()
    {
        return _eventDate;
    }

    public void setEventDate(Date eventDate)
    {
        _eventDate = eventDate;
    }

    public String getEventPeriodFrom()
    {
        return _eventPeriodFrom;
    }

    public void setEventPeriodFrom(String eventPeriodFrom)
    {
        _eventPeriodFrom = eventPeriodFrom;
    }

    public String getEventPeriodTo()
    {
        return _eventPeriodTo;
    }

    public void setEventPeriodTo(String eventPeriodTo)
    {
        _eventPeriodTo = eventPeriodTo;
    }

    public DataWrapper getDiscipline()
    {
        return _discipline;
    }

    public void setDiscipline(DataWrapper discipline)
    {
        _discipline = discipline;
    }

    public EnrExamGroupSet getExamGroupSet()
    {
        return _examGroupSet;
    }

    public void setExamGroupSet(EnrExamGroupSet examGroupSet)
    {
        _examGroupSet = examGroupSet;
    }

    public List<EnrExamGroup> getExamGroupList()
    {
        return _examGroupList;
    }

    public void setExamGroupList(List<EnrExamGroup> examGroupList)
    {
        _examGroupList = examGroupList;
    }

    public Long getTerrOrgUnitId()
    {
        return _terrOrgUnitId;
    }

    public void setTerrOrgUnitId(Long terrOrgUnitId)
    {
        _terrOrgUnitId = terrOrgUnitId;
    }

    public Long getExamRoomId()
    {
        return _examRoomId;
    }

    public void setExamRoomId(Long examRoomId)
    {
        _examRoomId = examRoomId;
    }

    public ISingleSelectModel getDisciplineModel()
    {
        return _disciplineModel;
    }

    public void setDisciplineModel(ISingleSelectModel disciplineModel)
    {
        _disciplineModel = disciplineModel;
    }

    public boolean isSubAdd()
    {
        return _subAdd;
    }

    public void setSubAdd(boolean subAdd)
    {
        _subAdd = subAdd;
    }
}
