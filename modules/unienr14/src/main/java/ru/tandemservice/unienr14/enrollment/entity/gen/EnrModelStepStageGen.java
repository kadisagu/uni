package ru.tandemservice.unienr14.enrollment.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.enrollment.entity.EnrModelStep;
import ru.tandemservice.unienr14.enrollment.entity.EnrModelStepStage;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignEnrollmentStage;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Стадия зачисления, включенная в шаг в модели
 *
 * Фиксирует стадии зачисления, включенные в данный шаг зачисления.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrModelStepStageGen extends EntityBase
 implements INaturalIdentifiable<EnrModelStepStageGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.enrollment.entity.EnrModelStepStage";
    public static final String ENTITY_NAME = "enrModelStepStage";
    public static final int VERSION_HASH = 633900173;
    private static IEntityMeta ENTITY_META;

    public static final String L_STEP = "step";
    public static final String L_ENROLLMENT_STAGE = "enrollmentStage";

    private EnrModelStep _step;     // Шаг
    private EnrCampaignEnrollmentStage _enrollmentStage;     // Стадия зачисления

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Шаг. Свойство не может быть null.
     */
    @NotNull
    public EnrModelStep getStep()
    {
        return _step;
    }

    /**
     * @param step Шаг. Свойство не может быть null.
     */
    public void setStep(EnrModelStep step)
    {
        dirty(_step, step);
        _step = step;
    }

    /**
     * @return Стадия зачисления. Свойство не может быть null.
     */
    @NotNull
    public EnrCampaignEnrollmentStage getEnrollmentStage()
    {
        return _enrollmentStage;
    }

    /**
     * @param enrollmentStage Стадия зачисления. Свойство не может быть null.
     */
    public void setEnrollmentStage(EnrCampaignEnrollmentStage enrollmentStage)
    {
        dirty(_enrollmentStage, enrollmentStage);
        _enrollmentStage = enrollmentStage;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrModelStepStageGen)
        {
            if (withNaturalIdProperties)
            {
                setStep(((EnrModelStepStage)another).getStep());
                setEnrollmentStage(((EnrModelStepStage)another).getEnrollmentStage());
            }
        }
    }

    public INaturalId<EnrModelStepStageGen> getNaturalId()
    {
        return new NaturalId(getStep(), getEnrollmentStage());
    }

    public static class NaturalId extends NaturalIdBase<EnrModelStepStageGen>
    {
        private static final String PROXY_NAME = "EnrModelStepStageNaturalProxy";

        private Long _step;
        private Long _enrollmentStage;

        public NaturalId()
        {}

        public NaturalId(EnrModelStep step, EnrCampaignEnrollmentStage enrollmentStage)
        {
            _step = ((IEntity) step).getId();
            _enrollmentStage = ((IEntity) enrollmentStage).getId();
        }

        public Long getStep()
        {
            return _step;
        }

        public void setStep(Long step)
        {
            _step = step;
        }

        public Long getEnrollmentStage()
        {
            return _enrollmentStage;
        }

        public void setEnrollmentStage(Long enrollmentStage)
        {
            _enrollmentStage = enrollmentStage;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrModelStepStageGen.NaturalId) ) return false;

            EnrModelStepStageGen.NaturalId that = (NaturalId) o;

            if( !equals(getStep(), that.getStep()) ) return false;
            if( !equals(getEnrollmentStage(), that.getEnrollmentStage()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getStep());
            result = hashCode(result, getEnrollmentStage());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getStep());
            sb.append("/");
            sb.append(getEnrollmentStage());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrModelStepStageGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrModelStepStage.class;
        }

        public T newInstance()
        {
            return (T) new EnrModelStepStage();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "step":
                    return obj.getStep();
                case "enrollmentStage":
                    return obj.getEnrollmentStage();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "step":
                    obj.setStep((EnrModelStep) value);
                    return;
                case "enrollmentStage":
                    obj.setEnrollmentStage((EnrCampaignEnrollmentStage) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "step":
                        return true;
                case "enrollmentStage":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "step":
                    return true;
                case "enrollmentStage":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "step":
                    return EnrModelStep.class;
                case "enrollmentStage":
                    return EnrCampaignEnrollmentStage.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrModelStepStage> _dslPath = new Path<EnrModelStepStage>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrModelStepStage");
    }
            

    /**
     * @return Шаг. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelStepStage#getStep()
     */
    public static EnrModelStep.Path<EnrModelStep> step()
    {
        return _dslPath.step();
    }

    /**
     * @return Стадия зачисления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelStepStage#getEnrollmentStage()
     */
    public static EnrCampaignEnrollmentStage.Path<EnrCampaignEnrollmentStage> enrollmentStage()
    {
        return _dslPath.enrollmentStage();
    }

    public static class Path<E extends EnrModelStepStage> extends EntityPath<E>
    {
        private EnrModelStep.Path<EnrModelStep> _step;
        private EnrCampaignEnrollmentStage.Path<EnrCampaignEnrollmentStage> _enrollmentStage;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Шаг. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelStepStage#getStep()
     */
        public EnrModelStep.Path<EnrModelStep> step()
        {
            if(_step == null )
                _step = new EnrModelStep.Path<EnrModelStep>(L_STEP, this);
            return _step;
        }

    /**
     * @return Стадия зачисления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrModelStepStage#getEnrollmentStage()
     */
        public EnrCampaignEnrollmentStage.Path<EnrCampaignEnrollmentStage> enrollmentStage()
        {
            if(_enrollmentStage == null )
                _enrollmentStage = new EnrCampaignEnrollmentStage.Path<EnrCampaignEnrollmentStage>(L_ENROLLMENT_STAGE, this);
            return _enrollmentStage;
        }

        public Class getEntityClass()
        {
            return EnrModelStepStage.class;
        }

        public String getEntityName()
        {
            return "enrModelStepStage";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
