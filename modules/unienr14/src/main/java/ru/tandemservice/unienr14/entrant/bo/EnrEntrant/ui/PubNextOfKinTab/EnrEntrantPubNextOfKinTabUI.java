/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubNextOfKinTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.person.base.bo.Person.ui.NextOfKinAddEdit.PersonNextOfKinAddEdit;
import org.tandemframework.shared.person.base.bo.Person.util.ISecureRoleContext;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonNextOfKin;

import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.legacy.EnrPersonLegacyUtils;

/**
 * @author oleyba
 * @since 4/22/13
 */
@Input({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "entrant.id", required=true),
    @Bind(key = EnrEntrantPubNextOfKinTabUI.PARAM_INLINE, binding = "inline")
})
public class EnrEntrantPubNextOfKinTabUI extends UIPresenter implements EnrEntrantNextOfKinDataSourceUtil.Owner
{
    public static final String PARAM_INLINE = "inline";

    private EnrEntrant entrant = new EnrEntrant();
    private ISecureRoleContext secureRoleContext;

    private boolean inline;
    public boolean isInline() { return this.inline; }
    public void setInline(boolean inline) { this.inline = inline; }

    private DynamicListDataSource<PersonNextOfKin> dataSource;

    @Override
    public void onComponentRefresh()
    {
        setEntrant(IUniBaseDao.instance.get().getNotNull(EnrEntrant.class, getEntrant().getId()));
        setSecureRoleContext(EnrPersonLegacyUtils.getSecureRoleContext(getEntrant()));

        prepareNextOfKinDataSource();
    }

    public void onClickAddNextOfKin()
    {
        _uiActivation.asRegionDialog(PersonNextOfKinAddEdit.class)
        .parameter("personId", getPerson().getId())
        .parameter("personRoleName", getSecureRoleContext().getPersonRoleName())
        .activate();
    }

    public void onClickEditNextOfKin()
    {
        _uiActivation.asRegionDialog(PersonNextOfKinAddEdit.class)
        .parameter("personId", null)
        .parameter("nextOfKinId", getListenerParameterAsLong())
        .parameter("personRoleName", getSecureRoleContext().getPersonRoleName())
        .activate();
    }

    public void onClickDeleteNextOfKin()
    {
        PersonNextOfKin item = DataAccessServices.dao().getNotNull(getListenerParameterAsLong());
        DataAccessServices.dao().delete(item);
        getDataSource().refresh();
    }

    // presenter

    public Person getPerson() {
        return getEntrant().getPerson();
    }

    // utils

    private void prepareNextOfKinDataSource()
    {
        DynamicListDataSource<PersonNextOfKin> dataSource = EnrEntrantNextOfKinDataSourceUtil.createDataSource(this, true);

        if (getSecureRoleContext().isAccessible())
        {
            dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditNextOfKin").setPermissionKey("editPersonNextOfKin_enrEntrant"));
            dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteNextOfKin", "Удалить ближайшего родственника: {0} — «{1} {2} {3}»?", PersonNextOfKin.L_RELATION_DEGREE + "." + ICatalogItem.CATALOG_ITEM_TITLE, PersonNextOfKin.P_LAST_NAME, PersonNextOfKin.P_FIRST_NAME, PersonNextOfKin.P_MIDDLE_NAME).setPermissionKey("deletePersonNextOfKin_enrEntrant"));
        }

        setDataSource(dataSource);
    }


    // getters and setters

    public EnrEntrant getEntrant()
    {
        return entrant;
    }

    public void setEntrant(EnrEntrant entrant)
    {
        this.entrant = entrant;
    }

    public ISecureRoleContext getSecureRoleContext()
    {
        return secureRoleContext;
    }

    public void setSecureRoleContext(ISecureRoleContext secureRoleContext)
    {
        this.secureRoleContext = secureRoleContext;
    }

    public DynamicListDataSource<PersonNextOfKin> getDataSource()
    {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<PersonNextOfKin> dataSource)
    {
        this.dataSource = dataSource;
    }
}

