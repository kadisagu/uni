/* $Id:$ */
package ru.tandemservice.unienr14.exams.bo.EnrStateExamResult.logic;

import com.google.common.collect.Lists;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Colour;
import jxl.write.*;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Transformer;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.hibernate.Session;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.common.IIdentifiable;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLStatement;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.catalog.entity.codes.IdentityCardTypeCodes;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrStateExamSubject;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrRequestTypeCodes;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult;
import ru.tandemservice.unienr14.exams.bo.EnrStateExamResult.EnrStateExamResultManager;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentParagraph;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 4/22/13
 */
public class EnrStateExamResultDao extends UniBaseDao implements IEnrStateExamResultDao
{
    @Override
    public void doChangeResultChecked(Long id)
    {
        EnrEntrantStateExamResult certificate = getNotNull(EnrEntrantStateExamResult.class, id);
        certificate.setAccepted(!certificate.isAccepted());
        update(certificate);
    }

    @Override
    public void doChangeResultVerifiedByUser(Long id)
    {
        EnrEntrantStateExamResult certificate = getNotNull(EnrEntrantStateExamResult.class, id);
        certificate.setVerifiedByUser(!certificate.isVerifiedByUser());
        update(certificate);
    }


    @Override
    public List<EnrCompetition> getCoveredCompetitions(EnrEntrant entrant)
    {
        return Collections.emptyList();
//        Session session = getSession();
//
//        // перечень дисциплин ПК, покрывающийся свидетельствами абитуриента
//        List<Long> disciplineIds = new DQLSelectBuilder()
//        .predicate(DQLPredicateType.distinct)
//        .fromEntity(EnrCampaignDiscipline.class, "dsc").column(property("dsc.id"))
//        .where(eq(property(EnrCampaignDiscipline.enrollmentCampaign().fromAlias("dsc")), value(entrant.getEnrollmentCampaign())))
//
//        .fromEntity(EnrStateExamSubjectMark.class, "mk")
//        .where(eq(property(EnrStateExamSubjectMark.certificate().entrant().fromAlias("mk")), value(entrant)))
//
//        .where(eq(property(EnrCampaignDiscipline.stateExamSubject().fromAlias("dsc")), property(EnrStateExamSubjectMark.subject().fromAlias("mk"))))
//        .createStatement(session).<Long>list();
//
//
//        // перечень групп дисциплинт, покрывающихся свидетельствами (по включению дисциплины)
//        List<Long> groupIds = new DQLSelectBuilder()
//        .predicate(DQLPredicateType.distinct)
//        .fromEntity(EnrCampaignDisciplineGroupElement.class, "e")
//        .column(property(EnrCampaignDisciplineGroupElement.group().id().fromAlias("e")))
//        .where(eq(property(EnrCampaignDisciplineGroupElement.group().enrollmentCampaign().fromAlias("e")), value(entrant.getEnrollmentCampaign())))
//        .where(in(property(EnrCampaignDisciplineGroupElement.discipline().id().fromAlias("e")), disciplineIds))
//        .createStatement(session).<Long>list();
//
//
//        // полный перечень покрытых ВИ
//        List<Long> examSetValueIds = new ArrayList<>();
//        examSetValueIds.addAll(disciplineIds);
//        examSetValueIds.addAll(groupIds);
//
//        // перечень КГ, не подходящих нам в силу ряда причин
//        Set<Long> ignoredEcgIds = new HashSet<>();
//
//        // 1. в КГ есть ВИ-КГ без ВИ-КГ-Ф по форме ЕГЭ
//        EnrExamPassForm passFormStateExam = getCatalogItem(EnrExamPassForm.class, EnrExamPassFormCodes.STATE_EXAM);
//        ignoredEcgIds.addAll(
//            new DQLSelectBuilder()
//            .fromEntity(EnrCompetitionGroupExam.class, "e")
//            .column(property(EnrCompetitionGroupExam.competitionGroup().fromAlias("e")))
//            .where(eq(property(EnrCompetitionGroupExam.competitionGroup().enrollmentCampaign().fromAlias("e")), value(entrant.getEnrollmentCampaign())))
//            .where(notExists(
//                new DQLSelectBuilder()
//                .fromEntity(EnrCompetitionGroupExamPassForm.class, "epf")
//                .column(property("epf.id"))
//                .where(eq(property(EnrCompetitionGroupExamPassForm.competitionGroupExam().fromAlias("epf")), property("e")))
//                .where(eq(property(EnrCompetitionGroupExamPassForm.passForm().fromAlias("epf")), value(passFormStateExam)))
//                .buildQuery()
//            ))
//            .createStatement(session).<Long>list()
//        );
//
//        // 2. в КГ есть ВИ-КГ, lbcwbgkbys ВИ которых не входит в перечень disciplineIds
//        ignoredEcgIds.addAll(
//            new DQLSelectBuilder()
//            .fromEntity(EnrCompetitionGroupExam.class, "e")
//            .column(property(EnrCompetitionGroupExam.competitionGroup().fromAlias("e")))
//            .where(eq(property(EnrCompetitionGroupExam.competitionGroup().enrollmentCampaign().fromAlias("e")), value(entrant.getEnrollmentCampaign())))
//            .where(notIn(EnrCompetitionGroupExam.examSetElement().value().id().fromAlias("e"), examSetValueIds))
//            .createStatement(session).<Long>list()
//        );
//
//        // выводим все КГ из ПК абитуриента, которые не засветились в ignoredEcgIds
//        return new DQLSelectBuilder()
//        .fromEntity(EnrCompetitionGroup.class, "g").column(property("g"))
//        .where(eq(property(EnrCompetitionGroup.enrollmentCampaign().fromAlias("g")), value(entrant.getEnrollmentCampaign())))
//        .where(notIn(property("g.id"), ignoredEcgIds))
//        .createStatement(session).<EnrCompetitionGroup>list();
    }

    @Override
    public byte[] getEntrantStateExamResultExportContent(EnrEnrollmentCampaign enrCampaign, Date requestRegDateFrom, Date requestRegDateTo, boolean includeForeignCitizens, DataWrapper icType, List<EnrOrder> enrOrders) throws UnsupportedEncodingException
    {
        DQLSelectBuilder enrEntrantRequestDql = new DQLSelectBuilder()
            .fromEntity(EnrEntrantRequest.class, "r")
            .where(eq(property("r", EnrEntrantRequest.entrant().person().id()), property("ic", IdentityCard.person().id())))
            .where(eq(property("r", EnrEntrantRequest.type().code()), value(EnrRequestTypeCodes.BS)))
            .where(eq(property("r", EnrEntrantRequest.entrant().enrollmentCampaign()), value(enrCampaign)))
            .where(betweenDays(EnrEntrantRequest.regDate().fromAlias("r"), requestRegDateFrom, requestRegDateTo));

        //если выбраны приказы о зачислении
        if (org.apache.commons.collections.CollectionUtils.isNotEmpty(enrOrders)) {
            DQLSelectBuilder inDql = new DQLSelectBuilder().fromEntity(EnrEnrollmentExtract.class, "ext")
                    .where(exists(
                            new DQLSelectBuilder().fromEntity(EnrEnrollmentParagraph.class, "paragraph")
                                    .where(eq(EnrEnrollmentExtract.paragraph().id().fromAlias("ext"), EnrEnrollmentParagraph.id().fromAlias("paragraph")))
                                    .where(exists(
                                            EnrOrder.class,
                                            EnrOrder.id().s(), property(EnrEnrollmentParagraph.order().id().fromAlias("paragraph")),
                                            EnrOrder.id().s(), CommonDAO.ids(enrOrders)
                                    ))
                                    .buildQuery()
                    )).column(property(EnrEnrollmentExtract.id().fromAlias("ext")));

            enrEntrantRequestDql.where(exists(
                    new DQLSelectBuilder()
                            .fromEntity(EnrRequestedCompetition.class, "c")
                            .where(eq(property("r", EnrEntrantRequest.id()), property("c", EnrRequestedCompetition.request().id())))
                            .where(exists(
                                    new DQLSelectBuilder().fromEntity(EnrEnrollmentExtract.class, "extract")
                                            .where(eq(property(EnrEnrollmentExtract.entity().id().fromAlias("extract")), property(EnrRequestedCompetition.id().fromAlias("c"))))
                                            .where(in(EnrEnrollmentExtract.id().fromAlias("extract"), inDql.buildQuery()))
                                            .buildQuery()
                            ))
                            .buildQuery()
            ));
        }

        DQLSelectBuilder identityCardBuilder = new DQLSelectBuilder()
                .fromEntity(IdentityCard.class, "ic")
                .where(exists(enrEntrantRequestDql.buildQuery()))
                .where(includeForeignCitizens ? null : eq(property("ic", IdentityCard.citizenship().code()), value(IKladrDefines.RUSSIA_COUNTRY_CODE)))
                .order(property("ic", IdentityCard.lastName()))
                .order(property("ic", IdentityCard.firstName()))
                .order(property("ic", IdentityCard.middleName()))
                .order(property("ic", IdentityCard.seria()))
                .order(property("ic", IdentityCard.number()));

        if(icType != null)
        {
            if(EnrStateExamResultManager.IC_PASSPORT_RF.equals(icType.getId()))
            {
                identityCardBuilder.where(eq(property("ic", IdentityCard.cardType().code()), value(IdentityCardTypeCodes.PASPORT_GRAJDANINA_ROSSIYSKOY_FEDERATSII)));
            }
            else if(EnrStateExamResultManager.IC_OTHER.equals(icType.getId()))
            {
                identityCardBuilder.where(ne(property("ic", IdentityCard.cardType().code()), value(IdentityCardTypeCodes.PASPORT_GRAJDANINA_ROSSIYSKOY_FEDERATSII)));
            }
            else
            {
                throw new UnsupportedOperationException();
            }
        }

        List<IdentityCard> identityCardList = this.getList(identityCardBuilder);

        ErrorCollector errorCollector = ContextLocal.getErrorCollector();
        List<String> result = new ArrayList<>();
        for (IdentityCard identityCard: identityCardList)
        {
            if (identityCard.getNumber() == null)
                errorCollector.add("У абитуриента " + identityCard.getFio() + " не указан номер удостоверения личности.");
            else
                result.add(new StringBuilder()
                    .append(identityCard.getLastName()).append("%")
                    .append(identityCard.getFirstName()).append("%")
                    .append(StringUtils.trimToEmpty(identityCard.getMiddleName())).append("%")
                    .append(StringUtils.trimToEmpty(identityCard.getSeria())).append("%")
                    .append(StringUtils.trimToEmpty(identityCard.getNumber()))
                    .toString());
        }

        return errorCollector.hasErrors() ? null : StringUtils.join(result, '\n').getBytes("CP1251");
    }

    @SuppressWarnings("unchecked")
    @Override
    public byte[] updateStateExamResultsAndGetImportResultsContent(EnrEnrollmentCampaign enrCampaign, InputStream inputStream) throws Exception
    {
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        WritableFont arial_10 = new WritableFont(WritableFont.ARIAL, 10);
        WritableFont err_arial_10 = new WritableFont(WritableFont.ARIAL, 10);
        {
            err_arial_10.setColour(Colour.RED);
        }

        WritableFont warn_arial_10 = new WritableFont(WritableFont.ARIAL, 10);
        {
            warn_arial_10.setColour(Colour.DARK_YELLOW);
        }

        WritableCellFormat common_format = new WritableCellFormat(arial_10);
        WritableCellFormat err_format = new WritableCellFormat(err_arial_10);
        WritableCellFormat warn_format = new WritableCellFormat(warn_arial_10);

        WorkbookSettings ws = new WorkbookSettings();
        {
            ws.setMergedCellChecking(false);
            ws.setRationalization(false);
            ws.setGCDisabled(true);
            ws.setEncoding("CP1251");
        }

        WritableWorkbook workbook = Workbook.createWorkbook(out, ws);
        WritableSheet sheet = workbook.createSheet("name", 0);
        {
            sheet.setColumnView(0, 50);
        }

        Session session = getSession();

        Map<String, EnrStateExamSubject> subjectMap = new HashMap<>();
        {
            for (EnrStateExamSubject subject: this.getList(EnrStateExamSubject.class))
                subjectMap.put(subject.getSubjectFISTitle(), subject);
        }

        IDQLStatement stateExamResultsStatement = new DQLSelectBuilder()
            .fromEntity(EnrEntrantStateExamResult.class, "r")
            .where(in(property("r", EnrEntrantStateExamResult.entrant()), parameter("entrants", PropertyType.LONG)))
            .where(eq(property("r", EnrEntrantStateExamResult.subject()), parameter("subject", PropertyType.LONG)))
            .createStatement(session);

        Map<EnrEntrant, List<EnrEntrantStateExamResult>> processedResults = SafeMap.get(ArrayList.class);

        MutableInt rowHolder = new MutableInt(0);
        List<StateExamResultImportRecordWrapper> records = Lists.newArrayList();

        for (String line: IOUtils.readLines(inputStream, "CP1251"))
        {
            StateExamResultImportRecordWrapper record = StateExamResultImportRecordWrapper.buildRecordWrapper(line);

            if (!record.isInvalidFormat())
            {
                record.setEntrants(new DQLSelectBuilder()
                        .fromEntity(EnrEntrant.class, "e")
                        .where(eq(property("e", EnrEntrant.enrollmentCampaign()), value(enrCampaign)))
                        .where(exists(new DQLSelectBuilder().fromEntity(IdentityCard.class, "ic")
                                .where(eq(DQLFunctions.replace(DQLFunctions.upper(property("ic", IdentityCard.lastName())), value("Ё"), value("Е")), value(StringUtils.isEmpty(record.getLastName()) ? null: record.getLastName().toUpperCase().replaceAll("Ё", "Е"))))
                                .where(eq(DQLFunctions.replace(DQLFunctions.upper(property("ic", IdentityCard.firstName())), value("Ё"), value("Е")), value(StringUtils.isEmpty(record.getFirstName()) ? null : record.getFirstName().toUpperCase().replaceAll("Ё", "Е"))))
                                .where(record.getMiddleName() == null ? null : eq(DQLFunctions.replace(DQLFunctions.upper(property("ic", IdentityCard.middleName())), value("Ё"), value("Е")), value(StringUtils.isEmpty(record.getMiddleName()) ? null : record.getMiddleName().toUpperCase().replaceAll("Ё", "Е"))))
                                .where(eq(property("ic", IdentityCard.number()), value(record.getNumber())))
                                .where(eq(property("ic", IdentityCard.person().id()), property("e", EnrEntrant.person().id())))
                                .buildQuery()))
                        .createStatement(this.getSession()).<EnrEntrant>list());
            }
            else
            {
                record.setEntrants(Collections.<EnrEntrant>emptyList());
            }

            if(!record.getEntrants().isEmpty())
            {
                record.setPerson(record.getEntrants().get(0).getPerson());
            }
            records.add(record);
        }

        Collections.sort(records, (o1, o2) -> {
            if(!o1.isInvalidFormat() && !o2.isInvalidFormat())
            {
                if(o1.getPerson() != null && o2.getPerson() != null)
                {
                    if(o1.getPerson().equals(o2.getPerson()))
                    {
                        if(o1.getStatus().isValid() && !o2.getStatus().isValid())
                            return 1;
                        else if(!o1.getStatus().isValid() && o2.getStatus().isValid())
                            return -1;
                        else if(!o1.getStatus().isValid())  // && !o2.getStatus().isValid()
                        {
                            return 0;
                        }
                        else
                        {
                            if(o1.getMark() == null && o2.getMark() != null)
                            {
                                return -1;
                            }
                            else if(o1.getMark() != null && o2.getMark() == null)
                            {
                                return 1;
                            }
                            else if(o1.getMark() != null) //  && o2.getMark() != null
                            {
                                return o1.getMark().compareTo(o2.getMark());
                            }
                            else
                            {
                                return 0;
                            }
                        }
                    }
                    else
                        return o1.getPerson().getFullFio().compareTo(o2.getPerson().getFullFio());
                }
                else if(o1.getPerson() != null) // && o2.getPerson() == null
                    return 1;
                else if(o2.getPerson() != null) // && o1.getPerson() == null
                    return -1;
                else
                    return 0;
            }
            else if(o1.isInvalidFormat() && !o2.isInvalidFormat())
                return -1;
            else if(!o1.isInvalidFormat()) // && o2.isInvalidFormat()
                return 1;
            else
                return 0;
        });

        for(final StateExamResultImportRecordWrapper record : records)
        {
            if (record.isInvalidFormat())
            {
                int row = rowHolder.intValue();
                sheet.addCell(new Label(0, row, "НЕВЕРНЫЙ ФОРМАТ", err_format));
                sheet.addCell(new Label(1, row, record.getSource(), err_format));

                rowHolder.increment();

            } else
            {
                final EnrStateExamSubject subject = subjectMap.get(record.getSubject());

                List<EnrEntrant> entrants = record.getEntrants();

                if (entrants.isEmpty())
                {
                    addRow(sheet, rowHolder, "АБИТУРИЕНТА НЕТ В СИСТЕМЕ", record, err_format);

                } else
                {
                    if (subject == null)
                    {
                        if(record.isNotFound())
                        {

                            for(EnrEntrant entrant : record.getEntrants())
                            {
                                processedResults.get(entrant);
                            }
                            continue;
                        }
                        else
                        {

                            addRow(sheet, rowHolder, "НЕИЗВЕСТНЫЙ ПРЕДМЕТ ЕГЭ", record, common_format);
                            continue;
                        }
                    }

                    Transformer<IEntity, Long> idTransformer = IIdentifiable::getId;
                    Collection<Long> entrantIds = CollectionUtils.collect(entrants, idTransformer);
                    Map<EnrEntrant, List<EnrEntrantStateExamResult>> entrantStateExamResultsMap = SafeMap.get(ArrayList.class);
                    for (EnrEntrantStateExamResult stateExamResult: stateExamResultsStatement.setParameter("entrants", entrantIds).setParameter("subject", subject).<EnrEntrantStateExamResult>list())
                        entrantStateExamResultsMap.get(stateExamResult.getEntrant()).add(stateExamResult);

                    for (EnrEntrant entrant: entrants)
                    {
                        Collection<EnrEntrantStateExamResult> sameYearResults = CollectionUtils.select(entrantStateExamResultsMap.get(entrant), result -> result.getYear() == record.getYear());

                        if (sameYearResults.isEmpty())
                        {

                            if (record.getStatus().isValid())
                            {
                                processedResults.get(entrant).add(createEntrantStateExamResult(entrant, record.getYear(), subject, record.getMark(), record.getCertNumber(), "Проверен. " + record.getStatus(), true, true, true));

                            } else
                            {
                                processedResults.get(entrant).add(createEntrantStateExamResult(entrant, record.getYear(), subject, record.getMark(), record.getCertNumber(), "Проверен. " + record.getStatus(), false, true, false));
                                addRow(sheet, rowHolder, "РЕЗУЛЬТАТ НЕ ЗАЧТЕН", record, warn_format);
                            }

                        } else
                        {
                            EnrEntrantStateExamResult result = sameYearResults.iterator().next();
                            if (result.isVerifiedByUser())
                            {
                                String mismatchStatusPostfix = "";
                                if (result.getMark() == null || !result.getMark().equals(record.getMark())) mismatchStatusPostfix += ". Не совпал балл";
                                if (result.getDocumentNumber() == null ? record.getCertNumber() != null : !result.getDocumentNumber().equals(record.getCertNumber())) mismatchStatusPostfix += ". Не совпал номер";
                                result.setAbovePassingMark(record.getStatus().isValid());
                                result.setStateCheckStatus("Проверен. " + record.getStatus() + mismatchStatusPostfix);
                                result.setFoundInFis(true);

                                session.update(result);
                                addRow(sheet, rowHolder, "РЕЗУЛЬТАТ ПРОВЕРЕН ВРУЧНУЮ" + mismatchStatusPostfix.toUpperCase(), record, record.getStatus().isValid() && mismatchStatusPostfix.isEmpty() ? warn_format : err_format);
                                processedResults.get(entrant).add(result);

                            } else
                            {
                                if (record.getStatus().isValid())
                                {
                                    if (result.getMark() == null || !result.getMark().equals(record.getMark()))
                                        addRow(sheet, rowHolder, "РЕЗУЛЬТАТ ЗАЧТЕН. ИЗМЕНЕН БАЛЛ", record, warn_format);
                                    result.setMark(record.getMark());
                                    result.setDocumentNumber(record.getCertNumber());
                                    result.setStateCheckStatus("Проверен. " + record.getStatus());
                                    result.setAccepted(true);
                                    result.setFoundInFis(true);
                                    result.setAbovePassingMark(true);

                                    session.update(result);

                                    processedResults.get(entrant).add(result);

                                } else
                                {
                                    result.setMark(record.getMark());
                                    result.setDocumentNumber(record.getCertNumber());
                                    result.setStateCheckStatus("Проверен. " + record.getStatus());
                                    result.setAccepted(false);
                                    result.setFoundInFis(true);
                                    result.setAbovePassingMark(false);

                                    session.update(result);

                                    addRow(sheet, rowHolder, "СНЯТ ПРИЗНАК «ЗАЧТЕН»", record, err_format);
                                    processedResults.get(entrant).add(result);
                                }
                            }
                        }
                    }
                }
            }
        }

        for(Map.Entry<EnrEntrant, List<EnrEntrantStateExamResult>> entry : processedResults.entrySet())
        {
            EnrEntrant entrant = entry.getKey();
            List<EnrEntrantStateExamResult> entrantProcessedResults = entry.getValue();
            DQLSelectBuilder resultBuilder = new DQLSelectBuilder()
                    .fromEntity(EnrEntrantStateExamResult.class, "r")
                    .column(property("r"))
                    .fetchPath(DQLJoinType.inner, EnrEntrantStateExamResult.entrant().person().identityCard().fromAlias("r"), "ic")
                    .fetchPath(DQLJoinType.inner, EnrEntrantStateExamResult.subject().fromAlias("r"), "s");
            resultBuilder.where(eq(property("r", EnrEntrantStateExamResult.entrant()), value(entrant)));
            if(!entrantProcessedResults.isEmpty())
                resultBuilder.where(notIn(property("r", EnrEntrantStateExamResult.id()), CommonBaseEntityUtil.getIdList(entrantProcessedResults)));

            for (EnrEntrantStateExamResult result: this.<EnrEntrantStateExamResult>getList(resultBuilder))
            {
                result.setStateCheckStatus("Проверен. Не найдено");
                result.setFoundInFis(false);
                result.setAbovePassingMark(false);
                if(!result.isVerifiedByUser())
                {
                    result.setAccepted(false);
                }
                session.update(result);

                int row = rowHolder.intValue();
                sheet.addCell(new Label(0, row, result.isVerifiedByUser() ? "РЕЗУЛЬТАТ ПРОВЕРЕН ВРУЧНУЮ, НО ЕГО НЕТ В ФИС" : "НЕ НАЙДЕН", err_format));
                sheet.addCell(new Label(1, row, result.getEntrant().getPerson().getIdentityCard().getLastName(), err_format));
                sheet.addCell(new Label(2, row, result.getEntrant().getPerson().getIdentityCard().getFirstName(), err_format));
                sheet.addCell(new Label(3, row, result.getEntrant().getPerson().getIdentityCard().getMiddleName(), err_format));
                sheet.addCell(new Label(4, row, result.getEntrant().getPerson().getIdentityCard().getSeria(), err_format));
                sheet.addCell(new Label(5, row, result.getEntrant().getPerson().getIdentityCard().getNumber(), err_format));
                sheet.addCell(new Label(6, row, result.getSubject().getSubjectFISTitle(), err_format));
                sheet.addCell(new Label(7, row, result.getMark() == null ? "" : String.valueOf(result.getMark()), err_format));
                sheet.addCell(new Label(8, row, String.valueOf(result.getYear()), err_format));
                sheet.addCell(new Label(9, row, "", err_format));
                sheet.addCell(new Label(10, row, "Не найдено", err_format));
                sheet.addCell(new Label(11, row, "", err_format));
                sheet.addCell(new Label(12, row, result.getDocumentNumber(), err_format));
                sheet.addCell(new Label(13, row, "", err_format));
                rowHolder.increment();
            }
        }

//
//
//
//        new DQLUpdateBuilder(EnrEntrantStateExamResult.class)
//            .set(EnrEntrantStateExamResult.stateCheckStatus().s(), value("Не найден"))
//            .where(eq(property(EnrEntrantStateExamResult.entrant().enrollmentCampaign()), value(enrCampaign)))
//            .where(eq(property(EnrEntrantStateExamResult.foundInFis()), value(Boolean.FALSE)))
//            .createStatement(this.getSession()).execute();
//
//        if (uncheckMissingResultsAccepted)
//            new DQLUpdateBuilder(EnrEntrantStateExamResult.class)
//                .set(EnrEntrantStateExamResult.accepted().s(), value(Boolean.FALSE))
//                .set(EnrEntrantStateExamResult.stateCheckStatus().s(), value("Не найден. Снят признак «Зачтен»"))
//                .where(eq(property(EnrEntrantStateExamResult.entrant().enrollmentCampaign()), value(enrCampaign)))
//                .where(eq(property(EnrEntrantStateExamResult.foundInFis()), value(Boolean.FALSE)))
//                .where(eq(property(EnrEntrantStateExamResult.verifiedByUser()), value(Boolean.FALSE)))
//                .createStatement(this.getSession()).execute();

        workbook.write();
        workbook.close();

        return out.toByteArray();
    }

    private EnrEntrantStateExamResult createEntrantStateExamResult(EnrEntrant entrant, int year, EnrStateExamSubject subject, int mark, String certNumber, String fisStatus, boolean accepted, boolean foundInFis, boolean abovePassingMark)
    {
        EnrEntrantStateExamResult result = new EnrEntrantStateExamResult();
        result.setEntrant(entrant);
        result.setYear(year);
        result.setSubject(subject);
        result.setMark(mark);
        result.setDocumentNumber(certNumber);
        result.setStateCheckStatus(fisStatus);
        result.setAccepted(accepted);
        result.setFoundInFis(foundInFis);
        result.setAbovePassingMark(abovePassingMark);

        this.save(result);
        return result;
    }

    private void addRow(WritableSheet sheet, MutableInt rowHolder, String importStatus, StateExamResultImportRecordWrapper record, WritableCellFormat format) throws WriteException
    {
        int row = rowHolder.intValue();
        sheet.addCell(new Label(0, row, importStatus, format));
        sheet.addCell(new Label(1, row, record.getLastName(), format));
        sheet.addCell(new Label(2, row, record.getFirstName(), format));
        sheet.addCell(new Label(3, row, record.getMiddleName(), format));
        sheet.addCell(new Label(4, row, record.getSeria(), format));
        sheet.addCell(new Label(5, row, record.getNumber(), format));
        sheet.addCell(new Label(6, row, record.getSubject(), format));
        sheet.addCell(new Label(7, row, String.valueOf(record.getMark()), format));
        sheet.addCell(new Label(8, row, String.valueOf(record.getYear()), format));
        sheet.addCell(new Label(9, row, record.getRegion(), format));
        sheet.addCell(new Label(10, row, record.getStatus().toString(), format));
        sheet.addCell(new Label(11, row, record.getAppeal(), format));
        sheet.addCell(new Label(12, row, record.getCertNumber(), format));
        sheet.addCell(new Label(13, row, record.getTypoNumber(), format));
        sheet.addCell(new Label(14, row, record.getStatusAppeal(), format));

        rowHolder.increment();
    }

    private static class StateExamResultImportRecordWrapper
    {
        private final String _source;
        private final Map<Integer, Object> _valueMap;
        private List<EnrEntrant> _entrants;
        private Person _person;

        private StateExamResultImportRecordWrapper(String source)
        {
            this(source, null);
        }

        private StateExamResultImportRecordWrapper(String source, Map<Integer, Object> valueMap)
        {
            _source = source;
            _valueMap = valueMap;
        }

        public List<EnrEntrant> getEntrants()
        {
            return _entrants;
        }

        public void setEntrants(List<EnrEntrant> entrants)
        {
            _entrants = entrants;
        }

        public Person getPerson()
        {
            return _person;
        }

        public void setPerson(Person person)
        {
            _person = person;
        }

        public String getSource(){ return _source; }
        public boolean isInvalidFormat(){ return _valueMap == null || _valueMap.get(id_status) == null; }
        public boolean isNotFound() {
            return StateExamResultFisStatus.NOT_FOUND.equals(getStatus());
        }

        public String getLastName(){ return (String) _valueMap.get(id_lastName); }
        public String getFirstName(){ return (String) _valueMap.get(id_firstName); }
        public String getMiddleName(){ return (String) _valueMap.get(id_middleName); }
        public String getSeria(){ return (String) _valueMap.get(id_seria); }
        public String getNumber(){ return (String) _valueMap.get(id_number); }
        public String getSubject(){ return (String) _valueMap.get(id_subject); }
        public Integer getMark(){ return (Integer) _valueMap.get(id_mark); }
        public Integer getYear(){ return (Integer) _valueMap.get(id_year); }
        public String getRegion(){ return (String) _valueMap.get(id_region); }
        public StateExamResultFisStatus getStatus(){ return (StateExamResultFisStatus) _valueMap.get(id_status); }
        public String getAppeal(){ return (String) _valueMap.get(id_appeal); }
        public String getCertNumber(){ return (String) _valueMap.get(id_certNumber); }
        public String getTypoNumber(){ return (String) _valueMap.get(id_typoNumber); }
        public String getStatusAppeal(){ return (String) _valueMap.get(id_statusAppeal); }


        private static final String DELIMITER = "%";
        private static final int id_lastName = 0;
        private static final int id_firstName = 1;
        private static final int id_middleName = 2;
        private static final int id_seria = 3;
        private static final int id_number = 4;
        private static final int id_subject = 5;
        private static final int id_mark = 6;
        private static final int id_year = 7;
        private static final int id_region = 8;
        private static final int id_status = 9;
        private static final int id_appeal = 10;
        private static final int id_certNumber = 11;
        private static final int id_typoNumber = 12;
        private static final int id_statusAppeal = 13;

        private static final List<Integer> ids = Arrays.asList(id_lastName, id_firstName, id_middleName, id_seria, id_number, id_subject, id_mark, id_year, id_region, id_status, id_appeal, id_certNumber, id_typoNumber, id_statusAppeal);
        static
        {
            assert Collections.min(ids).equals(0) && Collections.max(ids).equals(ids.size() - 1) && ids.size() == new HashSet<>(ids).size();
        }

        private static final Set<Integer> required_properties_ids = new HashSet<>(Arrays.asList(id_lastName, id_firstName, id_number, id_status));

        public static StateExamResultImportRecordWrapper buildRecordWrapper(String source)
        {
            if (source == null || StringUtils.countMatches(source, DELIMITER) != ids.size() - 1)
                return new StateExamResultImportRecordWrapper(source);

            Map<Integer, Object> valueMap = new HashMap<>(ids.size());
            String[] values = StringUtils.splitByWholeSeparatorPreserveAllTokens(source, DELIMITER);
            for (Integer id: ids)
                valueMap.put(id, StringUtils.trimToNull(values[id]));

            try
            {
                String markAsString = (String) valueMap.get(id_mark);
                if (markAsString != null)
                {
                    valueMap.put(id_mark, Integer.valueOf(markAsString));
                }

                String yearAsString = (String) valueMap.get(id_year);
                if (yearAsString != null)
                    valueMap.put(id_year, Integer.valueOf(yearAsString));

                String statusAsString = (String) valueMap.get(id_status);
                if (statusAsString != null)
                    valueMap.put(id_status, StateExamResultFisStatus.getStatusByTitle(statusAsString));

            } catch (IllegalArgumentException e)
            {
                return new StateExamResultImportRecordWrapper(source);
            }

            for (Integer id: required_properties_ids)
                if (valueMap.get(id) == null)
                    return new StateExamResultImportRecordWrapper(source);

            return new StateExamResultImportRecordWrapper(source, valueMap);
        }

        public boolean isAbovePassingMark()
        {
            return !StateExamResultFisStatus.BELOW_MINIMUM.equals(getStatus());
        }
    }

    private enum StateExamResultFisStatus
    {
        VALID                       ("Действующий"),
        BELOW_MINIMUM               ("Ниже минимума"),
        ORDER_VIOLATION_GIA         ("Недействительный результат по причине нарушения порядка проведения ГИА"),
        VOID_WITH_RETAKE_ALLOWED    ("Аннулирован с правом пересдачи"),
        VOID_WITH_RETAKE_DENIED     ("Аннулирован без права пересдачи"),
        EXPIRED                     ("Истек срок"),
        NOT_FOUND                   ("Не найдено");

        private String _title;

        StateExamResultFisStatus(String title){ _title = title; }

        @Override public String toString(){ return _title; }

        public boolean isValid(){ return this == VALID; }


        public static StateExamResultFisStatus getStatusByTitle(String title){ return STATUS_TITLE_MAP.get(title); }

        private static final Map<String, StateExamResultFisStatus> STATUS_TITLE_MAP = new HashMap<>(StateExamResultFisStatus.values().length);
        static
        {
            for (StateExamResultFisStatus value: StateExamResultFisStatus.values())
                STATUS_TITLE_MAP.put(value._title, value);
        }
    }
}
