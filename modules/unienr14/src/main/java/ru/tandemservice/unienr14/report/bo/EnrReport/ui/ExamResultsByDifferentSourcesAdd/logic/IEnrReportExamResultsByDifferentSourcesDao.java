/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.ExamResultsByDifferentSourcesAdd.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.ExamResultsByDifferentSourcesAdd.EnrReportExamResultsByDifferentSourcesAddUI;

/**
 * @author rsizonenko
 * @since 01.07.2014
 */
public interface IEnrReportExamResultsByDifferentSourcesDao extends INeedPersistenceSupport {
    long createReport(EnrReportExamResultsByDifferentSourcesAddUI model);
}
