/* $Id:$ */
package ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.shared.commonbase.base.util.CommonBaseSettingsUtil;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.CorrectBSPlans.EnrCompetitionCorrectBSPlans;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.CorrectBSPlans.EnrCompetitionCorrectBSPlansUI;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.EnrProgramSetManager;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.AddEdit.EnrProgramSetAddEdit;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.AddEdit.EnrProgramSetAddEditUI;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.Collection;
import java.util.Map;

/**
 * @author oleyba
 * @since 2/4/14
 */
public class EnrProgramSetListUI extends UIPresenter
{
    @Override
    public void onComponentRefresh()
    {
        this.getSettings().set(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
    }

    @Override
    public void onComponentBindReturnParameters(String childRegionName, Map<String, Object> returnedData)
    {
        if (UIDefines.DIALOG_REGION_NAME.equals(childRegionName)) {
            Collection<IEntity> selected = getConfig().<BaseSearchListDataSource>getDataSource(EnrProgramSetList.EXAM_SET_SEARCH_DS).getOptionColumnSelectedObjects("select");
            selected.clear();
        }
    }

    public void onClickAddProgramSet()
    {
        EnrEnrollmentCampaign ec = getSettings().get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
        if (null != ec) ec.checkOpen();
        getActivationBuilder()
                .asRegion(EnrProgramSetAddEdit.class)
                .parameter(EnrProgramSetAddEditUI.SECONDARY, false)
                .parameter(EnrProgramSetAddEditUI.REQUEST_TYPE_BIND, getRequestType())
                .activate();
    }

    public void onClickEditProgramSet()
    {
        getActivationBuilder()
                .asRegion(EnrProgramSetAddEdit.class)
                .parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong())
                .parameter(EnrProgramSetAddEditUI.SECONDARY, false)
                .activate();
    }

    public void onClickDeleteProgramSet()
    {
        EnrProgramSetManager.instance().dao().deleteProgramSet(getListenerParameterAsLong());
    }

    public void onClickCalculatePlans()
    {
        final Collection<IEntity> selectedPsOu = getConfig().<BaseSearchListDataSource>getDataSource(EnrProgramSetList.EXAM_SET_SEARCH_DS).getOptionColumnSelectedObjects("select");
        if (selectedPsOu.isEmpty())
            throw new ApplicationException("Необходимо выбрать хотя бы один объект из списка.");

        getActivationBuilder().asRegionDialog(EnrCompetitionCorrectBSPlans.class)
            .parameter(PUBLISHER_ID, getCampaign().getId())
            .parameter(EnrCompetitionCorrectBSPlansUI.BIND_PS_IDS, UniBaseDao.ids(selectedPsOu))
            .activate();
    }

    public void onEnrCampaignRefresh()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getCampaign());
    }

    public void onChangeRequestType()
    {
        onClickClear();
    }

    public void onClickSearch()
    {
        this.saveSettings();
    }

    public void onClickClear()
    {
        CommonBaseSettingsUtil.clearAndSaveSettingsExcept(this.getSettings(), EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, EnrProgramSetList.BIND_REQUEST_TYPE);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, getCampaign());
        dataSource.put(EnrProgramSetList.BIND_REQUEST_TYPE, getRequestType());
        dataSource.put(EnrProgramSetList.BIND_PROGRAM_FORM, this.getSettings().get(EnrProgramSetList.BIND_PROGRAM_FORM));
        dataSource.put(EnrProgramSetList.BIND_ORG_UNITS, this.getSettings().get(EnrProgramSetList.BIND_ORG_UNITS));
        dataSource.put(EnrProgramSetList.BIND_SUBJECT_CODE, this.getSettings().get(EnrProgramSetList.BIND_SUBJECT_CODE));
        dataSource.put(EnrProgramSetList.BIND_PROGRAM_SUBJECTS, this.getSettings().get(EnrProgramSetList.BIND_PROGRAM_SUBJECTS));
        dataSource.put(EnrProgramSetList.BIND_EDU_PROGRAMS, this.getSettings().get(EnrProgramSetList.BIND_EDU_PROGRAMS));
        dataSource.put(EnrProgramSetList.BIND_PROGRAM_SET_TITLE, this.getSettings().get(EnrProgramSetList.BIND_PROGRAM_SET_TITLE));
    }

    public EnrEnrollmentCampaign getCampaign(){ return this.getSettings().get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN); }

    public EnrRequestType getRequestType(){ return this.getSettings().get(EnrProgramSetList.BIND_REQUEST_TYPE); }

    public boolean isNothingSelected(){ return getCampaign() == null || getRequestType() == null; }
}