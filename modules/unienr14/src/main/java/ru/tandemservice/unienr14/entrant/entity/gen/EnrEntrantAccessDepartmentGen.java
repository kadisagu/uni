package ru.tandemservice.unienr14.entrant.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.catalog.entity.EnrAccessDepartment;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantAccessDepartment;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Подготовительное отделение абитуриента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrEntrantAccessDepartmentGen extends EntityBase
 implements INaturalIdentifiable<EnrEntrantAccessDepartmentGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.entrant.entity.EnrEntrantAccessDepartment";
    public static final String ENTITY_NAME = "enrEntrantAccessDepartment";
    public static final int VERSION_HASH = 1085563201;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTRANT = "entrant";
    public static final String L_ACCESS_DEPARTMENT = "accessDepartment";

    private EnrEntrant _entrant;     // Абитуриент
    private EnrAccessDepartment _accessDepartment;     // Отделение

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     */
    @NotNull
    public EnrEntrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant Абитуриент. Свойство не может быть null.
     */
    public void setEntrant(EnrEntrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    /**
     * @return Отделение. Свойство не может быть null.
     */
    @NotNull
    public EnrAccessDepartment getAccessDepartment()
    {
        return _accessDepartment;
    }

    /**
     * @param accessDepartment Отделение. Свойство не может быть null.
     */
    public void setAccessDepartment(EnrAccessDepartment accessDepartment)
    {
        dirty(_accessDepartment, accessDepartment);
        _accessDepartment = accessDepartment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrEntrantAccessDepartmentGen)
        {
            if (withNaturalIdProperties)
            {
                setEntrant(((EnrEntrantAccessDepartment)another).getEntrant());
                setAccessDepartment(((EnrEntrantAccessDepartment)another).getAccessDepartment());
            }
        }
    }

    public INaturalId<EnrEntrantAccessDepartmentGen> getNaturalId()
    {
        return new NaturalId(getEntrant(), getAccessDepartment());
    }

    public static class NaturalId extends NaturalIdBase<EnrEntrantAccessDepartmentGen>
    {
        private static final String PROXY_NAME = "EnrEntrantAccessDepartmentNaturalProxy";

        private Long _entrant;
        private Long _accessDepartment;

        public NaturalId()
        {}

        public NaturalId(EnrEntrant entrant, EnrAccessDepartment accessDepartment)
        {
            _entrant = ((IEntity) entrant).getId();
            _accessDepartment = ((IEntity) accessDepartment).getId();
        }

        public Long getEntrant()
        {
            return _entrant;
        }

        public void setEntrant(Long entrant)
        {
            _entrant = entrant;
        }

        public Long getAccessDepartment()
        {
            return _accessDepartment;
        }

        public void setAccessDepartment(Long accessDepartment)
        {
            _accessDepartment = accessDepartment;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrEntrantAccessDepartmentGen.NaturalId) ) return false;

            EnrEntrantAccessDepartmentGen.NaturalId that = (NaturalId) o;

            if( !equals(getEntrant(), that.getEntrant()) ) return false;
            if( !equals(getAccessDepartment(), that.getAccessDepartment()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEntrant());
            result = hashCode(result, getAccessDepartment());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEntrant());
            sb.append("/");
            sb.append(getAccessDepartment());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrEntrantAccessDepartmentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrEntrantAccessDepartment.class;
        }

        public T newInstance()
        {
            return (T) new EnrEntrantAccessDepartment();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entrant":
                    return obj.getEntrant();
                case "accessDepartment":
                    return obj.getAccessDepartment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entrant":
                    obj.setEntrant((EnrEntrant) value);
                    return;
                case "accessDepartment":
                    obj.setAccessDepartment((EnrAccessDepartment) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entrant":
                        return true;
                case "accessDepartment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entrant":
                    return true;
                case "accessDepartment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entrant":
                    return EnrEntrant.class;
                case "accessDepartment":
                    return EnrAccessDepartment.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrEntrantAccessDepartment> _dslPath = new Path<EnrEntrantAccessDepartment>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrEntrantAccessDepartment");
    }
            

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantAccessDepartment#getEntrant()
     */
    public static EnrEntrant.Path<EnrEntrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return Отделение. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantAccessDepartment#getAccessDepartment()
     */
    public static EnrAccessDepartment.Path<EnrAccessDepartment> accessDepartment()
    {
        return _dslPath.accessDepartment();
    }

    public static class Path<E extends EnrEntrantAccessDepartment> extends EntityPath<E>
    {
        private EnrEntrant.Path<EnrEntrant> _entrant;
        private EnrAccessDepartment.Path<EnrAccessDepartment> _accessDepartment;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantAccessDepartment#getEntrant()
     */
        public EnrEntrant.Path<EnrEntrant> entrant()
        {
            if(_entrant == null )
                _entrant = new EnrEntrant.Path<EnrEntrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return Отделение. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantAccessDepartment#getAccessDepartment()
     */
        public EnrAccessDepartment.Path<EnrAccessDepartment> accessDepartment()
        {
            if(_accessDepartment == null )
                _accessDepartment = new EnrAccessDepartment.Path<EnrAccessDepartment>(L_ACCESS_DEPARTMENT, this);
            return _accessDepartment;
        }

        public Class getEntityClass()
        {
            return EnrEntrantAccessDepartment.class;
        }

        public String getEntityName()
        {
            return "enrEntrantAccessDepartment";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
