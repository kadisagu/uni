/**
 *$Id: SystemActionPubExt.java 26199 2013-02-18 06:54:37Z vdanilov $
 */
package ru.tandemservice.unienr14.base.ext.SystemAction.ui.Pub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.ui.Pub.SystemActionPub;


@Configuration
public class SystemActionPubExt extends BusinessComponentExtensionManager
{
    public static final String ACTION_PUB_ADDON_NAME = "ec14SystemActionPubAddon";

    @Autowired
    private SystemActionPub _systemActionPub;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_systemActionPub.presenterExtPoint())
        .addAddon(uiAddon(ACTION_PUB_ADDON_NAME, EnrSystemActionPubAddon.class))
        .create();
    }
}
