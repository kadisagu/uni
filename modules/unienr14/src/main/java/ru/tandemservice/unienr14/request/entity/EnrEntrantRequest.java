package ru.tandemservice.unienr14.request.entity;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;

import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.EnrEntrantRequestManager;
import ru.tandemservice.unienr14.request.entity.gen.EnrEntrantRequestGen;

/**
 * Заявление абитуриента (ВПО)
 */
public class EnrEntrantRequest extends EnrEntrantRequestGen implements ITitled
{
    @Override
    public String getTitle() {
        return "Заявление №" + getStringNumber();
    }

    @Override
    @EntityDSLSupport(parts = EnrEntrantRequest.P_REG_NUMBER)
    public String getStringNumber() {
        return EnrEntrantRequestManager.instance().getEntrantRequestNumberFormatter().format(getRegNumber());
    }

    @Override
    @EntityDSLSupport (parts = EnrEntrantRequest.P_REG_NUMBER)
    public String getStringNumberWithEcTitle() {
        return getStringNumber() + " ("+ getEntrant().getEnrollmentCampaign().getTitle()+")";
    }

    public String getStateTitle() {
        return isTakeAwayDocument() ? "забрал документы" : "активное";
    }

    @Override
    public String getBenefitStatementTitle(){ return ""; }

    public boolean isEditDisabled() {
        return isTakeAwayDocument() || !getEntrant().isAccessible();
    }

    @Override public EnrEntrantRequest getRequest(){ return this; }

    public static EntityComboDataSourceHandler defaultSelectDSHandler(String name) {
        return new EntityComboDataSourceHandler(name, EnrEntrantRequest.class)
                .filter(EnrEntrantRequest.regNumber())
                .order(EnrEntrantRequest.regNumber());
    }
}