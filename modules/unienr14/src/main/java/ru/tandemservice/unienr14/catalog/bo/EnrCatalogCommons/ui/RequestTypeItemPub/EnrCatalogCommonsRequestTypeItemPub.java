/* $Id$ */
package ru.tandemservice.unienr14.catalog.bo.EnrCatalogCommons.ui.RequestTypeItemPub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.ui.DynamicItemPub.CatalogDynamicItemPub;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType2eduProgramKindRel;

import static org.tandemframework.hibsupport.dql.DQLExpressions.exists;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Nikolay Fedorovskih
 * @since 02.04.2015
 */
@Configuration
public class EnrCatalogCommonsRequestTypeItemPub extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(searchListDS("eduProgramKindDS", eduProgramKindDSColumns(), eduProgramKindDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint eduProgramKindDSColumns()
    {
        return columnListExtPointBuilder("eduProgramKindDS")
                .addColumn(publisherColumn("title", EduProgramKind.title())
                                   .businessComponent(CatalogDynamicItemPub.class))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> eduProgramKindDSHandler()
    {
        final EntityComboDataSourceHandler handler = new EntityComboDataSourceHandler(getName(), EduProgramKind.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(exists(
                        EnrRequestType2eduProgramKindRel.class,
                        EnrRequestType2eduProgramKindRel.requestType().s(), context.get(EnrCatalogCommonsRequestTypeItemPubUI.REQUEST_TYPE_ID_FILTER_PROPERTY),
                        EnrRequestType2eduProgramKindRel.programKind().s(), property(alias)
                ));
            }
        };
        handler.order(EduProgramKind.priority());
        return handler;
    }
}