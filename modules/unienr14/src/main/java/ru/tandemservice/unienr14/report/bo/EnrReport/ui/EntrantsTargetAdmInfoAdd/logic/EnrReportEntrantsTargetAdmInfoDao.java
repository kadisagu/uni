/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsTargetAdmInfoAdd.logic;

import com.google.common.primitives.Ints;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.MergeType;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.IRtfRowIntercepter;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import org.tandemframework.shared.person.base.entity.PersonForeignLanguage;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrExamSetVariant;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.exams.entity.EnrExamSetElement;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsTargetAdmInfoAdd.EnrReportEntrantsTargetAdmInfoAddUI;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportUtil;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.OrgUnitPrintTiltleComparator;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.RtfBackslashScreener;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder;
import ru.tandemservice.unienr14.report.entity.EnrReportEntrantsTargetAdmInfo;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA;
import ru.tandemservice.unienr14.request.entity.EnrRequestedProgram;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignTargetAdmissionKind;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.text.SimpleDateFormat;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 20.06.2014
 */
public class EnrReportEntrantsTargetAdmInfoDao extends UniBaseDao implements IEnrReportEntrantsTargetAdmInfoDao {

    private static String FILIAL = "filial";
    private static String PROGRAM_SPECIALIZATION = "programSpecialization";
    private static String PROGRAM_SUBJECT = "programSubject";
    private static String FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    private static String PROGRAM_SET = "programSet";
    private static String FORM_DATE = "formDate";


    @Override
    public long createReport(EnrReportEntrantsTargetAdmInfoAddUI model) {
        EnrReportEntrantsTargetAdmInfo report = new EnrReportEntrantsTargetAdmInfo();
        
        report.setFormingDate(new Date());

        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateSelector().getDateFrom());
        report.setDateTo(model.getDateSelector().getDateTo());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, EnrReportEntrantsTargetAdmInfo.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE, EnrReportEntrantsTargetAdmInfo.P_COMPENSATION_TYPE, "shortTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, EnrReportEntrantsTargetAdmInfo.P_PROGRAM_FORM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPETITION_TYPE, EnrReportEntrantsTargetAdmInfo.P_COMPETITION_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, EnrReportEntrantsTargetAdmInfo.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, EnrReportEntrantsTargetAdmInfo.P_FORMATIVE_ORG_UNIT, "fullTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, EnrReportEntrantsTargetAdmInfo.P_PROGRAM_SUBJECT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, EnrReportEntrantsTargetAdmInfo.P_EDU_PROGRAM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, EnrReportEntrantsTargetAdmInfo.P_PROGRAM_SET, "title");

        report.setFormingType(model.getFormType().getTitle());

        DatabaseFile content = new DatabaseFile();

        content.setContent(buildReport(model));
        content.setFilename("EnrReportEntrantsTargetAdmInfo.rtf");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        save(content);

        report.setContent(content);
        save(report);

        return report.getId();
    }

    private byte[] buildReport(EnrReportEntrantsTargetAdmInfoAddUI model)
    {
        //rtf
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(EnrScriptItem.class, EnrScriptItemCodes.REPORT_ENTRANTS_TARGET_ADM_INFO);
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();

        EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder()
                .notArchiveOnly()
                .filter(model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo())
                .filter(model.getEnrollmentCampaign());
        filterAddon.applyFilters(requestedCompDQL, requestedCompDQL.competition());

        // Убираем отозванные заявления
        requestedCompDQL
                .where(eq(property("request", EnrEntrantRequest.takeAwayDocument()), value(Boolean.FALSE)))
                .column(property("reqComp"));
        List<EnrRequestedCompetitionTA> fromQueryReqCompList = getList(requestedCompDQL);

        requestedCompDQL
                .resetColumns()
                .joinEntity("reqComp", DQLJoinType.left, EnrRequestedProgram.class, "reqProg", eq(property(EnrRequestedProgram.requestedCompetition().fromAlias("reqProg")), property("reqComp")))
                .column(property("reqProg"));

        List<EnrRequestedProgram> fromQueryReqProgList = getList(requestedCompDQL);

        requestedCompDQL.resetColumns();
        requestedCompDQL
                .joinEntity("reqComp", DQLJoinType.left, EnrChosenEntranceExam.class, "vvi", eq(property("reqComp"), property("vvi", EnrChosenEntranceExam.requestedCompetition())))
                .column(property("vvi"));

        List<EnrChosenEntranceExam> fromQueryVviList = getList(requestedCompDQL);


        requestedCompDQL
                .resetColumns()
                .joinEntity("entrant", DQLJoinType.left, PersonForeignLanguage.class, "lang", eq(property("entrant", EnrEntrant.person().id()), property("lang", PersonForeignLanguage.person().id())))
                .column(property("lang"));

        List<PersonForeignLanguage> fromQueryLangs = getList(requestedCompDQL);

        requestedCompDQL
                .resetColumns()
                .joinEntity("ps", DQLJoinType.left, EnrExamSetVariant.class, "examSet", eq(property("ps", EnrProgramSetBase.id()), property("examSet", EnrExamSetVariant.owner().id())))
                .column("examSet");

        List<EnrExamSetVariant> fromQueryExamVariants = getList(requestedCompDQL);

        requestedCompDQL.resetColumns()
                .joinEntity("ps", DQLJoinType.inner, EnrProgramSetItem.class, "psitem", eq(property(EnrProgramSetItem.programSet().fromAlias("psitem")), property("ps")))
                .column(property("psitem"));

        List<EnrProgramSetItem> fromQueryProgSetItems = getList(requestedCompDQL);

        Map<EnrProgramSetBase, Set<EnrProgramSetItem>> programSetItemsMap = SafeMap.get(HashSet.class);

        for (EnrProgramSetItem item : fromQueryProgSetItems) {
            programSetItemsMap.get(item.getProgramSet()).add(item);
        }



        Map<EnrRequestedCompetitionTA, Set<EnrChosenEntranceExam>> vviMap = new HashMap<>();

        for (EnrChosenEntranceExam exam : fromQueryVviList)
        {
            if (exam == null) continue;
            if (!vviMap.containsKey((EnrRequestedCompetitionTA) exam.getRequestedCompetition()))
                vviMap.put((EnrRequestedCompetitionTA) exam.getRequestedCompetition(), new HashSet<EnrChosenEntranceExam>());
            vviMap.get((EnrRequestedCompetitionTA) exam.getRequestedCompetition()).add(exam);
        }

        Map<Person, Set<PersonForeignLanguage>> langMap = new HashMap<>();

        for (PersonForeignLanguage lang : fromQueryLangs)
        {
            if (lang == null) continue;
            if (!langMap.containsKey(lang.getPerson()))
                langMap.put(lang.getPerson(), new HashSet<PersonForeignLanguage>());
            langMap.get(lang.getPerson()).add(lang);
        }

        Map<EnrProgramSetBase, Set<EnrExamSetVariant>> examVarMap = new HashMap<>();

        for (EnrExamSetVariant examVar : fromQueryExamVariants)
        {
            if (examVar == null) continue;
            if (!examVarMap.containsKey((EnrProgramSetBase)examVar.getOwner()))
                examVarMap.put((EnrProgramSetBase) examVar.getOwner(), new HashSet<EnrExamSetVariant>());
            examVarMap.get((EnrProgramSetBase) examVar.getOwner()).add(examVar);
        }

        // компараторы
        Comparator<EnrProgramSetBase> programSetBaseComparator = new Comparator<EnrProgramSetBase>() {
            @Override
            public int compare(EnrProgramSetBase o1, EnrProgramSetBase o2) {
                return o1.getTitle().compareToIgnoreCase(o2.getTitle());
            }
        };

        Comparator<EnrCampaignTargetAdmissionKind> admissionKindComparator = new Comparator<EnrCampaignTargetAdmissionKind>() {
            @Override
            public int compare(EnrCampaignTargetAdmissionKind o1, EnrCampaignTargetAdmissionKind o2) {
                return o1.getTitle().compareToIgnoreCase(o2.getTitle());
            }
        };
        Comparator<ExternalOrgUnit> externalOrgUnitComparator = new Comparator<ExternalOrgUnit>() {
            @Override
            public int compare(ExternalOrgUnit o1, ExternalOrgUnit o2) {
                if (o1==o2) return 0;
                if (o1==null) return 1;
                if (o2==null) return -1;
                return o1.getTitle().compareToIgnoreCase(o2.getTitle());
            }
        };
        Comparator<OrgUnit> topDependentOrgUnitComparator = new Comparator<OrgUnit>() {
            @Override
            public int compare(OrgUnit o1, OrgUnit o2) {
                int i = Boolean.compare(null == o1.getParent(), null == o2.getParent());
                if (0 != i) { return -i; }

                return o1.getPrintTitle().compareToIgnoreCase(o2.getPrintTitle());
            }
        };
        Comparator<EnrProgramSetItem> programSetItemComparator = new Comparator<EnrProgramSetItem>() {
            @Override
            public int compare(EnrProgramSetItem o1, EnrProgramSetItem o2) {
                int result = o1.getProgramSet().getProgramKind().getCode().compareTo(o2.getProgramSet().getProgramKind().getCode());
                if (result != 0) return result;
                result = o1.getProgramSet().getProgramSubject().getTitleWithCode().compareToIgnoreCase(o2.getProgramSet().getProgramSubject().getTitleWithCode());
                if (result != 0) return result;
                if (o1.getProgram() == o2.getProgram())
                    return 0;
                if (o1.getProgram() == null)
                    return 1;
                if (o2.getProgram() == null)
                    return -1;
                return o1.getProgram().getProgramSpecialization().getTitle().compareToIgnoreCase(o2.getProgram().getProgramSpecialization().getTitle());
            }
        };

        // вытаскиваем из шаблона таблицу и удаляем её
        RtfTable tTableElement = (RtfTable) UniRtfUtil.findElement(document.getElementList(), "T");
        document.getElementList().clear();



        boolean formByProgramSets = model.getFormType().getId() == 0L;

        boolean notFirstIteration = false;

        if (formByProgramSets) {

            Map<OrgUnit, Map<OrgUnit, Map<EnrProgramSetBase, Map<EnrCampaignTargetAdmissionKind, Map<ExternalOrgUnit, Set<EnrRequestedCompetitionTA>>>>>> dataMap = new TreeMap<>(topDependentOrgUnitComparator);


            // карта с данными
            for (EnrRequestedCompetitionTA reqCompTA : fromQueryReqCompList) {
                OrgUnit orgUnit = reqCompTA.getCompetition().getProgramSetOrgUnit().getOrgUnit().getInstitutionOrgUnit().getOrgUnit();
                OrgUnit formative = reqCompTA.getCompetition().getProgramSetOrgUnit().getFormativeOrgUnit();
                EnrProgramSetBase programSet = reqCompTA.getCompetition().getProgramSetOrgUnit().getProgramSet();
                Map<OrgUnit, Map<EnrProgramSetBase, Map<EnrCampaignTargetAdmissionKind, Map<ExternalOrgUnit, Set<EnrRequestedCompetitionTA>>>>> orgUnitMap = SafeMap.safeGet(dataMap, orgUnit, TreeMap.class, new OrgUnitPrintTiltleComparator());
                Map<EnrProgramSetBase, Map<EnrCampaignTargetAdmissionKind, Map<ExternalOrgUnit, Set<EnrRequestedCompetitionTA>>>> enrProgramSetBaseMap = SafeMap.safeGet(orgUnitMap, formative, TreeMap.class, programSetBaseComparator);
                Map<EnrCampaignTargetAdmissionKind, Map<ExternalOrgUnit, Set<EnrRequestedCompetitionTA>>> enrCampaignTargetAdmissionKindMap = SafeMap.safeGet(enrProgramSetBaseMap, programSet, TreeMap.class, admissionKindComparator);
                Map<ExternalOrgUnit, Set<EnrRequestedCompetitionTA>> externalOrgUnitMap = SafeMap.safeGet(enrCampaignTargetAdmissionKindMap, reqCompTA.getTargetAdmissionKind(), TreeMap.class, externalOrgUnitComparator);
                Set<EnrRequestedCompetitionTA> requestedCompetitionTASet = SafeMap.safeGet(externalOrgUnitMap, reqCompTA.getTargetAdmissionOrgUnit(), HashSet.class);
                requestedCompetitionTASet.add(reqCompTA);
            }






            // заполняем таблицу
            for (Map.Entry<OrgUnit, Map<OrgUnit, Map<EnrProgramSetBase, Map<EnrCampaignTargetAdmissionKind, Map<ExternalOrgUnit, Set<EnrRequestedCompetitionTA>>>>>> filialEntry : dataMap.entrySet()) {

                OrgUnit filial = filialEntry.getKey();


                for (Map.Entry<OrgUnit, Map<EnrProgramSetBase, Map<EnrCampaignTargetAdmissionKind, Map<ExternalOrgUnit, Set<EnrRequestedCompetitionTA>>>>> formativeEntry : filialEntry.getValue().entrySet()) {

                    OrgUnit formative = formativeEntry.getKey();


                    for (Map.Entry<EnrProgramSetBase, Map<EnrCampaignTargetAdmissionKind, Map<ExternalOrgUnit, Set<EnrRequestedCompetitionTA>>>> programSetEntry : formativeEntry.getValue().entrySet()) {
                        EnrProgramSetBase programSetBase = programSetEntry.getKey();

                        Set<EnrExamSetVariant> examVars = examVarMap.containsKey(programSetBase) ? examVarMap.get(programSetBase) : null;

                        StringBuilder psTitleBuilder = new StringBuilder();
                        psTitleBuilder
                                .append(programSetBase.getProgramSubject().getTitleWithCode())
                                .append(" (")
                                .append(programSetBase.getProgramForm().getTitle())
                                .append(") | ")
                                .append(programSetBase.getTitle());
                        Map<String, String> tagMap = new HashMap<>();

                        tagMap.put(PROGRAM_SET, psTitleBuilder.toString());
                        tagMap.put(PROGRAM_SUBJECT, "");
                        tagMap.put(PROGRAM_SPECIALIZATION, "");
                        tagMap.put(FORMATIVE_ORG_UNIT, formative == null ? "Формирующее подразделение не указано" : formative.getPrintTitle());
                        if (formative!=null && filial != null && formative.equals(filial))
                            tagMap.put(FILIAL, "");
                        else if (filial == null)
                            tagMap.put(FILIAL, "Подразделение не указано");
                        else tagMap.put(FILIAL, filial.getPrintTitle());
                        tagMap.put(FORM_DATE, new SimpleDateFormat("dd.MM.yyyy").format(new Date()));


                        Map<EnrCampaignTargetAdmissionKind, Map<ExternalOrgUnit, Set<EnrRequestedCompetitionTA>>> tableMap = programSetEntry.getValue();
                        notFirstIteration = formTable(document, vviMap, langMap, examVars, tTableElement, notFirstIteration, tagMap, tableMap);
                    }
                }
            }
        }
        else
        {

            Map<OrgUnit, Map<OrgUnit, Map<EnrProgramSetItem, Map<EnrCampaignTargetAdmissionKind, Map<ExternalOrgUnit, Set<EnrRequestedCompetitionTA>>>>>> dataMap = new TreeMap<>(topDependentOrgUnitComparator);

            Set<EnrRequestedCompetitionTA> addedReqComps = new HashSet<>();

            // карта с данными
            for (EnrRequestedProgram reqProgram : fromQueryReqProgList) {
                if (reqProgram == null) continue;
                if (reqProgram.getPriority() != 1) continue;
                EnrProgramSetItem item = reqProgram.getProgramSetItem();
                EnrRequestedCompetitionTA reqCompTA = (EnrRequestedCompetitionTA) reqProgram.getRequestedCompetition();
                ExternalOrgUnit targetAdmissionOrgUnit = reqCompTA.getTargetAdmissionOrgUnit();
                EnrCampaignTargetAdmissionKind targetAdmissionKind = reqCompTA.getTargetAdmissionKind();
                OrgUnit orgUnit = reqCompTA.getCompetition().getProgramSetOrgUnit().getOrgUnit().getInstitutionOrgUnit().getOrgUnit();
                OrgUnit formative = reqCompTA.getCompetition().getProgramSetOrgUnit().getFormativeOrgUnit();


                Map<OrgUnit, Map<EnrProgramSetItem, Map<EnrCampaignTargetAdmissionKind, Map<ExternalOrgUnit, Set<EnrRequestedCompetitionTA>>>>> orgUnitMap = SafeMap.safeGet(dataMap, orgUnit, TreeMap.class, new OrgUnitPrintTiltleComparator());
                Map<EnrProgramSetItem, Map<EnrCampaignTargetAdmissionKind, Map<ExternalOrgUnit, Set<EnrRequestedCompetitionTA>>>> formativeMap = SafeMap.safeGet(orgUnitMap, formative, TreeMap.class, programSetItemComparator);
                Map<EnrCampaignTargetAdmissionKind, Map<ExternalOrgUnit, Set<EnrRequestedCompetitionTA>>> admissionKindMap = SafeMap.safeGet(formativeMap, item, TreeMap.class, admissionKindComparator);
                Map<ExternalOrgUnit, Set<EnrRequestedCompetitionTA>> externalOrgUnitMap = SafeMap.safeGet(admissionKindMap, targetAdmissionKind, TreeMap.class, externalOrgUnitComparator);
                Set<EnrRequestedCompetitionTA> dataSet = SafeMap.safeGet(externalOrgUnitMap, targetAdmissionOrgUnit, HashSet.class);
                dataSet.add(reqCompTA);
                addedReqComps.add(reqCompTA);
            }

            for (EnrRequestedCompetitionTA reqCompTA : fromQueryReqCompList) {
                if (addedReqComps.contains(reqCompTA)) continue;
                EnrProgramSetItem item = new EnrProgramSetItem();
                item.setProgramSet(reqCompTA.getCompetition().getProgramSetOrgUnit().getProgramSet());
                ExternalOrgUnit targetAdmissionOrgUnit = reqCompTA.getTargetAdmissionOrgUnit();
                EnrCampaignTargetAdmissionKind targetAdmissionKind = reqCompTA.getTargetAdmissionKind();
                OrgUnit orgUnit = reqCompTA.getCompetition().getProgramSetOrgUnit().getOrgUnit().getInstitutionOrgUnit().getOrgUnit();
                OrgUnit formative = reqCompTA.getCompetition().getProgramSetOrgUnit().getFormativeOrgUnit();
                Map<OrgUnit, Map<EnrProgramSetItem, Map<EnrCampaignTargetAdmissionKind, Map<ExternalOrgUnit, Set<EnrRequestedCompetitionTA>>>>> orgUnitMap = SafeMap.safeGet(dataMap, orgUnit, TreeMap.class, new OrgUnitPrintTiltleComparator());
                Map<EnrProgramSetItem, Map<EnrCampaignTargetAdmissionKind, Map<ExternalOrgUnit, Set<EnrRequestedCompetitionTA>>>> formativeMap = SafeMap.safeGet(orgUnitMap, formative, TreeMap.class, programSetItemComparator);
                Map<EnrCampaignTargetAdmissionKind, Map<ExternalOrgUnit, Set<EnrRequestedCompetitionTA>>> admissionKindMap = SafeMap.safeGet(formativeMap, item, TreeMap.class, admissionKindComparator);
                Map<ExternalOrgUnit, Set<EnrRequestedCompetitionTA>> externalOrgUnitMap = SafeMap.safeGet(admissionKindMap, targetAdmissionKind, TreeMap.class, externalOrgUnitComparator);
                Set<EnrRequestedCompetitionTA> dataSet = SafeMap.safeGet(externalOrgUnitMap, targetAdmissionOrgUnit, HashSet.class);
                dataSet.add(reqCompTA);
                addedReqComps.add(reqCompTA);
            }


            // заполняем таблицу
            for (Map.Entry<OrgUnit, Map<OrgUnit, Map<EnrProgramSetItem, Map<EnrCampaignTargetAdmissionKind, Map<ExternalOrgUnit, Set<EnrRequestedCompetitionTA>>>>>> filialEntry : dataMap.entrySet()) {

                OrgUnit filial = filialEntry.getKey();


                for (Map.Entry<OrgUnit, Map<EnrProgramSetItem, Map<EnrCampaignTargetAdmissionKind, Map<ExternalOrgUnit, Set<EnrRequestedCompetitionTA>>>>> formativeEntry : filialEntry.getValue().entrySet()) {

                    OrgUnit formative = formativeEntry.getKey();


                    for (Map.Entry<EnrProgramSetItem, Map<EnrCampaignTargetAdmissionKind, Map<ExternalOrgUnit, Set<EnrRequestedCompetitionTA>>>> setItemEntry : formativeEntry.getValue().entrySet()) {


                        Map<String, String> tagMap = new HashMap<>();

                        Set<EnrExamSetVariant> examVars = examVarMap.containsKey(setItemEntry.getKey().getProgramSet()) ? examVarMap.get(setItemEntry.getKey().getProgramSet()) : null;

                        tagMap.put(PROGRAM_SET, "");
                        tagMap.put(PROGRAM_SUBJECT, setItemEntry.getKey().getProgramSet().getProgramSubject().getTitleWithCode());
                        if (setItemEntry.getKey().getProgram() == null) {
                            if (programSetItemsMap.get(setItemEntry.getKey().getProgramSet()).size() == 1)
                                for (EnrProgramSetItem item : programSetItemsMap.get(setItemEntry.getKey().getProgramSet())) {
                                    tagMap.put(PROGRAM_SPECIALIZATION, item.getProgram().getProgramSpecialization().getTitle());
                                }
                            else
                                tagMap.put(PROGRAM_SPECIALIZATION, "У абитуриентов не выбраны образовательные программы");
                        }
                        else
                            tagMap.put(PROGRAM_SPECIALIZATION, setItemEntry.getKey().getProgram().getProgramSpecialization().getTitle());

                        tagMap.put(FORMATIVE_ORG_UNIT, formative == null ? "Формирующее подразделение не указано" : formative.getPrintTitle());
                        if (formative!=null && filial != null && formative.equals(filial))
                            tagMap.put(FILIAL, "");
                        else if (filial == null)
                            tagMap.put(FILIAL, "Подразделение не указано");
                        else tagMap.put(FILIAL, filial.getPrintTitle());
                        tagMap.put(FORM_DATE, new SimpleDateFormat("dd.MM.yyyy").format(new Date()));

                        Map<EnrCampaignTargetAdmissionKind, Map<ExternalOrgUnit, Set<EnrRequestedCompetitionTA>>> tableMap = setItemEntry.getValue();

                        notFirstIteration = formTable(document, vviMap, langMap, examVars, tTableElement, notFirstIteration, tagMap, tableMap);


                    }
                }
            }
        }
        return RtfUtil.toByteArray(document);
    }

    private boolean formTable(RtfDocument document,
                              Map<EnrRequestedCompetitionTA, Set<EnrChosenEntranceExam>> vviMap,
                              Map<Person, Set<PersonForeignLanguage>> langMap,
                              Set<EnrExamSetVariant> examVars,
                              RtfTable tTableElement,
                              boolean notFirstIteration,
                              Map<String, String> tagMap,
                              Map<EnrCampaignTargetAdmissionKind, Map<ExternalOrgUnit, Set<EnrRequestedCompetitionTA>>> tableMap)
    {
        if (notFirstIteration)
        {
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
        }
        notFirstIteration = true;
        document.getElementList().add(tTableElement.getClone());


        RtfInjectModifier rtfInjectModifier = new RtfInjectModifier();
        for (Map.Entry<String, String> entry : tagMap.entrySet()) {
            rtfInjectModifier.put(entry.getKey(), entry.getValue());
        }
        rtfInjectModifier.modify(document);


        final HashSet<EnrCampaignDiscipline> disciplines = new HashSet<>();
        if (examVars != null)
        {
            for (EnrExamSetVariant examVar : examVars)
                for (EnrExamSetElement element : examVar.getExamSet().getElementList())
                    if (element.getValue() instanceof EnrCampaignDiscipline)
                        disciplines.add(((EnrCampaignDiscipline) element.getValue()));
        }
        final List<EnrCampaignDiscipline> discList = new ArrayList<>();
        discList.addAll(disciplines);

        List<String[]> content = new ArrayList<>();
        final List<String> contentMarkers = new ArrayList<>();


        for (Map.Entry<EnrCampaignTargetAdmissionKind, Map<ExternalOrgUnit, Set<EnrRequestedCompetitionTA>>> admKindEntry : tableMap.entrySet())
        {

            content.add(new String[]{

                    admKindEntry.getKey().getTitle()
            });
            contentMarkers.add("kind");
            for (Map.Entry<ExternalOrgUnit, Set<EnrRequestedCompetitionTA>> admOrg : admKindEntry.getValue().entrySet())
            {

                content.add(new String[]{
                        admOrg.getKey() != null ? admOrg.getKey().getTitle() : "Организация не указана"
                });
                contentMarkers.add("org");
                List<String[]> subContent = new ArrayList<>();

                for (EnrRequestedCompetitionTA reqCompTA : admOrg.getValue()) {
                    List<String> row = new ArrayList<>();

                    row.add("");
                    final EnrEntrantRequest request = reqCompTA.getRequest();
                    row.add(request.getIdentityCard().getFullFio());
                    row.add(reqCompTA.isOriginalDocumentHandedIn() ? "Оригинал" : "Копия");
                    row.add(admKindEntry.getKey().getTitle());
                    row.add(admOrg.getKey() != null ? admOrg.getKey().getTitle() : "Организация не указана");
                    final EnrEntrant entrant = request.getEntrant();
                    row.add(entrant.getPersonalNumber());
                    final PersonEduDocument eduDocument = request.getEduDocument();
                    StringBuilder builder = new StringBuilder().append(eduDocument.getAvgMarkAsDouble() != null ? eduDocument.getAvgMarkAsDouble() : "");
                    if (eduDocument.getGraduationHonour() != null)
                        builder.append("\\line").append(eduDocument.getGraduationHonour().getTitle());

                    row.add(new RtfBackslashScreener().screenBackslashes(builder.toString()));
                    row.add(entrant.getPerson().isNeedDormitory() ? "Да" : "Нет");
                    row.add(new RtfBackslashScreener().screenBackslashes(UniStringUtils.join(langMap.get(entrant.getPerson()), PersonForeignLanguage.language().title().s(), "\\line")));
                    int sum = 0;
                    for (EnrCampaignDiscipline discipline : discList) {

                        boolean filled = false;
                        if (vviMap.containsKey(reqCompTA)) {
                            for (EnrChosenEntranceExam exam : vviMap.get(reqCompTA))
                                if (exam.getDiscipline().equals(discipline))
                                {
                                    if (exam.getMarkSource()!=null && exam.getMaxMarkForm()!=null) {
                                        row.add(new StringBuilder()
                                                .append(exam.getMarkSource().getMarkAsString())
                                                .append(" (")
                                                .append(exam.getMaxMarkForm().getPassForm().getTitle())
                                                .append(")")
                                                .toString());
                                        sum += exam.getMarkAsDouble().intValue();
                                        filled = true;
                                    }
                                }

                        }
                        if (!filled)
                            row.add("");
                    }
                    if (discList.size() == 0)
                        row.add("");

                    row.add(String.valueOf(sum));
                    StringBuilder address = new StringBuilder();
                    final AddressBase addressBase = request.getIdentityCard().getAddress();
                    if (addressBase != null) {
                        if (addressBase instanceof AddressDetailed) {
                            final AddressItem settlement = ((AddressDetailed) addressBase).getSettlement();
                            address
                                    .append(settlement != null ?
                                            settlement.getTitleWithType()
                                            : "Город не указан");
                        } else {
                            address.append(request.getIdentityCard().getCitizenship().getTitle());
                        }
                    } else address.append(request.getIdentityCard().getCitizenship().getTitle());

                    final String phoneDefault = entrant.getPerson().getContactData().getPhoneDefault();
                    if (phoneDefault != null)
                        address.append("\\line ").append(phoneDefault);

                    row.add(address.toString());

                    subContent.add(row.toArray(new String[row.size()]));
                    contentMarkers.add("row");
                }


                Collections.sort(subContent, new Comparator<String[]>() {
                    @Override
                    public int compare(String[] o1, String[] o2) {
                        final Integer val1 = Ints.tryParse(o1[o2.length - 2]);
                        final Integer val2 = Ints.tryParse(o2[o1.length - 2]);
                        return val2.compareTo(val1);
                    }
                });


                for (int i = 0; i < subContent.size(); i++) {
                    subContent.get(i)[0] = String.valueOf(i + 1);
                }

                content.addAll(subContent);

            }
        }

        RtfTableModifier modifier = new RtfTableModifier();

        // записываем в документ

        // здесь интерцептор разбивает на нужное кол-во ячеек ячейку под дисциплины либо пишет "не выбраны ви"
        modifier.put("D", new String[][]{{"№ п/п"}});
        modifier.put("D", new IRtfRowIntercepter() {
                    @Override
                    public void beforeModify(RtfTable table, int currentRowIndex) {
                        if (discList.size() > 0) {
                            int[] splitter = new int[discList.size()];
                            Arrays.fill(splitter, 1);


                            RtfUtil.splitRow(table.getRowList().get(2), 9, (newCell, index) -> newCell.addElement(RtfBean.getElementFactory().createRtfText(discList.get(index).getTitle())), splitter);
                            RtfUtil.splitRow(table.getRowList().get(3), 9, null, splitter);
                        }
                        else RtfUtil.splitRow(table.getRowList().get(2), 9, (newCell, index) -> newCell.addElement(RtfBean.getElementFactory().createRtfText("ВИ не выбраны")), new int[]{1});
                    }

                    @Override
                    public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value) {
                        return null;
                    }

                    @Override
                    public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex) {

                    }
                } );

        // здесь заполняется все остальное, мерджатся ячейки с заголовками, болдятся типы целевого приема
        modifier.put("T", content.toArray(new String[content.size()][]));
        modifier.put("T", new IRtfRowIntercepter() {
            @Override
            public void beforeModify(RtfTable table, int currentRowIndex) {
            }
            @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value) {
                RtfString string = new RtfString();
                if (contentMarkers.get(rowIndex).equals("kind"))
                    string.boldBegin().append(value, true).boldEnd();
                else
                    string.append(value, true);
                return string.toList();
            }
            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex) {
                for (int i = 3; i < newRowList.size(); i++) {
                    if (i-3>= 0 && i-3 < contentMarkers.size() && (contentMarkers.get(i-3).equals("kind") || contentMarkers.get(i-3).equals("org")))
                    {
                        for(RtfCell cell : newRowList.get(i).getCellList())
                            cell.setMergeType(MergeType.HORIZONTAL_MERGED_NEXT);
                        newRowList.get(i).getCellList().get(0).setMergeType(MergeType.HORIZONTAL_MERGED_FIRST);
                        newRowList.get(i).getCellList().get(0).append(IRtfData.QC);
                    }
                }
            }
        });
        modifier.modify(document);
        return notFirstIteration;
    }
}
