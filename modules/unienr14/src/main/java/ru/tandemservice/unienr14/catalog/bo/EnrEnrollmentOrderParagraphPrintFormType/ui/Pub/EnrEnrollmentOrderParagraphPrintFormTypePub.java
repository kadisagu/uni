/* $Id$ */
package ru.tandemservice.unienr14.catalog.bo.EnrEnrollmentOrderParagraphPrintFormType.ui.Pub;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;

/**
 * @author azhebko
 * @since 18.07.2014
 */
@Configuration
public class EnrEnrollmentOrderParagraphPrintFormTypePub extends BusinessComponentManager
{
}