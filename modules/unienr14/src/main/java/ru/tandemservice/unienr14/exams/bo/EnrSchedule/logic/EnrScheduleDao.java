/* $Id:$ */
package ru.tandemservice.unienr14.exams.bo.EnrSchedule.logic;

import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.exams.bo.EnrSchedule.EnrScheduleManager;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroup;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroupScheduleEvent;
import ru.tandemservice.unienr14.exams.entity.EnrExamScheduleEvent;
import ru.tandemservice.unienr14.exams.entity.gen.EnrExamGroupScheduleEventGen;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unischedule.base.entity.event.ScheduleEventPlace;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 5/16/13
 */
public class EnrScheduleDao extends UniBaseDao implements IEnrScheduleDao
{

    @Override
    public Long saveOrUpdateExamEvent(EnrExamScheduleEvent examEvent)
    {
        if (validateExamEvent(examEvent).hasErrors())
            throw new ApplicationException();

        getSession().saveOrUpdate(examEvent.getScheduleEvent());
        getSession().saveOrUpdate(examEvent.getScheduleEventPlace());
        getSession().saveOrUpdate(examEvent);

        return examEvent.getId();
    }

    private ErrorCollector validateExamEvent(EnrExamScheduleEvent examEvent)
    {
        final ErrorCollector errorCollector = ContextLocal.getErrorCollector();

        // даты
        {
            if (examEvent.getScheduleEvent().getDurationBegin().getTime() > examEvent.getScheduleEvent().getDurationEnd().getTime())
                errorCollector.add(EnrScheduleManager.instance().getProperty("enrScheduleDao.validateExamEvent.eventDateException"), "eventPeriodFrom", "eventPeriodTo");
        }

        // события в аудитории не должны пересекаться по дате и времени
        {
            final List<Object[]> dateList = new DQLSelectBuilder().fromEntity(ScheduleEventPlace.class, "p")
                    .column(property(ScheduleEventPlace.scheduleEvent().durationBegin().fromAlias("p")))
                    .column(property(ScheduleEventPlace.scheduleEvent().durationEnd().fromAlias("p")))
                    .where(eq(property(ScheduleEventPlace.place().fromAlias("p")), value(examEvent.getScheduleEventPlace().getPlace())))
                    .where(ne(property(ScheduleEventPlace.scheduleEvent().fromAlias("p")), value(examEvent.getScheduleEvent())))
                    .createStatement(getSession()).list();

            final Date begin1 = examEvent.getScheduleEvent().getDurationBegin();
            final Date end1 = examEvent.getScheduleEvent().getDurationEnd();

            for (Object[] objects : dateList)
            {
                final Date begin2 = (Date) objects[0];
                final Date end2 = (Date) objects[1];

                if (CommonBaseDateUtil.isBetween(begin1, begin2, end2) || CommonBaseDateUtil.isBetween(end1, begin2, end2))
                    errorCollector.add(EnrScheduleManager.instance().getProperty("enrScheduleDao.validateExamEvent.placeDateCrossException"));
                else {
                    if (CommonBaseDateUtil.isBetween(begin2, begin1, end1) || CommonBaseDateUtil.isBetween(end2, begin1, end1))
                        errorCollector.add(EnrScheduleManager.instance().getProperty("enrScheduleDao.validateExamEvent.placeDateCrossException"));
                }
            }
        }

        // дата проведения должна быть в периоде приемной кампании
        {
            final EnrEnrollmentCampaign enrCampaign = examEvent.getExamRoom().getEnrollmentCampaign();
            if (!CommonBaseDateUtil.isBetween(examEvent.getScheduleEvent().getDurationBegin(), enrCampaign.getDateFrom(), enrCampaign.getDateTo()))
                errorCollector.add(EnrScheduleManager.instance().getProperty("enrScheduleDao.validateExamEvent.eventDateEnrCampMismatchException"));
        }

        return errorCollector;
    }

    @Override
    public void doAttachExamGroupEvent(EnrExamScheduleEvent event, Collection<EnrExamGroup> examGroups)
    {
        final Long groupEventCount = new DQLSelectBuilder().fromEntity(EnrExamGroupScheduleEvent.class, "r").column(property(EnrExamGroupScheduleEvent.id().fromAlias("r")))
                .where(in(property(EnrExamGroupScheduleEvent.examGroup().fromAlias("r")), examGroups))
                .where(ne(property(EnrExamGroupScheduleEvent.examScheduleEvent().fromAlias("r")), value(event)))
                .createCountStatement(new DQLExecutionContext(getSession())).uniqueResult();

        if (groupEventCount > 0)
            throw new ApplicationException(EnrScheduleManager.instance().getProperty("enrScheduleDao.doAttachExamGroupEvent"));

        final List<EnrExamGroupScheduleEvent> targetExamGroupScheduleEventList = new ArrayList<>();
        for (EnrExamGroup examGroup : examGroups == null ? new ArrayList<EnrExamGroup>() : examGroups)
            targetExamGroupScheduleEventList.add(new EnrExamGroupScheduleEvent(examGroup, event));

        new MergeAction.SessionMergeAction<EnrExamGroupScheduleEvent.NaturalId, EnrExamGroupScheduleEvent>()
        {

            @Override
            protected EnrExamGroupScheduleEventGen.NaturalId key(EnrExamGroupScheduleEvent source)
            {
                return (EnrExamGroupScheduleEventGen.NaturalId) source.getNaturalId();
            }

            @Override
            protected EnrExamGroupScheduleEvent buildRow(EnrExamGroupScheduleEvent source)
            {
                return new EnrExamGroupScheduleEvent(source.getExamGroup(), source.getExamScheduleEvent());
            }

            @Override
            protected void fill(EnrExamGroupScheduleEvent target, EnrExamGroupScheduleEvent source)
            {

            }
        }
                .merge(getList(EnrExamGroupScheduleEvent.class, EnrExamGroupScheduleEvent.examScheduleEvent(), event), targetExamGroupScheduleEventList);
    }

    @Override
    public void deleteEvent(Long eventId)
    {
        EnrExamScheduleEvent examGroupScheduleEvent = get(EnrExamScheduleEvent.class, eventId);
        delete(eventId);
        delete(examGroupScheduleEvent.getScheduleEvent());
    }
}
