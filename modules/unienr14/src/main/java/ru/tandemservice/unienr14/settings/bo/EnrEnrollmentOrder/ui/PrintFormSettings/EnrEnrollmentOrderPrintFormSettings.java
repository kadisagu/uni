/* $Id:$ */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentOrder.ui.PrintFormSettings;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.util.Icon;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderPrintFormType;

/**
 * @author oleyba
 * @since 7/7/14
 */
@Configuration
public class EnrEnrollmentOrderPrintFormSettings extends BusinessComponentManager
{
    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
            .addDataSource(searchListDS("printFormDS", orderTypeSearchDSColumns(), orderTypeSearchDSHandler()))
            .create();
    }

    @Bean
    public ColumnListExtPoint orderTypeSearchDSColumns()
    {
        return columnListExtPointBuilder("printFormDS")
            .addColumn(textColumn("orderType", EnrOrderPrintFormType.orderType().title()))
            .addColumn(textColumn("printForm", EnrOrderPrintFormType.title()))
            .addColumn(actionColumn("up", new Icon("up"), "onClickUp"))
            .addColumn(actionColumn("down", new Icon("down"), "onClickDown"))
            .addColumn(toggleColumn("used", EnrOrderPrintFormType.used())
                .toggleOnListener("onToggleUsed")
                .toggleOffListener("onToggleUsed"))
            .addColumn(toggleColumn("basic", EnrOrderPrintFormType.basic())
                .toggleOnListener("onToggleBasic")
                .toggleOffListener("onToggleBasic"))
            .addColumn(toggleColumn("command", EnrOrderPrintFormType.command())
                .toggleOnListener("onToggleCommand")
                .toggleOffListener("onToggleCommand"))
            .addColumn(toggleColumn("group", EnrOrderPrintFormType.group())
                .toggleOnListener("onToggleGroup")
                .toggleOffListener("onToggleGroup"))
            .addColumn(toggleColumn("headman", EnrOrderPrintFormType.selectHeadman())
                .toggleOnListener("onToggleHeadman")
                .toggleOffListener("onToggleHeadman"))
            .addColumn(toggleColumn("reasonAndBasic", EnrOrderPrintFormType.reasonAndBasic())
                .toggleOnListener("onToggleReasonAndBasic")
                .toggleOffListener("onToggleReasonAndBasic"))
            .create();
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> orderTypeSearchDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrOrderPrintFormType.class)
            .order(EnrOrderPrintFormType.priority())
            .pageable(false);
    }
}