/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EnrollmentResultsBySubjectGroupsAdd.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EnrollmentResultsBySubjectGroupsAdd.EnrReportEnrollmentResultsBySubjectGroupsAddUI;

/**
 * @author rsizonenko
 * @since 28.08.2014
 */
public interface IEnrReportEnrollmentResultsBySubjectGroupsDao extends INeedPersistenceSupport {
    long createReport(EnrReportEnrollmentResultsBySubjectGroupsAddUI model);
}
