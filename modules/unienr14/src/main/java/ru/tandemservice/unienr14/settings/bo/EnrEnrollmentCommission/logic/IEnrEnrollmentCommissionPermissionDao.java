/* $Id$ */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.sec.IPrincipalContext;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;

import java.util.List;

/**
 * @author nvankov
 * @since 21.03.2016
 */
public interface IEnrEnrollmentCommissionPermissionDao extends INeedPersistenceSupport
{
    /**
     * Список id ОК, на которые есть глобальные или локальные права у пользовательского контекста
     *
     * @param principalContext Контекст, под которым в данный момент выступает пользователь
     */
    List<Long> getEnrollmentCommissionIds(IPrincipalContext principalContext);

    /**
     * Список ОК, на которые есть глобальные или локальные права у пользовательского контекста
     *
     * @param principalContext Контекст, под которым в данный момент выступает пользователь
     */
    List<EnrEnrollmentCommission> getEnrollmentCommissionList(IPrincipalContext principalContext);


    /**
     * true, если у пользовательского контекста есть глобальнок или локальное право на ОК
     *
     * @param enrollmentCommission ОК
     * @param principalContext Контекст, под которым в данный момент выступает пользователь
     */
    boolean hasPermissionForEnrCommission(EnrEnrollmentCommission enrollmentCommission, IPrincipalContext principalContext);

    /**
     * true, если у пользовательского контекста есть глобальнок право на выбор ОК
     *
     * @param principalContext Контекст, под которым в данный момент выступает пользователь
     */
    boolean hasGlobalPermissionForEnrCommission(IPrincipalContext principalContext);
}
