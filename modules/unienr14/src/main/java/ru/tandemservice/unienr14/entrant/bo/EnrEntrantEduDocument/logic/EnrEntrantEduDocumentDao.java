/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrantEduDocument.logic;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.person.base.bo.PersonEduDocument.PersonEduDocumentManager;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantOriginalDocumentStatus;
import ru.tandemservice.unienr14.entrant.entity.gen.EnrEntrantOriginalDocumentStatusGen;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;

import java.util.Date;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
/**
 * @author oleyba
 * @since 3/18/14
 */
public class EnrEntrantEduDocumentDao extends UniBaseDao implements IEnrEntrantEduDocumentDao
{
    @Override
    public void saveOrUpdateEduDocument(EnrEntrant entrant, PersonEduDocument document, IUploadFile scanCopyFile, Date originalInDate, Date originalOutDate, EnrRequestedCompetition requestedCompetition)
    {
        boolean isFirst = !existsEntity(PersonEduDocument.class, PersonEduDocument.person().s(), entrant.getPerson());
        PersonEduDocumentManager.instance().dao().saveOrUpdateDocument(document, scanCopyFile);

        if (isFirst)
        {
            entrant.getPerson().setMainEduDocument(document);
            update(entrant.getPerson());
        }

        EnrEntrantOriginalDocumentStatus existingOriginalStatus = getByNaturalId(new EnrEntrantOriginalDocumentStatusGen.NaturalId(entrant, document));

        if (originalInDate != null) {
            EnrEntrantOriginalDocumentStatus status = existingOriginalStatus != null ? existingOriginalStatus : new EnrEntrantOriginalDocumentStatus(entrant, document);
            status.setRegistrationDate(originalInDate);
            status.setTakeAwayDate(originalOutDate);
            status.setRequestedCompetition(requestedCompetition);

            saveOrUpdate(status);
        } else if (existingOriginalStatus != null) {
            delete(existingOriginalStatus);
        }
    }

    @Override
    public boolean isNeedReqCompForOriginal(EnrEntrant entrant, Long eduDocumentId)
    {
        if(eduDocumentId != null && TwinComboDataSourceHandler.NO_ID.equals(entrant.getEnrollmentCampaign().getSettings().getAccountingRulesOrigEduDoc()))
        {

            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrRequestedCompetition.class, "rc")
                    .where(eq(property("rc", EnrRequestedCompetition.request().entrant()), value(entrant)))
                    .where(eq(property("rc", EnrRequestedCompetition.request().eduDocument().id()), value(eduDocumentId)));

            return existsEntity(builder.buildQuery());
        }

        return false;
    }
}