/* $Id$ */
package ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.bean.FastBeanUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.dsl.MetaDSLPath;
import org.tandemframework.core.entity.dsl.PropertyPath;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetSecondary;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.coalesce;

/**
 * @author azhebko
 * @since 28.04.2014
 */
public class EnrCompetitionFilterAddon extends CommonFilterAddon<EnrCompetition>
{

    private boolean _refresh = true;
    private boolean _refreshed = false;

    @Override
    public void onComponentRefresh()
    {
        if (_refresh || (!_refresh && !_refreshed)) {
            super.onComponentRefresh();
            _refreshed = true;
        }
    }

    public EnrCompetitionFilterAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @Override
    public Class<EnrCompetition> getEntityClass()
    {
        return EnrCompetition.class;
    }

    // Правило фильтрации по подстроке филиала
    public static final CommonFilterContentConfig.ISubstringFilterRule ENR_ORG_UNIT_SUBSTRING_FILTER_RULE = (alias, filter) -> {
        // При фильтрации использован case, т.к. в селекте для головного подразделения выводится не его shortTitle, а захардкоженное "Головное подразделение".
        // Поэтому фильтровать надо соответственно тому, как видит это пользователь.
        final DQLSelectBuilder orgUnitBuilder = new DQLSelectBuilder();
        orgUnitBuilder.fromEntity(OrgUnit.class, "ou");
        orgUnitBuilder.column("ou.id");
        OrgUnit.applyShortTitleDQLFilter(orgUnitBuilder, "ou", filter);

        return in(property(alias, EnrOrgUnit.institutionOrgUnit().orgUnit().id()), orgUnitBuilder.buildQuery());
    };

    public static final String SETTING_NAME_REQUEST_TYPE = "enrRequestType";
    public static final String SETTING_NAME_COMPENSATION_TYPE = "compensationType";
    public static final String SETTING_NAME_PROGRAM_FORM = "eduProgramForm";
    public static final String SETTING_NAME_COMPETITION_TYPE = "enrCompetitionType";
    public static final String SETTING_NAME_ENR_ORG_UNIT = "enrOrgUnit";
    public static final String SETTING_NAME_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String SETTING_NAME_EDU_PROGRAM_SUBJECT = "eduProgramSubject";
    public static final String SETTING_NAME_PROGRAM_SET = "programSet";
    public static final String SETTING_NAME_EDU_PROGRAM = "eduProgram";

    public static final CommonFilterContentConfig<EnrRequestType> REQUEST_TYPE = new CommonFilterContentConfig<>(EnrRequestType.class, SETTING_NAME_REQUEST_TYPE, "Вид заявления", EnrCompetition.requestType(), EnrRequestType.shortTitle(), singletonList(EnrRequestType.shortTitle()), singletonList(EnrRequestType.code()));
    public static final CommonFilterContentConfig<CompensationType> COMPENSATION_TYPE = new CommonFilterContentConfig<>(CompensationType.class, SETTING_NAME_COMPENSATION_TYPE, "Вид возмещения затрат", EnrCompetition.type().compensationType(), CompensationType.shortTitle(), singletonList(CompensationType.shortTitle()), singletonList(CompensationType.code()));
    public static final CommonFilterContentConfig<EduProgramForm> PROGRAM_FORM = new CommonFilterContentConfig<>(EduProgramForm.class, SETTING_NAME_PROGRAM_FORM, "Форма обучения", EnrCompetition.programSetOrgUnit().programSet().programForm(), EduProgramForm.title(), singletonList(EduProgramForm.title()), singletonList(EduProgramForm.code()));
    public static final CommonFilterContentConfig<EnrCompetitionType> COMPETITION_TYPE = new CommonFilterContentConfig<>(EnrCompetitionType.class, SETTING_NAME_COMPETITION_TYPE, "Вид приема", EnrCompetition.type(), EnrCompetitionType.title(), singletonList(EnrCompetitionType.title()), singletonList(EnrRequestType.code()));
    public static final CommonFilterContentConfig<EnrOrgUnit> ENR_ORG_UNIT = new CommonFilterContentConfig<>(EnrOrgUnit.class, SETTING_NAME_ENR_ORG_UNIT, "Филиал", EnrCompetition.programSetOrgUnit().orgUnit(), EnrOrgUnit.institutionOrgUnit().orgUnit().shortTitleWithTopEmphasized(), ENR_ORG_UNIT_SUBSTRING_FILTER_RULE, EnrOrgUnit.ENR_ORG_UNIT_DEPARTMENT_TITLE_COMPARATOR);
    public static final CommonFilterContentConfig<OrgUnit> FORMATIVE_ORG_UNIT = new CommonFilterContentConfig<>(OrgUnit.class, SETTING_NAME_FORMATIVE_ORG_UNIT, "Формирующее подр.", EnrCompetition.programSetOrgUnit().formativeOrgUnit(), OrgUnit.fullTitle(), singletonList(OrgUnit.fullTitle()), singletonList(OrgUnit.fullTitle()));
    public static final CommonFilterContentConfig<EduProgramSubject> PROGRAM_SUBJECT = new CommonFilterContentConfig<>(EduProgramSubject.class, SETTING_NAME_EDU_PROGRAM_SUBJECT, "Направление, спец., профессия", EnrCompetition.programSetOrgUnit().programSet().programSubject(), EduProgramSubject.titleWithCode(), asList(EduProgramSubject.subjectCode(), EduProgramSubject.title()), asList(EduProgramSubject.subjectCode(), EduProgramSubject.title()));
    public static final CommonFilterContentConfig<EnrProgramSetBase> PROGRAM_SET = new CommonFilterContentConfig<>(EnrProgramSetBase.class, SETTING_NAME_PROGRAM_SET, "Набор образовательных программ", EnrCompetition.programSetOrgUnit().programSet(), EnrProgramSetBase.title(), singletonList(EnrProgramSetBase.title()), singletonList(EnrProgramSetBase.title()));

    public static final ICommonFilterFactory<IEntity> EDU_PROGRAM = EduProgramFilterItem::new;

    public boolean isRefresh()
    {
        return _refresh;
    }

    public void setRefresh(boolean refresh)
    {
        this._refresh = refresh;
    }

    public static class EduProgramFilterItem implements ICommonFilterItem
    {
        private Object _value;
        private ISelectModel model;
        private CommonFilterFormConfig config;
        private boolean _enableCheckboxChecked;
        private boolean _nextWithNewLine = true;
        private boolean _doubleWidth = false;
        private int _colSpan = 1;

        public EduProgramFilterItem(CommonFilterFormConfig config)
        {
            this.config = config;
            _enableCheckboxChecked = config.isEnableCheckboxChecked();
        }

        @Override
        public CommonFilterFormConfig getFormConfig()
        {
            return config;
        }

        @Override
        public String getSettingsName()
        {
            return SETTING_NAME_EDU_PROGRAM;
        }

        @Override
        public String getDisplayName()
        {
            return "Образовательная программа";
        }

        @Override
        public List<String> getDependentFields()
        {
            return null;
        }

        @Override
        public void setValue(Object value)
        {
            _value = value;
        }

        @Override
        public Object getValue()
        {
            return _value;
        }

        @Override
        public ISelectModel getModel()
        {
            return model;
        }

        @Override
        public void setModel(ISelectModel model)
        {
            this.model = model;
        }

        public boolean isEnableCheckboxChecked()
        {
            return _enableCheckboxChecked;
        }

        public void setEnableCheckboxChecked(boolean enableCheckboxChecked)
        {
            _enableCheckboxChecked = enableCheckboxChecked;
        }

        public boolean isFilterItemDisabled()
        {
            return config.isDisabled() || !isEnableCheckboxChecked();
        }

        @Override
        public boolean isNextWithNewLine() { return _nextWithNewLine; }
        @Override
        public ICommonFilterItem setNextWithNewLine(boolean nextWithNewLine) {_nextWithNewLine = nextWithNewLine; return this; }

        @Override
        public boolean isDoubleWidth() { return _doubleWidth; }
        @Override
        public ICommonFilterItem setDoubleWidth(boolean doubleWidth) { _doubleWidth = doubleWidth; return this; }

        @Override
        public int getColSpan() { return _colSpan; }
        @Override
        public ICommonFilterItem setColSpan(int colSpan) { _colSpan = colSpan; return this; }

        @Override
        public void apply(DQLSelectBuilder builder, String alias)
        {
            builder.where(or(
                    exists(EnrProgramSetItem.class,
                           EnrProgramSetItem.programSet().s(), property(alias, EnrCompetition.programSetOrgUnit().programSet()),
                           EnrProgramSetItem.program().s(), getValue()),
                    exists(EnrProgramSetSecondary.class,
                           EnrProgramSetSecondary.id().s(), property(alias, EnrCompetition.programSetOrgUnit().programSet()),
                           EnrProgramSetSecondary.program().s(), getValue())
            ));
        }

        private static final List<PropertyPath<String>> sortAndFilterProperties = Arrays.asList(
            EduProgramProf.programSubject().subjectCode(),
            EduProgramProf.title(),
            EduProgramProf.form().title(),
            EduProgramProf.duration().title(),
            EduProgramProf.eduProgramTrait().shortTitle(),
            EduProgramProf.institutionOrgUnit().orgUnit().shortTitle(),
            EduProgramProf.ownerOrgUnit().orgUnit().shortTitle());

        @Override
        public void init(final CommonFilterAddon commonFilterAddon, final String alias)
        {
            if (getModel() != null)
                return;

            setModel(new CommonFilterSelectModel()
            {
                @Override
                protected IListResultBuilder createBuilder(String filter, Object o)
                {
                    DQLSelectBuilder idBuilder = commonFilterAddon.prepareEntityBuilder(alias, EduProgramFilterItem.this);
                    idBuilder.column(property(EnrCompetition.programSetOrgUnit().programSet().id().fromAlias(alias)), "id");
                    idBuilder.predicate(DQLPredicateType.distinct);

                    String entityAlias = "entity";
                    DQLSelectBuilder builder = new DQLSelectBuilder()
                            .fromEntity(EduProgramProf.class, entityAlias)
                            .where(or(
                                exists(new DQLSelectBuilder()
                                    .fromEntity(EnrProgramSetItem.class, "psi")
                                    .joinDataSource("psi", DQLJoinType.inner, idBuilder.buildQuery(), "id", eq(property("psi", EnrProgramSetItem.programSet().id()), property("id.id")))
                                    .where(eq(property("psi", EnrProgramSetItem.program().id()), property(entityAlias, EduProgramProf.id())))
                                    .buildQuery()),
                                exists(new DQLSelectBuilder()
                                    .fromEntity(EnrProgramSetSecondary.class, "pss")
                                    .where(eq(property("pss", EnrProgramSetSecondary.program().id()), property(entityAlias, EduProgramProf.id())))
                                    .joinDataSource("pss", DQLJoinType.inner, idBuilder.buildQuery(), "id", eq(property("pss", EnrProgramSetSecondary.id()), property("id.id")))
                                    .buildQuery())))
                            .column(property(entityAlias))
                            .joinPath(DQLJoinType.left, EduProgramProf.eduProgramTrait().fromAlias(entityAlias), "tr")
                        ;

                    FilterUtils.applySelectFilter(builder, entityAlias, EduProgramProf.id(), o);

                    if (!StringUtils.isEmpty(filter))
                    {
                        /* введена подстрока */
                        List<IDQLExpression> concatExpressions = new ArrayList<>();
                        for (MetaDSLPath filterPropertyPath : sortAndFilterProperties)
                        {
                            IDQLExpression property = property(entityAlias, filterPropertyPath);
                            concatExpressions.add(filterPropertyPath.equals(EduProgramProf.eduProgramTrait().shortTitle()) ? coalesce(property, value("")) : property);
                        }

                        builder.where(likeUpper(DQLFunctions.concat(concatExpressions.toArray(new IDQLExpression[concatExpressions.size()])), value(CoreStringUtils.escapeLike(filter, true))));
                    }

                    /* сортировка по полям */
                    for (MetaDSLPath orderPropertyPath : sortAndFilterProperties)
                        builder.order(property(entityAlias, orderPropertyPath), OrderDirection.asc);

                    return new DQLListResultBuilder(builder);
                }

                @Override
                public String getLabelFor(Object value, int columnIndex)
                {
                    return (String) FastBeanUtils.getValue(value, EduProgramProf.titleWithCodeAndConditionsShortWithForm());
                }
            });
        }
    }
}