/* $Id$ */
// Copyright 2006-2012 Tandem Service Software
package ru.tandemservice.unienr14.component.catalog.enrScriptItem.EnrScriptItemPub;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.DisabledPropertyEntityHandler;
import org.tandemframework.core.view.list.column.EnabledPropertyEntityHandler;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultScriptCatalogPub.DefaultScriptCatalogPubController;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultScriptCatalogPub.DefaultScriptCatalogPubDAO;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultScriptCatalogPub.DefaultScriptCatalogPubModel;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultScriptCatalogPub.IDefaultScriptCatalogPubDAO;
import ru.tandemservice.unienr14.catalog.entity.IEnrScriptItem;

/**
 * @author Vasily Zhukov
 * @since 22.12.2011
 */

//public class DefaultScriptCatalogPubController<T extends ICatalogItem & IScriptItem, Model extends DefaultScriptCatalogPubModel<T>, IDAO extends IDefaultScriptCatalogPubDAO<T, Model>> extends DefaultCatalogPubController<T, Model, IDAO>

public class Controller<T extends ICatalogItem & IScriptItem & IEnrScriptItem, Model extends ru.tandemservice.unienr14.component.catalog.enrScriptItem.EnrScriptItemPub.Model<T>, IDAO extends ru.tandemservice.unienr14.component.catalog.enrScriptItem.EnrScriptItemPub.IDAO<T, Model>> extends DefaultScriptCatalogPubController<T, Model, IDAO>
{
    @Override
    protected DynamicListDataSource createListDataSource(IBusinessComponent context)
    {
        DynamicListDataSource dataSource = super.createListDataSource(context);
        int columnsSize = dataSource.getColumns().size();
        dataSource.addColumn(new ToggleColumn("Печать в pdf", IEnrScriptItem.PRINT_PDF)
                        .onLabel("Печатать в pdf")
                        .toggleOnListener("togglePrintPdfScript")
                        .offLabel("Печатать в rtf")
                        .toggleOffListener("togglePrintPdfScript").setDisableHandler(new EnabledPropertyEntityHandler(IEnrScriptItem.PRINTABLE_TO_PDF)), columnsSize - 1
        );

        return dataSource;
    }

    public void togglePrintPdfScript(IBusinessComponent context)
    {

        getDao().doTogglePrintPdfScript(context.<Long>getListenerParameter());
    }
}