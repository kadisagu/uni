package ru.tandemservice.unienr14.request.entity;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;

import ru.tandemservice.unienr14.catalog.entity.EnrBenefitCategory;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;

/**
 * @author vdanilov
 */
public interface IEnrEntrantBenefitStatement extends IEntity {

    public static final String P_BENEFIT_STATEMENT_TITLE = "benefitStatementTitle";

    @EntityDSLSupport
    String getBenefitStatementTitle();

    EnrBenefitCategory getBenefitCategory();

    void setBenefitCategory(EnrBenefitCategory benefitCategory);

    EnrEntrant getEntrant();

    EnrEntrantRequest getRequest();
}
