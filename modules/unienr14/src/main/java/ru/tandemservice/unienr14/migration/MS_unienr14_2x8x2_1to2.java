package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
public class MS_unienr14_2x8x2_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[] {
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrEnrollmentCampaignSettings

		// создано обязательное свойство contractEnrollmentAgreement
        if (!tool.columnExists("enr14_campaign_settings_t", "contractenrollmentagreement_p"))
		{
			// создать колонку
			tool.createColumn("enr14_campaign_settings_t", new DBColumn("contractenrollmentagreement_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			tool.executeUpdate("update enr14_campaign_settings_t set contractenrollmentagreement_p=? where contractenrollmentagreement_p is null", true);

			// сделать колонку NOT NULL
			tool.setColumnNullable("enr14_campaign_settings_t", "contractenrollmentagreement_p", false);
		}
    }
}