/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.ui.Pub;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.unienr14.catalog.entity.EnrTargetAdmissionKind;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.ui.CompetitionPub.EnrEnrollmentStepCompetitionPubUI;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepCompetitionPlan;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItem;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignTargetAdmissionKind;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 12/17/13
 */
public class EnrEnrollmentStepCompetitionDSHandler extends DefaultSearchDataSourceHandler
{
    public static final String VIEW_PROP_PLAN = "plan";

    public EnrEnrollmentStepCompetitionDSHandler(String ownerId)
    {
        super(ownerId);
    }

    public static final String BIND_ENR_STEP = "step";
    public static final String BIND_ENR_COMPETITION_UTIL = CommonFilterAddon.class.getSimpleName();

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context) {

        EnrEnrollmentStep step = context.get(BIND_ENR_STEP);
        CommonFilterAddon util = context.get(BIND_ENR_COMPETITION_UTIL);

        if (step.getEnrollmentCampaign().getSettings().isTargetAdmissionCompetition()) {

            final DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EnrEnrollmentStepItem.class, "i")
                .joinPath(DQLJoinType.inner, EnrEnrollmentStepItem.entity().fromAlias("i"), "r")
                .joinEntity("i", DQLJoinType.left, EnrRequestedCompetitionTA.class, "rta", eq(property("rta"), property(EnrRatingItem.requestedCompetition().fromAlias("r"))))
                .joinPath(DQLJoinType.left, EnrRequestedCompetitionTA.targetAdmissionKind().fromAlias("rta"), "eta")
                .joinPath(DQLJoinType.left, EnrCampaignTargetAdmissionKind.targetAdmissionKind().fromAlias("eta"), "ta")
                .where(eq(property(EnrEnrollmentStepItem.step().fromAlias("i")), value(step)))
                .column(property(EnrRequestedCompetition.competition().fromAlias("r")))
                .column("ta");

            if (util != null) dql.where(in(property("r", EnrRequestedCompetition.competition().id()), util.getEntityIdsFilteredBuilder("alias").buildQuery()));

            List<PairKey<EnrCompetition, EnrTargetAdmissionKind>> pairList = dql.createStatement(context.getSession()).<Object[]>list().stream()
                    .map(row -> new PairKey<>((EnrCompetition) row[0], (EnrTargetAdmissionKind) row[1]))
                    .distinct()
                    .sorted((o1, o2) -> {
                        if (o1.equals(o2)) return 0;
                        if (o1.getFirst().equals(o2.getFirst())) {
                            if (o1.getSecond() == null || o2.getSecond() == null) return 0;
                            return o1.getSecond().getTitle().compareTo(o2.getSecond().getTitle());
                        }
                        return o1.getFirst().getTitle().compareTo(o2.getFirst().getTitle());
                    })
                    .collect(Collectors.toList());

            DSOutput output = new DSOutput(input);
            output.setTotalSize(pairList.size());

            Map<Long, CompetitionWrapper> wrapperMap = new HashMap<>();
            List<Object> wrappers = new ArrayList<>();
            for (PairKey<EnrCompetition, EnrTargetAdmissionKind> pair : pairList.subList(output.getStartRecord(), Math.min(output.getStartRecord() + output.getCountRecord(), pairList.size()))) {
                CompetitionWrapper wrapper = new CompetitionWrapper(step, pair.getFirst(), pair.getSecond());
                wrappers.add(wrapper);
                wrapperMap.put(wrapper.getId(), wrapper);
            }
            calculatePlan(step, wrapperMap);
            output.setRecordList(wrappers);

            return output;
        } else {
            final DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EnrCompetition.class, "k")
                .column(property("k"))
                .where(exists(new DQLSelectBuilder()
                    .fromEntity(EnrEnrollmentStepItem.class, "i")
                    .where(eq(property(EnrEnrollmentStepItem.step().fromAlias("i")), commonValue(context.get(BIND_ENR_STEP))))
                    .where(eq(property(EnrEnrollmentStepItem.entity().requestedCompetition().competition().fromAlias("i")), property("k")))
                    .buildQuery()))
                .order(property(EnrCompetition.programSetOrgUnit().programSet().title().fromAlias("k")))
                .order(property(EnrCompetition.programSetOrgUnit().orgUnit().institutionOrgUnit().orgUnit().parent().fromAlias("k")), OrderDirection.desc)
                .order(property(EnrCompetition.programSetOrgUnit().orgUnit().institutionOrgUnit().orgUnit().territorialShortTitle().fromAlias("k")))
                .order(property(EnrCompetition.programSetOrgUnit().programSet().programForm().title().fromAlias("k")))
                .order(property(EnrCompetition.type().code().fromAlias("k")))
                .order(property(EnrCompetition.eduLevelRequirement().code().fromAlias("k")))
                .order(property(EnrCompetition.id().fromAlias("k")));

            if (util != null) dql.where(in(property("k", EnrCompetition.id()), util.getEntityIdsFilteredBuilder("alias").buildQuery()));

            DSOutput output = DQLSelectOutputBuilder.get(input, dql, context.getSession()).pageable(true).order().build();

            Map<Long, CompetitionWrapper> wrapperMap = new HashMap<>();
            List<Object> wrappers = new ArrayList<>();
            for (Object row : output.getRecordList()) {
                EnrCompetition competition = (EnrCompetition) row;
                CompetitionWrapper wrapper = new CompetitionWrapper(step, competition, null);
                wrappers.add(wrapper);
                wrapperMap.put(wrapper.getId(), wrapper);
            }
            calculatePlan(step, wrapperMap);
            output.setRecordList(wrappers);

            return output;
        }
    }

    private void calculatePlan(EnrEnrollmentStep step, Map<Long, CompetitionWrapper> wrappers)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(EnrEnrollmentStepCompetitionPlan.class, "plan")
            .where(eq(property("plan", EnrEnrollmentStepCompetitionPlan.enrollmentStep()), value(step)))
            .where(in(property("plan", EnrEnrollmentStepCompetitionPlan.competition().id()), wrappers.keySet()))
            ;
        for (EnrEnrollmentStepCompetitionPlan plan : DataAccessServices.dao().<EnrEnrollmentStepCompetitionPlan>getList(dql)) {
            CompetitionWrapper wrapper = wrappers.get(plan.getCompetition().getId());
            if (null != wrapper) wrapper.plan = plan.getPlan();
        }
    }

    public static class CompetitionWrapper extends IdentifiableWrapper {
        private static final long serialVersionUID = 1L;

        private EnrEnrollmentStep step;
        private EnrCompetition competition;
        private EnrTargetAdmissionKind targetAdmissionKind;
        private int plan;

        public CompetitionWrapper(EnrEnrollmentStep step, EnrCompetition competition, EnrTargetAdmissionKind targetAdmissionKind)
        {
            super(competition.getId(), competition.getTitle() + (targetAdmissionKind != null ? " (" + targetAdmissionKind.getTitle() + ")" : ""));
            this.step = step;
            this.competition = competition;
            this.targetAdmissionKind = targetAdmissionKind;
        }

        public Map getPublisherParameters() {
            ParametersMap parametersMap = new ParametersMap()
                .add(EnrEnrollmentStepCompetitionPubUI.PARAM_STEP, step.getId())
                .add(EnrEnrollmentStepCompetitionPubUI.PARAM_COMPETITION, competition.getId());
            if (targetAdmissionKind != null) {
                parametersMap.add(EnrEnrollmentStepCompetitionPubUI.PARAM_TARGET_ADMISSION_KIND, targetAdmissionKind.getId());
            }
            return parametersMap;
        }

        public int getPlan()
        {
            return plan;
        }
    }
}


