/* $Id:$ */
package ru.tandemservice.unienr14.sec.ext.Sec;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.organization.sec.bo.Sec.SecManager;
import org.tandemframework.shared.organization.sec.bo.Sec.logic.ISecLocalDescription;
import org.tandemframework.shared.organization.sec.bo.Sec.util.RoleConfigLogic;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.unienr14.catalog.entity.codes.LocalRoleScopeCodes;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.sec.bo.EnrSec.EnrSecManager;
import ru.tandemservice.unienr14.sec.entity.RoleAssignmentLocalEnrCampaign;
import ru.tandemservice.unienr14.sec.entity.RoleAssignmentLocalEnrCommission;
import ru.tandemservice.unienr14.sec.entity.RoleAssignmentTemplateEnrCampaign;
import ru.tandemservice.unienr14.sec.entity.RoleAssignmentTemplateEnrCommission;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.logic.EnrCampaignDao;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.ui.LocalRoleAssign.EnrEnrollmentCampaignLocalRoleAssign;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.ui.TemplateRoleAssign.EnrEnrollmentCampaignTemplateRoleAssign;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.logic.EnrEnrollmentCommissionDao;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.ui.LocalRoleAssign.EnrEnrollmentCommissionLocalRoleAssign;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.ui.TemplateRoleAssign.EnrEnrollmentCommissionTemplateRoleAssign;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

/**
 * @author oleyba
 * @since 5/7/15
 */
@Configuration
public class SecExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private SecManager secManager;

    @Bean
    public ItemListExtension<ISecLocalDescription> localDescriptionExtension()
    {
        return this.itemListExtension(secManager.localDescriptionExtPoint())
            .add("enrEntrant", new ISecLocalDescription<EnrEntrant>() {
                @Override public Collection<String> getLocalRoleScopes() {
                    return Arrays.asList(LocalRoleScopeCodes.ORG_UNIT, LocalRoleScopeCodes.ENROLLMENT_CAMPAIGN, LocalRoleScopeCodes.ENROLLMENT_COMMISSION);
                }

                @Override public Collection<IEntity> getLocalRoleContexts(EnrEntrant entity, String localRoleScope) {
                    return EnrSecManager.instance().dao().getLocalRoleContexts(entity, localRoleScope);
                }
            })
            .add("enrEntrantRequest", new ISecLocalDescription<EnrEntrantRequest>() {
                @Override public Collection<String> getLocalRoleScopes() {
                    return Arrays.asList(LocalRoleScopeCodes.ENROLLMENT_CAMPAIGN, LocalRoleScopeCodes.ENROLLMENT_COMMISSION);
                }

                @Override public Collection<IEntity> getLocalRoleContexts(EnrEntrantRequest entity, String localRoleScope) {
                    return EnrSecManager.instance().dao().getLocalRoleContexts(entity, localRoleScope);
                }
            })
            .add("enrRequestedCompetition", new ISecLocalDescription<EnrRequestedCompetition>() {
                @Override public Collection<String> getLocalRoleScopes() {
                    return Arrays.asList(LocalRoleScopeCodes.ORG_UNIT, LocalRoleScopeCodes.ENROLLMENT_CAMPAIGN, LocalRoleScopeCodes.ENROLLMENT_COMMISSION);
                }

                @Override public Collection<IEntity> getLocalRoleContexts(EnrRequestedCompetition entity, String localRoleScope) {
                    return EnrSecManager.instance().dao().getLocalRoleContexts(entity, localRoleScope);
                }
            })
                .add("enrEnrollmentCampaign", new ISecLocalDescription<EnrEnrollmentCampaign>() {
                    @Override public Collection<String> getLocalRoleScopes() {
                        return Collections.singletonList(LocalRoleScopeCodes.ENROLLMENT_CAMPAIGN);
                    }

                    @Override public Collection<IEntity> getLocalRoleContexts(EnrEnrollmentCampaign entity, String localRoleScope) {
                        return EnrSecManager.instance().dao().getLocalRoleContexts(entity, localRoleScope);
                    }
                })
                .add("enrEnrollmentCommission", new ISecLocalDescription<EnrEnrollmentCommission>() {
                    @Override public Collection<String> getLocalRoleScopes() {
                        return Collections.singletonList(LocalRoleScopeCodes.ENROLLMENT_COMMISSION);
                    }

                    @Override public Collection<IEntity> getLocalRoleContexts(EnrEnrollmentCommission entity, String localRoleScope) {
                        return EnrSecManager.instance().dao().getLocalRoleContexts(entity, localRoleScope);
                    }
                })
            .create();
    }

    @Bean
    public ItemListExtension<RoleConfigLogic> roleLogicExtension()
    {
        return this.itemListExtension(secManager.roleLogicExtPoint())
            .add("templateEnrCampaign", new RoleConfigLogic("templateEnrCampaign", EnrEnrollmentCampaignTemplateRoleAssign.class)
                .source(EnrCampaignDao.RA_SRC_TEMPLATE_ENR_CAMPAIGN)
                .dependOn(RoleAssignmentTemplateEnrCampaign.class)
            )
            .add("localEnrCampaign", new RoleConfigLogic("localEnrCampaign", EnrEnrollmentCampaignLocalRoleAssign.class)
                .source(EnrCampaignDao.RA_SRC_LOCAL_ENR_CAMPAIGN)
                .dependOn(RoleAssignmentLocalEnrCampaign.class)
            )
            .add("templateEnrCommission", new RoleConfigLogic("templateEnrCommission", EnrEnrollmentCommissionTemplateRoleAssign.class)
                .source(EnrEnrollmentCommissionDao.RA_SRC_TEMPLATE_ENR_COMMISSION)
                .dependOn(RoleAssignmentTemplateEnrCommission.class)
            )
            .add("localEnrCommission", new RoleConfigLogic("localEnrCommission", EnrEnrollmentCommissionLocalRoleAssign.class)
                .source(EnrEnrollmentCommissionDao.RA_SRC_LOCAL_ENR_COMMISSION)
                .dependOn(RoleAssignmentLocalEnrCommission.class)
            )
            .create();
    }
}
