/* $Id$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.util;

import ru.tandemservice.unienr14.catalog.entity.EnrStateExamSubject;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult;

import java.util.Comparator;

/**
 * @author nvankov
 * @since 6/10/14
 */
public class EnrEntrantStateExamVO
{
    public static final Comparator<EnrEntrantStateExamVO> COMPARATOR = new Comparator<EnrEntrantStateExamVO>() {
        @Override public int compare(EnrEntrantStateExamVO o1, EnrEntrantStateExamVO o2) {
            if(o1.getSubject().equals(o2.getSubject())) {
                return Integer.compare(o1.getResult().getYear(), o2.getResult().getYear());
            } else {
                return o1.getSubject().getTitle().compareTo(o2.getSubject().getTitle());
            }
        }
    };

    private boolean _selected = false;
    private EnrStateExamSubject _subject;
    private EnrEntrantStateExamResult _result;
    private boolean _createdInCurrentWizard;

    public EnrEntrantStateExamVO(boolean selected, EnrStateExamSubject subject, EnrEntrantStateExamResult result, boolean createdInCurrentWizard)
    {
        _selected = selected;
        _subject = subject;
        _result = result;
        _createdInCurrentWizard = createdInCurrentWizard;
    }

    public EnrStateExamSubject getSubject()
    {
        return _subject;
    }

    public EnrEntrantStateExamResult getResult()
    {
        return _result;
    }

    public boolean isCreatedInCurrentWizard()
    {
        return _createdInCurrentWizard;
    }

    public boolean isDisabled()
    {
        return !isCreatedInCurrentWizard();
    }

    public boolean isDisabledValue()
    {
        return !isSelected() || isDisabled();
    }

    public boolean isSelected()
    {
        return _selected;
    }

    public void setSelected(boolean selected)
    {
        _selected = selected;
    }

    public boolean isSecondWaveExamPlaceDisabled()
    {
        return isDisabledValue() || !_result.isSecondWave();
    }

    public boolean isDisabledYearValue()
    {
        return isDisabledValue() || _result.getId() != null;
    }
}
