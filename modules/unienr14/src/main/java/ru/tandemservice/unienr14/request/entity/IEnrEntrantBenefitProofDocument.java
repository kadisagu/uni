package ru.tandemservice.unienr14.request.entity;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.person.catalog.entity.PersonDocumentType;
import ru.tandemservice.unienr14.fis.IFisUidByIdOwner;

import java.util.Date;

/**
 * @author vdanilov
 */
public interface IEnrEntrantBenefitProofDocument extends IEntity, ITitled, IFisUidByIdOwner
{
    public static final String DISPLAYABLE_TITLE = "displayableTitle";

    /** @return тип документа */
    PersonDocumentType getDocumentType();

    /** @return дата регистрации документа в системе */
    Date getRegistrationDate();

    /** @return Отображаемое название */
    String getDisplayableTitle();

    /** @return title + дополнительная информация (если есть) */
    String getExtendedTitle();

    String getSeria();

    String getNumber();

    Date getIssuanceDate();

    String getIssuancePlace();

    String getDescription();
}
