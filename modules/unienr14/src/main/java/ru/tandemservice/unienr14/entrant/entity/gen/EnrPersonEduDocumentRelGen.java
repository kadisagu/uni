package ru.tandemservice.unienr14.entrant.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import org.tandemframework.shared.person.catalog.entity.PersonDocumentType;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrPersonEduDocumentRel;
import ru.tandemservice.unienr14.entrant.entity.IEnrEntrantAchievementProofDocument;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь абитуриента с документом об образовании для ИД
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrPersonEduDocumentRelGen extends EntityBase
 implements IEnrEntrantAchievementProofDocument, INaturalIdentifiable<EnrPersonEduDocumentRelGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.entrant.entity.EnrPersonEduDocumentRel";
    public static final String ENTITY_NAME = "enrPersonEduDocumentRel";
    public static final int VERSION_HASH = 1742256626;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTRANT = "entrant";
    public static final String L_EDU_DOCUMENT = "eduDocument";
    public static final String L_DOCUMENT_TYPE = "documentType";
    public static final String P_REGISTRATION_DATE = "registrationDate";
    public static final String P_SERIA = "seria";
    public static final String P_NUMBER = "number";
    public static final String P_ISSUANCE_DATE = "issuanceDate";
    public static final String P_ISSUANCE_PLACE = "issuancePlace";

    private EnrEntrant _entrant;     // Абитуриент
    private PersonEduDocument _eduDocument;     // Документ об образовании
    private PersonDocumentType _documentType;     // Тип документа
    private Date _registrationDate;     // Дата добавления
    private String _seria;     // Серия
    private String _number;     // Номер
    private Date _issuanceDate;     // Дата выдачи
    private String _issuancePlace;     // Кем выдан

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     */
    @NotNull
    public EnrEntrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant Абитуриент. Свойство не может быть null.
     */
    public void setEntrant(EnrEntrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    /**
     * @return Документ об образовании. Свойство не может быть null.
     */
    @NotNull
    public PersonEduDocument getEduDocument()
    {
        return _eduDocument;
    }

    /**
     * @param eduDocument Документ об образовании. Свойство не может быть null.
     */
    public void setEduDocument(PersonEduDocument eduDocument)
    {
        dirty(_eduDocument, eduDocument);
        _eduDocument = eduDocument;
    }

    /**
     * @return Тип документа. Свойство не может быть null.
     */
    @NotNull
    public PersonDocumentType getDocumentType()
    {
        return _documentType;
    }

    /**
     * @param documentType Тип документа. Свойство не может быть null.
     */
    public void setDocumentType(PersonDocumentType documentType)
    {
        dirty(_documentType, documentType);
        _documentType = documentType;
    }

    /**
     * @return Дата добавления.
     *
     * Это формула "eduDocument.creationDate".
     */
    public Date getRegistrationDate()
    {
        return _registrationDate;
    }

    /**
     * @param registrationDate Дата добавления.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setRegistrationDate(Date registrationDate)
    {
        dirty(_registrationDate, registrationDate);
        _registrationDate = registrationDate;
    }

    /**
     * @return Серия.
     *
     * Это формула "eduDocument.seria".
     */
    // @Length(max=255)
    public String getSeria()
    {
        return _seria;
    }

    /**
     * @param seria Серия.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setSeria(String seria)
    {
        dirty(_seria, seria);
        _seria = seria;
    }

    /**
     * @return Номер.
     *
     * Это формула "eduDocument.number".
     */
    // @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Дата выдачи.
     *
     * Это формула "eduDocument.issuanceDate".
     */
    public Date getIssuanceDate()
    {
        return _issuanceDate;
    }

    /**
     * @param issuanceDate Дата выдачи.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setIssuanceDate(Date issuanceDate)
    {
        dirty(_issuanceDate, issuanceDate);
        _issuanceDate = issuanceDate;
    }

    /**
     * @return Кем выдан.
     *
     * Это формула "eduDocument.eduOrganization".
     */
    // @Length(max=255)
    public String getIssuancePlace()
    {
        return _issuancePlace;
    }

    /**
     * @param issuancePlace Кем выдан.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setIssuancePlace(String issuancePlace)
    {
        dirty(_issuancePlace, issuancePlace);
        _issuancePlace = issuancePlace;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrPersonEduDocumentRelGen)
        {
            if (withNaturalIdProperties)
            {
                setEntrant(((EnrPersonEduDocumentRel)another).getEntrant());
                setEduDocument(((EnrPersonEduDocumentRel)another).getEduDocument());
            }
            setDocumentType(((EnrPersonEduDocumentRel)another).getDocumentType());
            setRegistrationDate(((EnrPersonEduDocumentRel)another).getRegistrationDate());
            setSeria(((EnrPersonEduDocumentRel)another).getSeria());
            setNumber(((EnrPersonEduDocumentRel)another).getNumber());
            setIssuanceDate(((EnrPersonEduDocumentRel)another).getIssuanceDate());
            setIssuancePlace(((EnrPersonEduDocumentRel)another).getIssuancePlace());
        }
    }

    public INaturalId<EnrPersonEduDocumentRelGen> getNaturalId()
    {
        return new NaturalId(getEntrant(), getEduDocument());
    }

    public static class NaturalId extends NaturalIdBase<EnrPersonEduDocumentRelGen>
    {
        private static final String PROXY_NAME = "EnrPersonEduDocumentRelNaturalProxy";

        private Long _entrant;
        private Long _eduDocument;

        public NaturalId()
        {}

        public NaturalId(EnrEntrant entrant, PersonEduDocument eduDocument)
        {
            _entrant = ((IEntity) entrant).getId();
            _eduDocument = ((IEntity) eduDocument).getId();
        }

        public Long getEntrant()
        {
            return _entrant;
        }

        public void setEntrant(Long entrant)
        {
            _entrant = entrant;
        }

        public Long getEduDocument()
        {
            return _eduDocument;
        }

        public void setEduDocument(Long eduDocument)
        {
            _eduDocument = eduDocument;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrPersonEduDocumentRelGen.NaturalId) ) return false;

            EnrPersonEduDocumentRelGen.NaturalId that = (NaturalId) o;

            if( !equals(getEntrant(), that.getEntrant()) ) return false;
            if( !equals(getEduDocument(), that.getEduDocument()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEntrant());
            result = hashCode(result, getEduDocument());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEntrant());
            sb.append("/");
            sb.append(getEduDocument());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrPersonEduDocumentRelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrPersonEduDocumentRel.class;
        }

        public T newInstance()
        {
            return (T) new EnrPersonEduDocumentRel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entrant":
                    return obj.getEntrant();
                case "eduDocument":
                    return obj.getEduDocument();
                case "documentType":
                    return obj.getDocumentType();
                case "registrationDate":
                    return obj.getRegistrationDate();
                case "seria":
                    return obj.getSeria();
                case "number":
                    return obj.getNumber();
                case "issuanceDate":
                    return obj.getIssuanceDate();
                case "issuancePlace":
                    return obj.getIssuancePlace();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entrant":
                    obj.setEntrant((EnrEntrant) value);
                    return;
                case "eduDocument":
                    obj.setEduDocument((PersonEduDocument) value);
                    return;
                case "documentType":
                    obj.setDocumentType((PersonDocumentType) value);
                    return;
                case "registrationDate":
                    obj.setRegistrationDate((Date) value);
                    return;
                case "seria":
                    obj.setSeria((String) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "issuanceDate":
                    obj.setIssuanceDate((Date) value);
                    return;
                case "issuancePlace":
                    obj.setIssuancePlace((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entrant":
                        return true;
                case "eduDocument":
                        return true;
                case "documentType":
                        return true;
                case "registrationDate":
                        return true;
                case "seria":
                        return true;
                case "number":
                        return true;
                case "issuanceDate":
                        return true;
                case "issuancePlace":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entrant":
                    return true;
                case "eduDocument":
                    return true;
                case "documentType":
                    return true;
                case "registrationDate":
                    return true;
                case "seria":
                    return true;
                case "number":
                    return true;
                case "issuanceDate":
                    return true;
                case "issuancePlace":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entrant":
                    return EnrEntrant.class;
                case "eduDocument":
                    return PersonEduDocument.class;
                case "documentType":
                    return PersonDocumentType.class;
                case "registrationDate":
                    return Date.class;
                case "seria":
                    return String.class;
                case "number":
                    return String.class;
                case "issuanceDate":
                    return Date.class;
                case "issuancePlace":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrPersonEduDocumentRel> _dslPath = new Path<EnrPersonEduDocumentRel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrPersonEduDocumentRel");
    }
            

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrPersonEduDocumentRel#getEntrant()
     */
    public static EnrEntrant.Path<EnrEntrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return Документ об образовании. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrPersonEduDocumentRel#getEduDocument()
     */
    public static PersonEduDocument.Path<PersonEduDocument> eduDocument()
    {
        return _dslPath.eduDocument();
    }

    /**
     * @return Тип документа. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrPersonEduDocumentRel#getDocumentType()
     */
    public static PersonDocumentType.Path<PersonDocumentType> documentType()
    {
        return _dslPath.documentType();
    }

    /**
     * @return Дата добавления.
     *
     * Это формула "eduDocument.creationDate".
     * @see ru.tandemservice.unienr14.entrant.entity.EnrPersonEduDocumentRel#getRegistrationDate()
     */
    public static PropertyPath<Date> registrationDate()
    {
        return _dslPath.registrationDate();
    }

    /**
     * @return Серия.
     *
     * Это формула "eduDocument.seria".
     * @see ru.tandemservice.unienr14.entrant.entity.EnrPersonEduDocumentRel#getSeria()
     */
    public static PropertyPath<String> seria()
    {
        return _dslPath.seria();
    }

    /**
     * @return Номер.
     *
     * Это формула "eduDocument.number".
     * @see ru.tandemservice.unienr14.entrant.entity.EnrPersonEduDocumentRel#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Дата выдачи.
     *
     * Это формула "eduDocument.issuanceDate".
     * @see ru.tandemservice.unienr14.entrant.entity.EnrPersonEduDocumentRel#getIssuanceDate()
     */
    public static PropertyPath<Date> issuanceDate()
    {
        return _dslPath.issuanceDate();
    }

    /**
     * @return Кем выдан.
     *
     * Это формула "eduDocument.eduOrganization".
     * @see ru.tandemservice.unienr14.entrant.entity.EnrPersonEduDocumentRel#getIssuancePlace()
     */
    public static PropertyPath<String> issuancePlace()
    {
        return _dslPath.issuancePlace();
    }

    public static class Path<E extends EnrPersonEduDocumentRel> extends EntityPath<E>
    {
        private EnrEntrant.Path<EnrEntrant> _entrant;
        private PersonEduDocument.Path<PersonEduDocument> _eduDocument;
        private PersonDocumentType.Path<PersonDocumentType> _documentType;
        private PropertyPath<Date> _registrationDate;
        private PropertyPath<String> _seria;
        private PropertyPath<String> _number;
        private PropertyPath<Date> _issuanceDate;
        private PropertyPath<String> _issuancePlace;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrPersonEduDocumentRel#getEntrant()
     */
        public EnrEntrant.Path<EnrEntrant> entrant()
        {
            if(_entrant == null )
                _entrant = new EnrEntrant.Path<EnrEntrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return Документ об образовании. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrPersonEduDocumentRel#getEduDocument()
     */
        public PersonEduDocument.Path<PersonEduDocument> eduDocument()
        {
            if(_eduDocument == null )
                _eduDocument = new PersonEduDocument.Path<PersonEduDocument>(L_EDU_DOCUMENT, this);
            return _eduDocument;
        }

    /**
     * @return Тип документа. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrPersonEduDocumentRel#getDocumentType()
     */
        public PersonDocumentType.Path<PersonDocumentType> documentType()
        {
            if(_documentType == null )
                _documentType = new PersonDocumentType.Path<PersonDocumentType>(L_DOCUMENT_TYPE, this);
            return _documentType;
        }

    /**
     * @return Дата добавления.
     *
     * Это формула "eduDocument.creationDate".
     * @see ru.tandemservice.unienr14.entrant.entity.EnrPersonEduDocumentRel#getRegistrationDate()
     */
        public PropertyPath<Date> registrationDate()
        {
            if(_registrationDate == null )
                _registrationDate = new PropertyPath<Date>(EnrPersonEduDocumentRelGen.P_REGISTRATION_DATE, this);
            return _registrationDate;
        }

    /**
     * @return Серия.
     *
     * Это формула "eduDocument.seria".
     * @see ru.tandemservice.unienr14.entrant.entity.EnrPersonEduDocumentRel#getSeria()
     */
        public PropertyPath<String> seria()
        {
            if(_seria == null )
                _seria = new PropertyPath<String>(EnrPersonEduDocumentRelGen.P_SERIA, this);
            return _seria;
        }

    /**
     * @return Номер.
     *
     * Это формула "eduDocument.number".
     * @see ru.tandemservice.unienr14.entrant.entity.EnrPersonEduDocumentRel#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(EnrPersonEduDocumentRelGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Дата выдачи.
     *
     * Это формула "eduDocument.issuanceDate".
     * @see ru.tandemservice.unienr14.entrant.entity.EnrPersonEduDocumentRel#getIssuanceDate()
     */
        public PropertyPath<Date> issuanceDate()
        {
            if(_issuanceDate == null )
                _issuanceDate = new PropertyPath<Date>(EnrPersonEduDocumentRelGen.P_ISSUANCE_DATE, this);
            return _issuanceDate;
        }

    /**
     * @return Кем выдан.
     *
     * Это формула "eduDocument.eduOrganization".
     * @see ru.tandemservice.unienr14.entrant.entity.EnrPersonEduDocumentRel#getIssuancePlace()
     */
        public PropertyPath<String> issuancePlace()
        {
            if(_issuancePlace == null )
                _issuancePlace = new PropertyPath<String>(EnrPersonEduDocumentRelGen.P_ISSUANCE_PLACE, this);
            return _issuancePlace;
        }

        public Class getEntityClass()
        {
            return EnrPersonEduDocumentRel.class;
        }

        public String getEntityName()
        {
            return "enrPersonEduDocumentRel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
