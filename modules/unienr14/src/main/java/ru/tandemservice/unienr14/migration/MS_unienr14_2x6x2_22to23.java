package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x2_22to23 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrEnrollmentParagraph

		// создано обязательное свойство enrOrgUnit
		{
			// создать колонку
			tool.createColumn("enr14_order_enr_par_t", new DBColumn("enrorgunit_id", DBType.LONG));
			tool.setColumnNullable("enr14_order_enr_par_t", "enrorgunit_id", false);

		}

		// создано обязательное свойство formativeOrgUnit
		{
			// создать колонку
			tool.createColumn("enr14_order_enr_par_t", new DBColumn("formativeorgunit_id", DBType.LONG));
			tool.setColumnNullable("enr14_order_enr_par_t", "formativeorgunit_id", false);

		}

		// создано обязательное свойство programSubject
		{
			// создать колонку
			tool.createColumn("enr14_order_enr_par_t", new DBColumn("programsubject_id", DBType.LONG));
			tool.setColumnNullable("enr14_order_enr_par_t", "programsubject_id", false);

		}

		// создано обязательное свойство competitionType
		{
			// создать колонку
			tool.createColumn("enr14_order_enr_par_t", new DBColumn("competitiontype_id", DBType.LONG));
			tool.setColumnNullable("enr14_order_enr_par_t", "competitiontype_id", false);

		}

		// создано обязательное свойство parallel
		{
			// создать колонку
			tool.createColumn("enr14_order_enr_par_t", new DBColumn("parallel_p", DBType.BOOLEAN));
			tool.setColumnNullable("enr14_order_enr_par_t", "parallel_p", false);

		}


    }
}