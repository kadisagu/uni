/**
 *$Id$
 */
package ru.tandemservice.unienr14.debug.bo.EnrDebug.ui.EnrollmentCampaignRandomAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.event.IEventServiceLock;
import ru.tandemservice.unienr14.debug.bo.EnrDebug.EnrDebugManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

/**
 * @author Alexander Zhebko
 * @since 17.03.2014
 */
public class EnrDebugEnrollmentCampaignRandomAddUI extends UIPresenter
{
    private EnrEnrollmentCampaign _enrollmentCampaign;
    public EnrEnrollmentCampaign getEnrollmentCampaign(){ return _enrollmentCampaign; }
    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign){ _enrollmentCampaign = enrollmentCampaign; }

    public void onClickApply()
    {
        final IEventServiceLock eventLock = CoreServices.eventService().lock();
        try { EnrDebugManager.instance().dao().doFullEcRandom(getEnrollmentCampaign()); }
        finally { eventLock.release(); }

        deactivate();
    }
}