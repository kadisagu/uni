/* $Id$ */
package ru.tandemservice.unienr14.order.bo.EnrEnrollmentMinisteryParagraph.ui.AddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.PageableSearchListDataSource;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.order.bo.EnrEnrollmentMinisteryParagraph.EnrEnrollmentMinisteryParagraphManager;
import ru.tandemservice.unienr14.order.entity.*;
import ru.tandemservice.unienr14.request.entity.EnrEntrantForeignRequest;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unimove.UnimoveDefines;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author nvankov
 * @since 9/1/14
 */
@Input({
        @Bind(key = EnrEnrollmentMinisteryParagraphAddEditUI.PARAMETER_ORDER_ID, binding = EnrEnrollmentMinisteryParagraphAddEditUI.PARAMETER_ORDER_ID),
        @Bind(key = EnrEnrollmentMinisteryParagraphAddEditUI.PARAMETER_PARAGRAPH_ID, binding = EnrEnrollmentMinisteryParagraphAddEditUI.PARAMETER_PARAGRAPH_ID)
})
public class EnrEnrollmentMinisteryParagraphAddEditUI extends UIPresenter
{
    public static final String PARAMETER_ORDER_ID = "order.id";
    public static final String PARAMETER_PARAGRAPH_ID = "paragraph.id";

    private EnrOrder _order = new EnrOrder();
    private EnrEnrollmentMinisteryParagraph _paragraph = new EnrEnrollmentMinisteryParagraph();
    private boolean _editForm;

    // выбор конкурсов
    private EnrProgramSetBase _programSet;
    private EnrCompetition _competition;

    private Set<Long> _selectedEntrantIds = new HashSet<>();

    @Override
    public void onComponentRefresh()
    {
        if (getParagraph().getId() == null)
        {
            // создание параграфа
            setEditForm(false);
            setOrder(DataAccessServices.dao().getNotNull(EnrOrder.class, getOrder().getId()));
            getParagraph().setOrder(getOrder());
        }
        else
        {
            // редактирование параграфа
            setEditForm(true);
            setParagraph(DataAccessServices.dao().getNotNull(EnrEnrollmentMinisteryParagraph.class, getParagraph().getId()));
            setOrder((EnrOrder) getParagraph().getOrder());

            for (EnrEnrollmentMinisteryExtract extract : IUniBaseDao.instance.get().getList(EnrEnrollmentMinisteryExtract.class, EnrEnrollmentMinisteryExtract.paragraph(), getParagraph(), EnrEnrollmentMinisteryExtract.number().s()))
            {
                getSelectedEntrantIds().add(extract.getEntity().getId());                
            }            
        }
        if (!UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(getOrder().getState().getCode())) {
            throw new ApplicationException("Приказ находится в состоянии «" + getOrder().getState().getTitle() +"», редактирование параграфов запрещено.");
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrEnrollmentMinisteryParagraphAddEdit.BIND_PARAGRAPH, getParagraph());        
        dataSource.put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, getOrder().getEnrollmentCampaign());
        dataSource.put(EnrEnrollmentMinisteryParagraphAddEdit.BIND_REQUEST_TYPE, getOrder().getRequestType());
        dataSource.put(EnrEnrollmentMinisteryParagraphAddEdit.BIND_COMPENSATION_TYPE, getOrder().getCompensationType());
        dataSource.put(EnrEnrollmentMinisteryParagraphAddEdit.BIND_FORM, getParagraph().getProgramForm());
        dataSource.put(EnrEnrollmentMinisteryParagraphAddEdit.BIND_ORG_UNIT, getParagraph().getEnrOrgUnit());
        dataSource.put(EnrEnrollmentMinisteryParagraphAddEdit.BIND_FORMATIVE_ORG_UNIT, getParagraph().getFormativeOrgUnit());
        dataSource.put(EnrEnrollmentMinisteryParagraphAddEdit.BIND_SUBJECT, getParagraph().getProgramSubject());
        dataSource.put(EnrEnrollmentMinisteryParagraphAddEdit.BIND_PROGRAM_SET, getProgramSet());
        dataSource.put(EnrEnrollmentMinisteryParagraphAddEdit.BIND_COMPETITION_TYPE, getParagraph().getCompetitionType());
        dataSource.put(EnrEnrollmentMinisteryParagraphAddEdit.BIND_COMPETITION, getCompetition());
    }

    @Override
    @SuppressWarnings("unchecked")
    public void onAfterDataSourceFetch(IUIDataSource dataSource)
    {
        if (isEditForm() && "entrantDS".equals(dataSource.getName()))
        {
            DynamicListDataSource ds = ((PageableSearchListDataSource) dataSource).getLegacyDataSource();

            List<IEntity> checkboxColumnSelected = new ArrayList<IEntity>();


            for (IEntity record : (List<IEntity>) ds.getEntityList())
            {
                if (getSelectedEntrantIds().contains(record.getId()))
                    checkboxColumnSelected.add(record);
            }

            CheckboxColumn checkboxColumn = ((CheckboxColumn) ds.getColumn(EnrEnrollmentMinisteryParagraphAddEdit.CHECKBOX_COLUMN));
            if (checkboxColumn != null)
                checkboxColumn.setSelectedObjects(checkboxColumnSelected);
        }
    }


    // Listeners
    public void onRefreshSelectionParams() {
        // ?
    }

    public void onClickApply()
    {
        try {

            DynamicListDataSource ds = ((PageableSearchListDataSource) _uiConfig.getDataSource("entrantDS")).getLegacyDataSource();
            CheckboxColumn checkboxColumn = ((CheckboxColumn) ds.getColumn(EnrEnrollmentMinisteryParagraphAddEdit.CHECKBOX_COLUMN));

            List<EnrEntrantForeignRequest> preStudents = new ArrayList<EnrEntrantForeignRequest>();
            if (checkboxColumn != null) {
                for (IEntity entity : checkboxColumn.getSelectedObjects()) {
                    preStudents.add((EnrEntrantForeignRequest) ((DataWrapper) entity).getWrapped());
                    getSelectedEntrantIds().add(entity.getId());
                }
            }

            // сохраняем или обновляем параграф о зачислении
            EnrEnrollmentMinisteryParagraphManager.instance().dao().saveOrUpdateParagraph(_order, _paragraph, preStudents);

            // закрываем форму
            deactivate();

        } catch (Throwable t) {
            Debug.exception(t);
            _uiSupport.setRefreshScheduled(true);
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    // presenter

    public String getFormTitle() {
        String formTitle = (StringUtils.isNotEmpty(_order.getNumber()) ? "№" + _order.getNumber() + " " : "") + (_order.getCommitDate() != null ? "от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(_order.getCommitDate()) : "");
        if (isEditForm())
            formTitle = "Редактирование параграфа из приказа о зачислении по направлению Минобрнауки " + formTitle;
        else
            formTitle = "Добавление параграфа приказа о зачислении по направлению Минобрнауки " + formTitle;
        return formTitle;
    }

    // Getters & Setters

    public boolean isShowAgreeEnrollContract()
    {
        return CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT.equals(getOrder().getCompensationType().getCode());
    }

    public EnrOrder getOrder()
    {
        return _order;
    }

    public void setOrder(EnrOrder order)
    {
        _order = order;
    }

    public EnrEnrollmentMinisteryParagraph getParagraph()
    {
        return _paragraph;
    }

    public void setParagraph(EnrEnrollmentMinisteryParagraph paragraph)
    {
        _paragraph = paragraph;
    }

    public boolean isEditForm()
    {
        return _editForm;
    }

    public void setEditForm(boolean editForm)
    {
        _editForm = editForm;
    }

    public Set<Long> getSelectedEntrantIds()
    {
        return _selectedEntrantIds;
    }

    public void setSelectedEntrantIds(Set<Long> selectedEntrantIds)
    {
        _selectedEntrantIds = selectedEntrantIds;
    }

    public EnrProgramSetBase getProgramSet()
    {
        return _programSet;
    }

    public void setProgramSet(EnrProgramSetBase programSet)
    {
        _programSet = programSet;
    }

    public EnrCompetition getCompetition()
    {
        return _competition;
    }

    public void setCompetition(EnrCompetition competition)
    {
        _competition = competition;
    }
}
