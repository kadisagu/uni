/**
 *$Id: EnrRequestedDirectionPub.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.request.bo.EnrRequestedCompetition.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author Alexander Shaburov
 * @since 13.05.13
 */
@Configuration
public class EnrRequestedCompetitionPub extends BusinessComponentManager
{
    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .create();
    }
}
