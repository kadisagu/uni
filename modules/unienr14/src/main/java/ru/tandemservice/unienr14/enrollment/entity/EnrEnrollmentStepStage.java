package ru.tandemservice.unienr14.enrollment.entity;

import ru.tandemservice.unienr14.enrollment.entity.gen.*;

/**
 * Стадия зачисления, включенная в шаг
 *
 * Фиксирует стадии зачисления, включенные в данный шаг зачисления.
 */
public class EnrEnrollmentStepStage extends EnrEnrollmentStepStageGen
{
}