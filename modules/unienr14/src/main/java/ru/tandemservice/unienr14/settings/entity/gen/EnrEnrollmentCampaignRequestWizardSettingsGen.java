package ru.tandemservice.unienr14.settings.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignRequestWizardSettings;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Настройки мастера добавления заявления приемной кампании
 *
 * Хранит настройки полей, шагов и проверок для мастера добавления заявления в приемной кампании.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrEnrollmentCampaignRequestWizardSettingsGen extends EntityBase
 implements INaturalIdentifiable<EnrEnrollmentCampaignRequestWizardSettingsGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignRequestWizardSettings";
    public static final String ENTITY_NAME = "enrEnrollmentCampaignRequestWizardSettings";
    public static final int VERSION_HASH = 352364880;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_MAIN_DATA_STEP_VISIBLE = "mainDataStepVisible";
    public static final String P_CONTACTS_STEP_VISIBLE = "contactsStepVisible";
    public static final String P_NEXT_OF_KIN_STEP_VISIBLE = "nextOfKinStepVisible";
    public static final String P_MILITARY_STEP_VISIBLE = "militaryStepVisible";

    private EnrEnrollmentCampaign _enrollmentCampaign;     // ПК
    private boolean _mainDataStepVisible = true;     // Отображать шаг «Основные данные»
    private boolean _contactsStepVisible = true;     // Отображать шаг «Контактная информация»
    private boolean _nextOfKinStepVisible = true;     // Отображать шаг «Ближайшие родственники»
    private boolean _militaryStepVisible = true;     // Отображать шаг «Служба в армии»

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return ПК. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign ПК. Свойство не может быть null и должно быть уникальным.
     */
    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Отображать шаг «Основные данные». Свойство не может быть null.
     */
    @NotNull
    public boolean isMainDataStepVisible()
    {
        return _mainDataStepVisible;
    }

    /**
     * @param mainDataStepVisible Отображать шаг «Основные данные». Свойство не может быть null.
     */
    public void setMainDataStepVisible(boolean mainDataStepVisible)
    {
        dirty(_mainDataStepVisible, mainDataStepVisible);
        _mainDataStepVisible = mainDataStepVisible;
    }

    /**
     * @return Отображать шаг «Контактная информация». Свойство не может быть null.
     */
    @NotNull
    public boolean isContactsStepVisible()
    {
        return _contactsStepVisible;
    }

    /**
     * @param contactsStepVisible Отображать шаг «Контактная информация». Свойство не может быть null.
     */
    public void setContactsStepVisible(boolean contactsStepVisible)
    {
        dirty(_contactsStepVisible, contactsStepVisible);
        _contactsStepVisible = contactsStepVisible;
    }

    /**
     * @return Отображать шаг «Ближайшие родственники». Свойство не может быть null.
     */
    @NotNull
    public boolean isNextOfKinStepVisible()
    {
        return _nextOfKinStepVisible;
    }

    /**
     * @param nextOfKinStepVisible Отображать шаг «Ближайшие родственники». Свойство не может быть null.
     */
    public void setNextOfKinStepVisible(boolean nextOfKinStepVisible)
    {
        dirty(_nextOfKinStepVisible, nextOfKinStepVisible);
        _nextOfKinStepVisible = nextOfKinStepVisible;
    }

    /**
     * @return Отображать шаг «Служба в армии». Свойство не может быть null.
     */
    @NotNull
    public boolean isMilitaryStepVisible()
    {
        return _militaryStepVisible;
    }

    /**
     * @param militaryStepVisible Отображать шаг «Служба в армии». Свойство не может быть null.
     */
    public void setMilitaryStepVisible(boolean militaryStepVisible)
    {
        dirty(_militaryStepVisible, militaryStepVisible);
        _militaryStepVisible = militaryStepVisible;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrEnrollmentCampaignRequestWizardSettingsGen)
        {
            if (withNaturalIdProperties)
            {
                setEnrollmentCampaign(((EnrEnrollmentCampaignRequestWizardSettings)another).getEnrollmentCampaign());
            }
            setMainDataStepVisible(((EnrEnrollmentCampaignRequestWizardSettings)another).isMainDataStepVisible());
            setContactsStepVisible(((EnrEnrollmentCampaignRequestWizardSettings)another).isContactsStepVisible());
            setNextOfKinStepVisible(((EnrEnrollmentCampaignRequestWizardSettings)another).isNextOfKinStepVisible());
            setMilitaryStepVisible(((EnrEnrollmentCampaignRequestWizardSettings)another).isMilitaryStepVisible());
        }
    }

    public INaturalId<EnrEnrollmentCampaignRequestWizardSettingsGen> getNaturalId()
    {
        return new NaturalId(getEnrollmentCampaign());
    }

    public static class NaturalId extends NaturalIdBase<EnrEnrollmentCampaignRequestWizardSettingsGen>
    {
        private static final String PROXY_NAME = "EnrEnrollmentCampaignRequestWizardSettingsNaturalProxy";

        private Long _enrollmentCampaign;

        public NaturalId()
        {}

        public NaturalId(EnrEnrollmentCampaign enrollmentCampaign)
        {
            _enrollmentCampaign = ((IEntity) enrollmentCampaign).getId();
        }

        public Long getEnrollmentCampaign()
        {
            return _enrollmentCampaign;
        }

        public void setEnrollmentCampaign(Long enrollmentCampaign)
        {
            _enrollmentCampaign = enrollmentCampaign;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrEnrollmentCampaignRequestWizardSettingsGen.NaturalId) ) return false;

            EnrEnrollmentCampaignRequestWizardSettingsGen.NaturalId that = (NaturalId) o;

            if( !equals(getEnrollmentCampaign(), that.getEnrollmentCampaign()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEnrollmentCampaign());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEnrollmentCampaign());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrEnrollmentCampaignRequestWizardSettingsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrEnrollmentCampaignRequestWizardSettings.class;
        }

        public T newInstance()
        {
            return (T) new EnrEnrollmentCampaignRequestWizardSettings();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "mainDataStepVisible":
                    return obj.isMainDataStepVisible();
                case "contactsStepVisible":
                    return obj.isContactsStepVisible();
                case "nextOfKinStepVisible":
                    return obj.isNextOfKinStepVisible();
                case "militaryStepVisible":
                    return obj.isMilitaryStepVisible();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrEnrollmentCampaign) value);
                    return;
                case "mainDataStepVisible":
                    obj.setMainDataStepVisible((Boolean) value);
                    return;
                case "contactsStepVisible":
                    obj.setContactsStepVisible((Boolean) value);
                    return;
                case "nextOfKinStepVisible":
                    obj.setNextOfKinStepVisible((Boolean) value);
                    return;
                case "militaryStepVisible":
                    obj.setMilitaryStepVisible((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "mainDataStepVisible":
                        return true;
                case "contactsStepVisible":
                        return true;
                case "nextOfKinStepVisible":
                        return true;
                case "militaryStepVisible":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "mainDataStepVisible":
                    return true;
                case "contactsStepVisible":
                    return true;
                case "nextOfKinStepVisible":
                    return true;
                case "militaryStepVisible":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrollmentCampaign":
                    return EnrEnrollmentCampaign.class;
                case "mainDataStepVisible":
                    return Boolean.class;
                case "contactsStepVisible":
                    return Boolean.class;
                case "nextOfKinStepVisible":
                    return Boolean.class;
                case "militaryStepVisible":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrEnrollmentCampaignRequestWizardSettings> _dslPath = new Path<EnrEnrollmentCampaignRequestWizardSettings>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrEnrollmentCampaignRequestWizardSettings");
    }
            

    /**
     * @return ПК. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignRequestWizardSettings#getEnrollmentCampaign()
     */
    public static EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Отображать шаг «Основные данные». Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignRequestWizardSettings#isMainDataStepVisible()
     */
    public static PropertyPath<Boolean> mainDataStepVisible()
    {
        return _dslPath.mainDataStepVisible();
    }

    /**
     * @return Отображать шаг «Контактная информация». Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignRequestWizardSettings#isContactsStepVisible()
     */
    public static PropertyPath<Boolean> contactsStepVisible()
    {
        return _dslPath.contactsStepVisible();
    }

    /**
     * @return Отображать шаг «Ближайшие родственники». Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignRequestWizardSettings#isNextOfKinStepVisible()
     */
    public static PropertyPath<Boolean> nextOfKinStepVisible()
    {
        return _dslPath.nextOfKinStepVisible();
    }

    /**
     * @return Отображать шаг «Служба в армии». Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignRequestWizardSettings#isMilitaryStepVisible()
     */
    public static PropertyPath<Boolean> militaryStepVisible()
    {
        return _dslPath.militaryStepVisible();
    }

    public static class Path<E extends EnrEnrollmentCampaignRequestWizardSettings> extends EntityPath<E>
    {
        private EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<Boolean> _mainDataStepVisible;
        private PropertyPath<Boolean> _contactsStepVisible;
        private PropertyPath<Boolean> _nextOfKinStepVisible;
        private PropertyPath<Boolean> _militaryStepVisible;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return ПК. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignRequestWizardSettings#getEnrollmentCampaign()
     */
        public EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Отображать шаг «Основные данные». Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignRequestWizardSettings#isMainDataStepVisible()
     */
        public PropertyPath<Boolean> mainDataStepVisible()
        {
            if(_mainDataStepVisible == null )
                _mainDataStepVisible = new PropertyPath<Boolean>(EnrEnrollmentCampaignRequestWizardSettingsGen.P_MAIN_DATA_STEP_VISIBLE, this);
            return _mainDataStepVisible;
        }

    /**
     * @return Отображать шаг «Контактная информация». Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignRequestWizardSettings#isContactsStepVisible()
     */
        public PropertyPath<Boolean> contactsStepVisible()
        {
            if(_contactsStepVisible == null )
                _contactsStepVisible = new PropertyPath<Boolean>(EnrEnrollmentCampaignRequestWizardSettingsGen.P_CONTACTS_STEP_VISIBLE, this);
            return _contactsStepVisible;
        }

    /**
     * @return Отображать шаг «Ближайшие родственники». Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignRequestWizardSettings#isNextOfKinStepVisible()
     */
        public PropertyPath<Boolean> nextOfKinStepVisible()
        {
            if(_nextOfKinStepVisible == null )
                _nextOfKinStepVisible = new PropertyPath<Boolean>(EnrEnrollmentCampaignRequestWizardSettingsGen.P_NEXT_OF_KIN_STEP_VISIBLE, this);
            return _nextOfKinStepVisible;
        }

    /**
     * @return Отображать шаг «Служба в армии». Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignRequestWizardSettings#isMilitaryStepVisible()
     */
        public PropertyPath<Boolean> militaryStepVisible()
        {
            if(_militaryStepVisible == null )
                _militaryStepVisible = new PropertyPath<Boolean>(EnrEnrollmentCampaignRequestWizardSettingsGen.P_MILITARY_STEP_VISIBLE, this);
            return _militaryStepVisible;
        }

        public Class getEntityClass()
        {
            return EnrEnrollmentCampaignRequestWizardSettings.class;
        }

        public String getEntityName()
        {
            return "enrEnrollmentCampaignRequestWizardSettings";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
