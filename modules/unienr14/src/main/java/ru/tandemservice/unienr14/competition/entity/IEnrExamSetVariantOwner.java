/* $Id:$ */
package ru.tandemservice.unienr14.competition.entity;

import org.tandemframework.core.common.IEntityDebugTitled;
import org.tandemframework.core.entity.IEntity;

import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

/**
 * @author oleyba
 * @since 2/18/14
 */
public interface IEnrExamSetVariantOwner extends IEntity, IEntityDebugTitled
{
    EnrEnrollmentCampaign getEnrollmentCampaign();
}
