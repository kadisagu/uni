package ru.tandemservice.unienr14.sec.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.organization.sec.entity.RoleConfigLocal;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.unienr14.sec.entity.RoleConfigLocalEnrCommission;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Конфигурация локальной роли для ОК
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RoleConfigLocalEnrCommissionGen extends RoleConfigLocal
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.sec.entity.RoleConfigLocalEnrCommission";
    public static final String ENTITY_NAME = "roleConfigLocalEnrCommission";
    public static final int VERSION_HASH = -1440281188;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_COMMISSION = "enrollmentCommission";

    private EnrEnrollmentCommission _enrollmentCommission;     // Отборочная комиссия

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Отборочная комиссия. Свойство не может быть null.
     */
    @NotNull
    public EnrEnrollmentCommission getEnrollmentCommission()
    {
        return _enrollmentCommission;
    }

    /**
     * @param enrollmentCommission Отборочная комиссия. Свойство не может быть null.
     */
    public void setEnrollmentCommission(EnrEnrollmentCommission enrollmentCommission)
    {
        dirty(_enrollmentCommission, enrollmentCommission);
        _enrollmentCommission = enrollmentCommission;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof RoleConfigLocalEnrCommissionGen)
        {
            setEnrollmentCommission(((RoleConfigLocalEnrCommission)another).getEnrollmentCommission());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RoleConfigLocalEnrCommissionGen> extends RoleConfigLocal.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RoleConfigLocalEnrCommission.class;
        }

        public T newInstance()
        {
            return (T) new RoleConfigLocalEnrCommission();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCommission":
                    return obj.getEnrollmentCommission();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "enrollmentCommission":
                    obj.setEnrollmentCommission((EnrEnrollmentCommission) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCommission":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCommission":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCommission":
                    return EnrEnrollmentCommission.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RoleConfigLocalEnrCommission> _dslPath = new Path<RoleConfigLocalEnrCommission>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RoleConfigLocalEnrCommission");
    }
            

    /**
     * @return Отборочная комиссия. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.sec.entity.RoleConfigLocalEnrCommission#getEnrollmentCommission()
     */
    public static EnrEnrollmentCommission.Path<EnrEnrollmentCommission> enrollmentCommission()
    {
        return _dslPath.enrollmentCommission();
    }

    public static class Path<E extends RoleConfigLocalEnrCommission> extends RoleConfigLocal.Path<E>
    {
        private EnrEnrollmentCommission.Path<EnrEnrollmentCommission> _enrollmentCommission;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Отборочная комиссия. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.sec.entity.RoleConfigLocalEnrCommission#getEnrollmentCommission()
     */
        public EnrEnrollmentCommission.Path<EnrEnrollmentCommission> enrollmentCommission()
        {
            if(_enrollmentCommission == null )
                _enrollmentCommission = new EnrEnrollmentCommission.Path<EnrEnrollmentCommission>(L_ENROLLMENT_COMMISSION, this);
            return _enrollmentCommission;
        }

        public Class getEntityClass()
        {
            return RoleConfigLocalEnrCommission.class;
        }

        public String getEntityName()
        {
            return "roleConfigLocalEnrCommission";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
