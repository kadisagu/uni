/* $Id$ */
package ru.tandemservice.unienr14.catalog.bo.EnrEnrollmentOrderParagraphPrintFormType.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author azhebko
 * @since 17.07.2014
 */
@Configuration
public class EnrEnrollmentOrderParagraphPrintFormTypeList extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
            .create();
    }
}