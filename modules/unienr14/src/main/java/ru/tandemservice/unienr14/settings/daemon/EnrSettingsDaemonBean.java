package ru.tandemservice.unienr14.settings.daemon;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import static org.tandemframework.hibsupport.dql.DQLFunctions.*;

import org.hibernate.Session;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrDiscipline;
import ru.tandemservice.unienr14.competition.daemon.EnrCompetitionDaemonDao;
import ru.tandemservice.unienr14.exams.daemon.EnrExamSetDaemonDao;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDisciplineGroup;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDisciplineGroupElement;

/**
 * Демон, синхранизирующий данные настроек:
 * <ul>
 * <li>наборы ВИ</li>
 * <li>группы ВИ</li>
 * </ul>
 * 
 * @author vdanilov
 */
public class EnrSettingsDaemonBean extends UniBaseDao implements IEnrSettingsDaemonBean {

    public static final SyncDaemon DAEMON = new SyncDaemon(EnrSettingsDaemonBean.class.getName(), 120, IEnrSettingsDaemonBean.GLOBAL_DAEMON_LOCK) {
        @Override protected void main()
        {
            // обновляет зачетный балл для групп ВИ (на основе его состава)
            try { IEnrSettingsDaemonBean.instance.get().doUpdateGroupDefaultPassMark(); }
            catch (final Exception t) { Debug.exception(t.getMessage(), t); }

            // опреации с наборами ВИ
            try { EnrExamSetDaemonDao.SETTINGS_DAEMON_ACTIONS.run(); }
            catch (final Exception t) { Debug.exception(t.getMessage(), t); }

            // опреации с настроенными наборами ВИ
            try { EnrCompetitionDaemonDao.SETTINGS_DAEMON_ACTIONS.run(); }
            catch (final Exception t) { Debug.exception(t.getMessage(), t); }

        }
    };

    @Override
    protected void initDao()
    {
        // вызывать демон при изменении состава группы (собственно меняется набор из чего брать максимум)
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, EnrCampaignDisciplineGroupElement.class, DAEMON.getAfterCompleteWakeUpListener());
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, EnrCampaignDisciplineGroupElement.class, DAEMON.getAfterCompleteWakeUpListener());

        // вызывать демон при изменении дисциплины справочника (меняется значение поля, на сонове которого вычисляется максимум)
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, EnrDiscipline.class, DAEMON.getAfterCompleteWakeUpListener());
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, EnrDiscipline.class, DAEMON.getAfterCompleteWakeUpListener());
    }


    @Override
    public boolean doUpdateGroupDefaultPassMark()
    {
        final int updates = 0;
        Debug.begin("EnrSettingsDaemonBean.doUpdateGroupDefaultPassMark");
        try {
            final Session session = lock(EnrCampaignDisciplineGroup.class.getName());
            executeAndClear(
                new DQLUpdateBuilder(EnrCampaignDisciplineGroup.class)
                .fromDataSource(
                    new DQLSelectBuilder()
                        .fromEntity(EnrCampaignDisciplineGroupElement.class, "gel")
                        .joinPath(DQLJoinType.inner, EnrCampaignDisciplineGroupElement.discipline().discipline().fromAlias("gel"), "dsc")
                        .column(property(EnrCampaignDisciplineGroupElement.group().id().fromAlias("gel")), "group_id")
                        .group(property(EnrCampaignDisciplineGroupElement.group().id().fromAlias("gel")))
                        .column(max(property(EnrDiscipline.defaultPassMarkAsLong().fromAlias("dsc"))), "maxPassMark")
                        .buildQuery(),
                        "ds"
                )
                .where(eq(property(EnrCampaignDisciplineGroup.P_ID), property("ds.group_id")))
                .set(EnrCampaignDisciplineGroup.P_DEFAULT_PASS_MARK_AS_LONG, property("ds.maxPassMark"))
                .where(ne(property(EnrCampaignDisciplineGroup.P_DEFAULT_PASS_MARK_AS_LONG), property("ds.maxPassMark"))),
                session
            );

        } finally {
            Debug.end();
        }
        return updates > 0;
    }


}

