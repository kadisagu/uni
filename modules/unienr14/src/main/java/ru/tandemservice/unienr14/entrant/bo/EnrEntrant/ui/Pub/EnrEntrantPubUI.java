/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.shared.person.base.bo.Person.util.ISecureRoleContext;
import ru.tandemservice.uni.component.person.util.ISecureRoleContextOwner;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.legacy.EnrPersonLegacyUtils;
import ru.tandemservice.unienr14.request.bo.EnrEntrantForeignRequest.EnrEntrantForeignRequestManager;

/**
 * @author oleyba
 * @since 4/10/13
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "entrant.id", required = true),
    @Bind(key = "selectedTab"),
    @Bind(key = "selectedDataTab")
})
@Output({
        @Bind(key = ISecureRoleContext.SECURE_ROLE_CONTEXT, binding = "secureRoleContext")
})
public class EnrEntrantPubUI extends UIPresenter implements ISecureRoleContextOwner
{
    private EnrEntrant entrant = new EnrEntrant();

    private String selectedTab;
    private String selectedDataTab;
    private boolean benefitTabVisible;

    @Override
    public void onComponentRefresh()
    {
        setEntrant(IUniBaseDao.instance.get().getNotNull(EnrEntrant.class, getEntrant().getId()));
    }

    public boolean isForeignTabVisible()
    {
        return EnrEntrantForeignRequestManager.instance().dao().entrantTabVisible(entrant.getId());
    }

    @Override
    public ISecureRoleContext getSecureRoleContext()
    {
        return EnrPersonLegacyUtils.getSecureRoleContext(getEntrant());
    }
    
    public String getRegionName() {
        return EnrEntrantPub.TAB_PANEL_REGION_NAME;
    }

    public EnrEntrant getEntrant()
    {
        return entrant;
    }

    public void setEntrant(EnrEntrant entrant)
    {
        this.entrant = entrant;
    }

    public String getSelectedTab()
    {
        return selectedTab;
    }

    public void setSelectedTab(String selectedTab)
    {
        this.selectedTab = selectedTab;
    }

    public String getSelectedDataTab()
    {
        return selectedDataTab;
    }

    public void setSelectedDataTab(String selectedDataTab)
    {
        this.selectedDataTab = selectedDataTab;
    }

    public boolean isBenefitTabVisible()
    {
        return benefitTabVisible;
    }

    public void setBenefitTabVisible(boolean benefitTabVisible)
    {
        this.benefitTabVisible = benefitTabVisible;
    }
}
