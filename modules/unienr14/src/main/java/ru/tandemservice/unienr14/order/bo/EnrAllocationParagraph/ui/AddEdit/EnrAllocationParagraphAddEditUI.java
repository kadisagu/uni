/* $Id$ */
package ru.tandemservice.unienr14.order.bo.EnrAllocationParagraph.ui.AddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.PageableSearchListDataSource;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.RadioButtonColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.competition.entity.gen.EnrProgramSetOrgUnitGen;
import ru.tandemservice.unienr14.order.bo.EnrAllocationParagraph.EnrAllocationParagraphManager;
import ru.tandemservice.unienr14.order.entity.EnrAllocationExtract;
import ru.tandemservice.unienr14.order.entity.EnrAllocationParagraph;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import ru.tandemservice.unimove.UnimoveDefines;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author nvankov
 * @since 8/8/14
 */
@Input({
        @Bind(key = EnrAllocationParagraphAddEditUI.PARAMETER_ORDER_ID, binding = EnrAllocationParagraphAddEditUI.PARAMETER_ORDER_ID),
        @Bind(key = EnrAllocationParagraphAddEditUI.PARAMETER_PARAGRAPH_ID, binding = EnrAllocationParagraphAddEditUI.PARAMETER_PARAGRAPH_ID)
})
public class EnrAllocationParagraphAddEditUI extends UIPresenter
{
    public static final String PARAMETER_ORDER_ID = "order.id";
    public static final String PARAMETER_PARAGRAPH_ID = "paragraph.id";

    private EnrOrder _order = new EnrOrder();
    private EnrAllocationParagraph _paragraph = new EnrAllocationParagraph();
    private boolean _editForm;

    // выбор конкурсов

    private Set<Long> _selectedEntrantIds = new HashSet<>();
    private Long _groupManagerEntrantId;
    private String _groupTitle;

    private boolean _onlyAllocated = false;
    private boolean _onlyEduProgram = false;

    private EduProgramForm _eduProgramForm;
    private EnrOrgUnit _enrOrgUnit;
    private OrgUnit _orgUnit;
    private EduProgramSubject _eduProgramSubject;
    private EnrProgramSetBase _enrProgramSetBase;

    @Override
    public void onComponentRefresh()
    {
        if (getParagraph().getId() == null)
        {
            // создание параграфа
            setEditForm(false);
            setOrder(DataAccessServices.dao().getNotNull(EnrOrder.class, getOrder().getId()));
            getParagraph().setOrder(getOrder());
        }
        else
        {
            // редактирование параграфа
            setEditForm(true);
            setParagraph(DataAccessServices.dao().getNotNull(EnrAllocationParagraph.class, getParagraph().getId()));
            setOrder((EnrOrder) getParagraph().getOrder());

            _eduProgramForm = getParagraph().getEnrProgramSetItem().getProgramSet().getProgramForm();
            _enrOrgUnit = getParagraph().getEnrProgramSetOrgUnit().getOrgUnit();
            _orgUnit = getParagraph().getEnrProgramSetOrgUnit().getFormativeOrgUnit();
            _eduProgramSubject = getParagraph().getEnrProgramSetItem().getProgramSet().getProgramSubject();
            _enrProgramSetBase = getParagraph().getEnrProgramSetItem().getProgramSet();

            for (EnrAllocationExtract extract : IUniBaseDao.instance.get().getList(EnrAllocationExtract.class, EnrAllocationExtract.paragraph(), getParagraph(), EnrAllocationExtract.number().s()))
            {
                getSelectedEntrantIds().add(extract.getEntity().getId());

                if (null != extract.getGroupTitle()) setGroupTitle(extract.getGroupTitle());

                //EduProgramForm extractForm = extract.getEntity().getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramForm();
            }
            setGroupManagerEntrantId(getParagraph().getGroupManager() == null ? null : getParagraph().getGroupManager().getEntity().getId());
        }

        if (!UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(getOrder().getState().getCode())) {
            throw new ApplicationException("Приказ находится в состоянии «" + getOrder().getState().getTitle() +"», редактирование параграфов запрещено.");
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrAllocationParagraphAddEdit.BIND_PARAGRAPH, getParagraph());
        dataSource.put(EnrAllocationParagraphAddEdit.BIND_EDIT_FORM, isEditForm());
        dataSource.put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, getOrder().getEnrollmentCampaign());
        dataSource.put(EnrAllocationParagraphAddEdit.BIND_ONLY_ALLOCATED, isOnlyAllocated());
        dataSource.put(EnrAllocationParagraphAddEdit.BIND_ONLY_EDU_PROGRAM, isOnlyEduProgram());
        dataSource.put(EnrAllocationParagraphAddEdit.BIND_FORM, getEduProgramForm());
        dataSource.put(EnrAllocationParagraphAddEdit.BIND_ORG_UNIT, getEnrOrgUnit());
        dataSource.put(EnrAllocationParagraphAddEdit.BIND_FORMATIVE_ORG_UNIT, getOrgUnit());
        dataSource.put(EnrAllocationParagraphAddEdit.BIND_SUBJECT, getEduProgramSubject());
        dataSource.put(EnrAllocationParagraphAddEdit.BIND_PROGRAM_SET, getEnrProgramSetBase());
        dataSource.put(EnrAllocationParagraphAddEdit.BIND_PROGRAM_SET_ITEM, getParagraph().getEnrProgramSetItem());

    }

    @Override
    @SuppressWarnings("unchecked")
    public void onAfterDataSourceFetch(IUIDataSource dataSource)
    {
        if (isEditForm() && "entrantDS".equals(dataSource.getName()))
        {
            DynamicListDataSource ds = ((PageableSearchListDataSource) dataSource).getLegacyDataSource();

            List<IEntity> checkboxColumnSelected = new ArrayList<IEntity>();
            IEntity radioboxColumnSelected = null;

            for (IEntity record : (List<IEntity>) ds.getEntityList())
            {
                if (getSelectedEntrantIds().contains(record.getId()))
                    checkboxColumnSelected.add(record);

                if (record.getId().equals(getGroupManagerEntrantId()))
                    radioboxColumnSelected = record;
            }

            CheckboxColumn checkboxColumn = ((CheckboxColumn) ds.getColumn(EnrAllocationParagraphAddEdit.CHECKBOX_COLUMN));
            if (checkboxColumn != null)
                checkboxColumn.setSelectedObjects(checkboxColumnSelected);

            RadioButtonColumn radioboxColumn = ((RadioButtonColumn) ds.getColumn(EnrAllocationParagraphAddEdit.RADIOBOX_COLUMN));
            if (radioboxColumn != null)
                radioboxColumn.setSelectedEntity(radioboxColumnSelected);
        }
    }


    // Listeners

    public void onRefreshSelectionParams() {
        // ?
    }

    public void onClickApply()
    {
        try {

            DynamicListDataSource ds = ((PageableSearchListDataSource) _uiConfig.getDataSource("entrantDS")).getLegacyDataSource();
            CheckboxColumn checkboxColumn = ((CheckboxColumn) ds.getColumn(EnrAllocationParagraphAddEdit.CHECKBOX_COLUMN));
            RadioButtonColumn radioboxColumn = ((RadioButtonColumn) ds.getColumn(EnrAllocationParagraphAddEdit.RADIOBOX_COLUMN));

            List<EnrRequestedCompetition> preStudents = new ArrayList<EnrRequestedCompetition>();
            if (checkboxColumn != null) {
                for (IEntity entity : checkboxColumn.getSelectedObjects()) {
                    preStudents.add((EnrRequestedCompetition) ((DataWrapper) entity).getWrapped());
                    getSelectedEntrantIds().add(entity.getId());
                }
            }

            EnrRequestedCompetition manager = null;
            if (getOrder().getPrintFormType().isSelectHeadman() && radioboxColumn != null && radioboxColumn.getSelectedEntity() != null)
                manager = ((DataWrapper) radioboxColumn.getSelectedEntity()).getWrapped();

            // сохраняем или обновляем параграф о зачислении
            _paragraph.setEnrProgramSetOrgUnit(DataAccessServices.dao().<EnrProgramSetOrgUnit>getByNaturalId(new EnrProgramSetOrgUnitGen.NaturalId(_enrProgramSetBase, _enrOrgUnit)));
            EnrAllocationParagraphManager.instance().dao().saveOrUpdateParagraph(_order, _paragraph, preStudents, manager, getGroupTitle());

            // закрываем форму
            deactivate();

        } catch (Throwable t) {
            Debug.exception(t);
            _uiSupport.setRefreshScheduled(true);
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    // presenter

    public boolean isOriginalColumnVisible() {
        return getOrder().getCompensationType().isBudget();
    }

    public boolean isAcceptedColumnVisible() {
        return !getOrder().getCompensationType().isBudget();
    }

    public String getFormTitle() {
        String formTitle = (StringUtils.isNotEmpty(_order.getNumber()) ? "№" + _order.getNumber() + " " : "") + (_order.getCommitDate() != null ? "от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(_order.getCommitDate()) : "");
        if (isEditForm())
            formTitle = "Редактирование параграфа из приказа о зачислении " + formTitle;
        else
            formTitle = "Добавление параграфа приказа о зачислении " + formTitle;
        return formTitle;
    }

    // Getters & Setters

    public EnrOrder getOrder()
    {
        return _order;
    }

    public void setOrder(EnrOrder order)
    {
        _order = order;
    }

    public EnrAllocationParagraph getParagraph()
    {
        return _paragraph;
    }

    public void setParagraph(EnrAllocationParagraph paragraph)
    {
        _paragraph = paragraph;
    }

    public boolean isEditForm()
    {
        return _editForm;
    }

    public void setEditForm(boolean editForm)
    {
        _editForm = editForm;
    }

    public Set<Long> getSelectedEntrantIds()
    {
        return _selectedEntrantIds;
    }

    public void setSelectedEntrantIds(Set<Long> selectedEntrantIds)
    {
        _selectedEntrantIds = selectedEntrantIds;
    }

    public Long getGroupManagerEntrantId()
    {
        return _groupManagerEntrantId;
    }

    public void setGroupManagerEntrantId(Long groupManagerEntrantId)
    {
        _groupManagerEntrantId = groupManagerEntrantId;
    }

    public String getGroupTitle()
    {
        return _groupTitle;
    }

    public void setGroupTitle(String groupTitle)
    {
        _groupTitle = groupTitle;
    }

    public boolean isOnlyAllocated()
    {
        return _onlyAllocated;
    }

    public void setOnlyAllocated(boolean onlyAllocated)
    {
        _onlyAllocated = onlyAllocated;
    }

    public boolean isOnlyEduProgram()
    {
        return _onlyEduProgram;
    }

    public void setOnlyEduProgram(boolean onlyEduProgram)
    {
        _onlyEduProgram = onlyEduProgram;
    }

    public EduProgramForm getEduProgramForm()
    {
        return _eduProgramForm;
    }

    public void setEduProgramForm(EduProgramForm eduProgramForm)
    {
        _eduProgramForm = eduProgramForm;
    }

    public EnrOrgUnit getEnrOrgUnit()
    {
        return _enrOrgUnit;
    }

    public void setEnrOrgUnit(EnrOrgUnit enrOrgUnit)
    {
        _enrOrgUnit = enrOrgUnit;
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }

    public EduProgramSubject getEduProgramSubject()
    {
        return _eduProgramSubject;
    }

    public void setEduProgramSubject(EduProgramSubject eduProgramSubject)
    {
        _eduProgramSubject = eduProgramSubject;
    }

    public EnrProgramSetBase getEnrProgramSetBase()
    {
        return _enrProgramSetBase;
    }

    public void setEnrProgramSetBase(EnrProgramSetBase enrProgramSetBase)
    {
        _enrProgramSetBase = enrProgramSetBase;
    }
}
