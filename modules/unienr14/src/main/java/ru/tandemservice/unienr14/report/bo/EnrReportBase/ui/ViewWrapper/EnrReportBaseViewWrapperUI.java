/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReportBase.ui.ViewWrapper;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.EnrReportBaseManager;

/**
 * @author oleyba
 * @since 5/26/14
 */
@State({
    @Bind(key = EnrReportBaseViewWrapperUI.REPORT_NAME_KEY, binding = "reportName")
})
public class EnrReportBaseViewWrapperUI extends UIPresenter
{
    public static final String REPORT_NAME_KEY = "reportName";

    private String reportName;

    @Override
    public void onComponentRefresh()
    {
        EnrReportBaseManager.instance().reportListExtPoint().getItems().get(getReportName()).onClickViewReport(this.<String>getListenerParameter(), _uiActivation);
    }

    // getters and setters

    public String getReportName()
    {
        return reportName;
    }

    public void setReportName(String reportName)
    {
        this.reportName = reportName;
    }
}