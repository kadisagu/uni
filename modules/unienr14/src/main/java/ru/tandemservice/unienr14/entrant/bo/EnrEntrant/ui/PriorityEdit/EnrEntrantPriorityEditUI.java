/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PriorityEdit;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionExclusive;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionNoExams;
import ru.tandemservice.unienr14.request.entity.EnrRequestedProgram;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 5/30/13
 */
@State({
    @Bind(key = "entrantId", binding = "entrant.id")
})
public class EnrEntrantPriorityEditUI extends UIPresenter
{
    private EnrEntrant entrant = new EnrEntrant();

    private List<IdentifiableWrapper> _rowList = Lists.newLinkedList();
    List<ReqCompWrapper> reqCompRowList;
    private ReqCompWrapper currentReqCompRow;
    private ReqProgramWrapper currentProgramRow;

    private ISelectModel _requestTypeModel;
    private EnrRequestType _requestType;
    
    @Override
    public void onComponentRefresh()
    {
        _uiSupport.getSession().clear();

        setEntrant(IUniBaseDao.instance.get().getNotNull(EnrEntrant.class, getEntrant().getId()));
        List<EnrRequestType> requestTypes = IUniBaseDao.instance.get().getList(new DQLSelectBuilder()
                .fromEntity(EnrRequestType.class, "rt")
                .where(or(
                        in(property("rt.id"),
                                new DQLSelectBuilder()
                                    .fromEntity(EnrRequestedCompetition.class, "c")
                                    .column(property("c", EnrRequestedCompetition.request().type().id()))
                                    .where(eq(property("c", EnrRequestedCompetition.request().entrant()), value(getEntrant())))
                                    .where(eq(property("c", EnrRequestedCompetition.request().takeAwayDocument()), value(Boolean.FALSE)))
                                    .buildQuery()),
                        in(property("rt.id"),
                                new DQLSelectBuilder()
                                    .fromEntity(EnrRequestedProgram.class, "p")
                                    .where(eq(property("p", EnrRequestedProgram.requestedCompetition().request().entrant()), value(getEntrant())))
                                    .where(eq(property("p", EnrRequestedProgram.requestedCompetition().request().takeAwayDocument()), value(Boolean.FALSE)))
                                    .buildQuery())))
                .order(property("rt", EnrRequestType.code())));

        setRequestTypeModel(new LazySimpleSelectModel<>(requestTypes).setSearchFromStart(false));
        if (requestTypes.size() == 1)
            setRequestType(requestTypes.iterator().next());

        prepareReqComRowList();
    }

    public void onChangeRequestType()
    {
        prepareReqComRowList();
    }

    public void onClickApply()
    {
        try
        {
            Map<EnrRequestedCompetition, Map<EnrProgramSetItem, Integer>> priorityMap = Maps.newHashMap();
            if (validate(priorityMap).hasErrors()) return;
            saveFormData(priorityMap);
        }
        catch (Exception e)
        {
            ContextLocal.getSite().getDesktop().setRefreshScheduled(true);
            throw CoreExceptionUtils.getRuntimeException(e);
        }

        deactivate();
    }

    // utils

    protected ErrorCollector validate(Map<EnrRequestedCompetition, Map<EnrProgramSetItem, Integer>> priorityMap)
    {
        ErrorCollector errors = ContextLocal.getErrorCollector();

        Map<EnrRequestedCompetition, List<String>> wrapperMap = Maps.newHashMap();
        for (ReqCompWrapper cg : getReqCompRowList())
        {
            List<String> priorityIds = Lists.newArrayList();
            HashMap<EnrProgramSetItem, Integer> itemMap = Maps.newHashMap();
            priorityMap.put(cg.getEntity(), itemMap);

            cg.getChildren().stream().filter(ReqProgramWrapper::isSelected).forEach(profile -> {
                itemMap.put(profile.getEntity(), profile.getPriority());
                priorityIds.add(profile.getPriorityId());
            });
            wrapperMap.put(cg.getEntity(), priorityIds);
        }

        for (Map.Entry<EnrRequestedCompetition, Map<EnrProgramSetItem, Integer>> entry : priorityMap.entrySet())
        {
            Map<EnrProgramSetItem, Integer> itemMap = entry.getValue();
            if (!itemMap.isEmpty() && itemMap.values().stream().noneMatch(p -> p != null)) {
                errors.add("Необходимо выбрать хотя бы одну образовательную программу.", wrapperMap.get(entry.getKey()).stream().toArray(String[]::new));
            }
        }
        return errors;
    }

    protected void saveFormData(Map<EnrRequestedCompetition, Map<EnrProgramSetItem, Integer>> priorityMap)
    {
        EnrEntrantManager.instance().dao().saveSelectedProfilesAndAllPriorities(getEntrant(), getRequestType(), priorityMap);
    }

    protected void prepareReqComRowList()
    {

        setReqCompRowList(new ArrayList<>());
        Set<Long> programSetIds = new HashSet<>();
        List<EnrRequestedCompetition> requestedCompetitions = IUniBaseDao.instance.get().getList(
                new DQLSelectBuilder()
                        .fromEntity(EnrRequestedCompetition.class, "c")
                        .where(eq(property("c", EnrRequestedCompetition.request().entrant()), value(getEntrant())))
                        .where(eq(property("c", EnrRequestedCompetition.request().takeAwayDocument()), value(Boolean.FALSE)))
                        .where(eq(property("c", EnrRequestedCompetition.request().type()), value(getRequestType())))
                        .order(property("c", EnrRequestedCompetition.priority().s()))
        );

        for (EnrRequestedCompetition competition : requestedCompetitions) {
            getReqCompRowList().add(new ReqCompWrapper(competition));
            if (competition.getCompetition().isAllowProgramPriorities()) {
                programSetIds.add(competition.getCompetition().getProgramSetOrgUnit().getProgramSet().getId());
            }
        }

        Map<EnrRequestedCompetition, Map<EnrProgramSetItem, Integer>> priorityMap = SafeMap.get(HashMap.class);
        List<EnrRequestedProgram> requestedPrograms = IUniBaseDao.instance.get().getList(
                new DQLSelectBuilder()
                        .fromEntity(EnrRequestedProgram.class, "p")
                        .where(eq(property("p", EnrRequestedProgram.requestedCompetition().request().entrant()), value(getEntrant())))
                        .where(eq(property("p", EnrRequestedProgram.requestedCompetition().request().takeAwayDocument()), value(Boolean.FALSE)))
                        .where(eq(property("p", EnrRequestedProgram.requestedCompetition().request().type()), value(getRequestType())))
        );
        for (EnrRequestedProgram reqProgram : requestedPrograms) {
            priorityMap.get(reqProgram.getRequestedCompetition()).put(reqProgram.getProgramSetItem(), reqProgram.getPriority());
        }

        Map<EnrProgramSetBase, List<EnrProgramSetItem>> programMap = SafeMap.get(ArrayList.class);
        for (EnrProgramSetItem item : IUniBaseDao.instance.get().getList(EnrProgramSetItem.class, EnrProgramSetItem.programSet().id(), programSetIds)) {
            programMap.get(item.getProgramSet()).add(item);
        }

        for (ReqCompWrapper reqCompWrapper : getReqCompRowList()) {
            EnrRequestedCompetition requestedCompetition = reqCompWrapper.getEntity();
            if (!requestedCompetition.getCompetition().isAllowProgramPriorities())
                continue;
            for (EnrProgramSetItem item : programMap.get(requestedCompetition.getCompetition().getProgramSetOrgUnit().getProgramSet())) {
                if (requestedCompetition instanceof EnrRequestedCompetitionNoExams)
                {
                    EnrRequestedCompetitionNoExams comp = (EnrRequestedCompetitionNoExams) requestedCompetition;
                    if (!item.equals(comp.getProgramSetItem())) continue;
                }
                else if (requestedCompetition instanceof EnrRequestedCompetitionExclusive)
                {
                    EnrRequestedCompetitionExclusive comp = (EnrRequestedCompetitionExclusive) requestedCompetition;
                    if (!item.equals(comp.getProgramSetItem())) continue;
                }
                ReqProgramWrapper reqProgramWrapper = new ReqProgramWrapper(item, reqCompWrapper);
                reqCompWrapper.getChildren().add(reqProgramWrapper);
                reqProgramWrapper.setPriority(priorityMap.get(reqCompWrapper.getEntity()).get(item));
                reqProgramWrapper.setSelected(true);
            }

            Collections.sort(reqCompWrapper.getChildren(), (o1, o2) -> {
                int result = UniBaseUtils.compare(o1.getPriority(), o2.getPriority(), false);
                if (result == 0)
                    result = o1.getEntity().getTitle().compareTo(o2.getEntity().getTitle());
                if (result == 0)
                    result = o1.getId().compareTo(o2.getId());
                return result;
            });
        }

        _rowList.clear();
        for (ReqCompWrapper compWrapper : getReqCompRowList())
        {
            _rowList.add(compWrapper);
            for (ReqProgramWrapper programWrapper : compWrapper.getChildren())
            {
                _rowList.add(programWrapper);
            }
        }
    }

    // getters and setters

    public EnrEntrant getEntrant()
    {
        return entrant;
    }

    public void setEntrant(EnrEntrant entrant)
    {
        this.entrant = entrant;
    }

    public int getCurrentCompIndex()
    {
        return _rowList.indexOf(currentReqCompRow) + 1;
    }

    public int getCurrentProgramIndex()
    {
        return _rowList.indexOf(currentProgramRow) + 1;
    }

    public boolean isProgramPriorityVisible()
    {
        for (ReqCompWrapper reqCompWrapper : getReqCompRowList())
            if (reqCompWrapper.getEntity().getCompetition().isAllowProgramPriorities())
                return true;
        return false;
    }

    public ReqProgramWrapper getCurrentProgramRow()
    {
        return currentProgramRow;
    }

    public void setCurrentProgramRow(ReqProgramWrapper currentProgramRow)
    {
        this.currentProgramRow = currentProgramRow;
    }

    public ReqCompWrapper getCurrentReqCompRow()
    {
        return currentReqCompRow;
    }

    public void setCurrentReqCompRow(ReqCompWrapper currentReqCompRow)
    {
        this.currentReqCompRow = currentReqCompRow;
    }

    public List<IdentifiableWrapper> getRowList()
    {
        return _rowList;
    }

    public void setRowList(List<IdentifiableWrapper> rowList)
    {
        _rowList = rowList;
    }

    public List<ReqCompWrapper> getReqCompRowList()
    {
        return reqCompRowList;
    }

    public void setReqCompRowList(List<ReqCompWrapper> reqCompRowList)
    {
        this.reqCompRowList = reqCompRowList;
    }

    public EnrRequestType getRequestType(){ return _requestType; }
    public void setRequestType(EnrRequestType requestType){ _requestType = requestType; }

    public ISelectModel getRequestTypeModel(){ return _requestTypeModel; }
    public void setRequestTypeModel(ISelectModel requestTypeModel){ _requestTypeModel = requestTypeModel; }

    // inner classes
    
    protected static class ReqCompWrapper extends IdentifiableWrapper<EnrRequestedCompetition> {
        protected EnrRequestedCompetition entity;
        protected List<ReqProgramWrapper> children = new ArrayList<>();

        protected ReqCompWrapper(EnrRequestedCompetition entity) throws ClassCastException
        {
            super(entity.getId(), entity.getPriorityRowTitle());
            this.entity = entity;
        }

        public List<ReqProgramWrapper> getChildren() { return children; }
        public EnrRequestedCompetition getEntity() { return entity; }
        public String getPriorityId() {return "priority_" + getEntity().getId().hashCode(); }
    }

    protected static class ReqProgramWrapper extends IdentifiableWrapper<EnrRequestedProgram> {
        protected EnrProgramSetItem entity;
        protected ReqCompWrapper parent;
        protected boolean selected;
        protected Integer priority;

        protected ReqProgramWrapper(EnrProgramSetItem entity, ReqCompWrapper parent) throws ClassCastException
        {
            super(entity.getId(), "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + entity.getProgram().getTitleWithCodeAndConditionsShortWithForm());
            this.entity = entity;
            this.parent = parent;
        }

        public EnrProgramSetItem getEntity() { return entity; }
        public ReqCompWrapper getParent() { return parent; }
        public Integer getPriority() { return priority; }
        public void setPriority(Integer priority) { this.priority = priority; }
        public boolean isSelected() { return selected; }
        public void setSelected(boolean selected) { this.selected = selected; }
        public String getCheckboxId() {return "check_" + getEntity().getId().hashCode() + "_" + getParent().getEntity().getId().hashCode(); }
        public String getCheckboxSpanId() {return "checkSpan_" + getEntity().getId().hashCode() + "_" + getParent().getEntity().getId().hashCode(); }
        public String getPriorityId() {return "priority_" + getEntity().getId().hashCode() + "_" + getParent().getEntity().getId().hashCode(); }
        
    }
}