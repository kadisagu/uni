/* $Id$ */
package ru.tandemservice.unienr14.order.bo.EnrAllocationParagraph.ui.Pub;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.order.bo.EnrAllocationParagraph.logic.EnrAllocationExtractDSHandler;
import ru.tandemservice.unienr14.order.bo.EnrAllocationParagraph.ui.AddEdit.EnrAllocationParagraphAddEdit;
import ru.tandemservice.unienr14.order.bo.EnrAllocationParagraph.ui.AddEdit.EnrAllocationParagraphAddEditUI;
import ru.tandemservice.unienr14.order.bo.EnrOrder.EnrOrderManager;
import ru.tandemservice.unienr14.order.bo.EnrOrder.ui.StudentForExtractEdit.EnrOrderStudentForExtractEdit;
import ru.tandemservice.unienr14.order.entity.EnrAllocationExtract;
import ru.tandemservice.unienr14.order.entity.EnrAllocationParagraph;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.codes.OrderStatesCodes;

import java.util.List;

/**
 * @author nvankov
 * @since 8/8/14
 */

@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "paragraphId")
})
public class EnrAllocationParagraphPubUI extends UIPresenter
{
    private Long _paragraphId;
    private EnrAllocationParagraph _paragraph;
    private CommonPostfixPermissionModel _secModel;

    private DataWrapper params = new DataWrapper();

    @Override
    public void onComponentRefresh()
    {
        setParagraph(DataAccessServices.dao().getNotNull(EnrAllocationParagraph.class, getParagraphId()));
        setSecModel(new CommonPostfixPermissionModel("enrOrder"));

        List<EnrAllocationExtract> extracts = IUniBaseDao.instance.get().getList(EnrAllocationExtract.class, EnrAllocationExtract.paragraph(), getParagraph(), EnrAllocationExtract.number().s());
        getParams().put("programSet", CommonBaseStringUtil.joinUniqueSorted(extracts, EnrAllocationExtract.entity().competition().programSetOrgUnit().programSet().title().s(), ";"));
        getParams().put("competitionType", CommonBaseStringUtil.joinUniqueSorted(extracts, EnrAllocationExtract.entity().competition().type().title().s(), ";"));
        getParams().put("competition", CommonBaseStringUtil.joinUniqueSorted(extracts, EnrAllocationExtract.entity().competition().title().s(), ";"));
        getParams().put("groupTitle", CommonBaseStringUtil.joinUniqueSorted(extracts, EnrAllocationExtract.groupTitle().s(), ";"));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (EnrAllocationParagraphPub.ALLOCATION_EXTRACT_DS.equals(dataSource.getName())) {
            dataSource.put(EnrAllocationExtractDSHandler.ALLOCATION_PARAGRAPH, _paragraph);
        }
    }

    public boolean isCanAnnul()
    {
        return UnimoveDefines.CATALOG_ORDER_STATE_FINISHED.equals(_paragraph.getOrder().getState().getCode());
    }

    public boolean isChooseStudentVisible()
    {
        return OrderStatesCodes.FINISHED.equals(_paragraph.getOrder().getState().getCode());
    }

    public String getFormTitle()
    {
        return "Параграф приказа «" + getParagraph().getOrder().getType().getTitle() + "»";
    }

    // Listeners

    public void onClickEditParagraph()
    {
        _uiActivation.asRegion(EnrAllocationParagraphAddEdit.class)
                .parameter(EnrAllocationParagraphAddEditUI.PARAMETER_PARAGRAPH_ID, getParagraph().getId())
                .activate();
    }

    public void onClickDeleteParagraph()
    {
        EnrOrderManager.instance().dao().deleteParagraph(getParagraph().getId());
        deactivate();
    }

    public void onClickSetStudent()
    {
        _uiActivation.asRegionDialog(EnrOrderStudentForExtractEdit.class).parameter("extractId", getListenerParameterAsLong()).activate();
    }

    public void onClickDeleteExtract()
    {
        EnrAllocationExtract extract = DataAccessServices.dao().getNotNull(getListenerParameterAsLong());

        EnrOrderManager.instance().dao().deleteExtract(extract);
    }

    // Getters & Setters

    public Long getParagraphId()
    {
        return _paragraphId;
    }

    public void setParagraphId(Long paragraphId)
    {
        _paragraphId = paragraphId;
    }

    public EnrAllocationParagraph getParagraph()
    {
        return _paragraph;
    }

    public void setParagraph(EnrAllocationParagraph paragraph)
    {
        _paragraph = paragraph;
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        _secModel = secModel;
    }

    public DataWrapper getParams()
    {
        return params;
    }

    public void setParams(DataWrapper params)
    {
        this.params = params;
    }
}
