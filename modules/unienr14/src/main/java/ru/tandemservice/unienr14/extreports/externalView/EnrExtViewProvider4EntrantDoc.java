/* $Id:$ */
package ru.tandemservice.unienr14.extreports.externalView;

import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.extview.SimpleDQLExternalViewConfig;
import org.tandemframework.shared.person.catalog.entity.PersonDocumentCategory;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantBaseDocument;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequestAttachment;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author oleyba
 * @since 8/27/13
 */
public class EnrExtViewProvider4EntrantDoc extends SimpleDQLExternalViewConfig
{

    @Override
    protected DQLSelectBuilder buildDqlQuery() {
        DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(EnrEntrantBaseDocument.class, "doc")
            .joinEntity("doc", DQLJoinType.left, EnrEntrantRequestAttachment.class, "att", eq(property("doc"), property(EnrEntrantRequestAttachment.document().fromAlias("att"))))
            .joinPath(DQLJoinType.left, EnrEntrantRequestAttachment.entrantRequest().fromAlias("att"), "request");

        column(dql, property(EnrEntrantBaseDocument.id().fromAlias("doc")), "documentId").comment("uid документа");
        column(dql, property(EnrEntrantBaseDocument.entrant().id().fromAlias("doc")), "entrantId").comment("uid абитуриента");
        column(dql, property(EnrEntrantRequest.id().fromAlias("request")), "entrantRequestId").comment("uid заявления");
        column(dql, property(EnrEntrantBaseDocument.docRelation().document().comment().fromAlias("doc")), "comment1").comment("примечание");
        column(dql, property(EnrEntrantBaseDocument.docRelation().document().description().fromAlias("doc")), "description").comment("описание");
        column(dql, property(EnrEntrantBaseDocument.documentType().title().fromAlias("doc")), "documentType").comment("тип документа");
        column(dql, property(EnrEntrantBaseDocument.documentType().code().fromAlias("doc")), "documentTypeCode").comment("код типа документа");
        column(dql, property(EnrEntrantBaseDocument.docRelation().document().documentCategory().title().fromAlias("doc")), "docCategory").comment("Категория документа");
        column(dql, property(EnrEntrantBaseDocument.docRelation().document().documentCategory().code().fromAlias("doc")), "docCategoryCode").comment("Код категории документа");
        column(dql, property(EnrEntrantBaseDocument.docRelation().document().expirationDate().fromAlias("doc")), "expirationDate").comment("дата окончания действия документа");
        column(dql, property(EnrEntrantBaseDocument.docRelation().document().issuanceDate().fromAlias("doc")), "issuanceDate").comment("дата выдачи");
        column(dql, property(EnrEntrantBaseDocument.docRelation().document().issuancePlace().fromAlias("doc")), "issuancePlace").comment("кем выдан");
        column(dql, property(EnrEntrantBaseDocument.number().fromAlias("doc")), "number1").comment("номер");
        column(dql, property(EnrEntrantBaseDocument.seria().fromAlias("doc")), "seria").comment("серия");


        return dql;
    }
}