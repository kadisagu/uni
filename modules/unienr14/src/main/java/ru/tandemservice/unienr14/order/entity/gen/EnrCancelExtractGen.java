package ru.tandemservice.unienr14.order.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unienr14.order.entity.EnrAbstractExtract;
import ru.tandemservice.unienr14.order.entity.EnrCancelExtract;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка приказа об отмене приказа по абитуриентам (2014)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrCancelExtractGen extends EnrAbstractExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.order.entity.EnrCancelExtract";
    public static final String ENTITY_NAME = "enrCancelExtract";
    public static final int VERSION_HASH = 1672104680;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTITY = "entity";
    public static final String L_REQUESTED_COMPETITION = "requestedCompetition";

    private EnrAbstractExtract _entity;     // Абстрактная выписка на абитуриента (2014)
    private EnrRequestedCompetition _requestedCompetition;     // Выбранный конкурс

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Абстрактная выписка на абитуриента (2014). Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EnrAbstractExtract getEntity()
    {
        return _entity;
    }

    /**
     * @param entity Абстрактная выписка на абитуриента (2014). Свойство не может быть null и должно быть уникальным.
     */
    public void setEntity(EnrAbstractExtract entity)
    {
        dirty(_entity, entity);
        _entity = entity;
    }

    /**
     * @return Выбранный конкурс. Свойство не может быть null.
     */
    @NotNull
    public EnrRequestedCompetition getRequestedCompetition()
    {
        return _requestedCompetition;
    }

    /**
     * @param requestedCompetition Выбранный конкурс. Свойство не может быть null.
     */
    public void setRequestedCompetition(EnrRequestedCompetition requestedCompetition)
    {
        dirty(_requestedCompetition, requestedCompetition);
        _requestedCompetition = requestedCompetition;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EnrCancelExtractGen)
        {
            setEntity(((EnrCancelExtract)another).getEntity());
            setRequestedCompetition(((EnrCancelExtract)another).getRequestedCompetition());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrCancelExtractGen> extends EnrAbstractExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrCancelExtract.class;
        }

        public T newInstance()
        {
            return (T) new EnrCancelExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "entity":
                    return obj.getEntity();
                case "requestedCompetition":
                    return obj.getRequestedCompetition();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "entity":
                    obj.setEntity((EnrAbstractExtract) value);
                    return;
                case "requestedCompetition":
                    obj.setRequestedCompetition((EnrRequestedCompetition) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "entity":
                        return true;
                case "requestedCompetition":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "entity":
                    return true;
                case "requestedCompetition":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "entity":
                    return EnrAbstractExtract.class;
                case "requestedCompetition":
                    return EnrRequestedCompetition.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrCancelExtract> _dslPath = new Path<EnrCancelExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrCancelExtract");
    }
            

    /**
     * @return Абстрактная выписка на абитуриента (2014). Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.order.entity.EnrCancelExtract#getEntity()
     */
    public static EnrAbstractExtract.Path<EnrAbstractExtract> entity()
    {
        return _dslPath.entity();
    }

    /**
     * @return Выбранный конкурс. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrCancelExtract#getRequestedCompetition()
     */
    public static EnrRequestedCompetition.Path<EnrRequestedCompetition> requestedCompetition()
    {
        return _dslPath.requestedCompetition();
    }

    public static class Path<E extends EnrCancelExtract> extends EnrAbstractExtract.Path<E>
    {
        private EnrAbstractExtract.Path<EnrAbstractExtract> _entity;
        private EnrRequestedCompetition.Path<EnrRequestedCompetition> _requestedCompetition;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Абстрактная выписка на абитуриента (2014). Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.order.entity.EnrCancelExtract#getEntity()
     */
        public EnrAbstractExtract.Path<EnrAbstractExtract> entity()
        {
            if(_entity == null )
                _entity = new EnrAbstractExtract.Path<EnrAbstractExtract>(L_ENTITY, this);
            return _entity;
        }

    /**
     * @return Выбранный конкурс. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrCancelExtract#getRequestedCompetition()
     */
        public EnrRequestedCompetition.Path<EnrRequestedCompetition> requestedCompetition()
        {
            if(_requestedCompetition == null )
                _requestedCompetition = new EnrRequestedCompetition.Path<EnrRequestedCompetition>(L_REQUESTED_COMPETITION, this);
            return _requestedCompetition;
        }

        public Class getEntityClass()
        {
            return EnrCancelExtract.class;
        }

        public String getEntityName()
        {
            return "enrCancelExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
