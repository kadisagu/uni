/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantListAdd.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantListAdd.EnrReportEntrantListAddUI;

/**
 * @author oleyba
 * @since 5/14/14
 */
public interface IEnrReportEntrantListDao extends INeedPersistenceSupport
{
    Long createReport(EnrReportEntrantListAddUI enrReportEntrantListAddUI);
}