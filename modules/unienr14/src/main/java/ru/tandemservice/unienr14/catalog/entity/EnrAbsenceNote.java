package ru.tandemservice.unienr14.catalog.entity;

import ru.tandemservice.unienr14.catalog.entity.codes.EnrAbsenceNoteCodes;
import ru.tandemservice.unienr14.catalog.entity.gen.EnrAbsenceNoteGen;

/**
 * Отметка в отношении отсутствующих абитуриентов
 */
public class EnrAbsenceNote extends EnrAbsenceNoteGen
{
    public boolean isValid()
    {
        return EnrAbsenceNoteCodes.VALID.equals(getCode());
    }
}