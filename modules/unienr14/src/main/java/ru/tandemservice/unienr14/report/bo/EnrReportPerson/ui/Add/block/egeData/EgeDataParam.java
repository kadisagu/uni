package ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.block.egeData;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IIdentifiableWrapper;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.hibsupport.dql.IDQLSelectableQuery;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.*;
import ru.tandemservice.unienr14.catalog.entity.EnrStateExamSubject;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.EnrReportPersonAddUI;

import java.util.ArrayList;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Vasily Zhukov
 * @since 08.02.2011
 */
public class EgeDataParam implements IReportDQLModifier
{
    private IReportParam<DataWrapper> _stateExamSecondWave = new ReportParam<>();
    private IReportParam<DataWrapper> _hasStateExamCertificate = new ReportParam<>();
    private IReportParam<List<IIdentifiableWrapper>> _certificateIssuanceYear = new ReportParam<>();
    private IReportParam<List<EnrStateExamSubject>> _stateExamSubject = new ReportParam<>();
    private IReportParam<DataWrapper> _stateExamResultAccepted = new ReportParam<>();
    private IReportParam<DataWrapper> _stateExamResultChecked = new ReportParam<>();
    private IReportParam<String> _stateExamResultCheckStatus = new ReportParam<>();
    private IReportParam<DataWrapper> _stateExamResultVerifiedByUser = new ReportParam<>();
    private IReportParam<String> _stateExamResultVerifiedByUserComment = new ReportParam<>();

    private String entrantAlias(ReportDQL dql)
    {
        // соединяем абитуриента
        String entrantAlias = dql.innerJoinEntity(EnrEntrant.class, EnrEntrant.person());

        // добавляем сортировку
        dql.order(entrantAlias, EnrEntrant.enrollmentCampaign().id());

        return entrantAlias;
    }

    private String stateExamResultAlias(ReportDQL dql)
    {
        // соединяем результат ЕГЭ
        return dql.innerJoinEntity(entrantAlias(dql), EnrEntrantStateExamResult.class, EnrEntrantStateExamResult.entrant());
    }

    @Override
    public void validateReportParams(ErrorCollector errorCollector)
    {

    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        if (_stateExamSecondWave.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "egeData.stateExamSecondWave", _stateExamSecondWave.getData().getTitle());

            dql.builder.where(eq(property(stateExamResultAlias(dql), EnrEntrantStateExamResult.secondWave()), value(TwinComboDataSourceHandler.getSelectedValueNotNull(_stateExamSecondWave.getData()))));
        }

        if (_hasStateExamCertificate.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "egeData.hasStateExamCertificate", _hasStateExamCertificate.getData().getTitle());

            String nextAlias = dql.nextAlias();

            IDQLSelectableQuery query = new DQLSelectBuilder().fromEntity(EnrEntrantStateExamResult.class, nextAlias)
                    .column(property(EnrEntrantStateExamResult.id().fromAlias(nextAlias)))
                    .where(eq(property(EnrEntrantStateExamResult.entrant().id().fromAlias(nextAlias)), property(EnrEntrant.id().fromAlias(entrantAlias(dql)))))
                    .buildQuery();

            dql.builder.where(TwinComboDataSourceHandler.getSelectedValueNotNull(_hasStateExamCertificate.getData()) ? exists(query) : notExists(query));
        }

        if (_certificateIssuanceYear.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "egeData.certificateIssuanceYear", CommonBaseStringUtil.join(_certificateIssuanceYear.getData(), "title", ", "));

            List<Integer> yearList = new ArrayList<>();
            for (IIdentifiableWrapper wrapper : _certificateIssuanceYear.getData())
                yearList.add(wrapper.getId().intValue());

            dql.builder.where(in(property(stateExamResultAlias(dql), EnrEntrantStateExamResult.year()), yearList));
        }

        if (_stateExamSubject.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "egeData.stateExamSubject", CommonBaseStringUtil.join(_stateExamSubject.getData(), EnrStateExamSubject.P_TITLE, ", "));

            dql.builder.where(in(property(stateExamResultAlias(dql), EnrEntrantStateExamResult.subject()), _stateExamSubject.getData()));
        }

        if (_stateExamResultAccepted.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "egeData.stateExamResultAccepted", _stateExamResultAccepted.getData().getTitle());

            dql.builder.where(eq(property(stateExamResultAlias(dql), EnrEntrantStateExamResult.accepted()), value(TwinComboDataSourceHandler.getSelectedValueNotNull(_stateExamResultAccepted.getData()))));
        }

        if (_stateExamResultChecked.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "egeData.stateExamResultChecked", _stateExamResultChecked.getData().getTitle());

            IDQLExpression property = property(stateExamResultAlias(dql), EnrEntrantStateExamResult.stateCheckStatus());
            boolean checked = TwinComboDataSourceHandler.getSelectedValueNotNull(_stateExamResultChecked.getData());

            dql.builder.where(checked ? isNotNull(property) : isNull(property));
        }

        if (_stateExamResultCheckStatus.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "egeData.stateExamResultCheckStatus", _stateExamResultCheckStatus.getData());

            dql.builder.where(likeUpper(property(stateExamResultAlias(dql), EnrEntrantStateExamResult.stateCheckStatus()), value(CoreStringUtils.escapeLike(_stateExamResultCheckStatus.getData()))));
        }

        if (_stateExamResultVerifiedByUser.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "egeData.stateExamResultVerifiedByUser", _stateExamResultVerifiedByUser.getData().getTitle());

            dql.builder.where(eq(property(stateExamResultAlias(dql), EnrEntrantStateExamResult.verifiedByUser()), value(TwinComboDataSourceHandler.getSelectedValueNotNull(_stateExamResultVerifiedByUser.getData()))));
        }

        if (_stateExamResultVerifiedByUserComment.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "egeData.stateExamResultVerifiedByUserComment", _stateExamResultVerifiedByUserComment.getData());

            dql.builder.where(likeUpper(property(stateExamResultAlias(dql), EnrEntrantStateExamResult.verifiedByUserComment()), value(CoreStringUtils.escapeLike(_stateExamResultVerifiedByUserComment.getData()))));
        }
    }

    // Getters


    public IReportParam<DataWrapper> getStateExamSecondWave()
    {
        return _stateExamSecondWave;
    }

    public IReportParam<DataWrapper> getHasStateExamCertificate()
    {
        return _hasStateExamCertificate;
    }

    public IReportParam<List<IIdentifiableWrapper>> getCertificateIssuanceYear()
    {
        return _certificateIssuanceYear;
    }

    public IReportParam<List<EnrStateExamSubject>> getStateExamSubject()
    {
        return _stateExamSubject;
    }

    public IReportParam<DataWrapper> getStateExamResultAccepted(){ return _stateExamResultAccepted; }

    public IReportParam<DataWrapper> getStateExamResultChecked(){ return _stateExamResultChecked; }

    public IReportParam<String> getStateExamResultCheckStatus(){ return _stateExamResultCheckStatus; }

    public IReportParam<DataWrapper> getStateExamResultVerifiedByUser(){ return _stateExamResultVerifiedByUser; }

    public IReportParam<String> getStateExamResultVerifiedByUserComment(){ return _stateExamResultVerifiedByUserComment; }
}
