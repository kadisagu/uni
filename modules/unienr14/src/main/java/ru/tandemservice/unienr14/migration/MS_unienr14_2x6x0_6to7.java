package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x0_6to7 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrRequestedCompetitionExclusive

		// создано обязательное свойство programSetItem
		{
			// создать колонку
			tool.createColumn("enr14_requested_comp_ex_t", new DBColumn("programsetitem_id", DBType.LONG));

            Map<Long, Long> reqComp2ProgramSetMap = new HashMap<>();
            {
                Statement statement = tool.getConnection().createStatement();
                statement.execute("" +
                    "select rce.id, ps.id " +
                    "from enr14_requested_comp_ex_t rce  " +
                    "inner join enr14_requested_comp_t rc on rc.id = rce.id " +
                    "inner join enr14_competition_t c on c.id = rc.competition_id " +
                    "inner join enr14_program_set_ou_t psou on psou.id = c.programsetorgunit_id " +
                    "inner join enr14_program_set_base_t ps on ps.id = psou.programset_id ");

                ResultSet resultSet = statement.getResultSet();
                while (resultSet.next())
                {
                    reqComp2ProgramSetMap.put(resultSet.getLong(1), resultSet.getLong(2));
                }
            }

            Map<Long, Long> programSet2ProgramMap = new HashMap<>();
            {
                Statement statement = tool.getConnection().createStatement();
                statement.execute("" +
                    "select programset_id, min(id) " +
                    "from enr14_program_set_item_t " +
                    "group by programset_id");

                ResultSet resultSet = statement.getResultSet();
                while (resultSet.next())
                {
                    programSet2ProgramMap.put(resultSet.getLong(1), resultSet.getLong(2));
                }
            }

            PreparedStatement preparedStatement = tool.getConnection().prepareStatement("update enr14_requested_comp_ex_t set programsetitem_id = ? where id = ?");
            for (Map.Entry<Long, Long> entry: reqComp2ProgramSetMap.entrySet())
            {
                preparedStatement.setLong(1, programSet2ProgramMap.get(entry.getValue()));
                preparedStatement.setLong(2, entry.getKey());
                preparedStatement.execute();
            }

			// сделать колонку NOT NULL
			tool.setColumnNullable("enr14_requested_comp_ex_t", "programsetitem_id", false);
		}
    }
}