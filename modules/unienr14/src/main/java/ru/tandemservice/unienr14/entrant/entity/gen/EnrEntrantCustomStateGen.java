package ru.tandemservice.unienr14.entrant.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantCustomStateType;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantCustomState;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Дополнительный статус абитуриента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrEntrantCustomStateGen extends EntityBase
 implements ILazyFieldOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.entrant.entity.EnrEntrantCustomState";
    public static final String ENTITY_NAME = "enrEntrantCustomState";
    public static final int VERSION_HASH = 1726350884;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTRANT = "entrant";
    public static final String L_CUSTOM_STATE = "customState";
    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_END_DATE = "endDate";
    public static final String P_DESCRIPTION = "description";

    private EnrEntrant _entrant;     // Абитуриент
    private EnrEntrantCustomStateType _customState;     // Статус
    private Date _beginDate;     // Дата начала действия статуса
    private Date _endDate;     // Дата окончания действия статуса
    private String _description;     // Примечание

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     */
    @NotNull
    public EnrEntrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant Абитуриент. Свойство не может быть null.
     */
    public void setEntrant(EnrEntrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    /**
     * @return Статус. Свойство не может быть null.
     */
    @NotNull
    public EnrEntrantCustomStateType getCustomState()
    {
        return _customState;
    }

    /**
     * @param customState Статус. Свойство не может быть null.
     */
    public void setCustomState(EnrEntrantCustomStateType customState)
    {
        dirty(_customState, customState);
        _customState = customState;
    }

    /**
     * @return Дата начала действия статуса.
     */
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата начала действия статуса.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Дата окончания действия статуса.
     */
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания действия статуса.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Примечание.
     */
    public String getDescription()
    {
        initLazyForGet("description");
        return _description;
    }

    /**
     * @param description Примечание.
     */
    public void setDescription(String description)
    {
        initLazyForSet("description");
        dirty(_description, description);
        _description = description;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrEntrantCustomStateGen)
        {
            setEntrant(((EnrEntrantCustomState)another).getEntrant());
            setCustomState(((EnrEntrantCustomState)another).getCustomState());
            setBeginDate(((EnrEntrantCustomState)another).getBeginDate());
            setEndDate(((EnrEntrantCustomState)another).getEndDate());
            setDescription(((EnrEntrantCustomState)another).getDescription());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrEntrantCustomStateGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrEntrantCustomState.class;
        }

        public T newInstance()
        {
            return (T) new EnrEntrantCustomState();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entrant":
                    return obj.getEntrant();
                case "customState":
                    return obj.getCustomState();
                case "beginDate":
                    return obj.getBeginDate();
                case "endDate":
                    return obj.getEndDate();
                case "description":
                    return obj.getDescription();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entrant":
                    obj.setEntrant((EnrEntrant) value);
                    return;
                case "customState":
                    obj.setCustomState((EnrEntrantCustomStateType) value);
                    return;
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "description":
                    obj.setDescription((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entrant":
                        return true;
                case "customState":
                        return true;
                case "beginDate":
                        return true;
                case "endDate":
                        return true;
                case "description":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entrant":
                    return true;
                case "customState":
                    return true;
                case "beginDate":
                    return true;
                case "endDate":
                    return true;
                case "description":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entrant":
                    return EnrEntrant.class;
                case "customState":
                    return EnrEntrantCustomStateType.class;
                case "beginDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "description":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrEntrantCustomState> _dslPath = new Path<EnrEntrantCustomState>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrEntrantCustomState");
    }
            

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantCustomState#getEntrant()
     */
    public static EnrEntrant.Path<EnrEntrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return Статус. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantCustomState#getCustomState()
     */
    public static EnrEntrantCustomStateType.Path<EnrEntrantCustomStateType> customState()
    {
        return _dslPath.customState();
    }

    /**
     * @return Дата начала действия статуса.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantCustomState#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Дата окончания действия статуса.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantCustomState#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Примечание.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantCustomState#getDescription()
     */
    public static PropertyPath<String> description()
    {
        return _dslPath.description();
    }

    public static class Path<E extends EnrEntrantCustomState> extends EntityPath<E>
    {
        private EnrEntrant.Path<EnrEntrant> _entrant;
        private EnrEntrantCustomStateType.Path<EnrEntrantCustomStateType> _customState;
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Date> _endDate;
        private PropertyPath<String> _description;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantCustomState#getEntrant()
     */
        public EnrEntrant.Path<EnrEntrant> entrant()
        {
            if(_entrant == null )
                _entrant = new EnrEntrant.Path<EnrEntrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return Статус. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantCustomState#getCustomState()
     */
        public EnrEntrantCustomStateType.Path<EnrEntrantCustomStateType> customState()
        {
            if(_customState == null )
                _customState = new EnrEntrantCustomStateType.Path<EnrEntrantCustomStateType>(L_CUSTOM_STATE, this);
            return _customState;
        }

    /**
     * @return Дата начала действия статуса.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantCustomState#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(EnrEntrantCustomStateGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Дата окончания действия статуса.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantCustomState#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(EnrEntrantCustomStateGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Примечание.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantCustomState#getDescription()
     */
        public PropertyPath<String> description()
        {
            if(_description == null )
                _description = new PropertyPath<String>(EnrEntrantCustomStateGen.P_DESCRIPTION, this);
            return _description;
        }

        public Class getEntityClass()
        {
            return EnrEntrantCustomState.class;
        }

        public String getEntityName()
        {
            return "enrEntrantCustomState";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
