package ru.tandemservice.unienr14.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Категория лиц, обладающих особыми правами при поступлении"
 * Имя сущности : enrBenefitCategory
 * Файл data.xml : unienr.catalog.data.xml
 */
public interface EnrBenefitCategoryCodes
{
    /** Константа кода (code) элемента : Члены сборных команд (title) */
    String BEZ_V_I_CHLENY_SBORNYH_KOMAND = "001";
    /** Константа кода (code) элемента : Победители всероссийской олимпиады школьников (IV этапа всеукраинских ученических олимпиад) (title) */
    String BEZ_V_I_POBEDITELI_VSEROSSIYSKOY_OLIMPIADY_SHKOLNIKOV = "002";
    /** Константа кода (code) элемента : Призеры всероссийской олимпиады школьников (IV этапа всеукраинских ученических олимпиад) (title) */
    String BEZ_V_I_PRIZERY_VSEROSSIYSKOY_OLIMPIADY_SHKOLNIKOV = "003";
    /** Константа кода (code) элемента : Чемпионы и призеры в области спорта (title) */
    String BEZ_V_I_CHEMPIONY_I_PRIZERY_V_OBLASTI_SPORTA = "004";
    /** Константа кода (code) элемента : Победители олимпиад школьников (title) */
    String BEZ_V_I_POBEDITELI_OLIMPIAD_SHKOLNIKOV = "005";
    /** Константа кода (code) элемента : Призеры олимпиад школьников (title) */
    String BEZ_V_I_PRIZERY_OLIMPIAD_SHKOLNIKOV = "006";
    /** Константа кода (code) элемента : Инвалиды (title) */
    String KVOTA_INVALIDY = "007";
    /** Константа кода (code) элемента : Дети-сироты и оставшиеся без попечения родителей (title) */
    String KVOTA_DETI_SIROTY_I_OSTAVSHIESYA_BEZ_POPECHENIYA_RODITELEY = "008";
    /** Константа кода (code) элемента : Дети-сироты и оставшиеся без попечения родителей (title) */
    String PREIM_PRAVO_DETI_SIROTY_I_OSTAVSHIESYA_BEZ_POPECHENIYA_RODITELEY = "009";
    /** Константа кода (code) элемента : Инвалиды (title) */
    String PREIM_PRAVO_INVALIDY = "010";
    /** Константа кода (code) элемента : Родитель-инвалид (title) */
    String PREIM_PRAVO_RODITEL_INVALID = "011";
    /** Константа кода (code) элемента : Подвергшиеся воздействию радиации ЧАЭС (title) */
    String PREIM_PRAVO_PODVERGSHIESYA_VOZDEYSTVIYU_RADIATSII_CH_A_E_S = "012";
    /** Константа кода (code) элемента : Дети погибших военнослужащих (title) */
    String PREIM_PRAVO_DETI_POGIBSHIH_VOENNOSLUJATSHIH = "013";
    /** Константа кода (code) элемента : Дети погибших Героев СССР, РФ (title) */
    String PREIM_PRAVO_DETI_POGIBSHIH_GEROEV_S_S_S_R_R_F = "014";
    /** Константа кода (code) элемента : Дети погибших сотрудников гос. органов (title) */
    String PREIM_PRAVO_DETI_POGIBSHIH_SOTRUDNIKOV_GOS_ORGANOV = "015";
    /** Константа кода (code) элемента : Дети погибших прокурорских работников (title) */
    String PREIM_PRAVO_DETI_POGIBSHIH_PROKURORSKIH_RABOTNIKOV = "016";
    /** Константа кода (code) элемента : Военнослужащие по контракту (title) */
    String PREIM_PRAVO_VOENNOSLUJATSHIE_PO_KONTRAKTU = "017";
    /** Константа кода (code) элемента : Уволенные с военной службы (title) */
    String PREIM_PRAVO_UVOLENNYE_S_VOENNOY_SLUJBY = "018";
    /** Константа кода (code) элемента : Ветераны боевых действий (title) */
    String PREIM_PRAVO_VETERANY_BOEVYH_DEYSTVIY = "019";
    /** Константа кода (code) элемента : Ликвидаторы радиационных аварий (title) */
    String PREIM_PRAVO_LIKVIDATORY_RADIATSIONNYH_AVARIY = "020";
    /** Константа кода (code) элемента : Участники вооруженного конфликта в Чеченской республике (title) */
    String PREIM_PRAVO_UCHASTNIKI_VOORUJENNOGO_KONFLIKTA_V_CHECHENSKOY_RESPUBLIKE = "021";
    /** Константа кода (code) элемента : Члены сборных команд (title) */
    String STO_BALLOV_CHLENY_SBORNYH_KOMAND = "022";
    /** Константа кода (code) элемента : Победители и призеры всероссийской олимпиады школьников (IV этапа всеукраинских ученических олимпиад) (title) */
    String STO_BALLOV_POBEDITELI_I_PRIZERY_VSEROSSIYSKOY_OLIMPIADY_SHKOLNIKOV = "023";
    /** Константа кода (code) элемента : Чемпионы и призеры в области спорта (title) */
    String STO_BALLOV_CHEMPIONY_I_PRIZERY_V_OBLASTI_SPORTA = "024";
    /** Константа кода (code) элемента : Победители и призеры олимпиад школьников (title) */
    String STO_BALLOV_POBEDITELI_I_PRIZERY_OLIMPIAD_SHKOLNIKOV = "025";
    /** Константа кода (code) элемента : Ветераны боевых действий (title) */
    String KVOTA_VETERANY_BOEVYH_DEYSTVIY = "026";
    /** Константа кода (code) элемента : Выпускники с подготовкой к военной или иной государственной службе (title) */
    String PREIM_PRAVO_VYPUSKNIKI_S_PODGOTOVKOY_K_VOENNOY_ILI_INOY_GOSUDARSTVENNOY_SLUJBE = "027";

    Set<String> CODES = ImmutableSet.of(BEZ_V_I_CHLENY_SBORNYH_KOMAND, BEZ_V_I_POBEDITELI_VSEROSSIYSKOY_OLIMPIADY_SHKOLNIKOV, BEZ_V_I_PRIZERY_VSEROSSIYSKOY_OLIMPIADY_SHKOLNIKOV, BEZ_V_I_CHEMPIONY_I_PRIZERY_V_OBLASTI_SPORTA, BEZ_V_I_POBEDITELI_OLIMPIAD_SHKOLNIKOV, BEZ_V_I_PRIZERY_OLIMPIAD_SHKOLNIKOV, KVOTA_INVALIDY, KVOTA_DETI_SIROTY_I_OSTAVSHIESYA_BEZ_POPECHENIYA_RODITELEY, PREIM_PRAVO_DETI_SIROTY_I_OSTAVSHIESYA_BEZ_POPECHENIYA_RODITELEY, PREIM_PRAVO_INVALIDY, PREIM_PRAVO_RODITEL_INVALID, PREIM_PRAVO_PODVERGSHIESYA_VOZDEYSTVIYU_RADIATSII_CH_A_E_S, PREIM_PRAVO_DETI_POGIBSHIH_VOENNOSLUJATSHIH, PREIM_PRAVO_DETI_POGIBSHIH_GEROEV_S_S_S_R_R_F, PREIM_PRAVO_DETI_POGIBSHIH_SOTRUDNIKOV_GOS_ORGANOV, PREIM_PRAVO_DETI_POGIBSHIH_PROKURORSKIH_RABOTNIKOV, PREIM_PRAVO_VOENNOSLUJATSHIE_PO_KONTRAKTU, PREIM_PRAVO_UVOLENNYE_S_VOENNOY_SLUJBY, PREIM_PRAVO_VETERANY_BOEVYH_DEYSTVIY, PREIM_PRAVO_LIKVIDATORY_RADIATSIONNYH_AVARIY, PREIM_PRAVO_UCHASTNIKI_VOORUJENNOGO_KONFLIKTA_V_CHECHENSKOY_RESPUBLIKE, STO_BALLOV_CHLENY_SBORNYH_KOMAND, STO_BALLOV_POBEDITELI_I_PRIZERY_VSEROSSIYSKOY_OLIMPIADY_SHKOLNIKOV, STO_BALLOV_CHEMPIONY_I_PRIZERY_V_OBLASTI_SPORTA, STO_BALLOV_POBEDITELI_I_PRIZERY_OLIMPIAD_SHKOLNIKOV, KVOTA_VETERANY_BOEVYH_DEYSTVIY, PREIM_PRAVO_VYPUSKNIKI_S_PODGOTOVKOY_K_VOENNOY_ILI_INOY_GOSUDARSTVENNOY_SLUJBE);
}
