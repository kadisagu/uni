/* $Id:$ */
package ru.tandemservice.unienr14.student.bo.EnrStudent.ui.Tab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unienr14.order.entity.EnrAllocationExtract;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;

/**
 * @author oleyba
 * @since 8/14/14
 */
@State({
    @Bind(key = "publisherId", binding = "studentId")
})
public class EnrStudentTabUI extends UIPresenter
{
    private Long _studentId;
    private EnrEnrollmentExtract _enrollmentExtract;
    private EnrAllocationExtract _allocationExtract;

    @Override
    public void onComponentRefresh()
    {
        setAllocationExtract(DataAccessServices.dao().get(EnrAllocationExtract.class, EnrAllocationExtract.student().id(), getStudentId()));
        setEnrollmentExtract(DataAccessServices.dao().get(EnrEnrollmentExtract.class, EnrEnrollmentExtract.student().id(), getStudentId()));
    }

    // getters and setters

    public Long getStudentId()
    {
        return _studentId;
    }

    public void setStudentId(Long studentId)
    {
        _studentId = studentId;
    }

    public EnrAllocationExtract getAllocationExtract()
    {
        return _allocationExtract;
    }

    public void setAllocationExtract(EnrAllocationExtract allocationExtract)
    {
        _allocationExtract = allocationExtract;
    }

    public EnrEnrollmentExtract getEnrollmentExtract()
    {
        return _enrollmentExtract;
    }

    public void setEnrollmentExtract(EnrEnrollmentExtract enrollmentExtract)
    {
        _enrollmentExtract = enrollmentExtract;
    }
}