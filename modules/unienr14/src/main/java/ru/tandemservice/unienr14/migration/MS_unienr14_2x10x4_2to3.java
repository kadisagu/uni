package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.ISQLTranslator;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLSelectQuery;
import org.tandemframework.dbsupport.sql.SQLUpdateQuery;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

import java.util.List;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unienr14_2x10x4_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.10.4")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ISQLTranslator translator = tool.getDialect().getSQLTranslator();

        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrEnrollmentCampaignSettings

        // создано обязательное свойство excludeEnrolledByLowPriority
        if(!tool.columnExists("enr14_campaign_settings_t", "excludeenrolledbylowpriority_p"))
        {
            // создать колонку
            tool.createColumn("enr14_campaign_settings_t", new DBColumn("excludeenrolledbylowpriority_p", DBType.BOOLEAN));

            tool.executeUpdate("update enr14_campaign_settings_t set excludeenrolledbylowpriority_p=? where enrollmentcampaigntype_id is null", true);

            SQLSelectQuery selectQuery = new SQLSelectQuery().from(SQLFrom.table("enr14_campaign_settings_t", "s")
                    .innerJoin(SQLFrom.table("enr14_c_campaign_type_t", "ct"), "ct.id=s.enrollmentcampaigntype_id"))
                    .column("s.id")
                    .column("ct.code_p");



            List<Object[]> rows = tool.executeQuery(MigrationUtils.processor(Long.class, String.class), translator.toSql(selectQuery));

            SQLUpdateQuery updateQuery = new SQLUpdateQuery("enr14_campaign_settings_t", "s")
                    .set("excludeenrolledbylowpriority_p", "?")
                    .where("s.id=?");

            for(Object[] row : rows)
            {
                Long settingsId = (Long) row[0];
                String typeCode = (String) row[1];

                // EnrEnrollmentCampaignTypeCodes  Прием на обучение по программам бакалавриата/специалитета BS = "1";
                if("1".equals(typeCode))
                {
                    tool.executeUpdate(translator.toSql(updateQuery), false, settingsId);
                }
                else
                {
                    tool.executeUpdate(translator.toSql(updateQuery), true, settingsId);
                }
            }

            // сделать колонку NOT NULL
            tool.setColumnNullable("enr14_campaign_settings_t", "excludeenrolledbylowpriority_p", false);
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrRatingItem

        SQLUpdateQuery ratingUpdateQuery = new SQLUpdateQuery("enr14_rating_item_t", "ri");

        ratingUpdateQuery.getUpdatedTableFrom()
                .innerJoin(SQLFrom.table("enr14_entrant_t", "en"), "en.id=ri.entrant_id")
                .innerJoin(SQLFrom.table("enr14_campaign_t", "ec"), "ec.id=en.enrollmentcampaign_id")
                .innerJoin(SQLFrom.table("educationyear_t", "ey"), "ey.id=ec.educationyear_id");

        ratingUpdateQuery.set("excludedbystepitem_id", "?")
                .set("xclddbyextrctmnstrl_id", "?")
                .where("ey.intvalue_p >= ?");

        tool.executeUpdate(translator.toSql(ratingUpdateQuery), null, null, 2016);
    }
}