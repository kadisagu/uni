/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.logic.IEnrEnrollmentModelDao;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.logic.EnrEnrollmentModelDao;

/**
 * @author oleyba
 * @since 3/27/15
 */
@Configuration
public class EnrEnrollmentModelManager extends BusinessObjectManager
{
    public static EnrEnrollmentModelManager instance()
    {
        return instance(EnrEnrollmentModelManager.class);
    }

    @Bean
    public IEnrEnrollmentModelDao dao()
    {
        return new EnrEnrollmentModelDao();
    }
}
