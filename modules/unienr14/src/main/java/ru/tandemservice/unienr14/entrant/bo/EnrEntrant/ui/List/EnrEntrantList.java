/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.List;

import com.google.common.collect.Lists;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantCustomStateType;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantState;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.formatter.EnrEntrantCustomStateCollectionFormatter;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.logic.EnrEntrantDSHandler;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.EnrEntrantRequestManager;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.logic.ExternalOrgUnitDSHandler;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.EnrEnrollmentCommissionManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.Arrays;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 4/10/13
 */
@Configuration
public class EnrEntrantList extends BusinessComponentManager
{
    public static final String ENTRANT_DS = "entrantDS";
    public static final String ENTRANT_CUSTOM_STATE_DS = "entrantCustomStateDS";
    public static final String ACCEPT_REQUEST_DS = "acceptRequestDS";
    public static final String TARGET_CONTRACT_OU_DS = "targetContractOUDS";

    public static final String PARAM_ENROLLMENT_COMMISSION = "enrCommissions";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
                .addDataSource(EnrEnrollmentCommissionManager.instance().enrollmentCommissionDSConfig())
                .addDataSource(selectDS("entrantStateDS", entrantStateDSHandler()))
                .addDataSource(selectDS(ENTRANT_CUSTOM_STATE_DS, entrantCustomStateDSHandler()))
                .addDataSource(selectDS("archivalDS", archivalDSHandler()))
                .addDataSource(searchListDS(ENTRANT_DS, entrantDSColumns(), entrantDSHandler()))
                .addDataSource(selectDS(ACCEPT_REQUEST_DS, EnrEntrantRequestManager.instance().acceptRequestDSHandler()))
                .addDataSource(selectDS(TARGET_CONTRACT_OU_DS, targetContractOrgUnits()).addColumn(ExternalOrgUnit.titleWithLegalForm().s()))
                .addAddon(uiAddon(CommonFilterAddon.class.getSimpleName(), EnrCompetitionFilterAddon.class))
                .create();
    }

    @Bean
    public ColumnListExtPoint entrantDSColumns()
    {
        return columnListExtPointBuilder(ENTRANT_DS)
            .addColumn(textColumn("personalNumber", EnrEntrant.personalNumber()).order())
            .addColumn(publisherColumn("requestNumber", "title")
                .entityListProperty(EnrEntrantDSHandler.VIEW_PROP_REQUEST)
                .formatter(CollectionFormatter.COLLECTION_FORMATTER))
            .addColumn(publisherColumn("entrant", PersonRole.person().identityCard().fullFio()).required(Boolean.TRUE).parameters("mvel:['selectedTab':'requestTab']").order())
            .addColumn(textColumn("customState", EnrEntrantDSHandler.VIEW_PROP_CUSTOM_STATES).formatter(new EnrEntrantCustomStateCollectionFormatter()))
            .addColumn(textColumn("sex", PersonRole.person().identityCard().sex().shortTitle()).order())
            .addColumn(textColumn("passport", PersonRole.person().fullIdentityCardNumber()))
            .addColumn(textColumn("birthDate", PersonRole.person().identityCard().birthDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order())
            .addColumn(textColumn("state", EnrEntrant.state().stateDaemonSafe().s()))
            .addColumn(textColumn("registrationDate", EnrEntrant.registrationDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order())
            .addColumn(textColumn("workPlace", EnrEntrant.person().workPlace()))
            .addColumn(toggleColumn("status", "status")
                               .toggleOffListener("onClickSwitchEntrantArchival").toggleOnListener("onClickSwitchEntrantArchival")
                               .toggleOffLabel("Абитуриент в архиве").toggleOnLabel("Абитуриент не в архиве")
                               .toggleOffListenerAlert(alert("Списать абитуриента «{0}» в архив?", EnrEntrant.P_FULL_FIO))
                               .toggleOnListenerAlert(alert("Восстановить абитуриента «{0}» из архива?", EnrEntrant.P_FULL_FIO))
                               .permissionKey("enr14EntrantListEntrantArchive"))
            .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, "onClickDeleteEntrant").permissionKey("enr14EntrantListEntrantDelete").alert(alert("Удалить абитуриента «{0}»?", EnrEntrant.P_FULL_FIO)))
        .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler entrantDSHandler()
    {
        return new EnrEntrantDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler entrantStateDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrEntrantState.class)
                .order(EnrEntrantState.title())
                .filter(EnrEntrantState.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler entrantCustomStateDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrEntrantCustomStateType.class)
                .order(EnrEntrantCustomStateType.title())
                .filter(EnrEntrantCustomStateType.title());
    }

    @Bean IDefaultComboDataSourceHandler archivalDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
                .addAll(Arrays.asList(
                        new IdentifiableWrapper(EnrEntrantListUI.SHOW_ARCHIVAL_CODE, "Показывать архивных"),
                        new IdentifiableWrapper(EnrEntrantListUI.SHOW_NON_ARCHIVAL_CODE, "Показывать не архивных")
                ))
                .filtered(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler targetContractOrgUnits()
    {
        return new ExternalOrgUnitDSHandler(getName())
        {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
//                super.prepareConditions(ep);
                EnrEnrollmentCampaign campaign = ep.context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
                List<EnrEnrollmentCommission> commissions = ep.context.get(PARAM_ENROLLMENT_COMMISSION);

                DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(EnrRequestedCompetition.class, "rComp")
                        .where(eq(property("rComp", EnrRequestedCompetition.request().entrant().enrollmentCampaign()), value(campaign)));
                if (commissions != null && !commissions.isEmpty())
                    subBuilder.where(eq(property("rComp", EnrRequestedCompetition.request().enrollmentCommission()), value(commissions.get(0))));
                subBuilder.where(in(property("rComp", EnrRequestedCompetition.competition().type().code()), Lists.newArrayList(EnrCompetitionTypeCodes.CONTRACT, EnrCompetitionTypeCodes.NO_EXAM_CONTRACT)))
                        .column(property("rComp", EnrRequestedCompetition.externalOrgUnit().id()));

                ep.dqlBuilder.where(in(property("e", ExternalOrgUnit.id()), subBuilder.buildQuery()));
            }
        };
    }
}
