package ru.tandemservice.unienr14.catalog.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.catalog.entity.EnrMethodDivCompetitions;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Способ деления конкурсов
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrMethodDivCompetitionsGen extends EntityBase
 implements INaturalIdentifiable<EnrMethodDivCompetitionsGen>, org.tandemframework.common.catalog.entity.ICatalogItem, ILazyFieldOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.catalog.entity.EnrMethodDivCompetitions";
    public static final String ENTITY_NAME = "enrMethodDivCompetitions";
    public static final int VERSION_HASH = -899884119;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_DESCRIPTION = "description";
    public static final String L_ENR_REQUEST_TYPE = "enrRequestType";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private String _shortTitle;     // Сокращенное название
    private String _description;     // Описание
    private EnrRequestType _enrRequestType;     // Вид заявления
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название. Свойство не может быть null.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return Описание. Свойство не может быть null.
     */
    @NotNull
    public String getDescription()
    {
        initLazyForGet("description");
        return _description;
    }

    /**
     * @param description Описание. Свойство не может быть null.
     */
    public void setDescription(String description)
    {
        initLazyForSet("description");
        dirty(_description, description);
        _description = description;
    }

    /**
     * @return Вид заявления. Свойство не может быть null.
     */
    @NotNull
    public EnrRequestType getEnrRequestType()
    {
        return _enrRequestType;
    }

    /**
     * @param enrRequestType Вид заявления. Свойство не может быть null.
     */
    public void setEnrRequestType(EnrRequestType enrRequestType)
    {
        dirty(_enrRequestType, enrRequestType);
        _enrRequestType = enrRequestType;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrMethodDivCompetitionsGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((EnrMethodDivCompetitions)another).getCode());
            }
            setShortTitle(((EnrMethodDivCompetitions)another).getShortTitle());
            setDescription(((EnrMethodDivCompetitions)another).getDescription());
            setEnrRequestType(((EnrMethodDivCompetitions)another).getEnrRequestType());
            setTitle(((EnrMethodDivCompetitions)another).getTitle());
        }
    }

    public INaturalId<EnrMethodDivCompetitionsGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<EnrMethodDivCompetitionsGen>
    {
        private static final String PROXY_NAME = "EnrMethodDivCompetitionsNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrMethodDivCompetitionsGen.NaturalId) ) return false;

            EnrMethodDivCompetitionsGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrMethodDivCompetitionsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrMethodDivCompetitions.class;
        }

        public T newInstance()
        {
            return (T) new EnrMethodDivCompetitions();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "shortTitle":
                    return obj.getShortTitle();
                case "description":
                    return obj.getDescription();
                case "enrRequestType":
                    return obj.getEnrRequestType();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "description":
                    obj.setDescription((String) value);
                    return;
                case "enrRequestType":
                    obj.setEnrRequestType((EnrRequestType) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "shortTitle":
                        return true;
                case "description":
                        return true;
                case "enrRequestType":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "shortTitle":
                    return true;
                case "description":
                    return true;
                case "enrRequestType":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "shortTitle":
                    return String.class;
                case "description":
                    return String.class;
                case "enrRequestType":
                    return EnrRequestType.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrMethodDivCompetitions> _dslPath = new Path<EnrMethodDivCompetitions>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrMethodDivCompetitions");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrMethodDivCompetitions#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrMethodDivCompetitions#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return Описание. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrMethodDivCompetitions#getDescription()
     */
    public static PropertyPath<String> description()
    {
        return _dslPath.description();
    }

    /**
     * @return Вид заявления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrMethodDivCompetitions#getEnrRequestType()
     */
    public static EnrRequestType.Path<EnrRequestType> enrRequestType()
    {
        return _dslPath.enrRequestType();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrMethodDivCompetitions#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EnrMethodDivCompetitions> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<String> _description;
        private EnrRequestType.Path<EnrRequestType> _enrRequestType;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrMethodDivCompetitions#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(EnrMethodDivCompetitionsGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrMethodDivCompetitions#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(EnrMethodDivCompetitionsGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return Описание. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrMethodDivCompetitions#getDescription()
     */
        public PropertyPath<String> description()
        {
            if(_description == null )
                _description = new PropertyPath<String>(EnrMethodDivCompetitionsGen.P_DESCRIPTION, this);
            return _description;
        }

    /**
     * @return Вид заявления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrMethodDivCompetitions#getEnrRequestType()
     */
        public EnrRequestType.Path<EnrRequestType> enrRequestType()
        {
            if(_enrRequestType == null )
                _enrRequestType = new EnrRequestType.Path<EnrRequestType>(L_ENR_REQUEST_TYPE, this);
            return _enrRequestType;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrMethodDivCompetitions#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EnrMethodDivCompetitionsGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EnrMethodDivCompetitions.class;
        }

        public String getEntityName()
        {
            return "enrMethodDivCompetitions";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
