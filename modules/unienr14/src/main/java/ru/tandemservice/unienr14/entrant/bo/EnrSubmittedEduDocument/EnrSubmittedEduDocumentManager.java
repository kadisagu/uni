/* $Id$ */
package ru.tandemservice.unienr14.entrant.bo.EnrSubmittedEduDocument;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unienr14.entrant.bo.EnrSubmittedEduDocument.logic.EnrSubmittedEduDocumentDao;
import ru.tandemservice.unienr14.entrant.bo.EnrSubmittedEduDocument.logic.IEnrSubmittedEduDocumentDao;

/**
 * @author azhebko
 * @since 31.07.2014
 */
@Configuration
public class EnrSubmittedEduDocumentManager extends BusinessObjectManager
{
    public static EnrSubmittedEduDocumentManager instance() { return instance(EnrSubmittedEduDocumentManager.class); }

    @Bean
    public IEnrSubmittedEduDocumentDao dao()
    {
        return new EnrSubmittedEduDocumentDao();
    }
}