/**
 *$Id: EnrStateExamResultListUI.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.exams.bo.EnrStateExamResult.ui.List;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.tapsupport.component.selection.ListResult;
import org.tandemframework.tapsupport.component.selection.SingleSelectTextModel;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult;
import ru.tandemservice.unienr14.exams.bo.EnrStateExamResult.EnrStateExamResultManager;
import ru.tandemservice.unienr14.exams.bo.EnrStateExamResult.logic.EnrStateExamResultSearchDS;
import ru.tandemservice.unienr14.exams.bo.EnrStateExamResult.ui.AddEdit.EnrStateExamResultAddEdit;
import ru.tandemservice.unienr14.exams.bo.EnrStateExamResult.ui.AddEdit.EnrStateExamResultAddEditUI;
import ru.tandemservice.unienr14.exams.bo.EnrStateExamResult.ui.Export.EnrStateExamResultExport;
import ru.tandemservice.unienr14.exams.bo.EnrStateExamResult.ui.Import.EnrStateExamResultImport;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 13.06.13
 */
public class EnrStateExamResultListUI extends UIPresenter
{
    private EnrEnrollmentCampaign _enrCampaign;

    private String stateFis;

    private boolean notVerified;

    // к сожалению, CAF этого не умеет
    private final SingleSelectTextModel _stateFisModel = new SingleSelectTextModel()
    {
        @Override
        public ListResult findValues(final String filter)
        {
            final DQLSelectBuilder dql = new DQLSelectBuilder()
                    .fromEntity(EnrEntrantStateExamResult.class, "e")
                    .column(property(EnrEntrantStateExamResult.stateCheckStatus().fromAlias("e")))
                    .predicate(DQLPredicateType.distinct)
                    .where(likeUpper(property(EnrEntrantStateExamResult.stateCheckStatus().fromAlias("e")), value(CoreStringUtils.escapeLike(filter, true))))
                    .order(property(EnrEntrantStateExamResult.stateCheckStatus().fromAlias("e")));
            final List<String> list = dql.createStatement(EnrStateExamResultListUI.this.getSupport().getSession()).setMaxResults(50).list();
            final Number number = dql.createCountStatement(new DQLExecutionContext(EnrStateExamResultListUI.this.getSupport().getSession())).uniqueResult();
            return new ListResult<String>(list, number == null ? 0 : number.intValue());
        }
    };

    @Override
    public void onComponentRefresh()
    {
        _enrCampaign = EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign();
    }

    public void onToggleAccepted()
    {
        EnrStateExamResultManager.instance().dao().doChangeResultChecked(getListenerParameterAsLong());
    }

    public void onToggleVerified()
    {
        EnrStateExamResultManager.instance().dao().doChangeResultVerifiedByUser(getListenerParameterAsLong());
    }

    public void onClickEdit()
    {
        final EnrEntrantStateExamResult entity = getEntityByListenerParameterAsLong();

        getActivationBuilder().asRegionDialog(EnrStateExamResultAddEdit.class)
                .parameter(EnrStateExamResultAddEditUI.BIND_ENTRANT, entity.getEntrant().getId())
                .parameter(EnrStateExamResultAddEditUI.BIND_RESULT, entity.getId())
                .activate();
    }

    public void onClickExportEntrantStateExamResults()
    {
        getActivationBuilder().asCurrent(EnrStateExamResultExport.class).activate();
    }

    public void onClickImportEntrantStateExamResults()
    {
        getActivationBuilder().asCurrent(EnrStateExamResultImport.class).activate();
    }

    public void onClickDelete()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public void onEnrCampaignRefresh()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(_enrCampaign);
    }

    public boolean isNothingSelected()
    {
        return _enrCampaign == null;
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, _enrCampaign);
        dataSource.put(EnrStateExamResultSearchDS.BIND_LAST_NAME, getSettings().get("lastName"));
        dataSource.put(EnrStateExamResultSearchDS.BIND_IC_SERIA, getSettings().get("icSeria"));
        dataSource.put(EnrStateExamResultSearchDS.BIND_IC_NUMBER, getSettings().get("icNumber"));
        dataSource.put(EnrStateExamResultSearchDS.BIND_ADDED_DATE_FROM, getSettings().get("addedDateFrom"));
        dataSource.put(EnrStateExamResultSearchDS.BIND_ADDED_DATE_TO, getSettings().get("addedDateTo"));
        dataSource.put(EnrStateExamResultSearchDS.BIND_CERT_NUMB_TERRITORY_CODE, getSettings().get("certTerritoryCode"));
        dataSource.put(EnrStateExamResultSearchDS.BIND_CERT_NUMB_NUMBER, getSettings().get("certNumber"));
        dataSource.put(EnrStateExamResultSearchDS.BIND_CERT_NUMB_YEAR, getSettings().get("certYear"));
        dataSource.put(EnrStateExamResultSearchDS.BIND_ACCEPTED, TwinComboDataSourceHandler.getSelectedValue(getSettings().get("accepted")));
        dataSource.put(EnrStateExamResultSearchDS.BIND_VERIFIED_BY_USER, TwinComboDataSourceHandler.getSelectedValue(getSettings().get("verifiedByUser")));
        dataSource.put(EnrStateExamResultSearchDS.BIND_NOT_VERIFIED, getSettings().get("notVerified"));
        dataSource.put(EnrStateExamResultSearchDS.BIND_STATE_FIS, getSettings().get("stateFis"));
        dataSource.put(EnrStateExamResultSearchDS.BIND_SECOND_WAVE, TwinComboDataSourceHandler.getSelectedValue(getSettings().get("secondWave")));
        dataSource.put(EnrStateExamResultSearchDS.BIND_STATE_EXAM_SUBJECT, getSettings().get("subject"));
    }

    // Getters & Setters

    //Accessors

    public EnrEnrollmentCampaign getEnrCampaign()
    {
        return _enrCampaign;
    }

    public void setEnrCampaign(EnrEnrollmentCampaign enrCampaign)
    {
        _enrCampaign = enrCampaign;
    }


    public String getEditAlert()
    {
        EnrEntrantStateExamResult result = _uiConfig.getDataSource(EnrStateExamResultList.STATE_EXAM_CERTIFICATE_SEARCH_DS).getCurrent();
        if(result != null && !StringUtils.isEmpty(result.getStateCheckStatus()))
        {
            return "При изменении балла или номера свидетельства для результата ЕГЭ будет сброшен «Статус проверки в ФИС». Продолжить редактирование?";
        }
        return null;
    }

    public boolean isNotVerified() {
        return notVerified;
    }

    public void setNotVerified(boolean notVerified) {
        this.notVerified = notVerified;
        getSettings().set("notVerified", notVerified);
        if (notVerified)
            setStateFis(null);
    }

    public String getStateFis() {
        return stateFis;
    }

    public void setStateFis(String stateFis) {
        this.stateFis = stateFis;
        getSettings().set("stateFis", stateFis);
    }

    public void clearStateFis(){
        setStateFis("");
    }

    @Override
    public void clearSettings() {
        setNotVerified(false);
        super.clearSettings();
    }
}
