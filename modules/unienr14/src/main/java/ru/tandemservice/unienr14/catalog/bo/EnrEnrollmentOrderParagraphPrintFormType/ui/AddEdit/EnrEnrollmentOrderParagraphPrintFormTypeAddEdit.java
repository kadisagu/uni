/* $Id$ */
package ru.tandemservice.unienr14.catalog.bo.EnrEnrollmentOrderParagraphPrintFormType.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.EnrEntrantRequestManager;

/**
 * @author azhebko
 * @since 17.07.2014
 */
@Configuration
public class EnrEnrollmentOrderParagraphPrintFormTypeAddEdit extends BusinessComponentManager
{
    public static final Long ID_ALL_PARAGRAPHS = 0L;
    public static final Long ID_PARALLEL_PARAGRAPHS = 1L;
    public static final Long ID_NOT_PARALLEL_PARAGRAPHS = 2L;

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(EnrEntrantRequestManager.instance().requestTypeDS_1_Config())
                .addDataSource(selectDS("competitionTypeDS", competitionTypeDSHandler()))
                .addDataSource(selectDS("parallelDS", parallelDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler competitionTypeDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), EnrCompetitionType.class)
            .filter(EnrCompetitionType.title())
            .order(EnrCompetitionType.code());
    }

    @Bean
    public ItemListExtPoint<DataWrapper> parallelOptions()
    {
        return this.itemList(DataWrapper.class)
            .add(String.valueOf(ID_ALL_PARAGRAPHS), new DataWrapper(ID_ALL_PARAGRAPHS, "parallelDS.id.all"))
            .add(String.valueOf(ID_PARALLEL_PARAGRAPHS), new DataWrapper(ID_PARALLEL_PARAGRAPHS, "parallelDS.id.parallel"))
            .add(String.valueOf(ID_NOT_PARALLEL_PARAGRAPHS), new DataWrapper(ID_NOT_PARALLEL_PARAGRAPHS, "parallelDS.id.notParallel"))
            .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler parallelDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(this.getName())
            .addItemList(parallelOptions())
            .filtered(true);
    }
}