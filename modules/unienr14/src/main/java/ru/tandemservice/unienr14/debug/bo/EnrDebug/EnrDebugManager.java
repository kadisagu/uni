/**
 *$Id: EppWorkPlanManager.java 26089 2013-02-11 08:34:37Z vdanilov $
 */
package ru.tandemservice.unienr14.debug.bo.EnrDebug;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.debug.bo.EnrDebug.logic.EnrDebugDao;
import ru.tandemservice.unienr14.debug.bo.EnrDebug.logic.IEnrDebugDao;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

/**
 * @author Zhuj
 */
@Configuration
public class EnrDebugManager extends BusinessObjectManager
{
    public static EnrDebugManager instance() {
        return instance(EnrDebugManager.class);
    }

    @Bean
    public IEnrDebugDao dao() {
        return new EnrDebugDao();
    }

}
