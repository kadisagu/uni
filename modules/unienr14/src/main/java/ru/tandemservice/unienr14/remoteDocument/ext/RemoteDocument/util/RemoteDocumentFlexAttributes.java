/**
 *$Id$
 */
package ru.tandemservice.unienr14.remoteDocument.ext.RemoteDocument.util;

import java.util.Arrays;
import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 07.03.2014
 */
public interface RemoteDocumentFlexAttributes
{
    // "гибкие" атрибуты диплома участника олимпиады - уникальные ключи
    public static final String OLYMPIAD_DIPLOMA_PERSON = "olympiadDiplomaPerson";
    public static final String OLYMPIAD_DIPLOMA_VERSION = "olympiadDiplomaVersion";
    public static final String OLYMPIAD_DIPLOMA_REGISTRATION_DATE = "olympiadDiplomaRegistrationDate";
    public static final String OLYMPIAD_DIPLOMA_SERIA = "olympiadDiplomaSeria";
    public static final String OLYMPIAD_DIPLOMA_NUMBER = "olympiadDiplomaNumber";
    public static final String OLYMPIAD_DIPLOMA_ISSUANCE_DATE = "olympiadDiplomaIssuanceDate";
    public static final String OLYMPIAD_DIPLOMA_SETTLEMENT = "olympiadDiplomaSettlement";
    public static final String OLYMPIAD_DIPLOMA_OLYMPIAD = "olympiadDiplomaOlympiad";
    public static final String OLYMPIAD_DIPLOMA_SUBJECT = "olympiadDiplomaSubject";
    public static final String OLYMPIAD_DIPLOMA_HONOUR = "olympiadDiplomaHonour";
    public static final String OLYMPIAD_DIPLOMA_DOCUMENT_TYPE = "olympiadDiplomaDocumentType";

    /** Шаблон сортировки "гибких" атрибутов документа абитуриента */
    public static final List<String> OLYMPIAD_DIPLOMA_ATTRIBUTES_ORDER_TEMPLATE =
            Arrays.asList(OLYMPIAD_DIPLOMA_PERSON,
                    OLYMPIAD_DIPLOMA_VERSION,
                    OLYMPIAD_DIPLOMA_REGISTRATION_DATE,
                    OLYMPIAD_DIPLOMA_SERIA,
                    OLYMPIAD_DIPLOMA_NUMBER,
                    OLYMPIAD_DIPLOMA_ISSUANCE_DATE,
                    OLYMPIAD_DIPLOMA_SETTLEMENT,
                    OLYMPIAD_DIPLOMA_OLYMPIAD,
                    OLYMPIAD_DIPLOMA_SUBJECT,
                    OLYMPIAD_DIPLOMA_HONOUR,
                    OLYMPIAD_DIPLOMA_DOCUMENT_TYPE);
}