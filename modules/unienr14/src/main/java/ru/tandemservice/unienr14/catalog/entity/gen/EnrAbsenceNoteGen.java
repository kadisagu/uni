package ru.tandemservice.unienr14.catalog.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.catalog.entity.EnrAbsenceNote;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Отметка в отношении отсутствующих абитуриентов
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrAbsenceNoteGen extends EntityBase
 implements INaturalIdentifiable<EnrAbsenceNoteGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.catalog.entity.EnrAbsenceNote";
    public static final String ENTITY_NAME = "enrAbsenceNote";
    public static final int VERSION_HASH = -1392230108;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private String _shortTitle;     // Сокращенное название
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Сокращенное название. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название. Свойство не может быть null и должно быть уникальным.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrAbsenceNoteGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((EnrAbsenceNote)another).getCode());
            }
            setShortTitle(((EnrAbsenceNote)another).getShortTitle());
            setTitle(((EnrAbsenceNote)another).getTitle());
        }
    }

    public INaturalId<EnrAbsenceNoteGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<EnrAbsenceNoteGen>
    {
        private static final String PROXY_NAME = "EnrAbsenceNoteNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrAbsenceNoteGen.NaturalId) ) return false;

            EnrAbsenceNoteGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrAbsenceNoteGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrAbsenceNote.class;
        }

        public T newInstance()
        {
            return (T) new EnrAbsenceNote();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "shortTitle":
                    return obj.getShortTitle();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "shortTitle":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "shortTitle":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "shortTitle":
                    return String.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrAbsenceNote> _dslPath = new Path<EnrAbsenceNote>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrAbsenceNote");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrAbsenceNote#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Сокращенное название. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrAbsenceNote#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrAbsenceNote#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EnrAbsenceNote> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrAbsenceNote#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(EnrAbsenceNoteGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Сокращенное название. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrAbsenceNote#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(EnrAbsenceNoteGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrAbsenceNote#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EnrAbsenceNoteGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EnrAbsenceNote.class;
        }

        public String getEntityName()
        {
            return "enrAbsenceNote";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
