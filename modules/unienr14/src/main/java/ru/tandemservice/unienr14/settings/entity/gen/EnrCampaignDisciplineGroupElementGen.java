package ru.tandemservice.unienr14.settings.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDisciplineGroup;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDisciplineGroupElement;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Дисциплина в группе в рамках ПК
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrCampaignDisciplineGroupElementGen extends EntityBase
 implements INaturalIdentifiable<EnrCampaignDisciplineGroupElementGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.settings.entity.EnrCampaignDisciplineGroupElement";
    public static final String ENTITY_NAME = "enrCampaignDisciplineGroupElement";
    public static final int VERSION_HASH = 1198791452;
    private static IEntityMeta ENTITY_META;

    public static final String L_DISCIPLINE = "discipline";
    public static final String L_GROUP = "group";

    private EnrCampaignDiscipline _discipline;     // Дисциплина
    private EnrCampaignDisciplineGroup _group;     // Группа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дисциплина. Свойство не может быть null.
     */
    @NotNull
    public EnrCampaignDiscipline getDiscipline()
    {
        return _discipline;
    }

    /**
     * @param discipline Дисциплина. Свойство не может быть null.
     */
    public void setDiscipline(EnrCampaignDiscipline discipline)
    {
        dirty(_discipline, discipline);
        _discipline = discipline;
    }

    /**
     * @return Группа. Свойство не может быть null.
     */
    @NotNull
    public EnrCampaignDisciplineGroup getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа. Свойство не может быть null.
     */
    public void setGroup(EnrCampaignDisciplineGroup group)
    {
        dirty(_group, group);
        _group = group;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrCampaignDisciplineGroupElementGen)
        {
            if (withNaturalIdProperties)
            {
                setDiscipline(((EnrCampaignDisciplineGroupElement)another).getDiscipline());
                setGroup(((EnrCampaignDisciplineGroupElement)another).getGroup());
            }
        }
    }

    public INaturalId<EnrCampaignDisciplineGroupElementGen> getNaturalId()
    {
        return new NaturalId(getDiscipline(), getGroup());
    }

    public static class NaturalId extends NaturalIdBase<EnrCampaignDisciplineGroupElementGen>
    {
        private static final String PROXY_NAME = "EnrCampaignDisciplineGroupElementNaturalProxy";

        private Long _discipline;
        private Long _group;

        public NaturalId()
        {}

        public NaturalId(EnrCampaignDiscipline discipline, EnrCampaignDisciplineGroup group)
        {
            _discipline = ((IEntity) discipline).getId();
            _group = ((IEntity) group).getId();
        }

        public Long getDiscipline()
        {
            return _discipline;
        }

        public void setDiscipline(Long discipline)
        {
            _discipline = discipline;
        }

        public Long getGroup()
        {
            return _group;
        }

        public void setGroup(Long group)
        {
            _group = group;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrCampaignDisciplineGroupElementGen.NaturalId) ) return false;

            EnrCampaignDisciplineGroupElementGen.NaturalId that = (NaturalId) o;

            if( !equals(getDiscipline(), that.getDiscipline()) ) return false;
            if( !equals(getGroup(), that.getGroup()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getDiscipline());
            result = hashCode(result, getGroup());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getDiscipline());
            sb.append("/");
            sb.append(getGroup());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrCampaignDisciplineGroupElementGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrCampaignDisciplineGroupElement.class;
        }

        public T newInstance()
        {
            return (T) new EnrCampaignDisciplineGroupElement();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "discipline":
                    return obj.getDiscipline();
                case "group":
                    return obj.getGroup();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "discipline":
                    obj.setDiscipline((EnrCampaignDiscipline) value);
                    return;
                case "group":
                    obj.setGroup((EnrCampaignDisciplineGroup) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "discipline":
                        return true;
                case "group":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "discipline":
                    return true;
                case "group":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "discipline":
                    return EnrCampaignDiscipline.class;
                case "group":
                    return EnrCampaignDisciplineGroup.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrCampaignDisciplineGroupElement> _dslPath = new Path<EnrCampaignDisciplineGroupElement>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrCampaignDisciplineGroupElement");
    }
            

    /**
     * @return Дисциплина. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignDisciplineGroupElement#getDiscipline()
     */
    public static EnrCampaignDiscipline.Path<EnrCampaignDiscipline> discipline()
    {
        return _dslPath.discipline();
    }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignDisciplineGroupElement#getGroup()
     */
    public static EnrCampaignDisciplineGroup.Path<EnrCampaignDisciplineGroup> group()
    {
        return _dslPath.group();
    }

    public static class Path<E extends EnrCampaignDisciplineGroupElement> extends EntityPath<E>
    {
        private EnrCampaignDiscipline.Path<EnrCampaignDiscipline> _discipline;
        private EnrCampaignDisciplineGroup.Path<EnrCampaignDisciplineGroup> _group;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дисциплина. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignDisciplineGroupElement#getDiscipline()
     */
        public EnrCampaignDiscipline.Path<EnrCampaignDiscipline> discipline()
        {
            if(_discipline == null )
                _discipline = new EnrCampaignDiscipline.Path<EnrCampaignDiscipline>(L_DISCIPLINE, this);
            return _discipline;
        }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignDisciplineGroupElement#getGroup()
     */
        public EnrCampaignDisciplineGroup.Path<EnrCampaignDisciplineGroup> group()
        {
            if(_group == null )
                _group = new EnrCampaignDisciplineGroup.Path<EnrCampaignDisciplineGroup>(L_GROUP, this);
            return _group;
        }

        public Class getEntityClass()
        {
            return EnrCampaignDisciplineGroupElement.class;
        }

        public String getEntityName()
        {
            return "enrCampaignDisciplineGroupElement";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
