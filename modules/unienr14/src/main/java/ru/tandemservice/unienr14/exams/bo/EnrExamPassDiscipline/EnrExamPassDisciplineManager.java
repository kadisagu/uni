/* $Id$ */
package ru.tandemservice.unienr14.exams.bo.EnrExamPassDiscipline;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.catalog.entity.EnrAbsenceNote;
import ru.tandemservice.unienr14.exams.bo.EnrExamPassDiscipline.logic.EnrExamPassDisciplineDao;
import ru.tandemservice.unienr14.exams.bo.EnrExamPassDiscipline.logic.IEnrExamPassDisciplineDao;

/**
 * @author nvankov
 * @since 4/6/15
 */
@Configuration
public class EnrExamPassDisciplineManager extends BusinessObjectManager
{
    public static EnrExamPassDisciplineManager instance()
    {
        return instance(EnrExamPassDisciplineManager.class);
    }

    @Bean
    public IEnrExamPassDisciplineDao dao()
    {
        return new EnrExamPassDisciplineDao();
    }

    @Bean
    public IDefaultComboDataSourceHandler absenceNoteDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrAbsenceNote.class)
                .filter(EnrAbsenceNote.title())
                .order(EnrAbsenceNote.title())
                .pageable(true);
    }
}



    