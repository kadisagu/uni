/* $Id:$ */
package ru.tandemservice.unienr14.competition.bo.EnrProgramSet.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.SecondaryList.EnrProgramSetSecondaryList;
import ru.tandemservice.unienr14.competition.entity.EnrExamSetVariant;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetSecondary;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.EnrExamSetManager;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.logic.IEnrExamSetDao;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 2/19/14
 */
public class EnrProgramSetSecondaryDSHandler extends DefaultSearchDataSourceHandler
{
    public static final String VIEW_PROP_EXAMS = "exams";
    public static final String VIEW_PROP_OU = "orgUnits";

    public EnrProgramSetSecondaryDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final DQLOrderDescriptionRegistry orderDescriptionRegistry = new DQLOrderDescriptionRegistry(EnrProgramSetSecondary.class, "d");
        final DQLSelectBuilder dql = orderDescriptionRegistry.buildDQLSelectBuilder().column(property("d"));

        filter(dql, context, input, "d");

        final DSOutput output = DQLSelectOutputBuilder.get(input, dql, context.getSession())
            .order(orderDescriptionRegistry)
            .pageable(isPageable())
            .build();

        if (!isPageable()) {
            output.setCountRecord(Math.max(1, output.getRecordList().size()));

            output.ordering(new EntityComparator(
                    new EntityOrder(EnrProgramSetSecondary.title(), OrderDirection.asc),
                    new EntityOrder(EnrProgramSetSecondary.program().programSubject().subjectCode().s(), OrderDirection.asc),
                    new EntityOrder(EnrProgramSetSecondary.program().programSubject().title().s(), OrderDirection.asc),
                    new EntityOrder(EnrProgramSetSecondary.program().form().code().s(), OrderDirection.asc),
                    new EntityOrder(EnrProgramSetSecondary.program().baseLevel().code().s(), OrderDirection.asc),
                    new EntityOrder(EnrProgramSetSecondary.program().inDepthStudy().s(), OrderDirection.asc)
            ));
        }

        return wrap(input, output, context);
    }

    /**
     * Фильтрует запрос.
     * @param dql builder from EnrDirectionCompetition
     */
    protected void filter(DQLSelectBuilder dql, ExecutionContext context, DSInput input, String alias)
    {
        EnrEnrollmentCampaign campaign = context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
        EduProgramForm programForm = context.get(EnrProgramSetSecondaryList.BIND_PROGRAM_FORM);
        List<EnrOrgUnit> orgUnits = context.get(EnrProgramSetSecondaryList.BIND_ORG_UNITS);
        String subjectCode = context.get(EnrProgramSetSecondaryList.BIND_SUBJECT_CODE);
        List<EduProgramSubject> programSubjects = context.get(EnrProgramSetSecondaryList.BIND_PROGRAM_SUBJECTS);
        String title = context.get(EnrProgramSetSecondaryList.BIND_TITLE);
        
        EnrProgramSetSecondaryDSHandler.applyFilters(dql, "d", campaign, programForm, orgUnits, programSubjects);

        FilterUtils.applySimpleLikeFilter(dql, "d", EnrProgramSetBase.title(), title);
        FilterUtils.applySimpleLikeFilter(dql, "d", EnrProgramSetBase.programSubject().code(), subjectCode);
    }

    protected DSOutput wrap(final DSInput input, DSOutput output, ExecutionContext context)
    {
        Set<Long> variants = new HashSet<>();
        Set<Long> competitions = new HashSet<>();
        for (EnrProgramSetSecondary competition : output.<EnrProgramSetSecondary>getRecordList()) {
            variants.add(competition.getExamSetVariant().getId());
            competitions.add(competition.getId());
        }
        Map<Long, IEnrExamSetDao.IExamSetSettings> contents = EnrExamSetManager.instance().dao().getExamSetContent(variants);

        Map<Long, List<EnrOrgUnit>> orgUnits = SafeMap.get(ArrayList.class);
        for (EnrProgramSetOrgUnit ou : IUniBaseDao.instance.get().getList(EnrProgramSetOrgUnit.class, EnrProgramSetOrgUnit.programSet().id().s(), competitions, EnrProgramSetOrgUnit.orgUnit().institutionOrgUnit().orgUnit().shortTitle().s())) {
            orgUnits.get(ou.getProgramSet().getId()).add(ou.getOrgUnit());
        }

        for (DataWrapper wrapper : DataWrapper.wrap(output)) {
            wrapper.setProperty(VIEW_PROP_EXAMS, contents.get(((EnrExamSetVariant) wrapper.getProperty(EnrProgramSetSecondary.L_EXAM_SET_VARIANT)).getId()).getTitle());
            wrapper.setProperty(VIEW_PROP_OU, orgUnits.get(wrapper.getId()));
        }

        return output;
    }

    public static DQLSelectBuilder applyFilters(DQLSelectBuilder builder, String alias, EnrEnrollmentCampaign campaign, EduProgramForm programForm, List<EnrOrgUnit> orgUnits, List<EduProgramSubject> programSubjects)
    {
        boolean useProgramSetOrgUnitBuilder = false;
        DQLSelectBuilder programSetOrgUnitBuilder = new DQLSelectBuilder()
            .fromEntity(EnrProgramSetOrgUnit.class, "psou")
            .column(property("psou", EnrProgramSetOrgUnit.programSet().id()));

        if (orgUnits != null && !orgUnits.isEmpty())
        {
            useProgramSetOrgUnitBuilder = true;
            programSetOrgUnitBuilder.where(in(property("psou", EnrProgramSetOrgUnit.orgUnit()), orgUnits));
        }

        if (programSubjects != null && !programSubjects.isEmpty())
        {
            builder.where(in(property(alias, EnrProgramSetBase.programSubject()), programSubjects));
        }

        builder.where(eq(property(alias, EnrProgramSetBase.enrollmentCampaign()), value(campaign)));

        if (programForm != null)
            builder.where(eq(property(alias, EnrProgramSetBase.programForm()), value(programForm)));

        if (useProgramSetOrgUnitBuilder)
            builder.where(in(property(alias, EnrProgramSetBase.id()), programSetOrgUnitBuilder.buildQuery()));

        return builder;
    }
}
