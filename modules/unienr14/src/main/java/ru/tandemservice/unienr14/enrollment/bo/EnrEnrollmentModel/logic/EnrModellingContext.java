/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.logic;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Ordering;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.sandbox.utils.FastBeanFunction;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEnrollmentStepKindCodes;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.select.*;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.xml.EnrXmlEnrollmentStepState;
import ru.tandemservice.unienr14.enrollment.entity.EnrModelStep;
import ru.tandemservice.unienr14.enrollment.entity.EnrModelStepItem;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 4/2/15
 */
public class EnrModellingContext extends EnrSelectionContext
{
    private EnrModelStep step;
    public EnrModelStep getStep() { return step; }

    protected EnrModellingContext(final EnrModelStep step, boolean debug) {
        this.step = step;
        this.debug = debug;
        this.targetAdmissionCompetition = step.getModel().getEnrollmentCampaign().getSettings().isTargetAdmissionCompetition();
    }

    @Override
    public boolean isCheckPrevEnrollmentDuplicates() { return false; }

    // WIKI: Подходящие для выбора к зачислению абитуриенты
    @Override
    public boolean isSelectionAllowed(final EnrSelectionItem selectionItem)
    {
        if (!super.isSelectionAllowed(selectionItem)) { return false; }

        return selectionItem.getStepItem().isEnrollmentAvailable();
    }

    @Override
    public void executeSelection()
    {
        final String kindCode = this.getStep().getKind().getCode();

        if (!getStep().getKind().isUseRecommendation())
        {
            // «Без рекомендации»
            // WIKI: Точно так же, как выбор абитуриентов для рекомендации (см. п. выше)
            super.executeSelection();
            return;
        }

        if (EnrEnrollmentStepKindCodes.REC_AND_ADHERE.equals(kindCode))
        {
            // «С рекомендацией, зачислять только рекомендованных»
            // WIKI: Выставляем признак «Отмечен к зачислению» всем подходящим (см. п «Подходящие для выбора к зачислению абитуриенты») абитуриентам в шаге зачисления, у которых признак «Рекомендован» = да.
            this.executeSelectAllRecommended();
            return;
        }

        if (EnrEnrollmentStepKindCodes.REC_AND_IGNORE.equals(kindCode))
        {
            // «С рекомендацией, зачислять до заполнения мест»
            // WIKI: Сначала отмечаются «К зачислению» все абитуриенты из подходящих, у которых признак «Рекомендован» = «да» (далее эти абитуриенты называются предварительно выбранными).
            final Set<EnrSelectionItem> preSelectedStepItems = this.setPreSelectedStepItems(this.executeSelectAllRecommended());
            //XXX: for (EnrSelectionItem w : preSelectedStepItems) { w.setSelected(false); }

            // WIKI: Затем из отобранных с признаком «Рекомендован» = «нет» выбираются лучшие абитуриенты на лучшие приоритеты в объеме вакантных планов приема по конкурсу
            this.executeSelection(input -> {
                if (input.getStepItem().isRecommended()) {
                    return false; /* тех, кто рекомендован, мы уже выбрали, теперь выбираем тех, кто не рекомендован */
                }
                if (!isSelectionAllowed(input)) { return false; }
                if (preSelectedStepItems.contains(input)) { return false; }
                for (EnrSelectionItem pre : preSelectedStepItems) {
                    if (!pre.getEntrant().equals(input.getEntrant())) { continue; }
                    if (pre.getRequestedCompetition().isParallel() != input.getRequestedCompetition().isParallel()) {
                        continue;
                    }
                    if (pre.getRequestedCompetition().getPriority() <= input.getRequestedCompetition().getPriority()) {
                        return false;
                    }
                }
                return true;
            });

            List<EnrSelectionItem> newlySelected = new ArrayList<>();
            for (final EnrSelectionGroup group: getStepItemGroupMap().values()) {
                for (EnrSelectionItem w : group.getItemMap().values()) {
                    if (w.isSelected() && !preSelectedStepItems.contains(w)) {
                        newlySelected.add(w);
                    }
                }
            }

            for (EnrSelectionItem current : preSelectedStepItems) {
                //XXX: current.setSelected(true);
                if (current.isSelected()) {
                    for (EnrSelectionItem newly : newlySelected) {
                        if (!current.getEntrant().equals(newly.getEntrant())) { continue; }
                        if (current.getRequestedCompetition().isParallel() != newly.getRequestedCompetition().isParallel()) { continue; }
                        if (newly.getRequestedCompetition().getPriority() < current.getRequestedCompetition().getPriority()) {
                            current.setSelected(false);
                            break;
                        }
                    }
                }
            }

            return;
        }

        throw new IllegalStateException("undefined-step-kind-code: " + kindCode);
    }

    // тупо выбираем всех, кто рекомендован (если несколько раз - то несколько раз)
    protected List<EnrSelectionItem> executeSelectAllRecommended()
    {
        final List<EnrSelectionItem> selectedStepItems = new ArrayList<>();
        for (final EnrSelectionGroup group: this.getStepItemGroupMap().values()) {
            for (final EnrSelectionItem selectionItem : group.getItemMap().values()) {
                if (!selectionItem.getStepItem().isRecommended()) { continue; }
                if (!this.isSelectionAllowed(selectionItem)) { continue; }
                if (this.selectStepItem(selectionItem)) {
                    // здесь (при предвыборе) освобождать места в соответсвии с приказами (чтобы зачислить дозачислить на них людей)
                    selectedStepItems.add(selectionItem);
                }
            }
        }
        return selectedStepItems;
    }

    @Override
    public void applyResults()
    {
        IUniBaseDao.instance.get().doInTransaction(session -> {

            Map<EnrModelStepItem, Set<EnrModelStepItem>> prevMap = SafeMap.get(HashSet.class);

            DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EnrModelStepItem.class, "i").column("i")
                .fromEntity(EnrModelStepItem.class, "p").column("p")
                .where(eq(property("i", EnrModelStepItem.step().model()), property("p", EnrModelStepItem.step().model())))
                .where(eq(property("i", EnrModelStepItem.entity().entrant()), property("p", EnrModelStepItem.entity().entrant())))
                .where(eq(property("i", EnrModelStepItem.entity().requestedCompetition().parallel()), property("p", EnrModelStepItem.entity().requestedCompetition().parallel())))
                .where(eq(property("p", EnrModelStepItem.enrolled()), value(Boolean.TRUE)))
                .where(lt(property("p", EnrModelStepItem.step().enrollmentDate()), property("i", EnrModelStepItem.step().enrollmentDate())))
                .where(eq(property("i", EnrModelStepItem.step()), value(step)));
            for (Object[] row : dql.createStatement(session).<Object[]>list()) {
                prevMap.get((EnrModelStepItem) row[0]).add((EnrModelStepItem) row[1]);
            }

            // выставляем признак в объекты
            for (final EnrSelectionGroup group: getStepItemGroupMap().values()) {
                for (final EnrSelectionItem selectionItem : group.getItemMap().values()) {
                    final EnrModelStepItem item = (EnrModelStepItem) selectionItem.getStepItem();
                    item.setEnrolled(selectionItem.isSelected());
                    item.setReEnrolled(false);
                    session.saveOrUpdate(item);

                    if (item.isEnrolled()) {
                        for (EnrModelStepItem p : prevMap.get(item)) {
                            p.setReEnrolled(true);
                            session.saveOrUpdate(p);
                        }
                    }
                }
            }

            // сохраняем состояние и признак
            final EnrModelStep step1 = getStep();
            step1.setAutoEnrolledMarked(true);
            session.saveOrUpdate(step1);

            // сохраняем в базу
            session.flush();
            return null;
        });
    }

    public Map<EnrModelStepItem, EnrSelectionItem> setupStepItemList(List<EnrModelStepItem> stepItemList)
    {
        Debug.begin("setupStepItemList(" + stepItemList.size() + ")");
        final Map<EnrModelStepItem, EnrSelectionItem> result = new LinkedHashMap<>();
        for (final EnrModelStepItem stepItem: stepItemList)
        {
            final EnrSelectionItem selectionItem = this.getGroup(stepItem).add(stepItem);
            if (null != result.put(stepItem, selectionItem)) {
                throw new IllegalStateException("duplicate-step-item: stepItemId=" + stepItem.getId());
            }

            // http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20686888
            // признак «включен в конкурсный список» должен быть «да» (здесь уже проверено, что все ВИ сданы)
            if (!stepItem.isIncluded()) {
                selectionItem.skip(new EnrSelectionSkipReason.NotIncluded());
            }

            // http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20686888
            // признак «Забрал документы» у заявления выбранного конкурса должен быть «нет»
            if (stepItem.isEntrantRequestTakeAwayDocuments()) {
                selectionItem.skip(new EnrSelectionSkipReason.TakeAwayDocument());
            }

            // http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20686888
            // признак «Архивный» у абитуриента должен быть «нет»
            if (stepItem.isEntrantArchived()) {
                selectionItem.skip(new EnrSelectionSkipReason.Archival());
            }
        }
        Debug.end();
        return result;
    }

    private List<EnrEnrollmentSelectionCheck> buildCheckWrapperList()
    {
        final Predicate<EnrSelectionItem> available = this::isSelectionAllowed;

        final List<EnrEnrollmentSelectionCheck> stepItemGroupList = new ArrayList<>(Collections2.transform(
            this.getStepItemGroupMap().values(),
            input -> new EnrEnrollmentSelectionCheck(input, available)
        ));

        for (EnrEnrollmentSelectionCheck a : stepItemGroupList) {
            for (EnrEnrollmentSelectionCheck b : stepItemGroupList) {
                a.crossCheck(b);
            }
        }

        final Comparator<EnrEnrollmentSelectionCheck> comparator = Ordering.compound(Arrays.asList(
            Ordering.natural().onResultOf(new FastBeanFunction<EnrEnrollmentSelectionCheck, String>("selectionGroup.key.competition.programSetOrgUnit.programSet.title")),
            Ordering.natural().onResultOf(new FastBeanFunction<EnrEnrollmentSelectionCheck, String>("selectionGroup.key.competition.programSetOrgUnit.orgUnit.title")),
            Ordering.natural().onResultOf(new FastBeanFunction<EnrEnrollmentSelectionCheck, String>("selectionGroup.key.competition.type.code")),
            Ordering.natural().onResultOf(new FastBeanFunction<EnrEnrollmentSelectionCheck, String>("selectionGroup.key.competition.eduLevelRequirement.code")),
            Ordering.natural().onResultOf(new FastBeanFunction<EnrEnrollmentSelectionCheck, String>("selectionGroup.key.competition.title")),
            Ordering.natural().onResultOf(new FastBeanFunction<EnrEnrollmentSelectionCheck, Long>("selectionGroup.key.competition.id")),
            Ordering.natural().nullsLast().onResultOf(new FastBeanFunction<EnrEnrollmentSelectionCheck, String>("selectionGroup.key.targetAdmissionKind.title"))
        ));

        Collections.sort(stepItemGroupList, comparator);
        return stepItemGroupList;
    }


    public EnrXmlEnrollmentStepState buildEnrXmlEnrollmentStepState()
    {
        final List<EnrEnrollmentSelectionCheck> stepItemGroupList = buildCheckWrapperList();
        final EnrXmlEnrollmentStepState root = new EnrXmlEnrollmentStepState();

        Map<EnrCompetition, List<EnrEnrollmentSelectionCheck>> comp2grpupMap = new LinkedHashMap<>();
        for (EnrEnrollmentSelectionCheck g: stepItemGroupList) {
            SafeMap.safeGet(comp2grpupMap, g.getSelectionGroup().getKey().getCompetition(), ArrayList.class).add(g);
        }
        for (Map.Entry<EnrCompetition, List<EnrEnrollmentSelectionCheck>> e: comp2grpupMap.entrySet()) {
            EnrXmlEnrollmentStepState.Competition xmlc = new EnrXmlEnrollmentStepState.Competition(e.getKey());
            for (EnrEnrollmentSelectionCheck g: e.getValue()) {
                EnrXmlEnrollmentStepState.Group xmlg = new EnrXmlEnrollmentStepState.Group(g.getSelectionGroup());
                xmlc.groupList.add(xmlg);

                EnrXmlEnrollmentStepState.GroupEnrCheckStatus xmlec = new EnrXmlEnrollmentStepState.GroupEnrCheckStatus();
                xmlec.errorList = new ArrayList<>(g.getErrors());
                xmlec.stepItemAvailable = g.getStepItemsAvailable().size();
                xmlec.planBalance = g.getPlanBalance();
                xmlec.entrantBalance = g.getEntrantBalance();
                xmlec.extractMarkedEnrByOther = g.getExtractsMarkEnrolledByOther().size();
                xmlec.stepItemMarkedEnrByOther = g.getStepItemsMarkEnrolledByOther().size();
                xmlec.stepItemMarkedEnrByThis = g.getStepItemsMarkEnrolledByThis().size();
                xmlec.stepItemAvailableMarkedEnrByThisOrOther = g.getStepItemsMarkEnrolledByThisOrOtherAndAvailable().size();

                xmlg.checkEnr = xmlec;

                for (IEnrSelectionPrevEnrollmentFact extract: g.getSelectionGroup().getCompetitionExtractMap().values()) {
                    EnrXmlEnrollmentStepState.ExtractInfo xmle = new EnrXmlEnrollmentStepState.ExtractInfo(extract);
                    xmlg.enrolledList.add(xmle);
                }

                for (EnrSelectionItem item: g.getSelectionGroup().getItemMap().values()) {
                    EnrXmlEnrollmentStepState.StepItem xmli = new EnrXmlEnrollmentStepState.StepItem(item);
                    xmlg.itemList.add(xmli);

                    for (IEnrSelectionItemPrevEnrollmentInfo reenrollFrom: item.getReenrollFromList()) {
                        EnrSelectionGroup otherG = getStepItemGroup(reenrollFrom); // nullable
                        EnrXmlEnrollmentStepState.ExtractInfo xmle = new EnrXmlEnrollmentStepState.ExtractInfo(otherG, reenrollFrom);
                        xmli.reenrollFromList.add(xmle);
                    }

                    for (IEnrSelectionItemPrevEnrollmentInfo addEnroll: item.getAddEnrollList()) {
                        EnrSelectionGroup otherG = getStepItemGroup(addEnroll); // nullable
                        EnrXmlEnrollmentStepState.ExtractInfo xmle = new EnrXmlEnrollmentStepState.ExtractInfo(otherG, addEnroll);
                        xmli.addEnrollList.add(xmle);
                    }
                }
            }
            root.competitionList.add(xmlc);
        }

        return root;
    }


}

