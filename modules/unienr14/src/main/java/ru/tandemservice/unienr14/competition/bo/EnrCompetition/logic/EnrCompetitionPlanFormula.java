/* $Id:$ */
package ru.tandemservice.unienr14.competition.bo.EnrCompetition.logic;

import com.google.common.collect.ImmutableMap;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetHigher;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;

import java.util.Map;

import static ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes.*;
import static ru.tandemservice.unienr14.catalog.entity.codes.EnrEduLevelRequirementCodes.*;
import static ru.tandemservice.unienr14.catalog.entity.codes.EnrMethodDivCompetitionsCodes.*;
import static ru.tandemservice.unienr14.competition.bo.EnrCompetition.logic.IEnrCompetitionDao.PlanCalculationRule.BY_PASSED_AND_FREE;
import static ru.tandemservice.unienr14.competition.bo.EnrCompetition.logic.IEnrCompetitionDao.PlanCalculationRule.EQUAL_PARTS;
import static ru.tandemservice.unienr14.competition.bo.EnrCompetition.logic.IEnrCompetitionDao.PlanCalculationRule.BY_PASSED;

/**
 * @author oleyba
 * @since 2/2/15
 */
public abstract class EnrCompetitionPlanFormula
{
    public interface IPlanCalculationContext
    {
        IEnrCompetitionDao.PlanCalculationRule correctionRule();

        int s(String compType, String level);
        int e(String compType);
        int e(String compType, String level);
        int e(String taKind, String compType, String level);
    }
    
    public static int calculatePlan(EnrCompetition c, IPlanCalculationContext context) {
        IEnrCompetitionDao.PlanCalculationRule rule = context.correctionRule();
        String method = c.getProgramSetOrgUnit().getProgramSet().getMethodDivCompetitions().getCode();
        // они все одинаковые, поэтому берем для БС добавлены
        if (EnrProgramSetHigher.AVAILABLE_DIV_METHOD_CODE.contains(method) || MASTER_NO_DIV.equals(method) || SEC_NO_DIV.equals(method)) {
            method = BS_NO_DIV;
        }
        EnrCompetitionPlanFormula formula = formulaMap.get(new MultiKey(rule, method, c.getType().getCode(), c.getEduLevelRequirement().getCode()));
        if (null == formula) {
            formula = formulaMap.get(new MultiKey(rule, method, c.getType().getCode()));
        }
        if (null == formula) {
            throw new IllegalArgumentException(UniStringUtils.joinWithSeparator(", ", rule.name(), method, c.getType().getShortTitle(), c.getEduLevelRequirement().getShortTitle()));
        }
        return formula.plan(c.getProgramSetOrgUnit(), context);
    }

    abstract int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c);

    private static Map<MultiKey, EnrCompetitionPlanFormula> formulaMap = ImmutableMap.<MultiKey, EnrCompetitionPlanFormula>builder()
        // ------------- деление в равных долях

        // 1. Способ деления конкурсов набора ОП — без деления
        //|без ВИ (КЦП)|ministerialPlan|
        .put(new MultiKey(EQUAL_PARTS, BS_NO_DIV, NO_EXAM_MINISTERIAL), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return psOu.getMinisterialWithoutExclusiveAndTA();
            }
        })
        //|особ. права|exclusivePlan|
        .put(new MultiKey(EQUAL_PARTS, BS_NO_DIV, EXCLUSIVE), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return psOu.getExclusivePlan();
            }
        })
        //|ЦП|targetAdmPlan|
        .put(new MultiKey(EQUAL_PARTS, BS_NO_DIV, TARGET_ADMISSION), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return psOu.getTargetAdmPlan();
            }
        })
        //|Общий конкурс|ministerialPlan|
        .put(new MultiKey(EQUAL_PARTS, BS_NO_DIV, MINISTERIAL), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return psOu.getMinisterialWithoutExclusiveAndTA();
            }
        })
        //|Без ВИ (договор)|contractPlan|
        .put(new MultiKey(EQUAL_PARTS, BS_NO_DIV, NO_EXAM_CONTRACT), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return psOu.getContractPlan();
            }
        })
        //|Договор|contractPlan|
        .put(new MultiKey(EQUAL_PARTS, BS_NO_DIV, CONTRACT), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return psOu.getContractPlan();
            }
        })

        //2. Способ деления конкурсов набора ОП — СОО, СПО, ВО
        //|без ВИ (КЦП)|СОО и СПО|ministerialPlan|
        .put(new MultiKey(EQUAL_PARTS, BS_SOO_SPO_VO, NO_EXAM_MINISTERIAL, SOO_AND_SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return psOu.getMinisterialWithoutExclusiveAndTA();
            }
        })
        //|особ. права|СОО|округл(exclusivePlan/2)|
        .put(new MultiKey(EQUAL_PARTS, BS_SOO_SPO_VO, EXCLUSIVE, SOO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return half(psOu.getExclusivePlan());
            }
        })
        //|особ. права|СПО|exclusivePlan-округл(exclusivePlan/2)|
        .put(new MultiKey(EQUAL_PARTS, BS_SOO_SPO_VO, EXCLUSIVE, SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return restFromHalf(psOu.getExclusivePlan());
            }
        })
        //|ЦП|СОО|округл(targetAdmPlan/2)|
        .put(new MultiKey(EQUAL_PARTS, BS_SOO_SPO_VO, TARGET_ADMISSION, SOO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return half(psOu.getTargetAdmPlan());
            }
        })
        //|ЦП|СПО|targetAdmPlan-округл(targetAdmPlan/2)|
        .put(new MultiKey(EQUAL_PARTS, BS_SOO_SPO_VO, TARGET_ADMISSION, SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return restFromHalf(psOu.getTargetAdmPlan());
            }
        })
        //|Общий конкурс|СОО|округл(ministerialPlan/2)|
        .put(new MultiKey(EQUAL_PARTS, BS_SOO_SPO_VO, MINISTERIAL, SOO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return half(psOu.getMinisterialWithoutExclusiveAndTA());
            }
        })
        //|Общий конкурс|СПО|ministerialPlan-округл(ministerialPlan/2)|
        .put(new MultiKey(EQUAL_PARTS, BS_SOO_SPO_VO, MINISTERIAL, SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return restFromHalf(psOu.getMinisterialWithoutExclusiveAndTA());
            }
        })
        //|Без ВИ (договор)|СОО и СПО|contractPlan|
        .put(new MultiKey(EQUAL_PARTS, BS_SOO_SPO_VO, NO_EXAM_CONTRACT, SOO_AND_SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return psOu.getContractPlan();
            }
        })
        //|Договор|СОО|contractPlan-2*округл(contractPlan/3)|
        .put(new MultiKey(EQUAL_PARTS, BS_SOO_SPO_VO, CONTRACT, SOO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return restFromTwoThirds(psOu.getContractPlan());
            }
        })
        //|Договор|СПО|округл(contractPlan/3)|
        .put(new MultiKey(EQUAL_PARTS, BS_SOO_SPO_VO, CONTRACT, SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return third(psOu.getContractPlan());
            }
        })
        //|Договор|ВО|округл(contractPlan/3)|
        .put(new MultiKey(EQUAL_PARTS, BS_SOO_SPO_VO, CONTRACT, VO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return third(psOu.getContractPlan());
            }
        })

        //3. Способ деления конкурсов набора ОП — СОО, ПО
        //||Вид приема||На базе||Число мест||
        //|без ВИ (КЦП)|СОО и СПО|ministerialPlan|
        .put(new MultiKey(EQUAL_PARTS, BS_SOO_PO, NO_EXAM_MINISTERIAL, SOO_AND_SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return psOu.getMinisterialWithoutExclusiveAndTA();
            }
        })
        //|особ. права|СОО|округл(exclusivePlan/2)|
        .put(new MultiKey(EQUAL_PARTS, BS_SOO_PO, EXCLUSIVE, SOO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return half(psOu.getExclusivePlan());
            }
        })
        //|особ. права|СПО|exclusivePlan-округл(exclusivePlan/2)|
        .put(new MultiKey(EQUAL_PARTS, BS_SOO_PO, EXCLUSIVE, SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return restFromHalf(psOu.getExclusivePlan());
            }
        })
        //|ЦП|СОО|округл(targetAdmPlan/2)|
        .put(new MultiKey(EQUAL_PARTS, BS_SOO_PO, TARGET_ADMISSION, SOO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return half(psOu.getTargetAdmPlan());
            }
        })
        //|ЦП|СПО|targetAdmPlan-округл(targetAdmPlan/2)|
        .put(new MultiKey(EQUAL_PARTS, BS_SOO_PO, TARGET_ADMISSION, SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return restFromHalf(psOu.getTargetAdmPlan());
            }
        })
        //|Общий конкурс|СОО|округл(ministerialPlan/2)|
        .put(new MultiKey(EQUAL_PARTS, BS_SOO_PO, MINISTERIAL, SOO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return half(psOu.getMinisterialWithoutExclusiveAndTA());
            }
        })
        //|Общий конкурс|СПО|ministerialPlan-округл(ministerialPlan/2)|
        .put(new MultiKey(EQUAL_PARTS, BS_SOO_PO, MINISTERIAL, SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return restFromHalf(psOu.getMinisterialWithoutExclusiveAndTA());
            }
        })
        //|Без ВИ (договор)|СОО и СПО|contractPlan|
        .put(new MultiKey(EQUAL_PARTS, BS_SOO_PO, NO_EXAM_CONTRACT, SOO_AND_SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return psOu.getContractPlan();
            }
        })
        //|Договор|СОО|округл(contractPlan/2)|
        .put(new MultiKey(EQUAL_PARTS, BS_SOO_PO, CONTRACT, SOO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return half(psOu.getContractPlan());
            }
        })
        //|Договор|ПО|contractPlan-округл(contractPlan/2)|
        .put(new MultiKey(EQUAL_PARTS, BS_SOO_PO, CONTRACT, PO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return restFromHalf(psOu.getContractPlan());
            }
        })

        //4. Способ деления конкурсов набора ОП — СОО и СПО, ВО
        //||Вид приема||На базе||Число мест||
        //|без ВИ (КЦП)|СОО и СПО|ministerialPlan|
        .put(new MultiKey(EQUAL_PARTS, BS_SOO_AND_SPO_APART_VO, NO_EXAM_MINISTERIAL, SOO_AND_SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return psOu.getMinisterialWithoutExclusiveAndTA();
            }
        })
        //|особ. права|СОО и СПО|exclusivePlan|
        .put(new MultiKey(EQUAL_PARTS, BS_SOO_AND_SPO_APART_VO, EXCLUSIVE, SOO_AND_SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return psOu.getExclusivePlan();
            }
        })
        //|ЦП|СОО и СПО|targetAdmPlan|
        .put(new MultiKey(EQUAL_PARTS, BS_SOO_AND_SPO_APART_VO, TARGET_ADMISSION, SOO_AND_SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return psOu.getTargetAdmPlan();
            }
        })
        //|Общий конкурс|СОО и СПО|ministerialPlan|
        .put(new MultiKey(EQUAL_PARTS, BS_SOO_AND_SPO_APART_VO, MINISTERIAL, SOO_AND_SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return psOu.getMinisterialWithoutExclusiveAndTA();
            }
        })
        //|Без ВИ (договор)|СОО и СПО|contractPlan|
        .put(new MultiKey(EQUAL_PARTS, BS_SOO_AND_SPO_APART_VO, NO_EXAM_CONTRACT, SOO_AND_SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return psOu.getContractPlan();
            }
        })
        //|Договор|СОО и СПО|округл(contractPlan/2)|
        .put(new MultiKey(EQUAL_PARTS, BS_SOO_AND_SPO_APART_VO, CONTRACT, SOO_AND_SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return half(psOu.getContractPlan());
            }
        })
        //|Договор|ВО|contractPlan-округл(contractPlan/2)|
        .put(new MultiKey(EQUAL_PARTS, BS_SOO_AND_SPO_APART_VO, CONTRACT, VO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return restFromHalf(psOu.getContractPlan());
            }
        })

        //5. Способ деления конкурсов набора ОП — СОО и ВО, СПО
        //||Вид приема||На базе||Число мест||
        //|без ВИ (КЦП)|СОО и СПО|ministerialPlan|
        .put(new MultiKey(EQUAL_PARTS, BS_SOO_AND_VO_APART_SPO, NO_EXAM_MINISTERIAL, SOO_AND_SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return psOu.getMinisterialWithoutExclusiveAndTA();
            }
        })
        //|особ. права|СОО|округл(exclusivePlan/2)|
        .put(new MultiKey(EQUAL_PARTS, BS_SOO_AND_VO_APART_SPO, EXCLUSIVE, SOO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return half(psOu.getExclusivePlan());
            }
        })
        //|особ. права|СПО|exclusivePlan-округл(exclusivePlan/2)|
        .put(new MultiKey(EQUAL_PARTS, BS_SOO_AND_VO_APART_SPO, EXCLUSIVE, SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return restFromHalf(psOu.getExclusivePlan());
            }
        })
        //|ЦП|СОО|округл(targetAdmPlan/2)|
        .put(new MultiKey(EQUAL_PARTS, BS_SOO_AND_VO_APART_SPO, TARGET_ADMISSION, SOO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return half(psOu.getTargetAdmPlan());
            }
        })
        //|ЦП|СПО|targetAdmPlan-округл(targetAdmPlan/2)|
        .put(new MultiKey(EQUAL_PARTS, BS_SOO_AND_VO_APART_SPO, TARGET_ADMISSION, SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return restFromHalf(psOu.getTargetAdmPlan());
            }
        })
        //|Общий конкурс|СОО|округл(ministerialPlan/2)|
        .put(new MultiKey(EQUAL_PARTS, BS_SOO_AND_VO_APART_SPO, MINISTERIAL, SOO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return half(psOu.getMinisterialWithoutExclusiveAndTA());
            }
        })
        //|Общий конкурс|СПО|ministerialPlan-округл(ministerialPlan/2)|
        .put(new MultiKey(EQUAL_PARTS, BS_SOO_AND_VO_APART_SPO, MINISTERIAL, SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return restFromHalf(psOu.getMinisterialWithoutExclusiveAndTA());
            }
        })
        //|Без ВИ (договор)|СОО и СПО|contractPlan|
        .put(new MultiKey(EQUAL_PARTS, BS_SOO_AND_VO_APART_SPO, NO_EXAM_CONTRACT, SOO_AND_SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return psOu.getContractPlan();
            }
        })
        //|Договор|СОО и ВО|округл(contractPlan/2)|
        .put(new MultiKey(EQUAL_PARTS, BS_SOO_AND_VO_APART_SPO, CONTRACT, SOO_AND_VO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return half(psOu.getContractPlan());
            }
        })
        //|Договор|СПО|contractPlan-округл(contractPlan/2)|
        .put(new MultiKey(EQUAL_PARTS, BS_SOO_AND_VO_APART_SPO, CONTRACT, SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return restFromHalf(psOu.getContractPlan());
            }
        })

        // ------------- деление по сдавшим ВИ

        // 1. Способ деления конкурсов набора ОП — без деления
        //|без ВИ (КЦП)|ministerialPlan|
        .put(new MultiKey(BY_PASSED, BS_NO_DIV, NO_EXAM_MINISTERIAL), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return psOu.getMinisterialWithoutExclusiveAndTA();
            }
        })
        //|особ. права|exclusivePlan|
        .put(new MultiKey(BY_PASSED, BS_NO_DIV, EXCLUSIVE), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return psOu.getExclusivePlan();
            }
        })
        //|ЦП|targetAdmPlan|
        .put(new MultiKey(BY_PASSED, BS_NO_DIV, TARGET_ADMISSION), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return psOu.getTargetAdmPlan();
            }
        })
        //|Общий конкурс|ministerialPlan|
        .put(new MultiKey(BY_PASSED, BS_NO_DIV, MINISTERIAL), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return psOu.getMinisterialWithoutExclusiveAndTA();
            }
        })
        //|Без ВИ (договор)|contractPlan|
        .put(new MultiKey(BY_PASSED, BS_NO_DIV, NO_EXAM_CONTRACT), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return psOu.getContractPlan();
            }
        })
        //|Договор|contractPlan|
        .put(new MultiKey(BY_PASSED, BS_NO_DIV, CONTRACT), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return psOu.getContractPlan();
            }
        })

        //2. Способ деления конкурсов набора ОП — СОО, СПО, ВО
        //|без ВИ (КЦП)|СОО и СПО|ministerialPlan|
        .put(new MultiKey(BY_PASSED, BS_SOO_SPO_VO, NO_EXAM_MINISTERIAL, SOO_AND_SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return psOu.getMinisterialWithoutExclusiveAndTA();
            }
        })
        //|особ. права|СОО|
        // если усп(СОО)+усп(СПО)>0, то округл(exclusivePlan*усп(СОО)/(усп(СОО)+усп(СПО)))
        // иначе округл(exclusivePlan/2)
        .put(new MultiKey(BY_PASSED, BS_SOO_SPO_VO, EXCLUSIVE, SOO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                if (c.s(EXCLUSIVE, SOO) + c.s(EXCLUSIVE, SPO) > 0) return mult(psOu.getExclusivePlan(), k(c, EXCLUSIVE, SOO, SOO, SPO));
                return half(psOu.getExclusivePlan());
            }
        })
        //особ. права|СПО|
        //если усп(СОО)+усп(СПО)>0, то exclusivePlan-округл(exclusivePlan*усп(СОО)/(усп(СОО)+усп(СПО)))
        // иначе  exclusivePlan-округл(exclusivePlan/2)|
        .put(new MultiKey(BY_PASSED, BS_SOO_SPO_VO, EXCLUSIVE, SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                if (c.s(EXCLUSIVE, SOO) + c.s(EXCLUSIVE, SPO) > 0) return restFromMult(psOu.getExclusivePlan(), k(c, EXCLUSIVE, SOO, SOO, SPO));
                return half(psOu.getExclusivePlan());
            }
        })
        //ЦП|СОО|
        //если усп(СОО)+усп(СПО)>0, то округл(targetAdmPlan*усп(СОО)/(усп(СОО)+усп(СПО)))
        // иначе  округл(targetAdmPlan/2)|
        .put(new MultiKey(BY_PASSED, BS_SOO_SPO_VO, TARGET_ADMISSION, SOO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                if (c.s(TARGET_ADMISSION, SOO) + c.s(TARGET_ADMISSION, SPO) > 0) return mult(psOu.getTargetAdmPlan(), k(c, TARGET_ADMISSION, SOO, SOO, SPO));
                return half(psOu.getTargetAdmPlan());
            }
        })
        //ЦП|СПО|
        //если усп(СОО)+усп(СПО)>0, то targetAdmPlan-округл(targetAdmPlan*усп(СОО)/(усп(СОО)+усп(СПО)))
        // иначе  targetAdmPlan-округл(targetAdmPlan/2)|
        .put(new MultiKey(BY_PASSED, BS_SOO_SPO_VO, TARGET_ADMISSION, SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                if (c.s(TARGET_ADMISSION, SOO) + c.s(TARGET_ADMISSION, SPO) > 0) return restFromMult(psOu.getTargetAdmPlan(), k(c, TARGET_ADMISSION, SOO, SOO, SPO));
                return restFromHalf(psOu.getTargetAdmPlan());
            }
        })
        //Общий конкурс|СОО|
        //если усп(СОО)+усп(СПО)>0, то округл((ministerialWithoutExclusiveAndTA)*усп(СОО)/(усп(СОО)+усп(СПО)))
        // иначе  округл((ministerialWithoutExclusiveAndTA)/2)|
        .put(new MultiKey(BY_PASSED, BS_SOO_SPO_VO, MINISTERIAL, SOO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                if (c.s(MINISTERIAL, SOO) + c.s(MINISTERIAL, SPO) > 0)
                    return mult(psOu.getMinisterialWithoutExclusiveAndTA(), k(c, MINISTERIAL, SOO, SOO, SPO));
                return half(psOu.getMinisterialWithoutExclusiveAndTA());
            }
        })
        //Общий конкурс|СПО|
        //если усп(СОО)+усп(СПО)>0, то ministerialWithoutExclusiveAndTA-округл((ministerialWithoutExclusiveAndTA)*усп(СОО)/(усп(СОО)+усп(СПО)))
        // иначе  ministerialWithoutExclusiveAndTA-округл((ministerialWithoutExclusiveAndTA)/2)|
        .put(new MultiKey(BY_PASSED, BS_SOO_SPO_VO, MINISTERIAL, SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                if (c.s(MINISTERIAL, SOO) + c.s(MINISTERIAL, SPO) > 0)
                    return restFromMult(psOu.getMinisterialWithoutExclusiveAndTA(), k(c, MINISTERIAL, SOO, SOO, SPO));
                return restFromHalf(psOu.getMinisterialWithoutExclusiveAndTA());
            }
        })
        //Без ВИ (договор)|СОО и СПО|contractPlan|
        .put(new MultiKey(BY_PASSED, BS_SOO_SPO_VO, NO_EXAM_CONTRACT, SOO_AND_SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return psOu.getContractPlan();
            }
        })
        //Договор|СОО|
        //если усп(СОО)+усп(СПО)+усп(ВО)>0, то contractPlan-округл(contractPlan*усп(СПО)/(усп(СОО)+усп(СПО)+усп(ВО)))-округл(contractPlan*усп(ВО)/(усп(СОО)+усп(СПО)+усп(ВО)))
        // иначе  contractPlan-2*округл(contractPlan/3)|
        .put(new MultiKey(BY_PASSED, BS_SOO_SPO_VO, CONTRACT, SOO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                if (c.s(CONTRACT, SOO) + c.s(CONTRACT, SPO) + c.s(CONTRACT, VO) > 0)
                    return restFromMult(psOu.getContractPlan(), k(c, CONTRACT, SPO, SOO, SPO, VO), k(c, CONTRACT, VO, SOO, SPO, VO));
                return restFromTwoThirds(psOu.getContractPlan());
            }
        })
        //Договор|СПО|
        //если усп(СОО)+усп(СПО)+усп(ВО)>0, то округл(contractPlan*усп(СПО)/(усп(СОО)+усп(СПО)+усп(ВО)))
        // иначе  округл(contractPlan/3)|
        .put(new MultiKey(BY_PASSED, BS_SOO_SPO_VO, CONTRACT, SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                if (c.s(CONTRACT, SOO) + c.s(CONTRACT, SPO) + c.s(CONTRACT, VO) > 0)
                    return mult(psOu.getContractPlan(), k(c, CONTRACT, SPO, SOO, SPO, VO));
                return third(psOu.getContractPlan());
            }
        })
        //Договор|ВО|
        //если усп(СОО)+усп(СПО)+усп(ВО)>0, то округл(contractPlan*усп(ВО)/(усп(СОО)+усп(СПО)+усп(ВО)))
        // иначе  округл(contractPlan/3)|
        .put(new MultiKey(BY_PASSED, BS_SOO_SPO_VO, CONTRACT, VO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                if (c.s(CONTRACT, SOO) + c.s(CONTRACT, SPO) + c.s(CONTRACT, VO) > 0) return mult(psOu.getContractPlan(), k(c, CONTRACT, VO, SOO, SPO, VO));
                return third(psOu.getContractPlan());
            }
        })

        //3. Способ деления конкурсов набора ОП — СОО, ПО
        //||Вид приема||На базе||Число мест||
        //без ВИ (КЦП)|СОО и СПО|ministerialWithoutExclusiveAndTA|
        .put(new MultiKey(BY_PASSED, BS_SOO_PO, NO_EXAM_MINISTERIAL, SOO_AND_SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return psOu.getMinisterialWithoutExclusiveAndTA();
            }
        })
        //особ. права|СОО|
        //если усп(СОО)+усп(СПО)>0, то округл(exclusivePlan*усп(СОО)/(усп(СОО)+усп(СПО)))
        // иначе  округл(exclusivePlan/2)|
        .put(new MultiKey(BY_PASSED, BS_SOO_PO, EXCLUSIVE, SOO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                if (c.s(EXCLUSIVE, SOO) + c.s(EXCLUSIVE, SPO) > 0) return mult(psOu.getExclusivePlan(), k(c, EXCLUSIVE, SOO, SOO, SPO));
                return half(psOu.getExclusivePlan());
            }
        })
        //особ. права|СПО|
        //если усп(СОО)+усп(СПО)>0, то exclusivePlan-округл(exclusivePlan*усп(СОО)/(усп(СОО)+усп(СПО)))
        // иначе  exclusivePlan-округл(exclusivePlan/2)|
        .put(new MultiKey(BY_PASSED, BS_SOO_PO, EXCLUSIVE, SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                if (c.s(EXCLUSIVE, SOO) + c.s(EXCLUSIVE, SPO) > 0) return restFromMult(psOu.getExclusivePlan(), k(c, EXCLUSIVE, SOO, SOO, SPO));
                return restFromHalf(psOu.getExclusivePlan());
            }
        })
        //ЦП|СОО|
        //если усп(СОО)+усп(СПО)>0, то округл(targetAdmPlan*усп(СОО)/(усп(СОО)+усп(СПО)))
        // иначе  округл(targetAdmPlan/2)|
        .put(new MultiKey(BY_PASSED, BS_SOO_PO, TARGET_ADMISSION, SOO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                if (c.s(TARGET_ADMISSION, SOO) + c.s(TARGET_ADMISSION, SPO) > 0) return mult(psOu.getTargetAdmPlan(), k(c, TARGET_ADMISSION, SOO, SOO, SPO));
                return half(psOu.getTargetAdmPlan());
            }
        })
        //ЦП|СПО|
        //если усп(СОО)+усп(СПО)>0, то targetAdmPlan-округл(targetAdmPlan*усп(СОО)/(усп(СОО)+усп(СПО)))
        // иначе  targetAdmPlan-округл(targetAdmPlan/2)|
        .put(new MultiKey(BY_PASSED, BS_SOO_PO, TARGET_ADMISSION, SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                if (c.s(TARGET_ADMISSION, SOO) + c.s(TARGET_ADMISSION, SPO) > 0) return restFromMult(psOu.getTargetAdmPlan(), k(c, TARGET_ADMISSION, SOO, SOO, SPO));
                return restFromHalf(psOu.getTargetAdmPlan());
            }
        })
        //Общий конкурс|СОО|
        //если усп(СОО)+усп(СПО)>0, то округл((ministerialWithoutExclusiveAndTA)*усп(СОО)/(усп(СОО)+усп(СПО)))
        // иначе  округл((ministerialWithoutExclusiveAndTA)/2)|
        .put(new MultiKey(BY_PASSED, BS_SOO_PO, MINISTERIAL, SOO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                if (c.s(MINISTERIAL, SOO) + c.s(MINISTERIAL, SPO) > 0)
                    return mult(psOu.getMinisterialWithoutExclusiveAndTA(), k(c, MINISTERIAL, SOO, SOO, SPO));
                return half(psOu.getMinisterialWithoutExclusiveAndTA());
            }
        })
        //Общий конкурс|СПО|
        //если усп(СОО)+усп(СПО)>0, то ministerialWithoutExclusiveAndTA-округл((ministerialWithoutExclusiveAndTA)*усп(СОО)/(усп(СОО)+усп(СПО)))
        // иначе  ministerialWithoutExclusiveAndTA-округл((ministerialWithoutExclusiveAndTA)/2)|
        .put(new MultiKey(BY_PASSED, BS_SOO_PO, MINISTERIAL, SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                if (c.s(MINISTERIAL, SOO) + c.s(MINISTERIAL, SPO) > 0)
                    return restFromMult(psOu.getMinisterialWithoutExclusiveAndTA(), k(c, MINISTERIAL, SOO, SOO, SPO));
                return restFromHalf(psOu.getMinisterialWithoutExclusiveAndTA());
            }
        })
        //|Без ВИ (договор)|СОО и СПО|contractPlan|
        .put(new MultiKey(BY_PASSED, BS_SOO_PO, NO_EXAM_CONTRACT, SOO_AND_SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return psOu.getContractPlan();
            }
        })
        //Договор|СОО|
        //если усп(СОО)+усп(ПО), то округл(contractPlan*усп(СОО)/(усп(СОО)+усп(ПО)))
        // иначе  округл(contractPlan/2)|
        .put(new MultiKey(BY_PASSED, BS_SOO_PO, CONTRACT, SOO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                if (c.s(CONTRACT, SOO) + c.s(CONTRACT, PO) > 0) return mult(psOu.getContractPlan(), k(c, CONTRACT, SOO, SOO, PO));
                return half(psOu.getContractPlan());
            }
        })
        //Договор|ПО|
        //если усп(СОО)+усп(ПО), то contractPlan-округл(contractPlan*усп(СОО)/(усп(СОО)+усп(ПО)))
        // иначе  contractPlan-округл(contractPlan/2)|
        .put(new MultiKey(BY_PASSED, BS_SOO_PO, CONTRACT, PO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                if (c.s(CONTRACT, SOO) + c.s(CONTRACT, PO) > 0) return restFromMult(psOu.getContractPlan(), k(c, CONTRACT, SOO, SOO, PO));
                return restFromHalf(psOu.getContractPlan());
            }
        })

        //4. Способ деления конкурсов набора ОП — СОО и СПО, ВО
        //||Вид приема||На базе||Число мест||
        //|без ВИ (КЦП)|СОО и СПО|ministerialPlan|
        .put(new MultiKey(BY_PASSED, BS_SOO_AND_SPO_APART_VO, NO_EXAM_MINISTERIAL, SOO_AND_SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return psOu.getMinisterialWithoutExclusiveAndTA();
            }
        })
        //|особ. права|СОО и СПО|exclusivePlan|
        .put(new MultiKey(BY_PASSED, BS_SOO_AND_SPO_APART_VO, EXCLUSIVE, SOO_AND_SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return psOu.getExclusivePlan();
            }
        })
        //|ЦП|СОО и СПО|targetAdmPlan|
        .put(new MultiKey(BY_PASSED, BS_SOO_AND_SPO_APART_VO, TARGET_ADMISSION, SOO_AND_SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return psOu.getTargetAdmPlan();
            }
        })
        //|Общий конкурс|СОО и СПО|ministerialPlan|
        .put(new MultiKey(BY_PASSED, BS_SOO_AND_SPO_APART_VO, MINISTERIAL, SOO_AND_SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return psOu.getMinisterialWithoutExclusiveAndTA();
            }
        })
        //|Без ВИ (договор)|СОО и СПО|contractPlan|
        .put(new MultiKey(BY_PASSED, BS_SOO_AND_SPO_APART_VO, NO_EXAM_CONTRACT, SOO_AND_SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return psOu.getContractPlan();
            }
        })
        //Договор|СОО и СПО|
        //если усп(СОО и СПО)+усп(ВО)>0, то округл(contractPlan*усп(СОО и СПО)/(усп(СОО и СПО)+усп(ВО)))
        // иначе  округл(contractPlan/2)|
        .put(new MultiKey(BY_PASSED, BS_SOO_AND_SPO_APART_VO, CONTRACT, SOO_AND_SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                if (c.s(CONTRACT, SOO_AND_SPO) + c.s(CONTRACT, VO) > 0)
                    return mult(psOu.getContractPlan(), k(c, CONTRACT, SOO_AND_SPO, SOO_AND_SPO, VO));
                return half(psOu.getContractPlan());
            }
        })
        //Договор|ВО|
        //если усп(СОО и СПО)+усп(ВО)>0, то contractPlan-округл(contractPlan*усп(СОО и СПО)/(усп(СОО и СПО)+усп(ВО)))
        // иначе  contractPlan-округл(contractPlan/2)|
        .put(new MultiKey(BY_PASSED, BS_SOO_AND_SPO_APART_VO, CONTRACT, VO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                if (c.s(CONTRACT, SOO_AND_SPO) + c.s(CONTRACT, VO) > 0)
                    return restFromMult(psOu.getContractPlan(), k(c, CONTRACT, SOO_AND_SPO, SOO_AND_SPO, VO));
                return restFromHalf(psOu.getContractPlan());
            }
        })

        //5. Способ деления конкурсов набора ОП — СОО и ВО, СПО
        //||Вид приема||На базе||Число мест||
        //|без ВИ (КЦП)|СОО и СПО|ministerialPlan|
        .put(new MultiKey(BY_PASSED, BS_SOO_AND_VO_APART_SPO, NO_EXAM_MINISTERIAL, SOO_AND_SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return psOu.getMinisterialWithoutExclusiveAndTA();
            }
        })
        //особ. права|СОО|
        //если усп(СОО)+усп(СПО)>0, то округл(exclusivePlan*усп(СОО)/(усп(СОО)+усп(СПО)))
        // иначе  округл(exclusivePlan/2)|
        .put(new MultiKey(BY_PASSED, BS_SOO_AND_VO_APART_SPO, EXCLUSIVE, SOO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                if (c.s(EXCLUSIVE, SOO) + c.s(EXCLUSIVE, SPO) > 0) return mult(psOu.getExclusivePlan(), k(c, EXCLUSIVE, SOO, SOO, SPO));
                return half(psOu.getExclusivePlan());
            }
        })
        //особ. права|СПО|
        //если усп(СОО)+усп(СПО)>0, то exclusivePlan-округл(exclusivePlan*усп(СОО)/(усп(СОО)+усп(СПО)))
        // иначе  exclusivePlan-округл(exclusivePlan/2)|
        .put(new MultiKey(BY_PASSED, BS_SOO_AND_VO_APART_SPO, EXCLUSIVE, SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                if (c.s(EXCLUSIVE, SOO) + c.s(EXCLUSIVE, SPO) > 0) return restFromMult(psOu.getExclusivePlan(), k(c, EXCLUSIVE, SOO, SOO, SPO));
                return restFromHalf(psOu.getExclusivePlan());
            }
        })
        //ЦП|СОО|
        //если усп(СОО)+усп(СПО)>0, то округл(targetAdmPlan*усп(СОО)/(усп(СОО)+усп(СПО)))
        // иначе  округл(targetAdmPlan/2)|
        .put(new MultiKey(BY_PASSED, BS_SOO_AND_VO_APART_SPO, TARGET_ADMISSION, SOO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                if (c.s(TARGET_ADMISSION, SOO) + c.s(TARGET_ADMISSION, SPO) > 0) return mult(psOu.getTargetAdmPlan(), k(c, TARGET_ADMISSION, SOO, SOO, SPO));
                return half(psOu.getTargetAdmPlan());
            }
        })
        //ЦП|СПО|
        //если усп(СОО)+усп(СПО)>0, то targetAdmPlan-округл(targetAdmPlan*усп(СОО)/(усп(СОО)+усп(СПО)))
        // иначе  targetAdmPlan-округл(targetAdmPlan/2)|
        .put(new MultiKey(BY_PASSED, BS_SOO_AND_VO_APART_SPO, TARGET_ADMISSION, SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                if (c.s(TARGET_ADMISSION, SOO) + c.s(TARGET_ADMISSION, SPO) > 0) return restFromMult(psOu.getTargetAdmPlan(), k(c, TARGET_ADMISSION, SOO, SOO, SPO));
                return restFromHalf(psOu.getTargetAdmPlan());
            }
        })
        //Общий конкурс|СОО|
        //если усп(СОО)+усп(СПО)>0, то округл((ministerialWithoutExclusiveAndTA)*усп(СОО)/(усп(СОО)+усп(СПО)))
        // иначе  округл((ministerialWithoutExclusiveAndTA)/2)|
        .put(new MultiKey(BY_PASSED, BS_SOO_AND_VO_APART_SPO, MINISTERIAL, SOO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                if (c.s(MINISTERIAL, SOO) + c.s(MINISTERIAL, SPO) > 0)
                    return mult(psOu.getMinisterialWithoutExclusiveAndTA(), k(c, MINISTERIAL, SOO, SOO, SPO));
                return half(psOu.getMinisterialWithoutExclusiveAndTA());
            }
        })
        //Общий конкурс|СПО|
        //если усп(СОО)+усп(СПО)>0, то ministerialWithoutExclusiveAndTA-округл((ministerialWithoutExclusiveAndTA)*усп(СОО)/(усп(СОО)+усп(СПО)))
        // иначе  ministerialWithoutExclusiveAndTA-округл((ministerialWithoutExclusiveAndTA)/2)|
        .put(new MultiKey(BY_PASSED, BS_SOO_AND_VO_APART_SPO, MINISTERIAL, SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                if (c.s(MINISTERIAL, SOO) + c.s(MINISTERIAL, SPO) > 0)
                    return restFromMult(psOu.getMinisterialWithoutExclusiveAndTA(), k(c, MINISTERIAL, SOO, SOO, SPO));
                return restFromHalf(psOu.getMinisterialWithoutExclusiveAndTA());
            }
        })
        //|Без ВИ (договор)|СОО и СПО|contractPlan|
        .put(new MultiKey(BY_PASSED, BS_SOO_AND_VO_APART_SPO, NO_EXAM_CONTRACT, SOO_AND_SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return psOu.getContractPlan();
            }
        })
        //Договор|СОО и ВО|
        //если усп(СОО и ВО)+усп(СПО)>0, то округл(contractPlan*усп(СОО и ВО)/(усп(СОО и ВО)+усп(СПО)))
        // иначе  округл(contractPlan/2)|
        .put(new MultiKey(BY_PASSED, BS_SOO_AND_VO_APART_SPO, CONTRACT, SOO_AND_VO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                if (c.s(CONTRACT, SOO_AND_VO) + c.s(CONTRACT, SPO) > 0)
                    return mult(psOu.getContractPlan(), k(c, CONTRACT, SOO_AND_VO, SOO_AND_VO, SPO));
                return half(psOu.getContractPlan());
            }
        })
        //Договор|СПО|
        //если усп(СОО и ВО)+усп(СПО)>0, то contractPlan-округл(contractPlan*усп(СОО и ВО)/(усп(СОО и ВО)+усп(СПО)))
        // иначе  contractPlan-округл(contractPlan/2)|
        .put(new MultiKey(BY_PASSED, BS_SOO_AND_VO_APART_SPO, CONTRACT, SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                if (c.s(CONTRACT, SOO_AND_VO) + c.s(CONTRACT, SPO) > 0)
                    return restFromMult(psOu.getContractPlan(), k(c, CONTRACT, SOO_AND_VO, SOO_AND_VO, SPO));
                return restFromHalf(psOu.getContractPlan());
            }
        })

        // ------------- деление по сдавшим ВИ и перенос мест

        // 1. Способ деления конкурсов набора ОП — без деления
        //без ВИ (КЦП)|зач(без ВИ(КЦП))|
        .put(new MultiKey(BY_PASSED_AND_FREE, BS_NO_DIV, NO_EXAM_MINISTERIAL), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return c.e(NO_EXAM_MINISTERIAL);
            }
        })
        //особ. права|зач(особ. права)|
        .put(new MultiKey(BY_PASSED_AND_FREE, BS_NO_DIV, EXCLUSIVE), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return c.e(EXCLUSIVE);
            }
        })
        //ЦП|зач(ЦП)|
        .put(new MultiKey(BY_PASSED_AND_FREE, BS_NO_DIV, TARGET_ADMISSION), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return c.e(TARGET_ADMISSION);
            }
        })
        //Общий конкурс|ministerialPlan-зач(без ВИ(КЦП))-зач(особ. права)-зач(ЦП)|
        .put(new MultiKey(BY_PASSED_AND_FREE, BS_NO_DIV, MINISTERIAL), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return psOu.getMinisterialPlan() - c.e(NO_EXAM_MINISTERIAL) - c.e(EXCLUSIVE) - c.e(TARGET_ADMISSION);
            }
        })
        //Без ВИ (договор)|зач(без ВИ (договор))|
        .put(new MultiKey(BY_PASSED_AND_FREE, BS_NO_DIV, NO_EXAM_CONTRACT), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return c.e(NO_EXAM_CONTRACT);
            }
        })
        //Договор|contractPlan-зач(без ВИ (договор))|
        .put(new MultiKey(BY_PASSED_AND_FREE, BS_NO_DIV, CONTRACT), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return conBase(psOu, c);
            }
        })

        //2. Способ деления конкурсов набора ОП — СОО, СПО, ВО
        //без ВИ (КЦП)|СОО и СПО|зач(без ВИ(КЦП))|
        .put(new MultiKey(BY_PASSED_AND_FREE, BS_SOO_SPO_VO, NO_EXAM_MINISTERIAL, SOO_AND_SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return c.e(NO_EXAM_MINISTERIAL);
            }
        })
        //особ. права|СОО|зач(особ. права, СОО)|
        .put(new MultiKey(BY_PASSED_AND_FREE, BS_SOO_SPO_VO, EXCLUSIVE, SOO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return c.e(EXCLUSIVE, SOO);
            }
        })
        ///особ. права|СПО|зач(особ. права, СПО)|
        .put(new MultiKey(BY_PASSED_AND_FREE, BS_SOO_SPO_VO, EXCLUSIVE, SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return c.e(EXCLUSIVE, SPO);
            }
        })
        //ЦП|СОО|зач(ЦП, СОО)|
        .put(new MultiKey(BY_PASSED_AND_FREE, BS_SOO_SPO_VO, TARGET_ADMISSION, SOO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return c.e(TARGET_ADMISSION, SOO);
            }
        })
        //ЦП|СПО|зач(ЦП, СПО)|
        .put(new MultiKey(BY_PASSED_AND_FREE, BS_SOO_SPO_VO, TARGET_ADMISSION, SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return c.e(TARGET_ADMISSION, SPO);
            }
        })
        //Общий конкурс|СОО|
        //если усп(СОО)+усп(СПО)>0, то округл((ministerialPlan-зач(без ВИ(КЦП))-зач(особ. права, СОО)-зач(особ. права, СПО)-зач(ЦП, СОО))-зач(ЦП, СПО))*усп(СОО)/(усп(СОО)+усп(СПО)))
        // иначе  округл((ministerialPlan-зач(без ВИ(КЦП))-зач(особ. права, СОО)-зач(особ. права, СПО)-зач(ЦП, СОО))-зач(ЦП, СПО))/2)|
        .put(new MultiKey(BY_PASSED_AND_FREE, BS_SOO_SPO_VO, MINISTERIAL, SOO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                int base = minBase(psOu, c);
                if (c.s(MINISTERIAL, SOO) + c.s(MINISTERIAL, SPO) > 0) return mult(base, k(c, MINISTERIAL, SOO, SOO, SPO));
                return half(base);
            }
        })
        //Общий конкурс|СПО|
        //если усп(СОО)+усп(СПО)>0, то ministerialPlan-зач(без ВИ(КЦП))-зач(особ. права, СОО)-зач(особ. права, СПО)-зач(ЦП, СОО))-зач(ЦП, СПО)-округл((ministerialPlan-зач(без ВИ(КЦП))-зач(особ. права, СОО)-зач(особ. права, СПО)-зач(ЦП, СОО))-зач(ЦП, СПО))*усп(СОО)/(усп(СОО)+усп(СПО)))
        // иначе  ministerialPlan-зач(без ВИ(КЦП))-зач(особ. права, СОО)-зач(особ. права, СПО)-зач(ЦП, СОО))-зач(ЦП, СПО)-округл((ministerialPlan-зач(без ВИ(КЦП))-зач(особ. права, СОО)-зач(особ. права, СПО)-зач(ЦП, СОО))-зач(ЦП, СПО))/2)|
        .put(new MultiKey(BY_PASSED_AND_FREE, BS_SOO_SPO_VO, MINISTERIAL, SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                int base = minBase(psOu, c);
                if (c.s(MINISTERIAL, SOO) + c.s(MINISTERIAL, SPO) > 0) return mult(base, k(c, MINISTERIAL, SPO, SOO, SPO));
                return restFromHalf(base);
            }
        })
        //Без ВИ (договор)|СОО и СПО|зач(без ВИ (договор))|
        .put(new MultiKey(BY_PASSED_AND_FREE, BS_SOO_SPO_VO, NO_EXAM_CONTRACT, SOO_AND_SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return c.e(NO_EXAM_CONTRACT);
            }
        })
        //Договор|СОО|
        //если усп(СОО)+усп(СПО)+усп(ВО)>0, то contractPlan-зач(без ВИ(договор))-округл((contractPlan-зач(без ВИ(договор)))*усп(СПО)/(усп(СОО)+усп(СПО)+усп(ВО)))-округл((contractPlan-зач(без ВИ(договор)))*усп(ВО)/(усп(СОО)+усп(СПО)+усп(ВО)))
        // иначе  contractPlan-зач(без ВИ(договор))-2*округл((contractPlan-зач(без ВИ(договор)))/3)|
        .put(new MultiKey(BY_PASSED_AND_FREE, BS_SOO_SPO_VO, CONTRACT, SOO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                int base = conBase(psOu, c);
                if (c.s(CONTRACT, SOO) + c.s(CONTRACT, SPO) + c.s(CONTRACT, VO) > 0) return restFromMult(base, k(c, CONTRACT, SPO, SOO, SPO, VO), k(c, CONTRACT, VO, SOO, SPO, VO));
                return restFromTwoThirds(base);
            }
        })
        //Договор|СПО|
        //если усп(СОО)+усп(СПО)+усп(ВО)>0, то округл((contractPlan-зач(без ВИ(договор)))*усп(СПО)/(усп(СОО)+усп(СПО)+усп(ВО)))
        // иначе  округл((contractPlan-зач(без ВИ(договор)))/3)|
        .put(new MultiKey(BY_PASSED_AND_FREE, BS_SOO_SPO_VO, CONTRACT, SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                int base = conBase(psOu, c);
                if (c.s(CONTRACT, SOO) + c.s(CONTRACT, SPO) + c.s(CONTRACT, VO) > 0) return mult(base, k(c, CONTRACT, SPO, SOO, SPO, VO));
                return third(base);
            }
        })
        //Договор|ВО|
        //если усп(СОО)+усп(СПО)+усп(ВО)>0, то округл((contractPlan-зач(без ВИ(договор)))*усп(ВО)/(усп(СОО)+усп(СПО)+усп(ВО)))
        // иначе  округл((contractPlan-зач(без ВИ(договор)))/3)|
        .put(new MultiKey(BY_PASSED_AND_FREE, BS_SOO_SPO_VO, CONTRACT, VO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                int base = conBase(psOu, c);
                if (c.s(CONTRACT, SOO) + c.s(CONTRACT, SPO) + c.s(CONTRACT, VO) > 0) return mult(base, k(c, CONTRACT, VO, SOO, SPO, VO));
                return third(base);
            }
        })

        //3. Способ деления конкурсов набора ОП — СОО, ПО
        //||Вид приема||На базе||Число мест||
        //без ВИ (КЦП)|СОО и СПО|зач(без ВИ(КЦП))|
        .put(new MultiKey(BY_PASSED_AND_FREE, BS_SOO_PO, NO_EXAM_MINISTERIAL, SOO_AND_SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return c.e(NO_EXAM_MINISTERIAL);
            }
        })
        //особ. права|СОО|зач(особ. права, СОО)|
        .put(new MultiKey(BY_PASSED_AND_FREE, BS_SOO_PO, EXCLUSIVE, SOO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return c.e(EXCLUSIVE, SOO);
            }
        })
        //особ. права|СПО|зач(особ. права, СПО)|
        .put(new MultiKey(BY_PASSED_AND_FREE, BS_SOO_PO, EXCLUSIVE, SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return c.e(EXCLUSIVE, SPO);
            }
        })
        //ЦП|СОО|зач(ЦП, СОО)|
        .put(new MultiKey(BY_PASSED_AND_FREE, BS_SOO_PO, TARGET_ADMISSION, SOO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return c.e(TARGET_ADMISSION, SOO);
            }
        })
        //ЦП|СПО|зач(ЦП, СПО)|
        .put(new MultiKey(BY_PASSED_AND_FREE, BS_SOO_PO, TARGET_ADMISSION, SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return c.e(TARGET_ADMISSION, SPO);
            }
        })
        //Общий конкурс|СОО|
        //если усп(СОО)+усп(СПО)>0, то округл((ministerialPlan-зач(без ВИ(КЦП))-зач(особ. права, СОО)-зач(особ. права, СПО)-зач(ЦП, СОО))-зач(ЦП, СПО))*усп(СОО)/(усп(СОО)+усп(СПО)))
        // иначе  округл((ministerialPlan-зач(без ВИ(КЦП))-зач(особ. права, СОО)-зач(особ. права, СПО)-зач(ЦП, СОО))-зач(ЦП, СПО))/2)|
        .put(new MultiKey(BY_PASSED_AND_FREE, BS_SOO_PO, MINISTERIAL, SOO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                int base = minBase(psOu, c);
                if (c.s(MINISTERIAL, SOO) + c.s(MINISTERIAL, SPO) > 0) return mult(base, k(c, MINISTERIAL, SOO, SOO, SPO));
                return half(base);
            }
        })
        //Общий конкурс|СПО|
        //если усп(СОО)+усп(СПО)>0, то ministerialPlan-зач(без ВИ(КЦП))-зач(особ. права, СОО)-зач(особ. права, СПО)-зач(ЦП, СОО))-зач(ЦП, СПО)-округл((ministerialPlan-зач(без ВИ(КЦП))-зач(особ. права, СОО)-зач(особ. права, СПО)-зач(ЦП, СОО))-зач(ЦП, СПО))*усп(СОО)/(усп(СОО)+усп(СПО)))
        // иначе  ministerialPlan-зач(без ВИ(КЦП))-зач(особ. права, СОО)-зач(особ. права, СПО)-зач(ЦП, СОО))-зач(ЦП, СПО)-округл((ministerialPlan-зач(без ВИ(КЦП))-зач(особ. права, СОО)-зач(особ. права, СПО)-зач(ЦП, СОО))-зач(ЦП, СПО))/2)|
        .put(new MultiKey(BY_PASSED_AND_FREE, BS_SOO_PO, MINISTERIAL, SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                int base = minBase(psOu, c);
                if (c.s(MINISTERIAL, SOO) + c.s(MINISTERIAL, SPO) > 0) return restFromMult(base, k(c, MINISTERIAL, SOO, SOO, SPO));
                return restFromHalf(base);
            }
        })
        //Без ВИ (договор)|СОО и СПО|зач(без ВИ (договор))|
        .put(new MultiKey(BY_PASSED_AND_FREE, BS_SOO_PO, NO_EXAM_CONTRACT, SOO_AND_SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return c.e(NO_EXAM_CONTRACT);
            }
        })
        //Договор|СОО|
        //если усп(СОО)+усп(ПО), то округл((contractPlan-зач(без ВИ (договор)))*усп(СОО)/(усп(СОО)+усп(ПО)))
        // иначе  округл((contractPlan-зач(без ВИ(договор)))/2)|
        .put(new MultiKey(BY_PASSED_AND_FREE, BS_SOO_PO, CONTRACT, SOO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                int base = conBase(psOu, c);
                if (c.s(CONTRACT, SOO) + c.s(CONTRACT, PO) > 0) return mult(base, k(c, CONTRACT, SOO, SOO, PO));
                return half(base);
            }
        })
        //Договор|ПО|
        //если усп(СОО)+усп(ПО), то contractPlan-зач(без ВИ(договор))-округл((contractPlan-зач(без ВИ(договор)))*усп(СОО)/(усп(СОО)+усп(ПО)))
        // иначе  contractPlan-зач(без ВИ(договор))-округл((contractPlan-зач(без ВИ(договор)))/2)|
        .put(new MultiKey(BY_PASSED_AND_FREE, BS_SOO_PO, CONTRACT, PO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                int base = conBase(psOu, c);
                if (c.s(CONTRACT, SOO) + c.s(CONTRACT, PO) > 0) return restFromMult(base, k(c, CONTRACT, SOO, SOO, PO));
                return restFromHalf(base);
            }
        })

        //4. Способ деления конкурсов набора ОП — СОО и СПО, ВО
        //||Вид приема||На базе||Число мест||
        //без ВИ (КЦП)|СОО и СПО|зач(без ВИ (КЦП))|
        .put(new MultiKey(BY_PASSED_AND_FREE, BS_SOO_AND_SPO_APART_VO, NO_EXAM_MINISTERIAL, SOO_AND_SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return c.e(NO_EXAM_MINISTERIAL);
            }
        })
        //особ. права|СОО и СПО|зач(особ. права)|
        .put(new MultiKey(BY_PASSED_AND_FREE, BS_SOO_AND_SPO_APART_VO, EXCLUSIVE, SOO_AND_SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return c.e(EXCLUSIVE);
            }
        })
        //ЦП|СОО и СПО|зач(ЦП)|
        .put(new MultiKey(BY_PASSED_AND_FREE, BS_SOO_AND_SPO_APART_VO, TARGET_ADMISSION, SOO_AND_SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return c.e(TARGET_ADMISSION);
            }
        })
        //Общий конкурс|СОО и СПО|ministerialPlan-зач(без ВИ (КЦП))-зач(особ. права)-зач(ЦП)|
        .put(new MultiKey(BY_PASSED_AND_FREE, BS_SOO_AND_SPO_APART_VO, MINISTERIAL, SOO_AND_SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return psOu.getMinisterialPlan()-c.e(NO_EXAM_MINISTERIAL)-c.e(EXCLUSIVE)-c.e(TARGET_ADMISSION);
            }
        })
        //Без ВИ (договор)|СОО и СПО|зач(без ВИ (договор))|
        .put(new MultiKey(BY_PASSED_AND_FREE, BS_SOO_AND_SPO_APART_VO, NO_EXAM_CONTRACT, SOO_AND_SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return c.e(NO_EXAM_CONTRACT);
            }
        })
        //Договор|СОО и СПО|
        //если усп(СОО и СПО)+усп(ВО)>0, то округл((contractPlan-зач(без ВИ (договор)))*усп(СОО и СПО)/(усп(СОО и СПО)+усп(ВО)))
        // иначе  округл((contractPlan-зач(без ВИ (договор)))/2)|
        .put(new MultiKey(BY_PASSED_AND_FREE, BS_SOO_AND_SPO_APART_VO, CONTRACT, SOO_AND_SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                int base = conBase(psOu, c);
                if (c.s(CONTRACT, SOO_AND_SPO) + c.s(CONTRACT, VO) > 0) return mult(base, k(c, CONTRACT, SOO_AND_SPO, SOO_AND_SPO, VO));
                return half(psOu.getContractPlan());
            }
        })
        //Договор|ВО|
        //если усп(СОО и СПО)+усп(ВО)>0, то contractPlan-зач(без ВИ (договор))-округл((contractPlan-зач(без ВИ (договор)))*усп(СОО и СПО)/(усп(СОО и СПО)+усп(ВО)))
        // иначе  contractPlan-зач(без ВИ (договор))-округл((contractPlan-зач(без ВИ (договор)))/2)|
        .put(new MultiKey(BY_PASSED_AND_FREE, BS_SOO_AND_SPO_APART_VO, CONTRACT, VO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                int base = conBase(psOu, c);
                if (c.s(CONTRACT, SOO_AND_SPO) + c.s(CONTRACT, VO) > 0) return restFromMult(base, k(c, CONTRACT, SOO_AND_SPO, SOO_AND_SPO, VO));
                return restFromHalf(base);
            }
        })

        //5. Способ деления конкурсов набора ОП — СОО и ВО, СПО
        //||Вид приема||На базе||Число мест||
        //без ВИ (КЦП)|СОО и СПО|зач(без ВИ(КЦП))|
        .put(new MultiKey(BY_PASSED_AND_FREE, BS_SOO_AND_VO_APART_SPO, NO_EXAM_MINISTERIAL, SOO_AND_SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return c.e(NO_EXAM_MINISTERIAL);
            }
        })
        //особ. права|СОО|зач(особ. права, СОО)|
        .put(new MultiKey(BY_PASSED_AND_FREE, BS_SOO_AND_VO_APART_SPO, EXCLUSIVE, SOO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return c.e(EXCLUSIVE, SOO);
            }
        })
        //особ. права|СПО|зач(особ. права, СПО)|
        .put(new MultiKey(BY_PASSED_AND_FREE, BS_SOO_AND_VO_APART_SPO, EXCLUSIVE, SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return c.e(EXCLUSIVE, SPO);
            }
        })
        //ЦП|СОО|зач(ЦП, СОО)|
        .put(new MultiKey(BY_PASSED_AND_FREE, BS_SOO_AND_VO_APART_SPO, TARGET_ADMISSION, SOO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return c.e(TARGET_ADMISSION, SOO);
            }
        })
        //ЦП|СПО|зач(ЦП, СПО)|
        .put(new MultiKey(BY_PASSED_AND_FREE, BS_SOO_AND_VO_APART_SPO, TARGET_ADMISSION, SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return c.e(TARGET_ADMISSION, SPO);
            }
        })
        //Общий конкурс|СОО|
        //если усп(СОО)+усп(СПО)>0, то округл((ministerialPlan-зач(без ВИ(КЦП))-зач(особ. права, СОО)-зач(особ. права, СПО)-зач(ЦП, СОО))-зач(ЦП, СПО))*усп(СОО)/(усп(СОО)+усп(СПО)))
        // иначе  округл((ministerialPlan-зач(без ВИ(КЦП))-зач(особ. права, СОО)-зач(особ. права, СПО)-зач(ЦП, СОО))-зач(ЦП, СПО))/2)|
        .put(new MultiKey(BY_PASSED_AND_FREE, BS_SOO_AND_VO_APART_SPO, MINISTERIAL, SOO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                int base = minBase(psOu, c);
                if (c.s(MINISTERIAL, SOO) + c.s(MINISTERIAL, SPO) > 0) return mult(base, k(c, MINISTERIAL, SOO, SOO, SPO));
                return half(base);
            }
        })
        //Общий конкурс|СПО|
        //если усп(СОО)+усп(СПО)>0, то ministerialPlan-зач(без ВИ(КЦП))-зач(особ. права, СОО)-зач(особ. права, СПО)-зач(ЦП, СОО))-зач(ЦП, СПО)-округл((ministerialPlan-зач(без ВИ(КЦП))-зач(особ. права, СОО)-зач(особ. права, СПО)-зач(ЦП, СОО))-зач(ЦП, СПО))*усп(СОО)/(усп(СОО)+усп(СПО)))
        // иначе  ministerialPlan-зач(без ВИ(КЦП))-зач(особ. права, СОО)-зач(особ. права, СПО)-зач(ЦП, СОО))-зач(ЦП, СПО)-округл((ministerialPlan-зач(без ВИ(КЦП))-зач(особ. права, СОО)-зач(особ. права, СПО)-зач(ЦП, СОО))-зач(ЦП, СПО))/2)|
        .put(new MultiKey(BY_PASSED_AND_FREE, BS_SOO_AND_VO_APART_SPO, MINISTERIAL, SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                int base = minBase(psOu, c);
                if (c.s(MINISTERIAL, SOO) + c.s(MINISTERIAL, SPO) > 0) return restFromMult(base, k(c, MINISTERIAL, SOO, SOO, SPO));
                return half(base);
            }
        })
        //Без ВИ (договор)|СОО и СПО|зач(без ВИ (договор))|
        .put(new MultiKey(BY_PASSED_AND_FREE, BS_SOO_AND_VO_APART_SPO, NO_EXAM_CONTRACT, SOO_AND_SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                return c.e(NO_EXAM_CONTRACT);
            }
        })
        //Договор|СОО и ВО|
        //если усп(СОО и ВО)+усп(СПО)>0, то округл((contractPlan-зач(без ВИ (договор)))*усп(СОО и ВО)/(усп(СОО и ВО)+усп(СПО)))
        // иначе  округл((contractPlan-зач(без ВИ (договор)))/2)|
        .put(new MultiKey(BY_PASSED_AND_FREE, BS_SOO_AND_VO_APART_SPO, CONTRACT, SOO_AND_VO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                int base = conBase(psOu, c);
                if (c.s(CONTRACT, SOO_AND_VO) + c.s(CONTRACT, SPO) > 0) return mult(base, k(c, CONTRACT, SOO_AND_VO, SOO_AND_VO, SPO));
                return half(psOu.getContractPlan());
            }
        })
        //Договор|СПО|
        //если усп(СОО и ВО)+усп(СПО)>0, то contractPlan-зач(без ВИ (договор))-округл((contractPlan-зач(без ВИ (договор)))*усп(СОО и ВО)/(усп(СОО и ВО)+усп(СПО)))
        // иначе  contractPlan-зач(без ВИ (договор))-округл((contractPlan-зач(без ВИ (договор)))/2)|
        .put(new MultiKey(BY_PASSED_AND_FREE, BS_SOO_AND_VO_APART_SPO, CONTRACT, SPO), new EnrCompetitionPlanFormula() {
            @Override int plan(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c) {
                int base = conBase(psOu, c);
                if (c.s(CONTRACT, SOO_AND_VO) + c.s(CONTRACT, SPO) > 0) return restFromMult(base, k(c, CONTRACT, SOO_AND_VO, SOO_AND_VO, SPO));
                return restFromHalf(psOu.getContractPlan());
            }
        })

        .build()
    ;

    private static int conBase(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c)
    {
        return psOu.getContractPlan() - c.e(NO_EXAM_CONTRACT);
    }

    private static int minBase(EnrProgramSetOrgUnit psOu, IPlanCalculationContext c)
    {
        return psOu.getMinisterialPlan() - c.e(NO_EXAM_MINISTERIAL) - c.e(EXCLUSIVE, SOO) - c.e(EXCLUSIVE, SPO) - c.e(TARGET_ADMISSION, SOO) - c.e(TARGET_ADMISSION, SPO);
    }

    public static int restFromTwoThirds(int p) {
        return p - 2 * third(p);
    }

    public static int third(int p) {
        return (int) Math.round(p / 3.);
    }

    public static int restFromHalf(int p) {
        return p - half(p);
    }

    public static int half(int p) {
        return (int) Math.round(p / 2.);
    }
    
    public static int mult(int p, double k) {
        return (int) Math.round(p * k);
    }

    public static int restFromMult(int p, double k) {
        return p - (int) Math.round(p * k);
    }

    public static int restFromMult(int p, double k1, double k2) {
        return p - (int) Math.round(p * k1) - (int) Math.round(p * k2);
    }

    public static double k(IPlanCalculationContext c, String compType, String num, String... dens) {
        double den = .0; for (String s : dens) den += c.s(compType, s);
        return c.s(compType, num) / den;
    }
//
//    public static void main(String args[]) {
//        System.out.println(mult(62, 1.0 / 4));
//    }
}
