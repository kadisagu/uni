package ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.block.entrantData;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IIdentifiableWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLSelectableQuery;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.*;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unienr14.catalog.entity.EnrAccessCourse;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantCustomStateType;
import ru.tandemservice.unienr14.catalog.entity.EnrSourceInfoAboutUniversity;
import ru.tandemservice.unienr14.entrant.entity.*;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.EnrReportPersonAdd;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.EnrReportPersonAddUI;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.EnrEnrollmentCommissionManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Vasily Zhukov
 * @since 08.02.2011
 */
public class EntrantDataParam implements IReportDQLModifier
{
    private final EnrReportPersonAddUI _presenter;

    public EntrantDataParam(EnrReportPersonAddUI presenter)
    {
        _presenter = presenter;
    }

    private IReportParam<DataWrapper> _entrant = new ReportParam<DataWrapper>()
    {
        @Override
        @SuppressWarnings("SimplifiableConditionalExpression")
        public boolean isDisabled()
        {
            if (_presenter.getSummaryReport() == null)
                return super.isDisabled();

            return _presenter.getSummaryReport() ? super.isDisabled() : true;
        }
    };
    private IReportParam<EnrEnrollmentCampaign> _enrollmentCampaign = new ReportParam<>();
    private IReportParam<List<EnrEnrollmentCommission>> _enrollmentCommission = new ReportParam<>();
    private IReportParam<DataWrapper> _hasOlympiadDiploma = new ReportParam<>();
    private IReportParam<List<EnrAccessCourse>> _accessCourses = new ReportParam<>();
    private IReportParam<DataWrapper> _hasExamResult = new ReportParam<>();
    private IReportParam<List<EnrSourceInfoAboutUniversity>> _infoSource = new ReportParam<>();
    private IReportParam<DataWrapper> _entrantArchival = new ReportParam<>();
    private IReportParam<List<EnrEntrantCustomStateType>> _entrantCustomState = new ReportParam<>();
    private IReportParam<DataWrapper> _needSpecialExamConditions = new ReportParam<>();
    private IReportParam<String> _specialExamConditionsDetails = new ReportParam<>();

    private String entrantAlias(ReportDQL dql)
    {
        // соединяем абитуриента
        String entrantAlias = dql.innerJoinEntity(EnrEntrant.class, EnrEntrant.person());

        // добавляем сортировку
        dql.order(entrantAlias, EnrEntrant.enrollmentCampaign().id());

        return entrantAlias;
    }

    private String acAlias(ReportDQL dql)
    {
        // соединяем подготовительный курс абитуриента

        return dql.innerJoinEntity(entrantAlias(dql), EnrEntrantAccessCourse.class, EnrEntrantAccessCourse.entrant());
    }

    private String iauAlias(ReportDQL dql)
    {
        // соединяем выбранный источник информации об ОУ

        return dql.innerJoinEntity(entrantAlias(dql), EnrEntrantInfoAboutUniversity.class, EnrEntrantInfoAboutUniversity.entrant());
    }

    @Override
    public void validateReportParams(ErrorCollector errorCollector)
    {

    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
//        if (_entrant.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "entrantData.entrant", _entrant.getData().getTitle());

            if (TwinComboDataSourceHandler.getSelectedValueNotNull(_entrant.getData()))
            {
                entrantAlias(dql);
            } else
            {
                String entrantAlias = dql.nextAlias();

                dql.builder.where(notIn(property(dql.getFromEntityAlias()),
                        new DQLSelectBuilder().fromEntity(EnrEntrant.class, entrantAlias).column(property(EnrEntrant.person().fromAlias(entrantAlias))).buildQuery()
                ));
            }
        }

//        if (_enrollmentCampaign.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "entrantData.enrollmentCampaign", _enrollmentCampaign.getData().getTitle());

            // запоминаем выбранную приемную кампанию
            printInfo.putSharedObject(EnrReportPersonAdd.PREFETCH_ENROLLMENT_CAMPAIGN, _enrollmentCampaign.getData());

            dql.builder.where(eq(property(EnrEntrant.enrollmentCampaign().fromAlias(entrantAlias(dql))), value(_enrollmentCampaign.getData())));
        }

        if (_enrollmentCommission.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "entrantData.enrollmentCommission", CommonBaseStringUtil.join(_enrollmentCommission.getData(), EnrEnrollmentCommission.P_TITLE, "; "));

            EnrEnrollmentCommissionManager.instance().dao().filterEntrantByEnrCommission(dql.builder, entrantAlias(dql), _enrollmentCommission.getData());
        }

        if (_hasOlympiadDiploma.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "entrantData.hasOlympiadDiploma", _hasOlympiadDiploma.getData().getTitle());

            String alias = dql.nextAlias();

            IDQLSelectableQuery query = new DQLSelectBuilder().fromEntity(EnrEntrantBaseDocument.class, alias)
                    .joinEntity(alias, DQLJoinType.inner, EnrOlympiadDiploma.class, "od", eq(property("od.id"), property(alias, EnrEntrantBaseDocument.docRelation().document().id())))
                    .column(property(EnrOlympiadDiploma.id().fromAlias("od")))
                    .where(eq(property(EnrEntrantBaseDocument.entrant().fromAlias(alias)), property(entrantAlias(dql))))
                    .buildQuery();

            dql.builder.where(TwinComboDataSourceHandler.getSelectedValueNotNull(_hasOlympiadDiploma.getData()) ? exists(query) : notExists(query));
        }

        if (_accessCourses.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "entrantData.accessCourses", CommonBaseStringUtil.join(_accessCourses.getData(), EnrAccessCourse.P_TITLE, ", "));

            if (_accessCourses.getData().isEmpty())
            {
                String nextAlias = dql.nextAlias();

                dql.builder.where(notExists(new DQLSelectBuilder().fromEntity(EnrEntrantAccessCourse.class, nextAlias)
                        .column(property(EnrEntrantAccessCourse.id().fromAlias(nextAlias)))
                        .where(eq(property(EnrEntrantAccessCourse.entrant().fromAlias(nextAlias)), property(entrantAlias(dql))))
                        .buildQuery()
                ));
            } else
                dql.builder.where(in(property(EnrEntrantAccessCourse.course().fromAlias(acAlias(dql))), _accessCourses.getData()));
        }

        if (_hasExamResult.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "entrantData.hasExamResult", _hasExamResult.getData().getTitle());

            String ratingAlias = dql.nextAlias();

            IDQLSelectableQuery query = new DQLSelectBuilder().fromEntity(EnrRatingItem.class, ratingAlias)
                .column(property(EnrRatingItem.id().fromAlias(ratingAlias)))
                .where(eq(property(EnrRatingItem.statusRatingComplete().fromAlias(ratingAlias)), value(Boolean.TRUE)))
                .where(eq(property(EnrRatingItem.entrant().fromAlias(ratingAlias)), property(entrantAlias(dql))))
                .buildQuery();

            dql.builder.where(TwinComboDataSourceHandler.getSelectedValueNotNull(_hasExamResult.getData()) ? exists(query) : notExists(query));
        }

        if (_infoSource.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "entrantData.infoSource", CommonBaseStringUtil.join(_infoSource.getData(), EnrSourceInfoAboutUniversity.P_TITLE, ", "));

            dql.builder.where(in(property(EnrEntrantInfoAboutUniversity.sourceInfo().fromAlias(iauAlias(dql))), _infoSource.getData()));
        }

        if (_entrantArchival.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "entrantData.entrantArchival", _entrantArchival.getData().getTitle());

            dql.builder.where(eq(property(EnrEntrant.archival().fromAlias(entrantAlias(dql))), value(TwinComboDataSourceHandler.getSelectedValueNotNull(_entrantArchival.getData()))));
        }

        if (_entrantCustomState.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "entrantData.entrantCustomState", CommonBaseStringUtil.join(_entrantCustomState.getData(), EnrEntrantCustomStateType.P_TITLE, ", "));

            DQLSelectBuilder csDQL = new DQLSelectBuilder()
                    .fromEntity(EnrEntrantCustomState.class, "ecs").column("ecs.id")
                    .where(eq(property("ecs", EnrEntrantCustomState.entrant()), property(entrantAlias(dql))))
                    .where(in(property("ecs", EnrEntrantCustomState.customState()), _entrantCustomState.getData()));

            FilterUtils.applyInPeriodFilterNullSafe(csDQL, "ecs", EnrEntrantCustomState.P_BEGIN_DATE, EnrEntrantCustomState.P_END_DATE, new Date());

            dql.builder.where(exists(csDQL.buildQuery()));
        }

        if (_needSpecialExamConditions.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "entrantData.needSpecialExamConditions", _needSpecialExamConditions.getData().getTitle());

            dql.builder.where(eq(property(EnrEntrant.needSpecialExamConditions().fromAlias(entrantAlias(dql))), value(TwinComboDataSourceHandler.getSelectedValueNotNull(_needSpecialExamConditions.getData()))));
        }

        if (_specialExamConditionsDetails.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "entrantData.specialExamConditionsDetails", _specialExamConditionsDetails.getData());

            dql.builder.where(likeUpper(property(EnrEntrant.specialExamConditionsDetails().fromAlias(entrantAlias(dql))), value(CoreStringUtils.escapeLike(_specialExamConditionsDetails.getData()))));
        }
    }

    // Getters

    public IReportParam<DataWrapper> getEntrant()
    {
        return _entrant;
    }

    public IReportParam<EnrEnrollmentCampaign> getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public IReportParam<List<EnrEnrollmentCommission>> getEnrollmentCommission()
    {
        return _enrollmentCommission;
    }

    public IReportParam<DataWrapper> getHasOlympiadDiploma()
    {
        return _hasOlympiadDiploma;
    }

    public IReportParam<List<EnrAccessCourse>> getAccessCourses()
    {
        return _accessCourses;
    }

    public IReportParam<DataWrapper> getHasExamResult()
    {
        return _hasExamResult;
    }

    public IReportParam<List<EnrSourceInfoAboutUniversity>> getInfoSource()
    {
        return _infoSource;
    }

    public IReportParam<DataWrapper> getEntrantArchival()
    {
        return _entrantArchival;
    }

    public IReportParam<List<EnrEntrantCustomStateType>> getEntrantCustomState()
    {
        return _entrantCustomState;
    }

    public IReportParam<DataWrapper> getNeedSpecialExamConditions(){ return _needSpecialExamConditions; }

    public IReportParam<String> getSpecialExamConditionsDetails(){ return _specialExamConditionsDetails; }
}
