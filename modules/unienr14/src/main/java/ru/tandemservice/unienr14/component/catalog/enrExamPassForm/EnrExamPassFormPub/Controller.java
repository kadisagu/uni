/**
 *$Id: Controller.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.component.catalog.enrExamPassForm.EnrExamPassFormPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm;

/**
 * @author Alexander Shaburov
 * @since 14.06.13
 */
public class Controller extends DefaultCatalogPubController<EnrExamPassForm, Model, IDAO>
{
    @Override
    protected void addColumnsBeforeEditColumn(IBusinessComponent context, DynamicListDataSource<EnrExamPassForm> dataSource)
    {
        dataSource.addColumn(new SimpleColumn("Внутренняя", EnrExamPassForm.internal().s(), YesNoFormatter.INSTANCE).setClickable(false));
    }
}
