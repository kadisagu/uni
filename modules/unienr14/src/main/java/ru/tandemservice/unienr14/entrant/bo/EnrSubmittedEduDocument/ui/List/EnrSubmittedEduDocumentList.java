/* $Id$ */
package ru.tandemservice.unienr14.entrant.bo.EnrSubmittedEduDocument.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.DefaultPublisherLinkResolver;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.person.base.bo.Person.util.ISecureRoleContext;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import org.tandemframework.shared.person.catalog.entity.EduDocumentKind;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.Pub.EnrEntrantPub;
import ru.tandemservice.unienr14.entrant.bo.EnrSubmittedEduDocument.logic.EnrSubmittedEduDocumentDSHandler;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.legacy.EnrPersonLegacyUtils;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.EnrEnrollmentCommissionManager;

import java.util.HashMap;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author azhebko
 * @since 31.07.2014
 */
@Configuration
public class EnrSubmittedEduDocumentList extends BusinessComponentManager
{
    public static final String ADDON_COMPETITION_FILTER = CommonFilterAddon.class.getSimpleName();
    public static final String DS_EDU_DOCUMENT_KIND = "eduDocumentKindDS";
    public static final String DS_SUBMITTED_EDU_DOCUMENT = "submittedEduDocumentDS";

    public static final String ENR_COMMISSION_LIST_COLUMN = "enrollmentCommissionList";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
            .addAddon(uiAddon(ADDON_COMPETITION_FILTER, EnrCompetitionFilterAddon.class))
            .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
            .addDataSource(EnrEnrollmentCommissionManager.instance().enrollmentCommissionDSConfig())
            .addDataSource(selectDS(DS_EDU_DOCUMENT_KIND, eduDocumentKindDS()))
            .addDataSource(searchListDS(DS_SUBMITTED_EDU_DOCUMENT, submittedEduDocumentDSColumns(), submittedEduDocumentDSHandler()))
            .create();
    }

    @Bean
    public ColumnListExtPoint submittedEduDocumentDSColumns()
    {
        IPublisherLinkResolver eduDocumentPublisherLinkResolver = new DefaultPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                PersonEduDocument eduDocument = ((DataWrapper) entity).getWrapped();
                EnrEntrant entrant = (EnrEntrant) entity.getProperty(EnrSubmittedEduDocumentDSHandler.VIEW_ENTRANT);
                ISecureRoleContext personRoleContext = EnrPersonLegacyUtils.getSecureRoleContext(entrant);

                return new ParametersMap()
                    .add(PublisherActivator.PUBLISHER_ID_KEY, eduDocument.getId())
                    .add("securedObjectId", personRoleContext.getSecuredObject().getId())
                    .add("accessible", personRoleContext.isAccessible())
                    .add("personRoleName", personRoleContext.getPersonRoleName());
            }
        };

        IPublisherLinkResolver entrantPubLinkResolver = new IPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                Long entrantId = ((EnrEntrant) (entity.getProperty(EnrSubmittedEduDocumentDSHandler.VIEW_ENTRANT))).getId();
                Map<String, Object> parameters = new HashMap<>(3);
                parameters.put(PublisherActivator.PUBLISHER_ID_KEY, entrantId);
                parameters.put("selectedTab", "documentTab");
                parameters.put("selectedDataTab", "eduDocumentTab");
                return parameters;
            }

            @Override public String getComponentName(IEntity entity){ return EnrEntrantPub.class.getSimpleName(); }
        };

        return columnListExtPointBuilder(DS_SUBMITTED_EDU_DOCUMENT)
            .addColumn(textColumn("personalNumber", EnrSubmittedEduDocumentDSHandler.VIEW_PERSONAL_NUMBER).order())
            .addColumn(publisherColumn("entrant", EnrSubmittedEduDocumentDSHandler.VIEW_ENTRANT + "." + EnrEntrant.P_FULL_FIO)
                .publisherLinkResolver(entrantPubLinkResolver)
                .required(Boolean.TRUE).order())
            .addColumn(textColumn("sex", PersonEduDocument.person().identityCard().sex().shortTitle()))
            .addColumn(textColumn("identityCard", PersonEduDocument.person().identityCard().fullNumber()))
            .addColumn(textColumn("birthDate", PersonEduDocument.person().identityCard().birthDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER))
            .addColumn(textColumn("eduDocumentKind", PersonEduDocument.eduDocumentKind().shortTitle()).required(Boolean.TRUE).order())
            .addColumn(publisherColumn("eduDocument", PersonEduDocument.fullNumber()).publisherLinkResolver(eduDocumentPublisherLinkResolver).required(Boolean.TRUE))
            .addColumn(textColumn("issuanceDate", PersonEduDocument.issuanceDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER))
			.addColumn(booleanColumn("isMainDocument", EnrSubmittedEduDocumentDSHandler.VIEW_MAIN_DOCUMENT))
            .addColumn(publisherColumn("requestList", "title")
                .entityListProperty(EnrSubmittedEduDocumentDSHandler.VIEW_REQUEST_NUMBER)
                .parameters("mvel:['selectedTab':'requestTab']")
                .formatter(CollectionFormatter.COLLECTION_FORMATTER))
            .addColumn(textColumn(ENR_COMMISSION_LIST_COLUMN, EnrSubmittedEduDocumentDSHandler.VIEW_ENROLLMENT_COMMISSION).formatter(CollectionFormatter.COLLECTION_FORMATTER))
            .addColumn(toggleColumn("original", EnrSubmittedEduDocumentDSHandler.VIEW_ORIGINAL).required(Boolean.TRUE)
                .toggleOnLabel("submittedEduDocumentDS.original.yes").toggleOnListener("onClickSwitchOriginalStatus")
                .toggleOffLabel("submittedEduDocumentDS.original.no").toggleOffListener("onClickSwitchOriginalStatus")
                .permissionKey("enr14SubmittedEduDocumentListDocumentStatusEdit").visible("ui:originalVisible"))
            .addColumn(booleanColumn("originalVal", EnrSubmittedEduDocumentDSHandler.VIEW_ORIGINAL).required(Boolean.TRUE).visible("ui:reqCompVisible"))
            .addColumn(textColumn("reqComp", EnrSubmittedEduDocumentDSHandler.VIEW_REQ_COMP + ".title").visible("ui:reqCompVisible"))
            .addColumn(blockColumn("organizationOriginalIn", "organizationOriginalInBlock"))
            .addColumn(blockColumn("organizationOriginalInAction", "organizationOriginalInActionBlock").width("1").hint("Редактировать").permissionKey("enr14SubmittedEduDocumentListEditOrgOriginalIn"))
            .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler submittedEduDocumentDSHandler()
    {
        return new EnrSubmittedEduDocumentDSHandler(this.getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler eduDocumentKindDS()
    {
        return new EntityComboDataSourceHandler(this.getName(), EduDocumentKind.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(exists(
                    new DQLSelectBuilder()
                        .fromEntity(EnrEntrantRequest.class, "r")
                        .where(eq(property("r", EnrEntrantRequest.eduDocument().eduDocumentKind().id()), property(alias, EduDocumentKind.id())))
                        .where(eq(property("r", EnrEntrantRequest.entrant().enrollmentCampaign()), commonValue(context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN))))
                        .buildQuery()));
            }
        }
            .order(EduDocumentKind.code())
            .filter(EduDocumentKind.title());
    }
}