/* $Id$ */
package ru.tandemservice.unienr14.settings.bo.EnrIndividualAchievement;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

/**
 * @author nvankov
 * @since 4/11/14
 */
@Configuration
public class EnrIndividualAchievementManager extends BusinessObjectManager
{
    public static EnrIndividualAchievementManager instance()
    {
        return instance(EnrIndividualAchievementManager.class);
    }
}



    