package ru.tandemservice.unienr14.order.entity;

import org.tandemframework.core.common.ITitled;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unienr14.order.entity.gen.*;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

import java.util.List;

/**
 * Абстрактный параграф на абитуриентов (2014)
 */
public abstract class EnrAbstractParagraph extends EnrAbstractParagraphGen implements IAbstractParagraph, ITitled, Comparable<EnrAbstractParagraph>
{
    @Override
    @SuppressWarnings({"unchecked"})
    public List<? extends IAbstractExtract> getExtractList()
    {
        return UniDaoFacade.getCoreDao().getList(EnrAbstractExtract.class, IAbstractExtract.L_PARAGRAPH, this, IAbstractExtract.P_NUMBER);
    }

    @Override
    public int getExtractCount()
    {
        return UniDaoFacade.getCoreDao().getCount(EnrAbstractExtract.class, IAbstractExtract.L_PARAGRAPH, this);
    }

    @Override
    public void setOrder(IAbstractOrder order)
    {
        setOrder((EnrAbstractOrder) order);
    }

    @Override
    public int compareTo(EnrAbstractParagraph o)
    {
        return Integer.compare(getNumber(), o.getNumber());
    }

    @Override
    public String getTitle()
    {
        return "Параграф №" + getNumber();
    }
}