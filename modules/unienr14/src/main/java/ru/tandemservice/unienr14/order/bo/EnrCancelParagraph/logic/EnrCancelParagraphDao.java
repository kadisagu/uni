/* $Id:$ */
package ru.tandemservice.unienr14.order.bo.EnrCancelParagraph.logic;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.order.entity.EnrAbstractExtract;
import ru.tandemservice.unienr14.order.entity.EnrCancelExtract;
import ru.tandemservice.unienr14.order.entity.EnrCancelParagraph;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;
import ru.tandemservice.unimove.entity.catalog.codes.ExtractStatesCodes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author oleyba
 * @since 7/10/14
 */
public class EnrCancelParagraphDao extends UniBaseDao implements IEnrCancelParagraphDao
{
    @Override
    public void saveOrUpdateParagraph(EnrOrder order, EnrCancelParagraph paragraph, List<? extends EnrAbstractExtract> extracts)
    {
        // В один и тот же приказ нельзя одновременно добавлять параграф
        NamedSyncInTransactionCheckLocker.register(getSession(), "EnrOrder" + order.getId());

        // V A L I D A T E

        if (extracts == null || extracts.isEmpty())
            throw new ApplicationException("Параграф не может быть пустым.");

        final Session session = getSession();
        session.refresh(order);

        if (!UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(order.getState().getCode())) {
            throw new ApplicationException("Приказ находится в состоянии «" + order.getState().getTitle() +"», редактирование параграфов запрещено.");
        }

        List<EnrAbstractExtract> list = new ArrayList<>(extracts);
        Collections.sort(list, ITitled.TITLED_COMPARATOR);

        // пока мы были на форме state мог очень сильно поменяться, поэтому надо все данные проверить на актуаность
        for (EnrAbstractExtract cancelledExtract : list)
        {
            EnrEntrant entrant = cancelledExtract.getEntrant();

            // нельзя отменять выписку два раза
            EnrCancelExtract existingExtract = get(EnrCancelExtract.class, EnrCancelExtract.entity().s(), cancelledExtract);
            if (existingExtract != null && !(paragraph != null && paragraph.equals(existingExtract.getParagraph())))
            {
                String orderNumber = existingExtract.getParagraph().getOrder().getNumber();
                throw new ApplicationException("Выписка по абитуриенту «" + entrant.getPerson().getFullFio() + "» уже отменена в приказе" + (StringUtils.isEmpty(orderNumber) ? "" : " №" + orderNumber) + ".");
            }

            // данные приказа должны совпадать с данными выписки
            if (!order.getEnrollmentCampaign().equals(entrant.getEnrollmentCampaign()))
                throw new ApplicationException("Приемная кампания у абитуриента «" + entrant.getPerson().getFullFio() + "» отличается от указанной в приказе.");
            if (!order.getRequestType().equals(cancelledExtract.getRequestedCompetition().getCompetition().getRequestType()))
                throw new ApplicationException("Вид заявления у выбранного конкурса «" + cancelledExtract.getTitle() + "» отличается от указанного в приказе.");
            if (!order.getCompensationType().equals(cancelledExtract.getRequestedCompetition().getCompetition().getType().getCompensationType()))
                throw new ApplicationException("Вид возмещения затрат у выбранного конкурса «" + cancelledExtract.getTitle() + "» отличается от указанного в приказе.");

            // правильное состояние
            if (!ExtractStatesCodes.ACCEPTED.equals(cancelledExtract.getState().getCode()) && !ExtractStatesCodes.FINISHED.equals(cancelledExtract.getState().getCode()))
                throw new ApplicationException("Выписка по абитуриенту «" + entrant.getPerson().getFullFio() + "» не может быть включена в приказ, так как находится в состоянии «" + cancelledExtract.getState().getTitle() + "».");
        }

        if (paragraph.getId() != null)
        {
            // E D I T   P A R A G R A P H
            // удаляем выписки у студентов, которые не выбранны на форме. тут специально не перенумеровываем выписки,
            // так как потом будет общая перенумерация по ФИО
            for (EnrCancelExtract extract : (List<EnrCancelExtract>) paragraph.getExtractList()) {
                if (!list.remove(extract.getEntity())) {
                    session.delete(extract);
                }
            }
        }
        else
        {
            paragraph.setOrder(order);
            paragraph.setNumber(order.getParagraphCount() + 1);  // номер параграфа с единицы
            session.save(paragraph);
        }

        int counter = -1;
        for (EnrAbstractExtract cancelledExtract : list)
        {
            // создаем выписку
            EnrCancelExtract extract = new EnrCancelExtract();

            // в preStudent уже есть все данные для проведения выписки
            extract.setEntity(cancelledExtract);
            extract.setRequestedCompetition(cancelledExtract.getRequestedCompetition());
            extract.setParagraph(paragraph);
            extract.setCreateDate(order.getCreateDate());
            extract.setNumber(counter--); // нет номера. перенумеруем в конце метода update
            extract.setState(getByCode(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_IN_ORDER));

            // устанавливается состояние «В приказе» для выбранного направления приема
//            extract.getEntity().getRequestedDirection().setState(getByCode(EnrEntrantState.class, EnrEntrantStateCodes.IN_ORDER));
//            session.update(extract.getEntity().getRequestedDirection());

            // сохраняем выписку
            session.save(extract);
        }

        // перенумеруем все выписки, так чтобы сортировка была по ФИО
        session.flush(); // flush перед запросом, чтобы исзлечь ВСЕ выписки
        List<EnrCancelExtract> extractList = getList(EnrCancelExtract.class, IAbstractExtract.L_PARAGRAPH, paragraph);
        Collections.sort(extractList);

        // присваиваем уникальные номера выпискам
        counter = -1 - extractList.size(); /* заведомо неиспользованные номера */
        for (EnrCancelExtract enrollmentExtract : extractList)
        {
            enrollmentExtract.setNumber(counter--);
            session.update(enrollmentExtract);
        }
        session.flush();

        // нумеруем выписки с единицы, расставляем имена групп зачисления, вычисляем выписку старосты
        counter = 1;
        EnrCancelExtract groupManager = null;
        for (EnrCancelExtract enrollmentExtract : extractList)
        {
            enrollmentExtract.setNumber(counter++);
            session.update(enrollmentExtract);
        }

        session.update(paragraph);
    }
}