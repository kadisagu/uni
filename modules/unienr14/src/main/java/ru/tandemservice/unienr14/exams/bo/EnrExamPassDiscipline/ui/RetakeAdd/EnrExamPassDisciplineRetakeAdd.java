/* $Id:$ */
package ru.tandemservice.unienr14.exams.bo.EnrExamPassDiscipline.ui.RetakeAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrAbsenceNoteCodes;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 6/21/13
 */
@Configuration
public class EnrExamPassDisciplineRetakeAdd extends BusinessComponentManager
{
    public static final String DS_EXAM = "examDS";
    public static final String BIND_ENTRANT = "entrant";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(selectDS(DS_EXAM, absenceNoteSelectDSHandler()))
            .create();
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> absenceNoteSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrExamPassDiscipline.class) {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                // уважительная неявка
                dql.where(eq(property(EnrExamPassDiscipline.absenceNote().code().fromAlias(alias)), value(EnrAbsenceNoteCodes.VALID)));
                // не пересдача
                dql.where(eq(property(EnrExamPassDiscipline.retake().fromAlias(alias)), value(Boolean.FALSE)));
                // и пересдачи для нее еще нет
                dql.where(notExists(new DQLSelectBuilder()
                    .fromEntity(EnrExamPassDiscipline.class, "ret").column("ret.id")
                    .where(eq(property(EnrExamPassDiscipline.entrant().fromAlias(alias)), property(EnrExamPassDiscipline.entrant().fromAlias("ret"))))
                    .where(eq(property(EnrExamPassDiscipline.discipline().fromAlias(alias)), property(EnrExamPassDiscipline.discipline().fromAlias("ret"))))
                    .where(eq(property(EnrExamPassDiscipline.passForm().fromAlias(alias)), property(EnrExamPassDiscipline.passForm().fromAlias("ret"))))
                    .where(eq(property(EnrExamPassDiscipline.retake().fromAlias("ret")), value(Boolean.TRUE)))
                    .buildQuery()));
            }
        }
            .where(EnrExamPassDiscipline.entrant(), BIND_ENTRANT)
            .order(EnrExamPassDiscipline.discipline().discipline().title())
            .order(EnrExamPassDiscipline.passForm().code())
            ;
    }

}