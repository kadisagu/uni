/* $Id$ */
package ru.tandemservice.unienr14.exams.bo.EnrExamPassDiscipline.logic;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.exams.bo.EnrExamGroup.EnrExamGroupManager;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassAppeal;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;
import ru.tandemservice.unienr14.rating.entity.gen.EnrExamPassDisciplineGen;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 4/15/15
 */
public class EnrExamPassDisciplineDao extends UniBaseDao implements IEnrExamPassDisciplineDao
{
    public static final String PARAM_ENR_COMPETITION_UTIL = EnrCompetitionFilterAddon.class.getSimpleName();

    @Override
    public void updateExamPassDisciplineData(EnrEnrollmentCampaign enrollmentCampaign, List<? extends IExamPassDisciplineMarkData> wrappers)
    {
        Session session = getSession();
        NamedSyncInTransactionCheckLocker.register(session, "examPassDisciplineLock-" + enrollmentCampaign.getId());

        for (IExamPassDisciplineMarkData wrapper : wrappers)
        {
            EnrExamPassDiscipline discipline = wrapper.getDiscipline();

            discipline.setAbsenceNote(wrapper.getAbsenceNote());
            discipline.setMarkAsLong(wrapper.getMarkAsLong());
            discipline.setMarkDate(wrapper.getMarkDate());

            updateExamPassDiscipline(discipline);
        }
    }

    @Override
    public void updateExamPassDiscipline(EnrExamPassDiscipline discipline)
    {
        NamedSyncInTransactionCheckLocker.register(getSession(), "examPassDisciplineLock-" + discipline.getEntrant().getEnrollmentCampaign().getId());

        discipline.syncAbsenceNoteAndMarkState();
        getSession().update(discipline);
    }

    @Override
    public void doRefreshExamList(EnrEntrant entrant)
    {

        Map<Long, CoreCollectionUtils.Pair<Integer, OrgUnit>> territorialOuByCgIdMap = new LinkedHashMap<>();

        for (EnrRequestedCompetition direction : getList(EnrRequestedCompetition.class, EnrRequestedCompetition.request().entrant(), entrant)) {
            if (direction.getRequest().isTakeAwayDocument()) continue;
            Long cgId = direction.getId();
            if (!territorialOuByCgIdMap.containsKey(cgId)) {
                OrgUnit territorialOrgUnit = direction.getCompetition().getProgramSetOrgUnit().getOrgUnit().getInstitutionOrgUnit().getOrgUnit();
                territorialOuByCgIdMap.put(cgId, new CoreCollectionUtils.Pair<>(direction.getPriority(), territorialOrgUnit));
            }
        }


        Map<EnrCampaignDiscipline, Set<EnrExamPassForm>> examListMap = new HashMap<>();
        Map<MultiKey, Set<CoreCollectionUtils.Pair<Integer, OrgUnit>>> territorialOuSetMap = new HashMap<>();
        for (EnrChosenEntranceExamForm chosenForm: getList(EnrChosenEntranceExamForm.class, EnrChosenEntranceExamForm.chosenEntranceExam().requestedCompetition().request().entrant(), entrant)) {
            if (chosenForm.getChosenEntranceExam().getRequestedCompetition().getRequest().isTakeAwayDocument()) continue;

            SafeMap.safeGet(examListMap, chosenForm.getChosenEntranceExam().getDiscipline(), HashSet.class).add(chosenForm.getPassForm());

            // запоминаем терр. подр.
            MultiKey key = new MultiKey(chosenForm.getChosenEntranceExam().getDiscipline().getId(), chosenForm.getPassForm().getId());
            CoreCollectionUtils.Pair<Integer, OrgUnit> orgUnitPair = territorialOuByCgIdMap.get(chosenForm.getChosenEntranceExam().getRequestedCompetition().getId());
            if (null != orgUnitPair) {
                SafeMap.safeGet(territorialOuSetMap, key, HashSet.class).add(orgUnitPair);
            }
        }

        // схлапываем мап - выбираем лучшее терр. подр. для ДДС
        Map<MultiKey, OrgUnit> territorialOuMap = new HashMap<>();
        for (Map.Entry<MultiKey, Set<CoreCollectionUtils.Pair<Integer, OrgUnit>>> entry : territorialOuSetMap.entrySet()) {
            if (org.apache.commons.collections.CollectionUtils.isEmpty(entry.getValue()))
                continue;
            List<CoreCollectionUtils.Pair<Integer, OrgUnit>> pairs = new ArrayList<>(entry.getValue());
            Collections.sort(pairs, new Comparator<CoreCollectionUtils.Pair<Integer, OrgUnit>>() {
                @Override public int compare(CoreCollectionUtils.Pair<Integer, OrgUnit> o1, CoreCollectionUtils.Pair<Integer, OrgUnit> o2) {
                    return o1.getX() - o2.getX();
                }
            });
            territorialOuMap.put(entry.getKey(), pairs.iterator().next().getY());
        }


        Set<Long> updatedIds = new HashSet<>();

        for (Map.Entry<EnrCampaignDiscipline, Set<EnrExamPassForm>> entry : examListMap.entrySet()) {
            EnrCampaignDiscipline discipline = entry.getKey();
            for (EnrExamPassForm passForm : entry.getValue()) {
                if (!passForm.isInternal())
                    continue;

                MultiKey key = new MultiKey(discipline.getId(), passForm.getId());
                OrgUnit territorialOu = territorialOuMap.get(key);
                if (null == territorialOu)
                    continue;

                EnrExamPassDiscipline examPassDiscipline = getByNaturalId(new EnrExamPassDisciplineGen.NaturalId(entrant, discipline, passForm, false));
                if (null == examPassDiscipline)
                    examPassDiscipline = new EnrExamPassDiscipline(entrant, discipline, passForm);
                examPassDiscipline.setTerritorialOrgUnit(territorialOu);
                saveOrUpdate(examPassDiscipline);
                updatedIds.add(examPassDiscipline.getId());
            }
        }

        for (EnrExamPassDiscipline examPassDiscipline : getList(EnrExamPassDiscipline.class, EnrExamPassDiscipline.entrant(), entrant)) {
            if (!updatedIds.contains(examPassDiscipline.getId()) && examPassDiscipline.isCanDeleteOnRefresh()) {
                delete(examPassDiscipline);
            }
        }
    }

    @Override
    public Set<Long> getActualExamPassDisciplineIds(EnrEntrant entrant)
    {
        return new HashSet<>(new DQLSelectBuilder()
        .fromEntity(EnrExamPassDiscipline.class, "d").column("d.id")
        .fromEntity(EnrChosenEntranceExamForm.class, "c")
        .where(eq(property(EnrExamPassDiscipline.entrant().fromAlias("d")), value(entrant)))
        .where(eq(property(EnrExamPassDiscipline.retake().fromAlias("d")), value(Boolean.FALSE)))
        .where(eq(property(EnrChosenEntranceExamForm.chosenEntranceExam().requestedCompetition().request().entrant().fromAlias("c")), value(entrant)))
        .where(eq(property(EnrExamPassDiscipline.passForm().fromAlias("d")), property(EnrChosenEntranceExamForm.passForm().fromAlias("c"))))
        .where(eq(property(EnrExamPassDiscipline.discipline().fromAlias("d")), property(EnrChosenEntranceExamForm.chosenEntranceExam().discipline().fromAlias("c"))))
        .createStatement(getSession()).<Long>list());
    }

    @Override
    public void doCreateRetake(EnrExamPassDiscipline exam)
    {
        if (exam.getAbsenceNote() == null || !exam.getAbsenceNote().isValid())
            throw new ApplicationException(EnrEntrantManager.instance().getProperty("entrantDao.retakeCanNotBeAdded"));
        EnrExamPassDiscipline retake = new EnrExamPassDiscipline(exam.getEntrant(), exam.getDiscipline(), exam.getPassForm());
        retake.setTerritorialOrgUnit(exam.getTerritorialOrgUnit());
        retake.setRetake(true);
        save(retake);
    }

    @Override
    public void saveOrUpdateAppeal(EnrExamPassAppeal appeal)
    {
        if (appeal.getAppealDate() == null) {
            throw new ApplicationException("Поле «Дата подачи» обязательно для заполнения.");
        }

        if (appeal.getDecisionDate() == null) {
            throw new ApplicationException("Поле «Дата решения апелляционной комиссии» обязательно для заполнения.");
        }

        EnrEnrollmentCampaignSettings settings = appeal.getExamPassDiscipline().getEntrant().getEnrollmentCampaign().getSettings();
        Date markDate = appeal.getExamPassDiscipline().getMarkDate();
        String markDateStr = DateFormatter.DEFAULT_DATE_FORMATTER.format(markDate);

        if (markDate.getTime() > appeal.getAppealDate().getTime())
            throw new ApplicationException(EnrEntrantManager.instance().getProperty("ui.error.appeal.tooEarlyAppeal", markDateStr));

        if (appeal.getAppealDate().getTime() > appeal.getDecisionDate().getTime())
            throw new ApplicationException(EnrEntrantManager.instance().getProperty("ui.error.appeal.tooEarlyHearing"));

        if (CoreDateUtils.getDateDifferenceInDays(appeal.getAppealDate(), markDate) > settings.getAppealDeadlineDays())
            throw new ApplicationException(EnrEntrantManager.instance().getProperty("ui.error.appeal.tooLateAppeal", String.valueOf(settings.getAppealDeadlineDays()), markDateStr));

        if (CoreDateUtils.getDateDifferenceInDays(appeal.getDecisionDate(), appeal.getAppealDate()) > settings.getAppealHearingDays())
            throw new ApplicationException(EnrEntrantManager.instance().getProperty("ui.error.appeal.tooLateHearing", String.valueOf(settings.getAppealHearingDays())));

        saveOrUpdate(appeal);
    }

    @Override
    public void deleteExamPassDiscipline(Long disciplineId)
    {
        EnrExamPassDiscipline discipline = getNotNull(EnrExamPassDiscipline.class, disciplineId);
        EnrExamGroupManager.instance().groupDao().doExcludeEntrant(disciplineId);
        delete(discipline);
    }

}
