/* $Id: SecLocalRoleAccessMatrixEdit.java 6467 2015-05-07 07:00:30Z oleyba $ */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.ui.AccessMatrixEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author nvankov
 * @since 10/6/14
 */
@Configuration
public class EnrEnrollmentCommissionAccessMatrixEdit extends BusinessComponentManager
{
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .create();
    }
}



    