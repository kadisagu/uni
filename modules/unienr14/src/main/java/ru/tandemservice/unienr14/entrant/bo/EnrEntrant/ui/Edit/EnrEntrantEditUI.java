/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.Edit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrAccessCourse;
import ru.tandemservice.unienr14.catalog.entity.EnrAccessDepartment;
import ru.tandemservice.unienr14.catalog.entity.EnrSourceInfoAboutUniversity;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantInfoAboutUniversity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author oleyba
 * @since 5/18/14
 */
@Input({
        @Bind(key = IUIPresenter.PUBLISHER_ID, binding = "entrant.id")
})
public class EnrEntrantEditUI extends UIPresenter
{
    private EnrEntrant _entrant = new EnrEntrant();

    private ISelectModel _accessCourseListModel;
    private ISelectModel _accessDepartmentListModel;
    private ISelectModel _sourceInfoAboutUniversityListModel;

    private List<EnrSourceInfoAboutUniversity> _sourceInfoAboutUniversityList;

    private boolean registrationDateDisabled;
    private boolean personalNumberDisabled;

    @Override
    public void onComponentRefresh()
    {
        setEntrant(IUniBaseDao.instance.get().getNotNull(EnrEntrant.class, getEntrant().getId()));

        setAccessCourseListModel(new LazySimpleSelectModel<EnrAccessCourse>(EnrAccessCourse.class).setSearchFromStart(false));
        setAccessDepartmentListModel(new LazySimpleSelectModel<EnrAccessDepartment>(EnrAccessDepartment.class).setSearchFromStart(false));
        setSourceInfoAboutUniversityListModel(new LazySimpleSelectModel<EnrSourceInfoAboutUniversity>(EnrSourceInfoAboutUniversity.class).setSearchFromStart(false));

        setSourceInfoAboutUniversityList(new ArrayList<EnrSourceInfoAboutUniversity>());
        for (EnrEntrantInfoAboutUniversity item : IUniBaseDao.instance.get().getList(EnrEntrantInfoAboutUniversity.class, EnrEntrantInfoAboutUniversity.L_ENTRANT, getEntrant()))
            getSourceInfoAboutUniversityList().add(item.getSourceInfo());

        setRegistrationDateDisabled(!CoreServices.securityService().check(CoreServices.securityService().getCommonSecurityObject(), UserContext.getInstance().getPrincipalContext(), "enr14EntrantPubContactDataTabMainDataBlockEditEntrantRegistrationDate"));
        setPersonalNumberDisabled(!CoreServices.securityService().check(CoreServices.securityService().getCommonSecurityObject(), UserContext.getInstance().getPrincipalContext(), "enr14EntrantPubContactDataTabMainDataBlockEditEntrantPersonalNumber"));
        
    }

    public void onClickApply() {
        if (getEntrant().isPassArmy() && getEntrant().getBeginArmy() != null && getEntrant().getEndArmy() != null && !getEntrant().getBeginArmy().before(getEntrant().getEndArmy())) {
            ContextLocal.getErrorCollector().add("Дата начала службы должна быть раньше даты ее окончания", "beginArmy");
            return;
        }

        if (!getEntrant().isPassArmy()) {
            getEntrant().setBeginArmy(null);
            getEntrant().setEndArmy(null);
            getEntrant().setEndArmyYear(null);
        }

        EnrEntrantManager.instance().dao().updateEntrantData(getEntrant(), getSourceInfoAboutUniversityList());

        deactivate();
    }
    
    
    // getters and setters

    public ISelectModel getAccessCourseListModel()
    {
        return _accessCourseListModel;
    }

    public void setAccessCourseListModel(ISelectModel accessCourseListModel)
    {
        _accessCourseListModel = accessCourseListModel;
    }

    public ISelectModel getAccessDepartmentListModel()
    {
        return _accessDepartmentListModel;
    }

    public void setAccessDepartmentListModel(ISelectModel accessDepartmentListModel)
    {
        _accessDepartmentListModel = accessDepartmentListModel;
    }

    public EnrEntrant getEntrant()
    {
        return _entrant;
    }

    public void setEntrant(EnrEntrant entrant)
    {
        _entrant = entrant;
    }

    public List<EnrSourceInfoAboutUniversity> getSourceInfoAboutUniversityList()
    {
        return _sourceInfoAboutUniversityList;
    }

    public void setSourceInfoAboutUniversityList(List<EnrSourceInfoAboutUniversity> sourceInfoAboutUniversityList)
    {
        _sourceInfoAboutUniversityList = sourceInfoAboutUniversityList;
    }

    public ISelectModel getSourceInfoAboutUniversityListModel()
    {
        return _sourceInfoAboutUniversityListModel;
    }

    public void setSourceInfoAboutUniversityListModel(ISelectModel sourceInfoAboutUniversityListModel)
    {
        _sourceInfoAboutUniversityListModel = sourceInfoAboutUniversityListModel;
    }

    public boolean isPersonalNumberDisabled()
    {
        return personalNumberDisabled;
    }

    public void setPersonalNumberDisabled(boolean personalNumberDisabled)
    {
        this.personalNumberDisabled = personalNumberDisabled;
    }

    public boolean isRegistrationDateDisabled()
    {
        return registrationDateDisabled;
    }

    public void setRegistrationDateDisabled(boolean registrationDateDisabled)
    {
        this.registrationDateDisabled = registrationDateDisabled;
    }
}
