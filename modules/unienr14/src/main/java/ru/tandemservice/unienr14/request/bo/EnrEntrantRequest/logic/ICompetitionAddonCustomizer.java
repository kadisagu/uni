/* $Id$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.logic;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;

/**
 * @author Nikolay Fedorovskih
 * @since 30.06.2015
 */
public interface ICompetitionAddonCustomizer
{
    void customizeCompetitionDQL(DQLSelectBuilder dql, String competitionAlias);
}