package ru.tandemservice.unienr14.ws.campaignInfo;

import com.google.common.collect.Maps;
import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.DeclinableManager;
import org.tandemframework.shared.commonbase.base.util.CommonCatalogUtil;
import org.tandemframework.shared.commonbase.catalog.entity.InflectorVariant;
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;
import ru.tandemservice.uniedu.program.entity.EduProgramSecondaryProf;
import ru.tandemservice.unienr14.catalog.entity.*;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantStateCodes;
import ru.tandemservice.unienr14.competition.entity.*;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.EnrExamSetManager;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.logic.IEnrExamSetDao;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.bo.EnrTargetAdmission.EnrTargetAdmissionManager;
import ru.tandemservice.unienr14.settings.entity.*;

import java.util.*;

/**
 * @author Vasily Zhukov
 * @since 15.02.2011
 */
public class EnrCampaignInfoServiceDao extends UniBaseDao implements IEnrCampaignInfoServiceDao
{
    @Override
    public EnrCampaignInfoEnvironmentNode getEnrollmentEnvironmentData(String enrollmentCampaignTitle)
    {
        EnrEnrollmentCampaign enrollmentCampaign = get(EnrEnrollmentCampaign.class, EnrEnrollmentCampaign.P_TITLE, enrollmentCampaignTitle);

        if (enrollmentCampaign == null)
            throw new RuntimeException("EnrEnrollmentCampaign with title '" + enrollmentCampaignTitle + "' not found!");

        EnrCampaignInfoEnvironmentNode envNode = new EnrCampaignInfoEnvironmentNode();

        envNode.enrollmentCampaignTitle = enrollmentCampaign.getTitle();
        envNode.educationYearTitle = enrollmentCampaign.getEducationYear().getTitle();

        // сохраняем нужные справочники

        for (EduLevel row : getList(EduLevel.class, EduLevel.P_CODE))
            envNode.eduLevel.row.add(new EnrCampaignInfoEnvironmentNode.Row(row.getTitle(), row.getCode()));

        for (EduProgramForm row : getList(EduProgramForm.class, EduProgramForm.P_CODE))
            envNode.eduProgramForm.row.add(new EnrCampaignInfoEnvironmentNode.Row(row.getShortTitle(), row.getTitle(), row.getCode()));

        for (EduProgramKind row : getList(EduProgramKind.class, EduProgramKind.P_PRIORITY))
            envNode.eduProgramKind.row.add(new EnrCampaignInfoEnvironmentNode.Row(row.getShortTitle(), row.getTitle(), row.getCode()));

        List<EnrProgramSetBase> programSetList = getList(EnrProgramSetBase.class, EnrProgramSetBase.enrollmentCampaign(), enrollmentCampaign, EnrProgramSetBase.title().s());
        Set<EduProgramSubject> subjects = new HashSet<>(CollectionUtils.collect(programSetList, EnrProgramSetBase::getProgramSubject));
        List<EduProgramSubject> sortedSubjects = new ArrayList<>();
        sortedSubjects.addAll(subjects);
        Collections.sort(sortedSubjects, ITitled.TITLED_COMPARATOR);
        for (EduProgramSubject row : sortedSubjects)
            envNode.eduProgramSubject.row.add(new EnrCampaignInfoEnvironmentNode.Row(row.getSubjectCode(), row.getTitle(), row.getCode()));

        for (EnrRequestType row : getList(EnrRequestType.class, EnrRequestType.P_CODE))
            envNode.requestType.row.add(new EnrCampaignInfoEnvironmentNode.RequestTypeNode.RequestTypeRow(row.getShortTitle(), row.getTitle(), row.getCode(), row.getPriority()));

        for (EnrCompetitionType row : getList(EnrCompetitionType.class, EnrCompetitionType.P_CODE))
            envNode.competitionType.row.add(new EnrCampaignInfoEnvironmentNode.CompetitionTypeNode.CompetitionTypeRow(row.getShortTitle(), row.getTitle(), row.getCode(), row.getPriority(), row.getPublicTitle(), row.getCompensationType().getCode()));

        for (CompensationType row : getList(CompensationType.class, CompensationType.P_CODE))
            envNode.compensationType.row.add(new EnrCampaignInfoEnvironmentNode.Row(row.getShortTitle(), row.getTitle(), row.getCode()));

        for (EnrEduLevelRequirement row : getList(EnrEduLevelRequirement.class, EnrEduLevelRequirement.P_CODE))
        {
            InflectorVariant variant = IUniBaseDao.instance.get().getCatalogItem(InflectorVariant.class, InflectorVariantCodes.RU_GENITIVE);
            String genitiveTitle = DeclinableManager.instance().dao().getPropertyValue(row, EnrEduLevelRequirement.P_TITLE, variant);
            envNode.eduLevelRequirement.row.add(new EnrCampaignInfoEnvironmentNode.EduLevelRequirementNode.EduLevelRequirementRow(row.getShortTitle(), row.getTitle(), row.getCode(), genitiveTitle, row.getPriority()));
        }

        for (EnrEduLevelReqEduLevel row : getList(EnrEduLevelReqEduLevel.class))
            envNode.eduLevelReqEduLevel.row.add(new EnrCampaignInfoEnvironmentNode.EduLevelReqEduLevelNode.EduLevelReqEduLevelNodeRow(row.getEnrEduLevelRequirement().getCode(), row.getEduLevel().getCode()));

        for (EnrExamPassForm row : getList(EnrExamPassForm.class, EnrExamPassForm.P_CODE))
            envNode.examPassForm.row.add(new EnrCampaignInfoEnvironmentNode.Row(row.getTitle(), row.getCode()));

        for (EnrExamType row : getList(EnrExamType.class, EnrExamType.P_CODE))
            envNode.examType.row.add(new EnrCampaignInfoEnvironmentNode.Row(row.getTitle(), row.getCode()));

        for (EnrStateExamSubject row : getList(EnrStateExamSubject.class, EnrStateExamSubject.P_CODE))
            envNode.stateExamSubject.row.add(new EnrCampaignInfoEnvironmentNode.Row(row.getTitle(), row.getCode()));

        Set<EnrTargetAdmissionKind> targetAdmissionKindList = EnrTargetAdmissionManager.instance().dao().getUsedTaKinds(enrollmentCampaign);
        for (EnrTargetAdmissionKind row : targetAdmissionKindList)
            envNode.targetAdmissionKind.row.add(new EnrCampaignInfoEnvironmentNode.TargetAdmissionKindNode.TargetAdmissionKindRow(row.getTitle(), row.getFullTitle(), row.getCode(), row.getPriority()));

        for (EnrCampaignDiscipline discipline: getList(EnrCampaignDiscipline.class, EnrCampaignDiscipline.enrollmentCampaign(), enrollmentCampaign)) {
            envNode.discipline.row.add(new EnrCampaignInfoEnvironmentNode.Row(discipline.getTitle(), discipline.getDiscipline().getCode()));
        }

        // сохраняем все группы дисциплин

        List<EnrCampaignDisciplineGroupElement> disciplineGroupElementList = getList(
            EnrCampaignDisciplineGroupElement.class,
            EnrCampaignDisciplineGroupElement.group().enrollmentCampaign(), enrollmentCampaign,
            EnrCampaignDisciplineGroupElement.id().s());

        Map<EnrCampaignDisciplineGroup, List<String>> group2disciplines = new HashMap<>();
        for (EnrCampaignDisciplineGroupElement row : disciplineGroupElementList)
        {
            List<String> list = group2disciplines.get(row.getGroup());
            if (list == null)
                group2disciplines.put(row.getGroup(), list = new ArrayList<>());
            list.add(row.getDiscipline().getDiscipline().getCode());
        }

        for (Map.Entry<EnrCampaignDisciplineGroup, List<String>> entry : group2disciplines.entrySet())
        {
            EnrCampaignDisciplineGroup row = entry.getKey();
            envNode.disciplineGroup.row.add(new EnrCampaignInfoEnvironmentNode.DisciplineGroupNode.GroupRow(row.getShortTitle(), row.getTitle(), row.getId().toString(), entry.getValue()));
        }

        Collections.sort(envNode.disciplineGroup.row);

        // собираем информацию по всем вступительным испытаниям из указанной приемной кампании

        final List<EnrCompetition> competitionList = new DQLSelectBuilder().fromEntity(EnrCompetition.class, "d")
            .column("d")
            .where(DQLExpressions.eq(DQLExpressions.property(EnrCompetition.programSetOrgUnit().programSet().enrollmentCampaign().fromAlias("d")), DQLExpressions.value(enrollmentCampaign)))
            .order(DQLExpressions.property(EnrCompetition.id().fromAlias("d")))
            .createStatement(new DQLExecutionContext(getSession()))
            .list();

        DQLSelectBuilder requestsCountBuilder = new DQLSelectBuilder().fromEntity(EnrRequestedCompetition.class, "c");
        requestsCountBuilder.column(DQLExpressions.property("c", EnrRequestedCompetition.competition().id()));
        requestsCountBuilder.column(DQLFunctions.count(DQLExpressions.property("c", EnrRequestedCompetition.competition().id())));
        requestsCountBuilder.where(DQLExpressions.eq(DQLExpressions.property(EnrRequestedCompetition.competition().programSetOrgUnit().programSet().enrollmentCampaign().fromAlias("c")), DQLExpressions.value(enrollmentCampaign)));
        requestsCountBuilder.where(DQLExpressions.not(DQLExpressions.eq(DQLExpressions.property(EnrRequestedCompetition.state().code().fromAlias("c")), DQLExpressions.value(EnrEntrantStateCodes.ENTRANT_ARCHIVED))));
        requestsCountBuilder.group(DQLExpressions.property("c", EnrRequestedCompetition.competition().id()));

        DQLSelectBuilder requestsWithdrawalCountBuilder = new DQLSelectBuilder().fromEntity(EnrRequestedCompetition.class, "c");
        requestsWithdrawalCountBuilder.column(DQLExpressions.property("c", EnrRequestedCompetition.competition().id()));
        requestsWithdrawalCountBuilder.column(DQLFunctions.count(DQLExpressions.property("c", EnrRequestedCompetition.competition().id())));
        requestsWithdrawalCountBuilder.where(DQLExpressions.eq(DQLExpressions.property(EnrRequestedCompetition.competition().programSetOrgUnit().programSet().enrollmentCampaign().fromAlias("c")), DQLExpressions.value(enrollmentCampaign)));
        requestsWithdrawalCountBuilder.where(DQLExpressions.eq(DQLExpressions.property(EnrRequestedCompetition.state().code().fromAlias("c")), DQLExpressions.value(EnrEntrantStateCodes.TAKE_DOCUMENTS_AWAY)));
        requestsWithdrawalCountBuilder.group(DQLExpressions.property("c", EnrRequestedCompetition.competition().id()));

        List<Object[]> requestsCountResult = requestsCountBuilder.createStatement(getSession()).list();
        Map<Long, Integer> requestsCountMap = Maps.newHashMap();
        for(Object[] competition : requestsCountResult)
        {
            requestsCountMap.put((Long)competition[0], ((Long)competition[1]).intValue());
        }

        List<Object[]> requestsWithdrawalCountResult = requestsWithdrawalCountBuilder.createStatement(getSession()).list();
        Map<Long, Integer> requestsWithdrawalCountMap = Maps.newHashMap();
        for(Object[] competition : requestsWithdrawalCountResult)
        {
            requestsWithdrawalCountMap.put((Long)competition[0], ((Long)competition[1]).intValue());
        }


        final Collection<Long> examSetVariantIds = CollectionUtils.collect(competitionList, enrCompetition -> enrCompetition.getExamSetVariant().getId());

        final Map<Long, IEnrExamSetDao.IExamSetSettings> examSetContentMap = EnrExamSetManager.instance().dao().getExamSetContent(examSetVariantIds);

        // сохраняем все направления приема

        Set<Long> usedOrgUnitIds = new HashSet<>();
        Set<OrgUnitType> usedOrgUnitTypes = new HashSet<>();
        Set<EnrProgramSetBase> usedProgramSets = new HashSet<>();
        Set<EnrExamSetVariant> examSetVariants = new HashSet<>();

        for (EnrCompetition competition : competitionList)
        {
            OrgUnit enrOrgUnit = competition.getProgramSetOrgUnit().getOrgUnit().getInstitutionOrgUnit().getOrgUnit();
            OrgUnit formativeOrgUnit = competition.getProgramSetOrgUnit().getFormativeOrgUnit();

            for (OrgUnit orgUnit : new OrgUnit[] {enrOrgUnit, formativeOrgUnit})
                if (null != orgUnit && usedOrgUnitIds.add(orgUnit.getId())) {
                    usedOrgUnitTypes.add(orgUnit.getOrgUnitType());
                    envNode.orgUnit.row.add(getOrgUnitRow(orgUnit));
                }

            usedProgramSets.add(competition.getProgramSetOrgUnit().getProgramSet());

            EnrCampaignInfoEnvironmentNode.CompetitionRow row = new EnrCampaignInfoEnvironmentNode.CompetitionRow();

            row.id = competition.getId().toString();
            row.title = competition.getTitle();
            row.plan = competition.getPlan();
            row.requests = requestsCountMap.get(competition.getId());
            row.requestsWithdrawal = requestsWithdrawalCountMap.get(competition.getId());
            row.programSet = String.valueOf(competition.getProgramSetOrgUnit().getProgramSet().getId());
            row.enrOrgUnit = String.valueOf(enrOrgUnit.getId());
            row.formativeOrgUnit = String.valueOf(formativeOrgUnit.getId());
            row.competitionType = competition.getType().getCode();
            row.eduLevelRequirement = competition.getEduLevelRequirement().getCode();
            row.examSetVariant = String.valueOf(competition.getExamSetVariant().getId());
            examSetVariants.add(competition.getExamSetVariant());

            envNode.competition.row.add(row);
        }

        for(EnrExamSetVariant examSetVariant : examSetVariants)
        {
            EnrCampaignInfoEnvironmentNode.ExamSetVariantNode.ExamSetVariantRow row = new EnrCampaignInfoEnvironmentNode.ExamSetVariantNode.ExamSetVariantRow();
            row.id = examSetVariant.getId().toString();
            IEnrExamSetDao.IExamSetSettings examSetSettings = examSetContentMap.get(examSetVariant.getId());
            if (examSetSettings != null) {
                for (IEnrExamSetDao.IExamSetElementSettings discipline : examSetSettings.getElementList()) {
                    row.entranceDiscipline.row.add(fillDisciplineRow(discipline));
                }
            }
            envNode.examSetVariant.row.add(row);
        }

        for (EnrProgramSetBase programSetBase : usedProgramSets) {
            EnrCampaignInfoEnvironmentNode.ProgramSetRow row = new EnrCampaignInfoEnvironmentNode.ProgramSetRow();

            row.id = programSetBase.getId().toString();
            row.title = programSetBase.getTitle();
            row.printTitle = programSetBase.getPrintTitle();
            row.inCrimea = programSetBase.isAcceptPeopleResidingInCrimea();
            row.printTitle = programSetBase.getPrintTitle();
            row.requestType = programSetBase.getRequestType().getCode();
            row.eduProgramSubject = programSetBase.getProgramSubject().getCode();
            row.eduProgramForm = programSetBase.getProgramForm().getCode();
            row.eduProgramSubjectType = programSetBase.getProgramSubject().getSubjectIndex().getEduProgramSubjectType();
            row.eduProgramKind = programSetBase.getProgramSubject().getSubjectIndex().getProgramKind().getCode();
            row.eduLevel = programSetBase.getProgramSubject().getSubjectIndex().getProgramKind().getEduLevel().getCode();

            if(programSetBase instanceof EnrProgramSetSecondary)
            {
                EnrProgramSetSecondary programSetSecondary = (EnrProgramSetSecondary) programSetBase;
                EduProgramSecondaryProf eduProgram = programSetSecondary.getProgram();
                EnrCampaignInfoEnvironmentNode.ProgramSetRow.ProgramSetItemRow programRow = new EnrCampaignInfoEnvironmentNode.ProgramSetRow.ProgramSetItemRow(eduProgram.getShortTitle(), eduProgram.getTitle(), programSetSecondary.getId().toString());

                OrgUnit institutionOrgUnit = eduProgram.getInstitutionOrgUnit().getOrgUnit();
                OrgUnit ownerOrgUnit = eduProgram.getOwnerOrgUnit().getOrgUnit();

                for (OrgUnit orgUnit : new OrgUnit[] {institutionOrgUnit, ownerOrgUnit})
                {
                    if (null != orgUnit && usedOrgUnitIds.add(orgUnit.getId()))
                    {
                        usedOrgUnitTypes.add(orgUnit.getOrgUnitType());
                        envNode.orgUnit.row.add(getOrgUnitRow(orgUnit));
                    }
                }

                programRow.rootSpec = false;
                programRow.durationTitle = eduProgram.getDuration().getTitle();
                programRow.durationInMonths = 12 * eduProgram.getDuration().getNumberOfYears() + eduProgram.getDuration().getNumberOfMonths();
                programRow.institutionOrgUnit = String.valueOf(institutionOrgUnit.getId());
                programRow.ownerOrgUnit = String.valueOf(ownerOrgUnit.getId());
                programRow.programTrait = eduProgram.getEduProgramTrait() == null ? null : eduProgram.getEduProgramTrait().getTitle();
                programRow.secondaryInDepthStudyString = eduProgram.isInDepthStudy() ? "углуб. изуч." : "";

                row.eduProgram.add(programRow);
            }
            else
            {
                for (EnrProgramSetItem item : getList(EnrProgramSetItem.class, EnrProgramSetItem.programSet(), programSetBase))
                {
                    EduProgramHigherProf eduProgram = item.getProgram();
                    EnrCampaignInfoEnvironmentNode.ProgramSetRow.ProgramSetItemRow programRow = new EnrCampaignInfoEnvironmentNode.ProgramSetRow.ProgramSetItemRow(eduProgram.getShortTitle(), eduProgram.getTitle(), item.getId().toString());
                    OrgUnit institutionOrgUnit = eduProgram.getInstitutionOrgUnit().getOrgUnit();
                    OrgUnit ownerOrgUnit = eduProgram.getOwnerOrgUnit().getOrgUnit();

                    for (OrgUnit orgUnit : new OrgUnit[] {institutionOrgUnit, ownerOrgUnit})
                    {
                        if (null != orgUnit && usedOrgUnitIds.add(orgUnit.getId()))
                        {
                            usedOrgUnitTypes.add(orgUnit.getOrgUnitType());
                            envNode.orgUnit.row.add(getOrgUnitRow(orgUnit));
                        }
                    }
                    programRow.specTitle = eduProgram.getProgramSpecialization().getTitle();
                    programRow.rootSpec = eduProgram.getProgramSpecialization().isRootSpecialization();
                    programRow.durationTitle = eduProgram.getDuration().getTitle();
                    programRow.durationInMonths = 12 * eduProgram.getDuration().getNumberOfYears() + eduProgram.getDuration().getNumberOfMonths();
                    programRow.institutionOrgUnit = String.valueOf(institutionOrgUnit.getId());
                    programRow.ownerOrgUnit = String.valueOf(ownerOrgUnit.getId());
                    programRow.programTrait = eduProgram.getEduProgramTrait() == null ? null : eduProgram.getEduProgramTrait().getTitle();
                    programRow.bachelorQualificationShortTitle = eduProgram.getProgramOrientation() != null ? eduProgram.getProgramOrientation().getShortTitle() : null;
                    row.eduProgram.add(programRow);
                }
            }

            envNode.programSet.row.add(row);
        }


        List<OrgUnitType> orgUnitTypeList = new ArrayList<>(usedOrgUnitTypes);
        Collections.sort(orgUnitTypeList, CommonCatalogUtil.CATALOG_CODE_COMPARATOR);
        for (OrgUnitType type : orgUnitTypeList) {
            envNode.orgUnitType.row.add(new EnrCampaignInfoEnvironmentNode.Row(type.getTitle(), type.getCode()));
        }

        Collections.sort(envNode.orgUnit.row);

        return envNode;
    }

    private EnrCampaignInfoEnvironmentNode.OrgUnitNode.OrgUnitRow getOrgUnitRow(OrgUnit orgUnit)
    {
        String settlementTitle = null;
        if (orgUnit.getAddress() != null && orgUnit.getAddress().getSettlement() != null)
            settlementTitle = orgUnit.getAddress().getSettlement().getTitleWithType();
        return new EnrCampaignInfoEnvironmentNode.OrgUnitNode.OrgUnitRow(orgUnit.getShortTitle(), orgUnit.getTitle(), orgUnit.getId().toString(), orgUnit.getOrgUnitType().getCode(), orgUnit.getNominativeCaseTitle(), settlementTitle, orgUnit.getTerritorialTitle(), orgUnit.getTerritorialShortTitle(), orgUnit.getTerritorialFullTitle());
    }

    private EnrCampaignInfoEnvironmentNode.ExamSetVariantNode.ExamSetVariantRow.EntranceDisciplineRow fillDisciplineRow(IEnrExamSetDao.IExamSetElementSettings discipline)
    {
        EnrCampaignInfoEnvironmentNode.ExamSetVariantNode.ExamSetVariantRow.EntranceDisciplineRow disciplineRow = new EnrCampaignInfoEnvironmentNode.ExamSetVariantNode.ExamSetVariantRow.EntranceDisciplineRow();
        IEnrExamSetElementValue value = discipline.getElement().getValue();
        disciplineRow.discipline = value instanceof EnrCampaignDiscipline ? ((EnrCampaignDiscipline) value).getDiscipline().getCode() : value.getId().toString();
        disciplineRow.examType = discipline.getElement().getType().getCode();
        disciplineRow.passForms = new ArrayList<>();
        for (Map.Entry<EnrExamPassForm, Boolean> entry : discipline.getPassFormMap().entrySet()) {
            if (Boolean.TRUE.equals(entry.getValue())) {
                disciplineRow.passForms.add(entry.getKey().getCode());
            }
        }

        Set<String> stateExamSubjects = new HashSet<>();
        List disciplines = (value instanceof EnrCampaignDiscipline) ? Arrays.asList((EnrCampaignDiscipline) value) : getPropertiesList(EnrCampaignDisciplineGroupElement.class, EnrCampaignDisciplineGroupElement.group(), (EnrCampaignDisciplineGroup) value, false, EnrCampaignDisciplineGroupElement.discipline());
        for (Object disc : disciplines) {
            EnrStateExamSubject stateExamSubject = ((EnrCampaignDiscipline) disc).getStateExamSubject();
            if (null != stateExamSubject) stateExamSubjects.add(stateExamSubject.getCode());
        }
        disciplineRow.stateExamSubjects = new ArrayList<>();
        disciplineRow.stateExamSubjects.addAll(stateExamSubjects);
        Collections.sort(disciplineRow.stateExamSubjects);

        disciplineRow.passMarkMultipliedBy1000 = discipline.getPassMarkAsLong();
        disciplineRow.priority = discipline.getPriority();
        return disciplineRow;
    }
}
