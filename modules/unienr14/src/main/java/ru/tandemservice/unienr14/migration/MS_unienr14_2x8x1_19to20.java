package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x8x1_19to20 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
                 new ScriptDependency("org.tandemframework.shared", "1.8.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrOrganizationOriginalIn

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("enr14_org_orig_in_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_enrorganizationoriginalin"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("entrant_id", DBType.LONG).setNullable(false), 
				new DBColumn("edudocument_id", DBType.LONG).setNullable(false), 
				new DBColumn("title_p", DBType.createVarchar(255)).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("enrOrganizationOriginalIn");
		}
    }
}