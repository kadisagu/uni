/**
 *$Id: DAO.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.component.catalog.enrOlympiad.EnrOlympiadPub;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubDAO;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unienr14.catalog.entity.EnrOlympiad;

/**
 * @author Alexander Shaburov
 * @since 14.05.13
 */
public class DAO extends DefaultCatalogPubDAO<EnrOlympiad, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        model.setEduYearModel(EducationCatalogsManager.getEducationYearModel());

        if (model.getSettings().get("eduYear") == null) {
            try {
                model.getSettings().set("eduYear", get(EducationYear.class, EducationYear.intValue(), EducationYear.getCurrentRequired().getIntValue() + 1));
            } catch (ApplicationException e) {
                //
            }
        }
    }

    @Override
    protected void applyFilters(Model model, MQBuilder builder)
    {
        super.applyFilters(model, builder);

        final EducationYear eduYear = (EducationYear) model.getSettings().get("eduYear");

        if (eduYear!= null)
            builder.add(MQExpression.eq("ci", EnrOlympiad.eduYear(), eduYear));
    }
}
