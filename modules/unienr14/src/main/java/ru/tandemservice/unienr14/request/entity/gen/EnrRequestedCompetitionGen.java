package ru.tandemservice.unienr14.request.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantState;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выбранный конкурс
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrRequestedCompetitionGen extends EntityBase
 implements INaturalIdentifiable<EnrRequestedCompetitionGen>, ILazyFieldOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition";
    public static final String ENTITY_NAME = "enrRequestedCompetition";
    public static final int VERSION_HASH = 318624136;
    private static IEntityMeta ENTITY_META;

    public static final String L_REQUEST = "request";
    public static final String L_COMPETITION = "competition";
    public static final String P_UNIQUE_KEY = "uniqueKey";
    public static final String P_PRIORITY = "priority";
    public static final String P_PARALLEL = "parallel";
    public static final String P_PROFILE_EDUCATION = "profileEducation";
    public static final String P_ACCEPTED = "accepted";
    public static final String P_ACCEPTED_DATE = "acceptedDate";
    public static final String P_ENROLLMENT_AVAILABLE = "enrollmentAvailable";
    public static final String P_ACCEPTED_CONTRACT = "acceptedContract";
    public static final String P_CONTRACT_ENROLLMENT_AVAILABLE = "contractEnrollmentAvailable";
    public static final String P_REFUSED_TO_BE_ENROLLED = "refusedToBeEnrolled";
    public static final String P_REG_DATE = "regDate";
    public static final String P_ACCELERATED_LEARNING = "acceleratedLearning";
    public static final String L_STATE = "state";
    public static final String P_ORIGINAL_DOCUMENT_HANDED_IN = "originalDocumentHandedIn";
    public static final String P_REFUSED_DATE = "refusedDate";
    public static final String P_COMMENT = "comment";
    public static final String P_CONTRACT_COMMON_DATE = "contractCommonDate";
    public static final String P_CONTRACT_COMMON_NUMBER = "contractCommonNumber";
    public static final String L_EXTERNAL_ORG_UNIT = "externalOrgUnit";
    public static final String P_PARAMETERS_TITLE = "parametersTitle";
    public static final String P_TITLE = "title";

    private EnrEntrantRequest _request;     // Заявление
    private EnrCompetition _competition;     // Конкурс
    private String _uniqueKey;     // Уникальный ключ выбранного конкурса абитуриента
    private int _priority;     // Выбранный приоритет
    private boolean _parallel;     // Поступление на параллельное обучение
    private boolean _profileEducation;     // Получено профильное образование
    private boolean _accepted;     // Согласие на зачисление
    private Date _acceptedDate = null;     // Дата подачи согласия на зачисление
    private boolean _enrollmentAvailable;     // Согласие на зачисление (итоговое)
    private boolean _acceptedContract;     // Согласие на зачисление по договору (deprecated)
    private boolean _contractEnrollmentAvailable;     // Согласие на зачисление по договору (итоговое) (deprecated)
    private Boolean _refusedToBeEnrolled;     // Отказ от зачисления
    private Date _regDate;     // Дата добавления
    private boolean _acceleratedLearning;     // Индивидуальное ускоренное обучение
    private EnrEntrantState _state;     // Состояние
    private boolean _originalDocumentHandedIn = false;     // Оригинал
    private Date _refusedDate = null;     // Дата отказа от зачисления
    private String _comment;     // Комментарий
    private Date _contractCommonDate;     // Дата заключения договора
    private String _contractCommonNumber;     // Номер договора
    private ExternalOrgUnit _externalOrgUnit;     // Организация-заказчик

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Заявление. Свойство не может быть null.
     */
    @NotNull
    public EnrEntrantRequest getRequest()
    {
        return _request;
    }

    /**
     * @param request Заявление. Свойство не может быть null.
     */
    public void setRequest(EnrEntrantRequest request)
    {
        dirty(_request, request);
        _request = request;
    }

    /**
     * @return Конкурс. Свойство не может быть null.
     */
    @NotNull
    public EnrCompetition getCompetition()
    {
        return _competition;
    }

    /**
     * @param competition Конкурс. Свойство не может быть null.
     */
    public void setCompetition(EnrCompetition competition)
    {
        dirty(_competition, competition);
        _competition = competition;
    }

    /**
     * Обеспечивает уникальность в рамках абитуриента.
     * Если выбранный конкурс относится к отозванному заявлению, поле обнуляется.
     * Если выбранный конкурс относится к действующему заявлению, поле заполняется комбинацией идентификаторов абитуриента и конкурса (тем самым обеспечивается уникальность конкурса в рамках абитуриента).
     *
     * @return Уникальный ключ выбранного конкурса абитуриента. Свойство должно быть уникальным.
     */
    @Length(max=255)
    public String getUniqueKey()
    {
        return _uniqueKey;
    }

    /**
     * @param uniqueKey Уникальный ключ выбранного конкурса абитуриента. Свойство должно быть уникальным.
     */
    public void setUniqueKey(String uniqueKey)
    {
        dirty(_uniqueKey, uniqueKey);
        _uniqueKey = uniqueKey;
    }

    /**
     * Выбранный приоритет конкурса, глобальный.
     *
     * @return Выбранный приоритет. Свойство не может быть null.
     */
    @NotNull
    public int getPriority()
    {
        return _priority;
    }

    /**
     * @param priority Выбранный приоритет. Свойство не может быть null.
     */
    public void setPriority(int priority)
    {
        dirty(_priority, priority);
        _priority = priority;
    }

    /**
     * Абитуриент хочет быть зачисленным на данный конкурс параллельно основному зачислению. Всегда false для конкурсов, где вид приема не предусматривает договора на обучение (все, кроме "договор" и "без ВИ (договор)". См. констрейнт.
     *
     * @return Поступление на параллельное обучение. Свойство не может быть null.
     */
    @NotNull
    public boolean isParallel()
    {
        return _parallel;
    }

    /**
     * @param parallel Поступление на параллельное обучение. Свойство не может быть null.
     */
    public void setParallel(boolean parallel)
    {
        dirty(_parallel, parallel);
        _parallel = parallel;
    }

    /**
     * @return Получено профильное образование. Свойство не может быть null.
     */
    @NotNull
    public boolean isProfileEducation()
    {
        return _profileEducation;
    }

    /**
     * @param profileEducation Получено профильное образование. Свойство не может быть null.
     */
    public void setProfileEducation(boolean profileEducation)
    {
        dirty(_profileEducation, profileEducation);
        _profileEducation = profileEducation;
    }

    /**
     * Согласен быть зачисленным на данный конкурс.
     *
     * @return Согласие на зачисление. Свойство не может быть null.
     */
    @NotNull
    public boolean isAccepted()
    {
        return _accepted;
    }

    /**
     * @param accepted Согласие на зачисление. Свойство не может быть null.
     */
    public void setAccepted(boolean accepted)
    {
        dirty(_accepted, accepted);
        _accepted = accepted;
    }

    /**
     * @return Дата подачи согласия на зачисление.
     */
    public Date getAcceptedDate()
    {
        return _acceptedDate;
    }

    /**
     * @param acceptedDate Дата подачи согласия на зачисление.
     */
    public void setAcceptedDate(Date acceptedDate)
    {
        dirty(_acceptedDate, acceptedDate);
        _acceptedDate = acceptedDate;
    }

    /**
     * Заполняется системой на основе поля accepted и расширяемого механизма условий.
     *
     * @return Согласие на зачисление (итоговое). Свойство не может быть null.
     */
    @NotNull
    public boolean isEnrollmentAvailable()
    {
        return _enrollmentAvailable;
    }

    /**
     * @param enrollmentAvailable Согласие на зачисление (итоговое). Свойство не может быть null.
     */
    public void setEnrollmentAvailable(boolean enrollmentAvailable)
    {
        dirty(_enrollmentAvailable, enrollmentAvailable);
        _enrollmentAvailable = enrollmentAvailable;
    }

    /**
     * DEPRECATED
     *
     * @return Согласие на зачисление по договору (deprecated). Свойство не может быть null.
     *
     * Это формула "accepted".
     */
    // @NotNull
    public boolean isAcceptedContract()
    {
        return _acceptedContract;
    }

    /**
     * @param acceptedContract Согласие на зачисление по договору (deprecated). Свойство не может быть null.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setAcceptedContract(boolean acceptedContract)
    {
        dirty(_acceptedContract, acceptedContract);
        _acceptedContract = acceptedContract;
    }

    /**
     * DEPRECATED
     *
     * @return Согласие на зачисление по договору (итоговое) (deprecated). Свойство не может быть null.
     *
     * Это формула "enrollmentAvailable".
     */
    // @NotNull
    public boolean isContractEnrollmentAvailable()
    {
        return _contractEnrollmentAvailable;
    }

    /**
     * @param contractEnrollmentAvailable Согласие на зачисление по договору (итоговое) (deprecated). Свойство не может быть null.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setContractEnrollmentAvailable(boolean contractEnrollmentAvailable)
    {
        dirty(_contractEnrollmentAvailable, contractEnrollmentAvailable);
        _contractEnrollmentAvailable = contractEnrollmentAvailable;
    }

    /**
     * @return Отказ от зачисления.
     *
     * Это формула "case when refusedDate is null then false else true end".
     */
    public Boolean getRefusedToBeEnrolled()
    {
        return _refusedToBeEnrolled;
    }

    /**
     * @param refusedToBeEnrolled Отказ от зачисления.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setRefusedToBeEnrolled(Boolean refusedToBeEnrolled)
    {
        dirty(_refusedToBeEnrolled, refusedToBeEnrolled);
        _refusedToBeEnrolled = refusedToBeEnrolled;
    }

    /**
     * @return Дата добавления. Свойство не может быть null.
     */
    @NotNull
    public Date getRegDate()
    {
        return _regDate;
    }

    /**
     * @param regDate Дата добавления. Свойство не может быть null.
     */
    public void setRegDate(Date regDate)
    {
        dirty(_regDate, regDate);
        _regDate = regDate;
    }

    /**
     * @return Индивидуальное ускоренное обучение. Свойство не может быть null.
     */
    @NotNull
    public boolean isAcceleratedLearning()
    {
        return _acceleratedLearning;
    }

    /**
     * @param acceleratedLearning Индивидуальное ускоренное обучение. Свойство не может быть null.
     */
    public void setAcceleratedLearning(boolean acceleratedLearning)
    {
        dirty(_acceleratedLearning, acceleratedLearning);
        _acceleratedLearning = acceleratedLearning;
    }

    /**
     * Обновляется демоном IEnrEntrantStateDaemonDao#doUpdateRequestedCompetitionStatus.
     *
     * @return Состояние.
     */
    public EnrEntrantState getState()
    {
        return _state;
    }

    /**
     * @param state Состояние.
     */
    public void setState(EnrEntrantState state)
    {
        dirty(_state, state);
        _state = state;
    }

    /**
     * Обновляется демоном EnrRequestExtParamsDaemonBean#doUpdateEnrReqCompEnrollAvailableAndEduDoc.
     *
     * @return Оригинал. Свойство не может быть null.
     */
    @NotNull
    public boolean isOriginalDocumentHandedIn()
    {
        return _originalDocumentHandedIn;
    }

    /**
     * @param originalDocumentHandedIn Оригинал. Свойство не может быть null.
     */
    public void setOriginalDocumentHandedIn(boolean originalDocumentHandedIn)
    {
        dirty(_originalDocumentHandedIn, originalDocumentHandedIn);
        _originalDocumentHandedIn = originalDocumentHandedIn;
    }

    /**
     * Необязательное поле.
     *
     * @return Дата отказа от зачисления.
     */
    public Date getRefusedDate()
    {
        return _refusedDate;
    }

    /**
     * @param refusedDate Дата отказа от зачисления.
     */
    public void setRefusedDate(Date refusedDate)
    {
        dirty(_refusedDate, refusedDate);
        _refusedDate = refusedDate;
    }

    /**
     * @return Комментарий.
     */
    public String getComment()
    {
        initLazyForGet("comment");
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        initLazyForSet("comment");
        dirty(_comment, comment);
        _comment = comment;
    }

    /**
     * @return Дата заключения договора.
     */
    public Date getContractCommonDate()
    {
        return _contractCommonDate;
    }

    /**
     * @param contractCommonDate Дата заключения договора.
     */
    public void setContractCommonDate(Date contractCommonDate)
    {
        dirty(_contractCommonDate, contractCommonDate);
        _contractCommonDate = contractCommonDate;
    }

    /**
     * @return Номер договора.
     */
    @Length(max=255)
    public String getContractCommonNumber()
    {
        return _contractCommonNumber;
    }

    /**
     * @param contractCommonNumber Номер договора.
     */
    public void setContractCommonNumber(String contractCommonNumber)
    {
        dirty(_contractCommonNumber, contractCommonNumber);
        _contractCommonNumber = contractCommonNumber;
    }

    /**
     * @return Организация-заказчик.
     */
    public ExternalOrgUnit getExternalOrgUnit()
    {
        return _externalOrgUnit;
    }

    /**
     * @param externalOrgUnit Организация-заказчик.
     */
    public void setExternalOrgUnit(ExternalOrgUnit externalOrgUnit)
    {
        dirty(_externalOrgUnit, externalOrgUnit);
        _externalOrgUnit = externalOrgUnit;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrRequestedCompetitionGen)
        {
            if (withNaturalIdProperties)
            {
                setRequest(((EnrRequestedCompetition)another).getRequest());
                setCompetition(((EnrRequestedCompetition)another).getCompetition());
            }
            setUniqueKey(((EnrRequestedCompetition)another).getUniqueKey());
            setPriority(((EnrRequestedCompetition)another).getPriority());
            setParallel(((EnrRequestedCompetition)another).isParallel());
            setProfileEducation(((EnrRequestedCompetition)another).isProfileEducation());
            setAccepted(((EnrRequestedCompetition)another).isAccepted());
            setAcceptedDate(((EnrRequestedCompetition)another).getAcceptedDate());
            setEnrollmentAvailable(((EnrRequestedCompetition)another).isEnrollmentAvailable());
            setAcceptedContract(((EnrRequestedCompetition)another).isAcceptedContract());
            setContractEnrollmentAvailable(((EnrRequestedCompetition)another).isContractEnrollmentAvailable());
            setRefusedToBeEnrolled(((EnrRequestedCompetition)another).getRefusedToBeEnrolled());
            setRegDate(((EnrRequestedCompetition)another).getRegDate());
            setAcceleratedLearning(((EnrRequestedCompetition)another).isAcceleratedLearning());
            setState(((EnrRequestedCompetition)another).getState());
            setOriginalDocumentHandedIn(((EnrRequestedCompetition)another).isOriginalDocumentHandedIn());
            setRefusedDate(((EnrRequestedCompetition)another).getRefusedDate());
            setComment(((EnrRequestedCompetition)another).getComment());
            setContractCommonDate(((EnrRequestedCompetition)another).getContractCommonDate());
            setContractCommonNumber(((EnrRequestedCompetition)another).getContractCommonNumber());
            setExternalOrgUnit(((EnrRequestedCompetition)another).getExternalOrgUnit());
        }
    }

    public INaturalId<EnrRequestedCompetitionGen> getNaturalId()
    {
        return new NaturalId(getRequest(), getCompetition());
    }

    public static class NaturalId extends NaturalIdBase<EnrRequestedCompetitionGen>
    {
        private static final String PROXY_NAME = "EnrRequestedCompetitionNaturalProxy";

        private Long _request;
        private Long _competition;

        public NaturalId()
        {}

        public NaturalId(EnrEntrantRequest request, EnrCompetition competition)
        {
            _request = ((IEntity) request).getId();
            _competition = ((IEntity) competition).getId();
        }

        public Long getRequest()
        {
            return _request;
        }

        public void setRequest(Long request)
        {
            _request = request;
        }

        public Long getCompetition()
        {
            return _competition;
        }

        public void setCompetition(Long competition)
        {
            _competition = competition;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrRequestedCompetitionGen.NaturalId) ) return false;

            EnrRequestedCompetitionGen.NaturalId that = (NaturalId) o;

            if( !equals(getRequest(), that.getRequest()) ) return false;
            if( !equals(getCompetition(), that.getCompetition()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getRequest());
            result = hashCode(result, getCompetition());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getRequest());
            sb.append("/");
            sb.append(getCompetition());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrRequestedCompetitionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrRequestedCompetition.class;
        }

        public T newInstance()
        {
            return (T) new EnrRequestedCompetition();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "request":
                    return obj.getRequest();
                case "competition":
                    return obj.getCompetition();
                case "uniqueKey":
                    return obj.getUniqueKey();
                case "priority":
                    return obj.getPriority();
                case "parallel":
                    return obj.isParallel();
                case "profileEducation":
                    return obj.isProfileEducation();
                case "accepted":
                    return obj.isAccepted();
                case "acceptedDate":
                    return obj.getAcceptedDate();
                case "enrollmentAvailable":
                    return obj.isEnrollmentAvailable();
                case "acceptedContract":
                    return obj.isAcceptedContract();
                case "contractEnrollmentAvailable":
                    return obj.isContractEnrollmentAvailable();
                case "refusedToBeEnrolled":
                    return obj.getRefusedToBeEnrolled();
                case "regDate":
                    return obj.getRegDate();
                case "acceleratedLearning":
                    return obj.isAcceleratedLearning();
                case "state":
                    return obj.getState();
                case "originalDocumentHandedIn":
                    return obj.isOriginalDocumentHandedIn();
                case "refusedDate":
                    return obj.getRefusedDate();
                case "comment":
                    return obj.getComment();
                case "contractCommonDate":
                    return obj.getContractCommonDate();
                case "contractCommonNumber":
                    return obj.getContractCommonNumber();
                case "externalOrgUnit":
                    return obj.getExternalOrgUnit();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "request":
                    obj.setRequest((EnrEntrantRequest) value);
                    return;
                case "competition":
                    obj.setCompetition((EnrCompetition) value);
                    return;
                case "uniqueKey":
                    obj.setUniqueKey((String) value);
                    return;
                case "priority":
                    obj.setPriority((Integer) value);
                    return;
                case "parallel":
                    obj.setParallel((Boolean) value);
                    return;
                case "profileEducation":
                    obj.setProfileEducation((Boolean) value);
                    return;
                case "accepted":
                    obj.setAccepted((Boolean) value);
                    return;
                case "acceptedDate":
                    obj.setAcceptedDate((Date) value);
                    return;
                case "enrollmentAvailable":
                    obj.setEnrollmentAvailable((Boolean) value);
                    return;
                case "acceptedContract":
                    obj.setAcceptedContract((Boolean) value);
                    return;
                case "contractEnrollmentAvailable":
                    obj.setContractEnrollmentAvailable((Boolean) value);
                    return;
                case "refusedToBeEnrolled":
                    obj.setRefusedToBeEnrolled((Boolean) value);
                    return;
                case "regDate":
                    obj.setRegDate((Date) value);
                    return;
                case "acceleratedLearning":
                    obj.setAcceleratedLearning((Boolean) value);
                    return;
                case "state":
                    obj.setState((EnrEntrantState) value);
                    return;
                case "originalDocumentHandedIn":
                    obj.setOriginalDocumentHandedIn((Boolean) value);
                    return;
                case "refusedDate":
                    obj.setRefusedDate((Date) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
                case "contractCommonDate":
                    obj.setContractCommonDate((Date) value);
                    return;
                case "contractCommonNumber":
                    obj.setContractCommonNumber((String) value);
                    return;
                case "externalOrgUnit":
                    obj.setExternalOrgUnit((ExternalOrgUnit) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "request":
                        return true;
                case "competition":
                        return true;
                case "uniqueKey":
                        return true;
                case "priority":
                        return true;
                case "parallel":
                        return true;
                case "profileEducation":
                        return true;
                case "accepted":
                        return true;
                case "acceptedDate":
                        return true;
                case "enrollmentAvailable":
                        return true;
                case "acceptedContract":
                        return true;
                case "contractEnrollmentAvailable":
                        return true;
                case "refusedToBeEnrolled":
                        return true;
                case "regDate":
                        return true;
                case "acceleratedLearning":
                        return true;
                case "state":
                        return true;
                case "originalDocumentHandedIn":
                        return true;
                case "refusedDate":
                        return true;
                case "comment":
                        return true;
                case "contractCommonDate":
                        return true;
                case "contractCommonNumber":
                        return true;
                case "externalOrgUnit":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "request":
                    return true;
                case "competition":
                    return true;
                case "uniqueKey":
                    return true;
                case "priority":
                    return true;
                case "parallel":
                    return true;
                case "profileEducation":
                    return true;
                case "accepted":
                    return true;
                case "acceptedDate":
                    return true;
                case "enrollmentAvailable":
                    return true;
                case "acceptedContract":
                    return true;
                case "contractEnrollmentAvailable":
                    return true;
                case "refusedToBeEnrolled":
                    return true;
                case "regDate":
                    return true;
                case "acceleratedLearning":
                    return true;
                case "state":
                    return true;
                case "originalDocumentHandedIn":
                    return true;
                case "refusedDate":
                    return true;
                case "comment":
                    return true;
                case "contractCommonDate":
                    return true;
                case "contractCommonNumber":
                    return true;
                case "externalOrgUnit":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "request":
                    return EnrEntrantRequest.class;
                case "competition":
                    return EnrCompetition.class;
                case "uniqueKey":
                    return String.class;
                case "priority":
                    return Integer.class;
                case "parallel":
                    return Boolean.class;
                case "profileEducation":
                    return Boolean.class;
                case "accepted":
                    return Boolean.class;
                case "acceptedDate":
                    return Date.class;
                case "enrollmentAvailable":
                    return Boolean.class;
                case "acceptedContract":
                    return Boolean.class;
                case "contractEnrollmentAvailable":
                    return Boolean.class;
                case "refusedToBeEnrolled":
                    return Boolean.class;
                case "regDate":
                    return Date.class;
                case "acceleratedLearning":
                    return Boolean.class;
                case "state":
                    return EnrEntrantState.class;
                case "originalDocumentHandedIn":
                    return Boolean.class;
                case "refusedDate":
                    return Date.class;
                case "comment":
                    return String.class;
                case "contractCommonDate":
                    return Date.class;
                case "contractCommonNumber":
                    return String.class;
                case "externalOrgUnit":
                    return ExternalOrgUnit.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrRequestedCompetition> _dslPath = new Path<EnrRequestedCompetition>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrRequestedCompetition");
    }
            

    /**
     * @return Заявление. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#getRequest()
     */
    public static EnrEntrantRequest.Path<EnrEntrantRequest> request()
    {
        return _dslPath.request();
    }

    /**
     * @return Конкурс. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#getCompetition()
     */
    public static EnrCompetition.Path<EnrCompetition> competition()
    {
        return _dslPath.competition();
    }

    /**
     * Обеспечивает уникальность в рамках абитуриента.
     * Если выбранный конкурс относится к отозванному заявлению, поле обнуляется.
     * Если выбранный конкурс относится к действующему заявлению, поле заполняется комбинацией идентификаторов абитуриента и конкурса (тем самым обеспечивается уникальность конкурса в рамках абитуриента).
     *
     * @return Уникальный ключ выбранного конкурса абитуриента. Свойство должно быть уникальным.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#getUniqueKey()
     */
    public static PropertyPath<String> uniqueKey()
    {
        return _dslPath.uniqueKey();
    }

    /**
     * Выбранный приоритет конкурса, глобальный.
     *
     * @return Выбранный приоритет. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#getPriority()
     */
    public static PropertyPath<Integer> priority()
    {
        return _dslPath.priority();
    }

    /**
     * Абитуриент хочет быть зачисленным на данный конкурс параллельно основному зачислению. Всегда false для конкурсов, где вид приема не предусматривает договора на обучение (все, кроме "договор" и "без ВИ (договор)". См. констрейнт.
     *
     * @return Поступление на параллельное обучение. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#isParallel()
     */
    public static PropertyPath<Boolean> parallel()
    {
        return _dslPath.parallel();
    }

    /**
     * @return Получено профильное образование. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#isProfileEducation()
     */
    public static PropertyPath<Boolean> profileEducation()
    {
        return _dslPath.profileEducation();
    }

    /**
     * Согласен быть зачисленным на данный конкурс.
     *
     * @return Согласие на зачисление. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#isAccepted()
     */
    public static PropertyPath<Boolean> accepted()
    {
        return _dslPath.accepted();
    }

    /**
     * @return Дата подачи согласия на зачисление.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#getAcceptedDate()
     */
    public static PropertyPath<Date> acceptedDate()
    {
        return _dslPath.acceptedDate();
    }

    /**
     * Заполняется системой на основе поля accepted и расширяемого механизма условий.
     *
     * @return Согласие на зачисление (итоговое). Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#isEnrollmentAvailable()
     */
    public static PropertyPath<Boolean> enrollmentAvailable()
    {
        return _dslPath.enrollmentAvailable();
    }

    /**
     * DEPRECATED
     *
     * @return Согласие на зачисление по договору (deprecated). Свойство не может быть null.
     *
     * Это формула "accepted".
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#isAcceptedContract()
     */
    public static PropertyPath<Boolean> acceptedContract()
    {
        return _dslPath.acceptedContract();
    }

    /**
     * DEPRECATED
     *
     * @return Согласие на зачисление по договору (итоговое) (deprecated). Свойство не может быть null.
     *
     * Это формула "enrollmentAvailable".
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#isContractEnrollmentAvailable()
     */
    public static PropertyPath<Boolean> contractEnrollmentAvailable()
    {
        return _dslPath.contractEnrollmentAvailable();
    }

    /**
     * @return Отказ от зачисления.
     *
     * Это формула "case when refusedDate is null then false else true end".
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#getRefusedToBeEnrolled()
     */
    public static PropertyPath<Boolean> refusedToBeEnrolled()
    {
        return _dslPath.refusedToBeEnrolled();
    }

    /**
     * @return Дата добавления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#getRegDate()
     */
    public static PropertyPath<Date> regDate()
    {
        return _dslPath.regDate();
    }

    /**
     * @return Индивидуальное ускоренное обучение. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#isAcceleratedLearning()
     */
    public static PropertyPath<Boolean> acceleratedLearning()
    {
        return _dslPath.acceleratedLearning();
    }

    /**
     * Обновляется демоном IEnrEntrantStateDaemonDao#doUpdateRequestedCompetitionStatus.
     *
     * @return Состояние.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#getState()
     */
    public static EnrEntrantState.Path<EnrEntrantState> state()
    {
        return _dslPath.state();
    }

    /**
     * Обновляется демоном EnrRequestExtParamsDaemonBean#doUpdateEnrReqCompEnrollAvailableAndEduDoc.
     *
     * @return Оригинал. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#isOriginalDocumentHandedIn()
     */
    public static PropertyPath<Boolean> originalDocumentHandedIn()
    {
        return _dslPath.originalDocumentHandedIn();
    }

    /**
     * Необязательное поле.
     *
     * @return Дата отказа от зачисления.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#getRefusedDate()
     */
    public static PropertyPath<Date> refusedDate()
    {
        return _dslPath.refusedDate();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * @return Дата заключения договора.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#getContractCommonDate()
     */
    public static PropertyPath<Date> contractCommonDate()
    {
        return _dslPath.contractCommonDate();
    }

    /**
     * @return Номер договора.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#getContractCommonNumber()
     */
    public static PropertyPath<String> contractCommonNumber()
    {
        return _dslPath.contractCommonNumber();
    }

    /**
     * @return Организация-заказчик.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#getExternalOrgUnit()
     */
    public static ExternalOrgUnit.Path<ExternalOrgUnit> externalOrgUnit()
    {
        return _dslPath.externalOrgUnit();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#getParametersTitle()
     */
    public static SupportedPropertyPath<String> parametersTitle()
    {
        return _dslPath.parametersTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EnrRequestedCompetition> extends EntityPath<E>
    {
        private EnrEntrantRequest.Path<EnrEntrantRequest> _request;
        private EnrCompetition.Path<EnrCompetition> _competition;
        private PropertyPath<String> _uniqueKey;
        private PropertyPath<Integer> _priority;
        private PropertyPath<Boolean> _parallel;
        private PropertyPath<Boolean> _profileEducation;
        private PropertyPath<Boolean> _accepted;
        private PropertyPath<Date> _acceptedDate;
        private PropertyPath<Boolean> _enrollmentAvailable;
        private PropertyPath<Boolean> _acceptedContract;
        private PropertyPath<Boolean> _contractEnrollmentAvailable;
        private PropertyPath<Boolean> _refusedToBeEnrolled;
        private PropertyPath<Date> _regDate;
        private PropertyPath<Boolean> _acceleratedLearning;
        private EnrEntrantState.Path<EnrEntrantState> _state;
        private PropertyPath<Boolean> _originalDocumentHandedIn;
        private PropertyPath<Date> _refusedDate;
        private PropertyPath<String> _comment;
        private PropertyPath<Date> _contractCommonDate;
        private PropertyPath<String> _contractCommonNumber;
        private ExternalOrgUnit.Path<ExternalOrgUnit> _externalOrgUnit;
        private SupportedPropertyPath<String> _parametersTitle;
        private SupportedPropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Заявление. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#getRequest()
     */
        public EnrEntrantRequest.Path<EnrEntrantRequest> request()
        {
            if(_request == null )
                _request = new EnrEntrantRequest.Path<EnrEntrantRequest>(L_REQUEST, this);
            return _request;
        }

    /**
     * @return Конкурс. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#getCompetition()
     */
        public EnrCompetition.Path<EnrCompetition> competition()
        {
            if(_competition == null )
                _competition = new EnrCompetition.Path<EnrCompetition>(L_COMPETITION, this);
            return _competition;
        }

    /**
     * Обеспечивает уникальность в рамках абитуриента.
     * Если выбранный конкурс относится к отозванному заявлению, поле обнуляется.
     * Если выбранный конкурс относится к действующему заявлению, поле заполняется комбинацией идентификаторов абитуриента и конкурса (тем самым обеспечивается уникальность конкурса в рамках абитуриента).
     *
     * @return Уникальный ключ выбранного конкурса абитуриента. Свойство должно быть уникальным.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#getUniqueKey()
     */
        public PropertyPath<String> uniqueKey()
        {
            if(_uniqueKey == null )
                _uniqueKey = new PropertyPath<String>(EnrRequestedCompetitionGen.P_UNIQUE_KEY, this);
            return _uniqueKey;
        }

    /**
     * Выбранный приоритет конкурса, глобальный.
     *
     * @return Выбранный приоритет. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#getPriority()
     */
        public PropertyPath<Integer> priority()
        {
            if(_priority == null )
                _priority = new PropertyPath<Integer>(EnrRequestedCompetitionGen.P_PRIORITY, this);
            return _priority;
        }

    /**
     * Абитуриент хочет быть зачисленным на данный конкурс параллельно основному зачислению. Всегда false для конкурсов, где вид приема не предусматривает договора на обучение (все, кроме "договор" и "без ВИ (договор)". См. констрейнт.
     *
     * @return Поступление на параллельное обучение. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#isParallel()
     */
        public PropertyPath<Boolean> parallel()
        {
            if(_parallel == null )
                _parallel = new PropertyPath<Boolean>(EnrRequestedCompetitionGen.P_PARALLEL, this);
            return _parallel;
        }

    /**
     * @return Получено профильное образование. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#isProfileEducation()
     */
        public PropertyPath<Boolean> profileEducation()
        {
            if(_profileEducation == null )
                _profileEducation = new PropertyPath<Boolean>(EnrRequestedCompetitionGen.P_PROFILE_EDUCATION, this);
            return _profileEducation;
        }

    /**
     * Согласен быть зачисленным на данный конкурс.
     *
     * @return Согласие на зачисление. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#isAccepted()
     */
        public PropertyPath<Boolean> accepted()
        {
            if(_accepted == null )
                _accepted = new PropertyPath<Boolean>(EnrRequestedCompetitionGen.P_ACCEPTED, this);
            return _accepted;
        }

    /**
     * @return Дата подачи согласия на зачисление.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#getAcceptedDate()
     */
        public PropertyPath<Date> acceptedDate()
        {
            if(_acceptedDate == null )
                _acceptedDate = new PropertyPath<Date>(EnrRequestedCompetitionGen.P_ACCEPTED_DATE, this);
            return _acceptedDate;
        }

    /**
     * Заполняется системой на основе поля accepted и расширяемого механизма условий.
     *
     * @return Согласие на зачисление (итоговое). Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#isEnrollmentAvailable()
     */
        public PropertyPath<Boolean> enrollmentAvailable()
        {
            if(_enrollmentAvailable == null )
                _enrollmentAvailable = new PropertyPath<Boolean>(EnrRequestedCompetitionGen.P_ENROLLMENT_AVAILABLE, this);
            return _enrollmentAvailable;
        }

    /**
     * DEPRECATED
     *
     * @return Согласие на зачисление по договору (deprecated). Свойство не может быть null.
     *
     * Это формула "accepted".
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#isAcceptedContract()
     */
        public PropertyPath<Boolean> acceptedContract()
        {
            if(_acceptedContract == null )
                _acceptedContract = new PropertyPath<Boolean>(EnrRequestedCompetitionGen.P_ACCEPTED_CONTRACT, this);
            return _acceptedContract;
        }

    /**
     * DEPRECATED
     *
     * @return Согласие на зачисление по договору (итоговое) (deprecated). Свойство не может быть null.
     *
     * Это формула "enrollmentAvailable".
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#isContractEnrollmentAvailable()
     */
        public PropertyPath<Boolean> contractEnrollmentAvailable()
        {
            if(_contractEnrollmentAvailable == null )
                _contractEnrollmentAvailable = new PropertyPath<Boolean>(EnrRequestedCompetitionGen.P_CONTRACT_ENROLLMENT_AVAILABLE, this);
            return _contractEnrollmentAvailable;
        }

    /**
     * @return Отказ от зачисления.
     *
     * Это формула "case when refusedDate is null then false else true end".
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#getRefusedToBeEnrolled()
     */
        public PropertyPath<Boolean> refusedToBeEnrolled()
        {
            if(_refusedToBeEnrolled == null )
                _refusedToBeEnrolled = new PropertyPath<Boolean>(EnrRequestedCompetitionGen.P_REFUSED_TO_BE_ENROLLED, this);
            return _refusedToBeEnrolled;
        }

    /**
     * @return Дата добавления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#getRegDate()
     */
        public PropertyPath<Date> regDate()
        {
            if(_regDate == null )
                _regDate = new PropertyPath<Date>(EnrRequestedCompetitionGen.P_REG_DATE, this);
            return _regDate;
        }

    /**
     * @return Индивидуальное ускоренное обучение. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#isAcceleratedLearning()
     */
        public PropertyPath<Boolean> acceleratedLearning()
        {
            if(_acceleratedLearning == null )
                _acceleratedLearning = new PropertyPath<Boolean>(EnrRequestedCompetitionGen.P_ACCELERATED_LEARNING, this);
            return _acceleratedLearning;
        }

    /**
     * Обновляется демоном IEnrEntrantStateDaemonDao#doUpdateRequestedCompetitionStatus.
     *
     * @return Состояние.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#getState()
     */
        public EnrEntrantState.Path<EnrEntrantState> state()
        {
            if(_state == null )
                _state = new EnrEntrantState.Path<EnrEntrantState>(L_STATE, this);
            return _state;
        }

    /**
     * Обновляется демоном EnrRequestExtParamsDaemonBean#doUpdateEnrReqCompEnrollAvailableAndEduDoc.
     *
     * @return Оригинал. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#isOriginalDocumentHandedIn()
     */
        public PropertyPath<Boolean> originalDocumentHandedIn()
        {
            if(_originalDocumentHandedIn == null )
                _originalDocumentHandedIn = new PropertyPath<Boolean>(EnrRequestedCompetitionGen.P_ORIGINAL_DOCUMENT_HANDED_IN, this);
            return _originalDocumentHandedIn;
        }

    /**
     * Необязательное поле.
     *
     * @return Дата отказа от зачисления.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#getRefusedDate()
     */
        public PropertyPath<Date> refusedDate()
        {
            if(_refusedDate == null )
                _refusedDate = new PropertyPath<Date>(EnrRequestedCompetitionGen.P_REFUSED_DATE, this);
            return _refusedDate;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(EnrRequestedCompetitionGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * @return Дата заключения договора.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#getContractCommonDate()
     */
        public PropertyPath<Date> contractCommonDate()
        {
            if(_contractCommonDate == null )
                _contractCommonDate = new PropertyPath<Date>(EnrRequestedCompetitionGen.P_CONTRACT_COMMON_DATE, this);
            return _contractCommonDate;
        }

    /**
     * @return Номер договора.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#getContractCommonNumber()
     */
        public PropertyPath<String> contractCommonNumber()
        {
            if(_contractCommonNumber == null )
                _contractCommonNumber = new PropertyPath<String>(EnrRequestedCompetitionGen.P_CONTRACT_COMMON_NUMBER, this);
            return _contractCommonNumber;
        }

    /**
     * @return Организация-заказчик.
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#getExternalOrgUnit()
     */
        public ExternalOrgUnit.Path<ExternalOrgUnit> externalOrgUnit()
        {
            if(_externalOrgUnit == null )
                _externalOrgUnit = new ExternalOrgUnit.Path<ExternalOrgUnit>(L_EXTERNAL_ORG_UNIT, this);
            return _externalOrgUnit;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#getParametersTitle()
     */
        public SupportedPropertyPath<String> parametersTitle()
        {
            if(_parametersTitle == null )
                _parametersTitle = new SupportedPropertyPath<String>(EnrRequestedCompetitionGen.P_PARAMETERS_TITLE, this);
            return _parametersTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(EnrRequestedCompetitionGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EnrRequestedCompetition.class;
        }

        public String getEntityName()
        {
            return "enrRequestedCompetition";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getParametersTitle();

    public abstract String getTitle();
}
