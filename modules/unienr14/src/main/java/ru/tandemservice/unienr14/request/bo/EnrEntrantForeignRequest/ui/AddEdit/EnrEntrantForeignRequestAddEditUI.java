/* $Id$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantForeignRequest.ui.AddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unienr14.request.bo.EnrEntrantForeignRequest.EnrEntrantForeignRequestManager;
import ru.tandemservice.unienr14.request.entity.EnrEntrantForeignRequest;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.Date;

/**
 * @author nvankov
 * @since 7/15/14
 */
@Input({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "request.id")
})
public class EnrEntrantForeignRequestAddEditUI extends UIPresenter
{
    private EnrEntrantForeignRequest _request = new EnrEntrantForeignRequest();
    private EnrEnrollmentCampaign _enrCampaign;

    @Override
    public void onComponentRefresh()
    {
        if(isAddForm())
            _enrCampaign = EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign();
        else
        {
            _request = DataAccessServices.dao().get(_request.getId());
            _enrCampaign = _request.getEntrant().getEnrollmentCampaign();
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, _enrCampaign.getId());
        if(EnrEntrantForeignRequestAddEdit.IDENTITY_CARD_DS.equals(dataSource.getName())
                || EnrEntrantForeignRequestAddEdit.EDU_DOC_DS.equals(dataSource.getName()))
        {
            dataSource.put("entrantId", _request.getEntrant() != null ? _request.getEntrant().getId() : null);
        }
    }

    public boolean isAddForm()
    {
        return _request.getId() == null;
    }

    public boolean isEditForm()
    {
        return !isAddForm();
    }

    public void onClickApply()
    {
        if(_request.getEnrollmentDate() != null && _request.getEnrollmentDate().before(_request.getRegDate()))
            _uiSupport.error("Дата зачисления не может быть раньше даты подачи заявления.", "regDate", "enrollDate");

        if(getUserContext().getErrorCollector().hasErrors())
            return;

        EnrEntrantForeignRequestManager.instance().dao().createOrUpdate(_request);
        deactivate();
    }

    public EnrEnrollmentCampaign getEnrCampaign()
    {
        return _enrCampaign;
    }

    public void setEnrCampaign(EnrEnrollmentCampaign enrCampaign)
    {
        _enrCampaign = enrCampaign;
    }

    public EnrEntrantForeignRequest getRequest()
    {
        return _request;
    }

    public void setRequest(EnrEntrantForeignRequest request)
    {
        _request = request;
    }
}
