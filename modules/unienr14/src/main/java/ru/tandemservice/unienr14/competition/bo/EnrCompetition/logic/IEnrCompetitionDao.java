/* $Id:$ */
package ru.tandemservice.unienr14.competition.bo.EnrCompetition.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;

import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.Collection;
import java.util.Set;

/**
 * @author oleyba
 * @since 2/3/14
 */
public interface IEnrCompetitionDao extends INeedPersistenceSupport
{

    void doToggleAllowProgramPriorities(Long competitionId);


    enum PlanCalculationRule
    {

        /** только распределение мест между СОО и ПО в равных долях */
        EQUAL_PARTS,

        /** только распределение мест между СОО и ПО (без переноса КЦП в общий конкурс) */
        BY_PASSED,

        /** перенести невостребованные КЦП по квоте, ЦП, без ВИ в общий конкурс */
        BY_PASSED_AND_FREE

    }

    /**
     * для всех конкурсов ВПО бакалавриата и специалитета:
     * распределяет места между СОО и ПО, согласно числу подавших документов и правилу пересчета плана
     */
//    void doCorrectProgramSetBSPlans(EnrEnrollmentCampaign ec, PlanCalculationRule correctionRule, boolean budget, boolean contract, boolean updatePlansSetManually);


    /**
     * для набора ОП распределяет места между СОО и ПО, согласно числу подавших документов и правилу пересчета плана
     */
    void doCalculateProgramSetPlans(EnrEnrollmentCampaign ec, PlanCalculationRule correctionRule, Collection<Long> programSetIds, boolean updatePlansSetManually);

    interface ICalculatedPlanOwner extends IEntity
    {
        int getPlan();
        void setPlan(int plan);

        int getCalculatedPlan();
        void setCalculatedPlan(int plan);
    }
}