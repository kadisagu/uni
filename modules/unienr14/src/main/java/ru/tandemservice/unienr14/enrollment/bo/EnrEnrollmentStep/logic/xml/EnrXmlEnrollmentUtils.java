package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.xml;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 * @author vdanilov
 */
public class EnrXmlEnrollmentUtils {

    public static byte[] toXml(final EnrXmlEnrollmentStepState root) {
        try {
            final JAXBContext jc = JAXBContext.newInstance(root.getClass());
            final Marshaller m = jc.createMarshaller();
            m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            final ByteArrayOutputStream ba = new ByteArrayOutputStream(8192);
            m.marshal(root, ba);
            ba.flush();

            return ba.toByteArray();
        } catch (RuntimeException t) {
            throw t;
        } catch (Throwable t) {
            throw new RuntimeException(t);
        }
    }

    public static <T> T fromXml(final Class<T> clazz, final byte[] xml) {
        try {
            final JAXBContext jc = JAXBContext.newInstance(clazz);
            final Unmarshaller u = jc.createUnmarshaller();
            final Object object = u.unmarshal(new ByteArrayInputStream(xml));

            // если это нужный класс, то его и возвращаем
            if (clazz.isInstance(object)) {
                return clazz.cast(object);
            }

            // пришло хз что
            throw new IllegalStateException(new String(xml));
        } catch (RuntimeException t) {
            throw t;
        } catch (Throwable t) {
            throw new RuntimeException(t);
        }
    }
}
