/**
 *$Id: EnrExamRoomAddEditUI.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.exams.bo.EnrExamRoom.ui.AddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniedu.orgunit.entity.EduInstitutionOrgUnit;
import ru.tandemservice.unienr14.exams.bo.EnrExamRoom.EnrExamRoomManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignExamRoom;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;

/**
 * @author Alexander Shaburov
 * @since 21.05.13
 */
@Input({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "entityHolder.id"),
    @Bind(key = "territorialOrgUnitId", binding = "territorialOrgUnitId")
})
public class EnrExamRoomAddEditUI extends UIPresenter
{
    public static final String PROP_BUILDING = "building";

    private Long _territorialOrgUnitId;

    private EnrOrgUnit _orgUnit;

    private EntityHolder<EnrCampaignExamRoom> _entityHolder = new EntityHolder<>(new EnrCampaignExamRoom());
    private UniplacesBuilding _building;

    @Override
    public void onComponentRefresh()
    {
        _entityHolder.refresh();

        OrgUnit terrOrgUnit = null;
        if (_entityHolder.getId() == null)
        {
            _entityHolder.getValue().setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
            if (_territorialOrgUnitId != null)
            {
                terrOrgUnit = DataAccessServices.dao().getNotNull(OrgUnit.class, _territorialOrgUnitId);
                _entityHolder.getValue().setResponsibleOrgUnit(terrOrgUnit);
            }
        }
        else
        {
            _building = _entityHolder.getValue().getPlace().getFloor().getUnit().getBuilding();
            terrOrgUnit = _entityHolder.getValue().getResponsibleOrgUnit();
        }

        if (terrOrgUnit != null)
        {
            EduInstitutionOrgUnit institutionOrgUnit = IUniBaseDao.instance.get().getByNaturalId(new EduInstitutionOrgUnit.NaturalId(terrOrgUnit));
            if (institutionOrgUnit != null)
                setOrgUnit(IUniBaseDao.instance.get().<EnrOrgUnit>getByNaturalId(new EnrOrgUnit.NaturalId(_entityHolder.getValue().getEnrollmentCampaign(), institutionOrgUnit)));
        }
    }

    public void onClickApply()
    {
        _entityHolder.getValue().setResponsibleOrgUnit(getOrgUnit().getInstitutionOrgUnit().getOrgUnit());
        EnrExamRoomManager.instance().dao().saveOrUpdate(_entityHolder.getValue());
        deactivate();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(PROP_BUILDING, _building);
        dataSource.put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, _entityHolder.getValue().getEnrollmentCampaign());
    }

    // Getters & Setters

    public String getSticker()
    {
        if (isEditForm())
            return getConfig().getProperty("ui.sticker.edit");
        else
            return getConfig().getProperty("ui.sticker.add");
    }

    public boolean isEditForm()
    {
        return _entityHolder.getId() != null;
    }

    /* Generated */

    public UniplacesBuilding getBuilding()
    {
        return _building;
    }

    public void setBuilding(UniplacesBuilding building)
    {
        _building = building;
    }

    public EntityHolder<EnrCampaignExamRoom> getEntityHolder()
    {
        return _entityHolder;
    }

    public void setEntityHolder(EntityHolder<EnrCampaignExamRoom> entityHolder)
    {
        _entityHolder = entityHolder;
    }

    public Long getTerritorialOrgUnitId()
    {
        return _territorialOrgUnitId;
    }

    public void setTerritorialOrgUnitId(Long territorialOrgUnitId)
    {
        _territorialOrgUnitId = territorialOrgUnitId;
    }

    public EnrOrgUnit getOrgUnit(){ return _orgUnit; }

    public void setOrgUnit(EnrOrgUnit orgUnit){ _orgUnit = orgUnit; }
}
