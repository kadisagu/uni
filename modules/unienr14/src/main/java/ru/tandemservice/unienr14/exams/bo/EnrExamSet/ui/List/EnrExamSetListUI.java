/**
 *$Id: EnrExamSetListUI.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.exams.bo.EnrExamSet.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.ui.AddEdit.EnrExamSetAddEdit;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

/**
 * @author Alexander Shaburov
 * @since 16.04.13
 */
public class EnrExamSetListUI extends UIPresenter
{
    @Override
    public void onComponentRefresh()
    {
        getSettings().set(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
    }

    public void onClickAddExamSet()
    {
        getEnrollmentCampaign().checkOpen();
        getActivationBuilder().asRegion(EnrExamSetAddEdit.class)
                .activate();
    }

    public void onClickEditExamSet()
    {
        getActivationBuilder().asRegion(EnrExamSetAddEdit.class)
                .parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong())
                .activate();
    }

    public void onClickDeleteExamSet()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public void onEnrCampaignRefresh()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, getEnrollmentCampaign());
    }

    // Getters & Setters

    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return getSettings().get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
    }

    public boolean isNothingSelected()
    {
        return getSettings().get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN) == null;
    }

    /* -- */
}
