/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.utils;

import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.Date;

/**
 * @author rsizonenko
 * @since 29.05.2014
 */
public class EnrReportDateSelector {

    public EnrReportDateSelector() {
        EnrEnrollmentCampaign campaign = EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign();
        refreshDates(campaign);
    }

    private Date dateFrom;
    private Date dateTo;

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public void refreshDates(EnrEnrollmentCampaign campaign)
    {
        if (campaign == null)
        {
            dateFrom = null;
            dateTo = null;
        } else {
            dateFrom = campaign.getDateFrom();
            dateTo = campaign.getDateTo();
        }
    }
}
