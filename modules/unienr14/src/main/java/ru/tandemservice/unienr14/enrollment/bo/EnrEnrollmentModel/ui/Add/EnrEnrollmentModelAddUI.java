/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.ui.Add;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unienr14.catalog.bo.EnrRequestType.EnrRequestTypeManager;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentStepKind;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEnrollmentStepKindCodes;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.EnrEnrollmentModelManager;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.ui.Pub.EnrEnrollmentModelPub;
import ru.tandemservice.unienr14.enrollment.entity.EnrModelStep;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignEnrollmentStage;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 3/27/15
 */
@Input({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "campaignId")
})
public class EnrEnrollmentModelAddUI extends UIPresenter
{
    private Long campaignId;
    private EnrEnrollmentCampaign enrollmentCampaign;
    private EnrRequestType requestType;
    private String title;
    private boolean inCrimea = false;

    private List<EnrModelStep> stepList = new ArrayList<>();
    private EnrModelStep step;

    @Override
    public void onComponentRefresh()
    {
        setEnrollmentCampaign(DataAccessServices.dao().getNotNull(getCampaignId()));
        setRequestType(EnrRequestTypeManager.instance().dao().getDefaultRequestType(getEnrollmentCampaign()));
        onChangeRequestType();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, getEnrollmentCampaign());
    }

    public void onChangeRequestType() {
        if (getRequestType() == null) {
            setStepList(new ArrayList<EnrModelStep>());
            return;
        }

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EnrCampaignEnrollmentStage.class, "s")
                .column(property("s", EnrCampaignEnrollmentStage.date()))
                .where(eq(property("s", EnrCampaignEnrollmentStage.requestType()), value(getRequestType())))
                .where(eq(property("s", EnrCampaignEnrollmentStage.enrollmentCampaign().id()), value(getCampaignId())))
                .where(eq(property("s", EnrCampaignEnrollmentStage.acceptPeopleResidingInCrimea()), value(isInCrimea())))
                .order(property("s", EnrCampaignEnrollmentStage.date()))
                .distinct();

        List<Date> stages = DataAccessServices.dao().getList(dql);
        setStepList(new ArrayList<EnrModelStep>(stages.size()));

        EnrEnrollmentStepKind defKind = DataAccessServices.dao().get(EnrEnrollmentStepKind.class, EnrEnrollmentStepKind.code(), EnrEnrollmentStepKindCodes.NO_REC);

        for (Date date : stages) {
            getStepList().add(new EnrModelStep(date, defKind));
        }
    }

    public void onChangeKind() {
        for (EnrModelStep step : getStepList()) {
            if (step.getKind() != null && EnrEnrollmentStepKindCodes.NO_REC_PERCENTAGE.equals(step.getKind().getCode()) && 0 == step.getPercentageAsLong()) {
                step.setPercentageAsLong(8000L);
            }
        }
    }

    public void onClickApply() {
        EnrRequestTypeManager.instance().dao().saveDefaultRequestType(getRequestType(), getEnrollmentCampaign());
        Long model = EnrEnrollmentModelManager.instance().dao().doCreateModel(getCampaignId(), getRequestType(), getTitle(), getStepList());
        _uiActivation.asDesktopRoot(EnrEnrollmentModelPub.class).parameter(PUBLISHER_ID, model).activate();
        deactivate();
    }

    // getters and setters

    public String getKindFieldName() {
        return "Вариант работы с шагом зачисления " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getStep().getEnrollmentDate());
    }

    public String getKindFieldId() {
        return "kind." + DateFormatter.DEFAULT_DATE_FORMATTER.format(getStep().getEnrollmentDate());
    }

    public String getPercentageFieldId() {
        return "p." + DateFormatter.DEFAULT_DATE_FORMATTER.format(getStep().getEnrollmentDate());
    }

    public boolean isShowPercentage() {
        return getStep().getKind() != null && getStep().getKind().isUsePercentage();
    }

    public EnrModelStep getStep()
    {
        return step;
    }

    public void setStep(EnrModelStep step)
    {
        this.step = step;
    }

    public List<EnrModelStep> getStepList()
    {
        return stepList;
    }

    public void setStepList(List<EnrModelStep> stepList)
    {
        this.stepList = stepList;
    }

    public EnrRequestType getRequestType()
    {
        return requestType;
    }

    public void setRequestType(EnrRequestType requestType)
    {
        this.requestType = requestType;
    }

    public Long getCampaignId()
    {
        return campaignId;
    }

    public void setCampaignId(Long campaignId)
    {
        this.campaignId = campaignId;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        this.enrollmentCampaign = enrollmentCampaign;
    }

    public boolean isInCrimea()
    {
        return inCrimea;
    }

    public void setInCrimea(boolean inCrimea)
    {
        this.inCrimea = inCrimea;
    }
}