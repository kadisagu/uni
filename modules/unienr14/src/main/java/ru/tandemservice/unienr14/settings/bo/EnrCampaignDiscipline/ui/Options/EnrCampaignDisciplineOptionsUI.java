/**
 *$Id: EnrCampaignDisciplineOptionsUI.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.settings.bo.EnrCampaignDiscipline.ui.Options;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings;

/**
 * @author Alexander Shaburov
 * @since 17.06.13
 */
public class EnrCampaignDisciplineOptionsUI extends UIPresenter
{
    private EnrEnrollmentCampaign _enrCampaign;
    private EnrEnrollmentCampaignSettings _enrCampSettings;

    private IdentifiableWrapper _checkExamInterval;

    @Override
    public void onComponentRefresh()
    {
        _enrCampaign = EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign();
        if (_enrCampaign != null)
            _enrCampSettings = _enrCampaign.getSettings();
        onEnrCampaignRefresh();
    }

    public void onClickApply()
    {
        DataAccessServices.dao().update(_enrCampSettings);
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(_enrCampaign);
    }

    public void onEnrCampaignRefresh()
    {
        if (_enrCampaign == null)
        {
            _enrCampSettings = null;
            return;
        }
        _enrCampSettings = _enrCampaign.getSettings();
    }

    // Getters & Setters

    public boolean isEmptySelectors()
    {
        return _enrCampaign == null;
    }

    // Accessors

    public EnrEnrollmentCampaign getEnrCampaign()
    {
        return _enrCampaign;
    }

    public void setEnrCampaign(EnrEnrollmentCampaign enrCampaign)
    {
        _enrCampaign = enrCampaign;
    }

    public EnrEnrollmentCampaignSettings getEnrCampSettings()
    {
        return _enrCampSettings;
    }

    public void setEnrCampSettings(EnrEnrollmentCampaignSettings enrCampSettings)
    {
        _enrCampSettings = enrCampSettings;
    }

    public IdentifiableWrapper getCheckExamInterval()
    {
        return _checkExamInterval;
    }

    public void setCheckExamInterval(IdentifiableWrapper checkExamInterval)
    {
        _checkExamInterval = checkExamInterval;
    }
}
