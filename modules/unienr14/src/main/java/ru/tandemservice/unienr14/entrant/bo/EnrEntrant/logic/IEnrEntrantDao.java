/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unienr14.catalog.entity.*;
import ru.tandemservice.unienr14.competition.entity.EnrExamVariant;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantCustomState;
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceBenefit;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionNoExams;
import ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitProofDocument;
import ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitStatement;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.*;

/**
 * @author oleyba
 * @since 4/10/13
 */
public interface IEnrEntrantDao extends INeedPersistenceSupport
{
    /**
     * Генерирует и возвращает новый персональный номер абитуриента в приемной кампании
     * @return новый персональный номер
     */
    String getUniqueEntrantNumber(EnrEntrant entrant);

    /**
     * Удаляет абитуриента.
     * Если у абитуриента есть заявления, или ДДС, включенные в ЭГ, то не позволяет его удалить.
     * Иначе при удалении абитуриента удаляются все связанные с ним объекты, включая ДДС вместе с оценками.
     *
     * @param entrant абитуриент
     */
    void deleteEntrant(EnrEntrant entrant);

    /**
     * Отправляет абитуриента в архив, если он не в архиве, иначе извлекает его из архива.
     * Проверяет допустимость состояния:
     * не разрешено добавлять в архив предзачисленных, зачисленных, и находящихся в приказе абитуриентов.
     *
     * @param entrant абитуриент
     */
    void doChangeEntrantArchival(EnrEntrant entrant);

    /**
     * Для более простой работы с приоритетами имеет смысл их нормализация (от 1 без пробелов)
     * Это актуально для приоритетов ВКГ среди всех заявлений, ВНП в рамках ВКГ, и профилей
     * Этот метод нормализует приоритеты и вызывается при удалении соотв. объектов и изменении приоритетов пользователем вручную
     * @param entrant абитуриент
     * @param requestType вид заявления
     */
    void doNormalizePriorityPerEntrantAndRequestType(EnrEntrant entrant, EnrRequestType requestType);

    /**
     * Для более простой работы с приоритетами имеет смысл их нормализация (от 1 без пробелов)
     * Это актуально для приоритетов выбранных конкурсов среди всех заявлений соотв. типа, и программ в рамках конкурсов
     * Этот метод нормализует приоритеты и вызывается при удалении соотв. объектов и изменении приоритетов пользователем вручную
     * Работает только с заявлениями, по которым не были отозваны документы
     * @param entrant абитуриент
     */
    void doNormalizePriorityPerEntrant(EnrEntrant entrant);

    void saveSelectedProfilesAndAllPriorities(EnrEntrant entrant, EnrRequestType requestType, Map<EnrRequestedCompetition, Map<EnrProgramSetItem, Integer>> priorityMap);

    void doSaveBenefitNoExamsData(EnrRequestedCompetitionNoExams requestedCompetition, EnrProgramSetItem programSetItem, EnrBenefitCategory benefitCategory, List<IEnrEntrantBenefitProofDocument> documentList);

    void doSaveBenefitMarkSourceData(EnrEntrantMarkSourceBenefit markSourceBenefit, EnrBenefitCategory benefitCategory, IEnrEntrantBenefitProofDocument mainProof);

    void doSaveBenefitData(IEnrEntrantBenefitStatement statement, EnrBenefitCategory benefitCategory, List<IEnrEntrantBenefitProofDocument> documentList);

    /** Список документов, подтверждающих особое право, данного факта использования особого права. */
    List<IEnrEntrantBenefitProofDocument> getBenefitStatementProofDocs(IEnrEntrantBenefitStatement statement);

    void saveOrUpdateCustomState(EnrEntrantCustomState entrantCustomState);

    Map<EnrEntrant, List<EnrEntrantCustomState>> getActiveCustomStatesMap(Collection<EnrEntrant> entrants, Date date);

    void saveOrUpdateAchievement(EnrEntrantAchievement achievement);

    /**
     * Обертка для информации о ВВИ
     */
    public interface IChosenExamWrapper
    {
        EnrExamVariant getActualExam();
        EnrCampaignDiscipline getDiscipline();
        Set<EnrExamPassForm> getSelectedPassForms();
        EnrBenefitCategory getDiplomaCategory();
        IEnrEntrantBenefitProofDocument getDiploma();
    }

    /**
     * Сохраняет ВВИ
     *
     * @param entrant абитуриент
     * @param chosenExamMap перечень ВВИ по КГ
     * @param internalExamReason категории лиц, обладающих правом сдавать общеобразовательные ВИ по материалам ОУ
     */
    void saveChosenExams(EnrEntrant entrant, Map<EnrRequestedCompetition, List<IChosenExamWrapper>> chosenExamMap, EnrInternalExamReason internalExamReason);

    /**
     * Сохраняет в базе абитуриента (или его данные):
     * <ul>
     * <li>если null != entrant.id, то обновляет данные абитуриента, его адреса, персоны и ее карточки</li>
     * <li>если null != entrant.person.id, то по персоне пытается найти абитуриента, если такового нет - создает, обновляет данные абитуриента, его адреса, персоны и ее карточки</li>
     * <li>если null == entrant.person.id, создает персоны и по ней абитуриента</li>
     * </ul>
     * @param entrant абитуриент
     * @return id абитуриента
     */
    Long doSaveEntrantWizardData(EnrEntrant entrant, AddressBase regAddressDTO, AddressBase factAddressDTO);

    /**
     * Находит абитуриента в рамках ПК для заданной персоны, если таковой уже есть
     * @param campaign ПК
     * @param person персона
     * @return существующий абитуриент, если есть
     */
    EnrEntrant findSuitableEntrant(EnrEnrollmentCampaign campaign, Person person);

    /**
     * Для вывода информационного сообщения в случае, если у абитуриента не выбраны ВИ по какой-то из КГ, либо они неактуальны.
     * Опирается на кэшированный признак в рейтинге абитуриента.
     *
     * @param entrant Абитуриент
     * @return true, если есть рейтинг с отметкой о некорректности ВИ, false в остальных случаях
     */
    boolean isEntranceExamIncorrect(EnrEntrant entrant);

    /**
     * Для вывода информационного сообщения в случае, если у абитуриента не выбраны профили по какому-то НП, где они предусмотрены.
     *
     * @param entrant Абитуриент
     * @return true, если есть ВНП без Впроф для НП, где есть НПпроф с приемом по соотв. виду конкурса, false в остальных случаях
     */
    boolean isProfileNotSelected(EnrEntrant entrant);

    /**
     * Для вывода информационного сообщения в случае, если у абитуриента есть незачтенный диплом олимпиады:
     * т.е. диплом, который не использован, как основание балла или документ, подтверждающий особое право
     *
     * @param entrant Абитуриент
     * @return true, если есть такой диплом, false в остальных случаях
     */
    boolean isOlympiadNotUsed(EnrEntrant entrant);

    /**
     * Для вывода информационного сообщения в случае, если у абитуриента список ДДС не соответствует ВВИ:
     * есть ВВИ, для которых нет ДДС, или наоборот
     *
     * @param entrant Абитуриент
     * @return true, если есть несоответствие, false в остальных случаях
     */
    boolean isExamListIncorrect(EnrEntrant entrant);

    /**
     * Обновляет сведения о профильной подготовке.
     * @param entrant Абитуриент
     * @param accessCourseList Подготовительные курсы
     * @param accessDepartmentList Подготовительные отделения
     */
    void updatePreparationData(EnrEntrant entrant, List<EnrAccessCourse> accessCourseList, List<EnrAccessDepartment> accessDepartmentList);

    /**
     * Сохраняет данные абитуриента и источники информации об ОУ
     * @param entrant абитуриент
     * @param sourceInfoList источники информации об ОУ
     */
    void updateEntrantData(EnrEntrant entrant, List<EnrSourceInfoAboutUniversity> sourceInfoList);


    /** Является ли подразделение лицензированным или формирующим. */
    public boolean isOrgUnitFormingOrEnrOrgUnit(final OrgUnit orgUnit);

    public List<IEntity> getSecLocalEntitiesForEntrant(Long entrantId);
}
