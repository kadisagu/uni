/* $Id:$ */
package ru.tandemservice.unienr14.order.bo.EnrCancelParagraph.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.order.entity.EnrAbstractExtract;
import ru.tandemservice.unienr14.order.entity.EnrCancelParagraph;
import ru.tandemservice.unienr14.order.entity.EnrOrder;

import java.util.List;

/**
 * @author oleyba
 * @since 7/10/14
 */
public interface IEnrCancelParagraphDao extends INeedPersistenceSupport
{
    void saveOrUpdateParagraph(EnrOrder order, EnrCancelParagraph paragraph, List<? extends EnrAbstractExtract> extracts);
}