package ru.tandemservice.unienr14.settings.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrExamGroupSet;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Набор экзаменационных групп
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrExamGroupSetGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.settings.entity.EnrExamGroupSet";
    public static final String ENTITY_NAME = "enrExamGroupSet";
    public static final int VERSION_HASH = 1638258811;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_END_DATE = "endDate";
    public static final String P_SIZE = "size";
    public static final String P_OPEN = "open";
    public static final String P_PERIOD_TITLE = "periodTitle";

    private EnrEnrollmentCampaign _enrollmentCampaign;     // ПК
    private Date _beginDate;     // Дата с
    private Date _endDate;     // Дата по
    private int _size;     // Число мест в ЭГ (по умолчанию)
    private boolean _open;     // Открыт

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return ПК. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign ПК. Свойство не может быть null и должно быть уникальным.
     */
    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Дата с. Свойство не может быть null.
     */
    @NotNull
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата с. Свойство не может быть null.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Дата по. Свойство не может быть null.
     */
    @NotNull
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата по. Свойство не может быть null.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Число мест в ЭГ (по умолчанию). Свойство не может быть null.
     */
    @NotNull
    public int getSize()
    {
        return _size;
    }

    /**
     * @param size Число мест в ЭГ (по умолчанию). Свойство не может быть null.
     */
    public void setSize(int size)
    {
        dirty(_size, size);
        _size = size;
    }

    /**
     * @return Открыт. Свойство не может быть null.
     */
    @NotNull
    public boolean isOpen()
    {
        return _open;
    }

    /**
     * @param open Открыт. Свойство не может быть null.
     */
    public void setOpen(boolean open)
    {
        dirty(_open, open);
        _open = open;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrExamGroupSetGen)
        {
            setEnrollmentCampaign(((EnrExamGroupSet)another).getEnrollmentCampaign());
            setBeginDate(((EnrExamGroupSet)another).getBeginDate());
            setEndDate(((EnrExamGroupSet)another).getEndDate());
            setSize(((EnrExamGroupSet)another).getSize());
            setOpen(((EnrExamGroupSet)another).isOpen());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrExamGroupSetGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrExamGroupSet.class;
        }

        public T newInstance()
        {
            return (T) new EnrExamGroupSet();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "beginDate":
                    return obj.getBeginDate();
                case "endDate":
                    return obj.getEndDate();
                case "size":
                    return obj.getSize();
                case "open":
                    return obj.isOpen();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrEnrollmentCampaign) value);
                    return;
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "size":
                    obj.setSize((Integer) value);
                    return;
                case "open":
                    obj.setOpen((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "beginDate":
                        return true;
                case "endDate":
                        return true;
                case "size":
                        return true;
                case "open":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "beginDate":
                    return true;
                case "endDate":
                    return true;
                case "size":
                    return true;
                case "open":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrollmentCampaign":
                    return EnrEnrollmentCampaign.class;
                case "beginDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "size":
                    return Integer.class;
                case "open":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrExamGroupSet> _dslPath = new Path<EnrExamGroupSet>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrExamGroupSet");
    }
            

    /**
     * @return ПК. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.settings.entity.EnrExamGroupSet#getEnrollmentCampaign()
     */
    public static EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Дата с. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrExamGroupSet#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Дата по. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrExamGroupSet#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Число мест в ЭГ (по умолчанию). Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrExamGroupSet#getSize()
     */
    public static PropertyPath<Integer> size()
    {
        return _dslPath.size();
    }

    /**
     * @return Открыт. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrExamGroupSet#isOpen()
     */
    public static PropertyPath<Boolean> open()
    {
        return _dslPath.open();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.settings.entity.EnrExamGroupSet#getPeriodTitle()
     */
    public static SupportedPropertyPath<String> periodTitle()
    {
        return _dslPath.periodTitle();
    }

    public static class Path<E extends EnrExamGroupSet> extends EntityPath<E>
    {
        private EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Date> _endDate;
        private PropertyPath<Integer> _size;
        private PropertyPath<Boolean> _open;
        private SupportedPropertyPath<String> _periodTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return ПК. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.settings.entity.EnrExamGroupSet#getEnrollmentCampaign()
     */
        public EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Дата с. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrExamGroupSet#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(EnrExamGroupSetGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Дата по. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrExamGroupSet#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(EnrExamGroupSetGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Число мест в ЭГ (по умолчанию). Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrExamGroupSet#getSize()
     */
        public PropertyPath<Integer> size()
        {
            if(_size == null )
                _size = new PropertyPath<Integer>(EnrExamGroupSetGen.P_SIZE, this);
            return _size;
        }

    /**
     * @return Открыт. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrExamGroupSet#isOpen()
     */
        public PropertyPath<Boolean> open()
        {
            if(_open == null )
                _open = new PropertyPath<Boolean>(EnrExamGroupSetGen.P_OPEN, this);
            return _open;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.settings.entity.EnrExamGroupSet#getPeriodTitle()
     */
        public SupportedPropertyPath<String> periodTitle()
        {
            if(_periodTitle == null )
                _periodTitle = new SupportedPropertyPath<String>(EnrExamGroupSetGen.P_PERIOD_TITLE, this);
            return _periodTitle;
        }

        public Class getEntityClass()
        {
            return EnrExamGroupSet.class;
        }

        public String getEntityName()
        {
            return "enrExamGroupSet";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getPeriodTitle();
}
