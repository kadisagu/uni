package ru.tandemservice.unienr14.rating.entity;

import org.tandemframework.core.view.formatter.DoubleFormatter;
import ru.tandemservice.unienr14.rating.entity.gen.*;

/**
 * Основание балла (балл по апелляции ДДС) (2013)
 *
 * Содзается и обновляется демоном на основе ДДС абитуриента IEnrRatingDaemonDao#doRefreshSource4Appeal (создание оснований по апелляциям ДДС абитуриента).
 */
public class EnrEntrantMarkSourceAppeal extends EnrEntrantMarkSourceAppealGen
{
    public EnrEntrantMarkSourceAppeal() {}

    public EnrEntrantMarkSourceAppeal(EnrChosenEntranceExamForm chosenEntranceExamForm, EnrExamPassAppeal examPassAppeal) {
        this.setChosenEntranceExamForm(chosenEntranceExamForm);
        this.setExamPassAppeal(examPassAppeal);
    }

    public EnrEntrantMarkSourceAppeal(EnrChosenEntranceExamForm chosenEntranceExamForm, EnrExamPassAppeal examPassAppeal, long markAsLong) {
        this(chosenEntranceExamForm, examPassAppeal);
        this.setMarkAsLong(markAsLong);
    }

    @Override
    public String getInfoString()
    {
        EnrExamPassDiscipline examPassDiscipline = getExamPassAppeal().getExamPassDiscipline();
        return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(getExamPassAppeal().getMarkAsDouble()) + " — " + examPassDiscipline.getPassForm().getFullTitle() + ", апелляция";
    }

    @Override
    public String getDocumentTitleForPrint()
    {
        return ""; // todo DEV-2931
    }
}