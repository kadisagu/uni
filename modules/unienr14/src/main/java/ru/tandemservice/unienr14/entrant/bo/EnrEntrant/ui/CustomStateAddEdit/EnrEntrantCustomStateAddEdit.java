/* $Id: EcEntrantCustomStateAddEdit.java 30177 2013-09-17 12:14:36Z oleyba $ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.CustomStateAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantCustomStateType;

/**
 * @author nvankov
 * @since 4/3/13
 */
@Configuration
public class EnrEntrantCustomStateAddEdit extends BusinessComponentManager
{
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS("customStateDS", entrantCustomStateCIComboDS()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler entrantCustomStateCIComboDS()
    {
        return new DefaultComboDataSourceHandler(getName(), EnrEntrantCustomStateType.class);
    }
}
