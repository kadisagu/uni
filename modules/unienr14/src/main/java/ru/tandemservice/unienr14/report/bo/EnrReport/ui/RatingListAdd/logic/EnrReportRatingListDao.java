/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.RatingListAdd.logic;

import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfText;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes;
import ru.tandemservice.uniedu.program.entity.EduProgram;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantAchievementKind;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantStateCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrRequestTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.*;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.EnrExamSetManager;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.logic.IEnrExamSetDao;
import ru.tandemservice.unienr14.order.entity.EnrCancelExtract;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;
import ru.tandemservice.unienr14.rating.entity.gen.EnrRatingItemGen;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.CompetitionListAdd.EnrReportCompetitionListAddUI;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.RatingListAdd.EnrReportRatingListAddUI;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportUtil;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder;
import ru.tandemservice.unienr14.report.entity.EnrReportCompetitionList;
import ru.tandemservice.unienr14.report.entity.EnrReportRatingList;
import ru.tandemservice.unienr14.request.entity.*;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignTargetAdmissionKind;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import ru.tandemservice.unienr14.settings.entity.IEnrExamSetElementValue;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.EnrEnrollmentStepDao.EXTRACT_STATUS_OK;

/**
 * @author oleyba
 * @since 5/20/14
 */
public class EnrReportRatingListDao extends UniBaseDao implements IEnrReportRatingListDao
{
    public long curtimemil;
    protected static final String T1 = "T1";
    protected static final String T2 = "T2";
    protected static final String T3 = "T3";
    protected static final String T4 = "T4";
    protected static final String T5 = "T5";
    protected static final String T6 = "T6";
    protected static final String T7 = "T7";

    @Override
    public Long createReport(EnrReportRatingListAddUI model)
    {
        EnrReportRatingList report = new EnrReportRatingList();
        report.setFormingDate(new Date());

        curtimemil = System.currentTimeMillis();
        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateFrom());
        report.setDateTo(model.getDateTo());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, EnrReportRatingList.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE, EnrReportRatingList.P_COMPENSATION_TYPE, "shortTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, EnrReportRatingList.P_PROGRAM_FORM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPETITION_TYPE, EnrReportRatingList.P_COMPETITION_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, EnrReportRatingList.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, EnrReportRatingList.P_FORMATIVE_ORG_UNIT, "fullTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, EnrReportRatingList.P_PROGRAM_SUBJECT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, EnrReportRatingList.P_EDU_PROGRAM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, EnrReportRatingList.P_PROGRAM_SET, "title");
        if (model.isParallelActive()) {
            report.setParallel(model.getParallel().getTitle());
        }

        if (model.isOrigEduDocumentActive())
            report.setOrigEduDocument(model.getOrigEduDocument().getTitle());
        if (model.isConsentEnrollmentActive())
            report.setConsentEnrollment(model.getConsentEnrollment().getTitle());
        if (model.isFinalAgreementEnrollmentActive())
            report.setFinalAgreementEnrollment(model.getFinalAgreementEnrollment().getTitle());


        DatabaseFile content = new DatabaseFile();
        content.setContent(buildReport(model, EnrScriptItemCodes.REPORT_RATING_LIST));
        content.setFilename("EnrReportRatingList.rtf");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        save(content);

        report.setContent(content);
        save(report);

        return report.getId();
    }


    @Override
    public Long createCompetitionListReport(EnrReportCompetitionListAddUI model)
    {
        EnrReportCompetitionList report = new EnrReportCompetitionList();
        report.setFormingDate(new Date());

        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateFrom());
        report.setDateTo(model.getDateTo());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, EnrReportRatingList.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE, EnrReportRatingList.P_COMPENSATION_TYPE, "shortTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, EnrReportRatingList.P_PROGRAM_FORM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPETITION_TYPE, EnrReportRatingList.P_COMPETITION_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, EnrReportRatingList.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, EnrReportRatingList.P_FORMATIVE_ORG_UNIT, "fullTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, EnrReportRatingList.P_PROGRAM_SUBJECT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, EnrReportRatingList.P_EDU_PROGRAM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, EnrReportRatingList.P_PROGRAM_SET, "title");
        if (model.isParallelActive()) {
            report.setParallel(model.getParallel().getTitle());
        }

        if (model.isOrigEduDocumentActive())
            report.setOrigEduDocument(model.getOrigEduDocument().getTitle());
        if (model.isConsentEnrollmentActive())
            report.setConsentEnrollment(model.getConsentEnrollment().getTitle());
        if (model.isFinalAgreementEnrollmentActive())
            report.setFinalAgreementEnrollment(model.getFinalAgreementEnrollment().getTitle());

        DatabaseFile content = new DatabaseFile();
        content.setContent(buildReport(model, EnrScriptItemCodes.REPORT_COMPETITION_LIST, true, true));
        content.setFilename("EnrReportCompetitionList.rtf");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        save(content);

        report.setContent(content);
        save(report);

        return report.getId();
    }

    @Override
    public byte[] buildReport(EnrReportRatingListAddUI model, String templateCatalogCode)
    {
        return buildReport(model, templateCatalogCode, false, false);
    }

    protected byte[] buildReport(final EnrReportRatingListAddUI model, String templateCatalogCode, boolean competitionReportForm, boolean reportHasEntrantStateFilter)
    {

        //rtf
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(EnrScriptItem.class, templateCatalogCode);
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();

        // Выбираем абитуриентов, подходящих под параметры отчета

        final Map<EnrCompetition, List<EnrRatingItem>> entrantMap = new HashMap<>();

        EnrEntrantDQLSelectBuilder requestedCompDQL = prepareEntrantDQL(model, filterAddon, true, reportHasEntrantStateFilter, competitionReportForm);

        requestedCompDQL.order(property(EnrRatingItem.position().fromAlias(requestedCompDQL.rating())));
        requestedCompDQL.column(requestedCompDQL.rating());

        Set<Long> entrantsIds = Sets.newHashSet();

        Set<EnrProgramSetOrgUnit> nonEmptyBlocks = new HashSet<>();
        for (EnrRatingItem ratingItem : requestedCompDQL.createStatement(getSession()).<EnrRatingItem>list()) {
            SafeMap.safeGet(entrantMap, ratingItem.getCompetition(), ArrayList.class).add(ratingItem);
            nonEmptyBlocks.add(ratingItem.getCompetition().getProgramSetOrgUnit());
            if(competitionReportForm)
                entrantsIds.add(ratingItem.getEntrant().getId());
        }

        //{entrant.id -> {requestedCompetition.id -> priority}}
        final Map<Long, Map<Long, Integer>> recommendedReqCompMap = Maps.newHashMap();


        if(competitionReportForm)
        {
            BatchUtils.execute(entrantsIds, 100, elements -> {
                DQLSelectBuilder rcSubBuilder = new DQLSelectBuilder().fromEntity(EnrRequestedCompetition.class, "rc");
                rcSubBuilder.column(property("rc", EnrRequestedCompetition.request().entrant().id()), "entrantId");
                rcSubBuilder.column(DQLFunctions.max(property("rc", EnrRequestedCompetition.priority())), "minPriority");
                rcSubBuilder.where(eq(property("rc", EnrRequestedCompetition.request().entrant().enrollmentCampaign().id()), value(model.getEnrollmentCampaign().getId())));
                rcSubBuilder.where(eq(property("rc", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.RECOMMENDED)));
                rcSubBuilder.where(eq(property("rc", EnrRequestedCompetition.competition().type().compensationType().code()), value(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET)));
                rcSubBuilder.where(in(property("rc", EnrRequestedCompetition.request().entrant().id()), elements));
                rcSubBuilder.group(property("rc", EnrRequestedCompetition.request().entrant().id()));


                DQLSelectBuilder rcBuilder = new DQLSelectBuilder().fromEntity(EnrRequestedCompetition.class, "c");
                rcBuilder.column(property("c", EnrRequestedCompetition.id()));
                rcBuilder.column(property("c", EnrRequestedCompetition.priority()));
                rcBuilder.column(property("c", EnrRequestedCompetition.request().entrant().id()));
                rcBuilder.joinPath(DQLJoinType.inner, EnrRequestedCompetition.request().entrant().fromAlias("c"), "entrant");
                rcBuilder.joinDataSource("c", DQLJoinType.inner, rcSubBuilder.buildQuery(), "ds", eq(property("ds.entrantId"), property("entrant", EnrEntrant.id())));
                rcBuilder.where(eq(property("ds.minPriority"), property("c", EnrRequestedCompetition.priority())));

                for(Object[] row : rcBuilder.createStatement(getSession()).<Object[]>list())
                {
                    Long competitionId = (Long) row[0];
                    Integer priority = (Integer) row[1];
                    Long entrantId = (Long) row[2];

                    recommendedReqCompMap.put(entrantId, Maps.<Long, Integer>newHashMap());
                    recommendedReqCompMap.get(entrantId).put(competitionId, priority);
                }
            });
        }


        // Еще нам нужны будут баллы по ВИ абитуриентов

        Map<EnrRequestedCompetition, Map<EnrExamVariant, EnrChosenEntranceExam>> entrantExamMap = SafeMap.get(HashMap.class);

        EnrEntrantDQLSelectBuilder examDQL = prepareEntrantDQL(model, filterAddon, false, reportHasEntrantStateFilter, competitionReportForm);

        examDQL.fromEntity(EnrChosenEntranceExam.class, "exam");
        examDQL.where(eq(property(EnrChosenEntranceExam.requestedCompetition().fromAlias("exam")), property(examDQL.reqComp())));
        examDQL.column("exam");
        for (EnrChosenEntranceExam exam : examDQL.createStatement(getSession()).<EnrChosenEntranceExam>list()) {
            if (exam.getActualExam() != null && exam.getMarkAsDouble() != null) {
                entrantExamMap.get(exam.getRequestedCompetition()).put(exam.getActualExam(), exam);
            }
        }

        Map<EnrRequestedCompetition, EnrOrder> orderMap = new HashMap<>();


        EnrEntrantDQLSelectBuilder orderDQL = prepareEntrantDQL(model, filterAddon, false, reportHasEntrantStateFilter, competitionReportForm);
        orderDQL.fromEntity(EnrEnrollmentExtract.class, "e");
        orderDQL.where(eq(property(EnrEnrollmentExtract.entity().fromAlias("e")), property(orderDQL.reqComp())));
        orderDQL.column(orderDQL.reqComp());
        orderDQL.column(property(EnrEnrollmentExtract.paragraph().order().fromAlias("e")));
        for (Object[] row : orderDQL.createStatement(getSession()).<Object[]>list()) {
            orderMap.put((EnrRequestedCompetition) row[0], (EnrOrder) row[1]);
        }

        Map<EnrRequestedCompetition, Set<EnrEntrantAchievement>> achievementMap = new HashMap<>();

        final Comparator<EnrEntrantAchievement> achievementComparator = (EnrEntrantAchievement o1, EnrEntrantAchievement o2) -> {
                Long mark1 = o1.getType().isMarked() ? o1.getMarkAsLong() : o1.getType().getAchievementMarkAsLong();
                Long mark2 = o2.getType().isMarked() ? o2.getMarkAsLong() : o2.getType().getAchievementMarkAsLong();
                int res = mark2.compareTo(mark1);
                if (res != 0) return res;
                return o1.getId().compareTo(o2.getId());
            };

        EnrEntrantDQLSelectBuilder achievementDQL = prepareEntrantDQL(model, filterAddon, false, reportHasEntrantStateFilter, competitionReportForm);


        achievementDQL.fromEntity(EnrEntrantAchievement.class, "achievement")
                .where(and(
                        eq(property("achievement", EnrEntrantAchievement.entrant()), property(achievementDQL.entrant())),
                        eq(property("achievement", EnrEntrantAchievement.type().achievementKind().requestType()), property(achievementDQL.request(), EnrEntrantRequest.type()))))
                .where(or(
                        eq(property("achievement", EnrEntrantAchievement.type().forRequest()), value(false)),
                        and(
                                eq(property("achievement", EnrEntrantAchievement.type().forRequest()), value(true)),
                                eq(property("achievement", EnrEntrantAchievement.request()), property(achievementDQL.request()))
                        )

                ))
                .column(property(achievementDQL.reqComp()))
                .column(property("achievement"))
                ;

        for (Object[] row : achievementDQL.createStatement(getSession()).<Object[]>list()) {
            final EnrEntrantAchievement achievement = (EnrEntrantAchievement) row[1];
            if (null != achievement)
                SafeMap.safeGet(achievementMap, (EnrRequestedCompetition)row[0], TreeSet.class, achievementComparator).add(achievement);
        }


        // После применения фильтров утили к общему массиву конкурсов мы получаем отфильтрованный массив конкурсов (не путать с массивом выбранных конкурсов, о котором уже было выше написано).

        Collection<EnrCompetition> competitions = filterAddon.getFilteredList();

        // Убираем конкурсы, которые никто не выбрал
        if (model.isSkipEmptyCompetition()) {
            competitions = entrantMap.keySet();
        }

        // Сортировка конкурсов (таблиц внутри списка) - по коду вида приема (т.е. вначале бюджетные потом внебюджетные: без ВИ КЦП, квота особых прав, и т.д.)
        List<EnrCompetition> competitionList = new ArrayList<>(competitions);
        Collections.sort(competitionList, new EntityComparator<>(new EntityOrder(EnrCompetition.type().code().s())));

        // Полученный массив конкурсов мы группируем по ключу: набор ОП, филиал, формирующее подразделение.
        Map<EnrProgramSetOrgUnit, List<EnrCompetition>> blockMap = new HashMap<>();
        for (EnrCompetition competition : competitionList) {
            // Убираем пустые таблицы
            if (model.isSkipEmptyList() && !nonEmptyBlocks.contains(competition.getProgramSetOrgUnit())) continue;
            SafeMap.safeGet(blockMap, competition.getProgramSetOrgUnit(), ArrayList.class).add(competition);
        }

        // Сортировка списков -
        // вначале по филиалу (вначале головная организация, потом остальные по названию),
        // затем по форме обучения (по коду),
        // затем по виду ОП,
        // затем по направлению подготовки,
        // затем по набору ОП (по названию)
        List<EnrProgramSetOrgUnit> blockList = new ArrayList<>();
        blockList.addAll(blockMap.keySet());
        Collections.sort(blockList, (o1, o2) -> {
            int result;
            result = - Boolean.compare(o1.getOrgUnit().isTop(), o2.getOrgUnit().isTop());
            if (0 == result) result = o1.getProgramSet().getProgramForm().getCode().compareTo(o2.getProgramSet().getProgramForm().getCode());
            if (0 == result) result = o1.getProgramSet().getProgramKind().getCode().compareTo(o2.getProgramSet().getProgramKind().getCode());
            if (0 == result) result = o1.getProgramSet().getProgramSubject().getTitleWithCode().compareTo(o2.getProgramSet().getProgramSubject().getTitleWithCode());
            if (0 == result) result = o1.getProgramSet().getTitle().compareTo(o2.getProgramSet().getTitle());
            return result;
        });

        // подгрузим наборы ВИ наших конкурсов

        Set<Long> examSetVariantIds = new HashSet<>();
        for (EnrCompetition competition : competitions) {
            examSetVariantIds.add(competition.getExamSetVariant().getId());
        }

        Map<Long, IEnrExamSetDao.IExamSetSettings> examSetContent = EnrExamSetManager.instance().dao().getExamSetContent(examSetVariantIds);

        // подгрузим образовательные программы наборов

        Collection<EnrProgramSetBase> programSets = CollectionUtils.collect(blockList, EnrProgramSetOrgUnit::getProgramSet);

        Map<EnrProgramSetBase, List<EduProgramProf>> programMap = SafeMap.get(ArrayList.class);
        for (EnrProgramSetItem item : getList(EnrProgramSetItem.class, EnrProgramSetItem.programSet(), programSets)) {
            programMap.get(item.getProgramSet()).add(item.getProgram());
        }
        for (EnrProgramSetBase programSetBase : programSets) {
            if (programSetBase instanceof EnrProgramSetSecondary) {
                programMap.get(programSetBase).add(((EnrProgramSetSecondary)programSetBase).getProgram());
            }
        }

        // определим, какие данные выводить, исходя из параметров отчета

        boolean printMinisterial = true;
        boolean printContract = true;
        CompensationType compensationType = (CompensationType) filterAddon.getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE).getValue();
        if (null != compensationType && filterAddon.getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE).isEnableCheckboxChecked()) {
            printMinisterial = compensationType.getCode().equals(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET);
            printContract = compensationType.getCode().equals(CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT);
        }

        // сразу заменим метку даты

        new RtfInjectModifier().put("dateFrom", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date())).modify(document);

        // берем из шаблона таблицы

        Map<String, RtfTable> headerTables = new HashMap<>();
        headerTables.put(EnrRequestTypeCodes.BS, UniRtfUtil.removeTable(document, "BS", true));
        headerTables.put(EnrRequestTypeCodes.MASTER, UniRtfUtil.removeTable(document, "MASTER", true));
        RtfTable higher = UniRtfUtil.removeTable(document, "HIGHER", true);
        headerTables.put(EnrRequestTypeCodes.HIGHER, higher);
        headerTables.put(EnrRequestTypeCodes.POSTGRADUATE, higher);
        RtfTable residency = UniRtfUtil.removeTable(document, "RESIDENCY", true);
        headerTables.put(EnrRequestTypeCodes.TRAINEESHIP, residency);
        headerTables.put(EnrRequestTypeCodes.INTERNSHIP, residency);
        headerTables.put(EnrRequestTypeCodes.SPO, UniRtfUtil.removeTable(document, "SPO", true));

        Map<String, RtfTable> contentTables = new HashMap<>();
        contentTables.put(T1, UniRtfUtil.removeTable(document, T1, false));
        contentTables.put(T2, UniRtfUtil.removeTable(document, T2, false));
        contentTables.put(T3, UniRtfUtil.removeTable(document, T3, false));
        contentTables.put(T4, UniRtfUtil.removeTable(document, T4, false));
        contentTables.put(T5, UniRtfUtil.removeTable(document, T5, false));
        contentTables.put(T6, UniRtfUtil.removeTable(document, T6, false));
        contentTables.put(T7, UniRtfUtil.removeTable(document, T7, false));

        // и чистим шаблон, чтобы не было ненужных переводов строк
        document.getElementList().clear();

        // выводим блоки отчета
        for (EnrProgramSetOrgUnit programSetOrgUnit : blockList) {

            // сначала выводим заголовочную таблицу блока

            List<EnrTargetAdmissionPlan> taPlans = getTaPlans(programSetOrgUnit);

            String tableCode = programSetOrgUnit.getProgramSet().getRequestType().getCode();
            if (EnrRequestTypeCodes.HIGHER.equals(tableCode))
                switch (programSetOrgUnit.getProgramSet().getProgramKind().getCode())
                {
                    case EduProgramKindCodes.PROGRAMMA_INTERNATURY : tableCode = EnrRequestTypeCodes.INTERNSHIP; break;
                    case EduProgramKindCodes.PROGRAMMA_ASPIRANTURY_ADYUNKTURY_: tableCode = EnrRequestTypeCodes.POSTGRADUATE; break;
                    case EduProgramKindCodes.PROGRAMMA_ORDINATURY: tableCode = EnrRequestTypeCodes.TRAINEESHIP; break;
                }
            RtfTable headerTable = headerTables.get(tableCode).getClone();
            document.getElementList().add(headerTable);

            List<EduProgramProf> programs = programMap.get(programSetOrgUnit.getProgramSet());
            boolean printOP = (model.isReplaceProgramSetTitle() && programs.size() == 1) || (programSetOrgUnit.getProgramSet() instanceof EnrProgramSetSecondary);
            boolean skipProgramsRow = (model.isSkipProgramSetTitle() || model.isReplaceProgramSetTitle()) && programs.size() == 1;
            boolean skipProgramSetRow = model.isSkipProgramSetTitle();
            if (skipProgramsRow && headerTable.getRowList().size() >= 6) {
                headerTable.getRowList().remove(5);
            }
            if (skipProgramSetRow && headerTable.getRowList().size() >= 4) {
                headerTable.getRowList().remove(3);
            }

            List<EnrCompetition> enrCompetitions = blockMap.get(programSetOrgUnit);

            boolean hasRecommendedOther = false;
            if (competitionReportForm)
            {
                for (EnrCompetition competition : enrCompetitions)
                {
                    if (competition.isTargetAdmission() && taPlans != null && taPlans.size() > 0)
                    {
                        // если прием по ЦП отдельно, делим списки по видам ЦП
                        for (EnrTargetAdmissionPlan taPlan : taPlans)
                        {
                            List<EnrRatingItem> entrantList = new ArrayList<>();
                            for (EnrRatingItem entrant : SafeMap.safeGet(entrantMap, competition, ArrayList.class))
                            {
                                if (entrant.getRequestedCompetition() instanceof EnrRequestedCompetitionTA && taPlan.getEnrCampaignTAKind().equals(((EnrRequestedCompetitionTA) entrant.getRequestedCompetition()).getTargetAdmissionKind()))
                                {
                                    entrantList.add(entrant);
                                }
                            }
                            if (model.isSkipEmptyCompetition() && entrantList.isEmpty())
                            {
                                continue;
                            }
                            for (EnrRatingItem entrant : entrantList)
                            {
                                Map<Long, Integer> recommendedReqComp = recommendedReqCompMap.get(entrant.getEntrant().getId());
                                if (CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(entrant.getCompetition().getType().getCompensationType().getCode()) && recommendedReqComp != null && recommendedReqComp.get(entrant.getRequestedCompetition().getId()) == null && recommendedReqComp.get(recommendedReqComp.keySet().iterator().next()) < entrant.getRequestedCompetition().getPriority())
                                {
                                    hasRecommendedOther = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        List<EnrRatingItem> entrantList = SafeMap.safeGet(entrantMap, competition, ArrayList.class);
                        if (entrantList != null)
                        {
                            for (EnrRatingItem entrant : entrantList)
                            {
                                Map<Long, Integer> recommendedReqComp = recommendedReqCompMap.get(entrant.getEntrant().getId());
                                if (CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(entrant.getCompetition().getType().getCompensationType().getCode()) && recommendedReqComp != null && recommendedReqComp.get(entrant.getRequestedCompetition().getId()) == null && recommendedReqComp.get(recommendedReqComp.keySet().iterator().next()) < entrant.getRequestedCompetition().getPriority())
                                {
                                    hasRecommendedOther = true;
                                }
                            }
                        }
                    }
                }
            }

            fillHeaderData(document, programSetOrgUnit, programs, printMinisterial, printContract, printOP, competitionReportForm, enrCompetitions,
                           getExamSetElements(enrCompetitions, examSetContent), getAchievementKinds(enrCompetitions, achievementMap), hasRecommendedOther, entrantMap, orderMap);

            // теперь списки по каждому конкурсу
            MutableInt ministerialRequestTotal = new MutableInt(0); MutableInt contractRequestTotal = new MutableInt(0);
            boolean competitionPrintForm = model instanceof EnrReportCompetitionListAddUI;
            for (EnrCompetition competition : enrCompetitions) {

                IEnrExamSetDao.IExamSetSettings examSet = examSetContent.get(competition.getExamSetVariant().getId());

                if (competition.isTargetAdmission() && taPlans != null && taPlans.size() > 0) {
                    // если прием по ЦП отдельно, делим списки по видам ЦП
                    for (EnrTargetAdmissionPlan taPlan : taPlans) {
                        List<EnrRatingItem> entrantList = new ArrayList<>();
                        for (EnrRatingItem entrant : SafeMap.safeGet(entrantMap, competition, ArrayList.class)) {
                            if (entrant.getRequestedCompetition() instanceof EnrRequestedCompetitionTA && taPlan.getEnrCampaignTAKind().equals(((EnrRequestedCompetitionTA)entrant.getRequestedCompetition()).getTargetAdmissionKind())) {
                                entrantList.add(entrant);
                            }
                        }
                        if (model.isSkipEmptyCompetition() && entrantList.isEmpty()) {
                            continue;
                        }
                        final String tableHeader = getTableHeader(competition, taPlan, entrantList.size(), competitionReportForm);
                        printContentTable(competition, entrantList, document, tableHeader,
                            ministerialRequestTotal, contractRequestTotal,
                            contentTables, examSet, entrantExamMap, orderMap, achievementMap, model.isPrintEntrantNumberColumn(), model.isAddBenefitsToComment(),
                            recommendedReqCompMap, competitionReportForm, model.isPrintPriority(), competitionPrintForm);
                    }
                } else {
                    List<EnrRatingItem> entrantList = SafeMap.safeGet(entrantMap, competition, ArrayList.class);
                    final String tableHeader = getTableHeader(competition, null, entrantList.size(), competitionReportForm);
                    printContentTable(competition, entrantList, document, tableHeader,
                        ministerialRequestTotal, contractRequestTotal,
                        contentTables, examSet, entrantExamMap, orderMap, achievementMap, model.isPrintEntrantNumberColumn(), model.isAddBenefitsToComment(),
                        recommendedReqCompMap, competitionReportForm, model.isPrintPriority(), competitionPrintForm);
                }
            }

            // выводим в заголовочную таблицу информацию по числу поданных заявлений

            //numberOfRequest - число заявлений: сумма на бюджетные конкурсы (сумма по всем табличкам бюджетных конкурсов), сумма по конкурсам по договору; если отчет строится только по бюджету - строки по договору не будет, если отчет по договору - строки по бюджету не будет; если КЦП 0 (план на бюджет не задан) - строки по бюджету не будет, если по договору план 0 (не задан) - строки по договору не будет
            //Подано заявлений:
            //    на бюджет (КЦП) – 101
            //    на места с оплатой стоимости обучения – 49

            RtfString requestTotal = new RtfString();
            if (printMinisterial) {
                requestTotal.append("    на бюджет (КЦП) \u2013 ").append(String.valueOf(ministerialRequestTotal.intValue()));
            }
            if (printContract) {
                if (requestTotal.toList().size() != 0) requestTotal.par();
                requestTotal.append("    на места с оплатой стоимости обучения \u2013 ").append(String.valueOf(contractRequestTotal.intValue()));
            }
            new RtfInjectModifier().put("requestsTotal", requestTotal).modify(document);

            if (blockList.indexOf(programSetOrgUnit) != blockList.size() - 1) {
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
        
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
        
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
            }
        }


        return RtfUtil.toByteArray(document);
    }

    private List<IEnrExamSetElementValue> getExamSetElements(List<EnrCompetition> enrCompetitions, Map<Long, IEnrExamSetDao.IExamSetSettings> examSetContent)
    {
        List<IEnrExamSetElementValue> examSetElements = Lists.newArrayList();

        // только для конкурсов с ВИ
        Set<Long> examSetVariantsIds = CommonBaseEntityUtil.getPropertiesSet(enrCompetitions.stream()
                .filter(enrCompetition -> !EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL.equals(enrCompetition.getType().getCode())
                        && !EnrCompetitionTypeCodes.NO_EXAM_CONTRACT.equals(enrCompetition.getType().getCode()))
                .collect(Collectors.toList()), EnrCompetition.examSetVariant().id().s());

        examSetContent.entrySet().stream().filter(entry -> examSetVariantsIds.contains(entry.getKey())).forEach(entry ->
        {
            entry.getValue().getElementList().forEach(value ->
            {
                if(!examSetElements.contains(value.getElement().getValue()))
                    examSetElements.add(value.getElement().getValue());
            });
        });

        Collections.sort(examSetElements, new EntityComparator<>(new EntityOrder(IEnrExamSetElementValue.P_TITLE)));

        return examSetElements;
    }

    private List<EnrEntrantAchievementKind> getAchievementKinds(List<EnrCompetition> enrCompetitions, Map<EnrRequestedCompetition, Set<EnrEntrantAchievement>> achievementMap)
    {
        List<EnrEntrantAchievementKind> kindList = Lists.newArrayList();

        achievementMap.entrySet().stream()
                .filter(entry -> enrCompetitions.contains(entry.getKey().getCompetition()))
                .forEach(entry -> entry.getValue().forEach(e -> {
                    if(!kindList.contains(e.getType().getAchievementKind()))
                    {
                        kindList.add(e.getType().getAchievementKind());
                    }
                }));

        Collections.sort(kindList, new EntityComparator<>(new EntityOrder(EnrEntrantAchievementKind.P_TITLE)));
        return kindList;
    }

    private EnrEntrantDQLSelectBuilder prepareEntrantDQL(EnrReportRatingListAddUI model, EnrCompetitionFilterAddon filterAddon, boolean fetch, boolean reportHasEntrantStateFilter, boolean competitionReportForm)
    {

        EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder(fetch, true)
            .notArchiveOnly()
            .filter(model.getDateFrom(), model.getDateTo())
            .filter(model.getEnrollmentCampaign())
            ;

        if (!reportHasEntrantStateFilter) {
            requestedCompDQL.defaultActiveFilter();
        }

        if (competitionReportForm) {
            requestedCompDQL.where(eq(property(requestedCompDQL.rating(), EnrRatingItem.statusRatingPositive()), value(Boolean.TRUE)));
        }

        if (model instanceof EnrReportCompetitionListAddUI)
            requestedCompDQL.where(in(property(requestedCompDQL.reqComp(), EnrRequestedCompetition.state()), ((EnrReportCompetitionListAddUI) model).getReqCompStates()));


        filterAddon.applyFilters(requestedCompDQL, requestedCompDQL.competition());

        if (model.isParallelOnly()) {
            requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.TRUE)));
        }
        if (model.isSkipParallel()) {
            requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.FALSE)));
        }

        if (model.isOrigEduDocumentActive())
            requestedCompDQL.where(eq(property(EnrRequestedCompetition.originalDocumentHandedIn().fromAlias(requestedCompDQL.reqComp())), value(0L == model.getOrigEduDocument().getId())));

        if (model.isConsentEnrollmentActive())
            requestedCompDQL.where(eq(property(EnrRequestedCompetition.accepted().fromAlias(requestedCompDQL.reqComp())), value(0L == model.getConsentEnrollment().getId())));

        if (model.isFinalAgreementEnrollmentActive())
            requestedCompDQL.where(eq(property(EnrRequestedCompetition.enrollmentAvailable().fromAlias(requestedCompDQL.reqComp())), value(0L == model.getFinalAgreementEnrollment().getId())));


        return requestedCompDQL;
    }

    private List<EnrTargetAdmissionPlan> getTaPlans(EnrProgramSetOrgUnit programSetOrgUnit)
    {
        return programSetOrgUnit.getProgramSet().getEnrollmentCampaign().getSettings().isTargetAdmissionCompetition() ?
                    getList(EnrTargetAdmissionPlan.class, EnrTargetAdmissionPlan.programSetOrgUnit(), programSetOrgUnit, EnrTargetAdmissionPlan.enrCampaignTAKind().targetAdmissionKind().priority().s())
                    : null;
    }

    private String getTableHeader(EnrCompetition competition, EnrTargetAdmissionPlan taPlan, int size, boolean competitionReportForm)
    {
        StringBuilder header = new StringBuilder();
        header.append(competition.getType().getPrintTitle());
        if (null != taPlan) {
            header.append(" – ").append(taPlan.getEnrCampaignTAKind().getTitle());
        }

        header.append(" (заявлений \u2013 ").append(size);
        if(competitionReportForm && !EnrCompetitionTypeCodes.NO_EXAM_CONTRACT.equals(competition.getType().getCode()) && !EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL.equals(competition.getType().getCode()))
        {
            int plan;
            if (null != taPlan)
            {
                final EnrTargetAdmissionCompetitionPlan compPlan = getByNaturalId(new EnrTargetAdmissionCompetitionPlan.NaturalId(taPlan, competition));
                plan = compPlan.getPlan();
            }
            else
                plan = competition.getPlan();

            header.append(", число мест — ").append(plan);
        }
        header.append(")");
        return header.toString();
    }

    private void printContentTable(EnrCompetition competition, List<EnrRatingItem> entrantList, RtfDocument document, final String tableHeader,
                                   MutableInt ministerialRequestTotal, MutableInt contractRequestTotal,
                                   Map<String, RtfTable> contentTables,
                                   final IEnrExamSetDao.IExamSetSettings examSet,
                                   final Map<EnrRequestedCompetition, Map<EnrExamVariant, EnrChosenEntranceExam>> entrantExamMap,
                                   final Map<EnrRequestedCompetition, EnrOrder> orderMap,
                                   final Map<EnrRequestedCompetition, Set<EnrEntrantAchievement>> achievementMap,
                                   final boolean printEntrantNumber,
                                   final boolean addBenefits,
                                   Map<Long, Map<Long, Integer>> recommendedReqCompMap, final boolean competitionReportForm,
                                   final boolean printPriority, boolean competitionPrintForm)
    {
        final String tableName = chooseRtfTable(competition);
        RtfTable contentTable = contentTables.get(tableName).getClone();

        document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
        document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
        document.getElementList().add(contentTable);




        final List<String[]> tableContent = new ArrayList<>();
        int number = 1;
        if (entrantList != null) {
            for (EnrRatingItem entrant : entrantList) {
                List<Double> marks = new ArrayList<>();
                for (IEnrExamSetDao.IExamSetElementSettings exam : examSet.getElementList()) {
                    EnrChosenEntranceExam chosenEntranceExam = entrantExamMap.get(entrant.getRequestedCompetition()).get(exam.getCgExam());
                    marks.add(chosenEntranceExam == null ? null : chosenEntranceExam.getMarkAsDouble());
                }

                boolean printStarForState = false;
                if(competitionReportForm )
                {
                    Map<Long, Integer> recommendedReqComp = recommendedReqCompMap.get(entrant.getEntrant().getId());
                    if(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(entrant.getCompetition().getType().getCompensationType().getCode()) && recommendedReqComp != null && recommendedReqComp.get(entrant.getRequestedCompetition().getId()) == null && recommendedReqComp.get(recommendedReqComp.keySet().iterator().next()) < entrant.getRequestedCompetition().getPriority())
                    {
                        printStarForState = true;
                    }
                }


                tableContent.add(printEntrant(number++, entrant, tableName, marks, achievementMap.get(entrant.getRequestedCompetition()), orderMap.get(entrant.getRequestedCompetition()),
                                              printEntrantNumber, addBenefits, competitionReportForm, printStarForState, printPriority, competitionPrintForm));

                if (CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(entrant.getCompetition().getType().getCompensationType().getCode())) {
                    ministerialRequestTotal.increment();
                }
                if (CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT.equals(entrant.getCompetition().getType().getCompensationType().getCode())) {
                    contractRequestTotal.increment();
                }
            }
        }

        if(tableContent.isEmpty())
            tableContent.add(new String[1]);

        modifiTableModifier(document, tableHeader, examSet, printEntrantNumber, printPriority, tableName, tableContent, competitionPrintForm, competition);
    }

    protected void modifiTableModifier(RtfDocument document, final String tableHeader, final IEnrExamSetDao.IExamSetSettings examSet, final boolean printEntrantNumber,
                                       final boolean printPriority, final String tableName, List<String[]> tableContent, boolean competitionPrintForm, EnrCompetition competition)
    {
        new RtfTableModifier()
            .put(tableName, tableContent.toArray(new String[tableContent.size()][]))
            .put(tableName, new RtfRowIntercepterBase() {

                @Override
                public void beforeModify(RtfTable table, int currentRowIndex)
                {
                    if (!printPriority)
                    {
                        int column = 4;
                        switch (tableName)
                        {
                            case T2:
                            case T4:
                                column=5;
                                break;
                            case T6:
                            case T7:
                                column=6;
                                break;
                        }
                        RtfUtil.deleteCellPrevIncrease(table.getRowList().get(currentRowIndex), column); //ячейка в строке с меткой T
                        RtfUtil.deleteCellPrevIncrease(table.getRowList().get(currentRowIndex - 1), column); //ячейка в шапке
                        if (T2.equals(tableName) || T4.equals(tableName) || T6.equals(tableName) || T7.equals(tableName))
                            RtfUtil.deleteCellPrevIncrease(table.getRowList().get(currentRowIndex - 2), column);
                    }

                    if (examSet.getElementList().isEmpty()) return;
                    if (!T2.equals(tableName) && !T4.equals(tableName) && !T6.equals(tableName) && !T7.equals(tableName)) return;

                    final int[] scales = new int[examSet.getElementList().size()];
                    Arrays.fill(scales, 1);

                    // разбиваем ячейку заголовка таблицы для названий оценок на нужное число элементов
                    RtfUtil.splitRow(table.getRowList().get(currentRowIndex - 1), printEntrantNumber ? 4:3, (newCell, index) -> {
                        String content = examSet.getElementList().get(index).getElement().getShortTitle();
                        newCell.getElementList().addAll(new RtfString().append(IRtfData.QC).append(content).toList());
                    }, scales);
                    RtfUtil.splitRow(table.getRowList().get(currentRowIndex), printEntrantNumber ? 4: 3, null, scales);
                }
                // todo сделать утильный класс
                @Override public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value) {
                    List<IRtfElement> list = new ArrayList<>();
                    IRtfText text = RtfBean.getElementFactory().createRtfText(value);
                    text.setRaw(true);
                    list.add(text);
                    return list;
                }
                @Override public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex) {
                    newRowList.get(0).getCellList().get(0).addElements(new RtfString().boldBegin().append(tableHeader).boldEnd().toList());
                }
            })
            .modify(document);
    }

    protected String[] printEntrant(int number, EnrRatingItem ratingItem, String tableName, List<Double> marks, Set<EnrEntrantAchievement> achievements,
                                    EnrOrder enrOrder, boolean printEntrantNumber, boolean addBenefits, final boolean competitionReportForm,
                                    boolean printStartForState, boolean printPriority, boolean competitionPrintForm)
    {

        EnrRequestedCompetition reqComp = ratingItem.getRequestedCompetition();

        final EnrEntrantRequest request = reqComp.getRequest();
        final EnrEntrant entrant = request.getEntrant();


        List<String> row = new ArrayList<>();
        // номер
        row.add(String.valueOf(number));
        // Личный номер абитуриента
        if (printEntrantNumber)
            row.add(entrant.getPersonalNumber());
        // фио
        row.add(entrant.getFullFio());
        // категория
        if (T1.equals(tableName) || T3.equals(tableName)) {
            if (reqComp instanceof IEnrEntrantBenefitStatement) {
                row.add(((IEnrEntrantBenefitStatement)reqComp).getBenefitCategory().getShortTitle());
            } else row.add("");
        }
        // ср. балл
        if (T5.equals(tableName)) {
            Double avgMark = request.getEduDocument().getAvgMarkAsDouble();
            row.add(avgMark == null ? "\u2013" : new DoubleFormatter(3, true).format(avgMark));
        }
        // сумма и результаты
        if (T2.equals(tableName) || T4.equals(tableName) || T6.equals(tableName) || T7.equals(tableName)) {
            row.add(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(ratingItem.getTotalMarkAsDouble()));
            for (Double mark : marks) {
                row.add(mark == null ? "" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(mark));
            }
            if (marks.isEmpty()) row.add(""); // иначе таблица поедет
        }

        if (T6.equals(tableName) || T7.equals(tableName)) {
            Double avgMark = reqComp.getRequest().getEduDocument().getAvgMarkAsDouble();
            row.add(avgMark == null ? "\u2013" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(avgMark));
        }

        // индивидуальные достижения
        NumberFormat formatter = new DecimalFormat("#0.##");
        row.add(formatter.format(ratingItem.getAchievementMarkAsLong()/1000.0));

        // приоритет
        if (printPriority)
            row.add(String.valueOf(reqComp.getPriority()));

        // оригинал
        row.add(YesNoFormatter.INSTANCE.format(reqComp.isOriginalDocumentHandedIn()));

        // согласие
        if (T1.equals(tableName) || T2.equals(tableName) || T3.equals(tableName) || T4.equals(tableName) || T6.equals(tableName) || T7.equals(tableName))
        {
            row.add(YesNoFormatter.INSTANCE.format(reqComp.isAccepted()));
        }

        // примечание
        StringBuilder info = new StringBuilder();
        if (reqComp instanceof EnrRequestedCompetitionNoExams) {
            info.append("Выбранная ОП: ").append(((EnrRequestedCompetitionNoExams) reqComp).getProgramSetItem().getProgram().getShortTitle());
        }
        if (addBenefits && reqComp.getCompetition().getType().getCode().equals(EnrCompetitionTypeCodes.EXCLUSIVE) && reqComp instanceof IEnrEntrantBenefitStatement )
        {
            if (info.length() > 0) info.append("\\par ");
            info.append("Особое право: ").append(((IEnrEntrantBenefitStatement) reqComp).getBenefitCategory().getShortTitle());
        }
        if (request.getBenefitCategory() != null) {
            if (info.length() > 0) info.append("\\par ");
            info.append("Преимущественное право: ").append(request.getBenefitCategory().getShortTitle());
        }
        int n = 0;
        if (achievements != null && achievements.size() > 0) {
            if (info.length() > 0) info.append("\\par ");
            info.append("Индивидуальные достижения:");
            for (EnrEntrantAchievement achievement : achievements) {
                info.append("\\par ");
                info.append(++n).append(". ").append(achievement.getType().getAchievementKind().getShortTitle()).append(" — ")
                        .append(achievement.getType().isMarked() ? formatter.format(achievement.getRatingMarkAsLong()/1000.0) : formatter.format(achievement.getType().getAchievementMarkAsLong()/1000.0));
            }
        }


        row.add(info.toString());
        // Информация о зачислении
        if (competitionReportForm ) {
            if(printStartForState)
            {
                row.add("*");
            }
            else
            {
                if (reqComp.getState() != null)
                {
                    String stateCode = reqComp.getState().getCode();
                    switch (stateCode)
                    {
                        case EnrEntrantStateCodes.ENROLLED:
                            String enrollInfo = UniStringUtils.joinWithSeparator(", ", enrOrder == null ? null : "приказ " + enrOrder.getDateAndNumber(), reqComp.getCompetition().isTargetAdmission() ? ((EnrRequestedCompetitionTA) reqComp).getTargetAdmissionKind().getTitle() : null);
                            row.add(enrollInfo == null ? reqComp.getState().getTitle() : reqComp.getState().getTitle() + " (" + enrollInfo + ")");
                            break;
                        case EnrEntrantStateCodes.IN_ORDER:
                            row.add("Выбран к зачислению");
                            break;
                        case EnrEntrantStateCodes.RECOMMENDED:
                        case EnrEntrantStateCodes.SELECT_SHOULD_BE_ENROLLED:
                        case EnrEntrantStateCodes.EXCLUDED_BY_EXTRACT:
                        case EnrEntrantStateCodes.EXCLUDED_BY_REJECT:
                        case EnrEntrantStateCodes.TAKE_DOCUMENTS_AWAY:
                            row.add(reqComp.getState().getTitle());
                            break;
                        default:
                            row.add("");
                            break;
                    }
                }
            }
        }
        fillAdditionalCells(row, reqComp, achievements, tableName, printPriority, competitionPrintForm);
        return row.toArray(new String[row.size()]);
    }

    private void fillHeaderData(RtfDocument document, EnrProgramSetOrgUnit programSetOrgUnit, List<EduProgramProf> programs, boolean printMinisterial, boolean printContract,
                                boolean printOP, boolean competitionReportForm, List<EnrCompetition> enrCompetitions, List<IEnrExamSetElementValue> examSetElements,
                                List<EnrEntrantAchievementKind> achievementKinds, boolean hasRecommendedOther, Map<EnrCompetition, List<EnrRatingItem>> entrantMap,
                                Map<EnrRequestedCompetition, EnrOrder> orderMap)
    {
        Collections.sort(programs, (o1, o2) -> o1.getTitleAndConditionsShort().compareTo(o2.getTitleAndConditionsShort()));

        String duration = null; boolean diffDuration = false;
        for (EduProgram program : programs) {
            if (null == duration) {
                duration = program.getDuration().getTitle();
            } else {
                diffDuration = diffDuration || !program.getDuration().getTitle().equals(duration);
            }
        }
        String programConditions = null; boolean diffConditions = false;
        for (EduProgramProf program : programs) {
            if (null == programConditions) {
                programConditions = StringUtils.trimToEmpty(program.getImplConditionsShort());
            } else {
                diffConditions = diffConditions || !StringUtils.trimToEmpty(program.getImplConditionsShort()).equals(programConditions);
            }
        }

        OrgUnit filial = programSetOrgUnit.getOrgUnit().getInstitutionOrgUnit().getOrgUnit();

        RtfInjectModifier modifier = new RtfInjectModifier();
        modifier
            .put("filial", filial.getPrintTitle())
            .put("formOrgUnit", programSetOrgUnit.getFormativeOrgUnit() == null || programSetOrgUnit.getFormativeOrgUnit().equals(filial) ? "" : programSetOrgUnit.getFormativeOrgUnit().getPrintTitle())
            .put("eduSubjectKind", programSetOrgUnit.getProgramSet().getProgramKind().getEduProgramSubjectKindTitle(InflectorVariantCodes.RU_NOMINATIVE))
            .put("eduSubject", programSetOrgUnit.getProgramSet().getProgramSubject().getTitleWithCode())
            .put("programForm", programSetOrgUnit.getProgramSet().getProgramForm().getTitle())
            .put("duration", diffDuration ? "" : duration)
            .put("programTrait", diffConditions? "" : programConditions)
            .put("eduProgramKind", programSetOrgUnit.getProgramSet().getProgramKind().getShortTitle())
            .put("programSetOrProgram", printOP ? "Образовательная программа" : "Набор ОП")
            .put("programSet", printOP ? programs.get(0).getTitleAndConditionsShort() : programSetOrgUnit.getProgramSet().getPrintTitle())
            ;

        RtfString eduProgramTitles = new RtfString();
        for (EduProgramProf program : programs) {
            if (eduProgramTitles.toList().size() > 0) eduProgramTitles.par();
            eduProgramTitles.append(program.getShortTitle() + " \u2013 " + program.getTitleAndConditionsShort());
        }
        modifier.put("eduProgram", eduProgramTitles);

        if(!achievementKinds.isEmpty())
        {
            RtfString rtfString = new RtfString().append(":").par();
            for(EnrEntrantAchievementKind achievementKind : achievementKinds)
            {
                rtfString.boldBegin().append(achievementKind.getShortTitle()).boldEnd().append(" \u2013 " + StringUtils.trimToEmpty(achievementKind.getTitle()));
                if(achievementKinds.indexOf(achievementKind) != achievementKinds.size() - 1)
                {
                    rtfString.append("; ");
                }
            }

            modifier.put("IndividualAchievement", rtfString);
        }
        else
        {
            modifier.put("IndividualAchievement", "");
        }

        if(!examSetElements.isEmpty())
        {
            RtfString rtfString = new RtfString().append(":").par();
            for(IEnrExamSetElementValue element : examSetElements)
            {
                rtfString.boldBegin().append(element.getShortTitle()).boldEnd().append(" \u2013 " + element.getTitle());
                if(examSetElements.indexOf(element) != examSetElements.size() - 1)
                {
                    rtfString.append("; ");
                }
            }

            modifier.put("EntranceTests", rtfString);
        }
        else
        {
            modifier.put("EntranceTests", "");
        }

        //numberOfPlaces - число мест для приема (для комбинации набор ОП, филиал); не печатаем те строчки, в которых план 0 (не задан); если план с квотой ЦП бьется на подквоты (более чем на одну), то выводить детализацию по видам ЦП; например, если есть КЦП и нет квоты особых прав и ЦП, то будет строка "Число мест на бюджет (КЦП) - 50"; если отчет строится только по бюджету - строки с планом по договору не будет, если отчет по договору - строк по бюджету не будет (включая расшифровку по квотам и видам ЦП)
        //Число мест на бюджет (КЦП) – 50, из них:
        //       квота лиц с особыми правами – 5
        //       квота целевого приема – 10
        //(из них по видам целевого приема: Южный фед. округ – 2, ФГУП Спецмнотаж – 3, МО Курганской обл. – 1, Тюменский район – 3)
        //Число мест с оплатой стоимости обучения – 100


        boolean printMinisterialPlan = printMinisterial && programSetOrgUnit.getMinisterialPlan() > 0;
        boolean printExclusivePlan = EnrRequestTypeCodes.BS.equals(programSetOrgUnit.getProgramSet().getRequestType().getCode());
        boolean printTaPlan = programSetOrgUnit.getTargetAdmPlan() > 0;
        boolean printContractPlan = printContract && programSetOrgUnit.getContractPlan() > 0;

        List<String> planLines = new ArrayList<>();
        if (printMinisterialPlan)
        {
            StringBuilder minPlan = new StringBuilder();
            minPlan.append("Число мест на бюджет (КЦП) \u2013 ").append(String.valueOf(programSetOrgUnit.getMinisterialPlan()));

            Map<EnrCompetition, List<EnrRequestedCompetition>> mapEnrolled = getEnrolledCompetitionMap(enrCompetitions, entrantMap);
            int countEnrolled = getSum(mapEnrolled.values().stream().map(List::size).collect(Collectors.toList()));
            if (countEnrolled > 0)
            {
                minPlan.append(", из них:");
                planLines.add(minPlan.toString());

                Collection<EnrCompetition> ministerCompetitions = enrCompetitions.stream()
                        .filter(input -> EnrCompetitionTypeCodes.MINISTERIAL.equals(input.getType().getCode())).collect(Collectors.toList());

                int plan = getSum(CommonBaseEntityUtil.<Integer>getPropertiesList(ministerCompetitions, EnrCompetition.plan()));
                String ministerPlan = "         общий конкурс \u2013 " + String.valueOf(plan);
                planLines.add(ministerPlan);

                String enrolled = "         зачислено \u2013 " + String.valueOf(countEnrolled);
                planLines.add(enrolled);

                for (Map.Entry<EnrCompetition, List<EnrRequestedCompetition>> entry : mapEnrolled.entrySet())
                {
                    String planEnrolled = "";
                    switch (entry.getKey().getType().getCode())
                    {
                        case EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL:
                            planEnrolled = "                без вступительных испытаний \u2013 " + String.valueOf(entry.getValue().size());
                            planEnrolled += getEnrolledOrdersString(entry.getValue(), orderMap);
                            planLines.add(planEnrolled);
                            break;
                        case EnrCompetitionTypeCodes.EXCLUSIVE:
                            planEnrolled = "                квота лиц с особыми правами \u2013 " + String.valueOf(entry.getValue().size());
                            planEnrolled += getEnrolledOrdersString(entry.getValue(), orderMap);
                            planLines.add(planEnrolled);
                            break;
                        case EnrCompetitionTypeCodes.TARGET_ADMISSION:
                            StringBuilder builder = new StringBuilder("                квота целевого приема \u2013 ").append(String.valueOf(entry.getValue().size()));
                            Map<EnrCampaignTargetAdmissionKind, List<EnrRequestedCompetitionTA>> mapReqCompTAKind = new TreeMap<>(new EntityComparator<>(new EntityOrder(EnrCampaignTargetAdmissionKind.targetAdmissionKind().priority())));
                            entry.getValue().stream().map(e->(EnrRequestedCompetitionTA)e)
                                    .forEach(e -> SafeMap.safeGet(mapReqCompTAKind, e.getTargetAdmissionKind(), ArrayList.class).add(e));
                            if (mapReqCompTAKind.size() > 0)
                            {
                                builder.append(", из них по видам целевого приема: ");
                                boolean first = true;
                                for (Map.Entry<EnrCampaignTargetAdmissionKind, List<EnrRequestedCompetitionTA>> kindTA : mapReqCompTAKind.entrySet())
                                {
                                    if (kindTA.getValue().size() > 0)
                                    {
                                        if (!first)
                                            builder.append(", ");
                                        first = false;
                                        builder.append(kindTA.getKey().getTitle()).append(" \u2013 ").append(kindTA.getValue().size());
                                    }
                                }
                            }
                            builder.append(getEnrolledOrdersString(entry.getValue(), orderMap));
                            planLines.add(builder.toString());
                    }
                }
            }
            else
            {
                int plan=0;
                if (printExclusivePlan)
                {
                    if (competitionReportForm)
                    {
                        Collection<EnrCompetition> exclusiveCompetitions = Collections2.filter(enrCompetitions, input -> EnrCompetitionTypeCodes.EXCLUSIVE.equals(input.getType().getCode()));

                        plan = getSum(CommonBaseEntityUtil.<Integer>getPropertiesList(exclusiveCompetitions, EnrCompetition.plan()));
                    }
                    else
                        plan = programSetOrgUnit.getExclusivePlan();
                }

                if ((printExclusivePlan && plan >0) || printTaPlan)
                {
                    minPlan.append(", из них:");
                }
                planLines.add(minPlan.toString());

                if (printExclusivePlan && plan >0)
                    planLines.add("       квота лиц с особыми правами \u2013 " + String.valueOf(plan));

                if (printTaPlan)
                {
                    int planT;
                    if (competitionReportForm)
                    {
                        Collection<EnrCompetition> taCompetitions = Collections2.filter(enrCompetitions, input -> EnrCompetitionTypeCodes.TARGET_ADMISSION.equals(input.getType().getCode()));

                        planT = getSum(CommonBaseEntityUtil.<Integer>getPropertiesList(taCompetitions, EnrCompetition.plan()));
                    }
                    else
                        planT = programSetOrgUnit.getTargetAdmPlan();

                    if (planT > 0)
                    {
                        StringBuilder builder = new StringBuilder("       квота целевого приема \u2013 ").append(planT);
                        List<EnrTargetAdmissionPlan> taPlans = getTaPlans(programSetOrgUnit);
                        if (taPlans != null && taPlans.size() > 0)
                        {
                            builder.append(", из них по видам целевого приема: " + UniStringUtils.join(taPlans, EnrTargetAdmissionPlan.planString().s(), ", "));
                        }
                        planLines.add(builder.toString());
                    }
                }
            }
        }
        if (printContractPlan) {
            planLines.add("Число мест с оплатой стоимости обучения \u2013 " + String.valueOf(programSetOrgUnit.getContractPlan()));
        }

        RtfString planStr = new RtfString();
        for (String planLine : planLines) {
            if (planLines.indexOf(planLine) > 0) planStr.par();
            planStr.append(planLine);
        }

        modifier.put("plan", planStr);

        if(hasRecommendedOther)
        {
            modifier.put("hasRecommended", "* \u2013 рекомендован в рамках другого конкурсного списка по более высокому приоритету");
        }
        else
        {
            UniRtfUtil.deleteRowsWithLabels(document, Lists.newArrayList("hasRecommended"));
        }

        addCustomLabelForHeader(document, modifier, programSetOrgUnit);
        modifier.modify(document);

    }

    private String getEnrolledOrdersString(List<EnrRequestedCompetition> requestedCompetitions, Map<EnrRequestedCompetition, EnrOrder> ordersMap)
    {

        Set<EnrOrder> orderSet = new TreeSet<>(Comparator.comparing(EnrOrder::getCommitDate).thenComparing(EnrOrder::getCommitDateSystem).thenComparing(EnrOrder::getNumber));

        orderSet.addAll(ordersMap.entrySet().stream()
                .filter(e -> requestedCompetitions.contains(e.getKey()))
                .map(Map.Entry::getValue)
                .collect(Collectors.toSet()));

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrEnrollmentExtract.class, "e")
                .where(in(property(EnrEnrollmentExtract.paragraph().order().fromAlias("e")), orderSet))
                .joinEntity("e", DQLJoinType.inner, EnrCancelExtract.class, "c", eq(property("e.id"), property("c", EnrCancelExtract.entity())))
                .where(in(property("c", EnrCancelExtract.state().code()), EXTRACT_STATUS_OK))
                .where(eq(property("c", EnrCancelExtract.cancelled()), value(Boolean.FALSE)))
                .column(property("c", EnrCancelExtract.paragraph().order()));
        orderSet.addAll(getList(builder));

        if (orderSet.isEmpty())
            return "";
        StringBuilder strBuilder = new StringBuilder(" (приказы : ").append(UniStringUtils.join(orderSet, EnrOrder.dateAndNumber().s(), ", ")).append(")");
        return strBuilder.toString();
    }

    private Map<EnrCompetition, List<EnrRequestedCompetition>> getEnrolledCompetitionMap(List<EnrCompetition> competitons, Map<EnrCompetition, List<EnrRatingItem>> entrantMap)
    {
        List<EnrCompetition> customCompetition = competitons.stream()
                .filter(e->(EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL.equals(e.getType().getCode())|| EnrCompetitionTypeCodes.EXCLUSIVE.equals(e.getType().getCode())
                        || EnrCompetitionTypeCodes.TARGET_ADMISSION.equals(e.getType().getCode()))).collect(Collectors.toList());
        Map<EnrCompetition, List<EnrRequestedCompetition>> enrolledMap = new TreeMap<>(new EntityComparator<>(new EntityOrder(EnrCompetition.type().code().s())));
        for (EnrCompetition item : customCompetition)
        {
            if (entrantMap.get(item) == null)
                continue;
            List<EnrRequestedCompetition> requestedComp = entrantMap.get(item).stream().filter(e -> EnrEntrantStateCodes.ENROLLED.equals(e.getRequestedCompetition().getState().getCode()))
                    .map(EnrRatingItemGen::getRequestedCompetition).collect(Collectors.toList());
            if (!requestedComp.isEmpty())
                enrolledMap.put(item, requestedComp);
        }
        return enrolledMap;
    }

    private int getSum(List<Integer> integerList)
    {
        if(integerList == null || integerList.isEmpty()) return 0;
        int sum = 0;
        for(Integer integer : integerList)
        {
            sum += integer;
        }
        return sum;
    }

    private String chooseRtfTable(EnrCompetition competition) {
        String requestTypeCode = competition.getRequestType().getCode();        
        String compTypeCode = competition.getType().getCode();
        // Бакалавриат, специалитет:
        if (EnrRequestTypeCodes.BS.equals(requestTypeCode)) {
            // Без ВИ в рамках КЦП - "T1"
            if (EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL.equals(compTypeCode)) return T1;
            // В рамках квоты лиц, имеющих особые права - "T2"
            if (EnrCompetitionTypeCodes.EXCLUSIVE.equals(compTypeCode)) return T2;
            // Целевой прием (в т.ч. по видам) - T2
            if (EnrCompetitionTypeCodes.TARGET_ADMISSION.equals(compTypeCode)) return T2;
            // Общий конкурс - T2
            if (EnrCompetitionTypeCodes.MINISTERIAL.equals(compTypeCode)) return T2;
            // Без ВИ по договору - T3
            if (EnrCompetitionTypeCodes.NO_EXAM_CONTRACT.equals(compTypeCode)) return T3;
            // По договору - T4
            if (EnrCompetitionTypeCodes.CONTRACT.equals(compTypeCode)) return T4;
        }
        // Магистратура:
        if (EnrRequestTypeCodes.MASTER.equals(requestTypeCode)) {
            // Целевой прием (в т.ч. по видам) - T2
            if (EnrCompetitionTypeCodes.TARGET_ADMISSION.equals(compTypeCode)) return T2;
            // Общий конкурс - T2
            if (EnrCompetitionTypeCodes.MINISTERIAL.equals(compTypeCode)) return T2;
            // По договору - T4
            if (EnrCompetitionTypeCodes.CONTRACT.equals(compTypeCode)) return T4;
        }
        // Аспирантура:
        if ((EnrRequestTypeCodes.HIGHER.equals(requestTypeCode) && EduProgramKindCodes.PROGRAMMA_ASPIRANTURY_ADYUNKTURY_.equals(competition.getProgramSetOrgUnit().getProgramSet().getProgramKind().getCode()))
                || EnrRequestTypeCodes.POSTGRADUATE.equals(requestTypeCode)) {
            // Общий конкурс - T2
            if (EnrCompetitionTypeCodes.MINISTERIAL.equals(compTypeCode) || EnrCompetitionTypeCodes.TARGET_ADMISSION.equals(compTypeCode)) return T2;
            // По договору - T4
            if (EnrCompetitionTypeCodes.CONTRACT.equals(compTypeCode)) return T4;
        }

        // СПО:
        if (EnrRequestTypeCodes.SPO.equals(requestTypeCode)) {
            // Общий конкурс - T5
            if (EnrCompetitionTypeCodes.MINISTERIAL.equals(compTypeCode)) return T5;
            // По договору - T5
            if (EnrCompetitionTypeCodes.CONTRACT.equals(compTypeCode)) return T5;
        }

        // Ординатура, Интернатура
        if(EnrRequestTypeCodes.TRAINEESHIP.equals(requestTypeCode) || EnrRequestTypeCodes.INTERNSHIP.equals(requestTypeCode)
                || (EnrRequestTypeCodes.HIGHER.equals(requestTypeCode) &&
                (EduProgramKindCodes.PROGRAMMA_ORDINATURY.equals(competition.getProgramSetOrgUnit().getProgramSet().getProgramKind().getCode()) ||
                        EduProgramKindCodes.PROGRAMMA_INTERNATURY.equals(competition.getProgramSetOrgUnit().getProgramSet().getProgramKind().getCode()))))
        {
            // Целевой прием (в т.ч. по видам), Общий конкурс - T6
            if (EnrCompetitionTypeCodes.TARGET_ADMISSION.equals(compTypeCode) || EnrCompetitionTypeCodes.MINISTERIAL.equals(compTypeCode))
            {
                return T6;
            }
            // По договору - T7
            if (EnrCompetitionTypeCodes.CONTRACT.equals(compTypeCode))
            {
                return T7;
            }
        }

        throw new IllegalArgumentException();
    }

    /**
     * точка расширения для печати дополнительных ячеек в строке таблицы в проектном слое
     * @param row список ячеек строки таблицы
     * @param reqComp Выбранный конкурс (ВПО), по которому заполняется строка
     */
    protected void fillAdditionalCells(List<String> row, EnrRequestedCompetition reqComp, Set<EnrEntrantAchievement> achievements,
                                       String tableName, boolean printPriority, boolean competitionPrintForm)
    {
    }

    /**
     * Расширение для кастомизации печати таблицы заголовка
     */
    protected void addCustomLabelForHeader(RtfDocument document, RtfInjectModifier modifier, EnrProgramSetOrgUnit programSetOrgUnit)
    {

    }
}