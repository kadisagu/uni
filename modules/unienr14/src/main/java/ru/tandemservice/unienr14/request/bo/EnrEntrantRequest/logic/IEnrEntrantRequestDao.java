/* $Id:$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.logic;

import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.util.EnrRequestedCompetitionWrapper;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitProofDocument;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author oleyba
 * @since 4/24/13
 */
public interface IEnrEntrantRequestDao extends INeedPersistenceSupport
{
    EnrEntrantRequest doCreateEntrantRequest(EnrEntrant entrant, EnrEntrantRequest entrantRequest, List<IEnrEntrantBenefitProofDocument> requestBenefitProofDocs, List<EnrRequestedCompetitionWrapper> selectedDirections);

    void doAppendDirections(EnrEntrantRequest entrantRequest, List<EnrRequestedCompetitionWrapper> rowList);

    void updateDocumentsReturn(Long entrantRequestId);

    /**
     * Обновляет список документов, приложенных к заявлению
     * @param request заявление
     * @param attachedMap id документа абитуриента -> оригинал/копия приложены. null'ы будут проинтерпретированы, как то, что документ не приложен к заявлению.
     * @param oldEduDocument
     */
    void doUpdateAttachments(EnrEntrantRequest request, Map<Long, Boolean> attachedMap, boolean eduDocumentOriginalHandedInNow, String currentEditOrgOriginalInTitle, EnrRequestedCompetition requestedCompetition, PersonEduDocument oldEduDocument);

    String getNextEntrantRequestNumber(EnrEntrantRequest entrantRequest);

    void doUpdateEntrantRequest(EnrEntrantRequest entrantRequest, PersonEduDocument oldEduDocument, List<IEnrEntrantBenefitProofDocument> requestBenefitProofDocs);

    /**
     * Удаляет заявление.
     *
     * @param entrantRequestId id заявления
     */
    void deleteEntrantRequest(Long entrantRequestId);

    /**
     * Удаляет выбранный конкурс
     * @param competitionId id выбранного конкурса
     */
    public void deleteRequestedCompetition(Long competitionId);

    public void doChangePriorityUp(Long requestedCompetitionId);
    public void doChangePriorityDown(Long requestedCompetitionId);

    void doChangeParallel(Long requestedCompetitionId);

    void doChangeAcceptedByDate(Long requestedCompetitionId, Date acceptedDate);

    void doChangeAcceleratedLearning(Long requestedCompetitionId);

    /** Меняет признак "Получено профильное образование" выбранного конкурса на противоположный. */
    void doChangeProfileEducation(Long requestedCompetitionId);

    /** Меняет признак "Согласие на зачисление по договору" выбранного конкурса на противоположный. */
    void doChangeAccepted(Long requestedCompetitionId);

    /**
     * Меняет дату "Дата отказа от зачисления" выбранного конкурса на текущую, если не указана специфическая, или затирает дату, если дата указана.
     */
    void doChangeRefusedToBeEnrolled(Long requestedCompetitionId);

    /** Обновляет кеш оригиналов, enrollmentAvailable для выбранных конкурсов для всех открытых пк */
    int doUpdateEnrReqCompEnrollAvailableAndEduDoc();

    void doChangeRefusedToBeEnrolledByDate(Long requestedCompetitionId, Date refusedDate);

    /** Обновляет кеш оригиналов, enrollmentAvailable для выбранных конкурсов для абитуриента */
    void doUpdateEnrReqCompEnrollAvailableAndEduDocForEntrant(EnrEntrant entrant);

    /** Меняет признак "Оригинал" выбранного конкурса на противоположный. */
    void doChangeOriginalForReqComp(Long requestedCompetitionId);

    /** Проверяет наличие оригинала на новый или старый приложенный документ */
    @Transactional(readOnly = true)
    boolean checkOrigDocuments(PersonEduDocument oldEduDocument, PersonEduDocument newEduDocument, EnrEntrantRequest request);
    /**
     * true, если:
     * - есть вид ИД данной ПК с включенным флагом «Учитывается в рамках заявления»
     * - у абитуриента есть заявление с видом заявления как у вида ИД из предыдущего пункта (DEV-9005)
     */
    boolean isShowRequestColumn(Long entrantId);
}
