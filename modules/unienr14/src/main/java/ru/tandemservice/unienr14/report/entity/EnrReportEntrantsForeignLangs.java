package ru.tandemservice.unienr14.report.entity;

import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsForeignLangsAdd.EnrReportEntrantsForeignLangsAdd;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrReport;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrStorableReportDesc;
import ru.tandemservice.unienr14.report.entity.gen.EnrReportEntrantsForeignLangsGen;

import java.util.Arrays;
import java.util.List;

/**
 * Список абитуриентов, изучающих иностранные языки (по подразделениям)
 */
public class EnrReportEntrantsForeignLangs extends EnrReportEntrantsForeignLangsGen implements IEnrReport
{
    public static final String REPORT_KEY = "enr14ReportEntrantsForeignLangs";

    @SuppressWarnings("unchecked")
    private static List<String> properties = Arrays.asList(P_ENROLLMENT_STAGE,
        P_REQUEST_TYPE,
        P_COMPENSATION_TYPE,
        P_PROGRAM_FORM,
        P_COMPETITION_TYPE,
        P_ENR_ORG_UNIT,
        P_FORMATIVE_ORG_UNIT,
        P_PROGRAM_SUBJECT,
        P_EDU_PROGRAM,
        P_PROGRAM_SET,
        P_PARALLEL,
        P_SPLITTED);


    public static IEnrStorableReportDesc getDescription() {
        return new IEnrStorableReportDesc() {
            @Override public String getReportKey() { return REPORT_KEY; }
            @Override public Class<? extends IEnrReport> getReportClass() { return EnrReportEntrantsForeignLangs.class; }
            @Override public List<String> getPropertyList() { return properties; }

            @Override public Class<? extends BusinessComponentManager> getAddFormComponent() { return EnrReportEntrantsForeignLangsAdd.class; }
            @Override public String getPubTitle() { return "Отчет «Список абитуриентов, изучающих иностранные языки (по подразделениям)»"; }
            @Override public String getListTitle() { return "Список отчетов «Список абитуриентов, изучающих иностранные языки (по подразделениям)»"; }
        };
    }

    @Override
    public IEnrStorableReportDesc getDesc() {
        return getDescription();
    }

    @Override
    public String getPeriodTitle() {
        return "с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateFrom()) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateTo());
    }
}