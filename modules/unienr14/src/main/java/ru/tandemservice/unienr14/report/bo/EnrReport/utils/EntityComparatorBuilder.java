/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.utils;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;

import java.util.*;

/**
 * @author rsizonenko
 * @since 25.06.2014
 *
 * Компаратор билдер. Пока что работает только с параметрами String, Number.
 */
public class EntityComparatorBuilder<T extends IEntity> {
    public EntityComparatorBuilder() {
    }

    private OrderDirection direction = OrderDirection.asc;
    private boolean ignoreCase = false;
    private boolean nullFirst = false;
    private int directionValue = 1;


    private List<String> propertyList = new ArrayList<>();

    private Map<String, Object> eq = new HashMap<>();


    /*  Строит компаратор по заданным параметрам
    * */
    public Comparator<T> build()
    {
        return new Comparator<T>(){
            @Override
            public int compare(T o1, T o2) {
                int res = 0;
                for (String propertyPath : propertyList)
                {
                    if (eq.containsKey(propertyPath))
                        res = compareBothToValues(o1.getProperty(propertyPath), o2.getProperty(propertyPath), eq.get(propertyPath));
                    else
                        res = compareValues(o1.getProperty(propertyPath), o2.getProperty(propertyPath));
                    if (res != 0)
                        break;
                }

                return directionValue*res;
            }
        };
    }

    /*  Утильный метод для сравнения двух вэлью
    * */
    private int compareValues(Object value1, Object value2)
    {
        if (value1 == null && value2 == null)
            return 0;
        int nullDirection = nullFirst ? -1 : 1;
        if (value2 == null)
            return -1 * nullDirection * directionValue;
        if (value1 == null)
            return nullDirection * directionValue;

        if (value1 instanceof String && value2 instanceof String)
            if (ignoreCase)
                return ((String) value1).compareToIgnoreCase((String)value2);
            else return ((String) value1).compareTo((String) value2);

        if (value1 instanceof Number && value2 instanceof Number)
        {
            Double double1 = ((Number) value1).doubleValue();
            Double double2 = ((Number) value2).doubleValue();
            return double1.compareTo(double2);
        }

        return 0;
    }

    /*  Утильный метод для сравнения двух вэлью c образцовым значением
    * */
    private int compareBothToValues(Object value1, Object value2, Object eqParam)
    {
        int nullValue = nullFirst ? 1 : -1;
        if (eqParam == null)
            if (value1 == null && value2 == null)
                return 0;
            else if (value1 == null)
                return -1 * nullValue;
            else if (value2 == null)
                return 1 * nullValue;

        Class class1 = value1.getClass();
        Class class2 = value2.getClass();
        Class eqClass = eqParam.getClass();
        return 0;
    }

    /*  Установка направления сортировки
    * */
    public EntityComparatorBuilder setDirection(OrderDirection direction)
    {
        directionValue = direction == OrderDirection.asc ? 1 : -1;
        this.direction = direction;
        return this;
    }

    /*  Добавляет проперти для сравнения. Проперти сравниваются в порядке очередности того, как они заданы.
    * */
    public EntityComparatorBuilder addProperty(String propertyPath)
    {
        propertyList.add(propertyPath);
        return this;
    }


    /*  Добавляет проперти, у которого есть образец для сортировки. Чем удаленней от него параметр объекта, тем ниже объект будет в таблице
        Если передать вторым параметром передать null, то разместит все объекты, у которых свойство по проперти = null вверху или внизу
        списка, в зависимости от параметра nullFirst.
    * */
    public EntityComparatorBuilder addProperty(String propertyPath, Object eqParam)
    {
        propertyList.add(propertyPath);
        return this;
    }

    /*  Задает способ сортировки, где null значения идут первыми
    * */
    public EntityComparatorBuilder nullFirst()
    {
        nullFirst = true;
        return this;
    }

    /*  Задает способ сортировки, где null значения идут последними
    * */
    public EntityComparatorBuilder nullLast()
    {
        nullFirst = false;
        return this;
    }

    /*  При сравнении строк будет игнорироваться регистр
    * */
    public EntityComparatorBuilder ignoreCase()
    {
        ignoreCase = true;
        return this;
    }
}
