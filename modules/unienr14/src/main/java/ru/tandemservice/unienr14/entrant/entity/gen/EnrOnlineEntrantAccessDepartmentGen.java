package ru.tandemservice.unienr14.entrant.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unienr14.catalog.entity.EnrAccessDepartment;
import ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantAccessDepartment;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Подготовительное отделение онлайн-абитуриента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrOnlineEntrantAccessDepartmentGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantAccessDepartment";
    public static final String ENTITY_NAME = "enrOnlineEntrantAccessDepartment";
    public static final int VERSION_HASH = -623999030;
    private static IEntityMeta ENTITY_META;

    public static final String L_DEPARTMENT = "department";
    public static final String L_ONLINE_ENTRANT = "onlineEntrant";

    private EnrAccessDepartment _department;     // Отделение
    private EnrOnlineEntrant _onlineEntrant;     // Абитуриент

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Отделение. Свойство не может быть null.
     */
    @NotNull
    public EnrAccessDepartment getDepartment()
    {
        return _department;
    }

    /**
     * @param department Отделение. Свойство не может быть null.
     */
    public void setDepartment(EnrAccessDepartment department)
    {
        dirty(_department, department);
        _department = department;
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     */
    @NotNull
    public EnrOnlineEntrant getOnlineEntrant()
    {
        return _onlineEntrant;
    }

    /**
     * @param onlineEntrant Абитуриент. Свойство не может быть null.
     */
    public void setOnlineEntrant(EnrOnlineEntrant onlineEntrant)
    {
        dirty(_onlineEntrant, onlineEntrant);
        _onlineEntrant = onlineEntrant;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrOnlineEntrantAccessDepartmentGen)
        {
            setDepartment(((EnrOnlineEntrantAccessDepartment)another).getDepartment());
            setOnlineEntrant(((EnrOnlineEntrantAccessDepartment)another).getOnlineEntrant());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrOnlineEntrantAccessDepartmentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrOnlineEntrantAccessDepartment.class;
        }

        public T newInstance()
        {
            return (T) new EnrOnlineEntrantAccessDepartment();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "department":
                    return obj.getDepartment();
                case "onlineEntrant":
                    return obj.getOnlineEntrant();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "department":
                    obj.setDepartment((EnrAccessDepartment) value);
                    return;
                case "onlineEntrant":
                    obj.setOnlineEntrant((EnrOnlineEntrant) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "department":
                        return true;
                case "onlineEntrant":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "department":
                    return true;
                case "onlineEntrant":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "department":
                    return EnrAccessDepartment.class;
                case "onlineEntrant":
                    return EnrOnlineEntrant.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrOnlineEntrantAccessDepartment> _dslPath = new Path<EnrOnlineEntrantAccessDepartment>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrOnlineEntrantAccessDepartment");
    }
            

    /**
     * @return Отделение. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantAccessDepartment#getDepartment()
     */
    public static EnrAccessDepartment.Path<EnrAccessDepartment> department()
    {
        return _dslPath.department();
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantAccessDepartment#getOnlineEntrant()
     */
    public static EnrOnlineEntrant.Path<EnrOnlineEntrant> onlineEntrant()
    {
        return _dslPath.onlineEntrant();
    }

    public static class Path<E extends EnrOnlineEntrantAccessDepartment> extends EntityPath<E>
    {
        private EnrAccessDepartment.Path<EnrAccessDepartment> _department;
        private EnrOnlineEntrant.Path<EnrOnlineEntrant> _onlineEntrant;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Отделение. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantAccessDepartment#getDepartment()
     */
        public EnrAccessDepartment.Path<EnrAccessDepartment> department()
        {
            if(_department == null )
                _department = new EnrAccessDepartment.Path<EnrAccessDepartment>(L_DEPARTMENT, this);
            return _department;
        }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantAccessDepartment#getOnlineEntrant()
     */
        public EnrOnlineEntrant.Path<EnrOnlineEntrant> onlineEntrant()
        {
            if(_onlineEntrant == null )
                _onlineEntrant = new EnrOnlineEntrant.Path<EnrOnlineEntrant>(L_ONLINE_ENTRANT, this);
            return _onlineEntrant;
        }

        public Class getEntityClass()
        {
            return EnrOnlineEntrantAccessDepartment.class;
        }

        public String getEntityName()
        {
            return "enrOnlineEntrantAccessDepartment";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
