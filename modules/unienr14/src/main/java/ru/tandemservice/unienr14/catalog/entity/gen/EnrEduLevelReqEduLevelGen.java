package ru.tandemservice.unienr14.catalog.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import ru.tandemservice.unienr14.catalog.entity.EnrEduLevelReqEduLevel;
import ru.tandemservice.unienr14.catalog.entity.EnrEduLevelRequirement;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Разрешенный уровень образования для поступающих
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrEduLevelReqEduLevelGen extends EntityBase
 implements INaturalIdentifiable<EnrEduLevelReqEduLevelGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.catalog.entity.EnrEduLevelReqEduLevel";
    public static final String ENTITY_NAME = "enrEduLevelReqEduLevel";
    public static final int VERSION_HASH = -694806740;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENR_EDU_LEVEL_REQUIREMENT = "enrEduLevelRequirement";
    public static final String L_EDU_LEVEL = "eduLevel";

    private EnrEduLevelRequirement _enrEduLevelRequirement;     // Базовые уровни образования поступающих
    private EduLevel _eduLevel;     // Уровень (вид, подвид) образования

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Базовые уровни образования поступающих. Свойство не может быть null.
     */
    @NotNull
    public EnrEduLevelRequirement getEnrEduLevelRequirement()
    {
        return _enrEduLevelRequirement;
    }

    /**
     * @param enrEduLevelRequirement Базовые уровни образования поступающих. Свойство не может быть null.
     */
    public void setEnrEduLevelRequirement(EnrEduLevelRequirement enrEduLevelRequirement)
    {
        dirty(_enrEduLevelRequirement, enrEduLevelRequirement);
        _enrEduLevelRequirement = enrEduLevelRequirement;
    }

    /**
     * @return Уровень (вид, подвид) образования. Свойство не может быть null.
     */
    @NotNull
    public EduLevel getEduLevel()
    {
        return _eduLevel;
    }

    /**
     * @param eduLevel Уровень (вид, подвид) образования. Свойство не может быть null.
     */
    public void setEduLevel(EduLevel eduLevel)
    {
        dirty(_eduLevel, eduLevel);
        _eduLevel = eduLevel;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrEduLevelReqEduLevelGen)
        {
            if (withNaturalIdProperties)
            {
                setEnrEduLevelRequirement(((EnrEduLevelReqEduLevel)another).getEnrEduLevelRequirement());
                setEduLevel(((EnrEduLevelReqEduLevel)another).getEduLevel());
            }
        }
    }

    public INaturalId<EnrEduLevelReqEduLevelGen> getNaturalId()
    {
        return new NaturalId(getEnrEduLevelRequirement(), getEduLevel());
    }

    public static class NaturalId extends NaturalIdBase<EnrEduLevelReqEduLevelGen>
    {
        private static final String PROXY_NAME = "EnrEduLevelReqEduLevelNaturalProxy";

        private Long _enrEduLevelRequirement;
        private Long _eduLevel;

        public NaturalId()
        {}

        public NaturalId(EnrEduLevelRequirement enrEduLevelRequirement, EduLevel eduLevel)
        {
            _enrEduLevelRequirement = ((IEntity) enrEduLevelRequirement).getId();
            _eduLevel = ((IEntity) eduLevel).getId();
        }

        public Long getEnrEduLevelRequirement()
        {
            return _enrEduLevelRequirement;
        }

        public void setEnrEduLevelRequirement(Long enrEduLevelRequirement)
        {
            _enrEduLevelRequirement = enrEduLevelRequirement;
        }

        public Long getEduLevel()
        {
            return _eduLevel;
        }

        public void setEduLevel(Long eduLevel)
        {
            _eduLevel = eduLevel;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrEduLevelReqEduLevelGen.NaturalId) ) return false;

            EnrEduLevelReqEduLevelGen.NaturalId that = (NaturalId) o;

            if( !equals(getEnrEduLevelRequirement(), that.getEnrEduLevelRequirement()) ) return false;
            if( !equals(getEduLevel(), that.getEduLevel()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEnrEduLevelRequirement());
            result = hashCode(result, getEduLevel());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEnrEduLevelRequirement());
            sb.append("/");
            sb.append(getEduLevel());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrEduLevelReqEduLevelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrEduLevelReqEduLevel.class;
        }

        public T newInstance()
        {
            return (T) new EnrEduLevelReqEduLevel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrEduLevelRequirement":
                    return obj.getEnrEduLevelRequirement();
                case "eduLevel":
                    return obj.getEduLevel();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrEduLevelRequirement":
                    obj.setEnrEduLevelRequirement((EnrEduLevelRequirement) value);
                    return;
                case "eduLevel":
                    obj.setEduLevel((EduLevel) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrEduLevelRequirement":
                        return true;
                case "eduLevel":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrEduLevelRequirement":
                    return true;
                case "eduLevel":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrEduLevelRequirement":
                    return EnrEduLevelRequirement.class;
                case "eduLevel":
                    return EduLevel.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrEduLevelReqEduLevel> _dslPath = new Path<EnrEduLevelReqEduLevel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrEduLevelReqEduLevel");
    }
            

    /**
     * @return Базовые уровни образования поступающих. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrEduLevelReqEduLevel#getEnrEduLevelRequirement()
     */
    public static EnrEduLevelRequirement.Path<EnrEduLevelRequirement> enrEduLevelRequirement()
    {
        return _dslPath.enrEduLevelRequirement();
    }

    /**
     * @return Уровень (вид, подвид) образования. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrEduLevelReqEduLevel#getEduLevel()
     */
    public static EduLevel.Path<EduLevel> eduLevel()
    {
        return _dslPath.eduLevel();
    }

    public static class Path<E extends EnrEduLevelReqEduLevel> extends EntityPath<E>
    {
        private EnrEduLevelRequirement.Path<EnrEduLevelRequirement> _enrEduLevelRequirement;
        private EduLevel.Path<EduLevel> _eduLevel;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Базовые уровни образования поступающих. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrEduLevelReqEduLevel#getEnrEduLevelRequirement()
     */
        public EnrEduLevelRequirement.Path<EnrEduLevelRequirement> enrEduLevelRequirement()
        {
            if(_enrEduLevelRequirement == null )
                _enrEduLevelRequirement = new EnrEduLevelRequirement.Path<EnrEduLevelRequirement>(L_ENR_EDU_LEVEL_REQUIREMENT, this);
            return _enrEduLevelRequirement;
        }

    /**
     * @return Уровень (вид, подвид) образования. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrEduLevelReqEduLevel#getEduLevel()
     */
        public EduLevel.Path<EduLevel> eduLevel()
        {
            if(_eduLevel == null )
                _eduLevel = new EduLevel.Path<EduLevel>(L_EDU_LEVEL, this);
            return _eduLevel;
        }

        public Class getEntityClass()
        {
            return EnrEduLevelReqEduLevel.class;
        }

        public String getEntityName()
        {
            return "enrEduLevelReqEduLevel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
