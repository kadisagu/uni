/* $Id:$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.util;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import ru.tandemservice.unienr14.catalog.entity.EnrBenefitCategory;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantCompetition;
import ru.tandemservice.unienr14.request.entity.*;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignTargetAdmissionKind;

import java.util.Date;
import java.util.List;

/**
* @author oleyba
* @since 2/21/14
*/
public class EnrRequestedCompetitionWrapper extends IdentifiableWrapper<IEntity>
{
    private Long existingCompetitionId;

    private EnrCompetition competition;
    private String requestTitle;
    private boolean fromCurrentRequest;

    private EnrCampaignTargetAdmissionKind targetAdmissionKind;
    private ExternalOrgUnit targetAdmissionOrgUnit;
    private String contractNumber;
    private Date contractDate;
    private EnrBenefitCategory benefitCategory;
    private EnrProgramSetItem programSetItem;
    private boolean targetContractProgramSet;
    private ExternalOrgUnit targetContractOrgUnit;
    private String contractCommonNumber;
    private Date contractCommonDate;

    private List<IEnrEntrantBenefitProofDocument> benefitProofDocs;

    public EnrRequestedCompetitionWrapper(EnrRequestedCompetition reqComp, EnrEntrantRequest currentRequest) {
        super(reqComp.getId(), reqComp.getTitle());
        existingCompetitionId = reqComp.getId();
        setCompetition(reqComp.getCompetition());
        fromCurrentRequest = reqComp.getRequest().equals(currentRequest);
        requestTitle = fromCurrentRequest ? "текущее" : reqComp.getRequest().getTitle();

        //Заполняем данные из выбранного конкурса
        setContractCommonDate(reqComp.getContractCommonDate());
        setContractCommonNumber(reqComp.getContractCommonNumber());
        setTargetContractOrgUnit(reqComp.getExternalOrgUnit());

        if (reqComp instanceof EnrRequestedCompetitionTA) {
            setTargetAdmissionKind(((EnrRequestedCompetitionTA) reqComp).getTargetAdmissionKind());
            setTargetAdmissionOrgUnit(((EnrRequestedCompetitionTA) reqComp).getTargetAdmissionOrgUnit());
            setContractNumber(((EnrRequestedCompetitionTA) reqComp).getContractNumber());
            setContractDate(((EnrRequestedCompetitionTA) reqComp).getContractDate());
        } else if (reqComp instanceof EnrRequestedCompetitionExclusive) {
            setBenefitCategory(((EnrRequestedCompetitionExclusive) reqComp).getBenefitCategory());
            setProgramSetItem(((EnrRequestedCompetitionExclusive) reqComp).getProgramSetItem());
            setBenefitProofDocs(EnrEntrantManager.instance().dao().getBenefitStatementProofDocs((EnrRequestedCompetitionExclusive) reqComp));
        } else if (reqComp instanceof EnrRequestedCompetitionNoExams) {
            setBenefitCategory(((EnrRequestedCompetitionNoExams) reqComp).getBenefitCategory());
            setProgramSetItem(((EnrRequestedCompetitionNoExams) reqComp).getProgramSetItem());
            setBenefitProofDocs(EnrEntrantManager.instance().dao().getBenefitStatementProofDocs((EnrRequestedCompetitionNoExams) reqComp));
        }
    }

    public EnrRequestedCompetitionWrapper(EnrOnlineEntrantCompetition onlineComp)
    {
        super(onlineComp.getId(), onlineComp.getCompetition().getTitle());
        setCompetition(onlineComp.getCompetition());
        requestTitle = "текущее";
        fromCurrentRequest = true;
    }

    public EnrRequestedCompetitionWrapper(Long id, String title) {
        super(id, title);
        requestTitle = "текущее";
        fromCurrentRequest = true;
    }

    public EnrRequestedCompetitionWrapper(EnrRequestedCompetitionWrapper selectedRow)
    {
        super(selectedRow.getId(), selectedRow.getTitle());
        this.existingCompetitionId = selectedRow.existingCompetitionId;
        this.competition = selectedRow.competition;
        this.requestTitle = selectedRow.requestTitle;
        this.fromCurrentRequest = selectedRow.fromCurrentRequest;
        this.targetAdmissionKind = selectedRow.targetAdmissionKind;
        this.targetAdmissionOrgUnit = selectedRow.targetAdmissionOrgUnit;
        this.contractNumber = selectedRow.contractNumber;
        this.contractDate = selectedRow.contractDate;
        this.benefitCategory = selectedRow.benefitCategory;
        this.programSetItem = selectedRow.programSetItem;
        this.benefitProofDocs = selectedRow.benefitProofDocs;
        this.targetContractProgramSet = selectedRow.targetContractProgramSet;
        this.contractCommonDate = selectedRow.contractCommonDate;
        this.contractCommonNumber = selectedRow.contractCommonNumber;
        this.targetContractOrgUnit = selectedRow.targetContractOrgUnit;
    }

    public String getParametersTitle() {
        if (getBenefitCategory() == null && getCompetition().isExclusive())
            return "не указаны";
        if (getTargetAdmissionKind() == null && getCompetition().isTargetAdmission())
            return "не указаны";

        if (isTargetContractProgramSet() && getTargetContractOrgUnit() == null)
            return "не указаны";

        if (getCompetition().isNoExams() &&  getBenefitCategory() == null)
            return "не указаны";

//        if (getBenefitCategory() == null && getCompetition().isNoExams())
//            return "не указаны";

        if (getCompetition().isTargetAdmission())
        {
            StringBuilder contractStr = new StringBuilder();
            if (getContractNumber() != null || getContractDate() != null)
            {
                contractStr.append(", договор");
                if (getContractNumber() != null)
                    contractStr.append(" №").append(getContractNumber());

                if (getContractDate() != null)
                    contractStr.append(" от ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getContractDate()));
            }

            return (getTargetAdmissionOrgUnit() == null ? getTargetAdmissionKind().getTitle() : getTargetAdmissionKind().getTitle() + ", " + getTargetAdmissionOrgUnit().getTitleWithLegalForm()) + contractStr.toString();
        }
        if (getCompetition().isExclusive())
            return getBenefitCategory().getTitle() + " (" + getProgramSetItem().getProgram().getTitle() + ")";

        StringBuilder builder = new StringBuilder();
        if (getCompetition().isNoExams())
            builder.append(getBenefitCategory().getTitle()).append(" (").append(getProgramSetItem().getProgram().getTitle()).append(")");

        if (getCompetition().isContract())
        {
            StringBuilder contractStr = new StringBuilder();
            if (getContractCommonNumber() != null || getContractCommonDate() != null)
            {
                contractStr.append("договор");
                if (getContractCommonNumber() != null)
                    contractStr.append(" №").append(getContractCommonNumber());

                if (getContractCommonDate() != null)
                    contractStr.append(" от ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getContractCommonDate()));
            }
            if (builder.length() > 0)
                builder.append(", ");
            builder.append(getTargetContractOrgUnit() == null ? "" : getTargetContractOrgUnit().getTitleWithLegalForm())
                    .append(contractStr.length() > 0? ", " : "")
                    .append(contractStr.toString());
        }


        return builder.toString();
    }

    public void updateFields(EnrRequestedCompetition reqComp) {
        if (reqComp instanceof EnrRequestedCompetitionTA) {
            ((EnrRequestedCompetitionTA) reqComp).setTargetAdmissionKind(getTargetAdmissionKind());
            ((EnrRequestedCompetitionTA) reqComp).setTargetAdmissionOrgUnit(getTargetAdmissionOrgUnit());
            ((EnrRequestedCompetitionTA) reqComp).setContractNumber(getContractNumber());
            ((EnrRequestedCompetitionTA) reqComp).setContractDate(getContractDate());

        } else if (reqComp instanceof EnrRequestedCompetitionExclusive) {
            ((EnrRequestedCompetitionExclusive) reqComp).setBenefitCategory(getBenefitCategory());
            ((EnrRequestedCompetitionExclusive) reqComp).setProgramSetItem(getProgramSetItem());
        }  else if (reqComp instanceof EnrRequestedCompetitionNoExams) {
            ((EnrRequestedCompetitionNoExams) reqComp).setBenefitCategory(getBenefitCategory());
            ((EnrRequestedCompetitionNoExams) reqComp).setProgramSetItem(getProgramSetItem());
        }
        reqComp.setExternalOrgUnit(getTargetContractOrgUnit());
        reqComp.setContractCommonDate(getContractCommonDate());
        reqComp.setContractCommonNumber(getContractCommonNumber());
    }

    public void validate(ErrorCollector errorCollector)
    {
        if (getCompetition().isTargetAdmission())
        {
            // ЦП
            if ((getTargetAdmissionKind() == null || getTargetAdmissionOrgUnit() == null))
                errorCollector.addError("Для выбранного конкурса «"+competition.getTitle()+"» не указаны параметры целевого приема.");
        }
        if (getCompetition().isExclusive())
        {
            // Особые права
            if (getBenefitCategory() == null)
                errorCollector.addError("Для выбранного конкурса «"+competition.getTitle()+"» не указана категория граждан, дающая право на участие в конкурсе по квоте особых прав.");
            if (getProgramSetItem() == null)
                errorCollector.addError("Для выбранного конкурса «"+competition.getTitle()+"» не указана образовательная программа для поступления по квоте особых прав.");
            if (getBenefitProofDocs() == null)
                errorCollector.addError("Для выбранного конкурса «"+competition.getTitle()+"» не указаны документы, подтверждающие право на поступление по квоте.");
        }
        if (getCompetition().isNoExams())
        {
            // Без ВИ
            if (getBenefitCategory() == null)
                errorCollector.addError("Для выбранного конкурса «"+competition.getTitle()+"» не указана категория граждан, дающая право на поступление без вступительных испытаний.");
            if (getProgramSetItem() == null)
                errorCollector.addError("Для выбранного конкурса «"+competition.getTitle()+"» не указана образовательная программа для поступления без вступительных испытаний.");
            if (getBenefitProofDocs() == null)
                errorCollector.addError("Для выбранного конкурса «"+competition.getTitle()+"» не указаны документы, подтверждающие право на поступление без вступительных испытаний.");
        }
        //целевая контрактная подготовка
        if (isTargetContractProgramSet() && getTargetContractOrgUnit() == null)
            errorCollector.addError("Для выбранного конкурса «"+competition.getTitle()+"» не указаны параметры целевой контрактной подготовки.");
    }

    public boolean isAllowEdit() {
        return isFromCurrentRequest() && (getCompetition().isExclusive() || getCompetition().isTargetAdmission() || getCompetition().isNoExams() || getCompetition().isContract());
    }

    public boolean isAllowDelete() {
        return isFromCurrentRequest();
    }

    public EnrCompetition getCompetition() { return competition; }

    public void setCompetition(EnrCompetition competition) {
        this.competition = competition;
        this.targetContractProgramSet = competition.getProgramSetOrgUnit().getProgramSet().isTargetContractTraining();
    }

    public Long getExistingCompetitionId() { return existingCompetitionId; }

    public String getRequestTitle()
    {
        return requestTitle;
    }

    public boolean isFromCurrentRequest()
    {
        return fromCurrentRequest;
    }

    public EnrBenefitCategory getBenefitCategory()
    {
        return benefitCategory;
    }

    public void setBenefitCategory(EnrBenefitCategory benefitCategory)
    {
        this.benefitCategory = benefitCategory;
    }

    public void setExistingCompetitionId(Long existingCompetitionId)
    {
        this.existingCompetitionId = existingCompetitionId;
    }

    public EnrCampaignTargetAdmissionKind getTargetAdmissionKind()
    {
        return targetAdmissionKind;
    }

    public void setTargetAdmissionKind(EnrCampaignTargetAdmissionKind targetAdmissionKind)
    {
        this.targetAdmissionKind = targetAdmissionKind;
    }

    public ExternalOrgUnit getTargetAdmissionOrgUnit()
    {
        return targetAdmissionOrgUnit;
    }

    public void setTargetAdmissionOrgUnit(ExternalOrgUnit targetAdmissionOrgUnit)
    {
        this.targetAdmissionOrgUnit = targetAdmissionOrgUnit;
    }

    public String getContractNumber(){ return contractNumber; }
    public void setContractNumber(String contractNumber){ this.contractNumber = contractNumber; }

    public Date getContractDate()
    {
        return contractDate;
    }

    public void setContractDate(Date contractDate)
    {
        this.contractDate = contractDate;
    }

    public EnrProgramSetItem getProgramSetItem()
    {
        return programSetItem;
    }

    public void setProgramSetItem(EnrProgramSetItem programSetItem)
    {
        this.programSetItem = programSetItem;
    }

    public boolean isTargetContractProgramSet()
    {
        return targetContractProgramSet;
    }

    public void setTargetContractProgramSet(boolean targetContractProgramSet)
    {
        this.targetContractProgramSet = targetContractProgramSet;
    }

    public ExternalOrgUnit getTargetContractOrgUnit()
    {
        return targetContractOrgUnit;
    }

    public void setTargetContractOrgUnit(ExternalOrgUnit targetContractOrgUnit)
    {
        this.targetContractOrgUnit = targetContractOrgUnit;
    }

    public String getContractCommonNumber()
    {
        return contractCommonNumber;
    }

    public void setContractCommonNumber(String contractCommonNumber)
    {
        this.contractCommonNumber = contractCommonNumber;
    }

    public Date getContractCommonDate()
    {
        return contractCommonDate;
    }

    public void setContractCommonDate(Date contractCommonDate)
    {
        this.contractCommonDate = contractCommonDate;
    }

    public List<IEnrEntrantBenefitProofDocument> getBenefitProofDocs(){ return benefitProofDocs; }
    public void setBenefitProofDocs(List<IEnrEntrantBenefitProofDocument> benefitProofDocs){ this.benefitProofDocs = benefitProofDocs; }
}
