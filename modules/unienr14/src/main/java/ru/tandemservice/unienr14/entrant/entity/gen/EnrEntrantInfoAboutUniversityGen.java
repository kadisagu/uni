package ru.tandemservice.unienr14.entrant.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.catalog.entity.EnrSourceInfoAboutUniversity;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantInfoAboutUniversity;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Источник информации об ОУ абитуриента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrEntrantInfoAboutUniversityGen extends EntityBase
 implements INaturalIdentifiable<EnrEntrantInfoAboutUniversityGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.entrant.entity.EnrEntrantInfoAboutUniversity";
    public static final String ENTITY_NAME = "enrEntrantInfoAboutUniversity";
    public static final int VERSION_HASH = 680783267;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTRANT = "entrant";
    public static final String L_SOURCE_INFO = "sourceInfo";

    private EnrEntrant _entrant;     // Абитуриент
    private EnrSourceInfoAboutUniversity _sourceInfo;     // Источник

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     */
    @NotNull
    public EnrEntrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant Абитуриент. Свойство не может быть null.
     */
    public void setEntrant(EnrEntrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    /**
     * @return Источник. Свойство не может быть null.
     */
    @NotNull
    public EnrSourceInfoAboutUniversity getSourceInfo()
    {
        return _sourceInfo;
    }

    /**
     * @param sourceInfo Источник. Свойство не может быть null.
     */
    public void setSourceInfo(EnrSourceInfoAboutUniversity sourceInfo)
    {
        dirty(_sourceInfo, sourceInfo);
        _sourceInfo = sourceInfo;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrEntrantInfoAboutUniversityGen)
        {
            if (withNaturalIdProperties)
            {
                setEntrant(((EnrEntrantInfoAboutUniversity)another).getEntrant());
                setSourceInfo(((EnrEntrantInfoAboutUniversity)another).getSourceInfo());
            }
        }
    }

    public INaturalId<EnrEntrantInfoAboutUniversityGen> getNaturalId()
    {
        return new NaturalId(getEntrant(), getSourceInfo());
    }

    public static class NaturalId extends NaturalIdBase<EnrEntrantInfoAboutUniversityGen>
    {
        private static final String PROXY_NAME = "EnrEntrantInfoAboutUniversityNaturalProxy";

        private Long _entrant;
        private Long _sourceInfo;

        public NaturalId()
        {}

        public NaturalId(EnrEntrant entrant, EnrSourceInfoAboutUniversity sourceInfo)
        {
            _entrant = ((IEntity) entrant).getId();
            _sourceInfo = ((IEntity) sourceInfo).getId();
        }

        public Long getEntrant()
        {
            return _entrant;
        }

        public void setEntrant(Long entrant)
        {
            _entrant = entrant;
        }

        public Long getSourceInfo()
        {
            return _sourceInfo;
        }

        public void setSourceInfo(Long sourceInfo)
        {
            _sourceInfo = sourceInfo;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrEntrantInfoAboutUniversityGen.NaturalId) ) return false;

            EnrEntrantInfoAboutUniversityGen.NaturalId that = (NaturalId) o;

            if( !equals(getEntrant(), that.getEntrant()) ) return false;
            if( !equals(getSourceInfo(), that.getSourceInfo()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEntrant());
            result = hashCode(result, getSourceInfo());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEntrant());
            sb.append("/");
            sb.append(getSourceInfo());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrEntrantInfoAboutUniversityGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrEntrantInfoAboutUniversity.class;
        }

        public T newInstance()
        {
            return (T) new EnrEntrantInfoAboutUniversity();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entrant":
                    return obj.getEntrant();
                case "sourceInfo":
                    return obj.getSourceInfo();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entrant":
                    obj.setEntrant((EnrEntrant) value);
                    return;
                case "sourceInfo":
                    obj.setSourceInfo((EnrSourceInfoAboutUniversity) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entrant":
                        return true;
                case "sourceInfo":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entrant":
                    return true;
                case "sourceInfo":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entrant":
                    return EnrEntrant.class;
                case "sourceInfo":
                    return EnrSourceInfoAboutUniversity.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrEntrantInfoAboutUniversity> _dslPath = new Path<EnrEntrantInfoAboutUniversity>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrEntrantInfoAboutUniversity");
    }
            

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantInfoAboutUniversity#getEntrant()
     */
    public static EnrEntrant.Path<EnrEntrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return Источник. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantInfoAboutUniversity#getSourceInfo()
     */
    public static EnrSourceInfoAboutUniversity.Path<EnrSourceInfoAboutUniversity> sourceInfo()
    {
        return _dslPath.sourceInfo();
    }

    public static class Path<E extends EnrEntrantInfoAboutUniversity> extends EntityPath<E>
    {
        private EnrEntrant.Path<EnrEntrant> _entrant;
        private EnrSourceInfoAboutUniversity.Path<EnrSourceInfoAboutUniversity> _sourceInfo;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantInfoAboutUniversity#getEntrant()
     */
        public EnrEntrant.Path<EnrEntrant> entrant()
        {
            if(_entrant == null )
                _entrant = new EnrEntrant.Path<EnrEntrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return Источник. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantInfoAboutUniversity#getSourceInfo()
     */
        public EnrSourceInfoAboutUniversity.Path<EnrSourceInfoAboutUniversity> sourceInfo()
        {
            if(_sourceInfo == null )
                _sourceInfo = new EnrSourceInfoAboutUniversity.Path<EnrSourceInfoAboutUniversity>(L_SOURCE_INFO, this);
            return _sourceInfo;
        }

        public Class getEntityClass()
        {
            return EnrEntrantInfoAboutUniversity.class;
        }

        public String getEntityName()
        {
            return "enrEntrantInfoAboutUniversity";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
