/* $Id$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.ExamsStep;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;

import org.tandemframework.core.util.cache.SafeMap;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm;
import ru.tandemservice.unienr14.catalog.entity.EnrStateExamSubject;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrExamPassFormCodes;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.EditChosenExams.EnrEntrantEditChosenExamsUI;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.EnrEntrantRequestAddWizardWizardUI;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.EnrEntrantWizardState;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.IWizardStep;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author nvankov
 * @since 6/5/14
 */
@Input( {
    @Bind(key = EnrEntrantRequestAddWizardWizardUI.PARAM_WIZARD_STATE, binding = "state")
})
public class EnrEntrantRequestAddWizardExamsStepUI extends EnrEntrantEditChosenExamsUI implements IWizardStep
{
    private EnrEntrantWizardState _state;

    private boolean initialized = false;

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();

        if (!isInitialized()) {

            // нам нужно превыбрать форму сдачи «ЕГЭ», если эти ЕГЭ можно зачесть

            Map<EnrStateExamSubject, List<EnrCampaignDiscipline>> disciplineMap = SafeMap.get(ArrayList.class);
            for (EnrCampaignDiscipline discipline : IUniBaseDao.instance.get().getList(EnrCampaignDiscipline.class, EnrCampaignDiscipline.enrollmentCampaign(), getEntrant().getEnrollmentCampaign())) {
                if (discipline.getStateExamSubject() != null) {
                    disciplineMap.get(discipline.getStateExamSubject()).add(discipline);
                }
            }

            EnrExamPassForm stateExamPassForm = IUniBaseDao.instance.get().getCatalogItem(EnrExamPassForm.class, EnrExamPassFormCodes.STATE_EXAM);

            List<EnrEntrantStateExamResult> stateExamResults = IUniBaseDao.instance.get().getList(EnrEntrantStateExamResult.class, EnrEntrantStateExamResult.entrant(), getEntrant());

            Map<EnrCampaignDiscipline, Integer> resultMap = new HashMap<>();
            for (EnrEntrantStateExamResult result : stateExamResults) {
                for (EnrCampaignDiscipline discipline : disciplineMap.get(result.getSubject())) {
                    Integer mark = resultMap.get(discipline);
                    if (Integer.valueOf(0).equals(mark)) continue;
                    if (result.getMark() == null) {
                        resultMap.put(discipline, 0);
                    } else if (mark == null || result.getMark() > mark) {
                        resultMap.put(discipline, result.getMark());
                    }
                }
            }
            for (CompetitionRowWrapper row : getRowList()) {
                for (DiscWrapper discWrapper : row.getDiscRowList()) {
                    if (!CollectionUtils.isEmpty(discWrapper.getSelectedPassForms()))
                        continue;
                    for (EnrCampaignDiscipline discipline : discWrapper.getDiscList()) {
                        Integer mark = resultMap.get(discipline);
                        if (null == mark) continue;
                        if (mark != 0 && discWrapper.getActualExam() != null && discWrapper.getActualExam().getPassMarkAsDouble() <= mark) {
                            discWrapper.setDisc(discipline);
                            discWrapper.getSelectedPassForms().add(stateExamPassForm);
                        }
                    }
                }
            }
        }

        setInitialized(true);
    }

    @Override
    public void saveStepData()
    {
        if (saveFormData()) return;
        getState().getStateExamsStep().setDisabled(true);
    }

    // getters and setters

    public EnrEntrantWizardState getState()
    {
        return _state;
    }

    public void setState(EnrEntrantWizardState state)
    {
        _state = state;
    }

    public boolean isInitialized()
    {
        return initialized;
    }

    public void setInitialized(boolean initialized)
    {
        this.initialized = initialized;
    }
}
