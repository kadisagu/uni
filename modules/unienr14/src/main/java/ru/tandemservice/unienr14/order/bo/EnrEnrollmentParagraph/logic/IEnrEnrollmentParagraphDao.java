/* $Id:$ */
package ru.tandemservice.unienr14.order.bo.EnrEnrollmentParagraph.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.order.entity.EnrAbstractExtract;
import ru.tandemservice.unienr14.order.entity.EnrAbstractParagraph;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentParagraph;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.List;

/**
 * @author oleyba
 * @since 7/8/14
 */
public interface IEnrEnrollmentParagraphDao extends INeedPersistenceSupport
{
    void saveOrUpdateParagraph(EnrOrder order, EnrEnrollmentParagraph paragraph, List<EnrRequestedCompetition> preStudents, EnrRequestedCompetition manager, String groupTitle);

    void doCreateOrders(EnrEnrollmentStep step);

    /** Прочие выписки абитуриентов (кроме данного параграфа). */
    Collection<EnrAbstractExtract> getOtherExtracts(Collection<EnrEntrant> entrants, @Nullable EnrAbstractParagraph paragraph);
}