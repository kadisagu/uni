/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.xml;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.CoreMessageDefines;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.EnrEnrollmentModelManager;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.logic.EnrModellingContext;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.EnrEnrollmentStepManager;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.select.EnrEnrollmentStepEnrollmentContext;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.select.EnrEnrollmentStepRecommendationContext;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep;
import ru.tandemservice.unienr14.enrollment.entity.EnrModelStep;

import java.util.ArrayList;
import java.util.List;

/**
 * @author oleyba
 * @since 4/2/15
 */
public class EnrXmlEnrollmentCheckData
{
    private int version = -1;
    private byte[] xml;
    private Throwable lastException;

    private List<Item> itemList;
    private Item item;

    private final boolean recommendation;

    public EnrXmlEnrollmentCheckData(boolean recommendation)
    {
        this.recommendation = recommendation;
    }

    public void forceRefresh() {
        this.setXml(null);
        this.setItemList(null);
        this.setLastException(null);
        this.setVersion(-1);
    }

    public void refresh(EnrEnrollmentStep step) {
        if (this.getVersion() == step.getVersion()) return;

        forceRefresh();
        this.setVersion(step.getVersion());

        try {
            final EnrXmlEnrollmentStepState state;

            final byte[] xml = recommendation ? step.getXmlRecommendationState() : step.getXmlEnrollmentState();
            if (null != xml) {
                if (Debug.isEnabled()) { this.setXml(xml); }
                state = EnrXmlEnrollmentUtils.fromXml(EnrXmlEnrollmentStepState.class, xml);
            } else {
                if (recommendation) {
                    final EnrEnrollmentStepRecommendationContext context = EnrEnrollmentStepManager.instance().selectionDao().prepareRecommendationContext(step, false);
                    state = context.buildEnrXmlEnrollmentStepState();
                } else {
                    final EnrEnrollmentStepEnrollmentContext context = EnrEnrollmentStepManager.instance().selectionDao().prepareEnrollmentContext(step, false);
                    state = context.buildEnrXmlEnrollmentStepState();
                }
                if (Debug.isEnabled()) { this.setXml(EnrXmlEnrollmentUtils.toXml(state)); }
            }

            itemList = new ArrayList<>();
            for (final EnrXmlEnrollmentStepState.Competition c: state.competitionList) {
                for (final EnrXmlEnrollmentStepState.Group g: c.groupList) {
                    if (recommendation && null == g.checkRec) { throw new IllegalStateException("wrong-xml: g.checkRec"); }
                    if (!recommendation && null == g.checkEnr) { throw new IllegalStateException("wrong-xml: g.checkEnr"); }
                    itemList.add(new Item(c, g));
                }
            }
        } catch (final Throwable t) {
            if (Debug.isEnabled()) { this.setLastException(t); }
            else { throw CoreExceptionUtils.getRuntimeException(t); /* клиенту показываем ошибку */ }
        }
    }

    public void refresh(EnrModelStep step)
    {
        try {
            final EnrXmlEnrollmentStepState state;

            final EnrModellingContext context = EnrEnrollmentModelManager.instance().dao().prepareContext(step, false);
            state = context.buildEnrXmlEnrollmentStepState();
            if (Debug.isEnabled()) { this.setXml(EnrXmlEnrollmentUtils.toXml(state)); }

            itemList = new ArrayList<>();
            for (final EnrXmlEnrollmentStepState.Competition c: state.competitionList) {
                for (final EnrXmlEnrollmentStepState.Group g: c.groupList) {
                    if (null == g.checkEnr) { throw new IllegalStateException("wrong-xml: g.checkEnr"); }
                    itemList.add(new Item(c, g));
                }
            }
        } catch (final Throwable t) {
            if (Debug.isEnabled()) { this.setLastException(t); }
            else { throw CoreExceptionUtils.getRuntimeException(t); /* клиенту показываем ошибку */ }
        }
    }

    public boolean isShowData() {
        return null == lastException;
    }

    public String getErrorText() {
        final Throwable lastException = this.getLastException();
        if (null == lastException) { return ""; }
        if (lastException instanceof ApplicationException) return lastException.getMessage();
        return ApplicationRuntime.getProperty(CoreMessageDefines.APPLICATION_ERROR);
    }

    public String getErrorAsStringFromDebug() {
        final Throwable lastException = this.getLastException();
        if (null == lastException) { return ""; }
        return ExceptionUtils.getFullStackTrace(lastException);
    }

    public void onClickDownloadXml() {
        if (null != this.getXml()) {
            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().fileName("enrollmentCheck.xml").document(this.getXml()).xml(), false);
        }
    }



    public class Item {
        private final EnrXmlEnrollmentStepState.Competition xmlCompetition;
        private final EnrXmlEnrollmentStepState.Group xmlGroup;
        public Item(final EnrXmlEnrollmentStepState.Competition xmlCompetition, final EnrXmlEnrollmentStepState.Group xmlGroup) {
            this.xmlCompetition = xmlCompetition;
            this.xmlGroup = xmlGroup;
        }

        public String getTitle() {
            return xmlGroup.title;
        }
        public Long getCompetitionId() {
            return xmlCompetition.id;
        }
        public Long getTargetAdmissionKindId() {
            return xmlGroup.taKindId;
        }
        public int getPlan() {
            return xmlGroup.plan;
        }
        public int getEnrolledBefore() {
            return xmlGroup.enrolledList.size();
        }
        public int getAvailable() {
            return EnrXmlEnrollmentCheckData.this.recommendation ?  xmlGroup.checkRec.stepItemAvailable : xmlGroup.checkEnr.stepItemAvailable;
        }
        public int getPlanBalance() {
            return EnrXmlEnrollmentCheckData.this.recommendation ?  xmlGroup.checkRec.planBalance : xmlGroup.checkEnr.planBalance;
        }
        public int getEntrantBalance() {
            return EnrXmlEnrollmentCheckData.this.recommendation ?  xmlGroup.checkRec.entrantBalance : xmlGroup.checkEnr.entrantBalance;
        }
        public int getReMarked() {
            return EnrXmlEnrollmentCheckData.this.recommendation ?  xmlGroup.checkRec.extractRecByOther : xmlGroup.checkEnr.extractMarkedEnrByOther;
        }
        public int getMarkedByThis() {
            return EnrXmlEnrollmentCheckData.this.recommendation ?  xmlGroup.checkRec.stepItemRecByThis : xmlGroup.checkEnr.stepItemMarkedEnrByThis;
        }
        public int getMarkedByOther() {
            return EnrXmlEnrollmentCheckData.this.recommendation ?  xmlGroup.checkRec.stepItemRecByOther : xmlGroup.checkEnr.stepItemMarkedEnrByOther;
        }
        public boolean isError() {
            return EnrXmlEnrollmentCheckData.this.recommendation ?  xmlGroup.checkRec.errorList.size() > 0 : xmlGroup.checkEnr.errorList.size() > 0;
        }
        public List<String> getErrorList() {
            return EnrXmlEnrollmentCheckData.this.recommendation ?  xmlGroup.checkRec.errorList : xmlGroup.checkEnr.errorList;
        }
    }

    // getters and setters

    public String getRowClass()
    {
        if (getItem() == null || getItemList() == null) {
            return "";
        }

        if (getItem().isError()) {
            return "error";
        } else {
            return (getItemList().indexOf(getItem()) & 1) == 0 ? "list-row-even" : "list-row-odd";
        }
    }

    public byte[] getXml() { return this.xml; }
    public void setXml(final byte[] xml) { this.xml = xml; }

    public int getVersion() { return this.version; }
    public void setVersion(final int version) { this.version = version; }

    public Throwable getLastException() { return this.lastException; }
    public void setLastException(final Throwable lastException) { this.lastException = lastException; }

    public List<Item> getItemList() { return this.itemList; }
    public void setItemList(final List<Item> itemList) { this.itemList = itemList; }

    public Item getItem() { return this.item; }
    public void setItem(final Item item) { this.item = item; }


}
