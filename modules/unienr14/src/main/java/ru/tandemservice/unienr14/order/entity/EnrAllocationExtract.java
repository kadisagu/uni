package ru.tandemservice.unienr14.order.entity;

import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.order.entity.gen.EnrAllocationExtractGen;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unimove.UnimoveDefines;

/**
 * Выписка приказа о распределении по образовательным программам
 */
public class EnrAllocationExtract extends EnrAllocationExtractGen
{
    @Override
    public EnrRequestedCompetition getRequestedCompetition()
    {
        return getEntity();
    }

    @Override
    public EnrCompetition getCompetition()
    {
        return getEntity().getCompetition();
    }

    @Override
    public EnrEntrant getEntrant()
    {
        return getEntity().getRequest().getEntrant();
    }

    @Override
    public int compareTo(Object o)
    {
        if (o instanceof EnrAllocationExtract) {
            EnrAllocationExtract e = (EnrAllocationExtract) o;
            int i = Long.compare(this.getId(), e.getId());
            if (i != 0) { return i; }
            return Person.FULL_FIO_AND_ID_COMPARATOR.compare(getEntity().getRequest().getEntrant().getPerson(), e.getEntity().getRequest().getEntrant().getPerson());
        }
        return 0;
    }

    @Override
    public void setEntity(Object entity)
    {
        setEntity((EnrRequestedCompetition)entity);
    }

    @Override
    public boolean isCommitted()
    {
        return UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED.equals(getState().getCode());
    }

    @Override
    public void setCommitted(boolean committed)
    {
    }
}