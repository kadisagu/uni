package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.xml;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.tandemframework.common.catalog.entity.ICatalogItem;

import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.select.EnrSelectionItem;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.select.EnrSelectionGroup;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.select.IEnrSelectionItemPrevEnrollmentInfo;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.select.IEnrSelectionPrevEnrollmentFact;

/**
 * @author vdanilov
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name = "state")
public class EnrXmlEnrollmentStepState {

    @XmlElements(@XmlElement(type=Competition.class, name="competition"))
    public List<Competition> competitionList = new ArrayList<Competition>();

    @XmlType
    public static class CodeWithTitle
    {
        public CodeWithTitle() {}
        public CodeWithTitle(String code, String title) { this.code = code; this.title = title; }
        public CodeWithTitle(ICatalogItem item) { this(item.getCode(), item.getTitle()); }

        @XmlAttribute(name="code", required=true)
        public String code;

        @XmlAttribute(name="title", required=true)
        public String title;
    }


    @XmlType
    public static class Competition
    {
        public Competition() {}
        public Competition(EnrCompetition comp) {
            id = comp.getId();
            title = comp.getTitle();
            requestType = new CodeWithTitle(comp.getRequestType());
            competitionType = new CodeWithTitle(comp.getType());
            compensationType = new CodeWithTitle(comp.getType().getCompensationType());
            levelRequired = new CodeWithTitle(comp.getEduLevelRequirement());
            plan = comp.getPlan();
        }

        @XmlAttribute(required=true)
        public long id = -1;

        @XmlElement(name="title", required=true)
        public String title;

        @XmlElement(name="request-type", required=true)
        public CodeWithTitle requestType;

        @XmlElement(name="competition-type", required=true)
        public CodeWithTitle competitionType;

        @XmlElement(name="compensation-type", required=false)
        public CodeWithTitle compensationType;

        @XmlElement(name="base-level", required=true)
        public CodeWithTitle levelRequired;

        @XmlElement(name="plan", required=true)
        public int plan = 0;

        @XmlElements(@XmlElement(type=Group.class, name="group"))
        public List<Group> groupList = new ArrayList<Group>();

    }

    @XmlType
    public static class Group {

        public Group() {}
        public Group(EnrSelectionGroup group) {
            groupCompositeId = group.getKey().getCompositeId();
            taKindId = group.getKey().getTaKindId();
            title = group.getKey().getTitle();
            plan = group.getCompetitionPlan();
        }

        @XmlAttribute(name="group-id", required=true)
        public String groupCompositeId;

        @XmlAttribute(name="ta-kind-id", required=false)
        public Long taKindId = null;

        @XmlElement(name="title", required=true)
        public String title;

        @XmlElement(name="plan", required=true)
        public int plan = 0;

        @XmlElements(@XmlElement(type=ExtractInfo.class, name="enrolled"))
        public List<ExtractInfo> enrolledList = new ArrayList<ExtractInfo>();

        @XmlElements(@XmlElement(type=StepItem.class, name="item"))
        public List<StepItem> itemList = new ArrayList<StepItem>();

        @XmlElement(name="check-status-rec", required=false)
        public GroupRecCheckStatus checkRec;

        @XmlElement(name="check-status-enr", required=false)
        public GroupEnrCheckStatus checkEnr;

    }


    @XmlType
    public static class StepItem {

        public StepItem() {}
        public StepItem(EnrSelectionItem item) {
            entrantId = item.getEntrant().getId();
            entrantFio = item.getEntrantFio();
            included = item.getStepItem().isIncluded();
            archived = item.getStepItem().isEntrantArchived();
            takeAway = item.getStepItem().isEntrantRequestTakeAwayDocuments();
            params.parallel = item.getRequestedCompetition().isParallel();
            params.priority = item.getRequestedCompetition().getPriority();
            rating.requestedCompetitionId = item.getRequestedCompetition().getId();
            rating.position = item.getRatingItem().getPosition();
            enrollment.originalIn = item.getStepItem().isOriginalIn();
            enrollment.accepted = item.getStepItem().isAccepted();
            enrollment.enrollmentAvailable = item.getStepItem().isEnrollmentAvailable();

            flagRec = item.getStepItem().isRecommended();
            flagEnr = item.getStepItem().isShouldBeEnrolled();

            skipped = item.getSelectedInfo();
        }

        @XmlAttribute(name="entrant-id", required=true)
        public long entrantId = -1;

        @XmlAttribute(name="included", required=true)
        public boolean included = false;

        @XmlAttribute(name="archived", required=true)
        public boolean archived = false;

        @XmlAttribute(name="take-away", required=true)
        public boolean takeAway = false;

        @XmlElement(name="skipped", required=false)
        public String skipped;

        @XmlElement(name="entrant", required=true)
        public String entrantFio;

        @XmlElement(name="rating", required=true)
        public Rating rating = new Rating();

        @XmlElement(name="params", required=true)
        public StepItemParams params = new StepItemParams();

        @XmlElements(@XmlElement(type=ExtractInfo.class, name="reenroll-from"))
        public List<ExtractInfo> reenrollFromList = new ArrayList<ExtractInfo>();

        @XmlElements(@XmlElement(type=ExtractInfo.class, name="add-enroll"))
        public List<ExtractInfo> addEnrollList = new ArrayList<ExtractInfo>();

        @XmlElement(name="enr-condition", required=true)
        public CanEnrollStatus enrollment = new CanEnrollStatus();

        @XmlElement(name="select-rec", required=true)
        public boolean flagRec = false;

        @XmlElement(name="select-enr", required=true)
        public boolean flagEnr = false;

    }

    @XmlType
    public static class StepItemParams {

        @XmlAttribute(name="parallel", required=true)
        public boolean parallel = false;

        @XmlAttribute(name="priority", required=true)
        public int priority = -1;

    }

    @XmlType
    public static class CanEnrollStatus {

        @XmlAttribute(name="original-in", required=false)
        public Boolean originalIn;

        @XmlAttribute(name="accepted", required=false)
        public Boolean accepted;

        @XmlAttribute(name="enrollment-available", required=false)
        public Boolean enrollmentAvailable;
    }

    @XmlType
    public static class Rating {

        @XmlAttribute(name="requested-competition-id", required=true)
        public long requestedCompetitionId = -1;

        @XmlAttribute(name="position", required=true)
        public int position = -1;

    }

    @XmlType
    public static class ExtractInfo {

        public ExtractInfo() {}
        public ExtractInfo(IEnrSelectionPrevEnrollmentFact extract) {
            requestedCompetitionId = extract.getRequestedCompetition().getId();
            orderInfo = extract.getInfo();
            entrantId = extract.getRequestedCompetition().getRequest().getEntrant().getId();
            entrantFio = extract.getRequestedCompetition().getRequest().getEntrant().getFio();
        }

        public ExtractInfo(EnrSelectionGroup selectionGroup, IEnrSelectionItemPrevEnrollmentInfo info) {
            extractGroupCompositeId = (null == selectionGroup ? null : selectionGroup.getKey().getCompositeId());
            requestedCompetitionId = info.getRequestedCompetition().getId();
            orderInfo = info.getInfo();
            resolution = new CodeWithTitle(info.getSolution());
        }

        @XmlAttribute(name="group-id", required=false)
        public String extractGroupCompositeId;

        @XmlAttribute(name="entrant-id", required=false)
        public Long entrantId;

        @XmlAttribute(name="requested-competition-id", required=true)
        public long requestedCompetitionId = -1;

        @XmlElement(name="entrant", required=false)
        public String entrantFio;

        @XmlElement(name="resolution", required=false)
        public CodeWithTitle resolution;

        @XmlElement(name="info", required=false)
        public String orderInfo;

    }

    @XmlType
    public static class GroupRecCheckStatus {

        @XmlElement(name="item-available", required=true)
        public int stepItemAvailable;

        @XmlElement(name="plan-balance", required=true)
        public int planBalance;

        @XmlElement(name="entrant-balance", required=true)
        public int entrantBalance;

        @XmlElement(name="extract-rec-by-other", required=true)
        public int extractRecByOther;

        @XmlElement(name="item-rec-by-this", required=true)
        public int stepItemRecByThis;

        @XmlElement(name="item-rec-by-other", required=true)
        public int stepItemRecByOther;

        @XmlElement(name="item-rec-by-this-or-other", required=true)
        public int stepItemRecByThisOrOther;

        @XmlElements(@XmlElement(name="error", type=String.class))
        public List<String> errorList = new ArrayList<String>();

    }

    @XmlType
    public static class GroupEnrCheckStatus {

        @XmlElement(name="item-available", required=true)
        public int stepItemAvailable;

        @XmlElement(name="plan-balance", required=true)
        public int planBalance;

        @XmlElement(name="entrant-balance", required=true)
        public int entrantBalance;

        @XmlElement(name="extract-marked-enr-by-other", required=true)
        public int extractMarkedEnrByOther;

        @XmlElement(name="item-marked-enr-by-this", required=true)
        public int stepItemMarkedEnrByThis;

        @XmlElement(name="item-marked-enr-by-other", required=true)
        public int stepItemMarkedEnrByOther;

        @XmlElement(name="item-available-marked-enr-by-this-or-other", required=true)
        public int stepItemAvailableMarkedEnrByThisOrOther;

        @XmlElements(@XmlElement(name="error", type=String.class))
        public List<String> errorList = new ArrayList<String>();

    }


}
