/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsBenefitDistributionAdd.logic;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.RtfRowIntercepterRawText;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.IRtfRowIntercepter;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonCatalogUtil;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unienr14.catalog.entity.EnrBenefitCategory;
import ru.tandemservice.unienr14.catalog.entity.EnrBenefitType;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm;
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceBenefit;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsBenefitDistributionAdd.EnrReportEntrantsBenefitDistributionAddUI;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportUtil;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.FilterParametersPrinter;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder;
import ru.tandemservice.unienr14.report.entity.EnrReportEntrantsBenefitDistribution;
import ru.tandemservice.unienr14.report.entity.EnrReportSelectedEduProgramsSummary;
import ru.tandemservice.unienr14.request.entity.*;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 09.07.2014
 */
public class EnrReportEntrantsBenefitDistributionDao extends UniBaseDao implements IEnrReportEntrantsBenefitDistributionDao {

    List<EnrBenefitCategory> benefitCategoryList = new ArrayList<>();

    @Override
    public long createReport(EnrReportEntrantsBenefitDistributionAddUI model) {

        EnrReportEntrantsBenefitDistribution report = new EnrReportEntrantsBenefitDistribution();

        report.setFormingDate(new Date());

        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateSelector().getDateFrom());
        report.setDateTo(model.getDateSelector().getDateTo());
        report.setStage(model.getStageSelector().getStage().getTitle());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, EnrReportSelectedEduProgramsSummary.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE, EnrReportSelectedEduProgramsSummary.P_COMPENSATION_TYPE, "shortTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, EnrReportSelectedEduProgramsSummary.P_PROGRAM_FORM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, EnrReportSelectedEduProgramsSummary.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, EnrReportSelectedEduProgramsSummary.P_FORMATIVE_ORG_UNIT, "fullTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, EnrReportSelectedEduProgramsSummary.P_PROGRAM_SUBJECT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, EnrReportSelectedEduProgramsSummary.P_EDU_PROGRAM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, EnrReportSelectedEduProgramsSummary.P_PROGRAM_SET, "title");
        if (model.getParallelSelector().isParallelActive())
            report.setParallel(model.getParallelSelector().getParallel().getTitle());
        if (model.isFilterByBenefitType())
            report.setBenefitType(UniStringUtils.join(model.getBenefitTypeList(), EnrBenefitType.title().s(), "; "));
        report.setRowFormingType(model.getRowFormingType().getTitle());

        DatabaseFile content = new DatabaseFile();

        content.setContent(buildReport(model));
        content.setFilename("EnrReportEntrantOnExam.rtf");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        save(content);

        report.setContent(content);
        save(report);

        return report.getId();
    }

    private byte[] buildReport(EnrReportEntrantsBenefitDistributionAddUI model)
    {

        //rtf
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(EnrScriptItem.class, EnrScriptItemCodes.REPORT_ENTRANTS_BENEFIT_DISTRIBUTION);
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();

        EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder()
                .notArchiveOnly()
                .filter(model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo())
                .filter(model.getEnrollmentCampaign());

        filterAddon.applyFilters(requestedCompDQL, requestedCompDQL.competition());

        model.getStageSelector().applyFilter(requestedCompDQL, requestedCompDQL.reqComp());

        if (model.getParallelSelector().isParallelActive())
        {
            if (model.getParallelSelector().isParallelOnly())
                requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.TRUE)));
            if (model.getParallelSelector().isSkipParallel())
                requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.FALSE)));
        }

        // Формируем запрос
        requestedCompDQL
                .joinEntity("reqComp", DQLJoinType.left, EnrChosenEntranceExam.class, "vvi", eq(property("reqComp"), property(EnrChosenEntranceExam.requestedCompetition().fromAlias("vvi"))))
                .joinEntity("vvi", DQLJoinType.left, EnrChosenEntranceExamForm.class, "vvif", eq(property("vvi"), property(EnrChosenEntranceExamForm.chosenEntranceExam().fromAlias("vvif"))))
                .joinEntity("vvif", DQLJoinType.full, EnrEntrantMarkSourceBenefit.class, "markBenefit", eq(property(EnrEntrantMarkSourceBenefit.chosenEntranceExamForm().fromAlias("markBenefit")), property("vvif")))
                .joinEntity("reqComp", DQLJoinType.full, EnrRequestedCompetitionExclusive.class, "reqCompEX", eq(property("reqComp"), property("reqCompEX")))
                .joinEntity("reqComp", DQLJoinType.full, EnrRequestedCompetitionNoExams.class, "reqCompNE", eq(property("reqComp"), property("reqCompNE")));



        requestedCompDQL
                .column(property("reqComp"))
                .column(property("request", EnrEntrantRequest.benefitCategory()))
                .column(property("request"));

        // Собираем общий массив EnrRequestedCompetition - EnrBenefitCategory - IEnrEntrantBenefitStatement
        List<Object[]> fromQueryReqCompsAndBenefits = getList(requestedCompDQL);

        requestedCompDQL.resetColumns()
                .column(property("reqComp"))
                .column(property("reqCompEX", EnrRequestedCompetitionExclusive.benefitCategory()))
                .column(property("reqCompEX"));

        List<Object[]> objects = getList(requestedCompDQL);

        fromQueryReqCompsAndBenefits.addAll(objects);


        requestedCompDQL.resetColumns()
                .column(property("reqComp"))
                .column(property("markBenefit", EnrEntrantMarkSourceBenefit.benefitCategory()))
                .column(property("markBenefit"));

        objects = getList(requestedCompDQL);

        fromQueryReqCompsAndBenefits.addAll(objects);

        requestedCompDQL.resetColumns()
                .column(property("reqComp"))
                .column(property("reqCompNE", EnrRequestedCompetitionNoExams.benefitCategory()))
                .column(property("reqCompNE"));

        objects = getList(requestedCompDQL);

        fromQueryReqCompsAndBenefits.addAll(objects);


        Map<EnrBenefitType, Map<EnrBenefitCategory, Set<EnrRequestedCompetition>>> t1dataMap = new TreeMap<>(CommonCatalogUtil.CATALOG_CODE_COMPARATOR);
        Map<IEnrEntrantBenefitStatement, PairKey<EnrBenefitCategory, Set<EnrRequestedCompetition>>> t2dataMap = new HashMap<>();
        Set<IEnrEntrantBenefitStatement> statementSet = new HashSet<>();

        for (Object[] row : fromQueryReqCompsAndBenefits)
        {
            EnrBenefitCategory category = (EnrBenefitCategory)row[1];

            if (model.isFilterByBenefitType() && !model.getBenefitTypeList().contains(category.getBenefitType()))
                continue;

            IEnrEntrantBenefitStatement benefitStatement = (IEnrEntrantBenefitStatement) row[2];
            EnrRequestedCompetition reqComp = (EnrRequestedCompetition) row[0];

            statementSet.add(benefitStatement);
            PairKey<EnrBenefitCategory, Set<EnrRequestedCompetition>> pair = new PairKey<>(category, new HashSet<>());
            pair.getSecond().add(reqComp);
            if (!t2dataMap.containsKey(benefitStatement)) {
                t2dataMap.put(benefitStatement, pair);
            }

            Map<EnrBenefitCategory, Set<EnrRequestedCompetition>> benefitCategoryMap = SafeMap.safeGet(t1dataMap, category.getBenefitType(), HashMap.class);
            Set<EnrRequestedCompetition> requestedCompetitionSet = SafeMap.safeGet(benefitCategoryMap, category, HashSet.class);
            requestedCompetitionSet.add(reqComp);
        }

        // hTable
        FilterParametersPrinter filterParametersPrinter = new FilterParametersPrinter();
        filterParametersPrinter.setParallelSelector(model.getParallelSelector());
        filterParametersPrinter.setStageSelector(model.getStageSelector());
        if (model.isFilterByBenefitType())
            filterParametersPrinter.addVariableParameter("Вид особого права", UniStringUtils.join(model.getBenefitTypeList(), EnrBenefitType.title().s(), "\\line "));
        filterParametersPrinter.addVariableParameter("Формирование строк отчета", model.getRowFormingType().getTitle());

        List<String[]> hTable = filterParametersPrinter.getTable(model.getCompetitionFilterAddon(), model.getDateSelector());

        RtfTableModifier headerModifier = new RtfTableModifier();
        headerModifier.put("H", hTable.toArray(new String[hTable.size()][]));
        headerModifier.put("H", new RtfRowIntercepterRawText());
        headerModifier.modify(document);

        // DateForm
        new RtfInjectModifier().put("dateForm", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date())).modify(document);

        //Чистим документ
        RtfTable t1TableElement = (RtfTable) UniRtfUtil.findElement(document.getElementList(), "T");
        document.getElementList().remove(t1TableElement);
        RtfTable t2TableElement = (RtfTable) UniRtfUtil.findElement(document.getElementList(), "T2");
        document.getElementList().remove(t2TableElement);

        StringBuilder programLabel = new StringBuilder();
        if (model.getRowFormingType().getId() == 1L)
            programLabel.append("Набор образовательных программ");
        else programLabel.append("Направление (специальность, профессия)");

        // Заполняем Т-таблицы, заносим их обратно в документ
        for (Map.Entry<EnrBenefitType, Map<EnrBenefitCategory, Set<EnrRequestedCompetition>>> benefitTypeEntry : t1dataMap.entrySet())
        {
            List<String[]> t1Table = buildT1Table(model, benefitTypeEntry.getValue());

            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));

            document.getElementList().add(t1TableElement.getClone());

            new RtfInjectModifier()
                    .put("BenefitType", "Категории абитуриентов, обладающих особым правом «" + benefitTypeEntry.getKey().getTitle() + "»")
                    .modify(document);

            RtfTableModifier tableModifier = new RtfTableModifier();
            tableModifier.put("T", t1Table.toArray(new String[t1Table.size()][]));

            // Интерцептор сплитит ячейку под категории и болдит строку "Итого"
            tableModifier.put("T", new IRtfRowIntercepter() {
                int memRowIndex = 0;

                // Сплит
                @Override
                public void beforeModify(RtfTable table, int currentRowIndex) {
                    if (benefitCategoryList.size() > 0) {
                        int[] splitter = new int[benefitCategoryList.size()];
                        Arrays.fill(splitter, 1);
                        RtfUtil.splitRow(table.getRowList().get(1), 2, (newCell, index) -> newCell.addElement(RtfBean.getElementFactory().createRtfText(benefitCategoryList.get(index).getShortTitle())), splitter);
                        RtfUtil.splitRow(table.getRowList().get(2), 2, null, splitter);
                    }
                    else RtfUtil.splitRow(table.getRowList().get(1), 2, (newCell, index) -> newCell.addElement(RtfBean.getElementFactory().createRtfText("")), new int[]{1});
                }

                // Болд
                @Override
                public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value) {
                    if (value.equals("Итого") || (rowIndex == memRowIndex && rowIndex !=0))
                    {
                        memRowIndex = rowIndex;
                        RtfString string = new RtfString();
                        string
                                .boldBegin()
                                .append(value)
                                .boldEnd();
                        return string.toList();
                    }
                    return null;
                }

                @Override
                public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex) {

                }
            } );
            new RtfInjectModifier().put("program", programLabel.toString()).modify(document);
            tableModifier.modify(document);

            // Чистим лист с категорями
            benefitCategoryList.clear();
        }

        // Запрос с подтверждениями
        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder
                .fromEntity(EnrEntrantBenefitProof.class, "proof")
                .where(in(property(EnrEntrantBenefitProof.benefitStatement().fromAlias("proof")), statementSet));

        List<EnrEntrantBenefitProof> proofList = getList(builder);

        Map<IEnrEntrantBenefitStatement,Set<EnrEntrantBenefitProof>> proofMap = SafeMap.get(HashSet.class);

        for (EnrEntrantBenefitProof enrEntrantBenefitProof : proofList) {

            proofMap.get(enrEntrantBenefitProof.getBenefitStatement()).add(enrEntrantBenefitProof);
        }

        // Т2 таблица
        List<String[]> t2Table = new ArrayList<>();

        for (Map.Entry<IEnrEntrantBenefitStatement, PairKey<EnrBenefitCategory, Set<EnrRequestedCompetition>>> entry :  t2dataMap.entrySet())
        {
            List<String> row = new ArrayList<>();
            row.add(entry.getValue().getFirst().getCode());

            Set<EnrProgramSetBase> enrProgramSetBases = new TreeSet<>(ITitled.TITLED_COMPARATOR);
            Set<EduProgramSubject> subjectSet = new TreeSet<>((o1, o2) -> o1.getTitleWithCode().compareToIgnoreCase(o2.getTitleWithCode()));

            StringBuilder fullFio = new StringBuilder();
            StringBuilder reqNumber = new StringBuilder();
            boolean isOriginEduDocumentsHandledIn = false;
            boolean firstIter = true;

            for (EnrRequestedCompetition reqComp : entry.getValue().getSecond())
            {
                if (firstIter) {
                    fullFio.append(reqComp.getRequest().getIdentityCard().getFullFio());
                    reqNumber.append(reqComp.getRequest().getStringNumber());
                    isOriginEduDocumentsHandledIn = reqComp.isOriginalDocumentHandedIn();
                    firstIter = false;
                }
                enrProgramSetBases.add(reqComp.getCompetition().getProgramSetOrgUnit().getProgramSet());
                subjectSet.add(reqComp.getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramSubject());



            }


            row.add(fullFio.toString());

            StringBuilder programCell = new StringBuilder();

            if (model.getRowFormingType().getId() == 1L)
                for (EnrProgramSetBase enrProgramSetBase : enrProgramSetBases) {
                    programCell.append(enrProgramSetBase.getTitle()).append("\\line ");
                }
            else
                for (EduProgramSubject subject : subjectSet) {
                    programCell.append(subject.getTitleWithCode()).append("\\line ");
                }

            row.add(programCell.toString());


            row.add(reqNumber.toString());
            row.add(isOriginEduDocumentsHandledIn ? "Да" : "Нет");
            row.add(entry.getValue().getFirst().getBenefitType().getShortTitle() + " : " + entry.getValue().getFirst().getShortTitle());
            StringBuilder documents =  new StringBuilder();
            if (proofMap.containsKey(entry.getKey()))
            {
                boolean first = true;
                for (EnrEntrantBenefitProof proof : proofMap.get(entry.getKey()))
                {
                    if (!first)
                        documents.append("\\line");
                    documents.append(proof.getDocument().getDisplayableTitle());
                    first = false;
                }
                row.add(documents.toString());
            }
            else row.add("документ не приложен");

            t2Table.add(row.toArray(new String[row.size()]));

        }
        int num = 0;

        // Сортируем по ФИО и по коду категории бенефита
        Collections.sort(t2Table, (o1, o2) -> {
            int res = o1[1].compareTo(o2[1]);
            if (res != 0) return res;
            return o1[0].compareTo(o2[0]);
        });

        // Нумеруем
        for (String[] strings : t2Table) {
            strings[0] = String.valueOf(++num);
        }

        // Заносим в документ
        document.getElementList().add(t2TableElement.getClone());

        new RtfInjectModifier().put("program", programLabel.toString()).modify(document);
        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("T2", t2Table.toArray(new String[t2Table.size()][]));
        tableModifier.put("T2", new RtfRowIntercepterRawText());
        tableModifier.modify(document);

        return RtfUtil.toByteArray(document);
    }


    private List<String[]> buildT1Table(EnrReportEntrantsBenefitDistributionAddUI model, Map<EnrBenefitCategory, Set<EnrRequestedCompetition>> benefitMap)
    {
        Comparator<EnrBenefitCategory> categoryComparator = (o1, o2) -> {
            int res = o1.getCode().compareTo(o2.getCode());
            if (res != 0) return res;
            else return o1.getShortTitle().compareTo(o2.getShortTitle());
        };

        Comparator<EduProgramSubject> subjectComparator = (o1, o2) -> o1.getTitleWithCode().compareTo(o2.getTitleWithCode());



        List<String[]> result = new ArrayList<>();

        if (model.getRowFormingType().getId() == 1L)
        {
            Map<EnrProgramSetBase, Map<EnrBenefitCategory, Set<EnrEntrant>>> dataMap = new TreeMap<>(ITitled.TITLED_COMPARATOR);
            Set<EnrBenefitCategory> benefits = new HashSet<>();

            for (Map.Entry<EnrBenefitCategory, Set<EnrRequestedCompetition>> entry : benefitMap.entrySet())
            {
                if (!benefits.contains(entry.getKey()))
                    benefits.add(entry.getKey());
                for (EnrRequestedCompetition reqComp : entry.getValue())
                {
                    EnrProgramSetBase programSetBase = reqComp.getCompetition().getProgramSetOrgUnit().getProgramSet();
                    if (!dataMap.containsKey(programSetBase))
                        dataMap.put(programSetBase, new TreeMap<>(categoryComparator));
                    if (!dataMap.get(programSetBase).containsKey(entry.getKey()))
                        dataMap.get(programSetBase).put(entry.getKey(), new HashSet<>());
                    dataMap.get(programSetBase).get(entry.getKey()).add(reqComp.getRequest().getEntrant());
                }

            }

            benefitCategoryList.addAll(benefits);
            Collections.sort(benefitCategoryList, categoryComparator);

            int[] totalCounts = new int[benefits.size()];
            int totalInType = 0;

            for (Map.Entry<EnrProgramSetBase, Map<EnrBenefitCategory, Set<EnrEntrant>>> programSetEntry : dataMap.entrySet())
            {
                List<String> row = new ArrayList<>();
                row.add(programSetEntry.getKey().getPrintTitle());
                int total = 0;
                List<Integer> rowValues = new ArrayList<>();

                for (EnrBenefitCategory benefit : benefitCategoryList)
                {
                    if (programSetEntry.getValue().containsKey(benefit)){
                        rowValues.add(programSetEntry.getValue().get(benefit).size());
                        total += programSetEntry.getValue().get(benefit).size();
                    }
                    else rowValues.add(0);
                }

                row.add(String.valueOf(total));
                for (int i = 0; i < rowValues.size(); i++) {
                    totalCounts[i] += rowValues.get(i);
                    row.add(String.valueOf(rowValues.get(i)));
                }

                totalInType += total;

                result.add(row.toArray(new String[row.size()]));
            }


            List<String> row = new ArrayList<>();

            row.add("Итого");
            row.add(String.valueOf(totalInType));

            for (int totalCount : totalCounts) {
                row.add(String.valueOf(totalCount));
            }

            result.add(row.toArray(new String[row.size()]));

        }
        else
        {
            Map<EduProgramSubject, Map<EnrBenefitCategory, Set<EnrEntrant>>> dataMap = new TreeMap<>(subjectComparator);
            Set<EnrBenefitCategory> benefits = new HashSet<>();

            for (Map.Entry<EnrBenefitCategory, Set<EnrRequestedCompetition>> entry : benefitMap.entrySet())
            {
                if (!benefits.contains(entry.getKey()))
                    benefits.add(entry.getKey());
                for (EnrRequestedCompetition reqComp : entry.getValue())
                {
                    EduProgramSubject programSubject = reqComp.getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramSubject();
                    if (!dataMap.containsKey(programSubject))
                        dataMap.put(programSubject, new TreeMap<>(categoryComparator));
                    if (!dataMap.get(programSubject).containsKey(entry.getKey()))
                        dataMap.get(programSubject).put(entry.getKey(), new HashSet<>());
                    dataMap.get(programSubject).get(entry.getKey()).add(reqComp.getRequest().getEntrant());
                }


            }


            benefitCategoryList.addAll(benefits);
            Collections.sort(benefitCategoryList, categoryComparator);

            int[] totalCounts = new int[benefits.size()];
            int totalInType = 0;

            for (Map.Entry<EduProgramSubject, Map<EnrBenefitCategory, Set<EnrEntrant>>> programSetEntry : dataMap.entrySet())
            {
                List<String> row = new ArrayList<>();
                row.add(programSetEntry.getKey().getTitleWithCode());
                int total = 0;
                List<Integer> rowValues = new ArrayList<>();

                for (EnrBenefitCategory benefit : benefitCategoryList)
                {
                    if (programSetEntry.getValue().containsKey(benefit)){
                        rowValues.add(programSetEntry.getValue().get(benefit).size());
                        total += programSetEntry.getValue().get(benefit).size();
                    }
                    else rowValues.add(0);
                }

                row.add(String.valueOf(total));
                for (int i = 0; i < rowValues.size(); i++) {
                    totalCounts[i] += rowValues.get(i);
                    row.add(String.valueOf(rowValues.get(i)));
                }

                totalInType += total;

                result.add(row.toArray(new String[row.size()]));
            }

            List<String> row = new ArrayList<>();

            row.add("Итого");
            row.add(String.valueOf(totalInType));

            for (int totalCount : totalCounts) {
                row.add(String.valueOf(totalCount));
            }

            result.add(row.toArray(new String[row.size()]));
        }


        return result;
    }
}
