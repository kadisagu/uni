package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x7_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.7")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrEntrantRequest

		// создано обязательное свойство generalEduSchool
		{
			// создать колонку
			tool.createColumn("enr14_request_t", new DBColumn("generaleduschool_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			java.lang.Boolean defaultGeneralEduSchool = false;
			tool.executeUpdate("update enr14_request_t set generaleduschool_p=? where generaleduschool_p is null", defaultGeneralEduSchool);

            defaultGeneralEduSchool = true;
			tool.executeUpdate("update enr14_request_t set generaleduschool_p=? where enr14_request_t.id IN (SELECT a.id FROM enr14_request_t AS a INNER JOIN person_edu_doc_t AS b ON a.edudocument_id = b.id INNER JOIN c_edu_level_t AS c ON b.edulevel_id = c.id WHERE c.code_p LIKE '2013.1%')", defaultGeneralEduSchool);

			// сделать колонку NOT NULL
			tool.setColumnNullable("enr14_request_t", "generaleduschool_p", false);

		}


    }
}