/* $Id:$ */
package ru.tandemservice.unienr14.ws.order;

import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Date;
import java.util.List;

/**
 * @author oleyba
 * @since 7/20/14
 */
@WebService
public class EnrOrderService
{
    /**
     * Получение информации о приказах о зачислении
     *
     * @param enrollmentCampaignTitle название приемной кампании
     * @param dateFrom                Дата приказа с
     * @param dateTo                  Дата приказа по
     * @param compensationTypeId      Код вида возмещения затрат
     * @param formativeOrgUnitIds     Айди формирующих подразделений
     * @param programFormIds          Коды форм освоения
     * @param withPrintData           true, если нужно прицеплять печатные формы, false в противном случае
     * @return Инфомрация о приказах о зачислении
     */
    public EnrOrderEnvironmentNode getOrderData(
            @WebParam(name = "enrollmentCampaignTitle") String enrollmentCampaignTitle,
            @WebParam(name = "dateFrom") Date dateFrom,
            @WebParam(name = "dateTo") Date dateTo,
            @WebParam(name = "compensationTypeId") String compensationTypeId,
            @WebParam(name = "formativeOrgUnitIds") List<String> formativeOrgUnitIds,
            @WebParam(name = "programFormIds") List<String> programFormIds,
            @WebParam(name = "withPrintData") Boolean withPrintData)
    {
        if (enrollmentCampaignTitle == null)
            throw new RuntimeException("WebParam 'enrollmentCampaignTitle' is not specified!");

        return IEnrOrderServiceDao.INSTANCE.get().getOrderData(enrollmentCampaignTitle, dateFrom, dateTo, compensationTypeId, formativeOrgUnitIds, programFormIds, withPrintData != null ? withPrintData : false);
    }
}
