/* $Id:$ */
package ru.tandemservice.unienr14.settings.bo.EnrCampaignEnrollmentStage.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.settings.bo.EnrCampaignEnrollmentStage.EnrCampaignEnrollmentStageManager;
import ru.tandemservice.unienr14.settings.bo.EnrCampaignEnrollmentStage.ui.AddEdit.EnrCampaignEnrollmentStageAddEdit;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrEntrantDocument.EnrEntrantDocumentManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

/**
 * @author oleyba
 * @since 3/17/14
 */
public class EnrCampaignEnrollmentStageListUI extends UIPresenter
{
    @Override
    public void onComponentRefresh()
    {
        final EnrEnrollmentCampaign defaultCampaign = EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign();
        getSettings().set(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, defaultCampaign);
    }

    public void onEnrCampaignRefresh()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrEnrollmentCampaign());

        if (getEnrEnrollmentCampaign() != null)
            EnrEntrantDocumentManager.instance().typeDao().doSetEnrCampDefaultSettings(getEnrEnrollmentCampaign().getId());
    }

    public void onClickAddDefaultDates() {
        EnrCampaignEnrollmentStageManager.instance().dao().doAddDefaultDates(getEnrEnrollmentCampaign());
    }

    public void onClickDelete() {
        IUniBaseDao.instance.get().delete(getListenerParameterAsLong());
    }

    public void onClickEdit() {
        _uiActivation.asRegionDialog(EnrCampaignEnrollmentStageAddEdit.class).parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong()).activate();
    }

    public void onClickAdd() {
        _uiActivation.asRegionDialog(EnrCampaignEnrollmentStageAddEdit.class).activate();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, getEnrEnrollmentCampaign());
    }

    // presenter

    public boolean isNothingSelected()
    {
        return getEnrEnrollmentCampaign() == null;
    }

    // Getters & Setters

    public EnrEnrollmentCampaign getEnrEnrollmentCampaign()
    {
        return getSettings().get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
    }
}