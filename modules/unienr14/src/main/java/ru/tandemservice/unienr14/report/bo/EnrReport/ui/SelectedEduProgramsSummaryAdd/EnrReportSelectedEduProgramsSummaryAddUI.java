/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.SelectedEduProgramsSummaryAdd;/**
 * @author rsizonenko
 * @since 07.07.2014
 */

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilter;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilterType;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterFormConfig;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import org.tandemframework.tapsupport.component.selection.SimpleListResultBuilder;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrRequestTypeCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.report.bo.EnrReport.EnrReportManager;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportDateSelector;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportParallelSelector;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportStageSelector;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.ui.Pub.EnrReportBasePub;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.EnrReportPersonAdd;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class EnrReportSelectedEduProgramsSummaryAddUI extends UIPresenter {
    private EnrReportDateSelector dateSelector = new EnrReportDateSelector();
    private EnrEnrollmentCampaign enrollmentCampaign;
    private EnrReportParallelSelector parallelSelector = new EnrReportParallelSelector();

    private EnrReportStageSelector stageSelector = new EnrReportStageSelector();

    private IdentifiableWrapper rowFormingType;

    private List<IdentifiableWrapper> rowFormingTypeList;

    private boolean includeDetailSheets;
    private boolean firstPriorityOnly;

    // From UI
    @Override
    public void onComponentRefresh() {
        setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
        setRowFormingTypeList(Arrays.asList(new IdentifiableWrapper(0L, "По направлениям и направленностям (профилям, специализациям)"),
                                            new IdentifiableWrapper(1L, "По наборам ОП и образовательным программам")));

        configUtil(getCompetitionFilterAddon());
    }

    // validate
    private void validate()
    {
        if (dateSelector.getDateFrom().after(dateSelector.getDateTo()))
            _uiSupport.error("Дата, указанная в параметре \"Заявления с\" не должна быть позже даты в параметре \"Заявления по\".", "dateFrom");

        if(getUserContext().getErrorCollector().hasErrors())
            throw new ApplicationException();
    }

    // Listeners
    public void onClickApply() {
        validate();
        Long reportId = EnrReportManager.instance().selectedEduProgramsSummaryDao().createReport(this);
        deactivate();
        _uiActivation.asDesktopRoot(EnrReportBasePub.class)
                .parameter(PUBLISHER_ID, reportId)
                .activate();
    }

    public void onChangeEnrollmentCampaign()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
        final EnrCompetitionFilterAddon entrantRequestUtil = getCompetitionFilterAddon();
        configUtilWhere(entrantRequestUtil);
        dateSelector.refreshDates(getEnrollmentCampaign());
    }
    // Util


    private void configUtil(EnrCompetitionFilterAddon util)
    {
        util
                .configDoubleWidthFilters(false)
                .configUseEnableCheckbox(true)
                .configSettings(getSettingsKey());

        util.clearFilterItems();

        util
                .addFilterItem(EnrCompetitionFilterAddon.REQUEST_TYPE, new CommonFilterFormConfig(true, true, true, false, true, true))
                .addFilterItem(EnrCompetitionFilterAddon.COMPENSATION_TYPE, new CommonFilterFormConfig(true, false, true, false, true, true))
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_FORM, new CommonFilterFormConfig(true, true, true, false, true, true))
                .addFilterItem(EnrCompetitionFilterAddon.COMPETITION_TYPE, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.ENR_ORG_UNIT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.FORMATIVE_ORG_UNIT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SUBJECT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.EDU_PROGRAM, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SET, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG);


        // Фильтруем список видов заявлений в утили
        util.getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE).setModel(new CommonFilterSelectModel() {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o) {
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(EnrRequestType.class, "rt")
                        .column(property("rt"))
                        .where(ne(property("rt", EnrRequestType.code()), value(EnrRequestTypeCodes.SPO)));
                if (o != null)
                    builder.where(((o instanceof Set ? CommonFilterAdditionalFilterType.COLLECTION : CommonFilterAdditionalFilterType.EQ).whereExpression("rt", EnrRequestType.id(), o)));
                if (!StringUtils.isEmpty(filter))
                {
                    //* введена подстрока *//
                    List<IDQLExpression> concatExpressions = new ArrayList<>();
                    concatExpressions.add(property("rt", EnrRequestType.title()));

                    builder.where(likeUpper(DQLFunctions.concat(concatExpressions.toArray(new IDQLExpression[concatExpressions.size()])), value(CoreStringUtils.escapeLike(filter, true))));
                }
                builder.order(property("rt", EnrRequestType.title()), OrderDirection.asc);
                if (builder.createCountStatement(new DQLExecutionContext(getCompetitionFilterAddon().getSession())).<Long>uniqueResult() > 50)
                    builder.top(50);
                List<EnrRequestType> result = builder.createStatement(getCompetitionFilterAddon().getSession()).list();
                return new SimpleListResultBuilder<>(result);
            }
        });


        configUtilWhere(util);
    }
    private void configUtilWhere(EnrCompetitionFilterAddon util)
    {
        util.clearWhereFilter();
        util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EnrCompetition.programSetOrgUnit().orgUnit().enrollmentCampaign(), getEnrollmentCampaign()));
    }


    public EnrCompetitionFilterAddon getCompetitionFilterAddon()
    {
        return (EnrCompetitionFilterAddon) getConfig().getAddon(EnrReportPersonAdd.COMPETITION_FILTERS_ENTRANT_REQUEST);
    }




    // Getters - Setters

    public EnrReportDateSelector getDateSelector() {
        return dateSelector;
    }

    public EnrReportParallelSelector getParallelSelector() {
        return parallelSelector;
    }

    public EnrReportStageSelector getStageSelector() {
        return stageSelector;
    }

    public List<IdentifiableWrapper> getRowFormingTypeList() {
        return rowFormingTypeList;
    }

    public void setRowFormingTypeList(List<IdentifiableWrapper> rowFormingTypeList) {
        this.rowFormingTypeList = rowFormingTypeList;
    }

    public IdentifiableWrapper getRowFormingType() {
        return rowFormingType;
    }

    public void setRowFormingType(IdentifiableWrapper rowFormingType) {
        this.rowFormingType = rowFormingType;
    }

    public boolean isFirstPriorityOnly() {
        return firstPriorityOnly;
    }

    public void setFirstPriorityOnly(boolean firstPriorityOnly) {
        this.firstPriorityOnly = firstPriorityOnly;
    }

    public boolean isIncludeDetailSheets() {
        return includeDetailSheets;
    }

    public void setIncludeDetailSheets(boolean includeDetailSheets) {
        this.includeDetailSheets = includeDetailSheets;
    }

    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        this.enrollmentCampaign = enrollmentCampaign;
    }
}