/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrWSExport.ui.EntrantRating;/**
 * @author rsizonenko
 * @since 27.06.2014
 */

import org.apache.commons.io.IOUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.ws.entrantrating.EnrEntrantRatingEnvironmentNode;
import ru.tandemservice.unienr14.ws.entrantrating.IEnrEntrantRatingServiceDao;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.ByteArrayOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class EnrWSExportEntrantRatingUI extends UIPresenter {

    private EnrEnrollmentCampaign enrEnrollmentCampaign;


    @Override
    public void onComponentRefresh() {
        setEnrEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
    }

    public void onClickSave() {
        EnrEntrantRatingEnvironmentNode node = IEnrEntrantRatingServiceDao.INSTANCE.get().getEntrantRatingTAEnvironmentData(enrEnrollmentCampaign.getTitle());

        ByteArrayOutputStream output = new ByteArrayOutputStream();
        ZipOutputStream zip = new ZipOutputStream(output);

        try {
            ByteArrayOutputStream result = new ByteArrayOutputStream();
            Marshaller marshaller = JAXBContext.newInstance(EnrEntrantRatingEnvironmentNode.class).createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(node, result);

            zip.putNextEntry(new ZipEntry("data.xml"));
            IOUtils.write(result.toByteArray(), zip);
            zip.close();
        } catch (Throwable e)
        {
            throw CoreExceptionUtils.getRuntimeException(e);
        }
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().fileName("Ratings.zip").document(output.toByteArray()).zip(), false);

        deactivate();
    }

    public EnrEnrollmentCampaign getEnrEnrollmentCampaign() {
        return enrEnrollmentCampaign;
    }

    public void setEnrEnrollmentCampaign(EnrEnrollmentCampaign enrEnrollmentCampaign) {
        this.enrEnrollmentCampaign = enrEnrollmentCampaign;
    }
    public boolean isNothingSelected() {
        return getEnrEnrollmentCampaign() == null;
    }
}