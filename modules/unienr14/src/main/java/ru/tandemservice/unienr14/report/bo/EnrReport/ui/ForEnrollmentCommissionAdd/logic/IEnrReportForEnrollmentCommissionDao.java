/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.ForEnrollmentCommissionAdd.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.ForEnrollmentCommissionAdd.EnrReportForEnrollmentCommissionAddUI;

/**
 * @author rsizonenko
 * @since 06.06.2014
 */
public interface IEnrReportForEnrollmentCommissionDao extends INeedPersistenceSupport {
    Long createReport(EnrReportForEnrollmentCommissionAddUI enrReportForEnrollmentCommissionAddUI);
}
