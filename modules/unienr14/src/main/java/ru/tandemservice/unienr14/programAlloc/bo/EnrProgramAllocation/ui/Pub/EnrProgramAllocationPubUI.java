/* $Id: EcProfileDistributionPubUI.java 25114 2012-12-03 11:25:11Z vdanilov $ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.EnrProgramAllocationManager;
import ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.logic.EnrProgramDSHandler;
import ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.ui.EntrantAdd.EnrProgramAllocationEntrantAdd;
import ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.ui.EntrantAdd.EnrProgramAllocationEntrantAddUI;
import ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.util.IEnrPAQuotaFreeWrapper;
import ru.tandemservice.unienr14.programAlloc.entity.EnrProgramAllocation;

import java.util.Map;

/**
 * @author Vasily Zhukov
 * @since 10.05.2012
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "allocation.id", required = true),
        @Bind(key = "selectedTab", binding = "selectedTab")
})
public class EnrProgramAllocationPubUI extends UIPresenter
{
    private EnrProgramAllocation _allocation = new EnrProgramAllocation();
    private String _selectedTab;

    private String quotaUsage;

    @Override
    public void onComponentRefresh()
    {
        setAllocation(DataAccessServices.dao().getNotNull(EnrProgramAllocation.class, getAllocation().getId()));
        IEnrPAQuotaFreeWrapper freeQuotaWrapper = EnrProgramAllocationManager.instance().dao().getFreeQuota(getAllocation().getId());
        Map<Long, String> quotaHtmlDescMap = EnrProgramAllocationManager.instance().dao().getQuotaHTMLDescription(freeQuotaWrapper);
        setQuotaUsage(quotaHtmlDescMap.get(0L));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrProgramAllocationPub.PARAM_ALLOCATION_ID, getAllocation().getId());
    }

    // Listeners

    public void onClickAdd()
    {
        _uiActivation.asRegion(EnrProgramAllocationEntrantAdd.class)
            .parameter(EnrProgramAllocationEntrantAddUI.ALLOCATION_ID, getAllocation().getId())
            .activate();
    }

    public void onDeleteEntityFromList()
    {
        _uiSupport.setRefreshScheduled(true);
        EnrProgramAllocationManager.instance().dao().deleteAllocItem(getListenerParameterAsLong());
    }

    // quota

    public Object getQuota()
    {
        return getConfig().getDataSource(EnrProgramAllocationPub.PROGRAM_DS).<IEntity>getCurrent().getProperty(EnrProgramDSHandler.VIEW_PROP_QUOTA);
    }

    public Object getUsed()
    {
        return getConfig().getDataSource(EnrProgramAllocationPub.PROGRAM_DS).<IEntity>getCurrent().getProperty(EnrProgramDSHandler.VIEW_PROP_USED);
    }

    // Getters & Setters

    public String getSelectedTab()
    {
        return _selectedTab;
    }

    public void setSelectedTab(String selectedTab)
    {
        _selectedTab = selectedTab;
    }

    public EnrProgramAllocation getAllocation()
    {
        return _allocation;
    }

    public void setAllocation(EnrProgramAllocation allocation)
    {
        _allocation = allocation;
    }

    public String getQuotaUsage()
    {
        return quotaUsage;
    }

    public void setQuotaUsage(String quotaUsage)
    {
        this.quotaUsage = quotaUsage;
    }
}
