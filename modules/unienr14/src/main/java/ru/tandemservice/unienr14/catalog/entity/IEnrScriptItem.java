/* $Id$ */
package ru.tandemservice.unienr14.catalog.entity;

import org.tandemframework.shared.commonbase.base.util.IScriptItem;

/**
 * @author nvankov
 * @since 8/7/14
 */
public interface IEnrScriptItem extends IScriptItem
{
    public static final String PRINT_PDF = "printPdf";
    public static final String PRINTABLE_TO_PDF = "printableToPdf";

    public boolean isPrintPdf();

    public boolean isPrintableToPdf();
}
