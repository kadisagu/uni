/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubMainDataTab;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import org.tandemframework.shared.person.base.bo.Person.ui.DormitoryEdit.PersonDormitoryEdit;
import org.tandemframework.shared.person.base.bo.Person.ui.Edit.PersonEdit;
import org.tandemframework.shared.person.base.bo.Person.ui.LanguageAddEdit.PersonLanguageAddEdit;
import org.tandemframework.shared.person.base.bo.Person.ui.SportAddEdit.PersonSportAddEdit;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonDormitoryBenefit;
import org.tandemframework.shared.person.base.entity.PersonForeignLanguage;
import org.tandemframework.shared.person.base.entity.PersonSportAchievement;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.formatter.EnrEntrantCustomStateCollectionFormatter;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.Edit.EnrEntrantEdit;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.EditPreparationData.EnrEntrantEditPreparationData;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.EditRegDate.EnrEntrantEditRegDate;
import ru.tandemservice.unienr14.entrant.entity.*;

import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @author oleyba
 * @since 4/10/13
 */
@Input({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "entrant.id", required = true),
    @Bind(key = EnrEntrantPubMainDataTabUI.PARAM_INLINE, binding = "inline")
})
public class EnrEntrantPubMainDataTabUI extends UIPresenter
{
    public static final String PARAM_INLINE = "inline";

    private boolean inline;
    public boolean isInline() { return this.inline; }
    public void setInline(boolean inline) { this.inline = inline; }

    private EnrEntrant entrant = new EnrEntrant();

    private String _dormitoryBenefits;
    private String _sourceInfoTitle;
    private String _accessCoursesTitle;
    private String _accessDepartmentsTitle;

    private DynamicListDataSource<PersonSportAchievement> _personSportAchievementDataSource;
    private DynamicListDataSource<PersonForeignLanguage> _personForeignLanguageDataSource;

    private List<EnrEntrantCustomState> _entrantCustomStates;

    @Override
    public void onComponentRefresh()
    {
        setEntrant(IUniBaseDao.instance.get().getNotNull(EnrEntrant.class, getEntrant().getId()));

        setDormitoryBenefits(UniStringUtils.join(IUniBaseDao.instance.get().getList(PersonDormitoryBenefit.class, PersonDormitoryBenefit.person(), getPerson(), PersonDormitoryBenefit.benefit().title().s()), PersonDormitoryBenefit.DISPLAYABLE_TITLE, "\n"));
        setSourceInfoTitle(UniStringUtils.join(IUniBaseDao.instance.get().getList(EnrEntrantInfoAboutUniversity.class, EnrEntrantInfoAboutUniversity.entrant(), getEntrant(), EnrEntrantInfoAboutUniversity.sourceInfo().title().s()), EnrEntrantInfoAboutUniversity.sourceInfo().title().s(), "\n"));
        setAccessCoursesTitle(UniStringUtils.join(IUniBaseDao.instance.get().getList(EnrEntrantAccessCourse.class, EnrEntrantAccessCourse.entrant(), getEntrant(), EnrEntrantAccessCourse.course().title().s()), EnrEntrantAccessCourse.course().title().s(), "\n"));
        setAccessDepartmentsTitle(UniStringUtils.join(IUniBaseDao.instance.get().getList(EnrEntrantAccessDepartment.class, EnrEntrantAccessDepartment.entrant(), getEntrant(), EnrEntrantAccessDepartment.accessDepartment().title().s()), EnrEntrantAccessDepartment.accessDepartment().title().s(), "\n"));

        prepareLanguageDataSource();
        prepareSportDataSource();

        setEntrantCustomStates(EnrEntrantManager.instance().dao().getActiveCustomStatesMap(Collections.singletonList(getEntrant()), new Date()).get(getEntrant()));
    }

    public void onClickEditEntrantData()
    {
        _uiActivation.asRegionDialog(EnrEntrantEdit.class)
        .parameter(IUIPresenter.PUBLISHER_ID, getEntrant().getId())
        .activate();
    }

    public void onClickEditPreparationData()
    {
        _uiActivation.asRegionDialog(EnrEntrantEditPreparationData.class)
                .parameter(UIPresenter.PUBLISHER_ID, getEntrant().getId())
                .activate();
    }

    public void onClickEditEntrantRegistrationDate()
    {
        _uiActivation.asRegionDialog(EnrEntrantEditRegDate.class)
        .parameter(IUIPresenter.PUBLISHER_ID, getEntrant().getId())
        .activate();
    }

    public void onClickEditPersonDormitoryData()
    {
        _uiActivation.asRegion(PersonDormitoryEdit.class).top()
            .parameter("personId", getPerson().getId())
            .activate();
    }

    public void onClickEditPersonalData()
    {
        _uiActivation.asRegionDialog(PersonEdit.class)
        .parameter(PersonEdit.PERSON_ID, getPerson().getId())
        .parameter(PersonEdit.EDIT_MAIN_DATA, true)
        .parameter(PersonEdit.EDIT_WORKPLACE_DATA, false)
        .parameter(PersonEdit.FORM_TITLE, "Редактирование персональных данных")
        .activate();
    }

    public void onClickEditWorkplaceData()
    {
        _uiActivation.asRegionDialog(PersonEdit.class)
        .parameter(PersonEdit.PERSON_ID, getPerson().getId())
        .parameter(PersonEdit.EDIT_MAIN_DATA, false)
        .parameter(PersonEdit.EDIT_WORKPLACE_DATA, true)
        .parameter(PersonEdit.FORM_TITLE, "Редактирование сведений о трудоустройстве")
        .activate();
    }

    public void onClickAddSportAchievement()
    {
        _uiActivation.asRegionDialog(PersonSportAddEdit.class)
        .parameter("personId", getPerson().getId())
        .activate();
    }

    public void onClickEditSportAchievement()
    {
        _uiActivation.asRegionDialog(PersonSportAddEdit.class)
        .parameter("personId", null)
        .parameter("sportAchievementId", getListenerParameterAsLong())
        .activate();
    }

    public void onClickDeleteSportAchievement()
    {
        PersonSportAchievement item = DataAccessServices.dao().getNotNull(getListenerParameterAsLong());
        DataAccessServices.dao().delete(item);
        getPersonSportAchievementDataSource().refresh();
    }

    public void onClickAddForeignLanguage()
    {
        _uiActivation.asRegionDialog(PersonLanguageAddEdit.class)
        .parameter("personId", getPerson().getId())
        .activate();
    }
    public void onClickEditForeignLanguage()
    {
        _uiActivation.asRegionDialog(PersonLanguageAddEdit.class)
        .parameter("personId", null)
        .parameter("foreignLanguageId", getListenerParameterAsLong())
        .activate();
    }

    public void onClickDeleteForeignLanguage()
    {
        PersonForeignLanguage item = DataAccessServices.dao().getNotNull(getListenerParameterAsLong());
        DataAccessServices.dao().delete(item);
        getPersonForeignLanguageDataSource().refresh();
    }

    // presenter

    public boolean isViewMode()
    {
        return getEntrant() != null && !getEntrant().isAccessible();
    }

    public Person getPerson()
    {
        return getEntrant().getPerson();
    }

    public boolean isHasDormitoryBenefits()
    {
        return StringUtils.isNotEmpty(getDormitoryBenefits());
    }

    public String getEntrantCustomStateString()
    {
        return new EnrEntrantCustomStateCollectionFormatter().format(_entrantCustomStates);
    }

    // utils

    private void prepareSportDataSource()
    {
        DynamicListDataSource<PersonSportAchievement> dataSource = new DynamicListDataSource<>(getConfig().getBusinessComponent(), component -> {
            prepareCustomDataSource(_personSportAchievementDataSource, PersonSportAchievement.ENTITY_CLASS, PersonSportAchievement.L_PERSON);
        }, 5);

        dataSource.addColumn(new SimpleColumn("Вид спорта", PersonSportAchievement.sportKind().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Разряд", PersonSportAchievement.sportRank().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Наивысшее достижение", PersonSportAchievement.P_BEST_ACHIEVEMENT).setClickable(false));

        if (getEntrant().isAccessible())
        {
            dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditSportAchievement").setPermissionKey("editPersonSportAchievement_enrEntrant"));
            dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteSportAchievement", "Удалить спортивное достижение: {0} — {1}?", PersonSportAchievement.L_SPORT_KIND + "." + ICatalogItem.CATALOG_ITEM_TITLE, PersonSportAchievement.L_SPORT_RANK + "." + ICatalogItem.CATALOG_ITEM_TITLE).setPermissionKey("deletePersonSportAchievement_enrEntrant"));
        }
        setPersonSportAchievementDataSource(dataSource);
    }

    private void prepareLanguageDataSource()
    {
        DynamicListDataSource<PersonForeignLanguage> dataSource = new DynamicListDataSource<>(getConfig().getBusinessComponent(), component -> {
            prepareCustomDataSource(_personForeignLanguageDataSource, PersonForeignLanguage.ENTITY_CLASS, PersonForeignLanguage.L_PERSON);
        }, 5);

        dataSource.addColumn(new SimpleColumn("Язык", PersonForeignLanguage.language().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Степень владения", PersonForeignLanguage.skill().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Основной", PersonForeignLanguage.P_MAIN).setFormatter(YesNoFormatter.INSTANCE).setClickable(false));

        if (getEntrant().isAccessible())
        {
            dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditForeignLanguage").setPermissionKey("editPersonForeignLanguage_enrEntrant"));
            dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteForeignLanguage", "Удалить иностранный язык «{0}»?", PersonForeignLanguage.L_LANGUAGE + "." + ICatalogItem.CATALOG_ITEM_TITLE).setPermissionKey("deletePersonForeignLanguage_enrEntrant"));
        }
        setPersonForeignLanguageDataSource(dataSource);
    }

    private <T extends IEntity> void prepareCustomDataSource(DynamicListDataSource<T> dataSource, String entityClass, String property)
    {
        MQBuilder builder = new MQBuilder(entityClass, "o");
        builder.addJoin("o", property, "e");

        builder.add(MQExpression.eq("e", "id", getPerson().getId()));

        OrderDescriptionRegistry orderSettings = new OrderDescriptionRegistry("o");
        orderSettings.applyOrder(builder, dataSource.getEntityOrder());

        List<T> entities = ISharedBaseDao.instance.get().getList(builder);
        int count = Math.max(entities.size() + 1, 2);
        dataSource.setTotalSize(count);
        dataSource.setCountRow(count);
        dataSource.createPage(entities);
    }

    // getters and setters

    public EnrEntrant getEntrant()
    {
        return entrant;
    }

    public void setEntrant(EnrEntrant entrant)
    {
        this.entrant = entrant;
    }

    public String getDormitoryBenefits()
    {
        return _dormitoryBenefits;
    }

    public void setDormitoryBenefits(String dormitoryBenefits)
    {
        _dormitoryBenefits = dormitoryBenefits;
    }

    public DynamicListDataSource<PersonSportAchievement> getPersonSportAchievementDataSource()
    {
        return _personSportAchievementDataSource;
    }

    public void setPersonSportAchievementDataSource(DynamicListDataSource<PersonSportAchievement> personSportAchievementDataSource)
    {
        _personSportAchievementDataSource = personSportAchievementDataSource;
    }

    public DynamicListDataSource<PersonForeignLanguage> getPersonForeignLanguageDataSource()
    {
        return _personForeignLanguageDataSource;
    }

    public void setPersonForeignLanguageDataSource(DynamicListDataSource<PersonForeignLanguage> personForeignLanguageDataSource)
    {
        _personForeignLanguageDataSource = personForeignLanguageDataSource;
    }

    public String getSourceInfoTitle()
    {
        return _sourceInfoTitle;
    }

    public void setSourceInfoTitle(String sourceInfoTitle)
    {
        _sourceInfoTitle = sourceInfoTitle;
    }

    public String getAccessCoursesTitle()
    {
        return _accessCoursesTitle;
    }

    public void setAccessCoursesTitle(String accessCoursesTitle)
    {
        _accessCoursesTitle = accessCoursesTitle;
    }

    public String getAccessDepartmentsTitle()
    {
        return _accessDepartmentsTitle;
    }

    public void setAccessDepartmentsTitle(String accessDepartmentsTitle)
    {
        _accessDepartmentsTitle = accessDepartmentsTitle;
    }

    public List<EnrEntrantCustomState> getEntrantCustomStates()
    {
        return _entrantCustomStates;
    }

    public void setEntrantCustomStates(List<EnrEntrantCustomState> entrantCustomStates)
    {
        _entrantCustomStates = entrantCustomStates;
    }
}
