/* $Id$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.util;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;

/**
 * @author nvankov
 * @since 6/9/14
 */
public class PersonEduDocumentOption extends DataWrapper
{
    private PersonEduDocument _eduDocument;
    private boolean _onlineEntrant;

    public PersonEduDocumentOption(Long id, String title, PersonEduDocument eduDocument, boolean isOnlineEntrant)
    {
        super(id, title);
        _eduDocument = eduDocument;
        _onlineEntrant = isOnlineEntrant;
    }

    public PersonEduDocument getEduDocument()
    {
        return _eduDocument;
    }

    public boolean isOnlineEntrant()
    {
        return _onlineEntrant;
    }
}