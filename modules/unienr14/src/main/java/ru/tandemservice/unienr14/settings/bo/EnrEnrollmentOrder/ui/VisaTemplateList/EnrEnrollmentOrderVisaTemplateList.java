/**
 *$Id: EnrEnrollmentOrderVisaTemplateList.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentOrder.ui.VisaTemplateList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.list.column.IMergeRowIdResolver;
import org.tandemframework.core.view.list.column.SimpleMergeIdResolver;
import org.tandemframework.core.view.util.Icon;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignOrderVisaItem;

/**
 * @author Alexander Shaburov
 * @since 31.07.13
 */
@Configuration
public class EnrEnrollmentOrderVisaTemplateList extends BusinessComponentManager
{
    public static final String VISA_TEMPLATE_SEARCH_DS = "visaTemplateSearchDS";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(VISA_TEMPLATE_SEARCH_DS, visaTemplateSearchDSColumns(), visaTemplateSearchDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint visaTemplateSearchDSColumns()
    {
        final IMergeRowIdResolver groupMerge = entity -> {
            EnrCampaignOrderVisaItem item = (EnrCampaignOrderVisaItem) entity;
            return item.getVisaTemplate().getId().toString() + "." + item.getGroupsMemberVising().getId().toString();
        };

        final SimpleMergeIdResolver visaTemplate = new SimpleMergeIdResolver(EnrCampaignOrderVisaItem.visaTemplate().id());

        return columnListExtPointBuilder(VISA_TEMPLATE_SEARCH_DS)
                .addColumn(textColumn("title", EnrCampaignOrderVisaItem.visaTemplate().title()).merger(visaTemplate))
                .addColumn(textColumn("groupTitle", EnrCampaignOrderVisaItem.groupsMemberVising().title()).merger(groupMerge))
                .addColumn(textColumn("postTitle", EnrCampaignOrderVisaItem.possibleVisa().title()))
                .addColumn(textColumn("fio", EnrCampaignOrderVisaItem.possibleVisa().entity().person().fullFio()))
                .addColumn(actionColumn("up", new Icon("up"), "onClickUp"))
                .addColumn(actionColumn("down", new Icon("down"), "onClickDown"))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, new Icon(EDIT_COLUMN_NAME), "onClickEdit").merger(visaTemplate))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, new Icon(DELETE_COLUMN_NAME), "onClickDelete").merger(visaTemplate).alert(new FormattedMessage("visaTemplateSearchDS.delete.alert", EnrCampaignOrderVisaItem.visaTemplate().title().s())))
                .create();
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> visaTemplateSearchDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrCampaignOrderVisaItem.class)
                .order(EnrCampaignOrderVisaItem.visaTemplate().title())
                .order(EnrCampaignOrderVisaItem.groupsMemberVising().index())
                .order(EnrCampaignOrderVisaItem.priority())
                .pageable(false);
    }
}
