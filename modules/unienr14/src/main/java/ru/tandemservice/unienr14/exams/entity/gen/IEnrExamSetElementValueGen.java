package ru.tandemservice.unienr14.exams.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.IEnrExamSetElementValue;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import ru.tandemservice.unienr14.settings.entity.IEnrExamSetElementValue;

/**
 * Элемент набора ВИ (дисциплина или группа)
 *
 * Интерфейс для объектов, которые могут быть включены в набор ВИ.
 */
@SuppressWarnings({"all", "unchecked", "unused"})
public abstract class IEnrExamSetElementValueGen extends InterfaceStubBase
 implements IEnrExamSetElementValue{
    public static final int VERSION_HASH = 1325082740;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";

    private EnrEnrollmentCampaign _enrollmentCampaign;


    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }

    private static final Path<IEnrExamSetElementValue> _dslPath = new Path<IEnrExamSetElementValue>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ru.tandemservice.unienr14.settings.entity.IEnrExamSetElementValue");
    }
            

    /**
     * @return Приемная кампания.
     * @see ru.tandemservice.unienr14.settings.entity.IEnrExamSetElementValue#getEnrollmentCampaign()
     */
    public static EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    public static class Path<E extends IEnrExamSetElementValue> extends EntityPath<E>
    {
        private EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> _enrollmentCampaign;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приемная кампания.
     * @see ru.tandemservice.unienr14.settings.entity.IEnrExamSetElementValue#getEnrollmentCampaign()
     */
        public EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

        public Class getEntityClass()
        {
            return IEnrExamSetElementValue.class;
        }

        public String getEntityName()
        {
            return "ru.tandemservice.unienr14.settings.entity.IEnrExamSetElementValue";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
