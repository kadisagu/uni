package ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.block.entrantRequestData;

import com.google.common.collect.Lists;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.ICommonFilterItem;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.*;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.program.entity.EduProgram;
import ru.tandemservice.unienr14.catalog.entity.*;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrBenefitTypeCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceBenefit;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.EnrReportPersonAdd;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.EnrReportPersonAddUI;
import ru.tandemservice.unienr14.request.entity.*;
import ru.tandemservice.unienr14.request.entity.gen.IEnrEntrantBenefitStatementGen;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.EnrEnrollmentCommissionManager;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Vasily Zhukov
 * @since 08.02.2011
 */
public class EntrantRequestDataParam implements IReportDQLModifier
{
    private final UIPresenter _presenter;

    public EntrantRequestDataParam(UIPresenter presenter)
    {
        _presenter = presenter;
    }

    private IReportParam<Date> _reqDateFrom = new ReportParam<>();
    private IReportParam<Date> _reqDateTo = new ReportParam<>();
    private IReportParam<List<EnrRequestType>> _requestType = new EnrCompetitionFilterAddonReportParam<List<EnrRequestType>>(EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE);
    private IReportParam<CompensationType> _compensationType = new EnrCompetitionFilterAddonReportParam<CompensationType>(EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE);
    private IReportParam<List<EduProgramForm>> _programForm = new EnrCompetitionFilterAddonReportParam<List<EduProgramForm>>(EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM);
    private IReportParam<List<EnrCompetitionType>> _competitionType = new EnrCompetitionFilterAddonReportParam<List<EnrCompetitionType>>(EnrCompetitionFilterAddon.SETTING_NAME_COMPETITION_TYPE);
    private IReportParam<List<EnrOrgUnit>> _enrOrgUnit = new EnrCompetitionFilterAddonReportParam<List<EnrOrgUnit>>(EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT);
    private IReportParam<List<OrgUnit>> _formativeOrgUnit = new EnrCompetitionFilterAddonReportParam<List<OrgUnit>>(EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT);
    private IReportParam<List<EduProgramSubject>> _eduProgramSubject = new EnrCompetitionFilterAddonReportParam<List<EduProgramSubject>>(EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT);
    private IReportParam<List<EnrProgramSetBase>> _programSet = new EnrCompetitionFilterAddonReportParam<List<EnrProgramSetBase>>(EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET);
    private IReportParam<List<EduProgram>> _eduProgram = new EnrCompetitionFilterAddonReportParam<List<EduProgram>>(EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM);
    private IReportParam<DataWrapper> _parallel = new ReportParam<>();
    private IReportParam<List<EnrBenefitType>> _benefitType = new ReportParam<>();
    private IReportParam<List<EnrBenefitCategory>> _benefitCategory = new ReportParam<>();
    private IReportParam<DataWrapper> _targetAdmission = new ReportParam<>();
    private IReportParam<List<EnrTargetAdmissionKind>> _targetAdmissionKind = new ReportParam<>();
    private IReportParam<List<EnrEntrantState>> _entrantState = new ReportParam<>();
    private IReportParam<DataWrapper> _originalDocuments = new ReportParam<>();
    private IReportParam<DataWrapper> _accepted = new ReportParam<>();
    private IReportParam<DataWrapper> _enrollmentAvailable = new ReportParam<>();
    private IReportParam<DataWrapper> _passPartnerEduInstitution = new ReportParam<>();
    private IReportParam<String> _technicCommission = new ReportParam<>();
    private IReportParam<String> _entrReqComment = new ReportParam<>();
    private IReportParam<String> _reqEnrDirectionComment = new ReportParam<>();
    private IReportParam<DataWrapper> _filedByTrustee = new ReportParam<>();
    private IReportParam<String> _trusteeDetails = new ReportParam<>();
    private IReportParam<EnrOriginalSubmissionAndReturnWay> _originalSubmissionWay = new ReportParam<>();
    private IReportParam<EnrOriginalSubmissionAndReturnWay> _originalReturnWay = new ReportParam<>();
    private IReportParam<DataWrapper> _crimea = new ReportParam<>();
    private IReportParam<List<DataWrapper>> _acceptRequest = new ReportParam<>();
    private IReportParam<DataWrapper> _targetContract = new ReportParam<>();
    private IReportParam<List<ExternalOrgUnit>> _targetContractOrgUnit = new ReportParam<>();


    private ICommonFilterItem getCompetitionFilterItem(String settingsName) {
        return ((EnrCompetitionFilterAddon) _presenter.getConfig().getAddon(EnrReportPersonAdd.COMPETITION_FILTERS_ENTRANT_REQUEST)).getFilterItem(settingsName);
    }

    private String entrantAlias(ReportDQL dql)
    {
        // соединяем абитуриента
        return dql.innerJoinEntity(EnrEntrant.class, EnrEntrant.person());
    }

    private String requestAlias(ReportDQL dql)
    {
        // соединяем заявление
        return dql.innerJoinEntity(entrantAlias(dql), EnrEntrantRequest.class, EnrEntrantRequest.entrant());
    }

    private String reqCompAlias(ReportDQL dql)
    {
        // соединяем абитуриента
        String entrantAlias = entrantAlias(dql);

        // соединяем заявление
        String requestAlias = requestAlias(dql);

        // соединяем выбранные конкурсы
        String reqCompAlias = dql.innerJoinEntity(requestAlias, EnrRequestedCompetition.class, EnrRequestedCompetition.request());

        // добавляем сортировки
        dql.order(entrantAlias, EnrEntrant.enrollmentCampaign().id());
        dql.order(requestAlias, EnrEntrantRequest.regDate());
        dql.order(requestAlias, EnrEntrantRequest.id());
        dql.order(reqCompAlias, EnrRequestedCompetition.priority());

        return reqCompAlias;
    }

    @Override
    public void validateReportParams(ErrorCollector errorCollector)
    {

    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        if (_reqDateFrom.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.reqDateFrom", DateFormatter.DEFAULT_DATE_FORMATTER.format(_reqDateFrom.getData()));

            dql.builder.where(ge(property(EnrEntrantRequest.regDate().fromAlias(requestAlias(dql))), valueTimestamp(CoreDateUtils.getDayFirstTimeMoment(_reqDateFrom.getData()))));
        }

        if (_reqDateTo.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.reqDateTo", DateFormatter.DEFAULT_DATE_FORMATTER.format(_reqDateTo.getData()));

            dql.builder.where(lt(property(EnrEntrantRequest.regDate().fromAlias(requestAlias(dql))),
                                 valueTimestamp(CoreDateUtils.getDayFirstTimeMoment(CoreDateUtils.add(_reqDateTo.getData(), Calendar.DAY_OF_YEAR, 1)))));
        }

        if (_requestType.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.requestType", CommonBaseStringUtil.join(_requestType.getData(), EnrRequestType.P_TITLE, ", "));
            dql.builder.where(in(property(EnrRequestedCompetition.competition().requestType().fromAlias(reqCompAlias(dql))), _requestType.getData()));
        }

        if (_compensationType.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.compensationType", _compensationType.getData().getTitle());
            dql.builder.where(eq(property(EnrRequestedCompetition.competition().type().compensationType().fromAlias(reqCompAlias(dql))), value(_compensationType.getData())));
        }

        if (_programForm.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.programForm", CommonBaseStringUtil.join(_programForm.getData(), EduProgramForm.P_TITLE, ", "));
            dql.builder.where(in(property(EnrRequestedCompetition.competition().programSetOrgUnit().programSet().programForm().fromAlias(reqCompAlias(dql))), _programForm.getData()));
        }

        if (_competitionType.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.competitionType", CommonBaseStringUtil.join(_competitionType.getData(), EnrCompetitionType.P_TITLE, ", "));
            dql.builder.where(in(property(EnrRequestedCompetition.competition().type().fromAlias(reqCompAlias(dql))), _competitionType.getData()));
        }

        if (_enrOrgUnit.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.enrOrgUnit", CommonBaseStringUtil.join(_enrOrgUnit.getData(), EnrOrgUnit.institutionOrgUnit().orgUnit().title().s(), ", "));
            dql.builder.where(in(property(EnrRequestedCompetition.competition().programSetOrgUnit().orgUnit().fromAlias(reqCompAlias(dql))), _enrOrgUnit.getData()));
        }

        if (_formativeOrgUnit.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.formativeOrgUnit", CommonBaseStringUtil.join(_formativeOrgUnit.getData(), OrgUnit.P_TITLE, ", "));
            dql.builder.where(in(property(EnrRequestedCompetition.competition().programSetOrgUnit().formativeOrgUnit().fromAlias(reqCompAlias(dql))), _formativeOrgUnit.getData()));
        }

        if (_eduProgramSubject.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.eduProgramSubject", CommonBaseStringUtil.join(_eduProgramSubject.getData(), EduProgramSubject.P_TITLE, ", "));
            dql.builder.where(in(property(EnrRequestedCompetition.competition().programSetOrgUnit().programSet().programSubject().fromAlias(reqCompAlias(dql))), _eduProgramSubject.getData()));
        }

        if (_programSet.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.programSet", CommonBaseStringUtil.join(_programSet.getData(), EnrProgramSetBase.P_TITLE, ", "));
            dql.builder.where(in(property(EnrRequestedCompetition.competition().programSetOrgUnit().programSet().fromAlias(reqCompAlias(dql))), _programSet.getData()));
        }

        if (_eduProgram.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.eduProgram", CommonBaseStringUtil.join(_eduProgram.getData(), EduProgram.P_TITLE, ", "));
            String alias = dql.nextAlias();
            dql.builder.where(exists(new DQLSelectBuilder()
                .fromEntity(EnrProgramSetItem.class, alias)
                .where(eq(property(EnrProgramSetItem.programSet().fromAlias(alias)), property(EnrRequestedCompetition.competition().programSetOrgUnit().programSet().fromAlias(reqCompAlias(dql)))))
                .where(in(property(EnrProgramSetItem.program().fromAlias(alias)), _eduProgram.getData()))
                .buildQuery()));
        }

        if (_parallel.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.parallel", _parallel.getData().getTitle());

            dql.builder.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(reqCompAlias(dql))), value(TwinComboDataSourceHandler.getSelectedValueNotNull(_parallel.getData()))));
        }

        if (_benefitType.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.benefitType", CommonBaseStringUtil.join(_benefitType.getData(), EnrBenefitType.title().s(), ", "));

            String alias = reqCompAlias(dql);

            List<IDQLExpression> orExpressions = Lists.newArrayList();

            for(EnrBenefitType benefitType : _benefitType.getData())
            {
                if(EnrBenefitTypeCodes.EXCLUSIVE.equals(benefitType.getCode())) /** В пределах квоты */
                {
                    orExpressions.add(exists(new DQLSelectBuilder().fromEntity(EnrRequestedCompetitionExclusive.class, "exclusive")
                            .where(eq(property("exclusive", EnrRequestedCompetitionExclusive.id()), property(alias, EnrRequestedCompetition.id())))
                            .buildQuery()));
                }
                else if(EnrBenefitTypeCodes.MAX_EXAM_RESULT.equals(benefitType.getCode())) /**100 баллов за вступительное испытание */
                {
                    orExpressions.add(exists(new DQLSelectBuilder().fromEntity(EnrEntrantMarkSourceBenefit.class, "markbenefit")
                            .where(eq(property("markbenefit", EnrEntrantMarkSourceBenefit.chosenEntranceExamForm().chosenEntranceExam().requestedCompetition().id()), property(alias, EnrRequestedCompetition.id())))
                            .buildQuery()));
                }
                else if(EnrBenefitTypeCodes.NO_ENTRANCE_EXAMS.equals(benefitType.getCode())) /** Без вступительных испытаний */
                {
                    orExpressions.add(exists(new DQLSelectBuilder().fromEntity(EnrRequestedCompetitionNoExams.class, "noexam")
                            .where(eq(property("noexam", EnrRequestedCompetitionNoExams.id()), property(alias, EnrRequestedCompetition.id())))
                            .buildQuery()));

                }
                else if(EnrBenefitTypeCodes.PREFERENCE.equals(benefitType.getCode())) /**  Преимущественное право */
                {
                    orExpressions.add(exists(new DQLSelectBuilder().fromEntity(EnrEntrantRequest.class, "benefitrequest")
                            .where(isNotNull(property("benefitrequest", EnrEntrantRequest.benefitCategory())))
                            .where(eq(property("benefitrequest", EnrEntrantRequest.id()), property(alias, EnrRequestedCompetition.request().id())))
                            .buildQuery()));
                }
            }

            if(!orExpressions.isEmpty())
                dql.builder.where(or(orExpressions.toArray(new IDQLExpression[orExpressions.size()])));
        }

        if (_benefitCategory.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.benefitCategory", CommonBaseStringUtil.join(_benefitCategory.getData(), EnrBenefitCategory.title().s(), ", "));

            String alias = dql.nextAlias();

            final DQLSelectBuilder reqDirIdDQL = new DQLSelectBuilder().fromEntity(IEnrEntrantBenefitStatement.class, alias)
                .column(property(IEnrEntrantBenefitStatementGen.entrant().id().fromAlias(alias)))
                .where(in(property(IEnrEntrantBenefitStatementGen.benefitCategory().fromAlias(alias)), _benefitCategory.getData()));

            dql.builder.where(in(property(EnrEntrant.id().fromAlias(entrantAlias(dql))), reqDirIdDQL.buildQuery()));
        }

        if (_targetAdmission.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.targetAdmission", _targetAdmission.getData().getTitle());

            if (TwinComboDataSourceHandler.getSelectedValueNotNull(_targetAdmission.getData()))
                dql.builder.where(instanceOf(reqCompAlias(dql), EnrRequestedCompetitionTA.class));
            else
                dql.builder.where(notInstanceOf(reqCompAlias(dql), EnrRequestedCompetitionTA.class));
        }

        if (_targetAdmissionKind.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.targetAdmissionKind", CommonBaseStringUtil.join(_targetAdmissionKind.getData(), EnrTargetAdmissionKind.P_TITLE, ", "));

            String alias = dql.nextAlias();
            dql.builder.joinEntity(reqCompAlias(dql), DQLJoinType.inner, EnrRequestedCompetitionTA.class, alias, eq(alias, reqCompAlias(dql)));
            dql.builder.where(in(property(EnrRequestedCompetitionTA.targetAdmissionKind().targetAdmissionKind().fromAlias(alias)), _targetAdmissionKind.getData()));
        }

        if (_entrantState.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.entrantState", CommonBaseStringUtil.join(_entrantState.getData(), EnrEntrantState.P_TITLE, ", "));

            dql.builder.where(in(property(EnrRequestedCompetition.state().fromAlias(reqCompAlias(dql))), _entrantState.getData()));
        }

        if (_originalDocuments.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.originalDocuments", _originalDocuments.getData().getTitle());

            dql.builder.where(eq(property(EnrRequestedCompetition.originalDocumentHandedIn().fromAlias(reqCompAlias(dql))), value(TwinComboDataSourceHandler.getSelectedValueNotNull(_originalDocuments.getData()))));
        }

        if (_accepted.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.accepted", _accepted.getData().getTitle());

            dql.builder.where(eq(property(EnrRequestedCompetition.accepted().fromAlias(reqCompAlias(dql))), value(TwinComboDataSourceHandler.getSelectedValueNotNull(_accepted.getData()))));
        }

        if (_enrollmentAvailable.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.enrollmentAvailable", _enrollmentAvailable.getData().getTitle());

            dql.builder.where(eq(property(EnrRequestedCompetition.enrollmentAvailable().fromAlias(reqCompAlias(dql))), value(TwinComboDataSourceHandler.getSelectedValueNotNull(_enrollmentAvailable.getData()))));
        }

//        if (_passPartnerEduInstitution.isActive())
//        {
//            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.passPartnerEduInstitution", _passPartnerEduInstitution.getData().getTitle());
//
//            String alias = dql.nextAlias();
//
//            final boolean pass = EntrantRequestDataBlock.PASS_PARTNER_EDU_INSTITUTION_YES.equals(_passPartnerEduInstitution.getData().getId());
//
//            final IDQLSelectableQuery query = new DQLSelectBuilder().fromEntity(EnrPartnerEduInstitution.class, alias).column(property(EnrPartnerEduInstitution.id().fromAlias(alias)))
//                    .where(eq(property(EnrPartnerEduInstitution.eduInstitution().fromAlias(alias)), property(EnrRequestedCompetition.request().eduInstitution().eduInstitution().fromAlias(reqDirAlias(dql)))))
//                    .buildQuery();
//
//            dql.builder.where(pass ? exists(query) : notExists(query));
//        }

//        if (_technicCommission.isActive())
//        {
//            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.technicCommission", _technicCommission.getData());
//
//            dql.builder.where(DQLExpressions.likeUpper(DQLExpressions.property(EnrRequestedCompetition.entrantRequest().technicCommission().fromAlias(reqDirAlias(dql))), DQLExpressions.value(CoreStringUtils.escapeLike(_technicCommission.getData()))));
//        }

        if (_entrReqComment.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.entrReqComment", _entrReqComment.getData());

            dql.builder.where(likeUpper(property(EnrEntrantRequest.comment().fromAlias(requestAlias(dql))), value(CoreStringUtils.escapeLike(_entrReqComment.getData()))));
        }

        if (_reqEnrDirectionComment.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.reqEnrDirectionComment", _reqEnrDirectionComment.getData());

            dql.builder.where(likeUpper(property(EnrRequestedCompetition.comment().fromAlias(reqCompAlias(dql))), value(CoreStringUtils.escapeLike(_reqEnrDirectionComment.getData()))));
        }

        if (_filedByTrustee.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.filedByTrustee", _filedByTrustee.getData().getTitle());

            dql.builder.where(eq(property(EnrEntrantRequest.filedByTrustee().fromAlias(requestAlias(dql))), value(TwinComboDataSourceHandler.getSelectedValueNotNull(_filedByTrustee.getData()))));
        }

        if (_trusteeDetails.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.trusteeDetails", _trusteeDetails.getData());

            dql.builder.where(likeUpper(property(EnrEntrantRequest.trusteeDetails().fromAlias(requestAlias(dql))), value(CoreStringUtils.escapeLike(_trusteeDetails.getData()))));
        }

        if (_originalSubmissionWay.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.originalSubmissionWay", _originalSubmissionWay.getData().getTitle());

            dql.builder.where(eq(property(EnrEntrantRequest.originalSubmissionWay().fromAlias(requestAlias(dql))), value(_originalSubmissionWay.getData())));
        }

        if (_originalReturnWay.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.originalReturnWay", _originalReturnWay.getData().getTitle());

            dql.builder.where(eq(property(EnrEntrantRequest.originalReturnWay().fromAlias(requestAlias(dql))), value(_originalReturnWay.getData())));
        }

        if (_crimea.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.crimea", _crimea.getData().getTitle());
            dql.builder.where(eq(property(EnrRequestedCompetition.competition().programSetOrgUnit().programSet().acceptPeopleResidingInCrimea().fromAlias(reqCompAlias(dql))),
                                 value(TwinComboDataSourceHandler.getSelectedValueNotNull(_crimea.getData()))));
        }

        if (_acceptRequest.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.acceptRequest", CommonBaseStringUtil.join(_acceptRequest.getData(), "title", ", "));
            if (_acceptRequest.getData().isEmpty())
                dql.builder.where(isNull(property(EnrEntrantRequest.acceptRequest().fromAlias(requestAlias(dql)))));
            else
                dql.builder.where(in(property(EnrEntrantRequest.acceptRequest().fromAlias(requestAlias(dql))), CommonBaseEntityUtil.getPropertiesList(_acceptRequest.getData(), "title")));
        }

        if (_targetContract.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.targetContract", _targetContract.getData().getTitle());
            dql.builder.where(eq(property(EnrRequestedCompetition.competition().programSetOrgUnit().programSet().targetContractTraining().fromAlias(reqCompAlias(dql))),
                                 value(TwinComboDataSourceHandler.getSelectedValueNotNull(_targetContract.getData()))));
        }

        if (_targetContractOrgUnit.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.targetContractOrgUnit", CommonBaseStringUtil.join(_targetContractOrgUnit.getData(), "titleWithLegalForm", ", "));
            dql.builder.where(in(property(EnrRequestedCompetition.externalOrgUnit().fromAlias(reqCompAlias(dql))),
                                 _targetContractOrgUnit.getData()));
        }

        final EnrReportPersonAddUI presenter = (EnrReportPersonAddUI) _presenter;
        final IReportParam<List<EnrEnrollmentCommission>> enrCommissionParam = presenter.getEntrantData().getEnrollmentCommission();
        if (enrCommissionParam.isActive())
        {
            EnrEnrollmentCommissionManager.instance().dao().filterRequestedCompetitionsByEnrCommission(dql.builder, reqCompAlias(dql), enrCommissionParam.getData());
        }
    }

    private class EnrCompetitionFilterAddonReportParam<T> extends ReportParam<T> {
        private final String paramName;

        private EnrCompetitionFilterAddonReportParam(String paramName) { this.paramName = paramName; }

        @Override public boolean isActive() {
            return getCompetitionFilterItem(paramName).isEnableCheckboxChecked();
        }

        @Override @SuppressWarnings("unchecked") public T getData() {
            return (T) getCompetitionFilterItem(paramName).getValue();
        }
    }

    // Getters

    public IReportParam<Date> getReqDateFrom()
    {
        return _reqDateFrom;
    }

    public IReportParam<Date> getReqDateTo()
    {
        return _reqDateTo;
    }

    public IReportParam<DataWrapper> getTargetAdmission()
    {
        return _targetAdmission;
    }

    public IReportParam<List<EnrTargetAdmissionKind>> getTargetAdmissionKind()
    {
        return _targetAdmissionKind;
    }

    public IReportParam<List<EnrEntrantState>> getEntrantState()
    {
        return _entrantState;
    }

    public IReportParam<DataWrapper> getOriginalDocuments()
    {
        return _originalDocuments;
    }

    public IReportParam<DataWrapper> getPassPartnerEduInstitution()
    {
        return _passPartnerEduInstitution;
    }

    public IReportParam<String> getTechnicCommission()
    {
        return _technicCommission;
    }

    public IReportParam<String> getEntrReqComment()
    {
        return _entrReqComment;
    }

    public IReportParam<String> getReqEnrDirectionComment()
    {
        return _reqEnrDirectionComment;
    }

    public IReportParam<DataWrapper> getFiledByTrustee(){ return _filedByTrustee; }

    public IReportParam<String> getTrusteeDetails(){ return _trusteeDetails; }

    public IReportParam<List<EnrBenefitType>> getBenefitType()
    {
        return _benefitType;
    }

    public IReportParam<List<EnrBenefitCategory>> getBenefitCategory()
    {
        return _benefitCategory;
    }

    public IReportParam<DataWrapper> getParallel()
    {
        return _parallel;
    }

    public IReportParam<EnrOriginalSubmissionAndReturnWay> getOriginalSubmissionWay(){ return _originalSubmissionWay; }

    public IReportParam<EnrOriginalSubmissionAndReturnWay> getOriginalReturnWay(){ return _originalReturnWay; }

    public IReportParam<DataWrapper> getCrimea()
    {
        return _crimea;
    }

    public IReportParam<List<DataWrapper>> getAcceptRequest()
    {
        return _acceptRequest;
    }

    public IReportParam<List<ExternalOrgUnit>> getTargetContractOrgUnit()
    {
        return _targetContractOrgUnit;
    }

    public IReportParam<DataWrapper> getTargetContract()
    {
        return _targetContract;
    }


    public IReportParam<DataWrapper> getAccepted()
    {
        return _accepted;
    }

    public IReportParam<DataWrapper> getEnrollmentAvailable()
    {
        return _enrollmentAvailable;
    }


}
