package ru.tandemservice.unienr14.rating.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult;
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSource;
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceStateExam;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Основание балла (ЕГЭ)
 *
 * Создается и обновляется демоном на основе свидетельств ЕГЭ абитуриента IEnrRatingDaemonDao#doRefreshSource4StateExam (создание оснований по данным свидетельств ЕГЭ).
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrEntrantMarkSourceStateExamGen extends EnrEntrantMarkSource
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceStateExam";
    public static final String ENTITY_NAME = "enrEntrantMarkSourceStateExam";
    public static final int VERSION_HASH = 1134385353;
    private static IEntityMeta ENTITY_META;

    public static final String L_STATE_EXAM_RESULT = "stateExamResult";

    private EnrEntrantStateExamResult _stateExamResult;     // Результат ЕГЭ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Результат ЕГЭ.
     */
    public EnrEntrantStateExamResult getStateExamResult()
    {
        return _stateExamResult;
    }

    /**
     * @param stateExamResult Результат ЕГЭ.
     */
    public void setStateExamResult(EnrEntrantStateExamResult stateExamResult)
    {
        dirty(_stateExamResult, stateExamResult);
        _stateExamResult = stateExamResult;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EnrEntrantMarkSourceStateExamGen)
        {
            setStateExamResult(((EnrEntrantMarkSourceStateExam)another).getStateExamResult());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrEntrantMarkSourceStateExamGen> extends EnrEntrantMarkSource.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrEntrantMarkSourceStateExam.class;
        }

        public T newInstance()
        {
            return (T) new EnrEntrantMarkSourceStateExam();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "stateExamResult":
                    return obj.getStateExamResult();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "stateExamResult":
                    obj.setStateExamResult((EnrEntrantStateExamResult) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "stateExamResult":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "stateExamResult":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "stateExamResult":
                    return EnrEntrantStateExamResult.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrEntrantMarkSourceStateExam> _dslPath = new Path<EnrEntrantMarkSourceStateExam>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrEntrantMarkSourceStateExam");
    }
            

    /**
     * @return Результат ЕГЭ.
     * @see ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceStateExam#getStateExamResult()
     */
    public static EnrEntrantStateExamResult.Path<EnrEntrantStateExamResult> stateExamResult()
    {
        return _dslPath.stateExamResult();
    }

    public static class Path<E extends EnrEntrantMarkSourceStateExam> extends EnrEntrantMarkSource.Path<E>
    {
        private EnrEntrantStateExamResult.Path<EnrEntrantStateExamResult> _stateExamResult;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Результат ЕГЭ.
     * @see ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceStateExam#getStateExamResult()
     */
        public EnrEntrantStateExamResult.Path<EnrEntrantStateExamResult> stateExamResult()
        {
            if(_stateExamResult == null )
                _stateExamResult = new EnrEntrantStateExamResult.Path<EnrEntrantStateExamResult>(L_STATE_EXAM_RESULT, this);
            return _stateExamResult;
        }

        public Class getEntityClass()
        {
            return EnrEntrantMarkSourceStateExam.class;
        }

        public String getEntityName()
        {
            return "enrEntrantMarkSourceStateExam";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
