/* $Id: SecLocalRoleAddEditUI.java 6522 2015-05-18 13:35:33Z oleyba $ */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.ui.LocalRoleAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.entity.Role;
import org.tandemframework.shared.organization.sec.bo.Sec.util.BaseRoleAddEditUI;
import ru.tandemservice.unienr14.sec.entity.RoleConfigLocalEnrCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

/**
 * @author Vasily Zhukov
 * @since 07.11.2011
 */
@Input({
        @Bind(key = EnrEnrollmentCampaignLocalRoleAddEditUI.ENROLLMENT_CAMPAIGN_ID, binding = EnrEnrollmentCampaignLocalRoleAddEditUI.ENROLLMENT_CAMPAIGN_ID)
})
public class EnrEnrollmentCampaignLocalRoleAddEditUI extends BaseRoleAddEditUI<RoleConfigLocalEnrCampaign>
{
    public static final String ENROLLMENT_CAMPAIGN_ID = "enrollmentCampaignId";

    private Long enrollmentCampaignId;

    @Override
    public void initRoleConfigForAddForm()
    {
        setRole(new Role());
        setRoleConfig(new RoleConfigLocalEnrCampaign());
        getRoleConfig().setRole(getRole());
        getRoleConfig().setEnrollmentCampaign(DataAccessServices.dao().getNotNull(EnrEnrollmentCampaign.class, getEnrollmentCampaignId()));
    }

    // Getters & Setters

    public Long getEnrollmentCampaignId()
    {
        return enrollmentCampaignId;
    }

    public void setEnrollmentCampaignId(Long enrollmentCampaignId)
    {
        this.enrollmentCampaignId = enrollmentCampaignId;
    }
}
