/**
 *$Id: EnrEnrollmentCampaignAddEditUI.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.ui.AddEdit;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.orgunit.entity.EduInstitutionOrgUnit;
import ru.tandemservice.unienr14.catalog.entity.EnrLevelBudgetFinancing;
import ru.tandemservice.unienr14.catalog.entity.EnrTieBreakRatingRule;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEnrollmentCampaignTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrTieBreakRatingRuleCodes;
import ru.tandemservice.unienr14.entrant.daemon.EnrEntrantDaemonBean;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexander Shaburov
 * @since 09.04.13
 */
@Input({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "entityHolder.id")
})
public class EnrEnrollmentCampaignAddEditUI extends UIPresenter
{
    public static final String PARAM_EDU_YEAR = "eduYear";

    private EntityHolder<EnrEnrollmentCampaign> _entityHolder = new EntityHolder<>(new EnrEnrollmentCampaign());

    private EducationYear _eduYear;
    private List<EduInstitutionOrgUnit> enrOrgUnitList = new ArrayList<>();

    @Override
    public void onComponentRefresh()
    {
        getEntityHolder().refresh();

        if (getEduYear() == null) {
            if (!isAddForm()) {
                setEduYear(getCampaign().getEducationYear());

                setEnrOrgUnitList(new ArrayList<EduInstitutionOrgUnit>());
                for (EnrOrgUnit orgUnit : IUniBaseDao.instance.get().getList(EnrOrgUnit.class, EnrOrgUnit.enrollmentCampaign(), getCampaign(), EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s())) {
                    getEnrOrgUnitList().add(orgUnit.getInstitutionOrgUnit());
                }
            }
            else {
                setEduYear(DataAccessServices.dao().get(EducationYear.class, EducationYear.intValue(), EducationYear.getCurrentRequired().getIntValue() + 1));
                getCampaign().setEducationYear(getEduYear());

                getCampaign().setTitle(getEduYear().getTitle());

                setEnrOrgUnitList(new ArrayList<EduInstitutionOrgUnit>());
                EduInstitutionOrgUnit academy = DataAccessServices.dao().get(EduInstitutionOrgUnit.class, EduInstitutionOrgUnit.orgUnit().parent(), null);
                if (academy != null) {
                    getEnrOrgUnitList().add(academy);
                }

                if (getEcSettings() == null) {
                    getCampaign().setSettings(new EnrEnrollmentCampaignSettings());
                    getEcSettings().setStateExamRestriction(false);
                    getEcSettings().setStateExamAccepted(true);
                    getEcSettings().setTargetAdmissionCompetition(true);
                    getEcSettings().setTieBreakRatingRule(IUniBaseDao.instance.get().getCatalogItem(EnrTieBreakRatingRule.class, EnrTieBreakRatingRuleCodes.SUM_THEN_PRIORITY));

                    EnrLevelBudgetFinancing levelBudgetFinancing = DataAccessServices.dao().getNotNull(EnrLevelBudgetFinancing.class, EnrLevelBudgetFinancing.priority(), 1);
                    getEcSettings().setLevelBudgetFinancing(levelBudgetFinancing);
                }
            }
        }
    }

    public void onClickApply()
    {
        if (!isCampaignYearMore2016())
        {
            if (null != getEcSettings())
            {
                getEcSettings().setEnrollmentCampaignType(null);
            }
        }

        if(getEcSettings().getId() == null)
        {
            // DEV-9677
            if(getEcSettings().getEnrollmentCampaignType() == null || EnrEnrollmentCampaignTypeCodes.BS.equals(getEcSettings().getEnrollmentCampaignType().getCode()))
            {
                getEcSettings().setExcludeEnrolledByLowPriority(false);
            }
            else
            {
                getEcSettings().setExcludeEnrolledByLowPriority(true);
            }
        }

        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveOrUpdateCampaign(getCampaign(), getEnrOrgUnitList());
        deactivate();
        EnrEntrantDaemonBean.DAEMON.wakeUpDaemon();
    }

    public void onUpdateTitle()
    {
        getCampaign().setEducationYear(getEduYear());
    }


    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(PARAM_EDU_YEAR, _eduYear);
    }

    // presenter

    public boolean isAddForm() {
        return getEntityHolder().getId() == null;
    }

    public boolean isVisibleCrimea()
    {
        return !isAddForm() && !(isCampaignYearMore2016());
    }

    public boolean isCampaignYearMore2016()
    {
        return getCampaign().getEducationYear().getIntValue() >= 2016;
    }

    public String getSticker() {
        return isAddForm() ? getConfig().getProperty("ui.sticker.add") : getConfig().getProperty("ui.sticker.edit");
    }

    public EnrEnrollmentCampaign getCampaign() {
        return getEntityHolder().getValue();
    }

    public DataWrapper getStateExamRestriction() {
        final SelectDataSource ds = getConfig().getDataSource(EnrEnrollmentCampaignAddEdit.STATE_EXAM_RESTRICTION_DS);

        final Boolean restriction = getEcSettings().getStateExamRestriction();
        if (null == restriction) { return ds.getRecordById(EnrEnrollmentCampaignAddEdit.STATE_EXAM_RESTRICTION_NONE); }
        return ds.getRecordById(restriction ? EnrEnrollmentCampaignAddEdit.STATE_EXAM_RESTRICTION_TRUE : EnrEnrollmentCampaignAddEdit.STATE_EXAM_RESTRICTION_FALSE);
    }

    public void setStateExamRestriction(DataWrapper stateExamRestriction) {
        if (EnrEnrollmentCampaignAddEdit.STATE_EXAM_RESTRICTION_NONE.equals(stateExamRestriction.getId())) {
            getEcSettings().setStateExamRestriction(null);
        } else if (EnrEnrollmentCampaignAddEdit.STATE_EXAM_RESTRICTION_FALSE.equals(stateExamRestriction.getId())) {
            getEcSettings().setStateExamRestriction(Boolean.FALSE);
        } else if (EnrEnrollmentCampaignAddEdit.STATE_EXAM_RESTRICTION_TRUE.equals(stateExamRestriction.getId())) {
            getEcSettings().setStateExamRestriction(Boolean.TRUE);
        } else {
            throw new IllegalArgumentException("setStateExamRestriction(id="+stateExamRestriction.getId()+")");
        }

    }

    public DataWrapper getTargetAdmissionCompetition()
    {
        final SelectDataSource ds = getConfig().getDataSource(EnrEnrollmentCampaignAddEdit.TARGET_ADMISSION_COMPETITION_DS);

        return ds.<DataWrapper>getRecordById(getEcSettings().isTargetAdmissionCompetition() ? TwinComboDataSourceHandler.YES_ID : TwinComboDataSourceHandler.NO_ID);
    }

    public void setTargetAdmissionCompetition(DataWrapper targetAdmissionCompetition)
    {
        getEcSettings().setTargetAdmissionCompetition(TwinComboDataSourceHandler.getSelectedValueNotNull(targetAdmissionCompetition));
    }

    public DataWrapper getAcceptPeopleResidingInCrimea()
    {
        final SelectDataSource ds = getConfig().getDataSource(EnrEnrollmentCampaignAddEdit.ACCEPT_PEOPLE_RESIDING_IN_CRIMEA_DS);

        return ds.getRecordById(getEcSettings().isAcceptPeopleResidingInCrimea() ? TwinComboDataSourceHandler.YES_ID: TwinComboDataSourceHandler.NO_ID);
    }

    public void setAcceptPeopleResidingInCrimea(DataWrapper acceptPeopleResidingInCrimea)
    {
        getEcSettings().setAcceptPeopleResidingInCrimea(TwinComboDataSourceHandler.getSelectedValueNotNull(acceptPeopleResidingInCrimea));
    }

    public DataWrapper getExamListPrintType()
    {
        final SelectDataSource ds = getConfig().getDataSource(EnrEnrollmentCampaignAddEdit.EXAM_LIST_PRINT_TYPE_DS);

        return ds.<DataWrapper>getRecordById(getEcSettings().getEnrEntrantExamListPrintType() != 0L ? getEcSettings().getEnrEntrantExamListPrintType() : EnrEnrollmentCampaignAddEdit.EXAM_LIST_PRINT_TYPE_ALL_EXAMS);
    }

    public void setExamListPrintType(DataWrapper examListPrintType)
    {
        getEcSettings().setEnrEntrantExamListPrintType(examListPrintType.getId());
    }

    public DataWrapper getNumberOnRequest()
    {
        final SelectDataSource ds = getConfig().getDataSource(EnrEnrollmentCampaignAddEdit.NUMBER_ON_REQUEST_SELECT_DS);

        return ds.getRecordById(getEcSettings().isNumberOnRequestManual() ? TwinComboDataSourceHandler.YES_ID : TwinComboDataSourceHandler.NO_ID);
    }

    public void setNumberOnRequest(DataWrapper numberOnRequest)
    {
        getEcSettings().setNumberOnRequestManual(TwinComboDataSourceHandler.getSelectedValueNotNull(numberOnRequest));
    }

    public DataWrapper getExamGroupTitleForming()
    {
        final SelectDataSource ds = getConfig().getDataSource(EnrEnrollmentCampaignAddEdit.EXAM_GROUP_TITLE_FORMING_SELECT_DS);

        return ds.getRecordById(getEcSettings().isExamGroupTitleFormingManual() ? TwinComboDataSourceHandler.YES_ID: TwinComboDataSourceHandler.NO_ID);
    }

    public void setExamGroupTitleForming(DataWrapper examGroupTitleForming)
    {
        getEcSettings().setExamGroupTitleFormingManual(TwinComboDataSourceHandler.getSelectedValueNotNull(examGroupTitleForming));
    }

    public DataWrapper getStateExamAccepted()
    {
        final SelectDataSource ds = getConfig().getDataSource(EnrEnrollmentCampaignAddEdit.STATE_EXAM_ACCEPTED_SELECT_DS);

        return ds.getRecordById(getEcSettings().isStateExamAccepted() ? TwinComboDataSourceHandler.YES_ID: TwinComboDataSourceHandler.NO_ID);
    }

    public void setStateExamAccepted(DataWrapper stateExamCertificateAccepted)
    {
        getEcSettings().setStateExamAccepted(TwinComboDataSourceHandler.getSelectedValueNotNull(stateExamCertificateAccepted));
    }

    public EnrEnrollmentCampaignSettings getEcSettings() {
        return getCampaign().getSettings();
    }


    /* getters and setters */

    public EntityHolder<EnrEnrollmentCampaign> getEntityHolder()
    {
        return _entityHolder;
    }

    public void setEntityHolder(EntityHolder<EnrEnrollmentCampaign> entityHolder)
    {
        _entityHolder = entityHolder;
    }

    public EducationYear getEduYear()
    {
        return _eduYear;
    }

    public void setEduYear(EducationYear eduYear)
    {
        _eduYear = eduYear;
    }

    public List<EduInstitutionOrgUnit> getEnrOrgUnitList()
    {
        return enrOrgUnitList;
    }

    public void setEnrOrgUnitList(List<EduInstitutionOrgUnit> enrOrgUnitList)
    {
        this.enrOrgUnitList = enrOrgUnitList;
    }
}
