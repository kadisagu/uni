/* $Id$ */
package ru.tandemservice.unienr14.competition.bo.EnrProgramSet.ui.EnrollmentCommissionEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetEnrollmentCommission;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.EnrEnrollmentCommissionManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.List;
import java.util.Map;

/**
 * @author Alexey Lopatin
 * @since 03.06.2015
 */
@Input({
        @Bind(key = EnrProgramSetEnrollmentCommissionEditUI.PARAM_CAMPAIGN, binding = "campaign", required = true),
        @Bind(key = EnrProgramSetEnrollmentCommissionEditUI.PARAM_REQUEST_TYPE, binding = "requestType", required = true),
        @Bind(key = EnrProgramSetEnrollmentCommissionEditUI.PARAM_PROGRAM_SET_ORG_UNIT_IDS, binding = "programSetOrgUnitIds", required = true)
})
public class EnrProgramSetEnrollmentCommissionEditUI extends UIPresenter
{
    public static final String PARAM_CAMPAIGN = EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN;
    public static final String PARAM_REQUEST_TYPE = "requestType";
    public static final String PARAM_PROGRAM_SET_ORG_UNIT_IDS = "programSetOrgUnitIds";

    private EnrEnrollmentCampaign _campaign;
    private EnrRequestType _requestType;
    private List<Long> _programSetOrgUnitIds;
    private Map<EnrProgramSetOrgUnit, EnrProgramSetEnrollmentCommission> _orgUnit2SetECMap;

    private EnrEnrollmentCommission _enrollmentCommission;
    private boolean _requiredEnrCommission;

    @Override
    public void onComponentRefresh()
    {
        _orgUnit2SetECMap = EnrEnrollmentCommissionManager.instance().dao().getProgramSetOrgUnit2EnrollmentCommissionMap(_campaign, _requestType);
        setRequiredEnrCommission(!EnrEnrollmentCommissionManager.instance().permissionDao().hasGlobalPermissionForEnrCommission(UserContext.getInstance().getPrincipalContext()));
        setEnrollmentCommission(EnrEnrollmentCommissionManager.instance().dao().getDefaultCommission(isRequiredEnrCommission()));
    }

    public void onClickApply()
    {
        EnrEnrollmentCommissionManager.instance().dao().saveDefaultCommission(getEnrollmentCommission());
        List<EnrProgramSetOrgUnit> programSetOrgUnitList = DataAccessServices.dao().getList(EnrProgramSetOrgUnit.class, getProgramSetOrgUnitIds());
        for (EnrProgramSetOrgUnit programSetOrgUnit : programSetOrgUnitList)
        {
            EnrEnrollmentCommissionManager.instance().dao().saveProgramSetEnrollmentCommission(getOrgUnit2SetECMap(), programSetOrgUnit, _enrollmentCommission);
        }
        deactivate();
    }

    public EnrEnrollmentCampaign getCampaign()
    {
        return _campaign;
    }

    public void setCampaign(EnrEnrollmentCampaign campaign)
    {
        _campaign = campaign;
    }

    public EnrRequestType getRequestType()
    {
        return _requestType;
    }

    public void setRequestType(EnrRequestType requestType)
    {
        _requestType = requestType;
    }

    public List<Long> getProgramSetOrgUnitIds()
    {
        return _programSetOrgUnitIds;
    }

    public void setProgramSetOrgUnitIds(List<Long> programSetOrgUnitIds)
    {
        _programSetOrgUnitIds = programSetOrgUnitIds;
    }

    public Map<EnrProgramSetOrgUnit, EnrProgramSetEnrollmentCommission> getOrgUnit2SetECMap()
    {
        return _orgUnit2SetECMap;
    }

    public void setOrgUnit2SetECMap(Map<EnrProgramSetOrgUnit, EnrProgramSetEnrollmentCommission> orgUnit2SetECMap)
    {
        _orgUnit2SetECMap = orgUnit2SetECMap;
    }

    public EnrEnrollmentCommission getEnrollmentCommission()
    {
        return _enrollmentCommission;
    }

    public void setEnrollmentCommission(EnrEnrollmentCommission enrollmentCommission)
    {
        _enrollmentCommission = enrollmentCommission;
    }

    public boolean isRequiredEnrCommission()
    {
        return _requiredEnrCommission;
    }

    public void setRequiredEnrCommission(boolean requiredEnrCommission)
    {
        _requiredEnrCommission = requiredEnrCommission;
    }
}
