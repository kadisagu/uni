package ru.tandemservice.unienr14.entrant.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantCompetition;
import ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantStatement;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выбранный конкурс онлайн-абитуриента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrOnlineEntrantCompetitionGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantCompetition";
    public static final String ENTITY_NAME = "enrOnlineEntrantCompetition";
    public static final int VERSION_HASH = 1697637746;
    private static IEntityMeta ENTITY_META;

    public static final String L_STATEMENT = "statement";
    public static final String P_PRIORITY = "priority";
    public static final String L_COMPETITION = "competition";

    private EnrOnlineEntrantStatement _statement;     // Заявление
    private int _priority;     // Приоритет
    private EnrCompetition _competition;     // Конкурс

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Заявление. Свойство не может быть null.
     */
    @NotNull
    public EnrOnlineEntrantStatement getStatement()
    {
        return _statement;
    }

    /**
     * @param statement Заявление. Свойство не может быть null.
     */
    public void setStatement(EnrOnlineEntrantStatement statement)
    {
        dirty(_statement, statement);
        _statement = statement;
    }

    /**
     * @return Приоритет. Свойство не может быть null.
     */
    @NotNull
    public int getPriority()
    {
        return _priority;
    }

    /**
     * @param priority Приоритет. Свойство не может быть null.
     */
    public void setPriority(int priority)
    {
        dirty(_priority, priority);
        _priority = priority;
    }

    /**
     * @return Конкурс. Свойство не может быть null.
     */
    @NotNull
    public EnrCompetition getCompetition()
    {
        return _competition;
    }

    /**
     * @param competition Конкурс. Свойство не может быть null.
     */
    public void setCompetition(EnrCompetition competition)
    {
        dirty(_competition, competition);
        _competition = competition;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrOnlineEntrantCompetitionGen)
        {
            setStatement(((EnrOnlineEntrantCompetition)another).getStatement());
            setPriority(((EnrOnlineEntrantCompetition)another).getPriority());
            setCompetition(((EnrOnlineEntrantCompetition)another).getCompetition());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrOnlineEntrantCompetitionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrOnlineEntrantCompetition.class;
        }

        public T newInstance()
        {
            return (T) new EnrOnlineEntrantCompetition();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "statement":
                    return obj.getStatement();
                case "priority":
                    return obj.getPriority();
                case "competition":
                    return obj.getCompetition();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "statement":
                    obj.setStatement((EnrOnlineEntrantStatement) value);
                    return;
                case "priority":
                    obj.setPriority((Integer) value);
                    return;
                case "competition":
                    obj.setCompetition((EnrCompetition) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "statement":
                        return true;
                case "priority":
                        return true;
                case "competition":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "statement":
                    return true;
                case "priority":
                    return true;
                case "competition":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "statement":
                    return EnrOnlineEntrantStatement.class;
                case "priority":
                    return Integer.class;
                case "competition":
                    return EnrCompetition.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrOnlineEntrantCompetition> _dslPath = new Path<EnrOnlineEntrantCompetition>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrOnlineEntrantCompetition");
    }
            

    /**
     * @return Заявление. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantCompetition#getStatement()
     */
    public static EnrOnlineEntrantStatement.Path<EnrOnlineEntrantStatement> statement()
    {
        return _dslPath.statement();
    }

    /**
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantCompetition#getPriority()
     */
    public static PropertyPath<Integer> priority()
    {
        return _dslPath.priority();
    }

    /**
     * @return Конкурс. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantCompetition#getCompetition()
     */
    public static EnrCompetition.Path<EnrCompetition> competition()
    {
        return _dslPath.competition();
    }

    public static class Path<E extends EnrOnlineEntrantCompetition> extends EntityPath<E>
    {
        private EnrOnlineEntrantStatement.Path<EnrOnlineEntrantStatement> _statement;
        private PropertyPath<Integer> _priority;
        private EnrCompetition.Path<EnrCompetition> _competition;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Заявление. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantCompetition#getStatement()
     */
        public EnrOnlineEntrantStatement.Path<EnrOnlineEntrantStatement> statement()
        {
            if(_statement == null )
                _statement = new EnrOnlineEntrantStatement.Path<EnrOnlineEntrantStatement>(L_STATEMENT, this);
            return _statement;
        }

    /**
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantCompetition#getPriority()
     */
        public PropertyPath<Integer> priority()
        {
            if(_priority == null )
                _priority = new PropertyPath<Integer>(EnrOnlineEntrantCompetitionGen.P_PRIORITY, this);
            return _priority;
        }

    /**
     * @return Конкурс. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantCompetition#getCompetition()
     */
        public EnrCompetition.Path<EnrCompetition> competition()
        {
            if(_competition == null )
                _competition = new EnrCompetition.Path<EnrCompetition>(L_COMPETITION, this);
            return _competition;
        }

        public Class getEntityClass()
        {
            return EnrOnlineEntrantCompetition.class;
        }

        public String getEntityName()
        {
            return "enrOnlineEntrantCompetition";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
