/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantsTargetAdmInfoAdd;/**
 * @author rsizonenko
 * @since 20.06.2014
 */

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.person.catalog.entity.EduDocumentKind;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

@Configuration
public class EnrReportEntrantsTargetAdmInfoAdd extends BusinessComponentManager {

    public static final String COMPENSATION_TYPE = "CompensationType";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
                .addAddon(uiAddon(CommonFilterAddon.class.getSimpleName(), EnrCompetitionFilterAddon.class))
                .addDataSource(selectDS(COMPENSATION_TYPE, compensationTypeHandler()))
                .create();
    }


    @Bean
    public IDefaultComboDataSourceHandler compensationTypeHandler()
    {
        return new EntityComboDataSourceHandler(getName(), CompensationType.class){
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context) {
                super.applyWhereConditions(alias, dql, context);

                dql
                        .fromEntity(CompensationType.class, "comp")
                        .where(eq(property("comp", CompensationType.code()), value(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET)));
            }
        };

    }
}
