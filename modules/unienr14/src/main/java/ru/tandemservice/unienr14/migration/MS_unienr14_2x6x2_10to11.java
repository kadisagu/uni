package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x2_10to11 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        if (!tool.columnExists("enr14_order_enr_extract_t", "xcldlwrprrtycmpttns_p")) {
			tool.createColumn("enr14_order_enr_extract_t", new DBColumn("xcldlwrprrtycmpttns_p", DBType.BOOLEAN));
            tool.executeUpdate("update enr14_order_enr_extract_t set xcldlwrprrtycmpttns_p=? where xcldlwrprrtycmpttns_p is null", Boolean.TRUE);
			tool.setColumnNullable("enr14_order_enr_extract_t", "xcldlwrprrtycmpttns_p", false);
		}

        if (!tool.columnExists("enr14_order_t", "requesttype_id")) {
			tool.createColumn("enr14_order_t", new DBColumn("requesttype_id", DBType.LONG));
			tool.setColumnNullable("enr14_order_t", "requesttype_id", false);
		}

        if (!tool.columnExists("enr14_order_t", "compensationtype_id")) {
			tool.createColumn("enr14_order_t", new DBColumn("compensationtype_id", DBType.LONG));
			tool.setColumnNullable("enr14_order_t", "compensationtype_id", false);
		}
    }
}