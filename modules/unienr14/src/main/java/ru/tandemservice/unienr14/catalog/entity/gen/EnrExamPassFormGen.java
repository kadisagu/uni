package ru.tandemservice.unienr14.catalog.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Форма сдачи
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrExamPassFormGen extends EntityBase
 implements INaturalIdentifiable<EnrExamPassFormGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm";
    public static final String ENTITY_NAME = "enrExamPassForm";
    public static final int VERSION_HASH = -1111369689;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_FULL_TITLE = "fullTitle";
    public static final String P_INTERNAL = "internal";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private String _fullTitle;     // Полное название название
    private boolean _internal;     // Тип формы сдачи
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Полное название название. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getFullTitle()
    {
        return _fullTitle;
    }

    /**
     * @param fullTitle Полное название название. Свойство не может быть null и должно быть уникальным.
     */
    public void setFullTitle(String fullTitle)
    {
        dirty(_fullTitle, fullTitle);
        _fullTitle = fullTitle;
    }

    /**
     * @return Тип формы сдачи. Свойство не может быть null.
     */
    @NotNull
    public boolean isInternal()
    {
        return _internal;
    }

    /**
     * @param internal Тип формы сдачи. Свойство не может быть null.
     */
    public void setInternal(boolean internal)
    {
        dirty(_internal, internal);
        _internal = internal;
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrExamPassFormGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((EnrExamPassForm)another).getCode());
            }
            setFullTitle(((EnrExamPassForm)another).getFullTitle());
            setInternal(((EnrExamPassForm)another).isInternal());
            setTitle(((EnrExamPassForm)another).getTitle());
        }
    }

    public INaturalId<EnrExamPassFormGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<EnrExamPassFormGen>
    {
        private static final String PROXY_NAME = "EnrExamPassFormNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrExamPassFormGen.NaturalId) ) return false;

            EnrExamPassFormGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrExamPassFormGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrExamPassForm.class;
        }

        public T newInstance()
        {
            return (T) new EnrExamPassForm();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "fullTitle":
                    return obj.getFullTitle();
                case "internal":
                    return obj.isInternal();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "fullTitle":
                    obj.setFullTitle((String) value);
                    return;
                case "internal":
                    obj.setInternal((Boolean) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "fullTitle":
                        return true;
                case "internal":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "fullTitle":
                    return true;
                case "internal":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "fullTitle":
                    return String.class;
                case "internal":
                    return Boolean.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrExamPassForm> _dslPath = new Path<EnrExamPassForm>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrExamPassForm");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Полное название название. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm#getFullTitle()
     */
    public static PropertyPath<String> fullTitle()
    {
        return _dslPath.fullTitle();
    }

    /**
     * @return Тип формы сдачи. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm#isInternal()
     */
    public static PropertyPath<Boolean> internal()
    {
        return _dslPath.internal();
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EnrExamPassForm> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _fullTitle;
        private PropertyPath<Boolean> _internal;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(EnrExamPassFormGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Полное название название. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm#getFullTitle()
     */
        public PropertyPath<String> fullTitle()
        {
            if(_fullTitle == null )
                _fullTitle = new PropertyPath<String>(EnrExamPassFormGen.P_FULL_TITLE, this);
            return _fullTitle;
        }

    /**
     * @return Тип формы сдачи. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm#isInternal()
     */
        public PropertyPath<Boolean> internal()
        {
            if(_internal == null )
                _internal = new PropertyPath<Boolean>(EnrExamPassFormGen.P_INTERNAL, this);
            return _internal;
        }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EnrExamPassFormGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EnrExamPassForm.class;
        }

        public String getEntityName()
        {
            return "enrExamPassForm";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
