/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrantAchievement;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

/**
 * @author oleyba
 * @since 7/16/14
 */
@Configuration
public class EnrEntrantAchievementManager extends BusinessObjectManager
{
    public static EnrEntrantAchievementManager instance()
    {
        return instance(EnrEntrantAchievementManager.class);
    }
}
