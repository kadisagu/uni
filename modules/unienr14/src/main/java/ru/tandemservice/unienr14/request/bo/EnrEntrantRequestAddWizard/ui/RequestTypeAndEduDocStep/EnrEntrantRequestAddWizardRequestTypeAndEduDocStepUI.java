/* $Id$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.RequestTypeAndEduDocStep;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.runtime.SecurityRuntime;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.person.base.bo.PersonEduDocument.PersonEduDocumentManager;
import org.tandemframework.shared.person.base.bo.PersonEduDocument.ui.AddEdit.PersonEduDocumentAddEditUI;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.catalog.bo.EnrRequestType.EnrRequestTypeManager;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrRequestTypeCodes;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrantBaseDocument.EnrEntrantBaseDocumentManager;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantEduDocument;
import ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrantStatement;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.EnrEntrantRequestAddWizardManager;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.EnrEntrantRequestAddWizardWizardUI;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.EnrEntrantWizardState;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.IWizardStep;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.util.PersonEduDocumentOption;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

/**
 * @author nvankov
 * @since 6/5/14
 */
@Input( {
    @Bind(key = EnrEntrantRequestAddWizardWizardUI.PARAM_WIZARD_STATE, binding = "state")
})
public class EnrEntrantRequestAddWizardRequestTypeAndEduDocStepUI extends PersonEduDocumentAddEditUI implements IWizardStep
{
    private static final String ADD_OTHER_EDU_DOC_PERMISSION = "enr14WizardAddOtherEduDoc";

    private EnrEntrantWizardState _state;

    private EnrEntrant _entrant;
    private EnrOnlineEntrant _onlineEntrant;
    private EnrRequestType _requestType;
    private List<PersonEduDocumentOption> _eduDocumentOptions = Lists.newArrayList();
    private PersonEduDocumentOption _eduDocumentOption;
    private Map<Long, EnrOnlineEntrantEduDocument> _onlineEntrantDocsMap = Maps.newHashMap();

    private boolean _viewDoc = false;

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();
        _entrant = DataAccessServices.dao().getNotNull(_state.getEntrantId());
        if(_state.getOnlineEntrantId() != null)
            _onlineEntrant = DataAccessServices.dao().get(_state.getOnlineEntrantId());

        // если вернулись с следующих шагов
        if(_state.getRequestTypeId() != null)
            _requestType = DataAccessServices.dao().get(_state.getRequestTypeId());
        else
        {
            if(getOnlineEntrant() != null)
            {
                List<EnrOnlineEntrantStatement> statements = DataAccessServices.dao().getList(EnrOnlineEntrantStatement.class, EnrOnlineEntrantStatement.onlineEntrant().id(), getOnlineEntrant().getId());
                EnrOnlineEntrantStatement statement = statements.isEmpty() ? null : statements.get(0);
                if(statement != null && !StringUtils.isEmpty(statement.getWantRequestType()) && EnrRequestTypeCodes.CODES.contains(statement.getWantRequestType()))
                {
                    _requestType = IUniBaseDao.instance.get().getCatalogItem(EnrRequestType.class, statement.getWantRequestType());
                }
                else
                {
                    setRequestType(EnrRequestTypeManager.instance().dao().getDefaultRequestType(getEntrant().getEnrollmentCampaign()));
                }
            }
            else
            {
                setRequestType(EnrRequestTypeManager.instance().dao().getDefaultRequestType(getEntrant().getEnrollmentCampaign()));
            }
        }
        onSelectRequestType();

        // если вернулись с следующих шагов
        if(_state.getEduDocumentId() != null)
        {
            for(PersonEduDocumentOption option : _eduDocumentOptions)
            {
                if(_state.getEduDocumentId().equals(option.getId()))
                    _eduDocumentOption = option;
            }
        }

        onChangeDocumentKind();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        super.onBeforeDataSourceFetch(dataSource);

        if (_requestType != null)
        {
            dataSource.put(EnrEntrantBaseDocumentManager.BIND_REQUEST_TYPE, _requestType.getId());
            dataSource.put(EnrEntrantBaseDocumentManager.BIND_RECEIVE_EDU_LEVEL_FIRST, getState().isReceiveEduLevelFirst());
        }

	    switch (dataSource.getName())
	    {
		    case EnrEntrantRequestAddWizardRequestTypeAndEduDocStep.DS_DOCUMENT_KIND:
			    dataSource.put("otherEduDocPermission", CoreServices.securityService().check(SecurityRuntime.getInstance().getCommonSecurityObject(), ContextLocal.getUserContext().getPrincipalContext(), ADD_OTHER_EDU_DOC_PERMISSION));
			    break;
		    case EnrEntrantRequestAddWizardRequestTypeAndEduDocStep.DS_EDU_LEVEL:
			    if (getDocument().getEduDocumentKind() != null)
				    dataSource.put(EnrEntrantBaseDocumentManager.BIND_DOCUMENT_KIND, getDocument().getEduDocumentKind().getId());
			    break;
		    case EnrEntrantRequestAddWizardRequestTypeAndEduDocStep.DS_SETTLEMENT:
			    dataSource.put(AddressItem.PARAM_COUNTRY, getCountry());
			    break;
	    }

        dataSource.put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, getEntrant().getEnrollmentCampaign());
    }

    // actions

    public void onSelectRequestType()
    {
        prepareEduDocumentOptions();
        if(getRequestType() != null) {
            getState().setRequestTypeId(getRequestType().getId());
            if (isStateExamStepNeeded()) {
                if (!getState().getStepsOrder().contains(EnrEntrantRequestAddWizardWizardUI.STATE_EXAMS_STEP_CODE))
                    getState().getStepsOrder().add(getState().getStepsOrder().indexOf(EnrEntrantRequestAddWizardWizardUI.DOCUMENTS_STEP_CODE), EnrEntrantRequestAddWizardWizardUI.STATE_EXAMS_STEP_CODE);
            }
            else {
                if (getState().getStepsOrder().contains(EnrEntrantRequestAddWizardWizardUI.STATE_EXAMS_STEP_CODE))
                    getState().getStepsOrder().remove(getState().getStepsOrder().indexOf(EnrEntrantRequestAddWizardWizardUI.STATE_EXAMS_STEP_CODE));
            }
        }
    }

    public void onSelectOption()
    {
        updateEduDoc();
    }

    @Override
    public void saveStepData()
    {
        EnrRequestTypeManager.instance().dao().saveDefaultRequestType(getRequestType(), getEntrant().getEnrollmentCampaign());
        applyStep();
    }

    public void onClickDownloadFile()
    {
        EnrOnlineEntrantEduDocument document = _onlineEntrantDocsMap.get(_eduDocumentOption.getId());
        byte[] content = document.getScanCopy().getContent();
        if (content == null)
            throw new ApplicationException("Файл скан-копии пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType(document.getScanCopy().getContentType()).fileName(document.getScanCopy().getFilename()).document(content), false);
    }

    // presenter

    public boolean isShowOnlineEntrantEduOrgAddressItem()
    {
        return _eduDocumentOption != null && _eduDocumentOption.isOnlineEntrant() && _onlineEntrantDocsMap.get(_eduDocumentOption.getId()) != null;
    }

    public String getOnlineEntrantEduOrgAddressItem()
    {
        return _eduDocumentOption != null ? _onlineEntrantDocsMap.get(_eduDocumentOption.getId()).getEduOrgCountry().getTitle() + ", " + _onlineEntrantDocsMap.get(_eduDocumentOption.getId()).getEduOrganizationSettlement() : "";
    }

    public boolean isEduDocumentOptionGroupVisible()
    {
        return getEduDocumentOptions().size() > 1;
    }

    public boolean isShowDocument()
    {
        return getEduDocumentOption() != null;
    }

    @Override
    public boolean isEduLevelRequired()
    {
        return true;
    }

    public boolean isShowOnlineEntrantEduDocFile()
    {
        if(_eduDocumentOption == null) return false;
        EnrOnlineEntrantEduDocument document = _onlineEntrantDocsMap.get(_eduDocumentOption.getId());
        return _eduDocumentOption.isOnlineEntrant()
        && document != null
        && document.getScanCopy() != null;
    }

    public String getOnlineEntrantEduDocFileName()
    {
        return _eduDocumentOption != null ? _onlineEntrantDocsMap.get(_eduDocumentOption.getId()).getScanCopy().getFilename() : "";
    }

    // utils

    private boolean isStateExamStepNeeded() {
        return EnrRequestTypeCodes.BS.equals(_requestType.getCode());
    }

    private void prepareEduDocumentOptions()
    {
        _eduDocumentOptions.clear();
        if(_requestType != null)
        {
            List<PersonEduDocument> documents = EnrEntrantRequestAddWizardManager.instance().dao().getEduDocumentsFilteredByRequestType(_entrant.getPerson().getId(), _requestType.getCode(), getState().isReceiveEduLevelFirst());
            for (PersonEduDocument eduDocument : documents)
            {
                _eduDocumentOptions.add(new PersonEduDocumentOption(eduDocument.getId(), eduDocument.getDisplayableTitle(), eduDocument, false));
            }
            if (_onlineEntrant != null)
            {
                List<EnrOnlineEntrantEduDocument> onlineEntrantDocuments = EnrEntrantRequestAddWizardManager.instance().dao().getOnlineEntrantEduDocumentsFilteredByRequestType(_onlineEntrant.getId(), _requestType.getCode(), getState().isReceiveEduLevelFirst());
                if (onlineEntrantDocuments.isEmpty())
                {
                    _eduDocumentOptions.add(new PersonEduDocumentOption(1L, "Создать новый документ", null, false));
                }
                else
                {
                    for (EnrOnlineEntrantEduDocument onlineEntrantEduDocument : onlineEntrantDocuments)
                    {
                        _onlineEntrantDocsMap.put(onlineEntrantEduDocument.getId(), onlineEntrantEduDocument);

                        PersonEduDocument eduDocument = new PersonEduDocument();
                        eduDocument.setPerson(_entrant.getPerson());
                        eduDocument.setEduDocumentKind(onlineEntrantEduDocument.getDocKind());
                        eduDocument.setDocumentKindDetailed(onlineEntrantEduDocument.getDocumentKindDetailed());
                        eduDocument.setSeria(onlineEntrantEduDocument.getSeria());
                        eduDocument.setNumber(onlineEntrantEduDocument.getNumber());
                        eduDocument.setEduOrganization(onlineEntrantEduDocument.getEduOrganization());
                        eduDocument.setYearEnd(onlineEntrantEduDocument.getYearEnd());
                        eduDocument.setIssuanceDate(onlineEntrantEduDocument.getIssuanceDate());

                        // TODO: отображать отдельно private AddressItem _eduOrganizationAddressItem;     // Местонахождение образовательной организации, выдавшей документ

                        eduDocument.setEduLevel(onlineEntrantEduDocument.getEduLevel());
                        eduDocument.setDocumentEducationLevel(onlineEntrantEduDocument.getDocumentEducationLevel());
                        eduDocument.setQualification(onlineEntrantEduDocument.getQualification());
                        eduDocument.setEduProgramSubject(onlineEntrantEduDocument.getEduProgramSubject());
                        eduDocument.setGraduationHonour(onlineEntrantEduDocument.getHonour());
                        eduDocument.setAvgMarkAsLong(onlineEntrantEduDocument.getAvgMarkAsLong());

                        StringBuilder title = new StringBuilder();
                        title.append(eduDocument.getEduDocumentKind().getShortTitle()).append(" - ");
                        if (eduDocument.getSeria() != null)
                            title.append(eduDocument.getSeria()).append(" ");

                        title.append(eduDocument.getNumber()).append(" (");

                        if (eduDocument.getEduLevel() != null)
                            title.append(eduDocument.getEduLevel().getShortTitle()).append(", ");

                        title
                        .append(eduDocument.getYearEnd()).append(" г., ");

                        _eduDocumentOptions.add(new PersonEduDocumentOption(onlineEntrantEduDocument.getId(), title.toString() + " (данные из онлайн-регистрации)", eduDocument, true));
                    }
                }
            }
            else
                _eduDocumentOptions.add(new PersonEduDocumentOption(1L, "Создать новый документ", null, false));

            _eduDocumentOption = _eduDocumentOptions.get(0);
        }
        else
            _eduDocumentOption = null;
        updateEduDoc();
    }

    private void updateEduDoc()
    {
        if(_eduDocumentOption != null)
        {
            if (_eduDocumentOption.getId() != 1L)
            {
                if(_eduDocumentOption.isOnlineEntrant() && _onlineEntrantDocsMap.get(_eduDocumentOption.getId()) != null)
                {
                    setCountry(_onlineEntrantDocsMap.get(_eduDocumentOption.getId()).getEduOrgCountry());
                    _viewDoc = false;
                }
                else
                {
                    _viewDoc = EnrEntrantRequestAddWizardManager.instance().dao().eduDocAppliedForRequest(_eduDocumentOption.getEduDocument().getId());
                    setCountry(_eduDocumentOption.getEduDocument().getEduOrganizationAddressItem().getCountry());
                }
                setDocument(_eduDocumentOption.getEduDocument());
            }
            else
            {
                _viewDoc = false;
                setDocument(new PersonEduDocument());
                getDocument().setPerson(getPerson());
                getDocument().setYearEnd(Calendar.getInstance().get(Calendar.YEAR));
            }
        }
    }

    private void applyStep()
    {
        // DEV-7933
        if (getState().isReceiveEduLevelFirst()
                && EnrRequestTypeCodes.MASTER.equals(getRequestType().getCode())
                && getDocument().getEduDocumentKind().isCanCertifyQualificationSpec()
                && !getDocument().isCertifyQualificationSpec())
        {
            getSupport().error("Документ абитуриента, впервые поступающего в магистратуру, должен подтверждать присвоение квалификации «дипломированный специалист».", "certifyQualificationSpec");
        }

        boolean isFirst = !IUniBaseDao.instance.get().existsEntity(PersonEduDocument.class, PersonEduDocument.person().s(), getEntrant().getPerson());

        if (isShowOnlineEntrantEduDocFile() && getScanCopyFile() == null)
        {
            final DatabaseFile file = _onlineEntrantDocsMap.get(_eduDocumentOption.getId()).getScanCopy();
            if (file.getContent() == null)
                throw new ApplicationException("Файл скан-копии пуст.");
            PersonEduDocumentManager.instance().dao().saveOrUpdateDocument(getDocument(), new IUploadFile()
            {
                @Override public String getFileName() { return file.getFilename(); }
                @Override public String getFilePath() { return null; }
                @Override public InputStream getStream() { return new ByteArrayInputStream(file.getContent()); }
                @Override public String getContentType() { return file.getContentType(); }
                @Override public void write(File file) { }
                @Override public boolean isInMemory() { return true; }
                @Override public long getSize() { return file.getContent().length; }
            });
        }
        else
        {
            PersonEduDocumentManager.instance().dao().saveOrUpdateDocument(getDocument(), getScanCopyFile());
        }

        if (isFirst)
        {
            getEntrant().getPerson().setMainEduDocument(getDocument());
            IUniBaseDao.instance.get().update(getEntrant().getPerson());
        }

        _state.setEduDocumentId(getDocument().getId());
    }

    // getters and setters

    public EnrRequestType getRequestType()
    {
        return _requestType;
    }

    public void setRequestType(EnrRequestType requestType)
    {
        _requestType = requestType;
    }

    public EnrEntrant getEntrant()
    {
        return _entrant;
    }

    public void setEntrant(EnrEntrant entrant)
    {
        _entrant = entrant;
    }

    public EnrOnlineEntrant getOnlineEntrant()
    {
        return _onlineEntrant;
    }

    public void setOnlineEntrant(EnrOnlineEntrant onlineEntrant)
    {
        _onlineEntrant = onlineEntrant;
    }

    public List<PersonEduDocumentOption> getEduDocumentOptions()
    {
        return _eduDocumentOptions;
    }

    public void setEduDocumentOptions(List<PersonEduDocumentOption> eduDocumentOptions)
    {
        _eduDocumentOptions = eduDocumentOptions;
    }

    public PersonEduDocumentOption getEduDocumentOption()
    {
        return _eduDocumentOption;
    }

    public void setEduDocumentOption(PersonEduDocumentOption eduDocumentOption)
    {
        _eduDocumentOption = eduDocumentOption;
    }

    public boolean isViewDoc()
    {
        return _viewDoc;
    }

    public void setViewDoc(boolean viewDoc)
    {
        _viewDoc = viewDoc;
    }

    public EnrEntrantWizardState getState()
    {
        return _state;
    }

    public void setState(EnrEntrantWizardState state)
    {
        _state = state;
    }
}
