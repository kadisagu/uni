/**
 *$Id: EnrExamGroupListUI.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.exams.bo.EnrExamGroup.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.unienr14.exams.bo.EnrExamGroup.EnrExamGroupManager;
import ru.tandemservice.unienr14.exams.bo.EnrExamGroup.ui.AddEdit.EnrExamGroupAddEdit;
import ru.tandemservice.unienr14.exams.bo.EnrExamGroup.ui.AddEdit.EnrExamGroupAddEditUI;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

/**
 * @author Alexander Shaburov
 * @since 29.05.13
 */
public class EnrExamGroupListUI extends UIPresenter
{
    public static final String SETTING_ORG_UNIT = "orgUnit";
    public static final String SETTING_EXAM_GROUP_SET = "examGroupSet";
    public static final String SETTING_DISCIPLINE_LIST = "disciplineList";
    public static final String SETTING_DAYS_LIST = "daysList";

    @Override
    public void onComponentRefresh()
    {
        getSettings().set(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
    }

    public void onClearSettings()
    {
        getSettings().remove(SETTING_EXAM_GROUP_SET);
        getSettings().remove(SETTING_DISCIPLINE_LIST);
        getSettings().remove(SETTING_DAYS_LIST);
        getSettings().save();
    }

    public void onEnrCampaignRefresh()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getSettings().<EnrEnrollmentCampaign>get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, getSettings().get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN));
        dataSource.put(EnrExamGroupList.BIND_ORG_UNIT, getOrgUnit() == null ? null : getOrgUnit().getInstitutionOrgUnit().getOrgUnit());
        dataSource.put(SETTING_EXAM_GROUP_SET, getSettings().get(SETTING_EXAM_GROUP_SET));
        dataSource.put(SETTING_DISCIPLINE_LIST, getSettings().get(SETTING_DISCIPLINE_LIST));
        dataSource.put(SETTING_DAYS_LIST, getSettings().get(SETTING_DAYS_LIST));
    }

    public void onClickAddExamGroup()
    {
        getActivationBuilder().asRegion(EnrExamGroupAddEdit.class)
                .parameter(EnrExamGroupAddEditUI.BIND_ENR_ORG_UNIT, getOrgUnit() == null ? null : getOrgUnit().getInstitutionOrgUnit().getOrgUnit().getId())
                .activate();
    }

    public void onClickEditExamGroup()
    {
        getActivationBuilder().asRegion(EnrExamGroupAddEdit.class)
                .parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong())
                .activate();
    }

    public void onClickDeleteExamGroup()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public void onClickPrintEnrPassSheet()
    {
        final RtfDocument document = EnrExamGroupManager.instance().enrPassSheetPrintDao().createEnrPassSheetRtf(getListenerParameterAsLong());
        if (document != null)
        {
            EnrScriptItem scriptItem = IUniBaseDao.instance.get().getCatalogItem(EnrScriptItem.class, EnrScriptItemCodes.EXAM_GROUP_ENROLLMENT_PASS_SHEET);
            if(scriptItem.isPrintPdf())
            {
                UniRtfUtil.downloadRtfAsPdf("enrollmentPassSheet.pdf", RtfUtil.toByteArray(document));
            }
            else
            {
                BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("enrollmentPassSheet.rtf").document(document), true);
            }
        }

    }

    public void onClickPrintEnrPassSheetCoded()
    {
        final RtfDocument document = EnrExamGroupManager.instance().enrPassSheetPrintDao().createEnrPassSheetCodedRtf(getListenerParameterAsLong());
        if (document != null)
        {
            EnrScriptItem scriptItem = IUniBaseDao.instance.get().getCatalogItem(EnrScriptItem.class, EnrScriptItemCodes.EXAM_GROUP_ENROLLMENT_PASS_SHEET_CODED);
            if(scriptItem.isPrintPdf())
            {
                UniRtfUtil.downloadRtfAsPdf("enrollmentPassSheetCoded.pdf", RtfUtil.toByteArray(document));
            }
            else
            {
                BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("enrollmentPassSheetCoded.rtf").document(document), true);
            }
        }
    }

    public void onClickPrintEnrPassSheetMarks()
    {
        EnrScriptItem scriptItem = IUniBaseDao.instance.get().getCatalogItem(EnrScriptItem.class, EnrScriptItemCodes.EXAM_GROUP_ENROLLMENT_PASS_SHEET_MARKS);

        final RtfDocument document = EnrExamGroupManager.instance().enrPassSheetPrintDao().createEnrPassSheetMarksRtf(getListenerParameterAsLong());
        if (document != null)
        {
            if(scriptItem.isPrintPdf())
            {
                UniRtfUtil.downloadRtfAsPdf("enrollmentPassSheetMarks.pdf", RtfUtil.toByteArray(document));
            }
            else
            {
                BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("enrollmentPassSheetMarks.rtf").document(document), true);
            }
        }
    }

    // Getters & Setters

    public boolean isNothingSelected()
    {
        return getSettings().get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN) == null || getOrgUnit() == null;
    }

    public EnrOrgUnit getOrgUnit(){ return getSettings().get(SETTING_ORG_UNIT); }

    // Accessors
}
