/* $Id:$ */
package ru.tandemservice.unienr14.competition.bo.EnrProgramSet;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.logic.IEnrProgramSetDao;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.logic.EnrProgramSetDao;

/**
 * @author oleyba
 * @since 2/3/14
 */
@Configuration
public class EnrProgramSetManager extends BusinessObjectManager
{
    public static EnrProgramSetManager instance()
    {
        return instance(EnrProgramSetManager.class);
    }

    @Bean
    public IEnrProgramSetDao dao()
    {
        return new EnrProgramSetDao();
    }
}
