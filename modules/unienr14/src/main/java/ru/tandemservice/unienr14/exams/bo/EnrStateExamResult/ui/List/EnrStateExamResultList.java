/**
 *$Id: EnrStateExamResultList.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.exams.bo.EnrStateExamResult.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.resolver.DefaultLinkResolver;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.util.Icon;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.catalog.entity.EnrStateExamSubject;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.Pub.EnrEntrantPub;
import ru.tandemservice.unienr14.exams.bo.EnrStateExamResult.logic.EnrStateExamResultSearchDS;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;

/**
 * @author Alexander Shaburov
 * @since 13.06.13
 */
@Configuration
public class EnrStateExamResultList extends BusinessComponentManager
{
    public static final String ENR_CAMP_SELECT_DS = EnrEnrollmentCampaignManager.DS_ENR_CAMPAIGN;
    public static final String STATE_EXAM_CERTIFICATE_SEARCH_DS = "stateExamCertificateSearchDS";
    public static final String STATE_EXAM_SUBJECT_DS = "stateExamSubjectDS";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
                .addDataSource(CommonManager.instance().yesNoDSConfig())
                .addDataSource(selectDS(STATE_EXAM_SUBJECT_DS, stateExamSubjectDSHandler()))
                .addDataSource(searchListDS(STATE_EXAM_CERTIFICATE_SEARCH_DS, stateExamCertificateSearchDSColumns(), stateExamCertificateSearchDSHandler()))
                .create();
    }

    @Bean
    @SuppressWarnings("unchecked")
    public ColumnListExtPoint stateExamCertificateSearchDSColumns()
    {
        return columnListExtPointBuilder(STATE_EXAM_CERTIFICATE_SEARCH_DS)
                .addColumn(textColumn("year", EnrEntrantStateExamResult.year().s()).order())
                .addColumn(textColumn("certNumber", EnrEntrantStateExamResult.documentNumber().s()).order())
                .addColumn(publisherColumn("fio", EnrEntrantStateExamResult.entrant().person().fullFio()).order()
                    .publisherLinkResolver(DefaultLinkResolver.with()
                        .component(EnrEntrantPub.class)
                        .primaryKeyPath(EnrEntrantStateExamResult.entrant().id())
                        .primaryKeyAsParameter(UIPresenter.PUBLISHER_ID)
                        .parametersMapBinding("mvel:['selectedTab':'stateExamResultTab']")
                        .create()))
                .addColumn(textColumn("sex", EnrEntrantStateExamResult.entrant().person().identityCard().sex().shortTitle()))
                .addColumn(textColumn("icNumber", EnrEntrantStateExamResult.entrant().person().identityCard().fullNumber()))
                .addColumn(dateColumn("birthDate", EnrEntrantStateExamResult.entrant().person().identityCard().birthDate()))
                .addColumn(textColumn("subject", EnrEntrantStateExamResult.subject().title().s()))
                .addColumn(textColumn("mark", EnrEntrantStateExamResult.mark().s()))
                .addColumn(booleanColumn("abovePassingMark", EnrEntrantStateExamResult.abovePassingMark().s()))
                .addColumn(textColumn("stateCheckStatus", EnrEntrantStateExamResult.stateCheckStatus().s()))
                .addColumn(booleanColumn("secondWave", EnrEntrantStateExamResult.secondWave().s()))
                .addColumn(textColumn("secondWaveExamPlace", EnrEntrantStateExamResult.secondWaveExamPlace().title().s()))
                .addColumn(toggleColumn("accepted", EnrEntrantStateExamResult.accepted())
                    .permissionKey("enr14StateExamCertificateListAcceptCert")
                    .toggleOnListener("onToggleAccepted")
                    .toggleOffListener("onToggleAccepted"))
                .addColumn(toggleColumn("verified", EnrEntrantStateExamResult.verifiedByUser())
                    .permissionKey("enr14StateExamCertificateListVerifyByUserCert")
                    .toggleOnListener("onToggleVerified")
                    .toggleOffListener("onToggleVerified"))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, new Icon(EDIT_COLUMN_NAME), "onClickEdit").permissionKey("enr14StateExamCertificateListEditCert").disabled(EnrEntrantStateExamResult.P_EDIT_DISABLED).alert("ui:editAlert"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, new Icon(DELETE_COLUMN_NAME), "onClickDelete").permissionKey("enr14StateExamCertificateListDeleteCert").disabled(EnrEntrantStateExamResult.P_EDIT_DISABLED).alert(new FormattedMessage("stateExamCertificateSearchDS.delete.alert", EnrEntrantStateExamResult.title().s())))
                .create();
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> stateExamCertificateSearchDSHandler()
    {
        return new EnrStateExamResultSearchDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler stateExamSubjectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrStateExamSubject.class)
                .filter(EnrStateExamSubject.title())
                .order(EnrStateExamSubject.title())
                .pageable(true);
    }

}
