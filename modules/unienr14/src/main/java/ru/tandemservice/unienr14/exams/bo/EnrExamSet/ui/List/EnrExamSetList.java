/**
 *$Id: EnrExamSetList.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.exams.bo.EnrExamSet.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.formatter.RowCollectionFormatter;
import org.tandemframework.core.view.formatter.RawFormatter;
import org.tandemframework.core.view.util.Icon;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.logic.EnrExamSetSearchDSHandler;
import ru.tandemservice.unienr14.exams.entity.EnrExamSet;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;

/**
 * @author Alexander Shaburov
 * @since 16.04.13
 */
@Configuration
public class EnrExamSetList extends BusinessComponentManager
{
    public static final String EXAM_SET_SEARCH_DS = "examSetSearchDS";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
        .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
        .addDataSource(searchListDS(EXAM_SET_SEARCH_DS, examSetSearchDSColumns(), examSetSearchDSHandler()))
        .create();
    }

    @Bean
    public ColumnListExtPoint examSetSearchDSColumns()
    {
        return columnListExtPointBuilder(EXAM_SET_SEARCH_DS)
                .addColumn(publisherColumn("title", EnrExamSetSearchDSHandler.PROP_TITLE).formatter(new RowCollectionFormatter(RawFormatter.INSTANCE)).primaryKeyPath(EnrExamSet.id()))
                .addColumn(textColumn("competitionCount", EnrExamSetSearchDSHandler.PROP_COMPETITION_COUNT))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, new Icon(EDIT_COLUMN_NAME), "onClickEditExamSet").permissionKey("enr14ExamSetListEditExamSet"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, new Icon(DELETE_COLUMN_NAME), "onClickDeleteExamSet").alert(new FormattedMessage("examSetSearchDS.delete.alert")).permissionKey("enr14ExamSetListDeleteExamSet"))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> examSetSearchDSHandler()
    {
        return new EnrExamSetSearchDSHandler(getName());
    }
}
