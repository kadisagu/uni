/* $Id$ */
package ru.tandemservice.unienr14.component.catalog.enrOrderPrintFormType.EnrOrderPrintFormTypePub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultScriptCatalogPub.DefaultScriptCatalogPubDAO;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderPrintFormType;

/**
 * @author azhebko
 * @since 15.07.2014
 */
public class DAO extends DefaultScriptCatalogPubDAO<EnrOrderPrintFormType, Model> implements IDAO
{
    public void doTogglePrintPdfScript(Long itemId)
    {
        EnrOrderPrintFormType scriptItem = get(EnrOrderPrintFormType.class, itemId);
        if(scriptItem.isPrintPdf())
            scriptItem.setPrintPdf(false);
        else
            scriptItem.setPrintPdf(true);
        update(scriptItem);
    }
}