/* $Id$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.EdiSpecialExamConditions;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author azhebko
 * @since 04.06.2014
 */
@Configuration
public class EnrEntrantEdiSpecialExamConditions extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
            .create();
    }
}