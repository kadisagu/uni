package ru.tandemservice.unienr14.order.entity;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.util.NumberAsStringComparator;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrOrderTypeCodes;
import ru.tandemservice.unienr14.fis.IFisUidByIdOwner;
import ru.tandemservice.unienr14.order.entity.gen.EnrOrderGen;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.entity.catalog.codes.OrderStatesCodes;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Приказ на абитуриентов (2014)
 */
public class EnrOrder extends EnrOrderGen implements IFisUidByIdOwner
{
    public static final String PARAM_ENR_CAMPAIGN = "enrCampaign";

    @Override
    @EntityDSLSupport
    public String getTitleWithOrder()
    {
        StringBuilder b = new StringBuilder("Приказ");
        if (getNumber() != null) {
            b.append(" №").append(getNumber());
        }
        if (getCommitDate() != null) {
            b.append(" от ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getCommitDate()));
        }
        b.append(" (").append(getRequestType().getShortTitle()).append("). ");
        b.append(getState().getTitle());
        if (getState().getCode().equals(OrderStatesCodes.FORMING)) {
            b.append(" c ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getCreateDate()));
        }
        return b.toString();
    }

    @Override
    @EntityDSLSupport(parts = {IAbstractOrder.P_NUMBER, EnrOrder.P_COMMIT_DATE, EnrOrder.P_ID})
    public String getTitle()
    {
        if (getType() == null) {
            return this.getClass().getSimpleName();
        }
        StringBuilder b = new StringBuilder(getType().getTitle());
        if (getNumber() != null) {
            b.append(" №").append(getNumber());
        }
        if (getCommitDate() != null) {
            b.append(" от ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getCommitDate()));
        }
        return b.toString();
    }

    @Override
    @EntityDSLSupport(parts = {IAbstractOrder.P_NUMBER, EnrOrder.P_COMMIT_DATE, EnrOrder.P_ID})
    public String getInfo()
    {
        StringBuilder b = new StringBuilder(getType().getTitle());
        if (getNumber() != null) {
            b.append(" №").append(getNumber());
        }
        if (getCommitDate() != null) {
            b.append(" от ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getCommitDate()));
        }
        b.append(" (").append(getState().getTitle()).append(")");
        return b.toString();
    }

    @Override
    @EntityDSLSupport(parts = {IAbstractOrder.P_NUMBER, EnrOrder.P_COMMIT_DATE, EnrOrder.P_ID})
    public String getDateAndNumber()
    {
        StringBuilder b = new StringBuilder();
        if (getNumber() != null) {
            b.append("№").append(getNumber());
        }
        if (getCommitDate() != null) {
            b.append(" от ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getCommitDate()));
        }
        return b.toString();
    }


    public String getActionDateTitle()
    {
        if (getType() != null && (EnrOrderTypeCodes.ENROLLMENT.equals(getType().getCode()) || EnrOrderTypeCodes.ENROLLMENT_MIN.equals(getType().getCode())))
            return "Дата зачисления";
        if (getType() != null && EnrOrderTypeCodes.CANCEL.equals(getType().getCode()))
            return "Дата отмены";
        if (getType() != null && EnrOrderTypeCodes.ALLOCATION.equals(getType().getCode()))
            return "Дата распределения";
        throw new IllegalStateException();
    }

    @Override
    public String getFisUidTitle()
    {
        return getTitle();
    }

    public static EntityComboDataSourceHandler defaultSelectDSHandler(String ownerId)
    {
        return new EntityComboDataSourceHandler(ownerId, EnrOrder.class)
        {
            @Override
            protected boolean isExternalSortRequired()
            {
                return true;
            }

            @Override
            protected List<IEntity> sortSelectedValues(List<IEntity> list, Set primaryKeys)
            {
                Collections.sort(list, (o1, o2) -> NumberAsStringComparator.get().compare(((EnrOrder) o1).getNumber(), ((EnrOrder) o2).getNumber()));
                return list;
            }

            @Override
            protected List<IEntity> sortOptions(List<IEntity> list)
            {
                Collections.sort(list, (o1, o2) -> NumberAsStringComparator.get().compare(((EnrOrder) o1).getNumber(), ((EnrOrder) o2).getNumber()));
                return list;
            }
        }
                .titleProperty(titleWithOrder().s())
                .customize((alias, dql, context, filter) -> dql.where(eq(property(EnrOrder.enrollmentCampaign().fromAlias(alias)), commonValue(context.get(PARAM_ENR_CAMPAIGN))))
                        .where(eq(property(EnrOrder.type().code().fromAlias(alias)), value(EnrOrderTypeCodes.ENROLLMENT))))
                .filter(EnrOrder.requestType().title())
                .filter(EnrOrder.number())
                .filter(EnrOrder.state().title())
                .order(EnrOrder.commitDate())
                .order(EnrOrder.state().code())
                .order(EnrOrder.createDate());
    }
}