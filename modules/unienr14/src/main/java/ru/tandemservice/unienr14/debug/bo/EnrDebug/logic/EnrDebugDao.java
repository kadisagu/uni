package ru.tandemservice.unienr14.debug.bo.EnrDebug.logic;

import org.apache.commons.collections15.CollectionUtils;
import org.hibernate.Session;
import org.tandemframework.core.common.INaturalId;
import org.tandemframework.core.entity.dsl.MetaDSLPath;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.tool.IdGen;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniedu.catalog.bo.EduProgramSpecialization.logic.SpecializationShortTitleBuilder;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes;
import ru.tandemservice.uniedu.catalog.entity.codes.EduLevelCodes;
import ru.tandemservice.uniedu.catalog.entity.subjects.*;
import ru.tandemservice.uniedu.orgunit.entity.EduInstitutionOrgUnit;
import ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.uniedu.program.entity.EduProgramSecondaryProf;
import ru.tandemservice.unienr14.catalog.entity.*;
import ru.tandemservice.unienr14.catalog.entity.codes.*;
import ru.tandemservice.unienr14.catalog.entity.gen.EnrRequestTypeGen;
import ru.tandemservice.unienr14.competition.bo.EnrProgramSet.EnrProgramSetManager;
import ru.tandemservice.unienr14.competition.entity.*;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItem;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantOriginalDocumentStatus;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult;
import ru.tandemservice.unienr14.entrant.entity.gen.EnrEntrantOriginalDocumentStatusGen;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.EnrExamSetManager;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.logic.IEnrExamSetDao;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.ui.AddEdit.ExamSetElementWrapper;
import ru.tandemservice.unienr14.exams.entity.EnrExamSet;
import ru.tandemservice.unienr14.exams.entity.EnrExamSetElement;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;
import ru.tandemservice.unienr14.request.entity.*;
import ru.tandemservice.unienr14.settings.entity.*;

import java.nio.channels.IllegalSelectorException;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.random;

/**
 * @author vdanilov
 */
@SuppressWarnings("deprecation")
public class EnrDebugDao extends UniBaseDao implements IEnrDebugDao
{

    private static final Random random = new Random();

    // минимальное число АккОУ
    public static final int EDU_INSTITUTION_MINIMA = 5;

    // кафедры
    public static final int EDU_OWNER_MINIMA = 25;

    // минимальное число обобщенных направленностей
    public static final int EDU_PROGRAM_SPECIALIZATION_MINIMA = 50;

    // среднее число направленностей на одно направление (число направленностей будет от 1 до 2*x-1)
    public static final int EDU_PROGRAM_SPECIALIZATION_PER_SUBJECT = 6;

    public static final int PROGRAM_SET_COUNT = 1; // todo 20
    public static final boolean BS_ONLY = true; // todo false

    @Override
    public void doFullEduRandom(EducationYear year) {

        /*
            delete from edu_program_prof_secondary_t;
            delete from edu_program_prof_higher_t;
            delete from edu_program_prof_t;
            delete from edu_program_primary_t;
            delete from edu_program_t;
            delete from edu_specialization_child_t;
            delete from edu_specialization_root_t;
            delete from edu_specialization_base_t;
         */


        // создаем обобщенные направления (и направленности)
        final List<EduProgramSpecialization> specializationList = this.doCreateSpecializationList();
        if (specializationList.isEmpty()) { return; }

        // проверяем, что у нас достаточно аккредитованных ОУ (если не хватает - создаем)
        final List<EduInstitutionOrgUnit> institutionList = this.doCreateEduInstitutionList();
        if (institutionList.isEmpty()) { return; }

        // списки ответсвенных за разработку ОП
        final List<EduOwnerOrgUnit> ownerOrgUnitList = this.doCreateEduOwnerList();
        if (ownerOrgUnitList.isEmpty()) { return; }

        // уровни образования, на базе которых будет строится программа
        final EduLevel l1 = getCatalogItem(EduLevel.class, EduLevelCodes.SREDNEE_OBTSHEE_OBRAZOVANIE);
        //final EduLevel l2 = getCatalogItem(EduLevel.class, EduLevelCodes.SREDNEE_PROFESSIONALNOE_OBRAZOVANIE);
        final EduLevel l3 = getCatalogItem(EduLevel.class, EduLevelCodes.PROFESSIONALNOE_OBRAZOVANIE);

        // формы освоения
        final List<EduProgramForm> formList = getList(EduProgramForm.class, EduProgramForm.P_CODE);

        // (100*y + m) -> duration
        final Map<Integer, EduProgramDuration> durationMap = getDurationMap();

        // список обобщенных направленностей, по которым нет образовательных программ
        final List<EduProgramSpecialization> uncoveredSpecializationList = this.getUncoveredSpecializationList(specializationList, year);
        if (uncoveredSpecializationList.isEmpty()) { return; }

        // по каждой непокрытой направленности будем создавать несколько образовательных программ
        for (final EduProgramSpecialization spec: uncoveredSpecializationList)
        {
            final EduProgramKind kind = spec.getProgramSubject().getSubjectIndex().getProgramKind();
            final EduOwnerOrgUnit ownerOrgUnit = ownerOrgUnitList.get(random.nextInt(ownerOrgUnitList.size()));

            List<EduProgramQualification> qualifications = getPropertiesList(EduProgramSubjectQualification.class,
                EduProgramSubjectQualification.programSubject(), spec.getProgramSubject(),
                false,
                EduProgramSubjectQualification.programQualification());
            if (qualifications.isEmpty())
                continue;

            Collections.shuffle(qualifications, random);
            Collections.shuffle(institutionList, random);
            for (final EduInstitutionOrgUnit institutionOrgUnit: institutionList.subList(0, 1+random.nextInt(2))) {

                // список форм не перемешиваем, очка идет в самом начале
                for (final EduProgramForm form: formList.subList(0, 1+random.nextInt(2+random.nextInt(2)))) {

                    EduProgramProf program;
                    if (kind.isProgramHigherProf())
                    {
                        final EduProgramHigherProf p = (EduProgramHigherProf)(program = new EduProgramHigherProf());
                        p.setYear(year);
                        p.setKind(kind);
                        p.setForm(form);
                        p.setOwnerOrgUnit(ownerOrgUnit);
                        p.setProgramSubject(spec.getProgramSubject());
                        p.setProgramSpecialization(spec);
                        p.setInstitutionOrgUnit(institutionOrgUnit);
                        p.setProgramQualification(qualifications.get(0));
                        p.setTitle(spec.getProgramSubject().getTitleWithCode() + "("+spec.getTitle()+", "+year.getTitle()+")");
                        if (p.getTitle().length() > 255) {
                            p.setTitle(spec.getProgramSubject().getShortTitleWithCode() + "("+spec.getTitle()+", "+year.getTitle()+")");
                        }

                        p.setShortTitle(spec.getShortTitle()+" ("+year.getTitle()+")");


                        //                        if ((EduProgramKindCodes.PROGRAMMA_BAKALAVRIATA.equals(kind.getCode()) || EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV.equals(kind.getCode())) && (random.nextInt(10) < 1))
                        //                        {
                        //                            // это программа бакалавров и специалистов (но не магистров) на базе СПО (редкий случай)
                        //                            // как правило, это уменьшенный срок
                        //                            p.setBaseLevel(l2);
                        //                            if (EduProgramKindCodes.PROGRAMMA_BAKALAVRIATA.equals(kind.getCode())) {
                        //                                p.setDuration(durationMap.get(206));
                        //                            } else if (EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV.equals(kind.getCode())) {
                        //                                p.setDuration(durationMap.get(306));
                        //                            } else {
                        //                                // ой, что-то новое
                        //                                throw new IllegalStateException();
                        //                            }
                        //                        }
                        //                        else
                        {
                            // программа на базе СО (обычный случай)
                            p.setBaseLevel(l1);
                            if (EduProgramKindCodes.PROGRAMMA_BAKALAVRIATA.equals(kind.getCode())) {
                                p.setDuration(durationMap.get(400));
                            } else if (EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV.equals(kind.getCode())) {
                                p.setDuration(durationMap.get(500));
                            } else if (EduProgramKindCodes.PROGRAMMA_MAGISTRATURY.equals(kind.getCode())) {
                                p.setBaseLevel(l3); // на базе проффесионального образования
                                p.setDuration(durationMap.get(200));
                            } else if (EduProgramKindCodes.PROGRAMMA_ORDINATURY.equals(kind.getCode())) {
                                p.setBaseLevel(l3); // на базе проффесионального образования
                                p.setDuration(durationMap.get(300));
                            } else if (EduProgramKindCodes.PROGRAMMA_ASPIRANTURY_ADYUNKTURY_.equals(kind.getCode())) {
                                p.setBaseLevel(l3); // на базе проффесионального образования
                                p.setDuration(durationMap.get(300));
                            } else {
                                // ой, что-то новое
                                throw new IllegalStateException();
                            }
                        }
                    }
                    else if (kind.isProgramSecondaryProf())
                    {
                        final EduProgramSecondaryProf p = (EduProgramSecondaryProf)(program = new EduProgramSecondaryProf());
                        p.setYear(year);
                        p.setKind(kind);
                        p.setForm(form);
                        p.setOwnerOrgUnit(ownerOrgUnit);
                        p.setProgramSubject(spec.getProgramSubject());
                        p.setInstitutionOrgUnit(institutionOrgUnit);
                        p.setTitle(spec.getProgramSubject().getTitleWithCode() + "(" + spec.getTitle() + ", " + year.getTitle() + ")");
                        if (p.getTitle().length() > 255)
                            p.setTitle(spec.getProgramSubject().getShortTitleWithCode() + "("+spec.getTitle()+", "+year.getTitle()+")");
                        p.setShortTitle(spec.getShortTitle()+" ("+year.getTitle()+")");

                        // todo прицепить квалификации и сюда тоже

                        p.setBaseLevel(l1); // для СПО всегда общее образование

                        if (random.nextInt(10) < 3)
                        {
                            // углубленное изучение, бывает крайне редко
                            p.setInDepthStudy(true);
                            p.setDuration(durationMap.get(306));
                        }
                        else
                        {
                            // обычное изучение
                            p.setInDepthStudy(false);
                            p.setDuration(durationMap.get(206));
                        }
                    }
                    else
                    {
                        // что-то интересное нашлось :(
                        throw new IllegalStateException();
                    }

                    // сохраняем
                    program.setProgramUniqueKey("debug."+program.generateUniqueKey());
                    if (program.getTitle().length() > 255)
                        program.setTitle(program.getTitle().substring(0, 254));
                    save(program);
                }
            }
        }
    }

    @Override
    public void doFullEcRandom(EnrEnrollmentCampaign ec) {


        // создаем ПК
        // final EnrEnrollmentCampaign ec = doCreateEnrollmentCampaign(getNextEducationYear());
        if (null == ec) { return; }

        // добавляем в нее Акк.ОУ (все, которые есть)
        final Map<EduInstitutionOrgUnit, EnrOrgUnit> ecOrgUnitMap = doCreateEnrollmentOrgUnitMap(ec);
        if (ecOrgUnitMap.isEmpty()) { return; }

        // настройки ВИ
        doFillExamSettings(ec);

        // Индивидуальные достижения в рамках ПК
        doFillAchievmentTypes(ec);

        // Виды целевого приема в рамках пк, организации ЦП в рамках ПК
        doFillTargetAdmission(ec);

        // создаем конкурсы на Акк.ОУ в ПК
        final List<EnrCompetition> cmpList = doCreateEcCompetitionList(ec, ecOrgUnitMap);
        if (cmpList.isEmpty()) { return; }
    }

    private void doFillAchievmentTypes(EnrEnrollmentCampaign ec) {
        final List<EnrEntrantAchievementKind> kinds = getList(EnrEntrantAchievementKind.class);
        for (EnrEntrantAchievementKind kind : kinds) {
            final EnrEntrantAchievementType type = new EnrEntrantAchievementType();
            type.setEnrollmentCampaign(ec);
            type.setAchievementKind(kind);
            type.setMarked(random.nextBoolean());
            Long mark = 1000L + random.nextInt(900)*10;
            type.setAchievementMarkAsLong(mark);
            save(type);
        }
        getSession().flush();
        getSession().clear();
    }

    private void doFillTargetAdmission(EnrEnrollmentCampaign ec) {
        // надо сгенерить виды целевого приема, из них сделать виды ЦП в рамках ПК, добавить связи с внешними организациями

        List<EnrCampaignTargetAdmissionKind> eCTAKinds = new ArrayList<>();
        List<EnrTargetAdmissionKind> taKinds = getList(EnrTargetAdmissionKind.class);

        if (taKinds.isEmpty()) return;

        for (EnrTargetAdmissionKind taKind : taKinds) {
            final EnrCampaignTargetAdmissionKind campKind = new EnrCampaignTargetAdmissionKind();
            campKind.setEnrollmentCampaign(ec);
            campKind.setTargetAdmissionKind(taKind);
            save(campKind);
            eCTAKinds.add(campKind);
        }


        List<ExternalOrgUnit> externalOrgUnits = getList(ExternalOrgUnit.class);
        final int extOuListSize = externalOrgUnits.size();
        if (extOuListSize != 0)
        {
            for (EnrCampaignTargetAdmissionKind kind : eCTAKinds) {
                final EnrTargetAdmissionOrgUnit admissionOrgUnit = new EnrTargetAdmissionOrgUnit(kind, externalOrgUnits.get(random.nextInt(extOuListSize)));
                save(admissionOrgUnit);
            }
        }

        getSession().flush();
        getSession().clear();

    }


    private <T> T choice(List<T> list) {
        return list.get(random.nextInt(list.size()));
    }

    @Override
    public void doFullEcRandomEntrant(EnrEnrollmentCampaign ec, int entrantsNumber) {

        final Session session = getSession();

        final List<EnrEntrantAchievementType> achievementTypes = getList(EnrEntrantAchievementType.class, EnrEntrantAchievementType.enrollmentCampaign(), ec);

        // берем ПК на следующий учебный год
        // final EnrEnrollmentCampaign ec = EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign();
        if (null == ec) { return; }

        // список АккОУ
        final List<EnrOrgUnit> enrollmentOrgUnitList = getList(EnrOrgUnit.class, EnrOrgUnit.enrollmentCampaign(), ec);
        if (enrollmentOrgUnitList.isEmpty()) { return; }

        // наборы ОП
        final Map<EnrProgramSetBase, List<EnrProgramSetItem>> programSetItemMap = SafeMap.get(ArrayList.class);
        for (final EnrProgramSetItem item: getList(EnrProgramSetItem.class, EnrProgramSetItem.programSet().enrollmentCampaign(), ec)) {
            programSetItemMap.get(item.getProgramSet()).add(item);
        }

        // начинаем выдавать номера с максимального - потом все равно пересчитаем
        int reqNumber = Integer.MAX_VALUE-1;

        // конкурсы
        List<EnrCompetition> competitionList = getList(EnrCompetition.class, EnrCompetition.programSetOrgUnit().programSet().enrollmentCampaign(), ec);
        if (competitionList.isEmpty()) {
            return;
        }

        final Set<EnrStateExamSubject> stateExamSubjectList = new HashSet<>(this.<EnrCampaignDiscipline, EnrEnrollmentCampaign, EnrStateExamSubject>getPropertiesList(EnrCampaignDiscipline.class, EnrCampaignDiscipline.enrollmentCampaign(), ec, false, EnrCampaignDiscipline.stateExamSubject()));
        final List<EnrBenefitCategory> noExamCategories = getList(EnrBenefitCategory.class, EnrBenefitCategory.benefitType().code(), EnrBenefitTypeCodes.NO_ENTRANCE_EXAMS);
        final List<EnrBenefitCategory> exclusiveCategories = getList(EnrBenefitCategory.class, EnrBenefitCategory.benefitType().code(), EnrBenefitTypeCodes.EXCLUSIVE);
        final List<EnrCampaignTargetAdmissionKind> targetAdmissionKindList = getList(EnrCampaignTargetAdmissionKind.class, EnrCampaignTargetAdmissionKind.enrollmentCampaign(), ec);
        final List<ExternalOrgUnit> targetAdmissionOrgUnitList = getList(ExternalOrgUnit.class);

        // нам нужен список людей, произвольных
        final List<Person> personList = new DQLSelectBuilder()
        .fromEntity(Person.class, "p")
        .where(isNotNull(property(Person.personEduInstitution().fromAlias("p"))))
        .where(notExists(
            new DQLSelectBuilder()
            .fromEntity(EnrEntrant.class, "e")
            .column(property("e.id"))
            .where(eq(property(EnrEntrant.person().fromAlias("e")), property("p")))
            .where(eq(property(EnrEntrant.enrollmentCampaign().fromAlias("e")), value(ec)))
            .buildQuery()
        ))
        .order(DQLFunctions.random())
        .createStatement(session).setMaxResults(entrantsNumber)
        .list();
        if (personList.isEmpty()) {
            return;
        }

        //final EnrRequestType requestTypeBS = getCatalogItem(EnrRequestType.class, EnrRequestTypeCodes.BS);
        final List<EnrRequestType> requestTypes = getList(EnrRequestType.class);
        final EnrOriginalSubmissionAndReturnWay submissionAndReturnWay = IUniBaseDao.instance.get().getCatalogItem(EnrOriginalSubmissionAndReturnWay.class, EnrOriginalSubmissionAndReturnWayCodes.LICHNO);

        for (final Person person: personList) {

            // создаем абитуриента
            final EnrEntrant entrant = new EnrEntrant();
            entrant.setArchival(false);
            entrant.setPerson(person);
            entrant.setEnrollmentCampaign(ec);
            entrant.setPersonalNumber(Long.toString(IdGen.getTime(person.getId()), 32));
            entrant.setRegistrationDate(new Date());
            save(entrant);

            // ИД
            if (achievementTypes.size() > 0)
                if (dice(8, 10)) {
                    for (int i = random.nextInt(3); i >= 0; i--) {
                        final EnrEntrantAchievement achievement = new EnrEntrantAchievement();
                        achievement.setEntrant(entrant);
                        achievement.setMarkAsLong(1000L + random.nextInt(900) * 10);
                        achievement.setType(choice(achievementTypes));
                        save(achievement);
                    }
                }

            // ЕГЭ
            for (EnrStateExamSubject stateExamSubject : stateExamSubjectList)
            {
                if (dice(100, 5)) continue;

                final EnrEntrantStateExamResult s = new EnrEntrantStateExamResult();
                s.setEntrant(entrant);
                s.setRegistrationDate(new Date());
                s.setAccepted(true);
                s.setYear(2014);
                s.setSubject(stateExamSubject);
                s.setMark(25+random.nextInt(75));
                save(s);
            }

            // набор удостоверений личности (иногда будем смотреть в базу)
            final List<IdentityCard> personIdcList = random.nextBoolean() ? Collections.singletonList(person.getIdentityCard()) : getList(IdentityCard.class, IdentityCard.person(), person);

            // набор документов об образовании (todo optimize)
            final List<PersonEduDocument> personEduInstitutionList =  getList(PersonEduDocument.class, PersonEduDocument.person(), person);

            // перечень выбранных конкурсов (с указанием вида)
            final Set<EnrCompetition> selectedCompetitionSet = new HashSet<>();

            // ДДС
            final Map<INaturalId, EnrExamPassDiscipline> ddsMap = new HashMap<>();

            // создаем заявления
            final Set<INaturalId<EnrEntrantOriginalDocumentStatusGen>> orignalSet = new HashSet<>();
            for (int rqn=1+random.nextInt(4); rqn>0; rqn--)
            {
                // создаем заявление
                final EnrEntrantRequest request = new EnrEntrantRequest();

                request.setType(requestTypes.get(random.nextInt(requestTypes.size())));
                request.setOriginalSubmissionWay(submissionAndReturnWay);
                request.setOriginalReturnWay(submissionAndReturnWay);
                request.setEntrant(entrant);
                request.setRegNumber(String.valueOf(reqNumber--));
                request.setEduDocument(choice(personEduInstitutionList));
                request.setIdentityCard(choice(personIdcList));
                request.setRegDate(new Date());

                // выбираем конкурсы
                Collections.shuffle(competitionList, random);
                for (int cmpn=random.nextInt(Math.min(competitionList.size(), 5)); cmpn>=0; cmpn--)
                {
                    final EnrCompetition competition = competitionList.get(cmpn);

                    // проверяем, что конкурс еще не добавили
                    if (!selectedCompetitionSet.add(competition))
                    {
                        continue;
                    }

                    if (!competition.getRequestType().equals(request.getType()))
                    {
                        cmpn++;
                        continue;
                    }

                    final EnrRequestedCompetition requestCompetition;
                    if (EnrCompetitionTypeCodes.EXCLUSIVE.equals(competition.getType().getCode()))
                    {
                        // не все идут по квоте
                        if (random.nextInt(100)<80) { continue; }
                        requestCompetition = new EnrRequestedCompetitionExclusive();
                        ((EnrRequestedCompetitionExclusive)requestCompetition).setBenefitCategory(choice(exclusiveCategories));
                        ((EnrRequestedCompetitionExclusive)requestCompetition).setProgramSetItem(choice(programSetItemMap.get(competition.getProgramSetOrgUnit().getProgramSet())));
                    }
                    else if (EnrCompetitionTypeCodes.TARGET_ADMISSION.equals(competition.getType().getCode()))
                    {
                        // если нет плана, то выбранных конкурсов быть не может тоже
                        if (competition.getProgramSetOrgUnit().getTargetAdmPlan() == 0) continue;

                        // не все идут по ЦП
                        if (dice(10, 2)) { continue; }
                        if (targetAdmissionKindList.isEmpty()) { continue; }
                        if (targetAdmissionOrgUnitList.isEmpty()) { continue; }
                        requestCompetition = new EnrRequestedCompetitionTA();
                        ((EnrRequestedCompetitionTA)requestCompetition).setTargetAdmissionKind(choice(targetAdmissionKindList));
                        ((EnrRequestedCompetitionTA)requestCompetition).setTargetAdmissionOrgUnit(choice(targetAdmissionOrgUnitList));

                    }
                    else if (EnrCompetitionTypeCodes.NO_EXAM_CONTRACT.equals(competition.getType().getCode()) || EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL.equals(competition.getType().getCode()))
                    {
                        requestCompetition = new EnrRequestedCompetitionNoExams();
                        ((EnrRequestedCompetitionNoExams)requestCompetition).setBenefitCategory(choice(noExamCategories));
                        ((EnrRequestedCompetitionNoExams)requestCompetition).setProgramSetItem(choice(programSetItemMap.get(competition.getProgramSetOrgUnit().getProgramSet())));
                    }
                    else
                    {
                        requestCompetition = new EnrRequestedCompetition();
                    }

                    // ключи
                    requestCompetition.setRequest(request);
                    requestCompetition.setCompetition(competition);
                    requestCompetition.setAccepted(!competition.getType().getCompensationType().isBudget());
                    requestCompetition.setPriority(random.nextInt(2048));
                    requestCompetition.refreshUniqueKey();
                    saveOrUpdate(request);
                    save(requestCompetition);

                    {
                        // выбор ВИ и ФС, создание ДДС

                        final List<EnrExamVariant> ecgExamList = getList(EnrExamVariant.class, EnrExamVariant.examSetVariant(), competition.getExamSetVariant());
                        for (final EnrExamVariant exam: ecgExamList)
                        {
                            final EnrChosenEntranceExam chExam = new EnrChosenEntranceExam();
                            chExam.setRequestedCompetition(requestCompetition);

                            final IEnrExamSetElementValue value = exam.getExamSetElement().getValue();
                            if (value instanceof EnrCampaignDiscipline) {
                                chExam.setDiscipline((EnrCampaignDiscipline) value);
                                if (dice(20, 1)) {
                                    continue;
                                }
                            } else {
                                final List<EnrCampaignDisciplineGroupElement> disciplineList = getList(EnrCampaignDisciplineGroupElement.class, EnrCampaignDisciplineGroupElement.group(), (EnrCampaignDisciplineGroup) value);

                                if (disciplineList.isEmpty()) {
                                    throw new IllegalStateException();
                                }

                                Collections.shuffle(disciplineList, random);
                                chExam.setDiscipline(disciplineList.get(0).getDiscipline());
                            }

                            save(chExam);

                            final List<EnrExamVariantPassForm> allowedPassFormList = getList(EnrExamVariantPassForm.class, EnrExamVariantPassForm.examVariant(), exam);
                            if (allowedPassFormList.isEmpty()) {
                                continue;
                            }
                            Collections.shuffle(allowedPassFormList);

                            // для каждого внутреннего ВВИ - ддс 1, очень редко 2 (по одной и той же форме)
                            boolean passFormNeeded = true;
                            for (EnrExamVariantPassForm allowedPassForm: allowedPassFormList)
                            {
                                EnrExamPassForm passForm = allowedPassForm.getPassForm();
                                if (!passForm.isInternal()) {
                                    // если форма сдачи не внутренняя - то ее выбираем всегда
                                    final EnrChosenEntranceExamForm chExamForm = new EnrChosenEntranceExamForm();
                                    chExamForm.setChosenEntranceExam(chExam);
                                    chExamForm.setPassForm(passForm);
                                    save(chExamForm);
                                    passFormNeeded = false;
                                    continue;
                                }

                                // если форма сдачи внутренняя - то создаем, если еще нет ДДС, или просто так иногда
                                if (passFormNeeded || dice(10, 3)) {
                                    passFormNeeded = false;

                                    final EnrChosenEntranceExamForm chExamForm = new EnrChosenEntranceExamForm();
                                    chExamForm.setChosenEntranceExam(chExam);
                                    chExamForm.setPassForm(passForm);
                                    save(chExamForm);

                                    final EnrExamPassDiscipline dds = new EnrExamPassDiscipline();
                                    dds.setEntrant(entrant);
                                    dds.setDiscipline(chExam.getDiscipline());
                                    dds.setPassForm(chExamForm.getPassForm());
                                    dds.setMarkAsLong((long) (1000 * (25 + random.nextInt(75))));
                                    dds.setModificationDate(new Date());
                                    dds.setMarkDate(new Date());
                                    dds.setTerritorialOrgUnit(requestCompetition.getCompetition().getProgramSetOrgUnit().getOrgUnit().getInstitutionOrgUnit().getOrgUnit());

                                    if (!ddsMap.containsKey(dds.getNaturalId())) {
                                        ddsMap.put(dds.getNaturalId(), dds);
                                        save(dds);
                                    }
                                } else {
                                    // если форма сдачи не внутренняя - то ее выбираем всегда
                                    final EnrChosenEntranceExamForm chExamForm = new EnrChosenEntranceExamForm();
                                    chExamForm.setChosenEntranceExam(chExam);
                                    chExamForm.setPassForm(passForm);
                                    save(chExamForm);
                                }
                            }
                        }
                    }
                }

                // иногда он приносит оригинал
                if (request.getId() != null && random.nextBoolean()) {
                    EnrEntrantOriginalDocumentStatus original = new EnrEntrantOriginalDocumentStatus();
                    original.setEduDocument(request.getEduDocument());
                    original.setEntrant(entrant);
                    original.setRegistrationDate(new Date());
                    if (orignalSet.add(original.getNaturalId())) {
                        session.save(original);
                    }
                }

            }
        }

        session.flush();
        session.clear();

        // перенумеровать заявления (все заявления)
        {
            final List<EnrEntrantRequest> list = getList(EnrEntrantRequest.class, EnrEntrantRequest.entrant().enrollmentCampaign(), ec, EnrEntrantRequest.P_REG_NUMBER);
            int requestNumber = 1;
            for (EnrEntrantRequest erq: list) {
                erq.setRegNumber(String.valueOf(requestNumber++));
                session.save(erq);
                // session.flush(); // здесь flush вызывать не надо, все новые элементы идут с maxInt вниз
            }
            session.flush();
            session.clear();
        }

        // перенумеровать приоритеты
        {
            for (EnrEntrant e: getList(EnrEntrant.class, EnrEntrant.enrollmentCampaign(), ec)) {
                List<EnrRequestedCompetition> list = getList(EnrRequestedCompetition.class, EnrRequestedCompetition.request().entrant(), e, EnrRequestedCompetition.P_PRIORITY);
                int priority = 1;
                for (EnrRequestedCompetition rqc: list) {
                    rqc.setPriority(priority++);
                    session.save(rqc);
                }
                session.flush();
                session.clear();
            }
        }

    }

    @Override
    public void doFullEcRating(EnrEnrollmentCampaign ec) {

        //        final EducationYear year = getNextEducationYear();
        //        final EnrEnrollmentCampaign ec = getByNaturalId(new EnrEnrollmentCampaignGen.NaturalId(year));
        //        if (null == ec) { return; }
        //
        //        Session session = getSession();
        //
        //        // создаем рейтинговые списки по конкурсам, если их еще нет
        //        {
        //            final List<EnrCompetitionType> competitionKindList = new DQLSelectBuilder()
        //            .fromEntity(EnrCompetitionType.class, "ck").column(property("ck"))
        //            .where(notExists(
        //                new DQLSelectBuilder()
        //                .fromEntity(EcCompetitionRatingList.class, "l").column(property("l.id"))
        //                .where(eq(property(EcCompetitionRatingList.competitionKind().fromAlias("l")), property("ck")))
        //                .buildQuery()
        //            ))
        //            .where(eq(property(EnrCompetitionType.competition().programSet().enrollmentOrgUnit().enrollmentCampaign().fromAlias("ck")), value(ec)))
        //            .createStatement(getSession())
        //            .list();
        //
        //            for (final EnrCompetitionType ck: competitionKindList) {
        //                final EcCompetitionRatingList l = new EcCompetitionRatingList();
        //                l.setCompetitionKind(ck);
        //                save(l);
        //            }
        //        }
        //
        //        session.flush();
        //        session.clear();
        //
        //        // для каждого списка обновляем элементы
        //        {
        //            final IEcCompetitionRatingListDao dao = EcCompetitionRatingListManager.instance().dao();
        //            for (final EcCompetitionRatingList l: getList(EcCompetitionRatingList.class, EcCompetitionRatingList.competitionKind().competition().programSet().enrollmentOrgUnit().enrollmentCampaign(), ec)) {
        //                dao.doRefreshRatingList(l);
        //                session.flush();
        //                session.clear();
        //            }
        //        }
    }

    @Override
    public void doDeleteEcEntrants(EnrEnrollmentCampaign ec)
    {
        final Session session = getSession();
	    deleteEntityByCampaign(EnrEnrollmentStep.class, EnrEnrollmentStep.enrollmentCampaign(), ec, session);
	    deleteEntityByCampaign(EnrEnrollmentStepItem.class, EnrEnrollmentStepItem.entity().entrant().enrollmentCampaign(), ec, session);
	    deleteEntityByCampaign(EnrRatingItem.class, EnrRatingItem.requestedCompetition().request().entrant().enrollmentCampaign(), ec, session);
	    deleteEntityByCampaign(EnrEnrollmentExtract.class, EnrEnrollmentExtract.entity().request().entrant().enrollmentCampaign(), ec, session);
	    deleteEntityByCampaign(EnrRequestedCompetition.class, EnrRequestedCompetition.request().entrant().enrollmentCampaign(), ec, session);
	    deleteEntityByCampaign(EnrEntrantRequest.class, EnrEntrantRequest.entrant().enrollmentCampaign(), ec, session);
	    deleteEntityByCampaign(EnrEntrant.class, EnrEntrant.enrollmentCampaign(), ec, session);
    }

	private static void deleteEntityByCampaign(Class entityClass, MetaDSLPath pathToCampaign, EnrEnrollmentCampaign campaign, Session session)
	{
		new DQLDeleteBuilder(entityClass)
				.where(eq(property(pathToCampaign), value(campaign)))
				.createStatement(session).execute();
	}

    @Override
    public void doCleanEnrollmentCampaign(EnrEnrollmentCampaign ec)
    {
        if (existsEntity(EnrEntrant.class, EnrEntrant.L_ENROLLMENT_CAMPAIGN, ec))
            throw new ApplicationException("Невозможно удалить ПК, т.к. в рамках нее уже добавлены данные абитуриентов.");

        Session session = getSession();

        new DQLUpdateBuilder(EnrExamSetVariant.class).set(EnrExamSetVariant.owner().s(), nul(PropertyType.LONG))
        .where(eq(property(EnrExamSetVariant.examSet().enrollmentCampaign()), value(ec)))
        .createStatement(session).execute();

        //Конкурс
        new DQLDeleteBuilder(EnrCompetition.class)
        .fromEntity(EnrProgramSetOrgUnit.class, "ou")
        .where(eq(property(EnrCompetition.programSetOrgUnit().id()), property("ou", EnrProgramSetOrgUnit.id())))
        .fromEntity(EnrProgramSetBase.class, "ps")
        .where(eq(property("ou", EnrProgramSetOrgUnit.programSet().id()), property("ps", EnrProgramSetBase.id())))
        .where(eq(property("ps", EnrProgramSetBase.enrollmentCampaign()), value(ec)))
        .createStatement(session).execute();

        // Набор ОП для приема
        new DQLDeleteBuilder(EnrProgramSetBase.class).where(eq(property(EnrProgramSetBase.enrollmentCampaign()), value(ec))).createStatement(session).execute();

        // Настроенный набор ВИ
        new DQLDeleteBuilder(EnrExamSetVariant.class).where(eq(property(EnrExamSetVariant.examSet().enrollmentCampaign()), value(ec))).createStatement(session).execute();

        // Набор вступительных испытаний (ВИ) // Группа дисциплин включена в набор ВИ. Изменение содержимого группы запрещено.
        new DQLDeleteBuilder(EnrExamSet.class).where(eq(property(EnrExamSet.enrollmentCampaign()), value(ec))).createStatement(session).execute();

        // Группа дисциплин, используемая в рамках ПК // Группа дисциплин включена в набор ВИ. Изменение содержимого группы запрещено.
        new DQLDeleteBuilder(EnrCampaignDisciplineGroup.class).where(eq(property(EnrCampaignDisciplineGroup.enrollmentCampaign()), value(ec))).createStatement(session).execute();

        // ИД в рамках ПК
        new DQLDeleteBuilder(EnrEntrantAchievementType.class).where(eq(property(EnrEntrantAchievementType.enrollmentCampaign()), value(ec))).createStatement(session).execute();

        // ЦП в рамках ПК
        new DQLDeleteBuilder(EnrTargetAdmissionOrgUnit.class)
                .fromDataSource(new DQLSelectBuilder()
                        .fromEntity(EnrTargetAdmissionOrgUnit.class, "a")
                        .where(eq(property("a", EnrTargetAdmissionOrgUnit.enrCampaignTAKind().enrollmentCampaign()), value(ec))).buildQuery(), "e")
                .createStatement(session)
                .execute();
        new DQLDeleteBuilder(EnrCampaignTargetAdmissionKind.class).where(eq(property(EnrCampaignTargetAdmissionKind.enrollmentCampaign()), value(ec))).createStatement(session).execute();


    }

    protected EducationYear getNextEducationYear() {
        final EducationYear year = EducationYear.getCurrentRequired();
        return get(EducationYear.class, EducationYear.intValue(), 1+year.getIntValue());
    }


    /** @return перечень Акк.ОУ (существующие + создает недостающие) */
    private List<EduInstitutionOrgUnit> doCreateEduInstitutionList()
    {
        final List<EduInstitutionOrgUnit> result = getList(EduInstitutionOrgUnit.class);
        if (result.size() < EDU_INSTITUTION_MINIMA) {

            // порядок поиска кандидатов
            final Iterator<String> typeCodes = Arrays.asList(new String[] {
                OrgUnitTypeCodes.TOP,
                OrgUnitTypeCodes.DEPARTMENT,
                OrgUnitTypeCodes.REPRESENTATION,
                OrgUnitTypeCodes.INSTITUTE,
                OrgUnitTypeCodes.FACULTY
            }).iterator();

            // основной цикл
            main: while (result.size() < EDU_INSTITUTION_MINIMA) {
                if (!typeCodes.hasNext()) {
                    // в базе больне нет подразделений
                    throw new IllegalStateException();
                }

                final String typeCode = typeCodes.next();

                final List<OrgUnit> ouList = new DQLSelectBuilder()
                .fromEntity(OrgUnit.class, "ou").column(property("ou"))
                .where(eq(property(OrgUnit.archival().fromAlias("ou")), value(Boolean.FALSE)))
                .where(eq(property(OrgUnit.orgUnitType().code().fromAlias("ou")), value(typeCode)))
                .where(notExists(
                    new DQLSelectBuilder()
                    .fromEntity(EduInstitutionOrgUnit.class, "x").column(property("x.id"))
                    .where(eq(property(EduInstitutionOrgUnit.orgUnit().fromAlias("x")), property("ou")))
                    .where(in(property("x"), result))
                    .buildQuery()
                ))
                .order(random())
                .createStatement(getSession()).list();

                for (final OrgUnit ou: ouList) {
                    if (result.size() >= EDU_INSTITUTION_MINIMA) { break main; }

                    final EduInstitutionOrgUnit i = new EduInstitutionOrgUnit();
                    i.setOrgUnit(ou);
                    save(i);
                    result.add(i);
                }
            }
        }
        return result;
    }

    /** @return перечень отв.подразделений (существующе + создает недостающие) */
    private List<EduOwnerOrgUnit> doCreateEduOwnerList()
    {
        final List<EduOwnerOrgUnit> result = getList(EduOwnerOrgUnit.class);
        if (result.size() < EDU_OWNER_MINIMA) {

            // порядок поиска кандидатов
            final Iterator<String> typeCodes = Arrays.asList(new String[] {
                OrgUnitTypeCodes.CATHEDRA,
                OrgUnitTypeCodes.INSTITUTE
            }).iterator();

            // основной цикл
            main: while (result.size() < EDU_OWNER_MINIMA) {
                if (!typeCodes.hasNext()) {
                    // в базе больне нет подразделений
                    throw new IllegalStateException();
                }

                final String typeCode = typeCodes.next();

                final List<OrgUnit> ouList = new DQLSelectBuilder()
                .fromEntity(OrgUnit.class, "ou").column(property("ou"))
                .where(eq(property(OrgUnit.archival().fromAlias("ou")), value(Boolean.FALSE)))
                .where(eq(property(OrgUnit.orgUnitType().code().fromAlias("ou")), value(typeCode)))
                .where(notExists(
                    new DQLSelectBuilder()
                    .fromEntity(EduOwnerOrgUnit.class, "x").column(property("x.id"))
                    .where(eq(property(EduOwnerOrgUnit.orgUnit().fromAlias("x")), property("ou")))
                    .where(in(property("x"), result))
                    .buildQuery()
                ))
                .order(random())
                .createStatement(getSession()).list();

                for (final OrgUnit ou: ouList) {
                    if (result.size() >= EDU_OWNER_MINIMA) { break main; }

                    final EduOwnerOrgUnit i = new EduOwnerOrgUnit();
                    i.setOrgUnit(ou);
                    save(i);
                    result.add(i);
                }
            }
        }
        return result;
    }


    /** @return перечень направленностей (существующие + создает недостающие) */
    private List<EduProgramSpecialization> doCreateSpecializationList()
    {
        final List<EduProgramSpecialization> result = getList(EduProgramSpecialization.class);
        if (result.size() < EDU_PROGRAM_SPECIALIZATION_MINIMA) {

            // постфиксы для названий направленностей (null - само направление)
            final List<String> postfixList = Arrays.asList(
                null, // основное направление
                "в лесном массиве",
                "на стадионах",
                "в морге",
                "на взлетной полосе",
                "в вакууме",
                "при сверхвысоких температурах",
                "в столовых",
                "в полной темноте",
                "в наряде клоуна",
                "на водной глади",
                "в небесной тверди",
                "под землей",
                "при сверхнизких температурах",
                "на потолке",
                "под проливным дождем",
                "за чашкой чая",
                "в аду",
                "на северном полюсе",
                "в виртуальной реальности",
                "на заднем дворе",
                "в сложных погодных условиях",
                "в подземном бункере",
                "на краю земли",
                "для тех, кто в танке",
                "для чайников",
                "под мухой",
                "на околоземной орбите",
                "в космосе",
                "за пределами Солнечной системы",
                "в вечной мерзлоте",
                "под слоем льда"
            );


            // список направлений профессионального образования, отсортированных произсольно-с-весом
            // с учетом, что на каждые 10 НП 2009 будет 1 НП ОКСО и 3 НП 2013
            final Iterator<Long> scroller = new DQLSelectBuilder()
            .fromEntity(EduProgramSubject.class, "x").column(property("x.id"))
            .where(notExists(
                new DQLSelectBuilder()
                .fromEntity(EduProgramSpecialization.class, "s").column(property("s.id"))
                .where(eq(property(EduProgramSpecialization.programSubject().fromAlias("s")), property("x")))
                .buildQuery()
            ))
            .where(eq(property(EduProgramSubject.subjectIndex().programKind().programProf().fromAlias("x")), value(Boolean.TRUE)))
            .order(
                new DQLCaseExpressionBuilder()
                .when(instanceOf("x", EduProgramSubjectOkso.class), mul(value(10), random()))
                .when(instanceOf("x", EduProgramSubject2009.class), mul(value(1), random()))
                .when(instanceOf("x", EduProgramSubject2013.class), mul(value(3), random()))
                .otherwise(mul(value(100), random()))
                .build()
            )
            .createStatement(getSession())
            .<Long>list()
            .iterator();

            // основной цикл
            while (result.size() < EDU_PROGRAM_SPECIALIZATION_MINIMA) {
                if (!scroller.hasNext()) {
                    // в базе больше нет НП
                    throw new IllegalStateException();
                }

                final EduProgramSubject subject = get(EduProgramSubject.class, scroller.next());
                if (subject.getSubjectIndex().getProgramKind().isProgramHigherProf())
                {
                    // для ВПО будем делать профили и специализации

                    // перед каждым использованием перемешиваем постфиксы
                    Collections.shuffle(postfixList, random);
                    final Iterator<String> postfixIt = postfixList.iterator();

                    // создаем произвольное количество обобщенных направленностей
                    for (int i = random.nextInt(2*EDU_PROGRAM_SPECIALIZATION_PER_SUBJECT); i>=0; i--)
                    {
                        final String postfix = postfixIt.next();
                        if (null == postfix) {
                            // если постфикса нет, создаем общую направленность
                            final EduProgramSpecializationRoot sp = new EduProgramSpecializationRoot();
                            sp.setCode("debug."+subject.getCode()+".root");
                            sp.setProgramSubject(subject);
                            sp.setTitle(subject.getTitle());
	                        sp.setShortTitle(SpecializationShortTitleBuilder.buildRootShortTitle(sp));
                            save(sp);
                            result.add(sp);
                        }
                        else
                        {
                            // если постфикс есть, то это профиль или специализация
                            final String title = subject.getTitle() + " " + postfix;
                            final EduProgramSpecializationChild sp = new EduProgramSpecializationChild();
                            sp.setCode("debug."+subject.getCode()+".ch."+i);
                            sp.setProgramSubject(subject);
                            sp.setTitle(title);
	                        sp.setShortTitle(SpecializationShortTitleBuilder.buildChildShortTitle(sp));
                            save(sp);
                            result.add(sp);
                        }
                    }
                }
                else
                {
                    // иначе, делаем только по самому направлению
                    final EduProgramSpecializationRoot sp = new EduProgramSpecializationRoot();
                    sp.setCode("debug."+subject.getCode()+".root");
                    sp.setProgramSubject(subject);
                    sp.setTitle(subject.getTitle());
	                sp.setShortTitle(SpecializationShortTitleBuilder.buildRootShortTitle(sp));
                    save(sp);
                    result.add(sp);
                }

            }

        }
        return result;
    }

    /** @return перечень обобщенных направленностей, по которым нет образовательных программ */
    private List<EduProgramSpecialization> getUncoveredSpecializationList(final List<EduProgramSpecialization> specializationList, final EducationYear year) {
        final Set<EduProgramSpecialization> result = new HashSet<>();
        BatchUtils.execute(specializationList, 200, elements -> {

            // корневое направление (профобразование)
            result.addAll(
                new DQLSelectBuilder()
                .fromEntity(EduProgramSpecializationRoot.class, "x").column(property("x"))
                .where(in(property("x"), elements))
                .where(and(
                    notExists(
                        new DQLSelectBuilder()
                        .fromEntity(EduProgramProf.class, "p1")
                        .column(property("p1.id"))
                        .where(eq(property(EduProgramProf.programSubject().fromAlias("p1")), property(EduProgramSpecializationRoot.programSubject().fromAlias("x"))))
                        .where(eq(property(EduProgramProf.year().fromAlias("p1")), value(year)))
                        .buildQuery()
                    ),
                    notExists(
                        new DQLSelectBuilder()
                        .fromEntity(EduProgramHigherProf.class, "p2")
                        .column(property("p2.id"))
                        .where(eq(property(EduProgramHigherProf.programSpecialization().fromAlias("p2")), property("x")))
                        .where(eq(property(EduProgramProf.year().fromAlias("p2")), value(year)))
                        .buildQuery()
                    )
                ))
                .createStatement(getSession())
                .<EduProgramSpecialization>list()
            );

            // профиль, специализация (только для ВПО)
            result.addAll(
                new DQLSelectBuilder()
                .fromEntity(EduProgramSpecializationChild.class, "x").column(property("x"))
                .where(in(property("x"), elements))
                .where(and(
                    notExists(
                        new DQLSelectBuilder()
                        .fromEntity(EduProgramHigherProf.class, "p2")
                        .column(property("p2.id"))
                        .where(eq(property(EduProgramHigherProf.programSpecialization().fromAlias("p2")), property("x")))
                        .where(eq(property(EduProgramProf.year().fromAlias("p2")), value(year)))
                        .buildQuery()
                    )
                ))
                .createStatement(getSession())
                .<EduProgramSpecialization>list()
            );

        });
        return new ArrayList<>(result);
    }

    /** @return { (100*y + m) -> duration } */
    private Map<Integer, EduProgramDuration> getDurationMap()
    {
        final Map<Integer, EduProgramDuration> map = SafeMap.get(key -> {
            final int y = key / 100;
            final int m = key % 100;
            final EduProgramDuration d = new EduProgramDuration();
            d.setCode("debug."+y+"."+m);
            d.setNumberOfYears(y);
            d.setNumberOfMonths(m);
            d.setTitle(CommonBaseDateUtil.getDatePeriodWithNames(y, m, null));
            save(d);
            return d;
        });

        for (final EduProgramDuration d: getList(EduProgramDuration.class)) {
            final int key = (100*d.getNumberOfYears()) + d.getNumberOfMonths();
            if (null != map.put(key, d)) {
                // опа, в базе не уникальные пары число лет + число месяцев
                throw new IllegalStateException();
            }
        }
        return map;
    }




    /** @return ПК, на указанный год (создает новую, если требуется) */
    private EnrEnrollmentCampaign doFillExamSettings(final EnrEnrollmentCampaign ec) {

        final String prefix = DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()).replace(' ', '-')+"-"+(System.currentTimeMillis()%(24*60*60*1000));
        final IEnrExamSetDao examSetDao = EnrExamSetManager.instance().dao();

        // дисциплины и группы
        {
            final List<EnrCampaignDiscipline> campDisciplineList;
            {
                final List<EnrDiscipline> disciplineList = getList(EnrDiscipline.class);
                final List<EnrStateExamSubject> stateExamSubjectList = getList(EnrStateExamSubject.class);

                final Map<String, EnrStateExamSubject> stateExamSubjectMap = new HashMap<>();

                for (EnrStateExamSubject enrStateExamSubject : stateExamSubjectList) {
                    stateExamSubjectMap.put(enrStateExamSubject.getTitle(), enrStateExamSubject);
                }

                // заполняем ПК дисциплинами
                campDisciplineList = new MergeAction.SessionMergeAction<INaturalId, EnrCampaignDiscipline>() {
                    @Override protected INaturalId key(final EnrCampaignDiscipline source) { return source.getNaturalId(); }
                    @Override protected EnrCampaignDiscipline buildRow(final EnrCampaignDiscipline source) { return source; }
                    @Override protected void fill(final EnrCampaignDiscipline target, final EnrCampaignDiscipline source) { target.update(source, false); }
                }.merge(
                    getList(EnrCampaignDiscipline.class, EnrCampaignDiscipline.enrollmentCampaign(), ec),
                    CollectionUtils.collect(disciplineList, discipline -> {
                        final EnrCampaignDiscipline dsc = new EnrCampaignDiscipline();
                        dsc.setDiscipline(discipline);
                        dsc.setEnrollmentCampaign(ec);
                        dsc.setStateExamSubject(stateExamSubjectMap.get(dsc.getTitle()));
                        return dsc;
                    })
                );
            }

            // добавляем в ПК группы дисциплин
            for (int i=0; i<2; i++)
            {
                final EnrCampaignDisciplineGroup grp = new EnrCampaignDisciplineGroup();
                grp.setEnrollmentCampaign(ec);
                grp.setTitle(prefix+"-"+i);
                grp.setShortTitle(grp.getTitle());
                save(grp);

                final int jMax = 2+random.nextInt(2);
                for (int j=0;j<jMax;j++) {
                    final EnrCampaignDisciplineGroupElement grpElement = new EnrCampaignDisciplineGroupElement();
                    grpElement.setGroup(grp);
                    grpElement.setDiscipline(campDisciplineList.remove(random.nextInt(campDisciplineList.size())));
                    save(grpElement);
                }
            }
        }

        // наборы ВИ
        {
            final List<EnrExamType> examTypeList = getList(EnrExamType.class);
            final List<IEnrExamSetElementValue> setValueList = new ArrayList<>();
            setValueList.addAll(getList(EnrCampaignDiscipline.class, EnrCampaignDiscipline.enrollmentCampaign(), ec));
            setValueList.addAll(getList(EnrCampaignDisciplineGroup.class, EnrCampaignDisciplineGroup.enrollmentCampaign(), ec));

            for (int i=0;i<10;i++) {

                final List<IEnrExamSetDao.IExamSetElementWrapper> elementWrapperList = new ArrayList<>();
                final int jMax = 2+random.nextInt(2);

                //                boolean pp = false;
                final List<IEnrExamSetElementValue> list = new ArrayList<>(setValueList);
                final Set<EnrCampaignDiscipline> set = new HashSet<>();

                for (int j=0;j<jMax;j++) {
                    final ExamSetElementWrapper examSetElementWrapper = new ExamSetElementWrapper();
                    examSetElementWrapper.setExamType(examTypeList.get(random.nextInt(examTypeList.size())));

                    while (!list.isEmpty()) {
                        final IEnrExamSetElementValue value = list.remove(random.nextInt(list.size()));

                        if (value instanceof EnrCampaignDiscipline) {
                            if (set.contains(value)) { continue; }
                            set.add((EnrCampaignDiscipline)value);
                        } else {
                            boolean has = false;
                            final List<EnrCampaignDisciplineGroupElement> x = getList(EnrCampaignDisciplineGroupElement.class, EnrCampaignDisciplineGroupElement.group(), (EnrCampaignDisciplineGroup)value);
                            for (final EnrCampaignDisciplineGroupElement e: x) {
                                if (set.contains(e.getDiscipline())) { has = true; break; }
                            }
                            if (has) { continue; }
                            for (final EnrCampaignDisciplineGroupElement e: x) {
                                set.add(e.getDiscipline());
                            }
                        }

                        examSetElementWrapper.setElementValue(value);
                        elementWrapperList.add(examSetElementWrapper);
                        break;
                    }
                }

                final EnrExamSet examSet = new EnrExamSet();
                examSet.setEnrollmentCampaign(ec);
                examSetDao.saveOrUpdateExamSet(examSet, elementWrapperList);
            }

        }

        return ec;
    }

    /** @return Список Акк.ОУ в ПК (существующие + создает недостающие) */
    private Map<EduInstitutionOrgUnit, EnrOrgUnit> doCreateEnrollmentOrgUnitMap(final EnrEnrollmentCampaign ec) {
        final Map<EduInstitutionOrgUnit, EnrOrgUnit> result = new HashMap<>();

        for (final EnrOrgUnit ecou: getList(EnrOrgUnit.class, EnrOrgUnit.enrollmentCampaign(), ec)) {
            if (null != result.put(ecou.getInstitutionOrgUnit(), ecou)) {
                throw new IllegalStateException();
            }
        }

        final List<EduInstitutionOrgUnit> ouList = new DQLSelectBuilder()
        .fromEntity(EduInstitutionOrgUnit.class, "i").column(property("i"))
        .where(exists(
            // только те АккОУ, на которых есть программы на этот год
            new DQLSelectBuilder()
            .fromEntity(EduProgramProf.class, "p").column(property("p.id"))
            .where(eq(property(EduProgramProf.institutionOrgUnit().fromAlias("p")), property("i")))
            .where(eq(property(EduProgramProf.year().fromAlias("p")), value(ec.getEducationYear())))
            .buildQuery()
        ))
        .where(notExists(
            // только те АккОУ, которые еще не включены в ПК
            new DQLSelectBuilder()
            .fromEntity(EnrOrgUnit.class, "o").column(property("o.id"))
            .where(eq(property(EnrOrgUnit.institutionOrgUnit().fromAlias("o")), property("i")))
            .where(eq(property(EnrOrgUnit.enrollmentCampaign().fromAlias("o")), value(ec)))
            .buildQuery()
        ))
        .createStatement(getSession()).list();

        for (final EduInstitutionOrgUnit ou: ouList) {
            final EnrOrgUnit x = new EnrOrgUnit();
            x.setEnrollmentCampaign(ec);
            x.setInstitutionOrgUnit(ou);
            save(x);
            if (null != result.put(x.getInstitutionOrgUnit(), x)) {
                throw new IllegalSelectorException();
            }
        }

        return result;
    }


    /** @return создает конкурсы на АккОУ в ПК (все ОП-ВПО на год должны попасть в конкурс, иначе, зачем же их создавали) */
    private List<EnrCompetition> doCreateEcCompetitionList(final EnrEnrollmentCampaign ec, final Map<EduInstitutionOrgUnit, EnrOrgUnit> ecOrgUnitMap) {
        // Важно, здесь некоторые методы отмечены как @Deprecated, и являются формулами.
        // На деле, необходимо выставлять руками, иначе при генерации будут иметь неправильное состояние.

        {
            // Высшее образование


            final List<EduProgramHigherProf> programList = new DQLSelectBuilder()
                    .fromEntity(EduProgramHigherProf.class, "p").column(property("p"))
                    .where(eq(property(EduProgramHigherProf.year().fromAlias("p")), value(ec.getEducationYear())))
                    .where(in(property(EduProgramHigherProf.institutionOrgUnit().id().fromAlias("p")), ecOrgUnitMap.keySet()))
                    .where(or(
                            eq(property(EduProgramHigherProf.programSubject().subjectIndex().programKind().programBachelorDegree().fromAlias("p")), value(Boolean.TRUE)),
                            eq(property(EduProgramHigherProf.programSubject().subjectIndex().programKind().programSpecialistDegree().fromAlias("p")), value(Boolean.TRUE)),
                            eq(property(EduProgramHigherProf.programSubject().subjectIndex().programKind().programHigherProf().fromAlias("p")), value(Boolean.TRUE)),
                            eq(property(EduProgramHigherProf.programSubject().subjectIndex().programKind().programMasterDegree().fromAlias("p")), value(Boolean.TRUE))
                    ))
                    .where(notExists(
                            new DQLSelectBuilder()
                                    .fromEntity(EnrProgramSetItem.class, "i").column(property("i.id"))
                                    .where(eq(property(EnrProgramSetItem.program().fromAlias("i")), property("p")))
                                    .where(eq(property(EnrProgramSetItem.programSet().enrollmentCampaign().fromAlias("i")), value(ec)))
                                    .buildQuery()
                    ))
                    .createStatement(getSession()).list();

            List<EnrExamSet> examSetList = getList(EnrExamSet.class, EnrExamSet.enrollmentCampaign(), ec);
            List<EnrOrgUnit> orgUnits = new ArrayList<>(ecOrgUnitMap.values());

            // бакалавриат, специалитет

            {
                int count = 0;
                if (!programList.isEmpty()) {

                    // будем включать программы в новые конкусры (искать подходящие конкурсы среди добавленных варьируя параметры конкурсов)
                    //            final Set<INaturalId> programSetItemUniqSet = new HashSet<>();
                    final Map<String, EnrProgramSetBS> programSetMap = new HashMap<>();
                    for (final EduProgramHigherProf p : programList) {
                        if (!p.getKind().isBS()) continue;

                        count++;
                        if (count > PROGRAM_SET_COUNT) break;

                        // конструируем рандомный конкурс под ОП
                        final EnrProgramSetBS tmp = new EnrProgramSetBS();
                        tmp.setRequestType(this.<EnrRequestType>getByNaturalId(new EnrRequestTypeGen.NaturalId(EnrRequestTypeCodes.BS)));
                        tmp.setEnrollmentCampaign(ec);
                        tmp.setProgramForm(p.getForm());
                        tmp.setProgramSubject(p.getProgramSubject());
                        tmp.setTitle(tmp.generateTitle());
                        tmp.setMethodDivCompetitions(getRandomMethodDivCompetitions(tmp));

                        // формируем ключ
                        final String key = String.valueOf(tmp.getProgramForm().getId()) + ('\n')
                                + (tmp.getProgramSubject().getId())+('\n')
                                + ((dice(10, 8)) ? 0 : 1 /* рандомизатор, нелинейный */);

                        EnrProgramSetBS programSet = programSetMap.get(key);
                        if (null == programSet) {
                            // это новый ключ, сохраняем конкурс
                            save(programSet = tmp);
                            programSetMap.put(key, programSet);

                            Collections.shuffle(orgUnits, random);
                            for (EnrOrgUnit orgUnit : orgUnits) {
                                if (orgUnits.indexOf(orgUnit) != 0 && dice(10, 3))
                                    continue;
                                EnrProgramSetOrgUnit psOu = new EnrProgramSetOrgUnit(programSet, orgUnit);
                                psOu.setFormativeOrgUnit(orgUnit.getInstitutionOrgUnit().getOrgUnit());
                                psOu.setContractPlan(10 + random.nextInt(30));
                                psOu.setContractOpen(true);

                                if (dice(10, 5)) {
                                    psOu.setMinisterialPlan(random.nextInt(50));
                                    psOu.setExclusivePlan(Math.round(psOu.getMinisterialPlan() / 10));
                                    if (dice(10, 5)) {
                                        psOu.setTargetAdmPlan(Math.round(psOu.getMinisterialPlan() * random.nextInt(10) / 20));
                                    }
                                    if (psOu.getMinisterialPlan() > 0)
                                        psOu.setMinisterialOpen(true);
                                }
                                save(psOu);

                                if (psOu.getTargetAdmPlan() > 0) {
                                    // здесь распределение ТА плана между организациями
                                    addTargetAdmPlansToPSOU(psOu);
                                    psOu.setTargetAdmOpen(true);
                                }
                            }

                            getSession().flush();

                            if (examSetList.isEmpty())
                                continue;

                            updateExamSetVariantsForProgramSet(programSet, examSetList);
                        }

                        // включаем программу в конкурс (программа может быть включена в несколько наборов программ)
                        save(new EnrProgramSetItem(programSet, p));
                    }
                }
            }



            // магистранты
            if (!BS_ONLY) {
                int count = 0;
                if (!programList.isEmpty()) {

                    // будем включать программы в новые конкусры (искать подходящие конкурсы среди добавленных варьируя параметры конкурсов)
                    //            final Set<INaturalId> programSetItemUniqSet = new HashSet<>();
                    final Map<String, EnrProgramSetMaster> programSetMap = new HashMap<>();
                    for (final EduProgramHigherProf p : programList) {

                        if (!p.getKind().getCode().equals(EduProgramKindCodes.PROGRAMMA_MAGISTRATURY)) continue;

                        count++;
                        if (count > PROGRAM_SET_COUNT) break;

                        // конструируем рандомный конкурс под ОП
                        final EnrProgramSetMaster tmp = new EnrProgramSetMaster();
                        tmp.setRequestType(this.<EnrRequestType>getByNaturalId(new EnrRequestTypeGen.NaturalId(EnrRequestTypeCodes.MASTER)));
                        tmp.setEnrollmentCampaign(ec);
                        tmp.setProgramForm(p.getForm());
                        tmp.setProgramSubject(p.getProgramSubject());
                        tmp.setTitle(tmp.getProgramSubject().getTitleWithCode() + ", " + tmp.getProgramForm().getShortTitle());
                        tmp.setMethodDivCompetitions(getRandomMethodDivCompetitions(tmp));


                        final EnrExamSetVariant examSetVariant = doCreateExamSetVariant(tmp, examSetList.get(random.nextInt(examSetList.size())));
                        tmp.setExamSetVariant(examSetVariant);
                        tmp.setExamSetVariantCon(examSetVariant);
                        tmp.setExamSetVariantMin(examSetVariant);



                        // формируем ключ
                        final String key = String.valueOf(tmp.getProgramForm().getId()) + ('\n')
                                + (tmp.getProgramSubject().getId())+('\n')
                                + ((dice(10, 8)) ? 0 : 1 /* рандомизатор, нелинейный */);

                        EnrProgramSetMaster programSet = programSetMap.get(key);
                        if (null == programSet) {
                            // это новый ключ, сохраняем конкурс
                            save(programSet = tmp);
                            examSetVariant.setOwner(tmp);
                            update(examSetVariant);
                            programSetMap.put(key, programSet);

                            Collections.shuffle(orgUnits, random);
                            for (EnrOrgUnit orgUnit : orgUnits) {
                                if (orgUnits.indexOf(orgUnit) != 0 && dice(10, 3))
                                    continue;
                                EnrProgramSetOrgUnit psOu = new EnrProgramSetOrgUnit(programSet, orgUnit);
                                psOu.setFormativeOrgUnit(orgUnit.getInstitutionOrgUnit().getOrgUnit());
                                psOu.setContractPlan(10 + random.nextInt(30));
                                psOu.setContractOpen(true);

                                if (dice(10, 5)) {
                                    psOu.setMinisterialPlan(random.nextInt(50));
                                    psOu.setExclusivePlan(Math.round(psOu.getMinisterialPlan() / 10));
                                    if (dice(10, 5)) {
                                        psOu.setTargetAdmPlan(Math.round(psOu.getMinisterialPlan() * random.nextInt(10) / 20));
                                    }
                                    if (psOu.getMinisterialPlan() > 0)
                                        psOu.setMinisterialOpen(true);
                                }
                                save(psOu);

                                if (psOu.getTargetAdmPlan() > 0) {
                                    // здесь распределение ТА плана между организациями
                                    addTargetAdmPlansToPSOU(psOu);
                                    psOu.setTargetAdmOpen(true);
                                }
                            }

                            getSession().flush();

                        }

                        // включаем программу в конкурс (программа может быть включена в несколько наборов программ)
                        save(new EnrProgramSetItem(programSet, p));
                    }
                }
            }

            // аспиранты
            if (!BS_ONLY) {
                int count = 0;
                if (!programList.isEmpty()) {

                    // будем включать программы в новые конкусры (искать подходящие конкурсы среди добавленных варьируя параметры конкурсов)
                    //            final Set<INaturalId> programSetItemUniqSet = new HashSet<>();
                    final Map<String, EnrProgramSetHigher> programSetMap = new HashMap<>();
                    for (final EduProgramHigherProf p : programList) {

                        if (!p.getKind().getCode().equals(EduProgramKindCodes.PROGRAMMA_ASPIRANTURY_ADYUNKTURY_)) continue;

                        count++;
                        if (count > PROGRAM_SET_COUNT) break;

                        // конструируем рандомный конкурс под ОП
                        final EnrProgramSetHigher tmp = new EnrProgramSetHigher();
                        tmp.setEnrollmentCampaign(ec);
                        tmp.setProgramForm(p.getForm());
                        tmp.setProgramSubject(p.getProgramSubject());
                        tmp.setTitle(tmp.getProgramSubject().getTitleWithCode() + ", " + tmp.getProgramForm().getShortTitle());
                        tmp.setMethodDivCompetitions(getRandomMethodDivCompetitions(tmp));
                        tmp.setRequestType(getCatalogItem(EnrRequestType.class, EnrRequestTypeCodes.POSTGRADUATE));

                        final EnrExamSetVariant examSetVariant = doCreateExamSetVariant(tmp, examSetList.get(random.nextInt(examSetList.size())));
                        tmp.setExamSetVariant(examSetVariant);

                        // формируем ключ
                        final String key = String.valueOf(tmp.getProgramForm().getId()) + ('\n')
                                + (tmp.getProgramSubject().getId())+('\n')
                                + ((dice(10, 8)) ? 0 : 1 /* рандомизатор, нелинейный */);

                        EnrProgramSetHigher programSet = programSetMap.get(key);
                        if (null == programSet) {
                            // это новый ключ, сохраняем конкурс
                            save(programSet = tmp);
                            examSetVariant.setOwner(tmp);
                            update(examSetVariant);
                            programSetMap.put(key, programSet);

                            Collections.shuffle(orgUnits, random);
                            for (EnrOrgUnit orgUnit : orgUnits) {
                                if (orgUnits.indexOf(orgUnit) != 0 && dice(10, 3))
                                    continue;
                                EnrProgramSetOrgUnit psOu = new EnrProgramSetOrgUnit(programSet, orgUnit);
                                psOu.setFormativeOrgUnit(orgUnit.getInstitutionOrgUnit().getOrgUnit());
                                psOu.setContractPlan(10 + random.nextInt(30));
                                psOu.setContractOpen(true);

                                if (dice(10, 5)) {
                                    psOu.setMinisterialPlan(random.nextInt(50));
                                    psOu.setExclusivePlan(Math.round(psOu.getMinisterialPlan() / 10));
                                    if (dice(10, 5)) {
                                        psOu.setTargetAdmPlan(Math.round(psOu.getMinisterialPlan() * random.nextInt(10) / 20));
                                    }
                                    if (psOu.getMinisterialPlan() > 0)
                                        psOu.setMinisterialOpen(true);
                                }
                                save(psOu);

                                if (psOu.getTargetAdmPlan() > 0) {
                                    // здесь распределение ТА плана между организациями
                                    addTargetAdmPlansToPSOU(psOu);
                                    psOu.setTargetAdmOpen(true);
                                }
                            }

                            getSession().flush();

                        }

                        // включаем программу в конкурс (программа может быть включена в несколько наборов программ)
                        save(new EnrProgramSetItem(programSet, p));
                    }
                }
            }

        }

        if (!BS_ONLY) {
            // СПО
            final List<EduProgramSecondaryProf> programList = new DQLSelectBuilder()
                    .fromEntity(EduProgramSecondaryProf.class, "p").column(property("p"))
                    .where(eq(property(EduProgramSecondaryProf.year().fromAlias("p")), value(ec.getEducationYear())))
                    .where(in(property(EduProgramSecondaryProf.institutionOrgUnit().id().fromAlias("p")), ecOrgUnitMap.keySet()))
                    // только спо ?
                    .where(eq(property(EduProgramSecondaryProf.programSubject().subjectIndex().programKind().programSecondaryProf().fromAlias("p")), value(Boolean.TRUE)))
                    .where(notExists(
                            new DQLSelectBuilder()
                                    .fromEntity(EnrProgramSetItem.class, "i").column(property("i.id"))
                                    .where(eq(property(EnrProgramSetItem.program().fromAlias("i")), property("p")))
                                    .where(eq(property(EnrProgramSetItem.programSet().enrollmentCampaign().fromAlias("i")), value(ec)))
                                    .buildQuery()
                    ))
                    .createStatement(getSession()).list();

            List<EnrExamSet> examSetList = getList(EnrExamSet.class, EnrExamSet.enrollmentCampaign(), ec);
            List<EnrOrgUnit> orgUnits = new ArrayList<>(ecOrgUnitMap.values());
            //List<EnrExamPassForm> passForms = getList(EnrExamPassForm.class);

            int count = 0;
            if (!programList.isEmpty()) {

                // будем включать программы в новые конкусры (искать подходящие конкурсы среди добавленных варьируя параметры конкурсов)
                //            final Set<INaturalId> programSetItemUniqSet = new HashSet<>();
                final Map<String, EnrProgramSetSecondary> programSetMap = new HashMap<>();
                for (final EduProgramSecondaryProf p : programList) {

                    count++;
                    if (count > PROGRAM_SET_COUNT) break;

                    // конструируем рандомный конкурс под ОП
                    final EnrProgramSetSecondary tmp = new EnrProgramSetSecondary();
                    tmp.setEnrollmentCampaign(ec);
                    tmp.setProgramForm(p.getForm());
                    tmp.setProgramSubject(p.getProgramSubject());
                    tmp.setTitle(tmp.getProgramSubject().getTitleWithCode() + ", " + tmp.getProgramForm().getShortTitle());
                    tmp.setMethodDivCompetitions(getRandomMethodDivCompetitions(tmp));
                    tmp.setProgram(p);

                    final EnrExamSetVariant examSetVariant = doCreateExamSetVariant(tmp, examSetList.get(random.nextInt(examSetList.size())));
                    tmp.setExamSetVariant(examSetVariant);

                    // формируем ключ
                    final String key = String.valueOf(tmp.getProgramForm().getId()) + ('\n')
                            + (tmp.getProgramSubject().getId())+('\n')
                            + ((dice(10, 8)) ? 0 : 1 /* рандомизатор, нелинейный */);

                    EnrProgramSetSecondary programSet = programSetMap.get(key);
                    if (null == programSet) {
                        // это новый ключ, сохраняем конкурс
                        save(programSet = tmp);
                        examSetVariant.setOwner(tmp);
                        programSetMap.put(key, programSet);

                        Collections.shuffle(orgUnits, random);
                        for (EnrOrgUnit orgUnit : orgUnits) {
                            if (orgUnits.indexOf(orgUnit) != 0 && dice(10, 3))
                                continue;
                            EnrProgramSetOrgUnit psOu = new EnrProgramSetOrgUnit(programSet, orgUnit);
                            psOu.setFormativeOrgUnit(orgUnit.getInstitutionOrgUnit().getOrgUnit());
                            psOu.setContractPlan(10 + random.nextInt(30));
                            psOu.setContractOpen(true);

                            if (dice(10, 5)) {
                                psOu.setMinisterialPlan(random.nextInt(50));
                                psOu.setExclusivePlan(Math.round(psOu.getMinisterialPlan() / 10));
                                if (dice(10, 5)) {
                                    psOu.setTargetAdmPlan(Math.round(psOu.getMinisterialPlan() * random.nextInt(10) / 20));
                                }
                                if (psOu.getMinisterialPlan() > 0)
                                    psOu.setMinisterialOpen(true);
                            }
                            save(psOu);

                            if (psOu.getTargetAdmPlan() > 0) {
                                // здесь распределение ТА плана между организациями
                                addTargetAdmPlansToPSOU(psOu);
                                psOu.setTargetAdmOpen(true);

                            }
                        }
                        saveOrUpdate(examSetVariant);

                        getSession().flush();
                    }
                }
            }
        }

        // исправляем названия для наборов ОП и создаем конкурсы
        {
            int n = 1;
            for (EnrProgramSetBase prSet: getList(EnrProgramSetBase.class, EnrProgramSetBS.enrollmentCampaign(), ec)) {
                prSet.setTitle(prSet.getTitle() + " №" + (n++));
                saveOrUpdate(prSet);
                EnrProgramSetManager.instance().dao().doOpenCompetition(prSet);
            }
        }

        // возвращаем все, что в базе
        return getList(EnrCompetition.class, EnrCompetition.programSetOrgUnit().programSet().enrollmentCampaign(), ec);
    }

    private void addTargetAdmPlansToPSOU(EnrProgramSetOrgUnit psOu) {
        int nonFilledLeft = psOu.getTargetAdmPlan();
        List<EnrCampaignTargetAdmissionKind> kindList = getList(EnrCampaignTargetAdmissionKind.class, EnrCampaignTargetAdmissionKind.enrollmentCampaign(), psOu.getProgramSet().getEnrollmentCampaign());

        Collections.shuffle(kindList, random);

        for (EnrCampaignTargetAdmissionKind kind : kindList) {
            int plan = random.nextInt(nonFilledLeft);
            new EnrTargetAdmissionPlan(psOu, kind, plan);
            nonFilledLeft = nonFilledLeft - plan;
            if (nonFilledLeft == 0) return;
        }
    }

    private void updateExamSetVariantsForProgramSet(EnrProgramSetBase programSet, List<EnrExamSet> examSetList) {

        EnrExamSetVariant variant = doCreateExamSetVariant(programSet, examSetList.get(random.nextInt(examSetList.size())));

        switch (programSet.getMethodDivCompetitions().getCode())
        {
            case EnrMethodDivCompetitionsCodes.BS_NO_DIV :
            {
                EnrProgramSetBS setBS = (EnrProgramSetBS) programSet;
                setBS.setExamSetVariantBase(variant);
                setBS.setExamSetVariantHigh(variant);
                setBS.setExamSetVariantSec(variant);
                break;
            }
            case EnrMethodDivCompetitionsCodes.BS_SOO_AND_SPO_APART_VO :
            {
                EnrProgramSetBS setBS = (EnrProgramSetBS) programSet;
                setBS.setExamSetVariantBase(variant);
                setBS.setExamSetVariantHigh(doCreateExamSetVariant(programSet, examSetList.get(random.nextInt(examSetList.size()))));
                setBS.setExamSetVariantSec(variant);
                break;
            }
            case EnrMethodDivCompetitionsCodes.BS_SOO_AND_VO_APART_SPO :
            {
                EnrProgramSetBS setBS = (EnrProgramSetBS) programSet;
                setBS.setExamSetVariantBase(variant);
                setBS.setExamSetVariantHigh(variant);
                setBS.setExamSetVariantSec(doCreateExamSetVariant(programSet, examSetList.get(random.nextInt(examSetList.size()))));
                break;
            }
            case EnrMethodDivCompetitionsCodes.BS_SOO_PO :
            {
                EnrProgramSetBS setBS = (EnrProgramSetBS) programSet;
                setBS.setExamSetVariantBase(doCreateExamSetVariant(programSet, examSetList.get(random.nextInt(examSetList.size()))));
                setBS.setExamSetVariantHigh(variant);
                setBS.setExamSetVariantSec(variant);
                break;
            }
            case EnrMethodDivCompetitionsCodes.BS_SOO_SPO_VO :
            {
                EnrProgramSetBS setBS = (EnrProgramSetBS) programSet;
                setBS.setExamSetVariantBase(variant);
                setBS.setExamSetVariantHigh(doCreateExamSetVariant(programSet, examSetList.get(random.nextInt(examSetList.size()))));
                setBS.setExamSetVariantSec(doCreateExamSetVariant(programSet, examSetList.get(random.nextInt(examSetList.size()))));
                break;
            }

            case EnrMethodDivCompetitionsCodes.SEC_NO_DIV :
            {
                EnrProgramSetSecondary setSecondary = (EnrProgramSetSecondary) programSet;
                setSecondary.setExamSetVariant(doCreateExamSetVariant(programSet, examSetList.get(random.nextInt(examSetList.size()))));
                break;
            }
            case EnrMethodDivCompetitionsCodes.HIGHER_NO_DIV :
            {
                EnrProgramSetHigher setHigher = (EnrProgramSetHigher) programSet;
                setHigher.setExamSetVariant(doCreateExamSetVariant(programSet, examSetList.get(random.nextInt(examSetList.size()))));
                break;
            }
            case EnrMethodDivCompetitionsCodes.MASTER_NO_DIV :
            {
                EnrProgramSetMaster setMaster = (EnrProgramSetMaster) programSet;
                setMaster.setExamSetVariant(doCreateExamSetVariant(programSet, examSetList.get(random.nextInt(examSetList.size()))));
                break;
            }
        }

        update(programSet);
    }

    private EnrExamSetVariant doCreateExamSetVariant(EnrProgramSetBase programSet, EnrExamSet enrExamSet)
    {
        EnrExamSetVariant variant = new EnrExamSetVariant();
        if (programSet.getId() != null)
            variant.setOwner(programSet);
        variant.setExamSet(enrExamSet);
        save(variant);

        List<EnrExamSetElement> elementList = getList(EnrExamSetElement.class, EnrExamSetElement.examSet(), enrExamSet);
        final List<EnrExamPassForm> passForms = getList(EnrExamPassForm.class);

        EnrExamPassForm stateExam = get(EnrExamPassForm.class, EnrExamPassForm.code(), EnrExamPassFormCodes.STATE_EXAM);
        EnrExamPassForm olymp = get(EnrExamPassForm.class, EnrExamPassForm.code(), EnrExamPassFormCodes.OLYMPIAD);
        passForms.remove(stateExam);
        passForms.remove(olymp);

        int i = 1;
        for (EnrExamSetElement element : elementList) {
            final EnrExamVariant examVariant = new EnrExamVariant(variant, element, (long)random.nextInt(100000));
            examVariant.setPriority(i++);
            save(examVariant);

            Collections.shuffle(passForms);

            // генерируем для создаваемой настройки ВИ формы сдачи ВИ, рандомно от 1 до 3

            int size = Math.min(random.nextInt(3), passForms.size());

            List<EnrExamPassForm> selPassForms = new ArrayList<>();
            if (element.getType().getCode().equals(EnrExamTypeCodes.USUAL) && programSet instanceof EnrProgramSetBS) {
                selPassForms.add(stateExam);
                selPassForms.add(olymp);
                size = Math.min(random.nextInt(1), passForms.size());
            }

            for (int j = size; j >= 0 ; j--) {
                selPassForms.add(passForms.get(j));
            }

            for (EnrExamPassForm passForm : selPassForms) {
                final EnrExamVariantPassForm variantPassForm = new EnrExamVariantPassForm();
                variantPassForm.setExamVariant(examVariant);
                variantPassForm.setPassForm(passForm);
                save(variantPassForm);
            }
        }

        return variant;
    }


    private boolean dice(int dim, int max) {return random.nextInt(dim) < max;}

    private EnrMethodDivCompetitions getRandomMethodDivCompetitions(EnrProgramSetBase programSet)
    {
        if (programSet instanceof EnrProgramSetBS)
        {
            return getCatalogItem(EnrMethodDivCompetitions.class, EnrMethodDivCompetitionsCodes.BS_NO_DIV);
//            switch (random.nextInt(4))
//            {
//                case 0 : return getCatalogItem(EnrMethodDivCompetitions.class, EnrMethodDivCompetitionsCodes.BS_NO_DIV);
//                case 1 : return getCatalogItem(EnrMethodDivCompetitions.class, EnrMethodDivCompetitionsCodes.BS_SOO_AND_SPO_APART_VO);
//                case 2 : return getCatalogItem(EnrMethodDivCompetitions.class, EnrMethodDivCompetitionsCodes.BS_SOO_AND_VO_APART_SPO);
//                case 3 : return getCatalogItem(EnrMethodDivCompetitions.class, EnrMethodDivCompetitionsCodes.BS_SOO_PO);
//                case 4 : return getCatalogItem(EnrMethodDivCompetitions.class, EnrMethodDivCompetitionsCodes.BS_SOO_SPO_VO);
//
//            }
        }
        if (programSet instanceof EnrProgramSetMaster)
        {
            return getCatalogItem(EnrMethodDivCompetitions.class, EnrMethodDivCompetitionsCodes.MASTER_NO_DIV);
        }
        if (programSet instanceof EnrProgramSetHigher)
        {
            return getCatalogItem(EnrMethodDivCompetitions.class, EnrMethodDivCompetitionsCodes.POSTGRADUATE_NO_DIV);
        }
        if (programSet instanceof EnrProgramSetSecondary)
        {
            return getCatalogItem(EnrMethodDivCompetitions.class, EnrMethodDivCompetitionsCodes.SEC_NO_DIV);
        }

        return getCatalogItem(EnrMethodDivCompetitions.class, EnrMethodDivCompetitionsCodes.BS_SOO_SPO_VO);

    }

}

