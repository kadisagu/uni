/* $Id$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.IEntityHandler;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrRequestTypeCodes;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementType;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author nvankov
 * @since 6/2/14
 */
public class EnrEntrantWizardState implements IEntrantWizardState
{
    private final Map<String, EnrEntrantWizardStep> _steps = Maps.newHashMap();
    private final List<String> _stepsOrder = Lists.newArrayList();

    private Long _identityCardId;
    private Long _personId;
    private Long _enrollmentCampaignId;
    private boolean _entrantNew;
    private Long _entrantId;
    private Long _onlineEntrantId;
    private Long _eduDocumentId;
    private Long _entrantRequestId;
    private boolean _receiveEduLevelFirst = true;
    private Long _requestTypeId;
    private Set<Long> createdObjectIds = new HashSet<>();

    // Шаги
    private EnrEntrantWizardStep _identityCardStep;
    private EnrEntrantWizardStep _requestTypeAndEduDocStep;
    private EnrEntrantWizardStep _stateExamsStep;
    private EnrEntrantWizardStep _documentsStep;
    private EnrEntrantWizardStep _nextOfKinStep;
    private EnrEntrantWizardStep _addInfoStep;
    private EnrEntrantWizardStep _contactsStep;
    private EnrEntrantWizardStep _indAchievementsStep;
    private EnrEntrantWizardStep _competitionsStep;
    private EnrEntrantWizardStep _examsStep;
    private EnrEntrantWizardStep _prioritiesStep;
    private EnrEntrantWizardStep _attachedDocsStep;


    // утилиты всякие

    private IEntityHandler disableHandler = entity -> !getCreatedObjectIds().contains(entity.getId());

    public EnrEntrant getEntrant()
    {
        return getEntrantId() == null ? null : IUniBaseDao.instance.get().getNotNull(EnrEntrant.class, getEntrantId());
    }

    public void activateNextStep(String currentStepCode) {
        updateStepList();
        getSteps().get(getStepsOrder().get(getStepsOrder().indexOf(currentStepCode) + 1)).setDisabled(false);
        if (getEntrantId() != null) {
            getIdentityCardStep().setDisabled(true);
            getRequestTypeAndEduDocStep().setDisabled(false);
        }
        if (getEntrantRequestId() != null) {
            getRequestTypeAndEduDocStep().setDisabled(true);
        }
    }

    public void updateStepList() {
        if (getRequestTypeId() != null) {
            if (isStateExamStepNeeded() && !getStepsOrder().contains(EnrEntrantRequestAddWizardWizardUI.STATE_EXAMS_STEP_CODE)) {
                getStepsOrder().add(getStepsOrder().indexOf(EnrEntrantRequestAddWizardWizardUI.DOCUMENTS_STEP_CODE), EnrEntrantRequestAddWizardWizardUI.STATE_EXAMS_STEP_CODE);
            }
            if (!isStateExamStepNeeded() && getStepsOrder().contains(EnrEntrantRequestAddWizardWizardUI.STATE_EXAMS_STEP_CODE)) {
                getStepsOrder().remove(getStepsOrder().indexOf(EnrEntrantRequestAddWizardWizardUI.STATE_EXAMS_STEP_CODE));
            }

            boolean indAchievementsAvailable = ISharedBaseDao.instance.get().existsEntity(EnrEntrantAchievementType.class, EnrEntrantAchievementType.enrollmentCampaign().id().s(), getEnrollmentCampaignId(), EnrEntrantAchievementType.achievementKind().requestType().id().s(), getRequestTypeId());
            if (getStepsOrder().contains(EnrEntrantRequestAddWizardWizardUI.IND_ACHIEVEMENTS_STEP_CODE) && !indAchievementsAvailable)
                getStepsOrder().remove(getStepsOrder().indexOf(EnrEntrantRequestAddWizardWizardUI.IND_ACHIEVEMENTS_STEP_CODE));

            if (!getStepsOrder().contains(EnrEntrantRequestAddWizardWizardUI.IND_ACHIEVEMENTS_STEP_CODE) && indAchievementsAvailable)
                getStepsOrder().add(getStepsOrder().indexOf(EnrEntrantRequestAddWizardWizardUI.COMPETITIONS_STEP_CODE), EnrEntrantRequestAddWizardWizardUI.IND_ACHIEVEMENTS_STEP_CODE);
        }
        if (getEntrantRequestId() != null) {
            boolean hasExams = false;
            for (EnrRequestedCompetition competition : IUniBaseDao.instance.get().getList(EnrRequestedCompetition.class, EnrRequestedCompetition.request().id(), getEntrantRequestId())) {
                hasExams = hasExams || (!competition.getCompetition().isNoExams() && !competition.getCompetition().getExamSetVariant().getExamSet().isEmptySet());
            }
            if (hasExams && !getStepsOrder().contains(EnrEntrantRequestAddWizardWizardUI.EXAMS_STEP_CODE)) {
                    getStepsOrder().add(getStepsOrder().indexOf(EnrEntrantRequestAddWizardWizardUI.COMPETITIONS_STEP_CODE), EnrEntrantRequestAddWizardWizardUI.EXAMS_STEP_CODE);
            }
            if (!hasExams && getStepsOrder().contains(EnrEntrantRequestAddWizardWizardUI.EXAMS_STEP_CODE)) {
                getStepsOrder().remove(getStepsOrder().indexOf(EnrEntrantRequestAddWizardWizardUI.EXAMS_STEP_CODE));
            }
        }
    }

    public boolean isStateExamStepNeeded() {
        return getRequestTypeId() != null && EnrRequestTypeCodes.BS.equals(IUniBaseDao.instance.get().get(EnrRequestType.class, getRequestTypeId()).getCode());
    }

    // getters and setters

    public Map<String, EnrEntrantWizardStep> getSteps()
    {
        return _steps;
    }

    public List<String> getStepsOrder()
    {
        return _stepsOrder;
    }

    public Long getIdentityCardId()
    {
        return _identityCardId;
    }

    public void setIdentityCardId(Long identityCardId)
    {
        _identityCardId = identityCardId;
    }

    public boolean isEntrantNew()
    {
        return _entrantNew;
    }

    public void setEntrantNew(boolean entrantNew)
    {
        _entrantNew = entrantNew;
    }

    public Long getEntrantId()
    {
        return _entrantId;
    }

    public void setEntrantId(Long entrantId)
    {
        _entrantId = entrantId;
    }

    public Long getOnlineEntrantId()
    {
        return _onlineEntrantId;
    }

    public void setOnlineEntrantId(Long onlineEntrantId)
    {
        _onlineEntrantId = onlineEntrantId;
    }

    public EnrEntrantWizardStep getIdentityCardStep()
    {
        return _identityCardStep;
    }

    public void setIdentityCardStep(EnrEntrantWizardStep identityCardStep)
    {
        _identityCardStep = identityCardStep;
    }

    public EnrEntrantWizardStep getRequestTypeAndEduDocStep()
    {
        return _requestTypeAndEduDocStep;
    }

    public void setRequestTypeAndEduDocStep(EnrEntrantWizardStep requestTypeAndEduDocStep)
    {
        _requestTypeAndEduDocStep = requestTypeAndEduDocStep;
    }

    public EnrEntrantWizardStep getStateExamsStep()
    {
        return _stateExamsStep;
    }

    public void setStateExamsStep(EnrEntrantWizardStep stateExamsStep)
    {
        _stateExamsStep = stateExamsStep;
    }

    public EnrEntrantWizardStep getDocumentsStep()
    {
        return _documentsStep;
    }

    public void setDocumentsStep(EnrEntrantWizardStep documentsStep)
    {
        _documentsStep = documentsStep;
    }

    public EnrEntrantWizardStep getNextOfKinStep()
    {
        return _nextOfKinStep;
    }

    public void setNextOfKinStep(EnrEntrantWizardStep nextOfKinStep)
    {
        _nextOfKinStep = nextOfKinStep;
    }

    public EnrEntrantWizardStep getAddInfoStep()
    {
        return _addInfoStep;
    }

    public void setAddInfoStep(EnrEntrantWizardStep addInfoStep)
    {
        _addInfoStep = addInfoStep;
    }

    public EnrEntrantWizardStep getContactsStep()
    {
        return _contactsStep;
    }

    public void setContactsStep(EnrEntrantWizardStep contactsStep)
    {
        _contactsStep = contactsStep;
    }

    public EnrEntrantWizardStep getIndAchievementsStep()
    {
        return _indAchievementsStep;
    }

    public void setIndAchievementsStep(EnrEntrantWizardStep indAchievementsStep)
    {
        _indAchievementsStep = indAchievementsStep;
    }

    public EnrEntrantWizardStep getCompetitionsStep()
    {
        return _competitionsStep;
    }

    public void setCompetitionsStep(EnrEntrantWizardStep competitionsStep)
    {
        _competitionsStep = competitionsStep;
    }

    public EnrEntrantWizardStep getExamsStep()
    {
        return _examsStep;
    }

    public void setExamsStep(EnrEntrantWizardStep examsStep)
    {
        _examsStep = examsStep;
    }

    public EnrEntrantWizardStep getPrioritiesStep()
    {
        return _prioritiesStep;
    }

    public void setPrioritiesStep(EnrEntrantWizardStep prioritiesStep)
    {
        _prioritiesStep = prioritiesStep;
    }

    public EnrEntrantWizardStep getAttachedDocsStep()
    {
        return _attachedDocsStep;
    }

    public void setAttachedDocsStep(EnrEntrantWizardStep attachedDocsStep)
    {
        _attachedDocsStep = attachedDocsStep;
    }


    public Long getEnrollmentCampaignId()
    {
        return _enrollmentCampaignId;
    }

    public void setEnrollmentCampaignId(Long enrollmentCampaignId)
    {
        _enrollmentCampaignId = enrollmentCampaignId;
    }

    public Long getPersonId()
    {
        return _personId;
    }

    public void setPersonId(Long personId)
    {
        _personId = personId;
    }

    public void setEduDocumentId(Long eduDocumentId)
    {
        _eduDocumentId = eduDocumentId;
    }

    public Long getEduDocumentId()
    {
        return _eduDocumentId;
    }

    public void setRequestTypeId(Long requestTypeId)
    {
        _requestTypeId = requestTypeId;
    }

    public Long getRequestTypeId()
    {
        return _requestTypeId;
    }

    public Set<Long> getCreatedObjectIds()
    {
        return createdObjectIds;
    }

    public IEntityHandler getDisableHandler()
    {
        return disableHandler;
    }

    public Long getEntrantRequestId()
    {
        return _entrantRequestId;
    }

    public void setEntrantRequestId(Long entrantRequestId)
    {
        _entrantRequestId = entrantRequestId;
    }

    public boolean isReceiveEduLevelFirst()
    {
        return _receiveEduLevelFirst;
    }

    public void setReceiveEduLevelFirst(boolean receiveEduLevelFirst)
    {
        _receiveEduLevelFirst = receiveEduLevelFirst;
    }
}