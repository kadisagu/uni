/* $Id$ */
package ru.tandemservice.unienr14.settings.bo.EnrIndividualAchievement.ui.AddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantAchievementKindCodes;
import ru.tandemservice.unienr14.entrant.daemon.EnrEntrantDaemonBean;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementType;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
/**
 * @author nvankov
 * @since 4/11/14
 */
@Input({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "entityHolder.id"),
        @Bind(key = "requestType", binding = "requestTypeId")
})
public class EnrIndividualAchievementAddEditUI extends UIPresenter
{
    public static final String REQUEST_TYPE_PROPERTY = "requestTypeProperty";

    private EntityHolder<EnrEntrantAchievementType> _entityHolder = new EntityHolder<>(new EnrEntrantAchievementType());
    private Long _requestTypeId;
    private EnrRequestType _requestType;

    @Override
    public void onComponentRefresh()
    {
        getEntityHolder().refresh();
        if (isAddForm())
        {
            setRequestType(DataAccessServices.dao().<EnrRequestType>getNotNull(getRequestTypeId()));
            getEntrantAchievementType().setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
        }
        else
        {
            setRequestType(getEntrantAchievementType().getAchievementKind().getRequestType());
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(REQUEST_TYPE_PROPERTY, getRequestType().getId());
    }

    // Listeners
    public void onClickApply()
    {
        validate();
        if(getUserContext().getErrorCollector().hasErrors())
        {
            return;
        }
        DataAccessServices.dao().saveOrUpdate(getEntrantAchievementType());
        deactivate();
        EnrEntrantDaemonBean.DAEMON.wakeUpDaemon();
    }

    private void validate()
    {
        if(!isAddForm() && getEntrantAchievementType().isForRequest())
        {
            int count = DataAccessServices.dao().getCount(new DQLSelectBuilder().fromEntity(EnrEntrantAchievement.class, "ea")
                    .where(eq(property("ea", EnrEntrantAchievement.type()), value(getEntrantAchievementType())))
                    .where(isNull(property("ea", EnrEntrantAchievement.request())))
            );
            if(count > 0)
                _uiSupport.error("Нельзя включить учет инд. достижения в рамках заявления, т.к. абитуриентам уже добавлены такие инд. достижения и для них не указано заявление для учета.");
        }
    }

    // Getters && Setters
    public EntityHolder<EnrEntrantAchievementType> getEntityHolder(){ return _entityHolder; }
    public EnrEntrantAchievementType getEntrantAchievementType(){ return getEntityHolder().getValue(); }

    public boolean isAddForm(){ return getEntityHolder().getId() == null; }

    public boolean isEditForm(){ return !isAddForm(); }

    public String getSticker(){ return isAddForm() ? getConfig().getProperty("ui.sticker.add") : getConfig().getProperty("ui.sticker.edit"); }

    public Long getRequestTypeId(){ return _requestTypeId; }
    public void setRequestTypeId(Long requestTypeId){ _requestTypeId = requestTypeId; }

    public EnrRequestType getRequestType()
    {
        return _requestType;
    }

    public void setRequestType(EnrRequestType requestType)
    {
        _requestType = requestType;
    }
}
