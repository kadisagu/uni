/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.InWizardDataTabContacts;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

@Configuration
public class EnrEntrantInWizardDataTabContacts extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
        .create();
    }
}
