package ru.tandemservice.unienr14.catalog.entity;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;

import ru.tandemservice.unienr14.catalog.entity.gen.EnrBenefitCategoryGen;

/**
 * Категория лиц, обладающих особыми правами при поступлении
 */
public class EnrBenefitCategory extends EnrBenefitCategoryGen implements IDynamicCatalogItem
{
    @Override
    @EntityDSLSupport(parts = {EnrBenefitCategory.P_SHORT_TITLE, EnrBenefitCategory.L_BENEFIT_TYPE + "." + EnrBenefitType.P_SHORT_TITLE})
    public String getTitleWithType() {
        return getShortTitle() + " (" + getBenefitType().getShortTitle() + ")";
    }
}