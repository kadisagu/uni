/* $Id:$ */
package ru.tandemservice.unienr14.order.bo.EnrEnrollmentParagraph.logic;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Transformer;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.util.cache.SafeMap;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantCustomState;
import ru.tandemservice.unienr14.order.bo.EnrEnrollmentParagraph.EnrEnrollmentParagraphManager;
import ru.tandemservice.unienr14.order.entity.EnrAbstractExtract;
import ru.tandemservice.unienr14.order.entity.EnrCancelExtract;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentParagraph;

import java.util.*;

/**
 * @author oleyba
 * @since 7/8/14
 */
public class EnrEnrollmentExtractDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String ENROLLMENT_PARAGRAPH = "enrollmentParagraphView";
    public static final String NO_EXCLUDE = "noExcludeView";
    public static final String CANCEL_EXTRACT = "cancelExtract";
    public static final String OTHER_EXTRACTS = "otherExtracts";
    public static final String CUSTOM_STATES = "customStates";

    public EnrEnrollmentExtractDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    @SuppressWarnings("unchecked")
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        EnrEnrollmentParagraph paragraph = context.get(ENROLLMENT_PARAGRAPH);

        List<EnrEnrollmentExtract> extractList = (List<EnrEnrollmentExtract>) paragraph.getExtractList();

        if (extractList.isEmpty()) return new DSOutput(input);

        boolean noExclude = paragraph.getOrder().isReadonly() || extractList.size() == 1;

        List<DataWrapper> recordList = new ArrayList<>();

        List<EnrCancelExtract> cancelExtracts = IUniBaseDao.instance.get().getList(EnrCancelExtract.class, EnrCancelExtract.entity().paragraph(), paragraph);
        Map<EnrEnrollmentExtract, EnrCancelExtract> cancelExtractMap = new HashMap<>();
        for (EnrCancelExtract cancelExtract : cancelExtracts) {
            if (cancelExtract.getEntity() instanceof  EnrEnrollmentExtract) {
                cancelExtractMap.put((EnrEnrollmentExtract) cancelExtract.getEntity(), cancelExtract);
            }
        }

        Transformer<EnrAbstractExtract, EnrEntrant> extractEntrantTransformer = EnrAbstractExtract::getEntrant;

        Collection<EnrAbstractExtract> otherExtracts = EnrEnrollmentParagraphManager.instance().dao().getOtherExtracts(CollectionUtils.collect(extractList, extractEntrantTransformer), paragraph);
        Map<Long, List<DataWrapper>> otherExtractMap = SafeMap.get(ArrayList.class);
        for (EnrAbstractExtract extract: otherExtracts)
        {
            otherExtractMap.get(extract.getEntrant().getId()).add(new DataWrapper(extract.getParagraph().getOrder().getId(), extract.getShortTitle()));
        }

        Collection<EnrEntrant> entrants = CollectionUtils.collect(extractList, extractEntrantTransformer);
        Map<EnrEntrant, List<EnrEntrantCustomState>> customStatesMap = EnrEntrantManager.instance().dao().getActiveCustomStatesMap(entrants, new Date());

        for (EnrEnrollmentExtract extract : extractList)
        {
            DataWrapper record = new DataWrapper(extract.getId(), extract.getTitle(), extract);
            record.setProperty(NO_EXCLUDE, noExclude);
            record.setProperty(CANCEL_EXTRACT, cancelExtractMap.get(extract));
            record.setProperty(OTHER_EXTRACTS, otherExtractMap.get(extract.getEntrant().getId()));
            record.setProperty(CUSTOM_STATES, customStatesMap.get(extract.getEntrant()));
            recordList.add(record);
        }

        return ListOutputBuilder.get(input, recordList).build();
    }
}

