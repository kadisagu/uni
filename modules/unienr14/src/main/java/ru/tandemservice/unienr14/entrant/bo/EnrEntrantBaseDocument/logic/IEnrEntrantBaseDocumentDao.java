/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrantBaseDocument.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.person.base.bo.PersonDocument.logic.IPersonDocumentContextDao;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;

/**
 * @author nvankov
 * @since 27.03.2017
 */
public interface IEnrEntrantBaseDocumentDao extends INeedPersistenceSupport, IPersonDocumentContextDao
{
}
