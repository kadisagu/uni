package ru.tandemservice.unienr14.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Вид индивидуального достижения"
 * Имя сущности : enrEntrantAchievementKind
 * Файл data.xml : unienr.catalog.data.xml
 */
public interface EnrEntrantAchievementKindCodes
{
    /** Константа кода (code) элемента : Итоговое сочинение (title) */
    String BS_FINAL_COMPOSITION = "bs_final_composition";

    Set<String> CODES = ImmutableSet.of(BS_FINAL_COMPOSITION);
}
