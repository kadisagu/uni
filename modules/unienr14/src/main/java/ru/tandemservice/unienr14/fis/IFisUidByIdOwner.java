/* $Id:$ */
package ru.tandemservice.unienr14.fis;

import org.tandemframework.core.entity.IEntity;

/**
 * Используется для выдачи UID'ов ФИC на базе id объектов, и последующего поиска их по выданным UID'ам.
 * У объекта должна быть карточка (можно в форме редиректа на другую, см. EnrEntrantAchievementPub)
 * @author oleyba
 * @since 7/16/14
 */
public interface IFisUidByIdOwner extends IEntity
{
    String getFisUidTitle();
}
