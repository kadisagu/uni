/* $Id:$ */
package ru.tandemservice.unienr14.rating.bo.EnrChosenEntranceExam;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

/**
 * @author oleyba
 * @since 7/16/14
 */
@Configuration
public class EnrChosenEntranceExamManager extends BusinessObjectManager
{
    public static EnrChosenEntranceExamManager instance()
    {
        return instance(EnrChosenEntranceExamManager.class);
    }

}
