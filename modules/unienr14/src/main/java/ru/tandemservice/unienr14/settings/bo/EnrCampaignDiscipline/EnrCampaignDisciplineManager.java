/**
 *$Id: EnrCampaignDisciplineManager.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.settings.bo.EnrCampaignDiscipline;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.core.view.formatter.LongAsDoubleFormatter;
import ru.tandemservice.unienr14.settings.bo.EnrCampaignDiscipline.logic.EnrCampDisciplineDao;
import ru.tandemservice.unienr14.settings.bo.EnrCampaignDiscipline.logic.EnrCampDisciplineGroupDao;
import ru.tandemservice.unienr14.settings.bo.EnrCampaignDiscipline.logic.IEnrCampDisciplineDao;
import ru.tandemservice.unienr14.settings.bo.EnrCampaignDiscipline.logic.IEnrCampDisciplineGroupDao;

/**
 * @author Alexander Shaburov
 * @since 09.04.13
 */
@Configuration
public class EnrCampaignDisciplineManager extends BusinessObjectManager
{
    public static final IFormatter<Long> DEFAULT_MARK_FORMATTER = LongAsDoubleFormatter.LONG_AS_DOUBLE_FORMATTER_3_SCALE;

    public static EnrCampaignDisciplineManager instance()
    {
        return instance(EnrCampaignDisciplineManager.class);
    }

    @Bean
    public IEnrCampDisciplineGroupDao groupDao()
    {
        return new EnrCampDisciplineGroupDao();
    }

    @Bean
    public IEnrCampDisciplineDao dao()
    {
        return new EnrCampDisciplineDao();
    }
}
