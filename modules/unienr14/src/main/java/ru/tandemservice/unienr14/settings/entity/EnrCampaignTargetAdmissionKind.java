package ru.tandemservice.unienr14.settings.entity;

import org.tandemframework.core.common.ITitled;
import ru.tandemservice.unienr14.catalog.entity.EnrTargetAdmissionKind;
import ru.tandemservice.unienr14.settings.entity.gen.*;

/**
 * Вид ЦП, используемый в рамках ПК
 */
public class EnrCampaignTargetAdmissionKind extends EnrCampaignTargetAdmissionKindGen implements ITitled
{
    public EnrCampaignTargetAdmissionKind()
    {

    }

    public EnrCampaignTargetAdmissionKind(EnrEnrollmentCampaign enrollmentCampaign, EnrTargetAdmissionKind targetAdmissionKind)
    {
        this.setEnrollmentCampaign(enrollmentCampaign);
        this.setTargetAdmissionKind(targetAdmissionKind);
    }

    @Override
    public String getTitle()
    {
        if (getTargetAdmissionKind() == null) {
            return this.getClass().getSimpleName();
        }
        return getTargetAdmissionKind().getTitle();
    }
}