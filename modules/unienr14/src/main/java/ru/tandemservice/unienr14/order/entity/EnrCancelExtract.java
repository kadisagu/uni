package ru.tandemservice.unienr14.order.entity;

import org.tandemframework.shared.commonbase.utils.CommonCollator;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.order.entity.gen.EnrCancelExtractGen;
import ru.tandemservice.unimove.UnimoveDefines;

/**
 * Выписка приказа об отмене приказа по абитуриентам (2014)
 */
public class EnrCancelExtract extends EnrCancelExtractGen
{
    @Override
    public int compareTo(Object o)
    {
        if (o instanceof EnrCancelExtract) {
            EnrCancelExtract e = (EnrCancelExtract) o;
            return CommonCollator.RUSSIAN_COLLATOR.compare(getRequestedCompetition().getRequest().getEntrant().getPerson().getFullFio(), e.getRequestedCompetition().getRequest().getEntrant().getPerson().getFullFio());
        }
        return 0;
    }

    @Override
    public boolean isCommitted()
    {
        return UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED.equals(getState().getCode());
    }

    @Override
    public void setEntity(Object entity)
    {
        setEntity((EnrAbstractExtract) entity);
    }

    @Override
    public void setCommitted(boolean committed)
    {
        // не нужно, состояние обновят без в основном методе
    }

    @Override
    public EnrCompetition getCompetition()
    {
        return getRequestedCompetition().getCompetition();
    }

    @Override
    public EnrEntrant getEntrant()
    {
        return getRequestedCompetition().getRequest().getEntrant();
    }
}