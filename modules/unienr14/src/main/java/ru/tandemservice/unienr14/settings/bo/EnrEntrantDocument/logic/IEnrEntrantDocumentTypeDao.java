/**
 *$Id: IEnrEntrantDocumentTypeDao.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.settings.bo.EnrEntrantDocument.logic;

import javax.validation.constraints.NotNull;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;

/**
 * @author Alexander Shaburov
 * @since 30.04.13
 */
public interface IEnrEntrantDocumentTypeDao extends INeedPersistenceSupport
{
    /**
     * Сохраняет настройку по умолчанию для системных Типов документов абитуриента.
     * Если для ПК и Типа уже есть настройка, то она не меняется.
     * @param enrollmentCampaignId id ПК, для которой сохраняем
     */
    void doSetEnrCampDefaultSettings(Long enrollmentCampaignId);

    /**
     * Настраивается использование Типа документа абитуриента в рамках ПК.
     * @param documentTypeId enrEntrantDocumentType.id Тип документа абитуриента, который настраивается
     * @param enrollmentCampaignId enrEnrollmentCampaign.id ПК, в рамках которой настраивается
     * @param used Используется в рамках ПК (true - объект при необходимости enrCampaignEntrantDocument будет создан, false - при наличии объекта, он будет удален)
     * @return enrCampaignEntrantDocument.id (если used = true), null (если used = false)
     */
    Long doToggleUsed(@NotNull Long documentTypeId, @NotNull Long enrollmentCampaignId, boolean used);

    /**
     * Настраивается использование Типа документа абитуриента в рамках ПК.
     * При вызове этого метода будет вызван doToggleUsed(used = true).
     * @param documentTypeId enrEntrantDocumentType.id Тип документа абитуриента, который настраивается
     * @param enrollmentCampaignId enrEnrollmentCampaign.id ПК, в рамках которой настраивается
     * @param propertyName название свойства enrCampaignEntrantDocument, которое нужно обновить
     * @param propertyValue новое значение свойства
     * @return enrCampaignEntrantDocument.id
     */
    Long doToggleProperty(@NotNull Long documentTypeId, @NotNull Long enrollmentCampaignId, String propertyName, boolean propertyValue);
}
