/* $Id: EcProfileDistributionEntrantAdd.java 25114 2012-12-03 11:25:11Z vdanilov $ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.unienr14.programAlloc.bo.EnrProgramAllocation.ui.EntrantAdd;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;

/**
 * @author Vasily Zhukov
 * @since 10.05.2012
 */
@Configuration
public class EnrProgramAllocationEntrantAdd extends BusinessComponentManager
{
//    public static final String PRE_STUDENT_DS = "preStudentDS";
//
//    @Override
//    @Bean
//    public PresenterExtPoint presenterExtPoint()
//    {
//        return presenterExtPointBuilder()
//                .addDataSource(searchListDS(PRE_STUDENT_DS, entrantDS(), entrantDSHandler()))
//                .create();
//    }
//
//    @Bean
//    public ColumnListExtPoint entrantDS()
//    {
//        return columnListExtPointBuilder(PRE_STUDENT_DS)
//                .addColumn(textColumn("competitionKind", IEcgEntrantRateRowDTO.COMPETITION_KIND_TITLE).create())
//                .addColumn(textColumn("targetAdmissionKind", IEcgEntrantRateRowDTO.TARGET_ADMISSION_KIND_TITLE).visible("ui:targetAdmission").create())
//                .addColumn(textColumn("finalMark", IEcgEntrantRateRowDTO.FINAL_MARK).formatter(DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS).create())
//                .addColumn(textColumn("profileMark", IEcgEntrantRateRowDTO.PROFILE_MARK).formatter(DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS).create())
//                .addColumn(textColumn("graduatedProfileEduInstitution", IEcgEntrantRateRowDTO.GRADUATED_PROFILE_EDU_INSTITUTION_TITLE).width("1").create())
//                .addColumn(textColumn("certificateAverageMark", IEcgEntrantRateRowDTO.CERTIFICATE_AVERAGE_MARK).width("5").formatter(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS).create())
//                .addColumn(textColumn("fio", IEcgEntrantRateRowDTO.FIO))
//                .addColumn(checkboxColumn("checkbox").listener("onRateRefresh").controlInHeader(false).create())
//                .addColumn(blockColumn("distributionGroup", "distributionGroupBlock").width("5").create())
//                .addColumn(textColumn("priorities", IEcgEntrantRateRowDTO.PRIORITIES).create())
//                .addColumn(textColumn("originals", IEcgEntrantRateRowDTO.ORIGINAL_DOCUMENT_HANDED_IN_TITLE).create())
//                .create();
//    }
//
//    @Bean
//    public IBusinessHandler<DSInput, DSOutput> entrantDSHandler()
//    {
//        return new EcSimpleDSHandler(getName());
//    }
}
