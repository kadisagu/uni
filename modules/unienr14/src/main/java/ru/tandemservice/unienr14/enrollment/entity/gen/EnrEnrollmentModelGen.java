package ru.tandemservice.unienr14.enrollment.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentModel;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Модель зачисления
 *
 * Моделирование зачисления в ПК.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrEnrollmentModelGen extends VersionedEntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentModel";
    public static final String ENTITY_NAME = "enrEnrollmentModel";
    public static final int VERSION_HASH = -1309836723;
    private static IEntityMeta ENTITY_META;

    public static final String P_VERSION = "version";
    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String L_REQUEST_TYPE = "requestType";
    public static final String P_TITLE = "title";

    private int _version; 
    private EnrEnrollmentCampaign _enrollmentCampaign;     // ПК
    private EnrRequestType _requestType;     // Вид заявления
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public int getVersion()
    {
        return _version;
    }

    /**
     * @param version  Свойство не может быть null.
     */
    public void setVersion(int version)
    {
        dirty(_version, version);
        _version = version;
    }

    /**
     * @return ПК. Свойство не может быть null.
     */
    @NotNull
    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign ПК. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Вид заявления. Свойство не может быть null.
     */
    @NotNull
    public EnrRequestType getRequestType()
    {
        return _requestType;
    }

    /**
     * @param requestType Вид заявления. Свойство не может быть null.
     */
    public void setRequestType(EnrRequestType requestType)
    {
        dirty(_requestType, requestType);
        _requestType = requestType;
    }

    /**
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrEnrollmentModelGen)
        {
            setVersion(((EnrEnrollmentModel)another).getVersion());
            setEnrollmentCampaign(((EnrEnrollmentModel)another).getEnrollmentCampaign());
            setRequestType(((EnrEnrollmentModel)another).getRequestType());
            setTitle(((EnrEnrollmentModel)another).getTitle());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrEnrollmentModelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrEnrollmentModel.class;
        }

        public T newInstance()
        {
            return (T) new EnrEnrollmentModel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "version":
                    return obj.getVersion();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "requestType":
                    return obj.getRequestType();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "version":
                    obj.setVersion((Integer) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrEnrollmentCampaign) value);
                    return;
                case "requestType":
                    obj.setRequestType((EnrRequestType) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "version":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "requestType":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "version":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "requestType":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "version":
                    return Integer.class;
                case "enrollmentCampaign":
                    return EnrEnrollmentCampaign.class;
                case "requestType":
                    return EnrRequestType.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrEnrollmentModel> _dslPath = new Path<EnrEnrollmentModel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrEnrollmentModel");
    }
            

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentModel#getVersion()
     */
    public static PropertyPath<Integer> version()
    {
        return _dslPath.version();
    }

    /**
     * @return ПК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentModel#getEnrollmentCampaign()
     */
    public static EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Вид заявления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentModel#getRequestType()
     */
    public static EnrRequestType.Path<EnrRequestType> requestType()
    {
        return _dslPath.requestType();
    }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentModel#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EnrEnrollmentModel> extends EntityPath<E>
    {
        private PropertyPath<Integer> _version;
        private EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> _enrollmentCampaign;
        private EnrRequestType.Path<EnrRequestType> _requestType;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentModel#getVersion()
     */
        public PropertyPath<Integer> version()
        {
            if(_version == null )
                _version = new PropertyPath<Integer>(EnrEnrollmentModelGen.P_VERSION, this);
            return _version;
        }

    /**
     * @return ПК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentModel#getEnrollmentCampaign()
     */
        public EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Вид заявления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentModel#getRequestType()
     */
        public EnrRequestType.Path<EnrRequestType> requestType()
        {
            if(_requestType == null )
                _requestType = new EnrRequestType.Path<EnrRequestType>(L_REQUEST_TYPE, this);
            return _requestType;
        }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentModel#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EnrEnrollmentModelGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EnrEnrollmentModel.class;
        }

        public String getEntityName()
        {
            return "enrEnrollmentModel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
