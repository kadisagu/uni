package ru.tandemservice.unienr14.request.entity;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.common.IEntityDebugTitled;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.entity.dsl.MetaDSLPath;
import org.tandemframework.core.tool.IdGen;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEduLevelRequirementCodes;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.fis.IFisUidByIdOwner;
import ru.tandemservice.unienr14.request.entity.gen.EnrRequestedCompetitionGen;

/**
 * Выбранный конкурс (ВПО)
 */
public class EnrRequestedCompetition extends EnrRequestedCompetitionGen implements ITitled, IEnrEntrantPrioritized, IEntityDebugTitled, IFisUidByIdOwner
{
    public EnrRequestedCompetition()
    {
    }

    public EnrRequestedCompetition(EnrEntrantRequest request, EnrCompetition competition)
    {
        setRequest(request);
        setCompetition(competition);
    }

    @Override
    public String getEntityDebugTitle() {
        return getRequest().getEntrant().getFio() + ", заявление № " + getRequest().getStringNumber() + ": " + getTitle();
    }

    @Override
    @EntityDSLSupport
    public String getTitle()
    {
        if (getCompetition() == null) {
            return this.getClass().getSimpleName();
        }
        // [название набора ОП] ([форма обучения], в.п.: [вид приема], [СОО/ПО][, ПАРАЛЛ.] | [филиал], [ФП])
        StringBuilder title = new StringBuilder();
        title.append(getCompetition().getProgramSetOrgUnit().getProgramSet().getTitle());
        title.append(" (").append(StringUtils.uncapitalize(getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramForm().getTitle()));
        title.append(", в.п.: ").append(getCompetition().getType().getShortTitle());
        if (!EnrEduLevelRequirementCodes.NO.equals(getCompetition().getEduLevelRequirement().getCode())) {
            title.append(", ").append(getCompetition().getEduLevelRequirement().getShortTitle());
        }
        if (isParallel()) {
            title.append(", ПАРАЛЛ.");
        }
        OrgUnit orgUnit = getCompetition().getProgramSetOrgUnit().getOrgUnit().getInstitutionOrgUnit().getOrgUnit();
        OrgUnit formingOrgUnit = getCompetition().getProgramSetOrgUnit().getEffectiveFormativeOrgUnit();
        if (!(orgUnit.isTop() && formingOrgUnit.isTop())) {
            title.append(" | ")
                .append(CommonBaseStringUtil.joinWithSeparator(", ",
                    orgUnit.isTop() ? null : orgUnit.getShortTitle(),
                    orgUnit.equals(formingOrgUnit) ? null : formingOrgUnit.getShortTitle()));
        }
        title.append(")");
        return title.toString();
    }

    @Override
    public String getFisUidTitle()
    {
        return "Выбранный конкурс: " + getRequest().getEntrant().getFio() + ", заявление № " + getRequest().getStringNumber() + ", " + getTitle();
    }

    /** @return по абитуриенту и конкурсу формирует значение ключа */
    public static String calculateUniqueKey(long entrantId, long competitionId) {
        return (Long.toString(entrantId >>> IdGen.CODE_BITS, 32)+"."+Long.toString(competitionId >>> IdGen.CODE_BITS, 32));
    }

    public void refreshUniqueKey()
    {
        EnrEntrantRequest request = getRequest();
        if (null == request.getTakeAwayDocumentDate()) {
            // заявление действующее, генерируем уникальный ключ
            this.setUniqueKey(calculateUniqueKey(request.getEntrant().getId(), getCompetition().getId()));
        } else {
            // заявление отозванно, проверки на уникальность не требуется
            this.setUniqueKey(null);
        }
    }

    @Override
    @EntityDSLSupport
    public String getParametersTitle() {
        if (this.getCompetition().isContract())
        {
            StringBuilder contractStr = new StringBuilder();
            if (getContractCommonNumber() != null || getContractCommonDate() != null)
            {
                contractStr.append("договор");
                if (getContractCommonNumber() != null)
                    contractStr.append(" №").append(getContractCommonNumber());

                if (getContractCommonDate() != null)
                    contractStr.append(" от ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getContractCommonDate()));
            }

            return (getExternalOrgUnit() == null ? "" : getExternalOrgUnit().getTitleWithLegalForm()) + (contractStr.length() > 0 ? ", " : "") + contractStr.toString();
        }
        return "";
    }
    
    // ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubProfileTab.IEnrEntrantPrioritized

    @Override public String getPriorityRowTitle() { return getTitle(); }
    @Override public MetaDSLPath getPriorityOwnerPath() { return request().entrant(); }
    @Override public MetaDSLPath getRequestTypePath() { return request().type(); }
    @Override public int getCompetitionPriority() { return getPriority(); }
    @Override public Integer getProgramPriority() { return null; }

    public static EntityComboDataSourceHandler defaultSelectDSHandler(String name) {
        return new EntityComboDataSourceHandler(name, EnrRequestedCompetition.class)
                .filter(EnrRequestedCompetition.competition().programSetOrgUnit().programSet().title())
                .filter(EnrRequestedCompetition.competition().programSetOrgUnit().educationOrgUnit().developForm().shortTitle())
                .filter(EnrRequestedCompetition.competition().type().shortTitle())
                .order(EnrRequestedCompetition.competition().programSetOrgUnit().programSet().title())
                .order(EnrRequestedCompetition.competition().programSetOrgUnit().educationOrgUnit().developForm().shortTitle())
                .order(EnrRequestedCompetition.competition().type().shortTitle())
                .order(EnrRequestedCompetition.parallel())
                .order(EnrRequestedCompetition.competition().programSetOrgUnit().orgUnit().institutionOrgUnit().orgUnit().title())
                .order(EnrRequestedCompetition.competition().programSetOrgUnit().educationOrgUnit().formativeOrgUnit().title());
    }
}