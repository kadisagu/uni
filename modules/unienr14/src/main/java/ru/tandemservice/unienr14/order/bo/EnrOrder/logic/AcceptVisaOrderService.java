/* $Id:$ */
package ru.tandemservice.unienr14.order.bo.EnrOrder.logic;

import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.order.bo.EnrOrder.EnrOrderManager;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.OrderStates;

/**
 * @author oleyba
 * @since 7/10/14
 */
public class AcceptVisaOrderService extends ru.tandemservice.unimove.service.AcceptVisaOrderService
{
    @Override
    protected void doExecute() throws Exception
    {
        EnrOrderManager.instance().dao().doExecuteAcceptVisaOrderService(getOrder());
    }
}
