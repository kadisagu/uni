/**
 *$Id: EnrExamGroupSetListUI.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.exams.bo.EnrExamGroup.ui.SetList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unienr14.exams.bo.EnrExamGroup.EnrExamGroupManager;
import ru.tandemservice.unienr14.exams.bo.EnrExamGroup.ui.SetAddEdit.EnrExamGroupSetAddEdit;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

/**
 * @author Alexander Shaburov
 * @since 17.05.13
 */
public class EnrExamGroupSetListUI extends UIPresenter
{
    @Override
    public void onComponentRefresh()
    {
        getSettings().set(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
    }

    public void onClickAddExamGroupSet()
    {
        if (EnrExamGroupManager.instance().setDao().hasOpened(getEnrollmentCampaign().getId()))
            throw new ApplicationException(EnrExamGroupManager.instance().getProperty("examGroupSet.add.hasOpenedException"));

        getActivationBuilder().asRegion(EnrExamGroupSetAddEdit.class)
                .activate();
    }

    public void onClickEditExamGroupSet()
    {
        getActivationBuilder().asRegion(EnrExamGroupSetAddEdit.class)
                .parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong())
                .activate();
    }

    public void onClickDeleteExamGroupSet()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public void onToggleOpen()
    {
        EnrExamGroupManager.instance().setDao().doToggleOpen(getListenerParameterAsLong());
    }

    public void onEnrCampaignRefresh()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, getEnrollmentCampaign());
    }

    // Getters & Setters

    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return getSettings().get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
    }

    public boolean isNothingSelected()
    {
        return getEnrollmentCampaign() == null;
    }
}
