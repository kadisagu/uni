/**
 *$Id: IEnrExamGroupDao.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.exams.bo.EnrExamGroup.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroup;
import ru.tandemservice.unienr14.exams.entity.EnrExamScheduleEvent;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;

import java.util.Collection;

/**
 * @author Alexander Shaburov
 * @since 29.05.13
 */
public interface IEnrExamGroupDao extends INeedPersistenceSupport
{
    /**
     * Сохраняет или обновляет ЭГ.
     * @param examGroup ЭГ
     * @return id ЭГ
     */
    Long saveOrUpdate(EnrExamGroup examGroup);

    /**
     * Указаны ли ДДС для ЭГ.
     * @param examGroupId id ЭГ
     * @return true, если в ЭГ есть ДДС, иначе false
     */
    boolean hasExamPassDiscipline(Long examGroupId);

    /**
     * Исключает ДДС из ЭГ.
     * @param examPassDisciplineId id ДДС
     */
    void doExcludeEntrant(Long examPassDisciplineId);

    /**
     * Помещает ЭГ в выбранные ячейки расписания.
     * Валидация.
     * @param examGroupId id  ЭГ
     * @param examScheduleEventList список id событий расписания
     */
    void doSetScheduleEvent(Long examGroupId, Collection<EnrExamScheduleEvent> examScheduleEventList);

    void setExamGroupAndUpdateTerritorial(EnrExamPassDiscipline examPassDiscipline, OrgUnit terrOu, EnrExamGroup examGroup, boolean manual);
}
