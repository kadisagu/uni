package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.ISQLTranslator;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLUpdateQuery;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unienr14_2x9x3_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.3")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrCampaignEntrantDocument

		// создано обязательное свойство originalCanBeAccepted
		{
			// создать колонку
			tool.createColumn("enr14_camp_entrant_doc_t", new DBColumn("originalcanbeaccepted_p", DBType.BOOLEAN));

			tool.executeUpdate("update enr14_camp_entrant_doc_t set originalcanbeaccepted_p=? where originalcanbeaccepted_p is null", true);

			ISQLTranslator translator = tool.getDialect().getSQLTranslator();

			SQLUpdateQuery updateQuery = new SQLUpdateQuery("enr14_camp_entrant_doc_t", "ced");
			updateQuery.getUpdatedTableFrom().innerJoin(SQLFrom.table("enr14_c_entrant_doc_type_t", "dt"), "ced.documenttype_id=dt.id");
			updateQuery.where("dt.code_p='1'");  // EnrEntrantDocumentTypeCodes  /** Константа кода (code) элемента : Удостоверение личности (title) */ String INDENTITY_CARD = "1"
			updateQuery.set("originalcanbeaccepted_p", "?");

			tool.executeUpdate(translator.toSql(updateQuery), false);


			// сделать колонку NOT NULL
			tool.setColumnNullable("enr14_camp_entrant_doc_t", "originalcanbeaccepted_p", false);

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enrRequestedCompetition

		// создано обязательное свойство originalDocumentHandedIn
		{
			// создать колонку
			tool.createColumn("enr14_requested_comp_t", new DBColumn("originaldocumenthandedin_p", DBType.BOOLEAN));

			tool.table("enr14_requested_comp_t").triggers().clear();
			tool.executeUpdate("update enr14_requested_comp_t set originaldocumenthandedin_p=? where originaldocumenthandedin_p is null", false);

			// сделать колонку NOT NULL
			tool.setColumnNullable("enr14_requested_comp_t", "originaldocumenthandedin_p", false);

		}

//		migrate acceptContract_enrEntrantRequestPub to acceptEnrollment_enrEntrantRequestPub
    }
}