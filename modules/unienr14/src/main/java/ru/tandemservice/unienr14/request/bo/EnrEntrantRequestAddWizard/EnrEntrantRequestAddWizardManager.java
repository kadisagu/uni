/* $Id$ */
package ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.logic.EnrEntrantRequestAddWizardDAO;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.logic.IEnrEntrantRequestAddWizardDAO;

/**
 * @author nvankov
 * @since 6/6/14
 */
@Configuration
public class EnrEntrantRequestAddWizardManager extends BusinessObjectManager
{
    public static EnrEntrantRequestAddWizardManager instance()
    {
        return instance(EnrEntrantRequestAddWizardManager.class);
    }

    @Bean
    public IEnrEntrantRequestAddWizardDAO dao()
    {
        return new EnrEntrantRequestAddWizardDAO();
    }
}



    