/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.ui.CompetitionPub;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrTargetAdmissionKind;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.ui.CompetitionInStepPub.EnrEnrollmentModelCompetitionInStepPubUI;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentModel;
import ru.tandemservice.unienr14.enrollment.entity.EnrModelStep;
import ru.tandemservice.unienr14.enrollment.entity.EnrModelStepItem;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 3/31/15
 */
public class EnrModelEntrantDSHandler extends DefaultSearchDataSourceHandler
{
    public EnrModelEntrantDSHandler(String ownerId)
    {
        super(ownerId);
    }

    public static final String BIND_COMPETITION = "competition";
    public static final String BIND_TA_KIND = "taKind";
    public static final String BIND_ENR_MODEL = "model";

    public static final String VIEW_PROP_ENROLLED = "enrolled";
    public static final String VIEW_PROP_INCLUDED = "included";
    public static final String VIEW_PROP_ORIGINAL_IN = "originalIn";
    public static final String VIEW_PROP_ACCEPTED = "accepted";
    public static final String VIEW_PROP_ENROLLMENT_AVAILABLE = "enrollmentAvailable";


    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context) {

        EnrEnrollmentModel model = context.get(BIND_ENR_MODEL);
        EnrCompetition competition = context.get(BIND_COMPETITION);

        final DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(EnrRatingItem.class, "r")
            .column(property("r"))
            .order(property(EnrRatingItem.position().fromAlias("r")))
            .where(eq(property(EnrRatingItem.requestedCompetition().competition().fromAlias("r")), commonValue(competition)))
            ;

        EnrTargetAdmissionKind taKind = context.get(BIND_TA_KIND);
        if (null != taKind) {
            dql
                .joinEntity("r", DQLJoinType.inner, EnrRequestedCompetitionTA.class, "rta", eq(property("rta"), property(EnrRatingItem.requestedCompetition().fromAlias("r"))))
                .where(eq(property(EnrRequestedCompetitionTA.targetAdmissionKind().targetAdmissionKind().fromAlias("rta")), value(taKind)));
        }

        DSOutput output = DQLSelectOutputBuilder.get(input, dql, context.getSession()).order().build();

        Map<EnrRatingItem, List<EnrModelStepItem>> modelData = getModelData(model, competition, taKind);

        for (DataWrapper wrapper : DataWrapper.wrap(output)) {

            EnrModelStepItem enrolled = null;
            List<String> included = new ArrayList<>();
            List<String> originalIn = new ArrayList<>();
            List<String> accepted = new ArrayList<>();
            List<String> enrollmentAvailable = new ArrayList<>();

            for (EnrModelStepItem item : modelData.get((EnrRatingItem) wrapper.getWrapped())) {
                if (enrolled == null && item.isEnrolled() && !item.isReEnrolled()) enrolled = item;
                if (item.isIncluded()) included.add(item.getStep().getDateStr());
                if (item.isOriginalIn()) originalIn.add(item.getStep().getDateStr());
                if (item.isAccepted()) accepted.add(item.getStep().getDateStr());
                if (item.isEnrollmentAvailable()) enrollmentAvailable.add(item.getStep().getDateStr());
            }

            Collections.reverse(included);
            Collections.reverse(originalIn);
            Collections.reverse(accepted);
            Collections.reverse(enrollmentAvailable);

            wrapper.setProperty(VIEW_PROP_ENROLLED, enrolled == null ? null : new EnrolledWrapper(enrolled));
            wrapper.setProperty(VIEW_PROP_INCLUDED, StringUtils.join(included, ", "));
            wrapper.setProperty(VIEW_PROP_ORIGINAL_IN, StringUtils.join(originalIn, ", "));
            wrapper.setProperty(VIEW_PROP_ACCEPTED, StringUtils.join(accepted, ", "));
            wrapper.setProperty(VIEW_PROP_ENROLLMENT_AVAILABLE, StringUtils.join(enrollmentAvailable, ", "));
        }

        return output;
    }

    private Map<EnrRatingItem, List<EnrModelStepItem>> getModelData(EnrEnrollmentModel model, EnrCompetition competition, EnrTargetAdmissionKind taKind)
    {
        Map<EnrRatingItem, List<EnrModelStepItem>> result = SafeMap.get(ArrayList.class);

        final DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(EnrModelStepItem.class, "i")
            .column(property("i"))
            .order(property(EnrModelStepItem.step().enrollmentDate().fromAlias("i")), OrderDirection.desc)
            .where(eq(property(EnrModelStepItem.step().model().fromAlias("i")), value(model)))
            .where(eq(property(EnrModelStepItem.entity().requestedCompetition().competition().fromAlias("i")), value(competition)))
            ;

        if (null != taKind) {
            dql
                .joinPath(DQLJoinType.inner, EnrModelStepItem.entity().fromAlias("i"), "r")
                .joinEntity("i", DQLJoinType.inner, EnrRequestedCompetitionTA.class, "rta", eq(property("rta"), property(EnrRatingItem.requestedCompetition().fromAlias("r"))))
                .where(eq(property(EnrRequestedCompetitionTA.targetAdmissionKind().targetAdmissionKind().fromAlias("rta")), value(taKind)));
        }

        for (EnrModelStepItem item : DataAccessServices.dao().<EnrModelStepItem>getList(dql)) {
            result.get(item.getEntity()).add(item);
        }

        return result;
    }

    public static class EnrolledWrapper extends IdentifiableWrapper {
        private static final long serialVersionUID = 1L;

        private EnrModelStep step;
        private EnrRequestedCompetition requestedCompetition;

        public EnrolledWrapper(EnrModelStepItem item)
        {
            super(item.getEntity().getRequestedCompetition().getId(), item.getStep().getDateStr());
            this.step = item.getStep();
            this.requestedCompetition = item.getEntity().getRequestedCompetition();
        }

        public Map getPublisherParameters() {
            ParametersMap parametersMap = new ParametersMap()
                .add(EnrEnrollmentModelCompetitionInStepPubUI.PARAM_STEP, step.getId())
                .add(EnrEnrollmentModelCompetitionInStepPubUI.PARAM_COMPETITION, requestedCompetition.getCompetition().getId());
            if (requestedCompetition instanceof EnrRequestedCompetitionTA && step.getModel().getEnrollmentCampaign().getSettings().isTargetAdmissionCompetition()) {
                parametersMap.add(EnrEnrollmentModelCompetitionInStepPubUI.PARAM_TARGET_ADMISSION_KIND, ((EnrRequestedCompetitionTA)requestedCompetition).getTargetAdmissionKind().getTargetAdmissionKind().getId());
            }
            return parametersMap;
        }
    }
}




