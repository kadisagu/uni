/* $Id: SecTemplateRoleAssignUI.java 6520 2015-05-18 12:42:03Z oleyba $ */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.ui.TemplateRoleAssign;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.organization.sec.bo.Sec.SecManager;
import org.tandemframework.shared.organization.sec.bo.Sec.ui.RoleAssign.SecRoleAssignUI;
import org.tandemframework.shared.organization.sec.entity.RoleConfigTemplate;
import ru.tandemservice.unienr14.sec.entity.RoleAssignmentTemplateEnrCampaign;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

/**
 * @author Vasily Zhukov
 * @since 09.11.2011
 */
@Input({
    @Bind(key = SecRoleAssignUI.BIND_PRINCIPAL_CONTEXT_ID, binding = "principalContextId", required = true)
})
public class EnrEnrollmentCampaignTemplateRoleAssignUI extends UIPresenter
{

    private Long _principalContextId;
    private IPrincipalContext _principalContext;
    private EnrEnrollmentCampaign enrollmentCampaign;
    private RoleConfigTemplate _localRole;

    @Override
    public void onComponentRefresh()
    {
        _principalContext = DataAccessServices.dao().getNotNull(_principalContextId);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(SecRoleAssignUI.BIND_PRINCIPAL_CONTEXT_ID, _principalContextId);
        dataSource.put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, getEnrollmentCampaign());
    }

    public void onClickApply()
    {
        SecManager.instance().dao().doAssignRole(new RoleAssignmentTemplateEnrCampaign(getPrincipalContext(), getLocalRole(), getEnrollmentCampaign()));
        _uiSupport.getParentUI().deactivate();
    }

    public void onClickCancel() {
        _uiSupport.getParentUI().deactivate();
    }

    // Getters & Setters

    public Long getPrincipalContextId()
    {
        return _principalContextId;
    }

    public void setPrincipalContextId(Long principalContextId)
    {
        _principalContextId = principalContextId;
    }

    public IPrincipalContext getPrincipalContext()
    {
        return _principalContext;
    }

    public void setPrincipalContext(IPrincipalContext principalContext)
    {
        _principalContext = principalContext;
    }

    public RoleConfigTemplate getLocalRole()
    {
        return _localRole;
    }

    public void setLocalRole(RoleConfigTemplate localRole)
    {
        _localRole = localRole;
    }

    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        this.enrollmentCampaign = enrollmentCampaign;
    }
}
