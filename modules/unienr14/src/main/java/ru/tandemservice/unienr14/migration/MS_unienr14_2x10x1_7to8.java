package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unienr14_2x10x1_7to8 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
		        new ScriptDependency("org.tandemframework", "1.6.18"),
		        new ScriptDependency("org.tandemframework.shared", "1.10.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrEnrollmentCampaignType

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("enr14_c_campaign_type_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_enrenrollmentcampaigntype"),
				new DBColumn("discriminator", DBType.SHORT).setNullable(false),
				new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
				new DBColumn("priority_p", DBType.INTEGER).setNullable(false),
				new DBColumn("title_p", DBType.createVarchar(1200))
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("enrEnrollmentCampaignType");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrEnrollmentCampaignSettings

        // создано свойство enrollmentCampaignType
        {
            // создать колонку
            tool.createColumn("enr14_campaign_settings_t", new DBColumn("enrollmentcampaigntype_id", DBType.LONG));
        }
    }
}