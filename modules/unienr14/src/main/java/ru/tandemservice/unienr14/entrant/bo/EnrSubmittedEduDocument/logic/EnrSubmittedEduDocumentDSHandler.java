/* $Id$ */
package ru.tandemservice.unienr14.entrant.bo.EnrSubmittedEduDocument.logic;

import com.beust.jcommander.internal.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Predicate;
import org.apache.commons.collections15.Transformer;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import org.tandemframework.shared.person.catalog.entity.EduDocumentKind;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantOriginalDocumentStatus;
import ru.tandemservice.unienr14.entrant.entity.EnrOrganizationOriginalIn;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.EnrEnrollmentCommissionManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author azhebko
 * @since 31.07.2014
 */
public class EnrSubmittedEduDocumentDSHandler extends DefaultSearchDataSourceHandler
{
    public static final String BIND_LAST_NAME = "lastName";
    public static final String BIND_FIRST_NAME = "firstName";
    public static final String BIND_MIDDLE_NAME = "middleName";
    public static final String BIND_PERSONAL_NUMBER = "personalNumber";
    public static final String BIND_REQUEST_NUMBER = "requestNumber";
    public static final String BIND_ORIGINAL_SUBMITTED_FROM = "originalSubmittedFrom";
    public static final String BIND_ORIGINAL_SUBMITTED_TO = "originalSubmittedTo";
    public static final String BIND_ORIGINAL_REJECTED_FROM = "originalRejectedFrom";
    public static final String BIND_ORIGINAL_REJECTED_TO = "originalRejectedTo";
    public static final String BIND_EDU_DOCUMENT_KIND = "eduDocumentKind";
    public static final String BIND_EDU_DOCUMENT_SERIA = "eduDocumentSeria";
    public static final String BIND_EDU_DOCUMENT_NUMBER = "eduDocumentNumber";
    public static final String BIND_COMPETITION_UTIL = CommonFilterAddon.class.getSimpleName();
    public static final String BIND_ENROLLMENT_COMMISSION = "enrollmentCommission";

	public static final String VIEW_MAIN_DOCUMENT = "mainDocument";
    public static final String VIEW_PERSONAL_NUMBER = "personalNumber";
    public static final String VIEW_ENTRANT = "entrant";
    public static final String VIEW_ORIGINAL = "original";
    public static final String VIEW_REQUEST_NUMBER = "requestNumber";
    public static final String VIEW_ENROLLMENT_COMMISSION = "enrollmentCommission";
    public static final String VIEW_ORG_ORIGINAL_IN = "orgOriginalIn";
    public static final String VIEW_REQ_COMP = "reqComp";

    public EnrSubmittedEduDocumentDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        EnrEnrollmentCampaign enrollmentCampaign = context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
        Object enrollmentCommission = context.get(BIND_ENROLLMENT_COMMISSION);
        String lastName = context.get(BIND_LAST_NAME);
        String firstName = context.get(BIND_FIRST_NAME);
        String middleName = context.get(BIND_MIDDLE_NAME);
        String personalNumber = context.get(BIND_PERSONAL_NUMBER);
        String requestNumber = context.get(BIND_REQUEST_NUMBER);
        Date originalSubmittedFrom = context.get(BIND_ORIGINAL_SUBMITTED_FROM);
        Date originalSubmittedTo = context.get(BIND_ORIGINAL_SUBMITTED_TO);
        Date originalRejectedFrom = context.get(BIND_ORIGINAL_REJECTED_FROM);
        Date originalRejectedTo = context.get(BIND_ORIGINAL_REJECTED_TO);
        List<EduDocumentKind> eduDocumentKindList = context.get(BIND_EDU_DOCUMENT_KIND);
        String eduDocumentSeria = context.get(BIND_EDU_DOCUMENT_SERIA);
        String eduDocumentNumber = context.get(BIND_EDU_DOCUMENT_NUMBER);
        CommonFilterAddon competitionUtil = context.get(BIND_COMPETITION_UTIL); // используется для филиала

        DQLSelectBuilder requestBuilder = new DQLSelectBuilder().fromEntity(EnrEntrantRequest.class, "r");

        EnrEnrollmentCommissionManager.instance().dao().filterEntrantRequestByEnrCommission(requestBuilder, "r", enrollmentCommission);

        FilterUtils.applySelectFilter(requestBuilder, EnrEntrantRequest.regNumber().fromAlias("r"), requestNumber);

        if (competitionUtil != null && !competitionUtil.isEmptyFilters())
        {
            requestBuilder.where(exists(
                    new DQLSelectBuilder().fromEntity(EnrRequestedCompetition.class, "rc").column(value(1))
                            .where(eq(property("rc", EnrRequestedCompetition.request()), property("r")))
                            .where(in(property("rc", EnrRequestedCompetition.competition()), competitionUtil.getEntityIdsFilteredBuilder("alias").buildQuery()))
                            .buildQuery()
            ));
        }

        if (originalSubmittedFrom != null || originalSubmittedTo != null || originalRejectedFrom != null || originalRejectedTo != null)
        {
            DQLSelectBuilder originalSubBuilder = new DQLSelectBuilder()
                .fromEntity(EnrEntrantOriginalDocumentStatus.class, "o")
                .where(eq(property("o", EnrEntrantOriginalDocumentStatus.eduDocument().id()), property("r", EnrEntrantRequest.eduDocument().id())));

            FilterUtils.applyBetweenFilter(originalSubBuilder, "o", EnrEntrantOriginalDocumentStatus.registrationDate().s(), originalSubmittedFrom, originalSubmittedTo);
            FilterUtils.applyBetweenFilter(originalSubBuilder, "o", EnrEntrantOriginalDocumentStatus.takeAwayDate().s(), originalRejectedFrom, originalRejectedTo);

            requestBuilder.where(exists(originalSubBuilder.buildQuery()));
        }

        final DQLOrderDescriptionRegistry orderDescription = new DQLOrderDescriptionRegistry(PersonEduDocument.class, "d");
        final DQLSelectBuilder docsDql = orderDescription.buildDQLSelectBuilder();
        docsDql.column(property("d"));
        docsDql.joinEntity("d", DQLJoinType.inner, EnrEntrant.class, "e", eq(property("e", EnrEntrant.person().id()), property("d", PersonEduDocument.person().id())));
        docsDql.where(eq(property("e", EnrEntrant.archival()), value(false)));
        docsDql.where(exists(
                requestBuilder.column(value(1))
                        .where(eq(property("r", EnrEntrantRequest.eduDocument()), property("d")))
                        .where(eq(property("r", EnrEntrantRequest.entrant()), property("e")))
                        .buildQuery()
        ));

        FilterUtils.applySelectFilter(docsDql, "e", EnrEntrant.enrollmentCampaign(), enrollmentCampaign);
        FilterUtils.applySelectFilter(docsDql, "e", EnrEntrant.personalNumber(), personalNumber);

        FilterUtils.applySelectFilter(docsDql, "d", PersonEduDocument.eduDocumentKind(), eduDocumentKindList);
        FilterUtils.applySelectFilter(docsDql, "d", PersonEduDocument.seria(), eduDocumentSeria);
        FilterUtils.applySelectFilter(docsDql, "d", PersonEduDocument.number(), eduDocumentNumber);

        FilterUtils.applySimpleLikeFilter(docsDql, "e", EnrEntrant.person().identityCard().lastName(), lastName);
        FilterUtils.applySimpleLikeFilter(docsDql, "e", EnrEntrant.person().identityCard().firstName(), firstName);
        FilterUtils.applySimpleLikeFilter(docsDql, "e", EnrEntrant.person().identityCard().middleName(), middleName);

        final OrderDescription personalNumberOrder = new OrderDescription("e", EnrEntrant.personalNumber());
        final OrderDescription lastNameOrder = new OrderDescription("e", EnrEntrant.person().identityCard().lastName());
        final OrderDescription firstNameOrder = new OrderDescription("e", EnrEntrant.person().identityCard().firstName());
        final OrderDescription middleNameOrder = new OrderDescription("e", EnrEntrant.person().identityCard().middleName());
        orderDescription
            .setOrders("personalNumber", personalNumberOrder)
            .setOrders(EnrSubmittedEduDocumentDSHandler.VIEW_ENTRANT + "." + EnrEntrant.P_FULL_FIO, lastNameOrder, firstNameOrder, middleNameOrder)
            .setOrders("eduDocumentKind", new OrderDescription("d", PersonEduDocument.eduDocumentKind().shortTitle()), personalNumberOrder);

        orderDescription.applyOrder(docsDql, input.getEntityOrder());

        final DSOutput dsOutput = DQLSelectOutputBuilder.get(input, docsDql, context.getSession()).pageable(true).build();
        final List<PersonEduDocument> eduDocuments = dsOutput.getRecordList();
        if (eduDocuments.isEmpty()) {
            return dsOutput;
        }

        final Map<Long, EnrEntrant> person2entrantMap = new HashMap<>();
        DQLSelectBuilder entrantBuilder = new DQLSelectBuilder().fromEntity(EnrEntrant.class, "e")
            .column(property("e"))
            .where(eq(property("e", EnrEntrant.enrollmentCampaign()), value(enrollmentCampaign)))
            .where(exists(
                    PersonEduDocument.class,
                    PersonEduDocument.person().s(), property("e", EnrEntrant.person()),
                    PersonEduDocument.id().s(), eduDocuments
            ));

        for (EnrEntrant entrant: IUniBaseDao.instance.get().<EnrEntrant>getList(entrantBuilder))
            person2entrantMap.put(entrant.getPerson().getId(), entrant);

        DQLSelectBuilder originalBuilder = new DQLSelectBuilder()
            .fromEntity(EnrEntrantOriginalDocumentStatus.class, "o")
            .column(property("o", EnrEntrantOriginalDocumentStatus.eduDocument().id()))
            .distinct()
            .where(in(property("o", EnrEntrantOriginalDocumentStatus.entrant()), person2entrantMap.values()))
            .where(in(property("o", EnrEntrantOriginalDocumentStatus.eduDocument()), eduDocuments))
            .where(eq(property("o", EnrEntrantOriginalDocumentStatus.originalDocumentHandedIn()), value(Boolean.TRUE)));

        FilterUtils.applyBetweenFilter(originalBuilder, "o", EnrEntrantOriginalDocumentStatus.registrationDate().s(), originalSubmittedFrom, originalSubmittedTo);
        FilterUtils.applyBetweenFilter(originalBuilder, "o", EnrEntrantOriginalDocumentStatus.takeAwayDate().s(), originalRejectedFrom, originalRejectedTo);

        final Collection<Long> originalDocumentsIds = new HashSet<>(IUniBaseDao.instance.get().<Long>getList(originalBuilder));

        final Map<Long, Collection<EnrEntrantRequest>> document2requestMap = SafeMap.get(ArrayList.class);
        final Map<Long, EnrRequestedCompetition> document2reqCompMap = Maps.newHashMap();

        List<EnrEntrantRequest> requestList = Lists.newArrayList();

        BatchUtils.execute(eduDocuments, DQL.MAX_VALUES_ROW_NUMBER, elements ->
        {
            DQLSelectBuilder reqBuilder = new DQLSelectBuilder().fromEntity(EnrEntrantRequest.class, "er")
                    .where(eq(property("er", EnrEntrantRequest.entrant().enrollmentCampaign()), value(enrollmentCampaign)))
                    .where(in(property("er", EnrEntrantRequest.eduDocument()), elements));

            List<EnrEntrantRequest> reqList = createStatement(reqBuilder).list();

            requestList.addAll(reqList);

            for (EnrEntrantRequest request:  reqList)
            {
                document2requestMap.get(request.getEduDocument().getId()).add(request);
                if(request.getEduInstDocOriginalRef() != null && request.getEduInstDocOriginalRef().getRequestedCompetition() != null)
                    document2reqCompMap.put(request.getEduDocument().getId(), request.getEduInstDocOriginalRef().getRequestedCompetition());

            }
        });

        final Map<Long, EnrOrganizationOriginalIn> document2orgOriginalInMap = Maps.newHashMap();
        for (EnrOrganizationOriginalIn orgOriginalIn : DataAccessServices.dao().getList(EnrOrganizationOriginalIn.class, EnrOrganizationOriginalIn.eduDocument(), eduDocuments))
            document2orgOriginalInMap.put(orgOriginalIn.getEduDocument().getId(), orgOriginalIn);

        final Transformer<EnrEntrantRequest, DataWrapper> requestWrapperTransformer = request -> new DataWrapper(request.getEntrant().getId(), request.getRegNumber());
        final Predicate<EnrEntrantRequest> requestWithCommissionPredicate = request -> request.getEnrollmentCommission() != null;
        final Transformer<EnrEntrantRequest, String> enrollmentCommissionShortTitleTransformer = request -> request.getEnrollmentCommission().getShortTitle();

        dsOutput.transform((PersonEduDocument document) -> {
            DataWrapper wrapper = new DataWrapper(document);
            Collection<EnrEntrantRequest> requests = document2requestMap.get(document.getId());
            EnrEntrant entrant = person2entrantMap.get(document.getPerson().getId());
            EnrRequestedCompetition requestedCompetition = document2reqCompMap.get(document.getId());
            boolean isMainDoc = document.getPerson().getMainEduDocument() != null && document.getId().equals(document.getPerson().getMainEduDocument().getId());

            wrapper.setProperty(VIEW_MAIN_DOCUMENT, isMainDoc);
            wrapper.setProperty(VIEW_ENTRANT, entrant);
            wrapper.setProperty(VIEW_PERSONAL_NUMBER, entrant.getPersonalNumber());
            wrapper.setProperty(VIEW_ORIGINAL, originalDocumentsIds.contains(document.getId()));
            wrapper.setProperty(VIEW_REQUEST_NUMBER, CollectionUtils.collect(requests, requestWrapperTransformer));
            wrapper.setProperty(VIEW_ENROLLMENT_COMMISSION, new TreeSet<>(CollectionUtils.collect(CollectionUtils.select(requests, requestWithCommissionPredicate), enrollmentCommissionShortTitleTransformer)));
            wrapper.setProperty(VIEW_ORG_ORIGINAL_IN, document2orgOriginalInMap.get(document.getId()));
            wrapper.setProperty(VIEW_REQ_COMP, requestedCompetition);
            return wrapper;
        });
        patchOutput(dsOutput.<DataWrapper>getRecordList(), document2requestMap, requestList);
        return dsOutput;
    }

    protected void patchOutput(List<DataWrapper> wrappers, Map<Long, Collection<EnrEntrantRequest>> document2requestMap, List<EnrEntrantRequest> requestList)
    {
        // customize in projects
    }
}