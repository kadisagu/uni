package ru.tandemservice.unienr14.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * TODO: перенести в бранч
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x3_8to9 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
        {
            new ScriptDependency("org.tandemframework", "1.6.15"),
            new ScriptDependency("org.tandemframework.shared", "1.6.3")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        tool.table("enr14_exam_pass_discipline_t").checkConstraints().clear();
        tool.table("enr14_exam_pass_discipline_t").triggers().clear();
        tool.executeUpdate("update enr14_exam_pass_discipline_t set markAsLong_p=? where absenceNote_id in (select id from enr14_c_abs_note_t where code_p=?)", 0l, "1");
        tool.executeUpdate("update enr14_exam_pass_discipline_t set markAsLong_p=null where absenceNote_id in (select id from enr14_c_abs_note_t where code_p=?)", "2");
    }
}