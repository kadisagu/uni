package ru.tandemservice.unienr14.order.entity;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;

import ru.tandemservice.unienr14.catalog.entity.EnrOrderType;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.order.entity.gen.EnrAbstractExtractGen;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;

/**
 * Абстрактная выписка на абитуриента (2013)
 */
public abstract class EnrAbstractExtract extends EnrAbstractExtractGen implements Comparable, IAbstractExtract
{
    public static final String SL_REQUESTED_COMPETITION = "requestedCompetition";

    @Override
    public String getTitle()
    {
        if (getParagraph() == null) {
            return this.getClass().getSimpleName();
        }
        return "Выписка " + getNumber() + "/" + getParagraph().getNumber() + " | " + getParagraph().getOrder().getTitle() + " | " + getEntrant().getPerson().getFullFio();
    }

    @Override
    public void setState(ICatalogItem state)
    {
        setState((ExtractStates) state);
    }

    @Override
    public EnrOrderType getType()
    {
        return (EnrOrderType) getParagraph().getOrder().getType();
    }

    @Override
    public void setType(ICatalogItem type)
    {
        throw new UnsupportedOperationException();
    }


    @Override
    public void setParagraph(IAbstractParagraph paragraph)
    {
        setParagraph((EnrAbstractParagraph) paragraph);
    }

    public abstract EnrRequestedCompetition getRequestedCompetition();

    public abstract EnrCompetition getCompetition();

    public abstract EnrEntrant getEntrant();

    @Override
    @EntityDSLSupport
    public String getShortTitle() {
        return "Выписка №" + getNumber() + "/" + getParagraph().getNumber() + " | " + getParagraph().getOrder().getTitle() + " (" + (isCancelled() ? "Отменена" : getState().getTitle()) + ")";
    }
}