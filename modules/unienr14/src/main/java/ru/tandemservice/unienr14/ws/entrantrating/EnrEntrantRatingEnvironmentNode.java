// Copyright 2006-2012 Tandem Service Software
// Proprietary Licence for Tandem University

package ru.tandemservice.unienr14.ws.entrantrating;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Ежедневный рейтинг абитуриентов (с выделением групп «Общий прием» и «Целевой прием»)
 *
 * @author Vasily Zhukov
 * @since 24.03.2011
 */
@XmlRootElement
public class EnrEntrantRatingEnvironmentNode
{
    public EnrEntrantRatingEnvironmentNode()
    {
        currentDateTime = new Date();
    }

    @XmlAttribute(required = true)
    public Date currentDateTime;

    /** Название приемной кампании */
    @XmlAttribute(required = true)
    public String enrollmentCampaignTitle;

    /**
     * Название года обучения приемной кампании
     */
    @XmlAttribute(required = true)
    public String educationYearTitle;

    /**
     * Идентификатор конкурса. Только при построении отчета по одному конкурсу.
     */
    @XmlAttribute
    public String competitionId;

    /**
     * Все виды целевого приема в указанной приемной кампании
     */
    public TargetAdmissionKindNode targetAdmissionKind = new TargetAdmissionKindNode();

    /**
     * Все категории особых прав
     */
    public BenefitCategoryNode benefitCategory = new BenefitCategoryNode();

    /**
     * Все способы подачи и возврата оригиналов документов
     */
    public OriginalSubmissionAndReturnWayNode originalSubmissionAndReturnWay = new OriginalSubmissionAndReturnWayNode();

    /**
     * Конкурсы (комбинации условий поступления) для приема
     */
    public CompetitionNode competition = new CompetitionNode();


    /**
     * Простая строка таблицы
     */
    public static class Row implements Comparable<Row>
    {
        public Row() { }

        public Row(String title, String id) {
            this.title = title;
            this.id = id;
        }

        public Row(String shortTitle, String title, String id) {
            this.shortTitle = shortTitle;
            this.title = title;
            this.id = id;
        }

        @Override public int compareTo(Row o) {
            return o.id.compareTo(o.toString());
        }

        /** Сокращенное название */
        @XmlAttribute
        public String shortTitle;

        /** Название */
        @XmlAttribute(required = true)
        public String title;

        /** Идентификатор строки */
        @XmlAttribute(required = true)
        public String id;
    }

    /**
     * Все виды целевого приема в указанной приемной кампании
     */
    public static class TargetAdmissionKindNode
    {
        public static class TargetAdmissionKindRow extends Row
        {
            public TargetAdmissionKindRow() { }

            public TargetAdmissionKindRow(String shortTitle, String title, String id, int priority, boolean used) {
                super(shortTitle, title, id);
                this.priority = priority;
                this.used = used;
            }

            /** Приоритет */
            @XmlAttribute(required = true)
            public int priority;

            /**
             * Используется ли в текущей приемной кампании
             * true - используется; false - не используется
             */
            @XmlAttribute(required = true)
            public boolean used;
        }

        public List<TargetAdmissionKindRow> row = new ArrayList<TargetAdmissionKindRow>();
    }

    /** Категории особых прав */
    public static class BenefitCategoryNode { public List<Row> row = new ArrayList<Row>(); }

    /** Способы подачи и возврата оригиналов документов */
    public static class OriginalSubmissionAndReturnWayNode { public List<Row> row = new ArrayList<Row>(); }

    /**
     * Конкурсы (комбинации условий поступления) для приема
     */
    public static class CompetitionNode { public List<CompetitionRow> row = new ArrayList<CompetitionRow>(); }

    /**
     * Конкурс (комбинация условий поступления) для приема
     */
    public static class CompetitionRow
    {
        /** Вступительное испытание конкурса */
        public static class EntranceDisciplineRow
        {
            /** Идентификатор вступительного испытания */
            @XmlAttribute(required = true)
            public String id;

            /** Название дисциплины набора вступительного испытания */
            @XmlAttribute
            public String title;

            /** Сокращенное название дисциплины набора вступительного испытания */
            @XmlAttribute
            public String shortTitle;

            /** Зачетный балл. Передается в целом числе со сдвигом на три порядка для дробной части - т.е. умноженный на 1000 */
            public long passMarkMultipliedBy1000;

            /** Приоритет */
            @XmlAttribute
            public Integer priority;

            /** Вид вступительного испытания */
            @XmlAttribute(required = true)
            public String examType;

            /** Формы сдачи вступительного испытания */
            public List<Row> passForms = new ArrayList<Row>();
        }

        /**
         * Строка отчета (выбранное направление приема)
         */
        public static class RequestedCompetitionRow
        {
            /** Регистрационный номер заявления */
            @XmlAttribute(required = true)
            public String regNumber;

            /** ФИО абитуриента */
            @XmlAttribute(required = true)
            public String fio;

            /** ФИО абитуриента */
            @XmlAttribute(required = true)
            public String fullFio;

            /**
             * Вид целевого приема
             * @see TargetAdmissionKindNode
             */
            @XmlAttribute
            public String targetAdmissionKind;

            /** Номер договора на ЦП */
            @XmlAttribute
            public String targetAdmissionContractNumber;

            /** Дата договора на ЦП */
            @XmlAttribute
            public Date targetAdmissionContractDate;

            /** Название организации ЦП; */
            @XmlAttribute
            public String targetAdmissionExtOrgUnit;

            /** Сокращенное название организации ЦП; */
            @XmlAttribute
            public String targetAdmissionExtOrgUnitShortTitle;

            /** Признак приема в интересах государства для вида ЦП */
            @XmlAttribute
            public boolean targetAdmissionKindInterestsState;

            /**
             * Категория особого права, использованного при поступлении по данному конкурсу
             * @see BenefitCategoryNode
             */
            @XmlAttribute
            public String preferenceCategory;

            /**
             * Категория особого права, использованного при поступлении по данному конкурсу
             * @see BenefitCategoryNode
             */
            @XmlAttribute
            public String benefitCategory;

            /**
             * Образовательная программа, выбранная при указании особого права
             * @see BenefitCategoryNode
             */
            @XmlAttribute
            public String benefitProgram;

            /**
             * Способ подачи оригиналов документов
             * @see OriginalSubmissionAndReturnWayNode
             */
            @XmlAttribute
            public String originalSubmissionWay;

            /**
             * Приоритет выбранного конкурса, глобальный
             */
            @XmlAttribute
            public Integer priority;

            /** Идентификатор приказа о зачислении */
            @XmlAttribute
            public String orderId;

            /** Номер приказа о зачислении */
            @XmlAttribute
            public String orderNumber;

            /** Дата приказа о зачислении */
            @XmlAttribute
            public Date orderDate;


            /** Сумма баллов по конкурсу */
            @XmlAttribute(required = true)
            public String finalMark;

            @XmlAttribute(required = true)
            public boolean passedExam;

            /** Сумма баллов за ВИ */
            @XmlAttribute(required = true)
            public String entranceMark;

            /** Сумма баллов за ИД */
            @XmlAttribute(required = true)
            public String achievementMark;

            /** Приоритет конкурса для абитуриента */
            @XmlAttribute(required = true)
            public Integer priorityPerEntrant;

            /** Позиция в конкурсе */
            @XmlAttribute(required = true)
            public Integer position;

            /** Абсолютная позиция в конкурсе */
            @XmlAttribute(required = true)
            public Integer absolutePosition;

            /** Средний балл по документу об образовании */
            @XmlAttribute
            public String averageEduInstitutionMark;

            /** Сдан оригинал документа об образовании */
            @XmlAttribute(required = true)
            public Boolean originalIn = false;

            /** Итоговое согласие на зачисление */
            @XmlAttribute(required = true)
            public Boolean accepted = false;

            /** Получено согласие абитуриента на зачисление */
            @XmlAttribute(required = true)
            public Boolean acceptedEntrant = false;

            /** Отказ от зачисления */
            @XmlAttribute(required = true)
            public Boolean refusedToBeEnrolled = false;

            /** Рекомендован */
            @XmlAttribute(required = true)
            public Boolean recommended = false;

            /** Дата зачисления, к которой рекомендован */
            @XmlAttribute
            public String recommendedEnrollmentDate;

            /** Состояние абитуриента */
            @XmlAttribute
            public String status;

            /** Код состояния абитуриента */
            @XmlAttribute
            public String statusCode;

            /** Требуется общежитие */
            @XmlAttribute
            public Boolean needDormitory = false;

            /**
             * Оценки по дисциплинам вступительных испытаний. Каждая оценка может быть:
             * 1. x, если нет актуальной ВВИ по данному конкурсу
             * 2. -, если для актуальной ВВИ нет основания балла
             * 3. иначе балл из основания балла             *
             * @see EntranceDisciplineNode
             */
            @XmlAttribute
            @XmlList
            public List<String> marks = new ArrayList<String>();


            /**
             * Оценки по дисциплинам вступительных испытаний.
             * @see EntranceDisciplineNode
             */
            public List<String> markEntranceExams = new ArrayList<String>();

            /**
             * Оценки по индивидуальным достижениям.
             */
            public List<String> markEntrantAchievements = new ArrayList<String>();

            /**
             * Идентификатор абитуриента
             */
            @XmlAttribute(required = true)
            public String entrantId;
        }

        /** Образовательная программа */
        public static class EduProgramRow
        {
            /** Идентификатор образовательной программы */
            @XmlAttribute(required = true)
            public String id;

            /** Направленность */
            @XmlAttribute
            public String programSpec;

            /** Общая направленность */
            @XmlAttribute(required = true)
            public boolean rootSpec;

            /** Форма обучения */
            @XmlAttribute(required = true)
            public String eduProgramForm;

            /** Углубленное изучение для ОП СПО */
            @XmlAttribute
            public String secondaryInDepth;

            /** Продолжительность обучения по образовательной программе */
            @XmlAttribute(required = true)
            public String duration;

            /** Особенность реализации */
            @XmlAttribute
            public String eduProgramTrait;

            /** Квалификация для ОП бакалавриата */
            @XmlAttribute
            public String bachelorQualificationShortTitle;
        }


        /**
         * Все вступительные испытания по данному конкурсу
         */
        public static class EntranceDisciplineNode
        {
            public List<EntranceDisciplineRow> row = new ArrayList<EntranceDisciplineRow>();
        }

        /**
         * Все абитуриенты, подавшие документы на данный конкурс
         */
        public static class EntrantNode
        {
            public List<RequestedCompetitionRow> row = new ArrayList<RequestedCompetitionRow>();
        }

        /**
         * Образовательные программы
         */
        public static class EduProgramsNode
        {
            public List<EduProgramRow> row = new ArrayList<EduProgramRow>();
        }

        /**
         * План приема
         */
        @XmlAttribute
        public Integer plan;

        /** Направление (профессия, специальность) */
        @XmlAttribute(required = true)
        public String eduProgramSubject;

        /** Тип направления для перечня */
        @XmlAttribute(required = true)
        public String eduProgramSubjectType;

        /** Форма обучения */
        @XmlAttribute(required = true)
        public String eduProgramForm;

        /** Вид обр. программы */
        @XmlAttribute(required = true)
        public String eduProgramKind;

        /** Уровень образования */
        @XmlAttribute(required = true)
        public String eduLevel;

        /** Уровня образования (код элемента справочника)*/
        @XmlAttribute(required = true)
        public String eduLevelCode;

        /** Набор ОП для приема */
        @XmlAttribute(required = true)
        public String programSet;

        /** Подразделение, ведущее прием */
        @XmlAttribute(required = true)
        public String enrOrgUnit;

        /** Подразделение, ведущее прием (территориальное название) */
        @XmlAttribute(required = true)
        public String enrOrgUnitTerritorialFullTitle;

        /** Тип подразделения, ведущего прием */
        @XmlAttribute(required = true)
        public String enrOrgUnitTypeCode;

        /** Населенный пункт подразделения, ведущего прием */
        @XmlAttribute(required = true)
        public String enrOrgUnitSettlement;

        /** Формирующее подразделение */
        @XmlAttribute(required = true)
        public String formativeOrgUnit;

        /** Тип формирующего подразделения */
        @XmlAttribute(required = true)
        public String formativeOrgUnitTypeCode;

        /** Печатное название формирующего подразделения (именительный падеж) */
        @XmlAttribute
        public String formativeOrgUnitNominativeCaseTitle;

        /** Населенный пункт формирующего подразделения */
        @XmlAttribute(required = true)
        public String formativeOrgUnitSettlement;

        /** Вид заявления */
        @XmlAttribute(required = true)
        public String requestType;

        /** Вид приема */
        @XmlAttribute(required = true)
        public String competitionType;

        /** Публичное название вида приема*/
        @XmlAttribute(required = true)
        public String competitionTypePublicTitle;

        /** Краткое название вида возмещения затрат */
        @XmlAttribute(required = true)
        public String compensationTypeShortTitle;

        /** Требования к уровню образования поступающих */
        @XmlAttribute(required = true)
        public String eduLevelRequirementCode;

        /** Требования к уровню образования поступающих */
        @XmlAttribute(required = true)
        public String eduLevelRequirement;

        /** Требования к уровню образования поступающих (родительный падеж) */
        @XmlAttribute
        public String eduLevelRequirementGenetiveTitle;

        /** Профили (ОП) в наборе ОП */
        @XmlAttribute
        public String profileTitle;

        /** Идентификатор конкурса */
        @XmlAttribute(required = true)
        public String id;

        /**
         * Все вступительные испытания по конкурсу
         */
        public EntranceDisciplineNode entranceDiscipline = new EntranceDisciplineNode();

        /**
         * Все абитуриенты, подавшие документы по конкурсу
         */
        public EntrantNode entrant = new EntrantNode();

        /**
         * Образовательные программы
         */
        public EduProgramsNode eduProgram = new EduProgramsNode();

    }


}
