package ru.tandemservice.unienr14.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Способы подачи и возврата оригиналов документов"
 * Имя сущности : enrOriginalSubmissionAndReturnWay
 * Файл data.xml : unienr.catalog.data.xml
 */
public interface EnrOriginalSubmissionAndReturnWayCodes
{
    /** Константа кода (code) элемента : Лично (title) */
    String LICHNO = "1";
    /** Константа кода (code) элемента : По доверенности (title) */
    String PO_DOVERENNOSTI = "2";
    /** Константа кода (code) элемента : По почте (title) */
    String PO_POCHTE = "3";

    Set<String> CODES = ImmutableSet.of(LICHNO, PO_DOVERENNOSTI, PO_POCHTE);
}
