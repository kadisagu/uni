/**
 *$Id: EnrTargetAdmissionKindSearchDSHandler.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.settings.bo.EnrTargetAdmission.logic;

import com.beust.jcommander.internal.Maps;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import ru.tandemservice.unienr14.catalog.entity.EnrTargetAdmissionKind;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrTargetAdmission.ui.List.EnrTargetAdmissionListUI;
import ru.tandemservice.unienr14.settings.bo.EnrTargetAdmission.util.TargetAdmissionKindDataWrapper;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignTargetAdmissionKind;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrTargetAdmissionOrgUnit;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 15.04.13
 */
public class EnrTargetAdmissionKindSearchDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public EnrTargetAdmissionKindSearchDSHandler(String ownerId)
    {
        super(ownerId, EnrTargetAdmissionKind.class);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final EnrEnrollmentCampaign enrCamp = context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);

        if (enrCamp == null)
            return ListOutputBuilder.get(input, new ArrayList()).build();

        final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrTargetAdmissionKind.class, "k").column(property("k"))
                .order(property(EnrTargetAdmissionKind.priority().fromAlias("k")));

        final List<EnrCampaignTargetAdmissionKind> enrCampTargetAdmissionKind = new DQLSelectBuilder().fromEntity(EnrCampaignTargetAdmissionKind.class, "ck")
                .where(eq(property(EnrCampaignTargetAdmissionKind.enrollmentCampaign().fromAlias("ck")), value(enrCamp)))
                .createStatement(context.getSession()).list();

        Map<EnrTargetAdmissionKind, Boolean> enrTargetAdmissionKindBooleanMap = Maps.newHashMap();
        for(EnrCampaignTargetAdmissionKind campaignTargetAdmissionKind : enrCampTargetAdmissionKind)
        {
            enrTargetAdmissionKindBooleanMap.put(campaignTargetAdmissionKind.getTargetAdmissionKind(), campaignTargetAdmissionKind.isStateInterest());
        }

//        final List<EnrTargetAdmissionKind> enrTargetAdmissionKind = new DQLSelectBuilder().fromEntity(EnrCampaignTargetAdmissionKind.class, "ck").column(property(EnrCampaignTargetAdmissionKind.targetAdmissionKind().fromAlias("ck")))
//                .where(eq(property(EnrCampaignTargetAdmissionKind.enrollmentCampaign().fromAlias("ck")), value(enrCamp)))
//                .createStatement(getSession()).list();

        List<EnrTargetAdmissionOrgUnit> targetAdmissionOrgUnitList = new DQLSelectBuilder()
                .fromEntity(EnrTargetAdmissionOrgUnit.class, "taou")
                .where(eq(property("taou", EnrTargetAdmissionOrgUnit.enrCampaignTAKind().enrollmentCampaign()), value(enrCamp)))
                .createStatement(context.getSession()).list();

        Map<EnrTargetAdmissionKind, List<ExternalOrgUnit>> externalOrgUnitMap = SafeMap.get(ArrayList.class);
        for (EnrTargetAdmissionOrgUnit ou: targetAdmissionOrgUnitList)
        {
            externalOrgUnitMap.get(ou.getEnrCampaignTAKind().getTargetAdmissionKind()).add(ou.getExternalOrgUnit());
        }

        List<TargetAdmissionKindDataWrapper> wrappers = new ArrayList<>();
        for (EnrTargetAdmissionKind kind: builder.createStatement(context.getSession()).<EnrTargetAdmissionKind>list())
        {
            Boolean stateInterest = enrTargetAdmissionKindBooleanMap.get(kind);
            wrappers.add(new TargetAdmissionKindDataWrapper(kind, enrTargetAdmissionKindBooleanMap.keySet().contains(kind), stateInterest != null ? stateInterest : false, externalOrgUnitMap.get(kind)));
        }

        return ListOutputBuilder.get(input, wrappers).pageable(false).build();
    }
}
