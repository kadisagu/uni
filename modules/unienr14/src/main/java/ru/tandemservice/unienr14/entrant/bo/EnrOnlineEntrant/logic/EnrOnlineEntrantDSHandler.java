/* $Id$ */
package ru.tandemservice.unienr14.entrant.bo.EnrOnlineEntrant.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unienr14.entrant.entity.EnrOnlineEntrant;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.Collections;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
/**
 * @author nvankov
 * @since 5/19/14
 */
public class EnrOnlineEntrantDSHandler extends DefaultSearchDataSourceHandler
{
    public EnrOnlineEntrantDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        EnrEnrollmentCampaign enrollmentCampaign = context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);

        if (enrollmentCampaign == null) return ListOutputBuilder.get(input, Collections.emptyList()).pageable(true).build();

        String lastName = context.get("lastNameFilter");
        String firstName = context.get("firstNameFilter");
        String middleName = context.get("middleNameFilter");
        Number personalNumber = context.get("personalNumberFilter");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrOnlineEntrant.class, "e");
        builder.column("e");
        builder.fetchPath(DQLJoinType.inner, EnrOnlineEntrant.identityCard().fromAlias("e"), "ic");
        builder.fetchPath(DQLJoinType.left, EnrOnlineEntrant.entrant().fromAlias("e"), "ent");

        builder.where(eq(property("e", EnrOnlineEntrant.enrEnrollmentCampaign().id()), value(enrollmentCampaign)));

        if(!StringUtils.isEmpty(lastName))
            builder.where(likeUpper(property("e", EnrOnlineEntrant.identityCard().lastName()), value(CoreStringUtils.escapeLike(lastName, true))));
        if(!StringUtils.isEmpty(firstName))
            builder.where(likeUpper(property("e", EnrOnlineEntrant.identityCard().firstName()), value(CoreStringUtils.escapeLike(firstName, true))));
        if(!StringUtils.isEmpty(middleName))
            builder.where(likeUpper(property("e", EnrOnlineEntrant.identityCard().middleName()), value(CoreStringUtils.escapeLike(middleName, true))));
        if(personalNumber != null)
            builder.where(eq(property("e", EnrOnlineEntrant.personalNumber()), value(personalNumber.intValue())));

        if("fio".equals(input.getEntityOrder().getColumnName()))
        {
            builder.order(property("e", EnrOnlineEntrant.identityCard().lastName()), input.getEntityOrder().getDirection());
            builder.order(property("e", EnrOnlineEntrant.identityCard().firstName()), input.getEntityOrder().getDirection());
            builder.order(property("e", EnrOnlineEntrant.identityCard().middleName()), input.getEntityOrder().getDirection());
        }

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).order().pageable(true).build();
    }
}
