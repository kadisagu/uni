/* $Id$ */
package ru.tandemservice.unienr14.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.ISQLTranslator;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLUpdateQuery;

/**
 * @author Ekaterina Zvereva
 * @since 14.07.2016
 */
public class MS_unienr14_2x10x4_4to5 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.10.4")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ISQLTranslator translator = tool.getDialect().getSQLTranslator();

        SQLUpdateQuery updateQuery = new SQLUpdateQuery("enr14_camp_entrant_doc_t", "ced");
        updateQuery.getUpdatedTableFrom().innerJoin(SQLFrom.table("enr14_c_entrant_doc_type_t", "dt"), "ced.documenttype_id=dt.id");
        updateQuery.where("dt.code_p='2016_11'");  // EnrEntrantDocumentTypeCodes  /** Документы, подтверждающие принадлежность к соотечественникам */ String BELONGING_NATION = "2016_11"
        updateQuery.set("achievement_p", "?");

        tool.executeUpdate(translator.toSql(updateQuery), false);
    }
}