/* $Id: SecLocalRoleAccessMatrixEditUI.java 6520 2015-05-18 12:42:03Z oleyba $ */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.ui.AccessMatrixEdit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.sec.entity.Role;
import org.tandemframework.shared.organization.sec.bo.Sec.ui.AccessMatrixEdit.SecAccessMatrixEdit;
import org.tandemframework.shared.organization.sec.bo.Sec.ui.AccessMatrixEdit.SecAccessMatrixEditUI;
import ru.tandemservice.unienr14.sec.entity.RoleConfigLocalEnrCommission;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 10/6/14
 */
@State({
    @Bind(key = EnrEnrollmentCommissionAccessMatrixEditUI.BIND_ENROLLMENT_COMMISSION_ID, binding = EnrEnrollmentCommissionAccessMatrixEditUI.BIND_ENROLLMENT_COMMISSION_ID, required = true),
    @Bind(key = SecAccessMatrixEditUI.BIND_GROUP_NAME, binding = SecAccessMatrixEditUI.BIND_GROUP_NAME, required = true),
    @Bind(key = SecAccessMatrixEditUI.BIND_PERMISSION_KEY, binding = SecAccessMatrixEditUI.BIND_PERMISSION_KEY)
})
public class EnrEnrollmentCommissionAccessMatrixEditUI extends UIPresenter
{
    public static final String ACCESS_MATRIX_EDIT_REGION = "accessMatrixEditRegion";
    public static final String BIND_ENROLLMENT_COMMISSION_ID = "enrollmentCommissionId";

    private String groupName;
    private Long enrollmentCommissionId;
    private String permissionKey;
    private boolean rolesEmpty;

    @Override
    public void onComponentRefresh()
    {
        List<Role> roles = DataAccessServices.dao().getList(new DQLSelectBuilder()
            .fromEntity(RoleConfigLocalEnrCommission.class, "c")
            .column(property("c", RoleConfigLocalEnrCommission.role()))
            .where(eq(property(RoleConfigLocalEnrCommission.role().active().fromAlias("c")), value(Boolean.TRUE)))
            .where(eq(property(RoleConfigLocalEnrCommission.enrollmentCommission().id().fromAlias("c")), value(getEnrollmentCommissionId())))
            .order(property(RoleConfigLocalEnrCommission.role().title().fromAlias("c"))));
        
        rolesEmpty = roles.isEmpty();

        _uiActivation.asRegion(SecAccessMatrixEdit.class, ACCESS_MATRIX_EDIT_REGION)
            .parameter(SecAccessMatrixEditUI.BIND_ROLES, roles)
            .parameter(SecAccessMatrixEditUI.BIND_GROUP_NAME, groupName)
            .parameter(SecAccessMatrixEditUI.BIND_PERMISSION_KEY, permissionKey)
            .activate();
    }

    public void onClickApply()
    {
        SecAccessMatrixEditUI presenter = getMatrixEditPresenter();
        presenter.onClickApply();
    }

    public void onClickSave()
    {
        SecAccessMatrixEditUI presenter = getMatrixEditPresenter();
        presenter.onClickSave();
        deactivate();
    }

    private SecAccessMatrixEditUI getMatrixEditPresenter()
    {
        IUIPresenter presenter = _uiSupport.getChildUI(ACCESS_MATRIX_EDIT_REGION);
        if(presenter != null && presenter instanceof SecAccessMatrixEditUI)
            return (SecAccessMatrixEditUI) presenter;
        else
            throw new RuntimeException();
    }
    
    // getters and setters
    
    public String getGroupName()
    {
        return groupName;
    }

    public void setGroupName(String groupName)
    {
        this.groupName = groupName;
    }

    public Long getEnrollmentCommissionId()
    {
        return enrollmentCommissionId;
    }

    public void setEnrollmentCommissionId(Long enrollmentCommissionId)
    {
        this.enrollmentCommissionId = enrollmentCommissionId;
    }

    public String getPermissionKey()
    {
        return permissionKey;
    }

    public void setPermissionKey(String permissionKey)
    {
        this.permissionKey = permissionKey;
    }

    public boolean isRolesEmpty()
    {
        return rolesEmpty;
    }
}
