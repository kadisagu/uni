package ru.tandemservice.unienr14.entrant.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.person.catalog.entity.PersonDocumentType;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.IEnrEntrantAchievementProofDocument;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import ru.tandemservice.unienr14.entrant.entity.IEnrEntrantAchievementProofDocument;

/**
 * Документ, подтверждающий индивидуальное достижение
 *
 * Интерфейс для документов, которые могут подтверждать индивидуальное достижение.
 */
@SuppressWarnings({"all", "unchecked", "unused"})
public abstract class IEnrEntrantAchievementProofDocumentGen extends InterfaceStubBase
 implements IEnrEntrantAchievementProofDocument{
    public static final int VERSION_HASH = -1670509425;

    public static final String L_ENTRANT = "entrant";
    public static final String P_SERIA = "seria";
    public static final String P_NUMBER = "number";
    public static final String L_DOCUMENT_TYPE = "documentType";

    private EnrEntrant _entrant;
    private String _seria;
    private String _number;
    private PersonDocumentType _documentType;


    public EnrEntrant getEntrant()
    {
        return _entrant;
    }

    public void setEntrant(EnrEntrant entrant)
    {
        _entrant = entrant;
    }

    @Length(max=255)

    public String getSeria()
    {
        return _seria;
    }

    public void setSeria(String seria)
    {
        _seria = seria;
    }

    @Length(max=255)

    public String getNumber()
    {
        return _number;
    }

    public void setNumber(String number)
    {
        _number = number;
    }


    public PersonDocumentType getDocumentType()
    {
        return _documentType;
    }

    public void setDocumentType(PersonDocumentType documentType)
    {
        _documentType = documentType;
    }

    private static final Path<IEnrEntrantAchievementProofDocument> _dslPath = new Path<IEnrEntrantAchievementProofDocument>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ru.tandemservice.unienr14.entrant.entity.IEnrEntrantAchievementProofDocument");
    }
            

    /**
     * @return Абитуриент.
     * @see ru.tandemservice.unienr14.entrant.entity.IEnrEntrantAchievementProofDocument#getEntrant()
     */
    public static EnrEntrant.Path<EnrEntrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return Серия.
     * @see ru.tandemservice.unienr14.entrant.entity.IEnrEntrantAchievementProofDocument#getSeria()
     */
    public static PropertyPath<String> seria()
    {
        return _dslPath.seria();
    }

    /**
     * @return Номер.
     * @see ru.tandemservice.unienr14.entrant.entity.IEnrEntrantAchievementProofDocument#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Тип документа.
     * @see ru.tandemservice.unienr14.entrant.entity.IEnrEntrantAchievementProofDocument#getDocumentType()
     */
    public static PersonDocumentType.Path<PersonDocumentType> documentType()
    {
        return _dslPath.documentType();
    }

    public static class Path<E extends IEnrEntrantAchievementProofDocument> extends EntityPath<E>
    {
        private EnrEntrant.Path<EnrEntrant> _entrant;
        private PropertyPath<String> _seria;
        private PropertyPath<String> _number;
        private PersonDocumentType.Path<PersonDocumentType> _documentType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Абитуриент.
     * @see ru.tandemservice.unienr14.entrant.entity.IEnrEntrantAchievementProofDocument#getEntrant()
     */
        public EnrEntrant.Path<EnrEntrant> entrant()
        {
            if(_entrant == null )
                _entrant = new EnrEntrant.Path<EnrEntrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return Серия.
     * @see ru.tandemservice.unienr14.entrant.entity.IEnrEntrantAchievementProofDocument#getSeria()
     */
        public PropertyPath<String> seria()
        {
            if(_seria == null )
                _seria = new PropertyPath<String>(IEnrEntrantAchievementProofDocumentGen.P_SERIA, this);
            return _seria;
        }

    /**
     * @return Номер.
     * @see ru.tandemservice.unienr14.entrant.entity.IEnrEntrantAchievementProofDocument#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(IEnrEntrantAchievementProofDocumentGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Тип документа.
     * @see ru.tandemservice.unienr14.entrant.entity.IEnrEntrantAchievementProofDocument#getDocumentType()
     */
        public PersonDocumentType.Path<PersonDocumentType> documentType()
        {
            if(_documentType == null )
                _documentType = new PersonDocumentType.Path<PersonDocumentType>(L_DOCUMENT_TYPE, this);
            return _documentType;
        }

        public Class getEntityClass()
        {
            return IEnrEntrantAchievementProofDocument.class;
        }

        public String getEntityName()
        {
            return "ru.tandemservice.unienr14.entrant.entity.IEnrEntrantAchievementProofDocument";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
