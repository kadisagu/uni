package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLUpdateQuery;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unienr14_2x10x2_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		// создание обязательное свойство printTitle
		if (!tool.columnExists("enr14_program_set_base_t", "printtitle_p")) {
			// создать колонку
			tool.createColumn("enr14_program_set_base_t", new DBColumn("printtitle_p", DBType.createVarchar(255)));

			//заменяем печатный тайтл на тайтл из направления подготовки
			SQLUpdateQuery updateQuery = new SQLUpdateQuery("enr14_program_set_base_t", "base");
			updateQuery.getUpdatedTableFrom().innerJoin(SQLFrom.table("edu_c_pr_subject_t", "subject"), "base.programsubject_id = subject.id");
			updateQuery.set("printtitle_p", tool.createStringConcatenationSQL("subject.subjectcode_p", "' '", "subject.title_p"));

			tool.executeUpdate(tool.getDialect().getSQLTranslator().toSql(updateQuery));

			// сделать колонку NOT NULL
			tool.setColumnNullable("enr14_program_set_base_t", "printtitle_p", false);
		}
	}
}