/**
 *$Id: EnrExamRoomDao.java 34429 2014-05-26 10:13:50Z oleyba $
 */
package ru.tandemservice.unienr14.exams.bo.EnrExamRoom.logic;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignExamRoom;

/**
 * @author Alexander Shaburov
 * @since 21.05.13
 */
public class EnrExamRoomDao extends UniBaseDao implements IEnrExamRoomDao
{
    @Override
    public Long saveOrUpdate(EnrCampaignExamRoom examRoom)
    {
        getSession().saveOrUpdate(examRoom);

        return examRoom.getId();
    }
}
