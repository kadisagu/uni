/* $Id$ */
package ru.tandemservice.unienr14.ws.onlineentrant;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.util.cache.SpringBeanCache;

import java.util.List;

/**
 * @author nvankov
 * @since 5/8/14
 */
public interface IEnrOnlineEntrantServiceDao extends INeedPersistenceSupport
{
    public static final SpringBeanCache<IEnrOnlineEntrantServiceDao> INSTANCE = new SpringBeanCache<IEnrOnlineEntrantServiceDao>(IEnrOnlineEntrantServiceDao.class.getName());

    CatalogsData getCatalogs(String enrollmentCampaignTitle);

    void createOrUpdateOnlineEntrant(OnlineEntrantData onlineEntrant);

    OnlineEntrantData getOnlineEntrant(Long onlineEntrantId, boolean isFullData);

    boolean isOnlineEntrantExist(Long onlineEntrantId);

    public boolean isOnlineEntrantStatementAccepted(Long onlineEntrantId);

    String getEntrantId(Long onlineEntrantId);

    List<String> getOnlineEntrantCompetitionsUids(Long onlineEntrantId);
}
