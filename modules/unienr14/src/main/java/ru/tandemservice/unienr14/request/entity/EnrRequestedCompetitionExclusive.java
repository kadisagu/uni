package ru.tandemservice.unienr14.request.entity;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;

import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.request.entity.gen.EnrRequestedCompetitionExclusiveGen;

/**
 * Выбранный конкурс по квоте
 */
public class EnrRequestedCompetitionExclusive extends EnrRequestedCompetitionExclusiveGen
{
    public EnrRequestedCompetitionExclusive()
    {
    }

    public EnrRequestedCompetitionExclusive(EnrEntrantRequest request, EnrCompetition competition)
    {
        setRequest(request);
        setCompetition(competition);
    }

    @Override
    @EntityDSLSupport
    public String getParametersTitle() {
        return "Ос. право: " + getBenefitCategory().getShortTitle();
    }

    @Override
    public String getBenefitStatementTitle()
    {
        return "Конкурс: " + getTitle();
    }
}