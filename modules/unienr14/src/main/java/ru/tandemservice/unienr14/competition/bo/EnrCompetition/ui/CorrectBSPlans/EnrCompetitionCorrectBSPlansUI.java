package ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.CorrectBSPlans;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.entity.EntityHolder;

import ru.tandemservice.unienr14.competition.bo.EnrCompetition.EnrCompetitionManager;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.logic.IEnrCompetitionDao;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

@Input({
    @Bind(key=UIPresenter.PUBLISHER_ID, binding="ecHolder.id", required=true),
    @Bind(key=EnrCompetitionCorrectBSPlansUI.BIND_PS_IDS, binding="programSetIds", required = true)
})
public class EnrCompetitionCorrectBSPlansUI extends UIPresenter
{
    public static final String BIND_PS_IDS = "programSetIds";

    private EntityHolder<EnrEnrollmentCampaign> ecHolder = new EntityHolder<>();
    public EntityHolder<EnrEnrollmentCampaign> getEcHolder() { return this.ecHolder; }
    public EnrEnrollmentCampaign getEc() { return getEcHolder().getValue(); }

    private final IdentifiableWrapper<IEntity> BY_PASSED = new IdentifiableWrapper<IEntity>(1L, "Распределить места между конкурсами в зависимости от уровня образования поступающих пропорционально числу абитуриентов, успешно сдавших ВИ");
    private final IdentifiableWrapper<IEntity> BY_PASSED_AND_FREE = new IdentifiableWrapper<IEntity>(2L, "Перенести вакантные места в пределах квот особых прав и целевого приема в общий конкурс, распределить места между конкурсами в зависимости от уровня образования поступающих пропорционально числу абитуриентов, успешно сдавших ВИ");
    private final IdentifiableWrapper<IEntity> EQUAL_PARTS = new IdentifiableWrapper<IEntity>(3L, "Распределить места между конкурсами в зависимости от уровня образования поступающих в равных долях");

    private final List<IdentifiableWrapper<IEntity>> paramList = Arrays.asList(EQUAL_PARTS, BY_PASSED, BY_PASSED_AND_FREE);
    public List<IdentifiableWrapper<IEntity>> getParamList() { return this.paramList; }

    private IdentifiableWrapper<IEntity> param;
    public IdentifiableWrapper<IEntity> getParam() { return this.param; }
    public void setParam(IdentifiableWrapper<IEntity> param) { this.param = param; }

    private Collection<Long> programSetIds;
    private boolean _updatePlansSetManually = false;


    @Override
    public void onComponentRefresh() {
        getEcHolder().refresh();
        getEc().checkOpen();
        setParam(EQUAL_PARTS);
    }

    public void onClickApply()
    {
        if(getUserContext().getErrorCollector().hasErrors())
            return;

        IEnrCompetitionDao.PlanCalculationRule rule;

        if (EQUAL_PARTS.equals(getParam())) {
            rule = IEnrCompetitionDao.PlanCalculationRule.EQUAL_PARTS;
        } else if (BY_PASSED.equals(getParam())) {
            rule = IEnrCompetitionDao.PlanCalculationRule.BY_PASSED;
        } else if (BY_PASSED_AND_FREE.equals(getParam())) {
            rule = IEnrCompetitionDao.PlanCalculationRule.BY_PASSED_AND_FREE;
        } else {
            throw new IllegalArgumentException();
        }

        EnrCompetitionManager.instance().dao().doCalculateProgramSetPlans(getEc(), rule, programSetIds, _updatePlansSetManually);

        deactivate();
    }

    // getters and setters

    public boolean isUpdatePlansSetManually()
    {
        return _updatePlansSetManually;
    }

    public void setUpdatePlansSetManually(boolean updatePlansSetManually)
    {
        _updatePlansSetManually = updatePlansSetManually;
    }

    public Collection<Long> getProgramSetIds()
    {
        return programSetIds;
    }

    public void setProgramSetIds(Collection<Long> programSetIds)
    {
        this.programSetIds = programSetIds;
    }
}
