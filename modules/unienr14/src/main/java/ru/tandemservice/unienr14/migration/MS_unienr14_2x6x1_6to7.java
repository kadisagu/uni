package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x1_6to7 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
        {
            new ScriptDependency("org.tandemframework", "1.6.15"),
            new ScriptDependency("org.tandemframework.shared", "1.6.1")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrRatingItem

        // создано обязательное свойство achievementMarkAsLong
        {
            // создать колонку
            tool.createColumn("enr14_rating_item_t", new DBColumn("achievementmarkaslong_p", DBType.LONG));

            // задать значение по умолчанию
            tool.executeUpdate("update enr14_rating_item_t set achievementmarkaslong_p=? where achievementmarkaslong_p is null", 0l);
            tool.executeUpdate("update enr14_rating_item_t set valueHash_p=?", 0l); // сбрасываем хэш

            // сделать колонку NOT NULL
            tool.setColumnNullable("enr14_rating_item_t", "achievementmarkaslong_p", false);


        }


    }
}