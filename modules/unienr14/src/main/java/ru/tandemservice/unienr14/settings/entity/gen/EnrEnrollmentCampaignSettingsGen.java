package ru.tandemservice.unienr14.settings.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCampaignType;
import ru.tandemservice.unienr14.catalog.entity.EnrLevelBudgetFinancing;
import ru.tandemservice.unienr14.catalog.entity.EnrTieBreakRatingRule;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Настройки приемной кампании
 *
 * Хранит все настройки приемной кампании.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrEnrollmentCampaignSettingsGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings";
    public static final String ENTITY_NAME = "enrEnrollmentCampaignSettings";
    public static final int VERSION_HASH = -616963264;
    private static IEntityMeta ENTITY_META;

    public static final String P_REENROLL_BY_PRIORITY_ENABLED = "reenrollByPriorityEnabled";
    public static final String P_STATE_EXAM_RESTRICTION = "stateExamRestriction";
    public static final String P_NUMBER_ON_REQUEST_MANUAL = "numberOnRequestManual";
    public static final String P_STATE_EXAM_AUTO_CHECK = "stateExamAutoCheck";
    public static final String P_EXAM_GROUP_TITLE_FORMING_MANUAL = "examGroupTitleFormingManual";
    public static final String P_STATE_EXAM_ACCEPTED = "stateExamAccepted";
    public static final String P_ACCEPTED_CONTRACT_DEFAULT = "acceptedContractDefault";
    public static final String P_TARGET_ADMISSION_COMPETITION = "targetAdmissionCompetition";
    public static final String P_EXAM_INTERVAL_AUTO = "examIntervalAuto";
    public static final String P_EXAM_INTERVAL_MANUAL = "examIntervalManual";
    public static final String P_APPEAL_DEADLINE_DAYS = "appealDeadlineDays";
    public static final String P_APPEAL_HEARING_DAYS = "appealHearingDays";
    public static final String P_ENR_ENTRANT_CONTRACT_EXIST_CHECK_MODE = "enrEntrantContractExistCheckMode";
    public static final String P_ACCOUNTING_RULES_ORIG_EDU_DOC = "accountingRulesOrigEduDoc";
    public static final String P_ENROLLMENT_RULES_BUDGET = "enrollmentRulesBudget";
    public static final String P_ACCOUNTING_RULES_AGREEMENT_BUDGET = "accountingRulesAgreementBudget";
    public static final String P_ENROLLMENT_RULES_CONTRACT = "enrollmentRulesContract";
    public static final String P_ACCOUNTING_RULES_AGREEMENT_CONTRACT = "accountingRulesAgreementContract";
    public static final String P_ENR_ENTRANT_EXAM_LIST_PRINT_TYPE = "enrEntrantExamListPrintType";
    public static final String P_EXCLUDE_ENROLLED_BY_LOW_PRIORITY = "excludeEnrolledByLowPriority";
    public static final String L_TIE_BREAK_RATING_RULE = "tieBreakRatingRule";
    public static final String L_LEVEL_BUDGET_FINANCING = "levelBudgetFinancing";
    public static final String P_ACCEPT_PEOPLE_RESIDING_IN_CRIMEA = "acceptPeopleResidingInCrimea";
    public static final String L_ENROLLMENT_CAMPAIGN_TYPE = "enrollmentCampaignType";

    private boolean _reenrollByPriorityEnabled = true;     // Перезачислять на лучший приоритет
    private Boolean _stateExamRestriction;     // Ограничение выбора конкурсов в заявлении по данным свидетельств ЕГЭ (покрытие набора ВИ баллами по предметам ЕГЭ)
    private boolean _numberOnRequestManual;     // Присваивать регистрационный номер заявления вручную
    private boolean _stateExamAutoCheck;     // Регистрировать свидетельства ЕГЭ проверенными
    private boolean _examGroupTitleFormingManual;     // Формировать названия экзам. групп вручную
    private boolean _stateExamAccepted;     // Добавлять результаты ЕГЭ по умолчанию зачтенными
    private boolean _acceptedContractDefault = false;     // При выборе конкурса на договор абитуриент по умолчанию согласен на зачисление
    private boolean _targetAdmissionCompetition;     // Конкурс по целевому приему
    private int _examIntervalAuto = 1;     // Промежуток между ВИ при автомат. выборе ЭГ (дн.)
    private int _examIntervalManual = 1;     // Промежуток между ВИ при ручном выборе ЭГ (дн.)
    private int _appealDeadlineDays = 1;     // Срок подачи апелляции
    private int _appealHearingDays = 1;     // Срок рассмотрения апелляции
    private long _enrEntrantContractExistCheckMode = 0;     // Вариант проверки наличия договора при определении возможности зачисления по договору
    private long _accountingRulesOrigEduDoc = 0;     // Правила учета оригинала документа об образовании абитуриента
    private long _enrollmentRulesBudget = 1;     // Условия зачисления при поступлении на места в рамках КЦП
    private long _accountingRulesAgreementBudget = 0;     // Правила учета согласия на зачисление абитуриента при поступлении на места в рамках КЦП
    private long _enrollmentRulesContract = 1;     // Условия зачисления при поступлении на места по договору
    private long _accountingRulesAgreementContract = 1;     // Правила учета согласия на зачисление абитуриента при поступлении на места по договору
    private long _enrEntrantExamListPrintType;     // Тип печати экзаменационного листа
    private boolean _excludeEnrolledByLowPriority;     // Исключать зачисленных абитуриентов из конкурсов с более низким приоритетом
    private EnrTieBreakRatingRule _tieBreakRatingRule;     // Способ ранжирования абитуриентов при равенстве суммы конкурсных баллов (бакалавриат, специалитет, магистратура)
    private EnrLevelBudgetFinancing _levelBudgetFinancing;     // Уровни бюджетного финансирования
    private boolean _acceptPeopleResidingInCrimea = false;     // Прием лиц, постоянно проживающих в Крыму
    private EnrEnrollmentCampaignType _enrollmentCampaignType;     // Тип приемной кампании

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20686876
     * Влияет на разрешение конфликтов при формировании шага зачисления:
     * false - не разрешать перезачислять абитуриентов;
     * true - разрешать перезачислять абитуриентов
     *
     * @return Перезачислять на лучший приоритет. Свойство не может быть null.
     */
    @NotNull
    public boolean isReenrollByPriorityEnabled()
    {
        return _reenrollByPriorityEnabled;
    }

    /**
     * @param reenrollByPriorityEnabled Перезачислять на лучший приоритет. Свойство не может быть null.
     */
    public void setReenrollByPriorityEnabled(boolean reenrollByPriorityEnabled)
    {
        dirty(_reenrollByPriorityEnabled, reenrollByPriorityEnabled);
        _reenrollByPriorityEnabled = reenrollByPriorityEnabled;
    }

    /**
     * DEV-2891 DEV-2856
     * Влияет на поведение формы добавления заявления (добавление НП/КГ) абитуриента: отображение и значение по умолчанию для поля «Свидетельство ЕГЭ накладывает ограничения на выбор направлений/специальностей в заявлении»:
     * null - Не используется (поле не отображается);
     * false - Используется, по умолчанию выбор не ограничен (поле отображается, по умолчанию выключено);
     * true - Используется, по умолчанию выбор ограничен (поле отображается, по умолчанию включено)
     *
     * @return Ограничение выбора конкурсов в заявлении по данным свидетельств ЕГЭ (покрытие набора ВИ баллами по предметам ЕГЭ).
     */
    public Boolean getStateExamRestriction()
    {
        return _stateExamRestriction;
    }

    /**
     * @param stateExamRestriction Ограничение выбора конкурсов в заявлении по данным свидетельств ЕГЭ (покрытие набора ВИ баллами по предметам ЕГЭ).
     */
    public void setStateExamRestriction(Boolean stateExamRestriction)
    {
        dirty(_stateExamRestriction, stateExamRestriction);
        _stateExamRestriction = stateExamRestriction;
    }

    /**
     * DEV-2840
     * Влияет на поведение формы добавления заявления абитуриента: отображение поля «№ заявления»:
     * false - поле не отображается, номер присваивается инкрементом от максимального в базе в рамках ПК при сохранении заявления;
     * true - поле не отображается, заполняется инкрементом от максимального в базе в рамках ПК на момент открытия формы, при сохранении проверяется на уникальность в рамках ПК
     *
     * @return Присваивать регистрационный номер заявления вручную. Свойство не может быть null.
     */
    @NotNull
    public boolean isNumberOnRequestManual()
    {
        return _numberOnRequestManual;
    }

    /**
     * @param numberOnRequestManual Присваивать регистрационный номер заявления вручную. Свойство не может быть null.
     */
    public void setNumberOnRequestManual(boolean numberOnRequestManual)
    {
        dirty(_numberOnRequestManual, numberOnRequestManual);
        _numberOnRequestManual = numberOnRequestManual;
    }

    /**
     * DEV-2920
     * Влияет на поведение формы создания свидетельства ЕГЭ:
     * false - свидетельство создается не проверенным (признак проверено = true);
     * true - свидетельство создается проверенным (признак проверено = true)
     *
     * @return Регистрировать свидетельства ЕГЭ проверенными. Свойство не может быть null.
     */
    @NotNull
    public boolean isStateExamAutoCheck()
    {
        return _stateExamAutoCheck;
    }

    /**
     * @param stateExamAutoCheck Регистрировать свидетельства ЕГЭ проверенными. Свойство не может быть null.
     */
    public void setStateExamAutoCheck(boolean stateExamAutoCheck)
    {
        dirty(_stateExamAutoCheck, stateExamAutoCheck);
        _stateExamAutoCheck = stateExamAutoCheck;
    }

    /**
     * Влияет на поведение формы создания ЭГ:
     * true - название ЭГ задается вручную
     * false - названия ЭГ формируются системой автоматически: представляют из себя натуральные числа, начиная с 1, при добавлении ЭГ в качестве ее названия берется наибольшее число из названий уже созданных ЭГ в ПК +1
     *
     * @return Формировать названия экзам. групп вручную. Свойство не может быть null.
     */
    @NotNull
    public boolean isExamGroupTitleFormingManual()
    {
        return _examGroupTitleFormingManual;
    }

    /**
     * @param examGroupTitleFormingManual Формировать названия экзам. групп вручную. Свойство не может быть null.
     */
    public void setExamGroupTitleFormingManual(boolean examGroupTitleFormingManual)
    {
        dirty(_examGroupTitleFormingManual, examGroupTitleFormingManual);
        _examGroupTitleFormingManual = examGroupTitleFormingManual;
    }

    /**
     * Влияет на поведение формы создания результатов ЕГЭ:
     * true – результаты ЕГЭ по умолчанию добавляются зачтенными;
     * false – результаты ЕГЭ по умолчанию добавляются не зачтенными.
     *
     * @return Добавлять результаты ЕГЭ по умолчанию зачтенными. Свойство не может быть null.
     */
    @NotNull
    public boolean isStateExamAccepted()
    {
        return _stateExamAccepted;
    }

    /**
     * @param stateExamAccepted Добавлять результаты ЕГЭ по умолчанию зачтенными. Свойство не может быть null.
     */
    public void setStateExamAccepted(boolean stateExamAccepted)
    {
        dirty(_stateExamAccepted, stateExamAccepted);
        _stateExamAccepted = stateExamAccepted;
    }

    /**
     * @return При выборе конкурса на договор абитуриент по умолчанию согласен на зачисление. Свойство не может быть null.
     */
    @NotNull
    public boolean isAcceptedContractDefault()
    {
        return _acceptedContractDefault;
    }

    /**
     * @param acceptedContractDefault При выборе конкурса на договор абитуриент по умолчанию согласен на зачисление. Свойство не может быть null.
     */
    public void setAcceptedContractDefault(boolean acceptedContractDefault)
    {
        dirty(_acceptedContractDefault, acceptedContractDefault);
        _acceptedContractDefault = acceptedContractDefault;
    }

    /**
     * false - общий для всех видов целевого приема
     * true - отдельный для каждого вида целевого приема
     *
     * @return Конкурс по целевому приему. Свойство не может быть null.
     */
    @NotNull
    public boolean isTargetAdmissionCompetition()
    {
        return _targetAdmissionCompetition;
    }

    /**
     * @param targetAdmissionCompetition Конкурс по целевому приему. Свойство не может быть null.
     */
    public void setTargetAdmissionCompetition(boolean targetAdmissionCompetition)
    {
        dirty(_targetAdmissionCompetition, targetAdmissionCompetition);
        _targetAdmissionCompetition = targetAdmissionCompetition;
    }

    /**
     * @return Промежуток между ВИ при автомат. выборе ЭГ (дн.). Свойство не может быть null.
     */
    @NotNull
    public int getExamIntervalAuto()
    {
        return _examIntervalAuto;
    }

    /**
     * @param examIntervalAuto Промежуток между ВИ при автомат. выборе ЭГ (дн.). Свойство не может быть null.
     */
    public void setExamIntervalAuto(int examIntervalAuto)
    {
        dirty(_examIntervalAuto, examIntervalAuto);
        _examIntervalAuto = examIntervalAuto;
    }

    /**
     * @return Промежуток между ВИ при ручном выборе ЭГ (дн.). Свойство не может быть null.
     */
    @NotNull
    public int getExamIntervalManual()
    {
        return _examIntervalManual;
    }

    /**
     * @param examIntervalManual Промежуток между ВИ при ручном выборе ЭГ (дн.). Свойство не может быть null.
     */
    public void setExamIntervalManual(int examIntervalManual)
    {
        dirty(_examIntervalManual, examIntervalManual);
        _examIntervalManual = examIntervalManual;
    }

    /**
     * @return Срок подачи апелляции. Свойство не может быть null.
     */
    @NotNull
    public int getAppealDeadlineDays()
    {
        return _appealDeadlineDays;
    }

    /**
     * @param appealDeadlineDays Срок подачи апелляции. Свойство не может быть null.
     */
    public void setAppealDeadlineDays(int appealDeadlineDays)
    {
        dirty(_appealDeadlineDays, appealDeadlineDays);
        _appealDeadlineDays = appealDeadlineDays;
    }

    /**
     * @return Срок рассмотрения апелляции. Свойство не может быть null.
     */
    @NotNull
    public int getAppealHearingDays()
    {
        return _appealHearingDays;
    }

    /**
     * @param appealHearingDays Срок рассмотрения апелляции. Свойство не может быть null.
     */
    public void setAppealHearingDays(int appealHearingDays)
    {
        dirty(_appealHearingDays, appealHearingDays);
        _appealHearingDays = appealHearingDays;
    }

    /**
     * Используется, только если модуль unienr14_ctr подключен, соответствует коду проверки в EnrEntrantContractExistsRule (unienr14_ctr).
     * 0 - означает, что проверка не используется (наличие договора не требуется).
     *
     * @return Вариант проверки наличия договора при определении возможности зачисления по договору. Свойство не может быть null.
     */
    @NotNull
    public long getEnrEntrantContractExistCheckMode()
    {
        return _enrEntrantContractExistCheckMode;
    }

    /**
     * @param enrEntrantContractExistCheckMode Вариант проверки наличия договора при определении возможности зачисления по договору. Свойство не может быть null.
     */
    public void setEnrEntrantContractExistCheckMode(long enrEntrantContractExistCheckMode)
    {
        dirty(_enrEntrantContractExistCheckMode, enrEntrantContractExistCheckMode);
        _enrEntrantContractExistCheckMode = enrEntrantContractExistCheckMode;
    }

    /**
     * TODO:
     *
     * @return Правила учета оригинала документа об образовании абитуриента. Свойство не может быть null.
     */
    @NotNull
    public long getAccountingRulesOrigEduDoc()
    {
        return _accountingRulesOrigEduDoc;
    }

    /**
     * @param accountingRulesOrigEduDoc Правила учета оригинала документа об образовании абитуриента. Свойство не может быть null.
     */
    public void setAccountingRulesOrigEduDoc(long accountingRulesOrigEduDoc)
    {
        dirty(_accountingRulesOrigEduDoc, accountingRulesOrigEduDoc);
        _accountingRulesOrigEduDoc = accountingRulesOrigEduDoc;
    }

    /**
     * TODO:
     *
     * @return Условия зачисления при поступлении на места в рамках КЦП. Свойство не может быть null.
     */
    @NotNull
    public long getEnrollmentRulesBudget()
    {
        return _enrollmentRulesBudget;
    }

    /**
     * @param enrollmentRulesBudget Условия зачисления при поступлении на места в рамках КЦП. Свойство не может быть null.
     */
    public void setEnrollmentRulesBudget(long enrollmentRulesBudget)
    {
        dirty(_enrollmentRulesBudget, enrollmentRulesBudget);
        _enrollmentRulesBudget = enrollmentRulesBudget;
    }

    /**
     * TODO:
     *
     * @return Правила учета согласия на зачисление абитуриента при поступлении на места в рамках КЦП. Свойство не может быть null.
     */
    @NotNull
    public long getAccountingRulesAgreementBudget()
    {
        return _accountingRulesAgreementBudget;
    }

    /**
     * @param accountingRulesAgreementBudget Правила учета согласия на зачисление абитуриента при поступлении на места в рамках КЦП. Свойство не может быть null.
     */
    public void setAccountingRulesAgreementBudget(long accountingRulesAgreementBudget)
    {
        dirty(_accountingRulesAgreementBudget, accountingRulesAgreementBudget);
        _accountingRulesAgreementBudget = accountingRulesAgreementBudget;
    }

    /**
     * TODO:
     *
     * @return Условия зачисления при поступлении на места по договору. Свойство не может быть null.
     */
    @NotNull
    public long getEnrollmentRulesContract()
    {
        return _enrollmentRulesContract;
    }

    /**
     * @param enrollmentRulesContract Условия зачисления при поступлении на места по договору. Свойство не может быть null.
     */
    public void setEnrollmentRulesContract(long enrollmentRulesContract)
    {
        dirty(_enrollmentRulesContract, enrollmentRulesContract);
        _enrollmentRulesContract = enrollmentRulesContract;
    }

    /**
     * TODO:
     *
     * @return Правила учета согласия на зачисление абитуриента при поступлении на места по договору. Свойство не может быть null.
     */
    @NotNull
    public long getAccountingRulesAgreementContract()
    {
        return _accountingRulesAgreementContract;
    }

    /**
     * @param accountingRulesAgreementContract Правила учета согласия на зачисление абитуриента при поступлении на места по договору. Свойство не может быть null.
     */
    public void setAccountingRulesAgreementContract(long accountingRulesAgreementContract)
    {
        dirty(_accountingRulesAgreementContract, accountingRulesAgreementContract);
        _accountingRulesAgreementContract = accountingRulesAgreementContract;
    }

    /**
     * DEV-5450:
     * 1 - Выводить все вступительные испытания;
     * 2 - Выводить только внутренние вступительные испытания.
     *
     * @return Тип печати экзаменационного листа. Свойство не может быть null.
     */
    @NotNull
    public long getEnrEntrantExamListPrintType()
    {
        return _enrEntrantExamListPrintType;
    }

    /**
     * @param enrEntrantExamListPrintType Тип печати экзаменационного листа. Свойство не может быть null.
     */
    public void setEnrEntrantExamListPrintType(long enrEntrantExamListPrintType)
    {
        dirty(_enrEntrantExamListPrintType, enrEntrantExamListPrintType);
        _enrEntrantExamListPrintType = enrEntrantExamListPrintType;
    }

    /**
     * @return Исключать зачисленных абитуриентов из конкурсов с более низким приоритетом. Свойство не может быть null.
     */
    @NotNull
    public boolean isExcludeEnrolledByLowPriority()
    {
        return _excludeEnrolledByLowPriority;
    }

    /**
     * @param excludeEnrolledByLowPriority Исключать зачисленных абитуриентов из конкурсов с более низким приоритетом. Свойство не может быть null.
     */
    public void setExcludeEnrolledByLowPriority(boolean excludeEnrolledByLowPriority)
    {
        dirty(_excludeEnrolledByLowPriority, excludeEnrolledByLowPriority);
        _excludeEnrolledByLowPriority = excludeEnrolledByLowPriority;
    }

    /**
     * @return Способ ранжирования абитуриентов при равенстве суммы конкурсных баллов (бакалавриат, специалитет, магистратура). Свойство не может быть null.
     */
    @NotNull
    public EnrTieBreakRatingRule getTieBreakRatingRule()
    {
        return _tieBreakRatingRule;
    }

    /**
     * @param tieBreakRatingRule Способ ранжирования абитуриентов при равенстве суммы конкурсных баллов (бакалавриат, специалитет, магистратура). Свойство не может быть null.
     */
    public void setTieBreakRatingRule(EnrTieBreakRatingRule tieBreakRatingRule)
    {
        dirty(_tieBreakRatingRule, tieBreakRatingRule);
        _tieBreakRatingRule = tieBreakRatingRule;
    }

    /**
     * @return Уровни бюджетного финансирования. Свойство не может быть null.
     */
    @NotNull
    public EnrLevelBudgetFinancing getLevelBudgetFinancing()
    {
        return _levelBudgetFinancing;
    }

    /**
     * @param levelBudgetFinancing Уровни бюджетного финансирования. Свойство не может быть null.
     */
    public void setLevelBudgetFinancing(EnrLevelBudgetFinancing levelBudgetFinancing)
    {
        dirty(_levelBudgetFinancing, levelBudgetFinancing);
        _levelBudgetFinancing = levelBudgetFinancing;
    }

    /**
     * @return Прием лиц, постоянно проживающих в Крыму. Свойство не может быть null.
     */
    @NotNull
    public boolean isAcceptPeopleResidingInCrimea()
    {
        return _acceptPeopleResidingInCrimea;
    }

    /**
     * @param acceptPeopleResidingInCrimea Прием лиц, постоянно проживающих в Крыму. Свойство не может быть null.
     */
    public void setAcceptPeopleResidingInCrimea(boolean acceptPeopleResidingInCrimea)
    {
        dirty(_acceptPeopleResidingInCrimea, acceptPeopleResidingInCrimea);
        _acceptPeopleResidingInCrimea = acceptPeopleResidingInCrimea;
    }

    /**
     * @return Тип приемной кампании.
     */
    public EnrEnrollmentCampaignType getEnrollmentCampaignType()
    {
        return _enrollmentCampaignType;
    }

    /**
     * @param enrollmentCampaignType Тип приемной кампании.
     */
    public void setEnrollmentCampaignType(EnrEnrollmentCampaignType enrollmentCampaignType)
    {
        dirty(_enrollmentCampaignType, enrollmentCampaignType);
        _enrollmentCampaignType = enrollmentCampaignType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrEnrollmentCampaignSettingsGen)
        {
            setReenrollByPriorityEnabled(((EnrEnrollmentCampaignSettings)another).isReenrollByPriorityEnabled());
            setStateExamRestriction(((EnrEnrollmentCampaignSettings)another).getStateExamRestriction());
            setNumberOnRequestManual(((EnrEnrollmentCampaignSettings)another).isNumberOnRequestManual());
            setStateExamAutoCheck(((EnrEnrollmentCampaignSettings)another).isStateExamAutoCheck());
            setExamGroupTitleFormingManual(((EnrEnrollmentCampaignSettings)another).isExamGroupTitleFormingManual());
            setStateExamAccepted(((EnrEnrollmentCampaignSettings)another).isStateExamAccepted());
            setAcceptedContractDefault(((EnrEnrollmentCampaignSettings)another).isAcceptedContractDefault());
            setTargetAdmissionCompetition(((EnrEnrollmentCampaignSettings)another).isTargetAdmissionCompetition());
            setExamIntervalAuto(((EnrEnrollmentCampaignSettings)another).getExamIntervalAuto());
            setExamIntervalManual(((EnrEnrollmentCampaignSettings)another).getExamIntervalManual());
            setAppealDeadlineDays(((EnrEnrollmentCampaignSettings)another).getAppealDeadlineDays());
            setAppealHearingDays(((EnrEnrollmentCampaignSettings)another).getAppealHearingDays());
            setEnrEntrantContractExistCheckMode(((EnrEnrollmentCampaignSettings)another).getEnrEntrantContractExistCheckMode());
            setAccountingRulesOrigEduDoc(((EnrEnrollmentCampaignSettings)another).getAccountingRulesOrigEduDoc());
            setEnrollmentRulesBudget(((EnrEnrollmentCampaignSettings)another).getEnrollmentRulesBudget());
            setAccountingRulesAgreementBudget(((EnrEnrollmentCampaignSettings)another).getAccountingRulesAgreementBudget());
            setEnrollmentRulesContract(((EnrEnrollmentCampaignSettings)another).getEnrollmentRulesContract());
            setAccountingRulesAgreementContract(((EnrEnrollmentCampaignSettings)another).getAccountingRulesAgreementContract());
            setEnrEntrantExamListPrintType(((EnrEnrollmentCampaignSettings)another).getEnrEntrantExamListPrintType());
            setExcludeEnrolledByLowPriority(((EnrEnrollmentCampaignSettings)another).isExcludeEnrolledByLowPriority());
            setTieBreakRatingRule(((EnrEnrollmentCampaignSettings)another).getTieBreakRatingRule());
            setLevelBudgetFinancing(((EnrEnrollmentCampaignSettings)another).getLevelBudgetFinancing());
            setAcceptPeopleResidingInCrimea(((EnrEnrollmentCampaignSettings)another).isAcceptPeopleResidingInCrimea());
            setEnrollmentCampaignType(((EnrEnrollmentCampaignSettings)another).getEnrollmentCampaignType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrEnrollmentCampaignSettingsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrEnrollmentCampaignSettings.class;
        }

        public T newInstance()
        {
            return (T) new EnrEnrollmentCampaignSettings();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "reenrollByPriorityEnabled":
                    return obj.isReenrollByPriorityEnabled();
                case "stateExamRestriction":
                    return obj.getStateExamRestriction();
                case "numberOnRequestManual":
                    return obj.isNumberOnRequestManual();
                case "stateExamAutoCheck":
                    return obj.isStateExamAutoCheck();
                case "examGroupTitleFormingManual":
                    return obj.isExamGroupTitleFormingManual();
                case "stateExamAccepted":
                    return obj.isStateExamAccepted();
                case "acceptedContractDefault":
                    return obj.isAcceptedContractDefault();
                case "targetAdmissionCompetition":
                    return obj.isTargetAdmissionCompetition();
                case "examIntervalAuto":
                    return obj.getExamIntervalAuto();
                case "examIntervalManual":
                    return obj.getExamIntervalManual();
                case "appealDeadlineDays":
                    return obj.getAppealDeadlineDays();
                case "appealHearingDays":
                    return obj.getAppealHearingDays();
                case "enrEntrantContractExistCheckMode":
                    return obj.getEnrEntrantContractExistCheckMode();
                case "accountingRulesOrigEduDoc":
                    return obj.getAccountingRulesOrigEduDoc();
                case "enrollmentRulesBudget":
                    return obj.getEnrollmentRulesBudget();
                case "accountingRulesAgreementBudget":
                    return obj.getAccountingRulesAgreementBudget();
                case "enrollmentRulesContract":
                    return obj.getEnrollmentRulesContract();
                case "accountingRulesAgreementContract":
                    return obj.getAccountingRulesAgreementContract();
                case "enrEntrantExamListPrintType":
                    return obj.getEnrEntrantExamListPrintType();
                case "excludeEnrolledByLowPriority":
                    return obj.isExcludeEnrolledByLowPriority();
                case "tieBreakRatingRule":
                    return obj.getTieBreakRatingRule();
                case "levelBudgetFinancing":
                    return obj.getLevelBudgetFinancing();
                case "acceptPeopleResidingInCrimea":
                    return obj.isAcceptPeopleResidingInCrimea();
                case "enrollmentCampaignType":
                    return obj.getEnrollmentCampaignType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "reenrollByPriorityEnabled":
                    obj.setReenrollByPriorityEnabled((Boolean) value);
                    return;
                case "stateExamRestriction":
                    obj.setStateExamRestriction((Boolean) value);
                    return;
                case "numberOnRequestManual":
                    obj.setNumberOnRequestManual((Boolean) value);
                    return;
                case "stateExamAutoCheck":
                    obj.setStateExamAutoCheck((Boolean) value);
                    return;
                case "examGroupTitleFormingManual":
                    obj.setExamGroupTitleFormingManual((Boolean) value);
                    return;
                case "stateExamAccepted":
                    obj.setStateExamAccepted((Boolean) value);
                    return;
                case "acceptedContractDefault":
                    obj.setAcceptedContractDefault((Boolean) value);
                    return;
                case "targetAdmissionCompetition":
                    obj.setTargetAdmissionCompetition((Boolean) value);
                    return;
                case "examIntervalAuto":
                    obj.setExamIntervalAuto((Integer) value);
                    return;
                case "examIntervalManual":
                    obj.setExamIntervalManual((Integer) value);
                    return;
                case "appealDeadlineDays":
                    obj.setAppealDeadlineDays((Integer) value);
                    return;
                case "appealHearingDays":
                    obj.setAppealHearingDays((Integer) value);
                    return;
                case "enrEntrantContractExistCheckMode":
                    obj.setEnrEntrantContractExistCheckMode((Long) value);
                    return;
                case "accountingRulesOrigEduDoc":
                    obj.setAccountingRulesOrigEduDoc((Long) value);
                    return;
                case "enrollmentRulesBudget":
                    obj.setEnrollmentRulesBudget((Long) value);
                    return;
                case "accountingRulesAgreementBudget":
                    obj.setAccountingRulesAgreementBudget((Long) value);
                    return;
                case "enrollmentRulesContract":
                    obj.setEnrollmentRulesContract((Long) value);
                    return;
                case "accountingRulesAgreementContract":
                    obj.setAccountingRulesAgreementContract((Long) value);
                    return;
                case "enrEntrantExamListPrintType":
                    obj.setEnrEntrantExamListPrintType((Long) value);
                    return;
                case "excludeEnrolledByLowPriority":
                    obj.setExcludeEnrolledByLowPriority((Boolean) value);
                    return;
                case "tieBreakRatingRule":
                    obj.setTieBreakRatingRule((EnrTieBreakRatingRule) value);
                    return;
                case "levelBudgetFinancing":
                    obj.setLevelBudgetFinancing((EnrLevelBudgetFinancing) value);
                    return;
                case "acceptPeopleResidingInCrimea":
                    obj.setAcceptPeopleResidingInCrimea((Boolean) value);
                    return;
                case "enrollmentCampaignType":
                    obj.setEnrollmentCampaignType((EnrEnrollmentCampaignType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "reenrollByPriorityEnabled":
                        return true;
                case "stateExamRestriction":
                        return true;
                case "numberOnRequestManual":
                        return true;
                case "stateExamAutoCheck":
                        return true;
                case "examGroupTitleFormingManual":
                        return true;
                case "stateExamAccepted":
                        return true;
                case "acceptedContractDefault":
                        return true;
                case "targetAdmissionCompetition":
                        return true;
                case "examIntervalAuto":
                        return true;
                case "examIntervalManual":
                        return true;
                case "appealDeadlineDays":
                        return true;
                case "appealHearingDays":
                        return true;
                case "enrEntrantContractExistCheckMode":
                        return true;
                case "accountingRulesOrigEduDoc":
                        return true;
                case "enrollmentRulesBudget":
                        return true;
                case "accountingRulesAgreementBudget":
                        return true;
                case "enrollmentRulesContract":
                        return true;
                case "accountingRulesAgreementContract":
                        return true;
                case "enrEntrantExamListPrintType":
                        return true;
                case "excludeEnrolledByLowPriority":
                        return true;
                case "tieBreakRatingRule":
                        return true;
                case "levelBudgetFinancing":
                        return true;
                case "acceptPeopleResidingInCrimea":
                        return true;
                case "enrollmentCampaignType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "reenrollByPriorityEnabled":
                    return true;
                case "stateExamRestriction":
                    return true;
                case "numberOnRequestManual":
                    return true;
                case "stateExamAutoCheck":
                    return true;
                case "examGroupTitleFormingManual":
                    return true;
                case "stateExamAccepted":
                    return true;
                case "acceptedContractDefault":
                    return true;
                case "targetAdmissionCompetition":
                    return true;
                case "examIntervalAuto":
                    return true;
                case "examIntervalManual":
                    return true;
                case "appealDeadlineDays":
                    return true;
                case "appealHearingDays":
                    return true;
                case "enrEntrantContractExistCheckMode":
                    return true;
                case "accountingRulesOrigEduDoc":
                    return true;
                case "enrollmentRulesBudget":
                    return true;
                case "accountingRulesAgreementBudget":
                    return true;
                case "enrollmentRulesContract":
                    return true;
                case "accountingRulesAgreementContract":
                    return true;
                case "enrEntrantExamListPrintType":
                    return true;
                case "excludeEnrolledByLowPriority":
                    return true;
                case "tieBreakRatingRule":
                    return true;
                case "levelBudgetFinancing":
                    return true;
                case "acceptPeopleResidingInCrimea":
                    return true;
                case "enrollmentCampaignType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "reenrollByPriorityEnabled":
                    return Boolean.class;
                case "stateExamRestriction":
                    return Boolean.class;
                case "numberOnRequestManual":
                    return Boolean.class;
                case "stateExamAutoCheck":
                    return Boolean.class;
                case "examGroupTitleFormingManual":
                    return Boolean.class;
                case "stateExamAccepted":
                    return Boolean.class;
                case "acceptedContractDefault":
                    return Boolean.class;
                case "targetAdmissionCompetition":
                    return Boolean.class;
                case "examIntervalAuto":
                    return Integer.class;
                case "examIntervalManual":
                    return Integer.class;
                case "appealDeadlineDays":
                    return Integer.class;
                case "appealHearingDays":
                    return Integer.class;
                case "enrEntrantContractExistCheckMode":
                    return Long.class;
                case "accountingRulesOrigEduDoc":
                    return Long.class;
                case "enrollmentRulesBudget":
                    return Long.class;
                case "accountingRulesAgreementBudget":
                    return Long.class;
                case "enrollmentRulesContract":
                    return Long.class;
                case "accountingRulesAgreementContract":
                    return Long.class;
                case "enrEntrantExamListPrintType":
                    return Long.class;
                case "excludeEnrolledByLowPriority":
                    return Boolean.class;
                case "tieBreakRatingRule":
                    return EnrTieBreakRatingRule.class;
                case "levelBudgetFinancing":
                    return EnrLevelBudgetFinancing.class;
                case "acceptPeopleResidingInCrimea":
                    return Boolean.class;
                case "enrollmentCampaignType":
                    return EnrEnrollmentCampaignType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrEnrollmentCampaignSettings> _dslPath = new Path<EnrEnrollmentCampaignSettings>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrEnrollmentCampaignSettings");
    }
            

    /**
     * http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20686876
     * Влияет на разрешение конфликтов при формировании шага зачисления:
     * false - не разрешать перезачислять абитуриентов;
     * true - разрешать перезачислять абитуриентов
     *
     * @return Перезачислять на лучший приоритет. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#isReenrollByPriorityEnabled()
     */
    public static PropertyPath<Boolean> reenrollByPriorityEnabled()
    {
        return _dslPath.reenrollByPriorityEnabled();
    }

    /**
     * DEV-2891 DEV-2856
     * Влияет на поведение формы добавления заявления (добавление НП/КГ) абитуриента: отображение и значение по умолчанию для поля «Свидетельство ЕГЭ накладывает ограничения на выбор направлений/специальностей в заявлении»:
     * null - Не используется (поле не отображается);
     * false - Используется, по умолчанию выбор не ограничен (поле отображается, по умолчанию выключено);
     * true - Используется, по умолчанию выбор ограничен (поле отображается, по умолчанию включено)
     *
     * @return Ограничение выбора конкурсов в заявлении по данным свидетельств ЕГЭ (покрытие набора ВИ баллами по предметам ЕГЭ).
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#getStateExamRestriction()
     */
    public static PropertyPath<Boolean> stateExamRestriction()
    {
        return _dslPath.stateExamRestriction();
    }

    /**
     * DEV-2840
     * Влияет на поведение формы добавления заявления абитуриента: отображение поля «№ заявления»:
     * false - поле не отображается, номер присваивается инкрементом от максимального в базе в рамках ПК при сохранении заявления;
     * true - поле не отображается, заполняется инкрементом от максимального в базе в рамках ПК на момент открытия формы, при сохранении проверяется на уникальность в рамках ПК
     *
     * @return Присваивать регистрационный номер заявления вручную. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#isNumberOnRequestManual()
     */
    public static PropertyPath<Boolean> numberOnRequestManual()
    {
        return _dslPath.numberOnRequestManual();
    }

    /**
     * DEV-2920
     * Влияет на поведение формы создания свидетельства ЕГЭ:
     * false - свидетельство создается не проверенным (признак проверено = true);
     * true - свидетельство создается проверенным (признак проверено = true)
     *
     * @return Регистрировать свидетельства ЕГЭ проверенными. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#isStateExamAutoCheck()
     */
    public static PropertyPath<Boolean> stateExamAutoCheck()
    {
        return _dslPath.stateExamAutoCheck();
    }

    /**
     * Влияет на поведение формы создания ЭГ:
     * true - название ЭГ задается вручную
     * false - названия ЭГ формируются системой автоматически: представляют из себя натуральные числа, начиная с 1, при добавлении ЭГ в качестве ее названия берется наибольшее число из названий уже созданных ЭГ в ПК +1
     *
     * @return Формировать названия экзам. групп вручную. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#isExamGroupTitleFormingManual()
     */
    public static PropertyPath<Boolean> examGroupTitleFormingManual()
    {
        return _dslPath.examGroupTitleFormingManual();
    }

    /**
     * Влияет на поведение формы создания результатов ЕГЭ:
     * true – результаты ЕГЭ по умолчанию добавляются зачтенными;
     * false – результаты ЕГЭ по умолчанию добавляются не зачтенными.
     *
     * @return Добавлять результаты ЕГЭ по умолчанию зачтенными. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#isStateExamAccepted()
     */
    public static PropertyPath<Boolean> stateExamAccepted()
    {
        return _dslPath.stateExamAccepted();
    }

    /**
     * @return При выборе конкурса на договор абитуриент по умолчанию согласен на зачисление. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#isAcceptedContractDefault()
     */
    public static PropertyPath<Boolean> acceptedContractDefault()
    {
        return _dslPath.acceptedContractDefault();
    }

    /**
     * false - общий для всех видов целевого приема
     * true - отдельный для каждого вида целевого приема
     *
     * @return Конкурс по целевому приему. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#isTargetAdmissionCompetition()
     */
    public static PropertyPath<Boolean> targetAdmissionCompetition()
    {
        return _dslPath.targetAdmissionCompetition();
    }

    /**
     * @return Промежуток между ВИ при автомат. выборе ЭГ (дн.). Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#getExamIntervalAuto()
     */
    public static PropertyPath<Integer> examIntervalAuto()
    {
        return _dslPath.examIntervalAuto();
    }

    /**
     * @return Промежуток между ВИ при ручном выборе ЭГ (дн.). Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#getExamIntervalManual()
     */
    public static PropertyPath<Integer> examIntervalManual()
    {
        return _dslPath.examIntervalManual();
    }

    /**
     * @return Срок подачи апелляции. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#getAppealDeadlineDays()
     */
    public static PropertyPath<Integer> appealDeadlineDays()
    {
        return _dslPath.appealDeadlineDays();
    }

    /**
     * @return Срок рассмотрения апелляции. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#getAppealHearingDays()
     */
    public static PropertyPath<Integer> appealHearingDays()
    {
        return _dslPath.appealHearingDays();
    }

    /**
     * Используется, только если модуль unienr14_ctr подключен, соответствует коду проверки в EnrEntrantContractExistsRule (unienr14_ctr).
     * 0 - означает, что проверка не используется (наличие договора не требуется).
     *
     * @return Вариант проверки наличия договора при определении возможности зачисления по договору. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#getEnrEntrantContractExistCheckMode()
     */
    public static PropertyPath<Long> enrEntrantContractExistCheckMode()
    {
        return _dslPath.enrEntrantContractExistCheckMode();
    }

    /**
     * TODO:
     *
     * @return Правила учета оригинала документа об образовании абитуриента. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#getAccountingRulesOrigEduDoc()
     */
    public static PropertyPath<Long> accountingRulesOrigEduDoc()
    {
        return _dslPath.accountingRulesOrigEduDoc();
    }

    /**
     * TODO:
     *
     * @return Условия зачисления при поступлении на места в рамках КЦП. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#getEnrollmentRulesBudget()
     */
    public static PropertyPath<Long> enrollmentRulesBudget()
    {
        return _dslPath.enrollmentRulesBudget();
    }

    /**
     * TODO:
     *
     * @return Правила учета согласия на зачисление абитуриента при поступлении на места в рамках КЦП. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#getAccountingRulesAgreementBudget()
     */
    public static PropertyPath<Long> accountingRulesAgreementBudget()
    {
        return _dslPath.accountingRulesAgreementBudget();
    }

    /**
     * TODO:
     *
     * @return Условия зачисления при поступлении на места по договору. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#getEnrollmentRulesContract()
     */
    public static PropertyPath<Long> enrollmentRulesContract()
    {
        return _dslPath.enrollmentRulesContract();
    }

    /**
     * TODO:
     *
     * @return Правила учета согласия на зачисление абитуриента при поступлении на места по договору. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#getAccountingRulesAgreementContract()
     */
    public static PropertyPath<Long> accountingRulesAgreementContract()
    {
        return _dslPath.accountingRulesAgreementContract();
    }

    /**
     * DEV-5450:
     * 1 - Выводить все вступительные испытания;
     * 2 - Выводить только внутренние вступительные испытания.
     *
     * @return Тип печати экзаменационного листа. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#getEnrEntrantExamListPrintType()
     */
    public static PropertyPath<Long> enrEntrantExamListPrintType()
    {
        return _dslPath.enrEntrantExamListPrintType();
    }

    /**
     * @return Исключать зачисленных абитуриентов из конкурсов с более низким приоритетом. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#isExcludeEnrolledByLowPriority()
     */
    public static PropertyPath<Boolean> excludeEnrolledByLowPriority()
    {
        return _dslPath.excludeEnrolledByLowPriority();
    }

    /**
     * @return Способ ранжирования абитуриентов при равенстве суммы конкурсных баллов (бакалавриат, специалитет, магистратура). Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#getTieBreakRatingRule()
     */
    public static EnrTieBreakRatingRule.Path<EnrTieBreakRatingRule> tieBreakRatingRule()
    {
        return _dslPath.tieBreakRatingRule();
    }

    /**
     * @return Уровни бюджетного финансирования. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#getLevelBudgetFinancing()
     */
    public static EnrLevelBudgetFinancing.Path<EnrLevelBudgetFinancing> levelBudgetFinancing()
    {
        return _dslPath.levelBudgetFinancing();
    }

    /**
     * @return Прием лиц, постоянно проживающих в Крыму. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#isAcceptPeopleResidingInCrimea()
     */
    public static PropertyPath<Boolean> acceptPeopleResidingInCrimea()
    {
        return _dslPath.acceptPeopleResidingInCrimea();
    }

    /**
     * @return Тип приемной кампании.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#getEnrollmentCampaignType()
     */
    public static EnrEnrollmentCampaignType.Path<EnrEnrollmentCampaignType> enrollmentCampaignType()
    {
        return _dslPath.enrollmentCampaignType();
    }

    public static class Path<E extends EnrEnrollmentCampaignSettings> extends EntityPath<E>
    {
        private PropertyPath<Boolean> _reenrollByPriorityEnabled;
        private PropertyPath<Boolean> _stateExamRestriction;
        private PropertyPath<Boolean> _numberOnRequestManual;
        private PropertyPath<Boolean> _stateExamAutoCheck;
        private PropertyPath<Boolean> _examGroupTitleFormingManual;
        private PropertyPath<Boolean> _stateExamAccepted;
        private PropertyPath<Boolean> _acceptedContractDefault;
        private PropertyPath<Boolean> _targetAdmissionCompetition;
        private PropertyPath<Integer> _examIntervalAuto;
        private PropertyPath<Integer> _examIntervalManual;
        private PropertyPath<Integer> _appealDeadlineDays;
        private PropertyPath<Integer> _appealHearingDays;
        private PropertyPath<Long> _enrEntrantContractExistCheckMode;
        private PropertyPath<Long> _accountingRulesOrigEduDoc;
        private PropertyPath<Long> _enrollmentRulesBudget;
        private PropertyPath<Long> _accountingRulesAgreementBudget;
        private PropertyPath<Long> _enrollmentRulesContract;
        private PropertyPath<Long> _accountingRulesAgreementContract;
        private PropertyPath<Long> _enrEntrantExamListPrintType;
        private PropertyPath<Boolean> _excludeEnrolledByLowPriority;
        private EnrTieBreakRatingRule.Path<EnrTieBreakRatingRule> _tieBreakRatingRule;
        private EnrLevelBudgetFinancing.Path<EnrLevelBudgetFinancing> _levelBudgetFinancing;
        private PropertyPath<Boolean> _acceptPeopleResidingInCrimea;
        private EnrEnrollmentCampaignType.Path<EnrEnrollmentCampaignType> _enrollmentCampaignType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20686876
     * Влияет на разрешение конфликтов при формировании шага зачисления:
     * false - не разрешать перезачислять абитуриентов;
     * true - разрешать перезачислять абитуриентов
     *
     * @return Перезачислять на лучший приоритет. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#isReenrollByPriorityEnabled()
     */
        public PropertyPath<Boolean> reenrollByPriorityEnabled()
        {
            if(_reenrollByPriorityEnabled == null )
                _reenrollByPriorityEnabled = new PropertyPath<Boolean>(EnrEnrollmentCampaignSettingsGen.P_REENROLL_BY_PRIORITY_ENABLED, this);
            return _reenrollByPriorityEnabled;
        }

    /**
     * DEV-2891 DEV-2856
     * Влияет на поведение формы добавления заявления (добавление НП/КГ) абитуриента: отображение и значение по умолчанию для поля «Свидетельство ЕГЭ накладывает ограничения на выбор направлений/специальностей в заявлении»:
     * null - Не используется (поле не отображается);
     * false - Используется, по умолчанию выбор не ограничен (поле отображается, по умолчанию выключено);
     * true - Используется, по умолчанию выбор ограничен (поле отображается, по умолчанию включено)
     *
     * @return Ограничение выбора конкурсов в заявлении по данным свидетельств ЕГЭ (покрытие набора ВИ баллами по предметам ЕГЭ).
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#getStateExamRestriction()
     */
        public PropertyPath<Boolean> stateExamRestriction()
        {
            if(_stateExamRestriction == null )
                _stateExamRestriction = new PropertyPath<Boolean>(EnrEnrollmentCampaignSettingsGen.P_STATE_EXAM_RESTRICTION, this);
            return _stateExamRestriction;
        }

    /**
     * DEV-2840
     * Влияет на поведение формы добавления заявления абитуриента: отображение поля «№ заявления»:
     * false - поле не отображается, номер присваивается инкрементом от максимального в базе в рамках ПК при сохранении заявления;
     * true - поле не отображается, заполняется инкрементом от максимального в базе в рамках ПК на момент открытия формы, при сохранении проверяется на уникальность в рамках ПК
     *
     * @return Присваивать регистрационный номер заявления вручную. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#isNumberOnRequestManual()
     */
        public PropertyPath<Boolean> numberOnRequestManual()
        {
            if(_numberOnRequestManual == null )
                _numberOnRequestManual = new PropertyPath<Boolean>(EnrEnrollmentCampaignSettingsGen.P_NUMBER_ON_REQUEST_MANUAL, this);
            return _numberOnRequestManual;
        }

    /**
     * DEV-2920
     * Влияет на поведение формы создания свидетельства ЕГЭ:
     * false - свидетельство создается не проверенным (признак проверено = true);
     * true - свидетельство создается проверенным (признак проверено = true)
     *
     * @return Регистрировать свидетельства ЕГЭ проверенными. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#isStateExamAutoCheck()
     */
        public PropertyPath<Boolean> stateExamAutoCheck()
        {
            if(_stateExamAutoCheck == null )
                _stateExamAutoCheck = new PropertyPath<Boolean>(EnrEnrollmentCampaignSettingsGen.P_STATE_EXAM_AUTO_CHECK, this);
            return _stateExamAutoCheck;
        }

    /**
     * Влияет на поведение формы создания ЭГ:
     * true - название ЭГ задается вручную
     * false - названия ЭГ формируются системой автоматически: представляют из себя натуральные числа, начиная с 1, при добавлении ЭГ в качестве ее названия берется наибольшее число из названий уже созданных ЭГ в ПК +1
     *
     * @return Формировать названия экзам. групп вручную. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#isExamGroupTitleFormingManual()
     */
        public PropertyPath<Boolean> examGroupTitleFormingManual()
        {
            if(_examGroupTitleFormingManual == null )
                _examGroupTitleFormingManual = new PropertyPath<Boolean>(EnrEnrollmentCampaignSettingsGen.P_EXAM_GROUP_TITLE_FORMING_MANUAL, this);
            return _examGroupTitleFormingManual;
        }

    /**
     * Влияет на поведение формы создания результатов ЕГЭ:
     * true – результаты ЕГЭ по умолчанию добавляются зачтенными;
     * false – результаты ЕГЭ по умолчанию добавляются не зачтенными.
     *
     * @return Добавлять результаты ЕГЭ по умолчанию зачтенными. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#isStateExamAccepted()
     */
        public PropertyPath<Boolean> stateExamAccepted()
        {
            if(_stateExamAccepted == null )
                _stateExamAccepted = new PropertyPath<Boolean>(EnrEnrollmentCampaignSettingsGen.P_STATE_EXAM_ACCEPTED, this);
            return _stateExamAccepted;
        }

    /**
     * @return При выборе конкурса на договор абитуриент по умолчанию согласен на зачисление. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#isAcceptedContractDefault()
     */
        public PropertyPath<Boolean> acceptedContractDefault()
        {
            if(_acceptedContractDefault == null )
                _acceptedContractDefault = new PropertyPath<Boolean>(EnrEnrollmentCampaignSettingsGen.P_ACCEPTED_CONTRACT_DEFAULT, this);
            return _acceptedContractDefault;
        }

    /**
     * false - общий для всех видов целевого приема
     * true - отдельный для каждого вида целевого приема
     *
     * @return Конкурс по целевому приему. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#isTargetAdmissionCompetition()
     */
        public PropertyPath<Boolean> targetAdmissionCompetition()
        {
            if(_targetAdmissionCompetition == null )
                _targetAdmissionCompetition = new PropertyPath<Boolean>(EnrEnrollmentCampaignSettingsGen.P_TARGET_ADMISSION_COMPETITION, this);
            return _targetAdmissionCompetition;
        }

    /**
     * @return Промежуток между ВИ при автомат. выборе ЭГ (дн.). Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#getExamIntervalAuto()
     */
        public PropertyPath<Integer> examIntervalAuto()
        {
            if(_examIntervalAuto == null )
                _examIntervalAuto = new PropertyPath<Integer>(EnrEnrollmentCampaignSettingsGen.P_EXAM_INTERVAL_AUTO, this);
            return _examIntervalAuto;
        }

    /**
     * @return Промежуток между ВИ при ручном выборе ЭГ (дн.). Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#getExamIntervalManual()
     */
        public PropertyPath<Integer> examIntervalManual()
        {
            if(_examIntervalManual == null )
                _examIntervalManual = new PropertyPath<Integer>(EnrEnrollmentCampaignSettingsGen.P_EXAM_INTERVAL_MANUAL, this);
            return _examIntervalManual;
        }

    /**
     * @return Срок подачи апелляции. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#getAppealDeadlineDays()
     */
        public PropertyPath<Integer> appealDeadlineDays()
        {
            if(_appealDeadlineDays == null )
                _appealDeadlineDays = new PropertyPath<Integer>(EnrEnrollmentCampaignSettingsGen.P_APPEAL_DEADLINE_DAYS, this);
            return _appealDeadlineDays;
        }

    /**
     * @return Срок рассмотрения апелляции. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#getAppealHearingDays()
     */
        public PropertyPath<Integer> appealHearingDays()
        {
            if(_appealHearingDays == null )
                _appealHearingDays = new PropertyPath<Integer>(EnrEnrollmentCampaignSettingsGen.P_APPEAL_HEARING_DAYS, this);
            return _appealHearingDays;
        }

    /**
     * Используется, только если модуль unienr14_ctr подключен, соответствует коду проверки в EnrEntrantContractExistsRule (unienr14_ctr).
     * 0 - означает, что проверка не используется (наличие договора не требуется).
     *
     * @return Вариант проверки наличия договора при определении возможности зачисления по договору. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#getEnrEntrantContractExistCheckMode()
     */
        public PropertyPath<Long> enrEntrantContractExistCheckMode()
        {
            if(_enrEntrantContractExistCheckMode == null )
                _enrEntrantContractExistCheckMode = new PropertyPath<Long>(EnrEnrollmentCampaignSettingsGen.P_ENR_ENTRANT_CONTRACT_EXIST_CHECK_MODE, this);
            return _enrEntrantContractExistCheckMode;
        }

    /**
     * TODO:
     *
     * @return Правила учета оригинала документа об образовании абитуриента. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#getAccountingRulesOrigEduDoc()
     */
        public PropertyPath<Long> accountingRulesOrigEduDoc()
        {
            if(_accountingRulesOrigEduDoc == null )
                _accountingRulesOrigEduDoc = new PropertyPath<Long>(EnrEnrollmentCampaignSettingsGen.P_ACCOUNTING_RULES_ORIG_EDU_DOC, this);
            return _accountingRulesOrigEduDoc;
        }

    /**
     * TODO:
     *
     * @return Условия зачисления при поступлении на места в рамках КЦП. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#getEnrollmentRulesBudget()
     */
        public PropertyPath<Long> enrollmentRulesBudget()
        {
            if(_enrollmentRulesBudget == null )
                _enrollmentRulesBudget = new PropertyPath<Long>(EnrEnrollmentCampaignSettingsGen.P_ENROLLMENT_RULES_BUDGET, this);
            return _enrollmentRulesBudget;
        }

    /**
     * TODO:
     *
     * @return Правила учета согласия на зачисление абитуриента при поступлении на места в рамках КЦП. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#getAccountingRulesAgreementBudget()
     */
        public PropertyPath<Long> accountingRulesAgreementBudget()
        {
            if(_accountingRulesAgreementBudget == null )
                _accountingRulesAgreementBudget = new PropertyPath<Long>(EnrEnrollmentCampaignSettingsGen.P_ACCOUNTING_RULES_AGREEMENT_BUDGET, this);
            return _accountingRulesAgreementBudget;
        }

    /**
     * TODO:
     *
     * @return Условия зачисления при поступлении на места по договору. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#getEnrollmentRulesContract()
     */
        public PropertyPath<Long> enrollmentRulesContract()
        {
            if(_enrollmentRulesContract == null )
                _enrollmentRulesContract = new PropertyPath<Long>(EnrEnrollmentCampaignSettingsGen.P_ENROLLMENT_RULES_CONTRACT, this);
            return _enrollmentRulesContract;
        }

    /**
     * TODO:
     *
     * @return Правила учета согласия на зачисление абитуриента при поступлении на места по договору. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#getAccountingRulesAgreementContract()
     */
        public PropertyPath<Long> accountingRulesAgreementContract()
        {
            if(_accountingRulesAgreementContract == null )
                _accountingRulesAgreementContract = new PropertyPath<Long>(EnrEnrollmentCampaignSettingsGen.P_ACCOUNTING_RULES_AGREEMENT_CONTRACT, this);
            return _accountingRulesAgreementContract;
        }

    /**
     * DEV-5450:
     * 1 - Выводить все вступительные испытания;
     * 2 - Выводить только внутренние вступительные испытания.
     *
     * @return Тип печати экзаменационного листа. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#getEnrEntrantExamListPrintType()
     */
        public PropertyPath<Long> enrEntrantExamListPrintType()
        {
            if(_enrEntrantExamListPrintType == null )
                _enrEntrantExamListPrintType = new PropertyPath<Long>(EnrEnrollmentCampaignSettingsGen.P_ENR_ENTRANT_EXAM_LIST_PRINT_TYPE, this);
            return _enrEntrantExamListPrintType;
        }

    /**
     * @return Исключать зачисленных абитуриентов из конкурсов с более низким приоритетом. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#isExcludeEnrolledByLowPriority()
     */
        public PropertyPath<Boolean> excludeEnrolledByLowPriority()
        {
            if(_excludeEnrolledByLowPriority == null )
                _excludeEnrolledByLowPriority = new PropertyPath<Boolean>(EnrEnrollmentCampaignSettingsGen.P_EXCLUDE_ENROLLED_BY_LOW_PRIORITY, this);
            return _excludeEnrolledByLowPriority;
        }

    /**
     * @return Способ ранжирования абитуриентов при равенстве суммы конкурсных баллов (бакалавриат, специалитет, магистратура). Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#getTieBreakRatingRule()
     */
        public EnrTieBreakRatingRule.Path<EnrTieBreakRatingRule> tieBreakRatingRule()
        {
            if(_tieBreakRatingRule == null )
                _tieBreakRatingRule = new EnrTieBreakRatingRule.Path<EnrTieBreakRatingRule>(L_TIE_BREAK_RATING_RULE, this);
            return _tieBreakRatingRule;
        }

    /**
     * @return Уровни бюджетного финансирования. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#getLevelBudgetFinancing()
     */
        public EnrLevelBudgetFinancing.Path<EnrLevelBudgetFinancing> levelBudgetFinancing()
        {
            if(_levelBudgetFinancing == null )
                _levelBudgetFinancing = new EnrLevelBudgetFinancing.Path<EnrLevelBudgetFinancing>(L_LEVEL_BUDGET_FINANCING, this);
            return _levelBudgetFinancing;
        }

    /**
     * @return Прием лиц, постоянно проживающих в Крыму. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#isAcceptPeopleResidingInCrimea()
     */
        public PropertyPath<Boolean> acceptPeopleResidingInCrimea()
        {
            if(_acceptPeopleResidingInCrimea == null )
                _acceptPeopleResidingInCrimea = new PropertyPath<Boolean>(EnrEnrollmentCampaignSettingsGen.P_ACCEPT_PEOPLE_RESIDING_IN_CRIMEA, this);
            return _acceptPeopleResidingInCrimea;
        }

    /**
     * @return Тип приемной кампании.
     * @see ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings#getEnrollmentCampaignType()
     */
        public EnrEnrollmentCampaignType.Path<EnrEnrollmentCampaignType> enrollmentCampaignType()
        {
            if(_enrollmentCampaignType == null )
                _enrollmentCampaignType = new EnrEnrollmentCampaignType.Path<EnrEnrollmentCampaignType>(L_ENROLLMENT_CAMPAIGN_TYPE, this);
            return _enrollmentCampaignType;
        }

        public Class getEntityClass()
        {
            return EnrEnrollmentCampaignSettings.class;
        }

        public String getEntityName()
        {
            return "enrEnrollmentCampaignSettings";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
