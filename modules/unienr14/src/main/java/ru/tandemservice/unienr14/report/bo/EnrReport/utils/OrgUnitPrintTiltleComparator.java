/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.utils;

import org.tandemframework.shared.organization.base.entity.OrgUnit;

import java.util.Comparator;

/**
 * @author rsizonenko
 * @since 23.06.2014
 */
public class OrgUnitPrintTiltleComparator implements Comparator<OrgUnit> {
    @Override
    public int compare(OrgUnit o1, OrgUnit o2) {
        if (o1 == o2) { return 0; }
        if (null == o1) { return  1; }
        if (null == o2) { return -1; }
        return o1.getPrintTitle().compareToIgnoreCase(o2.getPrintTitle());
    }
}
