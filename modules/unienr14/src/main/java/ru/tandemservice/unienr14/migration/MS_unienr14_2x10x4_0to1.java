package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.ISQLTranslator;
import org.tandemframework.dbsupport.sql.SQLSelectQuery;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import static org.tandemframework.dbsupport.sql.SQLFrom.table;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unienr14_2x10x4_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.4")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrOlympiadProfile

		// создана новая сущность
		if (!tool.tableExists("enr14_c_olimp_profile_t"))
        {
			// создать таблицу
			DBTable dbt = new DBTable("enr14_c_olimp_profile_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_enrolympiadprofile"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("title_p", DBType.createVarchar(1200)).setNullable(false), 
				new DBColumn("usercode_p", DBType.createVarchar(255)), 
				new DBColumn("disableddate_p", DBType.TIMESTAMP)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("enrOlympiadProfile");

		}

        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrDocumentCategory

        // создана новая сущность
        if (!tool.tableExists("enr14_document_category"))
        {
            // создать таблицу
            DBTable dbt = new DBTable("enr14_document_category",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_enrdocumentcategory"),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("entrantdocumenttype_id", DBType.LONG).setNullable(false),
                                      new DBColumn("title_p", DBType.createVarchar(1200)).setNullable(false),
                                      new DBColumn("usercode_p", DBType.createVarchar(255)),
                                      new DBColumn("disableddate_p", DBType.TIMESTAMP)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("enrDocumentCategory");

        }


        //В справочник «Типы документов абитуриента» добавить новые системные типы
        {
            String[] titles = new String[]{
                    "Документ, подтверждающий сиротство",
                    "Документ, подтверждающий принадлежность к ветеранам боевых действий",
                    "Диплом победителя/призера в области спорта",
                    "Документы, подтверждающие принадлежность к соотечественникам"};

            String[] shortTitles = new String[]{"док. сирот", "док. вет. БД", "дипл. спорт", "док. соотеч"};

            //Проверяем наличие пользовательских элементов с такими названиями и сокращенными названиями
            //Если такие элементы есть - переименовываем, добавляя указание кода элемента
            PreparedStatement updateTitle = tool.prepareStatement("update enr14_c_entrant_doc_type_t set title_p = ? where id = ?");
            PreparedStatement updateShortTitle = tool.prepareStatement("update enr14_c_entrant_doc_type_t set shorttitle_p = ? where id = ?");

            ISQLTranslator translator = tool.getDialect().getSQLTranslator();

            SQLSelectQuery sqlTitle = new SQLSelectQuery().from(
                    table("enr14_c_entrant_doc_type_t", "doc"))
                    .column("doc.id")
                    .column("doc.title_p")
                    .column("doc.code_p")
                    .where("doc.title_p in ('Документ, подтверждающий сиротство', 'Документ, подтверждающий принадлежность к ветеранам боевых действий'," +
                                   "'Диплом победителя/призера в области спорта', 'Документы, подтверждающие принадлежность к соотечественникам')");

            String selectTitle = translator.toSql(sqlTitle);

            Statement titleStatement = tool.getConnection().createStatement();
            titleStatement.execute(selectTitle);

            ResultSet rsTitle = titleStatement.getResultSet();
            while (rsTitle.next())
            {
                Long docId = rsTitle.getLong(1);
                String docTitle = rsTitle.getString(2);
                String code = rsTitle.getString(3);
                String newTitle = docTitle + "(" + code + ")";
                updateTitle.setString(1, newTitle);
                updateTitle.setLong(2, docId);
                updateTitle.execute();
            }

            SQLSelectQuery sqlShortTitle = new SQLSelectQuery().from(
                    table("enr14_c_entrant_doc_type_t", "doc"))
                    .column("doc.id")
                    .column("doc.shorttitle_p")
                    .column("doc.code_p")
                    .where("doc.shorttitle_p in ('док. сирот', 'док. вет. БД', 'дипл. спорт', 'док. соотеч')");

            String selectShortTitle = translator.toSql(sqlShortTitle);

            Statement shortTitleStatement = tool.getConnection().createStatement();
            shortTitleStatement.execute(selectShortTitle);

            ResultSet rsShortTitle = shortTitleStatement.getResultSet();
            while (rsShortTitle.next())
            {
                Long docId = rsShortTitle.getLong(1);
                String shortTitle = rsShortTitle.getString(2);
                String code = rsShortTitle.getString(3);
                String newShort = shortTitle + "(" + code + ")";
                updateShortTitle.setString(1, newShort);
                updateShortTitle.setLong(2, docId);
                updateShortTitle.execute();
            }

            //Добавляем новые
            Integer priority = ((Integer) tool.getUniqueResult("select max(priority_p) from enr14_c_entrant_doc_type_t")) + 1;
            short entityCode = tool.entityCodes().ensure("enrEntrantDocumentType");

            final MigrationUtils.BatchInsertBuilder insertBuilder = new MigrationUtils.BatchInsertBuilder(entityCode, "code_p", "shorttitle_p", "priority_p", "title_p");

            insertBuilder.addRow("2016_08", shortTitles[0], priority++, titles[0]);
            insertBuilder.addRow("2016_09", shortTitles[1], priority++, titles[1]);
            insertBuilder.addRow("2016_10", shortTitles[2], priority++, titles[2]);
            insertBuilder.addRow("2016_11", shortTitles[3], priority, titles[3]);
            insertBuilder.executeInsert(tool, "enr14_c_entrant_doc_type_t");
        }


        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrOlympiadType

        // создано обязательное свойство priority
        {
            // создать колонку
            tool.createColumn("enr14_c_olymp_type_t", new DBColumn("priority_p", DBType.INTEGER));


            // задать значение по умолчанию
            Map<String, Integer> priorityMap = new HashMap<>();
            priorityMap.put("1", 1);
            priorityMap.put("2", 2);
            priorityMap.put("3", 5);


            //устанавливаем приоритеты имеющимся элементам
            final MigrationUtils.BatchUpdater updater = new MigrationUtils.BatchUpdater("update enr14_c_olymp_type_t set priority_p = ? where code_p = ?", DBType.INTEGER, DBType.EMPTY_STRING);

            Number priorityNum = (Number) tool.getUniqueResult("select count(*) from enr14_c_olymp_type_t");
            Integer priority = priorityNum.intValue() + 3;
            PreparedStatement statement = tool.prepareStatement("select code_p from enr14_c_olymp_type_t");
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next())
            {
                String code = resultSet.getString(1);
                Integer newPriority = priorityMap.get(code);
                if (null == newPriority)
                    newPriority = priority++;
                updater.addBatch(newPriority, code);
            }
            updater.executeUpdate(tool);

            //добавляем новые элементы
            short entityCode = tool.entityCodes().ensure("enrOlympiadType");

            final MigrationUtils.BatchInsertBuilder insertBuilder = new MigrationUtils.BatchInsertBuilder(entityCode, "code_p", "shorttitle_p", "priority_p", "title_p");

            insertBuilder.addRow("4", "Всеукр. олимп", 3, "Всеукраинская ученическая олимпиада (4 этап)");
            insertBuilder.addRow("5", "Международ. олимп", 4, "Международная олимпиада");
            insertBuilder.executeInsert(tool, "enr14_c_olymp_type_t");

            // сделать колонку NOT NULL
            tool.setColumnNullable("enr14_c_olymp_type_t", "priority_p", false);

        }


        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrEntrantBaseDocument

        // создано свойство documentCategory
        {
            // создать колонку
            tool.createColumn("enr14_enrtant_base_doc_t", new DBColumn("documentcategory_id", DBType.LONG));

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrOlympiadDiploma

        // создано свойство olimpiadProfile
        {
            // создать колонку
            tool.createColumn("enr14_olymp_diploma_t", new DBColumn("olimpiadprofile_id", DBType.LONG));

        }

        // создано свойство trainingClass
        {
            // создать колонку
            tool.createColumn("enr14_olymp_diploma_t", new DBColumn("trainingclass_p", DBType.INTEGER));

        }

        // создано свойство combinedTeam
        {
            // создать колонку
            tool.createColumn("enr14_olymp_diploma_t", new DBColumn("combinedteam_p", DBType.BOOLEAN));

        }

        // создано свойство issuancePlace
        {
            // создать колонку
            tool.createColumn("enr14_olymp_diploma_t", new DBColumn("issuanceplace_p", DBType.createVarchar(255)));

        }

        //  свойство number стало необязательным
        {
            // сделать колонку NULL
            tool.setColumnNullable("enr14_olymp_diploma_t", "number_p", true);

        }



    }
}