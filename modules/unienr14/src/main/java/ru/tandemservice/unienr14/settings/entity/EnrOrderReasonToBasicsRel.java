package ru.tandemservice.unienr14.settings.entity;

import ru.tandemservice.unienr14.catalog.entity.EnrOrderBasic;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderReason;
import ru.tandemservice.unienr14.settings.entity.gen.*;

/**
 * Связь причин с основаниями для приказов по абитуриентам
 */
public class EnrOrderReasonToBasicsRel extends EnrOrderReasonToBasicsRelGen
{
    public EnrOrderReasonToBasicsRel()
    {
    }

    public EnrOrderReasonToBasicsRel(EnrOrderReason reason, EnrOrderBasic basic)
    {
        setReason(reason);
        setBasic(basic);
    }
}