/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrOlympiadDiploma.ui.Pub;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.sec.runtime.SecurityRuntime;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.commonbase.remoteDocument.dto.RemoteDocumentDTO;
import org.tandemframework.shared.commonbase.remoteDocument.service.ICommonRemoteDocumentService;
import org.tandemframework.shared.commonbase.remoteDocument.service.IRemoteDocumentProvider;
import org.tandemframework.shared.person.base.bo.Person.util.ISecureRoleContext;
import org.tandemframework.shared.person.base.bo.Person.util.PersonRoleSecModel;
import org.tandemframework.shared.person.base.bo.Person.util.SecureRoleContext;
import org.tandemframework.shared.person.base.bo.PersonDocument.PersonDocumentManager;
import org.tandemframework.shared.person.base.bo.PersonDocument.logic.PersonDocumentContext;
import org.tandemframework.shared.person.base.bo.PersonDocument.logic.PersonDocumentTypeSettingsConfig;
import org.tandemframework.shared.person.base.bo.PersonDocument.ui.Add.PersonDocumentAdd;
import org.tandemframework.shared.person.base.bo.PersonDocument.ui.Pub.PersonDocumentPubUI;
import org.tandemframework.shared.person.base.entity.PersonDocument;
import org.tandemframework.shared.person.base.entity.PersonDocumentRoleRel;
import org.tandemframework.shared.person.base.entity.PersonRole;
import org.tandemframework.shared.person.base.util.PersonDocumentUtil;
import org.tandemframework.shared.person.remoteDocument.ext.RemoteDocument.RemoteDocumentExtManager;
import ru.tandemservice.unienr14.entrant.bo.EnrOlympiadDiploma.EnrOlympiadDiplomaManager;
import ru.tandemservice.unienr14.entrant.bo.EnrOlympiadDiploma.ui.AddEdit.EnrOlympiadDiplomaAddEdit;
import ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma;

/**
 * @author oleyba
 * @since 7/16/14
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "personDocumentId", required = true),
        @Bind(key = PersonDocumentManager.BIND_PERSON_ROLE_ID, binding = "personRoleId", required = true),
        @Bind(key = PersonDocumentManager.BIND_SECURE_ROLE_CONTEXT, binding = "personRoleModel", required = true)
})
public class EnrOlympiadDiplomaPubUI extends PersonDocumentPubUI
{
//    private CommonPostfixPermissionModel _secModel;
//
//    private Long _personDocumentId;
//    private Long _personRoleId;
//
//    private ISecureRoleContext _personRoleModel;
//
//    private EnrOlympiadDiploma _document;
//    private PersonRole _personRole;
//
//    private RemoteDocumentDTO _scanCopy;
//    private PersonDocumentTypeSettingsConfig _docConfig;
//
//    private boolean _existRoleRelation;
//
//    @Override
//    public void onComponentRefresh()
//    {
//        setDocument(DataAccessServices.dao().get(getPersonDocumentId()));
//        setPersonRole(DataAccessServices.dao().get(getPersonRoleId()));
//
//        PersonDocumentContext context = PersonDocumentManager.instance().documentContext().getItem(PersonDocumentUtil.key(getPersonRole().getEntityMeta().getEntityClass()));
//
//        if(context == null)
//        {
//            throw new IllegalStateException("Unknown document context for personRole - " + getPersonRole().getEntityMeta().getEntityClass().getSimpleName());
//        }
//
//        setDocConfig(PersonDocumentManager.instance().dao().getConfig(context, getDocument().getDocumentType().getCode()));
//
//        if(getDocument().getScanCopy() != null )
//        {
//            try (ICommonRemoteDocumentService service = IRemoteDocumentProvider.instance.get().taskService(RemoteDocumentExtManager.PERSON_DOCUMENTS_SERVICE_NAME))
//            {
//                setScanCopy(service.get(getDocument().getScanCopy()));
//            }
//        }
//
//        setSecModel(PersonRoleSecModel.instance(getDocument().getPerson(), getPersonRole()));
//        setExistRoleRelation(DataAccessServices.dao().getByNaturalId(new PersonDocumentRoleRel.NaturalId(getDocument(), getPersonRole())) != null);
//    }
////
////    @Override
////    public ISecured getSecuredObject(){ return getDocument(); }
////
////    // getters and setters
////
////    public boolean isScanCopyExist()
////    {
////        return getDocument().getScanCopy() != null;
////    }
////
////    public void onClickEdit()
////    {
////        _uiActivation.asRegion(EnrOlympiadDiplomaAddEdit.class).top()
//////                .parameter(EnrOlympiadDiplomaAddEditUI.BIND_ENTRANT_ID, getDocument().getEntrant().getId()) // TODO: SH-1904
////                .parameter(PUBLISHER_ID, getDocument().getId())
////                .activate();
////    }
////
////    public void onClickDelete() {
////        EnrOlympiadDiplomaManager.instance().dao().doUnlinkDocument(getDocument().getId(), null); // TODO: SH-1904
////        deactivate();
////    }
////
////    public void onClickDownloadScanCopy()
////    {
////        if (getDocument().getScanCopy() == null) {
////            throw new ApplicationException("К документу не приложена скан-копия.");
////        }
////        try (ICommonRemoteDocumentService service = IRemoteDocumentProvider.instance.get().taskService(RemoteDocumentExtManager.PERSON_DOCUMENTS_SERVICE_NAME)) {
////            RemoteDocumentDTO scanCopy = service.get(getDocument().getScanCopy());
////            if (scanCopy != null) {
////                BusinessComponentUtils.downloadDocument(
////                        new CommonBaseRenderer()
////                                .contentType(scanCopy.getFileType())
////                                .fileName(scanCopy.getFileName())
////                                .document(scanCopy.getContent()), true);
////            }
////        } catch (Exception e) {
////            e.printStackTrace();
////            throw new ApplicationException("Не получается скачать скан-копию.");
////        }
////    }
////
////    public void onClickDeleteScanCopy()
////    {
////        EnrOlympiadDiplomaManager.instance().dao().deleteScanCopy(getDocument().getId());
////        getSupport().setRefreshScheduled(true);
////    }
////
////    public String getDeleteScanCopyAlertTitle()
////    {
////        return String.format(getConfig().getProperty("ui.deleteScanCopy.alert"), getDocument().getTitle());
////    }
////
////    public String getDeleteAlertTitle() {
////        return String.format(getConfig().getProperty("ui.delete.alert"), getDocument().getTitle());
////    }
////
////    public EnrOlympiadDiploma getDocument()
////    {
////        return getHolder().getValue();
////    }
////
////    public EntityHolder<EnrOlympiadDiploma> getHolder()
////    {
////        return holder;
////    }
//
//    public void onClickEdit() {
//        _uiActivation.asRegionDialog(PersonDocumentAdd.class)
//                .parameter(UIPresenter.PUBLISHER_ID, getDocument().getId())
//                .parameter(PersonDocumentManager.BIND_PERSON_ROLE_ID, getPersonRole().getId())
//                .activate();
//    }
//
//    public void onClickDelete() {
//        PersonDocumentManager.instance().dao().doUnlinkDocument(getDocument().getId(), getPersonRole());
//        deactivate();
//    }
//
//    public void onClickDownloadScanCopy()
//    {
//        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType(_scanCopy.getFileType()).fileName(_scanCopy.getFileName()).document(_scanCopy.getContent()), true);
//    }
//
//    public void onClickDeleteScanCopy()
//    {
//        PersonDocumentManager.instance().dao().deleteScanCopy(getDocument().getId());
//        _scanCopy = null;
//        getSupport().setRefreshScheduled(true);
//    }
//
//    public boolean isScanCopyButtonsVisible()
//    {
//        return getScanCopy() != null &&  getDocConfig().isShowScanCopy();
//    }
//
//    public String getDocTitle()
//    {
//        return getDocument().getDocumentType().getTitle() + ": " +
//                (StringUtils.isEmpty(getDocument().getSeria()) ? "" : getDocument().getSeria() +
//                        (StringUtils.isEmpty(getDocument().getNumber()) ? "" : getDocument().getNumber())) +
//                "(Персона: "+ getDocument().getPerson().getFullFio() +")";
//    }
//
//    // getters and setters
//
//
//    public Long getPersonDocumentId()
//    {
//        return _personDocumentId;
//    }
//
//    public void setPersonDocumentId(Long personDocumentId)
//    {
//        _personDocumentId = personDocumentId;
//    }
//
//    public Long getPersonRoleId()
//    {
//        return _personRoleId;
//    }
//
//    public void setPersonRoleId(Long personRoleId)
//    {
//        _personRoleId = personRoleId;
//    }
//
//    public ISecureRoleContext getPersonRoleModel()
//    {
//        return _personRoleModel;
//    }
//
//    public void setPersonRoleModel(ISecureRoleContext personRoleModel)
//    {
//        _personRoleModel = personRoleModel;
//    }
//
//    public PersonRole getPersonRole()
//    {
//        return _personRole;
//    }
//
//    public EnrOlympiadDiploma getDocument()
//    {
//        return _document;
//    }
//
//    public void setDocument(EnrOlympiadDiploma document)
//    {
//        _document = document;
//    }
//
//    public void setPersonRole(PersonRole personRole)
//    {
//        _personRole = personRole;
//    }
//
//    public RemoteDocumentDTO getScanCopy()
//    {
//        return _scanCopy;
//    }
//
//    public void setScanCopy(RemoteDocumentDTO scanCopy)
//    {
//        _scanCopy = scanCopy;
//    }
//
//    public CommonPostfixPermissionModel getSecModel()
//    {
//        return _secModel;
//    }
//
//    public void setSecModel(CommonPostfixPermissionModel secModel)
//    {
//        _secModel = secModel;
//    }
//
//    public boolean isExistRoleRelation()
//    {
//        return _existRoleRelation;
//    }
//
//    public void setExistRoleRelation(boolean existRoleRelation)
//    {
//        _existRoleRelation = existRoleRelation;
//    }
//
//    public PersonDocumentTypeSettingsConfig getDocConfig()
//    {
//        return _docConfig;
//    }
//
//    public void setDocConfig(PersonDocumentTypeSettingsConfig docConfig)
//    {
//        _docConfig = docConfig;
//    }

}