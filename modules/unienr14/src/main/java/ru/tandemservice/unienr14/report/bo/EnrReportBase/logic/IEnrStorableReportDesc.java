/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReportBase.logic;

import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrReport;

import java.util.List;

/**
 * @author oleyba
 * @since 5/14/14
 */
public interface IEnrStorableReportDesc
{
    String getReportKey();
    Class<? extends IEnrReport> getReportClass();

    List<String> getPropertyList();

//    String getPermissionKey();
    Class<? extends BusinessComponentManager> getAddFormComponent();
    String getPubTitle();
    String getListTitle();
}
