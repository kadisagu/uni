package ru.tandemservice.unienr14.entrant.daemon;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;

/**
 * @author vdanilov
 */
public interface IEnrEntrantDaemonBean extends INeedPersistenceSupport {

}
