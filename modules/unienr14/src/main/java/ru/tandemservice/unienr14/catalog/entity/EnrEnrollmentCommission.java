package ru.tandemservice.unienr14.catalog.entity;

import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.sec.entity.ILocalRoleContext;
import ru.tandemservice.unienr14.catalog.entity.gen.*;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.EnrEnrollmentCommissionManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.logic.EnrEnrollmentCommissionDao;

import java.util.List;

/**
 * Отборочная комиссия
 */
public class EnrEnrollmentCommission extends EnrEnrollmentCommissionGen implements ILocalRoleContext
{
    @Override
    public String getLocalRoleContextFullTitle()
    {
        return "Отборочная комиссия " + getTitle();
    }


    public static EntityComboDataSourceHandler defaultSelectDSHandler(String name) {
        return new EntityComboDataSourceHandler(name, EnrEnrollmentCommission.class)
                .filter(EnrEnrollmentCommission.title())
                .order(EnrEnrollmentCommission.title());
    }

    public static EntityComboDataSourceHandler permissionSelectDSHandler(String name) {
        return defaultSelectDSHandler(name)
                .customize((alias, dql, context, filter) ->
                        dql.where(DQLExpressions.in(DQLExpressions.property(alias, EnrEnrollmentCommission.id()),
                                EnrEnrollmentCommissionManager.instance().permissionDao().getEnrollmentCommissionIds(UserContext.getInstance().getPrincipalContext())))
                );
    }

    public static EntityComboDataSourceHandler enabledSelectDSHandler(String name) {
        return defaultSelectDSHandler(name)
                .where(EnrEnrollmentCommission.enabled(), Boolean.TRUE);
    }
}