/* $Id:$ */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentOrder.ui.PrintFormSettings;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentOrder.EnrEnrollmentOrderManager;

/**
 * @author oleyba
 * @since 7/7/14
 */
public class EnrEnrollmentOrderPrintFormSettingsUI extends UIPresenter
{
    @Override
    public void onComponentRefresh()
    {
    }

    public void onToggleUsed()
    {
        EnrEnrollmentOrderManager.instance().orderTypeDao().doToggleUsed(getListenerParameterAsLong());
    }

    public void onToggleBasic()
    {
        EnrEnrollmentOrderManager.instance().orderTypeDao().doToggleBasic(getListenerParameterAsLong());
    }

    public void onToggleCommand()
    {
        EnrEnrollmentOrderManager.instance().orderTypeDao().doToggleCommand(getListenerParameterAsLong());
    }

    public void onToggleGroup()
    {
        EnrEnrollmentOrderManager.instance().orderTypeDao().doToggleGroup(getListenerParameterAsLong());
    }

    public void onToggleHeadman()
    {
        EnrEnrollmentOrderManager.instance().orderTypeDao().doToggleHeadman(getListenerParameterAsLong());
    }

    public void onToggleReasonAndBasic()
    {
        EnrEnrollmentOrderManager.instance().orderTypeDao().doToggleReasonAndBasic(getListenerParameterAsLong());
    }

    public void onClickUp()
    {
        CommonManager.instance().commonPriorityDao().doChangePriorityUp(getListenerParameterAsLong(), true);
    }

    public void onClickDown()
    {
        CommonManager.instance().commonPriorityDao().doChangePriorityDown(getListenerParameterAsLong(), true);
    }

    // getters and setters
}