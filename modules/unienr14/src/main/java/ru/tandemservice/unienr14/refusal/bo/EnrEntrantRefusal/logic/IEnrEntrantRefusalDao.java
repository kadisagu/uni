/* $Id:$ */
package ru.tandemservice.unienr14.refusal.bo.EnrEntrantRefusal.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.refusal.entity.EnrEntrantRefusal;

/**
 * @author oleyba
 * @since 5/20/14
 */
public interface IEnrEntrantRefusalDao extends INeedPersistenceSupport
{
    public void saveOrUpdateEntrantRefusal(EnrEntrantRefusal entrantRefusal);

    public void deleteEntrantRefusal(Long id);
}