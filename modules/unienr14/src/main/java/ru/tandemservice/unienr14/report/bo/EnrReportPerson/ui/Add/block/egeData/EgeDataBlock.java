package ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.block.egeData;

import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.entity.IdentifiableWrapper;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unienr14.catalog.entity.EnrStateExamSubject;

import java.util.Arrays;

/**
 * @author Vasily Zhukov
 * @since 08.02.2011
 */
public class EgeDataBlock
{
    // names

    public static final String CERTIFICATE_ISSUANCE_YEAR_DS = "certificateIssuanceYearDS";
    public static final String STATE_EXAM_SUBJECT_DS = "stateExamSubjectDS";

    public static void onBeforeDataSourceFetch(IUIDataSource dataSource, EgeDataParam param)
    {
    }

    public static IDefaultComboDataSourceHandler createCertificateIssuanceYearDS(String name)
    {
        long currentYear = UniBaseUtils.getCurrentYear();

        return new SimpleTitledComboDataSourceHandler(name)
                .addAll(Arrays.asList(new IdentifiableWrapper(currentYear, Long.toString(currentYear)), new IdentifiableWrapper(currentYear - 1, Long.toString(currentYear - 1)), new IdentifiableWrapper(currentYear - 2, Long.toString(currentYear - 2))))
                .filtered(true);
    }

    public static IDefaultComboDataSourceHandler createStateExamSubjectDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, EnrStateExamSubject.class);
    }
}
