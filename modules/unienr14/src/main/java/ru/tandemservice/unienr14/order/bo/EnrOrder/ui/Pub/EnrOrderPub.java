/* $Id:$ */
package ru.tandemservice.unienr14.order.bo.EnrOrder.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.block.BlockListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import ru.tandemservice.unienr14.order.bo.EnrOrder.ui.VisaList.EnrOrderVisaList;

/**
 * @author oleyba
 * @since 5/17/13
 */
@Configuration
public class EnrOrderPub extends BusinessComponentManager
{
    private static final String BLOCK_LIST = "blockList";
    private static final String TAB_LIST = "tabList";


    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .create();
    }
    
    @Bean
    public BlockListExtPoint orderCardPubBlockList() {
        return blockListExtPointBuilder(BLOCK_LIST)
            .create();
    }
    
    @Bean
    public TabPanelExtPoint orderTabList() {
        return tabPanelExtPointBuilder(TAB_LIST)
            .addTab(htmlTab("orderDataTab", "EnrollmentOrderPub_Data").permissionKey("ui:secModel.viewData"))
            .addTab(componentTab("visaList", EnrOrderVisaList.class)
                .parameters("ui:visaListParameters")
                .permissionKey("ui:secModel.viewVisaList").visible("ui:needVising"))
            .addTab(componentTab("visaHistoryList", "ru.tandemservice.unimv.component.visahistory.VisaHistoryList")
                .parameters("ui:visaListParameters")
                .permissionKey("ui:secModel.viewVisaHistoryList").visible("ui:needVising"))
            .create();
    }
}