/* $Id$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubEduDocumentTab.logic;

import com.google.common.collect.Maps;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Transformer;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.bo.Person.util.ISecureRoleContext;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantOriginalDocumentStatus;
import ru.tandemservice.unienr14.entrant.entity.EnrOrganizationOriginalIn;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author azhebko
 * @since 31.07.2014
 */
public class EnrPersonEduDocumentDSHandler extends DefaultSearchDataSourceHandler
{
    public static final String PARAM_ENTRANT = "entrant";
    public static final String PARAM_PERSON_ROLE_CONTEXT = "personRoleContext";

	public static final String VIEW_MAIN_DOCUMENT = "mainDocument";
    public static final String VIEW_ORIGINAL = "original";
    public static final String VIEW_REQUEST_NUMBER = "requestNumber";
    public static final String VIEW_ORG_ORIGINAL_IN = "orgOriginalIn";
    public static final String VIEW_REQ_COMP = "reqComp";

    public EnrPersonEduDocumentDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        EnrEntrant entrant = context.getNotNull(PARAM_ENTRANT);

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(PersonEduDocument.class, "e").column("e")
                .where(eq(property(PersonEduDocument.person().fromAlias("e")), commonValue(entrant.getPerson())))
                .order(property(PersonEduDocument.yearEnd().fromAlias("e")), OrderDirection.desc)
                .order(property(PersonEduDocument.issuanceDate().fromAlias("e")), OrderDirection.desc)
                .joinPath(DQLJoinType.left, PersonEduDocument.eduLevel().fromAlias("e"), "l")
                .order(property(EduLevel.code().fromAlias("l")), OrderDirection.desc)
                .order(property(PersonEduDocument.creationDate().fromAlias("e")), OrderDirection.desc);

        DSOutput output = DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(false).build();
        output.setCountRecord(Math.max(1, output.getRecordList().size()));

        final List<PersonEduDocument> eduDocuments = output.getRecordList();

        final Map<Long, Collection<EnrEntrantRequest>> document2requestMap = SafeMap.get(ArrayList.class);
        final Map<Long, EnrRequestedCompetition> document2reqCompMap = Maps.newHashMap();

        BatchUtils.execute(eduDocuments, DQL.MAX_VALUES_ROW_NUMBER, elements ->
        {
            DQLSelectBuilder reqBuilder = new DQLSelectBuilder().fromEntity(EnrEntrantRequest.class, "er")
                    .where(eq(property("er", EnrEntrantRequest.entrant()), value(entrant)))
                    .where(in(property("er", EnrEntrantRequest.eduDocument()), elements));

            for (EnrEntrantRequest request:  createStatement(reqBuilder).<EnrEntrantRequest>list())
            {
                document2requestMap.get(request.getEduDocument().getId()).add(request);
                if(request.getEduInstDocOriginalRef() != null && request.getEduInstDocOriginalRef().getRequestedCompetition() != null)
                    document2reqCompMap.put(request.getEduDocument().getId(), request.getEduInstDocOriginalRef().getRequestedCompetition());

            }
        });

        DQLSelectBuilder originalBuilder = new DQLSelectBuilder()
                .fromEntity(EnrEntrantOriginalDocumentStatus.class, "o")
                .column(property("o", EnrEntrantOriginalDocumentStatus.eduDocument().id()))
                .distinct()
                .where(in(property("o", EnrEntrantOriginalDocumentStatus.entrant()), entrant))
                .where(in(property("o", EnrEntrantOriginalDocumentStatus.eduDocument()), eduDocuments))
                .where(eq(property("o", EnrEntrantOriginalDocumentStatus.originalDocumentHandedIn()), value(Boolean.TRUE)));

        final Map<Long, EnrOrganizationOriginalIn> document2orgOriginalInMap = Maps.newHashMap();
        for (EnrOrganizationOriginalIn orgOriginalIn : DataAccessServices.dao().getList(EnrOrganizationOriginalIn.class, EnrOrganizationOriginalIn.eduDocument(), eduDocuments))
            document2orgOriginalInMap.put(orgOriginalIn.getEduDocument().getId(), orgOriginalIn);

        final Collection<Long> originalDocumentsIds = new HashSet<>(IUniBaseDao.instance.get().<Long>getList(originalBuilder));

        final Transformer<EnrEntrantRequest, DataWrapper> requestWrapperTransformer = request -> new DataWrapper(request.getEntrant().getId(), request.getRegNumber());

        ISecureRoleContext personRoleContext = context.get(PARAM_PERSON_ROLE_CONTEXT);

        output.transform((PersonEduDocument document) -> {
            DataWrapper wrapper = new DataWrapper(document);
            Collection<EnrEntrantRequest> requests = document2requestMap.get(document.getId());
            EnrRequestedCompetition requestedCompetition = document2reqCompMap.get(document.getId());
            EnrOrganizationOriginalIn organizationOriginalIn = document2orgOriginalInMap.get(document.getId());
            boolean isOriginal = originalDocumentsIds.contains(document.getId());
			boolean isMainDoc = document.getPerson().getMainEduDocument() != null && document.getId().equals(document.getPerson().getMainEduDocument().getId());

			wrapper.setProperty(VIEW_MAIN_DOCUMENT, isMainDoc);
            wrapper.setProperty(VIEW_ORIGINAL, isOriginal);
            wrapper.setProperty(VIEW_REQUEST_NUMBER, requests != null ? CollectionUtils.collect(requests, requestWrapperTransformer) : Collections.emptyList());
            wrapper.setProperty(VIEW_ORG_ORIGINAL_IN, isOriginal ? "" : organizationOriginalIn != null ? organizationOriginalIn.getTitle() : "");
            wrapper.setProperty(VIEW_REQ_COMP, requestedCompetition);

            ParametersMap map = new ParametersMap();
            map.add(PublisherActivator.PUBLISHER_ID_KEY, wrapper.getId());
            map.add("securedObjectId", personRoleContext.getSecuredObject().getId());
            map.add("accessible", personRoleContext.isAccessible());
            map.add("personRoleName", personRoleContext.getPersonRoleName());

            wrapper.put("pubParameters", map);

            return wrapper;
        });

        return output;
    }
}