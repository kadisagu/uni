/* $Id:$ */
package ru.tandemservice.unienr14.settings.bo.EnrCampaignEnrollmentStage.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.util.Icon;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignEnrollmentStage;

/**
 * @author oleyba
 * @since 3/17/14
 */
@Configuration
public class EnrCampaignEnrollmentStageList extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
            .addDataSource(searchListDS("stageDS", stageDSColumns(), stageDSHandler()))
            .create();
    }

    @Bean
    public ColumnListExtPoint stageDSColumns()
    {
        return columnListExtPointBuilder("stageDS")
            .addColumn(textColumn("requestType", EnrCampaignEnrollmentStage.requestType().title()))
            .addColumn(textColumn("competitionType", EnrCampaignEnrollmentStage.competitionType().title()))
            .addColumn(textColumn("programForm", EnrCampaignEnrollmentStage.programForm().title()))
            .addColumn(textColumn("enrollmentDate", EnrCampaignEnrollmentStage.date()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER))
            .addColumn(booleanColumn("crimea", EnrCampaignEnrollmentStage.acceptPeopleResidingInCrimea()))
            .addColumn(actionColumn(EDIT_COLUMN_NAME, new Icon(EDIT_COLUMN_NAME), "onClickEdit"))
            .addColumn(actionColumn(DELETE_COLUMN_NAME, new Icon(DELETE_COLUMN_NAME), "onClickDelete")
                .alert(new FormattedMessage("stageDS.delete.alert", EnrCampaignEnrollmentStage.requestType().title().s(), EnrCampaignEnrollmentStage.competitionType().title().s(), EnrCampaignEnrollmentStage.programForm().shortTitle().s())))
            .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> stageDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrCampaignEnrollmentStage.class)
            .where(EnrCampaignEnrollmentStage.enrollmentCampaign(), EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN)
            .order(EnrCampaignEnrollmentStage.acceptPeopleResidingInCrimea())
            .order(EnrCampaignEnrollmentStage.date())
            .order(EnrCampaignEnrollmentStage.programForm().code())
            .order(EnrCampaignEnrollmentStage.requestType().code())
            .order(EnrCampaignEnrollmentStage.competitionType().code())
            .pageable(false);
    }
}