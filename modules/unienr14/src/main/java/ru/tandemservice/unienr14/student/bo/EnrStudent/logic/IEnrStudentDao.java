/* $Id:$ */
package ru.tandemservice.unienr14.student.bo.EnrStudent.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;

/**
 * @author oleyba
 * @since 8/14/14
 */
public interface IEnrStudentDao extends INeedPersistenceSupport
{
    boolean isEntrantTabVisible(Long studentId);
}