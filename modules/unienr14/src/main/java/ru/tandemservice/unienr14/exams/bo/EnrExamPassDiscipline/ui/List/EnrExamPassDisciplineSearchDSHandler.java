/* $Id$ */
package ru.tandemservice.unienr14.exams.bo.EnrExamPassDiscipline.ui.List;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unienr14.catalog.entity.EnrAbsenceNote;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrExamVariant;
import ru.tandemservice.unienr14.competition.entity.EnrExamVariantPassForm;
import ru.tandemservice.unienr14.exams.bo.EnrExamPassDiscipline.logic.DisciplineAndPassFormWrapper;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroup;
import ru.tandemservice.unienr14.exams.entity.gen.IEnrExamSetElementValueGen;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassAppeal;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.*;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
/**
 * @author nvankov
 * @since 4/6/15
 */
public class EnrExamPassDisciplineSearchDSHandler extends DefaultSearchDataSourceHandler
{
    public static final String PARAM_ENR_COMPETITION_UTIL = EnrCompetitionFilterAddon.class.getSimpleName();

    public EnrExamPassDisciplineSearchDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        EnrEnrollmentCampaign enrollmentCampaign = context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
        OrgUnit orgUnit = context.get(EnrExamPassDisciplineListUI.SETTING_ORG_UNIT);

        if(enrollmentCampaign == null || orgUnit == null)
            return ListOutputBuilder.get(input, Collections.emptyList()).build();

        DisciplineAndPassFormWrapper dds = context.get(EnrExamPassDisciplineListUI.SETTING_ENR_DISCIPLINE);
        Boolean marked = context.get(EnrExamPassDisciplineListUI.SETTING_MARKED);
        Date rateDateFrom = context.get(EnrExamPassDisciplineListUI.SETTING_RATE_DATE_FROM);
        Date rateDateTo = context.get(EnrExamPassDisciplineListUI.SETTING_RATE_DATE_TO);
        EnrAbsenceNote absenceNote = context.get(EnrExamPassDisciplineListUI.SETTING_ABSENCE_NOTE);
        DataWrapper examGroupSet = context.get(EnrExamPassDisciplineListUI.SETTING_EXAM_GROUP_SET);
        List<EnrExamGroup> examGroups = context.get(EnrExamPassDisciplineListUI.PARAM_EXAM_GROUP);

        EnrCompetitionFilterAddon util = context.get(PARAM_ENR_COMPETITION_UTIL);

        final DQLOrderDescriptionRegistry registry = new DQLOrderDescriptionRegistry(EnrExamPassDiscipline.class, "epd");
        registry.setOrders(EnrExamPassDiscipline.examGroup().title().s(), new OrderDescription("eg", EnrExamGroup.title()), new OrderDescription("epd", EnrExamPassDiscipline.entrant().person().identityCard().fullFio()));

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrExamPassDiscipline.class, "epd")
                .joinEntity("epd", DQLJoinType.left, EnrExamPassAppeal.class, "epa", eq(property("epa", EnrExamPassAppeal.examPassDiscipline()), property("epd")))
                .joinPath(DQLJoinType.left, EnrExamPassDiscipline.examGroup().fromAlias("epd"), "eg")
                .column(property("epd"))
                .column(property("epa"))
                .where(eq(property("epd", EnrExamPassDiscipline.entrant().enrollmentCampaign()), commonValue(enrollmentCampaign)))
                .where(eq(property("epd", EnrExamPassDiscipline.territorialOrgUnit()), commonValue(orgUnit)));

        if (dds != null)
        {
            builder.where(eq(property("epd", EnrExamPassDiscipline.discipline()), value(dds.getCampaignDisciplineId())));
            builder.where(eq(property("epd", EnrExamPassDiscipline.passForm()), value(dds.getPassFormId())));
        }

        if (util != null)
        {
            DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(EnrRequestedCompetition.class, "rc");
            subBuilder.joinPath(DQLJoinType.inner, EnrRequestedCompetition.competition().examSetVariant().fromAlias("rc"), "esv");
            subBuilder.joinEntity("rc", DQLJoinType.inner, EnrExamVariant.class, "ev", eq(property("ev", EnrExamVariant.examSetVariant()), property("esv")));
            subBuilder.joinPath(DQLJoinType.inner, EnrExamVariant.examSetElement().value().fromAlias("ev"), "v");
            subBuilder.joinEntity("rc", DQLJoinType.inner, IEnrExamSetElementValue.class, "el", eq(property("el"), property("v")));
            subBuilder.joinEntity("ev", DQLJoinType.inner, EnrExamVariantPassForm.class, "evpf", eq(property("evpf", EnrExamVariantPassForm.examVariant()), property("ev")));

            subBuilder.joinEntity("ev", DQLJoinType.left, EnrCampaignDiscipline.class, "cd", eq(property("cd", EnrCampaignDiscipline.id()), property("el", IEnrExamSetElementValueGen.id())));
            subBuilder.joinEntity("ev", DQLJoinType.left, EnrCampaignDisciplineGroup.class, "cdg", eq(property("cdg", EnrCampaignDisciplineGroup.id()), property("el", IEnrExamSetElementValueGen.id())));

            subBuilder.joinEntity("cdg", DQLJoinType.left, EnrCampaignDisciplineGroupElement.class, "cdge", eq(property("cdge", EnrCampaignDisciplineGroupElement.group()), property("cdg")));
            subBuilder.joinPath(DQLJoinType.left, EnrCampaignDisciplineGroupElement.discipline().fromAlias("cdge"), "gd");

            subBuilder.where(or(
                    eq(property("epd", EnrExamPassDiscipline.discipline()), property("cd")),
                    eq(property("epd", EnrExamPassDiscipline.discipline()), property("gd"))
            ));

            subBuilder.where(in(property("rc", EnrRequestedCompetition.competition()), util.getEntityIdsFilteredBuilder("a").buildQuery()));
            subBuilder.where(eq(property("rc", EnrRequestedCompetition.request().entrant()), property("epd", EnrExamPassDiscipline.entrant())));

            builder.where(exists(subBuilder.buildQuery()));
        }

        if(marked != null)
        {
            if(marked)
                builder.where(or(
                        isNotNull(property("epd", EnrExamPassDiscipline.markAsLong())),
                        isNotNull(property("epd", EnrExamPassDiscipline.absenceNote()))
                ));
            else
                builder.where(and(
                        isNull(property("epd", EnrExamPassDiscipline.markAsLong())),
                        isNull(property("epd", EnrExamPassDiscipline.absenceNote()))
                ));
        }

        builder.where(betweenDays(EnrExamPassDiscipline.markDate().fromAlias("epd"), rateDateFrom, rateDateTo));

        if(absenceNote != null)
            builder.where(eq(property("epd", EnrExamPassDiscipline.absenceNote()), commonValue(absenceNote)));

        if(examGroupSet != null)
            builder.where(eq(property("epd", EnrExamPassDiscipline.examGroup().examGroupSet().id()), value(examGroupSet.getId())));

        if(CollectionUtils.isNotEmpty(examGroups))
            builder.where(in(property("epd", EnrExamPassDiscipline.examGroup()), examGroups));

        registry.applyOrder(builder, input.getEntityOrder());

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build().transform((Object[] item) -> {
            ViewWrapper<EnrExamPassDiscipline> wrapper = new ViewWrapper<>((EnrExamPassDiscipline) item[0]);
            if(item[1] != null)
                wrapper.setViewProperty("appeal", ((EnrExamPassAppeal)item[1]).getMarkAsDouble());
            else
                wrapper.setViewProperty("appeal", null);
            return wrapper;
        });
    }
}
