package ru.tandemservice.unienr14.settings.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.catalog.entity.EnrDiscipline;
import ru.tandemservice.unienr14.catalog.entity.EnrStateExamSubject;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.IEnrExamSetElementValue;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Дисциплина, используемая в рамках ПК
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrCampaignDisciplineGen extends EntityBase
 implements IEnrExamSetElementValue, INaturalIdentifiable<EnrCampaignDisciplineGen>, ILazyFieldOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline";
    public static final String ENTITY_NAME = "enrCampaignDiscipline";
    public static final int VERSION_HASH = 929014673;
    private static IEntityMeta ENTITY_META;

    public static final String L_DISCIPLINE = "discipline";
    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String L_STATE_EXAM_SUBJECT = "stateExamSubject";
    public static final String P_DEFAULT_PASS_MARK_AS_LONG = "defaultPassMarkAsLong";
    public static final String P_TITLE = "title";

    private EnrDiscipline _discipline;     // Дисциплина
    private EnrEnrollmentCampaign _enrollmentCampaign;     // ПК
    private EnrStateExamSubject _stateExamSubject;     // Предмет ЕГЭ
    private long _defaultPassMarkAsLong;     // Зачетный балл по умолчанию

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дисциплина. Свойство не может быть null.
     */
    @NotNull
    public EnrDiscipline getDiscipline()
    {
        return _discipline;
    }

    /**
     * @param discipline Дисциплина. Свойство не может быть null.
     */
    public void setDiscipline(EnrDiscipline discipline)
    {
        dirty(_discipline, discipline);
        _discipline = discipline;
    }

    /**
     * @return ПК. Свойство не может быть null.
     */
    @NotNull
    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign ПК. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Предмет ЕГЭ.
     */
    public EnrStateExamSubject getStateExamSubject()
    {
        return _stateExamSubject;
    }

    /**
     * @param stateExamSubject Предмет ЕГЭ.
     */
    public void setStateExamSubject(EnrStateExamSubject stateExamSubject)
    {
        dirty(_stateExamSubject, stateExamSubject);
        _stateExamSubject = stateExamSubject;
    }

    /**
     * Хранится со смещением в три знака для дробной части.
     *
     * @return Зачетный балл по умолчанию. Свойство не может быть null.
     *
     * Это формула "discipline.defaultPassMarkAsLong".
     */
    // @NotNull
    public long getDefaultPassMarkAsLong()
    {
        initLazyForGet("defaultPassMarkAsLong");
        return _defaultPassMarkAsLong;
    }

    /**
     * @param defaultPassMarkAsLong Зачетный балл по умолчанию. Свойство не может быть null.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setDefaultPassMarkAsLong(long defaultPassMarkAsLong)
    {
        initLazyForSet("defaultPassMarkAsLong");
        dirty(_defaultPassMarkAsLong, defaultPassMarkAsLong);
        _defaultPassMarkAsLong = defaultPassMarkAsLong;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrCampaignDisciplineGen)
        {
            if (withNaturalIdProperties)
            {
                setDiscipline(((EnrCampaignDiscipline)another).getDiscipline());
                setEnrollmentCampaign(((EnrCampaignDiscipline)another).getEnrollmentCampaign());
            }
            setStateExamSubject(((EnrCampaignDiscipline)another).getStateExamSubject());
            setDefaultPassMarkAsLong(((EnrCampaignDiscipline)another).getDefaultPassMarkAsLong());
        }
    }

    public INaturalId<EnrCampaignDisciplineGen> getNaturalId()
    {
        return new NaturalId(getDiscipline(), getEnrollmentCampaign());
    }

    public static class NaturalId extends NaturalIdBase<EnrCampaignDisciplineGen>
    {
        private static final String PROXY_NAME = "EnrCampaignDisciplineNaturalProxy";

        private Long _discipline;
        private Long _enrollmentCampaign;

        public NaturalId()
        {}

        public NaturalId(EnrDiscipline discipline, EnrEnrollmentCampaign enrollmentCampaign)
        {
            _discipline = ((IEntity) discipline).getId();
            _enrollmentCampaign = ((IEntity) enrollmentCampaign).getId();
        }

        public Long getDiscipline()
        {
            return _discipline;
        }

        public void setDiscipline(Long discipline)
        {
            _discipline = discipline;
        }

        public Long getEnrollmentCampaign()
        {
            return _enrollmentCampaign;
        }

        public void setEnrollmentCampaign(Long enrollmentCampaign)
        {
            _enrollmentCampaign = enrollmentCampaign;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrCampaignDisciplineGen.NaturalId) ) return false;

            EnrCampaignDisciplineGen.NaturalId that = (NaturalId) o;

            if( !equals(getDiscipline(), that.getDiscipline()) ) return false;
            if( !equals(getEnrollmentCampaign(), that.getEnrollmentCampaign()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getDiscipline());
            result = hashCode(result, getEnrollmentCampaign());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getDiscipline());
            sb.append("/");
            sb.append(getEnrollmentCampaign());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrCampaignDisciplineGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrCampaignDiscipline.class;
        }

        public T newInstance()
        {
            return (T) new EnrCampaignDiscipline();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "discipline":
                    return obj.getDiscipline();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "stateExamSubject":
                    return obj.getStateExamSubject();
                case "defaultPassMarkAsLong":
                    return obj.getDefaultPassMarkAsLong();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "discipline":
                    obj.setDiscipline((EnrDiscipline) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrEnrollmentCampaign) value);
                    return;
                case "stateExamSubject":
                    obj.setStateExamSubject((EnrStateExamSubject) value);
                    return;
                case "defaultPassMarkAsLong":
                    obj.setDefaultPassMarkAsLong((Long) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "discipline":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "stateExamSubject":
                        return true;
                case "defaultPassMarkAsLong":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "discipline":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "stateExamSubject":
                    return true;
                case "defaultPassMarkAsLong":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "discipline":
                    return EnrDiscipline.class;
                case "enrollmentCampaign":
                    return EnrEnrollmentCampaign.class;
                case "stateExamSubject":
                    return EnrStateExamSubject.class;
                case "defaultPassMarkAsLong":
                    return Long.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrCampaignDiscipline> _dslPath = new Path<EnrCampaignDiscipline>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrCampaignDiscipline");
    }
            

    /**
     * @return Дисциплина. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline#getDiscipline()
     */
    public static EnrDiscipline.Path<EnrDiscipline> discipline()
    {
        return _dslPath.discipline();
    }

    /**
     * @return ПК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline#getEnrollmentCampaign()
     */
    public static EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Предмет ЕГЭ.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline#getStateExamSubject()
     */
    public static EnrStateExamSubject.Path<EnrStateExamSubject> stateExamSubject()
    {
        return _dslPath.stateExamSubject();
    }

    /**
     * Хранится со смещением в три знака для дробной части.
     *
     * @return Зачетный балл по умолчанию. Свойство не может быть null.
     *
     * Это формула "discipline.defaultPassMarkAsLong".
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline#getDefaultPassMarkAsLong()
     */
    public static PropertyPath<Long> defaultPassMarkAsLong()
    {
        return _dslPath.defaultPassMarkAsLong();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EnrCampaignDiscipline> extends EntityPath<E>
    {
        private EnrDiscipline.Path<EnrDiscipline> _discipline;
        private EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> _enrollmentCampaign;
        private EnrStateExamSubject.Path<EnrStateExamSubject> _stateExamSubject;
        private PropertyPath<Long> _defaultPassMarkAsLong;
        private SupportedPropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дисциплина. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline#getDiscipline()
     */
        public EnrDiscipline.Path<EnrDiscipline> discipline()
        {
            if(_discipline == null )
                _discipline = new EnrDiscipline.Path<EnrDiscipline>(L_DISCIPLINE, this);
            return _discipline;
        }

    /**
     * @return ПК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline#getEnrollmentCampaign()
     */
        public EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Предмет ЕГЭ.
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline#getStateExamSubject()
     */
        public EnrStateExamSubject.Path<EnrStateExamSubject> stateExamSubject()
        {
            if(_stateExamSubject == null )
                _stateExamSubject = new EnrStateExamSubject.Path<EnrStateExamSubject>(L_STATE_EXAM_SUBJECT, this);
            return _stateExamSubject;
        }

    /**
     * Хранится со смещением в три знака для дробной части.
     *
     * @return Зачетный балл по умолчанию. Свойство не может быть null.
     *
     * Это формула "discipline.defaultPassMarkAsLong".
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline#getDefaultPassMarkAsLong()
     */
        public PropertyPath<Long> defaultPassMarkAsLong()
        {
            if(_defaultPassMarkAsLong == null )
                _defaultPassMarkAsLong = new PropertyPath<Long>(EnrCampaignDisciplineGen.P_DEFAULT_PASS_MARK_AS_LONG, this);
            return _defaultPassMarkAsLong;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(EnrCampaignDisciplineGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EnrCampaignDiscipline.class;
        }

        public String getEntityName()
        {
            return "enrCampaignDiscipline";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getTitle();
}
