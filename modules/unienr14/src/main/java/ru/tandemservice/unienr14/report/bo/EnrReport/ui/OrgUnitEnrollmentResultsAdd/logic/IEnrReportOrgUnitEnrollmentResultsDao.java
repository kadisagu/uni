/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.OrgUnitEnrollmentResultsAdd.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.OrgUnitEnrollmentResultsAdd.EnrReportOrgUnitEnrollmentResultsAddUI;

/**
 * @author rsizonenko
 * @since 21.07.2014
 */
public interface IEnrReportOrgUnitEnrollmentResultsDao extends INeedPersistenceSupport {
    long createReport(EnrReportOrgUnitEnrollmentResultsAddUI model);
}
