/* $Id:$ */
package ru.tandemservice.unienr14.report.bo.EnrReport.ui.StateExamAverageScoreAdd.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.StateExamAverageScoreAdd.EnrReportStateExamAverageScoreAddUI;

/**
 * @author rsizonenko
 * @since 30.06.2014
 */
public interface IEnrReportStateExamAverageScoreDao extends INeedPersistenceSupport {
    public long createReport(EnrReportStateExamAverageScoreAddUI model);
}
