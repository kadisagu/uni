package ru.tandemservice.unienr14.migration;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.ISQLTranslator;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLSelectQuery;
import ru.tandemservice.uni.UniDefines;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;
import java.util.Map;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_2x6x3_5to6 extends IndependentMigrationScript
{

    protected final Logger logger = Logger.getLogger(this.getClass());

    private final FileAppender appender = this.initAppender(this.logger);

    private FileAppender initAppender(final Logger logger) {
        try {
            final String path = ApplicationRuntime.getAppInstallPath();
            final FileAppender appender = new FileAppender(new PatternLayout(), FilenameUtils.concat(path, "tomcat/logs/programSetOrgUnit-update.log"));
            appender.setName("programSetOrgUnit-update-appender");
            appender.setThreshold(Level.INFO);
            logger.addAppender(appender);
            return appender;
        } catch (final Throwable t) {
            logger.error(t.getMessage(), t);
            return null;
        }
    }

    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.3")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrProgramSetOrgUnit

		//  свойство formativeOrgUnit стало обязательным
        if(tool.tableExists("enr14_program_set_ou_t"))
		{
            ISQLTranslator translator = tool.getDialect().getSQLTranslator();

            SQLSelectQuery selectTopOrgUnitQuery = new SQLSelectQuery().from(SQLFrom.table("toporgunit_t", "tour"));
            selectTopOrgUnitQuery.column("tour.id", "id");
            selectTopOrgUnitQuery.top(1);
            Statement touStatement = tool.getConnection().createStatement();
            touStatement.execute(translator.toSql(selectTopOrgUnitQuery));
            ResultSet touResultSet = touStatement.getResultSet();
            Long topOu = null;
            while (touResultSet.next())
            {
                topOu = touResultSet.getLong("id");
            }


            SQLSelectQuery selectOrgUnitQuery = new SQLSelectQuery().from(SQLFrom.table("orgunittokindrelation_t", "our").
                            innerJoin(SQLFrom.table("orgunitkind_t", "ouk"), "our.orgunitkind_id=ouk.id").
                            innerJoin(SQLFrom.table("orgunit_t", "ou"), "our.orgunit_id=ou.id")
            );
//            String CATALOG_ORGUNIT_KIND_FORMING = "2";
            selectOrgUnitQuery.column("our.orgunit_id", "ouid");
            selectOrgUnitQuery.column("ou.title_p", "outitle");
            selectOrgUnitQuery.where("ouk.code_p = '2'");
            selectOrgUnitQuery.order("ou.title_p");

            Statement ouStatement = tool.getConnection().createStatement();
            ouStatement.execute(translator.toSql(selectOrgUnitQuery));

            ResultSet ouResultSet = ouStatement.getResultSet();
            List<Long> formOus = Lists.newArrayList();
            Long ouId = null;
            Map<Long, String> titleMap = Maps.newHashMap();
            while (ouResultSet.next())
            {
                Long id = ouResultSet.getLong("ouid");
                String title = ouResultSet.getString("outitle");
                titleMap.put(id, title);
                if(id.equals(topOu))
                {
                    ouId = topOu;
                    break;
                }
                else
                {
                    formOus.add(id);
                }
            }

            if(ouId == null)
            {
                ouId = formOus.get(0);
            }
            String ouTitle = titleMap.get(ouId);


            SQLSelectQuery selectQuery = new SQLSelectQuery().from(SQLFrom.table("enr14_program_set_ou_t", "pou").innerJoin(SQLFrom.table("enr14_program_set_base_t", "psb"), "psb.id=pou.programset_id"));
            selectQuery.column("pou.id", "id");
            selectQuery.column("pou.programset_id", "psetid");
            selectQuery.column("psb.title_p", "psbtitle");
            selectQuery.where("formativeorgunit_id is null");

            Statement statement = tool.getConnection().createStatement();
            statement.execute(translator.toSql(selectQuery));

            ResultSet resultSet = statement.getResultSet();

            logger.setLevel(Level.INFO);

            while (resultSet.next())
            {
                Long id = resultSet.getLong("id");
                Long psetId = resultSet.getLong("psetid");
                String psetTitle = resultSet.getString("psbtitle");
                logger.info(psetId + " (" + psetTitle + ") - " + ouTitle);

                tool.executeUpdate("update enr14_program_set_ou_t set formativeorgunit_id=? where id=?", ouId, id);
            }

			// сделать колонку NOT NULL
			tool.setColumnNullable("enr14_program_set_ou_t", "formativeorgunit_id", false);
		}
    }
}