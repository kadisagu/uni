/* $Id$ */
package ru.tandemservice.unienr14.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.PreparedStatement;

/**
 * @author Ekaterina Zvereva
 * @since 19.05.2016
 */
public class MS_unienr14_2x10x1_6to7 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.10.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        String description="Дети сотрудников органов внутренних дел, учреждений и органов уголовно-исполнительной системы, федеральной противопожарной службы Государственной противопожарной службы, органов по контролю за оборотом наркотических средств и психотропных веществ, таможенных органов, Следственного комитета Российской Федерации, погибших (умерших) вследствие увечья или иного повреждения здоровья, полученных ими в связи с выполнением служебных обязанностей, либо вследствие заболевания, полученного ими в период прохождения службы в указанных учреждениях и органах, и дети, находившиеся на их иждивении";
        PreparedStatement update = tool.prepareStatement("update enr14_c_benefit_category_t set description_p = ? where code_p=?");
        update.setString(1, description);
        update.setString(2, "015");
        update.executeUpdate();
    }
}