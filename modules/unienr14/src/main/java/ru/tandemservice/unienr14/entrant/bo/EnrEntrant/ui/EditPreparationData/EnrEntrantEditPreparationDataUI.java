/**
 *$Id: EnrEntrantEditPreparationDataUI.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.EditPreparationData;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.unienr14.catalog.entity.EnrAccessCourse;
import ru.tandemservice.unienr14.catalog.entity.EnrAccessDepartment;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantAccessCourse;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantAccessDepartment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Alexander Shaburov
 * @since 19.08.13
 */
@Input({
        @Bind(key = IUIPresenter.PUBLISHER_ID, binding = "entityHolder.id")
})
public class EnrEntrantEditPreparationDataUI extends UIPresenter
{
    private EntityHolder<EnrEntrant> _entityHolder = new EntityHolder<>();

    private List<EnrAccessCourse> _accessCourseList;
    private List<EnrAccessDepartment> _accessDepartmentList;

    @Override
    public void onComponentRefresh()
    {
        _entityHolder.refresh(EnrEntrant.class);

        _accessCourseList = new ArrayList<>();
        for (EnrEntrantAccessCourse item : DataAccessServices.dao().getList(EnrEntrantAccessCourse.class, EnrEntrantAccessCourse.L_ENTRANT, _entityHolder.getValue()))
            _accessCourseList.add(item.getCourse());
        Collections.sort(_accessCourseList, ITitled.TITLED_COMPARATOR);

        _accessDepartmentList = new ArrayList<>();
        for (EnrEntrantAccessDepartment item : DataAccessServices.dao().getList(EnrEntrantAccessDepartment.class, EnrEntrantAccessDepartment.L_ENTRANT, _entityHolder.getValue()))
            _accessDepartmentList.add(item.getAccessDepartment());
        Collections.sort(_accessDepartmentList, ITitled.TITLED_COMPARATOR);
    }

    public void onClickApply()
    {
        EnrEntrantManager.instance().dao().updatePreparationData(_entityHolder.getValue(), _accessCourseList, _accessDepartmentList);
        deactivate();
    }

    // Accessors

    public List<EnrAccessCourse> getAccessCourseList()
    {
        return _accessCourseList;
    }

    public void setAccessCourseList(List<EnrAccessCourse> accessCourseList)
    {
        _accessCourseList = accessCourseList;
    }

    public List<EnrAccessDepartment> getAccessDepartmentList()
    {
        return _accessDepartmentList;
    }

    public void setAccessDepartmentList(List<EnrAccessDepartment> accessDepartmentList)
    {
        _accessDepartmentList = accessDepartmentList;
    }

    public EntityHolder<EnrEntrant> getEntityHolder()
    {
        return _entityHolder;
    }

    public void setEntityHolder(EntityHolder<EnrEntrant> entityHolder)
    {
        _entityHolder = entityHolder;
    }
}
