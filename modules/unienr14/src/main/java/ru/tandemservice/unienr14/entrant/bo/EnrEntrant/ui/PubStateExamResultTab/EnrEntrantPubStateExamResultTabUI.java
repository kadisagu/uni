/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubStateExamResultTab;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.logic.EnrEntrantStateExamResultDSHandler;
import ru.tandemservice.unienr14.exams.bo.EnrStateExamResult.ui.AddEdit.EnrStateExamResultAddEdit;
import ru.tandemservice.unienr14.exams.bo.EnrStateExamResult.ui.AddEdit.EnrStateExamResultAddEditUI;
import ru.tandemservice.unienr14.exams.bo.EnrStateExamResult.EnrStateExamResultManager;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult;

/**
 * @author oleyba
 * @since 4/12/14
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "entrant.id")
})
public class EnrEntrantPubStateExamResultTabUI extends UIPresenter
{
    private EnrEntrant entrant = new EnrEntrant();

    @Override
    public void onComponentRefresh()
    {
        setEntrant(IUniBaseDao.instance.get().getNotNull(EnrEntrant.class, getEntrant().getId()));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrEntrantStateExamResultDSHandler.PARAM_ENTRANT, getEntrant());
    }

    public void onClickAdd()
    {
        _uiActivation.asRegionDialog(EnrStateExamResultAddEdit.class.getSimpleName())
            .parameter(EnrStateExamResultAddEditUI.BIND_ENTRANT, getEntrant().getId())
            .parameter(EnrStateExamResultAddEditUI.BIND_RESULT, null)
            .parameter("entrantPub", true)
            .activate();
    }

    public void onClickEdit()
    {
        final EnrEntrantStateExamResult entity = getEntityByListenerParameterAsLong();

        getActivationBuilder().asRegionDialog(EnrStateExamResultAddEdit.class)
                .parameter(EnrStateExamResultAddEditUI.BIND_ENTRANT, entity.getEntrant().getId())
                .parameter(EnrStateExamResultAddEditUI.BIND_RESULT, entity.getId())
                .parameter("entrantPub", true)
                .activate();
    }

    public void onToggleAccepted()
    {
        EnrStateExamResultManager.instance().dao().doChangeResultChecked(getListenerParameterAsLong());
    }

    public void onToggleVerified()
    {
        EnrStateExamResultManager.instance().dao().doChangeResultVerifiedByUser(getListenerParameterAsLong());
    }

    public void onClickDelete()
    {
        IUniBaseDao.instance.get().delete(getListenerParameterAsLong());
    }

    // getters and setters

    public EnrEntrant getEntrant()
    {
        return entrant;
    }

    public void setEntrant(EnrEntrant entrant)
    {
        this.entrant = entrant;
    }

    public String getEditAlert()
    {
        EnrEntrantStateExamResult result = _uiConfig.getDataSource("stateExamResultDS").getCurrent();
        if(result != null && !StringUtils.isEmpty(result.getStateCheckStatus()))
        {
            return "При изменении балла или номера свидетельства для результата ЕГЭ будет сброшен «Статус проверки в ФИС». Продолжить редактирование?";
        }
        return null;
    }
}
