package ru.tandemservice.unienr14.report.entity;

import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EnrollmentCampaignPassSummaryAdd.EnrReportEnrollmentCampaignPassSummaryAdd;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrReport;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrStorableReportDesc;
import ru.tandemservice.unienr14.report.entity.gen.EnrReportEnrollmentCampaignPassSummaryGen;

import java.util.Arrays;
import java.util.List;

/**
 * Сводка о ходе приемной кампании
 */
public class EnrReportEnrollmentCampaignPassSummary extends EnrReportEnrollmentCampaignPassSummaryGen implements IEnrReport
{
    public static final String REPORT_KEY = "enr14ReportEnrollmentCampaignPassSummary";

    @SuppressWarnings("unchecked")
    private static List<String> properties = Arrays.asList(
            P_STAGE,
            P_REQUEST_TYPE,
            P_COMPENSATION_TYPE,
            P_PROGRAM_FORM,
            P_COMPETITION_TYPE,
            P_ENR_ORG_UNIT,
            P_FORMATIVE_ORG_UNIT,
            P_PROGRAM_SUBJECT,
            P_EDU_PROGRAM,
            P_PROGRAM_SET,
            P_PARALLEL,
            P_ENROLLMENT_COMMISSION,
            P_FIRST_PRIORITY_ONLY,
            P_SKIP_EMPTY,
            P_ROW_FORMING_TYPE);

    public static IEnrStorableReportDesc getDescription() {
        return new IEnrStorableReportDesc() {
            @Override public String getReportKey() { return REPORT_KEY; }
            @Override public Class<? extends IEnrReport> getReportClass() { return EnrReportEnrollmentCampaignPassSummary.class; }
            @Override public List<String> getPropertyList() { return properties; }

            @Override public Class<? extends BusinessComponentManager> getAddFormComponent() { return EnrReportEnrollmentCampaignPassSummaryAdd.class; }
            @Override public String getPubTitle() { return "Отчет «Сводка о ходе приемной кампании»"; }
            @Override public String getListTitle() { return "Список отчетов «Сводка о ходе приемной кампании»"; }
        };
    }

    @Override
    public IEnrStorableReportDesc getDesc() {
        return getDescription();
    }

    @Override
    public String getPeriodTitle() {
        return "с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateFrom()) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateTo());
    }
}