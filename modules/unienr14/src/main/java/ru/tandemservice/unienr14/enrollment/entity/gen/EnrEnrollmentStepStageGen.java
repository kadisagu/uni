package ru.tandemservice.unienr14.enrollment.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepStage;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignEnrollmentStage;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Стадия зачисления, включенная в шаг
 *
 * Фиксирует стадии зачисления, включенные в данный шаг зачисления.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrEnrollmentStepStageGen extends EntityBase
 implements INaturalIdentifiable<EnrEnrollmentStepStageGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepStage";
    public static final String ENTITY_NAME = "enrEnrollmentStepStage";
    public static final int VERSION_HASH = 550402150;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_STEP = "enrollmentStep";
    public static final String L_ENROLLMENT_STAGE = "enrollmentStage";

    private EnrEnrollmentStep _enrollmentStep;     // Шаг
    private EnrCampaignEnrollmentStage _enrollmentStage;     // Стадия зачисления

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Шаг. Свойство не может быть null.
     */
    @NotNull
    public EnrEnrollmentStep getEnrollmentStep()
    {
        return _enrollmentStep;
    }

    /**
     * @param enrollmentStep Шаг. Свойство не может быть null.
     */
    public void setEnrollmentStep(EnrEnrollmentStep enrollmentStep)
    {
        dirty(_enrollmentStep, enrollmentStep);
        _enrollmentStep = enrollmentStep;
    }

    /**
     * @return Стадия зачисления. Свойство не может быть null.
     */
    @NotNull
    public EnrCampaignEnrollmentStage getEnrollmentStage()
    {
        return _enrollmentStage;
    }

    /**
     * @param enrollmentStage Стадия зачисления. Свойство не может быть null.
     */
    public void setEnrollmentStage(EnrCampaignEnrollmentStage enrollmentStage)
    {
        dirty(_enrollmentStage, enrollmentStage);
        _enrollmentStage = enrollmentStage;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrEnrollmentStepStageGen)
        {
            if (withNaturalIdProperties)
            {
                setEnrollmentStep(((EnrEnrollmentStepStage)another).getEnrollmentStep());
                setEnrollmentStage(((EnrEnrollmentStepStage)another).getEnrollmentStage());
            }
        }
    }

    public INaturalId<EnrEnrollmentStepStageGen> getNaturalId()
    {
        return new NaturalId(getEnrollmentStep(), getEnrollmentStage());
    }

    public static class NaturalId extends NaturalIdBase<EnrEnrollmentStepStageGen>
    {
        private static final String PROXY_NAME = "EnrEnrollmentStepStageNaturalProxy";

        private Long _enrollmentStep;
        private Long _enrollmentStage;

        public NaturalId()
        {}

        public NaturalId(EnrEnrollmentStep enrollmentStep, EnrCampaignEnrollmentStage enrollmentStage)
        {
            _enrollmentStep = ((IEntity) enrollmentStep).getId();
            _enrollmentStage = ((IEntity) enrollmentStage).getId();
        }

        public Long getEnrollmentStep()
        {
            return _enrollmentStep;
        }

        public void setEnrollmentStep(Long enrollmentStep)
        {
            _enrollmentStep = enrollmentStep;
        }

        public Long getEnrollmentStage()
        {
            return _enrollmentStage;
        }

        public void setEnrollmentStage(Long enrollmentStage)
        {
            _enrollmentStage = enrollmentStage;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrEnrollmentStepStageGen.NaturalId) ) return false;

            EnrEnrollmentStepStageGen.NaturalId that = (NaturalId) o;

            if( !equals(getEnrollmentStep(), that.getEnrollmentStep()) ) return false;
            if( !equals(getEnrollmentStage(), that.getEnrollmentStage()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEnrollmentStep());
            result = hashCode(result, getEnrollmentStage());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEnrollmentStep());
            sb.append("/");
            sb.append(getEnrollmentStage());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrEnrollmentStepStageGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrEnrollmentStepStage.class;
        }

        public T newInstance()
        {
            return (T) new EnrEnrollmentStepStage();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrollmentStep":
                    return obj.getEnrollmentStep();
                case "enrollmentStage":
                    return obj.getEnrollmentStage();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrollmentStep":
                    obj.setEnrollmentStep((EnrEnrollmentStep) value);
                    return;
                case "enrollmentStage":
                    obj.setEnrollmentStage((EnrCampaignEnrollmentStage) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrollmentStep":
                        return true;
                case "enrollmentStage":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrollmentStep":
                    return true;
                case "enrollmentStage":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrollmentStep":
                    return EnrEnrollmentStep.class;
                case "enrollmentStage":
                    return EnrCampaignEnrollmentStage.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrEnrollmentStepStage> _dslPath = new Path<EnrEnrollmentStepStage>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrEnrollmentStepStage");
    }
            

    /**
     * @return Шаг. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepStage#getEnrollmentStep()
     */
    public static EnrEnrollmentStep.Path<EnrEnrollmentStep> enrollmentStep()
    {
        return _dslPath.enrollmentStep();
    }

    /**
     * @return Стадия зачисления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepStage#getEnrollmentStage()
     */
    public static EnrCampaignEnrollmentStage.Path<EnrCampaignEnrollmentStage> enrollmentStage()
    {
        return _dslPath.enrollmentStage();
    }

    public static class Path<E extends EnrEnrollmentStepStage> extends EntityPath<E>
    {
        private EnrEnrollmentStep.Path<EnrEnrollmentStep> _enrollmentStep;
        private EnrCampaignEnrollmentStage.Path<EnrCampaignEnrollmentStage> _enrollmentStage;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Шаг. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepStage#getEnrollmentStep()
     */
        public EnrEnrollmentStep.Path<EnrEnrollmentStep> enrollmentStep()
        {
            if(_enrollmentStep == null )
                _enrollmentStep = new EnrEnrollmentStep.Path<EnrEnrollmentStep>(L_ENROLLMENT_STEP, this);
            return _enrollmentStep;
        }

    /**
     * @return Стадия зачисления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepStage#getEnrollmentStage()
     */
        public EnrCampaignEnrollmentStage.Path<EnrCampaignEnrollmentStage> enrollmentStage()
        {
            if(_enrollmentStage == null )
                _enrollmentStage = new EnrCampaignEnrollmentStage.Path<EnrCampaignEnrollmentStage>(L_ENROLLMENT_STAGE, this);
            return _enrollmentStage;
        }

        public Class getEntityClass()
        {
            return EnrEnrollmentStepStage.class;
        }

        public String getEntityName()
        {
            return "enrEnrollmentStepStage";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
