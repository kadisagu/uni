/* $Id$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrant.logic;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantCustomStateType;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantState;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.List.EnrEntrantList;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.List.EnrEntrantListUI;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantCustomState;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.EnrEnrollmentCommissionManager;

import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author azhebko
 * @since 01.05.2014
 */
public class EnrEntrantDSHandler extends DefaultSearchDataSourceHandler
{
    public static final String PARAM_ENR_COMPETITION_UTIL = EnrCompetitionFilterAddon.class.getSimpleName();
    public static final String PARAM_ORG_UNIT = "orgUnit";

    public static final String VIEW_PROP_REQUEST = "entrantRequestsWrapperList";
    public static final String VIEW_PROP_CUSTOM_STATES = "entrantCustomStates";

    protected static final String ENTRANT_ALIAS = "e";

    public EnrEntrantDSHandler(String ownerId)
    {
        super(ownerId);
    }


    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        DQLSelectBuilder builder = getEntrantDQL(context);

        DQLOrderDescriptionRegistry orderDescriptionRegistry = new DQLOrderDescriptionRegistry(EnrEntrant.class, ENTRANT_ALIAS);
        orderDescriptionRegistry
                .setOrders("personalNumber", new OrderDescription(EnrEntrant.personalNumber()))
                .setOrders("entrant", new OrderDescription(PersonRole.person().identityCard().fullFio()))
                .setOrders("sex", new OrderDescription(PersonRole.person().identityCard().sex().shortTitle()))
                .setOrders("birthDate", new OrderDescription(PersonRole.person().identityCard().birthDate()))
                .setOrders("registrationDate", new OrderDescription(EnrEntrant.registrationDate()));

        orderDescriptionRegistry.applyOrder(builder, input.getEntityOrder());

        final DSOutput output = DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build();
        final List<Long> entrantIds = output.getRecordIds();

        final Multimap<Long, DataWrapper> entrant2RequestsMap = ArrayListMultimap.create(entrantIds.size(), 2);
        final Multimap<Long, EnrEntrantCustomState> entrantActiveCustomStateMap = ArrayListMultimap.create(entrantIds.size(), 2);
        fillRequestAndCustomStateMap(entrantIds, entrant2RequestsMap, entrantActiveCustomStateMap, context);

        return output.transform((EnrEntrant entrant) -> {
            ViewWrapper<EnrEntrant> wrapper = new ViewWrapper<>(entrant);
            wrapper.setViewProperty(VIEW_PROP_REQUEST, entrant2RequestsMap.get(wrapper.getEntity().getId()));
            wrapper.setViewProperty("status", !wrapper.getEntity().isArchival());
            wrapper.setViewProperty(VIEW_PROP_CUSTOM_STATES, entrantActiveCustomStateMap.get(wrapper.getEntity().getId()));
            return wrapper;
        });
    }

    protected DQLSelectBuilder getEntrantDQL(ExecutionContext context)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EnrEntrant.class, ENTRANT_ALIAS)
                .column(ENTRANT_ALIAS)
                .joinPath(DQLJoinType.inner, EnrEntrant.person().fromAlias(ENTRANT_ALIAS), "person")
                .joinPath(DQLJoinType.inner, Person.identityCard().fromAlias("person"), "idc");

        IDataSettings settings = context.get("settings");
        Object enrollmentCampaign = context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);
        Object enrollmentCommission = context.get(EnrEntrantList.PARAM_ENROLLMENT_COMMISSION);
        String lastName = settings.get("lastNameFilter");
        String firstName = settings.get("firstNameFilter");
        String middleName = settings.get("middleNameFilter");
        String personalNumber = settings.get("personalNumberFilter");
        String requestNumber = settings.get("requestNumberFilter");
        Date dateFrom = settings.get("registeredFromFilter");
        Date dateTo = settings.get("registeredToFilter");
        Long citizenshipFilter = settings.get("citizenshipFilter");
        String cardSeria = settings.get("identityCardSeriaFilter");
        String cardNumber = settings.get("identityCardNumberFilter");
        List<EnrEntrantState> entrantStates = settings.get("entrantStateFilter");
        List<EnrEntrantCustomStateType> entrantCustomStates = settings.get("entrantCustomStateFilter");
        IdentifiableWrapper archivalSelectOption = settings.get("archivalFilter");
        Long orgUnitId = context.get(PARAM_ORG_UNIT);

        FilterUtils.applySelectFilter(dql, ENTRANT_ALIAS, EnrEntrant.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign);
        FilterUtils.applySelectFilter(dql, "idc", IdentityCard.P_SERIA, StringUtils.trimToNull(cardSeria));
        FilterUtils.applySelectFilter(dql, "idc", IdentityCard.P_NUMBER, StringUtils.trimToNull(cardNumber));

        FilterUtils.applyLikeFilter(dql, lastName, IdentityCard.lastName().fromAlias("idc"));
        FilterUtils.applyLikeFilter(dql, firstName, IdentityCard.firstName().fromAlias("idc"));
        FilterUtils.applyLikeFilter(dql, middleName, IdentityCard.middleName().fromAlias("idc"));
        FilterUtils.applyLikeFilter(dql, personalNumber, EnrEntrant.personalNumber().fromAlias(ENTRANT_ALIAS));

        if (citizenshipFilter != null) {
            if (citizenshipFilter.equals(CitizenshipModel.NO_RUSSIAN_CITIZENSHIP))
                dql.where(ne(property("idc", IdentityCard.citizenship().code()), value(IKladrDefines.RUSSIA_COUNTRY_CODE)));
            else
                dql.where(eq(property("idc", IdentityCard.citizenship()), value(citizenshipFilter)));
        }

        if (archivalSelectOption != null) {
            dql.where(eq(property(ENTRANT_ALIAS, EnrEntrant.archival()), value(archivalSelectOption.getId().equals(EnrEntrantListUI.SHOW_ARCHIVAL_CODE))));
        }

        DQLSelectBuilder requestBuilder = new DQLSelectBuilder()
                .fromEntity(EnrEntrantRequest.class, "er")
                .column("er.id")
                .where(eq(property("er", EnrEntrantRequest.L_ENTRANT), property(ENTRANT_ALIAS)));

        boolean needFilterByRequest;
        needFilterByRequest = FilterUtils.applySimpleLikeFilter(requestBuilder, "er", EnrEntrantRequest.P_REG_NUMBER, requestNumber);
        needFilterByRequest |= FilterUtils.applyBetweenFilter(requestBuilder, "er", EnrEntrantRequest.P_REG_DATE, dateFrom, dateTo);
        needFilterByRequest |= EnrEnrollmentCommissionManager.instance().dao().filterEntrantRequestByEnrCommission(requestBuilder, "er", enrollmentCommission);

        if (orgUnitId != null) {
            needFilterByRequest = true;
            requestBuilder.where(exists(
                    new DQLSelectBuilder().fromEntity(EnrRequestedCompetition.class, "rComp")
                            .where(or(eq(property("rComp", EnrRequestedCompetition.competition().programSetOrgUnit().orgUnit().institutionOrgUnit().orgUnit()), value(orgUnitId)),
                                      eq(property("rComp", EnrRequestedCompetition.competition().programSetOrgUnit().formativeOrgUnit()), value(orgUnitId))))
                            .where(eq(property("rComp", EnrRequestedCompetition.request()), property("er")))
                            .buildQuery()
            ));
        }

        CommonFilterAddon util = context.get(PARAM_ENR_COMPETITION_UTIL);
        if (util != null && !util.isEmptyFilters()) {
            needFilterByRequest = true;
            requestBuilder.where(exists(
                    new DQLSelectBuilder()
                            .fromEntity(EnrRequestedCompetition.class, "rc")
                            .column("rc.id")
                            .where(eq(property("rc", EnrRequestedCompetition.request()), property("er")))
                            .where(in(property("rc", EnrRequestedCompetition.competition()), util.getEntityIdsFilteredBuilder("a").buildQuery()))
                            .buildQuery()
            ));
        }

        //Фильтрация по принявшему заявление
        List<String> users = context.get("acceptRequestFilter");
        if (users != null && !users.isEmpty())
        {
            needFilterByRequest = true;
            requestBuilder.where(in(property("er", EnrEntrantRequest.P_ACCEPT_REQUEST), users));
        }

        //Организация-заказчик
        List<ExternalOrgUnit> orgUnitList = settings.get("targetContractOrgUnitFilter");
        if (orgUnitList != null && !orgUnitList.isEmpty())
        {
            needFilterByRequest = true;
            requestBuilder.where(exists(new DQLSelectBuilder()
                                                .fromEntity(EnrRequestedCompetition.class, "com")
                                                .column("com.id")
                                                .where(eq(property("com", EnrRequestedCompetition.request()), property("er")))
                                                .where(in(property("com", EnrRequestedCompetition.externalOrgUnit()), orgUnitList))
                                        .buildQuery()));
        }

        // Признак целевой контрактной подготовки
        Boolean targetContract = settings.get("targetContract");
        if (targetContract != null && targetContract)
        {
            needFilterByRequest = true;
            requestBuilder.where(exists(new DQLSelectBuilder()
                                                .fromEntity(EnrRequestedCompetition.class, "rc")
                                                .column("rc.id")
                                                .where(eq(property("rc", EnrRequestedCompetition.request()), property("er")))
                                                .where(eq(property("rc", EnrRequestedCompetition.competition().programSetOrgUnit().programSet().targetContractTraining()), value(true)))
                                                .buildQuery()));
        }

        if (needFilterByRequest) {
            dql.where(exists(requestBuilder.buildQuery()));
        }

        if (entrantStates != null && !entrantStates.isEmpty()) {
            dql.where(in(property(EnrEntrant.state().fromAlias(ENTRANT_ALIAS)), entrantStates));
        }

        if (entrantCustomStates != null && !entrantCustomStates.isEmpty()) {
            DQLSelectBuilder csDQL = new DQLSelectBuilder()
                    .fromEntity(EnrEntrantCustomState.class, "ecs").column("ecs.id")
                    .where(eq(property("ecs", EnrEntrantCustomState.entrant()), property(ENTRANT_ALIAS)))
                    .where(in(property("ecs", EnrEntrantCustomState.customState()), entrantCustomStates));

            FilterUtils.applyInPeriodFilterNullSafe(csDQL, "ecs", EnrEntrantCustomState.P_BEGIN_DATE, EnrEntrantCustomState.P_END_DATE, new Date());

            dql.where(exists(csDQL.buildQuery()));
        }

        return dql;
    }

    protected void fillRequestAndCustomStateMap(List<Long> entrantsIds, final Multimap<Long, DataWrapper> entrant2RequestsMap, final Multimap<Long, EnrEntrantCustomState> stateMap, ExecutionContext context)
    {
        List<String> users = context.get("acceptRequestFilter");
        IDataSettings settings = context.get("settings");
        //Организация-заказчик
        List<ExternalOrgUnit> orgUnitList = settings.get("targetContractOrgUnitFilter");

        // Признак целевой контрактной подготовки
        Boolean targetContract = settings.get("targetContract");

        BatchUtils.execute(entrantsIds, DQL.MAX_VALUES_ROW_NUMBER, ids -> {
            // Заявления

            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrEntrantRequest.class, "r")
                .column(property(EnrEntrantRequest.entrant().id().fromAlias("r")))
                .column(property(EnrEntrantRequest.id().fromAlias("r")))
                .column(property(EnrEntrantRequest.regNumber().fromAlias("r")))
                .where(in(property(EnrEntrantRequest.entrant().fromAlias("r")), ids));

                if (users != null && !users.isEmpty())
                    builder.where(in(property("r", EnrEntrantRequest.acceptRequest()), users));

            if (orgUnitList != null && !orgUnitList.isEmpty())
            {
                builder.where(exists(new DQLSelectBuilder()
                                                    .fromEntity(EnrRequestedCompetition.class, "com")
                                                    .column("com.id")
                                                    .where(eq(property("com", EnrRequestedCompetition.request()), property("r")))
                                                    .where(in(property("com", EnrRequestedCompetition.externalOrgUnit()), orgUnitList))
                                                    .buildQuery()));
            }


            if (targetContract != null && targetContract)
            {
                builder.where(exists(new DQLSelectBuilder()
                                                    .fromEntity(EnrRequestedCompetition.class, "rc")
                                                    .column("rc.id")
                                                    .where(eq(property("rc", EnrRequestedCompetition.request()), property("r")))
                                                    .where(eq(property("rc", EnrRequestedCompetition.competition().programSetOrgUnit().programSet().targetContractTraining()), value(true)))
                                                    .buildQuery()));
            }

            final List<Object[]> entrantRequestList = builder.createStatement(context.getSession()).list();

            for (Object[] objects : entrantRequestList) {
                final Long entrantId = (Long) objects[0];
                final Long requestId = (Long) objects[1];
                final String reqNumber = (String) objects[2];

                entrant2RequestsMap.put(entrantId, new DataWrapper(requestId, reqNumber));
            }

            // Активные доп. статусы

            DQLSelectBuilder stateDQL = new DQLSelectBuilder().fromEntity(EnrEntrantCustomState.class, "st")
                    .column("st")
                    .where(in(property("st", EnrEntrantCustomState.entrant()), ids))
                    .order(property("st", EnrEntrantCustomState.customState().shortTitle()))
                    .order(property("st", EnrEntrantCustomState.beginDate()))
                    .order(property("st", EnrEntrantCustomState.endDate()));

            FilterUtils.applyInPeriodFilterNullSafe(stateDQL, "st", EnrEntrantCustomState.P_BEGIN_DATE, EnrEntrantCustomState.P_END_DATE, new Date());

            for (EnrEntrantCustomState customState : stateDQL.createStatement(context.getSession()).<EnrEntrantCustomState>list())
            {
                stateMap.put(customState.getEntrant().getId(), customState);
            }
        });
    }
}