package ru.tandemservice.unienr14.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unienr14_2x10x3_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.10.3")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrOnlineEntrant

        // создано свойство indAchievScanCopy
        if (!tool.columnExists("enronlineentrant_t", "indachievscancopy_id"))
        {
            // создать колонку
            tool.createColumn("enronlineentrant_t", new DBColumn("indachievscancopy_id", DBType.LONG));

        }

        // создано свойство otherDocScanCopy
        if (!tool.columnExists("enronlineentrant_t", "otherdocscancopy_id"))
        {
            // создать колонку
            tool.createColumn("enronlineentrant_t", new DBColumn("otherdocscancopy_id", DBType.LONG));

        }


    }
}