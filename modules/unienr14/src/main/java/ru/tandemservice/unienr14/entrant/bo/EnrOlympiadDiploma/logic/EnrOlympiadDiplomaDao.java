/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrOlympiadDiploma.logic;

import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.shared.person.base.bo.PersonDocument.logic.PersonDocumentDao;
import org.tandemframework.shared.person.base.entity.PersonDocument;
import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma;
import ru.tandemservice.unienr14.remoteDocument.ext.RemoteDocument.util.RemoteDocumentFlexAttributes;

import java.util.Calendar;
import java.util.Map;

/**
 * @author oleyba
 * @since 5/3/13
 */
public class EnrOlympiadDiplomaDao extends PersonDocumentDao implements IEnrOlympiadDiplomaDao
{
    @Override
    public void saveOrUpdateDiploma(EnrOlympiadDiploma diploma, PersonRole role, IUploadFile scanCopy)
    {
        if(diploma.getId() == null)
        {
            saveDocument(diploma, role, scanCopy);
        }
        else
        {
            updateDocument(diploma, role, scanCopy);
        }
    }

    @Override
    protected Map<String, Object> fillFlexMap(PersonDocument personDocument)
    {
        EnrOlympiadDiploma diploma = (EnrOlympiadDiploma) personDocument;

        final Map<String, Object> flexMap = Maps.newHashMap();
        flexMap.put(RemoteDocumentFlexAttributes.OLYMPIAD_DIPLOMA_PERSON, diploma.getPerson().getId());
        flexMap.put(RemoteDocumentFlexAttributes.OLYMPIAD_DIPLOMA_VERSION, diploma.getVersion());
        if (diploma.getRegistrationDate() != null)
        {
            Calendar regDate = Calendar.getInstance();
            regDate.setTime(diploma.getRegistrationDate());
            flexMap.put(RemoteDocumentFlexAttributes.OLYMPIAD_DIPLOMA_REGISTRATION_DATE, regDate);
        }
        if (!StringUtils.isEmpty(diploma.getSeria()))
            flexMap.put(RemoteDocumentFlexAttributes.OLYMPIAD_DIPLOMA_SERIA, diploma.getSeria());
        if (!StringUtils.isEmpty(diploma.getNumber()))
            flexMap.put(RemoteDocumentFlexAttributes.OLYMPIAD_DIPLOMA_NUMBER, diploma.getNumber());
        if (diploma.getIssuanceDate() != null)
        {
            Calendar issuanceDate = Calendar.getInstance();
            issuanceDate.setTime(diploma.getIssuanceDate());
            flexMap.put(RemoteDocumentFlexAttributes.OLYMPIAD_DIPLOMA_ISSUANCE_DATE, issuanceDate);
        }
        if (diploma.getSettlement() != null)
            flexMap.put(RemoteDocumentFlexAttributes.OLYMPIAD_DIPLOMA_SETTLEMENT, diploma.getSettlement().getId());
        if (diploma.getOlympiad() != null)
            flexMap.put(RemoteDocumentFlexAttributes.OLYMPIAD_DIPLOMA_OLYMPIAD, diploma.getOlympiad().getId());
        if (diploma.getSubject() != null)
            flexMap.put(RemoteDocumentFlexAttributes.OLYMPIAD_DIPLOMA_SUBJECT, diploma.getSubject().getId());
        if (diploma.getHonour() != null)
            flexMap.put(RemoteDocumentFlexAttributes.OLYMPIAD_DIPLOMA_HONOUR, diploma.getHonour().getId());
        if (diploma.getDocumentType() != null)
            flexMap.put(RemoteDocumentFlexAttributes.OLYMPIAD_DIPLOMA_DOCUMENT_TYPE, diploma.getDocumentType().getId());
        return flexMap;
    }
}
