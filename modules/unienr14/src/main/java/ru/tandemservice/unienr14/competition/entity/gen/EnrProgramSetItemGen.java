package ru.tandemservice.unienr14.competition.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Образовательная программа в наборе (ВО)
 *
 * Задает перечень ОП для набора ОП.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrProgramSetItemGen extends EntityBase
 implements INaturalIdentifiable<EnrProgramSetItemGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem";
    public static final String ENTITY_NAME = "enrProgramSetItem";
    public static final int VERSION_HASH = 1320676195;
    private static IEntityMeta ENTITY_META;

    public static final String L_PROGRAM_SET = "programSet";
    public static final String L_PROGRAM = "program";

    private EnrProgramSetBase _programSet;     // Набор ОП
    private EduProgramHigherProf _program;     // ОП

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Набор ОП. Свойство не может быть null.
     */
    @NotNull
    public EnrProgramSetBase getProgramSet()
    {
        return _programSet;
    }

    /**
     * @param programSet Набор ОП. Свойство не может быть null.
     */
    public void setProgramSet(EnrProgramSetBase programSet)
    {
        dirty(_programSet, programSet);
        _programSet = programSet;
    }

    /**
     * @return ОП. Свойство не может быть null.
     */
    @NotNull
    public EduProgramHigherProf getProgram()
    {
        return _program;
    }

    /**
     * @param program ОП. Свойство не может быть null.
     */
    public void setProgram(EduProgramHigherProf program)
    {
        dirty(_program, program);
        _program = program;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrProgramSetItemGen)
        {
            if (withNaturalIdProperties)
            {
                setProgramSet(((EnrProgramSetItem)another).getProgramSet());
                setProgram(((EnrProgramSetItem)another).getProgram());
            }
        }
    }

    public INaturalId<EnrProgramSetItemGen> getNaturalId()
    {
        return new NaturalId(getProgramSet(), getProgram());
    }

    public static class NaturalId extends NaturalIdBase<EnrProgramSetItemGen>
    {
        private static final String PROXY_NAME = "EnrProgramSetItemNaturalProxy";

        private Long _programSet;
        private Long _program;

        public NaturalId()
        {}

        public NaturalId(EnrProgramSetBase programSet, EduProgramHigherProf program)
        {
            _programSet = ((IEntity) programSet).getId();
            _program = ((IEntity) program).getId();
        }

        public Long getProgramSet()
        {
            return _programSet;
        }

        public void setProgramSet(Long programSet)
        {
            _programSet = programSet;
        }

        public Long getProgram()
        {
            return _program;
        }

        public void setProgram(Long program)
        {
            _program = program;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrProgramSetItemGen.NaturalId) ) return false;

            EnrProgramSetItemGen.NaturalId that = (NaturalId) o;

            if( !equals(getProgramSet(), that.getProgramSet()) ) return false;
            if( !equals(getProgram(), that.getProgram()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getProgramSet());
            result = hashCode(result, getProgram());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getProgramSet());
            sb.append("/");
            sb.append(getProgram());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrProgramSetItemGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrProgramSetItem.class;
        }

        public T newInstance()
        {
            return (T) new EnrProgramSetItem();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "programSet":
                    return obj.getProgramSet();
                case "program":
                    return obj.getProgram();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "programSet":
                    obj.setProgramSet((EnrProgramSetBase) value);
                    return;
                case "program":
                    obj.setProgram((EduProgramHigherProf) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "programSet":
                        return true;
                case "program":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "programSet":
                    return true;
                case "program":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "programSet":
                    return EnrProgramSetBase.class;
                case "program":
                    return EduProgramHigherProf.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrProgramSetItem> _dslPath = new Path<EnrProgramSetItem>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrProgramSetItem");
    }
            

    /**
     * @return Набор ОП. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem#getProgramSet()
     */
    public static EnrProgramSetBase.Path<EnrProgramSetBase> programSet()
    {
        return _dslPath.programSet();
    }

    /**
     * @return ОП. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem#getProgram()
     */
    public static EduProgramHigherProf.Path<EduProgramHigherProf> program()
    {
        return _dslPath.program();
    }

    public static class Path<E extends EnrProgramSetItem> extends EntityPath<E>
    {
        private EnrProgramSetBase.Path<EnrProgramSetBase> _programSet;
        private EduProgramHigherProf.Path<EduProgramHigherProf> _program;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Набор ОП. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem#getProgramSet()
     */
        public EnrProgramSetBase.Path<EnrProgramSetBase> programSet()
        {
            if(_programSet == null )
                _programSet = new EnrProgramSetBase.Path<EnrProgramSetBase>(L_PROGRAM_SET, this);
            return _programSet;
        }

    /**
     * @return ОП. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem#getProgram()
     */
        public EduProgramHigherProf.Path<EduProgramHigherProf> program()
        {
            if(_program == null )
                _program = new EduProgramHigherProf.Path<EduProgramHigherProf>(L_PROGRAM, this);
            return _program;
        }

        public Class getEntityClass()
        {
            return EnrProgramSetItem.class;
        }

        public String getEntityName()
        {
            return "enrProgramSetItem";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
