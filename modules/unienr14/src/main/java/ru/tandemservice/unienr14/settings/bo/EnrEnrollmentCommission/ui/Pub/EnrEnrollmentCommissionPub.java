/* $Id$ */
package ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.presenter.*;
import org.tandemframework.caf.ui.config.*;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.ui.SecTab.EnrEnrollmentCommissionSecTab;

/**
 * @author Alexey Lopatin
 * @since 29.05.2015
 */
@Configuration
public class EnrEnrollmentCommissionPub extends BusinessComponentManager
{
    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .create();
    }

    @Bean
    public TabPanelExtPoint tabPanelExtPoint()
    {
        return tabPanelExtPointBuilder("enrollmentCommissionPubTabPanel")
                .addTab(htmlTab("infoTab", "EnrEnrollmentCommissionInfoTab"))
                .addTab(componentTab("secTab", EnrEnrollmentCommissionSecTab.class))
                .create();
    }
}
