/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentModel.logic;

import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentConflictSolution;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEnrollmentConflictSolutionCodes;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.select.EnrSelectionGroup;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.select.EnrSelectionItem;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.select.EnrSelectionSkipReason;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.logic.select.IEnrSelectionItemPrevEnrollmentInfo;
import ru.tandemservice.unienr14.enrollment.entity.EnrModelStepItem;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;

/**
 * @author oleyba
 * @since 4/2/15
 */
public class EnrModelStepItemPrevEnrollmentInfo implements IEnrSelectionItemPrevEnrollmentInfo
{
    private EnrModelStepItem prevItem;
    private EnrModelStepItem currentItem;
    private EnrEnrollmentConflictSolution solution;

    public EnrModelStepItemPrevEnrollmentInfo(EnrModelStepItem prevItem, EnrModelStepItem currentItem, boolean reEnrollEnabled)
    {
        this.prevItem = prevItem;
        this.currentItem = currentItem;
        this.solution = DataAccessServices.dao().getByNaturalId(new EnrEnrollmentConflictSolution.NaturalId(getSolutionCode(reEnrollEnabled)));
    }

    private String getSolutionCode(boolean reEnrollEnabled)
    {
        EnrRequestedCompetition currentRc = currentItem.getEntity().getRequestedCompetition();
        EnrRequestedCompetition prevRc = prevItem.getEntity().getRequestedCompetition();

        boolean sameParallelFlag = (currentRc.isParallel() == prevRc.isParallel());                          // один и тот же признак «параллельноси»
        boolean sameRequestType = currentRc.getRequest().getType().equals(prevRc.getRequest().getType());    // один и тот же вид заявления

        boolean sameRequestAndParallel = sameParallelFlag && sameRequestType;
        if (!sameRequestAndParallel)
        {
            // зачислить повторно (выписки не влияют друг на друга)
            return EnrEnrollmentConflictSolutionCodes.ENROLL;
        }
        else
        {
            boolean extractPriorityHigher = (currentRc.getPriority() >= prevRc.getPriority()); // приоритет выписки выше (численно меньше)
            if (!reEnrollEnabled) { return EnrEnrollmentConflictSolutionCodes.SKIP; }
            if (extractPriorityHigher) { return EnrEnrollmentConflictSolutionCodes.SKIP; }

            // иначе, перезачислять
            return EnrEnrollmentConflictSolutionCodes.REENROLL;
        }
    }

    @Override
    public void apply(EnrSelectionItem item)
    {
        if (!currentItem.equals(item.getStepItem())) { throw new IllegalStateException("step-item-missmatch"); }

        if (!isCancelled()) {
            final EnrSelectionGroup otherGroup = item.getGroup().getContext().getStepItemGroup(this);
            if (null != otherGroup) { otherGroup.registerExtract(new EnrModelStepPrevEnrollmentFact(prevItem)); }
        }

        switch(solution.getCode()) {
            case EnrEnrollmentConflictSolutionCodes.ENROLL: {
                item.getAddEnrollList().add(this);
                break;
            }
            case EnrEnrollmentConflictSolutionCodes.REENROLL: {
                item.getReenrollFromList().add(this);
                break;
            }
            case EnrEnrollmentConflictSolutionCodes.SKIP: {
                item.skip(new EnrSelectionSkipReason.OrderInfoSkipConflictSolution(this));
                break;
            }
            default: throw new IllegalStateException("undefined-solution: "+getSolution().getCode());
        }
    }

    @Override
    public EnrRequestedCompetition getRequestedCompetition()
    {
        return prevItem.getEntity().getRequestedCompetition();
    }

    @Override
    public boolean isCancelled()
    {
        return false;
    }

    @Override
    public String getInfo()
    {
        return prevItem.getStep().getDateStr();
    }

    @Override
    public EnrEnrollmentConflictSolution getSolution()
    {
        return solution;
    }
}
