package ru.tandemservice.unienr14.request.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.person.catalog.entity.PersonDocumentType;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitProofDocument;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitProofDocument;

/**
 * Документ, подтверждающий особое право при поступлении
 *
 * Интерфейс для документов, которые могут подтверждать особое право.
 */
@SuppressWarnings({"all", "unchecked", "unused"})
public abstract class IEnrEntrantBenefitProofDocumentGen extends InterfaceStubBase
 implements IEnrEntrantBenefitProofDocument{
    public static final int VERSION_HASH = 115840579;

    public static final String L_ENTRANT = "entrant";
    public static final String P_SERIA = "seria";
    public static final String P_NUMBER = "number";
    public static final String P_REGISTRATION_DATE = "registrationDate";
    public static final String L_DOCUMENT_TYPE = "documentType";

    private EnrEntrant _entrant;
    private String _seria;
    private String _number;
    private Date _registrationDate;
    private PersonDocumentType _documentType;


    public EnrEntrant getEntrant()
    {
        return _entrant;
    }

    public void setEntrant(EnrEntrant entrant)
    {
        _entrant = entrant;
    }

    @Length(max=255)

    public String getSeria()
    {
        return _seria;
    }

    public void setSeria(String seria)
    {
        _seria = seria;
    }

    @Length(max=255)

    public String getNumber()
    {
        return _number;
    }

    public void setNumber(String number)
    {
        _number = number;
    }

    @NotNull

    public Date getRegistrationDate()
    {
        return _registrationDate;
    }

    public void setRegistrationDate(Date registrationDate)
    {
        _registrationDate = registrationDate;
    }


    public PersonDocumentType getDocumentType()
    {
        return _documentType;
    }

    public void setDocumentType(PersonDocumentType documentType)
    {
        _documentType = documentType;
    }

    private static final Path<IEnrEntrantBenefitProofDocument> _dslPath = new Path<IEnrEntrantBenefitProofDocument>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitProofDocument");
    }
            

    /**
     * @return Абитуриент.
     * @see ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitProofDocument#getEntrant()
     */
    public static EnrEntrant.Path<EnrEntrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return Серия.
     * @see ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitProofDocument#getSeria()
     */
    public static PropertyPath<String> seria()
    {
        return _dslPath.seria();
    }

    /**
     * @return Номер.
     * @see ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitProofDocument#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Дата добавления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitProofDocument#getRegistrationDate()
     */
    public static PropertyPath<Date> registrationDate()
    {
        return _dslPath.registrationDate();
    }

    /**
     * @return Тип документа.
     * @see ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitProofDocument#getDocumentType()
     */
    public static PersonDocumentType.Path<PersonDocumentType> documentType()
    {
        return _dslPath.documentType();
    }

    public static class Path<E extends IEnrEntrantBenefitProofDocument> extends EntityPath<E>
    {
        private EnrEntrant.Path<EnrEntrant> _entrant;
        private PropertyPath<String> _seria;
        private PropertyPath<String> _number;
        private PropertyPath<Date> _registrationDate;
        private PersonDocumentType.Path<PersonDocumentType> _documentType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Абитуриент.
     * @see ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitProofDocument#getEntrant()
     */
        public EnrEntrant.Path<EnrEntrant> entrant()
        {
            if(_entrant == null )
                _entrant = new EnrEntrant.Path<EnrEntrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return Серия.
     * @see ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitProofDocument#getSeria()
     */
        public PropertyPath<String> seria()
        {
            if(_seria == null )
                _seria = new PropertyPath<String>(IEnrEntrantBenefitProofDocumentGen.P_SERIA, this);
            return _seria;
        }

    /**
     * @return Номер.
     * @see ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitProofDocument#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(IEnrEntrantBenefitProofDocumentGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Дата добавления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitProofDocument#getRegistrationDate()
     */
        public PropertyPath<Date> registrationDate()
        {
            if(_registrationDate == null )
                _registrationDate = new PropertyPath<Date>(IEnrEntrantBenefitProofDocumentGen.P_REGISTRATION_DATE, this);
            return _registrationDate;
        }

    /**
     * @return Тип документа.
     * @see ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitProofDocument#getDocumentType()
     */
        public PersonDocumentType.Path<PersonDocumentType> documentType()
        {
            if(_documentType == null )
                _documentType = new PersonDocumentType.Path<PersonDocumentType>(L_DOCUMENT_TYPE, this);
            return _documentType;
        }

        public Class getEntityClass()
        {
            return IEnrEntrantBenefitProofDocument.class;
        }

        public String getEntityName()
        {
            return "ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitProofDocument";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
