package ru.tandemservice.unienr14.entrant.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.person.base.entity.PersonDocumentRoleRel;
import org.tandemframework.shared.person.catalog.entity.PersonDocumentType;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantBaseDocument;
import ru.tandemservice.unienr14.entrant.entity.IEnrEntrantAchievementProofDocument;
import ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitProofDocument;
import ru.tandemservice.unienr14.request.entity.IEnrEntrantRequestAttachable;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Документ абитуриента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrEntrantBaseDocumentGen extends EntityBase
 implements IEnrEntrantBenefitProofDocument, IEnrEntrantAchievementProofDocument, IEnrEntrantRequestAttachable, INaturalIdentifiable<EnrEntrantBaseDocumentGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.entrant.entity.EnrEntrantBaseDocument";
    public static final String ENTITY_NAME = "enrEntrantBaseDocument";
    public static final int VERSION_HASH = -1875048194;
    private static IEntityMeta ENTITY_META;

    public static final String L_DOC_RELATION = "docRelation";
    public static final String L_ENTRANT = "entrant";
    public static final String L_DOCUMENT_TYPE = "documentType";
    public static final String P_REGISTRATION_DATE = "registrationDate";
    public static final String P_SERIA = "seria";
    public static final String P_NUMBER = "number";
    public static final String P_DISPLAYABLE_TITLE = "displayableTitle";

    private PersonDocumentRoleRel _docRelation;     // Связь документа персоны
    private EnrEntrant _entrant;     // Абитуриент
    private PersonDocumentType _documentType;     // Тип документа
    private Date _registrationDate;     // Дата добавления
    private String _seria;     // Серия
    private String _number;     // Номер

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Связь документа персоны. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public PersonDocumentRoleRel getDocRelation()
    {
        return _docRelation;
    }

    /**
     * @param docRelation Связь документа персоны. Свойство не может быть null и должно быть уникальным.
     */
    public void setDocRelation(PersonDocumentRoleRel docRelation)
    {
        dirty(_docRelation, docRelation);
        _docRelation = docRelation;
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     */
    @NotNull
    public EnrEntrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant Абитуриент. Свойство не может быть null.
     */
    public void setEntrant(EnrEntrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    /**
     * @return Тип документа. Свойство не может быть null.
     */
    @NotNull
    public PersonDocumentType getDocumentType()
    {
        return _documentType;
    }

    /**
     * @param documentType Тип документа. Свойство не может быть null.
     */
    public void setDocumentType(PersonDocumentType documentType)
    {
        dirty(_documentType, documentType);
        _documentType = documentType;
    }

    /**
     * @return Дата добавления.
     *
     * Это формула "docRelation.document.registrationDate".
     */
    public Date getRegistrationDate()
    {
        return _registrationDate;
    }

    /**
     * @param registrationDate Дата добавления.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setRegistrationDate(Date registrationDate)
    {
        dirty(_registrationDate, registrationDate);
        _registrationDate = registrationDate;
    }

    /**
     * @return Серия.
     *
     * Это формула "docRelation.document.seria".
     */
    // @Length(max=255)
    public String getSeria()
    {
        return _seria;
    }

    /**
     * @param seria Серия.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setSeria(String seria)
    {
        dirty(_seria, seria);
        _seria = seria;
    }

    /**
     * @return Номер.
     *
     * Это формула "docRelation.document.number".
     */
    // @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrEntrantBaseDocumentGen)
        {
            if (withNaturalIdProperties)
            {
                setDocRelation(((EnrEntrantBaseDocument)another).getDocRelation());
            }
            setEntrant(((EnrEntrantBaseDocument)another).getEntrant());
            setDocumentType(((EnrEntrantBaseDocument)another).getDocumentType());
            setRegistrationDate(((EnrEntrantBaseDocument)another).getRegistrationDate());
            setSeria(((EnrEntrantBaseDocument)another).getSeria());
            setNumber(((EnrEntrantBaseDocument)another).getNumber());
        }
    }

    public INaturalId<EnrEntrantBaseDocumentGen> getNaturalId()
    {
        return new NaturalId(getDocRelation());
    }

    public static class NaturalId extends NaturalIdBase<EnrEntrantBaseDocumentGen>
    {
        private static final String PROXY_NAME = "EnrEntrantBaseDocumentNaturalProxy";

        private Long _docRelation;

        public NaturalId()
        {}

        public NaturalId(PersonDocumentRoleRel docRelation)
        {
            _docRelation = ((IEntity) docRelation).getId();
        }

        public Long getDocRelation()
        {
            return _docRelation;
        }

        public void setDocRelation(Long docRelation)
        {
            _docRelation = docRelation;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrEntrantBaseDocumentGen.NaturalId) ) return false;

            EnrEntrantBaseDocumentGen.NaturalId that = (NaturalId) o;

            if( !equals(getDocRelation(), that.getDocRelation()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getDocRelation());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getDocRelation());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrEntrantBaseDocumentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrEntrantBaseDocument.class;
        }

        public T newInstance()
        {
            return (T) new EnrEntrantBaseDocument();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "docRelation":
                    return obj.getDocRelation();
                case "entrant":
                    return obj.getEntrant();
                case "documentType":
                    return obj.getDocumentType();
                case "registrationDate":
                    return obj.getRegistrationDate();
                case "seria":
                    return obj.getSeria();
                case "number":
                    return obj.getNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "docRelation":
                    obj.setDocRelation((PersonDocumentRoleRel) value);
                    return;
                case "entrant":
                    obj.setEntrant((EnrEntrant) value);
                    return;
                case "documentType":
                    obj.setDocumentType((PersonDocumentType) value);
                    return;
                case "registrationDate":
                    obj.setRegistrationDate((Date) value);
                    return;
                case "seria":
                    obj.setSeria((String) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "docRelation":
                        return true;
                case "entrant":
                        return true;
                case "documentType":
                        return true;
                case "registrationDate":
                        return true;
                case "seria":
                        return true;
                case "number":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "docRelation":
                    return true;
                case "entrant":
                    return true;
                case "documentType":
                    return true;
                case "registrationDate":
                    return true;
                case "seria":
                    return true;
                case "number":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "docRelation":
                    return PersonDocumentRoleRel.class;
                case "entrant":
                    return EnrEntrant.class;
                case "documentType":
                    return PersonDocumentType.class;
                case "registrationDate":
                    return Date.class;
                case "seria":
                    return String.class;
                case "number":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrEntrantBaseDocument> _dslPath = new Path<EnrEntrantBaseDocument>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrEntrantBaseDocument");
    }
            

    /**
     * @return Связь документа персоны. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantBaseDocument#getDocRelation()
     */
    public static PersonDocumentRoleRel.Path<PersonDocumentRoleRel> docRelation()
    {
        return _dslPath.docRelation();
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantBaseDocument#getEntrant()
     */
    public static EnrEntrant.Path<EnrEntrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return Тип документа. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantBaseDocument#getDocumentType()
     */
    public static PersonDocumentType.Path<PersonDocumentType> documentType()
    {
        return _dslPath.documentType();
    }

    /**
     * @return Дата добавления.
     *
     * Это формула "docRelation.document.registrationDate".
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantBaseDocument#getRegistrationDate()
     */
    public static PropertyPath<Date> registrationDate()
    {
        return _dslPath.registrationDate();
    }

    /**
     * @return Серия.
     *
     * Это формула "docRelation.document.seria".
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantBaseDocument#getSeria()
     */
    public static PropertyPath<String> seria()
    {
        return _dslPath.seria();
    }

    /**
     * @return Номер.
     *
     * Это формула "docRelation.document.number".
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantBaseDocument#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantBaseDocument#getDisplayableTitle()
     */
    public static SupportedPropertyPath<String> displayableTitle()
    {
        return _dslPath.displayableTitle();
    }

    public static class Path<E extends EnrEntrantBaseDocument> extends EntityPath<E>
    {
        private PersonDocumentRoleRel.Path<PersonDocumentRoleRel> _docRelation;
        private EnrEntrant.Path<EnrEntrant> _entrant;
        private PersonDocumentType.Path<PersonDocumentType> _documentType;
        private PropertyPath<Date> _registrationDate;
        private PropertyPath<String> _seria;
        private PropertyPath<String> _number;
        private SupportedPropertyPath<String> _displayableTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Связь документа персоны. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantBaseDocument#getDocRelation()
     */
        public PersonDocumentRoleRel.Path<PersonDocumentRoleRel> docRelation()
        {
            if(_docRelation == null )
                _docRelation = new PersonDocumentRoleRel.Path<PersonDocumentRoleRel>(L_DOC_RELATION, this);
            return _docRelation;
        }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantBaseDocument#getEntrant()
     */
        public EnrEntrant.Path<EnrEntrant> entrant()
        {
            if(_entrant == null )
                _entrant = new EnrEntrant.Path<EnrEntrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return Тип документа. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantBaseDocument#getDocumentType()
     */
        public PersonDocumentType.Path<PersonDocumentType> documentType()
        {
            if(_documentType == null )
                _documentType = new PersonDocumentType.Path<PersonDocumentType>(L_DOCUMENT_TYPE, this);
            return _documentType;
        }

    /**
     * @return Дата добавления.
     *
     * Это формула "docRelation.document.registrationDate".
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantBaseDocument#getRegistrationDate()
     */
        public PropertyPath<Date> registrationDate()
        {
            if(_registrationDate == null )
                _registrationDate = new PropertyPath<Date>(EnrEntrantBaseDocumentGen.P_REGISTRATION_DATE, this);
            return _registrationDate;
        }

    /**
     * @return Серия.
     *
     * Это формула "docRelation.document.seria".
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantBaseDocument#getSeria()
     */
        public PropertyPath<String> seria()
        {
            if(_seria == null )
                _seria = new PropertyPath<String>(EnrEntrantBaseDocumentGen.P_SERIA, this);
            return _seria;
        }

    /**
     * @return Номер.
     *
     * Это формула "docRelation.document.number".
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantBaseDocument#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(EnrEntrantBaseDocumentGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrantBaseDocument#getDisplayableTitle()
     */
        public SupportedPropertyPath<String> displayableTitle()
        {
            if(_displayableTitle == null )
                _displayableTitle = new SupportedPropertyPath<String>(EnrEntrantBaseDocumentGen.P_DISPLAYABLE_TITLE, this);
            return _displayableTitle;
        }

        public Class getEntityClass()
        {
            return EnrEntrantBaseDocument.class;
        }

        public String getEntityName()
        {
            return "enrEntrantBaseDocument";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getDisplayableTitle();
}
