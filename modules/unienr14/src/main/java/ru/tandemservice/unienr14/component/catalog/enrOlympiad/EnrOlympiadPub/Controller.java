/**
 *$Id: Controller.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.component.catalog.enrOlympiad.EnrOlympiadPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.unienr14.catalog.entity.EnrOlympiad;

/**
 * @author Alexander Shaburov
 * @since 14.05.13
 */
public class Controller extends DefaultCatalogPubController<EnrOlympiad, Model, IDAO>
{
    @Override
    protected void addColumnsBeforeEditColumn(IBusinessComponent context, DynamicListDataSource<EnrOlympiad> dataSource)
    {
        dataSource.addColumn(new SimpleColumn("Тип олимпиады", EnrOlympiad.olympiadType().title().s()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Год приема", EnrOlympiad.eduYear().title().s()).setOrderable(false).setClickable(false));
    }
}
