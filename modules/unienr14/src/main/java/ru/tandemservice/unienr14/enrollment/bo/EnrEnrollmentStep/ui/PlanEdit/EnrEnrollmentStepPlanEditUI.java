/* $Id:$ */
package ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.ui.PlanEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStep;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepCompetitionPlan;
import ru.tandemservice.unienr14.enrollment.entity.gen.EnrEnrollmentStepCompetitionPlanGen;

/**
 * @author oleyba
 * @since 2/27/15
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "competitionId"),
    @Bind(key = EnrEnrollmentStepPlanEditUI.BIND_STEP_ID, binding = "stepId")
})
public class EnrEnrollmentStepPlanEditUI extends UIPresenter
{
    public static final String BIND_STEP_ID = "stepId";

    private EnrEnrollmentStepCompetitionPlan plan;
    private Long competitionId;
    private Long stepId;

    @Override
    public void onComponentRefresh()
    {
        if (getPlan() == null) {
            setPlan((EnrEnrollmentStepCompetitionPlan) DataAccessServices.dao().getByNaturalId(new EnrEnrollmentStepCompetitionPlan.NaturalId(
                DataAccessServices.dao().getNotNull(EnrEnrollmentStep.class, getStepId()),
                DataAccessServices.dao().getNotNull(EnrCompetition.class, getCompetitionId())
            )));
        }
    }

    public void onClickApply() {
        DataAccessServices.dao().update(getPlan());
        deactivate();
    }

    // getters and setters

    public int getPercentage() {
        return (int) Math.ceil(0.0001 * getPlan().getEnrollmentStep().getPercentageAsLong() * getPlan().getCompetition().getPlan());
    }

    public Long getCompetitionId()
    {
        return competitionId;
    }

    public void setCompetitionId(Long competitionId)
    {
        this.competitionId = competitionId;
    }

    public EnrEnrollmentStepCompetitionPlan getPlan()
    {
        return plan;
    }

    public void setPlan(EnrEnrollmentStepCompetitionPlan plan)
    {
        this.plan = plan;
    }

    public Long getStepId()
    {
        return stepId;
    }

    public void setStepId(Long stepId)
    {
        this.stepId = stepId;
    }
}