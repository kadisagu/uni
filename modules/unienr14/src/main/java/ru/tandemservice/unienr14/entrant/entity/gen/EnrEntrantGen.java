package ru.tandemservice.unienr14.entrant.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantState;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Абитуриент
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrEntrantGen extends PersonRole
 implements ILazyFieldOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.entrant.entity.EnrEntrant";
    public static final String ENTITY_NAME = "enrEntrant";
    public static final int VERSION_HASH = 110648440;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_PERSONAL_NUMBER = "personalNumber";
    public static final String P_REGISTRATION_DATE = "registrationDate";
    public static final String L_STATE = "state";
    public static final String P_ARCHIVAL = "archival";
    public static final String P_ADDITIONAL_INFO = "additionalInfo";
    public static final String P_PASS_ARMY = "passArmy";
    public static final String P_BEGIN_ARMY = "beginArmy";
    public static final String P_END_ARMY = "endArmy";
    public static final String P_END_ARMY_YEAR = "endArmyYear";
    public static final String P_PASS_MEDICAL_EDUCATION = "passMedicalEducation";
    public static final String P_NEED_SPECIAL_EXAM_CONDITIONS = "needSpecialExamConditions";
    public static final String P_SPECIAL_EXAM_CONDITIONS_DETAILS = "specialExamConditionsDetails";

    private EnrEnrollmentCampaign _enrollmentCampaign;     // ПК
    private String _personalNumber;     // Личный номер
    private Date _registrationDate;     // Дата добавления
    private EnrEntrantState _state;     // Состояние
    private boolean _archival;     // Архивный
    private String _additionalInfo;     // Дополнительная информация
    private boolean _passArmy;     // Служил в армии
    private Date _beginArmy;     // Дата начала службы
    private Date _endArmy;     // Дата окончания службы
    private Integer _endArmyYear;     // Год увольнения в запас
    private boolean _passMedicalEducation;     // Закончил медицинское (фармацевтическое) образовательное учреждение
    private boolean _needSpecialExamConditions;     // Нуждается в спец. условиях для прохождения ВИ
    private String _specialExamConditionsDetails;     // Примечание по спец. условиям для прохождения ВИ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return ПК. Свойство не может быть null.
     */
    @NotNull
    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign ПК. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Личный номер. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPersonalNumber()
    {
        return _personalNumber;
    }

    /**
     * @param personalNumber Личный номер. Свойство не может быть null.
     */
    public void setPersonalNumber(String personalNumber)
    {
        dirty(_personalNumber, personalNumber);
        _personalNumber = personalNumber;
    }

    /**
     * @return Дата добавления. Свойство не может быть null.
     */
    @NotNull
    public Date getRegistrationDate()
    {
        return _registrationDate;
    }

    /**
     * @param registrationDate Дата добавления. Свойство не может быть null.
     */
    public void setRegistrationDate(Date registrationDate)
    {
        dirty(_registrationDate, registrationDate);
        _registrationDate = registrationDate;
    }

    /**
     * Обновляется демоном IEnrEntrantStateDaemonDao#doUpdateEntrantStatus.
     *
     * @return Состояние.
     */
    public EnrEntrantState getState()
    {
        return _state;
    }

    /**
     * @param state Состояние.
     */
    public void setState(EnrEntrantState state)
    {
        dirty(_state, state);
        _state = state;
    }

    /**
     * @return Архивный. Свойство не может быть null.
     */
    @NotNull
    public boolean isArchival()
    {
        return _archival;
    }

    /**
     * @param archival Архивный. Свойство не может быть null.
     */
    public void setArchival(boolean archival)
    {
        dirty(_archival, archival);
        _archival = archival;
    }

    /**
     * @return Дополнительная информация.
     */
    public String getAdditionalInfo()
    {
        initLazyForGet("additionalInfo");
        return _additionalInfo;
    }

    /**
     * @param additionalInfo Дополнительная информация.
     */
    public void setAdditionalInfo(String additionalInfo)
    {
        initLazyForSet("additionalInfo");
        dirty(_additionalInfo, additionalInfo);
        _additionalInfo = additionalInfo;
    }

    /**
     * @return Служил в армии. Свойство не может быть null.
     */
    @NotNull
    public boolean isPassArmy()
    {
        return _passArmy;
    }

    /**
     * @param passArmy Служил в армии. Свойство не может быть null.
     */
    public void setPassArmy(boolean passArmy)
    {
        dirty(_passArmy, passArmy);
        _passArmy = passArmy;
    }

    /**
     * @return Дата начала службы.
     */
    public Date getBeginArmy()
    {
        return _beginArmy;
    }

    /**
     * @param beginArmy Дата начала службы.
     */
    public void setBeginArmy(Date beginArmy)
    {
        dirty(_beginArmy, beginArmy);
        _beginArmy = beginArmy;
    }

    /**
     * @return Дата окончания службы.
     */
    public Date getEndArmy()
    {
        return _endArmy;
    }

    /**
     * @param endArmy Дата окончания службы.
     */
    public void setEndArmy(Date endArmy)
    {
        dirty(_endArmy, endArmy);
        _endArmy = endArmy;
    }

    /**
     * @return Год увольнения в запас.
     */
    public Integer getEndArmyYear()
    {
        return _endArmyYear;
    }

    /**
     * @param endArmyYear Год увольнения в запас.
     */
    public void setEndArmyYear(Integer endArmyYear)
    {
        dirty(_endArmyYear, endArmyYear);
        _endArmyYear = endArmyYear;
    }

    /**
     * @return Закончил медицинское (фармацевтическое) образовательное учреждение. Свойство не может быть null.
     */
    @NotNull
    public boolean isPassMedicalEducation()
    {
        return _passMedicalEducation;
    }

    /**
     * @param passMedicalEducation Закончил медицинское (фармацевтическое) образовательное учреждение. Свойство не может быть null.
     */
    public void setPassMedicalEducation(boolean passMedicalEducation)
    {
        dirty(_passMedicalEducation, passMedicalEducation);
        _passMedicalEducation = passMedicalEducation;
    }

    /**
     * @return Нуждается в спец. условиях для прохождения ВИ. Свойство не может быть null.
     */
    @NotNull
    public boolean isNeedSpecialExamConditions()
    {
        return _needSpecialExamConditions;
    }

    /**
     * @param needSpecialExamConditions Нуждается в спец. условиях для прохождения ВИ. Свойство не может быть null.
     */
    public void setNeedSpecialExamConditions(boolean needSpecialExamConditions)
    {
        dirty(_needSpecialExamConditions, needSpecialExamConditions);
        _needSpecialExamConditions = needSpecialExamConditions;
    }

    /**
     * @return Примечание по спец. условиям для прохождения ВИ.
     */
    public String getSpecialExamConditionsDetails()
    {
        return _specialExamConditionsDetails;
    }

    /**
     * @param specialExamConditionsDetails Примечание по спец. условиям для прохождения ВИ.
     */
    public void setSpecialExamConditionsDetails(String specialExamConditionsDetails)
    {
        dirty(_specialExamConditionsDetails, specialExamConditionsDetails);
        _specialExamConditionsDetails = specialExamConditionsDetails;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EnrEntrantGen)
        {
            setEnrollmentCampaign(((EnrEntrant)another).getEnrollmentCampaign());
            setPersonalNumber(((EnrEntrant)another).getPersonalNumber());
            setRegistrationDate(((EnrEntrant)another).getRegistrationDate());
            setState(((EnrEntrant)another).getState());
            setArchival(((EnrEntrant)another).isArchival());
            setAdditionalInfo(((EnrEntrant)another).getAdditionalInfo());
            setPassArmy(((EnrEntrant)another).isPassArmy());
            setBeginArmy(((EnrEntrant)another).getBeginArmy());
            setEndArmy(((EnrEntrant)another).getEndArmy());
            setEndArmyYear(((EnrEntrant)another).getEndArmyYear());
            setPassMedicalEducation(((EnrEntrant)another).isPassMedicalEducation());
            setNeedSpecialExamConditions(((EnrEntrant)another).isNeedSpecialExamConditions());
            setSpecialExamConditionsDetails(((EnrEntrant)another).getSpecialExamConditionsDetails());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrEntrantGen> extends PersonRole.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrEntrant.class;
        }

        public T newInstance()
        {
            return (T) new EnrEntrant();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "personalNumber":
                    return obj.getPersonalNumber();
                case "registrationDate":
                    return obj.getRegistrationDate();
                case "state":
                    return obj.getState();
                case "archival":
                    return obj.isArchival();
                case "additionalInfo":
                    return obj.getAdditionalInfo();
                case "passArmy":
                    return obj.isPassArmy();
                case "beginArmy":
                    return obj.getBeginArmy();
                case "endArmy":
                    return obj.getEndArmy();
                case "endArmyYear":
                    return obj.getEndArmyYear();
                case "passMedicalEducation":
                    return obj.isPassMedicalEducation();
                case "needSpecialExamConditions":
                    return obj.isNeedSpecialExamConditions();
                case "specialExamConditionsDetails":
                    return obj.getSpecialExamConditionsDetails();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrEnrollmentCampaign) value);
                    return;
                case "personalNumber":
                    obj.setPersonalNumber((String) value);
                    return;
                case "registrationDate":
                    obj.setRegistrationDate((Date) value);
                    return;
                case "state":
                    obj.setState((EnrEntrantState) value);
                    return;
                case "archival":
                    obj.setArchival((Boolean) value);
                    return;
                case "additionalInfo":
                    obj.setAdditionalInfo((String) value);
                    return;
                case "passArmy":
                    obj.setPassArmy((Boolean) value);
                    return;
                case "beginArmy":
                    obj.setBeginArmy((Date) value);
                    return;
                case "endArmy":
                    obj.setEndArmy((Date) value);
                    return;
                case "endArmyYear":
                    obj.setEndArmyYear((Integer) value);
                    return;
                case "passMedicalEducation":
                    obj.setPassMedicalEducation((Boolean) value);
                    return;
                case "needSpecialExamConditions":
                    obj.setNeedSpecialExamConditions((Boolean) value);
                    return;
                case "specialExamConditionsDetails":
                    obj.setSpecialExamConditionsDetails((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                        return true;
                case "personalNumber":
                        return true;
                case "registrationDate":
                        return true;
                case "state":
                        return true;
                case "archival":
                        return true;
                case "additionalInfo":
                        return true;
                case "passArmy":
                        return true;
                case "beginArmy":
                        return true;
                case "endArmy":
                        return true;
                case "endArmyYear":
                        return true;
                case "passMedicalEducation":
                        return true;
                case "needSpecialExamConditions":
                        return true;
                case "specialExamConditionsDetails":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return true;
                case "personalNumber":
                    return true;
                case "registrationDate":
                    return true;
                case "state":
                    return true;
                case "archival":
                    return true;
                case "additionalInfo":
                    return true;
                case "passArmy":
                    return true;
                case "beginArmy":
                    return true;
                case "endArmy":
                    return true;
                case "endArmyYear":
                    return true;
                case "passMedicalEducation":
                    return true;
                case "needSpecialExamConditions":
                    return true;
                case "specialExamConditionsDetails":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return EnrEnrollmentCampaign.class;
                case "personalNumber":
                    return String.class;
                case "registrationDate":
                    return Date.class;
                case "state":
                    return EnrEntrantState.class;
                case "archival":
                    return Boolean.class;
                case "additionalInfo":
                    return String.class;
                case "passArmy":
                    return Boolean.class;
                case "beginArmy":
                    return Date.class;
                case "endArmy":
                    return Date.class;
                case "endArmyYear":
                    return Integer.class;
                case "passMedicalEducation":
                    return Boolean.class;
                case "needSpecialExamConditions":
                    return Boolean.class;
                case "specialExamConditionsDetails":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrEntrant> _dslPath = new Path<EnrEntrant>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrEntrant");
    }
            

    /**
     * @return ПК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrant#getEnrollmentCampaign()
     */
    public static EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Личный номер. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrant#getPersonalNumber()
     */
    public static PropertyPath<String> personalNumber()
    {
        return _dslPath.personalNumber();
    }

    /**
     * @return Дата добавления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrant#getRegistrationDate()
     */
    public static PropertyPath<Date> registrationDate()
    {
        return _dslPath.registrationDate();
    }

    /**
     * Обновляется демоном IEnrEntrantStateDaemonDao#doUpdateEntrantStatus.
     *
     * @return Состояние.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrant#getState()
     */
    public static EnrEntrantState.Path<EnrEntrantState> state()
    {
        return _dslPath.state();
    }

    /**
     * @return Архивный. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrant#isArchival()
     */
    public static PropertyPath<Boolean> archival()
    {
        return _dslPath.archival();
    }

    /**
     * @return Дополнительная информация.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrant#getAdditionalInfo()
     */
    public static PropertyPath<String> additionalInfo()
    {
        return _dslPath.additionalInfo();
    }

    /**
     * @return Служил в армии. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrant#isPassArmy()
     */
    public static PropertyPath<Boolean> passArmy()
    {
        return _dslPath.passArmy();
    }

    /**
     * @return Дата начала службы.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrant#getBeginArmy()
     */
    public static PropertyPath<Date> beginArmy()
    {
        return _dslPath.beginArmy();
    }

    /**
     * @return Дата окончания службы.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrant#getEndArmy()
     */
    public static PropertyPath<Date> endArmy()
    {
        return _dslPath.endArmy();
    }

    /**
     * @return Год увольнения в запас.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrant#getEndArmyYear()
     */
    public static PropertyPath<Integer> endArmyYear()
    {
        return _dslPath.endArmyYear();
    }

    /**
     * @return Закончил медицинское (фармацевтическое) образовательное учреждение. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrant#isPassMedicalEducation()
     */
    public static PropertyPath<Boolean> passMedicalEducation()
    {
        return _dslPath.passMedicalEducation();
    }

    /**
     * @return Нуждается в спец. условиях для прохождения ВИ. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrant#isNeedSpecialExamConditions()
     */
    public static PropertyPath<Boolean> needSpecialExamConditions()
    {
        return _dslPath.needSpecialExamConditions();
    }

    /**
     * @return Примечание по спец. условиям для прохождения ВИ.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrant#getSpecialExamConditionsDetails()
     */
    public static PropertyPath<String> specialExamConditionsDetails()
    {
        return _dslPath.specialExamConditionsDetails();
    }

    public static class Path<E extends EnrEntrant> extends PersonRole.Path<E>
    {
        private EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<String> _personalNumber;
        private PropertyPath<Date> _registrationDate;
        private EnrEntrantState.Path<EnrEntrantState> _state;
        private PropertyPath<Boolean> _archival;
        private PropertyPath<String> _additionalInfo;
        private PropertyPath<Boolean> _passArmy;
        private PropertyPath<Date> _beginArmy;
        private PropertyPath<Date> _endArmy;
        private PropertyPath<Integer> _endArmyYear;
        private PropertyPath<Boolean> _passMedicalEducation;
        private PropertyPath<Boolean> _needSpecialExamConditions;
        private PropertyPath<String> _specialExamConditionsDetails;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return ПК. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrant#getEnrollmentCampaign()
     */
        public EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Личный номер. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrant#getPersonalNumber()
     */
        public PropertyPath<String> personalNumber()
        {
            if(_personalNumber == null )
                _personalNumber = new PropertyPath<String>(EnrEntrantGen.P_PERSONAL_NUMBER, this);
            return _personalNumber;
        }

    /**
     * @return Дата добавления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrant#getRegistrationDate()
     */
        public PropertyPath<Date> registrationDate()
        {
            if(_registrationDate == null )
                _registrationDate = new PropertyPath<Date>(EnrEntrantGen.P_REGISTRATION_DATE, this);
            return _registrationDate;
        }

    /**
     * Обновляется демоном IEnrEntrantStateDaemonDao#doUpdateEntrantStatus.
     *
     * @return Состояние.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrant#getState()
     */
        public EnrEntrantState.Path<EnrEntrantState> state()
        {
            if(_state == null )
                _state = new EnrEntrantState.Path<EnrEntrantState>(L_STATE, this);
            return _state;
        }

    /**
     * @return Архивный. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrant#isArchival()
     */
        public PropertyPath<Boolean> archival()
        {
            if(_archival == null )
                _archival = new PropertyPath<Boolean>(EnrEntrantGen.P_ARCHIVAL, this);
            return _archival;
        }

    /**
     * @return Дополнительная информация.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrant#getAdditionalInfo()
     */
        public PropertyPath<String> additionalInfo()
        {
            if(_additionalInfo == null )
                _additionalInfo = new PropertyPath<String>(EnrEntrantGen.P_ADDITIONAL_INFO, this);
            return _additionalInfo;
        }

    /**
     * @return Служил в армии. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrant#isPassArmy()
     */
        public PropertyPath<Boolean> passArmy()
        {
            if(_passArmy == null )
                _passArmy = new PropertyPath<Boolean>(EnrEntrantGen.P_PASS_ARMY, this);
            return _passArmy;
        }

    /**
     * @return Дата начала службы.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrant#getBeginArmy()
     */
        public PropertyPath<Date> beginArmy()
        {
            if(_beginArmy == null )
                _beginArmy = new PropertyPath<Date>(EnrEntrantGen.P_BEGIN_ARMY, this);
            return _beginArmy;
        }

    /**
     * @return Дата окончания службы.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrant#getEndArmy()
     */
        public PropertyPath<Date> endArmy()
        {
            if(_endArmy == null )
                _endArmy = new PropertyPath<Date>(EnrEntrantGen.P_END_ARMY, this);
            return _endArmy;
        }

    /**
     * @return Год увольнения в запас.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrant#getEndArmyYear()
     */
        public PropertyPath<Integer> endArmyYear()
        {
            if(_endArmyYear == null )
                _endArmyYear = new PropertyPath<Integer>(EnrEntrantGen.P_END_ARMY_YEAR, this);
            return _endArmyYear;
        }

    /**
     * @return Закончил медицинское (фармацевтическое) образовательное учреждение. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrant#isPassMedicalEducation()
     */
        public PropertyPath<Boolean> passMedicalEducation()
        {
            if(_passMedicalEducation == null )
                _passMedicalEducation = new PropertyPath<Boolean>(EnrEntrantGen.P_PASS_MEDICAL_EDUCATION, this);
            return _passMedicalEducation;
        }

    /**
     * @return Нуждается в спец. условиях для прохождения ВИ. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrant#isNeedSpecialExamConditions()
     */
        public PropertyPath<Boolean> needSpecialExamConditions()
        {
            if(_needSpecialExamConditions == null )
                _needSpecialExamConditions = new PropertyPath<Boolean>(EnrEntrantGen.P_NEED_SPECIAL_EXAM_CONDITIONS, this);
            return _needSpecialExamConditions;
        }

    /**
     * @return Примечание по спец. условиям для прохождения ВИ.
     * @see ru.tandemservice.unienr14.entrant.entity.EnrEntrant#getSpecialExamConditionsDetails()
     */
        public PropertyPath<String> specialExamConditionsDetails()
        {
            if(_specialExamConditionsDetails == null )
                _specialExamConditionsDetails = new PropertyPath<String>(EnrEntrantGen.P_SPECIAL_EXAM_CONDITIONS_DETAILS, this);
            return _specialExamConditionsDetails;
        }

        public Class getEntityClass()
        {
            return EnrEntrant.class;
        }

        public String getEntityName()
        {
            return "enrEntrant";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
