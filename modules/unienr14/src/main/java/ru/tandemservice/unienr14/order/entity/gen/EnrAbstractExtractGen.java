package ru.tandemservice.unienr14.order.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unienr14.order.entity.EnrAbstractExtract;
import ru.tandemservice.unienr14.order.entity.EnrAbstractParagraph;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;
import ru.tandemservice.unimv.IAbstractDocument;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Абстрактная выписка на абитуриента (2014)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrAbstractExtractGen extends EntityBase
 implements IAbstractDocument{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14.order.entity.EnrAbstractExtract";
    public static final String ENTITY_NAME = "enrAbstractExtract";
    public static final int VERSION_HASH = -1531820682;
    private static IEntityMeta ENTITY_META;

    public static final String L_PARAGRAPH = "paragraph";
    public static final String P_NUMBER = "number";
    public static final String P_CREATE_DATE = "createDate";
    public static final String L_STATE = "state";
    public static final String P_CANCELLED = "cancelled";
    public static final String P_SHORT_TITLE = "shortTitle";

    private EnrAbstractParagraph _paragraph;     // Абстрактный параграф на абитуриентов (2014)
    private Integer _number;     // Номер выписки в параграфе
    private Date _createDate;     // Дата формирования
    private ExtractStates _state;     // Состояние выписки
    private boolean _cancelled;     // Отменена

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Абстрактный параграф на абитуриентов (2014).
     */
    public EnrAbstractParagraph getParagraph()
    {
        return _paragraph;
    }

    /**
     * @param paragraph Абстрактный параграф на абитуриентов (2014).
     */
    public void setParagraph(EnrAbstractParagraph paragraph)
    {
        dirty(_paragraph, paragraph);
        _paragraph = paragraph;
    }

    /**
     * @return Номер выписки в параграфе.
     */
    public Integer getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер выписки в параграфе.
     */
    public void setNumber(Integer number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getCreateDate()
    {
        return _createDate;
    }

    /**
     * @param createDate Дата формирования. Свойство не может быть null.
     */
    public void setCreateDate(Date createDate)
    {
        dirty(_createDate, createDate);
        _createDate = createDate;
    }

    /**
     * @return Состояние выписки. Свойство не может быть null.
     */
    @NotNull
    public ExtractStates getState()
    {
        return _state;
    }

    /**
     * @param state Состояние выписки. Свойство не может быть null.
     */
    public void setState(ExtractStates state)
    {
        dirty(_state, state);
        _state = state;
    }

    /**
     * Для отмены данной выписки, поскольку состояния выписок сейчас этого не поддерживают. ):
     *
     * @return Отменена. Свойство не может быть null.
     */
    @NotNull
    public boolean isCancelled()
    {
        return _cancelled;
    }

    /**
     * @param cancelled Отменена. Свойство не может быть null.
     */
    public void setCancelled(boolean cancelled)
    {
        dirty(_cancelled, cancelled);
        _cancelled = cancelled;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrAbstractExtractGen)
        {
            setParagraph(((EnrAbstractExtract)another).getParagraph());
            setNumber(((EnrAbstractExtract)another).getNumber());
            setCreateDate(((EnrAbstractExtract)another).getCreateDate());
            setState(((EnrAbstractExtract)another).getState());
            setCancelled(((EnrAbstractExtract)another).isCancelled());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrAbstractExtractGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrAbstractExtract.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("EnrAbstractExtract is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "paragraph":
                    return obj.getParagraph();
                case "number":
                    return obj.getNumber();
                case "createDate":
                    return obj.getCreateDate();
                case "state":
                    return obj.getState();
                case "cancelled":
                    return obj.isCancelled();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "paragraph":
                    obj.setParagraph((EnrAbstractParagraph) value);
                    return;
                case "number":
                    obj.setNumber((Integer) value);
                    return;
                case "createDate":
                    obj.setCreateDate((Date) value);
                    return;
                case "state":
                    obj.setState((ExtractStates) value);
                    return;
                case "cancelled":
                    obj.setCancelled((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "paragraph":
                        return true;
                case "number":
                        return true;
                case "createDate":
                        return true;
                case "state":
                        return true;
                case "cancelled":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "paragraph":
                    return true;
                case "number":
                    return true;
                case "createDate":
                    return true;
                case "state":
                    return true;
                case "cancelled":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "paragraph":
                    return EnrAbstractParagraph.class;
                case "number":
                    return Integer.class;
                case "createDate":
                    return Date.class;
                case "state":
                    return ExtractStates.class;
                case "cancelled":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrAbstractExtract> _dslPath = new Path<EnrAbstractExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrAbstractExtract");
    }
            

    /**
     * @return Абстрактный параграф на абитуриентов (2014).
     * @see ru.tandemservice.unienr14.order.entity.EnrAbstractExtract#getParagraph()
     */
    public static EnrAbstractParagraph.Path<EnrAbstractParagraph> paragraph()
    {
        return _dslPath.paragraph();
    }

    /**
     * @return Номер выписки в параграфе.
     * @see ru.tandemservice.unienr14.order.entity.EnrAbstractExtract#getNumber()
     */
    public static PropertyPath<Integer> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrAbstractExtract#getCreateDate()
     */
    public static PropertyPath<Date> createDate()
    {
        return _dslPath.createDate();
    }

    /**
     * @return Состояние выписки. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrAbstractExtract#getState()
     */
    public static ExtractStates.Path<ExtractStates> state()
    {
        return _dslPath.state();
    }

    /**
     * Для отмены данной выписки, поскольку состояния выписок сейчас этого не поддерживают. ):
     *
     * @return Отменена. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrAbstractExtract#isCancelled()
     */
    public static PropertyPath<Boolean> cancelled()
    {
        return _dslPath.cancelled();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.order.entity.EnrAbstractExtract#getShortTitle()
     */
    public static SupportedPropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    public static class Path<E extends EnrAbstractExtract> extends EntityPath<E>
    {
        private EnrAbstractParagraph.Path<EnrAbstractParagraph> _paragraph;
        private PropertyPath<Integer> _number;
        private PropertyPath<Date> _createDate;
        private ExtractStates.Path<ExtractStates> _state;
        private PropertyPath<Boolean> _cancelled;
        private SupportedPropertyPath<String> _shortTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Абстрактный параграф на абитуриентов (2014).
     * @see ru.tandemservice.unienr14.order.entity.EnrAbstractExtract#getParagraph()
     */
        public EnrAbstractParagraph.Path<EnrAbstractParagraph> paragraph()
        {
            if(_paragraph == null )
                _paragraph = new EnrAbstractParagraph.Path<EnrAbstractParagraph>(L_PARAGRAPH, this);
            return _paragraph;
        }

    /**
     * @return Номер выписки в параграфе.
     * @see ru.tandemservice.unienr14.order.entity.EnrAbstractExtract#getNumber()
     */
        public PropertyPath<Integer> number()
        {
            if(_number == null )
                _number = new PropertyPath<Integer>(EnrAbstractExtractGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrAbstractExtract#getCreateDate()
     */
        public PropertyPath<Date> createDate()
        {
            if(_createDate == null )
                _createDate = new PropertyPath<Date>(EnrAbstractExtractGen.P_CREATE_DATE, this);
            return _createDate;
        }

    /**
     * @return Состояние выписки. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrAbstractExtract#getState()
     */
        public ExtractStates.Path<ExtractStates> state()
        {
            if(_state == null )
                _state = new ExtractStates.Path<ExtractStates>(L_STATE, this);
            return _state;
        }

    /**
     * Для отмены данной выписки, поскольку состояния выписок сейчас этого не поддерживают. ):
     *
     * @return Отменена. Свойство не может быть null.
     * @see ru.tandemservice.unienr14.order.entity.EnrAbstractExtract#isCancelled()
     */
        public PropertyPath<Boolean> cancelled()
        {
            if(_cancelled == null )
                _cancelled = new PropertyPath<Boolean>(EnrAbstractExtractGen.P_CANCELLED, this);
            return _cancelled;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unienr14.order.entity.EnrAbstractExtract#getShortTitle()
     */
        public SupportedPropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new SupportedPropertyPath<String>(EnrAbstractExtractGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

        public Class getEntityClass()
        {
            return EnrAbstractExtract.class;
        }

        public String getEntityName()
        {
            return "enrAbstractExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getShortTitle();
}
