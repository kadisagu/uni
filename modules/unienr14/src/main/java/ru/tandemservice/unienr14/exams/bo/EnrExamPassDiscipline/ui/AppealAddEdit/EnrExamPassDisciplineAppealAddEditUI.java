/* $Id:$ */
package ru.tandemservice.unienr14.exams.bo.EnrExamPassDiscipline.ui.AppealAddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.exams.bo.EnrExamPassDiscipline.EnrExamPassDisciplineManager;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassAppeal;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;

/**
 * @author oleyba
 * @since 6/24/13
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "appeal.id"),
    @Bind(key = EnrExamPassDisciplineAppealAddEditUI.BIND_DSICIPLINE_ID, binding = "disciplineId")
})
public class EnrExamPassDisciplineAppealAddEditUI extends UIPresenter
{
    public static final String BIND_DSICIPLINE_ID = "disciplineId";

    private EnrExamPassAppeal appeal = new EnrExamPassAppeal();
    private Long disciplineId;

    @Override
    public void onComponentRefresh()
    {
        if (getAppeal().getId() != null) {
            setAppeal(IUniBaseDao.instance.get().getNotNull(EnrExamPassAppeal.class, getAppeal().getId()));
        }
        else if (getDisciplineId() != null) {
            getAppeal().setExamPassDiscipline(IUniBaseDao.instance.get().getNotNull(EnrExamPassDiscipline.class, getDisciplineId()));
        }
        
        if (getAppeal() != null && getAppeal().getExamPassDiscipline() != null && (getAppeal().getExamPassDiscipline().getMarkAsLong() == null || getAppeal().getExamPassDiscipline().getMarkDate() == null)) {
            throw new ApplicationException("Чтобы добавить данные об апелляции, необходимо ввести данные о результате сдачи дисциплины и дате сдачи.");
        }
    }

    @Override
    public void onComponentRender() {
        // ContextLocal.beginPageTitlePart(_uiConfig.getProperty(isEditForm() ? "ui.sticker.edit" : "ui.sticker.add"));
    }

    public void onClickApply() {
        EnrExamPassDisciplineManager.instance().dao().saveOrUpdateAppeal(getAppeal());
        deactivate();
    }

    public boolean isEditForm() {
        return getAppeal().getId() != null;
    }

    // getters and setters

    public EnrExamPassAppeal getAppeal()
    {
        return appeal;
    }

    public void setAppeal(EnrExamPassAppeal appeal)
    {
        this.appeal = appeal;
    }

    public Long getDisciplineId()
    {
        return disciplineId;
    }

    public void setDisciplineId(Long disciplineId)
    {
        this.disciplineId = disciplineId;
    }
}