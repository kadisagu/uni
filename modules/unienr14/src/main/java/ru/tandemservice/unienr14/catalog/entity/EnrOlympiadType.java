package ru.tandemservice.unienr14.catalog.entity;

import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IPrioritizedCatalogItem;
import ru.tandemservice.unienr14.catalog.entity.gen.EnrOlympiadTypeGen;

/**
 * Тип олимпиады
 */
public class EnrOlympiadType extends EnrOlympiadTypeGen implements IDynamicCatalogItem, IPrioritizedCatalogItem
{
    public static EntityComboDataSourceHandler defaultSelectDSHandler(String ownerId)
    {
        return new EntityComboDataSourceHandler(ownerId, EnrOlympiadType.class)
                .titleProperty(EnrOlympiadType.P_TITLE)
                .filter(EnrOlympiadType.title())
                .order(EnrOlympiadType.priority());
    }
}