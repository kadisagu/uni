/**
 *$Id: EnrExamSetSearchDSHandler.java 32113 2014-01-27 09:14:01Z hudson $
 */
package ru.tandemservice.unienr14.exams.bo.EnrExamSet.logic;

import org.apache.commons.lang.mutable.MutableInt;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.EnrExamSetManager;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.ui.List.EnrExamSetListUI;
import ru.tandemservice.unienr14.exams.entity.EnrExamSet;
import ru.tandemservice.unienr14.exams.entity.EnrExamSetElement;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 16.04.13
 */
public class EnrExamSetSearchDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public EnrExamSetSearchDSHandler(String ownerId)
    {
        super(ownerId, EnrExamSet.class);
    }

    protected static final String EXAM_SET_2__ELEMENTS_MAP = "examSet2ElementsMap";

    public static final String PROP_TITLE = "examSetElementRawTitle";
    public static final String PROP_COMPETITION_COUNT = "cgCount";

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final EnrEnrollmentCampaign enrCamp = context.get(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN);

        if (enrCamp == null)
            return ListOutputBuilder.get(input, new ArrayList()).build();

        final DQLSelectBuilder builder = new DQLSelectBuilder()
            .fromEntity(EnrExamSet.class, "s").column("s")
            .where(eq(property(EnrExamSet.enrollmentCampaign().fromAlias("s")), value(enrCamp)));

        final DSOutput output = DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build();

        final Map<EnrExamSet,List<EnrExamSetElement>> examSet2ElementsMap = EnrExamSetManager.instance().dao().examSet2ElementsMap(output.<EnrExamSet>getRecordList().toArray(new EnrExamSet[output.getRecordList().size()]));
        context.put(EXAM_SET_2__ELEMENTS_MAP, examSet2ElementsMap);

        output.ordering(EnrExamSet.getComparator(examSet2ElementsMap));

        wrap(output, context);

        return output;
    }

    protected List<DataWrapper> wrap(final DSOutput output, ExecutionContext context)
    {
        final Map<Long, MutableInt> examSet2CompCountMap = SafeMap.get(MutableInt.class);
        BatchUtils.execute(output.getRecordIds(), 128, new BatchUtils.Action<Long>()
        {
            @Override
            public void execute(Collection<Long> elements)
            {
                final List<Long> competitionList = new DQLSelectBuilder().fromEntity(EnrCompetition.class, "g")
                    .column(property(EnrCompetition.examSetVariant().examSet().id().fromAlias("g")))
                    .where(in(property(EnrCompetition.examSetVariant().examSet().id().fromAlias("g")), elements))
                    .createStatement(context.getSession()).list();

                for (Long id : competitionList)
                    examSet2CompCountMap.get(id).increment();
            }
        });

        final Map<EnrExamSet,List<EnrExamSetElement>> examSet2ElementsMap = context.get(EXAM_SET_2__ELEMENTS_MAP);

        for (DataWrapper wrapper : DataWrapper.wrap(output))
        {
            wrapper.setProperty(PROP_COMPETITION_COUNT, examSet2CompCountMap.get(wrapper.getId()).intValue());
            wrapper.setProperty(PROP_TITLE, Arrays.asList(EnrExamSetManager.instance().examSetElementTitle(examSet2ElementsMap.get(wrapper.<EnrExamSet>getWrapped()), "delim").split("delim")));
        }

        return output.getRecordList();
    }
}
