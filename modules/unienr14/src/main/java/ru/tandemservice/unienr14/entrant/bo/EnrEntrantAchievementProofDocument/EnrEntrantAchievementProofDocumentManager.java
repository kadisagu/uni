/* $Id:$ */
package ru.tandemservice.unienr14.entrant.bo.EnrEntrantAchievementProofDocument;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrantAchievementProofDocument.logic.EnrEntrantAchievementProofDocumentDSHandler;

/**
 * User: nvankov
 * Date: 5/30/16
 */
@Configuration
public class EnrEntrantAchievementProofDocumentManager extends BusinessObjectManager
{
    public static EnrEntrantAchievementProofDocumentManager instance() { return instance(EnrEntrantAchievementProofDocumentManager.class); }

    @Bean
    public IDefaultComboDataSourceHandler achievementProofDocumentDSHandler()
    {
        return new EnrEntrantAchievementProofDocumentDSHandler(getName());
    }
}
